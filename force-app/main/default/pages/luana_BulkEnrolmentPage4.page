<!--
    Developer: WDCi (Leo)
    Date: 21/02/2017
    Task#: CASM Bulk Enrolment Wizard - Page 4 [Summary of student programs to be generated]
-->
<apex:page standardStylesheets="false" applyBodyTag="false" docType="html-5.0" controller="luana_BulkEnrolmentController">

    <html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="x-ua-compatible" content="ie=edge" />        
        <apex:slds />        
    </head>
        
    <body>
        
        <div class="slds-scope">        
            
            <div class="slds-page-header" role="banner" style="margin-bottom:20px">
                <div class="slds-media">
                    <div class="slds-media__body">
                        <p class="slds-page-header__title slds-align-middle slds-text-heading--large"><apex:outputText value="{!headerTitle}" escape="false"/></p>
                        <p class="slds-text-body--small" title="Bulk Enrolment Wizard">
                            <apex:outputText value="{!headerDescription}" escape="false"/>
                            <apex:outputText value="{!headerDescriptionPage4}" escape="false"/>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="slds-media__body">
            
                <apex:pageMessages escape="false"/>
                <apex:form >
                    
                    <apex:outputPanel rendered="{!NOT(ERROR)}">
                    
                        <!-- Wizard Navigation buttons -->
                        <apex:pageBlock >
                            <div style="margin-left:20%">
                                <apex:commandButton value="Cancel" action="{!cancel}" styleClass="slds-button slds-button--neutral" style="margin-right:3%" disabled="{!enableDone}"/>
                                <apex:commandButton value="Back" action="{!backToPage3}" styleClass="slds-button slds-button--neutral" style="margin-right:3%" disabled="{!enableDone}"/>
                                <apex:commandButton value="Confirm" action="{!confirmEnrollment}" styleClass="slds-button slds-button--neutral"/>
                                <apex:commandButton value="Done" action="{!pageDone}" styleClass="slds-button slds-button--neutral" rendered="{!enableDone}"/>
                            </div>                            
                        </apex:pageBlock>
                    
                        <!-- Checkbox Panel -->
                        <apex:pageBlock >
                            <apex:outputPanel id="welcomeEmailCheckboxPanel">
                                <apex:pageBlockSection columns="1">
                                    <!-- <p class="slds-text-body--regular" title="Search Result" style="margin-bottom:10px;margin-left:10%">CASM Visibility: {!activeSubCourse.Visibility__c}</p> -->
                                    <apex:outputField value="{!activeSubCourse.Visibility__c}" label="CASM Visibility"/>
                                    <!-- <p class="slds-text-body--small" title="Test" style="margin-bottom:10px">Disable flag: {!disableWelcomeEmailCheckbox}</p> -->
                                    <apex:inputCheckbox value="{!welcomeEmailToBeSent}" id="welcomeEmailCheckbox" style="margin-top:3px;" label="Send Welcome Email?" disabled="{!disableWelcomeEmailCheckbox}"/>                                   
                                </apex:pageBlockSection>
                            </apex:outputPanel>
                        
                            <apex:outputPanel id="autoAttendanceCheckboxPanel">
                                <apex:pageBlockSection columns="1">
                                    <apex:pageBlockSectionItem >                                     
                                        <span class="helpButtonOn" id="Name-_help">
                                            <label>Auto Assign Employees to Current Session/s</label>
                                            <img src="/s.gif" alt="" class="helpOrb" title="" style="height:15px"/>
                                            <script type="text/javascript">sfdcPage.setHelp('Name', '{!autoAttendanceHelpText}');</script>
                                        </span>
                                        <apex:inputCheckbox value="{!autoAttendance}" id="autoAttendanceCheckbox" style="margin-top:3px;" label="Auto Assign Employees to Current Session/s"/>
                                    </apex:pageBlockSectionItem>
                                </apex:pageBlockSection>
                            </apex:outputPanel>
                        </apex:pageBlock>
                        
                        <!-- selected Employee/Student confirmation -->
                        <apex:pageBlock >
                            <apex:outputPanel id="employeeSelectedResult">
                                <p class="slds-text-body--small" title="Search Result" style="margin-bottom:10px">The following employee/s will be enroled into {!activeSubCourse.Name} with Employment Token: <b>{!employmentTokenCode}</b></p>
                                <table>
                                    <thead>
                                        <tr style="margin-bottom:20px;">                                            
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Member Id</th>
                                            <th>Customer Id</th>
                                            <th>Email Address</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <apex:repeat value="{!woToCreate}" var="tableEntry">
                                            <tr>
                                                <td> <apex:outputText value="{!tableEntry.storedObject.FirstName}"/> </td>
                                                <td> <apex:outputText value="{!tableEntry.storedObject.LastName}"/> </td>
                                                <td> <apex:outputText value="{!tableEntry.storedObject.Member_ID__c}"/> </td>
                                                <td> <apex:outputText value="{!tableEntry.storedObject.Customer_ID__c}"/> </td>
                                                <td> <apex:outputText value="{!tableEntry.storedObject.PersonEmail}"/> </td>
                                            </tr>
                                        </apex:repeat>
                                    </tbody>
                                </table>
                            </apex:outputPanel>
                        </apex:pageBlock>                                                                        
                        
                        <!-- Wizard Navigation buttons -->
                        <apex:pageBlock >
                            <div style="margin-left:20%">
                                <apex:commandButton value="Cancel" action="{!cancel}" styleClass="slds-button slds-button--neutral" style="margin-right:3%" disabled="{!enableDone}"/>
                                <apex:commandButton value="Back" action="{!backToPage3}" styleClass="slds-button slds-button--neutral" style="margin-right:3%" disabled="{!enableDone}"/>
                                <apex:commandButton value="Confirm" action="{!confirmEnrollment}" styleClass="slds-button slds-button--neutral"/>
                                <apex:commandButton value="Done" action="{!pageDone}" styleClass="slds-button slds-button--neutral" rendered="{!enableDone}"/>
                            </div>                            
                        </apex:pageBlock>
                        
                    </apex:outputPanel>
                    
                </apex:form>
                
            </div>
            
        </div>
        
    </body>
    
    <footer align="center">
        <!-- © Copyright 2017 -->
        {!VFFooter}
    </footer>
    
    </html>
</apex:page>