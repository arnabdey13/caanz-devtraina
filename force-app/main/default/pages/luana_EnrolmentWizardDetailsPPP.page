<!--
    Developer: WDCi (Lean)
    Development Date: 05/04/2016
    Task #: Enrollment wizard - Controller for Details PPP
    
    Change History:
    LCA-819 05/09/2016 WDCi - KH: Update PPP detail page
    pppr06 14/09/2016 WDCi - KH: Remove workshop preference selection from this page 
-->

<apex:page sidebar="false" showHeader="false" title="Enrolment Wizard - Details" controller="luana_EnrolmentWizardController" standardStylesheets="false" applyBodyTag="true" applyHtmlTag="false" action="{!validateAccess}"
    extensions="luana_EnrolmentWizardLandingController,luana_EnrolmentWizardProgramController,luana_EnrolmentWizardCourseController,luana_EnrolmentWizardSubjectController,luana_EnrolmentWizardDetailsDEController,luana_EnrolmentWizardDetailsCPController,luana_EnrolmentWizardDetailsCMController,luana_EnrolmentWizardDetailsMCController,luana_EnrolmentWizardDetailsPPController,luana_EnrolmentWizardDetailsMCController,luana_EnrolmentWizardDetailsFDController,luana_EnrolmentWizardDetailsNAController,luana_EnrolmentWizardSummaryController,luana_EnrolmentWizardDetailsLLController"
>
    
    <head>
        <link rel="icon shortcut" href="{!URLFOR($Resource.luana_CA_Websource_v2, 'images/logo.ico')}" />
    </head>
    
    <style>
    
        .content-container .main-content table.detail-two-column .select-wrapper.arrow:after {
            left: 160%;
            height: 34px!important;
        }
        .content-container .main-content table.detail-two-column .select-wrapper select {
            width: 180% !important;
        }
    </style>
    
    <apex:composition template="{!templateName}">
        <apex:define name="body">
            
            <div class="row-fluid" style="width:100%">
                <apex:form >
                    <apex:outputPanel id="forrender">
                        <apex:Messages />
                    </apex:outputPanel>

                    <apex:pageBlock mode="maindetail" id="mainPageBlock" >
                        <div class="table-header">
                            {!selectedCourse.Name} - Details
                        </div>
                        
                        <div class="table-subheader">Personal Details</div>
                        <table class="detail-two-column alt">
                            <tr>
                                <td>First Name: </td>
                                <td><apex:outputField value="{!custUser.Contact.Account.FirstName}"/></td>
                            </tr>
                            <tr>
                                <td>Preferred Name: </td>
                                <td><apex:outputField value="{!custUser.Contact.Account.Preferred_Name__c}"/></td>
                            </tr>
                            <tr>
                                <td>Last Name: </td>
                                <td><apex:outputField value="{!custUser.Contact.Account.LastName}"/></td>
                            </tr>
                            <tr>
                                <td>Course: </td>
                                <td>
                                    <apex:outputField value="{!selectedCourse.Name}" label="Module"/>
                                </td>
                            </tr>
                            <!--<tr style="display:{!isRegional == true, '', 'none')}">
                                <td>Are you working in regional areas (125km from location) and/or traveling from interstate?</td>
                                <td/>
                            </tr>-->
                            
                            <!--WDCi - KH pppr06 change on 14/09/2016
                            <tr>
                                <td class="req">Please select your workshop location preference</td>
                                <td>
                                    <div class="select-wrapper arrow">
                                        <apex:actionRegion >
                                            <apex:selectList value="{!selectedWorkshopLocation1Id}" multiselect="false" size="1" onchange="getDayOfWeekPref1()">
                                                 <apex:selectOptions value="{!Workshop1LocationOptions}" />
                                            </apex:selectList>
                                            
                                        </apex:actionRegion>
                                    </div>
                                </td>
                            </tr>
                            -->
                            
                            <tr>
                                <td>Terms and Conditions</td>
                                <td>
                                    <a href="{!domainUrl}/servlet/servlet.FileDownload?file={!TermAndCond.id}" target="_blank">View</a>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    I agree that: <Strong style="color: #FF0000;">*</Strong>
                                    <ol style="list-style:inherit;padding:20px;">
                                        <li>
                                            I have read and agree to be bound by the above Terms and Conditions for 
                                            enrolling in the Public Practice Program, including the privacy statement and the <a href="http://www.charteredaccountantsanz.com/privacy" target="_blank">Privacy 
                                            Policy</a> and
                                        </li>
                                        <li>
                                            I consent to this acceptance being relied on as evidence of my intention to be 
                                            so bound.
                                        </li>
                                    </ol>
                                </td>
                                <td >
                                    <apex:inputField value="{!workingStudentProgram.Accept_terms_and_conditions__c}" />
                                </td>
                            </tr>
                            <tr>
                                <td>Course Fee: </td>
                                <td>
                                    $ {!courseFee}
                                </td>
                            </tr>
                        </table>
                    </apex:pageBlock>
                    
                    <div class="table-footer">
                        <apex:commandButton value="Back" action="{!detailsPPBack}" styleClass="btn btn-default navigation prev-btn" rendered="{!formEnabled}" />
                        <apex:commandButton value="Next" action="{!detailsPPNext}" styleClass="btn btn-default view next" rendered="{!formEnabled}" />
                    </div>
                </apex:form>
            </div>
        </apex:define>
    </apex:composition>
    
</apex:page>