<!-- 
	Developer: WDCi (Lean)
    Date: 15/Aug/2016
    Task #: LCA-802 Luana implementation - Wizard for CASM workshop allocation
 -->
<apex:page standardStylesheets="false" applyBodyTag="false" docType="html-5.0" standardController="LuanaSMS__Course__c" extensions="luana_MasterclassAllocationWizardCtrl" title="Masterclass Allocation Wizard">
	<html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <head>
            <apex:stylesheet value="{!URLFOR($Resource.luana_SFDC_Lighting_Design, 'assets/styles/salesforce-lightning-design-system-vf.css')}" />
        </head>
        <body>
            <div class="slds">
                <div class="slds-page-header" role="banner" style="margin-bottom:20px">
                     <div class="slds-media">
                         <div class="slds-media__body">
                             <p class="slds-page-header__title slds-align-middle slds-text-heading--large">Masterclass Allocation Wizard</p>
                             <p class="slds-text-body--small" title="Student Session Allocation Wizard">
                                 This wizard allows you to mass allocate student enrolment to masterclass workshop sessions.
                             </p>
                         </div>
                     </div>
                </div>
                <apex:form id="theform">
                    <apex:pageMessages id="wizardmessage" escape="false" />
                    
                    <apex:pageBlock title="{!course.Name}" mode="view" id="wizardblock">
                        
                        <apex:pageBlockButtons >
                            <apex:commandButton value="Allocate" action="{!doSave}" disabled="{!NOT(enabled)}" styleClass="slds-button slds-button--brand"/>
                            <apex:commandButton value="Done" action="{!cancel}" styleClass="slds-button slds-button--brand"/>
                        </apex:pageBlockButtons>
                        <apex:pageBlockSection collapsible="true" columns="1" title="" rendered="{!IF(sessionErrorRecords.size > 0, true, false)}" >
                            <apex:pageBlockSectionItem >
                            <apex:outputPanel >
                                <div class="slds-card">
                                    <div class="slds-card__header slds-grid">
                                        <div class="slds-media slds-media--center slds-has-flexi-truncate">
                                            <div class="slds-media__body">
                                                The following session/s failed to be created. Please review:
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slds-card__body table-responsive">
                                        <table class="slds-table slds-table--bordered">
                                            <tr class="slds-text-heading--label">
                                                <th class="slds-cell-shrink"><div class="slds-truncate">Session</div></th>
                                                <th class="slds-cell-shrink"><div class="slds-truncate">Error</div></th>
                                            </tr>
                                            <apex:repeat value="{!sessionErrorRecords}" var="sessionErrorRecord">
                                                <tr class="slds-hint-parent">
                                                    <td class="slds-cell-shrink" data-label="Select Row"><apex:outputField value="{!sessionErrorRecord.session.Name}"></apex:outputField></td>
                                                    <td class="slds-cell-shrink" data-label="Select Row"><apex:outputText value="{!sessionErrorRecord.errorMessage}"></apex:outputText></td>
                                                </tr>
                                            </apex:repeat>
                                        </table>
                                    </div>
                                </div>
                            </apex:outputPanel>
                            </apex:pageBlockSectionItem>
                        </apex:pageBlockSection>
                        <apex:pageBlockSection collapsible="true" columns="1" title="" rendered="{!IF(attendanceErrorRecords.size > 0, true, false)}" >
                            <apex:pageBlockSectionItem >
                            <apex:outputPanel >
                                <div class="slds-card">
                                    <div class="slds-card__header slds-grid">
                                        <div class="slds-media slds-media--center slds-has-flexi-truncate">
                                            <div class="slds-media__body">
                                                The following enrolment/s failed to be allocated. Please review:
                                            </div>
                                        </div>
                                    </div>
                                    <div class="slds-card__body table-responsive">
                                        <table class="slds-table slds-table--bordered">
                                            <tr class="slds-text-heading--label">
                                                <th class="slds-cell-shrink"><div class="slds-truncate">Student Program</div></th>
                                                <th class="slds-cell-shrink"><div class="slds-truncate">Session</div></th>
                                                <th class="slds-cell-shrink"><div class="slds-truncate">Error</div></th>
                                            </tr>
                                            <apex:repeat value="{!attendanceErrorRecords}" var="attErrorRecord">
                                                <tr class="slds-hint-parent">
                                                    <td class="slds-cell-shrink" data-label="Select Row"><apex:outputField value="{!attErrorRecord.attendance.LuanaSMS__Student_Program__c}"></apex:outputField></td>
                                                    <td class="slds-cell-shrink" data-label="Select Row"><apex:outputField value="{!attErrorRecord.attendance.LuanaSMS__Session__c}"></apex:outputField></td>
                                                    <td class="slds-cell-shrink" data-label="Select Row"><apex:outputText value="{!attErrorRecord.errorMessage}"></apex:outputText></td>
                                                </tr>
                                            </apex:repeat>
                                        </table>
                                    </div>
                                </div>
                            </apex:outputPanel>
                            </apex:pageBlockSectionItem>
                        </apex:pageBlockSection>
                        <apex:pageBlockSection title="" collapsible="true" columns="1">
                            <apex:pageBlockSectionItem >
                                <apex:repeat value="{!workshopCourses}" var="wc">
                                    <div class="slds-card">
                                        <div class="slds-card__header slds-grid">
                                            <div class="slds-media slds-media--center slds-has-flexi-truncate">
                                                <div class="slds-media__body" style="font-weight:bold">
                                                    {!wc.displayName}
                                                </div>
                                            </div>
                                            <div class="slds-no-flex" style="font-weight:bold">
                                                Unallocated Student Program/s: {!wc.noOfStudents}
                                            </div>
                                        </div>
                                        <div class="slds-card__body table-responsive">
                                        	<apex:repeat value="{!wc.sessionGroups}" var="wsg">
	                                        	<div style="margin-bottom: 20px">
		                                            <table class="slds-table slds-table--bordered">
		                                            	<tr class="slds-hint-parent">
		                                            		<td style="width:20%"><b>{!wsg.Name}</b></td>
		                                            		<td style="width:20%">
		                                            			<apex:outputPanel rendered="{!wsg.isNew}">
		                                            				<b>Avg. Seat:</b>&nbsp;<apex:inputText rendered="{!wsg.isNew}" value="{!wsg.avgSeat}"></apex:inputText>
		                                            			</apex:outputPanel>
		                                            		</td>
		                                            		<td style="width:20%">
		                                            			<apex:outputPanel rendered="{!wsg.isNew}">
		                                            				<b>Max. Seat:</b>&nbsp;<apex:inputText required="true" rendered="{!wsg.isNew}" value="{!wsg.maxSeat}"></apex:inputText>
		                                            			</apex:outputPanel>
		                                            		</td>
		                                            		<td style="width:20%">
		                                            			<apex:outputPanel rendered="{!wsg.isNew}">
		                                            				<b># of Series:</b>&nbsp;<apex:inputText required="true" rendered="{!wsg.isNew}" value="{!wsg.noOfSeries}"></apex:inputText>
		                                            			</apex:outputPanel>
		                                            		</td>
		                                            		<td style="width:20%; text-align:right">
		                                            			<apex:outputPanel rendered="{!NOT(wsg.isNew)}">
		                                            				<b>Fill Up Session:</b>&nbsp;<apex:inputCheckbox rendered="{!NOT(wsg.isNew)}" value="{!wsg.fillUp}"></apex:inputCheckbox>
		                                            			</apex:outputPanel>
		                                            		</td>
		                                            	</tr>
		                                            </table>
		                                            <apex:outputPanel rendered="{!wsg.hasExistingSession}">
		                                            	<table class="slds-table slds-table--bordered">
			                                                <tr class="slds-text-heading--label">
			                                                    <th class="slds-cell-shrink"><div class="slds-truncate">Session Name</div></th>
			                                                    <th class="slds-cell-shrink"><div class="slds-truncate">Start Time</div></th>
			                                                    <th class="slds-cell-shrink"><div class="slds-truncate">End Time</div></th>
			                                                    <th class="slds-cell-shrink"><div class="slds-truncate">Venue</div></th>
			                                                    <th class="slds-cell-shrink"><div class="slds-truncate">Room</div></th>
			                                                    <th class="slds-cell-shrink"><div class="slds-truncate">Allocation</div></th>
			                                                </tr>
			                                                <apex:repeat value="{!wsg.sessions}" var="cs">
			                                                    <tr class="slds-hint-parent">
			                                                        <td class="slds-cell-shrink" data-label="Select Row"><apex:outputField value="{!cs.LuanaSMS__Session__r.name}"></apex:outputField></td>
			                                                        <td class="slds-cell-shrink" data-label="Select Row"><apex:outputField value="{!cs.LuanaSMS__Session__r.LuanaSMS__Start_Time__c}"></apex:outputField></td>
			                                                        <td class="slds-cell-shrink" data-label="Select Row"><apex:outputField value="{!cs.LuanaSMS__Session__r.LuanaSMS__End_Time__c}"></apex:outputField></td>
			                                                        <td class="slds-cell-shrink" data-label="Select Row"><apex:outputField value="{!cs.LuanaSMS__Session__r.LuanaSMS__Venue__r.Name}"></apex:outputField></td>
			                                                        <td class="slds-cell-shrink" data-label="Select Row"><apex:outputField value="{!cs.LuanaSMS__Session__r.LuanaSMS__Room__r.Name}"></apex:outputField></td>
			                                                        <td class="slds-cell-shrink" data-label="Select Row">{!cs.LuanaSMS__Session__r.LuanaSMS__Number_of_enrolled_students__c}/{!cs.LuanaSMS__Session__r.Maximum_Capacity__c}</td>
			                                                    </tr>
			                                                </apex:repeat>
			                                            </table>
		                                            </apex:outputPanel>
	                                            </div>
	                                            
                                            </apex:repeat>
                                        </div>
                                    </div>
                                </apex:repeat>
                            </apex:pageBlockSectionItem>
                        </apex:pageBlockSection>
                    </apex:pageBlock>
                </apex:form>
            </div>
        </body>
    </html>
</apex:page>