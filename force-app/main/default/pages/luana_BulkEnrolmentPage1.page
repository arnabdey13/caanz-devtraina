<apex:page standardStylesheets="false" applyBodyTag="false" docType="html-5.0" controller="luana_BulkEnrolmentController">
    
    <html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="x-ua-compatible" content="ie=edge" />        
        <apex:slds />        
    </head>
        
    <body>
        <div class="slds-scope">                        
            
            <div class="slds-page-header" role="banner" style="margin-bottom:20px">
                <div class="slds-media">
                    <div class="slds-media__body">
                        <p class="slds-page-header__title slds-align-middle slds-text-heading--large"><apex:outputText value="{!headerTitle}" escape="false"/></p>
                        <p class="slds-text-body--small" title="Bulk Enrolment Wizard">
                            <apex:outputText value="{!headerDescription}" escape="false"/>
                            <apex:outputText value="{!headerDescriptionPage1}" escape="false"/>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="slds-media__body">                      
                
            <apex:pageMessages />
            <apex:form id="myForm" >                                
                
                <apex:outputPanel rendered="{!NOT(ERROR)}">
                
                    <!-- Wizard Navigation buttons -->
                    <apex:pageBlock >
                        <div style="margin-left:20%">
                            <apex:commandButton value="Cancel" action="{!cancel}" styleClass="slds-button slds-button--neutral" style="margin-right:3%"/>                    
                            <apex:commandButton value="Next" action="{!nextToPage2}" styleClass="slds-button slds-button--neutral"/>
                        </div>                        
                    </apex:pageBlock>
                
                    <!-- Course Selection -->
                    <apex:pageBlock >                                                
                        
                        <apex:outputPanel id="courseSelection" style="margin-bottom:10px">
                            <p class="slds-text-body--medium" title="Course Selection" style="margin-bottom:10px">Please select the related CA Module Course</p>
                            <apex:pageBlockSection columns="1" collapsible="false">
                                <apex:selectList value="{!mainCourseID}" label="Accredited Module Course" size="1">
                                    <apex:selectOptions value="{!mainCourseOptions}"/>
                                </apex:selectList>                            
                            </apex:pageBlockSection>
                        </apex:outputPanel>                                        
                    </apex:pageBlock>
                    
                    <!-- Company filter -->
                    <apex:pageBlock >
                        <apex:outputPanel id="companySearchFilter" style="margin-bottom:10px">
                            <p class="slds-text-body--medium" title="Company Filter" style="margin-bottom:10px">Search and Select a Company</p>
                            <apex:pageBlockSection columns="1" collapsible="false">
                                <apex:inputText value="{!companyNameSearch}" label="Company Name"/>
                                <apex:inputText value="{!companyCustomerIDSearch}" label="Company Customer ID"/>
                                <apex:inputText value="{!companyMemberIDSearch}" label="Company Member ID"/>
                                <apex:commandButton action="{!searchCompany}" value="Search" rerender="companySearchResult" styleClass="slds-button slds-button--brand" style="margin-left:20%"/>
                            </apex:pageBlockSection>
                        </apex:outputPanel>
                        
                        <apex:outputPanel id="companySearchResult">                            
                            
                            <apex:variable value="{!0}" var="counter1" />

                            <fieldset class="slds-form-element" >
                                <p class="slds-text-body--medium" style="margin-top:20px;margin-bottom:10px">Note: The search engine will only return companies with employee enroled to selected course with active and paid enrolment.</p>
                                <!-- <legend class="slds-form-element__legend slds-form-element__label">Please narrow down the search result by putting in Company Name / Company Customer ID / Member ID and press Search</legend> -->                             
                                <div class="slds-form-element__control">
                                    
                                    <table class="slds-table slds-table--bordered slds-table--cell-buffer">
                                        <thead>
                                            <tr class="slds-test-title--caps">
                                                <th scope="col">
                                                    <div class="slds-truncate" title="Company Name"><b>Company Name</b></div>
                                                </th>
                                                <th scope="col">
                                                    <div class="slds-truncate" title="Company Customer ID"><b>Company Customer ID</b></div>
                                                </th>
                                                <th scope="col">
                                                    <div class="slds-truncate" title="Company Member ID"><b>Company Member ID</b></div>
                                                </th>
                                            </tr>                                           
                                        </thead>
                                        <tbody>
                                            <apex:repeat value="{!companyAccountList}" var="ca" id="caRepeater">
                                                <tr>
                                                    <th scope="row" data-label="Company Name">
                                                        <div class="slds-truncate" title="">
                                                            <span class="slds-radio">
                                                                <input type="radio" id="radio{!counter1}" name="options" checked="" onclick="updateCompanySelection('{!ca.Id}')"/>
                                                                <label class="slds-radio__label" for="radio{!counter1}">
                                                                    <span class="slds-radio--faux"></span>
                                                                    <span class="slds-form-element__label">{!ca.Name}</span>
                                                                </label>
                                                            </span>
                                                            <apex:variable var="counter1" value="{!counter1 + 1}"/>
                                                        </div>
                                                    </th>
                                                    <td data-label="Company Customer ID">
                                                        <div class="slds-truncate" title="Company Customer ID">{!ca.Customer_ID__c}</div>
                                                    </td>
                                                    <td data-label="Company Member ID">
                                                        <div class="slds-truncate" title="Company Member ID">{!ca.Member_ID__c}</div>
                                                    </td>
                                                </tr>
                                            </apex:repeat>
                                        </tbody>
                                    </table>
                                    <apex:outputPanel rendered="{!companySearchNoResult}">                                      
                                        <div class="slds-box slds-theme--default">
                                            No result found
                                        </div>
                                    </apex:outputPanel>
                                </div>
                            </fieldset>
                            
                        </apex:outputPanel>
                    </apex:pageBlock>                
                    
                    <!-- Wizard Navigation buttons -->
                    <apex:pageBlock >
                        <div style="margin-left:20%">
                            <apex:commandButton value="Cancel" action="{!cancel}" styleClass="slds-button slds-button--neutral" style="margin-right:3%"/>                    
                            <apex:commandButton value="Next" action="{!nextToPage2}" styleClass="slds-button slds-button--neutral"/>
                        </div>                        
                    </apex:pageBlock>

                    <div id="dummyPanel"/>
                    
                </apex:outputPanel>
                
                <apex:inputHidden value="{!selectedCompanyAccount}" id="hiddenInput"/>
                
            </apex:form>
            </div>
        </div>
    </body>
    
    <footer align="center">
        <!-- © Copyright 2017 -->
        {!VFFooter}
    </footer>
    
    <script type="text/javascript">
        function updateCompanySelection(companyName) {
            v = document.getElementById('{!$Component.myForm.hiddenInput}')
            v.value = companyName;
        }        
    </script>
    
    </html>
</apex:page>