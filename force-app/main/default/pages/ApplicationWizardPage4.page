<apex:page standardController="Application__c" extensions="ApplicationWizardController" title="Application Wizard">
	<apex:form >
		<apex:pageBlock >
		<apex:pageMessages id="pageMessagesId"/>
			<apex:pageBlockSection columns="1">
				<b>CRIMES AND OFFENCES</b><br/><br/>
				
				<b>Record of New Zealand criminal convictions</b><br/>

				NZICA requires that you submit a current criminal convictions record obtained from the New Zealand 
				Ministry of Justice with this application for full membership. The appropriate form 
				(Priv/F1) can be downloaded from 
				<a href="http://www.justice.govt.nz">www.justice.govt.nz</a>.
				<br/><br/>
				
				Please note your rights under the Criminal Records (Clean Slate) Act 2004 before requesting 
				a copy of your criminal record.<br/><br/>

				Where you do not have any criminal convictions recorded, you must still submit the record 
				you receive from the Ministry of Justice with this application. Applicants who have not 
				been to, or lived in, New Zealand are not required to submit a Priv/F1 form.
				<apex:outputPanel >
					<apex:outputLabel value=" NZ Police Check"/><br/>
					<apex:inputFile value="{!AttachmentNZPoliceCheck.body}" filename="{!AttachmentNZPoliceCheck.name}"/>
				</apex:outputPanel>
				
				<apex:pageBlockSectionItem >
					<apex:outputLabel value="Please tick if you have lived in any country other than New Zealand for periods of twelve months or more during the last ten years"/>
					<apex:inputCheckbox value="{!Application.Never_been_to_New_Zealand__c}"/>
				</apex:pageBlockSectionItem>
				
				
				<b>Overseas police clearances</b><br/>
				If you have lived in any country other than New Zealand for periods of twelve months or more 
				during the last ten years, you are required to obtain police clearance certificate(s) from the country 
				(or countries) where you have lived. The certificate(s) must be original(s) or photocopies signed 
				by a New Zealand Justice of the Peace and less than six months old at the time this application is 
				submitted. Any original documents submitted with your application will not be returned. 
				Further information about obtaining a police clearance certificate is available on the New Zealand 
				immigration website (<a href="http://www.immigration.govt.nz">www.immigration.govt.nz</a>).
				<br/><br/>
				
				<apex:outputPanel >
					<apex:outputLabel value="Overseas Police Check"/><br/>
					<apex:inputFile value="{!AttachmentOverSeasPoliceCheck.body}" filename="{!AttachmentOverSeasPoliceCheck.name}"/>
				</apex:outputPanel>
				<br/>
				
				<b>Charges pending</b><br/>
				Please provide details of any charges pending before a court in New Zealand or overseas:
				<apex:inputField value="{!Application.Charges_Pending__c}" style="width:100%;"/>
				
				<b><center>
				A conviction or offence will not automatically result in a declined application.<br/>
				Each case will be considered individually on its merits.<br/>
				Details of any convictions will be kept confidential.
				</center></b>
				
			</apex:pageBlockSection>
			
			<apex:pageBlockButtons >
				<apex:commandButton action="{!backOnPage4}" value="Back" immediate="true"/>
				<apex:commandButton action="{!savePage4}" value="Save & Continue"/>
			</apex:pageBlockButtons>
			
		</apex:pageBlock>
	</apex:form>
</apex:page>