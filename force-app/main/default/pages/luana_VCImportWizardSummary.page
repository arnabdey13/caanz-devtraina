<!-- 
    Developer: WDCi (Terry)
    Date: 23/May/2016
    Task #: LCA-531
    
    Change History:
    LCA-641 28/06/2016: WDCi Lean - improve wizard to take start/end date from child session
-->

<apex:page standardStylesheets="true" applyBodyTag="false" docType="html-5.0" standardController="LuanaSMS__Course__c" extensions="luana_VCImportWizardController" title="AdobeConnect Virtual Classroom Import Wizard">
    <html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <head>
            <apex:stylesheet value="{!URLFOR($Resource.luana_SFDC_Lighting_Design, 'assets/styles/salesforce-lightning-design-system-vf.css')}" />
        </head>
        <body>
            <div class="slds">
                <div class="slds-page-header" role="banner" style="margin-bottom:20px">
                     <div class="slds-media">
                         <div class="slds-media__body">
                             <p class="slds-page-header__title slds-align-middle slds-text-heading--large">AdobeConnect Virtual Classroom Import Wizard - Page 2</p>
                             <p class="slds-text-body--small" title="Student Session Allocation Wizard">
                                 Once your login to Adobe Connect is successful, the following page will display maximum of 50 results based on your search filter. Please select which Virtual Classroom you would like to import and populate all the required fields.<br/><br/>
                                 <b>Note: Topic Key is predicted based on a specific format &lt;Course Name&gt;_VC_T&lt;Topic Number&gt;_&lt;Session Number&gt; &lt;Random Number&gt; eg. CAP116_VC_T1_01 61349526. <br/>This key is used to prevent Member from signing up sessions from the same topic. Please manually correct it if it is incorrect.</b> 
                             </p>
                         </div>
                     </div>
                </div>
                <apex:form id="theform">
                    <apex:pageMessages id="wizardmessage" escape="false" />
                   
                    
                    <apex:pageBlock title="AdobeConnect Search Result for {!filter} : {!meetingCount} Records Found." mode="view" id="wizardblock">                        
                        <apex:pageBlockButtons >
                            <apex:commandButton value="Back" action="{!back}" styleClass="slds-button slds-button--brand"/>
                            <apex:commandButton value="Save" action="{!goToVCUpsert}"  styleClass="slds-button slds-button--brand"/>
                        </apex:pageBlockButtons>
                        <apex:dataTable value="{!sessWrapperList}" var="acs" >
                            <apex:column style="padding: 4px">
                                <apex:facet name="header">Action</apex:facet>
                                <apex:inputCheckbox selected="{!acs.isSelected}"/>
                            </apex:column>
                            <apex:column style="padding: 4px">
                                <apex:facet name="header">Session Name</apex:facet>
                                <apex:inputField value="{!acs.sess.Name}"/>
                            </apex:column>
                            <apex:column style="padding: 4px">
                                <apex:facet name="header">Topic Key</apex:facet>
                                <apex:inputField style="width: 130px" value="{!acs.sess.Topic_Key__c}"/>
                            </apex:column>
                            <apex:column style="padding: 4px">
                                <apex:facet name="header">Session Start Time</apex:facet>
                                <apex:outputField value="{!acs.sess.LuanaSMS__Start_Time__c}" rendered="{!IF(acs.sess.LuanaSMS__Start_Time__c == null, false, true)}"/> <!-- LCA-641 allow user to manual entry if empty -->
                                <apex:inputField value="{!acs.sess.LuanaSMS__Start_Time__c}" rendered="{!IF(acs.sess.LuanaSMS__Start_Time__c == null, true, false)}"/> <!-- LCA-641 allow user to manual entry if empty -->
                            </apex:column>
                            <apex:column style="padding: 4px">
                                <apex:facet name="header">Session End Time</apex:facet>
                                <apex:outputField value="{!acs.sess.LuanaSMS__End_Time__c}" rendered="{!IF(acs.sess.LuanaSMS__End_Time__c == null, false, true)}"/> <!-- LCA-641 allow user to manual entry if empty -->
                                <apex:inputField value="{!acs.sess.LuanaSMS__End_Time__c}" rendered="{!IF(acs.sess.LuanaSMS__End_Time__c == null, true, false)}"/> <!-- LCA-641 allow user to manual entry if empty -->
                            </apex:column>
                            <apex:column style="padding: 4px">
                                <apex:facet name="header">Registration Start Date</apex:facet>
                                <apex:inputField style="width: 100px" value="{!acs.sess.Registration_Start_Date__c}"/>
                            </apex:column>
                            <apex:column style="padding: 4px">
                                <apex:facet name="header">Registration End Date</apex:facet>
                                <apex:inputField style="width: 100px" value="{!acs.sess.Registration_End_Date__c}"/>
                            </apex:column>
                            <apex:column style="padding: 4px">
                                <apex:facet name="header">Maximum Capacity</apex:facet>
                                <apex:inputField style="width: 50px" value="{!acs.sess.Maximum_Capacity__c}"/>
                            </apex:column>                            
                            <apex:column style="padding: 4px">
                                <apex:facet name="header">Data Validation</apex:facet>
                                <apex:outputText style="color:red;" value="{!acs.errorMessage}"/>
                            </apex:column>

                        </apex:dataTable>
                    </apex:pageBlock>
                </apex:form>
            </div>
        </body>
    </html>
</apex:page>