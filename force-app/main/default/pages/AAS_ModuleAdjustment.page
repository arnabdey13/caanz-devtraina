<!-- 
    Developer: WDCi (KH)
    Development Date: 18/11/2016
    Task #: AAS Final Adjustment exam and sub-exam  
    Change on: 
        - 03/04/2017: WDCi - KH, split the FinalAdjustment into 2(Exam and Module) Adjustment
-->
<apex:page standardStylesheets="false" applyBodyTag="false" docType="html-5.0" standardController="AAS_Course_Assessment__c" extensions="AAS_ModuleAdjustmentController" title="Module Adjustment Wizard">
    
    <style>
        #select-table{
            margin-top:30px; 
            margin-left:350px;
        }
        
        #select-table td{
            padding-top: 15px; 
            padding-bottom: 15px;
        }
        
        .slds [hidden], .slds template {
            display: block;
        }
        
        .message .messageText a {
            margin: 0px;
            color: #333;
            font-size: 100%;
            font-weight: bold;
        }

    </style>
    
    <html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <head>
            <apex:stylesheet value="{!URLFOR($Resource.luana_SFDC_Lighting_Design, 'assets/styles/salesforce-lightning-design-system-vf.css')}" />
        </head>
        <body>
            <div class="slds">
                <div class="slds-page-header" role="banner" style="margin-bottom:20px">
                     <div class="slds-media">
                         <div class="slds-media__body">
                             <p class="slds-page-header__title slds-align-middle slds-text-heading--large">Module Adjustment Wizard</p>
                             <p class="slds-text-body--small" title="Student Session Allocation Wizard">
                                 This wizard allows you to review a list of Total Module Mark which fulfills the Module Adjustment criteria. Then, commit the Module Adjustment after review. 
                                 Example: For Module Passing Mark = 60, Total Module Mark of 59.5 will be given +0.5, Total Module Mark of 59 will be given -1.0, Total Module Mark of 58.5 will be given -0.5. 
                             </p>
                         </div>
                     </div>
                </div>
            </div>
            <div >                 
                <apex:form id="theform">
                <apex:pageMessages id="wizardmessage" escape="false" />
                    
                    <apex:pageBlock title="Final Adjustment for {!AAS_Course_Assessment__c.AAS_Course__r.Name}" mode="view" id="wizardblock" >

                        
                        <apex:pageBlockButtons >
                            <apex:commandButton id="remarkBtn" action="{!getResults}" value="Review" styleClass="slds-button slds-button--brand" status="myStatus" reRender="theform, resultTbl, resultSection" disabled="{!!validToProcess}" rendered="{!hasRecords}"/>
                            <apex:commandButton value="Done" action="{!doComplete}" styleClass="slds-button slds-button--brand" rendered="{!!hasRecords}"/>
                            <apex:commandButton value="Commit" action="{!doCommit}" styleClass="slds-button slds-button--brand" disabled="{!!readyToCommit}"/>
                            <apex:commandButton value="Cancel" action="{!doCancel}" styleClass="slds-button slds-button--brand"/>
                            
                        </apex:pageBlockButtons>
                            

                        <div style="margin-top:8px; margin-left:50px;">
                            <table width="450" border="0">
                                <tr>
                                    <td style="padding-top: 15px; padding-bottom: 15px;">
                                        <apex:outputLabel >
                                            <Strong>Type:</Strong>
                                        </apex:outputLabel>
                                    </td>
                                    <td style="padding-top: 15px; padding-bottom: 15px;">                                    
                                        <apex:outputPanel styleClass="requiredInput" layout="block">
                                            <apex:outputPanel styleClass="requiredBlock" layout="block"/>
                                            <apex:selectList value="{!selectedRecordType}" multiselect="false" id="recordTypeName" size="1" label="Type" required="true" onchange="javascript:doValidate(this.options[this.selectedIndex].value)">
                                                <apex:selectOptions value="{!RecordTypeOptions}"/>
                                            </apex:selectList>
                                        </apex:outputPanel>
                                    </td>
                                    <td />
                                </tr>
                            </table>
                        </div>
                        
                        <div>
                        
                        <apex:pageBlockSection title="Student Assessment Section" collapsible="false" columns="1" id="resultSection">
                            <apex:pageBlockTable value="{!finalAdjustmentWrapperList}" var="r">
                                <apex:column value="{!r.stuAssSec.AAS_Student_Assessment__r.AAS_Student_Program__r.LuanaSMS__Contact_Student__r.Member_ID__c}" headerValue="Member Id"/>
                                <apex:column value="{!r.stuAssSec.AAS_Student_Assessment__r.AAS_Student_Program__r.LuanaSMS__Contact_Student__r.Name}" headerValue="Member Name"/>
                                <apex:column headerValue="Student Asessment Section">
                                    <apex:outputLink value="/{!r.stuAssSec.Id}">{!r.stuAssSec.Name}</apex:outputLink>
                                </apex:column>
                                
                                <apex:column value="{!totalMarkValueMap[r.stuAssSec.AAS_Student_Assessment__r.Id]}" headerValue="{!totalMarkLabel}"/>
                                

                                
                                <apex:column value="{!r.finalAdjustMark}" headerValue="Module Adjustment Mark"/>
                                <apex:column headerValue="Student Program">
                                    <apex:outputLink value="/{!r.stuAssSec.AAS_Student_Assessment__r.AAS_Student_Program__r.Id}">{!r.stuAssSec.AAS_Student_Assessment__r.AAS_Student_Program__r.Name}</apex:outputLink>
                                </apex:column>
                                
                                
                            </apex:pageBlockTable>
                        </apex:pageBlockSection>
                        
                        
                        </div>
                        
                        
                    </apex:pageBlock>
                    
                    <apex:actionStatus startText="Calculating..." id="myStatus">
                        <apex:facet name="start">
                        <div style="position: fixed; top: 0; left: 0; right: 0; bottom: 0; opacity: 0.75; z-index: 1000; background-color: black;">
                            &nbsp;
                        </div>
                        <div style="position: fixed; left: 0; top: 0; bottom: 0; right: 0; z-index: 1001; margin: 30% 50%">
                            <img src="/img/loading.gif" />
                        </div>
                        </apex:facet>
                    </apex:actionStatus>
                    
                    <apex:actionFunction action="{!doValidation}" name="doValidate" reRender="wizardmessage, remarkBtn" status="myStatus" >
                        <apex:param id="param1" name="firstParam" assignTo="{!selectedRecordType}" value="" />
                    </apex:actionFunction>
                    
                </apex:form>
            </div>
        </body>
    </html>
    
    
</apex:page>