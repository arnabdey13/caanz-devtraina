<!-- 
    Search Page for Find CA
    ======================================================
    Changes:
        Sep 2016    Davanti - YK    Created
        July 2017   CAANZ - akopec Rev 01 - Specialties	 column

-->

<apex:page controller="FindCAController" showHeader="false" sidebar="false" standardStylesheets="false" applyHtmlTag="false" id="IdFindCAPage">
    
    <apex:includeScript value="{!URLFOR($Resource.CAANZ_Public_JSJSON, '/js/libs/jquery-2.1.4.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.FindCA_CSS, '/js/bootstrap.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.jQuery_Validation, 'dist/jquery.validate.min.js')}" />
    <!-- <apex:includeScript loadOnReady="true" value="{!URLFOR($Resource.FindCA_CSS, '/js/FindCA_CSS.js')}" /> -->
    <apex:includeScript value="{!URLFOR($Resource.FindCA_CSS, '/js/FindCA_CSS.js')}" />
    
	<html class="supports-no-js" lang="en">
		<head>
			<meta charset="utf-8" />
			<meta http-equiv="X-UA-Compatible" content="IE=edge" />
			<meta name="format-detection" content="telephone=no" />
			<meta name="theme-color" content="#FFF" />
			<meta name="msapplication-TileColor" content="#000" />
			<meta name="msapplication-TileImage" content="{!URLFOR($Resource.CAANZ_Public_FontImg, '/img/icons/metro-tile.png')}" />
			<link rel="apple-touch-icon-precomposed" href="{!URLFOR($Resource.CAANZ_Public_FontImg, '/img/icons/apple-touch-icon.png')}" />
			<link rel="shortcut icon" href="{!URLFOR($Resource.CAANZ_Public_FontImg, '/img/icons/favicon.ico')}" />
			<title>Find a Chartered Accountant</title>
			<!--[if lt IE 9]>
			<link rel="stylesheet" href="/assets/css/style.ie.static.css" />
			<![endif]-->
			<!--[if IE 9]>
			<meta name="viewport" content="width=device-width,initial-scale=1" />
			<link rel="stylesheet" href="/assets/css/style.ie.css" />
			<![endif]-->
			<!--[if gt IE 9]><!-->
			<meta name="viewport" content="width=device-width,initial-scale=1" />
			<link rel="stylesheet" href="{!URLFOR($Resource.CAANZ_Public_CSS, '/css/style.css')}" />
			<!--<![endif]-->
			<script src="{!URLFOR($Resource.CAANZ_Public_JSJSON, '/js/head.js')}"></script>
			<script src="{!$Label.FIND_CA_Analytics_Tag_Head}"></script>
			<apex:stylesheet value="{!URLFOR($Resource.FindCA_CSS, '/css/FindCA_Bootstrap.css')}" />
			<apex:stylesheet value="{!URLFOR($Resource.FindCA_CSS, '/css/FindCA_CSS.css')}" />
		</head>
		<body>
			<noscript>
				<div class="page-warning">
					<div class="l-padding">Please enable JavaScript in order to get the best experience when using this site.</div>
				</div>
			</noscript>
			<!--[if lte IE 8]>
			<div class="page-warning"><div class="l-padding">We've noticed that you're using an unsupported version of Internet Explorer. For the best experience please <a href="http://windows.microsoft.com/en-au/internet-explorer/download-ie" rel="nofollow">upgrade</a> or <a href="http://browsehappy.com/" rel="nofollow">use another browser</a>.</div></div>
			<![endif]-->
			<!-- START .accessibility-links - NOTE: these will change per template as not all areas will be on the page at once -->
			<ul id="top" class="accessibility-links" tabindex="-1">
				<li><a href="#nav">Jump to main navigation</a></li>
				<li><a href="#main">Jump to main content</a></li>
			</ul>
			<!-- END .accessibility-links -->
			
			<div class="page-wrap-outer">
	        
				<div class="page-wrap">
					<!-- START .ca-global-header-links -->
					<div class="ca-global-header-links cf">
						<div class="l-padding">
							<!--<nav>-->
							<nav class="nav-onscreen">
								<div class="mid-section">
									<a href="{!$Label.FIND_CA_URL_Home}" class="mid-section-home-link">HOME</a>  
									<a href="{!$Label.FIND_CA_URL_About}">About Us</a>
									<a href="{!$Label.FIND_CA_URL_Contact_Us}">Contact Us</a>
									<a href="{!$Label.FIND_CA_URL_Find_CA}">Find a CA</a>
									<a href="{!$Label.FIND_CA_URL_Acuity}">Acuity</a>
									<a href="{!$Label.FIND_CA_URL_Library}">Library</a>
								</div>
								<a href="{!$Label.FIND_CA_URL_Member_Login}" class="cta header">Login</a>
							</nav>
						</div>
					</div>
					<!-- END .ca-global-header-links -->
		
					<header class="global-header" >
						<div class="l-padding">
							<button class="nav-toggle js-offscreen-toggle" type="button" role="button" aria-label="Toggle navigation" data-offscreen-id="nav">
								<span class="lines"></span>
								<span class="vh">Toggle navigation</span>
							</button>
						</div>
					</header>
					          
					<nav id="nav" class="offscreen-panel js-offscreen" role="navigation" tabindex="-1" data-offscreen-at="0,m">
						<div class="offscreen-content">
							<h2 class="vh">Navigation</h2>
							<div class="nav-header">
								<div class="logo">
									<div alt="Chartered Accountants Logo" class="logo-only-wrapper svg-ca-logo-only" style="data-grunticon-embed"></div>
								</div>
								<button class="link-icon nav-close js-offscreen-close">
									<span class="icon svg-close-grey" style="data-grunticon-embed"></span>
									<span class="vh">Close navigation</span>
								</button>
							</div>
							<ul>
								<li><a class="nav-lvl1" href="{!$Label.FIND_CA_URL_Home}">HOME</a></li>
								<li><a class="nav-lvl1" href="{!$Label.FIND_CA_URL_About}">About Us</a></li>
								<li><a class="nav-lvl1" href="{!$Label.FIND_CA_URL_Contact_Us}">Contact Us</a></li>
								<li><a class="nav-lvl1" href="{!$Label.FIND_CA_URL_Find_CA}">Find a CA</a></li>
								<li><a class="nav-lvl1" href="{!$Label.FIND_CA_URL_Acuity}">Acuity</a></li>
								<li><a class="nav-lvl1" href="{!$Label.FIND_CA_URL_Library}">Library</a></li>
								<li><a class="cta header" href="{!$Label.FIND_CA_URL_Member_Login}">Login</a></li>
							</ul>
						</div>
					</nav>
		            
					<!-- START .l-layout -->
					<div class="l-layout l-one-column cf">
						<!-- START .breadcrumbs -->
						<div class="breadcrumbs">
							<div class="l-padding">
								<ul itemprop="breadcrumb">
									<li class="home">
										<a href="{!$Label.FIND_CA_URL_Home}" rel="home">Home</a>
									</li>
									<li class="is-active">
										Find a CA
									</li>
								</ul>
							</div>
						</div>
						<!-- END .breadcrumbs -->
			          
						<!-- START .l-content-container -->
						<div class="l-content-container l-padding cf">
							<!-- START .l-content-column -->
							<article class="l-content-column">
								<!-- START .l-main -->
								<main id="main" tabindex="-1" role="main" class="l-main">
									<!-- START .content-header -->
									<header class="content-header">
										<h1>Find a Chartered Accountant</h1>
									</header>
									<!-- END .content-header -->
									<div class="cm cm-rich-text">
			                  
										<apex:outputtext escape="false" value="{!$Label.FIND_CA_HEADER}"/>
				  
										<apex:form id="IdFindCAForm" style="overflow:auto;">  
											<apex:actionStatus id="IdSearchStatus" onStart="displayLinks(false);" onStop="displayLinks(true);" >
												<apex:facet name="start">
													<div id="IdLoadingScreen" style="position:fixed !important; left:0px; top: 0px; width: 100%; height:100%; opacity:0.6;filter:alpha(opacity=60);background-color:#efefef"> 
														<img src="{!$Resource.LoadingImage}" alt="Searching" style="position:relative; width:50%; top: 50%; width: 30px; height: 30px;" />
													</div>
												</apex:facet>
											</apex:actionStatus>
			                   
											<apex:actionFunction name="searchCA" action="{!searchCA}" status="IdSearchStatus" reRender="IdSearchResults" rendered="true" oncomplete="scrollTo('results');highlightSelected(1);">    
												<apex:param name="NewSearch" value="true" assignTo="{!bNewSearch}"/>
											</apex:actionFunction>                        		
			
											<apex:pageBlock id="IdSearchForMember">
												<apex:pageBlockSection columns="1">
			                          
													<apex:pageBlockSectionItem >
														<apex:outputText />
														<apex:outputText value="* required fields" styleClass="requiredFields"/>
													</apex:pageBlockSectionItem>
			                          
													<apex:pageBlockSectionItem >
														<apex:outputLabel value="Country*" for="IdAffiliatedBranchCountry" />    
														<apex:outputPanel >
															<apex:selectList id="IdAffiliatedBranchCountry" styleClass="paramDropdown" value="{!strSelectedBranchCountryOption}" size="1" >
																<apex:selectOptions value="{!listBranchCountryOptions}"/>
																<apex:actionSupport event="onchange" action="{!refreshSearchParameters}" reRender="IdMemberType,IdAffiliatedBranch" oncomplete="validateParameters();"/>
															</apex:selectList>
															<a id="search"></a>
															<br/>
															<apex:outputText id="IdCountryRequired" styleClass="placeHolder" />
														</apex:outputPanel>
													</apex:pageBlockSectionItem>
			                          
													<apex:pageBlockSectionItem >
														<apex:outputLabel value="Location*" for="IdAffiliatedBranch" />
														<apex:outputPanel >
															<apex:selectList id="IdAffiliatedBranch" styleClass="paramDropdown" value="{!strSelectedBranchOption}" size="1" >
																<apex:selectOptions value="{!listBranchOptions}"/>
															</apex:selectList>
															<br/>
															<apex:outputText id="IdLocationRequired" styleClass="placeHolder" />
														</apex:outputPanel>
													</apex:pageBlockSectionItem>
			                          
													<apex:pageBlockSectionItem >
														<apex:outputLabel value="Type*" for="IdMemberType" >
															<a data-toggle="modal" data-target="#IdModalType" class="help" id="IdToolTipType" onclick="setPositionToolTipType();">
																<apex:image value="{!URLFOR($Resource.FindCA_CSS, '/img/TooltipIcon.png')}" styleClass="imgHelptextIcon" />
															</a>
														</apex:outputLabel>
														<apex:outputPanel >
															<apex:selectList id="IdMemberType" styleClass="paramDropdown" value="{!strSelectedMemberTypeOption}" size="1" >
																<apex:selectOptions value="{!listMemberTypeOptions}"/>
															</apex:selectList>
															<br/>
															<apex:outputText id="IdTypeRequired" styleClass="placeHolder" />
														</apex:outputPanel>
													</apex:pageBlockSectionItem>
													
													<apex:pageBlockSectionItem >
														<apex:outputPanel >
															<apex:outputLabel value="Name" for="IdInputName">
																<a data-toggle="modal" data-target="#IdModalName" class="help" id="IdToolTipName" onclick="setPositionToolTipName();">
																	<apex:image value="{!URLFOR($Resource.FindCA_CSS, '/img/TooltipIcon.png')}" styleClass="imgHelptextIcon" />
																</a>
															</apex:outputLabel>
														</apex:outputPanel>
														<apex:outputPanel >
															<apex:inputText id="IdInputName" value="{!strInputName}" styleClass="paramInputText" />
															<br/>
															<apex:outputText styleClass="placeHolder" />
														</apex:outputPanel>
													</apex:pageBlockSectionItem>
			                          
													<apex:pageBlockSectionItem >
														<apex:outputText />
														<apex:outputpanel >
															<a id="results"></a>                                        
															<apex:commandLink id="IdSearchLink" styleClass="cta" value="Search" reRender="none" onclick="performSearch();">
																<!-- <apex:param name="NewSearch" value="true" assignTo="{!bNewSearch}"/> -->
															</apex:commandLink> 
				                						</apex:outputpanel>
			            							</apex:pageBlockSectionItem>
			        							</apex:pageBlockSection>                            
			    							</apex:pageBlock>
			    
											<apex:pageBlock id="IdSearchResults">
												
												<apex:outputText value="{!$Label.FIND_CA_No_Results_Found}" rendered="{!bNoResultsFound}" styleClass="searchResultsMessage"/>
												<apex:outputText value="{!$Label.FIND_CA_Too_Many_Results}" rendered="{!bTooManyResultsFound}" styleClass="searchResultsMessage"/>
												
												<apex:dataTable value="{!CAs}" var="CA" id="IdResultsTable" styleClass="js-responsive-table" rendered="{!bDisplayResults || bTooManyResultsFound}">
				
													<apex:column >
														<apex:facet name="header">
															<apex:commandLink value="Name " reRender="IdSearchResults" styleClass="colLinks" status="IdSearchStatus"  action="{!searchCA}" oncomplete="highlightSelected(1);">
																<apex:outputPanel layout="none" rendered="{!strSortExp=='Name' && strSortDir=='DESC'}">&#9660;</apex:outputPanel>
																<apex:outputPanel layout="none" rendered="{!strSortExp=='Name' && strSortDir!='DESC'}">&#9650;</apex:outputPanel>
																<apex:param name="Name" value="Name" assignTo="{!strSortExp}" />
															</apex:commandLink>
														</apex:facet>
														<apex:outputText value="{!CA.Name}"/>
													</apex:column>
					            
													<apex:column >
														<apex:facet name="header">
															<apex:commandLink value="Company " reRender="IdSearchResults" styleClass="colLinks" status="IdSearchStatus"  action="{!searchCA}" oncomplete="highlightSelected(1);">
																<apex:outputPanel layout="none" rendered="{!strSortExp=='Company' && strSortDir=='DESC'}">&#9660;</apex:outputPanel>
																<apex:outputPanel layout="none" rendered="{!strSortExp=='Company' && strSortDir!='DESC'}">&#9650;</apex:outputPanel>
																<apex:param name="Company" value="Company" assignTo="{!strSortExp}" />
															</apex:commandLink>
														</apex:facet>
														<apex:outputText value="{!CA.Company}"/>
													</apex:column>
					            
													<apex:column headerValue="Business Address">
														<apex:outputText value="{!CA.BusinessAddress}" escape="false"/>
													</apex:column>
													
													<apex:column headerValue="Phone" styleClass="resultsColCALink" rendered="{!!bQualifiedAuditorSelected}">
														<apex:outputlink value="tel:{!CA.Phone}" styleClass="linkCACompany" >
															{!CA.Phone}
														</apex:outputlink>
													</apex:column>
													
													<apex:column headerValue="Email" styleClass="resultsColCALink" rendered="{!!bQualifiedAuditorSelected}">
														<apex:outputlink value="mailto:{!CA.Email}" styleClass="linkCACompany" rendered="{!CA.Email != null && CA.Email != ''}">
															<apex:image value="{!URLFOR($Resource.FindCA_CSS, '/img/EmailIcon.png')}" styleClass="imgLink" title="Email Address" />
														</apex:outputlink>
													</apex:column>
													                        
													<apex:column headerValue="Website" styleClass="resultsColCALink" rendered="{!!bQualifiedAuditorSelected}">
														<apex:outputlink value="{!CA.CompanyWebsite}" target="_blank" styleClass="linkCACompany" rendered="{!CA.CompanyWebsite != null && CA.CompanyWebsite != ''}">
															<apex:image value="{!URLFOR($Resource.FindCA_CSS, '/img/GlobeIcon.png')}" styleClass="imgLink" title="Company Website" />
														</apex:outputLink>
													</apex:column>
					
													<apex:column styleClass="resultsColCALink" rendered="{!!bSpecialistSelected}">
														<apex:facet name="header">Special <br/>Conditions</apex:facet>
														<apex:outputPanel rendered="{!CA.SpecialConditions != null && CA.SpecialConditions != ''}">
															<a data-toggle="modal" data-target="#IdModalConditions" class="help" onclick="setPositionToolTipConditions(this, '{!CA.SpecialConditions}');" id="IdToolTipType">
																<apex:image id="IdTooltipConditions" value="{!URLFOR($Resource.FindCA_CSS, '/img/InfoIcon.png')}" styleClass="imgLink" />
															</a>
														</apex:outputPanel>											
													</apex:column>  
                                                    
                                                    <!-- Rev 01 start -->       
                                                    <apex:column styleClass="resultsColCALink" rendered="{!bSpecialistSelected}">
														<apex:facet name="header">Areas of Practice</apex:facet>
														<apex:outputPanel rendered="{!CA.Specialties != null && CA.Specialties != '' }" >
															<a data-toggle="modal" data-target="#IdModalSpecialties" class="help" onclick="setPositionToolTipSpecialties(this, '{!CA.Specialties}');" id="IdToolTipPractice">
																<apex:image id="IdTooltipSpecialties" value="{!URLFOR($Resource.FindCA_CSS, '/img/InfoIcon.png')}" styleClass="imgLink" />
															</a>
														</apex:outputPanel>											
													</apex:column> 
                                                    <!-- Rev 01 end -->         
                                                            
												</apex:dataTable>
			                    
												<apex:panelGrid id="IdResultsPanelGrid" columns="3" columnClasses="col1,col2,col3" rendered="{!bDisplayResults || bTooManyResultsFound}">
													<!--<apex:commandLink action="{!setCon.first}" status="search-status" value="First" reRender="IdSearchResults" id="linkFirst"/>-->
													<apex:panelGroup id="IdResultsPanelGroupPrevious">
														<apex:commandLink action="{!setCon.previous}" status="IdSearchStatus"  reRender="IdResultsTable, IdResultsPanelGroupNext, IdResultsPanelGroupPrevious" id="linkPrevious" rendered="{!setCon.hasPrevious}" onclick="highlightSelected('{!iSelectedPage-1}');">
															<apex:outputPanel layout="none" >&#9664; Previous</apex:outputPanel>
															<apex:param name="iSelectedPage" value="{!iSelectedPage-1}" assignTo="{!iSelectedPage}"/>
														</apex:commandLink>
													</apex:panelGroup>
													<apex:panelGroup >
														<!--
														<apex:outputText >
															Page {!setCon.pageNumber} of {!numOfPages}
														</apex:outputText>
														-->
														<apex:repeat var="pageNum" value="{!listPageNumbers}" id="resultsPageNum">
															<apex:commandLink id="IdResultsPageNum" status="IdSearchStatus" styleClass="linkPageNum" action="{!displaySelectedPage}" value="{!pageNum}" reRender="IdResultsTable, IdResultsPanelGroupNext, IdResultsPanelGroupPrevious" onclick="highlightSelected('{!pageNum}');">
																<apex:param name="iSelectedPage" value="{!pageNum}" assignTo="{!iSelectedPage}"/>
															</apex:commandLink>
														</apex:repeat>
													</apex:panelGroup>
												
													<apex:panelGroup id="IdResultsPanelGroupNext">
														<apex:commandLink action="{!setCon.next}" status="IdSearchStatus"  reRender="IdResultsTable, IdResultsPanelGroupNext, IdResultsPanelGroupPrevious" id="linkNext" rendered="{!setCon.hasNext}" onclick="highlightSelected('{!iSelectedPage+1}');">
															<apex:outputPanel layout="none" >Next &#9654;</apex:outputPanel>
															<apex:param name="iSelectedPage" value="{!iSelectedPage+1}" assignTo="{!iSelectedPage}"/>
														</apex:commandLink>
													</apex:panelGroup>
													<!--<apex:commandLink action="{!setCon.last}" status="search-status"  value="Last" reRender="IdSearchResults" id="linkLast"/>-->
												</apex:panelGrid>    
			                                
												<apex:outputPanel rendered="{!bDisplayResults}">
													<apex:outputText escape="false" value="{!$Label.FIND_CA_Disclaimer_1 & $Label.FIND_CA_Disclaimer_2 & $Label.FIND_CA_Disclaimer_3}" styleClass="disclaimer"/>
													<br/>
												</apex:outputPanel>
			                                    
											</apex:pageBlock>

											<apex:pageBlock >
												<div class="cm-content-tile">
													<div class="image">
														<!--<img class="img" src="/img/cm-16x9-m.jpg" alt="Placeholder medium image"/>-->
													</div>
													<div class="content">
														<apex:outputtext value="{!$Label.FIND_CA_ContactUs_Text}"/>
														&nbsp;
														<apex:outputlink value="{!$Label.FIND_CA_URL_Contact_Us}" styleClass="linkCustom" target="_blank">Contact Us</apex:outputlink>
													</div>
												</div>
											</apex:pageBlock>
										</apex:form>
									</div>
								</main>
								<!-- END .main -->
							</article>
							<!-- END .l-content-column -->
						</div>
						<!-- END .l-content-container -->
					</div>
					<!-- END .l-layout -->
			
					<!-- START .global-footer -->
					<footer class="global-footer" role="contentinfo">
						<div class="l-padding">
							<div class="ca-logo">
								<!-- <a href="/index.html" title="Chartered Accountants Homepage"><span class="icon svg-ca-logo-white" data-grunticon-embed></span></a> -->
								<a href="{!$Label.FIND_CA_URL_Home}" title="Chartered Accountants Homepage">
									<!-- <span class="icon svg-ca-logo-white" ></span> -->
									<apex:image value="{!URLFOR($Resource.FindCA_CSS, '/img/svg-ca-logo-white.png')}" styleClass="imgCALogoWhite"/>
								</a>
							</div>
							<h1 class="vh">Site footer</h1>
							<nav>
								<ul class="nav-footer has-6-items">
									<li>
										<h2><a href="{!$Label.FIND_CA_URL_Privacy}">Privacy</a></h2>
									</li>
									<li>
										<h2><a href="{!$Label.FIND_CA_URL_Legal}">Terms of Use</a></h2>
									</li>
									<!--  
									<li>
										<h2><a href="{!$Label.FIND_CA_URL_Accessibility}">Accessibility</a></h2>
									</li>
									-->
									<li>
										<h2><a href="{!$Label.FIND_CA_URL_Site_Map}">Site map</a></h2>
									</li>
									<li>
										<h2><a href="{!$Label.FIND_CA_URL_Contact_Us}">Contact</a></h2>
									</li>
									<li>
										<h2><a href="{!$Label.FIND_CA_URL_Complaint}">Make A Complaint</a></h2>
									</li>
									<li>
										<h2><a href="{!$Label.FIND_CA_URL_Member_Login}">Login</a></h2>
									</li>
									<li class="nav-footer-social link-icons-disabled">
										<h2>Connect with us</h2>
										<ul>
											<li>
												<apex:outputText style="display:none;" id="IdIconLinkedIn" value="&#xf0e1;" escape="false" />
												<a href="{!$Label.FIND_CA_URL_LinkedIn}" title="Follow us on LinkedIn"><span id="IdSpanLinkedIn" aria-hidden="true" ></span></a>
											</li>
											<li>
												<apex:outputText style="display:none;" id="IdIconTwitter" value="&#xf099;" escape="false" />
												<a href="{!$Label.FIND_CA_URL_Twitter}" title="Tweet us on Twitter"><span id="IdSpanTwitter" aria-hidden="true" ></span></a>
											</li>
											<li>
												<apex:outputText style="display:none;" id="IdIconFacebook" value="&#xf09a;" escape="false" />
												<a href="{!$Label.FIND_CA_URL_Facebook}" title="Like us on Facebook"><span id="IdSpanFacebook"  aria-hidden="true" ></span></a>
											</li>
											<li>
												<apex:outputText style="display:none;" id="IdIconInstagram" value="&#xf16d;" escape="false" />
												<a href="{!$Label.FIND_CA_URL_Instagram}" title="Follow us on Instagram"><span id="IdSpanInstagram" aria-hidden="true" ></span></a>
											</li>
											<li>
												<apex:outputText style="display:none;" id="IdIconYouTube" value="&#xf16a;" escape="false" />
												<a href="{!$Label.FIND_CA_URL_YouTube}" title="Follow us on YouTube"><span id="IdSpanYouTube" aria-hidden="true" ></span></a>
											</li>
										</ul>
									</li>
								</ul>
							</nav>
							<p>
								&copy; 2016 Chartered Accountants Australia and New Zealand ABN 50 084 642 571 (CA ANZ).
								<br /> Formed in Australia. Members of CA ANZ are not liable for the debts and liabilities of CA ANZ
							</p>
						</div>
					</footer>
			        <!-- END .global-footer -->
				</div>
			</div>
			<div class="modal-container js-modal-container">
				<div class="modal-bg"></div>
			</div>
		    
			<!-- Tooltip Type START -->
			<div class="modal fade" tabindex="-1" role="dialog" id="IdModalType">
				<div class="modal-dialog" role="document" id="IdModalDialogType">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title">Type</h4>
						</div>
						<div class="modal-body">
							<apex:outputtext escape="false" value="{!$Label.FIND_CA_Tooltip_Type}"/>
						</div>
					</div>
				</div>
			</div>
			<!-- Tooltip Type END -->
		    
			<!-- Tooltip Name START -->
			<div class="modal fade" tabindex="-1" role="dialog" id="IdModalName">
				<div class="modal-dialog" role="document" id="IdModalDialogName">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title">Name</h4>
						</div>
						<div class="modal-body">
							<p></p>
							<apex:outputtext value="{!$Label.FIND_CA_Tooltip_Name}"/>
						</div>
					</div>
				</div>
			</div>
			<!-- Tooltip Name END -->
			
			<!-- Tooltip Special Conditions START -->
			<div class="modal fade" tabindex="-1" role="dialog" id="IdModalConditions">
				<div class="modal-dialog" role="document" id="IdModalDialogConditions">
					<div class="modal-content" id="IdModalContentConditions">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title">Special Conditions</h4>
						</div>
						<div class="modal-body">
							<apex:outputtext id="IdModalConditionsText" value=""/>
						</div>
					</div>
				</div>
			</div>
			<!-- Tooltip Special Conditions END -->
                                                                                                                
			<!-- Rev 01 Tooltip Specialties START -->
			<div class="modal fade" tabindex="-1" role="dialog" id="IdModalSpecialties">
				<div class="modal-dialog" role="document" id="IdModalDialogSpecialties">
					<div class="modal-content" id="IdModalContentSpecialties">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title">Areas of Practice</h4>
						</div>
						<div class="modal-body">
                        	<apex:outputtext id="IdModalSpecialtiesText" escape="false" value=""/ >                                                                                   
						</div>
					</div>
				</div>
			</div>
			<!-- Rev 01 Tooltip Specialties END -->    
                                                                                                                
                                                                                                                
			<!--[if lt IE 9]>
			<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
			<script>window.jQuery || document.write('<script src="/assets/js/libs/jquery-1.11.3.js"><\/script>')</script>
			<![endif]-->
			
			<!--[if gte IE 9]><!-->
			<script src="{!URLFOR($Resource.CAANZ_Public_JSJSON, '/js/libs/jquery-2.1.4.js')}"></script>
			<script>
				window.jQuery || document.write("{!URLFOR($Resource.CAANZ_Public_JSJSON, '/js/libs/jquery-2.1.4.js')}")
			</script>
			<!--<![endif]-->
			<!-- if the CMS is in Page View/Edit mode:
			<script>DDIGITAL.IS_EDIT = true</script>
			-->
			<script src="{!URLFOR($Resource.CAANZ_Public_JSJSON, '/js/plugins.js')}"></script>
			<script src="{!URLFOR($Resource.CAANZ_Public_JSJSON, '/js/script.js')}"></script>
			<link href="{!URLFOR($Resource.CAANZ_Public_FontImg, '/img/svgs/svgs.css')}" rel="stylesheet"/>
			<script>
				grunticon.method = 'svg';
				<!--grunticon.href = '/assets/img/svgs/svgs.css';-->
				grunticon.href = "{!URLFOR($Resource.CAANZ_Public_FontImg, '/img/svgs/svgs.css')}";
				grunticon.ready(function()
				{
					grunticon.embedIcons(grunticon.getIcons(grunticon.getCSS(grunticon.href)));
				});
			</script>
			<!--
			<div class="js-buildinfo hidden">
				Middleman build #<span class="build-number middleman">28</span> (date: <span class="datetime middleman">19/07/2016 14:16 AEST</span>) (branch: <span class="branch middleman">develop</span>)
			</div>
			-->
			<script type="text/javascript">{!$Label.FIND_CA_Analytics_Tag_Body}</script>
		</body>
	</html>

</apex:page>