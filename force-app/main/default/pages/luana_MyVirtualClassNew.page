<!-- 
    Developer: WDCi (Lean)
    Development Date: 18/05/2016
    Task #: LCA-499
    
    Change History:
    LCA-626 09/06/2016: WDCi Lean - add timezone next to datetime field
    LCA-642 14/06/2016: WDCi KH - change timezone to always Australia/Sydney time
    LCA-887 04/08/2016 - WDCi Lean: append user timezone to date/time field
 -->
<apex:page sidebar="false" showHeader="false" title="Virtual Classroom Registration" controller="luana_MyVirtualClassNewController" docType="html-5.0" standardStylesheets="false" applyBodyTag="true" applyHtmlTag="true" >
    <head>
        <link rel="icon shortcut" href="{!URLFOR($Resource.luana_CA_Websource_v2, 'images/logo.ico')}" />
        <apex:stylesheet value="{!URLFOR($Resource.luana_SFDC_Lighting_Design, 'assets/styles/salesforce-lightning-design-system-vf.css')}" />
    </head>
    <apex:composition template="{!templateName}">
        <apex:define name="body">
            <apex:form id="myform">
            <apex:outputPanel id="forrender">
                <apex:Messages rendered="{!NOT(hasOverlapSession)}"/>
                <apex:pageMessage rendered="{!hasOverlapSession}" summary="Unfortunately, the session is overlapping with your existing session. Please select another date/time." severity="error" />
                <apex:pageMessage rendered="{!hasDuplicateTopic}" summary="You are not allowed to register to the same topic again." severity="error" />
            </apex:outputPanel>
            <div>
                
                    <p>
                        Please select a Virtual Classroom session below:
                        
                    </p>
                    <br/>
                    <table>
                        <tr>
                            <th>Select</th>
                            <th>Session</th>
                            <th>Start Date/Time ({!userTimezone})</th>
                            <th>End Date/Time ({!userTimezone})</th>
                            <th>Seats Enroled</th>
                        </tr>
                        <apex:repeat value="{!eligibleVirtualClasses}" var="vc">
                            <tr>
                                <td>
                                    <apex:outputPanel rendered="{!IF(vc.LuanaSMS__Session__r.Seats_Availability__c != 0, true, false)}">
                                        <input type="radio" value="{!vc.LuanaSMS__Session__c}" name="session" onchange="javascript:setSelectedValue('{!$Component.selectedsession}', '{!vc.LuanaSMS__Session__c}')" />
                                    </apex:outputPanel>
                                    <apex:outputPanel rendered="{!IF(vc.LuanaSMS__Session__r.Seats_Availability__c == 0, true, false)}">
                                        <input type="radio" value="" name="session" onchange="" disabled="true" />
                                    </apex:outputPanel>
                                </td>
                                <td>{!vc.LuanaSMS__Session__r.Name}</td>
                                <td>
                                    <apex:outputField value="{!vc.LuanaSMS__Session__r.LuanaSMS__Start_Time__c}"/>
                                    <!--LCA-887 <apex:outputText value="{!vcStartTime[vc.Id]}"></apex:outputText>-->
                                </td>
                                <td>
                                    <apex:outputField value="{!vc.LuanaSMS__Session__r.LuanaSMS__End_Time__c}"/>
                                    <!--LCA-887 <apex:outputText value="{!vcEndTime[vc.Id]}"></apex:outputText>-->
                                </td>
                                <td><div class="slds slds-no-flex">
                                        <apex:outputPanel rendered="{!vc.LuanaSMS__Session__r.Show_Seats__c}">{!vc.LuanaSMS__Session__r.LuanaSMS__Number_of_enrolled_students__c}/{!vc.LuanaSMS__Session__r.Maximum_Capacity__c}&nbsp;</apex:outputPanel>
                                        <apex:outputPanel styleClass="slds-badge slds-theme--inverse" style="background-color: green" rendered="{!IF(vc.LuanaSMS__Session__r.Seats_Availability__c > 20, true, false)}">Available</apex:outputPanel>
                                        <apex:outputPanel styleClass="slds-badge slds-theme--inverse" style="background-color: orange" rendered="{!IF((vc.LuanaSMS__Session__r.Seats_Availability__c <= 20) && (vc.LuanaSMS__Session__r.Seats_Availability__c > 0), true, false)}">Limited seat</apex:outputPanel>
                                        <apex:outputPanel styleClass="slds-badge slds-theme--inverse" style="background-color: red" rendered="{!IF(vc.LuanaSMS__Session__r.Seats_Availability__c == 0, true, false)}">Full</apex:outputPanel>
                                    </div>
                                </td>
                            </tr>
                        </apex:repeat>
                        <apex:inputHidden id="selectedsession" value="{!selectedSession}"/>
                        
                        <script>
                            function setSelectedValue(fieldId, val){
                                document.getElementById(fieldId).value = val;
                            }
                        </script>
                    </table>
                    <div class="table-footer">
                        <apex:commandButton value="Back" action="{!doCancel}" styleClass="btn btn-default navigation prev-btn"/>                    
                        <apex:commandButton value="Book" action="{!doSave}" styleClass="btn btn-default view next"/>
                        
                    </div>
                
            </div>
            </apex:form>
        </apex:define>
    </apex:composition>
</apex:page>