/*
* Invocable Method Handler Class for Account object
Can be called by Process Builder to execute Account Apex methods.
Since @InvocableMethod annotation can only be used once per class, this
handler was created to minimise the number of classes that could arise.  
======================================================
History 
Aug 201		RN Davanti 	Created
Aug 2017    AC Davanti  Added setEmploymentHistoryReviewContact()
*/ 
public with sharing class AccountInvocableMethodHandler {
    
    @InvocableMethod(label='' description='')
    public static void processAccountRequest(list<AccountRequest> listAccountRequest){
        // At the moment only one request type can be handled in each request
        if(listAccountRequest[0].functionType == 'createPIRelationship'){
            createPIRelationship(listAccountRequest);
        }
        if(listAccountRequest[0].functionType == 'setEmploymentHistoryReviewContact'){
            setEmploymentHistoryReviewContact(listAccountRequest);
        }
    }
    
    // EHCH - Create a relationship record on the account record if the Apply_PI_Contact_to_child_businesses__c is true and if Review_Contact__c has changed
    public static void createPIRelationship(list<AccountRequest> listAccountRequest){
        system.debug('### in createPIRelationship');
        
        List<Relationship__c> listRelationshipsToCreate = new list<Relationship__c>();
        map<Id, Account> mapAcctsInRequest = new map<Id, Account>();
        
        // Maps to captures exiting relationships for accounts to update
        map<Id, list<Relationship__c>> mapBusAccountIdsToConsider = new map<Id, list<Relationship__c>>(); // map of acct Ids whose Apply_PI_Contact_to_child_businesses__c has changed and their relationships 
        map<Id, list<Relationship__c>> mapBusAcctRelationshipsAcct = new map<Id, list<Relationship__c>>(); // map of acctIds and relationships where Account__c = acct.Parent_Review_Contact__c
        map<Id, list<Relationship__c>> mapBusAcctRelationshipsMember = new map<Id, list<Relationship__c>>(); // map of acctIds and relationships where Member__c = acct.Parent_Review_Contact__c
        
        for(AccountRequest acctReq : listAccountRequest){
            system.debug('### acctReq.acct: ' + acctReq.acct);
            mapAcctsInRequest.put(acctReq.acct.Id, acctReq.acct);
            mapBusAccountIdsToConsider.put(acctReq.acct.Id, new List<Relationship__c>());
            mapBusAcctRelationshipsAcct.put(acctReq.acct.Id, new List<Relationship__c>());
            mapBusAcctRelationshipsMember.put(acctReq.acct.Id, new List<Relationship__c>());
        }
        
        if(mapBusAccountIdsToConsider.size() > 0){
            for(Relationship__c rel : [SELECT Id, Account__c, Member__c, Status__c, Primary_Relationship__c, Reciprocal_Relationship__c
                                       FROM Relationship__c 
                                       WHERE Status__c = 'Current'
                                       AND (Reciprocal_Relationship__c = 'PI Contact' OR Primary_Relationship__c = 'PI Contact')
                                       AND (Member__c IN :mapBusAccountIdsToConsider.keyset() OR Account__c IN :mapBusAccountIdsToConsider.keyset())]){
                                           
                                           // Create a map of accounts being tracked and any relationship records that already exist					
                                           if(mapBusAccountIdsToConsider.containsKey(rel.Account__c)){
                                               List<Relationship__c> listRels = mapBusAccountIdsToConsider.get(rel.Account__c);
                                               listRels.add(rel);
                                               mapBusAccountIdsToConsider.put(rel.Account__c, listRels);
                                           }
                                           else if(mapBusAccountIdsToConsider.containsKey(rel.Member__c)){
                                               List<Relationship__c> listRels = mapBusAccountIdsToConsider.get(rel.Member__c);
                                               listRels.add(rel);
                                               mapBusAccountIdsToConsider.put(rel.Member__c, listRels);
                                           }	
                                       }
            
            // Populate maps which track relationships where the Account__c matches the Parent Review Contact, or the Member__c matches
            for(Id acctId : mapBusAccountIdsToConsider.keyset()){
                Account acct = mapAcctsInRequest.get(acctId);
                for(Relationship__c rel : mapBusAccountIdsToConsider.get(acctId)){
                    //The relationship Member__c value matches an account being tracked
                    if(rel.Member__c == acct.Review_Contact__c){
                        List<Relationship__c> listRels = mapBusAcctRelationshipsMember.get(acctId);
                        listRels.add(rel);
                        mapBusAcctRelationshipsMember.put(acctId, listRels);
                    }
                    //The relationship Account__c value matches an account being tracked
                    if(rel.Account__c == acct.Review_Contact__c){
                        List<Relationship__c> listRels = mapBusAcctRelationshipsAcct.get(acctId);
                        listRels.add(rel);
                        mapBusAcctRelationshipsAcct.put(acctId, listRels);
                    }
                }
                
            }
            
            
            
            // Determine if a relationship record should be created.
            for(Id acctId : mapBusAccountIdsToConsider.keyset()){
                Account acct = mapAcctsInRequest.get(acctId);
                List<Relationship__c> listRelsMember = mapBusAcctRelationshipsMember.get(acctId);
                List<Relationship__c> listRelsAcct = mapBusAcctRelationshipsAcct.get(acctId);
                
                if(listRelsMember.size() == 0){
                    listRelationshipsToCreate.add(new Relationship__c(Account__c = acct.Id, Member__c = acct.Review_Contact__c, Status__c = 'Current', Reciprocal_Relationship__c = 'PI Contact', Primary_Relationship__c = 'PI Contact' ));
                }
                if(listRelsAcct.size() == 0){
                    listRelationshipsToCreate.add(new Relationship__c(Member__c = acct.Id, Account__c = acct.Review_Contact__c, Status__c = 'Current', Reciprocal_Relationship__c = 'PI Contact', Primary_Relationship__c = 'PI Contact' ));
                }
            }
            system.debug('### listRelationshipsToCreate: ' + listRelationshipsToCreate);
            if(listRelationshipsToCreate.size() > 0){
                insert listRelationshipsToCreate;
            }
            
        }
        
    }
    
    // ACDC - Set Employment History Review Contact to true.
    public static void setEmploymentHistoryReviewContact(list<AccountRequest> listAccountRequest){
        
        String STATUS_CURRENT='Current';
        String PRIMARY_RELATIONSHIP_PI='PI Contact';
        Set<Id> PersonAccountSet=new Set<Id>();
        Set<Id> AccountSet=new Set<Id>();
        List<Employment_History__c> updateEmploymentHistoryList=new List<Employment_History__c>();
        
        for(AccountRequest acctReq : listAccountRequest){
            AccountSet.add(acctReq.acct.Id);
        }
        
        for(Relationship__c r:[SELECT Account__c,Status__c FROM Relationship__c WHERE Member__c IN:AccountSet AND Status__c=:STATUS_CURRENT AND Primary_Relationship__c=:PRIMARY_RELATIONSHIP_PI ORDER BY CreatedDate DESC]){
            if(!PersonAccountSet.contains(r.Account__c))PersonAccountSet.add(r.Account__c);
        }
        for(Employment_History__c eh:[SELECT Review_Contact__c FROM Employment_History__c WHERE Member__c=:PersonAccountSet]){
            if(eh.Review_Contact__c!=true){
                eh.Review_Contact__c=true;
                updateEmploymentHistoryList.add(eh);
            }
        }
        if(!updateEmploymentHistoryList.isEmpty()){
            try{
                update updateEmploymentHistoryList;
            }catch(exception e){
                System.debug(LoggingLevel.INFO,'Exception while updating Employment History from Account Trigger....'+e.getMessage());
                //error handling.
            }
        }
    }
    
    // Defined user-type to be used in the invocablemethod
    public class AccountRequest{
        @InvocableVariable(label='Account' required=true)
        public Account acct;
        
        @InvocableVariable(label='The function to call' required=true)
        public String functionType; 
        
        public AccountRequest(){
            
        }
        
        public AccountRequest(Account acct, String functionType){
            this.acct = acct;
            this.functionType = functionType;
        }
    }
}