/**
 * Developer: WDCi (LKoh)
 * Date: 19-10-2018
 * Task #: PAN5340 - Flexipath Gained Competency Area related test class
**/
@isTest
public class FP_GainedCompetencyAreaTrigger_TEST {
    
    static testmethod void validateGainedCompetencyAreaTrigger() {
        
        Luana_DataPrep_Test testDataGenerator = new Luana_DataPrep_Test();        
        
        // Generate test assets
        Map<String, SObject> assetMap = FP_Test_UTIL.assetGenerator('GCATrigger');
                        
        test.startTest();
        
        // Gained Competency Area
        List<FP_Gained_Competency_Area__c> gcaList = new List<FP_Gained_Competency_Area__c>();
        
        FP_Gained_Competency_Area__c gca1 = new FP_Gained_Competency_Area__c();
        gca1.FP_Competency_Area__c = assetMap.get('ca2').Id;
        gca1.FP_Contact__c = assetMap.get('cont1').Id;        
        gcaList.add(gca1);
        
        insert gcaList;

        test.stopTest();
    }        
}