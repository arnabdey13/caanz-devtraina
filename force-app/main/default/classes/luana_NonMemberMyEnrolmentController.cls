/* 
    Developer: WDCi (Lean)
    Development Date: 15/04/2016
    Task #: controller for luana_NonMemberMyEnrolment visualforce page LCA-247
    
    LCA-658: 17/06/2016 - include foundation enrolment checking
*/

public with sharing class luana_NonMemberMyEnrolmentController {
    
    public Id custCommConId {get; private set;}
    public Id custCommAccId {get; private set;}
    
    //LCA-658
    public Boolean hasAccessToCAFoundation {get; private set;}
    
    public luana_NonMemberMyEnrolmentController(){
           
        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId()); 
        
        // /*
        this.custCommConId = commUserUtil.custCommConId;       
        this.custCommAccId = commUserUtil.custCommAccId;
        // */
        
        /*
        this.custCommConId = '003p00000045JxJ';       
        this.custCommAccId = '001p0000005BdsA';
        */
        
        //LCA-658
        hasAccessToCAFoundation = commUserUtil.User.Contact.Account.Assessible_for_CAF_Program__c;
        
    }
    
    public PageReference doNewEnrolment(){
        return new PageReference('/nonmember/luana_EnrolmentWizardLanding?producttype=foundation');
    }
    
}