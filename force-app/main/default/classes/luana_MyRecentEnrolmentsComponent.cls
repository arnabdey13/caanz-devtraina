/**
    Developer: WDCi (Lean)
    Development Date: 12/04/2016
    Task #: Controller for luana_MyRecentEnrolments vf component
    
    Change History:
    LCA-553/LCA-899 19/08/2016 - WDCi Lean: hide view button for non accredited module sp
**/

public without sharing class luana_MyRecentEnrolmentsComponent {
    
    public string contactId;
    public boolean enabledView;
    
    public luana_MyRecentEnrolmentsComponent(){
        
    }
    
    public void setContactId(String contactId){
        this.contactId = contactId;
    }
    
    public String getContactId(){
        return this.contactId;
    }
    
    public void setEnabledView(boolean enabledView){
    	this.enabledView = enabledView;
    }
    
    public boolean getEnabledView(){
    	return this.enabledView;
    }
    
    public List<LuanaSMS__Student_Program__c> getEnrolments() {
        return [SELECT Id, Name, LuanaSMS__Course__c, LuanaSMS__Course__r.Name, LuanaSMS__Status__c, LuanaSMS__Course__r.LuanaSMS__Start_Date__c, LuanaSMS__Course__r.Enrolment_Start_Date__c, LuanaSMS__Course__r.Enrolment_End_Date__c, Paid__c, RecordType.DeveloperName FROM LuanaSMS__Student_Program__c WHERE LuanaSMS__Contact_Student__c =: getContactId() AND RecordType.DeveloperName <>: 'Accredited_Program' ORDER BY CreatedDate DESC LIMIT 10]; //LCA-899 add record type
    
    }
    
    public string getViewURL(){
    	//LCA-247
    	return luana_NetworkUtil.getCommunityPath() + '/luana_MyEnrolmentView';
    }
}