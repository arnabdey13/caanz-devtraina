/**
    Developer: WDCi (kh)
    Development Date:24/11/2016
    Task: Test class for AAS_CourseAssessmentReset
**/
@isTest(seeAllData=false)
private class AAS_Test_CourseAssessmentReset {

    final static String contextLongName = 'AAS_Test_CourseAssessmentReset_Test';
    final static String contextShortName = 'acar';
    
    public static AAS_Course_Assessment__c courseAss;
    public static AAS_Student_Assessment__c sa;
    public static AAS_Student_Assessment_Section__c sas;
    
    public static List<AAS_Student_Assessment_Section_Item__c> sasiList;
    
    public static void initial(Boolean hasSA){
        
        Luana_DataPrep_Test dataPrepUtil = new Luana_DataPrep_Test();
        AAS_DataPrep_Test aasdataPrepUtil = new AAS_DataPrep_Test();
        
        insert aasdataPrepUtil.defauleCustomSetting();
        
        Map<String, Id> recordTypeMap = aasdataPrepUtil.getRecordTypeMap('Exam');
        
        //Training Org
        LuanaSMS__Training_Organisation__c trainOrg = dataPrepUtil.createNewTraningOrg(contextLongName, contextLongName, contextShortName, 'address line 1', 'address loc 1', '5000');
        insert trainOrg;
        
        //Program
        LuanaSMS__Program__c prog = dataPrepUtil.createNewProgram(contextShortName, contextLongName, 'Confirmed', 'Graduate diploma', 'Nationally accredited qualification specified in a national training package');
        insert prog;
        
         //Product
        List<Product2> prodList = new List<Product2>();
        prodList.add(dataPrepUtil.createNewProduct('TEST_AU_' + contextShortName, 'AU0001'));
        prodList.add(dataPrepUtil.createNewProduct('TEST_NZ_' + contextShortName, 'NZ0001'));
        prodList.add(dataPrepUtil.createNewProduct('TEST_INT_' + contextShortName, 'INT0001'));
        insert prodList;
        
        //Program Offerings
        LuanaSMS__Program_Offering__c poAM = dataPrepUtil.createNewProgOffering('PO_AM_' + contextShortName, dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), prog.Id, trainOrg.Id, prodList[0].Id, prodList[1].Id, prodList[2].Id, 1, 1);
        insert poAM;
        System.debug('***poAM: ' + poAM);
        
        //Course
        LuanaSMS__Course__c courseAM = dataPrepUtil.createNewCourse('AAA216', poAM.Id, dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        courseAM.LuanaSMS__Allow_Online_Enrolment__c = true;
        insert courseAM;
        System.debug('***courseAM1: ' + courseAM);
        System.debug('***courseAM2: ' + courseAM.LuanaSMS__Program_Offering__r.IsCapstone__c);
        
        //CA
        courseAss = aasdataPrepUtil.createCourseAssessment(courseAM.Id, 'CA_' + contextShortName);
        courseAss.AAS_Module_Passing_Mark__c = 50;
        
        courseAss.AAS_Cohort_Adjustment_Exam__c = true;
        courseAss.AAS_Borderline_Remark__c = true;
        courseAss.AAS_Final_Adjustment_Exam__c = true;
        courseAss.AAS_Final_Result_Calculation_Exam__c = true;
        
        insert courseAss;
        System.debug('***courseAss1: ' + courseAss);
        
        //ASS
        AAS_Assessment_Schema_Section__c ass = aasdataPrepUtil.createAsessmentSchemaSection(courseAss.Id, 'ASS_' + contextShortName, recordTypeMap.get('AAS_Assessment_Schema_Section__c:Exam_Assessment'));
        ass.AAS_Passing_Mark__c = 40;
        ass.AAS_Cohort_Adjustment__c = 2;
        insert ass;
        
        List<AAS_Assessment_Schema_Section_Item__c> assiList = new List<AAS_Assessment_Schema_Section_Item__c>();
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA111 Q #1'));
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA111 Q #2'));
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA111 Q #3'));
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA111 Q #4'));
        insert assiList;
        
        if(hasSA){
            //SA
            sa = aasdataPrepUtil.createStudentAssessment(courseAss.Id);
            insert sa;
        
            //SAS
            sas = aasdataPrepUtil.createStudentAssessmentSection(sa.Id, recordTypeMap.get('AAS_Student_Assessment_Section__c:Exam_Assessment_SAS'));
            insert sas;
            
            //SASI
            sasiList = new List<AAS_Student_Assessment_Section_Item__c>();
            for(AAS_Assessment_Schema_Section_Item__c assi: assiList){
                AAS_Student_Assessment_Section_Item__c sasi = aasdataPrepUtil.createStudentAssessmentSectionItem(sas.Id, recordTypeMap.get('AAS_Student_Assessment_Section_Item__c:Exam_Assessment_SASI'), assi.Id);
                sasi.AAS_Raw_Mark__c = 9;
                sasiList.add(sasi);
            }
            insert sasiList;
        }  
    }
    
    public static testMethod void test_ResetNoSARecords() { 
    
        initial(false);
        
        Test.startTest();
            
            PageReference pageRef1 = Page.AAS_CourseAssessmentReset;
            pageRef1.getParameters().put('id', String.valueOf(courseAss.Id));
            pageRef1.getParameters().put('type', String.valueOf('Exam_Assessment'));
            Test.setCurrentPage(pageRef1);
            
            ApexPages.StandardController sc1 = new ApexPages.StandardController(courseAss);
            AAS_CourseAssessmentResetController cosAssessmentReset = new AAS_CourseAssessmentResetController(sc1);
            
            cosAssessmentReset.doReset();
            cosAssessmentReset.checkBatchCompletion();
            cosAssessmentReset.doCancel();
            
        Test.stopTest();
    }

    
    //RESET_01 & RESET_02
    public static testMethod void test_ResetMain() { 
    
        initial(true);
        
        Test.startTest();
            
            PageReference pageRef1 = Page.AAS_CourseAssessmentReset;
            pageRef1.getParameters().put('id', String.valueOf(courseAss.Id));
            pageRef1.getParameters().put('type', String.valueOf('Exam_Assessment'));
            Test.setCurrentPage(pageRef1);
            
            ApexPages.StandardController sc1 = new ApexPages.StandardController(courseAss);
            AAS_CourseAssessmentResetController cosAssessmentReset = new AAS_CourseAssessmentResetController(sc1);
            
            AAS_Course_Assessment__c ca = [Select Id, AAS_Course__r.Name from AAS_Course_Assessment__c Where Id =: courseAss.Id];
            System.assertEquals(ca.AAS_Course__r.Name, 'AAA216', 'RESET_02: Course Name displayed');
            
            
            //Before reset result
            List<AggregateResult> sasBeforeResults = [Select Count(Id) from AAS_Student_Assessment_Section__c where AAS_Student_Assessment__c =: sa.Id];
            Integer numOfSASBeforeRecordsDelete = (Integer) sasBeforeResults[0].get('expr0');
            
            List<AggregateResult> sasiBeforeResults = [Select Count(Id) from AAS_Student_Assessment_Section_Item__c where AAS_Student_Assessment_Section__r.AAS_Student_Assessment__c =: sa.Id];
            Integer numOfSASIBeforeRecordsDelete = (Integer) sasiBeforeResults[0].get('expr0');
            
            System.assertEquals(numOfSASBeforeRecordsDelete, 1, 'RESET_01: 1 SAS result found');
            System.assertEquals(numOfSASIBeforeRecordsDelete, 4, 'RESET_01: 4 SASI result found');
            
            
            cosAssessmentReset.doReset();
            
            List<SObject> saList = [Select Id from AAS_Student_Assessment__c Where AAS_Course_Assessment__c =: courseAss.Id];
            AAS_DeleteBatch delBatch = new AAS_DeleteBatch(saList);
            //delBatch.execute(null,saList);
            //delBatch.start(null);
            //delBatch.finish(null);
            
            // LKoh :: 15-05-2017
            // Note :: moved to below stopTest() so that the checkBatchCompletion can see the result of the async job
            // cosAssessmentReset.checkBatchCompletion();
            // cosAssessmentReset.doCancel();
            
            
        Test.stopTest();
        
        // LKoh :: 15-05-2017
        cosAssessmentReset.checkBatchCompletion();
        cosAssessmentReset.doCancel();
    }
    
    //RESET_04
    public static testMethod void test_ResetFinalCompletedMain(){
        initial(true);
        
        Test.startTest();
            
            courseAss.AAS_Final_Result_Release_Exam__c = true;
            update courseAss;
            
            PageReference pageRef1 = Page.AAS_CourseAssessmentReset;
            pageRef1.getParameters().put('id', String.valueOf(courseAss.Id));
            pageRef1.getParameters().put('type', String.valueOf('Exam_Assessment'));
            Test.setCurrentPage(pageRef1);
            
            ApexPages.StandardController sc1 = new ApexPages.StandardController(courseAss);
            AAS_CourseAssessmentResetController cosAssessmentReset = new AAS_CourseAssessmentResetController(sc1);
        
        Test.stopTest();
    }

}