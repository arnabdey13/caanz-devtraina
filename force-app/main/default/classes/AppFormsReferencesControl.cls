/********************************************
* Author: RXP
* Purpose: Used for Forms, Controller for AppForms_References component. Used in Special Admissions community page. Can be re used where ever applicable. 
* Date: Sep-2019
* **********************************************/

public without sharing class AppFormsReferencesControl { 

    public static string MEMBERSHIP_CLASS = 'Full';
    public static string MEMBERSHIP_TYPE = 'Member';
    public static String INTERNAL_REFRENCE_TYPE = 'Internal';
    public static String EXTERNAL_REFRENCE_TYPE = 'External'; 
    
    public static List<Database.SaveResult> processReferences(List<SObject> appRefs,ff_ServiceHandler.RecordIdProvider idProvider){            
        Application_References__c references =  (Application_References__c)appRefs[0];
        Map<String, Schema.RecordTypeInfo> referencesRectypes = Schema.SObjectType.Application_References__c.getRecordTypeInfosByDeveloperName(); 
        List<Application_References__c> insertRefs = new List<Application_References__c>();
        integer count=0;
        
        if(references.Internal_Member_Id__c!=null) { // Based on component input identify reference type
            String mentorId =  references.Internal_Member_Id__c.trim();
            List<Account> searchList = [SELECT Id
                                        FROM Account
                                        WHERE Member_ID__c = :mentorId AND
                                        (Membership_Class__c =: MEMBERSHIP_CLASS AND Membership_Type__c =:MEMBERSHIP_TYPE)                      
            ];  
            
            if(!searchList.isEmpty()) {
                
                if(searchList.size() == 1) {                                            
                    references.Member__c = searchList[0].Id;
                    references.Application__c = ff_ServiceHandler.getProvidedId(idProvider, Application__c.getSObjectType(), 0);                            
                    references.RecordTypeId = referencesRectypes.get(INTERNAL_REFRENCE_TYPE).getRecordTypeId();  //internalReference; 
                    references.Reference_Type__c = INTERNAL_REFRENCE_TYPE; // The component filters based on this value on page load              
                    insertRefs.add(references);
                    count=1;
                }
                else if(searchList.size() == 2){
                    count = 2;
                }
                                 
            }
        }
        else if(references.External_Reference_Email__c!=null) {
            references.Application__c = ff_ServiceHandler.getProvidedId(idProvider, Application__c.getSObjectType(), 0);
            references.RecordTypeId = referencesRectypes.get(EXTERNAL_REFRENCE_TYPE).getRecordTypeId();
            references.Reference_Type__c = EXTERNAL_REFRENCE_TYPE;
            insertRefs.add(references);                    
        }  
        
        List<Database.SaveResult> srList= Database.insert(insertRefs, false);
        
        if(!insertRefs.isEmpty()) {           
            
            for (Database.SaveResult sr: srList) {
                if (!sr.isSuccess()) {                
                    for(Database.Error err : sr.getErrors()) {                    
                        if(err.getmessage().contains('duplicate')) {                        
                            throw new SObjectException('Same reference is added already');
                        }
                    }
                }
            }                
            
        }  
        
        else if(count == 2)
        {
            throw new SObjectException('Duplicate references found');
        }  
        else if(count == 0)
        {
            throw new SObjectException('No Matching references found');
        }         
        
        return srList;  
    }
}