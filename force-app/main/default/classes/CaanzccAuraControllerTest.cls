/*------------------------------------------------------------------------------------
Author:        Sumit Gupta
Company:       Salesforce
Description:   Test class for most of the Lightning Component Controllers - MyCA Community 

History
Date            Author             Comments
--------------------------------------------------------------------------------------
04-06-2018     Sumit Gupta          Initial Release
------------------------------------------------------------------------------------*/
@isTest private class CaanzccAuraControllerTest {

    private static User currentUser;
    private static Account currentAccount;
    private static Contact currentContact;
    private static My_Preference__c myPreference;

    static {
        currentAccount = TestObjectCreator.createFullMemberAccount();
        currentAccount.Affiliated_Branch_Country__c = 'Australia';
        insert currentAccount;

        currentContact = [SELECT Id FROM Contact WHERE AccountId =: currentAccount.Id];

        /*currentUser = TestObjectCreator.createPersonAccountPortalUser();
        currentUser.ContactId = currentContact.Id;
        currentUser.Profileid = ProfileCache.getId('NZICA Community Login User');
        insert currentUser;*/
    }

    private static User getCurrentUser(){
        Test.startTest(); Test.stopTest(); // Future method needs to run to create the user.
        return currentUser = [SELECT Name FROM User WHERE AccountId =: currentAccount.Id];
    }

    private static void setPreferenceData(){
        Preference_Group__mdt preferenceGroup = [
            SELECT Id, DeveloperName, MasterLabel, (SELECT Id, DeveloperName, MasterLabel FROM Preference_Group_Items__r) 
            FROM Preference_Group__mdt WHERE Active__c = true LIMIT 1
        ];
        Preference_Group_Item__mdt preferenceGroupItem = preferenceGroup.Preference_Group_Items__r.get(0);

        List<My_Preference__c> myPreferences = new List<My_Preference__c>();
        myPreferences.add(myPreference = new My_Preference__c(
            Name = preferenceGroupItem.MasterLabel, Preference_Group__c = preferenceGroup.DeveloperName, 
            Preference_Group_Item__c = preferenceGroupItem.DeveloperName, User__c = currentUser.Id, 
            Type__c = 'Boolean', Value__c = 'true'
        ));
        insert myPreferences;
    }

    private static void setProductData(){
        List<Product2> products = new List<Product2>();
        products.add(getProduct('Test Product 1', 'Workshop'));
        products.add(getProduct('Test Product 2', 'E-Book'));
        products.add(getProduct('Test Product 3', 'Seminar'));
        products.add(getProduct('Test Product 4', 'Test'));
        insert products;
    }

    private static Product2 getProduct(String name, String format){
        return new Product2(
            Name = name, IsActive = true, NS_Format__c = format,
            NS_Country__c = currentAccount.Affiliated_Branch_Country__c, 
            NS_SubTopic__c = myPreference.Name
        );
    }
    
    @isTest static void test_CaanzAccountDetailsController() {
        System.runAs(getCurrentUser()) {
            String data = CaanzAccountDetailsController.getAccountData();
            System.assertNotEquals(data, null);
            try{
                CaanzAccountDetailsController.saveAccountData(data);
            } catch(Exception ex){
                System.debug(ex.getMessage());
            }
        }
    }
    
    @isTest static void test_CaanzAddressDetailsController() {
        System.runAs(getCurrentUser()) {
            String data = CaanzAddressDetailsController.getAddressData(null);
            System.assertNotEquals(data, null);
            String data2 = CaanzAddressDetailsController.getCountryAndStateOptions();
            System.assertNotEquals(data2, null);
            String data3 = CaanzAddressDetailsController.searchAddress('Test', 'AU', 10);
            System.assertNotEquals(data3, null);
            String data4 = CaanzAddressDetailsController.formatAddress('test', 'AU');
            System.assertNotEquals(data4, null);
            try{
                CaanzAddressDetailsController.saveAddressData(data);
            } catch(Exception ex){
                System.debug(ex.getMessage());
            }
        }
    }

    @isTest static void test_CaanzEmailNotificationsController() {
        System.runAs(getCurrentUser()) {
            String data = CaanzEmailNotificationsController.getNotificationData(null);
            System.assertNotEquals(data, null);
            try{
                CaanzEmailNotificationsController.saveNotificationData(data);
            } catch(Exception ex){
                System.debug(ex.getMessage());
            }
        }
    }

    @isTest static void test_CaanzCommunicationPreferencesController() {
        System.runAs(getCurrentUser()) {
            String data = CaanzCommunicationPreferencesController.getPreferenceData(null);
            System.assertNotEquals(data, null);
            try{
                CaanzCommunicationPreferencesController.savePreferenceData(data);
            } catch(Exception ex){
                System.debug(ex.getMessage());
            }
        }
    }

    @isTest static void test_CaanzArticleRecommendationsController() {
        System.runAs(getCurrentUser()) {
            setPreferenceData();
            String data = CaanzArticleRecommendationsController.getRecommendations(10);
            System.assertNotEquals(data, null);
           	new CaanzArticleRecommendationsController.Recommendation(new Content_KB__c());
        }
    }

    @isTest static void test_CaanzMyPreferencesController() {
        System.runAs(getCurrentUser()) {
            setPreferenceData();
            String data = CaanzMyPreferencesController.getPreferencesMetadata();
            System.assertNotEquals(data, null);
            try{
                List<CaanzMyPreferencesController.MyPreference> myPreferences = new List<CaanzMyPreferencesController.MyPreference> { 
                    new CaanzMyPreferencesController.MyPreference(
                        myPreference.Name, myPreference.Type__c, 
                        myPreference.Preference_Group__c, myPreference.Preference_Group_Item__c
                    )
                };
                CaanzMyPreferencesController.savePreferences(JSON.serialize(myPreferences));
            } catch(Exception ex){
                System.debug(ex.getMessage());
            }
        }
    }

    @isTest static void test_CaanzProductRecommendationsController() {
        System.runAs(getCurrentUser()) {
            setPreferenceData(); setProductData();
            String data = CaanzProductRecommendationsController.getRecommendations(3, true);
            System.assertNotEquals(data, null);
        }
    }
    
    @isTest static void test_CaanzccOnBoardWizardController() {
        String data = CaanzccOnBoardWizardController.getWizardData();
        System.assertNotEquals(data, null);
        try{
            CaanzccOnBoardWizardController.acceptTermsAndConditions();
            CaanzccOnBoardWizardController.rejectTermsAndConditions();
        } catch(Exception ex){
            System.debug(ex.getMessage());
        }
    }

    @isTest static void test_CaanzccProfileBasedRouterController() {
        String data = CaanzccProfileBasedRouterController.getCurrentUserProfile();
        System.assertNotEquals(data, null);
    }

    @isTest static void test_CaanzccLinkBoxController() {
        String data = CaanzccLinkBoxController.getUserPermissions(new List<String>{ 'test' });
        System.assertNotEquals(data, null);
    }
    
    @isTest static void test_FormsServiceFACA() {
        String data = FormsServiceFACA.getUserPermissions(new List<String>{ 'test' });
        System.assertNotEquals(data, null);
    }

    @isTest static void test_CaanzUserProfileDataProvider() {
        String data = CaanzUserProfileDataProvider.getUserProfile();
        System.assertNotEquals(data, null);
        String data2 = CaanzUserProfileDataProvider.getUserProfileNickname();
        System.assertNotEquals(data2, null);
        try{
            CaanzUserProfileDataProvider.updateUserProfileNickname('test');
        } catch(Exception ex){
            System.debug(ex.getMessage());
        }
    }
}