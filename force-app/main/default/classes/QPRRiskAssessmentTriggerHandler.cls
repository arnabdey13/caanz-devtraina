/***********************************************************************************************************************************************************************
Name: QPRRiskAssessmentTriggerHandler 
============================================================================================================================== 
Purpose: Trigger Handler for QPRRiskAssessment Object
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR                 DATE          DETAIL     Description 
1.0        Anitha T  			  2/08/2019    Created    Trigger Handler for QPRRiskAssessment Object


--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/


public class QPRRiskAssessmentTriggerHandler extends TriggerHandler {
    //Context Variable Collections after Type Casting
    List<QPR_Risk_Assessment__c> newList = (List<QPR_Risk_Assessment__c>)Trigger.new;
    List<QPR_Risk_Assessment__c> oldList = (List<QPR_Risk_Assessment__c>)Trigger.old;
    Map<Id,QPR_Risk_Assessment__c> oldMap = (Map<Id,QPR_Risk_Assessment__c>)Trigger.oldMap;
    Map<Id,QPR_Risk_Assessment__c> newMap = (Map<Id,QPR_Risk_Assessment__c>)Trigger.newMap;        
    List<Account> accList = new List<Account>();
    public static Boolean isTrigExec =true;
    //After Insert Trigger 
    public override void afterInsert() {
        updateQPRRiskAssessment(newList);
    }
    public override void afterUpdate(){
        updateQPRRiskAssessment(newList);
    }
    
    public void updateQPRRiskAssessment(List<QPR_Risk_Assessment__c> newQPRRiskAssessList)
    {
        if (isTrigExec ==true)
        {
            Map<Id, Id> qprriskAccountIdMap = new Map<Id, Id>();
            Map<Id, Account> qprriskAccountRecordMap = new Map<Id, Account>();
            List<QPR_Risk_Assessment__c> qprriskList  = new List<QPR_Risk_Assessment__c>();
            if(newQPRRiskAssessList!=null && !newQPRRiskAssessList.isEmpty())
            {
                for(QPR_Risk_Assessment__c qprrisk : newQPRRiskAssessList)
                {
                    if(!String.isEmpty(qprrisk.Account__c)){
                        qprriskAccountIdMap.put(qprrisk.Id, qprrisk.Account__c) ; 
                    }
                    else if(!String.isEmpty(qprrisk.Member_Name__c)){
                        qprriskAccountIdMap.put(qprrisk.Id, qprrisk.Member_Name__c) ; 
                    }
                   /* else 
                    {
                        QPR_Risk_Assessment__c targetqprRisk =   new QPR_Risk_Assessment__c();
                        targetqprRisk.Id = qprrisk.Id;
                        targetqprRisk.Hidden_Country__c =  null;
                        qprriskList.add(targetqprRisk);	
                    }
                   */
                }
                if(qprriskAccountIdMap!=null && !qprriskAccountIdMap.isEmpty())
                {
                    accList = [SELECT Name,isPersonAccount,BillingCountry, PersonMailingCountry,Affiliated_Branch_Country__c
                               FROM Account WHERE Id IN :  qprriskAccountIdMap.values()];    
                    if(accList != null && !accList.isEmpty())
                    {
                        for(Account acc : accList )
                        {qprriskAccountRecordMap.put(acc.Id, acc);}
                        for(QPR_Risk_Assessment__c qprriskAssess : newQPRRiskAssessList){
                            if(String.isNotEmpty(qprriskAssess.Account__c) && qprriskAccountRecordMap!=null && !qprriskAccountRecordMap.isEmpty()){
                                if(qprriskAccountRecordMap.containsKey(qprriskAssess.Account__c))
                                {
                                    QPR_Risk_Assessment__c targetqprRisk =   new QPR_Risk_Assessment__c();
                                    targetqprRisk.Id = qprriskAssess.Id;
                                    Account acc = qprriskAccountRecordMap.get(qprriskAssess.Account__c);
                                    if(acc.isPersonAccount== true)
                                    {
                                        if( acc.Affiliated_Branch_Country__c!=null && String.isNotEmpty(acc.Affiliated_Branch_Country__c))
                                        {targetqprRisk.Hidden_Country__c =  acc.Affiliated_Branch_Country__c;}
                                    }
                                    else
                                    {	if(acc.BillingCountry !=null && String.isNotEmpty(acc.BillingCountry))
                                        {
                                            if(acc.BillingCountry=='Australia' || acc.BillingCountry=='New Zealand')
                                            {targetqprRisk.Hidden_Country__c =  acc.BillingCountry;}
                                            else 
                                            {targetqprRisk.Hidden_Country__c ='Overseas';}
                                        }
                                    }
                                    qprriskList.add(targetqprRisk);		
                                }
                            }
                            else if(String.isNotEmpty(qprriskAssess.Member_Name__c) && qprriskAccountRecordMap!=null && !qprriskAccountRecordMap.isEmpty()){
                                if(qprriskAccountRecordMap.containsKey(qprriskAssess.Member_Name__c))
                                {
                                    QPR_Risk_Assessment__c targetqprRisk =   new QPR_Risk_Assessment__c();
                                    targetqprRisk.Id = qprriskAssess.Id;
                                    Account acc = qprriskAccountRecordMap.get(qprriskAssess.Member_Name__c);
                                    if(acc.isPersonAccount== true)
                                    {
                                        if(acc.Affiliated_Branch_Country__c!=null)
                                        {targetqprRisk.Hidden_Country__c =  acc.Affiliated_Branch_Country__c;}
                                    }
                                    else
                                    {		if(acc.BillingCountry!=null)
                                    {
                                        if(acc.BillingCountry=='Australia' || acc.BillingCountry=='New Zealand')
                                        {targetqprRisk.Hidden_Country__c =  acc.BillingCountry;}
                                        else 
                                        {targetqprRisk.Hidden_Country__c ='Overseas';} 
                                    }
                                    }
                                    qprriskList.add(targetqprRisk);		
                                }
                            }
                            
                        }
                    }
                }
                if(qprriskList!=null && !qprriskList.isEmpty())
                {
                    Database.update(qprriskList);
                } 
            }
            isTrigExec =false;
        }
        
    }
    public override void beforeInsert(){}
    public override void beforeUpdate(){}
    public override void afterDelete(){}
    public override void afterUndelete(){}
    
}