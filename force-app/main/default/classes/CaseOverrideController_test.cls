/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CaseOverrideController_test {
	private static Account FullMemberAccountObjectInDB; // Person Account
	private static Id FullMemberContactIdInDB;
	//private static User PersonAccountPortalUserObjectInDB;
	static{
		insertFullMemberAccountObject();
		//insertPersonAccountPortalUserObject();
	}
	private static void insertFullMemberAccountObject(){
		FullMemberAccountObjectInDB = TestObjectCreator.createFullMemberAccount();
		//## Required Relationships
		//## Additional fields and relationships / Updated fields
		insert FullMemberAccountObjectInDB;
		FullMemberContactIdInDB = [Select id from Contact where AccountId=:FullMemberAccountObjectInDB.Id].Id;
	}
	
	/*private static void insertPersonAccountPortalUserObject(){
		PersonAccountPortalUserObjectInDB = TestObjectCreator.createPersonAccountPortalUser();
		//## Required Relationships
		PersonAccountPortalUserObjectInDB.ContactId = FullMemberContactIdInDB;
		PersonAccountPortalUserObjectInDB.Profileid = ProfileCache.getId('NZICA Community Login User');
		//## Additional fields and relationships / Updated fields
		insert PersonAccountPortalUserObjectInDB;
	}*/
	
	//******************************************************************************************
	//                             TestMethods
	//******************************************************************************************
	static testMethod void CaseOverrideController_testForInternalUser() {
		Test.setCurrentPage( new PageReference('/500/o') );
		CaseOverrideController Controller = new CaseOverrideController();
		PageReference PR = Controller.gotoTabPage();
		System.assertEquals('/500/o?nooverride=1', PR.getUrl(), 'getUrl'); // need to be fixed
	}
	static testMethod void CaseOverrideController_testForPortalUser() {
		Test.startTest();
		Test.stopTest(); // Future method needs to run to create the user.
		
		List<User> PersonAccountPortalUserObject_List = [Select Name from User 
			where AccountId=:FullMemberAccountObjectInDB.Id];
		System.assertEquals(1, PersonAccountPortalUserObject_List.size(), 'PersonAccountPortalUserObject_List.size' );
		User PersonAccountPortalUserObjectInDB = PersonAccountPortalUserObject_List[0];
		
		System.runAs(PersonAccountPortalUserObjectInDB) {
			Test.setCurrentPage( new PageReference('/500/o') );
			CaseOverrideController Controller = new CaseOverrideController();
			PageReference PR = Controller.gotoTabPage();
			System.assertEquals('/apex/casetab', PR.getUrl(), 'getUrl');
		}
	}
}