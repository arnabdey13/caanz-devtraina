public with sharing class DeclarationWizardController {

    public String debugTest {get;set;}
    public String memberOf {get;set;}
    public Account AccountObj {get;set;}
    public Declaration__c Declaration {get;set;}
    Id DeclarationId;
    List<Declaration__c> declaration_List;
    public DeclarationWizardController(ApexPages.StandardController controller){
         String AccountId;
        List<User> User_List = [Select u.Contact.AccountId, u.ContactId From User u
            where u.Id=:UserInfo.getUserId() and u.IsPortalEnabled=true];
        if(User_List.size()>0){
            AccountId = User_List[0].Contact.AccountId;
        }
        AccountObj = [Select 
             Name
            , isPersonAccount
            , Salutation
            , Member_Of__c
            , FirstName
            , LastName
            , Membership_Class__c
            , Preferred_Name__c
            , PersonBirthDate
            , PersonOtherStreet
            , PersonOtherState
            , PersonOtherCity
            , PersonOtherPostalCode
            , PersonOtherCountry
            , PersonMailingStreet
            , PersonMailingState
            , PersonMailingCity
            , PersonMailingPostalCode
            , PersonMailingCountry
            , PersonEmail
            , PersonMobilePhone
            , PersonHomePhone
            , PersonOtherPhone
            , Mailing_and_Residential_is_the_same__c
         From Account where Id=:AccountId ];

        if(AccountObj != null){
            memberOf = AccountObj.Member_Of__c;
        }
             declaration_List = [Select 
                Total_Fees_Charged_NZ__c, 
                SystemModstamp, 
                Sole_Practitioner__c, 
                Services_with_no_CPP__c, 
                SMSF__c, 
                Reviewer_of_Second_Tier_Companies__c,
                Registered_Trustee_in_Bankruptcy__c,
                Registered_Tax_Agent__c, 
                Registered_Company_Liquidator__c, 
                Registered_Company_Auditor__c, 
                Registered_BAS_Agent__c, 
                Professional_Ethics_CPD_Required__c, 
                Partner_Principal_in_Accounting_Firm__c, 
                PE_CPD_Hours_Completed_2013_2014__c, 
                PE_CPD_Hours_Completed_2010_2014__c, 
                PE_CPD_Exemption_Reason__c, 
                Other_PE_CPD_Exemption_Reason__c, 
                Other_CPD_Exemption_Reason__c, 
                Number_of_Engagements__c, 
                Name, 
                Member_Name__c, 
                //LastViewedDate, 
                //LastReferencedDate, 
                LastModifiedDate, 
                LastModifiedById, 
                //LastActivityDate, 
                IsDeleted, 
                Id, 
                Declaration_Status__c, 
                Declaration_Date__c, 
                Declaration_Completed__c, 
                Declaration_Completed_Date__c, 
                CreatedDate, 
                CreatedById, 
                Complied_with_NZICA_Code_of_Ethics__c, 
                CPD_Hours_Completed__c, 
                CPD_Exemption_Reason__c, 
                CPD_Completion_Required__c, 
                Adequate_PI_Insurance__c, 
                AFS_Licensee_Representative__c, 
                AFS_Licensee_Name__c, 
                AFSL_Number__c, 
                AFSL_Licence_Holder__c
            From Declaration__c where Member_Name__c = :AccountObj.Id and Declaration_Status__c='In Progress' order by createdDate desc limit 1];       
        
            if(declaration_List.size() == 1 ){
                Declaration = declaration_List[0];
            }else{
                Declaration = (Declaration__c) controller.getRecord();
                if(Declaration.Member_Name__c == null && Declaration.Declaration_Status__c == null){
                    Declaration = new Declaration__c();
                    Declaration.Member_Name__c = AccountObj.Id;
                    Declaration.Declaration_Status__c = 'In Progress';
                }
            }
    }
    
     public PageReference saveAndContinuePage1(){
        savePage1();
        if( !ApexPages.hasMessages() ){
            return Page.DeclarationWizardPage2;
        }
        return null;
    }
    
    public PageReference backToPage1(){
        return Page.DeclarationWizardPage1;
    }
    
    public PageReference savePage1(){
         try{
			upsert Declaration;
        }
        catch(Exception ex){
            handleException(ex);
            return null;
        }
        return Page.DeclarationWizardPage2;
    }
    
     public PageReference saveAndExitPage1(){
        if(!Declaration.Declaration_Completed__c){
            Declaration.Declaration_Completed__c = false;                   
        }
        system.debug('######: Declaration ' +  Declaration);
        
        Declaration__c d;
         try{
            upsert Declaration;
        }
        catch(Exception ex){
            handleException(ex);
            return null;
        }
        return exitWizard();
    }
        
    public PageReference savePage2(){
        if(!Declaration.Declaration_Completed__c){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 
                'Please mark the Declaration as Completed before continuing.'));
            return null;                    
        }
        try{
            Declaration.Declaration_Completed_Date__c = system.today();
            Declaration.Declaration_Status__c = 'Completed';
            update this.Declaration;
        } catch (DmlException ex) {
            Declaration.Declaration_Completed_Date__c = null;
            Declaration.Declaration_Status__c = 'In Progress';
            handleException(ex);
            return null;
        }
        return exitWizard();
    }
    
    public PageReference exitWizard(){
        return new PageReference('/'+ Schema.sObjectType.Declaration__c.getKeyPrefix() +'/o');
    }
    
     private static void handleException(Exception ex){
        ExceptionHandler.handleException(ex);
    }


}