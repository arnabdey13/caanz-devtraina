public with sharing class AAS_ToolBox {
    
    private static Boolean debug = true;
    
    // Check for the Sequence checkbox
    // note: Step 5 checkbox does not exist
    public static String courseAssessmentSequenceChecker(AAS_Course_Assessment__c courseAssessment, List<String> checkboxReqList) {
    	
    	for (String checkboxReq : checkboxReqList) {
    		if (checkboxReq == 'Non Exam Data Loaded' && courseAssessment.AAS_Non_Exam_Data_Loaded__c == false) {
    			return 'Non Exam Data Loaded';
    		} else if (checkboxReq == 'Exam Data Loaded' && courseAssessment.AAS_Exam_Data_Loaded__c == false) {
    			return 'Exam Data Loaded';
    		} else if (checkboxReq == 'Cohort Adjustment' && courseAssessment.AAS_Cohort_Adjustment_Exam__c == false) {
    			return 'Cohort Adjustment';
    		} else if (checkboxReq == 'Borderline Remark' && courseAssessment.AAS_Borderline_Remark__c == false) {
    			return 'Borderline Remark';
    		} else if (checkboxReq == 'Exam Adjustment' && courseAssessment.AAS_Exam_Adjustment__c == false) {
    			return 'Exam Adjustment';
    		} else if (checkboxReq == 'Module Adjustment' && courseAssessment.AAS_Final_Adjustment_Exam__c == false) {
    			return 'Module Adjustment';
    		} else if (checkboxReq == 'Pre-Final Adjustment' && courseAssessment.AAS_Pre_Final_Adjustment__c == false) {
    			return 'Pre-Final Adjustment';
    		} else if (checkboxReq == 'Final Result Calculation' && courseAssessment.AAS_Final_Result_Calculation_Exam__c == false) {
    			return 'Final Result Calculation';
    		} else if (checkboxReq == 'Result Release Lock' && courseAssessment.AAS_Result_Release_Lock__c == false) {
    			return 'Result Release Lock';
    		} else if (checkboxReq == 'Final Result Release Display' && courseAssessment.AAS_Final_Result_Release_Exam__c == false) {
    			return 'Final Result Release Display';
    		} 
    	}
    	return 'PASS';
    }

    // Query for SAS records that are relevant for a particular CA
    public static List<AAS_Student_Assessment_Section__c> getAllRelevantSAS(AAS_Course_Assessment__c courseAssessment, String fieldList, String additionalQueryOption) {
		String queryString = 'SELECT ' +fieldList+ ' FROM AAS_Student_Assessment_Section__c WHERE AAS_Student_Assessment__r.AAS_Course_Assessment__c = \'' +courseAssessment.Id+ '\' ' +additionalQueryOption;        
		if (debug) system.debug('queryString: ' +queryString);
    	List<AAS_Student_Assessment_Section__c> sasList = Database.query(queryString);
    	return sasList;
    }

    // Wipe the old mark for adjustment on SAS records
    public static void resetMarkedSAS(List<AAS_Student_Assessment_Section__c> sasList) {

    	for (AAS_Student_Assessment_Section__c sas : sasList) {
    		sas.AAS_MarkedForAdjustment__c = false;
    	}
    	// update sasList;
    }
    
    public static Id recordTypeRetriever(SObjectType sObjectType, String recordTypeName) {
        
        // Generate a map of tokens for all the Record Types for the desired object
        Map<String,Schema.RecordTypeInfo> recordTypeInfo = sObjectType.getDescribe().getRecordTypeInfosByName();

        if(!recordTypeInfo.containsKey(recordTypeName)) return null;            

        //Retrieve the record type id by name
        return recordTypeInfo.get(recordTypeName).getRecordTypeId();
    }

/**
    public static void overrideUser(AAS_Settings__c aasConfigs, String userID) {

    	if (aasConfigs != null) {
    		// aasConfigs.Value__c = UserInfo.getUserId();
    		aasConfigs.Value__c = userID;
    		update aasConfigs;
    	}
    }


    public static Boolean preFinalAdjustmentCurrentlyUsed(AAS_Settings__c aasConfigs) {

    	if (aasConfigs != null) {
    		if (aasConfigs.Value__c == 'NULL') {
				return false;
    		} else if (Id.valueOf(aasConfigs.Value__c) != UserInfo.getUserId()) {
    			// There is another user that is currently using the PreFinal Adjustment wizard, return the user ID that is currently using it
    			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'Warning, a different user was using Pre Final Adjustment, continuing will override the old user'));
    			return true;
    			// return aasConfigs.Value__c;
    		} else {
    			return false;
    		}
    	} else {
    		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'Warning, custom setting for pre final adjustment could not be found'));
    		return true;
    		// return 'MISSING CONFIG';
    	}
    }

    public static Boolean updateSAS(Id caID, Id examAssessmentRTID) {
        system.debug('updateSAS called');
		try {
			List<AAS_Student_Assessment__c> saList = [SELECT Id FROM AAS_Student_Assessment__c WHERE AAS_Course_Assessment__c = :caID];
			List<AAS_Student_Assessment_Section__c> sasList = [SELECT Id, AAS_Update_Final_Result__c FROM AAS_Student_Assessment_Section__c WHERE AAS_Student_Assessment__c IN :saList AND RecordTypeID = :examAssessmentRTID];
			if (debug) system.debug('sasList size : ' +sasList.size());
			for (AAS_Student_Assessment_Section__c sas : sasList) {
			    sas.AAS_Update_Final_Result__c = true;
			}
			// DISABLED TEMPORARILY to speed testing
			// REMOVE BEFORE FULL TESTING AND USAGE
			update sasList;
			return true;
		} catch (Exception e) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'Error, problem occurred during SAS Update to refresh workflow value: ' +e));
			system.debug('Exception during initial update of SAS: ' +e);
			return false;
		}
    }
    
**/
    
    public static void calculateMarkAndPass(List<AAS_Student_Assessment_Section__c> sasList, String examPassingMark, Decimal modulePassingMark) {
        
        for (AAS_Student_Assessment_Section__c sas : sasList) {
        	
        	Decimal totalExamMark = sas.AAS_Total_Final_Mark_Baseline__c;
        	if (sas.AAS_Module_Mark_Adjustment__c != null) totalExamMark += sas.AAS_Module_Mark_Adjustment__c;
        	if (sas.AAS_Special_Consideration_Adjustment__c != null) totalExamMark += sas.AAS_Special_Consideration_Adjustment__c;
        	
        	// Calculates the accurate adjusted value to show for the user on page load (the javascript will handle the accurate value for run time)
        	sas.AAS_Temporary_Exam_Mark__c = totalExamMark;
        	sas.AAS_Temporary_Module_Mark__c = totalExamMark + sas.AAS_Total_Non_Exam_Mark_Formula__c;
            
            if (examPassingMark != 'EMPTY') {
                // Set the Temporary Exam Passing Status
                if (totalExamMark >= Decimal.valueOf(examPassingMark)) {
                    sas.AAS_Temporary_Exam_Result__c = 'Pass';
                } else {
                    sas.AAS_Temporary_Exam_Result__c = 'Fail';
                }
            }
            

            // Set the Temporary Module Passing Status
            if (sas.AAS_Temporary_Module_Mark__c >= modulePassingMark) {
                sas.AAS_Temporary_Module_Result__c = 'Pass';
            } else {
                sas.AAS_Temporary_Module_Result__c = 'Fail';
            }
            
            // Set the Temporary Final Result Status
            if (examPassingMark != 'EMPTY') {
                if (totalExamMark >= Decimal.valueOf(examPassingMark) && sas.AAS_Temporary_Module_Mark__c >= modulePassingMark) {
            		sas.AAS_Temporary_Final_Result__c = 'Pass';
            	} else {
            		sas.AAS_Temporary_Final_Result__c = 'Fail';
            	}
            } else {
                if (sas.AAS_Temporary_Module_Mark__c >= modulePassingMark) {
                    sas.AAS_Temporary_Final_Result__c = 'Pass';
                } else {
                    sas.AAS_Temporary_Final_Result__c = 'Fail';
                }
            }
            
        	
        }
    }
    
    public static void updateCASequence(AAS_Course_Assessment__c courseAssessment) {
        courseAssessment.AAS_Pre_Final_Adjustment__c = true;
        update courseAssessment;
    }
}