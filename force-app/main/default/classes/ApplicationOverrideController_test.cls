/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ApplicationOverrideController_test {
	private static Account FullMemberAccountObjectInDB; // Person Account
	private static Id FullMemberContactIdInDB;
	//private static User PersonAccountPortalUserObjectInDB;
	private static Application__c ApplicationObjectInDB;
	static{
		insertFullMemberAccountObject();
		//insertPersonAccountPortalUserObject();
		insertApplicationObject();
	}
	private static void insertFullMemberAccountObject(){
		FullMemberAccountObjectInDB = TestObjectCreator.createFullMemberAccount();
		//## Required Relationships
		//## Additional fields and relationships / Updated fields
		insert FullMemberAccountObjectInDB;
		FullMemberContactIdInDB = [Select id from Contact where AccountId=:FullMemberAccountObjectInDB.Id].Id;
	}
	/*private static void insertPersonAccountPortalUserObject(){
		PersonAccountPortalUserObjectInDB = TestObjectCreator.createPersonAccountPortalUser();
		//## Required Relationships
		PersonAccountPortalUserObjectInDB.ContactId = FullMemberContactIdInDB;
		PersonAccountPortalUserObjectInDB.Profileid = ProfileCache.getId('NZICA Community Login User');
		//## Additional fields and relationships / Updated fields
		insert PersonAccountPortalUserObjectInDB;
	}*/
	private static void insertApplicationObject(){
		ApplicationObjectInDB = TestObjectCreator.createApplication();
		//## Required Relationships
		ApplicationObjectInDB.Account__c = FullMemberAccountObjectInDB.Id;
		//## Additional fields and relationships / Updated fields
		insert ApplicationObjectInDB;
	}
	private static User getPersonAccountPortalUserObjectInDB(){
		Test.startTest();
		Test.stopTest(); // Future method needs to run to create the user.
		
		List<User> PersonAccountPortalUserObject_List = [Select Name from User 
			where AccountId=:FullMemberAccountObjectInDB.Id];
		System.assertEquals(1, PersonAccountPortalUserObject_List.size(), 'PersonAccountPortalUserObject_List.size' );
		return PersonAccountPortalUserObject_List[0];
	}
	
	//******************************************************************************************
	//                             TestMethods
	//******************************************************************************************
	static testMethod void AccountOverrideController_testNewPageForInternalUser() {
		ApexPages.StandardController Ctrl = new ApexPages.StandardController( new Application__c() );
		PageReference PR = Page.ApplicationNewOverride;
		PR.getParameters().put('id', FullMemberAccountObjectInDB.Id );
		Test.setCurrentPage( PR );
		ApplicationOverrideController CtrlExt = new ApplicationOverrideController( Ctrl );
		
		PageReference NewPage = CtrlExt.gotoNewPage();
		System.assertNotEquals( null, NewPage );
		System.assert( NewPage.getUrl().startsWith('/'+ Schema.sObjectType.Application__c.getKeyPrefix() + '/e'), 'getUrl:' + NewPage.getUrl() );
	}
	
	static testMethod void ApplicationOverrideController_testNewPageForPortalUser() {
		User PersonAccountPortalUserObjectInDB = getPersonAccountPortalUserObjectInDB();
		System.runAs(PersonAccountPortalUserObjectInDB) {
			ApexPages.StandardController Ctrl = new ApexPages.StandardController( new Application__c() );
			PageReference PR = Page.ApplicationNewOverride;
			Test.setCurrentPage( PR );
			ApplicationOverrideController CtrlExt = new ApplicationOverrideController( Ctrl );
			
			PageReference NewPage = CtrlExt.gotoNewPage();
			System.assertNotEquals( null, NewPage );
			System.assertEquals( '/apex/applicationwizardpage0', NewPage.getUrl(), 'getUrl' );
		}
	}
	
	static testMethod void AccountOverrideController_testEditPageForInternalUser() {
		ApexPages.StandardController Ctrl = new ApexPages.StandardController( ApplicationObjectInDB );
		PageReference PR = Page.ApplicationEditOverride;
		PR.getParameters().put('id', ApplicationObjectInDB.Id );
		Test.setCurrentPage( PR );
		ApplicationOverrideController CtrlExt = new ApplicationOverrideController( Ctrl );
		
		PageReference EditPage = CtrlExt.gotoEditPage();
		System.assertNotEquals( null, EditPage );
		System.assert( EditPage.getUrl().startsWith('/'+ ApplicationObjectInDB.Id + '/e'), 'getUrl' );
		// /a00O0000005L20SIAS/e?nooverride=1&retURL=%2Fa00O0000005L20SIAS
	}
	
	static testMethod void AccountOverrideController_testEditPageForPortalUserWithNoAccess() {
		// ApplicationObjectInDB.Application_Status__c is not 'Draft';
		User PersonAccountPortalUserObjectInDB = getPersonAccountPortalUserObjectInDB();
		System.runAs(PersonAccountPortalUserObjectInDB) {
			ApexPages.StandardController Ctrl = new ApexPages.StandardController( ApplicationObjectInDB );
			PageReference PR = Page.ApplicationEditOverride;
			PR.getParameters().put('id', ApplicationObjectInDB.Id );
			Test.setCurrentPage( PR );
			ApplicationOverrideController CtrlExt = new ApplicationOverrideController( Ctrl );
			
			PageReference EditPage = CtrlExt.gotoEditPage();
			System.assertNotEquals( null, EditPage );
			System.assertEquals( '/apex/applicationwizardpagenoaccess', EditPage.getUrl(), 'getUrl' );
		}
	}
	
	static testMethod void AccountOverrideController_testEditPageForPortalUser() {
		ApplicationObjectInDB.Application_Status__c = 'Draft';
		update ApplicationObjectInDB;
		User PersonAccountPortalUserObjectInDB = getPersonAccountPortalUserObjectInDB();
		System.runAs(PersonAccountPortalUserObjectInDB) {
			ApexPages.StandardController Ctrl = new ApexPages.StandardController( ApplicationObjectInDB );
			PageReference PR = Page.ApplicationEditOverride;
			PR.getParameters().put('id', ApplicationObjectInDB.Id );
			Test.setCurrentPage( PR );
			ApplicationOverrideController CtrlExt = new ApplicationOverrideController( Ctrl );
			
			PageReference EditPage = CtrlExt.gotoEditPage();
			System.assertNotEquals( null, EditPage );
			System.assertEquals( '/apex/applicationwizardpage0', EditPage.getUrl(), 'getUrl' );
		}
	}
	
	static testMethod void AccountOverrideController_testTabPageForInternalUser() {
		ApplicationOverrideController Ctrl = new ApplicationOverrideController();
		
		PageReference TabPage = Ctrl.gotoTabPage();
		System.assertNotEquals( null, TabPage );
		System.assert( TabPage.getUrl().startsWith('/'+ Schema.sObjectType.Application__c.getKeyPrefix() + '/o'), 'getUrl:' + TabPage.getUrl() );
	}
	
	static testMethod void ApplicationOverrideController_testTabPageForPortalUser() {
		User PersonAccountPortalUserObjectInDB = getPersonAccountPortalUserObjectInDB();
		System.runAs(PersonAccountPortalUserObjectInDB) {
			ApplicationOverrideController Ctrl = new ApplicationOverrideController();
			
			PageReference TabPage = Ctrl.gotoTabPage();
			System.assertNotEquals( null, TabPage );
			System.assert( TabPage.getUrl().startsWith('/'+ Schema.sObjectType.Application__c.getKeyPrefix() + '/o'), 'getUrl:' + TabPage.getUrl() );
		}
	}
	/*
	static testMethod void ApplicationOverrideController_testTabPageForPortalUserWhoIsAMemberOfICAA() {
		User PersonAccountPortalUserObjectInDB = getPersonAccountPortalUserObjectInDB();
		FullMemberAccountObjectInDB.Member_Of__c='ICAA';
		update FullMemberAccountObjectInDB;
		System.runAs(PersonAccountPortalUserObjectInDB) {
			ApplicationOverrideController Ctrl = new ApplicationOverrideController();
			
			PageReference TabPage = Ctrl.gotoTabPage();
			System.assertEquals( null, TabPage );
		}
	}
	*/
}