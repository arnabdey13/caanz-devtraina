@IsTest
public with sharing class TestDataApplicationBuilder {

    Id accountId;
    Id recordTypeId;
    String college;

    public TestDataApplicationBuilder(Id accountId, String recordTypeName) {
        this.accountId = accountId;
        this.recordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
    }

    public TestDataApplicationBuilder college(String college) {
        this.college = college;
        return this;
        //TODO: check that the college string is an available picklist value
    }

    public List<Application__c> create(Integer count, Boolean persisted) {
        List<Application__c> applications = new List<Application__c>();
        for (Integer i = 0; i < count; i++) {
            applications.add(new Application__c(Account__c = accountId,
                    RecordTypeId = recordTypeId,
                    College__c = college));
        }
        if(persisted) insert applications;
        return  applications;
    }

    public static TestDataApplicationBuilder provisionalCA(Id accountId){
        return new TestDataApplicationBuilder(accountId, 'Provisional Member').college('Chartered Accountants');
    }


}