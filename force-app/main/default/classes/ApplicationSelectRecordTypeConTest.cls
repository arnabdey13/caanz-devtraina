@isTest
public class ApplicationSelectRecordTypeConTest {
    static testMethod void testgetApplicationRecordTypeNames(){
        List<String> lstAppRecordTypes=ApplicationSelectRecordTypeController.getApplicationRecordTypeNames(false);
        System.assertNotEquals(lstAppRecordTypes,null,'No record types returned unexpectedly for Application');
    }
}