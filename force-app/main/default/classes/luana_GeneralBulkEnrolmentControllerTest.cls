@isTest
public class luana_GeneralBulkEnrolmentControllerTest {

    private static Boolean debug = true;
    
    private static Map<String,Id> accountRecordTypeMap;
    private static Map<String,Id> courseRecordTypeMap;
    private static Map<String,Id> programOfferingRecordTypeMap;
    private static Map<String, Account> personAccountMap;
    private static Map<String, Account> businessAccountMap;
    private static Map<String, LuanaSMS__Program__c> programMap;
    private static Map<String, LuanaSMS__Program_Offering__c> programOfferingMap;
    private static Map<String, Program_Offering_Related__c> programOfferingRelatedMap;
    private static Map<String, LuanaSMS__Course__c> courseMap;
    private static Map<String, Employment_Token__c> employmentTokenMap;
    private static Map<String, LuanaSMS__Student_Program__c> studentProgramMap;
    
    static testmethod void testPageInitialization() {
        
        testDataGenerator('TEST01');
        
        Test.startTest();       

        PageReference pg1 = Page.luana_BulkEnrolmentPage1;
        pg1.getParameters().put('id', courseMap.get('CASM Course').Id);
        Test.setCurrentPage(pg1);
                    
        luana_GeneralBulkEnrolmentController newController = new luana_GeneralBulkEnrolmentController();
        
        newController.checkCourseType();
        newController.searchCompany();
        newController.searchEmployee();            
        newController.getWrappedEmployees();
        newController.cancel();
        newController.nextToPage2or3();
        newController.nextToPage3();
        newController.nextToPage4();
        newController.nextToPage4Newor5();      
        newController.nextToPage5();
        newController.nextToPage6();
        newController.pageDone();
        newController.Previous();
        newController.Next();
        newController.Beginning();
        newController.End();
        //KH - 05/05/2017: commented this line, cannot find this method in luana_GeneralBulkEnrolmentController class
        //newController.getTotalSelected();
        boolean testHasNext = newController.hasNext;
        boolean testHasPrevious = newController.hasPrevious;
        
        Set<Id> spIdSet = new Set<Id>();
        spIdSet.add(studentProgramMap.get('StudentProgram1').Id);
        luana_FutureToolBox.setSPFuture(spIdSet);
        Test.stopTest();
    }
    
    static testmethod void testPageSuccessfulRun() {
        
        testDataGenerator('TEST02');
        
        Test.startTest();       
        
        PageReference pg1 = Page.luana_GeneralBulkEnrolmentPage1;
        pg1.getParameters().put('id', courseMap.get('AM Course').Id);
        Test.setCurrentPage(pg1);

        luana_GeneralBulkEnrolmentController newController = new luana_GeneralBulkEnrolmentController();
        newController.companyNameSearch = 'STAR1';
        newController.searchCompany();
        
        system.assert(!newController.companyAccountList.isEmpty(), 'Company Search failed during BulkEnrolmentController test');
        system.assert(newController.companyAccountList[0].Id == businessAccountMap.get('BusinessAccount1').Id, 'Company Search yielded wrong result during BulkEnrolmentController test');
        
        newController.selectedCompanyAccount = newController.companyAccountList[0].Id;
        
        if (debug) {
            system.debug('## TEST ## selectedCompanyAccount: ' +newController.selectedCompanyAccount);
            system.debug('## TEST ## companyAccountList: ' +newController.companyAccountList);
        }

        PageReference pg3 = newController.nextToPage2or3();            
        Test.setCurrentPage(pg3);

        if (debug) system.debug('## TEST ## WrappedEmployees :' +newController.getWrappedEmployees());        
        
        List<luana_GeneralBulkEnrolmentController.wrappedObject> checkWrappedObject = new List<luana_GeneralBulkEnrolmentController.wrappedObject>();
        checkWrappedObject = newController.getWrappedEmployees();

        system.debug('testClass :: checkWrappedObject: ' +checkWrappedObject);

        system.assert(!checkWrappedObject.isEmpty(), 'Wrapper map did not contain any search result during GeneralBulkEnrolmentController test');
        
        checkWrappedObject[0].selected = true;
        if (debug) system.debug('## TEST ## checkWrappedObject: ' +checkWrappedObject);
        
        PageReference pg4 = newController.nextToPage4();
        Test.setCurrentPage(pg4);
        
        if (debug) system.debug('## TEST ## employmentTokenOptions: ' +newController.employmentTokenOptions);
        system.assert(!newController.employmentTokenOptions.isEmpty(), 'No Employment Token found during BulkEnrolmentController test');
        
        newController.selectedEmploymentToken = newController.employmentTokenOptions[0].getValue();
        
        PageReference pg5 = newController.nextToPage4Newor5();
        Test.setCurrentPage(pg5);
        
        newController.checkForDuplicate = false;
        
        PageReference returnPage = newController.nextToPage6();
        Test.setCurrentPage(returnPage);
    
        newController.pageDone();
        
        Test.stopTest();
    }
    
    private static void testDataGenerator(String shortAnchor) {
        
        // Record Type map
        accountRecordTypeMap = recordTypeMapper('Account');
        courseRecordTypeMap = recordTypeMapper('LuanaSMS__Course__c');
        programOfferingRecordTypeMap = recordTypeMapper('LuanaSMS__Program_Offering__c');
                        
        // Business Account (Employer)
        businessAccountMap = businessAccountGenerator(accountRecordTypeMap, shortAnchor);
        
        // Personal Account (Employee that will be enrolled to the main course)
        personAccountMap = personAccountGenerator(accountRecordTypeMap, businessAccountMap.get('BusinessAccount1').Id, shortAnchor);
        
        // Program
        programMap = programGenerator();
        
        // Program Offering
        programOfferingMap = programOfferingGenerator(programMap, programOfferingRecordTypeMap);
        
        // Course
        courseMap = courseGenerator(programOfferingMap, courseRecordTypeMap);
        
        List<LuanaSMS__Program_Offering__c> childPOList = new List<LuanaSMS__Program_Offering__c>();
        childPOList.add(programOfferingMap.get('CASM ProgramOffering'));        
        
        // Program Offering Related
        programOfferingRelatedMap = programOfferingRelatedGenerator(childPOList, programOfferingMap.get('AM ProgramOffering'), 'Dependant on');
        
        // Employment Token
        employmentTokenMap = employmentTokenGenerator(businessAccountMap.get('BusinessAccount1').Id);
        
        // Student Program        
        studentProgramMap = studentProgramGenerator(courseMap.get('AM Course').Id, personAccountMap.values());
    }
    
    private static Map<String, Account> personAccountGenerator(Map<String, Id> recordTypeMap, Id employerID, String shortAnchor) {

        Map<String, Account> paMap = new Map<String, Account>();
        
        Account pa1 = new Account();
        pa1.RecordTypeId = recordTypeMap.get('Full_Member');
        pa1.LastName = 'A' + shortAnchor;
        pa1.FirstName = 'BRON';
        pa1.PersonEmail = 'TestEmail1@gmail.com';
        pa1.Salutation = 'TestSalutation';
        pa1.Primary_Employer__c = employerID;
        pa1.Communication_Preference__c= 'Home Phone';
        pa1.PersonHomePhone= '1234';
        pa1.PersonOtherStreet= '83 Saggers Road';
        pa1.PersonOtherCity='JITARNING';
        pa1.PersonOtherState='Western Australia';
        pa1.PersonOtherCountry='Australia';
        pa1.PersonOtherPostalCode='6365';  
        
        Account pa2 = new Account();
        pa2.RecordTypeId = recordTypeMap.get('Full_Member');
        pa2.LastName = 'B' + shortAnchor;
        pa2.FirstName = 'BRON';
        pa2.PersonEmail = 'TestEmail2@gmail.com';
        pa2.Salutation = 'TestSalutation';
        pa2.Primary_Employer__c = employerID;
        pa2.Communication_Preference__c= 'Home Phone';
        pa2.PersonHomePhone= '1234';
        pa2.PersonOtherStreet= '83 Saggers Road';
        pa2.PersonOtherCity='JITARNING';
        pa2.PersonOtherState='Western Australia';
        pa2.PersonOtherCountry='Australia';
        pa2.PersonOtherPostalCode='6365';
        
        List<Account> accountList = new List<Account>();
        accountList.add(pa1);
        accountList.add(pa2);
                
        if(debug) system.debug('##TEST## - pa accountList: ' +accountList);
        insert accountList;

        List<Account> paListCheck = [SELECT Id, LastName, FirstName, PersonEmail, Salutation, RecordTypeId, PersonContactId FROM Account WHERE Id in :accountList];
        for (Account pa : paListCheck) {
            
            system.debug('paListCheck: ' +paListCheck);
            
            if (pa.Id == pa1.Id) paMap.put('PersonAccount1', pa);
            if (pa.Id == pa2.Id) paMap.put('PersonAccount2', pa);
        }

        system.debug('paListCheck: ' +paListCheck); 
            
        return paMap;
    }
    
    private static Map<String, Account> businessAccountGenerator(Map<String, Id> recordTypeMap, String shortAnchor) {
        
        Map<String, Account> baMap = new Map<String, Account>();
        
        Account ba1 = new Account();
        ba1.RecordTypeId = recordTypeMap.get('Business_Account');
        ba1.Name = 'STAR1' + shortAnchor;
        ba1.BillingStreet = 'Test Address 1';
        ba1.Affiliated_Branch_Country__c = 'UNKNOWN'; // this was done to prevent a workflow that is automatically updating the citizen indicator from firing as it would not be compatible with business account
        
        List<Account> accountList = new List<Account>();
        accountList.add(ba1);
        
        if(debug) system.debug('##TEST## - ba accountList: ' +accountList);
        insert accountList;
        
        baMap.put('BusinessAccount1', ba1);
        
        return baMap;       
    }
    
    private static Map<String, LuanaSMS__Program__c> programGenerator() {
        
        Map<String, LuanaSMS__Program__c> newProgramMap = new Map<String, LuanaSMS__Program__c>();
        
        LuanaSMS__Program__c program1 = new LuanaSMS__Program__c();
        program1.name = 'PROGRAM1';
        
        List<LuanaSMS__Program__c> programList = new List<LuanaSMS__Program__c>();
        programList.add(program1);
        
        if (debug) system.debug('##TEST## - programList: ' +programList);
        insert programList;
        
        newProgramMap.put('Program1', program1);

        return newProgramMap;
    }
    
    private static Map<String, LuanaSMS__Program_Offering__c> programOfferingGenerator(Map<String, LuanaSMS__Program__c> pMap, Map<String, Id> pRTMap) {
        
        Map<String, LuanaSMS__Program_Offering__c> newProgramOfferingMap = new Map<String, LuanaSMS__Program_Offering__c>();
        
        LuanaSMS__Program_Offering__c newPO1 = new LuanaSMS__Program_Offering__c();
        newPO1.name = 'PROGRAMOFFERING1';
        newPO1.LuanaSMS__Program__c = pMap.get('Program1').Id;
        newPO1.RecordTypeID = pRTMap.get('Accredited_Module');
        
        
        LuanaSMS__Program_Offering__c newPO2 = new LuanaSMS__Program_Offering__c();
        newPO2.name = 'PROGRAMOFFERING2';
        newPO2.LuanaSMS__Program__c = pMap.get('Program1').Id;
        newPO2.RecordTypeID = pRTMap.get('CASM');
        
        List<LuanaSMS__Program_Offering__c> programOfferingList = new List<LuanaSMS__Program_Offering__c>();
        programOfferingList.add(newPO1);
        programOfferingList.add(newPO2);
        
        if (debug) system.debug('##TEST## - programOfferingList: ' +programOfferingList);
        insert programOfferingList;
        
        newProgramOfferingMap.put('AM ProgramOffering', newPO1);
        newProgramOfferingMap.put('CASM ProgramOffering', newPO2);
        
        return newProgramOfferingMap;
    }
    
    private static Map<String, LuanaSMS__Course__c> courseGenerator(Map<String, LuanaSMS__Program_Offering__c> poMap, Map<String, Id> courseRTMap) {
        
        Map<String, LuanaSMS__Course__c> newCourseMap = new Map<String, LuanaSMS__Course__c>();
        
        LuanaSMS__Course__c newCourse1 = new LuanaSMS__Course__c();
        newCourse1.name = 'COURSE1';
        newCourse1.LuanaSMS__Program_Offering__c = poMap.get('AM ProgramOffering').Id;
        newCourse1.RecordTypeID = courseRTMap.get('Accredited_Module');
        newCourse1.LuanaSMS__Status__c = 'Running';
        
        LuanaSMS__Course__c newCourse2 = new LuanaSMS__Course__c();
        newCourse2.name = 'COURSE2';
        newCourse2.LuanaSMS__Program_Offering__c = poMap.get('CASM ProgramOffering').Id;
        newCourse2.Maximum_Enrolment__c = 10;
        newCourse2.RecordTypeID = courseRTMap.get('CASM');
        
        List<LuanaSMS__Course__c> courseList = new List<LuanaSMS__Course__c>();
        courseList.add(newCourse1);
        courseList.add(newCourse2);
        
        if (debug) system.debug('##TEST## - courseList: ' +courseList);
        insert courseList;
        
        newCourseMap.put('AM Course', newCourse1);
        newCourseMap.put('CASM Course', newCourse2);
        
        return newCourseMap;
    }
    
    private static Map<String, Program_Offering_Related__c> programOfferingRelatedGenerator(List<LuanaSMS__Program_Offering__c> childPOList, LuanaSMS__Program_Offering__c parentPO, String porType) {
        
        Map<String, Program_Offering_Related__c> newPorMap = new Map<String, Program_Offering_Related__c>();

        List<Program_Offering_Related__c> porList = new List<Program_Offering_Related__c>();
        Integer i = 1;
        for (LuanaSMS__Program_Offering__c po : childPOList) {
            Program_Offering_Related__c newPOR = new Program_Offering_Related__c();
            newPOR.name = 'PROGRAMOFFERINGRELATED' + i;
            newPOR.Child_Program_Offering__c = po.Id;
            newPOR.Parent_Program_Offering__c = parentPO.Id;
            newPOR.Type__c = porType;
            porList.add(newPOR);
        }

        insert porList;
        
        Integer j = 1;
        for (Program_Offering_Related__c por : porList) {
            newPorMap.put('ProgramOfferingRelated' + j, por);
        }
        
        return newPorMap;
    }

    private static Map<String, Employment_Token__c> employmentTokenGenerator(Id employerID) {

        Map<String, Employment_Token__c> empTokenMap = new Map<String, Employment_Token__c>();

        Employment_Token__c empToken1 = new Employment_Token__c();
        empToken1.Name = 'EMPLOYMENTTOKEN1';
        empToken1.Active__c = true;
        empToken1.Expiry_Date__c = (system.today().addDays(5));
        empToken1.Employer__c = employerID;
        empToken1.Token_Code__c = '123456';

        List<Employment_Token__c> empTokenList = new List<Employment_Token__c>();
        empTokenList.add(empToken1);

        insert empTokenList;

        empTokenMap.put('EmploymentToken1', empToken1);

        return empTokenMap;
    }
    
    private static Map<String, LuanaSMS__Student_Program__c> studentProgramGenerator(Id mainCourseId, List<Account> studentAccountList) {
        
        Map<String, LuanaSMS__Student_Program__c> spMap = new Map<String, LuanaSMS__Student_Program__c>();
        
        List<LuanaSMS__Student_Program__c> spList = new List<LuanaSMS__Student_Program__c>();
        Integer i = 1;
        for (Account pa : studentAccountList) {
            LuanaSMS__Student_Program__c studentProgram1 = new LuanaSMS__Student_Program__c();          
            studentProgram1.LuanaSMS__Course__c = mainCourseId;
            studentProgram1.LuanaSMS__Contact_Student__c = pa.PersonContactId;
            studentProgram1.Paid__c = true;
            studentProgram1.LuanaSMS__Status__c = 'In Progress';
            spList.add(studentProgram1);
            spMap.put('StudentProgram' + i, studentProgram1);
            i++;
        }
        insert spList;
        
        return spMap;
    } 

    private static Map<String, Id> recordTypeMapper(String objName) {
        List<RecordType> recordTypeList = [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = :objName];
        if (!recordTypeList.isEmpty()) {
            Map<String, Id> recordTypeMap = new Map<String, Id>();
            for (RecordType rt : recordTypeList) {
                recordTypeMap.put(rt.DeveloperName, rt.Id);
            }
            return recordTypeMap;
        } else {
            return null;
        }
    }
}