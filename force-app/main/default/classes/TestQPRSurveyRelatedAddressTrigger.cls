@isTest
public class TestQPRSurveyRelatedAddressTrigger {
      public static Id questionnaireAU = Schema.SObjectType.QPR_Questionnaire__c.getRecordTypeInfosByName().get('QPR AUS').getRecordTypeId();

@testSetup
     Static void setupTestData(){
           
             // Create member
        Account a = TestObjectCreator.createBusinessAccount();
        insert a;
        
        // Create Review
        Review__c r = new Review__c();
        r.Practice_Name__c = a.Id;
        insert r;
        
        // Create QPR Questionnaire
        QPR_Questionnaire__c oneQuestionnaire = new QPR_Questionnaire__c();
        oneQuestionnaire.RecordTypeId = questionnaireAU;
        oneQuestionnaire.Practice__c = a.Id;
        oneQuestionnaire.Practice_ID__c = '12345';
        oneQuestionnaire.Review_Code__c = r.Id;
        insert oneQuestionnaire;
         
         //insert QPR Questionnaire related address
         QPR_Survey_Related_Address__c relatedAddress  = new QPR_Survey_Related_Address__c();
         relatedAddress.QPR_Survey__c = oneQuestionnaire.Id;
         relatedAddress.Address__c = '12 Ford Street';
         relatedAddress.City__c = 'Sydney12';
         relatedAddress.Postcode__c = '1118';
         insert relatedAddress;
    }
@isTest
    public static void testQPRSurveyRelatedAddress(){
        QPR_Survey_Related_Address__c relatedAddress = [select ID, RemoveFlag__c from QPR_Survey_Related_Address__c where Address__c = '12 Ford Street' and City__c ='Sydney12' and Postcode__c='1118'];
        relatedAddress.RemoveFlag__c= True;
       	update relatedAddress;
        
    }
    
}