/*
    Developer: WDCi (Leo)
    Date: 13/03/2017
    Task#: LCA-1081 Mass Course Delivery Location Creation using Wizard
    
    Usage: This is just a template for basic search. You shall change the code to include more fields in filterting to cater for your customer requirement.
*/

public class luana_MassCDLController {

    private static Boolean debug = true;
    private static final Integer LIST_SIZE = 200;
    private static String optionPageType = 'Workshop Location';

    public String searchTermName {get;set;}
    public String searchTermState {get;set;}
    public String searchTermCountry {get;set;}    
    
    public Map<Id, ObjectWrapper> queryResultHolderMap;
    public List<ObjectWrapper> dlWrapperToCreate {get;set;}

    // Dynamic Title and Descriptions
    public String VFFooter {get;set;}
    public String headerTitle {get;set;}
    public String headerDescription {get;set;}
    public String headerDescriptionPage1 {get;set;}
    public String headerDescriptionPage2 {get;set;}
    public String headerDescriptionPage3 {get;set;}
    public String headerDescriptionPage4 {get;set;}
    public String page2NextButton {get;set;}
    public String searchPopUpWarning {get;set;}
    public String existingAvailabilityDayPresent {get;set;}

    // Stores the Course record that was used to initialize the wizard
    public LuanaSMS__Course__c initialCourse {get;set;}

    // Used for storing value temporarily or for using the field in inputField
    // public Payment_Token__c dummyPaymentToken {get;set;}
    public Course_Delivery_Location__c dummyPaymentToken {get;set;}

    // Variable shown in page1
    public String selectedType {get;set;}
    public List<SelectOption> typeOptions {get;set;}
    
    public Map<Id, Course_Delivery_Location__c> existingCDLMap;
    public Map<Id, Course_Delivery_Location__c> cdlToCreateMap;
    
    // Variable shown in apge2
    public List<SelectOption> searchStateOptions {get;set;}
    public List<SelectOption> searchCountryOptions {get;set;}        
    
    // Variable shown in page3
    public List<Course_Delivery_Location__c> cdlToCreateList {get;set;}
    public Map<Id, String[]> selectedAvailableDayMap {get;set;}
        
    // ## TEST VAR ## //
    public Course_Delivery_Location__c testCDL {get;set;}
    public String[] testMS {get;set;}

    public luana_MassCDLController() {
        initialize();
    }

    private void initialize() {
        
        // URL param reader
        Map<String, String> urlParameterMap = ApexPages.currentPage().getParameters();      
        if (urlParameterMap.containsKey('id')) {
            initialCourse = [SELECT Id, LuanaSMS__Program_Offering__r.LuanaSMS__Training_Organisation__c FROM LuanaSMS__Course__c WHERE Id = :urlParameterMap.get('id') LIMIT 1];
        }
        
        searchTermName = '';        
        searchTermState = '';
        searchTermCountry = '';
        queryResultHolderMap = new Map<Id, ObjectWrapper>();

        // dummyPaymentToken = new Payment_Token__c();
        dummyPaymentToken = new Course_Delivery_Location__c();
        selectedType = '';
        typeOptions = new List<SelectOption>();                
        
        // Populating CDL type options                
        Schema.DescribeFieldResult fieldResult = Course_Delivery_Location__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();                
        
        for( Schema.PicklistEntry f : ple) {
            typeOptions.add(new SelectOption(f.getLabel(), f.getValue()));
        }

        // Initializing the Dynamic Title values
        headerTitle = 'Mass Course Delivery Location';
        headerDescription = 'Wizard to mass create Course Delivery Location/s. </br></br>';
        headerDescriptionPage1 = 'Step 1 : Fill up the following:</br></br>' +
                                    '<b>Course Delivery Location Type</b> - Type of the Course Delivery Location/s to be created. Only one type of Course Delivery Location can be created in each wizard run. </br>' +
                                    '<b>Inherit from Course</b> - If this is filled, the Delivery Location/s of the selected Type found from the inherited course will be pre-selected automatically at the next page. </br>';
        headerDescriptionPage2 = 'Step 2 : Select the Delivery Location/s and click on Create. </br>';
        headerDescriptionPage3 = 'Step 2 (a) : Fill in the Availability Day for each Delivery Location/s. Availability Day is mandatory field for Workshop Location. </br>';
        headerDescriptionPage4 = 'Step 3 : This is the creation result page. Click on Done to exit. Restart the Wizard process if there is any error. </br>';
        searchPopUpWarning = 'Running a Search will erase all previous selection/s (if there is any).';
        existingAvailabilityDayPresent = '</br><b>(Inherited from Selected Course)</b>';
    }
    
    public ApexPages.StandardSetController con {
        get {
            if (con == null) {
                searchRecord();
            }                
            return con;
        }
        set;
    }
    
    //######## Search method ########//
    public void searchRecord() {                
        
        existingCDLMap = new Map<Id, Course_Delivery_Location__c>();        
        
        String queryString = 'SELECT Id, Name, LuanaSMS__State__c, LuanaSMS__Country__c, LuanaSMS__Training_Organisation__c FROM LuanaSMS__Delivery_Location__c';

        String optionalQueryString = '';
        
        if (searchTermName != null && searchTermName != '') {
            optionalQueryString += ' WHERE Name LIKE \'%' +searchTermName+ '%\'';
        }
        
        if (searchTermState != null && searchTermState != '' && searchTermState != 'ALL') {
            if (optionalQueryString != '') {
                optionalQueryString += ' AND ';         
            } else {
                optionalQueryString += ' WHERE ';
            }
            optionalQueryString += 'LuanaSMS__State__c = \'' +searchTermState+ '\'';
        }
        
        if (searchTermCountry != null && searchTermCountry != '' && searchTermCountry != 'ALL') {
            if (optionalQueryString != '') {
                optionalQueryString += ' AND ';         
            } else {
                optionalQueryString += ' WHERE ';
            }
            optionalQueryString += 'LuanaSMS__Country__c = \'' +searchTermCountry+ '\'';
        }
        
        queryString += optionalQueryString;        
        queryString += ' ORDER BY Name ASC NULLS last';

        try {
            List<LuanaSMS__Delivery_Location__c> queryResultList = new List<LuanaSMS__Delivery_Location__c>();
            
            //reset the holder map on every search
            queryResultHolderMap = new Map<Id, ObjectWrapper>();
            
            if (debug) system.debug('queryString: ' +queryString);
            
            for(LuanaSMS__Delivery_Location__c dl : Database.query(queryString)){
                queryResultList.add(dl);
                
                ObjectWrapper newWP = new ObjectWrapper(dl); 
                queryResultHolderMap.put(dl.Id, newWP);
            }
            
            con = new ApexPages.StandardSetController(queryResultList);
            con.setPageSize(LIST_SIZE); 
                        
        } catch (QueryException e) {
            
            system.debug('Exception: ' + e);
            ApexPages.addMessages(e);
        }        
    }  
    //-------- Search method --------//
    
    //######## Object holder methods ########//
    public class objectWrapper {
        public LuanaSMS__Delivery_Location__c storedObject {get;set;}
        public boolean selected {get;set;}
        public List<String> availableDay {get;set;}
        public boolean oldValueExist {get;set;}
        
        public ObjectWrapper(LuanaSMS__Delivery_Location__c dl) {           
            storedObject = dl;
            selected = false;
            availableDay = new List<String>();
            oldValueExist = false;
        }
    }
    
    public List<ObjectWrapper> getWrappedDL() {
        List<ObjectWrapper> wrappedObjectList = new List<ObjectWrapper>();                      
        
        for (LuanaSMS__Delivery_Location__c dl : (List<LuanaSMS__Delivery_Location__c>)con.getRecords()) {
            wrappedObjectList.add(queryResultHolderMap.get(dl.Id));
        }
        
        return wrappedObjectList;
    }
    
    public List<SelectOption> getDays() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Monday', 'Monday'));
        options.add(new SelectOption('Tuesday', 'Tuesday'));
        options.add(new SelectOption('Wednesday', 'Wednesday'));
        options.add(new SelectOption('Thursday', 'Thursday'));
        options.add(new SelectOption('Friday', 'Friday'));
        options.add(new SelectOption('Saturday', 'Saturday'));
        options.add(new SelectOption('Sunday', 'Sunday'));
        return options;
    }
    
    public Boolean getSelectedItemExist() {
        if (!queryResultHolderMap.isEmpty()) {
            for (objectWrapper obj : queryResultHolderMap.values()) {
                if (obj.selected == true) return true;
            }
        }
        return false;
    }
    
    //-------- Object holder methods --------//
    
    //######## Navigation Methods ########//
    public PageReference cancel() {
        PageReference backToCourse = new PageReference('/' + initialCourse.Id);
        return backToCourse;
    }
    
    public PageReference nextToPage2() {
        
        system.debug('testCDL: ' +testCDL);
                
        if (selectedType == optionPageType) {
            page2NextButton = 'Next';
        } else {
            page2NextButton = 'Create';
        }

        // query all the viable Delivery Location of the type selected in page1 and set the result into the queryHolderMap      
        //List<LuanaSMS__Delivery_Location__c> viableDL = [SELECT Id, LuanaSMS__Training_Organisation__c FROM LuanaSMS__Delivery_Location__c WHERE LuanaSMS__Training_Organisation__c = :initialCourse.LuanaSMS__Program_Offering__r.LuanaSMS__Training_Organisation__c];
        searchRecord();
        
        // populate the state and country options based on the result of searchRecord above
        Set<String> stateSet = new Set<String>();
        Set<String> countrySet = new Set<String>();
        searchStateOptions = new List<SelectOption>();
        searchCountryOptions = new List<SelectOption>();
        for (objectWrapper obj : queryResultHolderMap.values()) {
            if (!String.isBlank(obj.storedObject.LuanaSMS__State__c)) stateSet.add(obj.storedObject.LuanaSMS__State__c);
            if (!String.isBlank(obj.storedObject.LuanaSMS__Country__c)) countrySet.add(obj.storedObject.LuanaSMS__Country__c);
        }
        
        searchStateOptions.add(new SelectOption('ALL', 'All'));     
        for (String state : stateSet) {
            searchStateOptions.add(new SelectOption(state, state));
        }       
        
        searchCountryOptions.add(new SelectOption('ALL', 'All'));
        for (String country : countrySet) {
            searchCountryOptions.add(new SelectOption(country, country));
        }       

        // if the related Course field is filled in then compile a map of all the CDL involved with the Course      
        if (dummyPaymentToken.Course_Lookup__c != null) {
            List<Course_Delivery_Location__c> cdlList = [SELECT Id, Delivery_Location__c, Course__c, Availability_Day__c, Type__c FROM Course_Delivery_Location__c WHERE Course__c = :dummyPaymentToken.Course_Lookup__c AND Type__c = :selectedType];
            
            // then use the list above to precheck the wrapper list for matching Delivery Location
            for (Course_Delivery_Location__c cdl : cdlList) {
                if (queryResultHolderMap.containsKey(cdl.Delivery_Location__c)) {
                    queryResultHolderMap.get(cdl.Delivery_Location__c).selected = true;
                }
                existingCDLMap.put(cdl.Delivery_Location__c, cdl);              
            }
        }

        // ## TEST ##
        // PageReference page2 = Page.TestPage2;
        
        PageReference page2 = Page.luana_MassCDLPage2;
        
        system.debug('testCDL: ' +testCDL);
        return page2;
    }    
    
    public PageReference nextToPage3() {
        
        system.debug('testCDL: ' +testCDL);

        // Wrapper only methods
        dlWrapperToCreate = new List<objectWrapper>();
        for (objectWrapper objWrap : queryResultHolderMap.values()) {
            if (objWrap.selected) {
                if (existingCDLMap.containsKey(objWrap.storedObject.Id)) {
                    List<String> newAvailableDayList = new List<String>();
                    if (existingCDLMap.get(objWrap.storedObject.Id).Availability_Day__c != null) {
                        String[] currentCDLavailabilityDays = (existingCDLMap.get(objWrap.storedObject.Id).Availability_Day__c).split(';');
                        for (String availableDay : currentCDLavailabilityDays) {
                            newAvailableDayList.add(availableDay);
                        }
                        objWrap.oldValueExist = true;
                    }
                    objWrap.availableDay = newAvailableDayList;
                }
                dlWrapperToCreate.add(objWrap);
            }           
        }
        
        if (dlWrapperToCreate.isEmpty()) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'No Delivery Location was selected. <br/>'));
            return null;
        } else {
            // If the Course Delivery Location type is Workshop, move to Page 3 else move to Page 4 to finalize and show the summary
            if (selectedType == optionPageType) {
                PageReference page3 = Page.luana_MassCDLPage3;              
                system.debug('testCDL: ' +testCDL);
                return page3;
            } else {
                return nextToPage4();
            }
        }
    }
    
    public PageReference nextToPage4() {
                
        cdlToCreateList = new List<Course_Delivery_Location__c>();
        for (objectWrapper objWrap : queryResultHolderMap.values()) {
            if (objWrap.selected) {
                Course_Delivery_Location__c cdl = new Course_Delivery_Location__c();
                cdl.Name = objWrap.storedObject.Name;
                cdl.Course__c = initialCourse.Id;
                cdl.Delivery_Location__c = objWrap.storedObject.Id;
                cdl.Type__c = selectedType;
                
                List<String> currentAvailableDayList = objWrap.availableDay;
                if (!currentAvailableDayList.isEmpty()) {
                    String availabilityDayString = '';
                    for (String availableDay : currentAvailableDayList) {
                        availabilityDayString += availableDay + ';';
                    }
                    availabilityDayString = availabilityDayString.removeEnd(';');
                    cdl.Availability_Day__c = availabilityDayString; 
                }                                       
                cdlToCreateList.add(cdl);
            }
        }
        
        if (debug) system.debug('cdlToCreateList: ' +cdlToCreateList);
        
        // Generate the CDL and then show the summary status page
        
        // Partial Success DML
        List<Course_Delivery_Location__c> cdlToCreate = cdlToCreateList.clone();
        Database.SaveResult[] insertResults = Database.insert(cdlToCreate, false);
        Map<Id, String> errorMessageMap = new Map<Id, String>();
        Map<Id, String> dlNameMap = new Map<Id, String>();
        List<Course_Delivery_Location__c> successfullCDL = new List<Course_Delivery_Location__c>();
        Integer errorCount = 0; 
        Integer successCount = 0;              
        Integer counter = 0;
        
        for(Database.SaveResult sr : insertResults){
            if (!sr.isSuccess()) {
                errorMessageMap.put(cdlToCreate.get(counter).Delivery_Location__c, sr.getErrors()[0].getMessage());
                dlNameMap.put(cdlToCreate.get(counter).Delivery_Location__c, cdlToCreate.get(counter).Name);
                errorCount ++;
            } else {
                successCount ++;
                successfullCDL.add(cdlToCreate.get(counter));
            }
            counter ++;
        }
                
        if (successCount > 0) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, successCount + ' Course Delivery Locations successfully created.<br/>'));               
        }
                
        if (errorCount > 0) {
            String detailsMsg = '';
            for(Id dlId : errorMessageMap.keySet()){
                detailsMsg += 'Course Delivery Location for ' + dlNameMap.get(dlId) + ' could not be created. Reason: ' + errorMessageMap.get(dlId);
            }
            
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, detailsMsg));
        }                       

        PageReference page4 = Page.luana_MassCDLPage4;
        return page4;               
    }
    
    public PageReference pageDone() {
        PageReference backToCourse = new PageReference('/' + initialCourse.Id);
        return backToCourse;
    }
    //-------- Navigation Methods --------//
    
    //######## Pagination Methods ########//
    public void Previous() {
        con.previous();
    }
    
    public void Next() {
        con.next();        
    }
    
    public void Beginning() {
        con.first();
    }
    
    public void End() {
        con.last();
    }
    
    public Boolean hasNext {        
        get {
            return con.getHasNext();
        }
        set;
    }

    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }
    
    public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set;
    }
    //-------- Pagination Methods --------//
}