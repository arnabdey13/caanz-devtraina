/*
    Controller class for SubmitAssessment.page
    ======================================================
    Changes:
        Feb 2015    Davanti         Created
        Mar 2017    Davanti - RN    Updated for PASA CR001, include applicant details in Description field when PASA application submitted.
*/
public without sharing class SubmitAssessmentController {
    // Global variables 
    public Boolean displayExistingActivatedOrder {get;set;}
    public Boolean bShowHeader {get;set;}   
    public String strUserType{get;set;}   
    public Boolean displayExistingOrder {get;set;}
    public Boolean displayNoMatchThankyou {get;set;}
    public Boolean displaySubmitForAssessmentBtn {get;set;}
    public Boolean displayReturnToApplicationBtn {get;set;}
    public Boolean displayCheckoutBtn {get;set;}
    public Boolean isAlreadySubmitted {get;set;}
    public Boolean bHasErrors {get;set;}
    public Boolean isPaidApp {get;set;}
    public Id ApplicationId {get;set;}
    public Id recordTypeId {get;set;}
    public Boolean displayCheckout {get;set;}
    public String productCode {get;set;}
    public List<Order> orderMainList {get;set;}
    public String ewayURL {get;set;}
    public Application__c application {get;set;}    
    // Internal variables   
    private List<PriceBookEntry> pricebookProductDetails;
    private Boolean requiresFee = false;

    private Order orderObj;
    public String memberURL {get;set;}
    public String memberOf {get;set;}
    public String encodedURL {get;set;}
    
    private static Id rtId_PASAApp = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('PASA Application (Migration Assessment)').getRecordTypeId(); //PASA CR001
    private static Id rtId_B2BOrder = ApexUtil.getRecordTypeId('Order','B2B_Order');//ARTTA-906.
    
    // Constructor
    public SubmitAssessmentController() {
        applicationId = ApexPages.currentPage().getParameters().get('Application__c');
        // Flags for message display
        displayCheckout = false;
        displayNoMatchThankyou = false;
        displayExistingActivatedOrder = false;
        displayExistingOrder = false;
        isAlreadySubmitted = false;
        bHasErrors = false;
        isPaidApp = false;
        // Flags for button display
        displaySubmitForAssessmentBtn = false;
        displayReturnToApplicationBtn = false;
        displayCheckoutBtn = false;
        // Determine if page is viewed from COMMUNITIES PORTAL OR ANONYMOUSLY
        bShowHeader = (UserInfo.getUserType() == 'CspLitePortal')? true:false;
        strUserType= UserInfo.getUserType();
        if(applicationid != null) {
            //Get the special criteria fields, customer ID and other account details, Updated for PASA CR001, added addtional application fields
            //Added Account__c field for ARTTA-906.
            String strQuery = 'select Id, Account__r.OwnerId, Account__r.Member_Of__c, Account__r.Owner.Name, Application_Status__c, Account__c, ';
            strQuery += ' Account__r.PersonEmail,  Account__r.BillingCity, Account__r.BillingStreet, Account__r.BillingCountry, Application_Fee_Paid1__c, ';
            strQuery += ' RecordTypeId, Account__r.Name, Account__r.Member_ID__c, Submitted_by_Migration_Agent__c, Migration_Agent__c, ';
            strQuery += ' Migration_Agent__r.Name, Migration_Agent__r.PersonEmail, Migration_Agent__r.BillingCity, ';
            strQuery += ' Migration_Agent__r.BillingStreet, Migration_Agent__r.BillingCountry, ';
            strQuery += ' Account__r.PersonMailingstreet, Account__r.PersonMailingCity, Account__r.PersonMailingCountry, ';
            strQuery += ' Account__r.PersonOtherStreet, Account__r.PersonOtherCity, Account__r.PersonOtherCountry ';
            strQuery += ' from Application__c where Id=\'' + String.escapeSingleQuotes(applicationId) + '\'';
            system.debug('## strQuery: ' + strQuery);
            
            List<Application__c> applicationList = Database.query(strQuery);
            if(applicationList.size() > 0) {
                application = applicationList.get(0);
                //Method to get pricebook for application form type criteria - should be unique
                pricebookProductDetails = retrievePricebookProduct(application.Id); 
                system.debug('### pricebookProductDetails.size(): ' + pricebookProductDetails.size()); // PASA
                // Product match found - Reference Appendix 1 if no match is found, it is free
                if(pricebookProductDetails.size() > 0) {
                    productCode = pricebookProductDetails.get(0).Product2.ProductCode;      
                    requiresFee = true;

                    orderMainList = [Select Id, NetSuite_Order_ID__c, Status From Order where Application__c =: application.Id];// and status='Activated'];
                    //Check if there is an existing orders with status for this application
                    if(orderMainList.size() > 0) {
                        for(Order ord: orderMainList) {
                            if(ord.Status == 'Activated') {
                                displayExistingActivatedOrder = true;
                                if(Application.Application_Status__c == 'Submitted for Assessment') {                                
                                    // 8/01/2015 logic moved wf and trigger for submitting assessment 
                                    displaySubmitForAssessmentBtn = false;
                                    isAlreadySubmitted = true;
                                }
                                break;
                            }
                        }
                    }

                /* Scenarios */
                } else {
                    //1. Free Application 
                    displayNoMatchThankyou = true;
                    displaySubmitForAssessmentBtn = true;
                    //2. Check if application already submitted
                    if(Application.Application_Status__c == 'Submitted for Assessment') {
                        isAlreadySubmitted = true;
                        displaySubmitForAssessmentBtn = false; //Do not display submit when already submitted
                        displayReturnToApplicationBtn = true;
                    }
                }

                //Paid Applications
                if(pricebookProductDetails.size() > 0) {
                    //3. Application is paid - no existing orders

                    // ## should be commented out - catch temporarily to be sure !!todo new requirement product match with $0 price does not require checkout
                    system.debug('In Scenario 3'); // PASA
                    PriceBookEntry pbeObj = pricebookProductDetails.get(0);
                    system.debug('pbeObj.UnitPrice: ' + pbeObj.UnitPrice); // PASA
                    if(pbeObj.UnitPrice == 0) {
                        requiresFee = false;
                        //1. Free Application 
                        displayNoMatchThankyou = true;
                        displaySubmitForAssessmentBtn = true;
                        //2. Check if application already submitted
                        if(application.Application_Status__c == 'Submitted for Assessment') {
                            isAlreadySubmitted = true;
                            displaySubmitForAssessmentBtn = false; //Do not display submit when already submitted
                            displayReturnToApplicationBtn = true;
                        }                       
                    } else {
                        //Standard match found with unit price > $0
                        displayCheckout = true;
                        isPaidApp = true;
                        displayCheckoutBtn = true;
                        displaySubmitForAssessmentBtn = true;
                        if(orderMainList.size() > 0){ //An existing order has been made
                            //4. Order in draft - no payments created yet - application status not submited 
                            displayExistingOrder = true;    
                            //5. Order is activated - application not submitted
                            if(displayExistingActivatedOrder) {
                                displayCheckoutBtn = false;
                                displaySubmitForAssessmentBtn = true;
                                //6. Order has been activated and status is submitted
                                if(application.Application_Status__c == 'Submitted for Assessment') {
                                    isAlreadySubmitted = true;
                                    displaySubmitForAssessmentBtn = false;
                                }
                            }
                        }
                    }  
                } 
             //Custom Settings
            Service_Integrations__c redirectLinkIntegration = Service_Integrations__c.getInstance(application.account__r.Member_Of__c);              
            memberOf = application.account__r.Member_Of__c;
            memberURL = redirectLinkIntegration.Endpoint_URL__c;
            encodedURL =  EncodingUtil.urlEncode(memberURL, 'UTF-8'); 
            }   
        }   
    }


    // perform checks
    public PageReference performChecks() {
        PageReference pref = null;
        if(strUserType== 'Guest') {
            pref = new PageReference('/apex/ThankYouPage?Application__c=' + application.Id);
            return pref;
        }        
        if(application.Application_Status__c == 'Info Received') {
            pref = new PageReference('/' + Application.id);
        }
        Savepoint sp = Database.setSavepoint();
        application.Application_Status__c = 'Submitted for Assessment';
        Database.saveResult sr1 = Database.update(application, false);
        if(!sr1.IsSuccess()) {
            addPageMessage(Apexpages.Severity.Error, 'PLEASE COMPLETE THE FOLLOWING FIELDS');
            for(Database.Error e:sr1.getErrors()) {
                addPageMessage(Apexpages.Severity.Error, e.getMessage());
                bHasErrors = true;
                pref = null;
            }           
        }
        Database.rollback(sp);  
        return pref;
    } 
    //Input: applicationId, output: productCode - can be used externally
    public static String retrieveProductCode(Id applicationId) {
        String productCode;
        List<PriceBookEntry> pbeList = retrievePricebookProduct(applicationId);
        if(pbeList.size() > 0)
            productCode = pbeList.get(0).Product2.ProductCode;
        return productCode; 
    }
    //Input application Id, output PricebookEntry(product and productCode)
    public static List<PriceBookEntry> retrievePricebookProduct(Id applicationId) {
        String criteriasField = null;
        List<PriceBookEntry> pricebookDetails = new List<PriceBookEntry>();
        //Query application for retrieving the recordTypeId as this method is used externally as well
        String strQuery = 'select Id, RecordTypeId, Account__r.Member_Of__c, Application_Status__c ';
        strQuery += ' from Application__c where Id=\'' + String.escapeSingleQuotes(applicationId) + '\'';
        List<Application__c> applicationList = Database.query(strQuery);
        if(applicationList.size() > 0) {
            //local application scope
            Application__c app = applicationList.get(0);
            //Todo check if application record type matches any product code from Appendix 1. if no match is found, it is free
            Map<Id, RecordType> recordTypeMap = new Map<Id, RecordType>([Select Id, DeveloperName From RecordType where SObjectType='Application__c']); 
            system.debug('## recordTypeMap =>' + recordTypeMap.values());
            String applicationType = recordTypeMap.get(app.recordTypeId).DeveloperName;

            Map<String, Application_Form__c> mapApplicationFormSettings = Application_Form__c.getAll();
            for(String s:mapApplicationFormSettings.keySet()) {
                Application_Form__c setting = mapApplicationFormSettings.get(s);
                if(setting.Record_Type__c == applicationType && setting.ReadOnly__c == false) {
                    criteriasField = setting.Criterias__c; //returns fields string separated by comma
                    break;
                }
            }
            List<String> criteriaList = new List<String>();
            if(criteriasField != null) 
                criteriaList = criteriasField.split(',');

            String strQueryCustom = 'Select Id ';
            if(criteriaList.size() > 0) {
                //faster loop
                for(Integer i = 0, j = criteriaList.size(); i<j; i++) {
                    if(criteriaList.get(i) != 'null')
                    strQueryCustom += ' , ' + criteriaList.get(i);
                }
            }
            strQueryCustom += ' from Application__c where Id=\'' + String.escapeSingleQuotes(applicationId) + '\'';
            system.debug('## strQueryCustom ' + strQueryCustom);
            sObject applicationTemp = Database.query(strQueryCustom); 

            //Assign value of the field 
            List<String> criteriaValueList = new List<String>();
            for(Integer i = 0, j = criteriaList.size(); i<j; i++) {
                criteriaValueList.add((String) applicationTemp.get(criteriaList.get(i)));
            }
            String memberOf = app.account__r.Member_Of__c;              
            system.debug('## applicationType =>' + applicationType ); 
            system.debug('## criteriaValueList =>' + criteriaValueList); 
            system.debug('## memberOf =>' + memberOf);  
            String productCode;
            String prodQuery = 'Select Id, UnitPrice, Pricebook2Id, Product2.ProductCode, Product2.Member_Of__c, NS_Product_IDX__c, NS_Pricing_LevelX__c, Product2Id From PriceBookEntry ' +
                            ' where Product2.Record_Type__c =: applicationType and product2.IsActive = true ';
                            if(criteriaValueList.size() > 0) {
                                for(Integer i = 0, j = criteriaList.size(); i<j; i++) {
                                    String isBlank = '';
                                    if(criteriaValueList.get(i) == null)
                                        prodQuery += ' and Product2.Criteria_' + (i+1) +'__c =:isBlank';
                                    else
                                        prodQuery += ' and Product2.Criteria_' + (i+1) +'__c =\'' + criteriaValueList.get(i) + '\'';
                                }
                            }
                   //Future proof for CAANZ group                    
                   //prodQuery += ' and Product2.Member_Of__c=:memberOf LIMIT 1';
            system.debug('## prodQuery >' + prodQuery);               
            List<PriceBookEntry> pricebookList = Database.query(prodQuery);
            system.debug('### pricebookList: ' + pricebookList); // PASA
            //Future proof for CAANZ group            
            if(pricebookList.size() > 0) {
               for(PriceBookEntry pbe : pricebookList){
                    if(pbe.Product2.Member_Of__c == 'CAANZ') {
                        //todo build more logic add pbe to list
                        if(app.account__r.Member_Of__c == 'ICAA' || app.account__r.Member_Of__c == 'NZICA') 
                            pricebookDetails.add(pbe);
                    } else {
                        if(app.account__r.Member_Of__c == pbe.Product2.Member_Of__c) 
                            pricebookDetails.add(pbe);
                    }
               } 
            }

        }
        system.debug('### pricebookDetails: ' + pricebookDetails); // PASA
        return pricebookDetails;    
    }
    public pageReference submitForAssessment() {
        PageReference pageRef = null;

        // Todo Paid Application check - query orders and check if they have been activated
        if(requiresFee){
            Boolean orderActivated = false;
            List<Order> orderList = [Select Id, NetSuite_Order_ID__c, Status From Order where Application__c =: application.Id];// and status='Activated'];
            //Check if there is an existing orders with status for this application
            if(orderList.size() > 0) {
                for(Order ord: orderList) {
                    if(ord.Status == 'Activated'){
                        orderActivated = true;
                        break;
                    }
                }
                if(orderActivated) {
                    /* 08/01/2015 moved logic on workflow and trigger */   
                    //Update the status if for some cases application status is still on draft
                    if(application.Application_Status__c != 'Info Received') {
                        application.Application_Status__c = 'Submitted for Assessment';
                    }    
                        Database.SaveResult sr = Database.update(application, false);
                        if(!sr.IsSuccess()) {
                            for(Database.Error e:sr.getErrors()) {
                                addPageMessage(Apexpages.Severity.Error, e.getMessage());
                            }
                            return null;
                        }
                   
                    displayExistingActivatedOrder = true;
                    displaySubmitForAssessmentBtn = false;
                    isAlreadySubmitted = true;
                    return new PageReference('/' + application.Id);

                                        
                } 
                else {
                    //Return message - payment is required
                    addPageMessage('Payment must be processed before submitting this application.'); 
                    return null;                    
                }
            } 
            else {
                addPageMessage('Payment must be made before submitting this application. Please click checkout.'); 
                return null;
            }
        } 
        else {
           // Free Application
           // Update the status
           if(application.Application_Status__c != 'Info Received') {
                application.Application_Status__c = 'Submitted for Assessment';
            } 
           Database.SaveResult sr = Database.update(application, false);
            if(!sr.IsSuccess()) {
                for(Database.Error e:sr.getErrors()) {
                    addPageMessage(Apexpages.Severity.Error, e.getMessage());
                }
                return null;
            }
            displayNoMatchThankyou = true;
            displaySubmitForAssessmentBtn = false;
            isAlreadySubmitted = true;
            String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
            String randomNum = '';
            while (randomNum.length() < 10) {
               Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), 62);
               randomNum += chars.substring(idx, idx+1);
            }

            //todo ID is different for pre-prod/uat if internal user redirect to application else for external users redirect to landing page   
            if(UserInfo.getUserType() == 'Guest') {
                //52 uat - 47 preprod
                //return new PageReference('http://charteredaccountantsanz.tfaforms.net/52?tfa_285=' + applicationId + '&r=' + randomNum + '&tfa_2301=' + encodedURL);
                pageRef = new PageReference('/apex/ThankYouPage?Application__c=' + application.Id);         
            } else {
                pageRef = new PageReference('/customer/application?id=' + application.Id);
            }
        }

        return pageRef;
    }
    //Creates the order, calls out to boomi and redirect to eway for payment
    public pageReference checkOut() { 
        //Custom Settings
        Service_Integrations__c ewayIntegration = Service_Integrations__c.getInstance('eway');              
        if(pricebookProductDetails.size() > 0) {
            PriceBookEntry pbeObj = pricebookProductDetails.get(0);
            productCode = pricebookProductDetails.get(0).Product2.ProductCode;
            //Check if there is an existing order with status of draft/pending for this application
            List<Order> orderList = [Select Id, NetSuite_Order_ID__c From Order where Application__c = :application.Id and (status='Draft' or status='Pending')];
            system.debug('## orderList =>' + orderList);
            if(orderList.size() > 0) {
                //Go to NetSuite Eway                   
                if(orderList.get(0).NetSuite_Order_ID__c == null) {
                    // Callout
                    BoomiMockCallOut.doCallout(orderList.get(0).Id);

                }
                ewayURL = ewayIntegration.Endpoint_URL__c;

            } else {                 

                orderObj = new Order();
                //a. Callout to boomi to create an invoice for NS pass the product code, callback url, orders and additional parameters          

                //Create Orders, Updated for PASA CR001, if PASA application and submitted by migration agent, account and billing details = migration agent
                if(application.Submitted_by_Migration_Agent__c && application.RecordTypeId == rtId_PASAApp && application.Migration_Agent__c != null){
                    orderObj.AccountId = application.Migration_Agent__c;
                    orderObj.BillingStreet = application.Migration_Agent__r.BillingStreet;
                    orderObj.BillingCity = application.Migration_Agent__r.BillingCity;
                    orderObj.BillingCountry = application.Migration_Agent__r.BillingCountry;
                    //Applicat Account, RecordTypeIs Populated on Order, added below for ARTTA-906
                    orderObj.Applicant_Account__c = application.Account__c;
                    orderObj.RecordTypeId = rtId_B2BOrder;
                    
                    string orderDescription = 'Applicant details: \r\n\r\n';
                    boolean bUseResidentialAddr = string.isNotBlank(application.Account__r.PersonOtherStreet) || string.isNotBlank(application.Account__r.PersonOtherCity) || string.isNotBlank(application.Account__r.PersonOtherCountry);
                    boolean bUseMailingAddr = string.isNotBlank(application.Account__r.PersonMailingstreet) || string.isNotBlank(application.Account__r.PersonMailingCity) || string.isNotBlank(application.Account__r.PersonMailingCountry);
                        
                    orderDescription += application.Account__r.Name +  '\r\n';
                    
                    if(bUseResidentialAddr){
                        orderDescription += string.isNotBlank(application.Account__r.PersonOtherStreet) ? (application.Account__r.PersonOtherStreet +  '\r\n') : '';
                        orderDescription += string.isNotBlank(application.Account__r.PersonOtherCity) ? (application.Account__r.PersonOtherCity +  '\r\n') : '';
                        orderDescription += string.isNotBlank(application.Account__r.PersonOtherCountry) ? (application.Account__r.PersonOtherCountry + '\r\n') : '';
                    }
                    else if(bUseMailingAddr){
                        orderDescription += string.isNotBlank(application.Account__r.PersonMailingstreet) ? (application.Account__r.PersonMailingstreet +  '\r\n') : '';
                        orderDescription += string.isNotBlank(application.Account__r.PersonMailingCity) ? (application.Account__r.PersonMailingCity +  '\r\n') : '';
                        orderDescription += string.isNotBlank(application.Account__r.PersonMailingCountry) ? (application.Account__r.PersonMailingCountry +  '\r\n') : '';
                    }

                    orderDescription += '\r\n Member ID: ' + application.Account__r.Member_ID__c; 
                    
                    orderObj.Description = orderDescription;
                }
                else{
                    orderObj.AccountId = application.Account__c;
                    orderObj.BillingStreet = application.Account__r.BillingStreet;
                    orderObj.BillingCity = application.Account__r.BillingCity;
                    orderObj.BillingCountry = application.Account__r.BillingCountry;
                    orderObj.Description = 'Boomi Order Created';
                }

                //Create Orders
                orderObj.Status = 'Pending'; //Orders created by Pay Now ref: Order Life Cycle
                orderObj.Application__c = application.Id;
                orderObj.NetSuite_Order_ID__c = null; //  this is updated via boomi upsert and should be blank
                orderObj.EffectiveDate = date.today();
                orderObj.Pricebook2Id = pbeObj.Pricebook2Id;    

                Database.SaveResult sr = Database.insert(orderObj, false);
                if(!sr.IsSuccess()) {
                    for(Database.Error e:sr.getErrors()) {
                        addPageMessage(Apexpages.Severity.Error, e.getMessage());
                    }
                    return null;                    
                }
                
                //Create Order Line Item
                OrderItem orderItem = new OrderItem();
                OrderItem.Quantity = 1;
                OrderItem.UnitPrice = pbeObj.UnitPrice; //can come from boomi if possible
                orderItem.PricebookEntryId = pbeObj.Id;
                orderItem.OrderId = orderObj.id;

                //NS_Product_IDX__c, NS_Pricing_LevelX__c
                //as per order items mapping
                system.debug('## => ' + pbeObj.NS_Product_IDX__c);
                //as per order items mapping
                orderItem.Netsuite_Product_ID__c = pbeObj.NS_Product_IDX__c;
                orderItem.NetSuite_Pricing_Level__c = pbeObj.NS_Pricing_LevelX__c;

                sr = Database.insert(orderItem, false);
                if(!sr.IsSuccess()) {
                    for(Database.Error e:sr.getErrors()) {
                        addPageMessage(Apexpages.Severity.Error, e.getMessage());
                    }
                    return null;
                }

                system.debug('## => ' + orderObj);
                /*moved boomiCalloutSync to synchrous call - note remove @future*/
                
                BoomiMockCallOut.doCallout(orderObj.Id);
                                
                ewayURL = ewayIntegration.Endpoint_URL__c;
                

            }       
            system.debug('## ewayURL =>' + ewayURL); 
        }
         return null;
    }
    

    
    // Add Page Messages
    public void addPageMessage(ApexPages.severity severity, Object objMessage) {
        ApexPages.Message pMessage = new ApexPages.Message(severity, String.valueOf(objMessage));
        ApexPages.addMessage(pMessage);
    }
    public void addPageMessage(Object objMessage) {
        addPageMessage(ApexPages.severity.INFO, objMessage);
    }   

}