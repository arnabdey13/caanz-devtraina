/**
    Developer: WDCi (Lean)
    Development Date: 26/04/2016
    Task #: Employee Enrolment Search wizard
    
    Change Histroy:
    LCA-487 07/07/2016 - WDCi Lean: update authentication redirection to custom login page
**/

public without sharing class luana_EmployeeEnrolmentSearchController {
    
    public Id custCommConId {set; get;}
    public Id custCommAccId {set; get;}
    
    public boolean validForSearch {public get; private set;}
    
    public String selectedEmployer {get; set;}
    public String selectedStudentProgramId {get; set;}
    
    public Date dateFrom {set; get;}
    public Date dateTo {set; get;}
    public String memberName {set; get;}
    public String memberId {set; get;}
    public String courseName {set; get;}
    
    public String revokedBulkCodeId {get; set;}
    
    boolean resetPage = true;
    
    //LCA-442
    private integer queryLimit;
    boolean hasRelatedEmployees;
    boolean hasPaymentTokenStudentProgram;
    
    public luana_EmployeeEnrolmentSearchController() {
        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId()); 
       
        //custCommConId = '003N000000enXgaIAE';
        //custCommAccId = '001N000000iJM8OIAW';
        custCommConId = commUserUtil.custCommConId;
        custCommAccId = commUserUtil.custCommAccId;
        
        validForSearch = false;
                
        resetPage = true;
        queryLimit = 10000;
        hasRelatedEmployees = false;
        hasPaymentTokenStudentProgram = false;
    }
    
    public List<SelectOption> getEmployerOptions(){
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        
        for(Employment_History__c empHistory : luana_EmploymentUtil.getUserActiveEmploymentHistory(custCommAccId)){
            options.add(new SelectOption(empHistory.Employer__c, empHistory.Employer__r.Name));
        }
        
        return options;
    }
    
    public List<LuanaSMS__Student_Program__c> getStudentPrograms(){
        try{
            List<LuanaSMS__Student_Program__c> studentPrograms = new List<LuanaSMS__Student_Program__c>();
            if(setCon != null){
                for(LuanaSMS__Student_Program__c sp: (List<LuanaSMS__Student_Program__c>)setCon.getRecords()){
                    studentPrograms.add(sp);
                }
            }
            return studentPrograms;
        }catch(Exception exp){
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Unexpected error found please contact support. ' + exp.getMessage()));
        }
        return null;
    }
    
    public PageReference forwardToAuthPage(){
        if(UserInfo.getUserType() == 'Guest'){
            //Fix for issue LCA-327 & LCA-328
            //return new PageReference('/CommunitiesLogin');
            return new PageReference(URL.getSalesforceBaseUrl().toExternalForm()+ '/employer/luana_Login?startURL=/employer/luana_EmployeeEnrolmentSearch'); //LCA-487
        }
        else{
            return null;
        }
    }
    
    public PageReference doView(){
        PageReference pageRef = Page.luana_EmployeeEnrolmentView;
        pageRef.getParameters().put('spid', selectedStudentProgramId);
        
        return pageRef;
    }
        
    public PageReference doSearch(){
        setCon = null;
        
        if(resetPage){
            currentPage = 1;
        }
         
        if(selectedEmployer == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please select an employer.'));
            validForSearch = false;
        }else{
            validForSearch = true;
        }
        
        return null;
    }
    
    public PageReference revokePaymentToken(){
        
        try{
            List<Payment_Token__c> paymentTokensUpdateList = new List<Payment_Token__c>();
            for(LuanaSMS__Student_Program__c sp: (List<LuanaSMS__Student_Program__c>)setCon.getRecords()){
                for(Payment_Token__c pt : sp.Payment_Tokens__r){
                    if(pt.Id == revokedBulkCodeId){
                        Payment_Token__c ptUpdate = new Payment_Token__c(Id = pt.Id);
                        ptUpdate.revoke__c = true;
                        
                        paymentTokensUpdateList.add(ptUpdate);
                    }
                }
            }
            
            update paymentTokensUpdateList;
            
            resetPage = false;
            PageReference pageRef = doSearch();
            resetPage = true;
            
            return pageRef;
            
        }catch(Exception exp){
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Unexpected error found please contact support. ' + exp.getMessage()));
        }
        return null;
    }
    
    public Integer noOfRecords{get; set;}
    public Integer size{get;set;}
    public Integer totalNumOfPage {get; set;}
    Integer currentPage;
    
    public ApexPages.StandardSetController setCon {
        get{
            if(selectedEmployer != null){
                if(setCon == null){
                	
                    Luana_Extension_Settings__c noPerPage = Luana_Extension_Settings__c.getValues('Default_Pagination_No_Per_Page');
                    Integer numberPerPageSize = Integer.valueOf(noPerPage.value__c);
                    size = numberPerPageSize;
                    queryLimit = 10000;
                    
                    Set<id> contactIds = new Set<Id>();
                    Set<id> paymentTokenRelatedStudProgIds = new Set<Id>();
                    
                    for(Employment_History__c empHistory : luana_EmploymentUtil.getEmployees(selectedEmployer, memberName, memberId)){
                        contactIds.add(empHistory.Member__r.PersonContactId);
                    }
                    
                    if(!contactIds.isEmpty()){
                    	hasRelatedEmployees = true;
                    } else {
                    	hasRelatedEmployees = false;
                    }
                    
                    for(Payment_Token__c pt : Database.query(getPaymentTokenFilterQuery())){
                    	paymentTokenRelatedStudProgIds.add(pt.Student_Program__c);
                    }
                    
                    if(!paymentTokenRelatedStudProgIds.isEmpty()){
                    	hasPaymentTokenStudentProgram = true;
                    	queryLimit -= paymentTokenRelatedStudProgIds.size();
                    } else {
                    	hasPaymentTokenStudentProgram = false;
                    }
                    
                    system.debug('contactIds :: ' + contactIds);
                    system.debug('paymentTokenRelatedStudProgIds :: ' + paymentTokenRelatedStudProgIds);
                    
                    if((memberName != '' || memberId != '') && !hasRelatedEmployees && !hasPaymentTokenStudentProgram){
                    	noOfRecords = 0;
                        return new ApexPages.StandardSetController(new List<LuanaSMS__Student_Program__c>());
                        
                    } else if(!hasRelatedEmployees && !hasPaymentTokenStudentProgram) {
                    	noOfRecords = 0;
                        return new ApexPages.StandardSetController(new List<LuanaSMS__Student_Program__c>());
                        
                    }
                    
                    string queryString = getStudentProgramFilterQuery();
                    
                    setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
                    
                    setCon.setPageSize(size);
                    noOfRecords = setCon.getResultSize();
                    
                    setCon.setPageNumber(currentPage);
                    
                    Decimal block = decimal.valueOf(noOfRecords).divide(integer.valueOf(size), 2, System.RoundingMode.UP); 
       
                    totalNumOfPage = integer.valueOf(block.round(System.RoundingMode.CEILING));
                    
                }
            }
            return setCon;
        }
        set;
    }
    
    public void next(){
        
        if(setCon.getHasNext()){
            setCon.next();
        }
        
        currentPage = setCon.getPageNumber();
    }
    
    public void previous(){
        
        if(setCon.getHasPrevious()){
            setCon.previous();
        }
        
        currentPage = setCon.getPageNumber();
    }
    
    public void first(){
        
        setCon.first();
        currentPage = setCon.getPageNumber();
    }
    
    public void last(){
        
        setCon.last();
        currentPage = setCon.getPageNumber();

    }
        
    public String getStudentProgramFilterQuery(){
        
        String whereStr = '';
        
        Map<String, String> filterMap = new Map<String, String>();
        
        filterMap.put('LuanaSMS__Course__r.Name', 'LuanaSMS__Course__r.Name like ');
        filterMap.put('LuanaSMS__Contact_Student__c', 'LuanaSMS__Contact_Student__c in: ');
        filterMap.put('RecordType.DeveloperName', 'RecordType.DeveloperName = ');
        
        Map<String, String> filterValue = new Map<String, String>();
		boolean hasFitlerValue = false;
		
        if(courseName !=null && courseName != ''){
            filterValue.put('LuanaSMS__Course__r.Name', '\'%'+String.escapeSingleQuotes(courseName)+'%\'');
            hasFitlerValue = true;
        }else{
            filterValue.put('LuanaSMS__Course__r.Name', null);
        }        
        
        if(hasRelatedEmployees){
            filterValue.put('LuanaSMS__Contact_Student__c', 'contactIds');
            hasFitlerValue = true;
        }else{
            filterValue.put('LuanaSMS__Contact_Student__c', null);
        }
                
        String dateSearch;
        if(dateFrom != null && dateTo == null){
            String fDate = datetime.newInstance(dateFrom.year(), dateFrom.month(),dateFrom.day()).format('yyyy-MM-dd');
            dateSearch = '(LuanaSMS__Enrolment_Date__c >= ' + fDate + ')';
            hasFitlerValue = true;
        } else if(dateTo != null && dateFrom == null){
            String tDate = datetime.newInstance(dateTo.year(), dateTo.month(),dateTo.day()).format('yyyy-MM-dd');
            dateSearch = '(LuanaSMS__Enrolment_Date__c <= ' + tDate + ')';
            hasFitlerValue = true;
        }else if(dateTo != null && dateFrom != null){
            String fDate = datetime.newInstance(dateFrom.year(), dateFrom.month(),dateFrom.day()).format('yyyy-MM-dd');
            String tDate = datetime.newInstance(dateTo.year(), dateTo.month(),dateTo.day()).format('yyyy-MM-dd');
            dateSearch = '(LuanaSMS__Enrolment_Date__c >= ' + fDate + ' and LuanaSMS__Enrolment_Date__c <= ' + tDate + ')';
            hasFitlerValue = true;
        }else{
            dateSearch = null;
        }
        
        if(hasFitlerValue){
        	filterValue.put('RecordType.DeveloperName', '\'Accredited_Module\'');
        }
        
        String filterStr = prepareFilterStr(filterMap, filterValue, dateSearch);
        
        string queryStr = 'Select Id, Opt_In__c, LuanaSMS__Course__r.Name, LuanaSMS__Status__c, LuanaSMS__Contact_Student__c, LuanaSMS__Contact_Student__r.Name, LuanaSMS__Contact_Student__r.Account.Member_Id__c, LuanaSMS__Contact_Student__r.Account.IsPersonAccount, LuanaSMS__Enrolment_Date__c, (select id, Ext_Id__c, Eligible_For_Revoke__c, Code__c, Date_Used__c, Revoke__c, Employer__r.BillingCity from Payment_Tokens__r order by createddate desc limit 1) from LuanaSMS__Student_Program__c ' + filterStr + ' ' + (hasPaymentTokenStudentProgram ? (filterStr != '' ? ' OR (Id in: paymentTokenRelatedStudProgIds) ' : ' where (Id in: paymentTokenRelatedStudProgIds) ') : ' ') + ' Order by LuanaSMS__Enrolment_Date__c DESC NULLS Last LIMIT ' + queryLimit;
        
        System.debug('***queryStr: ' + queryStr);
        return queryStr;
    }
    
    public String getPaymentTokenFilterQuery(){
        
        String whereStr = '';
        
        Map<String, String> filterMap = new Map<String, String>();
        
        filterMap.put('Student_Program__r.LuanaSMS__Course__r.Name', 'Student_Program__r.LuanaSMS__Course__r.Name like ');
        filterMap.put('Student_Program__r.RecordType.DeveloperName', 'Student_Program__r.RecordType.DeveloperName = ');
        filterMap.put('Student_Program__r.LuanaSMS__Contact_Student__r.Account.Name', 'Student_Program__r.LuanaSMS__Contact_Student__r.Account.Name like ');
        filterMap.put('Student_Program__r.LuanaSMS__Contact_Student__r.Account.Member_ID__c', 'Student_Program__r.LuanaSMS__Contact_Student__r.Account.Member_ID__c like ');
        filterMap.put('Employer__c', 'Employer__c = ');
        
        Map<String, String> filterValue = new Map<String, String>();
        boolean hasFitlerValue = false;
        
        if(courseName !=null && courseName != ''){
            filterValue.put('Student_Program__r.LuanaSMS__Course__r.Name', '\'%'+String.escapeSingleQuotes(courseName)+'%\'');
            hasFitlerValue = true;
        }else{
            filterValue.put('Student_Program__r.LuanaSMS__Course__r.Name', null);
        }
        
        if(memberName !=null && memberName != ''){
            filterValue.put('Student_Program__r.LuanaSMS__Contact_Student__r.Account.Name', '\'%'+String.escapeSingleQuotes(memberName)+'%\'');
            hasFitlerValue = true;
        }else{
            filterValue.put('Student_Program__r.LuanaSMS__Contact_Student__r.Account.Name', null);
        }
        
        if(memberId !=null && memberId != ''){
            filterValue.put('Student_Program__r.LuanaSMS__Contact_Student__r.Account.Member_ID__c', '\'%'+String.escapeSingleQuotes(memberId)+'%\'');
            hasFitlerValue = true;
        }else{
            filterValue.put('Student_Program__r.LuanaSMS__Contact_Student__r.Account.Member_ID__c', null);
        }
        
        if(selectedEmployer !=null && selectedEmployer != ''){
            filterValue.put('Employer__c', '\''+String.escapeSingleQuotes(selectedEmployer)+'\'');
            hasFitlerValue = true;
        }else{
            filterValue.put('Employer__c', null);
        }
        
        String dateSearch;
        if(dateFrom != null && dateTo == null){
            String fDate = datetime.newInstance(dateFrom.year(), dateFrom.month(),dateFrom.day()).format('yyyy-MM-dd');
            dateSearch = '(Student_Program__r.LuanaSMS__Enrolment_Date__c >= ' + fDate + ')';
            hasFitlerValue = true;
        } else if(dateTo != null && dateFrom == null){
            String tDate = datetime.newInstance(dateTo.year(), dateTo.month(),dateTo.day()).format('yyyy-MM-dd');
            dateSearch = '(Student_Program__r.LuanaSMS__Enrolment_Date__c <= ' + tDate + ')';
            hasFitlerValue = true;
        }else if(dateTo != null && dateFrom != null){
            String fDate = datetime.newInstance(dateFrom.year(), dateFrom.month(),dateFrom.day()).format('yyyy-MM-dd');
            String tDate = datetime.newInstance(dateTo.year(), dateTo.month(),dateTo.day()).format('yyyy-MM-dd');
            dateSearch = '(Student_Program__r.LuanaSMS__Enrolment_Date__c >= ' + fDate + ' and Student_Program__r.LuanaSMS__Enrolment_Date__c <= ' + tDate + ')';
            hasFitlerValue = true;
        }else{
            dateSearch = null;
        }
        
        if(hasFitlerValue){
        	filterValue.put('Student_Program__r.RecordType.DeveloperName', '\'Accredited_Module\'');
        }
        
        String filterStr = prepareFilterStr(filterMap, filterValue, dateSearch);
        
        string paymentTokenQueryStr = 'Select Id, Student_Program__c from Payment_Token__c ' + filterStr + ' Order by Student_Program__r.LuanaSMS__Enrolment_Date__c DESC NULLS Last LIMIT ' + queryLimit;
        
        System.debug('***paymentTokenQueryStr: ' + paymentTokenQueryStr);
        return paymentTokenQueryStr;
    }
    
    public String prepareFilterStr(Map<String, String> filterMap, Map<String, String> filterValue, String dateSearch){
        
        String whereStr = '';
        
        List<String> filters = new List<String>();
        if(!filterMap.isEmpty()){
            for(String strField: filterMap.keySet()){
                if(filterValue.get(strField) != null){
                    filters.add(filterMap.get(strField) + filterValue.get(strField));
                }
            }
            
        }
        if(dateSearch != null){
            filters.add(dateSearch);
        }
        for(String filterStr: filters){
            whereStr += filterStr + ' and ';
        }
        
        if(whereStr.length() > 0){
            return ' Where ( ' + whereStr.subString(0, whereStr.lastIndexOf('and')) + ' )';
        }else{
            return '';
        } 
        
        return null;
    }
    
}