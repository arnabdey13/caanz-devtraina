/*
    Test class for ExceptionHandler.cls
    ======================================================
    Changes:
    	Aug 2016 	Davanti - DN	Created
*/
@isTest
private class ExceptionHandlerTest {

	private static final String testErrorCode='FIELD_CUSTOM_VALIDATION_EXCEPTION';
	private static final String testErrorMessage='FIELD_CUSTOM_VALIDATION_EXCEPTION, this is the error message: []';
    
    static testMethod void Test_prettifyDMLExceptionMessage(){
    	String uglyErrorMessage=testErrorMessage;
    	system.debug('###uglyErrorMessage='+uglyErrorMessage);
    	String prettyErrorMessage=ExceptionHandler.prettifyDMLExceptionMessage(uglyErrorMessage);
    	system.debug('###prettyErrorMessage='+prettyErrorMessage);
    	system.assert(!prettyErrorMessage.contains(testErrorCode),'ugly error code found in pretty message');
    }
}