global class AccountCheck {

global class AccountExist
{
webservice string SFID;
webservice boolean bExists;
}

webService static AccountExist customerExist(string memberID){
        try{
                Account customerAccount = [SELECT Customer_ID__c, Member_ID__c from account WHERE Member_ID__c = :memberID Limit 1];
                System.debug('Account' + memberID + ' exists - Success');
                
                AccountExist oAccExist= new AccountExist();
                
                oAccExist.SFID = customerAccount.Customer_ID__c;
                oAccExist.bExists = true;
                
                return oAccExist;
                
            }
            catch (Exception e)
            {
                System.debug('An exception occurred: ' + e.getMessage());
                AccountExist oAccExist= new AccountExist();
                
                oAccExist.SFID = '0';
                oAccExist.bExists = false;
                
                return oAccExist;
            }

    }
}