/*------------------------------------------------------------------------------------
Author:        Sumit Gupta
Company:       Salesforce
Description:   Communication preferences component controller

History
Date            Author             Comments
--------------------------------------------------------------------------------------
11-07-2018     Sumit Gupta          Initial Release
------------------------------------------------------------------------------------*/
public with sharing class CaanzCommunicationPreferencesController {

    private static List<String> optoutFields = new List<String>{
        'HasOptedOutOfEmail', 'DoNotCall', 'PersonHasOptedOutOfMail__c', 'PersonHasOptedOutOfSMS__c', 'Marketing_Promo_Opt_Out__c'
    };

    private static List<String> newsletterFields = new List<String>{
        'NL_In_The_Know__c', 'NL_Institute_News__c', 'NL_Asia_Insight__c', 'NL_Educator_e_news__c', 'NL_Insight_UK__c', 'IT_Community_Newsletter__c', 'NL_Accounting_Audit_News_Today_ANT__c', 'NL_Superannuation_Bulletin__c', 'NL_CA_Tax_Bulletin__c', 'Tax_News_NZ__c', 'Financial_Advice_Alert__c', 'Business_Valuation_Community_Newsletter__c', 'Forensic_Accounting_Community_Newsletter__c', 'NL_Acuity_Newsletter__c',
        'NL_Acuity_eNewsletter__c'
    };

    private static List<String> affinityFields = new List<String>{
        'NL_Library_eNews__c', 'NL_Special_Interest_Group__c','CP_Training_Development_Events__c','NL_News_Events_Local_NZICA_Office__c','NL_CA_Program_Candidate__c','CP_Member_Surveys__c'
    };

    private static List<String> productsAndServicesFields = new List<String>{
        'NL_Affinity_Members_Programme__c', 'CP_Member_Benefits__c', 'CP_GAA_Export__c', 'CP_HCF__c' 
        
    };

    @AuraEnabled
    public static String getPreferenceData(Id contactId){
        try{
            return JSON.serialize(new PreferenceData(getPersonAccountData(contactId)));
        } catch (Exception ex){
            return ex.getMessage();
        }
    }

    @AuraEnabled
    public static String savePreferenceData(String preferencesDataJSONString){
        PreferenceData preferenceData = (PreferenceData)JSON.deserialize(preferencesDataJSONString, PreferenceData.class);
        if(preferenceData.contact != null) {
            preferenceData.setFieldValues();
            update preferenceData.contact;
        }
        return JSON.serialize(preferenceData);
    }

    private static Contact getPersonAccountData(Id contactId){
        if(contactId == null){
            contactId = [SELECT ContactId FROM User WHERE Id =: UserInfo.getUserId()].ContactId;
        }
        if(contactId == null) return null;
        String query = 'SELECT Id, ' + String.join(optoutFields, ',') + ',' + String.join(newsletterFields, ',') + 
            + ',' + String.join(affinityFields, ',') + ',' + String.join(productsAndServicesFields, ',') +
            + ' FROM Contact WHERE Id =: contactId';
        return Database.query(query);
    }

    public class PreferenceData{
        Contact contact;
        List<String> optoutValues;
        List<String> newsletterValues;
        List<String> affinityValues;
        List<String> productsAndServicesValues;
        Map<String, String> fieldHelpTextMap;

        public PreferenceData(Contact contact){
            this.contact = contact; 
            optoutValues = new List<String>();
            newsletterValues = new List<String>(); 
            affinityValues = new List<String>();
            productsAndServicesValues = new List<String>();
            fieldHelpTextMap = new Map<String, String>();

            Set<String> availableContactFields = contact.getPopulatedFieldsAsMap().keySet();
            Map<String, Schema.SObjectField> fieldMap = contact.getSObjectType().getDescribe().fields.getMap();

            for(String field : optoutFields){
                if(!availableContactFields.contains(field)) contact.put(field, false);
                if((Boolean)contact.get(field)) optoutValues.add(field); 
                fieldHelpTextMap.put(field, fieldMap.get(field).getDescribe().getInlineHelpText());
            }
            for(String field : newsletterFields){
                if(!availableContactFields.contains(field)) contact.put(field, 'No');
                if((String)contact.get(field) == 'Yes') newsletterValues.add(field); 
                fieldHelpTextMap.put(field, fieldMap.get(field).getDescribe().getInlineHelpText());
            }
            for(String field : affinityFields){
                if(!availableContactFields.contains(field)) contact.put(field, 'No');
                if((String)contact.get(field) == 'Yes') affinityValues.add(field); 
                fieldHelpTextMap.put(field, fieldMap.get(field).getDescribe().getInlineHelpText());
            }
            for(String field : productsAndServicesFields){
                if(!availableContactFields.contains(field)) contact.put(field, 'No');
                if((String)contact.get(field) == 'Yes') productsAndServicesValues.add(field); 
                fieldHelpTextMap.put(field, fieldMap.get(field).getDescribe().getInlineHelpText());
            }
        }

        public void setFieldValues(){
            for(String field : optoutFields){
                contact.put(field, (Object)(optoutValues.contains(field)));
            }
            for(String field : newsletterFields){
                contact.put(field, (Object)(newsletterValues.contains(field) ? 'Yes' : 'No'));
            }
            for(String field : affinityFields){
                contact.put(field, (Object)(affinityValues.contains(field) ? 'Yes' : 'No'));
            }
            for(String field : productsAndServicesFields){
                contact.put(field, (Object)(productsAndServicesValues.contains(field) ? 'Yes' : 'No'));
            }
        }
    }
}