/**
    Developer: WDCi (Lean)
    Development Date: 04/08/2016
    Task #: LCA-687 Bulk Payment Wizard
**/

public with sharing class luana_BulkPaymentWizardController {
    
    public Account account {get; set;}
    public Payment_Token__c filterObj {get; set;} 
    
    public boolean formEnabled {get; set;}
    public boolean dataValidated {get; set;}
    
    
    public ApexPages.StandardSetController con { get; set; }
    
    public integer totalRecords {set; get;}
    
    final static String STUDENTPROGRAM_STATUS_CANCELLED = 'Cancelled';
    final static String STUDENTPROGRAM_STATUS_DISCONTINUED = 'Discontinued';
    
    Map<Id, PaymentTokenWrapper> paymentTokensWrapper;
    
    integer pageSize;
    String currencyCode;
    String pricebookId;
    
    Set<Id> productIds;
    
    public luana_BulkPaymentWizardController (ApexPages.StandardController controller){
        
        if(Test.isRunningTest()){
            account = (Account) controller.getRecord();
            account = [select id, name, Affiliated_Branch_Country__c from Account where Id =: account.Id];
        } else {
            controller.addFields(new List<String>{'Id', 'Name', 'Affiliated_Branch_Country__c'});
            this.account = (Account) controller.getRecord();
        }
        
        init();
    }
    
    public void init(){
    	dataValidated = false;
    	formEnabled = false;
    	
        pageSize = integer.valueOf(Luana_Extension_Settings__c.getValues('Default_Pagination_No_Per_Page').value__c);
        
        filterObj = new Payment_Token__c();
        
        con = new ApexPages.StandardSetController(new List<Payment_Token__c>());
        paymentTokensWrapper = new Map<Id, PaymentTokenWrapper>();
        totalRecords = 0;
        currencyCode = '';
        pricebookId = '';
        productIds = new Set<Id>();
        
        String currencyKey = '';
        String pricebookKey = '';
        
    	if(account.Affiliated_Branch_Country__c != null){
    		
    		pricebookKey = 'Pricebook_' + account.Affiliated_Branch_Country__c;
    		currencyKey = 'Currency_' + account.Affiliated_Branch_Country__c;
    		
    		if(Luana_Extension_Settings__c.getValues(currencyKey) != null && Luana_Extension_Settings__c.getValues(pricebookKey) != null){
    			currencyCode = Luana_Extension_Settings__c.getValues(currencyKey).Value__c;
    			pricebookId = Luana_Extension_Settings__c.getValues(pricebookKey).Value__c;
    			
    			formEnabled = true;
    			
    		} else {
                addPageMessage(ApexPages.severity.FATAL, 'Unfortunately, we are not able to determine the currency/pricebook to be used based on the company\'s Affiliated Branch Country. Please check the data and run the wizard again.');
    		}
    		
        } else {
            addPageMessage(ApexPages.severity.FATAL, 'Unfortunately, we are not able to determine the company\'s Affiliated Branch Country. Please check the data and run the wizard again.');
        }
    }
    
    public PageReference doSearch(){
    	
    	Date fromDate = filterObj.Date_Used__c;
    	Date toDate = filterObj.Expiry_Date__c;
    	Id courseId = filterObj.Course__c;
    	Id employerId = account.Id;
    	
    	dataValidated = false;
    	
    	productIds = new Set<Id>();
    	
    	String paymentSOQL = 'select id, Name, Code__c, Date_Used__c, Employment_Token__c, Contact__c, Student_Program__c, Student_Program__r.LuanaSMS__Status__c, Course__c, Expiry_Date__c , Employment_Token__r.Discounted_Price__c, Employment_Token__r.Cancellation_Fee__c, Token_Currency__c, Apply_Cancellation_Fee__c, Billed__c, Billing_Date__c';
    	paymentSOQL += ', Student_Program__r.Name, Student_Program__r.LuanaSMS__Contact_Student__r.Account.Name';
    	paymentSOQL += ', Course__r.Netsuite_Event_Code_NZ__c, Course__r.Netsuite_Event_Code__c, Course__r.LuanaSMS__Start_Date__c, Course__r.End_Date__c, Course__r.LuanaSMS__Program_Offering__c, Course__r.LuanaSMS__Program_Offering__r.AU_Product__c, Course__r.LuanaSMS__Program_Offering__r.NZ_Product__c, Course__r.LuanaSMS__Program_Offering__r.INT_Product__c';
    	paymentSOQL += ' from Payment_Token__c';
    	paymentSOQL += ' where Employment_Token__r.Employer__c =: employerId and Date_Used__c >=: fromDate and Date_Used__c <=: toDate and Revoke__c = false and Billed__c = false';
    	
    	if(filterObj.Course__c != null){
    		paymentSOQL += ' and Course__c =: courseId';
    	}
    	
    	if(!filterObj.Apply_Cancellation_Fee__c){
    		paymentSOQL += ' and (Student_Program__r.LuanaSMS__Status__c not in (\'' + STUDENTPROGRAM_STATUS_CANCELLED + '\', \'' + STUDENTPROGRAM_STATUS_DISCONTINUED + '\'))';
    	}
    	
    	paymentSOQL += ' order by Name';
    	
    	List<Payment_Token__c> paymentTokens = new List<Payment_Token__c>();
    	Set<String> currencyTypes = new Set<String>();
    	
    	for(Payment_Token__c pToken : Database.query(paymentSOQL)){
    		
    		if(pToken.Course__c != null){
    			
    			if((currencyCode == 'AUD' && pToken.Course__r.LuanaSMS__Program_Offering__r.AU_Product__c != null) || 
    				(currencyCode == 'NZD' && pToken.Course__r.LuanaSMS__Program_Offering__r.NZ_Product__c != null) ||
    				(currencyCode == 'INT' && pToken.Course__r.LuanaSMS__Program_Offering__r.INT_Product__c != null)
    			){
	                paymentTokens.add(pToken);
    				
    				currencyTypes.add(pToken.Token_Currency__c);
    				
    				if(currencyCode == 'AUD'){
    					productIds.add(pToken.Course__r.LuanaSMS__Program_Offering__r.AU_Product__c);
    					
    				} else if(currencyCode == 'NZD'){
    					productIds.add(pToken.Course__r.LuanaSMS__Program_Offering__r.NZ_Product__c);
    					
    				} else if(currencyCode == 'INT'){
    					productIds.add(pToken.Course__r.LuanaSMS__Program_Offering__r.INT_Product__c);
    					
    				}
    				
	            } else {
            		addPageMessage(ApexPages.severity.FATAL, 'Unfortunately, we are not able to determine the ' + currencyCode + ' product id of the program offering (' + pToken.Course__r.LuanaSMS__Program_Offering__c + '). Please check the data and run the wizard again.');
	            }
    		} else {
            	addPageMessage(ApexPages.severity.FATAL, 'Unfortunately, we are not able to determine the course of the payment token (' + pToken.Id + '). Please check the data and run the wizard again.');
    		}
    		
    	}
    	
    	paymentTokensWrapper = new Map<Id, PaymentTokenWrapper>();
    	
    	if(currencyTypes.size() > 1){
            addPageMessage(ApexPages.severity.FATAL, 'The result contains payment token that is using different currecy code. Please check the data and run the wizard again.');
            
            con = new ApexPages.StandardSetController(new List<Payment_Token__c>());
            
    	} else {
	    	con = new ApexPages.StandardSetController(paymentTokens);
	    	
	    	for(Payment_Token__c pToken : paymentTokens){
	    		PaymentTokenWrapper ptWrapper = new PaymentTokenWrapper(pToken, false, (pToken.Student_Program__r.LuanaSMS__Status__c == STUDENTPROGRAM_STATUS_CANCELLED || pToken.Student_Program__r.LuanaSMS__Status__c == STUDENTPROGRAM_STATUS_DISCONTINUED ? true : false));
	    		
	    		paymentTokensWrapper.put(pToken.Id, ptWrapper);
	    		
	    		dataValidated = true;
	    	}
    	}
	    
	   	if(dataValidated){
	    	con.setPageSize(pageSize);
	    	totalRecords = con.getResultSize();
    	}
    	
    	return null;
    }
    
    public PageReference doSave(){
    	
    	//TODO need to create order records here
    	//set billed and billing date
    	//when creating order, need to get product price from au/nz/int product
    	
    	SavePoint sp = Database.setSavepoint();
    	
    	try{
    		Map<String, PricebookEntry> productDefaultPrices = new Map<String, PricebookEntry>();
    		
    		for(PricebookEntry pbe : [select Id, Product2Id, UnitPrice, Product2.NetSuite_Internal_Id__c from PricebookEntry where Pricebook2Id =: pricebookId and Product2Id in: productIds]){
    			productDefaultPrices.put(pbe.Product2Id, pbe);
    		}
    		
    		List<Payment_Token__c> paymentTokensUpdate = new List<Payment_Token__c>();
	    	List<OrderItem> newOrderLines = new List<OrderItem>();
	    	
	    	for(PaymentTokenWrapper ptWrapper : paymentTokensWrapper.values()){
	    		if(ptWrapper.isSelected){
	    			
	    			//for update payment token record
	    			Payment_Token__c ptTokenUpdate = new Payment_Token__c(Id = ptWrapper.paymentTokenObj.Id);
	    			ptTokenUpdate.Apply_Cancellation_Fee__c = ptWrapper.paymentTokenObj.Apply_Cancellation_Fee__c;
	    			ptTokenUpdate.Billed__c = true;
	    			ptTokenUpdate.Billing_Date__c = system.today();
	    			
	    			paymentTokensUpdate.add(ptTokenUpdate);
	    			
	    			//for create new order line
	    			PricebookEntry tempPBE;
	    			if(currencyCode == 'AUD'){
    					tempPBE = productDefaultPrices.get(ptWrapper.paymentTokenObj.Course__r.LuanaSMS__Program_Offering__r.AU_Product__c);
    				} else if(currencyCode == 'NZD'){
    					tempPBE = productDefaultPrices.get(ptWrapper.paymentTokenObj.Course__r.LuanaSMS__Program_Offering__r.NZ_Product__c);
    				} else if(currencyCode == 'INT'){
    					tempPBE = productDefaultPrices.get(ptWrapper.paymentTokenObj.Course__r.LuanaSMS__Program_Offering__r.INT_Product__c);
    				}
	    			
	    			
	    			OrderItem newOrderLine = new OrderItem();
	    			newOrderLine.PricebookEntryId = tempPBE.Id;
	    			newOrderLine.Start_Date__c = ptWrapper.paymentTokenObj.Course__r.LuanaSMS__Start_Date__c;
	    			newOrderLine.End_Date__c = ptWrapper.paymentTokenObj.Course__r.End_Date__c;
	    			newOrderLine.Quantity = 1;
	    			newOrderLine.NetSuite_Product_ID__c = tempPBE.Product2.NetSuite_Internal_Id__c;
	    			newOrderLine.Netsuite_Event_Code__c = (account.Affiliated_Branch_Country__c != null && account.Affiliated_Branch_Country__c == 'New Zealand' ? ptWrapper.paymentTokenObj.Course__r.Netsuite_Event_Code_NZ__c : ptWrapper.paymentTokenObj.Course__r.Netsuite_Event_Code__c);
	    			newOrderLine.Description = ptWrapper.paymentTokenObj.Code__c + ' - ' + (ptWrapper.paymentTokenObj.Student_Program__r.Name == null ? 'Unknown SP#' : ptWrapper.paymentTokenObj.Student_Program__r.Name) + ' - ' + (ptWrapper.paymentTokenObj.Student_Program__r.LuanaSMS__Contact_Student__r.Account.Name == null ? 'Unknown Student' : ptWrapper.paymentTokenObj.Student_Program__r.LuanaSMS__Contact_Student__r.Account.Name);
	    			
	    			if(ptWrapper.paymentTokenObj.Apply_Cancellation_Fee__c){
	    				newOrderLine.UnitPrice = ptWrapper.paymentTokenObj.Employment_Token__r.Cancellation_Fee__c;
	    				newOrderLine.Description += ' (' + ptWrapper.paymentTokenObj.Student_Program__r.LuanaSMS__Status__c + ')';
	    			} else {
	    				if(ptWrapper.paymentTokenObj.Employment_Token__r.Discounted_Price__c != null){
	    					newOrderLine.UnitPrice = ptWrapper.paymentTokenObj.Employment_Token__r.Discounted_Price__c;
	    				} else {
	    					newOrderLine.UnitPrice = tempPBE.UnitPrice;
	    				}
	    			}
	    			
	    			//we will only charge if apply cancellation fee or not cancelled/discontinued, however we will warn the user to remove from selection
	    			if(ptWrapper.paymentTokenObj.Student_Program__r.LuanaSMS__Status__c == STUDENTPROGRAM_STATUS_CANCELLED || ptWrapper.paymentTokenObj.Student_Program__r.LuanaSMS__Status__c == STUDENTPROGRAM_STATUS_DISCONTINUED){
	    				if(!ptWrapper.paymentTokenObj.Apply_Cancellation_Fee__c){
							
							addPageMessage(ApexPages.severity.FATAL, 'You have selected a cancelled enrolment record without "Apply Cancellation Fee" checked. Please deselect the record if you do not want to include it in the billing.');
							
	    					return null;
	    				}
	    			}
	    			
	    			newOrderLines.add(newOrderLine);
	    		}
	    	}
    		
	    	Order newOrder = new Order();
	    	newOrder.AccountId = account.Id;
	    	newOrder.EffectiveDate = system.today();
	    	newOrder.Status = 'Draft';
	    	newOrder.Pricebook2Id = pricebookId;
	    	
	    	insert newOrder;
	    	
	    	for(OrderItem orderLine : newOrderLines){
	    		orderLine.OrderId = newOrder.Id;
	    	}
    		
    		insert newOrderLines;
    		update paymentTokensUpdate;
    		
    		newOrder.Status = 'Activated';
    		update newOrder;
    		
    		addPageMessage(ApexPages.severity.INFO, 'Order is created successfully. Please click <a href="/' + newOrder.Id + '">here</a> to view the record.');
    		
    		init();
    		
    	} catch (Exception exp){
    		addPageMessage(ApexPages.severity.FATAL, 'Problem creating payment order. Error: ' + exp.getMessage() + ' - ' + exp.getStackTraceString());
    		Database.rollback(sp);
    	}
    	
    	return null;
    }
    
    public List<PaymentTokenWrapper> getCurrentPagePaymentTokens(){
    	
    	List<PaymentTokenWrapper> currentList = new List<PaymentTokenWrapper>();
    	
    	for(Payment_Token__c pToken : (List<Payment_Token__c>)con.getRecords()){
    		currentList.add(paymentTokensWrapper.get(pToken.Id));
    	}
    	
    	return currentList;
    }
    
    // indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return con.getHasNext();
        }
        set;
    }

    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }

    // returns the page number of the current page set
    public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set;
    }

    // returns the first page of records
    public void first() {
        con.first();
    }

    // returns the last page of records
    public void last() {
        con.last();
    }

    // returns the previous page of records
    public void previous() {
        con.previous();
    }

    // returns the next page of records
    public void next() {
        con.next();
    }
     
    public class PaymentTokenWrapper{
    	
    	public boolean isSelected {set;get;}
    	public boolean allowApplyCancellation {set; get;}
    	public Payment_Token__c paymentTokenObj {set;get;}
    	
    	public PaymentTokenWrapper(Payment_Token__c paymentTokenObj, boolean isSelected, boolean allowApplyCancellation){
    		this.paymentTokenObj = paymentTokenObj;
    		this.isSelected = isSelected;
    		this.allowApplyCancellation = allowApplyCancellation;
    	}
    	
    }
    
    public void addPageMessage(ApexPages.severity sev, String msg){
    	ApexPages.Message warningmsg = new ApexPages.Message(sev, msg);
        ApexPages.addmessage(warningmsg);
    }
    
}