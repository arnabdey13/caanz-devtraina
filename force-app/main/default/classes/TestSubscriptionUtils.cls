public class TestSubscriptionUtils {

    public static final String TEST_USER_PHONE = '0416334889';
    public static final String TEST_USER_MEMBER_ID = '99887766';

    public static Id getTestAccountId() {
        return [select Id from Account where PersonHomePhone = :TEST_USER_PHONE].Id;
    }

    public static void createTestData() {
        Account a = new Account(
                RecordTypeId = RecordTypeCache.getId('Account', 'PersonAccount'),
                FirstName = 'Test', LastName = 'User',
                Salutation = 'Mr',
                PersonEmail = 'apex-tests@testing.com.nz.au',
                Membership_Class__c = 'Full',
                Member_ID__c = TEST_USER_MEMBER_ID,
                Member_Of__c = 'NZICA',
                Communication_Preference__c = 'Home Phone',
                PersonHomePhone = TEST_USER_PHONE,
                PersonOtherCountry = 'Australia',
                PersonOtherStreet= '83 Saggers Road',
                PersonOtherCity='JITARNING',
                PersonOtherPostalCode='6365' 
             );
        insert a;
    }

}