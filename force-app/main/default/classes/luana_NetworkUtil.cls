/**
    Developer: WDCi (Lean)
    Development Date: 15/04/2016
    Task #: LCA-247
    
    Change History:
    LCA-553/LCA-901 10/08/2016 - WDCi Lean: remove noncommunity check
**/

public without sharing class luana_NetworkUtil {
    
    public static String getCommunityPath(){
        if(Network.getNetworkId() != null){
                        
            return Network.communitiesLanding().getUrl().replace('/home/home.jsp', '').replace('/one/one.app', '');
        }
        
        return '';
    }
    
    public static boolean isMemberCommunity(){
        
        if(Test.isRunningTest()){
            return true;
        } else if(Network.getNetworkId() != null){
            return Network.communitiesLanding().getUrl().contains('/member');
        }
        
        return false;
    }
    
    /*LCA-901
    public static boolean isNonMemberCommunity(){
        
        if(Test.isRunningTest()){
            return true;
        } else if(Network.getNetworkId() != null){
            return Network.communitiesLanding().getUrl().contains('/nonmember');
        }
        
        return false;
    }*/
    
    public static boolean isInternal(){
        
        if(Test.isRunningTest()){
            return true;
        } else if(Network.getNetworkId() == null){
            return true;
        }
        
        return false;
    }
    
    //LCA-901
    public static String getTemplateName(){
        if(isMemberCommunity()){
            return 'luana_MemberTemplate';
        } else if(isInternal()){
            return 'luana_InternalTemplate';
        }
        
        return '';
    }
}