/* 
    Developer: WDCi (KH)
    Development Date: 28/06/2016
    Task #: Contributor session Search wizard JIRA issue LCA-722
    
    Change History:
    WDCi - KH: 12/07/2016, include the try catch to avoid user hardcode the Id from url to access the vf page without permission
    PAN:6225 16/05/2019 WDCi Lean: support attendance update
*/
public without sharing class luana_ContributorResBookingDetailCtl {
    
    public Id custCommConId {set; get;}
    public Id custCommAccId {set; get;}
    
    public Boolean isAdobeConnMeetingHost {get; set;}
    public Boolean isWorkshopFacilitator {get; set;}
    public Boolean isExamMarker {get; set;}
    
    public String resourceBookingId;
    public String type;
    
    public Id sessionId;
    public LuanaSMS__Resource_Booking__c resourceBooking {get; set;}
    
    public Id selectedAttendanceId {get; set;}
    
    public List<LuanaSMS__Resource_Booking__c> relatedResourceBookingBySession {get; set;}
    public List<LuanaSMS__Attendance2__c> sessionAttendances {get; set;}

    public boolean enabledAttendanceMarking {get; set;}

    public luana_ContributorResBookingDetailCtl() {
        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId()); 

        custCommConId = commUserUtil.custCommConId;
        custCommAccId = commUserUtil.custCommAccId;
        
        resourceBookingId = ApexPages.currentPage().getParameters().get('id');
        
        isAdobeConnMeetingHost = false;
        isWorkshopFacilitator = false;
        isExamMarker = false;
        
        type = ApexPages.currentPage().getParameters().get('type');
        
        if(type == 'Virtual'){
            isAdobeConnMeetingHost = true;
        }else if(type == 'Workshop'){
            isWorkshopFacilitator = true;
        }else if(type == 'Exam Marking'){
            isExamMarker = true;
        }
        
        resourceBooking = getRelatedResourceBooking();
        
        relatedResourceBookingBySession = new List<LuanaSMS__Resource_Booking__c>();
                
        if(isAdobeConnMeetingHost || isWorkshopFacilitator){
            sessionId = resourceBooking.LuanaSMS__Session__c;
            getRelatedResourceBookingsBySession();

        }

        //PAN:6225
        sessionAttendances = new List<LuanaSMS__Attendance2__c>();
        sessionAttendances = getAttendances();

        //PAN:6225
        enabledAttendanceMarking = false;
        Luana_Extension_Settings__c attSetting = Luana_Extension_Settings__c.getInstance('AttendanceMarking_' + type);
        if(attSetting != null){
            if(attSetting.Value__c == 'true'){
                enabledAttendanceMarking = true;
            }
        }

    }
    
    public LuanaSMS__Resource_Booking__c getRelatedResourceBooking(){
        try{
            LuanaSMS__Resource_Booking__c resourceBooking = [select Id, Name, LuanaSMS__Resource__c, LuanaSMS__Resource__r.Name, LuanaSMS__Contact_Trainer_Assessor__c, 
                                                                        LuanaSMS__Contact_Trainer_Assessor__r.Name, LuanaSMS__Course__c, LuanaSMS__Course__r.Name, LuanaSMS__Session__c, 
                                                                        LuanaSMS__Session__r.Name, LuanaSMS__Session__r.RecordType.DeveloperName, LuanaSMS__Session__r.RecordType.Name, 
                                                                        LuanaSMS__Start_Time__c, LuanaSMS__End_Time__c, LuanaSMS__Session__r.LuanaSMS__Venue__r.Name, CPD_Hours__c, 
                                                                        LuanaSMS__Session__r.LuanaSMS__Start_Time__c, LuanaSMS__Session__r.LuanaSMS__End_Time__c,
                                                                        LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress, LuanaSMS__Session__r.LuanaSMS__Room__r.Name,
                                                                        LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingStreet, LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingCity,
                                                                        LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingState, LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingPostalCode,
                                                                        LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingCountry, LuanaSMS__Session__r.Adobe_Connect_Url__c
                                                                        from LuanaSMS__Resource_Booking__c Where Id =:resourceBookingId and LuanaSMS__Contact_Trainer_Assessor__c =: custCommConId 
                                                                        Order by LuanaSMS__Start_Time__c asc nulls last];
            

            return resourceBooking;
        }Catch(Exception exp){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'You do not have the permission to view the record. Please try again later or contact our support if the problem persists. '));
            return null;
        }
    }
    
    
    public List<LuanaSMS__Resource_Booking__c> getRelatedResourceBookingsBySession(){
        relatedResourceBookingBySession = [select Id, Name, LuanaSMS__Resource__c, LuanaSMS__Resource__r.Name, LuanaSMS__Contact_Trainer_Assessor__c, 
                                                                    LuanaSMS__Contact_Trainer_Assessor__r.Name, LuanaSMS__Course__c, LuanaSMS__Course__r.Name, LuanaSMS__Session__c, 
                                                                    LuanaSMS__Session__r.Name, LuanaSMS__Session__r.RecordType.DeveloperName, LuanaSMS__Session__r.RecordType.Name, 
                                                                    LuanaSMS__Start_Time__c, LuanaSMS__End_Time__c, LuanaSMS__Session__r.LuanaSMS__Venue__r.Name, CPD_Hours__c, 
                                                                    LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress, LuanaSMS__Session__r.LuanaSMS__Room__r.Name,
                                                                    LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingStreet, LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingCity,
                                                                    LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingState, LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingPostalCode,
                                                                    LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingCountry, LuanaSMS__Session__r.Adobe_Connect_Url__c,
                                                                    LuanaSMS__Contact_Trainer_Assessor__r.HomePhone,LuanaSMS__Contact_Trainer_Assessor__r.MobilePhone, LuanaSMS__Contact_Trainer_Assessor__r.Email
                                                                    from LuanaSMS__Resource_Booking__c Where LuanaSMS__Session__c =: sessionId
                                                                    Order by LuanaSMS__Start_Time__c asc nulls last];
        return relatedResourceBookingBySession;
    }
    public List<LuanaSMS__Attendance2__c> getAttendances(){
        
        return [Select Id, Name, LuanaSMS__Contact_Student__c, LuanaSMS__Contact_Student__r.Email, LuanaSMS__Contact_Student__r.Name, LuanaSMS__Contact_Student__r.MobilePhone, LuanaSMS__Start_time__c, LuanaSMS__End_time__c, LuanaSMS__Session_End_Time__c, LuanaSMS__Session_Start_Time__c, 
                                                        LuanaSMS__Student_Program__r.Name, LuanaSMS__Session__r.Name, LuanaSMS__Student_Program__r.LuanaSMS__Course__r.Name, Adobe_Connect_Url__c, 
                                                        LuanaSMS__Attendance_Status__c, LuanaSMS__Contact_Student__r.Account.Preferred_Name__c, LuanaSMS__Contact_Student__r.Account.Member_ID__c //PAN:6225                                                   
                                                        from LuanaSMS__Attendance2__c Where LuanaSMS__Session__c =: sessionId];
        
    }

    public PageReference saveAttendance(){

        boolean allSet = true;

        for(LuanaSMS__Attendance2__c att : sessionAttendances){
            if(String.isBlank(att.LuanaSMS__Attendance_Status__c)){
                allSet = false;

                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure that attendance status is set for all attendees.'));
                break;
            }
        }

        if(allSet){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Attendance status is saved.'));
            update sessionAttendances;
        }

        return null;

    }
    
    public PageReference adobeSessionAccess(){
        try{
            String cookie;
            luana_AdobeConnectUtil acUtil = new luana_AdobeConnectUtil();
            cookie = acUtil.getSessionCookie();
            if(acUtil.doExtLogin(custCommConId, cookie)){
                
                String adobeUrl;
                String sessionName;
                
                adobeUrl = resourceBooking.LuanaSMS__Session__r.Adobe_Connect_Url__c;
                
                if(adobeUrl != null){
                    PageReference pageRef = new PageReference(adobeUrl + '?session=' + cookie);
                    return pageRef;
                }else{
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Error connecting to adobe. Please contact support: adobe connection url is invalid (' + sessionName + ')'));
                    return null;        
                }
            }else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Error connecting to adobe. Please contact support: adobe login fail (' + custCommAccId + ')'));
                return null; 
            }
            
        }Catch(Exception exp){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Error connecting to adobe, Please contact support: ' + exp.getMessage()));
            return null;
        }
    }
    
    public String selectedSessionId {get; set;}
    public pageReference downloadAttendances(){
        
        PageReference pageRef;
        pageRef = Page.luana_ContributorAttDownload;
        pageRef.getParameters().put('id', selectedSessionId);
        
        return pageRef; 
    }
    
    public String getDomainUrl(){
        return luana_NetworkUtil.getCommunityPath();
    } 
}