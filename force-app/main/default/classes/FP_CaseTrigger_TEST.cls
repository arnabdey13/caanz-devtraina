/**
 * Developer: WDCi (LKoh)
 * Date: 22-10-2018
 * Task #: PAN5340 - Flexipath Gained Competency Area related test class
**/
@isTest
public class FP_CaseTrigger_TEST {
    
    static testmethod void validateCaseTrigger() {
        
        Map<String, SObject> assetMap = FP_Test_UTIL.assetGenerator('CaseTrigger');        
        
        Case case1 = FP_Test_UTIL.createCase(assetMap.get('cont1').Id, assetMap.get('rcaCaseID').Id, 'Approved', assetMap.get('ca1').Id);
        
        test.startTest();
        insert case1;
        test.stopTest();
    }
}