/*------------------------------------------------------------------------------------
Author:        Deepthi Thati
Company:       Tech Mahindra
Description:   Test class for mandatory notification controller - CASUB_Mandatory_Notification_Controller

History
Date            Author             Comments
--------------------------------------------------------------------------------------
06-05-2019     Deepthi Thati          Initial Release
------------------------------------------------------------------------------------*/

@isTest(seeAllData=false) 
public class CASUB_Mandatory_Notification_Ctrl_Test {
    private static User currentUser;
    private static Account currentAccount;
    private static Contact currentContact;
    private static Subscription__c subscription;
    private static Questionnaire_Response__c QuestionniareResponse;
    
    
    static {
        currentAccount = CASUB_Mandatory_Notification_Ctrl_Test.createFullMemberAccount(); 
        currentAccount.Affiliated_Branch_Country__c = 'Australia';
        insert currentAccount;
        currentContact = [SELECT Id FROM Contact WHERE AccountId =: currentAccount.Id];
    }
    
    private static User getCurrentUser(){
        Test.startTest(); Test.stopTest(); // Future method needs to run to create the user.
        return currentUser = [SELECT Name FROM User WHERE AccountId =: currentAccount.Id];
    }
    
    // <Create FullMemberAccount>
    public static Account createFullMemberAccount(){
        Account FullMemberAccountObject = new Account();
        FullMemberAccountObject.Salutation = 'Mr.';
        FullMemberAccountObject.FirstName = 'TestFName';
        FullMemberAccountObject.PersonEmail= 'Full_Member'+ CASUB_Mandatory_Notification_Ctrl_Test.getRandomNumber(910) +'@gmail.com';
        FullMemberAccountObject.LastName = 'Test'+ CASUB_Mandatory_Notification_Ctrl_Test.getRandomNumber(910);
        FullMemberAccountObject.RecordTypeId = RecordTypeCache.getId(FullMemberAccountObject, 'Full_Member');
        FullMemberAccountObject.Communication_Preference__c= 'Home Phone';
        FullMemberAccountObject.PersonHomePhone= '1234';
        FullMemberAccountObject.PersonOtherStreet= '83 Saggers Road';
        FullMemberAccountObject.PersonOtherCity='JITARNING';
        FullMemberAccountObject.PersonOtherState='Western Australia';
        FullMemberAccountObject.PersonOtherCountry='Australia';
        FullMemberAccountObject.PersonOtherPostalCode='6365';  
        return FullMemberAccountObject;
    }
    
    // <Create Subscription>
    public static Subscription__c createSubscriptionRecord(){
        Subscription__c subscription = new Subscription__c();
        subscription.Account__c = currentAccount.Id;
        subscription.Contact_Details_Status__c = 'Pending';
        subscription.Obligation_Status__c = 'Pending';
        subscription.Sales_Order_Status__c = 'Pending';
        subscription.Year__c = '2019';
        return subscription;
    }
    
    // <Create Questionnaire Response>
    public static Questionnaire_Response__c createQuestionnaireResponseRecord(){
        Questionnaire_Response__c QuestionnaireResponse = new Questionnaire_Response__c();
        QuestionnaireResponse.Question_1__c= 'Question1';
        QuestionnaireResponse.Question_2__c='Question1';
        QuestionnaireResponse.Question_3__c='Question1';
        QuestionnaireResponse.Question_4__c='Question1';
        QuestionnaireResponse.Question_5__c='Question1';
        QuestionnaireResponse.Question_6__c='Question1';
        QuestionnaireResponse.Question_7__c='Question1';
        QuestionnaireResponse.Question_8__c='Question1';
        QuestionnaireResponse.Question_9__c='Question1';
        QuestionnaireResponse.Question_10__c='Question1';
        QuestionnaireResponse.Question_11__c='Question1';
        QuestionnaireResponse.Question_12__c='Question1';
        QuestionnaireResponse.Question_13__c='Question1';
        QuestionnaireResponse.Question_14__c='Question1';
        QuestionnaireResponse.Question_15__c='Question1';
        QuestionnaireResponse.Question_16__c='Question1';
        QuestionnaireResponse.Question_17__c='Question1';
        QuestionnaireResponse.Question_18__c='Question1';
        QuestionnaireResponse.Question_19__c='Question1';
        QuestionnaireResponse.Question_20__c='Question1';
        QuestionnaireResponse.Question_21__c='Question1';
        QuestionnaireResponse.Question_22__c='Question1';
        QuestionnaireResponse.Question_23__c='Question1';
        QuestionnaireResponse.Question_24__c='Question1';
        QuestionnaireResponse.Question_25__c='Question1';
        QuestionnaireResponse.Question_26__c='Question1';
        QuestionnaireResponse.Question_27__c='Question1';
        QuestionnaireResponse.Question_28__c='Question1';
        QuestionnaireResponse.Question_29__c='Question1';
        QuestionnaireResponse.Question_30__c='Question1'; 
        QuestionnaireResponse.Account__c=currentAccount.Id;
        QuestionnaireResponse.Answer_1__c='Answer1';
        QuestionnaireResponse.Answer_2__c='Answer1';
        QuestionnaireResponse.Answer_3__c='Answer1';
        QuestionnaireResponse.Answer_4__c='Answer1';
        QuestionnaireResponse.Answer_5__c='Answer1';
        QuestionnaireResponse.Answer_6__c='Answer1';
        QuestionnaireResponse.Answer_7__c='Answer1';
        QuestionnaireResponse.Answer_8__c='Answer1';
        QuestionnaireResponse.Answer_9__c='Answer1';
        QuestionnaireResponse.Answer_10__c='Answer1';
        QuestionnaireResponse.Answer_11__c='Answer1';
        QuestionnaireResponse.Answer_12__c='Answer1';
        QuestionnaireResponse.Answer_13__c='Answer1';
        QuestionnaireResponse.Answer_14__c='Answer1';
        QuestionnaireResponse.Answer_15__c='Answer1';
        QuestionnaireResponse.Answer_16__c='Answer1';
        QuestionnaireResponse.Answer_17__c='yes';
        QuestionnaireResponse.Answer_18__c='I personally hold an AFS Licence';
        QuestionnaireResponse.Answer_19__c='Answer1';
        QuestionnaireResponse.Answer_20__c='Answer1';
        QuestionnaireResponse.Answer_21__c='full';
        QuestionnaireResponse.Answer_22__c='Answer1';
        QuestionnaireResponse.Answer_23__c='Answer1';
        QuestionnaireResponse.Privacy_Policy__c=true;
        QuestionnaireResponse.Name='Test Questionnaire Response';
        // QuestionnaireResponse.CreatedDate=System.today();
        QuestionnaireResponse.Residential_Country__c = 'New Zealand';
        QuestionnaireResponse.Year__c = '2019';
        return QuestionnaireResponse;
    }
    
    // <To generate random numbers to main uniquiness of email >
    public static Integer getRandomNumber(Integer size){
        Double d = math.random() * size;
        return d.intValue();
    }
    
    @isTest 
    static void test_CASUB_Mandatory_Notification_Controller_Australia_With_QuestionnaireResponse() {
        System.runAs(getCurrentUser()) {
            subscription = CASUB_Mandatory_Notification_Ctrl_Test.createSubscriptionRecord();
            insert subscription;
            System.debug('subscription' + subscription);
            QuestionniareResponse = CASUB_Mandatory_Notification_Ctrl_Test.createQuestionnaireResponseRecord();
            insert QuestionniareResponse;
            
            CASUB_Mandatory_Notification_Controller.getConfigurationMetaDataForMandatoryObligation('Australia');
            CASUB_Mandatory_Notification_Controller.getConfigurationMetaDataForMandatoryObligationOnCountryChange();
            CASUB_Mandatory_Notification_Controller.upsertQuestionaireResponse(currentAccount.Id,QuestionniareResponse,'Australia','RegistrationAU',subscription);
        	  CASUB_Mandatory_Notification_Controller.upsertQuestionaireResponse(currentAccount.Id,QuestionniareResponse,'Australia','PrivacyConsent',subscription);    

        }
    }
    @isTest 
    static void test_CASUB_Mandatory_Notification_Controller_Australia_Without_QuestionnaireResponse() {
        System.runAs(getCurrentUser()) {
            subscription = CASUB_Mandatory_Notification_Ctrl_Test.createSubscriptionRecord();
            insert subscription;
            
            CASUB_Mandatory_Notification_Controller.getConfigurationMetaDataForMandatoryObligation('Australia');
            CASUB_Mandatory_Notification_Controller.getConfigurationMetaDataForMandatoryObligationOnCountryChange();
            try{
              CASUB_Mandatory_Notification_Controller.upsertQuestionaireResponse(currentAccount.Id,QuestionniareResponse,'Australia','',subscription);    
            }
            catch(Exception ex){
                system.debug('exception-->'+ex.getMessage());
            }
            
        }
    }
    @isTest 
    static void test_CASUB_Mandatory_Notification_Controller_NewZealand_With_QuestionnaireResponse() {
        System.runAs(getCurrentUser()) {
            subscription = CASUB_Mandatory_Notification_Ctrl_Test.createSubscriptionRecord();
            insert subscription;
            System.debug('subscription' + subscription);
            QuestionniareResponse = CASUB_Mandatory_Notification_Ctrl_Test.createQuestionnaireResponseRecord();
            insert QuestionniareResponse;
            
            CASUB_Mandatory_Notification_Controller.getConfigurationMetaDataForMandatoryObligation('New Zealand');
            CASUB_Mandatory_Notification_Controller.getConfigurationMetaDataForMandatoryObligationOnCountryChange();
            CASUB_Mandatory_Notification_Controller.upsertQuestionaireResponse(currentAccount.Id,QuestionniareResponse,'New Zealand','',subscription);
        }
    }
    @isTest 
    static void test_CASUB_Mandatory_Notification_Controller_NewZealand_Without_QuestionnaireResponse() {
        System.runAs(getCurrentUser()) {
            subscription = CASUB_Mandatory_Notification_Ctrl_Test.createSubscriptionRecord();
            insert subscription;
            
            CASUB_Mandatory_Notification_Controller.getConfigurationMetaDataForMandatoryObligation('New Zealand');
            CASUB_Mandatory_Notification_Controller.getConfigurationMetaDataForMandatoryObligationOnCountryChange();
            try{
                CASUB_Mandatory_Notification_Controller.upsertQuestionaireResponse(currentAccount.Id,QuestionniareResponse,'New Zealand','',subscription);
        }
            catch(Exception ex){
                system.debug('exception-->'+ex.getMessage());
            }
        }
    }
    @isTest 
    static void test_CASUB_Mandatory_Notification_Controller_New_Zealand_Configuration() {
        System.runAs(getCurrentUser()) {
            subscription = CASUB_Mandatory_Notification_Ctrl_Test.createSubscriptionRecord();
            insert subscription;
            CASUB_Mandatory_Notification_Controller.getConfigurationMetaDataForMandatoryObligation('New Zealand');
            
        }
    }
  
    
    
    
    
}