public class luana_NMUserAndPermSetUtil {
    @future
    public static void runFutureUpdate(Map<Id, Id> userPermSetMap, Map<Id, Id> existPermSetMap) {
        List<User> userList = [SELECT Id, Email FROM User WHERE Id IN: userPermSetMap.keySet()];
        List<User> updatedUserList = new List<User>();
        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();
        
        for(User user : userList) {
            user.isActive = true;
            user.Username = user.Email;    
            updatedUserList.add(user);
            
            if(!existPermSetMap.containsKey(user.Id))
                psaList.add(new PermissionSetAssignment(AssigneeId = user.Id, PermissionSetId = userPermSetMap.get(user.Id)));
        }
        
        update updatedUserList;
        insert psaList;
    }
}