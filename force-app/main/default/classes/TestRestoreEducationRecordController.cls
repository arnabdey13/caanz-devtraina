@isTest
public class TestRestoreEducationRecordController {

    @testSetup
    public static void setupTestData(){
         Test.startTest();
        Profile profile1 = [Select Id from Profile where name = 'CAANZ Business Management User'];
        System.debug('What is the profile id ' + profile1);
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        User u = new User( UserRoleId = portalRole.Id,
                            ProfileId = profile1.Id,
                            Username = 'testtermsconditions1234423@charteredaccountantsanz.com',
                            Alias = 'batman',
                            Email='testtermsconditions1234423@charteredaccountantsanz.com',
                            EmailEncodingKey='UTF-8',
                            Firstname='Bruce',
                            Lastname='Wayne',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_NZ',
                            TimeZoneSidKey='Pacific/Auckland');
        insert u;  
        System.RunAs(u){
            Account a = new Account(Name='Test Account Name',BillingStreet ='1 Test Street');
           insert a;
            Contact c = new Contact(LastName = 'Contact Last Name', AccountId = a.id);
           insert c;
          CPD_Log_Backup__c cpdLogBackuprecord = new CPD_Log_Backup__c();
            cpdLogBackuprecord.Contact_Student__c = c.id;
            cpdLogBackuprecord.Award_as_Presenter__c = true;
            cpdLogBackuprecord.Bridging__c = false;
            cpdLogBackuprecord.CAHDP__c = false;
            cpdLogBackuprecord.Community_User_Entry__c = true;
            cpdLogBackuprecord.Country__c = 'Australia';
            cpdLogBackuprecord.Date_Commenced__c = Date.today();
            cpdLogBackuprecord.Date_completed__c = Date.today().addDays(1);
            cpdLogBackuprecord.Degree_Country__c = 'NZ';
            cpdLogBackuprecord.Degree_Other__c = 'NA';
            cpdLogBackuprecord.Degree__c = 'CA';
            cpdLogBackuprecord.Enter_university_instituition__c = 'University';
            cpdLogBackuprecord.Event_Course_name__c = 'Webinar';
            cpdLogBackuprecord.Ext_Id__c = 'dsfdrsfds';
            cpdLogBackuprecord.Other_State__c = 'NSW';
            cpdLogBackuprecord.Primary_Qualification__c = true;
            cpdLogBackuprecord.Provider_State__c = 'ACT';
            cpdLogBackuprecord.Qualifying_hours__c = 4;
            cpdLogBackuprecord.Qualifying_hours_type__c = 'test hours';
            cpdLogBackuprecord.Specialist_hours_code__c = 'code';
            cpdLogBackuprecord.State__c = 'VIC';
            cpdLogBackuprecord.Training_and_development__c = true;
            cpdLogBackuprecord.University_professional_organisation__c = a.id;
            cpdLogBackuprecord.Verified__c = true;
            cpdLogBackuprecord.Other_Type__c = 'other';
            cpdLogBackuprecord.Type__c = 'testType';
            cpdLogBackuprecord.Professional_Competency__c = 'Naive';
            cpdLogBackuprecord.Type_of_Activity__c = 'TestActivity';
            cpdLogBackuprecord.Migrated__c = false;
            cpdLogBackuprecord.Description_Of_Activity__c = 'Activity';
            cpdLogBackuprecord.CPD_Processed__c = true;
            cpdLogBackuprecord.Dummy_field_to_trigger_update__c = 'TestData';
            insert cpdLogBackuprecord;
        }
        
        
    }
    
    @isTest
    public static void testrestoreEducationRecord(){
        CPD_Log_Backup__c cpdLogBackup = [Select id, Name from CPD_Log_Backup__c];
        RestoreEducationRecordController.restoreEducationRecord(cpdLogBackup.Id);
    }    
    
}