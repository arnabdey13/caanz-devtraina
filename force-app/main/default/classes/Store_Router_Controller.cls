public without sharing class Store_Router_Controller{ 

//public String site {get; set;}

    @AuraEnabled
    public static void updateUser(String site)
    {
        if (site=='0')
        {
        String CountryCode = 'AU';
        User currentUser = [Select Community_User__c, contactid from User where id =: Userinfo.getUserid()];
        Boolean communityUser = currentUser.Community_User__c;
        
        
        if (communityUser) {
            
            ID AccID  = [Select AccountID from Contact where id =: currentUser.contactId].AccountId;
            CountryCode = [SELECT PersonOtherCountryCode FROM Account WHERE Id =: AccID].PersonOtherCountryCode;
            
        }
        
            if (CountryCode =='NZ')
            { 
               update new User(Id=UserInfo.getUserId(), Netsuite_site_destination__c  = '4');
            }
            else
            {
               update new User(Id=UserInfo.getUserId(), Netsuite_site_destination__c  = '3');
            }
        }
        else
        {
        update new User(Id=UserInfo.getUserId(), Netsuite_site_destination__c  = site);
        }
        
        return;
        
    }
    
    @AuraEnabled
    public static void restoreUser()
    {
        Integer myCPUtime = Limits.getCpuTime();
        Integer start = System.Now().millisecond();
        while(System.Now().millisecond()< start+80){ 
            myCPUtime = Limits.getCpuTime();
            if (myCPUtime > 5000) {
                update new User(Id=UserInfo.getUserId(), Netsuite_site_destination__c  = '1');
                return;
            }
        }
    
      update new User(Id=UserInfo.getUserId(), Netsuite_site_destination__c  = '1');
   }
}