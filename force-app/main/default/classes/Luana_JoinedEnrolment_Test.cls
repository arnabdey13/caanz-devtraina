/**
    Developer: WDCi (kh)
    Development Date:18/03/2016
    Task: Luana Test class for luana_JoinedEnrolment
    
    Change History:
    LCA-921     ??/??/???? - WDCi (KH):     Include personEmail for member
    PAN:6353    05/07/2019 - WDCi (LKoh):   review page added to JoinedEnrolment
**/
@isTest(seeAllData=false)
private class Luana_JoinedEnrolment_Test {
    
    public static User memberUser {get; set;}
    public static Luana_DataPrep_Test dataPrep;
    public static luana_CommUserUtil userUtil {get; set;} 
    
    private static String classNamePrefixLong = 'Luana_JoinedEnrolment_Test';
    private static String classNamePrefixShort = 'lje';
    private static map<String, Id> commProfIdMap {get; set;}
    public static Account memberAccount {get; set;}
    
    public static List<LuanaSMS__Student_Program_Subject__c> spsList;
    public static List<LuanaSMS__Subject__c> subjList;
    public static LuanaSMS__Student_Program__c sp;
    
    public static void initial(){
        //initialize
        dataPrep = new Luana_DataPrep_Test();
        
        //Create all the custom setting
        insert dataPrep.prepLuanaExtensionSettingCustomSettings();
        insert dataPrep.createLuanaConfigurationCustomSetting();
        
        //Create user with Member and Employer community access
        memberAccount = dataPrep.generateNewApplicantAcc('Joe_' + classNamePrefixShort, classNamePrefixLong, 'Full_Member'); //LCA-921 - include personEmail
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.personEmail = 'joe_1_' + classNamePrefixShort + '@gmail.com'; //LCA-921
        memberAccount.Communication_Preference__c= 'Home Phone';
        memberAccount.PersonHomePhone= '1234';
        memberAccount.PersonOtherStreet= '83 Saggers Road';
        memberAccount.PersonOtherCity='JITARNING';
        memberAccount.PersonOtherState='Western Australia';
        memberAccount.PersonOtherCountry='Australia';
        memberAccount.PersonOtherPostalCode='6365';  
        insert memberAccount;
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
    }
    
    public static void setup(){
        //LCA-921
        memberUser = [Select Id, Email, FirstName, LastName, UserName, Name from User Where Email =: 'joe_1_' + classNamePrefixShort+'@gmail.com' limit 1];
        
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
       
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defEmployerPermSetId.value__c));
        
        list<PermissionSetAssignment> permissAssign = [Select Id from PermissionSetAssignment where AssigneeId =: memberUser.Id AND (PermissionSetId =: defMemberPermSetId.value__c OR PermissionSetId =: defEmployerPermSetId.value__c)];
        System.runAs(memberUser){
            if(permissAssign == null)
                insert psas;
        }
        
        userUtil = new luana_CommUserUtil(memberUser.Id);
        
        //Create traning org
        LuanaSMS__Training_Organisation__c trainingOrg = dataPrep.createNewTraningOrg(classNamePrefixLong, classNamePrefixLong, classNamePrefixShort, 'St 123', 'Wollonggong', '2244');
        insert trainingOrg;

        //Create program
        LuanaSMS__Program__c progrm = dataPrep.createNewProgram(classNamePrefixShort, classNamePrefixLong, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        insert progrm;
        
        //Create Product
        List<Product2> prodList = new List<Product2>();
        prodList.add(dataPrep.createNewProduct('FIN_AU', 'AU0001'));
        prodList.add(dataPrep.createNewProduct('FIN_NZ', 'NZ0001'));
        prodList.add(dataPrep.createNewProduct('FIN_INT', 'INT0001'));
        insert prodList;
        
        //Create Program Offering
        LuanaSMS__Program_Offering__c po = dataPrep.createNewProgOffering('PO_' + classNamePrefixLong, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), progrm.Id, trainingOrg.Id, prodList[0].Id,prodList[1].Id, prodList[2].Id, 1, 1);
        insert po;

        //Create course
        LuanaSMS__Course__c course = dataPrep.createNewCourse('Graduate Diploma of Chartered Accounting', po.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        insert course;
        
        //Create Accredited Program
        sp =  dataPrep.createNewStudentProgram(dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), userUtil.custCommConId, course.Id, 'Australia', 'In Progress');
        insert sp;
        
        //Create multiple subjects
        subjList = new List<LuanaSMS__Subject__c>();
        subjList.add(dataPrep.createNewSubject('TAX AU_' + classNamePrefixShort, 'TAX AU_' + classNamePrefixLong, 'TAX AU_' + classNamePrefixShort, 1, 'Module'));
        subjList.add(dataPrep.createNewSubject('TAX NZ_' + classNamePrefixShort, 'TAX NZ_' + classNamePrefixLong, 'TAX NZ_' + classNamePrefixShort, 1, 'Module'));
        subjList.add(dataPrep.createNewSubject('AAA_' + classNamePrefixShort, 'AAA_' + classNamePrefixLong, 'AAA_' + classNamePrefixShort, 1, 'Module'));
        subjList.add(dataPrep.createNewSubject('Cap_' + classNamePrefixShort, 'Capstone_' + classNamePrefixLong, 'Cap_' + classNamePrefixShort, 1, 'Module'));
        subjList.add(dataPrep.createNewSubject('FIN_' + classNamePrefixShort, 'FIN_' + classNamePrefixLong, 'FIN_' + classNamePrefixShort, 55, 'Module'));
        subjList.add(dataPrep.createNewSubject('MAAF_' + classNamePrefixShort, 'MAAF_' + classNamePrefixLong, 'MAAF_' + classNamePrefixShort, 60, 'Module'));
        insert subjList;
        
        spsList = new List<LuanaSMS__Student_Program_Subject__c>();
        for(LuanaSMS__Subject__c sub: subjList){
            spsList.add(dataPrep.createNewStudentProgramSubject(userUtil.custCommConId, sp.Id, sub.Id));
        }
        insert spsList;
    }
    
    public static testMethod void testController() { 

        Test.startTest();
            initial();
        Test.stopTest();
        
        setup();
        
        System.runAs(memberUser){
            // PAN:6353
            luana_JoinedEnrolment je = new luana_JoinedEnrolment(spsList[0].Id,'subName',subjList[0].Id,'cname','status','result','merit',80,false,sp.Id,true,0,'actionurl','resolution',false);
        }
    }
}