/*
 * @author      WDCi (LKoh)
 * @date        04-June-2019
 * @description wrapper class for the wdciProvisionalAppStudentPaper Lightning Web Component
 * 
 * @changehistory
 */
public class wdci_WrapperClass {
    @AuraEnabled
    public Boolean selected {get;set;}    
    @AuraEnabled
    public String uniSubjectId {get;set;}
    @AuraEnabled
    public String uniSubjectName {get;set;}
    @AuraEnabled
    public String uniSubjectCode {get;set;}    

    public wdci_WrapperClass(Boolean checked, FT_University_Subject__c uniSubject) {
        this.selected = checked;        
        this.uniSubjectId = uniSubject.Id;
        this.uniSubjectName = uniSubject.FT_University_Subject_Name__c;
        this.uniSubjectCode = uniSubject.FT_Subject_Code__c;
    }

    public wdci_WrapperClass() {        
    }
}