/**
    Developer: WDCi (Lean)
    Development Date: 05/04/2016
    Task #: Enrollment wizard - Controller for Details Non Accredited Program
**/

public without sharing class luana_EnrolmentWizardDetailsNAController extends luana_EnrolmentWizardObject{
	
	luana_EnrolmentWizardController stdController;
    
    public luana_EnrolmentWizardDetailsNAController(luana_EnrolmentWizardController stdController){
        this.stdController = stdController;
		
    }
    
    public luana_EnrolmentWizardDetailsNAController(){
		
    }
    
    public PageReference detailsNANext(){
    	    	
        return null;
    }
    
    public PageReference detailsNABack(){
        return null;
    }
    
}