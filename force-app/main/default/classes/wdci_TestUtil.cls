@isTest
public with sharing class wdci_TestUtil {

    public static edu_University__c generateUniversity(String uniName, String countryString, String stateString, String statusString) {

        edu_University__c newUniversity = new edu_University__c();
        newUniversity.University_Name__c = uniName;                
        newUniversity.Country__c = countryString;
        newUniversity.State__c = stateString;
        newUniversity.Status__c = statusString;
        return newUniversity;
    }

    public static edu_Degree__c generateDegree(String degreeName, String degreeType, String degreeStatus) {

        edu_Degree__c newDegree = new edu_Degree__c();
        newDegree.Degree_Name__c = degreeName;
        newDegree.Degree_Type__c = degreeType;
        newDegree.Status__c = degreeStatus;
        return newDegree;
    }

    public static edu_University_Degree_Join__c generateUniversityDegreeJoin(Id universityId, Id degreeId) {

        edu_University_Degree_Join__c newUniversityDegreeJoin = new edu_University_Degree_Join__c();
        newUniversityDegreeJoin.University__c = universityId;
        newUniversityDegreeJoin.Degree__c = degreeId;
        return newUniversityDegreeJoin;
    }    

    public static FT_University_Subject__c generateUniversitySubject(Id universityId, String subjectCode, String uniSubjectName, String pathwaySubjectCode, String externalId, Boolean inactiveFlag) {

        FT_University_Subject__c newUniversitySubject = new FT_University_Subject__c();
        newUniversitySubject.FT_University__c = universityId;
        newUniversitySubject.FT_Subject_Code__c = subjectCode;
        newUniversitySubject.Name = uniSubjectName;
        newUniversitySubject.FT_University_Subject_Name__c = uniSubjectName;
        newUniversitySubject.FT_Pathway_Subject_Code__c = pathwaySubjectCode;
        newUniversitySubject.FT_External_ID__c = externalId;
        newUniversitySubject.FT_Inactive__c = inactiveFlag;
        return newUniversitySubject;
    }

    public static FT_University_Degree_Subject__c generateUniversityDegreeSubject(Id uniSubjectId, Id uniDegreeJoinId, String yearChoice, String externalId) {

        FT_University_Degree_Subject__c newUniversityDegreeSubject = new FT_University_Degree_Subject__c();
        newUniversityDegreeSubject.FT_University_Subject__c = uniSubjectId;
        newUniversityDegreeSubject.FT_University_Degree_Join__c = uniDegreeJoinId;
        newUniversityDegreeSubject.FT_Year__c = yearChoice;
        newUniversityDegreeSubject.FT_External_Id__c = externalId;
        return newUniversityDegreeSubject;
    }

    public static Account generateStudent(String firstName, String lastName, String membershipType, String memberId, String memberStatus) {

        Map<String, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName();

        Account newStudentAccount = new Account();
        newStudentAccount.FirstName = firstName;
        newStudentAccount.LastName = lastName;
        newStudentAccount.Membership_Type__c = membershipType;
        newStudentAccount.Member_ID__c = memberId;
        newStudentAccount.Status__c = memberStatus;
        newStudentAccount.RecordTypeId = rtMap.get('Full_Member').getRecordTypeId();
        newStudentAccount.Create_Portal_User__c = true;
        newStudentAccount.Assessible_for_CA_Program__c = false;
        newStudentAccount.Assessible_for_CAF_Program__c = false;
        newStudentAccount.Gender__c = 'Male';
        newStudentAccount.Member_Of__c = 'ICAA';
        newStudentAccount.PersonEmail = 'test@gmail.com';
        newStudentAccount.Salutation = 'Mr';
        newStudentAccount.PersonMailingStreet = 'Level 27, 66-68 Goulburn Street';
        newStudentAccount.PersonMailingCity = 'Sydney';
        newStudentAccount.PersonMailingCountry = 'Australia';
        newStudentAccount.PersonMailingPostalCode = '2000';

        return newStudentAccount;
    }

    public static Contact generateStudent(String studentLastName) {

        Contact newStudent = new Contact();
        newStudent.LastName = studentLastName;
        return newStudent;
    }

    public static FP_Competency_Area__c generateCompetencyArea(String name, String description, Boolean active) {

        FP_Competency_Area__c newCompetencyArea = new FP_Competency_Area__c();
        newCompetencyArea.Name = name;
        newCompetencyArea.FP_Description__c = description;
        newCompetencyArea.FP_Active__c = active;
        return newCompetencyArea;
    }

    public static FT_Pathway__c generatePathway(Id universityId, Id competencyAreaId, String pathway) {

        FT_Pathway__c newPathway = new FT_Pathway__c();        
        newPathway.FT_University__c = universityId;
        newPathway.FT_Competency_Area__c = competencyAreaId;
        newPathway.FT_Pathway__c = pathway;
        return newPathway;
    }

    public static Application__c generateApplication(Id applicantId, String collegePick, String applicationStatus, String priority) {

        Map<String, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Application__c.getRecordTypeInfosByDeveloperName();

        Application__c newApplication = new Application__c();
        newApplication.Account__c = applicantId;
        newApplication.College__c = collegePick;        
        newApplication.Application_Status__c = applicationStatus;
        newApplication.Priority__c = priority;
        newApplication.RecordTypeId = rtMap.get('Provisional_Member').getRecordTypeId();
        return newApplication;
    }
    
    public static edu_Education_History__c generateEducationHistory(Id applicationId, Id applicantId, Id universityDegreeJoinId, String yearCommenced, String yearFinished) {

        edu_Education_History__c newEducationHistory = new edu_Education_History__c();
        newEducationHistory.Application__c = applicationId;        
        newEducationHistory.University_Degree__c = universityDegreeJoinId;
        newEducationHistory.FP_Year_of_Commence__c = yearCommenced;
        newEducationHistory.FP_Year_of_Finish__c = yearFinished;
        //newEducationHistory.FP_Contact__c = applicantId; //PAN:6539 - Edy
        return newEducationHistory;
    }

    // Student Paper
    public static FT_Student_Paper__c generateStudentPaper(Id universitySubjectId, Id educationHistoryId, Boolean subjectPassed) {

        FT_Student_Paper__c newStudentPaper = new FT_Student_Paper__c();
        newStudentPaper.FT_University_Subject__c = universitySubjectId;
        newStudentPaper.FT_Education_History__c = educationHistoryId;
        newStudentPaper.FT_Passed_Credited__c = subjectPassed;
        return newStudentPaper;
    }

    // Subject
    public static LuanaSMS__Subject__c generateSubject(String subjectId, String subjectName, String subjectFlag) {

        LuanaSMS__Subject__c newSubject = new LuanaSMS__Subject__c();
        newSubject.LuanaSMS__Subject_Id__c = subjectId;
        newSubject.LuanaSMS__Subject_Name__c = subjectName;
        newSubject.LuanaSMS__Subject_Flag__c = subjectFlag;
        return newSubject;
    }

    // Deliery Location
    public static LuanaSMS__Delivery_Location__c generateDeliveryLocation(String deliveryLocationName, Id trainingOrganisationId) {

        LuanaSMS__Delivery_Location__c newDeliveryLocation = new LuanaSMS__Delivery_Location__c();
        newDeliveryLocation.Name = deliveryLocationName;
        newDeliveryLocation.LuanaSMS__Delivery_Location_Name__c = deliveryLocationName;
        newDeliveryLocation.LuanaSMS__Training_Organisation__c = trainingOrganisationId;
        return newDeliveryLocation;
    }

    // Student Program Subject
    public static LuanaSMS__Student_Program_Subject__c generateStudentProgramSubject(Id contactId, Id studentProgramId, Id subjectId, Id deliveryLocationId) {

        LuanaSMS__Student_Program_Subject__c newSPS = new LuanaSMS__Student_Program_Subject__c();
        newSPS.LuanaSMS__Contact_Student__c = contactId;
        newSPS.LuanaSMS__Student_Program__c = studentProgramId;
        newSPS.LuanaSMS__Subject__c = subjectId;
        newSPS.LuanaSMS__Delivery_Location__c = deliveryLocationId;
        return newSPS;
    }

    // Packaged Asset for easy startup in test classes
    public static void generateAsset() {

        String currentYear = String.valueOf(system.today().year());
        String educationHistoryCommencedYear = String.valueOf(system.today().addYears(-3).year());
        String uniDegreeSubjectYear = String.valueOf(system.today().addYears(-2).year()); // this should be within the education history duration
        String educationHistoryEndYear = String.valueOf(system.today().addYears(-1).year());

        // University
        edu_University__c testUniversity = wdci_TestUtil.generateUniversity('Universirty of Melbourne', 'Australia', 'Victoria', 'Approved');
        insert testUniversity;
        
        // Degree
        edu_Degree__c testDegree = wdci_TestUtil.generateDegree('Bachelor of Economics', 'Undergraduate', 'Approved');
        insert testDegree;

        // University Degree Join
        edu_University_Degree_Join__c testUniDegreeJoin = wdci_TestUtil.generateUniversityDegreeJoin(testUniversity.Id, testDegree.Id);
        insert testUniDegreeJoin;

        // Uni Subject
        List<FT_University_Subject__c> uniSubjectList = new List<FT_University_Subject__c>();
        FT_University_Subject__c testUniversitySubject1 = wdci_TestUtil.generateUniversitySubject(testUniversity.Id, 'BUSS1030', 'Accounting, Business and Society', 'BUSS1030 Accounting, Business and Society', null, false);
        uniSubjectList.add(testUniversitySubject1);
        
        FT_University_Subject__c testUniversitySubject2 = wdci_TestUtil.generateUniversitySubject(testUniversity.Id, 'ACCT1006', 'Accounting and Financial Management', 'ACCT1006 Accounting and Financial Management', null, false);
        uniSubjectList.add(testUniversitySubject2);
        insert uniSubjectList;

        // Uni Degree Subject
        List<FT_University_Degree_Subject__c> uniDegreeSubjectList = new List<FT_University_Degree_Subject__c>();
        FT_University_Degree_Subject__c testUniversityDegreeSubject1 = wdci_TestUtil.generateUniversityDegreeSubject(testUniversitySubject1.Id, testUniDegreeJoin.Id, uniDegreeSubjectYear, null);
        uniDegreeSubjectList.add(testUniversityDegreeSubject1);

        FT_University_Degree_Subject__c testUniversityDegreeSubject2 = wdci_TestUtil.generateUniversityDegreeSubject(testUniversitySubject2.Id, testUniDegreeJoin.Id, uniDegreeSubjectYear, null);
        uniDegreeSubjectList.add(testUniversityDegreeSubject2);
        insert uniDegreeSubjectList;

        // Student
        Account testStudentAccount = wdci_TestUtil.generateStudent('John', 'Doe', 'Member', '3053164', 'Active');
        insert testStudentAccount;
        /*
        Contact testtudent = wdci_TestUtil.generateStudent('Tester');
        insert testStudent;
        */

        // Competency Area
        FP_Competency_Area__c testCompetencyArea = wdci_TestUtil.generateCompetencyArea('Accounting Systems and Processes', 'CAF001 Accounting Systems and Processes', true);
        insert testCompetencyArea;

        // Pathway
        List<FT_Pathway__c> pathwayList = new List<FT_Pathway__c>();
        FT_Pathway__c testPathway1 = wdci_TestUtil.generatePathway(testUniversity.Id, testCompetencyArea.Id, 'BUSS1030 Accounting, Business and Society AND ACCT1006 Accounting and Financial Management');
        pathwayList.add(testPathway1);

        FT_Pathway__c testPathway2 = wdci_TestUtil.generatePathway(testUniversity.Id, testCompetencyArea.Id, 'BUSS1030 Accounting, Business and Society OR ACCT1006 Accounting and Financial Management');
        pathwayList.add(testPathway2);
        insert pathwayList;
    }

}