/**
    Developer: WDCi (kh)
    Development Date:24/11/2016
    Task: Test class for AAS_CohortAdjustmentController
**/
@isTest(seeAllData=false)
private class AAS_Test_CohortAdjustmentController {

    final static String contextLongName = 'AAS_Test_DataUploadFormatManualCtl_Test';
    final static String contextShortName = 'adum';
    
    public static AAS_Course_Assessment__c courseAss;
    public static AAS_Student_Assessment__c sa;
    public static AAS_Student_Assessment_Section__c sas;
    
    public static List<AAS_Student_Assessment_Section_Item__c> sasiList;
    
    public static void initial(Boolean hasDataLoaded){
        
        Luana_DataPrep_Test dataPrepUtil = new Luana_DataPrep_Test();
        AAS_DataPrep_Test aasdataPrepUtil = new AAS_DataPrep_Test();
        
        insert aasdataPrepUtil.defauleCustomSetting();
        
        Map<String, Id> recordTypeMap = aasdataPrepUtil.getRecordTypeMap('Exam');
        
        
        //Create all the luana custom setting
        insert dataPrepUtil.prepLuanaExtensionSettingCustomSettings();
        insert dataPrepUtil.createLuanaConfigurationCustomSetting();
  
        //Course
        Map<String, SObject> basicAssetMap = aasdataPrepUtil.createBasicAssets(contextLongName, contextShortName, dataPrepUtil, aasdataPrepUtil);
        LuanaSMS__Program_Offering__c po = new LuanaSMS__Program_Offering__c();
        po = (LuanaSMS__Program_Offering__c)basicAssetMap.get('PROGRAMOFFERRING1');

        LuanaSMS__Course__c courseAM = aasdataPrepUtil.createLuanaCourse(po.Id, dataPrepUtil);
        courseAM.LuanaSMS__Allow_Online_Enrolment__c = true;
        insert courseAM;
        
        //CA
        
        courseAss = aasdataPrepUtil.createCourseAssessment(courseAM.Id, 'CA_' + contextShortName);
        courseAss.AAS_Module_Passing_Mark__c = 50;
        if(hasDataLoaded){
            courseAss.AAS_Non_Exam_Data_Loaded__c = true;
            courseAss.AAS_Exam_Data_Loaded__c = true;
        }
        insert courseAss;
        
        //ASS
        AAS_Assessment_Schema_Section__c ass = aasdataPrepUtil.createAsessmentSchemaSection(courseAss.Id, 'ASS_' + contextShortName, recordTypeMap.get('AAS_Assessment_Schema_Section__c:Exam_Assessment'));
        ass.AAS_Passing_Mark__c = 40;
        ass.AAS_Cohort_Adjustment__c = 2;
        insert ass;
        
        // AAA117 Q #1
        List<AAS_Assessment_Schema_Section_Item__c> assiList = new List<AAS_Assessment_Schema_Section_Item__c>();
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'ASSI111 Q #1'));
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'ASSI111 Q #2'));
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'ASSI111 Q #3'));
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'ASSI111 Q #4'));
        insert assiList;
        
        //SA
        sa = aasdataPrepUtil.createStudentAssessment(courseAss.Id);
        insert sa;
        
        //SAS
        sas = aasdataPrepUtil.createStudentAssessmentSection(sa.Id, recordTypeMap.get('AAS_Student_Assessment_Section__c:Exam_Assessment_SAS'));
        insert sas;
        
        //SASI
        sasiList = new List<AAS_Student_Assessment_Section_Item__c>();
        for(AAS_Assessment_Schema_Section_Item__c assi: assiList){
            AAS_Student_Assessment_Section_Item__c sasi = aasdataPrepUtil.createStudentAssessmentSectionItem(sas.Id, recordTypeMap.get('AAS_Student_Assessment_Section_Item__c:Exam_Assessment_SASI'), assi.Id);
            sasi.AAS_Raw_Mark__c = 9;
            sasiList.add(sasi);
        }
        insert sasiList;
          
    }
    
    //COHORT_01
    public static testMethod void test_ExamMain() { 
    
        initial(true);
        
        Test.startTest();
            
            PageReference pageRef = Page.AAS_CohortAdjustment;
            pageRef.getParameters().put('id', String.valueOf(courseAss.Id));
            pageRef.getParameters().put('type', String.valueOf('Exam_Assessment'));
            Test.setCurrentPage(pageRef);
            
            ApexPages.StandardController sc = new ApexPages.StandardController(courseAss);
            AAS_CohortAdjustmentController cohortAdj = new AAS_CohortAdjustmentController(sc);
            
            cohortAdj.courseAssessment = courseAss;
            cohortAdj.getChartData();
            
            
            cohortAdj.doCommit();
            cohortAdj.checkBatchCompletion();
            
            AAS_Assessment_Schema_Section__c ass = [Select Id, AAS_Course_Assessment__c from AAS_Assessment_Schema_Section__c Where AAS_Course_Assessment__c =: courseAss.Id limit 1];
            
            AAS_CohortFlushRecordBatch upBatch = new AAS_CohortFlushRecordBatch(sasiList, sasiList, true, courseAss, ass);
            ID batchprocessid = Database.executeBatch(upBatch);
            
            //courseAss
            AAS_Course_Assessment__c updatedCA = [Select Id, AAS_Cohort_Adjustment_Exam__c,Name, AAS_Cohort_Batch_Processing__c, AAS_Module_Passing_Mark__c, AAS_Non_Exam_Data_Loaded__c, AAS_Exam_Data_Loaded__c from AAS_Course_Assessment__c Where Id =:courseAss.Id];
            //System.assertEquals(updatedCA.AAS_Cohort_Adjustment_Exam__c, true, 'COHORT_03: Course Assessment\'s Cohort Adjustment Exam checkbox is checked');  
            
            
            cohortAdj.doRedirect();
            cohortAdj.doCancel();
            cohortAdj.getRecordTypeOptions();
            
            
            
            //2nd time todo
            /*
            - See the warning message
            - CA's Cohort Adjustment (Exam) checked
            */
            PageReference pageRef2 = Page.AAS_CohortAdjustment;
            pageRef2.getParameters().put('id', String.valueOf(updatedCA.Id));
            pageRef2.getParameters().put('type', String.valueOf('Exam_Assessment'));
            Test.setCurrentPage(pageRef2);
            
            ApexPages.StandardController sc2 = new ApexPages.StandardController(updatedCA);
            AAS_CohortAdjustmentController cohortAdj2 = new AAS_CohortAdjustmentController(sc2);
            
            for(ApexPages.Message msg: ApexPages.getMessages()){
                System.assertEquals(msg.getSummary(), 'There is already a Cohort batch processing at the moment. When the processing is completed, the user who has requested for the Cohort Adjustment will receive an email notification.', 'COHORT_03: message for confirmation re do displayed');
            }
            
            //Test one of the assi has exceed the full mark
            /*
            Update one sasi which exceed the full mark
            */
            List<AAS_Student_Assessment_Section_Item__c> updateSASIList = new List<AAS_Student_Assessment_Section_Item__c>();
            for(AAS_Student_Assessment_Section_Item__c sasi: [Select Id from AAS_Student_Assessment_Section_Item__c Where AAS_Student_Assessment_Section__c =: sas.Id limit 1]){
                sasi.AAS_Raw_Mark__c = 21;
                updateSASIList.add(sasi);
            }
            update updateSASIList;
            
            PageReference pageRef3 = Page.AAS_CohortAdjustment;
            pageRef3.getParameters().put('id', String.valueOf(courseAss.Id));
            pageRef3.getParameters().put('type', String.valueOf('Exam_Assessment'));
            Test.setCurrentPage(pageRef3);
            
            ApexPages.StandardController sc3 = new ApexPages.StandardController(courseAss);
            AAS_CohortAdjustmentController cohortAdj3 = new AAS_CohortAdjustmentController(sc3);
            
            cohortAdj3.markValue = '2';
            cohortAdj3.markTypeValue = 'cohort';
            
            List<SObject> sobj1 = new List<SObject>();
            List<SObject> sobj2 = new List<SObject>();
            
            
            cohortAdj3.doCommit();
            
            
            
        Test.stopTest();
        
        //Check the Cohort batch processing flag is uncheck
        AAS_Course_Assessment__c checkCohortProcessingFlag = [Select Id, AAS_Cohort_Adjustment_Exam__c,Name, AAS_Cohort_Batch_Processing__c, AAS_Module_Passing_Mark__c, AAS_Non_Exam_Data_Loaded__c, AAS_Exam_Data_Loaded__c from AAS_Course_Assessment__c Where Id =:courseAss.Id];
        System.assertEquals(checkCohortProcessingFlag.AAS_Cohort_Batch_Processing__c, false, 'COHORT_04: Cohort Batch Processing uncheck at the end');
    
        List<AAS_Student_Assessment_Section__c> searchSASResults = [Select Id, AAS_Total_Raw_Mark__c, recordTypeId, AAS_Assessment_Schema_Section__r.AAS_Passing_Mark__c, recordType.developerName, AAS_Exceed_Full_Mark_Count__c, AAS_Student_Assessment__r.Total_Non_Exam_Mark__c, AAS_Student_Assessment__r.AAS_Total_Exam_Mark__c from AAS_Student_Assessment_Section__c 
                                            Where AAS_Student_Assessment__r.AAS_Course_Assessment__c =: courseAss.Id and RecordType.DeveloperName ='exam_assessment_sas'];
        
        List<Id> sasIdList = new List<Id>();
        for(AAS_Student_Assessment_Section__c sasObj: searchSASResults){
        sasIdList.add(sasObj.Id);
        }
        
        List<AAS_Student_Assessment_Section_Item__c> searchSASIResults = [Select Id, AAS_Adjustment__c, AAS_Student_Assessment_Section__c, AAS_Eligible_for_Addition__c, AAS_Eligible_for_Deduction__c, AAS_Student_Assessment_Section__r.AAS_Eligible_for_Deduction_Count__c, AAS_Student_Assessment_Section__r.AAS_Eligible_for_Addition_Count__c from AAS_Student_Assessment_Section_Item__c 
                                                                            Where AAS_Student_Assessment_Section__c in: sasIdList];
        
        //AAS_Assessment_Schema_Section__c assResult = [Select Id from AAS_Assessment_Schema_Section__c limit 1];
        AAS_CohortFlushRecordBatch cohortFlushBatch =  new AAS_CohortFlushRecordBatch(searchSASIResults, searchSASIResults, false, courseAss, ass);
        batchprocessid= database.executeBatch(cohortFlushBatch, 200);
    }
    
    //COHORT_01
    public static testMethod void test_NoDataLoadedMain() { 
    
        initial(false);
        
        Test.startTest();
            try{
                PageReference pageRef = Page.AAS_CohortAdjustment;
                pageRef.getParameters().put('id', String.valueOf(courseAss.Id));
                pageRef.getParameters().put('type', String.valueOf('Exam_Assessment'));
                Test.setCurrentPage(pageRef);
                
                ApexPages.StandardController sc = new ApexPages.StandardController(courseAss);
                AAS_CohortAdjustmentController cohortAdj = new AAS_CohortAdjustmentController(sc);
                
                cohortAdj.courseAssessment = courseAss;
                cohortAdj.getChartData();
                
                cohortAdj.doCommit();
            }Catch(Exception exp){
                Boolean expectedExceptionThrown =  exp.getMessage().contains('You are not allow to continue in this step. Please complete the Non Exam and Exam Data Loaded first.') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true, exp.getMessage());
            }
        Test.stopTest();
    }
    
    public static testMethod void test_SuppExamMain() { 
    
        initial(true);
        
        Test.startTest();
            
            PageReference pageRef = Page.AAS_CohortAdjustment;
            pageRef.getParameters().put('id', String.valueOf(courseAss.Id));
            pageRef.getParameters().put('type', String.valueOf('Supp_Exam_Assessment'));
            Test.setCurrentPage(pageRef);
            
            ApexPages.StandardController sc = new ApexPages.StandardController(courseAss);
            AAS_CohortAdjustmentController cohortAdj = new AAS_CohortAdjustmentController(sc);
            
            cohortAdj.courseAssessment = courseAss;
            cohortAdj.getChartData();
            
            
            cohortAdj.doCommit();
            cohortAdj.doRedirect();
            cohortAdj.doCancel();
            
            
        Test.stopTest();
    }
}