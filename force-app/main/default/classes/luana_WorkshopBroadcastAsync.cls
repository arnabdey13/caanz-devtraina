/*
    Developer: WDCi (KH)
    Date: 04/07/2016
    Task #: Luana implementation - Workshop Broadcast
*/

global with sharing class luana_WorkshopBroadcastAsync implements Database.Batchable<sObject>, Database.Stateful {
    
    String courseId;
    String errorRecords;
    boolean hasError;
    
    List<LuanaSMS__Attendance2__c> attList {get; set;}
    
    global luana_WorkshopBroadcastAsync(String courseId){
        this.courseId = courseId;
        this.errorRecords = '';
        this.hasError = false;
        
        this.attList = attList;
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator('select Id, LuanaSMS__Student_Program__c from LuanaSMS__Attendance2__c Where LuanaSMS__Student_Program__c in '+ 
                                        '(Select Id from LuanaSMS__Student_Program__c where LuanaSMS__Course__c = \'' + courseId + '\' and Workshop_Broadcasted__c = false '+ 
                                            'and LuanaSMS__Status__c = \'In Progress\' and Paid__c = true)' + 
                                        'and LuanaSMS__Attendance2__c.Record_Type_Name__c = \'Workshop\'');
    }
    
    
    global void execute(Database.BatchableContext bc, List<sObject> scope){
    
        List<LuanaSMS__Attendance2__c> attendancesToBroadcast = new List<LuanaSMS__Attendance2__c>();
        Set<Id> stuProgSet = new Set<Id>();
        for(SObject sobj : scope){
            LuanaSMS__Attendance2__c att = (LuanaSMS__Attendance2__c)sobj;
            att.Workshop_Broadcasted__c = true;
            
            stuProgSet.add(att.LuanaSMS__Student_Program__c);
            attendancesToBroadcast.add(att);
        }
        
        List<LuanaSMS__Student_Program__c> updateSPBroadcastFlag = new List<LuanaSMS__Student_Program__c>();
        for(LuanaSMS__Student_Program__c sp: [Select Id, Exam_Broadcasted__c from LuanaSMS__Student_Program__c Where Id in: stuProgSet]){
            sp.Workshop_Broadcasted__c = true;
            updateSPBroadcastFlag.add(sp);
        }
        
        integer counter = 0;
        for(Database.SaveResult sr : Database.update(attendancesToBroadcast, false)){
            if(!sr.isSuccess()){
                errorRecords += attendancesToBroadcast.get(counter) + ' - ' + sr.getErrors()[0].getMessage() + '\n';
                hasError = true;
            }
            
            counter ++;
        }
        
        integer spCounter = 0;
        for(Database.SaveResult sr : Database.update(updateSPBroadcastFlag, false)){
            if(!sr.isSuccess()){
                errorRecords += updateSPBroadcastFlag.get(spCounter) + ' - ' + sr.getErrors()[0].getMessage() + '\n';
                hasError = true;
            }
            
            spCounter ++;
        }
      
    }
    
    global void finish(Database.BatchableContext bc){
      
      LuanaSMS__Luana_Log__c errorLog = new LuanaSMS__Luana_Log__c();
      errorLog.LuanaSMS__Notify_User__c = false;
      errorLog.LuanaSMS__Related_Course__c = courseId;
      errorLog.LuanaSMS__Parent_Id__c = courseId;
      
      if(hasError){
        errorLog.LuanaSMS__Status__c = 'Failed';
        errorLog.LuanaSMS__Level__c = 'Fatal';
        errorLog.LuanaSMS__Log_Message__c = 'Error broadcasting to the following record/s:\n\n' + errorRecords;
        
      } else {
        errorLog.LuanaSMS__Status__c = 'Completed';
        errorLog.LuanaSMS__Level__c = 'Info';
      }
      
      insert errorLog;
      
    }
}