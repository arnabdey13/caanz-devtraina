/*
 * @author          WDCi (LKoh)
 * @date            27-June-2019
 * @description     Test class for the wdci_EducationHistoryHandler class
 * @changehistory
 //PAN:6539 - Edy: Changes for Contact lookup > Master Detail in Education History
 */
 @isTest
public class wdci_EducationHistoryTest {

    @TestSetup
    static void generateAssets() {

        wdci_TestUtil.generateAsset();
    }

    @isTest static void testEducationHistoryHandler() {

        String currentYear = String.valueOf(system.today().year());
        String educationHistoryCommencedYear = String.valueOf(system.today().addYears(-3).year());
        String educationHistoryEndYear = String.valueOf(system.today().addYears(-1).year());

        List<Account> studentAccount = [SELECT Id, personcontactid FROM Account WHERE Member_ID__c = '3053164']; //PAN:6539 - Edy
        List<edu_University_Degree_Join__c> uniDegreeJoin = [SELECT Id FROM edu_University_Degree_Join__c];
        List<FT_University_Subject__c> uniSubject = [SELECT Id FROM FT_University_Subject__c];

        // Application
        Application__c testApplication = wdci_TestUtil.generateApplication(studentAccount[0].Id, 'Chartered Accountants', 'Draft', 'Standard');
        insert testApplication;

        // Education History
        edu_Education_History__c testEducationHistory = wdci_TestUtil.generateEducationHistory(testApplication.Id, studentAccount[0].personcontactid, uniDegreeJoin[0].Id, educationHistoryCommencedYear, educationHistoryEndYear); //PAN:6539 - Edy
        insert testEducationHistory;

        // Student Paper
        FT_Student_Paper__c testStudentPaper = wdci_TestUtil.generateStudentPaper(uniSubject[0].Id, testEducationHistory.Id, true);
        insert testStudentPaper;

        Test.startTest();

        // There should be no student paper left anymore
        List<FT_Student_Paper__c> checkStudentPaperBefore = [SELECT Id FROM FT_Student_Paper__c];
        system.debug('checkStudentPaperBefore: ' +checkStudentPaperBefore);

        delete testEducationHistory;    

        // There should be no student paper left anymore
        List<FT_Student_Paper__c> checkStudentPaperAfter = [SELECT Id FROM FT_Student_Paper__c];
        system.debug('checkStudentPaperAfter: ' +checkStudentPaperAfter);

        Test.stopTest();
    }
}