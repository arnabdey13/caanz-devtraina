/* 
    Developer: WDCi (KH)
    Date: 03/Aug/2016
    Task #: Luana Search for Supplementary exam
    
*/
public without sharing class luana_SuppExamSearchCtl {
    
    public List<WrapperStudentProgramSubj> wStuProgramSubjs {get; set;}
    private LuanaSMS__Course__c course;
    
    public List<LuanaSMS__Student_Program_Subject__c> spsList {get; set;}
    public List<Case> caseList {get; set;}
    
    public integer totalRecords {get; set;}
    
    // instantiate the StandardSetController from a query locator
    public ApexPages.StandardSetController con { get; set; }
    
    public Luana_Extension_Settings__c luanaReportConfig;
    
    Map<Id, WrapperStudentProgramSubj> studentProgramWrapperMap;
    
    public luana_SuppExamSearchCtl(ApexPages.StandardController controller) {
        
        if(!Test.isRunningTest()) {
            controller.addFields(new List<String>{'Supp_Exam_Eligible_Score__c', 'Supp_Exam_End_Range__c', 'Name'});
        }
        
        this.course = (LuanaSMS__Course__c) controller.getRecord();
        wStuProgramSubjs = new List<WrapperStudentProgramSubj>();
        
        spsList = new List<LuanaSMS__Student_Program_Subject__c>();
        caseList = new List<Case>();
        
        studentProgramWrapperMap = new Map<Id, WrapperStudentProgramSubj>();
        
        spsList = [Select Id, Name, Result__c, Score__c, Decile_Band__c, LuanaSMS__Student_Program__r.LuanaSMS__Status__c, LuanaSMS__Student_Program__c,
                    LuanaSMS__Student_Program__r.LuanaSMS__Course__c, LuanaSMS__Student_Program__r.LuanaSMS__Course__r.Name, LuanaSMS__Student_Program__r.Supp_Exam_Charge__c,
                    LuanaSMS__Student_Program__r.recordType.Name, LuanaSMS__Student_Program__r.Name, LuanaSMS__Student_Program__r.LuanaSMS__Contact_Student__r.Name
                    from LuanaSMS__Student_Program_Subject__c 
                    where LuanaSMS__Student_Program__r.LuanaSMS__Course__c =: course.Id
                    and LuanaSMS__Student_Program__r.recordType.DeveloperName = 'Accredited_Module'
                    //and LuanaSMS__Student_Program__r.LuanaSMS__Status__c = 'Fail'
                    and LuanaSMS__Student_Program__r.Supp_Exam_Special_Consideration_Approved__c = null
                    and (Score__c >=: course.Supp_Exam_Eligible_Score__c AND Score__c <=: course.Supp_Exam_End_Range__c)];
        
        totalRecords = spsList.size();
        
        List<Id> spList = new List<Id>();
        for(LuanaSMS__Student_Program_Subject__c sps: spsList){
            spList.add(sps.LuanaSMS__Student_Program__c);
        }
        
        for(Case caseObj: [Select Id, Subject, CaseNumber, Student_Program__c from Case Where RecordType.DeveloperName = 'Request_Special_Consideration' and Student_Program__c In: spList]){
            caseList.add(caseObj);
        }
        
        //Set standardSetController for pagination
        con = new ApexPages.StandardSetController(spsList);
        //con.setPageSize(integer.valueOf(Luana_Extension_Settings__c.getValues('Default_Pagination_No_Per_Page').value__c));             
        con.setPageSize(2);
        
        for(LuanaSMS__Student_Program_Subject__c sps : spsList){
          WrapperStudentProgramSubj wSPS = new WrapperStudentProgramSubj(sps);
          studentProgramWrapperMap.put(sps.Id, wSPS);
          
        }
        
        luanaReportConfig = Luana_Extension_Settings__c.getValues('Supp Exam Search Report Params');
        
        String detailsMsg = 'Please see the<b><a href="'+luanaReportConfig.Value__c + String.valueOf(course.Id).substring(0, 15) + '" target="_blank">Approved List</a></b>for Course ('+course.Name +')';
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, detailsMsg));
        
    }

    // returns a list of wrapper objects for the sObjects in the current page set
    public List<WrapperStudentProgramSubj> getStuProgramSubjs() {
        wStuProgramSubjs = new List<WrapperStudentProgramSubj>();
        for (LuanaSMS__Student_Program_Subject__c sps : (List<LuanaSMS__Student_Program_Subject__c>)con.getRecords()){
            wStuProgramSubjs.add(studentProgramWrapperMap.get(sps.Id));
        }
        
        for(WrapperStudentProgramSubj wSps: wStuProgramSubjs){
            integer unresolveSpecialConsiderationCaseCount = 1;
            String caseNumber = '';
            for(Case caseObj: caseList){
                if(wSps.spsObj.LuanaSMS__Student_Program__c == caseObj.Student_Program__c){
                    caseNumber = caseNumber +', '+'<a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+caseObj.Id+'" target="_blank">'+caseObj.CaseNumber+'</a>';
                    wSps.message = unresolveSpecialConsiderationCaseCount + ' Unresolved Special Consideration Case: (' + caseNumber.subString(2, caseNumber.length()) + ')';
                    unresolveSpecialConsiderationCaseCount++;
                }
            }
        }
        return wStuProgramSubjs;
    }


    // indicates whether there are more records after the current page set.
    public Boolean hasNext {
        get {
            return con.getHasNext();
        }
        set;
    }

    // indicates whether there are more records before the current page set.
    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }

    // returns the page number of the current page set
    public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set;
    }

    // returns the first page of records
     public void first() {
         con.first();
     }

     // returns the last page of records
     public void last() {
         con.last();
     }

     // returns the previous page of records
     public void previous() {
         con.previous();
     }

     // returns the next page of records
     public void next() {
         con.next();
     }

    //Back to course page
    public PageReference redirectBack() {
        PageReference backRef = new PageReference('/' + course.Id);
        backRef.setRedirect(true);
        return backRef;
    }
    
    //Create a wrapper class to support select all function
    public class WrapperStudentProgramSubj {
        public LuanaSMS__Student_Program_Subject__c spsObj {get; set;}
        public Boolean selected {get; set;}
        public String message {get; set;}
 
        public WrapperStudentProgramSubj(LuanaSMS__Student_Program_Subject__c sps){
            spsObj = sps;
            spsObj.LuanaSMS__Student_Program__r.Supp_Exam_Charge__c = 'Payable';
            selected = false;
            message ='';
        }
    }
    
    //Do get report
    
    //Do update Student Program 
    public PageReference doUpdate(){
        
        List<LuanaSMS__Student_Program__c> spList = new List<LuanaSMS__Student_Program__c>();
        for(WrapperStudentProgramSubj spsWrapper: wStuProgramSubjs){
            if(spsWrapper.selected){
                LuanaSMS__Student_Program__c tempSP = new LuanaSMS__Student_Program__c();
                tempSP.Id = spsWrapper.spsObj.LuanaSMS__Student_Program__c;
                tempSP.Supp_Exam_Charge__c = spsWrapper.spsObj.LuanaSMS__Student_Program__r.Supp_Exam_Charge__c;
                tempSP.Supp_Exam_Special_Consideration_Approved__c = 'Approved';
                
                spList.add(tempSP);
            }
        }
        update spList;
        
        return redirectBack();
    }
}