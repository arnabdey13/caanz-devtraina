/**
* @author Jannis Bott
* @date 14/12/2016
*
* @group QAS
* @description  serves to the QAS lightning component the corresponding load and address search methods
* @change log   refactored this code from the original code written by DataColada
*/
public class EDQService {

    /**
    * @description  should be called to search an address
    * @param        String searchTerm - the street to search for
    * @param        String country - the country the address belongs to
    * @param        Integer maxResults - the number of results that should be returned
    *
    * @return       String - the response body of the callout
    */
    @AuraEnabled
    public static Map<String, List<Map<String, Object>>> searchAddress(String searchTerm, String countryCode, Integer maxResults) {
        return EDQAddressService.searchAddress(searchTerm, countryCode, maxResults);
    }

    /**
    * @description  should be called once a suggested address has been selected on client
    * @param        String formatUrl - the url that should be called to get the full QAS address
    * @return       String - the response body of the callout
    */
    @AuraEnabled
    public static Map<String, Object> formatAddress(String id, String countryCode) {
        return EDQAddressService.formatAddress(id, countryCode);
    }

    /**
    * @description  loads all countries and states
    *
    * @return       List<CountryPicklistSetting>
    */
    @AuraEnabled
    public static String loadCountryAndStates() {
        return (String)PlatformCacheUtil.readOrgCache('PICKLISTS', 'CountriesAndStatesJSON', new CountryObjectFactory());
    }

    public class CountryObjectFactory implements PlatformCacheUtil.ObjectFactory {

        public Object get(){
            Map<String, ff_WrapperSource> sources = new Map<String, ff_WrapperSource>();
            Map<String, ff_WrapperSource> countryAndStates = new ff_WrappedCountryStateSources().getSources();
            sources.put('User.Countries', countryAndStates.get('countries'));
            sources.put('User.States', countryAndStates.get('states'));
            Map<String, List<ff_Service.CustomWrapper>> wrappers = ff_ServiceHandler.loadWrappers(sources);

            return JSON.serialize(wrappers);
        }
    }
}