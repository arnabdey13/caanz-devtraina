/* 
    Developer: WDCi (KH)
    Development Date: 28/06/2016
    Task #: Contributor session Search wizard JIRA issue LCA-700
    
    Change Histroy:
    LCA-487 07/07/2016 - WDCi Lean: update authentication redirection to custom login page
*/
public without sharing class luana_ContributorResBookingSearchCtl {

    public Id custCommConId {set; get;}
    public Id custCommAccId {set; get;}
    public boolean validForSearch {public get; private set;}
    boolean resetPage = true;
    private integer queryLimit;
    
    public String assignmentType {set; get;}
    
    public luana_ContributorResBookingSearchCtl(){
        
        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId()); 

        custCommConId = commUserUtil.custCommConId;
        custCommAccId = commUserUtil.custCommAccId;
        
        validForSearch = false;
                
        resetPage = true;
        queryLimit = 10000;
        
        assignmentType = '';
        
    }
    
    public PageReference forwardToAuthPage() {
        if(UserInfo.getUserType() == 'Guest'){
            return new PageReference(URL.getSalesforceBaseUrl().toExternalForm()+'/contributor/luana_Login?startURL=/contributor/luana_ContributorResBookingSearch'); //LCA-487
        }
        else{
            assignmentType = ''; 
            doSearch();
            return null;
        }
    }
    
    public PageReference doSearch(){
        
        setCon = null;
        
        if(resetPage){
            currentPage = 1;
        }
        return null;
    }
    
    public Integer noOfRecords{get; set;}
    public Integer size{get;set;}
    public Integer totalNumOfPage {get; set;}
    Integer currentPage;
    public ApexPages.StandardSetController setCon {

        get{    
            
            if(assignmentType != null){
                if(setCon == null){
                  
                    Luana_Extension_Settings__c noPerPage = Luana_Extension_Settings__c.getValues('Default_Pagination_No_Per_Page');
                    Integer numberPerPageSize = Integer.valueOf(noPerPage.value__c);
                    size = numberPerPageSize;
                    queryLimit = 10000;
                    
                    Set<id> contactIds = new Set<Id>();
                    Set<id> paymentTokenRelatedStudProgIds = new Set<Id>();

                    string queryString = getResourceBookingFilterQuery();
                    
                    setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
                    System.debug('**Database.getQueryLocator(queryString): ' + Database.getQueryLocator(queryString) + '  -  '+ setCon);
                    setCon.setPageSize(size);
                    noOfRecords = setCon.getResultSize();
                    
                    setCon.setPageNumber(currentPage);
                    
                    Decimal block = decimal.valueOf(noOfRecords).divide(integer.valueOf(size), 2, System.RoundingMode.UP); 
       
                    totalNumOfPage = integer.valueOf(block.round(System.RoundingMode.CEILING));
                    
                }
            }
            return setCon;
            
        }
        set;
    }

    public List<LuanaSMS__Resource_Booking__c> getResourceBookings(){
        try{
            List<LuanaSMS__Resource_Booking__c> resourceBookings = new List<LuanaSMS__Resource_Booking__c>();
            if(setCon != null){
                for(LuanaSMS__Resource_Booking__c rb: (List<LuanaSMS__Resource_Booking__c>)setCon.getRecords()){
                    resourceBookings.add(rb);
                }
            }
            return resourceBookings;
        }catch(Exception exp){
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Unexpected error found please contact support. ' + exp.getMessage() + ' - ' + exp.getStackTraceString()));
        }
        return null;
    }
    
    public List<SelectOption> getResourceOptions(){
        
        List<LuanaSMS__Resource_Booking__c> rbs = [select Id, Name, LuanaSMS__Resource__c, LuanaSMS__Session__c, 
                                                    LuanaSMS__Session__r.Name, LuanaSMS__Session__r.RecordType.Name 
                                                    from LuanaSMS__Resource_Booking__c 
                                                    Where LuanaSMS__Contact_Trainer_Assessor__c =: custCommConId and LuanaSMS__Session__c != null and LuanaSMS__Start_Time__c >= TODAY];

        Set<String> sessRecTypeName = new Set<String>();
        for(LuanaSMS__Resource_Booking__c rb: rbs){
            sessRecTypeName.add(rb.LuanaSMS__Session__r.RecordType.Name);
        }
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('--None--','--None--'));
        
        for(String sessionName: sessRecTypeName){
            options.add(new SelectOption(sessionName, sessionName));
        }
        return options;
    }
    
    public String getResourceBookingFilterQuery(){
        
        String filterStr = '';
        if(assignmentType == '--None--' || assignmentType == ''){
            filterStr = 'Where LuanaSMS__Contact_Trainer_Assessor__c = \''+custCommConId+'\' and LuanaSMS__Session__c != null';
            
        }else{
            filterStr = 'Where LuanaSMS__Contact_Trainer_Assessor__c = \''+custCommConId+'\' and LuanaSMS__Session__r.RecordType.Name = \''+ assignmentType +'\' and LuanaSMS__Session__c != null';
        }
        
        filterStr += ' and (LuanaSMS__Start_Time__c >= TODAY)'; 
        
        string queryStr = 'select Id, Name, LuanaSMS__Resource__c, LuanaSMS__Resource__r.Name, LuanaSMS__Contact_Trainer_Assessor__c, '+
                            'LuanaSMS__Contact_Trainer_Assessor__r.Name, LuanaSMS__Course__c, LuanaSMS__Course__r.Name, LuanaSMS__Session__c, '+
                            'LuanaSMS__Session__r.Name, LuanaSMS__Session__r.RecordType.DeveloperName, LuanaSMS__Session__r.RecordType.Name, LuanaSMS__Start_Time__c, LuanaSMS__End_Time__c '+
                            'from LuanaSMS__Resource_Booking__c '+ filterStr + ' Order by LuanaSMS__Start_Time__c ASC NULLS LAST';
        
        System.debug('**queryStr: ' + queryStr);
        return queryStr;
    }
    
    public String selectedResourceBooking {get; set;}
    public pageReference viewDetail(){
        
        String [] rb = selectedResourceBooking.split(':');
        String selectedResourceBookingId = rb[0];
        String selectedResourceBookingType = rb[1];
        
        PageReference pageRef;
        pageRef = Page.luana_ContributorResBookingDetail;
        pageRef.getParameters().put('id', selectedResourceBookingId);
        pageRef.getParameters().put('type', selectedResourceBookingType);
        return pageRef; 
    }
}