/*
 * @author          WDCi (LKoh)
 * @date            24-June-2019
 * @description     Test class for the wdci_ProvisionalAppController class
 * @changehistory   PAN:6539 - Edy: Changes for Contact lookup > Master Detail in Education History  
 * @changehistory   #UAT001 - WDCi-LKoh - 09/09/2019 - updated test for the new top and bottom description
 */
@isTest
public class wdci_ProvisionalAppTest {

    @TestSetup
    static void generateAssets() {

        wdci_TestUtil.generateAsset();
    }

    @isTest static void testProvisionalAppController() {

        String currentYear = String.valueOf(system.today().year());
        String educationHistoryCommencedYear = String.valueOf(system.today().addYears(-3).year());
        String educationHistoryEndYear = String.valueOf(system.today().addYears(-1).year());

        List<Account> studentAccount = [SELECT Id, personcontactid FROM Account WHERE Member_ID__c = '3053164']; //PAN:6539 - Edy
        List<edu_University_Degree_Join__c> uniDegreeJoin = [SELECT Id FROM edu_University_Degree_Join__c];
        List<FT_University_Subject__c> uniSubject = [SELECT Id FROM FT_University_Subject__c];

        // Application
        Application__c testApplication = wdci_TestUtil.generateApplication(studentAccount[0].Id, 'Chartered Accountants', 'Draft', 'Standard');
        insert testApplication;

        // Education History
        edu_Education_History__c testEducationHistory = wdci_TestUtil.generateEducationHistory(testApplication.Id, studentAccount[0].personcontactid, uniDegreeJoin[0].Id, educationHistoryCommencedYear, educationHistoryEndYear); //PAN:6539 - Edy
        insert testEducationHistory;

        // Student Paper
        FT_Student_Paper__c testStudentPaper = wdci_TestUtil.generateStudentPaper(uniSubject[0].Id, testEducationHistory.Id, true);
        insert testStudentPaper;

        Test.startTest();

        // Testing the sub components for eQualification
        wdci_ProvisionalAppController.saveEQual(testEducationHistory.Id, 'Test eQual');
        wdci_ProvisionalAppController.grabEQual(testEducationHistory.Id);
        List<edu_Education_History__c> checkEducationHistory = [SELECT Id, FT_My_eQuals__c FROM edu_Education_History__c WHERE Id = :testEducationHistory.Id];
        system.debug('checkEducationHistory: ' +checkEducationHistory);        
        
        // Testing the sub components for retrieving Uni Subject
        List<wdci_WrapperClass> subjectListWrapper = wdci_ProvisionalAppController.grabUniSubjectListMk3(testEducationHistory.Id);
        system.debug('subjectListWrapper: ' +subjectListWrapper);

        // Testing the sub components for saving Uni Subject
        for (wdci_WrapperClass wrapper : subjectListWrapper) {
            if (wrapper.uniSubjectCode == 'BUSS1030') 
                wrapper.selected = false;
            else if (wrapper.uniSubjectCode == 'ACCT1006')
                wrapper.selected = true;
        }        

        wdci_ProvisionalAppController.saveUniSubjectList(testEducationHistory.Id, subjectListWrapper, 'Test Additional Remark');

        // The student paper for BUSS1030 should be deleted and a new student paper for ACCT1006 should be in place
        List<FT_Student_Paper__c> checkStudentPaper = [SELECT Id, FT_Uni_Subject_Code__c FROM FT_Student_Paper__c];
        system.debug('checkStudentPaper: ' +checkStudentPaper);

        // Testing the sub components for remark
        String additionalRemarkValue = wdci_ProvisionalAppController.grabAdditionalRemark(testEducationHistory.Id);
        system.debug('additionalRemarkValue: ' +additionalRemarkValue);

        // String remarkLabel = wdci_ProvisionalAppController.grabRemarkLabel(); OBSOLETE
        // system.debug('remarkLabel: ' +remarkLabel);

        String topDescription = wdci_ProvisionalAppController.grabTopDescription();
        system.debug('topDescription: ' +topDescription);

        String bottomDescription = wdci_ProvisionalAppController.grabBottomDescription();
        system.debug('bottomDescription: ' +bottomDescription);

        Test.stopTest();
    }
}