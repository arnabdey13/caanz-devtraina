/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private with sharing class RecordTypeCache_test {
	private static RecordType TestRecordTypeObjectInDB;
	static{
		getTestRecordTypeObject();
	}
	private static void getTestRecordTypeObject(){
		List<RecordType> RecordType_List = [Select r.SobjectType, r.Name, r.DeveloperName From RecordType r 
			where r.IsActive=true limit 1];
		if(RecordType_List.size()==1){
			TestRecordTypeObjectInDB = RecordType_List[0];
		}
		else{
			throw new newException('There are no RecordTypes set up in your org!');
		}
	}
	
	//******************************************************************************************
	//                             TestMethods
	//******************************************************************************************
	static testMethod void RecordTypeCache_testGetRecordTypeBySobjectTypeAndDeveloperName() {
		RecordType TestRecordTypeObject = RecordTypeCache.getRecordType(
			TestRecordTypeObjectInDB.SobjectType, TestRecordTypeObjectInDB.DeveloperName);
		System.assertNotEquals( null, TestRecordTypeObject );
		System.assertEquals( TestRecordTypeObjectInDB.Id, TestRecordTypeObject.Id, 'TestRecordTypeObject.Id' );
	}
	static testMethod void RecordTypeCache_testGetRecordTypeBySobjectAndDeveloperName() {
		sObject sObj = Schema.getGlobalDescribe().get(TestRecordTypeObjectInDB.SobjectType).newSObject();
		
		RecordType TestRecordTypeObject = RecordTypeCache.getRecordType(
			sObj, TestRecordTypeObjectInDB.DeveloperName);
		System.assertNotEquals( null, TestRecordTypeObject );
		System.assertEquals( TestRecordTypeObjectInDB.Id, TestRecordTypeObject.Id, 'TestRecordTypeObject.Id' );
	}
	
	static testMethod void RecordTypeCache_testGetRecordTypeById() {
		RecordType TestRecordTypeObject = RecordTypeCache.getRecordType( TestRecordTypeObjectInDB.Id );
		System.assertNotEquals( null, TestRecordTypeObject );
		System.assertEquals( TestRecordTypeObjectInDB.Id, TestRecordTypeObject.Id, 'TestRecordTypeObject.Id' );
	}
	
	static testMethod void RecordTypeCache_testGetRecordTypeIdBySobjectTypeAndDeveloperName() {
		Id TestRecordTypeId = RecordTypeCache.getId(
			TestRecordTypeObjectInDB.SobjectType, TestRecordTypeObjectInDB.DeveloperName);
		System.assertNotEquals( null, TestRecordTypeId );
		System.assertEquals( TestRecordTypeObjectInDB.Id, TestRecordTypeId, 'TestRecordTypeId' );
	}
	static testMethod void RecordTypeCache_testGetRecordTypeIdBySobjectAndDeveloperName() {
		sObject sObj = Schema.getGlobalDescribe().get(TestRecordTypeObjectInDB.SobjectType).newSObject();
		
		Id TestRecordTypeId = RecordTypeCache.getId(
			sObj, TestRecordTypeObjectInDB.DeveloperName);
		System.assertNotEquals( null, TestRecordTypeId );
		System.assertEquals( TestRecordTypeObjectInDB.Id, TestRecordTypeId, 'TestRecordTypeId' );
	}
	
	static testMethod void RecordTypeCache_testUnablToFindRecordTypeBySobjectTypeAndDeveloperName() {
		String FakeSobjectType = 'FakeSobjectType';
		String FakeDeveloperName = 'FakeDeveloperName';
		try{
			RecordTypeCache.getRecordType( FakeSobjectType, FakeDeveloperName );
		}
		catch(Exception ex){
			System.assertEquals(
				'Unable to find RecordType for ' + FakeSobjectType + '.' + FakeDeveloperName,
				ex.getMessage()
			);
			return;
		}
		System.assert(false, 'An error should have been throw!');
	}
	
	static testMethod void RecordTypeCache_testUnablToFindRecordTypeById() {
		Id FakeRecordTypeId = '01290099900Ocv0AAC';
		try{
			RecordTypeCache.getRecordType( FakeRecordTypeId );
		}
		catch(Exception ex){
			System.assertEquals(
				'Unable to find RecordType for ' + FakeRecordTypeId,
				ex.getMessage()
			);
			return;
		}
		System.assert(false, 'An error should have been throw!');
	}
}