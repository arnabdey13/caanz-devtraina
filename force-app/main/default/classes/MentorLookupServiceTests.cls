@isTest(seeAllData=false)
public with sharing class MentorLookupServiceTests {

    private static final String MENTOR_MEMBER_ID = '12345';
    private static final String OTHER_MEMBER_ID = '1234';

    @IsTest
    static void testCADesignation() {

        List<Account> mentor = [SELECT Id FROM Account WHERE Member_ID__c = :MENTOR_MEMBER_ID];
        Map<String, Map<String, String>> response = MentorLookupService.findMentorImpl(MENTOR_MEMBER_ID, 't@t.com', 'Chartered Accountants');
        System.assertEquals('Mentor CA', response.get('SUCCESS').get('name'),
                'Mentor search returned result');

        Map<String, Map<String, String>> response2 = MentorLookupService.findMentorImpl(OTHER_MEMBER_ID, 't@t.com', 'Chartered Accountants');
        System.assertEquals('No matching Mentor was found.', response2.get('ERROR').get('response'),
                'Mentor search did not return results');

        mentor[0].Designation__c = 'FACA';
        update mentor;

        Map<String, Map<String, String>> response3 = MentorLookupService.findMentorImpl(MENTOR_MEMBER_ID, 't@t.com', 'Chartered Accountants');
        System.assertNotEquals(NULL, response3.get('ERROR'), 'No matches found when designations do not match');
//        System.assertEquals('No matching Mentor was found.', response3.get('ERROR').get('response'),
//                'Mentor search did not return results');
    }

    @IsTest
    static void testACADesignation() {

        List<Account> mentor = [SELECT Id FROM Account WHERE Member_ID__c = :MENTOR_MEMBER_ID];
        Map<String, Map<String, String>> response = MentorLookupService.findMentorImpl(MENTOR_MEMBER_ID, 't@t.com', 'Associate Chartered Accountants');
        System.assertEquals('Mentor CA', response.get('SUCCESS').get('name'),
                'Mentor search returned result');

        Map<String, Map<String, String>> response2 = MentorLookupService.findMentorImpl(OTHER_MEMBER_ID, 't@t.com', 'Associate Chartered Accountants');
        System.assertEquals('No matching Mentor was found.', response2.get('ERROR').get('response'),
                'Mentor search did not return results');

        mentor[0].Designation__c = 'CA';
        update mentor;

        Map<String, Map<String, String>> response3 = MentorLookupService.findMentorImpl(MENTOR_MEMBER_ID, 't@t.com', 'Associate Chartered Accountants');
        System.assertEquals('Mentor CA', response3.get('SUCCESS').get('name'),
                'Mentor search returned result');
    }

    @IsTest
    static void testEnsureRelationship() {

        Map<String, Map<String, String>> response = MentorLookupService.findMentorImpl(MENTOR_MEMBER_ID, 't@t.com', 'Associate Chartered Accountants');
        System.assertEquals('Mentor CA', response.get('SUCCESS').get('name'), 'Mentor search returned result');

        Relationship__c relationship = MentorLookupService.ensureRelationship(TestSubscriptionUtils.getTestAccountId(), response.get('SUCCESS').get('id'));
        System.assertNotEquals(NULL, relationship, 'Relationship will be added for first match');
        insert relationship;

        response = MentorLookupService.findMentorImpl(MENTOR_MEMBER_ID, 't@t.com', 'Associate Chartered Accountants');
        System.assertEquals('Mentor CA', response.get('SUCCESS').get('name'), 'Mentor search returned result');

        relationship = MentorLookupService.ensureRelationship(TestSubscriptionUtils.getTestAccountId(), response.get('SUCCESS').get('id'));
        System.assertEquals(NULL, relationship, 'Relationship will be not be added for searches when relationship already exists');

    }

    @testSetup static void createTestData() {

        TestSubscriptionUtils.createTestData();

        Account mentor = new TestDataAccountBuilder('Member')
                .salutation('Sir')
                .firstName('Mentor')
                .lastName('CA')
                .email('t@t.com')
                .type('Member')
                .status('Active')
                .memberOf('NZICA')
                .billingAddress('Unit 1, 50 Customhouse Quay', 'Wellington', null, '6011', 'NZ')
                .gaa(false)
                .membershipClass('Full')
                .designation('CA')
                .memberId(MENTOR_MEMBER_ID)
                .create(1, true).get(0);
    }

}