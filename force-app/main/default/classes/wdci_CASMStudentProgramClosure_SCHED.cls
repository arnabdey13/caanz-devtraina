/**
* @author WDCi-Lean
* @date 18/06/2019
* @group CASM
* @description Scheduler to close casm student program
* @change-history
*/

global class wdci_CASMStudentProgramClosure_SCHED implements Schedulable {
    
    global void execute(SchedulableContext sc) {
        
        try{
            //default batch size
            Integer batchSize = 20;
            
            //get batch size from custom setting
            Luana_Extension_Settings__c batchSizeSetting = Luana_Extension_Settings__c.getValues(wdci_CASMStudentProgramClosure_BATCH.CASM_JOB_BATCHSIZE_KEY);
            if(batchSizeSetting != null && String.isNotBlank(batchSizeSetting.Value__c)){
                batchSize = Integer.valueOf(batchSizeSetting.Value__c);
            }

            wdci_CASMStudentProgramClosure_BATCH batchJob = new wdci_CASMStudentProgramClosure_BATCH();

            //init the batch job
            Database.executeBatch(batchJob, batchSize);

        } catch(Exception exp){
            
            insert wdci_CASMStudentProgramClosure_BATCH.initErrorLog(
                null, 
                'Problem executing wdci_CASMStudentProgramClosure_BATCH. Error: ' + exp.getLineNumber() + ' - ' + exp.getMessage(),
                System.now(), 
                sc.getTriggerId()
            );

        } finally {
            //do nothing
        }
    }
}