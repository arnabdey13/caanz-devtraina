/**
    Developer: WDCi (CM)
    Development Date: 26/08/2016
    Task: Luana Test class for Program Enrolment Wizard
          - luana_ProgramEnrolmentWizardController
          - luana_ProgramEnrolmentWizardCOController
          - luana_ProgramEnrolmentWizardPOController
          - luana_ProgramEnrolmentWizardSUController
          - luana_ProgramEnrolmentWizardSMController
          - luana_ProgramOfferingSubjectWrapper
**/
@isTest(seeAllData=false)
private class luana_AT_ProgramEnrolmentWizard_Test {

    private static String classNamePrefixLong = 'luana_AT_ProgramEnrolmentWizard_Test';
    private static String classNamePrefixShort = 'lapewt';
    
    private static Luana_DataPrep_Test dataPrep;
    private static List<LuanaSMS__Program_Offering__c> poList;
    private static LuanaSMS__Course__c course;
    private static List<LuanaSMS__Program_Offering_Subject__c> psList;
    private static Account memberAccount;
    

    private static void setup(){
        System.debug('##luana_AT_ProgramEnrolmentWizard_Test - Prepare Data##');
        dataPrep = new Luana_DataPrep_Test();
        
        insert dataPrep.prepLuanaExtensionSettingCustomSettings();
        insert dataPrep.createLuanaConfigurationCustomSetting();
        
        //Create traning org
        LuanaSMS__Training_Organisation__c trainingOrg = dataPrep.createNewTraningOrg(classNamePrefixLong, classNamePrefixLong, classNamePrefixShort, 'St 123', 'Wollonggong', '2244');
        insert trainingOrg;
        
        //Create program
        LuanaSMS__Program__c progrm = dataPrep.createNewProgram(classNamePrefixShort, classNamePrefixLong, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        insert progrm;
        
        //Create Product
        List<Product2> prodList = new List<Product2>();
        prodList.add(dataPrep.createNewProduct('FIN_AU_' + classNamePrefixLong, 'AU0001'));
        prodList.add(dataPrep.createNewProduct('FIN_NZ_' + classNamePrefixLong, 'NZ0001'));
        prodList.add(dataPrep.createNewProduct('FIN_INT_' + classNamePrefixLong, 'INT0001'));
        insert prodList;
        
        //Create Program Offerings
        poList = new List<LuanaSMS__Program_Offering__c>();
        for (integer i = 0; i < 2; i++){
            LuanaSMS__Program_Offering__c poAM = dataPrep.createNewProgOffering('PO_AM_' + i + classNamePrefixLong, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), progrm.Id, trainingOrg.Id, prodList[0].Id,prodList[1].Id, prodList[2].Id, 1, 1);
            poAM.LuanaSMS__Allow_Online_Enrolment__c = true;
            poAM.Product_Type__c ='CA Program';
            poList.add(poAM);
        }
        insert poList;
        
        //Create course
        course = dataPrep.createNewCourse('Graduate Diploma of Chartered Accounting', poList[0].Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        course.LuanaSMS__Allow_Online_Enrolment__c = true;
        insert course;
        
        //Create student
        memberAccount = dataPrep.generateNewApplicantAcc('Joe_' + classNamePrefixShort, classNamePrefixLong, 'Full_Member');
        memberAccount.PersonEmail=  classNamePrefixLong + '@example.com';
        memberAccount.Member_Id__c = '12345';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.Assessible_for_CA_Program__c = true;
        insert memberAccount;
        
        //Create Product Permissions
        List<Product_Permission__c> pList = new List<Product_Permission__c>();
        for (integer i = 0; i < 2; i++){
            Product_Permission__c productPermission = new Product_Permission__c();
            productPermission.Grant_Permission__c = 'Member Community';
            productPermission.Member_Criteria__c = 'Assessible for CA Program';
            productPermission.Program_Offering__c = poList[i].Id;
            pList.add(productPermission);
        }
        insert pList;
        
        //Create subjects
        List<LuanaSMS__Subject__c> subList = new List<LuanaSMS__Subject__c>();
        for (integer i = 0; i < 3; i++){
            LuanaSMS__Subject__c s = dataPrep.createNewSubject('S_' + i + classNamePrefixShort, 'S_' + i + classNamePrefixLong, 'S_' + i + classNamePrefixShort, 1, 'Module');
            if (i == 0){
                s.LuanaSMS__Essential__c = 'Core';
            } else {
                s.LuanaSMS__Essential__c = 'Elective';
            }
            subList.add(s);
        }
        insert subList;
        
        psList = new List<LuanaSMS__Program_Offering_Subject__c>();
        for (LuanaSMS__Subject__c s : subList){
            LuanaSMS__Program_Offering_Subject__c ps;
            ps = dataPrep.createNewProgOffSubject(poList[0].id, s.id, s.LuanaSMS__Essential__c);
            psList.add(ps);
        }
        insert psList;
        
        
    }

    private static testMethod void testProgramEnrolmentWizard() {
        
        //Using here to create user from account
        test.startTest();
        setup();
        
        //Create Terms and Conditions
        List<Terms_and_Conditions__c> tncs = dataPrep.createDefaultTNCs();
        insert tncs;
        
        List<Attachment> attchs = new List<Attachment>();
        for(Terms_and_Conditions__c tnc: tncs){
            attchs.add(dataPrep.addAttachmentToParent(tnc.Id, classNamePrefixLong + 'CAProgramEnrolment_T&C.pdf')); 
        }
        insert attchs;
        
        test.stopTest();
        
        User memberUser = [Select Id, Email, FirstName, LastName, UserName, Name from User Where Email =: classNamePrefixLong+'@example.com' limit 1];
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c);
        
        system.runAs(memberUser){
            insert psa;
        }
        
        Contact memberContact = [SELECT Id, AccountId FROM Contact WHERE AccountId = : memberAccount.Id];
            
        //Begin testing
        //Setup VF Page
        PageReference pageRef = Page.luana_ProgramEnrolmentWizardProgram;
        pageRef.getParameters().put('contactid', memberContact.id);
        pageRef.getParameters().put('producttype', 'CA Program');
        
        Test.setCurrentPage(pageRef);
        luana_ProgramEnrolmentWizardController lpewc = new luana_ProgramEnrolmentWizardController();
        lpewc.init();
        luana_ProgramEnrolmentWizardPOController poc = new luana_ProgramEnrolmentWizardPOController(lpewc);
        poc.init();
        luana_ProgramEnrolmentWizardCOController coc = new luana_ProgramEnrolmentWizardCOController(lpewc);
        coc.init();
        luana_ProgramEnrolmentWizardSUController suc = new luana_ProgramEnrolmentWizardSUController(lpewc);
        suc.init();
        luana_ProgramEnrolmentWizardSMController smc = new luana_ProgramEnrolmentWizardSMController(lpewc);
        smc.init();
        
        string templateString = lpewc.getTemplateName();
        List<SelectOption> programOfferingOptions = poc.getProgramOfferingOptions();
        system.assert(!programOfferingOptions.isEmpty(), 'Failed to find Program Offerings');
        
        PageReference nextPageRef;
        nextPageRef = poc.programNext();
        system.assert(nextPageRef == null, 'No Program Offering was selected, but went to the next page.');
        
        //User selects Program Offering
        lpewc.selectedProgramOfferingId = poList[0].Id;
        nextPageRef = poc.programNext();
        system.assert(nextPageRef != null, 'Selected Program Offering but failed to move to next page.');
        Test.setCurrentPage(nextPageRef);
        
        nextPageRef = null;
        nextPageRef = coc.courseNext();
        system.assert(nextPageRef == null, 'No Course was selected, but went to the next page.');
        
        //User selects Course
        lpewc.selectedCourseId = course.Id;
        nextPageRef = coc.courseNext();
        system.assert(nextPageRef != null, 'Selected Course but failed to move to next page.');
        Test.setCurrentPage(nextPageRef);
        
        nextPageRef = null;
        List<luana_ProgramOfferingSubjectWrapper> subjectList = suc.getSubjectOptions();
        for (luana_ProgramOfferingSubjectWrapper subject : subjectList){
            subject.isSelected = false;
        }        
    /*    nextPageRef = suc.subjectNext();
        system.assert(nextPageRef == null, 'No Subjects were selected, but went to the next page.');
        
        for (luana_ProgramOfferingSubjectWrapper subject : subjectList){
            subject.isSelected = true;
        }        
        nextPageRef = suc.subjectNext();
        system.assert(nextPageRef == null, 'Too many Subjects were selected, but went to the next page.');
    */    
        //Will enrol in core, cancel one elective and enrol in the other
        Boolean cancelled = false;
        for (luana_ProgramOfferingSubjectWrapper subject : subjectList){
            if (!subject.isCore){
                if (!cancelled){
                    subject.isSelected = false;
                    subject.sps.LuanaSMS__Outcome_National__c = 'Recognition of prior learning granted';
                    subject.sps.Module_Exemption_Without_Case__c = true;
                    cancelled = true;
                } else {
                    subject.isSelected = true;
                    subject.sps.LuanaSMS__Outcome_National__c = 'Did not start';
                }
            } else {
                subject.isSelected = true;
                subject.sps.LuanaSMS__Outcome_National__c = 'Did not start';
            }
        }

        nextPageRef = suc.subjectNext();
        system.assert(nextPageRef != null, 'Problem enrolling in subjects.');
        Test.setCurrentPage(nextPageRef);
        //This page doesn't actually do anything. Just calling these functions for completion's sake.
        nextPageRef = smc.summaryNext();
        nextPageRef = smc.summaryBack();
        
        //Assert correct enrolments were made
        Boolean error = false;
        LuanaSMS__Student_Program__c spCheck;
        try{
            spCheck = [SELECT Id, LuanaSMS__Contact_Student__c, LuanaSMS__Course__c FROM LuanaSMS__Student_Program__c
                                                    WHERE LuanaSMS__Contact_Student__c = : memberContact.Id AND LuanaSMS__Course__c = : course.Id];
        } catch (exception e){
            error = true;
        } 
        System.assert(error == false, 'Could not find new Student Program');
        
        List<LuanaSMS__Student_Program_Subject__c> spsCheckList = [SELECT Id, LuanaSMS__Contact_Student__c, LuanaSMS__Student_Program__c FROM LuanaSMS__Student_Program_Subject__c
                                                             WHERE LuanaSMS__Contact_Student__c = : memberContact.Id AND LuanaSMS__Student_Program__c = : spCheck.Id];
        //1 Elective was marked to be exempted.
        system.assertEquals(subjectList.size() - 1, spsCheckList.size(), 'Incorrect number of Student Program Subjects created.');
        
    }
    
    private static testmethod void testProgramEnrolmentWizardFail(){
        //Using here to create user from account
        test.startTest();
        setup();
        //No terms and conditions. This will cause failure at the end.        
        test.stopTest();
        
        User memberUser = [Select Id, Email, FirstName, LastName, UserName, Name from User Where Email =: classNamePrefixLong+'@example.com' limit 1];
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c);
        
        system.runAs(memberUser){
            insert psa;
        }
        
        Contact memberContact = [SELECT Id, AccountId FROM Contact WHERE AccountId = : memberAccount.Id];
        
        
        //Begin testing
        //Setup VF Page
        PageReference pageRef = Page.luana_ProgramEnrolmentWizardProgram;
        pageRef.getParameters().put('contactid', memberContact.id);
        pageRef.getParameters().put('producttype', 'CA Program');
        
        Test.setCurrentPage(pageRef);
        luana_ProgramEnrolmentWizardController lpewc = new luana_ProgramEnrolmentWizardController();
        lpewc.init();
        luana_ProgramEnrolmentWizardPOController poc = new luana_ProgramEnrolmentWizardPOController(lpewc);
        poc.init();
        luana_ProgramEnrolmentWizardCOController coc = new luana_ProgramEnrolmentWizardCOController(lpewc);
        coc.init();
        luana_ProgramEnrolmentWizardSUController suc = new luana_ProgramEnrolmentWizardSUController(lpewc);
        suc.init();
        luana_ProgramEnrolmentWizardSMController smc = new luana_ProgramEnrolmentWizardSMController(lpewc);
        smc.init();
        
        string templateString = lpewc.getTemplateName();
        List<SelectOption> programOfferingOptions = poc.getProgramOfferingOptions();
        system.assert(!programOfferingOptions.isEmpty(), 'Failed to find Program Offerings');
        
        PageReference nextPageRef;
        nextPageRef = poc.programNext();
        system.assert(nextPageRef == null, 'No Program Offering was selected, but went to the next page.');
        
        //User selects Program Offering
        lpewc.selectedProgramOfferingId = poList[0].Id;
        nextPageRef = poc.programNext();
        system.assert(nextPageRef != null, 'Selected Program Offering but failed to move to next page.');
        Test.setCurrentPage(nextPageRef);
        
        nextPageRef = null;
        nextPageRef = coc.courseNext();
        system.assert(nextPageRef == null, 'No Course was selected, but went to the next page.');
        
        //User selects Course
        lpewc.selectedCourseId = course.Id;
        nextPageRef = coc.courseNext();
        system.assert(nextPageRef != null, 'Selected Course but failed to move to next page.');
        Test.setCurrentPage(nextPageRef);
        
        nextPageRef = null;
        List<luana_ProgramOfferingSubjectWrapper> subjectList = suc.getSubjectOptions();
        //Will enrol in core, cancel one elective and enrol in the other
        Boolean cancelled = false;
        for (luana_ProgramOfferingSubjectWrapper subject : subjectList){
            if (!subject.isCore){
                if (!cancelled){
                    subject.isSelected = false;
                    subject.sps.LuanaSMS__Outcome_National__c = 'Recognition of prior learning granted';
                    subject.sps.Module_Exemption_Without_Case__c = true;
                    cancelled = true;
                } else {
                    subject.isSelected = true;
                    subject.sps.LuanaSMS__Outcome_National__c = 'Did not start';
                }
            } else {
                subject.isSelected = true;
                subject.sps.LuanaSMS__Outcome_National__c = 'Did not start';
            }
        }     

        nextPageRef = suc.subjectNext();
        system.assert(nextPageRef == null, 'Enrolment should have failed without terms and conditions.');
        
        //Testing back buttons
        Test.setCurrentPage(suc.subjectBack());
        Test.setCurrentPage(coc.courseBack());
        //Test.setCurrentPage(poc.programBack());
        nextPageRef = poc.programBack(); //Currently returns null
        
    }
    
}