/**
*   @author	Jannis Bott
*   @date	28/11/2016
*   @group	Register and Login
*   @description Sends an email to new community members. It distinguishes between member and non-member users
*   and sends different email template based on custom setting.
 */
public without sharing class RegisterOrLoginEmailProcess {
    @InvocableMethod(label='Send Welcome Email' description='Sends the correct welcome email to members depending on their membership type.')
    public static void sendWelcomeEmail(List<Id> ids) {
        List<Account> personAccounts = [SELECT Id, PersonContactId, PersonEmail, RecordType.Name, Registration_Temporary_Key__c FROM Account WHERE Id IN :ids];

        Id nonMemberTemplateId, memberTemplateId;
        if (!Test.isRunningTest()) {
            nonMemberTemplateId = Environment_Ids__c.getInstance('Non_Member_Welcome_Template_Id').Id__c;
            memberTemplateId = Environment_Ids__c.getInstance('Member_Welcome_Template_Id').Id__c;
        } else {
            Id anyTemplateId = [SELECT Id FROM EmailTemplate LIMIT 1].Id;
            nonMemberTemplateId = anyTemplateId;
            memberTemplateId = anyTemplateId;
        }

        Messaging.SingleEmailMessage statusEmail = null;
        Environment_Ids__c statusEmailRecipientId = Environment_Ids__c.getInstance('Non_Member_Welcome_Status_User_Id');
        if (statusEmailRecipientId != null) {
            statusEmail = new Messaging.SingleEmailMessage();
            String emailAddress = [SELECT Email FROM User WHERE Id = :statusEmailRecipientId.Id__c].Email;
            statusEmail.setToAddresses(new List<String>{
                    emailAddress
            });
        }

        try {

            if (!personAccounts.isEmpty()) {
                List<Messaging.SingleEmailMessage> allMessages = new List<Messaging.SingleEmailMessage>();

                for (Account a : personAccounts) {
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

                    // handling two types as a workaround to a production difference when formassembly creates members
                    Set<String> memberRecordTypes = new Set<String>{
                            'Applicant', 'Member'
                    };

                    if (memberTemplateId != null && memberRecordTypes.contains(a.RecordType.Name)) {
                        mail.setTemplateID(memberTemplateId);
                    } else if (nonMemberTemplateId != null && a.RecordType.Name.equals('Non Member')) {
                        mail.setTemplateID(nonMemberTemplateId);
                    } else continue;

                    mail.setSaveAsActivity(true);
                    mail.setTargetObjectId(a.PersonContactId);
                    mail.setToAddresses(new List<String>{
                            a.PersonEmail
                    });

                    Environment_Ids__c emailSenderId = Environment_Ids__c.getInstance('Member_Welcome_Sender_Id');
                    if (emailSenderId == null) {
                        // using these two methods cannot be used together with setOrgWideEmailAddressId
                        mail.setReplyTo('service@charteredaccountantsanz.com');
                        mail.setSenderDisplayName('Customer Service Centre');
                    } else {
                        mail.setOrgWideEmailAddressId(emailSenderId.Id__c);
                    }

                    allMessages.add(mail);
                }
                //just send the email if the test is not running and email messages exist
                if (allMessages.size() > 0) {
                    List<Messaging.SendEmailResult> results = Messaging.sendEmail(allMessages, false);
                    String sendErrors = '';
                    for (Messaging.SendEmailResult result : results) {
                        if (!result.isSuccess()) {
                            sendErrors = sendErrors + result.getErrors().get(0).getMessage() + '\n';
                        }
                    }
                    if (statusEmail != null && sendErrors != '') {
                        sendFailureEmail(statusEmail, 'Member welcome email send failed!', sendErrors);
                    }
                }
            }
        } catch (Exception e) {
            if (statusEmail != null) {
                sendFailureEmail(statusEmail, 'Member welcome email apex exception!', e.getMessage() + e.getStackTraceString());
            }
        }

    }

    @TestVisible
    private static void sendFailureEmail(Messaging.SingleEmailMessage statusEmail, String subject, String body) {
        statusEmail.setPlainTextBody(body);
        statusEmail.setSubject(subject);
        Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{
                statusEmail
        }, false);
    }
}