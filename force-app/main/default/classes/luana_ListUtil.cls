/*
	Developer: WDCi (Lean)
    Date: 04/Jul/2016
    Task #: LCA-710 Luana implementation - Wizard for captsone workshop allocation
*/

public with sharing class luana_ListUtil {
    
    public static List<List<SObject>> getPageIterable(List<SObject> sobjectList, integer pageSize){
         
        List<List<SObject>> iterableList = new List<List<SObject>>();
         
        if(sobjectList != null){
            integer size = pageSize;
             
            integer counter = 1;
            integer listCounter = 0;
            List<SObject> pageList = new List<SObject>();
             
            for(SObject sobj : sobjectList){
                 
                pageList.add(sobj);
                 
                if(counter == size || listCounter == (sobjectList.size() - 1)){
                    iterableList.add(pageList.clone());
                    pageList = new List<SObject>();
                    counter = 1;
                     
                } else {
                    counter ++;
                }
                 
                listCounter ++;
                 
            }
        }
        
        return iterableList;
    }
    
}