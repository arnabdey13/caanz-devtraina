/**
    Developer: WDCi 
    Task: Luana Test class for luana_AT_DeleteAuditTrail class
    
    Change History:
    LCA-921 23/08/2016: WDCi KH - to add person account email field
**/
@isTest(seeAllData=false)
private class luana_AT_DeleteAuditTrail_Test {
    
    final static String classNamePrefixShort = 'ATDAT';
    final static String classNamePrefixLong = 'luana_AT_DeleteAuditTrail_Test';
    
    static testMethod void testDeleteLog() {

        //Create all the custom setting
        Luana_DataPrep_Test dataPrep = new Luana_DataPrep_Test();
        
        LuanaSMS__Program__c p = dataPrep.createNewProgram('ATDAT', 'ATDAT', 'ATDAT', 'ATDAT', 'ATDAT');
        insert p;
        LuanaSMS__Training_Organisation__c to = dataPrep.createNewTraningOrg('ATDT', 'ATDT', 'ATDT', 'ATDT', 'ATDT', 'ATDT');
        insert to;
        Product2 pro = dataPrep.createNewProduct('ATDAT', 'ATDAT');
        insert pro;
        Map<String, Id> poRtMap = dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c');
        LuanaSMS__Program_Offering__c po = dataPrep.createNewProgOffering('ATDAT', poRtMap.get('Accredited_Program'), p.Id, to.Id, pro.Id, pro.Id, pro.Id, 1, 2);
        insert po;
        LuanaSMS__Subject__c sub = dataPrep.createNewSubject('ATDAT', 'ATDAT', 'ATDAT', 10, 'ATDAT');
        insert sub;
        LuanaSMS__Program_Subject__c ps = dataPrep.createProgramSubject(p, sub, '');
        insert ps;        
        LuanaSMS__Delivery_Location__c dl = dataPrep.createNewDeliveryLocationRecord('ATDT', 'ATDAT', to.Id, '4551', 'Australia');
        insert dl;
        LuanaSMS__Program_Offering_Subject__c pos = dataPrep.createNewProgOffSubject(po.Id, sub.Id, 'Elective');
        insert pos;
        Map<String, Id> cRtMap = dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c');
        LuanaSMS__Course__c c = dataPrep.createNewCourse('ATDAT', po.Id, cRtMap.get('Accredited_Program'), 'Running');
        insert c;        
        Course_Delivery_Location__c cdl = dataPrep.createCourseDeliveryLocation(c.Id, dl.Id, 'Exam Location');
        insert cdl;
        Account busAcc = dataPrep.generateNewBusinessAcc('ATDAT', 'Business_Account', 'Business Account', 'test1234', '04040404', 'ATDAT', 'ATDAT', 'ATDAT');
        insert busAcc;
        Account perAcc = dataPrep.generateNewApplicantAcc('ATDAT', 'ATDAT', 'Full_Member');
        perAcc.personEmail = 'joe_1_'+classNamePrefixShort+'@gmail.com';//LCA-921 add person account email 
        perAcc.Communication_Preference__c= 'Home Phone';
        perAcc.PersonHomePhone= '1234';
        perAcc.PersonOtherStreet= '83 Saggers Road';
        perAcc.PersonOtherCity='JITARNING';
        perAcc.PersonOtherState='Western Australia';
        perAcc.PersonOtherCountry='Australia';
        perAcc.PersonOtherPostalCode='6365';
        insert perAcc;
        //Contact con = dataPrep.generateNewContact('ATDAT', busAcc);
        Contact con = new Contact(LastName = 'ATDAT', AccountId = busAcc.Id);
        insert con;
        Education_Record__c er = dataPrep.createNewEducationRecords(con.Id, System.today(), 'test', 'test');
        insert er;
        Employment_Token__c et = dataPrep.createEmplToken('ATDAT', busAcc.Id, 'testluana_AT_DeleteAuditTrail_Test', 'ATDAT', System.today() + 5, TRUE);
        insert et;
        Map<String, Id> spRtMap = dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c');        
        LuanaSMS__Student_Program__c sp = dataPrep.createNewStudentProgram(spRtMap.get('Accredited_Program'), con.Id, c.Id, 'ATDAT', 'In Progress');
        sp.LuanaSMS__Auto_Student_Program_Subject_Creation__c = false;
        insert sp;
        LuanaSMS__Student_Program_Subject__c sps = dataPrep.createNewStudentProgramSubject(con.Id, sp.Id, sub.Id);
        insert sps;
        Payment_Token__c pt = dataPrep.createNewPaymentToken(busAcc.Id, c.Id, System.today() + 5, false);
        insert pt;
        
        
        Test.startTest();
        List<LuanaSMS__Luana_Log__c> logs = [SELECT Id FROM LuanaSMS__Luana_Log__c];
        System.assertEquals(logs.size(), 0, 'Expect no Luana logs returned');
        
        delete pt;
        delete sps;
        delete sp;
        delete et;
        delete er;
        //insert con;
        //insert perAcc;
        //insert busAcc;
        delete cdl;
        delete c;        
        delete pos;
        delete dl;
        delete ps;        
        delete sub;
        delete po;
        delete to;
        delete p;
        logs = [SELECT Id FROM LuanaSMS__Luana_Log__c];
        System.assertEquals(logs.size(), 14, 'Expect 14x Luana logs returned');
        
        Test.stopTest();
        
    }
}