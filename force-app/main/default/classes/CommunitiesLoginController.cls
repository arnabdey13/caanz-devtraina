global with sharing class CommunitiesLoginController {
    global CommunitiesLoginController () {}   
    // Code we will invoke on page load.
	/*global PageReference forwardToAuthPage() {
		String startUrl = System.currentPageReference().getParameters().get('startURL');
		return new PageReference(Site.getPrefix() + '/PortalLogin?startURL=' +
									EncodingUtil.urlEncode(startURL, 'UTF-8'));
	} */
    
    global PageReference forwardToAuthPage() {
               String startUrl = System.currentPageReference().getParameters().get('startURL');
               String displayType = System.currentPageReference().getParameters().get('display');
        return Network.forwardToAuthPage(startUrl, displayType);
    }

}