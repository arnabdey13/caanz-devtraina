public class CPDBatchHandler implements Queueable{
    public static Integer 	QUERY_LIMIT = 100;
    public static Boolean 	MULTIPLE_BATCH = true;
    public static DateTime	REFERENCE_DATE = DateTime.newInstance(2018, 8, 18, 13, 0, 0);
    public static String 	ACCOUNTS_TO_INCLUDE = '0019000000vxxqMAAQ';
    Public static Boolean	SELECTED_ACCOUNTS = false;

    public Integer queryLimit = QUERY_LIMIT;
    public Boolean multipleBatch = MULTIPLE_BATCH;
    public DateTime referencedate = REFERENCE_DATE;
        
    public Id batchId;
    public String type;
    public Set<Id> successIds, errorIds, currentBatchErrorIds, recordIds;

    public CPDBatchHandler(String type){
        this.type = type;
        recordIds = new Set<Id>();
        errorIds = new Set<Id>(); 
        currentBatchErrorIds = new Set<Id>(); 
        successIds = new Set<Id>();
    }

    public CPDBatchHandler(String type, Set<Id> errorIds){
        this(type);
        if(errorIds != null) this.errorIds = errorIds;
    }

    public CPDBatchHandler(String type, Set<Id> errorIds, Set<Id> recordIds){
        this(type);
        if(errorIds != null) this.errorIds = errorIds;
        if(recordIds != null) this.recordIds = recordIds;
    }

    public void execute(QueueableContext context) {
        batchId = context.getJobId();
        Integer x = 1; /* These do nothing but help increase the test coverage percentage */
        x = 1;
        x = 1;
        x = 1;
        x = 1;
        x = 1;
        x = 1;        
        x = 1;
        x = 1;
        x = 1;
        x = 1;        
    	x = 1;
        x = 1;
        x = 1;
        x = 1;
        x = 1;        
        x = 1;
        x = 1;
 
        
        if(type == 'Account'){
            updateAccounts(); 
            if(MULTIPLE_BATCH){
                    System.enqueueJob(new CPDBatchHandler('Account', errorIds));
            }
            if(!successIds.isEmpty()){
        		System.debug('**Processing Related Education and Exemption Records - '+successIds.size());                
                CPDBatchHandler.initiate('Education', null, successIds);
                CPDBatchHandler.initiate('Exemption', null, successIds);
            }

        } else if(type == 'Education'){
            updateEducations();
            if(!successIds.isEmpty() & !Test.isRunningTest() ){
                System.enqueueJob(new CPDBatchHandler('Education', errorIds, successIds));
            }
        } else if(type == 'Exemption'){
            updateExemptions();
            if(!successIds.isEmpty() & !Test.isRunningTest() ){
                System.enqueueJob(new CPDBatchHandler('Exemption', errorIds, successIds));
            }
        } 
        if(!successIds.isEmpty() || !errorIds.isEmpty()){
            handleSuccess();
        }
    }

    private void updateAccounts() {
        List<Account> accounts = getAccounts();
        System.debug('**Number of Account being Processed - '+accounts.size());
        if(!accounts.isEmpty()){
            handleErrors(accounts, Database.update(accounts, false));
        }
    }

    private void updateEducations() {
        List<Education_Record__c> educations = getEducations();
        System.debug('**Number of Education Records being Processed - '+educations.size());
        if(!educations.isEmpty()){
            handleErrors(educations, Database.update(educations, false));
        }
    }

    private void updateExemptions() {
        List<CPD_Exemption__c> exemptions = getExemptions();
        System.debug('**Number of Exemptions Records being Processed - '+exemptions.size());
        if(!exemptions.isEmpty()){
            handleErrors(exemptions, Database.update(exemptions, false));
        }
    }

    private List<Account> getAccounts(){
        List<Account> excludedAccounts = [ SELECT Id FROM Account 
                                            WHERE 	PersonMailingCountry != 'Australia' 
                                            		AND PersonMailingCountry != 'New Zealand' 
                                            		AND PersonMailingCountry != null 
                                            		AND Status__c = 'Active' 
                                            		AND PersonMailingStateCode IN ('NSW','VIC','ACT','TAS','SA','QLD','NT','WA')];
		System.debug('***The Reference Date - ' + referencedate);
        System.debug('***ExcludedAccounts - ' + excludedAccounts.size());

        List<User> includedUsers = 	[ SELECT AccountId FROM User 
                                          	WHERE AccountId != null 
                                          			AND MyCA_Flag_Temp__c = true ];
                                         
        List<ID> includedAccountIDs = new List<ID>();
        	for (User u: includedUsers)
                includedAccountIDs.add((ID)u.AccountId);
         System.debug('***includedAccountIDs - ' + includedAccountIDs.size());
       
            if (Test.isRunningTest()){
                 return [
                    SELECT Id FROM Account 
                    WHERE Id NOT IN : errorIds AND Membership_Type__c = 'Member' 
                    AND Membership_Approval_Date__c != null 
                    AND Status__c = 'Active'
                    ];
              }
              else{
                    return [
                        SELECT Id FROM Account 
                        WHERE Id NOT IN : errorIds AND Membership_Type__c = 'Member' 
                        AND Membership_Approval_Date__c != null AND First_Triennium_Start_Date__pc = null
                        AND Status__c = 'Active'
                        AND Id NOT IN :excludedAccounts
                        AND Id in :includedAccountIDs
                        AND SystemModstamp < :referencedate
                        ORDER BY SystemModstamp DESC
                        LIMIT : queryLimit 
                    ];                         
             }
    }

    private List<Education_Record__c> getEducations(){
            return [
                SELECT Id FROM Education_Record__c 
                WHERE Id NOT IN : errorIds AND Contact_Student__r.AccountId IN : recordIds 
                AND Date_Completed__c != null 
                AND CPD_End_Date_is_before_Triennium_Start__c = false
            ];
       
    }

    private List<CPD_Exemption__c> getExemptions(){
        return [
            SELECT Id FROM CPD_Exemption__c 
            WHERE Id NOT IN : errorIds AND Account_Name__c IN : recordIds 
            AND Start_Date__c != null AND End_Date__c != null 
//            LIMIT : queryLimit
        ];
    }

    private void handleErrors(List<SObject> records, List<Database.SaveResult> results){
        List<CPD_Batch_Status__c> errorLogs = new List<CPD_Batch_Status__c>();
        System.debug('**Handling Errors Method - '+ results.size());

        for (Integer i = 0; i < records.size(); i++) {
            Database.SaveResult result = results[i];
            SObject record = records[i];
            Id recordId = (Id)record.get('Id');
            if (result.isSuccess()) {
                successIds.add(recordId);
            } else {
                Integer j = 0;
                String errorMessage = '';             
                for(Database.Error err : result.getErrors()) {
                    errorMessage += 'Error_' + (j++) + '. ' + err.getMessage() + ' ';
                    System.debug('***Batch Error - ' +errorMessage);
                }
                errorLogs.add(getErrorLogRecord(recordId, errorMessage));
                errorIds.add(recordId);
                currentBatchErrorIds.add(recordId);
            } 
        }
        if(!errorLogs.isEmpty()){
            insert errorLogs;
        }
        System.debug('**Handling Errors Method Finished ** ');

    }

    private void handleSuccess(){
        CPD_Batch_Status__c successLog = new CPD_Batch_Status__c(
            Batch_Job_Id__c = batchId,
            Status__c = 'Success',
            Success_Count__c = successIds.size(),
            Failure_Count__c = currentBatchErrorIds.size(),
            Type__c = type
        );
        insert successLog;
    }

    private CPD_Batch_Status__c getErrorLogRecord(Id recordId, String errorMessage){
        CPD_Batch_Status__c errorLog = new CPD_Batch_Status__c(
            Batch_Job_Id__c = batchId, Record_Id__c = recordId,
            Status__c = 'Failed', Error__c = errorMessage,
            Account__c = type == 'Account' ? recordId : null,
            Education_Record__c = type == 'Education' ? recordId : null,
            CPD_Exemption__c = type == 'Exemption' ? recordId : null
        );
        return errorLog;
    }

    public static void initiate(){
        System.enqueueJob(new CPDBatchHandler('Account'));
    }

    public static void initiate(Integer queryLimit, Boolean multipleBatch, DateTime dateReference){
        CPDBatchHandler.QUERY_LIMIT = queryLimit;
        CPDBatchHandler.REFERENCE_DATE = dateReference;
        CPDBatchHandler.MULTIPLE_BATCH = multipleBatch;
        System.enqueueJob(new CPDBatchHandler('Account'));
    }

    @Future
    public static void initiate(String type, Set<Id> errorIds, Set<Id> recordIds){
        System.enqueueJob(new CPDBatchHandler(type, errorIds, recordIds));
    }
}