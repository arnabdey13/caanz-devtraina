/* 
    Developer: WDCi (Lean)
    Date: 02/Aug/2016
    Task #: LCA-822 Luana implementation - Wizard for Non AU program enrolment
*/

public without sharing class luana_ProgramEnrolmentWizardCOController {
    
    luana_ProgramEnrolmentWizardController stdController;
	
    public luana_ProgramEnrolmentWizardCOController(luana_ProgramEnrolmentWizardController stdController){
        this.stdController = stdController;
        
        init();
    }
    
    public void init(){
    	
    }
    
    public PageReference courseNext(){
    	
    	if(stdController.selectedCourseId != null && stdController.selectedCourseId != '' && stdController.selectedProgramOffering != null){
            
            for(LuanaSMS__Course__c course : stdController.selectedProgramOffering.LuanaSMS__Courses__r){
                if(course.Id == stdController.selectedCourseId){
                    stdController.selectedCourse = course;
                    
                    break;
                }
            }
            
            return Page.luana_ProgramEnrolmentWizardSubject;
                        
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please select a course below.'));
        }
        
        return null;
    	
    }
    
    public PageReference courseBack(){
    	
    	PageReference pageRef = Page.luana_ProgramEnrolmentWizardProgram;
        
        return pageRef;
    }
}