public without sharing class QuestionnaireListController {
    
    @AuraEnabled
    public static List<QPR_Questionnaire__c> fetchQPRQuestionnaire(String questionnaireStatus){
        List<QPR_Questionnaire__c> qprQuestionList = new List<QPR_Questionnaire__c>();
        Set<String> questionnaireStatusSet = new Set<String>();
        for(String so : questionnaireStatus.split(',')){
            questionnaireStatusSet.add(so);
        }
        
        Id userRec = [select AccountId from User where Id=:userInfo.getUserId() limit 1].AccountId;
        
        system.debug('***questionnaireStatusSet****'+questionnaireStatusSet);
        for(QPR_Questionnaire__c qpr : [SELECT Name, Practice__r.Name, Account__c, Account__r.Name, 
                                        Practice_ID__c, Status__c, Review_Number__c, Hidden_Country__c  
                                        FROM QPR_Questionnaire__c
                                        WHERE Status__c IN : questionnaireStatusSet AND Account__c =: userRec]){
            qprQuestionList.add(qpr);
        }
        return qprQuestionList ;
    }
}