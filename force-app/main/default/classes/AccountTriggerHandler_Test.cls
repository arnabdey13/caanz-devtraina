/***********************************************************************************************************************************************************************
Name: AccountTriggerHandler_Test 
============================================================================================================================== 
Purpose: This class contains code related to Unit Testing and test coverage of class AccountTriggerHandler 
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR           DATE          DETAIL     Description 
1.0        Rama Krishna    08/02/2019    Created     This class contains code related to Unit Testing and test coverage of class AccountTriggerHandler 
                                                             
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/
@isTest
public class AccountTriggerHandler_Test { 
    
    static RecordType personAccountRecordType = [SELECT Id, IsPersonType FROM RecordType WHERE Name = 'Member' and SObjectType = 'Account' and IsPersonType = true];   
    static Id personAccountRecordTypeId = personAccountRecordType.Id;

//******************************************************************************************
//                             testMethods
//******************************************************************************************         

    //test method to check coverage of all the events in the AccountTriggerHandler class
    static testMethod void testAccountHandler(){
        Test.startTest();
        Profile profile1 = [Select Id from Profile where name = 'CAANZ Business Management User'];
        System.debug('What is the profile id ' + profile1);
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        User u = new User( UserRoleId = portalRole.Id,
                            ProfileId = profile1.Id,
                            Username = 'testtermsconditions1234423@charteredaccountantsanz.com',
                            Alias = 'batman',
                            Email='testtermsconditions1234423@charteredaccountantsanz.com',
                            EmailEncodingKey='UTF-8',
                            Firstname='Bruce',
                            Lastname='Wayne',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_NZ',
                            TimeZoneSidKey='Pacific/Auckland');
        insert u; 
        //run as user to test code to perform user level testing                         
        System.runas(u){             
            Account acc = TestObjectCreator.createPersonAccount();
            acc.Designation__c = 'ACA';
            acc.Membership_Type__c = 'Member';
            acc.RollUpFromProfessionalConductAndFineCost__c=true;
            acc.Membership_Approval_Date__c = Date.today().addDays(1);
            acc.Communication_Preference__c='909090909090';
            acc.PersonOtherStreet= '83 Saggers Road';
            acc.PersonOtherCity='JITARNING';
            acc.PersonOtherState='Western Australia';
            acc.PersonOtherCountry='Australia';
            acc.PersonOtherPostalCode='6365';
            //insert acc;
            acc.PersonEmail = 'testterms1234423@charteredaccountantsanz.com';
            
            //update acc;
            //delete acc;
            Account[] savedAccts = [SELECT Id, Name FROM Account WHERE PersonEmail = 'testterms1234423@charteredaccountantsanz.com' ALL ROWS]; 
            //undelete savedAccts;             
        }
        Test.stopTest();
    }

    //test method to check the functionality of bulk test data insertion
    static testMethod void testAccountHandlerBulk() {
        Test.startTest();   
        List<Account> accList = new List<Account>();
        accList = TestObjectCreator.createPersonAccountBulk(25,personAccountRecordTypeId);        
        insert accList;        
        TestSubscriptionUtils.createTestData();
        Test.stopTest();    
    }
}