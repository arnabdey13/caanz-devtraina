public with sharing class TriggerConfigHandler {
	// Class Variables
	private static List<TriggerConfig__c> TriggerConfig_List = TriggerConfig__c.getAll().values();
	private static Map<String,Set<String>> TriggerConfigIdToSetOfSourceLookupFieldIds_Map = new Map<String,Set<String>>();
	private static Map<String,Set<Id>> TriggerConfigIdToSetOfUpdatedParentRecordIds_Map = new Map<String,Set<Id>>();
	private static Map<String,String> TriggerConfigIdAndParentIdToSourceFieldValue_Map = new Map<String,String>();
	
	// Class Methods
	public static void updatesBasedOnTriggerConfig(){
		List<TriggerConfig__c> ThisTriggerConfig_List = new List<TriggerConfig__c>();
		for(TriggerConfig__c TriggerConfig : TriggerConfig_List){
			if( TriggerConfig.isActive__c && TriggerConfig.SourceObject__c==String.valueOf(trigger.new.getSObjectType()) ){
				ThisTriggerConfig_List.add( TriggerConfig );
			}
		}
		
		if(ThisTriggerConfig_List.size()>0){
			for(Integer i=0; i<trigger.new.size(); i++){
				for(TriggerConfig__c ThisTriggerConfig : ThisTriggerConfig_List){
					if(
					ThisTriggerConfig.ParentToChild__c &&
					TriggerConfigIdToSetOfUpdatedParentRecordIds_Map.containsKey(ThisTriggerConfig.Id)
					){
						if(
						TriggerConfigIdToSetOfSourceLookupFieldIds_Map!=null && 
						TriggerConfigIdToSetOfSourceLookupFieldIds_Map.containsKey(ThisTriggerConfig.Id)
						){
							TriggerConfigIdToSetOfSourceLookupFieldIds_Map.remove(ThisTriggerConfig.Id);
							break; // Stops Recursion
						}
						continue;
					}
					
					if( // Dependant on old Value
					ThisTriggerConfig.SourceFieldOldValue__c!=null &&
					(ThisTriggerConfig.SourceFieldNewValue__c=='ANY_VALUE' ||
					trigger.new[i].get(ThisTriggerConfig.SourceField__c)==ThisTriggerConfig.SourceFieldNewValue__c) &&
					trigger.old[i].get(ThisTriggerConfig.SourceField__c)==ThisTriggerConfig.SourceFieldOldValue__c
					){
						populateMaps( ThisTriggerConfig, i );
					}
					else if( // Not dependant on old Value
					ThisTriggerConfig.SourceFieldOldValue__c==null &&
					(ThisTriggerConfig.SourceFieldNewValue__c=='ANY_VALUE' ||
					trigger.new[i].get(ThisTriggerConfig.SourceField__c)==ThisTriggerConfig.SourceFieldNewValue__c) &&
					trigger.new[i].get(ThisTriggerConfig.SourceField__c)!=trigger.old[i].get(ThisTriggerConfig.SourceField__c)
					){
						populateMaps( ThisTriggerConfig, i );
					}
				}
			}
		}
		
		if( TriggerConfigIdToSetOfSourceLookupFieldIds_Map.size()>0){
			for( TriggerConfig__c ThisTriggerConfig : ThisTriggerConfig_List ){
				if( TriggerConfigIdToSetOfSourceLookupFieldIds_Map.containsKey(ThisTriggerConfig.Id) ){
					Set<String> SourceLookupFieldId_Set = 
						TriggerConfigIdToSetOfSourceLookupFieldIds_Map.get(ThisTriggerConfig.Id);
					
					String DynamicSOQL='';
					DynamicSOQL+='Select '+ThisTriggerConfig.TargetField__c;
					DynamicSOQL+=(ThisTriggerConfig.ParentToChild__c)?', '+ThisTriggerConfig.SourceRelationshipToTarget__c:'';
					DynamicSOQL+=' from ' +ThisTriggerConfig.TargetObject__c;
					
					String FieldToLookUp = (!ThisTriggerConfig.ParentToChild__c)?'Id':ThisTriggerConfig.SourceRelationshipToTarget__c;
					DynamicSOQL+=' where '+FieldToLookUp+' IN :SourceLookupFieldId_Set';
					
					if(ThisTriggerConfig.TargetFieldOldValue__c!=null){
						DynamicSOQL+=' and ' +ThisTriggerConfig.TargetField__c+ '=\'';
						if( ThisTriggerConfig.TargetField__c=='RecordTypeId' ){
							DynamicSOQL+=RecordTypeCache.getId(ThisTriggerConfig.TargetObject__c, 
								ThisTriggerConfig.TargetFieldOldValue__c);
						}
						else{
							DynamicSOQL+=ThisTriggerConfig.TargetFieldOldValue__c;
						}
						DynamicSOQL+='\'';
					}
					
					List<sObject> sObject_List = Database.query( DynamicSOQL ); // ## Query is in a FOR loop!
					
					for( sObject sObj : sObject_List ){
						if( ThisTriggerConfig.TargetField__c=='RecordTypeId'){
							sObj.put(
								ThisTriggerConfig.TargetField__c,
								RecordTypeCache.getId(ThisTriggerConfig.TargetObject__c, ThisTriggerConfig.TargetFieldNewValue__c)
							);
						}
						else if( ThisTriggerConfig.TargetFieldNewValue__c=='SOURCE_FIELD' ){
							String SourceFieldNewValue = TriggerConfigIdAndParentIdToSourceFieldValue_Map.get(
								ThisTriggerConfig.Id+':'+
								sObj.get(
									(ThisTriggerConfig.ParentToChild__c)?
										ThisTriggerConfig.SourceRelationshipToTarget__c:
										'Id'
								)
							);
							sObj.put(ThisTriggerConfig.TargetField__c, SourceFieldNewValue);
						}
						else{
							sObj.put(ThisTriggerConfig.TargetField__c, ThisTriggerConfig.TargetFieldNewValue__c);
						}
					}
					update sObject_List;
				}
			}
		}
	}
	
	private static void populateMaps( TriggerConfig__c TriggerConfig, Integer i ){
		populateTriggerConfigIdToSetOfUpdatedParentRecordIdsMap( TriggerConfig, i );
		populateTriggerConfigIdToSetOfSourceLookupFieldIdsMap( TriggerConfig, i );
		populateTriggerConfigIdAndParentIdToSourceFieldValueMap( TriggerConfig, i );
	}
	private static void populateTriggerConfigIdAndParentIdToSourceFieldValueMap( TriggerConfig__c TriggerConfig, Integer i ){
		if( TriggerConfig.TargetFieldNewValue__c=='SOURCE_FIELD' ){
			TriggerConfigIdAndParentIdToSourceFieldValue_Map.put( 
				TriggerConfig.Id+':'+
				trigger.new[i].get(
					( TriggerConfig.ParentToChild__c )?
						'Id':TriggerConfig.SourceRelationshipToTarget__c
				), 
				String.valueOf(trigger.new[i].get(TriggerConfig.SourceField__c))
			);
		}
	}
	private static void populateTriggerConfigIdToSetOfUpdatedParentRecordIdsMap( TriggerConfig__c TriggerConfig, Integer i ){
		if( TriggerConfig.ParentToChild__c ){
			// Prevents recursion
			Set<Id> CurrentUpdatedParentRecordId_Set = 
				TriggerConfigIdToSetOfUpdatedParentRecordIds_Map.get( TriggerConfig.Id );
			if(CurrentUpdatedParentRecordId_Set==null){
				TriggerConfigIdToSetOfUpdatedParentRecordIds_Map.put( TriggerConfig.Id,
					new Set<Id>{ trigger.new[i].Id }
				);
			}
			else{
				CurrentUpdatedParentRecordId_Set.add( trigger.new[i].Id );
				TriggerConfigIdToSetOfUpdatedParentRecordIds_Map.put( TriggerConfig.Id, 
					CurrentUpdatedParentRecordId_Set );
			}
		}
	}
	private static void populateTriggerConfigIdToSetOfSourceLookupFieldIdsMap( TriggerConfig__c TriggerConfig, Integer i ){
		String FieldToLookUp = (TriggerConfig.ParentToChild__c)?'Id':TriggerConfig.SourceRelationshipToTarget__c;
		Set<String> CurrentSourceLookupFieldId_Set = 
			TriggerConfigIdToSetOfSourceLookupFieldIds_Map.get( TriggerConfig.Id );
		if(CurrentSourceLookupFieldId_Set==null){
			TriggerConfigIdToSetOfSourceLookupFieldIds_Map.put( TriggerConfig.Id,
				new Set<String>{
					String.valueOf( trigger.new[i].get( FieldToLookUp ) )
				}
			);
		}
		else{
			CurrentSourceLookupFieldId_Set.add( 
				String.valueOf( trigger.new[i].get( FieldToLookUp ) )
			);
			TriggerConfigIdToSetOfSourceLookupFieldIds_Map.put( TriggerConfig.Id, 
				CurrentSourceLookupFieldId_Set );
		}
	}
}