/**
    Change History: 
    LCA-1176            13/03/2018      WDCi LKoh:  added support for returning back to MyEnrolmentView
                        29/03/2018      WDCi KH: Update Special Consideration Case's FieldSet display view based on Publish flag
**/
public with sharing class luana_RequestsViewController {
    
    private Id caseId;
    private String caseURLParamValue = 'csId';
    public case caseObj { get; set; }
    public String detailMessage {get; private set;}
    
    public List<Schema.FieldSetMember> fieldSetMembers {get; set;}
    
    public Map<String, String> referenceNameMap {get; set;}
    public Map<String, Boolean> isReferenceNameMap {get; set;}
    
    //LCA-394, Phase 3
    public CaseComment caseCommObj{get; set;}
    public Attachment att {get; set;}
    
    // LCA-1176
    private Id spId;
    private String spURLParamValue = 'spid';
    
    public luana_RequestsViewController(){
        
        if(ApexPages.currentPage().getParameters().containsKey(caseURLParamValue)){
            caseId = ApexPages.currentPage().getParameters().get(caseURLParamValue);
        }
        
        // LCA-1176
        if(ApexPages.currentPage().getParameters().containsKey(spURLParamValue)){
            spId = ApexPages.currentPage().getParameters().get(spURLParamValue);
        }
        
        referenceNameMap = new Map<String, String>();
        isReferenceNameMap = new Map<String, Boolean>();
        this.caseObj = getCase();
        
        caseCommObj = new CaseComment();
        att = new Attachment();
    }
    
    public List<Schema.FieldSetMember> getFields(){
    
        List<Schema.FieldSetMember> fieldSetMembers = new List<Schema.FieldSetMember>();
        
        if(identifyRecordType(caseId).recordType.Name == 'Module Cancellation/Discontinue'){
            fieldSetMembers = SObjectType.Case.FieldSets.DW.getFields();
            detailMessage = 'Module Cancellation/Discontinue';
        }else if(identifyRecordType(caseId).recordType.Name == 'Module Exemption'){
            fieldSetMembers = SObjectType.Case.FieldSets.ME.getFields();
            detailMessage = 'Module Exemption';
        }else if(identifyRecordType(caseId).recordType.Name == 'Request Competency Area'){
            fieldSetMembers = SObjectType.Case.FieldSets.CA.getFields();
            detailMessage = 'Competency Area';
        }else if(identifyRecordType(caseId).recordType.Name == 'Out of Approved Employment'){
            fieldSetMembers = SObjectType.Case.FieldSets.OOAE.getFields();
            detailMessage = 'Out of Approved Employment';
        }else if(identifyRecordType(caseId).recordType.Name == 'Request Special Consideration'){
            if(identifyRecordType(caseId).publish__c){
                fieldSetMembers = SObjectType.Case.FieldSets.SC.getFields();
            }else{
                fieldSetMembers = SObjectType.Case.FieldSets.SC_Not_Pubish.getFields();
            }
            detailMessage = 'Request Special Consideration';
        }else if(identifyRecordType(caseId).recordType.Name == 'Request for Assistance'){
            fieldSetMembers = SObjectType.Case.FieldSets.RA.getFields();
            detailMessage = 'Request for Assistance';
        }
        return fieldSetMembers;
    }
    
    public Case getCase(){
        try{
            String query = 'SELECT ';
            for(Schema.FieldSetMember f : this.getFields()) {
                query += f.getFieldPath() + ', ';
                
                if(String.valueOf(f.getFieldPath()) == 'Module__r.Name' || String.valueOf(f.getFieldPath()) == 'Module__c'){
                    referenceNameMap.put(String.valueOf(f.getFieldPath()), 'Related Module');
                    isReferenceNameMap.put(String.valueOf(f.getFieldPath()), true);
                }else if(String.valueOf(f.getFieldPath()) == 'Student_Program__r.Name'){
                    referenceNameMap.put(String.valueOf(f.getFieldPath()), 'Related Enrolment');
                    isReferenceNameMap.put(String.valueOf(f.getFieldPath()), true);
                }else if(String.valueOf(f.getFieldPath()) == 'Student_Program__r.Course_Name__c'){
                    referenceNameMap.put(String.valueOf(f.getFieldPath()), 'Related Enrolment');
                    isReferenceNameMap.put(String.valueOf(f.getFieldPath()), true);
                }else{
                    referenceNameMap.put(String.valueOf(f.getFieldPath()), String.valueOf(f.getLabel()));
                    isReferenceNameMap.put(String.valueOf(f.getFieldPath()), false);
                }
            }
            
            query += 'Id, Publish__c FROM Case Where Id = \''+ caseId +'\' LIMIT 1';
            System.debug('**query: ' + query);
            return Database.query(query);
            
        }Catch(Exception exp){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Unexpected error found please contact support. ' + exp.getMessage()));
        }
        return null;
    }
    
    public Case identifyRecordType(Id caseId){
        Case currentCase = [select Id, recordType.Name, Publish__c from Case Where Id =: caseId limit 1];
        return currentCase;
    }
    
    public List<Attachment> getAttachment(){
        return [select Id, Name from Attachment Where ParentId =: caseId];
    }
    
    public PageReference back(){
        
        // LCA-1176
        if (spId != null) {
            PageReference myEnrolmentPage = Page.luana_MyEnrolmentView;
            myEnrolmentPage.getParameters().put('spid', spId);
            myEnrolmentPage.setRedirect(true);
            return myEnrolmentPage;
        }
        
        return page.luana_MyRequest;
    }
    
    //LCA-394 Phase 3
    public List<CaseComment> getRelatedComment(){
        return [SELECT CommentBody, IsPublished, ParentId, CreatedDate, CreatedBy.Name FROM CaseComment Where ParentId =: caseId Order by CreatedDate DESC];
    }
    
    //LCA-394 Phase 3
    public PageReference insertNewCaseComment(){
        try{
            uploadAttToCase(caseId);
            
            CaseComment cc = caseCommObj;
            cc.ParentId = caseId;
            cc.IsPublished = true;
            if(cc.CommentBody != null){
                insert caseCommObj;
            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Comment is a required. You must enter a value'));
            }        
            PageReference viewReqPage = new PageReference(luana_NetworkUtil.getCommunityPath() +'/luana_RequestView?csId=' + caseId);
            viewReqPage.setRedirect(true);
            return viewReqPage;
        }Catch(Exception exp){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Error create case comment. Please try again later, if the problem remain please contact us.'));
            return null;
        }
    }
    //LCA-394 Phase 3
    public void uploadAttToCase(Id csId) {
        List<Attachment> newAtts = new List<Attachment>();
        if(att.body != null){
            Attachment newAtt = att.clone(false, true);
            newAtt.ParentId = csId;
            newAtts.add(newAtt);
        }
        
        if(newAtts.size() > 0){
            insert newAtts;
            att = new Attachment();
            //LCA-497 bug fix, this will cause the comment is not created when an attachment added
            //caseCommObj = new CaseComment();        
        }
    }
}