@IsTest(seeAllData=false)
private class ff_ServiceHandlerTests {

    private static final String ACCOUNT_NAME = 'Test Account';

    @isTest
    private static void testUpload() {
        List<Account> accounts = getTestAccountList();

        ff_ServiceHandler handler = new ff_ServiceHandler(new MockRecordIdProvider(accounts[0].Id), new MockLoader(), new MockSaverContactInsertAccountUpsert());
        Id attachmentId = handler.uploadChunk(accounts[0].Id, '', 'Fake.txt', 'dGhpcyBpcyBhIHRlc3QgZmlsZSBmb3IgdXBsb2Fk', 'text/plain');

        System.assertNotEquals(null, attachmentId, 'file uploaded without errors');
    }

    @isTest
    private static void testLoadAndDeleteExisingAccount() {
        List<Account> accounts = getTestAccountList();
        ff_ServiceHandler handler = new ff_ServiceHandler(new MockRecordIdProvider(accounts[0].Id), new MockLoader(), new MockSaverContactInsertAccountUpsert());
        ff_ReadData read = handler.load(null);

        System.assertEquals(false, read.records.isEmpty(), 'ff_ReadData.records should not be empty');

        handler = new ff_ServiceHandler(new MockRecordIdProvider(accounts[0].Id), new MockLoader(), new MockSaverContactInsertAccountUpsert());
        ff_WriteResult deleteResult = handler.deleteRecord(accounts[0].Id);
        System.assertEquals(0, deleteResult.recordErrors.size(), 'no errors deleting primary account');
    }

    @IsTest
    private static void testLoadResult() {
        Opportunity a = new Opportunity();
        List<SObject> accs = new List<SObject>{
                a
        };
        ff_WriteData wd = new ff_WriteData();
        wd.records.put('INSERT-ACC1', a);
        List<Database.SaveResult> srs = Database.insert(accs, false);
        ff_ServiceHandler h = new ff_ServiceHandler(null, null, null);
        ff_WriteResult result = new ff_WriteResult();
        h.loadResults(wd, accs, result, srs);
        System.assertEquals(new Set<String>{
                'Name', 'CloseDate', 'StageName'
        }, result.fieldErrors.get('INSERT-ACC1').keySet(), 'Field errors are returned in fieldErrors');
    }

    @isTest
    private static void testLoadMethodFocusOnExisingAccount() {
        List<Account> accounts = getTestAccountList();
        ff_ServiceHandler handler = new ff_ServiceHandler(new MockRecordIdProvider(accounts[0].Id), new MockLoader(), new MockSaverContactInsertAccountUpsert());
        ff_ReadData read = handler.load(null);

        System.assertEquals(false, read.records.isEmpty(),
                'ff_ReadData.records should not be empty');
        System.assertNotEquals(null, read.records.get('Account'),
                'ff_ReadData.records should hold an account');
        System.assertNotEquals(null, read.records.get('Account')[0].sObjectList,
                'read.records.get(Account)[0].sObjectList should hold a List<Account>');
        System.assertEquals(ACCOUNT_NAME, read.records.get('Account')[0].sObjectList[0].get('Name'),
                'The load method should hold an account with the correct name.');
        System.assertEquals(false, read.wrappers.isEmpty(),
                'ff_ReadData.wrappers should not be empty');
    }

    @isTest
    private static void testSaveMethodFocusOnExistingAccount() {
        // focus behaviour for a Community User updating thier PersonAccount
        List<Account> accounts = getTestAccountList();
        ff_ServiceHandler handler = new ff_ServiceHandler(new MockRecordIdProvider(accounts[0].Id), new MockLoader(), new MockSaverAccountUpdate());
        ff_ReadData read = handler.load(null);

        ff_WriteData wd = new ff_WriteData();

        // PersonAccounts not enabled in the source DE org so only using Account fields in test
        Account accountForUpdate = (Account) read.records .get('Account')
                .get(0).sObjectList // first Accounts list is the only Accounts list for this form
                .get(0); // the sObjectList is a single list so safe to use first record
        accountForUpdate.Phone = '4155551212';
        wd.records.put(accountForUpdate.Id, accountForUpdate);

        ff_WriteResult saveResult = handler.save(wd);
        System.debug(wd);
        System.assertEquals(1, saveResult.results.size(), 'The updated Account should be present in the result');

    }

    @isTest
    private static void testSaveMethodFocusOnNewContactWithUpsertedAccount() {
        // focus behaviour for the sample registration form
        // pretty much the same as ff_SampleDriverTests.app
        ff_WriteData wd = new ff_WriteData();
        List<Account> accounts = getTestAccountList();
        Account accountForUpsert = new Account(Id = accounts.get(0).Id, Name = accounts.get(0).Name);
        accountForUpsert.Name = accountForUpsert.Name + 'XXX';
        // account being upserted is nested inside parent account so doesn't need to be in the records map
        wd.records.put('INSERT-Contact1', new Contact(LastName = 'Smith', Account = accountForUpsert));

        ff_ServiceHandler handler = new ff_ServiceHandler(
                new ff_SampleService.RecordIdProviderImpl(accountForUpsert.Id, null),
                new ff_SampleService.LoaderImpl(),
                new ff_SampleService.SaverImpl());
        ff_WriteResult saveResult = handler.save(wd);

        System.debug(saveResult);
        System.assertEquals(0, saveResult.recordErrors.size(), 'No errors for valid save data');

        System.assertEquals(1, saveResult.results.size(), 'The inserted Contact should be present in the result');
    }

    private static List<Account> getTestAccountList() {
        return new ff_TestDataAccountBuilder().name(ACCOUNT_NAME)
                .billingAddress('1 Market St', 'San Francisco', 'California', '94105', 'United States')
                .create(1, true);
    }

    // the account id here represents the only account that the current user can read/write based on the sharing model
    // this is often seen in Communities that use PersonAccounts attached to Users.
    private class MockRecordIdProvider implements ff_ServiceHandler.RecordIdProvider {
        Id accountId;

        public MockRecordIdProvider(Id accountId) {
            this.accountId = accountId;
        }
        public Map<Schema.sObjectType, Set<Id>> getContext() {
            Map<Schema.sObjectType, Set<Id>> result = new Map<Schema.sObjectType, Set<Id>>();
            if (accountId != null) {
                result.put(Account.getSObjectType(), new Set<Id>{
                        accountId
                });
            }
            return result;
        }
    }

    private class MockLoader extends ff_ServiceHandler.LoadHandler {

        public override Map<String, List<ff_Service.SObjectWrapper>> loadRecordsForRead(Map<Schema.sObjectType, Set<Id>> recordIds) {

            List<Account> accounts = [SELECT Name FROM Account WHERE Id IN:recordIds.get(Account.getSObjectType())];
            List<ff_Service.SObjectWrapper> accountWrappers = new List<ff_Service.SObjectWrapper>();
            accountWrappers.add(new ff_Service.SObjectWrapper(accounts, false));
            return new Map<String, List<ff_Service.SObjectWrapper>>{
                    'Account' => accountWrappers
            };
        }

        public override Map<String, List<ff_Service.CustomWrapper>> loadWrappersForRead(Map<Schema.sObjectType, Set<Id>> recordIds) {
            Map<String, ff_WrapperSource> sources = new Map<String, ff_WrapperSource>();
            sources.put('Account.Type', new ff_WrappedFieldDescribeSource(Account.Type));
            return ff_ServiceHandler.loadWrappers(sources);
        }
    }

    private class MockSaverContactInsertAccountUpsert extends ff_ServiceHandler.SaveHandler {
        public override List<sObject> processUpserts(RecordIdProvider idProvider, List<SObject> upsertRecords, List<SObject> otherRecords) {
            System.debug('Upserting: ' + upsertRecords);
            if (upsertRecords.size() != 1 || otherRecords.size() != 1) {
                throw new UnsupportedOperationException('Only one Contact and one account upsert supported');
            }
            Account upsertAccount = (Account) upsertRecords[0];
            Contact updateContact = (Contact) otherRecords[0];
            List<Account> matchingNames = [SELECT Id FROM Account WHERE Name = :upsertAccount.Name LIMIT 1];
            if (matchingNames.size() == 0) {
                insert upsertAccount;
                updateContact.Account = upsertAccount;
            } else {
                updateContact.Account = matchingNames.get(0);
            }
            return otherRecords;
        }
    }

    private class MockSaverAccountUpdate extends ff_ServiceHandler.SaveHandler {
        public override List<sObject> prepareUpdates(RecordIdProvider idProvider, List<SObject> updateRecords) {
            if (updateRecords.size() != 1 || updateRecords.get(0).getSObjectType() != SObjectType.Account.getSObjectType()) {
                throw new UnsupportedOperationException('Only one Account update supported');
            } else {
                return updateRecords;
            }
        }
    }
}