/**
    Developer: WDCi (Lean)
    Development Date: 15/04/2016
    Task #: Controller for luana_MyEnrolmentView vf page LCA-247
    
    Change History:
    LCA-572             09/06/2016      WDCi Lean:  to hide virtual class and master class when stud prog is unpaid
    LCA-626             09/06/2016      WDCi Lean:  add timezone next to datetime field
    LCA-642             14/06/2016      WDCi KH:    change timezone to always Australia/Sydney time
    LCA-639             14/06/2016      WDCi KH:    create masterclass tab flag for display or hide
    LCA-511             04/07/2016      WDCi KH:    include exam tab
    LCA-720             11/07/2016      WDCi Lean:  to list relationship with status Current only
    LCA-748             18/07/2016      WDCi Lean:  to support nz masterclass
    LCA-742             19/07/2016      WDCi Lean:  consolidate workshop attendance list
    LCA-867             29/07/2016      WDCi Lean:  change "Ready_for_Masterclass_Enrolment__c" to "Masterclass_Broadcasted__c"
    LCA-874             01/08/2016      WDCi Lean:  expose getStudentProgramIdFromUrl for RegisterMasterclass component
    LCA-873             01/08/2016      WDCi Lean:  enable the Masterclass tab in community as long as they have masterclass enrolment (to support bulk load without broadcast)
    LCA-887             04/08/2016      WDCi Lean:  append user timezone to date/time field, remove conversion as we now depends on user profile
    LCA-829             09/08/2016      WDCi KH:    include sup exam detail page in the exam tab & display supp exam button
    LCA-553/LCA-901     10/08/2016      WDCi Lean:  remove noncommunity check
    //discarded LCA-909 12/08/2016 - WDCi Lean: make overseas student to see only AU masterclass
    LCA-802             18/08/2016      WDCi Lean:  move masterclass view to sub vf component
    LCA-916             19/08/2016      WDCi Lean:  tentative solution - display session timestamp with timezone in capstone workshop section
    LCA-1061            25/11/2016      WDCi Lean:  Hide workshop preference after cutoff date
    LCA-1093            08/03/2017      WDCi LKoh:  Supplementary Exam Enrol button to only show up if supp exam enrol end date has not passed
    LCA-1161            07/03/2018      WDCi LKoh:  include Status & Final Module Mark column to the page
    LCA-1176            13/03/2018      WDCi LKoh:  added a new My Requests panel to MyEnrolmentView page and new fields to display on MyEnrolmentView Subjects panel   
                                                                               
**/

public without sharing class luana_MyEnrolmentViewController {
    
    public Id custCommConId {get; private set;}
    public Id custCommAccId {get; private set;}
    public String custCommCountry {get; private set;} //LCA-748
    
    public LuanaSMS__Student_Program__c selectedSP {public get; private set;}
    
    public boolean isValid {public get; private set;}
    public boolean isMember {public get; private set;}
    
    public Set<String> mentorList {public get; private set;}
    public List<Employment_History__c> employmentHistories {get; set;}
    
    //LCA-499
    public List<LuanaSMS__Attendance2__c> virtualClassSessions {get; set;}
    
    //LCA-572
    public List<LuanaSMS__Attendance2__c> examSessions {get; set;}
    
    //LCA-470
    //LCA-802 public List<LuanaSMS__Student_Program__c> enrolledMasterSPList {get; set;}
    public List<Program_Offering_Related__c> relatedMasterPOList {get; set;}
    public String selectedMasterPOId {get; set; }
    public Boolean isMasterclassEnrolled {get; set;}
    
    public boolean isCapstone {get; set;}
    
    //LCA-571
    public String cancelAttendanceId {get; set;}
    
    //LCA-572
    public boolean isEligibleForClassRegistration {get; private set;}
    
    //LCA-626
    public String userTimezone {get; private set;}
    
    //LCA-887 no longer require conversion
    //LCA-642
    //public Map<Id, String> vcStartTime {get; set;}
    //public Map<Id, String> vcEndTime {get; set;}
    //LCA-887 end
    
    //LCA-369
    public Boolean isMasterclassReady {get; private set;}
    
    //LCA-511
    public Boolean isExamBroadcasted {get; set;}
    public Boolean isWorkshopBroadcasted {get; set;}
    
    //LCA-829
    public Boolean isSuppExamBroadcasted {get; set;}
    public LuanaSMS__Attendance2__c attSuppExamObj {get; set;}
    
    public List<LuanaSMS__Attendance2__c> workshopAttendances {get; set;}
    public LuanaSMS__Attendance2__c attExamObj {get; set;}
    
    
    public String billingAdd {get; set;}
    
    //LCA-829
    public Boolean isSuppExamApproved {get; set;}
    public Boolean isSuppExamEnrolled {get; set;}
    
    //LCA-1061
    public boolean displayWorkshopPreference {get; private set;}
    
    //LCA-1161
    public boolean communityDisplayEnabled {get; set;}
    
    // LCA-1176
    public List<Case> requests {get;set;}
    public Map<Id, Boolean> hasUnreadComment {get; private set;}
    luana_MyRequestHome requestsCtrl;
    public String viewCaseId {get; set;}
    public Boolean markIsPublished {get; set;}
    public Boolean showAdditionalExamMarkFields {get;set;}
    public String statusDisplay {get;set;}
    public Decimal examMark {get;set;}
    public Decimal nonExamMark {get;set;}
    
    public Map<Id, Boolean> hasSubjectCompletedMap {get; set;}
    
    public luana_MyEnrolmentViewController(){
        //test data
        //luana_CommUserUtil commUserUtil = new luana_CommUserUtil('005N0000002Nw25');
        
        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId()); 
        this.custCommConId = commUserUtil.custCommConId;       
        this.custCommAccId = commUserUtil.custCommAccId;
        this.custCommCountry = commUserUtil.custCommCountry; //LCA-748
        
        relatedMasterPOList = new List<Program_Offering_Related__c>();
        //LCA-802 enrolledMasterSPList = new List<LuanaSMS__Student_Program__c>();
        
        isValid = false;
        isCapstone = false;
        isMasterclassEnrolled = false;
        isEligibleForClassRegistration = false; //LCA-572
        isMasterclassReady = true; //LCA-639, change it to always true
        isSuppExamApproved = false; //LCA-829
        isSuppExamEnrolled = false; //LCA-829
        
        //LCA-571
        cancelAttendanceId = null;
        
        hasSubjectCompletedMap = new Map<Id, Boolean>();
        
        //LCA-887 no longer required conversion
        //LCA-642
        //vcStartTime = new Map<Id, String>();
        //vcEndTime = new Map<Id, String>();
        //LCA-887 end
        
        //LCA-511
        isExamBroadcasted = false;
        isWorkshopBroadcasted = false;
        
        //LCA-829
        isSuppExamBroadcasted = false;
        workshopAttendances = new List<LuanaSMS__Attendance2__c>();
        billingAdd = '';
        
        //LCA-1161
        Luana_Extension_Settings__c luanaExtCS = Luana_Extension_Settings__c.getValues('CommunityDisplayStatus');
        if(luanaExtCS.Value__c == 'True'){
            communityDisplayEnabled = true;
        }else{
            communityDisplayEnabled = false;
        }
        
        if(getStudentProgramIdFromURL() != null){
            try{
                //LCA-466, Phase 3. Add Exam_Location__c to query
                //LCA-5, add workshop location and day of week preference
                //LCA-829 include supp exam fields
                // LCA-1176 include the published formula field
                selectedSP = [Select Id, Name, Paid__c, Final_Module_Status__c, Workshop_Broadcasted__c, Supp_Exam_Special_Consideration_Approved__c, LuanaSMS__Status__c, Exam_Location__c, Exam_Location__r.Name, Opt_In__c, LuanaSMS__Course__r.Name, LuanaSMS__Program_Id__c, LuanaSMS__Program_Name__c,
                    LuanaSMS__Enrolment_Date__c, LuanaSMS__Contact_Student__c, LuanaSMS__Contact_Student__r.Account.FirstName, LuanaSMS__Contact_Student__r.Account.Preferred_Name__c,
                    Sup_Exam_Enrolled__c, Supp_Exam_Broadcasted__c, Supp_Exam_Paid__c, Supp_Exam_Charge__c,
                    LuanaSMS__Contact_Student__r.Account.LastName, Select_your_exam_location_preference__r.Name, LuanaSMS__Contact_Student__r.Account.IsPersonAccount, LuanaSMS__Contact_Student__r.Name,
                    Workshop_Location_Preference_1__c, Workshop_Location_Preference_1__r.Name, Day_of_the_Week_Preference_1__c, Workshop_Location_Preference_2__c, Workshop_Location_Preference_2__r.Name, Day_of_the_Week_Preference_2__c,
                    LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.IsCapstone__c, Line_of_service__c, Organisation_type__c, LuanaSMS__Course__r.LuanaSMS__Program_Offering__c, LuanaSMS__Course__c, 
                     Exam_Broadcasted__c, LuanaSMS__Course__r.Workshop_Location_Cut_Off_Date__c, Masterclass_Broadcasted__c, //LCA-867 LuanaSMS__Course__r.Ready_for_Masterclass_Enrolment__c,
                     LuanaSMS__Course__r.Supp_Exam_Enrolment_Start_Date__c, LuanaSMS__Course__r.Supp_Exam_Enrolment_End_Date__c, //
                    Result_Publish__c, Transcript_Status__c, // LCA-1176
                    (select id, LuanaSMS__Subject__c, LuanaSMS__Subject__r.Name, Completed__c, Score__c, Result__c, Merit__c from LuanaSMS__Student_Program_Subjects__r),                    
                    (select Name, LuanaSMS__End_time__c, LuanaSMS__Start_time__c, Record_Type_Name__c, LuanaSMS__Session__r.Course_Delivery_Location__r.Delivery_Location__r.Name, LuanaSMS__Student_Program__c, Exam_Number__c, LuanaSMS__Session__r.LuanaSMS__Venue__r.Name, 
                        LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress, LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingStreet, LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingCity, LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingState, LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingPostalCode, LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingCountry,
                        LuanaSMS__Session__r.LuanaSMS__Venue__r.IsPersonAccount, LuanaSMS__Attendance_Status__c,
                        LuanaSMS__Session__r.Name, LuanaSMS__Session__r.Day_of_Week__c, LuanaSMS__Session__r.LuanaSMS__Start_Time__c, LuanaSMS__Session__r.LuanaSMS__End_Time__c, LuanaSMS__Session__r.LuanaSMS__Room__r.Special_Instructions__c, LuanaSMS__Session__r.LuanaSMS__Room__r.Name, 
                        LuanaSMS__Session__r.Session_Start_Time_Venue__c, LuanaSMS__Session__r.Session_End_Time_Venue__c //LCA-916
                        from LuanaSMS__Attendances__r Where (Record_Type_Name__c = 'Exam' or Record_Type_Name__c = 'Workshop' or Record_Type_Name__c = 'Supplementary_Exam') order by LuanaSMS__Start_time__c, name asc nulls last) //LCA-511
                    from LuanaSMS__Student_Program__c Where Id =: getStudentProgramIdFromURL() and LuanaSMS__Contact_Student__c =: custCommConId];
                
                
                
                for(LuanaSMS__Student_Program_Subject__c sps: selectedSP.LuanaSMS__Student_Program_Subjects__r){
                    if(selectedSP.Result_Publish__c){
                        if(sps.Completed__c){
                            hasSubjectCompletedMap.put(sps.Id, true);
                        }else{
                            hasSubjectCompletedMap.put(sps.Id, false);
                        }
                    }else{
                        hasSubjectCompletedMap.put(sps.Id, false);
                    }
                }
                
                //LCA-51l
                
                isExamBroadcasted = selectedSP.Exam_Broadcasted__c;
                isWorkshopBroadcasted = selectedSP.Workshop_Broadcasted__c;
                if(selectedSP.Supp_Exam_Special_Consideration_Approved__c == 'Approved'){
                                                        
                    // LCA-1093, added condition for the button to not show after supp exam enrolment end date
                    if (selectedSP.LuanaSMS__Course__r.Supp_Exam_Enrolment_End_Date__c != NULL && selectedSP.LuanaSMS__Course__r.Supp_Exam_Enrolment_End_Date__c < system.today() || (selectedSP.LuanaSMS__Course__r.Supp_Exam_Enrolment_End_Date__c == NULL)) {
                        isSuppExamApproved = false;
                    } else {
                        isSuppExamApproved = true;
                    }                    
                }
                
                isSuppExamEnrolled = selectedSP.Sup_Exam_Enrolled__c;

                //LCA-829
                isSuppExamBroadcasted = selectedSP.Supp_Exam_Broadcasted__c;
                
                for(LuanaSMS__Attendance2__c att: selectedSP.LuanaSMS__Attendances__r){
                    
                    if(att.Record_Type_Name__c == 'Exam' && isExamBroadcasted){
                        attExamObj = att;
                    }
                    //LCA-829
                    if(att.Record_Type_Name__c== 'Supplementary_Exam' && isSuppExamBroadcasted){
                        attSuppExamObj = att;
                    }
                    if(att.Record_Type_Name__c == 'Workshop' && isWorkshopBroadcasted){
                        workshopAttendances.add(att);
                    }
                }
                
                List<LuanaSMS__Attendance2__c> atts = [select Name,  LuanaSMS__Session__r.LuanaSMS__Room__r.Special_Instructions__c from LuanaSMS__Attendance2__c Where Id = 'a1WN0000007sh6PMAQ'];

                mentorList = new Set<String>();
                
                for(Relationship__c r: [Select Id, Name, Primary_Relationship__c, Account__c, Account__r.Name, Member__c, Member__r.Name, Reciprocal_Relationship__c 
                    from Relationship__c 
                    Where (((Reciprocal_Relationship__c = 'Mentee' and Member__c =: custCommAccId)) or
                    ((Primary_Relationship__c = 'Mentee' and Account__c =: custCommAccId))) and Status__c = 'Current' //LCA-720
                ]){
                    if(r.Reciprocal_Relationship__c == 'Mentee' && r.Member__c == custCommAccId){
                        mentorList.add(r.Account__r.Name);
                    }else if(r.Primary_Relationship__c == 'Mentee' && r.Account__c == custCommAccId){
                        mentorList.add(r.Member__r.Name);
                    }
                }
                
                employmentHistories = [Select Id, Employer__c, Job_Title__c, Employee_Start_Date__c, Employee_End_Date__c, Employer__r.Name from Employment_History__c Where Primary_Employer__c = true and Member__c =: custCommAccId order by Employee_Start_Date__c desc];
                
                initAttendacesList();
                
                isMember = luana_NetworkUtil.isMemberCommunity();
                
                isValid = true;
                
                isEligibleForClassRegistration = (selectedSP.Paid__c ? true : false); //LCA-572
                
                //LCA-639
                isMasterclassReady = true; // Change to always true
                
                //Replace by LCA-642, Reused on LCA-887
                userTimezone = UserInfo.getTimeZone().getID(); //LCA-626, LCA-887
                
                //userTimezone = System.now().format('z', 'Australia/Sydney'); //LCA-887
                
                //LCA-887 no longer required conversion
                //LCA-642 start
                //for(LuanaSMS__Attendance2__c att: virtualClassSessions){
                //    if(att.LuanaSMS__Start_time__c != null){
                //        vcStartTime.put(att.Id, att.LuanaSMS__Start_time__c.format('dd/MM/yyyy h:mm:ss a', 'Australia/Sydney'));
                //    }else{
                //        vcStartTime.put(att.Id, '');
                //    }
                //    
                //    if(att.LuanaSMS__End_time__c != null){
                //        vcEndTime.put(att.Id, att.LuanaSMS__End_time__c.format('dd/MM/yyyy h:mm:ss a', 'Australia/Sydney'));
                //    }else{
                //        vcEndTime.put(att.Id, '');
                //    }
                //}
                //LCA-642 end
                //LCA-887 end
                
                //LCA-470 Start
                
                for(LuanaSMS__Student_Program__c enrolledSP: [Select Id, Name, Related_Student_Program__c, Related_Student_Program__r.LuanaSMS__Course__r.Name, LuanaSMS__Course__r.Name,
                                                                LuanaSMS__Status__c, LuanaSMS__Program_Commencement_Date__c,LuanaSMS__Enrolment_Date__c
                                                                from LuanaSMS__Student_Program__c Where LuanaSMS__Status__c != 'Cancelled' AND LuanaSMS__Status__c != 'Withdrawn/Cancelled' AND Related_Student_Program__c =: selectedSP.Id]){
                    //LCA-802 enrolledMasterSPList.add(enrolledSP);
                    isMasterclassEnrolled = true;
                    isMasterclassReady = true; //LCA-873
                }
                
                Id parentPOId = selectedSP.LuanaSMS__Course__r.LuanaSMS__Program_Offering__c;
                String masterclassCountry = this.custCommCountry; //discarded (this.custCommCountry == 'Overseas' ? 'Australia' : this.custCommCountry); //LCA-909
                for(Program_Offering_Related__c por: [Select Id, Name, Child_Program_Offering__c, Parent_Program_Offering__c, Child_Program_Offering__r.Name, 
                                                        Parent_Program_Offering__r.Name, Type__c, Child_Program_Offering__r.Detail__c
                                                        from Program_Offering_Related__c Where Parent_Program_Offering__c =: parentPOId and Child_Program_Offering__r.Default_Program_Offering_Product__c =: masterclassCountry and Child_Program_Offering__r.LuanaSMS__Allow_Online_Enrolment__c = true Order by Name asc]){ //LCA-748, LCA-867
                    relatedMasterPOList.add(por);
                }
                
                isCapstone = selectedSP.LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.IsCapstone__c;
            
                //LCA-1061 - start
                if(selectedSP.LuanaSMS__Course__r.Workshop_Location_Cut_Off_Date__c < System.today()){
                    displayWorkshopPreference = false;
                } else {
                    displayWorkshopPreference = true;
                }
                //LCA-1061 - end
                
                // LCA-1176 - start
                Case dummyCase = new Case();
                ApexPages.StandardController std = new ApexPages.StandardController(dummyCase); 
                requestsCtrl = new luana_MyRequestHome(std);

                requests = new List<Case>();

                for (Case c : requestsCtrl.getRequests()) {
                    if (c.Student_Program__c != null && c.Student_Program__c == selectedSP.Id) requests.add(c);                  
                    
                }
                                                  
                hasUnreadComment = requestsCtrl.hasUnreadComment;                               
                
                // toggles the display of the marks column and final module mark
                markIsPublished = false;
                statusDisplay = selectedSP.Transcript_Status__c;
                if (selectedSP.Result_Publish__c) markIsPublished = true;
                
                // checks for the presence of Student Assessment, show the additional fields containing exam and non exam mark if SA exist, taking ONLY the latest SA.
                showAdditionalExamMarkFields = false;
                examMark = null;
                nonExamMark = null;
                    
                List<AAS_Student_Assessment__c> saList = [SELECT Id, AAS_Student_Program_Subject__c, AAS_Total_Exam_Mark__c, Total_Non_Exam_Mark__c FROM AAS_Student_Assessment__c WHERE AAS_Course_Assessment__r.AAS_Final_Result_Release_Exam__c = true AND AAS_Student_Program_Subject__c IN :selectedSP.LuanaSMS__Student_Program_Subjects__r ORDER BY CreatedDate DESC LIMIT 1];
                if (!saList.isEmpty()) {
                    showAdditionalExamMarkFields = true;
                    examMark = saList[0].AAS_Total_Exam_Mark__c;
                    nonExamMark = saList[0].Total_Non_Exam_Mark__c;
                }
                // LCA-1176 - end
            } catch(Exception exp){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'You do not have the permission to view the record. Please try again later or contact our support if the problem persists. ' + exp.getStackTraceString() +' - ' + exp.getMessage()));
            }
                
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Invalid request. Please try again later or contact our support if the problem persists.'));
        }
    }
    
    // LCA-1176
    public PageReference doDeleteCommentReadStatus(){       
        requestsCtrl.viewCaseId = viewCaseId;
        PageReference viewReqPage = requestsCtrl.doDeleteCommentReadStatus();
        if (selectedSP != null) viewReqPage.getParameters().put('spid', selectedSP.Id);        
        return viewReqPage;
    }
    
    public String getStudentProgramIdFromURL(){ //LCA-874
        if(ApexPages.currentPage().getParameters().containsKey('spid')){
            return ApexPages.currentPage().getParameters().get('spid');
            
        }
        
        return null;
    }
    
    private void initAttendacesList(){
        //LCA-499
        virtualClassSessions = new List<LuanaSMS__Attendance2__c>();
        examSessions = new List<LuanaSMS__Attendance2__c>();
        for(LuanaSMS__Attendance2__c attendance : [Select Id, Name, CreatedDate, LuanaSMS__Attendance_Status__c, Adobe_Connect_Url__c, LuanaSMS__Start_time__c, LuanaSMS__End_time__c, LuanaSMS__Session__r.Name, LuanaSMS__Session__r.RecordType.DeveloperName from LuanaSMS__Attendance2__c where LuanaSMS__Contact_Student__c =: custCommConId and LuanaSMS__Student_Program__c =: getStudentProgramIdFromURL()]){
            if(attendance.LuanaSMS__Session__r.RecordType.DeveloperName == luana_SessionConstants.RECORDTYPE_SESSION_VIRTUAL){
                virtualClassSessions.add(attendance);
            } else {
                examSessions.add(attendance);
            }
        }
    }
    
    public PageReference doBack(){
        /*LCA-901
        if(luana_NetworkUtil.isNonMemberCommunity()){
            //LCA-386 LCA-391 changed back to use nonmember as we need to deploy for MICPA
            return Page.luana_NonMemberMyEnrolment;
        } else {
            return Page.luana_MemberMyEnrolment;
        }*/
        
        return Page.luana_MemberMyEnrolment; //LCA-901
    }
    
    //KH change for LCA-223
    public PageReference doEdit(){
        PageReference pRef = new PageReference(luana_NetworkUtil.getCommunityPath() + '/luana_MyEnrolmentEdit?spid=' + selectedSP.Id);
        return pRef;
    }
    
    //LCA-311, get template name
    public String getTemplateName(){
        return luana_NetworkUtil.getTemplateName();
        
        /*LCA-901
        if(luana_NetworkUtil.isMemberCommunity()){
            return 'luana_MemberTemplate';
        } else if(luana_NetworkUtil.isNonMemberCommunity()){
            return 'luana_NonMemberTemplate';
        } else if(luana_NetworkUtil.isInternal()){
            return 'luana_InternalTemplate';
        }
      
        return '';
        */
    }
    
    //LCA-499
    public PageReference doNewVirtualClass(){
        
        PageReference pageRef = Page.luana_MyVirtualClassNew;
        pageRef.getParameters().put('spid', getStudentProgramIdFromURL());
        
        return pageRef;
    }
    
    //LCA-499
    public PageReference doEditVirtualClass(){
        
        return null;
    }
    
    //Do Master class
    public PageReference doNewMasterClass(){
        PageReference pageRef = Page.luana_EnrolmentWizardLanding;
        pageRef.getParameters().put('parentspid', getStudentProgramIdFromURL());
        pageRef.getParameters().put('poid', selectedMasterPOId);
        pageRef.getParameters().put('producttype', 'masterclass');
        return pageRef;
    }
    
    //LCA-571
    public PageReference doCancelVirtualClass(){
        if(cancelAttendanceId != null && cancelAttendanceId != ''){
            LuanaSMS__Attendance2__c oldAttendance = new LuanaSMS__Attendance2__c(Id = cancelAttendanceId);
            
            try{
                delete oldAttendance;
                initAttendacesList();
                
                PageReference pageRef = Page.luana_MyEnrolmentView;
                pageRef.getParameters().put('spid', selectedSP.Id);
                pageRef.setAnchor('virtualclass');
                pageRef.setRedirect(true);
                
                return pageRef;
                
            }catch(Exception exp){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Error cancelling the registration: ' + exp.getMessage()));
            }
        }
        
        return null;
    }
    
    //LCA-569
    public PageReference adobeSessionAccess(){
        try{
            String cookie;
            luana_AdobeConnectUtil acUtil = new luana_AdobeConnectUtil();
            cookie = acUtil.getSessionCookie();
            if(acUtil.doExtLogin(custCommConId, cookie)){
                
                String adobeUrl;
                String attName;
                for(LuanaSMS__Attendance2__c attendance : virtualClassSessions){
                    attName = attendance.Name;
                    if(attendance.Id == cancelAttendanceId){
                        adobeUrl = attendance.Adobe_Connect_Url__c;
                    }
                }
                if(adobeUrl != null){
                    PageReference pageRef = new PageReference(adobeUrl + '?session=' + cookie);
                    return pageRef;
                }else{
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Error connecting to adobe. Please contact support: adobe connection url is invalid (' + attName + ')'));
                    return null;        
                }
            }else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Error connecting to adobe. Please contact support: adobe login fail (' + custCommAccId + ')'));
                return null; 
            }
            
        }Catch(Exception exp){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Error connecting to adobe, Please contact support: ' + exp.getMessage()));
            return null;
        }
    }
    
    //LCA-571
    public PageReference doTransferVirtualClass(){
        
        PageReference pageRef = Page.luana_MyVirtualClassNew;
        pageRef.getParameters().put('spid', getStudentProgramIdFromURL());
        pageRef.getParameters().put('cancelattendanceid', cancelAttendanceId);
        
        return pageRef;
    }
    
    //LCA-829
    public PageReference doSuppExamEnrolment(){
    
        PageReference pageRef = Page.luana_SupplementaryWizardDetail;
        pageRef.getParameters().put('spid', getStudentProgramIdFromURL());
        
        return pageRef;
    }
}