/*
    Developer: WDCi (Lean)
    Development Date: 13/04/2016
    Task #: luana_MyNewEducationRecord for enrolment wizard
    
    Change History
    LCA-555 WDCi - KH: 29-06-2016 include edit function
*/

public without sharing class luana_MyNewEducationRecordController {
    
    public Id custCommConId {public get; private set;}
    public Id custCommAccId {public get; private set;}
    
    public Education_Record__c educationRecordObj {get; set;}
    
    public String searchUniProOrg {get; set;}
    public String selectedUniProOrg {get; set;}
    public String selectedUniProOrgName {get; set;}
    
    public Boolean isUpdate;
    
    public luana_MyNewEducationRecordController(){
        
        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId());
        // /*
        this.custCommConId = commUserUtil.custCommConId;
        this.custCommAccId = commUserUtil.custCommAccId;
        // */
        
         /*
        this.custCommConId = '003p000000431MN';
        this.custCommAccId = '001p0000004OMw7';
         */
        
        //LCA-555 check for insert or update
        if(getEduRecordIdFromURL() == null){
            isUpdate = false;
            educationRecordObj = new Education_Record__c();
        }else{
            isUpdate = true;
            educationRecordObj = [SELECT Id, Name, Specialist_hours_code__c, Community_User_Entry__c, Date_Commenced__c, Event_Course_name__c, Qualifying_hours__c, Qualifying_hours_type__c, 
                                    CAHDP__c, Date_completed__c, Country__c, State__c, University_professional_organisation__c, University_professional_organisation__r.Name, 
                                    Enter_university_instituition__c, Verified__c, Education_Record__c.Bridging__c, Education_Record__c.Training_and_development__c, 
                                    Education_Record__c.Degree_Country__c, Education_Record__c.Degree__c, Education_Record__c.Degree_Other__c, Professional_Competency__c, Type_of_Activity__c 
                                    FROM Education_Record__c where Id =: getEduRecordIdFromURL() limit 1];
                                    
            if(educationRecordObj.University_professional_organisation__r.Name != null){                                               
                searchUniProOrg = educationRecordObj.University_professional_organisation__r.Name;
            }else{
                searchUniProOrg = 'lookup for account';
            }
        }
    }
    
    //LCA-555
    private String getEduRecordIdFromURL(){
        if(ApexPages.currentPage().getParameters().containsKey('id')){
            return ApexPages.currentPage().getParameters().get('id');
            
        }
        return null;
    }
    
    public PageReference doSave(){    
        
        if(!isUpdate){//LCA-555
            educationRecordObj.Contact_Student__c = custCommConId;
        }
        educationRecordObj.Community_User_Entry__c = true;
        
        if(educationRecordObj.Degree_Country__c != 'Other'){
            educationRecordObj.Degree_Other__c = '';
        }
        
        if(selectedUniProOrg.trim() != null && selectedUniProOrg.trim().length() > 0){
            educationRecordObj.University_professional_organisation__c = selectedUniProOrg;
            
            if(educationRecordObj.Enter_university_instituition__c == selectedUniProOrgName){
                educationRecordObj.Enter_university_instituition__c = selectedUniProOrgName;
            }
        }
        
        try{
            upsert educationRecordObj id;
        } catch (Exception exp){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Error saving your education records. Please try again later or contact our support if the problem persists. Error: ' + exp.getMessage()));
            return null;
        }
        
        PageReference pageRef = Page.luana_MyEducationRecordsSearch;
        return pageRef;
    }
    
    public PageReference doCancel(){
        return Page.luana_MyEducationRecordsSearch;
    }
    
    // JS Remoting action called when searching for a University professional organisation
    @RemoteAction
    public static List<Account> searchUniProfessionalOrg(String searchUniProOrg) {
        List<Account> accounts = new List<Account>();
        accounts = Database.query('Select Id, Name from Account where name like \'%' + String.escapeSingleQuotes(searchUniProOrg) + '%\' and RecordType.Name = \'Business Account\'');
        return accounts;
    }
    
    //LCA-311, get template name
    public String getTemplateName(){
        return luana_NetworkUtil.getTemplateName();
        
        /*LCA-901
        if(luana_NetworkUtil.isMemberCommunity()){
            return 'luana_MemberTemplate';
        } else if(luana_NetworkUtil.isNonMemberCommunity()){
            return 'luana_NonMemberTemplate';
        } else if(luana_NetworkUtil.isInternal()){
            return 'luana_InternalTemplate';
        }
      
        return '';
        */
    }
}