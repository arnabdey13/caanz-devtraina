/* 
    Developer: WDCi (LKoh)
    Development Date: 09/07/2019
    Task #: ATAEC-14/15/15/53/16/17/18 - Module Enrolment Form Changes
    
    Change History
*/
public with sharing class wdci_EnrolmentPreValidationController {
        
    public Boolean bothAddressTheSame {get; set;}
    public String spsId {public get; private set;}
    public LuanaSMS__Student_Program_Subject__c currentSPS {get; set;}

    public Boolean showCitizenRow {get; set;}
    public Boolean showTemporaryVisaRow {get; set;}
    public Boolean showTemporaryVisaNationalityRow {get; set;}
    public Boolean showOffshoreRow {get; set;}

    private Map<String, ff_RichTextStore__c> richTextStoreMap;
    private Map<String, Integer> residencyStatusMap;
    String invalidVisaMsg;    
    static String residenceFieldErrorMsg = 'Please make sure all the Citizenship, Residence and Visa Status fields are filled';
    static String addressFieldErrorMsg = 'Please make sure all the Residential Address fields are filled';

    public void validatePage() {
    }

    public wdci_EnrolmentPreValidationController() {
        system.debug('Entering wdci_EnrolmentPreValidationController');
        this.bothAddressTheSame = false;
        this.showCitizenRow = true;
        this.showTemporaryVisaRow = false;
        this.showTemporaryVisaNationalityRow = false;
        this.showOffshoreRow = false;
        if (this.spsId == null) {
            this.spsId = Apexpages.currentPage().getParameters().get('spsId');
        }        
        system.debug('spsId: ' +this.spsId);
        
        this.residencyStatusMap = generateResidencyStatusMap();

        this.currentSPS = [SELECT Id,
                           LuanaSMS__Subject__r.Id,
                           LuanaSMS__Contact_Student__c,
                           LuanaSMS__Contact_Student__r.PIP_Citizen_or_Permanent_Resident__c,
                           LuanaSMS__Contact_Student__r.PIP_Temporary_Visa_Holder__c,
                           LuanaSMS__Contact_Student__r.PIP_Student_Nationality__c,                           
                           LuanaSMS__Contact_Student__r.OtherCountryCode,
                           LuanaSMS__Contact_Student__r.OtherStreet,
                           LuanaSMS__Contact_Student__r.OtherPostalCode,
                           LuanaSMS__Contact_Student__r.OtherCity,
                           LuanaSMS__Contact_Student__r.OtherStateCode,
                           LuanaSMS__Contact_Student__r.AccountId,
                           LuanaSMS__Contact_Student__r.Account.IsPersonAccount,
                           LuanaSMS__Contact_Student__r.Account.Current_citizen_residency_status__c,
                           LuanaSMS__Contact_Student__r.Account.Mailing_and_Residential_is_the_same__c
                           FROM LuanaSMS__Student_Program_Subject__c WHERE Id = :this.spsId];

        richTextStoreMap = new Map<String, ff_RichTextStore__c>();
        for (ff_RichTextStore__c richTextStore : [SELECT Id, Name__c, Display_Content__c FROM ff_RichTextStore__c]) {
            richTextStoreMap.put(richTextStore.Name__c, richTextStore);
        }

        invalidVisaMsg = this.richTextStoreMap.get('ProvApp.Stage2.InvalidVisa').Display_Content__c;
        system.debug('invalidVisaMsg: ' +invalidVisaMsg);

        if (checkResidencyStatus() == 3) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, invalidVisaMsg));
        }
        residencyStatusChanged();
        system.debug('Exiting wdci_EnrolmentPreValidationController');
    }

    public String getTemplateName() {
        return luana_NetworkUtil.getTemplateName();
    }

    public String getInvalidVisaText() {
        return this.richTextStoreMap.get('ProvApp.Stage2.InvalidVisa').Display_Content__c;
    }

    // Reroute to the original URL that the user should be heading towards
    public PageReference doNext() {
        system.debug('Entering doNext: ' +this.currentSPS);
        try {            
            String enrolURL = luana_NetworkUtil.getCommunityPath() + '/luana_EnrolmentWizardLanding?producttype=' + EncodingUtil.urlEncode('CA Module', 'UTF-8');
            PageReference redirectURL = new PageReference(enrolURL + '&linkedsubjectid=' + this.currentSPS.LuanaSMS__Subject__r.Id);
            if (!validateResidencyStatus()) {
                if (checkResidencyStatus() == 3) {
                    updateContact();
                    updateAccount();
                }
                return reloadPage();
            } else {
                updateContact();
                updateAccount();
                return redirectURL;                
            }
        } catch (Exception e) {
            system.debug('Exception during doNext: ' +e.getMessage() + ' : ' +e.getStackTraceString());
            String unknownExceptionMessage = 'There was a problem when submitting the form, please contact the administrator';                        
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, unknownExceptionMessage));
        }
        return reloadPage();
    }

    public void updateContact() {
        // Update the Contact fields
        Contact contactToUpdate = new Contact();
        contactToUpdate.Id = this.currentSPS.LuanaSMS__Contact_Student__c;
        contactToUpdate = updateContactFields(contactToUpdate);
        update contactToUpdate;
    }

    public void updateAccount() {
        // Update the Account field
        Account accountToUpdate = new Account();
        accountToUpdate.Id = this.currentSPS.LuanaSMS__Contact_Student__r.AccountId;
        accountToUpdate.Current_citizen_residency_status__c = this.currentSPS.LuanaSMS__Contact_Student__r.Account.Current_citizen_residency_status__c;
        accountToUpdate.Mailing_and_Residential_is_the_same__c = this.currentSPS.LuanaSMS__Contact_Student__r.Account.Mailing_and_Residential_is_the_same__c;
        update accountToUpdate;
    }

    private Contact updateContactFields(Contact contactToUpdate) {
        system.debug('Entering updateContactFields: ' +contactToUpdate);
        system.debug('currentSPS: ' +this.currentSPS);
        if (checkResidencyStatus() == 1) {
            contactToUpdate.PIP_Citizen_or_Permanent_Resident__c = this.currentSPS.LuanaSMS__Contact_Student__r.PIP_Citizen_or_Permanent_Resident__c;
            contactToUpdate.PIP_Temporary_Visa_Holder__c = null;
            contactToUpdate.PIP_Student_Nationality__c = null;
        } else if (checkResidencyStatus() == 2) {
            contactToUpdate.PIP_Citizen_or_Permanent_Resident__c = null;
            contactToUpdate.PIP_Temporary_Visa_Holder__c = this.currentSPS.LuanaSMS__Contact_Student__r.PIP_Temporary_Visa_Holder__c;
            contactToUpdate.PIP_Student_Nationality__c = this.currentSPS.LuanaSMS__Contact_Student__r.PIP_Student_Nationality__c;
        } else if (checkResidencyStatus() == 4 || checkResidencyStatus() == 5) {
            contactToUpdate.PIP_Citizen_or_Permanent_Resident__c = null;
            contactToUpdate.PIP_Temporary_Visa_Holder__c = null;
            contactToUpdate.PIP_Student_Nationality__c = this.currentSPS.LuanaSMS__Contact_Student__r.PIP_Student_Nationality__c;
        } else {
            contactToUpdate.PIP_Citizen_or_Permanent_Resident__c = null;
            contactToUpdate.PIP_Temporary_Visa_Holder__c = null;
            contactToUpdate.PIP_Student_Nationality__c = null;
        }

        contactToUpdate.OtherCountryCode = this.currentSPS.LuanaSMS__Contact_Student__r.OtherCountryCode;
        contactToUpdate.OtherStreet = this.currentSPS.LuanaSMS__Contact_Student__r.OtherStreet;
        contactToUpdate.OtherPostalCode = this.currentSPS.LuanaSMS__Contact_Student__r.OtherPostalCode;
        contactToUpdate.OtherCity = this.currentSPS.LuanaSMS__Contact_Student__r.OtherCity;
        contactToUpdate.OtherStateCode = this.currentSPS.LuanaSMS__Contact_Student__r.OtherStateCode;
        
        system.debug('Exiting updateContactFields: ' +contactToUpdate);
        return contactToUpdate;
    }

    public Boolean validateResidencyStatus() {
        system.debug('Entering validateResidencyStatus: ' +this.currentSPS);
        if (checkResidencyStatus() == 3) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, invalidVisaMsg));
            return false;
        } else if (String.isBlank(this.currentSPS.LuanaSMS__Contact_Student__r.Account.Current_citizen_residency_status__c) ||
                   (checkResidencyStatus() == 1 && 
                    String.isBlank(this.currentSPS.LuanaSMS__Contact_Student__r.PIP_Citizen_or_Permanent_Resident__c)) ||
                   (checkResidencyStatus() == 2 && 
                   (String.isBlank(this.currentSPS.LuanaSMS__Contact_Student__r.PIP_Temporary_Visa_Holder__c) || String.isBlank(this.currentSPS.LuanaSMS__Contact_Student__r.PIP_Student_Nationality__c))) ||
                   ((checkResidencyStatus() == 4 || checkResidencyStatus() == 5) &&
                    String.isBlank(this.currentSPS.LuanaSMS__Contact_Student__r.PIP_Student_Nationality__c))) {

            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, residenceFieldErrorMsg));
            return false;
        }

        if (String.isBlank(this.currentSPS.LuanaSMS__Contact_Student__r.OtherCountryCode) ||
            String.isBlank(this.currentSPS.LuanaSMS__Contact_Student__r.OtherStreet) || 
            String.isBlank(this.currentSPS.LuanaSMS__Contact_Student__r.OtherPostalCode) ||
            String.isBlank(this.currentSPS.LuanaSMS__Contact_Student__r.OtherCity)) {

            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, addressFieldErrorMsg));
            return false;
        }
        return true;
    }    

    public void residencyStatusChanged() {
        system.debug('Entering residencyStatusChanged');
        if (checkResidencyStatus() == 1) {
            showCitizenRow = true;
            showTemporaryVisaRow = false;
            showTemporaryVisaNationalityRow = false;
        } else if (checkResidencyStatus() == 2) {
            showCitizenRow = false;
            showTemporaryVisaRow = true;
            showTemporaryVisaNationalityRow = true;
        } else if (checkResidencyStatus() == 4) {
            showCitizenRow = false;
            showTemporaryVisaRow = false;
            showTemporaryVisaNationalityRow = true;
        } else if (checkResidencyStatus() == 5) {
            showCitizenRow = false;
            showTemporaryVisaRow = false;
            showTemporaryVisaNationalityRow = true;
        } else {
            showCitizenRow = false;
            showTemporaryVisaRow = false;
            showTemporaryVisaNationalityRow = false;
        }
        system.debug('Exiting residencyStatusChanged');
    }

    public PageReference reloadPage() {
        PageReference tempPage = ApexPages.currentPage();            
        tempPage.setRedirect(false);
        return tempPage;
    }

    // Loads the Residency Status value that should be checked and the mode that correspond to them from the Custom Metadata
    // The mode determines how the 
    private Map<String, Integer> generateResidencyStatusMap() {
        Map<String, Integer> newResidencyStatusMap = new Map<String, Integer>();
        for (Residency_Status__mdt mdtRecord : [SELECT Id, DeveloperName, Mode__c, Residency_Status_value__c FROM Residency_Status__mdt WHERE DeveloperName LIKE 'wdci_EnrolmentPreValidationController%']) {
            newResidencyStatusMap.put(mdtRecord.Residency_Status_value__c, Integer.valueOf(mdtRecord.Mode__c));
        }
        return newResidencyStatusMap;
    }

    private Integer checkResidencyStatus() {
        system.debug('Entering checkResidencyStatus: ' +this.currentSPS+ ' : ' +this.residencyStatusMap);
        /** Reference as of 09/07/2019
        'Australia/New Zealand Citizen or Permanent Resident'   mode 1
        'Australian or New Zealand Temporary Visa Holder'       mode 2
        'Australian Student Visa'                               mode 3
        'New Zealand Student Visa'                              mode 4
        'Studying outside of Australia/New Zealand'             mode 5
        */
        if (this.residencyStatusMap.containsKey(this.currentSPS.LuanaSMS__Contact_Student__r.Account.Current_citizen_residency_status__c)) {
            return this.residencyStatusMap.get(this.currentSPS.LuanaSMS__Contact_Student__r.Account.Current_citizen_residency_status__c);
        }
        return 0;
    }
}