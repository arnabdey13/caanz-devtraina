/*
    Developer: WDCi (Carter)
    Date: 07/June/2016
    Task #: Mock Http Responser Generator for luana_VCImportWizardController_Test
    
    Change History:
    01-July-2016 WDCi - KH: Include 'expanded-scos' for mock response
*/

@isTest(seeAllData=false)
global class luana_AT_VCImportWizardMockHttp_Test implements HttpCalloutMock {
    
    
    public static String commoInfoResp = '<?xml version="1.0" encoding="utf-8"?><results><status code="ok"/><OWASP_CSRF_TOKEN><token>c19fa09b94126b7ea7b93f362cc1f16aa72d1a3a2f503b7e9f789f215cfff75e</token></OWASP_CSRF_TOKEN><common locale="en" time-zone-id="255" time-zone-java-id="Australia/Sydney"><cookie>breezhupgyv7r4ibs4tbs</cookie><date>2016-06-07T16:02:39.297+10:00</date><host>https://connect-staging.charteredaccountantsanz.com</host><local-host>DC1-CCH-STAG-01</local-host><admin-host>connect-staging.charteredaccountantsanz.com</admin-host><url>/api/xml?action=common-info</url><version>9.5.2</version><account account-id="7"/><user user-id="4141721" type="user"><name>Fake Guy</name><login>tooi@wdcigroup.net</login></user><user-agent>Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36</user-agent><mobile-app-package>air.com.adobe.connectpro</mobile-app-package></common><reg-user><is-reg-user>false</is-reg-user></reg-user><expanded-scos><date-begin>2016-06-07T16:02:39.297+10:00</date-begin><date-end>2016-06-07T16:02:39.297+10:00</date-end></expanded-scos></results>';
    public static String loggingInResp = '<?xml version="1.0" encoding="utf-8"?><results><status code="ok"/><OWASP_CSRF_TOKEN><token>bla</token></OWASP_CSRF_TOKEN></results>';
    public static String queryMeeting = '<?xml version="1.0" encoding="utf-8"?><results><status code="ok"/><my-meetings><meeting sco-id="3960054" type="meeting" icon="meeting" permission-id="host" active-participants="0"><name>CAP116_VC_T1_01 61349526</name><domain-name>connect-staging.charteredaccountantsanz.com</domain-name><url-path>/r9o38r5a6if/</url-path><date-begin>2016-03-17T18:00:00.000+11:00</date-begin><date-end>2016-03-17T19:30:00.000+11:00</date-end><expired>true</expired><duration>01:30:00.000</duration></meeting><meeting sco-id="3960068" type="meeting" icon="meeting" permission-id="host" active-participants="0"><name>CAP116_VC_T1_02 61349530</name><domain-name>connect-staging.charteredaccountantsanz.com</domain-name><url-path>/r6ca1p46nqj/</url-path><date-begin>2016-03-22T12:30:00.000+11:00</date-begin><date-end>2016-03-22T14:00:00.000+11:00</date-end><expired>true</expired><duration>01:30:00.000</duration></meeting><meeting sco-id="3960082" type="meeting" icon="meeting" permission-id="host" active-participants="0"><name>CAP116_VC_T1_03 61349534</name><domain-name>connect-staging.charteredaccountantsanz.com</domain-name><url-path>/r30gun33515/</url-path><date-begin>2016-03-22T18:00:00.000+11:00</date-begin><date-end>2016-03-22T19:30:00.000+11:00</date-end><expired>true</expired><duration>01:30:00.000</duration></meeting><meeting sco-id="3964435" type="meeting" icon="meeting" permission-id="host" active-participants="0"><name>CAP116_VC_T2_01 61351085</name><domain-name>connect-staging.charteredaccountantsanz.com</domain-name><url-path>/r2ie1kc4l1m/</url-path><date-begin>2016-04-14T18:00:00.000+10:00</date-begin><date-end>2016-04-14T19:30:00.000+10:00</date-end><expired>true</expired><duration>01:30:00.000</duration></meeting><meeting sco-id="3964455" type="meeting" icon="meeting" permission-id="host" active-participants="0"><name>CAP116_VC_T2_02 61351108</name><domain-name>connect-staging.charteredaccountantsanz.com</domain-name><url-path>/r433sjt1u12/</url-path><date-begin>2016-04-19T12:30:00.000+10:00</date-begin><date-end>2016-04-19T14:00:00.000+10:00</date-end><expired>true</expired><duration>01:30:00.000</duration></meeting><meeting sco-id="3964477" type="meeting" icon="meeting" permission-id="host" active-participants="0"><name>CAP116_VC_T2_03 61351139</name><domain-name>connect-staging.charteredaccountantsanz.com</domain-name><url-path>/r1dzaoritdg/</url-path><date-begin>2016-04-19T18:00:00.000+10:00</date-begin><date-end>2016-04-19T19:30:00.000+10:00</date-end><expired>true</expired><duration>01:30:00.000</duration></meeting><meeting sco-id="3964491" type="meeting" icon="meeting" permission-id="host" active-participants="0"><name>CAP116_VC_T3_01 61351200</name><domain-name>connect-staging.charteredaccountantsanz.com</domain-name><url-path>/r6x6xx9431k/</url-path><date-begin>2016-05-12T18:00:00.000+10:00</date-begin><date-end>2016-05-12T19:30:00.000+10:00</date-end><expired>true</expired><duration>01:30:00.000</duration></meeting><meeting sco-id="3964519" type="meeting" icon="meeting" permission-id="view" active-participants="0"><name>CAP116_VC_T3_02 61351239</name><domain-name>connect-staging.charteredaccountantsanz.com</domain-name><url-path>/r94le3plu1r/</url-path><date-begin>2016-05-17T12:30:00.000+10:00</date-begin><date-end>2016-05-17T14:00:00.000+10:00</date-end><expired>true</expired><duration>01:30:00.000</duration></meeting><meeting sco-id="3964529" type="meeting" icon="meeting" permission-id="view" active-participants="0"><name>CAP116_VC_T3_03 61351242</name><domain-name>connect-staging.charteredaccountantsanz.com</domain-name><url-path>/r5hfug6q2bh/</url-path><date-begin>2016-05-17T18:00:00.000+10:00</date-begin><date-end>2016-05-17T19:30:00.000+10:00</date-end><expired>true</expired><duration>01:30:00.000</duration></meeting></my-meetings><expanded-scos><date-begin>2016-06-07T16:02:39.297+10:00</date-begin><date-end>2016-06-07T16:02:39.297+10:00</date-end></expanded-scos></results>';
    public static String noFound = '<?xml version="1.0" encoding="utf-8"?><results><status code="no-data"/><expanded-scos><expanded-sco><date-begin>2016-06-07T16:02:39.297+10:00</date-begin><date-end>2016-06-07T16:02:39.297+10:00</date-end></expanded-sco></expanded-scos></results>';
    
    global HTTPResponse respond(HTTPRequest req){
        
        HttpResponse res = new HttpResponse();
        //If common-info:
        if(req.getEndpoint() == 'https://connect-staging.charteredaccountantsanz.com/api/xml?action=common-info&session=breezhupgyv7r4ibs4tbs'){
            res.setBody(commoInfoResp);
        }       
        //If logging in:
        else if (req.getEndpoint() == 'https://connect-staging.charteredaccountantsanz.com/api/xml?action=login&session=breezhupgyv7r4ibs4tbs&login=test%40correct.com&password=abcde'){
            res.setBody(loggingInResp);
        }  
        // Terry
        //If query meetings
        else if (req.getEndpoint() == 'https://connect-staging.charteredaccountantsanz.com/api/xml?action=report-my-meetings&session=breezhupgyv7r4ibs4tbs&filter-like-name=test&filter-rows=50&sort-name=asc'){
            res.setBody(queryMeeting);
        } 
        
        
        else {
            
            //If nothing else is found
            res.setBody(noFound);
        }
        
        
        
        
        return res;
        
    }
}