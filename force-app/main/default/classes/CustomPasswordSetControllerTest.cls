@IsTest(seeAllData=false)
private class CustomPasswordSetControllerTest {

    private static String GUID = '093007cd-505c-49ae-1457-183caf86504b';

    testMethod static void testSetGoodPassword() {
        Id accountId = [select Id from Account where Registration_Temporary_Key__c = :GUID].Id;
        System.debug(accountId);

        PageReference current = Page.CustomPasswordSetPage;
        current.getParameters().put('k', GUID);
        Test.setCurrentPage(current);

        CustomPasswordSetController controller = new CustomPasswordSetController();

        System.assertEquals(true, controller.validLoad, 'load succeeds when guid is present');

        controller.password1 = 'l;jklkjoiuoiuo12340978q;lkjl;';
        controller.password2 = controller.password1;

        PageReference navigationResult = controller.setPassword();

        System.assertEquals(null, controller.message, 'no complaints from controller when password is complex');

    }


    testMethod static void testSetBadPassword() {
        Id accountId = [select Id from Account where Registration_Temporary_Key__c = :GUID].Id;
        System.debug(accountId);

        PageReference current = Page.CustomPasswordSetPage;
        current.getParameters().put('k', GUID);
        Test.setCurrentPage(current);

        CustomPasswordSetController controller = new CustomPasswordSetController();

        controller.password1 = 'a';
        controller.password2 = 'a';

        PageReference navigationResult = controller.setPassword();

        System.assertNotEquals(null, controller.message, 'SFDC blocks short passwords');

    }

    testMethod static void testSetMismatchedPasswords() {
        Id accountId = [select Id from Account where Registration_Temporary_Key__c = :GUID].Id;
        System.debug(accountId);

        PageReference current = Page.CustomPasswordSetPage;
        current.getParameters().put('k', GUID);
        Test.setCurrentPage(current);

        CustomPasswordSetController controller = new CustomPasswordSetController();

        controller.password1 = 'l;jklkjoiuoiuo12340978q;lkjl;';
        controller.password2 = 'foo';

        PageReference navigationResult = controller.setPassword();

        System.assertNotEquals(null, controller.message, 'controller blocks when passwords do not match');

    }

    testMethod static void testBadLoad() {
        Id accountId = [select Id from Account where Registration_Temporary_Key__c = :GUID].Id;
        System.debug(accountId);

        PageReference current = Page.CustomPasswordSetPage;
        current.getParameters().put('k', 'foo');
        Test.setCurrentPage(current);

        CustomPasswordSetController controller = new CustomPasswordSetController();

        System.assertEquals(false, controller.validLoad, 'load fails when no match for guid');
    }

    @testSetup static void createTestData() {
        // copied from RegisterOrLoginService
        Id nonMemberRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non Member').getRecordTypeId();
        Account newMember = new Account(RecordTypeId = nonMemberRecordTypeId,
                Salutation = 'Sir',
                FirstName = 'Steve',
                LastName = 'Buik',
                Member_Of__c = 'NZICA',
                PersonEmail = 's@t'+DateTime.now().getTime()+'.com',
                PersonMailingCountry = 'New Zealand',
                OwnerId = UserInfo.getUserId(),
                Registration_Temporary_Key__c = GUID);
        insert newMember;
    }

}