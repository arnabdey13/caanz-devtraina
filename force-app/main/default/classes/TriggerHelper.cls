public with sharing class TriggerHelper {
	// This class will prevent recursive trigger.
	private static boolean TriggerActionComplete = false;
	
	public static boolean isTriggerActionComplete(){
		return TriggerActionComplete;
	}
	
	public static void comepleteTriggerAction(){
		TriggerActionComplete = true;
	}
	
	// Currently only required for testing purposes
	public static void startNewTransaction(){
		TriggerActionComplete = false;
	}
}