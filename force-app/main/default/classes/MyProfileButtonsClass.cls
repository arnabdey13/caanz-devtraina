public class MyProfileButtonsClass {
    private static final Set<String> CLIENT_FIELDS = new Set<String>
    {'Id','FirstName','LastName','PersonEmail','Name','Preferred_Contact_Number__c',
        'Customer_ID__c','Member_ID__c','Salutation','Middle_Name__c','PersonMobilePhone','PersonHomePhone','Preferred_Name__c','PersonOtherStreet','PersonOtherCity','PersonOtherState',
        'PersonOtherStateCode','PersonOtherPostalCode','PersonOtherCountryCode','PersonMailingStreet','PersonMailingCity','PersonMailingStateCode','PersonMailingPostalCode','Mailing_and_Residential_is_the_same__c',
        'Newsletter_Recipient__c','Replacement_Certificate_request__c','PersonOtherPhone','PersonMailingCountryCode','PersonBirthdate','Gender__c','PersonHasOptedOutOfEmail',
        'PersonHasOptedOutOfMail__pc','PersonHasOptedOutOfSMS__pc','PersonDoNotCall','PersonOtherCountry','PersonMailingCountry','PersonMailingState', 'Designation__c', 'Membership_Approval_Date__c', 'Mailing_Company_Name__c', 'Consent_To_Fellowship__pc', 'Financial_Category__c', 'Membership_Class__c'};
            
            
            
            @AuraEnabled
            public static Map<String, Object> load() {
                //User userRec = [SELECT AccountId FROM User WHERE ID =: UserInfo.getUserId()] ;
                String accId = EditorPageUtils.getUsersAccountId() ; /*'001N000000cpg5QIAQ'*/
                Map<String, Object> properties = new Map<String, Object>();
                Account a = [SELECT Id, FirstName, LastName, PersonEmail,Name,Preferred_Contact_Number__c,
                             Customer_ID__c, Member_ID__c, Salutation, Middle_Name__c, PersonMobilePhone, PersonHomePhone, Preferred_Name__c, PersonOtherStreet, PersonOtherCity, PersonOtherState, 
                             PersonOtherStateCode, PersonOtherPostalCode, PersonOtherCountryCode, PersonMailingStreet,PersonMailingCity, PersonMailingStateCode, PersonMailingPostalCode, Mailing_and_Residential_is_the_same__c,
                             Newsletter_Recipient__c, Replacement_Certificate_request__c,PersonOtherPhone, PersonMailingCountryCode, PersonBirthdate, Gender__c, PersonHasOptedOutOfEmail,
                             PersonHasOptedOutOfMail__pc, PersonHasOptedOutOfSMS__pc, PersonDoNotCall, PersonOtherCountry, PersonMailingCountry, PersonMailingState, Designation__c, Membership_Approval_Date__c, Mailing_Company_Name__c, Consent_To_Fellowship__pc, Financial_Category__c, Membership_Class__c  
                             FROM Account WHERE Id =: accId
                            ];
                
                // client sees object as generic map i.e. JSON so translate the sobject into a map so that tests can read/write
                Map<Object, Object> accountAsJSON = new Map<Object, Object>();
                for (String f : CLIENT_FIELDS) {
                    accountAsJSON.put(f, a.get(f));
                }
                
                System.debug('***a***: '+a);
                properties.put('RECORD', accountAsJSON);
                properties.put('prop.countriesAndStates',EditorPageUtils.getCountriesAndStates());
                return properties ;
            }
    
    @AuraEnabled
    public static Map<String, Object> save(Map<String, Object> properties) {
        Map<String, Object> returnProperties = new Map<String, Object>();
        Map<Object, Object> fromClient = (Map<Object, Object>)properties.get('RECORD');
        System.debug('fromClient: '+fromClient.get('Id'));
        System.debug('dirty: '+properties.get('DIRTY'));
        
        // load the id from the user/session to avoid client tampering
        
        Account toSave = new Account();
        toSave.Id = (Id)fromClient.get('Id');
        Map<Object, Object> dirty = (Map<Object, Object>) properties.get('DIRTY');
        if (dirty != NULL) {
            for (Object f : dirty.keySet()) {
                System.debug('***f***: '+f);
                System.debug('***f***: '+fromClient.get((String)f));
                
                if((String)f == 'PersonEmail'){
                    User u = new User();
                    for(User userRc :  [SELECT Email, UserName FROM User WHERE UserName =: (String)fromClient.get((String)f)]){
                        u = userRc ;
                    }
                    if(u != null && String.isNotEmpty(u.Id)){
                        Map<String, List<String>> validationErrors = new Map<String, List<String>>();
                        List<String> emailErrors = new List<String>();
                        emailErrors.add('Duplicate email address. Update and save again.');
                        validationErrors.put('PersonEmail', emailErrors);
                        returnProperties.put('VALIDATION_ERRORS', validationErrors);
                        return returnProperties;
                    }
                    
                }
                
                if (dirty.get('PersonOtherState') != null) {
                    System.debug('PersonOtherState from client: '+fromClient.get('PersonOtherState'));
                    if (fromClient.get('PersonOtherState') == 'None') {
                        fromClient.put('PersonOtherState', NULL);
                    }
                }
                
                if((String)f == 'Mailing_and_Residential_is_the_same__c' && fromClient.get(f) == true){
                    System.debug('**inside check***: '+fromClient.get(f) );
                    toSave.put('PersonMailingStreet' , fromClient.get('PersonOtherStreet'));
                    toSave.put('PersonMailingStateCode' , fromClient.get('PersonOtherStateCode'));
                    toSave.put('PersonMailingState' , fromClient.get('PersonOtherState'));
                    toSave.put('PersonMailingCity' , fromClient.get('PersonOtherCity'));
                    toSave.put('PersonMailingCountry' , fromClient.get('PersonOtherCountry'));
                    toSave.put('PersonMailingPostalCode' , fromClient.get('PersonOtherPostalCode'));
                    toSave.put('PersonMailingCountryCode' , fromClient.get('PersonOtherCountryCode'));
                    toSave.put((String) f, fromClient.get(f));
                }else if( (String) f == 'PersonBirthdate' ){
                    Date mydate = Date.parse((String)fromClient.get(f));
                    toSave.put((String) f, mydate);
                }else{
                    toSave.put((String) f, fromClient.get(f));
                }                
            }
        }
        //update toSave;
        /*Database.SaveResult sr = Database.update(toSave);
if (sr.isSuccess()) {
// Operation was successful, so get the ID of the record that was processed
System.debug('Successfully updated account. Account ID: ' + sr.getId());
}
else {
// Operation failed, so get all errors                
for(Database.Error err : sr.getErrors()) {
System.debug('The following error has occurred.');                    
System.debug(err.getStatusCode() + ': ' + err.getMessage());
System.debug('Account fields that affected this error: ' + err.getFields());
}
}*/
        
        try{
            update toSave;
        }catch(DmlException e){
            System.debug('The following exception has occurred: ' + e.getMessage());
        }
        
        
        /*
Map<String, List<String>> validationErrors = new Map<String, List<String>>();
List<String> emailErrors = new List<String>();
emailErrors.add('Email Not Good');
emailErrors.add('Duplicate Error Email Not Good');
validationErrors.put('PersonEmail', emailErrors);
returnProperties.put('VALIDATION_ERRORS', validationErrors);
*/
        return returnProperties;
    }
    
}