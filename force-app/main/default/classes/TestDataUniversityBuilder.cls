@IsTest @TestVisible
private with sharing class TestDataUniversityBuilder {

    private String name;
    private String status;
    private String country;

    @TestVisible
    private TestDataUniversityBuilder(String name) {
        this.name = name;
    }

    @TestVisible
    private TestDataUniversityBuilder status(String status) {
        this.status = status;
        return this;
    }

    @TestVisible
    private TestDataUniversityBuilder country(String country) {
        this.country = country;
        return this;
    }

    @TestVisible
    private List<edu_University__c> create(Integer count, Boolean persisted) {
        List<edu_University__c> unis = new List<edu_University__c>();
        for (Integer i = 0; i < count; i++) {
            unis.add(new edu_University__c(
                    University_Name__c = this.name,
                    Country__c = this.country,
                    Status__c = this.status));
        }
        if (persisted) insert unis;
        return unis;
    }

}