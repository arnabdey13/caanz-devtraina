public class luana_AuditLog {
    public static void insertLog(String level, String logMessage, String ref, String status, String type) {        
        LuanaSMS__Luana_Log__c log = new LuanaSMS__Luana_Log__c(LuanaSMS__Level__c = level, LuanaSMS__Log_Message__c = logMessage, LuanaSMS__Reference__c = ref, LuanaSMS__Status__c = status, LuanaSMS__Type__c = type);
        insert log;
    }
}