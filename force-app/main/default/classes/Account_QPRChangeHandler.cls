public class Account_QPRChangeHandler {
         
        public static void qprAccountChanges(List<Account> newAccountList,Map<Id,Account> newAccountMap,List<Account> oldAccountList,Map<Id,Account> oldAccountMap)
            {
                   List<QPR_Risk_Assessment__c>  new_QPRRiskList = new List<QPR_Risk_Assessment__c>();
                   List<QPR_Referral__c> new_QPRReferralList = new List<QPR_Referral__c>();
                if(newAccountList!=null && oldAccountList!=null)
                {
                  for(Account acc: newAccountList)
                  {
                    Account prevAccount = (Account)Trigger.oldMap.get(acc.ID);
                      if(!acc.isPersonAccount)
                      {
                           if (acc.BillingCountry!=null && acc.BillingCountry != prevAccount.BillingCountry) {
                               List<QPR_Risk_Assessment__c> riskList1 = [select ID, Hidden_Country__c from QPR_Risk_Assessment__c where Account__c=:acc.ID];
                               for(QPR_Risk_Assessment__c risk : riskList1)
                               {	if(acc.BillingCountry=='Australia' || acc.BillingCountry=='New Zealand')
                               		{ risk.Hidden_Country__c = acc.BillingCountry; }
                                    else
                                    { risk.Hidden_Country__c = 'Overseas'; }
                                	new_QPRRiskList.add(risk);
				               }
                               List<QPR_Referral__c> referralList = [select ID, Hidden_Country__c from QPR_Referral__c where Referred_Account__c=:acc.ID or Referral_Contact__c=:acc.ID];
                               for(QPR_Referral__c referral : referralList )
                               {
                                   if(acc.BillingCountry=='Australia' || acc.BillingCountry=='New Zealand')
                               		{ referral.Hidden_Country__c = acc.BillingCountry; }
                                    else
                                    { referral.Hidden_Country__c = 'Overseas'; }
                                	new_QPRReferralList.add(referral);
                               }
                           }
                      }
                      else
                      {
                          if (acc.Affiliated_Branch_Country__c!=null && acc.Affiliated_Branch_Country__c != prevAccount.Affiliated_Branch_Country__c) {
                               List<QPR_Risk_Assessment__c> riskList = [select ID, Hidden_Country__c from QPR_Risk_Assessment__c where Member_Name__c=:acc.ID];
                               for(QPR_Risk_Assessment__c risk : riskList)
                               {	
                                   	risk.Hidden_Country__c = acc.Affiliated_Branch_Country__c; 
                                	new_QPRRiskList.add(risk);
				               }
                               List<QPR_Referral__c> referralList = [select ID, Hidden_Country__c from QPR_Referral__c where Referred_Account__c=:acc.ID or Referral_Contact__c=:acc.ID];
                               for(QPR_Referral__c referral : referralList )
                               {
                                    referral.Hidden_Country__c = acc.Affiliated_Branch_Country__c; 
                                	new_QPRReferralList.add(referral);
                               }

                           }  
                      } 
                  }
                }
                if(new_QPRRiskList!=null)
                {Database.update(new_QPRRiskList);}
                if(new_QPRReferralList!=null)
                {Database.update(new_QPRReferralList);}
            }
}