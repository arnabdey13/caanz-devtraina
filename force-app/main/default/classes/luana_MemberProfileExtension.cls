public without sharing class luana_MemberProfileExtension {
    
    
    Id custCommConId;
    Id custCommAccId;
    public User currentUser {get; set;}
    
    public luana_MemberProfileExtension(ApexPages.StandardController std){

        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId());
        this.custCommConId = commUserUtil.custCommConId;
        //this.custCommConId = '003p0000003PlpE'; 
        
        currentUser = [Select Id, FirstName, LastName, Email, Fax, MobilePhone, Phone, Country, Street, City, State, PostalCode from User Where Id =: UserInfo.getUserId()];
    }
    
    public PageReference save(){
        update currentUser;
        return null;
    }
    
    public PageReference cancel(){
        PageReference cancelPage = new PageReference('/luana_MemberHome');
        return cancelPage;
    }

}