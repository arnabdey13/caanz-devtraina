global class AccountUpdate {
    webService static String updateCustomerID(string SFID, decimal memberID){
        try{
                Account customerAccount = [SELECT ID, Member_ID__c from account WHERE Customer_ID__c= :SFID];
                customerAccount.Member_ID__c = String.valueOf(memberID);

                update customerAccount;
                System.debug('Success');
                return 'true';
                
            } catch (Exception e){
               
                System.debug('An exception occurred: ' + e.getMessage());
                return  String.valueOf(e.getMessage());
            }

    }
}