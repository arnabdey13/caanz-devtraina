/*
    Developer: WDCi (Lean)
    Date: 23/May/2016
    Task #: Luana implementation - Virtual Class
*/

public with sharing class luana_VirtualClassBroadcastController {
    
    public boolean enabled {private set; public get;}
    public String buttonLocation {private set; public get;}
    
    public LuanaSMS__Course__c course {private set; public get;}
    
    private String targetCourseId {set; get;}
    
    public luana_VirtualClassBroadcastController (ApexPages.StandardController controller) {
    	
    	enabled = false;
    	targetCourseId = getCourseIdFromUrl();
    	
    	buttonLocation = 'bottom';
    	
    	if(getCourseIdFromUrl() != null){
    		try{
    			course = [select Id, Name from LuanaSMS__Course__c where Id =: targetCourseId];
    			
    			enabled = true;
    		} catch(Exception exp){
    			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Error retrieving course. Please try again later or contact your system administrator. Error: ' + exp.getMessage()));
    		}
    	} else {
    		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Invalid course. Please try again later or contact your system administrator.'));
    	}
    }
    
    private String getCourseIdFromUrl(){
        if(ApexPages.currentPage().getParameters().containsKey('id')){
            return ApexPages.currentPage().getParameters().get('id');
            
        }
        
        return null;
    }
    
    public PageReference doBroadcast(){
    	
    	try{
	    	Database.executeBatch(new luana_VirtualClassBroadcastAsync(targetCourseId), 200);
	    	
	    	ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'The job has been submitted.'));
    	
    	} catch(Exception exp){
    		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Error submitting job: ' + exp.getMessage()));
    	}
    	
		enabled = false;
		return null;
    }
    
}