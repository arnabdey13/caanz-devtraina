/*
    ExceptionHandler.cls
    ======================================================
    Changes:
    	Aug 2016 	Davanti - DN	Added prettifyDMLExceptionMessage method
*/
public with sharing class ExceptionHandler {
	private static final String ERROR_TOKEN='FIELD_CUSTOM_VALIDATION_EXCEPTION';
	private static final String ERROR_PATTERN='(^.*FIELD_CUSTOM_VALIDATION_EXCEPTION, )(.*)(\\: \\[.*\\]$)';
	
	public static void handleException(Exception ex){
		if(ex.getMessage().contains(ERROR_TOKEN)){
			Pattern myPattern = Pattern.compile(ERROR_PATTERN);
			
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
				myPattern.matcher(ex.getMessage()).replaceAll('$2')
			));
		}
		else{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: '+ 
				ex.getMessage()
				//ex.getTypeName() + ': ' + ex.getMessage() + ' at ' + ex.getStackTraceString() + '.'
			));
		}
	}

	//DN20160811 put string parsing from handleException method into separate method
	public static String prettifyDMLExceptionMessage(String dmlExceptionMessage){
		Pattern myPattern = Pattern.compile(ERROR_PATTERN);
		return myPattern.matcher(dmlExceptionMessage).replaceAll('$2');
	}
}