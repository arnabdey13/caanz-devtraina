public without sharing class luana_MemberProgramEnrolmentController {

    Id custCommConId;
    Id custCommAccId;
    
    public String courseName {set; get;}
    public String courseId {set; get;}
    public String selectedCourseId {set; get;}    
    public String selectedPOId {set; get;}    
    public Integer minElective {set; get;}
    public Integer maxElective {set; get;}
    public List<SubjectWrapper> subWrapList {set; get;}
    
    public List<LuanaSMS__Course__c> courses {set; get;}
    
    //LCA-495: Update CA Program attachment 
    Private String attachmentFileName = 'CA Program';
    public Boolean hasCAProgAtt {get; set;}
    public Attachment caProgAtt {get; private set;}
    
    public luana_MemberProgramEnrolmentController() {
        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId()); 
       
        this.custCommConId = commUserUtil.custCommConId;       
        this.custCommAccId = commUserUtil.custCommAccId;    
        
        /* LCA-495 & LCA-472: CA Program Enrolment error
        Account acc = [Select Id, Name, Affiliated_Branch_Country__c from Account Where Id =: custCommAccId];  
        
        if(acc.Affiliated_Branch_Country__c == 'Australia'){
            attachmentFileName = 'CA Program AU';
        }else if(acc.Affiliated_Branch_Country__c == 'New Zealand'){
            attachmentFileName = 'CA Program NZ';
        }*/
        
        hasCAProgAtt = false;
    }
    
    public LuanaSMS__Student_Program__c newSp {get; set;}
    public List<LuanaSMS__Student_Program_Subject__c> newSpsList {get; set;}
    
    public PageReference doCompleteSubSelect() {
        Integer countElective = 0;
        RecordType studProgRT; 
        
        newSp = new LuanaSMS__Student_Program__c();
        newSpsList = new List<LuanaSMS__Student_Program_Subject__c>();
        
        //LCA-495, check has Program term and condition exist
        caProgAtt = getTermAndConAttachmentFile(true);
        if(hasCAProgAtt){
            
            for(SubjectWrapper subWrap: subWrapList) {
                if(subWrap.isSelected && !subWrap.isCore) {
                    countElective++;        
                }        
            }
            if(countElective >= minElective) {
                
                if(maxElective == 0 || countElective <= maxElective){
                    for(RecordType rT : [select id from RecordType where DeveloperName =: luana_EnrolmentConstants.RECORDTYPE_STUDENTPROGRAM_ACCREDITEDPROGRAM and SObjectType =: 'LuanaSMS__Student_Program__c']){
                        studProgRT = rt;
                    }
                    newSp = new LuanaSMS__Student_Program__c(RecordTypeId=studProgRT.Id, LuanaSMS__Status__c='In Progress', LuanaSMS__Contact_Student__c=custCommConId, LuanaSMS__Course__c=courseId, LuanaSMS__Program_Commencement_Date__c=System.today());
                    System.debug('***newSp: ' + newSp);
                    //insert newSp;
                    /*newSpsList = new List<LuanaSMS__Student_Program_Subject__c>();
                    for(SubjectWrapper subWrap: subWrapList) {
                        if(subWrap.isSelected)
                            newSpsList.add(new LuanaSMS__Student_Program_Subject__c(LuanaSMS__Program_Offering_Subject__c=subWrap.pos.Id, LuanaSMS__Subject__c=subWrap.pos.LuanaSMS__Subject__c, LuanaSMS__Contact_Student__c=custCommConId, LuanaSMS__Student_Program__c=newSp.Id));
                    }*/
                    //insert newSpsList;
        
                    //return Page.luana_MemberMyEnrolment;
                    return Page.luana_MemberProgramDetailPage;
                    
                } else {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'You can only select maximum ' + maxElective + ' elective/s.'));
                    return null;
                }
            }
            else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'You must select at least ' + minElective + ' elective/s before you can proceed'));
                return null;
            }
        
        }else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Problem enroling Program (Term and Condition file not found). Please try again later or contact us if the problem persists.'));
            return Page.luana_MemberProgramDetailPage;
        }
    }
    
    public PageReference createSpAndSps(){
        //LCA-472, include try catch and savepoint when enrol program
        Savepoint sp = Database.setSavepoint();
        try{
            if(!getProgramCourses().isEmpty()){
                courseName = getProgramCourses()[0].Name;
            }

            if(newSp.Accept_terms_and_conditions__c){
                insert newSp;
                
                Attachment newAtt = caProgAtt.clone(false, true);
                newAtt.ParentId = newSp.Id;
                insert newAtt;
                
                newSpsList = new List<LuanaSMS__Student_Program_Subject__c>();
                for(SubjectWrapper subWrap: subWrapList) {
                    if(subWrap.isSelected)
                        newSpsList.add(new LuanaSMS__Student_Program_Subject__c(LuanaSMS__Program_Offering_Subject__c=subWrap.pos.Id, LuanaSMS__Subject__c=subWrap.pos.LuanaSMS__Subject__c, LuanaSMS__Contact_Student__c=custCommConId, LuanaSMS__Student_Program__c=newSp.Id));
                }
                insert newSpsList;
                
                return Page.luana_membermyenrolment;
            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure that the "Accept Terms and Conditions" are checked.'));
                return null;
            }
        }Catch(Exception exp){
            Database.rollback(sp);
            
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Problem enroling Program. Please try again later or contact us if the problem persists.' + exp.getMessage()));
            return null;
        }
    }
    
    public Attachment getTermAndCond(){
        return getTermAndConAttachmentFile(false);
    }
    
    public Attachment getTermAndConAttachmentFile(Boolean hasBody){
        
        for(Terms_and_Conditions__c termAndCon : [Select Id, Name from Terms_and_Conditions__c Where Name =: attachmentFileName order by createddate desc limit 1]){
          if(hasBody){
              for(Attachment att : [Select Id, Name, Body from Attachment Where ParentId =: termAndCon.Id order by createddate desc limit 1]){
                  hasCAProgAtt = true;
                  return att;
              }
          }else{
              for(Attachment att : [Select Id, Name from Attachment Where ParentId =: termAndCon.Id order by createddate desc limit 1]){
                  return att;
              }
          }
        }
        hasCAProgAtt = false;
        return new Attachment();
    }
    
    public PageReference doNavigateMain() {
        return Page.luana_MemberMyEnrolment;
    }
    
    public PageReference doNavigateProgramCourseSelect() {
        return Page.luana_MemberSelectProgramCourse;
    }
    
    public PageReference doNavigateSubSelection() {
 
        return Page.luana_MemberProgramSubSelect;
    }
    
    public List<LuanaSMS__Course__c> getProgramCourses() {  
        Luana_Extension_Settings__c defCACourseId = Luana_Extension_Settings__c.getValues('Default_CA_Program_Course_Id');
          
        return [SELECT Id, Name, LuanaSMS__Program_Offering__c, LuanaSMS__Program_Offering__r.LuanaSMS__Number_of_Required_Electives__c, LuanaSMS__Program_Offering__r.Maximum_Number_of_Required_Electives__c FROM LuanaSMS__Course__c WHERE LuanaSMS__Allow_Online_Enrolment__c = true AND LuanaSMS__Status__c = 'Running' AND RecordType.DeveloperName='Accredited_Program' AND Id =: defCACourseId.Value__c];
    }
    
    public List<SubjectWrapper> getSWList() {
    
        subWrapList = new List<SubjectWrapper>();
        for(LuanaSMS__Program_Offering_Subject__c pos : [SELECT Id, LuanaSMS__Essential__c, LuanaSMS__Subject__r.Name, LuanaSMS__Subject__c FROM LuanaSMS__Program_Offering_Subject__c WHERE LuanaSMS__Program_Offering__c =: selectedPOId]) {
            if(pos.LuanaSMS__Essential__c == 'Core')
                subWrapList.add(new SubjectWrapper(true, true, pos));
            else
                subWrapList.add(new SubjectWrapper(false, false, pos));
        }    
        return subWrapList;
    }
    
    class SubjectWrapper {
        public Boolean isSelected {set; get;}
        public Boolean isCore {set; get;}
        public LuanaSMS__Program_Offering_Subject__c pos {set; get;}
        
        public SubjectWrapper(Boolean isSelected, Boolean isCore, LuanaSMS__Program_Offering_Subject__c pos) {
            this.isSelected = isSelected;
            this.isCore = isCore;
            this.pos = pos;
        }
    }
    
    public String getDomainUrl(){
        return luana_NetworkUtil.getCommunityPath();
    }
}