@RestResource(urlMapping='/SchoolListWS/*')
global without sharing class FormAssemblyWS { // without sharing because portal user will not have access to account data
            
    @HttpGet 
    global static void getDetails() {
        //Have to send back response using RestResponse instead of string to stop 
        //APEX from wrapping response in quotes (because jsonp is a String).  
        //The quotes break the jsonp javascript on client.
        RestResponse resp = RestContext.response;
        RestRequest req = RestContext.request;  

        String callback= req.params.get('callback');
        String schoolAutoCompleteTerm= req.params.get('schoolAutoCompleteTerm');
        String country=req.params.get('country');
        String studenttype=req.params.get('studenttype');
        if(schoolAutoCompleteTerm!= null) {
            List<SchoolDetail__c> schools= new List<SchoolDetail__c>();
            String countryWhereClause = '';
            String studenttypeWhereClause = '';
            if (String.isNotBlank(country)) {
                countryWhereClause = ' AND Country__c = \'' + String.escapeSingleQuotes(country) + '\'';
            }
            if (String.isNotBlank(studenttype)) {
                studenttypeWhereClause = ' AND StudentType__c = \'' + String.escapeSingleQuotes(studenttype) + '\'';
            }
            String strQuery  = 'SELECT Name__c, Region__c, Country__c, State__c, StudentType__c FROM SchoolDetail__c' +
                  ' where Name__c LIKE \'%' + String.escapeSingleQuotes(schoolAutoCompleteTerm)  + '%\'' + 
                  countryWhereClause +
                  studenttypeWhereClause +
                  ' order by Name__c ASC' + 
                  ' LIMIT 50 ';
            schools = Database.query(strQuery);  
            
            //Should be cleaning up the JSON as per JSENCODE rules.  THis inludes single quotes, backslashes, tabs etc.  
            //Can't find an appropriate cleansing method in Salesforce - but single quotes are the only ones that matter.  Others may never happen.
            String returnJSON = String.escapeSingleQuotes(JSON.serialize(schools));
            
            //Support JSONP callback.
            if (callback != null) {
                //Wrap the response so its like this -- mycallbackfunction('thejson');
                resp.responseBody = Blob.valueOf(callback + '(\'' + returnJSON + '\');');
             } else {
                //Otherwise revert to standard JSON
                resp.responseBody = Blob.valueOf(returnJSON );
            }
        }
    } 
}