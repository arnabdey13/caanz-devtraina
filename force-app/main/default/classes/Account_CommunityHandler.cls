/***********************************************************************************************************************************************************************
Name: Account_CommunityHandler
============================================================================================================================== 
Purpose: This Class contains the code related to Account Community Member Functionality invoked from the AccountTriggerHandler 
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR                 DATE          DETAIL     Description 
1.0        Rama Krishna    07/02/2019    Created          This Class contains the code related to Account Community Member Functionality invoked from the AccountTriggerHandler 
                                                             
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/
public class Account_CommunityHandler{
        static Id memPortalProId = ProfileCache.getId(System.Label.Member_Portal_ProfileName);
        //set of Account Id's for multiple purpose use.
        static Set<Id> acctIdSet;
        // The following method creates a Community Member User for Person Account created
        public static void afterInsertChanges(List<Account> newAccountList){     
            acctIdSet = new Set<Id>();            
            Id userProfileId = UserInfo.getProfileId();
            for(Account acc: newAccountList){  
             //Criteria check for skipping the Member Portal Profile user for the below action          
                if(acc.IsPersonAccount && userProfileId != memPortalProId &&acc.Create_Portal_User__c)
                   acctIdSet.add(acc.Id);            
            }
            
            if(acctIdSet.size()>0){
                AccountTriggerClass.createCommunityMemberUser(acctIdSet); 
            }
        
        
        }
        //The following method creates a Community Member User for Person Account and Updates the Custom Settings Trigger Config
        public static void afterUpdateChanges(List<Account> newAccountList,Map<Id,Account> oldAccountMap){      
            acctIdSet = new Set<Id>();
            Set<Id> accountIdSet = new Set<Id>();
            Id userProfileId = UserInfo.getProfileId();
            for(Account acc: newAccountList){
                //Criteria check for skipping the Member Portal Profile user for the below action               
                if(acc.IsPersonAccount && userProfileId != memPortalProId && 
                  acc.Create_Portal_User__c && oldAccountMap.get(acc.Id).Create_Portal_User__c != acc.Create_Portal_User__c)
                   acctIdSet.add(acc.Id); 
                //Criteria check for comparing old and current values of Account fields               
                if(acc.Membership_Class__c!=oldAccountMap.get(acc.Id).Membership_Class__c || acc.CPP__c != oldAccountMap.get(acc.Id).CPP__c ||
                  acc.CPP_Picklist_AU__c != oldAccountMap.get(acc.Id).CPP_Picklist_AU__c || acc.CPP_Picklist_NZ__c != oldAccountMap.get(acc.Id).CPP_Picklist_NZ__c ||
                  acc.Has_CPP_in_AU_and_or_NZ__c!=oldAccountMap.get(acc.Id).Has_CPP_in_AU_and_or_NZ__c)
                    accountIdSet.add(acc.Id);
                
            }
                      
            if(acctIdSet.size()>0){
                AccountTriggerClass.createCommunityMemberUser(acctIdSet); 
            }
            //Update EmployeeHistroy records related to Member with Field Rollups Recalculation set to True
            if(accountIdSet.size()>0){
                List<Employment_History__c> employmentHistoryList = [Select id from Employment_History__c where Member__c IN :accountIdSet];
                for(Employment_History__c EmploymentHistory : employmentHistoryList){
                    EmploymentHistory.Rollups_Recalculation__c = true;
                }
                update employmentHistoryList;
            }
            TriggerConfigHandler.updatesBasedOnTriggerConfig();                
        }

}