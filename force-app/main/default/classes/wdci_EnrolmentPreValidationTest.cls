/* 
    Developer: WDCi (LKoh)
    Development Date: 09/07/2019
    Task #: ATAEC-14/15/15/53/16/17/18 - Module Enrolment Form Changes
    
    Change History
*/
@isTest
public class wdci_EnrolmentPreValidationTest {
    
    final static String contextLongName = 'EnrolmentPreValidationTest';
    final static String contextShortName = 'epv';
    public static Luana_DataPrep_Test dataPrepUtil {get; set;}
    public static AAS_DataPrep_Test aasDataPrepUtil {get; set;}
    public static luana_CommUserUtil userUtil {get; set;}
    public static Account memberAccount {get; set;}

    @TestSetup
    static void generateAssets() {

        wdci_TestUtil.generateAsset();
        
        TestDataFFRichTextBuilder.multiple(RICH_TEXT_NAMES, 'fake content', true);

        List<Contact> studentContact = [SELECT Id, FirstName, LastName FROM Contact LIMIT 1];
        system.debug('studentContact: ' +studentContact);        

        // Create the basic assets including the Student Program
        dataPrepUtil = new Luana_DataPrep_Test();
        aasDataPrepUtil = new AAS_DataPrep_Test();
        insert dataPrepUtil.prepLuanaExtensionSettingCustomSettings();
        insert aasDataPrepUtil.defauleCustomSetting();        
        userUtil = new luana_CommUserUtil(UserInfo.getUserId());
        Map<String, Id> recordTypeMap = aasDataPrepUtil.getRecordTypeMap('Exam');
        Map<String, SObject> assetMap = aasDataPrepUtil.createBasicAssets(contextLongName, contextShortName, dataPrepUtil, aasDataPrepUtil);

        //Create new account for testing usage
        memberAccount = dataPrepUtil.generateNewApplicantAcc('Joe_' + contextShortName, contextLongName, 'Full_Member');
        memberAccount.Member_Id__c = '23456';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.Assessible_for_CA_Program__c = false;
        memberAccount.PersonEmail = 'joe_5_' + contextShortName +'@gmail.com';
        memberAccount.PersonHomePhone= '1234';
        memberAccount.PersonOtherStreet= '83 Saggers Road';
        memberAccount.PersonOtherCity='JITARNING';
        memberAccount.PersonOtherState='Western Australia';
        memberAccount.PersonOtherCountry='Australia';
        memberAccount.PersonOtherPostalCode='6365';
        insert memberAccount;
        
        memberAccount =  [SELECT Id, PersonContactID, Member_Id__c, Affiliated_Branch_Country__c, Membership_Class__c, Assessible_for_CA_Program__c, PersonEmail FROM Account WHERE Id =: memberAccount.Id LIMIT 1];        
        assetMap.put('MEMBERACCOUNT1', memberAccount);

        assetMap = aasDataPrepUtil.createBasicAssetsPart2(contextLongName, contextShortName, dataPrepUtil, aasDataPrepUtil, assetMap);

        LuanaSMS__Subject__c buss1030Subject = wdci_TestUtil.generateSubject('BUSS1030', 'Accounting, Business and Society', 'Module');
        insert buss1030Subject;
        
        LuanaSMS__Delivery_Location__c ausCampusDeliLoc = wdci_TestUtil.generateDeliveryLocation('Australia Campus', assetMap.get('TRAININGORG1').Id);
        insert ausCampusDeliLoc;

        // Generate Student Program Subject
        LuanaSMS__Student_Program_Subject__c studentProgramSubject1 = wdci_TestUtil.generateStudentProgramSubject(studentContact[0].Id, assetMap.get('STUDENTPROGRAM1').Id, buss1030Subject.Id, ausCampusDeliLoc.Id);
        insert studentProgramSubject1;

        // Sets the SPS to use with the EnrolmentPreValidationPage
        Test.setCurrentPageReference(new PageReference('Page.wdci_EnrolmentPreValidationPage'));
        System.currentPageReference().getParameters().put('spsId', studentProgramSubject1.Id);

        // Get the residency status map
        List<Residency_Status__mdt> residencyStatusList = [SELECT Id, Mode__c, Residency_Status_value__c FROM Residency_Status__mdt];
        Map<Integer, String> residencyStatusMap = new Map<Integer, String>();
        for (Residency_Status__mdt rs : residencyStatusList) {
            residencyStatusMap.put(Integer.valueOf(rs.Mode__c), rs.Residency_Status_value__c);
        }

        Test.startTest();
            wdci_EnrolmentPreValidationController newController = new wdci_EnrolmentPreValidationController();
            String templateName = newController.getTemplateName();
            String invalidVisaText = newController.getInvalidVisaText();

            // Simulating the user entering the form details

            newController.currentSPS.LuanaSMS__Contact_Student__r.Account.Current_citizen_residency_status__c = residencyStatusMap.get(3);
            newController.residencyStatusChanged();
            PageReference redirectPage = newController.doNext();

            newController.currentSPS.LuanaSMS__Contact_Student__r.OtherCountryCode = 'AU';
            newController.currentSPS.LuanaSMS__Contact_Student__r.OtherStreet = 'Level 27, 66-68 Goulburn Street';
            newController.currentSPS.LuanaSMS__Contact_Student__r.OtherPostalCode = '2000';
            newController.currentSPS.LuanaSMS__Contact_Student__r.OtherCity = 'Sydney';

            newController.currentSPS.LuanaSMS__Contact_Student__r.Account.Current_citizen_residency_status__c = residencyStatusMap.get(1);
            newController.residencyStatusChanged();
            newController.currentSPS.LuanaSMS__Contact_Student__r.PIP_Citizen_or_Permanent_Resident__c = 'Holder of Australian Permanent Humanitarian Visa';
            redirectPage = newController.doNext();

            newController.currentSPS.LuanaSMS__Contact_Student__r.Account.Current_citizen_residency_status__c = residencyStatusMap.get(2);
            newController.residencyStatusChanged();
            newController.currentSPS.LuanaSMS__Contact_Student__r.PIP_Temporary_Visa_Holder__c = 'In Australia on a visa other than student visa, intending to study while in Australia';
            newController.currentSPS.LuanaSMS__Contact_Student__r.PIP_Student_Nationality__c = 'Brazil';
            redirectPage = newController.doNext();

            newController.currentSPS.LuanaSMS__Contact_Student__r.Account.Current_citizen_residency_status__c = residencyStatusMap.get(4);
            newController.residencyStatusChanged();
            newController.currentSPS.LuanaSMS__Contact_Student__r.PIP_Student_Nationality__c = 'Brazil';
            redirectPage = newController.doNext();

            newController.currentSPS.LuanaSMS__Contact_Student__r.Account.Current_citizen_residency_status__c = residencyStatusMap.get(5);
            newController.residencyStatusChanged();
            newController.currentSPS.LuanaSMS__Contact_Student__r.PIP_Student_Nationality__c = 'Brazil';
            redirectPage = newController.doNext();

        Test.stopTest();
    }

    @isTest static void test_EnrolmentPreValidationController() {


        
    }

    private static final List<String> RICH_TEXT_NAMES = new List<String>{
            'ProvApp.Stage6.StatementPrivacyLink',
            'ProvApp.Stage0.InternationalAgreements',
            'ProvApp.Stage1.AddressContent',
            'ProvApp.Stage0.AssistanceRequired',
            'ProvApp.Stage6.StatementPrivacyStatement',
            'ProvApp.Stage5.LegalDisciplinaryByStatutory',
            'ProvApp.Stage5.LegalDisciplinaryByTertiary',
            'ProvApp.Stage5.Legal',
            'ProvApp.Stage4.Mentoring',
            'ProvApp.Stage6.StatementDeclaration',
            'ProvApp.Stage5.LegalBankruptcy',
            'ProvApp.Stage3.Education',
            'ProvApp.Stage7.Submitted',
            'ProvApp.Stage4.ApprovedEmployment',
            'ProvApp.Stage4.OutOfApprovedEmployment',
            'ProvApp.Stage5.LegalConviction',
            'ProvApp.Stage4.MentorSearch',
            'ProvApp.Stage4.EmploymentDetails',
            'ProvApp.Stage2.NZICARESIDENT',
            'ProvApp.Stage6.StatementEUGDPRStatement',
            // PAN:5340                
            'ProvApp.StageCheckList.RequiredDocument', 
            'ProvApp.StageCheckList.RequiredDocumentNote',
            'ProvApp.StageCheckList.AdditionalInfo',
            'ProvApp.StageCheckList.AdditionalInfoNote',
            'ProvApp.StageCheckList.ProvisionalMembershipObligations',
            'ProvApp.StageCheckList.ProvisionalMembershipObligationsNote',
            'ProvApp.StageCheckList.AdditionalInfo_MenterMembershipNumber',
            'ProvApp.StageCheckList.Certification',
            'ProvApp.StageCheckList.RequiredText',
            'ProvApp.StageCheckList.Optional',
            'Application.ProvisionalMembershipObligations',
            'EducationHistory.YearOfCommence',
            'EducationHistory.YearOfFinish',
            'Application.SubjectToDisciplinaryByCompany',
            'ProvApp.Stage5.NotToManageCorporation',
            'Status.opts',
            'Type.opts',
            // end of PAN:5340
            // PAN:6353
            'ProvApp.Stage2.InvalidVisa',
            // end of PAN:6353
            // PAN:6357
            'ProvApp.Stage6.StatementPrivacyStatementCA',
            'ProvApp.Stage6.StatementDeclarationCA',
            // end of PAN:6357
            // IPP - added to check coverage for ProvisionalApplicationService
            'IPP.StageCheckList.RequiredText',
            'IPP.StageCheckList.RequiredDocumentNote',
            'IPP.StageCheckList.ImportantInfo',
            'IPP.StageCheckList.Certification',
            'IPP.StageCheckList.GoodStanding'
            // end of IPP
    };
}