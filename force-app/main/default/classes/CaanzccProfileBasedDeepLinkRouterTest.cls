@IsTest
private class CaanzccProfileBasedDeepLinkRouterTest {


    @isTest static void test_CaanzccProfileBasedRouterController() {
        String data = CaanzccProfileBasedDeepLinkRouter.getCurrentUserProfile();
        System.assertNotEquals(data, null);
    }

    static testMethod void testLoadWithoutNavigation() {
        String destination = CaanzccProfileBasedDeepLinkRouter.load(TestSubscriptionUtils.getTestAccountId());
        System.assertEquals(null, destination, 'test members have no deep link by default');
    }

    static testMethod void testLoadWithNavigation() {
        String destination = '/myProfile';
        Account a = new Account(Id = TestSubscriptionUtils.getTestAccountId(),
                Registration_Destination_URL__c = destination);
        update a;

        String destinationReturned = CaanzccProfileBasedDeepLinkRouter.load(TestSubscriptionUtils.getTestAccountId());
        System.assertEquals(destination, destinationReturned, 'destination is return after first load');

        destinationReturned = CaanzccProfileBasedDeepLinkRouter.load(TestSubscriptionUtils.getTestAccountId());
        System.assertEquals(null, destinationReturned, 'destination was removed after first load');
    }

    @testSetup static void createTestData() {
        TestSubscriptionUtils.createTestData();
    }

}