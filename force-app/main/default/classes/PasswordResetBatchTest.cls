@isTest(SeeAllData=false)
private class PasswordResetBatchTest {
    public static testMethod void testBatch() {
        String profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;

        // Create a test user
        List<Account> testAccounts=new List<Account>();
        for(integer i=0;i<1;i++){
            Account testAccount=TestObjectCreator.createFullMemberAccount();
            testAccount.Membership_Type__c='Member';
            testAccounts.add(testAccount);
        }

        Test.startTest();
        insert testAccounts; // Future method
        User currentRunningUser=[select Id from User where Id=:UserInfo.getUserId()];
        system.runAs(currentRunningUser){
            PasswordResetBatch prb = new PasswordResetBatch(profileId);
            ID batchprocessid = Database.executeBatch(prb, 200);
        }
        Test.stopTest();
        
    }
}