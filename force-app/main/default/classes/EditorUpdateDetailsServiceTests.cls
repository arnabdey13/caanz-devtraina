@IsTest(seeAllData=false)
public class EditorUpdateDetailsServiceTests {

    @IsTest
    public static void testLoadFullCPPNZ() {
        update new Account(Id = TestSubscriptionUtils.getTestAccountId(), CPP_Picklist__c = 'Full');
        Map<String, Object> fromApex = (Map<String, Object>) EditorUpdateDetailsService.load(TestSubscriptionUtils.getTestAccountId());
        Map<Object, Object> record = (Map<Object, Object>) fromApex.get('RECORD');
        System.assertEquals(true, fromApex.get('prop.CPP'), 'CPP checkbox is ticked for NZ Full');
        System.assertEquals(TestSubscriptionUtils.TEST_USER_MEMBER_ID, record.get('Member_ID__c'),
                'Member Id is populated during read');
    }

    @IsTest
    public static void testLoadExemptCPP() {
        update new Account(Id = TestSubscriptionUtils.getTestAccountId(), CPP_Picklist__c = 'Exempt');
        Map<String, Object> fromApex = (Map<String, Object>) EditorUpdateDetailsService.load(TestSubscriptionUtils.getTestAccountId());
        System.assertEquals(false, fromApex.get('prop.CPP'), 'CPP checkbox is unticked for Exempt');
    }

    @IsTest
    public static void testLoadCPPNull() {
        Map<String, Object> fromApex = (Map<String, Object>) EditorUpdateDetailsService.load(TestSubscriptionUtils.getTestAccountId());
        System.assertEquals(false, fromApex.get('prop.CPP'), 'CPP checkbox is unticked for null');
    }

    @IsTest
    public static void testLoadFullCPP() {
        update new Account(Id = TestSubscriptionUtils.getTestAccountId(), CPP_Picklist__c = 'Full');
        Map<String, Object> fromApex = (Map<String, Object>) EditorUpdateDetailsService.load(TestSubscriptionUtils.getTestAccountId());
        System.assertEquals(true, fromApex.get('prop.CPP'), 'CPP checkbox is ticked for AU Full');
    }

    @IsTest
    public static void testSaveAccountRelinquish() {
        update new Account(Id = TestSubscriptionUtils.getTestAccountId(), CPP_Picklist__c = 'Full');
        Map<String, Object> properties = (Map<String, Object>) EditorUpdateDetailsService.load(TestSubscriptionUtils.getTestAccountId());

        setAnswer(properties, 'prop.CPP', false, 'CPP Checkbox');
        setAnswer(properties, 'prop.CPPRelinquishReason', 'Do not like counting', 'Reason for relinquish');
        setAnswer(properties, 'prop.PracticeCeasedDate', '3/5/16', 'Date stopped');
        setAnswer(properties, 'prop.CPPConfirmIndemnity', true, 'Confirm indemnity cover');
        setAnswer(properties, 'prop.ConfirmCPP', true, 'Confirm Relinquish');

        boolean caseCreated = EditorUpdateDetailsService.save(properties, TestSubscriptionUtils.getTestAccountId());
        System.assertEquals(true, caseCreated, 'Case was created if property is unchecked');
        List<Case> casesCreated = [select Id, AccountId, ContactId, Description from Case];
        System.assertEquals(1, casesCreated.size(), 'The case was persisted to db');
        System.assertNotEquals(null, casesCreated[0].AccountId, 'Account Id was populated');
        System.assertNotEquals(null, casesCreated[0].ContactId, 'Contact Id was populated');
        System.assertEquals('Confirmation Responses:\n\nCPP Checkbox => false\nReason for relinquish => Do not like counting\nDate stopped => 3/5/16\nConfirm indemnity cover => true\nConfirm Relinquish => true',
                casesCreated[0].Description, 'Description has summary of confirmation questions and answers');

        System.assertEquals('Removed', [select CPP_Picklist__c from Account where Id = :TestSubscriptionUtils.getTestAccountId()].CPP_Picklist__c,
                'CPP field is Removed after relinquish');
    }

    @IsTest
    public static void testSaveAccountChangingState() { // regression test for bug found in development
        Map<String, Object> properties = (Map<String, Object>) EditorUpdateDetailsService.load(TestSubscriptionUtils.getTestAccountId());
        setAnswer(properties, 'PersonOtherState', 'New South Wales', 'State');
        boolean caseCreated = EditorUpdateDetailsService.save(properties, TestSubscriptionUtils.getTestAccountId());
        System.assertEquals(false, caseCreated, 'No Case if state is changed');
        System.assertEquals('New South Wales', [select PersonOtherState from Account where Id = :TestSubscriptionUtils.getTestAccountId()].PersonOtherState,
                'State is changed');
    }

    @IsTest
    public static void testSaveAccountNotChangingCountry() {
        Map<String, Object> properties = (Map<String, Object>) EditorUpdateDetailsService.load(TestSubscriptionUtils.getTestAccountId());
        Account accountBefore = [select Id, PersonOtherCountryCode from Account where Id = :TestSubscriptionUtils.getTestAccountId()];
        Questionnaire_Response__c q = new Questionnaire_Response__c(Status__c = 'Draft', Account__c = TestSubscriptionUtils.getTestAccountId(),
                Question_1__c = 'How Many Hours', Answer_1__c = '345',
                // questions above #7 are country specific
                Question_7__c = 'Auditor?', Answer_7__c = 'Yes');
        insert q;
        boolean caseCreated = EditorUpdateDetailsService.save(properties, TestSubscriptionUtils.getTestAccountId());
    }

    @IsTest
    public static void testSaveAccountChangingStateToNone() { // regression test for bug found in development
        Map<String, Object> properties = (Map<String, Object>) EditorUpdateDetailsService.load(TestSubscriptionUtils.getTestAccountId());
        setAnswer(properties, 'PersonOtherState', 'None', 'State');
        boolean caseCreated = EditorUpdateDetailsService.save(properties, TestSubscriptionUtils.getTestAccountId());
        System.assertEquals(false, caseCreated, 'No Case if state is changed');
        System.assertEquals(null, [select PersonOtherState from Account where Id = :TestSubscriptionUtils.getTestAccountId()].PersonOtherState,
                'State is changed to null when None is sent to server');
    }

    @IsTest
    public static void testSaveAccountWithoutChanges() {
        Map<String, Object> properties = (Map<String, Object>) EditorUpdateDetailsService.load(TestSubscriptionUtils.getTestAccountId());
        boolean caseCreated = EditorUpdateDetailsService.save(properties, TestSubscriptionUtils.getTestAccountId());
        System.assertEquals(false, caseCreated, 'No Case if CPP checkbox is not dirty');
    }

    @IsTest
    public static void testSaveAccountConcessionPresent() {
        Map<String, Object> properties = (Map<String, Object>) EditorUpdateDetailsService.load(TestSubscriptionUtils.getTestAccountId());
        setAnswer(properties, 'Financial_Category__c', 'Retired', 'Concession');
        EditorUpdateDetailsService.save(properties, TestSubscriptionUtils.getTestAccountId());
        Map<String, Object> fromApex = (Map<String, Object>) EditorUpdateDetailsService.load(TestSubscriptionUtils.getTestAccountId());
        Map<Object, Object> record = (Map<Object, Object>) fromApex.get('RECORD');
        System.assertEquals('Retired', record.get('Financial_Category__c'), 'Concession is saved');
    }

    @IsTest
    public static void testConcessionsOther() {
        update new Account(Id = TestSubscriptionUtils.getTestAccountId(), Financial_Category__c = 'Life');
        Map<String, Object> fromApex = (Map<String, Object>) EditorUpdateDetailsService.load(TestSubscriptionUtils.getTestAccountId());
        Map<Object, Object> record = (Map<Object, Object>) fromApex.get('RECORD');
        System.assertEquals('None', record.get('Financial_Category__c'), 'All other concessions translate to None during load');

        // explicitely set to None to mark it dirty since we are testing save of None value here
        setAnswer(fromApex, 'Financial_Category__c', 'None', 'Concession');

        EditorUpdateDetailsService.save(fromApex, TestSubscriptionUtils.getTestAccountId());
        System.assertEquals('Life', [select Financial_Category__c from Account where id = :TestSubscriptionUtils.getTestAccountId()].Financial_Category__c,
                'None values do not overwrite mapped values during save');
    }

    @IsTest
    public static void testConcessionsPresent() {
        update new Account(Id = TestSubscriptionUtils.getTestAccountId(), Financial_Category__c = 'Retired');
        Map<String, Object> fromApex = (Map<String, Object>) EditorUpdateDetailsService.load(TestSubscriptionUtils.getTestAccountId());
        Map<Object, Object> record = (Map<Object, Object>) fromApex.get('RECORD');
        System.assertEquals('Retired', record.get('Financial_Category__c'), 'All other concessions translate to None during load');

        // explicitely set to None to mark it dirty since we are testing save of None value here
        setAnswer(fromApex, 'Financial_Category__c', 'None', 'Concession');

        EditorUpdateDetailsService.save(fromApex, TestSubscriptionUtils.getTestAccountId());
        System.assertEquals('Standard fee applies', [select Financial_Category__c from Account where id = :TestSubscriptionUtils.getTestAccountId()].Financial_Category__c,
                'None values do not overwrite mapped values during save');
    }

    @IsTest
    public static void testConcessionsAbsent() {
        Map<String, Object> fromApex = (Map<String, Object>) EditorUpdateDetailsService.load(TestSubscriptionUtils.getTestAccountId());
        Map<Object, Object> record = (Map<Object, Object>) fromApex.get('RECORD');
        System.assertEquals('None', record.get('Financial_Category__c'), 'All other concessions translate to None during load');

        // explicitely set to None to mark it dirty since we are testing save of None value here
        setAnswer(fromApex, 'Financial_Category__c', 'None', 'Concession');

        EditorUpdateDetailsService.save(fromApex, TestSubscriptionUtils.getTestAccountId());
        System.assertEquals('Standard fee applies', [select Financial_Category__c from Account where id = :TestSubscriptionUtils.getTestAccountId()].Financial_Category__c,
                'None values set default correctly');
    }

    @IsTest
    public static void testSaveAccountConcessionAbsent() {
        Map<String, Object> properties = (Map<String, Object>) EditorUpdateDetailsService.load(TestSubscriptionUtils.getTestAccountId());
        setAnswer(properties, 'Financial_Category__c', 'None', 'Concession');
        EditorUpdateDetailsService.save(properties, TestSubscriptionUtils.getTestAccountId());
        Map<String, Object> fromApex = (Map<String, Object>) EditorUpdateDetailsService.load(TestSubscriptionUtils.getTestAccountId());
        Map<Object, Object> record = (Map<Object, Object>) fromApex.get('RECORD');
        System.assertEquals('None', record.get('Financial_Category__c'), 'Concession is returned as expected');
    }

    @testSetup static void createTestData() {
        TestSubscriptionUtils.createTestData();
        BillingCPPConfig__c recipient = new BillingCPPConfig__c(Name = 'To', Recipient__c = 'fake@email.co.nz');
        insert recipient;
    }

    @testSetup static void countryPickListTestMethod() {
        CountryPicklistSetting c = new CountryPicklistSetting('AU', 'Australia');
    }

    // emulate how the Lightning code handles onChange events and keep tests DRY
    private static void setAnswer(Map<String, Object> properties, String field, Object value, String question) {
        Map<Object, Object> record = (Map<Object, Object>) properties.get('RECORD');
        Map<Object, Object> dirty = (Map<Object, Object>) properties.get('DIRTY');
        if (dirty == null) {
            dirty = new Map<Object, Object>();
            properties.put('DIRTY', dirty);
        }
        dirty.put(field, true);

        Map<Object, Object> labels = (Map<Object, Object>) properties.get('LABEL-MAPPINGS');
        if (labels == null) {
            labels = new Map<Object, Object>();
            properties.put('LABEL-MAPPINGS', labels);
        }
        labels.put(field, question);

        if (EditorPageUtils.isPropertyField(field)) {
            properties.put(field, value);
        } else {
            record.put(field, value);
        }
    }

}