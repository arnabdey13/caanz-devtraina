/**
* @author Steve Buikhuizen, Jannis Bott
* @date 14/11/2016
*
* @group FormsFramework
*
* @description  The wrapper that is returned on load when forms framework is initialised by Lightning Driver.
*               This must be a top level class to allow it to be used a Lightning attribute type.
*/
global class ff_ReadData {

    // map keys are strongly typed sObject type. The list of wrappers supports N lists of the same sObject type
    // strongly typed keys supports query auto-generation on the server based on client controls
    @AuraEnabled
    global Map<String, List<ff_Service.SObjectWrapper>> records {get;set;}

    @AuraEnabled
    global Map<String, List<ff_Service.CustomWrapper>>  wrappers {get;set;}
    @AuraEnabled
    global Map<String, Object> properties {get;set;}
}