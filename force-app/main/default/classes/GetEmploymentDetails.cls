global class GetEmploymentDetails
{
    global class EmploymentID
    {
        webservice string memberID;
        webservice string employerID;
    }
    global class EmploymentDetails
    {
        webservice string MemberGlobalID;
        webservice string MemberInstituteID;
        webservice string MemberFirstName;
        webservice string MemberLastName;
        webservice string CompanyGlobalID;
        webservice string CompanyInstituteID;
        webservice string CompanyName;
        webservice string MemberOf;
    }

    webService static EmploymentDetails getDetails(EmploymentID ObjectIDs)
    {
          
        Account customerAccount = [SELECT Customer_Id__c, Member_ID__c, FirstName, LastName, Member_Of__c from account WHERE Id= :ObjectIDs.memberID]; 
        Account employerAccount = [SELECT Customer_Id__c,  Member_ID__c, Name from account WHERE Id= :ObjectIDs.employerID];

        EmploymentDetails detailsEmployment = new EmploymentDetails();

        detailsEmployment.MemberGlobalID = String.valueOf(customerAccount.Customer_Id__c);
        detailsEmployment.MemberInstituteID = String.valueOf(customerAccount.Member_ID__c);
        detailsEmployment.MemberFirstName= customerAccount.FirstName;
        detailsEmployment.MemberLastName= customerAccount.LastName;
        detailsEmployment.CompanyGlobalID = String.valueOf(employerAccount.Customer_ID__c);
        detailsEmployment.CompanyInstituteID = String.valueOf(employerAccount.Member_ID__c);
        detailsEmployment.CompanyName = employerAccount.Name;
        detailsEmployment.MemberOf = customerAccount.Member_Of__c;

        return detailsEmployment;
    }
}