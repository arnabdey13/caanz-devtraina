public with sharing class AAS_DataUploadFormatManualController {
    
    public Id courseAssessmentId {get; set;}
    
    public Map<Id, String> assSchSectionMap {get; set;}
    
    public Map<Id, List<assiHeaderWrapper>> headerMap {get; set;}
    public Map<String, List<assiBodyWrapper>> bodyMap {get; set;}
    
    public AAS_DataUploadFormatManualController() {
        courseAssessmentId = ApexPages.currentPage().getParameters().get('id');
        
        
        List<AAS_Assessment_Schema_Section_Item__c> assessSchSecItems = [Select Id, Name, AAS_Weight__c, AAS_External_Id__c, AAS_Assessment_Schema_Section__c, AAS_Assessment_Schema_Section__r.AAS_Course_Assessment__c,
                                                        AAS_Assessment_Schema_Section__r.RecordType.DeveloperName, AAS_Assessment_Schema_Section__r.RecordType.Name, AAS_Full_Mark__c
                                                        from AAS_Assessment_Schema_Section_Item__c Where AAS_Assessment_Schema_Section__r.AAS_Course_Assessment__c =: courseAssessmentId Order by Name ASC];

        //This map used to set the record type name which used to display in the visualforce page 
        assSchSectionMap = new Map<Id, String>();
        
        headerMap = new Map<Id, List<assiHeaderWrapper>>();
        bodyMap = new Map<String, List<assiBodyWrapper>>();
        
        Integer newHeaderCounter = 1;
        Integer newBodyCounter = 1;
        for(AAS_Assessment_Schema_Section_Item__c asai: assessSchSecItems){
            
            assSchSectionMap.put(asai.AAS_Assessment_Schema_Section__c, asai.AAS_Assessment_Schema_Section__r.RecordType.Name);
          
            if(headerMap.containsKey(asai.AAS_Assessment_Schema_Section__c)){
                //Set the header value for the dynamic part
                assiHeaderWrapper schHeader = new assiHeaderWrapper('Assessment Schema Section Item Name #' + newHeaderCounter, newHeaderCounter);
                assiHeaderWrapper qHeader = new assiHeaderWrapper('Q' + newHeaderCounter, newHeaderCounter);
                
                headerMap.get(asai.AAS_Assessment_Schema_Section__c).add(schHeader);
                headerMap.get(asai.AAS_Assessment_Schema_Section__c).add(qHeader);
                newHeaderCounter++;
            }else{
                
                //Set the header value for Exam or Non Exam default value
                newHeaderCounter = 1; //This newHeaderCounter is to reset the value back to 1 again for earch different table
                List<assiHeaderWrapper> assiHeaderWrapperList = new List<assiHeaderWrapper>();
                
                //Seperate the Exam & (BB and Workshop) format
                if(asai.AAS_Assessment_Schema_Section__r.RecordType.DeveloperName.equals('Exam_Assessment') || asai.AAS_Assessment_Schema_Section__r.RecordType.DeveloperName.equals('Supp_Exam_Assessment')){
                    assiHeaderWrapper lNameHeader = new assiHeaderWrapper('Exam Number', 1);
                    assiHeaderWrapperList.add(lNameHeader);
                    headerMap.put(asai.AAS_Assessment_Schema_Section__c, assiHeaderWrapperList);
                }else{
                    assiHeaderWrapper memberHeader = new assiHeaderWrapper('Member Id', 1);
                    assiHeaderWrapperList.add(memberHeader);
                    
                    assiHeaderWrapper customerHeader = new assiHeaderWrapper('Customer Id', 2);
                    assiHeaderWrapperList.add(customerHeader);

                    headerMap.put(asai.AAS_Assessment_Schema_Section__c, assiHeaderWrapperList);
                }
                assiHeaderWrapper schHeader = new assiHeaderWrapper('Assessment Schema Section Item Name #' + newHeaderCounter, newHeaderCounter);
                assiHeaderWrapper qHeader = new assiHeaderWrapper('Q' + newHeaderCounter, newHeaderCounter);
                
                //Set initial header value
                headerMap.get(asai.AAS_Assessment_Schema_Section__c).add(schHeader);
                headerMap.get(asai.AAS_Assessment_Schema_Section__c).add(qHeader);
                newHeaderCounter++;
            }
            
            //Set Body data 0001 here
            if(bodyMap.containsKey(asai.AAS_Assessment_Schema_Section__c+'_0001')){
                //Set the body value for the dynamic part
                assiBodyWrapper schBody = new assiBodyWrapper(asai.Name, newBodyCounter);
                assiBodyWrapper qBody = new assiBodyWrapper(String.valueOf(newBodyCounter + 1), newBodyCounter);
                
                bodyMap.get(asai.AAS_Assessment_Schema_Section__c+'_0001').add(schBody);
                bodyMap.get(asai.AAS_Assessment_Schema_Section__c+'_0001').add(qBody);
                newBodyCounter++;
            }else{
                newBodyCounter = 1; //This newHeaderCounter is to reset the value back to 1 again for earch different table
                List<assiBodyWrapper> assiBodyWrapperList = new List<assiBodyWrapper>();
                
                //Seperate the Exam & (BB and Workshop) value
                if(asai.AAS_Assessment_Schema_Section__r.RecordType.DeveloperName.equals('Exam_Assessment') || asai.AAS_Assessment_Schema_Section__r.RecordType.DeveloperName.equals('Supp_Exam_Assessment')){
                    assiBodyWrapper examNum = new assiBodyWrapper('0001', 1);
                    assiBodyWrapperList.add(examNum);
                    bodyMap.put(asai.AAS_Assessment_Schema_Section__c+'_0001', assiBodyWrapperList);
                }else{
                    assiBodyWrapper memberId = new assiBodyWrapper('0001', 1);
                    assiBodyWrapperList.add(memberId);
                    
                    assiBodyWrapper custBody = new assiBodyWrapper('0022121', 2);
                    assiBodyWrapperList.add(custBody);

                    bodyMap.put(asai.AAS_Assessment_Schema_Section__c+'_0001', assiBodyWrapperList);
                }
                
                assiBodyWrapper schBody = new assiBodyWrapper(asai.Name, newBodyCounter);
                assiBodyWrapper qBody = new assiBodyWrapper(String.valueOf(newBodyCounter + 1), newBodyCounter);
                
                //Set initial body value
                bodyMap.get(asai.AAS_Assessment_Schema_Section__c+'_0001').add(schBody);
                bodyMap.get(asai.AAS_Assessment_Schema_Section__c+'_0001').add(qBody);
                newBodyCounter++;
            }
                
            //Set Body data 0001 here
            if(bodyMap.containsKey(asai.AAS_Assessment_Schema_Section__c+'_0002')){
                //Set the body value for the dynamic part
                assiBodyWrapper schBody = new assiBodyWrapper(asai.Name, newBodyCounter);
                assiBodyWrapper qBody = new assiBodyWrapper(String.valueOf(newBodyCounter + 1), newBodyCounter);
                
                bodyMap.get(asai.AAS_Assessment_Schema_Section__c+'_0002').add(schBody);
                bodyMap.get(asai.AAS_Assessment_Schema_Section__c+'_0002').add(qBody);
                newBodyCounter++;
            }else{
                newBodyCounter = 1; //This newHeaderCounter is to reset the value back to 1 again for earch different table
                List<assiBodyWrapper> assiBodyWrapperList = new List<assiBodyWrapper>();
                
                //Seperate the Exam & (BB and Workshop) value
                if(asai.AAS_Assessment_Schema_Section__r.RecordType.DeveloperName.equals('Exam_Assessment') || asai.AAS_Assessment_Schema_Section__r.RecordType.DeveloperName.equals('Supp_Exam_Assessment')){
                    assiBodyWrapper examNum = new assiBodyWrapper('0002', 1);
                    assiBodyWrapperList.add(examNum);
                    bodyMap.put(asai.AAS_Assessment_Schema_Section__c+'_0002', assiBodyWrapperList);
                }else{
                    assiBodyWrapper memberId = new assiBodyWrapper('0002', 1);
                    assiBodyWrapperList.add(memberId);
                    
                    assiBodyWrapper custIdBody = new assiBodyWrapper('0022122', 2);
                    assiBodyWrapperList.add(custIdBody);
                    
                    bodyMap.put(asai.AAS_Assessment_Schema_Section__c+'_0002', assiBodyWrapperList);
                }
                
                assiBodyWrapper schBody = new assiBodyWrapper(asai.Name, newBodyCounter);
                assiBodyWrapper qBody = new assiBodyWrapper(String.valueOf(newBodyCounter + 1), newBodyCounter);
                
                //Set initial body value
                bodyMap.get(asai.AAS_Assessment_Schema_Section__c+'_0002').add(schBody);
                bodyMap.get(asai.AAS_Assessment_Schema_Section__c+'_0002').add(qBody);
                newBodyCounter++;
            }
               
        }

    }
    
    //This wrapper is used to display the header value
    public class assiHeaderWrapper{
        public String headerValue {get; set;}
        public Integer counterW {get; set;}
        public Integer markW {get; set;}
        
        public assiHeaderWrapper(String hValue, Integer c){
            headerValue = hValue;
            counterW = c + 1;
            markW = counterW * 2;
        }
    }
    
    //This wrapper is used to display the body value
    public class assiBodyWrapper{
        public String BodyValue {get; set;}
        public Integer counterW {get; set;}
        public Integer markW {get; set;}
        
        public assiBodyWrapper(String bValue, Integer c){
            BodyValue = bValue;
            counterW = c + 1;
            markW = counterW * 2;
        }
    }
    
}