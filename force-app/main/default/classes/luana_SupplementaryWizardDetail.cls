/*
    Developer: WDCi (KH)
    Development Date: 01/08/2016
    Task #: Supplementary wizard - Detail controller
     
*/

public without sharing class luana_SupplementaryWizardDetail {

    
    luana_SupplementaryWizardController stdController;
    
    public luana_SupplementaryWizardDetail(luana_SupplementaryWizardController stdController) {
        this.stdController = stdController;
    }
    
    public Attachment getTermAndCond(){
        return getTermAndConAttachmentFile(false);
    }
    
    public Attachment getTermAndConAttachmentFile(Boolean hasBody){

        for(Terms_and_Conditions__c termAndCon : [Select Id, Name from Terms_and_Conditions__c Where Name =: luana_EnrolmentConstants.PRODUCTTYPE_CAMODULE order by createddate desc limit 1]){
          if(hasBody){
              for(Attachment att : [Select Id, Name, Body from Attachment Where ParentId =: termAndCon.Id order by createddate desc limit 1]){
                return att;
              }
          }else{
              for(Attachment att : [Select Id, Name from Attachment Where ParentId =: termAndCon.Id order by createddate desc limit 1]){
                return att;
              }
          }
        }
        
        return new Attachment();
    }
    
    public PageReference doBack(){
        PageReference backSPPage = Page.luana_MyEnrolmentView;
        backSPPage.getParameters().put('spid',stdController.selectedSP.Id);
        
        return backSPPage;
    }
    
    public PageReference doNext(){
        Boolean hasError = false;    
        
        if(stdController.workingStudentProgram.Accept_terms_and_conditions__c == false){
            hasError = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please accept the terms and conditions before continue.')); 
        }
        if(stdController.selectedExamLocationId == null){
            hasError = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please select your exam location preference before continue.')); 
        }
              
        if(!hasError){
            stdController.selectedExamLocation = stdController.examLocationMap.get(stdController.selectedExamLocationId).Delivery_Location__r.LuanaSMS__Delivery_Location_Name__c;
            
            PageReference nextSPPage = Page.luana_SupplementaryWizardSummary;
            nextSPPage.getParameters().put('spid',stdController.selectedSP.Id);
            
            return nextSPPage;
        }
        return null;
    }
    
    public PageReference doEnrol(){
        Boolean hasError = false;  
        Savepoint sp = Database.setSavepoint();
        try{
            if(stdController.workingStudentProgram.Accept_terms_and_conditions__c == false){
            hasError = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please accept the terms and conditions before continue.')); 
            }
            if(stdController.selectedExamLocationId == null){
                hasError = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please select your exam location preference before continue.')); 
            }
        
            if(!hasError){
                
                stdController.updateCurrentSP();
                
                PageReference viewSPPage = Page.luana_MyEnrolmentView;
                viewSPPage.getParameters().put('spid',stdController.selectedSP.Id);
                return viewSPPage;
            }
            
            return null;
            
        }Catch(Exception exp){
            Database.rollback(sp);
        }
        return null;
        
    }

}