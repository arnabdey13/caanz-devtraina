@isTest
public class MyProfileButtonsTestClass { 
    
    private static Account getFullMemberAccountObject(){
        Account FullMemberAccountObject = TestObjectCreator.createFullMemberAccount();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        return FullMemberAccountObject;
    }
    
    static testMethod void loadTestMethod(){
        Account FullMemberAccountObject = getFullMemberAccountObject();
        Test.startTest();
        insert FullMemberAccountObject; // Future method
        Test.stopTest();
        // Check Results
        Id FullMemberContactId = [Select id FROM Contact WHERE AccountId =: FullMemberAccountObject.Id].Id;
        List<User> User_List = [Select FederationIdentifier FROM User WHERE contactId=:FullMemberContactId];
        System.assertEquals(1, User_List.size(), 'User_List.size' );
        System.assertNotEquals(null, User_List[0].FederationIdentifier, 'FederationIdentifier' );
        
        system.runAs(User_List[0]) {
            Map<String, Object> properties = MyProfileButtonsClass.load();
            
            setAnswer(properties,'PersonEmail','jai.chaturvedi@gmail.com');
            MyProfileButtonsClass.save(properties);
            
            setAnswer(properties,'PersonHomePhone','0274063567');
            setAnswer(properties,'PersonOtherStreet', '8 Nelson Street');
            setAnswer(properties,'PersonOtherCity', 'Auckland');
            setAnswer(properties,'PersonOtherState', 'None');
            setAnswer(properties, 'PersonMailingPostalCode' ,'1010');
            setAnswer(properties,'Mailing_and_Residential_is_the_same__c',true);
            MyProfileButtonsClass.save(properties);
            
            setAnswer(properties, 'PersonBirthdate' ,'07/04/1994');
            MyProfileButtonsClass.save(properties);
            
        }
    }
    
    static testMethod void loadAndSaveTestMethod(){
        Account FullMemberAccountObject = getFullMemberAccountObject();
        Test.startTest();
        insert FullMemberAccountObject; // Future method
        Test.stopTest();
        // Check Results
        Id FullMemberContactId = [Select id FROM Contact WHERE AccountId =: FullMemberAccountObject.Id].Id;
        List<User> User_List = [Select FederationIdentifier FROM User WHERE contactId=:FullMemberContactId];
        System.assertEquals(1, User_List.size(), 'User_List.size' );
        System.assertNotEquals(null, User_List[0].FederationIdentifier, 'FederationIdentifier' );
        
        system.runAs(User_List[0]) {
            Map<String, Object> properties = MyProfileButtonsClass.load();            
            setAnswer(properties,'PersonMobilePhone', '4535135452');
            setAnswer(properties,'PersonOtherState', '');
            MyProfileButtonsClass.save(properties);
            
        }
    }
    
    // emulate how the Lightning code handles onChange events and keep tests DRY 
    private static void setAnswer(Map<String, Object> properties, String field, Object value) {
        Map<Object, Object> record = (Map<Object, Object>) properties.get('RECORD');
        properties.put('DIRTY',new Map<Object, Object>{field => true});
        if (EditorPageUtils.isPropertyField(field)) {
            properties.put(field, value);        
        } else {
            record.put(field, value);
        }
    }
}