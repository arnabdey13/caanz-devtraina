public class UserTriggerHandlerClass {
    //public static String suffixString = [SELECT DeveloperName FROM CAANZ_Setting__mdt WHERE DeveloperName = 'caanz' LIMIT 1].DeveloperName;
    
    /*
     * Before Insert method. Contains logic for before insert operation.
     */
    public static void onBeforeInsert(List<User> UserList) {
		//Write before insert logih here.
		/*for(User userRec : UserList){
            if(userRec.IsActive && userRec.IsPortalEnabled && UserInfo.getProfileId() == ProfileCache.getId('NZICA Community Login User')){
                userRec.Username = userRec.Email ; //+ '.' + suffixString ;        
            }
        }*/
    }
    
    /*
     * Before Update method. Contains logic for before update operation.
     */
    public static void onBeforeUpdate(List<User> UserList, Map<Id, User> mapUserOld) {
        Map<Id, Boolean> userIdisAllowedMap = UserObjectUtilityClass.checkProfileAccess(userList, 'CAANZCustomerCommunityProfile') ;
        for(User userRec : UserList){
            if(userRec.IsActive && userRec.IsPortalEnabled && userRec.Email != mapUserOld.get(userRec.Id).Email && userIdisAllowedMap.keySet().contains(userRec.Id)){
                userRec.Username = userRec.Email ;// + '.' + suffixString ;        
            }
        }
    }
    
    /*
     * After Insert method. Contains logic for after insert operation.
     */
    public static void onAfterInsert(List<User> UserList) {
        //UserObjectUtilityClass.updateUserName(UserList, suffixString) ;
        //UserObjectUtilityClass.updateContactEmail(UserList, null, true) ;
    }
    
    /*
     * After Update method. Contains logic for after update operation.
     */
    public static void onAfterUpdate(List<User> UserList, Map<Id, User> mapUserOld) {
		//UserObjectUtilityClass.updateContactEmail(UserList, mapUserOld, false) ;        
    }
    
}