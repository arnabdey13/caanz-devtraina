@IsTest(seeAllData=false)
private class RegisterOrLoginServiceTest {

    private static final String OK = String.valueOf(RegisterOrLoginService.RegistrationState.SUCCESS);
    private static final String REG = String.valueOf(RegisterOrLoginService.RegistrationState.REGISTER);
    private static final String NRE = String.valueOf(RegisterOrLoginService.RegistrationState.NON_REPEATABLE_ERROR);
    private static final String SREG = 'SELF_REGISTERED';
    
    
    private static final String fakeDestination = '/a00p0000004dMiGAAU?id=a00p0000004dMiG&nooverride=1';

    static testMethod void testNewNonMember() {

        // page loads via the load method
        RegisterOrLoginService.load();

        String uniqueEmail = getUniqueEmail();

        // then the user provides a new email address which is searched
        System.assertEquals(REG, RegisterOrLoginService.checkUser(uniqueEmail),
                'New emails should put the form into the REGISTER state');

        Environment_Ids__c ownerSetting = new Environment_Ids__c(
                Name = 'Non_Member_Registration',
                Id__c = UserInfo.getUserId());
        insert ownerSetting;
     
        // next they provide the fields for registration and call save
        System.assertEquals(OK, RegisterOrLoginService.save(
                uniqueEmail, 'Mr.', 'Don', 'Trumpet', 'Australia', fakeDestination, true, true),
                'save call succeeds if email address is unique i.e. form enters SUCCESS state');

        // user might wait too long and the key expires so they resend the email
        System.debug(RegisterOrLoginService.resendEmail(uniqueEmail));

        // after clicking the email link the user sets their password using the CustomPasswordSetPage link
        setPasswordMock(uniqueEmail);

       //System.assertEquals(NRE, RegisterOrLoginService.save(
       //         uniqueEmail, 'Mr.', 'Don', 'Trumpet', 'Australia', null, true, true),
        //                   'duplicate email returns an non-repeatable error since the form should not allow it');

        System.debug(RegisterOrLoginService.login(uniqueEmail, 'fakepassword', ''));

    }

    static testMethod void testExistingMember() {
        Id memberRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Member').getRecordTypeId();
        RecordType memberRecordType = [SELECT Id, Name FROM RecordType where Id = :memberRecordTypeId];
        Id nonMemberRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non Member').getRecordTypeId();
        RecordType nonMemberRecordType = [SELECT Id, Name FROM RecordType where Id = :nonMemberRecordTypeId];
        Id applicantRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Applicant').getRecordTypeId();
        RecordType applicantRecordType = [SELECT Id, Name FROM RecordType where Id = :applicantRecordTypeId];

        Account account = new Account(RecordType = memberRecordType);
        User existingMember = new User();
        System.assertEquals(RegisterOrLoginService.RegistrationState.INACTIVE,
                RegisterOrLoginService.existingMemberStatus(existingMember, account),
                'Members without active status set form into INACTIVE state i.e. tell user to contact support');

        existingMember.IsActive = true;
        account.Status__c = 'Active';
        System.assertEquals(RegisterOrLoginService.RegistrationState.LOGIN,
                RegisterOrLoginService.existingMemberStatus(existingMember, account),
                'Active members set the form into login mode');

        account.RecordType = nonMemberRecordType;
        System.assertEquals(RegisterOrLoginService.RegistrationState.NON_REPEATABLE_ERROR,
                RegisterOrLoginService.existingMemberStatus(existingMember, account),
                'Non members with a null Reg Key tell user something broke and do not try again');

        account.Registration_Temporary_Key__c = 'SELF_REGISTERED';
        System.assertEquals(RegisterOrLoginService.RegistrationState.LOGIN,
                RegisterOrLoginService.existingMemberStatus(existingMember, account),
                'Non members already registered set form into login mode');

        account.Registration_Temporary_Key__c = 'EXPIRED';
        System.assertEquals(RegisterOrLoginService.RegistrationState.EXPIRED,
                RegisterOrLoginService.existingMemberStatus(existingMember, account),
                'Non members who take too long to register will set message offering to resend confirmation email');

        account.RecordType = applicantRecordType;
        System.assertEquals(RegisterOrLoginService.RegistrationState.LOGIN,
                RegisterOrLoginService.existingMemberStatus(existingMember, account),
                'Applicants with a null Reg Key and Status equal active and user IsActive = true should be able to login');
        existingMember.IsActive = false;
        System.assertEquals(RegisterOrLoginService.RegistrationState.INACTIVE,
                RegisterOrLoginService.existingMemberStatus(existingMember, account),
                'Applicants with a null Reg Key and either Status not equal to active and user IsActive = false should see an message saying inactiven');
        existingMember.Email = 't@caanz.com';
        System.assertEquals(NRE,
                RegisterOrLoginService.resetPW(existingMember.Email),
                'Password reset should fail due to user not being inserted');

    }

    // mock the set password process to avoid the need for the @future method that creates a user
    private static void setPasswordMock(String email) {
        Account a = [select Registration_Temporary_Key__c from Account where PersonEmail = :email];
        a.Registration_Temporary_Key__c = SREG;
        update a;
    }

    private static String getUniqueEmail() {
        return 'donald' + DateTime.now().getTime() + '@tr.com';
    }

    @TestSetup
    private static void createCustomSettings() {
        Environment_Ids__c ownerSetting = new Environment_Ids__c(
                Name = 'Non_Member_Registration',
                Id__c = UserInfo.getUserId());
        insert ownerSetting;
    }
}