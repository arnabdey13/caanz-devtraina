global with sharing class PortalLogin {
    global String username {get; set;}
    global String password {get; set;}
	global String param {get; set;}   
   	global PortalLogin () {
   		//username = 'gilbert.deleon@nzica.portal';
   		//password = 'acmegx1024';
   	}
    global PageReference login() {
        return Site.login(username, password, '/home/home.jsp');
    }
    
    global PageReference redirect(){
    	PageReference pref = new PageReference('/login');
    	pref.setRedirect(true);
    	return pref;
    }
    global PageReference register() {
    	PageReference pref = new PageReference('/PortalSelfRegistration?type=' + param);
    	pref.setRedirect(true);
    	return pref;
    }   	
}