/*
 * @changehistory
 * Case 00008251 15/08/2019 Lean - Fixed unique enrolment key format to use contact id instead of account id
 * Case 00008253 15/08/2019 Lean - Fixed attendance creation to avoid creating unrelated attendance with wrong session for student
 */

public class LuanaAutoSessionAssignment {

    public static void autoAssignStudents(List<LuanaSMS__Student_Program__c> spList, List<LuanaSMS__Course_Session__c> csessList) {
    
        List<LuanaSMS__Attendance2__c> attList = new List<LuanaSMS__Attendance2__c>();
        
        // LKoh :: 09-05-2017
        system.debug('csessList: ' +csessList);
        
        for(LuanaSMS__Student_Program__c sp : spList) {
            for(LuanaSMS__Course_Session__c csess : csessList) {
                
                if(sp.LuanaSMS__Course__c == csess.LuanaSMS__Course__c){
                    LuanaSMS__Attendance2__c att = new LuanaSMS__Attendance2__c();
                    att.LuanaSMS__Contact_Student__c = sp.LuanaSMS__Contact_Student__c;
                    att.LuanaSMS__Student_Program__c = sp.Id;
                    att.LuanaSMS__Session__c = csess.LuanaSMS__Session__r.Id;
                    att.Unique_Enrolment_Key__c = String.valueOf(sp.LuanaSMS__Contact_Student__c).substring(0, 15) + '|' + csess.LuanaSMS__Session__r.Topic_Key__c;
                    System.debug('Unique Key: '+ att.Unique_Enrolment_Key__c);          
                    attList.add(att);
                }
            }
        }
        upsert attList Unique_Enrolment_Key__c;
    }
}