@isTest
private class DecideSocialAccountsVisibilityTest {
    static testMethod void testDecideSocialAccountsVisibilityMethod1() {
        Boolean testBoolean1 = DecideSocialAccountsVisibility.displaylinktofacebook();
        Boolean testBoolean2 = DecideSocialAccountsVisibility.displaylinkedinlinkapex();
        Boolean testBoolean3 = DecideSocialAccountsVisibility.unlinkUser();
        Boolean testBoolean4 = DecideSocialAccountsVisibility.unlinkLinkedinUserCallApex();
    }/*
    static testMethod void testDecideSocialAccountsVisibilityMethod2() {
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = System.now().millisecond() + 'test2@test.com',
        Alias = 'batman',
        Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);

        //Create account
        Account portalAccount1 = new Account(
        Name = 'TestAccount',
        BillingStreet = 'testbillingstreet',
        OwnerId = portalAccountOwner1.Id
        );
        System.runAs(new User(Id = portalAccountOwner1.id)) {
           Database.insert(portalAccount1);
        }
    
        
        //Create contact
        Contact contact1 = new Contact(
        FirstName = 'Test',
            Lastname = 'McTesty',
        AccountId = portalAccount1.Id,
            Email = System.now().millisecond() + 'test@test.com'
        );
        System.runAs(new User(Id = UserInfo.getUserId())) {
           Database.insert(contact1);
        }
    
        //Create user
        Profile portalProfile = [Select Id from Profile where name = 'NZICA Community Login User' limit 1];
        User user1 = new User(
        Username = 'standarduser@testorg.com',
        ContactId = contact1.Id,
        ProfileId = portalProfile.Id,
        Alias = 'test123',
        Email = 'test12345@test.com',
        EmailEncodingKey = 'UTF-8',
        LastName = 'McTesty',
        CommunityNickname = 'test12345',
        TimeZoneSidKey = 'America/Los_Angeles',
        LocaleSidKey = 'en_US',
        LanguageLocaleKey = 'en_US'
        );
        System.runAs(new User(Id = portalAccountOwner1.id)) {
           insert user1;
        }
        
        //AuthProvider au = new AuthProvider();
        //au.DeveloperName = 'testauth';
        //au.ProviderType = 'Facebook';
        //au.RegistrationHandlerId = [Select id from ApexClass where name = 'FacebookHandler'].id;
        //insert au;
        
        LinkedinHandler handler = new LinkedinHandler();
        Auth.UserData sampleData = new Auth.UserData('testId', 'testFirst', 'testLast',
        'testFirst testLast', 'standarduser@testorg.com', null, 'testuserlong', 'en_US', 'facebook',
        null, new Map<String, String>{'language' => 'en_US'});
        User u1 = handler.createUser(null, sampleData);
        update(u1);
        String uid = u1.id;
    
        sampleData = new Auth.UserData('testNewId', 'testNewFirst', 'testNewLast',
        'testNewFirst testNewLast', 'standarduser@testorg.com', null, 'testnewuserlong', 'en_US', 'facebook',
        null, new Map<String, String>{});
        handler.updateUser(uid, null, sampleData);
    
        User updatedUser = [SELECT userName, email, firstName, lastName, alias FROM user WHERE id=:uid];
        system.debug('all users: '+updatedUser);
        List<ThirdPartyAccountLink> tpalinks = [Select id, provider from ThirdPartyAccountLink];
        system.debug('all tpa: '+tpalinks);
        
        System.runAs(new User(Id = u1.id)) {
            test.starttest();
            Boolean testBoolean1 = DecideSocialAccountsVisibility.displaylinktofacebook();
            Boolean testBoolean2 = DecideSocialAccountsVisibility.displaylinkedinlinkapex();
            Boolean testBoolean3 = DecideSocialAccountsVisibility.unlinkUser();
            Boolean testBoolean4 = DecideSocialAccountsVisibility.unlinkLinkedinUserCallApex();
            test.stoptest();
        }
        
    }*/
}