/*
    Webserivce callout class, calls Boomi with Order details
    ======================================================
    Changes:
		Feb 2015	Davanti			Created
		Mar 2017	Davanti - RN	Updated for PASA CR001, include Order Description field in query and outbound payload
*/
public with sharing class BoomiMockCallOut {    
    @future(callout=true)
    public static void doCallout(Id orderId){
        system.debug('### Order Id: ' + orderId);
        String response = '';
        List<Order> orderList = [Select Id, Account.Salutation, Account.FirstName, Account.LastName, Account.PersonEmail,
                                Account.PersonMailingStreet, Account.PersonMailingCity, Account.PersonMailingState,
                                Account.PersonMailingPostalCode, Account.PersonMailingCountry, Account.Customer_ID__c,
                                Account.Member_ID__c, Account.Member_Of__c, Account.Membership_Type__c, Account.Mailing_Company_Name__c,
                                OrderNumber, Account.Affiliated_Branch_Country__c, 
                                Description, Account.Is_Migration_Agent__c, //PASA CR001 Added Description and Is_Migration_Agent
                                (Select Quantity, NetSuite_Product_ID__c From OrderItems)
                                From Order where Id=:orderId];
        if(orderList.size() > 0) {
            Order order = orderList.get(0);
            //fix for null argument
            String PersonMailingStreetX = '';
            if(order.Account.PersonMailingStreet != null)
                PersonMailingStreetX = order.Account.PersonMailingStreet;
            String PersonMailingCityX = '';
            if(order.Account.PersonMailingCity != null)
                PersonMailingCityX = order.Account.PersonMailingCity;
            String PersonMailingStateX = '';
            if(order.Account.PersonMailingState != null)
                PersonMailingStateX = order.Account.PersonMailingState;
            String PersonMailingPostalCodeX = '';
            if(order.Account.PersonMailingPostalCode != null)
                PersonMailingPostalCodeX = order.Account.PersonMailingPostalCode;
            String PersonMailingCountryX = '';
            if(order.Account.PersonMailingCountry != null)
                PersonMailingCountryX = order.Account.PersonMailingCountry;

            String Member_IDX = '';
            if(order.Account.Member_ID__c != null)
                Member_IDX = order.Account.Member_ID__c;
            String Mailing_Company_NameX = '';
            if(order.Account.Mailing_Company_Name__c != null)
                Mailing_Company_NameX = order.Account.Mailing_Company_Name__c;                
       
			//LCA-345
       		String Affiliated_Branch_CountryX = '';
       		if(order.Account.Affiliated_Branch_Country__c != null)
         		Affiliated_Branch_CountryX = order.Account.Affiliated_Branch_Country__c;
            
            // PASA CR001
            string Order_DescriptionX = (order.Description != null) ? order.Description : ''; 
            string Is_Migration_AgentX = order.Account.Is_Migration_Agent__c ? 'true' : 'false';
            system.debug('### Order_DescriptionX: ' + Order_DescriptionX);

            DOM.Document doc = new DOM.Document();
            dom.XmlNode envelope = doc.createRootElement('Order', null, null);

            dom.XmlNode orderNumber = envelope.addChildElement('OrderNumber', null, null).addTextNode(order.OrderNumber);
            dom.XmlNode owner = envelope.addChildElement('Owner', null, null);

            dom.XmlNode account = envelope.addChildElement('Account', null, null);            
            dom.XmlNode Salutation = account.addChildElement('Salutation', null,null).addTextNode(order.Account.Salutation);
            dom.XmlNode FirstName = account.addChildElement('FirstName', null,null).addTextNode(order.Account.FirstName);
            dom.XmlNode LastName = account.addChildElement('LastName', null,null).addTextNode(order.Account.LastName);
            dom.XmlNode PersonEmail = account.addChildElement('PersonEmail', null,null).addTextNode(order.Account.PersonEmail);
            dom.XmlNode PersonMailingStreet = account.addChildElement('PersonMailingStreet', null,null).addTextNode(PersonMailingStreetX);
            dom.XmlNode PersonMailingCity = account.addChildElement('PersonMailingCity', null,null).addTextNode(PersonMailingCityX);
            dom.XmlNode PersonMailingState = account.addChildElement('PersonMailingState', null,null).addTextNode(PersonMailingStateX);
            dom.XmlNode PersonMailingPostalCode = account.addChildElement('PersonMailingPostalCode', null,null).addTextNode(PersonMailingPostalCodeX);
            dom.XmlNode PersonMailingCountry = account.addChildElement('PersonMailingCountry', null,null).addTextNode(PersonMailingCountryX);
            dom.XmlNode Customer_ID = account.addChildElement('Customer_ID__c', null,null).addTextNode(order.Account.Customer_ID__c);
            dom.XmlNode Member_ID = account.addChildElement('Member_ID__c', null,null).addTextNode(Member_IDX);
            dom.XmlNode Member_Of = account.addChildElement('Member_Of__c', null,null).addTextNode(order.Account.Member_Of__c);
            dom.XmlNode Membership_Type = account.addChildElement('Membership_Type__c', null,null).addTextNode(order.Account.Membership_Type__c);
            dom.XmlNode Mailing_Company_Name = account.addChildElement('Mailing_Company_Name__c', null,null).addTextNode(Mailing_Company_NameX);
            dom.XmlNode Is_Migration_Agent = account.addChildElement('Is_Migration_Agent__c', null,null).addTextNode(Is_Migration_AgentX);
			                        
            //LCA-345
            dom.XmlNode Affiliated_Branch_Country = account.addChildElement('Affiliated_Branch_Country__c', null,null).addTextNode(Affiliated_Branch_CountryX);
                        
            dom.XmlNode orderProduct = envelope.addChildElement('OrderProduct', null, null);
            Integer qty = Integer.valueOf(order.OrderItems.get(0).Quantity);
            dom.XmlNode Quantity = orderProduct.addChildElement('Quantity', null, null).addTextNode(String.valueOf(qty));
            dom.XmlNode NS_Internal_ID = orderProduct.addChildElement('NS_Internal_ID__c', null, null).addTextNode(order.OrderItems.get(0).NetSuite_Product_ID__c);
			
            // PASA CR001
            dom.XmlNode Order_description = envelope.addChildElement('Description', null, null).addTextNode(Order_DescriptionX);
            
            /*
            Http h = new Http();
            
            HttpRequest req = new HttpRequest();
            //Custom Settings
            Service_Integrations__c boomiIntegration = Service_Integrations__c.getInstance('Boomi REST');  
            String url = boomiIntegration.Endpoint_URL__c; //'https://test.connect.boomi.com/ws/simple/upsertOrder_IUC02;boomi_auth=Q0FAY2hhcnRlcmVkYWNjb3VudGFudHNvZmF1cy04TlY0N0MuTDdBT0wzOmViOWViMTdhLTJjNGYtNGRhNi04NDBkLTg4NWQ0YTBmMDNkYg==';
            req.setEndpoint(url);
            req.setMethod('GET');
            req.setHeader('Content-Type', 'text/xml');            
            req.setBodyDocument(doc);
            req.setTimeout(120000);
        
            HttpResponse res = h.send(req);
            response = res.getBody();
            system.debug('## > ' + response);
            */
            if(!Test.isRunningTest()) {
                system.debug('### doc: ' + doc.toXMLString());
                 
                HttpResponse res = boomiCallout(doc);
                response = res.getBody();
                system.debug('## > ' + response);   
				
            }
        }

    }

    public static HttpResponse boomiCallout(DOM.Document doc) {
            Http h = new Http();
            
            HttpRequest req = new HttpRequest();
            //Custom Settings
            Service_Integrations__c boomiIntegration = Service_Integrations__c.getInstance('Boomi REST');  
            String url = boomiIntegration.Endpoint_URL__c; //'https://test.connect.boomi.com/ws/simple/upsertOrder_IUC02;boomi_auth=Q0FAY2hhcnRlcmVkYWNjb3VudGFudHNvZmF1cy04TlY0N0MuTDdBT0wzOmViOWViMTdhLTJjNGYtNGRhNi04NDBkLTg4NWQ0YTBmMDNkYg==';
            req.setEndpoint(url);
            req.setMethod('GET');
            req.setHeader('Content-Type', 'text/xml');            
            req.setBodyDocument(doc);
            req.setTimeout(120000);
        
            HttpResponse res = h.send(req);
            return res;        
    }
    
}