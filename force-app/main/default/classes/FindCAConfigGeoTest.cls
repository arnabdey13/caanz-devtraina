/*
    Test class for FindCAConfigGeo.cls
    ======================================================
    Changes:
        October 2017    Andrew Kopec  Created
*/
@isTest
public class FindCAConfigGeoTest {


static testMethod void testDoGet() {
        
    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();
  
    //req.requestURI = 'https://charteredaccountantsanz--preprod.cs5.my.salesforce.com/services/apexrest/FindCA/v1.0/Config/Geo';
    req.requestURI = '/services/apexrest/FindCA/v1.0/Config/Geo';  
    req.httpMethod = 'GET';
    req.addParameter('BranchCountry','Australia');
        
    RestContext.request = req;
    RestContext.response = res;    
        
    List<Region_State_Setting__mdt> results = FindCAConfigGeo.doGet();
    //System.assertEquals(1, results.size());

    System.debug(results);
        
  }

}