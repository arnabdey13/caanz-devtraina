/**
    Developer: WDCi (kh)
    Development Date:14/04/2016
    Task: Luana Test class for luana_RequestController_Test
    
    Change History
    LCA-921 23/08/2019 WDCi - KH: Add person account email
**/
@isTest(seeAllData=false)
private class luana_RequestController_Test {
    
    public static User memberUser {get; set;}
    public static Luana_DataPrep_Test dataPrep;
    public static luana_CommUserUtil userUtil {get; set;} 
    public static Account bussinessAcc {get; set;}
    
    public static LuanaSMS__Student_Program__c stuProgm {get; set;}
    public static LuanaSMS__Course__c course {get; set;}
    public static LuanaSMS__Delivery_Location__c devLocation {get; set;}
    
    public static List<Attachment> attchs {get; set;}
    
    private static String classNamePrefixLong = 'luana_RequestController_Test';
    private static String classNamePrefixShort = 'lrc';
    private static map<String, Id> commProfIdMap {get; set;}
    public static Account memberAccount {get; set;}
    
    //LCA-921
    public static void initial(){
        //initialize
        dataPrep = new Luana_DataPrep_Test();
        
        //Create all the custom setting
        insert dataPrep.prepLuanaExtensionSettingCustomSettings();
        insert dataPrep.createLuanaConfigurationCustomSetting();
        
        //Create user with Member and Employer community access
        memberAccount = dataPrep.generateNewApplicantAcc('Joe_' + classNamePrefixShort, classNamePrefixLong, 'Full_Member');
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.PersonEmail = 'joe_1_' +classNamePrefixShort+'@gmail.com';//LCA-921
        memberAccount.Communication_Preference__c= 'Home Phone';
        memberAccount.PersonHomePhone= '1234';
        memberAccount.PersonOtherStreet= '83 Saggers Road';
        memberAccount.PersonOtherCity='JITARNING';
        memberAccount.PersonOtherState='Western Australia';
        memberAccount.PersonOtherCountry='Australia';
        memberAccount.PersonOtherPostalCode='6365';  
        insert memberAccount;
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
    }
    
    public static void prepareSampleEduData(){
    
        //LCA-921
        memberUser = [Select Id, Email, FirstName, LastName, UserName, Name, ContactId from User Where Email =: 'joe_1_' + classNamePrefixShort+'@gmail.com' limit 1];
        //memberUser = testDataGenerator.generateNewApplicantUser(classNamePrefixLong, classNamePrefixshort, memberAccount, commProfIdMap.get('Customer Community Login User'));
        //insert memberUser;
        
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
       
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defEmployerPermSetId.value__c));
        list<PermissionSetAssignment> permissAssign = [Select Id from PermissionSetAssignment where AssigneeId =: memberUser.Id AND (PermissionSetId =: defMemberPermSetId.value__c OR PermissionSetId =: defEmployerPermSetId.value__c)];
        System.runAs(memberUser){
            if(permissAssign == null)
                insert psas;
        }        
        
        userUtil = new luana_CommUserUtil(memberUser.Id);
        
        //Create traning org
        LuanaSMS__Training_Organisation__c trainingOrg = dataPrep.createNewTraningOrg(classNamePrefixLong, classNamePrefixLong, classNamePrefixShort, 'St 123', 'Wollonggong', '2244');
        insert trainingOrg;

        //Create program
        LuanaSMS__Program__c progrm = dataPrep.createNewProgram(classNamePrefixShort, classNamePrefixLong, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        insert progrm;
        
        //Create Product
        List<Product2> prodList = new List<Product2>();
        prodList.add(dataPrep.createNewProduct('FIN_AU_Test', 'AU0001'));
        prodList.add(dataPrep.createNewProduct('FIN_NZ_Test', 'NZ0001'));
        prodList.add(dataPrep.createNewProduct('FIN_INT_Test', 'INT0001'));
        insert prodList;
        
        //Create Program Offering
        LuanaSMS__Program_Offering__c po = dataPrep.createNewProgOffering('PO_' + classNamePrefixLong, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), progrm.Id, trainingOrg.Id, prodList[0].Id,prodList[1].Id, prodList[2].Id, 1, 1);
        insert po;
        
        //Create course
        course = dataPrep.createNewCourse('Graduate Diploma of Chartered Accounting_Test', po.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Program'), 'Running');
        course.LuanaSMS__Allow_Online_Enrolment__c = true;
        insert course;
        
        //Set default enrolloment course id
        Luana_Extension_Settings__c defCourseEnrolmentId = new Luana_Extension_Settings__c(Name = 'Default_CA_Program_Course_Id', Value__c = course.Id);
        insert defCourseEnrolmentId;
        
        //Create Student Program
        stuProgm =  dataPrep.createNewStudentProgram(dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), userUtil.custCommConId, course.Id, 'Australia', 'In Progress');
        stuProgm.LuanaSMS__Enrolment_Date__c = System.today().addMonths(1);
        insert stuProgm;
        
        List<Terms_and_Conditions__c> tncs = dataPrep.createDefaultTNCs();
        insert tncs;
        
        attchs = new List<Attachment>();
        for(Terms_and_Conditions__c tnc: tncs){
            attchs.add(dataPrep.addAttachmentToParent(tnc.Id, classNamePrefixLong + '-attchment')); 
        }
        insert attchs;
    }
    
    public static testMethod void testRequestSCCtl() { 
        
        Test.startTest();
            initial();
        Test.stopTest();
        
        prepareSampleEduData();
        
        System.RunAs(memberUser){
            Case cas = new Case();
            Apexpages.StandardController sc = new Apexpages.standardController(cas);
            
            luana_RequestController rc = new luana_RequestController(sc);
            
            rc.uploadAttToCase(cas);
            
            rc.getMyModuleEnrolments();
            rc.getMyInProgressModuleEnrolments();
            rc.getModulePOs();
            rc.getSPWList();
            
            rc.SaveSC();
        }
    }
    
    public static testMethod void testRequestMECtl() { 

        Test.startTest();
            initial();
        Test.stopTest();
        prepareSampleEduData();
        
        System.RunAs(memberUser){
            Case cas = new Case();
            cas.Accept_Terms_And_Condition__c = true;
            Apexpages.StandardController sc = new Apexpages.standardController(cas);
            
            luana_RequestController rc = new luana_RequestController(sc);
            rc.cs.Accept_Terms_And_Condition__c = true;
            
            rc.uploadAttToCase(cas);
            rc.SaveME();
        }
    }
    
    public static testMethod void testRequestDWCtl() { 
        
        Test.startTest();
            initial();
        Test.stopTest();
        
        prepareSampleEduData();
        
        System.RunAs(memberUser){
        
            Case cas = new Case();
            cas.Accept_Fee__c = true;
            cas.Accept_Terms_And_Condition__c = true;
            Apexpages.StandardController sc = new Apexpages.standardController(cas);
            
            luana_RequestController rc = new luana_RequestController(sc);
            
            rc.additionalAtt1 = attchs[0];
            rc.additionalAtt2 = attchs[1];
            rc.additionalAtt3 = attchs[2];
            rc.additionalAtt4 = attchs[3];
            
            rc.cs.Accept_Fee__c = true;
            rc.cs.Accept_Terms_And_Condition__c = true;
            rc.SaveDW();
            rc.uploadAttToCase(cas);
        
        }
    }
    
    public static testMethod void testRequestOOAECtl() { 
        
        Test.startTest();
            initial();
        Test.stopTest();
        prepareSampleEduData();
        
        System.RunAs(memberUser){
        
            Case cas = new Case();
            
            Apexpages.StandardController sc = new Apexpages.standardController(cas);
            
            luana_RequestController rc = new luana_RequestController(sc);
             
            rc.att.Name='Unit Test Attachment';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            rc.att.body=bodyBlob;
            
            rc.SaveOOAE();
            
            rc.uploadAttToCase(cas);
        }
    }
    
    public static testMethod void testRequestRACtl() { 
        
        Test.startTest();
            initial();
        Test.stopTest();
        prepareSampleEduData();

        System.RunAs(memberUser){
            Case cas = new Case();
            Apexpages.StandardController sc = new Apexpages.standardController(cas);
            
            luana_RequestController rc = new luana_RequestController(sc);
            
            rc.uploadAttToCase(cas);
            rc.SaveRA();
        
            // Additional components added
            rc.getDiscontinuationAdditionalInfo();
            rc.getDiscontinuationTNC();
            rc.getExemptionAdditionalInfo();
            rc.getExemptionTNC();
            rc.getCompetencyAreaTNC();            
        }
    }
}