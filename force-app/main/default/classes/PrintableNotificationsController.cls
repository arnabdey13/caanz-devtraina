public with sharing class PrintableNotificationsController {

    public Integer year {
        get {
            if (year == null) {
                String yearParam = ApexPages.currentPage().getParameters().get('y');
                if (yearParam == null) {
                    year = EditorNotificationsService.getCurrentYear();
                } else {
                    year = EditorNotificationsService.getCurrentYear()-1;
                }
            }
            return year;
        }
        set;
    }

    // these properties are not covered in tests because the rely on the test users account id
    public Questionnaire_Response__c response {
        get {
            if (response == NULL) { response = getAccountResponse(EditorPageUtils.getUsersAccountId());}
            return response;
        }
        set;
    }
    public List<Response> answers { get { return getAnswers(response);} }

    @TestVisible
    private Questionnaire_Response__c getAccountResponse(Id accountId) {
        List<Response> result = new List<Response>();

        List<Questionnaire_Response__c> qs = [
                SELECT Id, Name, Account__c, Date__c, Lock__c, Residential_Country__c, Status__c, Year__c,
                        Answer_1__c, Answer_2__c, Answer_3__c, Answer_4__c, Answer_5__c, Answer_6__c, Answer_7__c, Answer_8__c, Answer_9__c, Answer_10__c,
                        Answer_11__c, Answer_12__c, Answer_13__c, Answer_14__c, Answer_15__c, Answer_16__c, Answer_17__c, Answer_18__c, Answer_19__c, Answer_20__c,
                        Answer_21__c, Answer_22__c, Answer_23__c, Answer_24__c, Answer_25__c, Answer_26__c, Answer_27__c, Answer_28__c, Answer_29__c, Answer_30__c,
                        Answer_31__c, Answer_32__c, Answer_33__c, Answer_34__c, Answer_35__c, Answer_36__c, Answer_37__c, Answer_38__c, Answer_39__c, Answer_40__c,
                        Answer_41__c, Answer_42__c, Answer_43__c, Answer_44__c, Answer_45__c, Answer_46__c, Answer_47__c, Answer_48__c, Answer_49__c, Answer_50__c,
                        Answer_51__c, Answer_52__c, Answer_53__c, Answer_54__c,
                        Question_1__c, Question_2__c, Question_3__c, Question_4__c, Question_5__c, Question_6__c, Question_7__c, Question_8__c, Question_9__c, Question_10__c,
                        Question_11__c, Question_12__c, Question_13__c, Question_14__c, Question_15__c, Question_16__c, Question_17__c, Question_18__c, Question_19__c, Question_20__c,
                        Question_21__c, Question_22__c, Question_23__c, Question_24__c, Question_25__c, Question_26__c, Question_27__c, Question_28__c, Question_29__c, Question_30__c,
                        Question_31__c, Question_32__c, Question_33__c, Question_34__c, Question_35__c, Question_36__c, Question_37__c, Question_38__c, Question_39__c, Question_40__c, 
                        Question_41__c, Question_42__c, Question_43__c, Question_44__c, Question_45__c, Question_46__c, Question_47__c, Question_48__c, Question_49__c, Question_50__c,
                        Question_51__c, Question_52__c, Question_53__c, Question_54__c
                FROM Questionnaire_Response__c
                WHERE Account__c = :accountId
                and Year__c = :String.valueOf(year)
        ];

        if (qs.size() != 1) {
            throw new SObjectException('No single questionnaire: '+qs.size());
        } else {
            return qs[0];
        }
    }

    @TestVisible
    private List<Response> getAnswers(Questionnaire_Response__c qr) {
        List<Response> result = new List<Response>();

        for (Integer i = 1; i < 55; i++) {  //changed 50 to 55 AELLIOTT

            String q = (String) qr.get('Question_'+i+'__c');
            String a = (String) qr.get('Answer_'+i+'__c');
            if (q != NULL) {
                // convert special tokens
                q = q.replaceAll('comma',',');
                q = q.replaceAll('lthan','<');
                q = q.replaceAll('gthan','>');
                q = q.replaceAll('<b>','');
                q = q.replaceAll('</b>','');

                result.add(new Response(i,q,a));
            }
        }

        return result;
    }
    
    public class Response {
        public String n {get;set;}
        public String q {get;set;}
        public String a {get;set;}
        public Response(Integer n, String q, String a) {
            this.n = String.valueOf(n);
            this.a = a;
            this.q = q;
        }
    }

}