/*
 * Porter Stemmer.
 *
 * Porter Stemmer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Porter Stemmer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Porter Stemmer.  If not, see <http://www.gnu.org/licenses/>.
 */

@isTest
public class RecognitionPorterStemmerTest {


   	private static void assertGetM(String word, Integer expectedM) {
        RecognitionPorterStemmer porterStemmer = new RecognitionPorterStemmer();
        String letterTypes = porterStemmer.getLetterTypes(word);
        Integer actualM = porterStemmer.getM(letterTypes);
        System.assertEquals(expectedM, actualM);
        List<String> fillers = new String[]{ String.valueOf(expectedM), String.valueOf(actualM)};
        System.debug(String.format('Expected ' + word + '-> {1} for step 1a,  got {2}', fillers));
        System.assertEquals(expectedM, actualM);
    }
    
    
    private static void assertStep1a(String input, String expectedOutput) {
        RecognitionPorterStemmer porterStemmer = new RecognitionPorterStemmer();
        String actualOutput = porterStemmer.stemStep1a(input);
        List<String> fillers = new String[]{ input, expectedOutput, actualOutput};
        System.debug(String.format('Expected {0} -> {1} for step 1a, got {2}', fillers));
        System.assertEquals( expectedOutput, actualOutput);
    }

    private static void assertStep1b(String input, String expectedOutput) {
        RecognitionPorterStemmer porterStemmer = new RecognitionPorterStemmer();
        String actualOutput = porterStemmer.stemStep1b(input);
        List<String> fillers = new String[]{ input, expectedOutput, actualOutput};
        System.debug(String.format('Expected {0} -> {1} for step 1b, got {2}', fillers));
        System.assertEquals( expectedOutput, actualOutput);
    }

    private static void assertStep1c(String input, String expectedOutput) {
        RecognitionPorterStemmer porterStemmer = new RecognitionPorterStemmer();
        String actualOutput = porterStemmer.stemStep1c(input);
        List<String> fillers = new String[]{ input, expectedOutput, actualOutput};
        System.debug(String.format('Expected {0} -> {1} for step 1c, but got {2}', fillers));
        System.assertEquals( expectedOutput, actualOutput);
    }

    private static void assertStep2(String input, String expectedOutput) {
        RecognitionPorterStemmer porterStemmer = new RecognitionPorterStemmer();
        String actualOutput = porterStemmer.stemStep2(input);
        List<String> fillers = new String[]{ input, expectedOutput, actualOutput};
        System.debug(String.format('Expected {0} -> {1} for step 2, got {2}', fillers));
        System.assertEquals( expectedOutput, actualOutput);
    }

    private static void assertStep3(String input, String expectedOutput) {
        RecognitionPorterStemmer porterStemmer = new RecognitionPorterStemmer();
        String actualOutput = porterStemmer.stemStep3(input);
        List<String> fillers = new String[]{ input, expectedOutput, actualOutput};
        System.debug(String.format('Expected {0} -> {1} for step 3, got {2}', fillers));
        System.assertEquals( expectedOutput, actualOutput);
    }

    private static void assertStep4(String input, String expectedOutput) {
        RecognitionPorterStemmer porterStemmer = new RecognitionPorterStemmer();
        String actualOutput = porterStemmer.stemStep4(input);
        List<String> fillers = new String[]{ input, expectedOutput, actualOutput};
        System.debug(String.format('Expected {0} -> {1} for step 4, got {2}', fillers));
        System.assertEquals( expectedOutput, actualOutput);
    }

    private static void assertStep5a(String input, String expectedOutput) {
        RecognitionPorterStemmer porterStemmer = new RecognitionPorterStemmer();
        String actualOutput = porterStemmer.stemStep5a(input);
        List<String> fillers = new String[]{ input, expectedOutput, actualOutput};
        System.debug(String.format('Expected {0} -> {1} for step 5a, but got {2}', fillers));
        System.assertEquals( expectedOutput, actualOutput);
    }

    private static void assertStep5b(String input, String expectedOutput) {
        RecognitionPorterStemmer porterStemmer = new RecognitionPorterStemmer();
        String actualOutput = porterStemmer.stemStep5b(input);
        List<String> fillers = new String[]{ input, expectedOutput, actualOutput};
        System.debug(String.format('Expected {0} -> {1} for step 5b, but got {2}', fillers));
        System.assertEquals( expectedOutput, actualOutput);
    }
    
    
    private static testMethod void test_getM(){        
        assertGetM('tr', 0);
        assertGetM('ee', 0);
        assertGetM('tree', 0);
        assertGetM('y', 0);
        assertGetM('by', 0);
        assertGetM('trouble', 1);
        assertGetM('oats', 1);
        assertGetM('trees', 1);
        assertGetM('ivy', 1);
        assertGetM('troubles', 2);
        assertGetM('private', 2);
        assertGetM('oaten', 2);
        assertGetM('orrery', 2);
    }


    private static testMethod void test_step1a() {        
        assertStep1a('caresses', 'caress');
        assertStep1a('ponies', 'poni');
        assertStep1a('ties', 'ti');
        assertStep1a('caress', 'caress');
        assertStep1a('cats', 'cat');
    }


    private static testMethod void  test_step1b() {
        assertStep1b('feed', 'feed');
        assertStep1b('agreed', 'agree');
        assertStep1b('plastered', 'plaster');
        assertStep1b('bled', 'bled');
        assertStep1b('motoring', 'motor');
        assertStep1b('sing', 'sing');
        assertStep1b('conflated', 'conflate');
        assertStep1b('troubled', 'trouble');
        assertStep1b('sized', 'size');
        assertStep1b('hopping', 'hop');
        assertStep1b('tanned', 'tan');
        assertStep1b('falling', 'fall');
        assertStep1b('hissing', 'hiss');
        assertStep1b('fizzed', 'fizz');
        assertStep1b('failing', 'fail');
        assertStep1b('filing', 'file');
    }


    private static testMethod void test_step1c() {
        assertStep1c('happy', 'happi');
        assertStep1c('sky', 'sky');
    }


    private static testMethod void test_step2() {
        assertStep2('relational', 'relate');
        assertStep2('conditional', 'condition');
        assertStep2('rational', 'rational');
        assertStep2('valenci', 'valence');
        assertStep2('hesitanci', 'hesitance');
        assertStep2('digitizer', 'digitize');
        assertStep2('conformabli', 'conformable');
        assertStep2('radicalli', 'radical');
        assertStep2('differentli', 'different');
        assertStep2('vileli', 'vile');
        assertStep2('analogousli', 'analogous');
        assertStep2('vietnamization', 'vietnamize');
        assertStep2('predication', 'predicate');
        assertStep2('operator', 'operate');
        assertStep2('feudalism', 'feudal');
        assertStep2('decisiveness', 'decisive');
        assertStep2('hopefulness', 'hopeful');
        assertStep2('callousness', 'callous');
        assertStep2('formaliti', 'formal');
        assertStep2('sensitiviti', 'sensitive');
        assertStep2('sensibiliti', 'sensible');
    }

 
    private static testMethod void test_step3() {
        assertStep3('triplicate','triplic');
        assertStep3('formative','form');
        assertStep3('formalize','formal');
        assertStep3('electriciti','electric');
        assertStep3('electrical','electric');
        assertStep3('hopeful','hope');
        assertStep3('goodness','good');
    }


    private static testMethod void test_step4() {
        assertStep4('revival','reviv');
        assertStep4('allowance','allow');
        assertStep4('inference','infer');
        assertStep4('airliner','airlin');
        assertStep4('gyroscopic','gyroscop');
        assertStep4('adjustable','adjust');
        assertStep4('defensible','defens');
        assertStep4('irritant','irrit');
        assertStep4('replacement','replac');
        assertStep4('adjustment','adjust');
        assertStep4('dependent','depend');
        assertStep4('adoption','adopt');
        assertStep4('homologou','homolog');
        assertStep4('communism','commun');
        assertStep4('activate','activ');
        assertStep4('angulariti','angular');
        assertStep4('homologous','homolog');
        assertStep4('effective','effect');
        assertStep4('bowdlerize','bowdler');
    }


    private static testMethod void test_step5a() {
        assertStep5a('probate', 'probat');
        assertStep5a('rate', 'rate');
        assertStep5a('cease', 'ceas');
    }


    private static testMethod void test_step5b() {
        assertStep5b('controll', 'control');
        assertStep5b('roll', 'roll');
    }
    
    
    private static testMethod void test_all() {
        RecognitionPorterStemmer porterStemmer = new RecognitionPorterStemmer();
        
        Map<String, String> testsMap = new Map<String, String>{           
'abaissiez' => 'abaissiez',
'abandon' => 'abandon',
'abandoned' => 'abandon',
'abase' => 'abas',
'abatements' => 'abat',
'abates' => 'abat',
'abbess' => 'abbess',
'abbominable' => 'abbomin',
'abbots' => 'abbot',
'abbreviated' => 'abbrevi',
'abed' => 'ab',
'abel' => 'abel',
'aberga' => 'aberga',
'abergavenny' => 'abergavenni',
'abet' => 'abet',
'abetting' => 'abet',
'abhominable' => 'abhomin',
'abhor' => 'abhor',
'abhorr' => 'abhorr',
'abhorred' => 'abhor',
'abhorring' => 'abhor',
'abhors' => 'abhor',
'abhorson' => 'abhorson',
'abide' => 'abid',
'abides' => 'abid',
'abilities' => 'abil',
'ability' => 'abil',
'abject' => 'abject',
'abjectly' => 'abjectli',
'abjects' => 'abject',
'abjur' => 'abjur',
'abjure' => 'abjur',
'able' => 'abl',
'abler' => 'abler',
'aboard' => 'aboard',
'abode' => 'abod',
'aboded' => 'abod',
'abodements' => 'abod',
'aboding' => 'abod',
'abominable' => 'abomin',
'abominably' => 'abomin',
'abominations' => 'abomin',
'abortive' => 'abort',
'abortives' => 'abort',
'abound' => 'abound',
'abounding' => 'abound',
'about' => 'about',
'above' => 'abov',
'abr' => 'abr',
'abraham' => 'abraham',
'abram' => 'abram',
'abreast' => 'abreast',
'abridg' => 'abridg',
'abridge' => 'abridg',
'abridged' => 'abridg',
'abridgment' => 'abridg',
'abroach' => 'abroach',
'abroad' => 'abroad',
'abrogate' => 'abrog',
'abrook' => 'abrook',
'abrupt' => 'abrupt',
'abruption' => 'abrupt',
'abruptly' => 'abruptli',
'absence' => 'absenc',
'absent' => 'absent',
'absey' => 'absei',
'absolute' => 'absolut',
'absolutely' => 'absolut',
'absolv' => 'absolv',
'absolver' => 'absolv',
'abstains' => 'abstain',
'abstemious' => 'abstemi',
'abstinence' => 'abstin',
'abstract' => 'abstract',
'absurd' => 'absurd',
'absyrtus' => 'absyrtu',
'abundance' => 'abund',
'abundant' => 'abund',
'abundantly' => 'abundantli',
'abus' => 'abu',
'abuse' => 'abus',
'abused' => 'abus',
'abuser' => 'abus',
'abuses' => 'abus',
'abusing' => 'abus',
'abutting' => 'abut',
'aby' => 'abi',
'abysm' => 'abysm',
'ac' => 'ac',
'academe' => 'academ',
'academes' => 'academ',
'accent' => 'accent',
'accents' => 'accent',
'accept' => 'accept',
'acceptable' => 'accept',
'acceptance' => 'accept',
'accepted' => 'accept',
'accepts' => 'accept',
'access' => 'access',
'accessary' => 'accessari',
'accessible' => 'access',
'accidence' => 'accid',
'accident' => 'accid',
'accidental' => 'accident',
'accidentally' => 'accident',
'accidents' => 'accid',
'accite' => 'accit',
'accited' => 'accit',
'accites' => 'accit',
'acclamations' => 'acclam',
'accommodate' => 'accommod',
'accommodated' => 'accommod',
'accommodation' => 'accommod',
'accommodations' => 'accommod',
'accommodo' => 'accommodo',
'accompanied' => 'accompani',
'accompany' => 'accompani',
'accompanying' => 'accompani',
'accomplices' => 'accomplic',
'accomplish' => 'accomplish',
'accomplished' => 'accomplish',
'accomplishing' => 'accomplish',
'accomplishment' => 'accomplish',
'accompt' => 'accompt',
'accord' => 'accord',
'accordant' => 'accord',
'accorded' => 'accord',
'accordeth' => 'accordeth',
'according' => 'accord',
'accordingly' => 'accordingli',
'accords' => 'accord',
'accost' => 'accost',
'accosted' => 'accost',
'account' => 'account',
'accountant' => 'account',
'accounted' => 'account',
'accounts' => 'account',
'accoutred' => 'accoutr',
'accoutrement' => 'accoutr',
'accoutrements' => 'accoutr',
'accrue' => 'accru',
'accumulate' => 'accumul',
'accumulated' => 'accumul',
'accumulation' => 'accumul',
'accurs' => 'accur',
'accursed' => 'accurs',
'accurst' => 'accurst',
'accus' => 'accu',
'accusation' => 'accus',
'accusations' => 'accus',
'accusative' => 'accus',
'accusativo' => 'accusativo',
'accuse' => 'accus',
'accused' => 'accus',
'accuser' => 'accus',
'accusers' => 'accus',
'accuses' => 'accus',
'accuseth' => 'accuseth',
'accusing' => 'accus',
'accustom' => 'accustom',
'accustomed' => 'accustom',
'ace' => 'ac',
'acerb' => 'acerb',
'ache' => 'ach',
'acheron' => 'acheron',
'aches' => 'ach',
'achiev' => 'achiev',
'achieve' => 'achiev',
'achieved' => 'achiev',
'achievement' => 'achiev',
'achievements' => 'achiev',
'achiever' => 'achiev',
'achieves' => 'achiev',
'achieving' => 'achiev',
'achilles' => 'achil',
'aching' => 'ach',
'achitophel' => 'achitophel',
'acknowledg' => 'acknowledg',
'acknowledge' => 'acknowledg',
'acknowledged' => 'acknowledg',
'acknowledgment' => 'acknowledg',
'acknown' => 'acknown',
'acold' => 'acold',
'aconitum' => 'aconitum',
'acordo' => 'acordo',
'acorn' => 'acorn',
'acquaint' => 'acquaint',
'acquaintance' => 'acquaint',
'acquainted' => 'acquaint',
'acquaints' => 'acquaint',
'acquir' => 'acquir',
'acquire' => 'acquir',
'acquisition' => 'acquisit',
'acquit' => 'acquit',
'acquittance' => 'acquitt',
'acquittances' => 'acquitt',
'acquitted' => 'acquit',
'acre' => 'acr',
'acres' => 'acr',
'across' => 'across',
'act' => 'act',
'actaeon' => 'actaeon',
'acted' => 'act',
'acting' => 'act',
'action' => 'action',
'actions' => 'action',
'actium' => 'actium',
'active' => 'activ',
'actively' => 'activ',
'activity' => 'activ',
'actor' => 'actor',
'actors' => 'actor',
'acts' => 'act',
'actual' => 'actual',
'acture' => 'actur',
'acute' => 'acut',
'acutely' => 'acut',
'ad' => 'ad',
'adage' => 'adag',
'adallas' => 'adalla',
'adam' => 'adam',
'adamant' => 'adam',
'add' => 'add',
'added' => 'ad',
'adder' => 'adder',
'adders' => 'adder',
'addeth' => 'addeth',
'addict' => 'addict',
'addicted' => 'addict',
'addiction' => 'addict',
'adding' => 'ad',
'addition' => 'addit',
'additions' => 'addit',
'addle' => 'addl',
'address' => 'address',
'addressing' => 'address',
'addrest' => 'addrest',
'adds' => 'add',
'adhere' => 'adher',
'adheres' => 'adher',
'adieu' => 'adieu',
'adieus' => 'adieu',
'adjacent' => 'adjac',
'adjoin' => 'adjoin',
'adjoining' => 'adjoin',
'adjourn' => 'adjourn',
'adjudg' => 'adjudg',
'adjudged' => 'adjudg',
'adjunct' => 'adjunct',
'administer' => 'administ',
'administration' => 'administr',
'admir' => 'admir',
'admirable' => 'admir',
'admiral' => 'admir',
'admiration' => 'admir',
'admire' => 'admir',
'admired' => 'admir',
'admirer' => 'admir',
'admiring' => 'admir',
'admiringly' => 'admiringli',
'admission' => 'admiss',
'admit' => 'admit',
'admits' => 'admit',
'admittance' => 'admitt',
'admitted' => 'admit',
'admitting' => 'admit',
'admonish' => 'admonish',
'admonishing' => 'admonish',
'admonishment' => 'admonish',
'admonishments' => 'admonish',
'admonition' => 'admonit',
'ado' => 'ado',
'adonis' => 'adoni',
'adopt' => 'adopt',
'adopted' => 'adopt',
'adoptedly' => 'adoptedli',
'adoption' => 'adopt',
'adoptious' => 'adopti',
'adopts' => 'adopt',
'ador' => 'ador',
'adoration' => 'ador',
'adorations' => 'ador',
'adore' => 'ador',
'adorer' => 'ador',
'adores' => 'ador',
'adorest' => 'adorest',
'adoreth' => 'adoreth',
'adoring' => 'ador',
'adorn' => 'adorn',
'adorned' => 'adorn',
'adornings' => 'adorn',
'adornment' => 'adorn',
'adorns' => 'adorn',
'adown' => 'adown',
'adramadio' => 'adramadio',
'adrian' => 'adrian',
'adriana' => 'adriana',
'adriano' => 'adriano',
'adriatic' => 'adriat',
'adsum' => 'adsum',
'adulation' => 'adul',
'adulterate' => 'adulter',
'adulterates' => 'adulter',
'adulterers' => 'adulter',
'adulteress' => 'adulteress',
'adulteries' => 'adulteri',
'adulterous' => 'adulter',
'adultery' => 'adulteri',
'adultress' => 'adultress',
'advanc' => 'advanc',
'advance' => 'advanc',
'advanced' => 'advanc',
'advancement' => 'advanc',
'advancements' => 'advanc',
'advances' => 'advanc',
'advancing' => 'advanc',
'advantage' => 'advantag',
'advantageable' => 'advantag',
'advantaged' => 'advantag',
'advantageous' => 'advantag',
'advantages' => 'advantag',
'advantaging' => 'advantag',
'advent' => 'advent',
'adventur' => 'adventur',
'adventure' => 'adventur',
'adventures' => 'adventur',
'adventuring' => 'adventur',
'adventurous' => 'adventur',
'adventurously' => 'adventur',
'adversaries' => 'adversari',
'adversary' => 'adversari',
'adverse' => 'advers',
'adversely' => 'advers',
'adversities' => 'advers',
'adversity' => 'advers',
'advertis' => 'adverti',
'advertise' => 'advertis',
'advertised' => 'advertis',
'advertisement' => 'advertis',
'advertising' => 'advertis',
'advice' => 'advic',
'advis' => 'advi',
'advise' => 'advis',
'advised' => 'advis',
'advisedly' => 'advisedli',
'advises' => 'advis',
'advisings' => 'advis',
'advocate' => 'advoc',
'advocation' => 'advoc',
'aeacida' => 'aeacida',
'aeacides' => 'aeacid',
'aedile' => 'aedil',
'aediles' => 'aedil',
'aegeon' => 'aegeon',
'aegion' => 'aegion',
'aegles' => 'aegl',
'aemelia' => 'aemelia',
'aemilia' => 'aemilia',
'aemilius' => 'aemiliu',
'aeneas' => 'aenea',
'aeolus' => 'aeolu',
'aer' => 'aer',
'aerial' => 'aerial',
'aery' => 'aeri',
'aesculapius' => 'aesculapiu',
'aeson' => 'aeson',
'aesop' => 'aesop',
'aetna' => 'aetna',
'afar' => 'afar',
'afear' => 'afear',
'afeard' => 'afeard',
'affability' => 'affabl',
'affable' => 'affabl',
'affair' => 'affair',
'affaire' => 'affair',
'affairs' => 'affair',
'affect' => 'affect',
'affectation' => 'affect',
'affectations' => 'affect',
'affected' => 'affect',
'affectedly' => 'affectedli',
'affecteth' => 'affecteth',
'affecting' => 'affect',
'affection' => 'affect',
'affectionate' => 'affection',
'affectionately' => 'affection',
'affections' => 'affect',
'affects' => 'affect',
'affeer' => 'affeer',
'affianc' => 'affianc',
'affiance' => 'affianc',
'affianced' => 'affianc',
'affied' => 'affi',
'affin' => 'affin',
'affined' => 'affin',
'affinity' => 'affin',
'affirm' => 'affirm',
'affirmation' => 'affirm',
'affirmatives' => 'affirm',
'afflict' => 'afflict',
'afflicted' => 'afflict',
'affliction' => 'afflict',
'afflictions' => 'afflict',
'afflicts' => 'afflict',
'afford' => 'afford',
'affordeth' => 'affordeth',
'affords' => 'afford',
'affray' => 'affrai',
'affright' => 'affright',
'affrighted' => 'affright',
'affrights' => 'affright',
'affront' => 'affront',
'affronted' => 'affront',
'affy' => 'affi',
'afield' => 'afield',
'afire' => 'afir',
'afloat' => 'afloat',
'afoot' => 'afoot',
'afore' => 'afor',
'aforehand' => 'aforehand',
'aforesaid' => 'aforesaid',
'afraid' => 'afraid',
'afresh' => 'afresh',
'afric' => 'afric',
'africa' => 'africa',
'african' => 'african',
'afront' => 'afront',
'after' => 'after',
'afternoon' => 'afternoon',
'afterward' => 'afterward',
'afterwards' => 'afterward',
'ag' => 'ag',
'again' => 'again',
'against' => 'against',
'agamemmon' => 'agamemmon',
'agamemnon' => 'agamemnon',
'agate' => 'agat',
'agaz' => 'agaz',
'age' => 'ag',
'aged' => 'ag',
'agenor' => 'agenor',
'agent' => 'agent',
'agents' => 'agent',
'ages' => 'ag',
'aggravate' => 'aggrav',
'aggrief' => 'aggrief',
'agile' => 'agil',
'agincourt' => 'agincourt',
'agitation' => 'agit',
'aglet' => 'aglet',
'agnize' => 'agniz',
'ago' => 'ago',
'agone' => 'agon',
'agony' => 'agoni',
'agree' => 'agre',
'agreed' => 'agre',
'agreeing' => 'agre',
'agreement' => 'agreement',
'agrees' => 'agre',
'agrippa' => 'agrippa',
'aground' => 'aground',
'ague' => 'agu',
'aguecheek' => 'aguecheek',
'agued' => 'agu',
'agueface' => 'aguefac',
'agues' => 'agu',
'ah' => 'ah',
'aha' => 'aha',
'ahungry' => 'ahungri',
'ai' => 'ai',
'aialvolio' => 'aialvolio',
'aiaria' => 'aiaria',
'aid' => 'aid',
'aidance' => 'aidanc',
'aidant' => 'aidant',
'aided' => 'aid',
'aiding' => 'aid',
'aidless' => 'aidless',
'aids' => 'aid',
'ail' => 'ail',
'aim' => 'aim',
'aimed' => 'aim',
'aimest' => 'aimest',
'aiming' => 'aim',
'aims' => 'aim',
'ainsi' => 'ainsi',
'aio' => 'aio',
'air' => 'air',
'aired' => 'air',
'airless' => 'airless',
'airs' => 'air',
'airy' => 'airi',
'ajax' => 'ajax',
'akilling' => 'akil',
'al' => 'al',
'alabaster' => 'alabast',
'alack' => 'alack',
'alacrity' => 'alacr',
'alarbus' => 'alarbu',
'alarm' => 'alarm',
'alarms' => 'alarm',
'alarum' => 'alarum',
'alarums' => 'alarum',
'alas' => 'ala',
'alb' => 'alb',
'alban' => 'alban',
'albans' => 'alban',
'albany' => 'albani',
'albeit' => 'albeit',
'albion' => 'albion',
'alchemist' => 'alchemist',
'alchemy' => 'alchemi',
'alcibiades' => 'alcibiad',
'alcides' => 'alcid',
'alder' => 'alder',
'alderman' => 'alderman',
'aldermen' => 'aldermen',
'ale' => 'al',
'alecto' => 'alecto',
'alehouse' => 'alehous',
'alehouses' => 'alehous',
'alencon' => 'alencon',
'alengon' => 'alengon',
'aleppo' => 'aleppo',
'ales' => 'al',
'alewife' => 'alewif',
'alexander' => 'alexand',
'alexanders' => 'alexand',
'alexandria' => 'alexandria',
'alexandrian' => 'alexandrian',
'alexas' => 'alexa',
'alias' => 'alia',
'alice' => 'alic',
'alien' => 'alien',
'aliena' => 'aliena',
'alight' => 'alight',
'alighted' => 'alight',
'alights' => 'alight',
'aliis' => 'alii',
'alike' => 'alik',
'alisander' => 'alisand',
'alive' => 'aliv',
'all' => 'all',
'alla' => 'alla',
'allay' => 'allai',
'allayed' => 'allai',
'allaying' => 'allai',
'allayment' => 'allay',
'allayments' => 'allay',
'allays' => 'allai',
'allegation' => 'alleg',
'allegations' => 'alleg',
'allege' => 'alleg',
'alleged' => 'alleg',
'allegiance' => 'allegi',
'allegiant' => 'allegi',
'alley' => 'allei',
'alleys' => 'allei',
'allhallowmas' => 'allhallowma',
'alliance' => 'allianc',
'allicholy' => 'allicholi',
'allied' => 'alli',
'allies' => 'alli',
'alligant' => 'allig',
'alligator' => 'allig',
'allons' => 'allon',
'allot' => 'allot',
'allots' => 'allot',
'allotted' => 'allot',
'allottery' => 'allotteri',
'allow' => 'allow',
'allowance' => 'allow',
'allowed' => 'allow',
'allowing' => 'allow',
'allows' => 'allow',
'allur' => 'allur',
'allure' => 'allur',
'allurement' => 'allur',
'alluring' => 'allur',
'allusion' => 'allus',
'ally' => 'alli',
'allycholly' => 'allycholli',
'almain' => 'almain',
'almanac' => 'almanac',
'almanack' => 'almanack',
'almanacs' => 'almanac',
'almighty' => 'almighti',
'almond' => 'almond',
'almost' => 'almost',
'alms' => 'alm',
'almsman' => 'almsman',
'aloes' => 'alo',
'aloft' => 'aloft',
'alone' => 'alon',
'along' => 'along',
'alonso' => 'alonso',
'aloof' => 'aloof',
'aloud' => 'aloud',
'alphabet' => 'alphabet',
'alphabetical' => 'alphabet',
'alphonso' => 'alphonso',
'alps' => 'alp',
'already' => 'alreadi',
'also' => 'also',
'alt' => 'alt',
'altar' => 'altar',
'altars' => 'altar',
'alter' => 'alter',
'alteration' => 'alter',
'altered' => 'alter',
'alters' => 'alter',
'althaea' => 'althaea',
'although' => 'although',
'altitude' => 'altitud',
'altogether' => 'altogeth',
'alton' => 'alton',
'alway' => 'alwai',
'always' => 'alwai',
'am' => 'am',
'amaimon' => 'amaimon',
'amain' => 'amain',
'amaking' => 'amak',
'amamon' => 'amamon',
'amaz' => 'amaz',
'amaze' => 'amaz',
'amazed' => 'amaz',
'amazedly' => 'amazedli',
'amazedness' => 'amazed',
'amazement' => 'amaz',
'amazes' => 'amaz',
'amazeth' => 'amazeth',
'amazing' => 'amaz',
'amazon' => 'amazon',
'amazonian' => 'amazonian',
'amazons' => 'amazon',
'ambassador' => 'ambassador',
'ambassadors' => 'ambassador',
'amber' => 'amber',
'ambiguides' => 'ambiguid',
'ambiguities' => 'ambigu',
'ambiguous' => 'ambigu',
'ambition' => 'ambit',
'ambitions' => 'ambit',
'ambitious' => 'ambiti',
'ambitiously' => 'ambiti',
'amble' => 'ambl',
'ambled' => 'ambl',
'ambles' => 'ambl',
'ambling' => 'ambl',
'ambo' => 'ambo',
'ambuscadoes' => 'ambuscado',
'ambush' => 'ambush',
'amen' => 'amen',
'amend' => 'amend',
'amended' => 'amend',
'amendment' => 'amend',
'amends' => 'amend',
'amerce' => 'amerc',
'america' => 'america',
'ames' => 'am',
'amiable' => 'amiabl',
'amid' => 'amid',
'amidst' => 'amidst',
'amiens' => 'amien',
'amis' => 'ami',
'amiss' => 'amiss',
'amities' => 'amiti',
'amity' => 'amiti',
'amnipotent' => 'amnipot',
'among' => 'among',
'amongst' => 'amongst',
'amorous' => 'amor',
'amorously' => 'amor',
'amort' => 'amort',
'amount' => 'amount',
'amounts' => 'amount',
'amour' => 'amour',
'amphimacus' => 'amphimacu',
'ample' => 'ampl',
'ampler' => 'ampler',
'amplest' => 'amplest',
'amplified' => 'amplifi',
'amplify' => 'amplifi',
'amply' => 'ampli',
'ampthill' => 'ampthil',
'amurath' => 'amurath',
'amyntas' => 'amynta',
'an' => 'an',
'anatomiz' => 'anatomiz',
'anatomize' => 'anatom',
'anatomy' => 'anatomi',
'ancestor' => 'ancestor',
'ancestors' => 'ancestor',
'ancestry' => 'ancestri',
'anchises' => 'anchis',
'anchor' => 'anchor',
'anchorage' => 'anchorag',
'anchored' => 'anchor',
'anchoring' => 'anchor',
'anchors' => 'anchor',
'anchovies' => 'anchovi',
'ancient' => 'ancient',
'ancientry' => 'ancientri',
'ancients' => 'ancient',
'ancus' => 'ancu',
'and' => 'and',
'andirons' => 'andiron',
'andpholus' => 'andpholu',
'andren' => 'andren',
'andrew' => 'andrew',
'andromache' => 'andromach',
'andronici' => 'andronici',
'andronicus' => 'andronicu',
'anew' => 'anew',
'ang' => 'ang',
'angel' => 'angel',
'angelica' => 'angelica',
'angelical' => 'angel',
'angelo' => 'angelo',
'angels' => 'angel',
'anger' => 'anger',
'angerly' => 'angerli',
'angers' => 'anger',
'anges' => 'ang',
'angiers' => 'angier',
'angl' => 'angl',
'anglais' => 'anglai',
'angle' => 'angl',
'angler' => 'angler',
'angleterre' => 'angleterr',
'angliae' => 'anglia',
'angling' => 'angl',
'anglish' => 'anglish',
'angrily' => 'angrili',
'angry' => 'angri',
'anguish' => 'anguish',
'angus' => 'angu',
'animal' => 'anim',
'animals' => 'anim',
'animis' => 'animi',
'anjou' => 'anjou',
'ankle' => 'ankl',
'anna' => 'anna',
'annals' => 'annal',
'anne' => 'ann',
'annex' => 'annex',
'annexed' => 'annex',
'annexions' => 'annexion',
'annexment' => 'annex',
'annothanize' => 'annothan',
'announces' => 'announc',
'annoy' => 'annoi',
'annoyance' => 'annoy',
'annoying' => 'annoi',
'annual' => 'annual',
'anoint' => 'anoint',
'anointed' => 'anoint',
'anon' => 'anon',
'another' => 'anoth',
'anselmo' => 'anselmo',
'answer' => 'answer',
'answerable' => 'answer',
'answered' => 'answer',
'anthropophagi' => 'anthropophagi',
'anthropophaginian' => 'anthropophaginian',
'antiates' => 'antiat',
'antic' => 'antic',
'anticipate' => 'anticip',
'anticipates' => 'anticip',
'anticipatest' => 'anticipatest',
'anticipating' => 'anticip',
'anticipation' => 'anticip',
'antick' => 'antick',
'apollodorus' => 'apollodoru',
'apology' => 'apolog',
'apoplex' => 'apoplex',
'apoplexy' => 'apoplexi',
'apostle' => 'apostl',
'apostles' => 'apostl',
'apostrophas' => 'apostropha',
'apoth' => 'apoth',
'apothecary' => 'apothecari',
'appal' => 'appal',
'appall' => 'appal',
'appalled' => 'appal',
'appals' => 'appal',
'apparel' => 'apparel',
'apparell' => 'apparel',
'apparelled' => 'apparel',
'apparent' => 'appar',
'apparently' => 'appar',
'apparition' => 'apparit',
'apparitions' => 'apparit',
'applying' => 'appli',
'appoint' => 'appoint',
'appointed' => 'appoint',
'appointment' => 'appoint',
'appointments' => 'appoint',
'appoints' => 'appoint',
'apprehend' => 'apprehend',
'apprehended' => 'apprehend',
'apprehends' => 'apprehend',
'apprehension' => 'apprehens',
'apprehensions' => 'apprehens',
'arabian' => 'arabian',
'araise' => 'arais',
'arbitrate' => 'arbitr',
'arbitrating' => 'arbitr',
'arbitrator' => 'arbitr',
'arbitrement' => 'arbitr',
'arbors' => 'arbor',
'arbour' => 'arbour',
'arc' => 'arc',
'arch' => 'arch',
'archbishop' => 'archbishop',
'archbishopric' => 'archbishopr',
'arraigning' => 'arraign',
'arraignment' => 'arraign',
'arrant' => 'arrant',
'arras' => 'arra',
'array' => 'arrai',
'arrearages' => 'arrearag',
'arrest' => 'arrest',
'arrested' => 'arrest',
'arrests' => 'arrest',
'arriv' => 'arriv',
'arrival' => 'arriv',
'arrivance' => 'arriv',
'arrive' => 'arriv',
'arrived' => 'arriv',
'arrives' => 'arriv',
'arriving' => 'arriv',
'assemblance' => 'assembl',
'assemble' => 'assembl',
'assembled' => 'assembl',
'assemblies' => 'assembl',
'assembly' => 'assembl',
'assent' => 'assent',
'asses' => 'ass',
'assez' => 'assez',
'assign' => 'assign',
'assigned' => 'assign',
'assigns' => 'assign',
'assinico' => 'assinico',
'assist' => 'assist',
'assistance' => 'assist',
'assistances' => 'assist',
'assistant' => 'assist',
'assistants' => 'assist',
'assisted' => 'assist',
'assisting' => 'assist',
'associate' => 'associ',
'associated' => 'associ',
'associates' => 'associ',
'assuage' => 'assuag',
'attaint' => 'attaint',
'attainted' => 'attaint',
'attainture' => 'attaintur',
'attempt' => 'attempt',
'attemptable' => 'attempt',
'attempted' => 'attempt',
'attempting' => 'attempt',
'attempts' => 'attempt',
'attend' => 'attend',
'attendance' => 'attend',
'attendant' => 'attend',
'attendants' => 'attend',
'attended' => 'attend',
'attendents' => 'attend',
'attendeth' => 'attendeth',
'attending' => 'attend',
'attends' => 'attend',
'attent' => 'attent',
'attention' => 'attent',
'attributed' => 'attribut',
'attributes' => 'attribut',
'attribution' => 'attribut',
'attributive' => 'attribut',
'atwain' => 'atwain',
'au' => 'au',
'aubrey' => 'aubrei',
'auburn' => 'auburn',
'aucun' => 'aucun',
'audacious' => 'audaci',
'audaciously' => 'audaci',
'audacity' => 'audac',
'audible' => 'audibl',
'audience' => 'audienc',
'audis' => 'audi',
'audit' => 'audit',
'auditor' => 'auditor',
'auditors' => 'auditor',
'auditory' => 'auditori',
'awakened' => 'awaken',
'awakens' => 'awaken',
'awakes' => 'awak',
'awaking' => 'awak',
'award' => 'award',
'awards' => 'award',
'awasy' => 'awasi',
'away' => 'awai',
'awe' => 'aw',
'aweary' => 'aweari',
'aweless' => 'aweless',
'awful' => 'aw',
'awhile' => 'awhil',
'awkward' => 'awkward',
'awl' => 'awl',
'ayli' => 'ayli',
'azur' => 'azur',
'azure' => 'azur'

    
};

        Set<String> lines = testsMap.keySet();
        for(String input : lines) {
            String expectedStem = testsMap.get(input);
            String actualStem = porterStemmer.stemWord(input);
            List<String> fillers = new String[]{ input, expectedStem, actualStem};
        	//System.debug(String.format('Expected {0} -> {1} for step 4, got {2}', fillers));
        	System.assertEquals( actualStem, expectedStem);
            if (!expectedStem.equals(actualStem)) {
				System.debug(String.format('Failed: Expected {0} -> {1} for step 4, but got {2}', fillers));
            }
        }
            
    }
    
}