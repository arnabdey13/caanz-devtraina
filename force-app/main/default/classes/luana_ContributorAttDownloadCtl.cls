/*
    Developer: WDCi (KH)
    Date: 07/07/2016
    Task #: Contributor Community Attendance List Download (LCA-722)
    
    //LCA-871 WDCi-KH: 16/08/2016 include the Primary Employer in the attachment download
   
*/
public without sharing class luana_ContributorAttDownloadCtl {

    
    public Id sessionId;
    public Id resourceBookingId;
    
    public Id custCommConId {set; get;}
    public Id custCommAccId {set; get;}
    
    public String rbId;
    public String type;
    
    public List<aAttendance> wpAttendance {get; set;}
    
    public static Map<Id, Employment_History__c> primaryEmployments;
    
    public luana_ContributorAttDownloadCtl(){
        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId()); 

        custCommConId = commUserUtil.custCommConId;
        custCommAccId = commUserUtil.custCommAccId;
    
        sessionId = ApexPages.currentPage().getParameters().get('id');
        resourceBookingId = ApexPages.currentPage().getParameters().get('rbid');
        
        wpAttendance = new List<aAttendance>();
        
        getAttendances();
    }
    
    public void getAttendances(){
        
        primaryEmployments = new Map<Id, Employment_History__c>();
        for(Employment_History__c eh: [Select Id, Employer__c, Employer__r.Name, Member__c, Status__c, Primary_Employer__c from Employment_History__c Where Member__c =: custCommAccId]){
            if(eh.Primary_Employer__c && eh.Status__c == 'Current'){
                primaryEmployments.put(eh.Member__c, eh);
            }
        }
        
        System.debug('***8primaryEmployments1:: ' + primaryEmployments + ' - '+ custCommAccId);
        
        List<LuanaSMS__Attendance2__c> attendances = [Select Id, Name, LuanaSMS__Attendance_Status__c, LuanaSMS__Start_time__c, LuanaSMS__End_time__c, LuanaSMS__Session_End_Time__c, LuanaSMS__Session_Start_Time__c, 
                                                        LuanaSMS__Student_Program__r.Name, LuanaSMS__Session__r.Name, LuanaSMS__Session__r.RecordType.Name, LuanaSMS__Student_Program__r.LuanaSMS__Course__r.Name, 
                                                        LuanaSMS__Student_Program__r.LuanaSMS__Course__r.LuanaSMS__Start_Date__c, LuanaSMS__Student_Program__r.LuanaSMS__Course__r.End_Date__c, 
                                                        LuanaSMS__Session__r.Session_Details__c, LuanaSMS__Contact_Student__r.Account.Preferred_Name__c, LuanaSMS__Contact_Student__r.Account.Primary_Employer__c, LuanaSMS__Contact_Student__r.Account.Primary_Employer__r.Name,
                                                        Adobe_Connect_Url__c, LuanaSMS__Contact_Student__r.FirstName, LuanaSMS__Contact_Student__r.LastName, LuanaSMS__Contact_Student__r.Email,
                                                        LuanaSMS__Contact_Student__r.Account.Customer_ID__c
                                                        from LuanaSMS__Attendance2__c Where LuanaSMS__Session__c =: sessionId];
        
        List<LuanaSMS__Resource_Booking__c> resourceBookings = [select Id, Name, LuanaSMS__Contact_Trainer_Assessor__c, LuanaSMS__Session__c
                                                                        from LuanaSMS__Resource_Booking__c 
                                                                        Where LuanaSMS__Contact_Trainer_Assessor__c =: custCommConId and LuanaSMS__Session__c =: sessionId];
        if(!resourceBookings.isEmpty()){
            for(LuanaSMS__Attendance2__c attObj: attendances){
                System.debug('****attObj: ' + attObj.LuanaSMS__Contact_Student__r.Account.Primary_Employer__r.Name);
            
                wpAttendance.add(new aAttendance(attObj));
                
                
            }
        }
    }
    
    public class aAttendance {
        public LuanaSMS__Attendance2__c att {get; set;}
        public String startTime {get; set;}
        public String endTime {get; set;}
        public String primaryEmployer {get; set;}

        //This is the contructor method. When we create a new cContact object we pass a Contact that is set to the con property. We also set the selected value to false
        public aAttendance(LuanaSMS__Attendance2__c a) {
            att = a;
            
            startTime = a.LuanaSMS__Start_time__c.format('dd-MM-yyyy HH:mm:ss', UserInfo.getTimeZone().getID());
            endTime = a.LuanaSMS__End_time__c.format('dd-MM-yyyy HH:mm:ss', UserInfo.getTimeZone().getID());
            
            primaryEmployer = a.LuanaSMS__Contact_Student__r.Account.Primary_Employer__r.Name;
            /*if(primaryEmployments.containsKey(a.LuanaSMS__Contact_Student__r.Account.Id)){
                primaryEmployer = primaryEmployments.get(a.LuanaSMS__Contact_Student__r.Account.Id).Employer__r.Name;
            }*/
        }
    }
}