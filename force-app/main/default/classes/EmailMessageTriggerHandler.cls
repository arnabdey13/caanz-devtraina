/***********************************************************************************************************************************************************************
Name: EmailMessageTriggerHandler 
============================================================================================================================== 
Purpose: 
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR                 DATE          DETAIL     Description 
1.0        Mayukhman Pathak       10/07/2019     Created    
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/
public class EmailMessageTriggerHandler extends TriggerHandler{
    //Context Variable Collections after Type Casting
    List<EmailMessage> newList = (List<EmailMessage>)Trigger.new;
    List<EmailMessage> oldList = (List<EmailMessage>)Trigger.old;
    Map<Id,EmailMessage> oldMap = (Map<Id,EmailMessage>)Trigger.oldMap;
    Map<Id,EmailMessage> newMap = (Map<Id,EmailMessage>)Trigger.newMap;    
    
    public EmailMessageTriggerHandler(){     
    }
    
    public override void beforeInsert(){  
    }
    
    
    public override void afterInsert(){   
        Set<Id> caseIds = new Set<Id>();
        List<Case> updateListCase = new List<Case>();
        for(EmailMessage tsk : newList) {
            if(tsk.Status == '3')
                caseIds.add(tsk.ParentId);
        }
        for(Case cse : [Select Id,RecordTypeId,RecordType.DeveloperName,Initial_Response_Time__c from Case where Id IN: caseIds]){
            if(cse.RecordType.DeveloperName == 'Case' && cse.Initial_Response_Time__c == null){
                cse.Initial_Response_Time__c = System.now();
                updateListCase.add(cse);
            }
        }
        if(updateListCase.size() > 0){            
            try{
                update updateListCase;
            }
            catch(Exception ae){
                system.debug('Exception : ' + ae );
            }
        }        
    }
    /*
    public override void beforeUpdate(){  
    }
    public override void afterUpdate(){   
    }    
    public override void afterDelete(){        
    }    
    public override void afterUndelete(){        
    }*/
}