@IsTest(seeAllData=false)
public class EditorNotificationsServiceTests {

    @testSetup static void createTestData() {
        TestSubscriptionUtils.createTestData();
    }

    @IsTest
    public static void testFieldMapping() {
        // round-trip the mappings to ensure that they translate correctly and print to the log in case a reference is required
        // when changing the config of the page

        String dbToPage = '\nDatabase field => Lightning Editor Field Name\n';

        for (Integer i = 1; i <= 31; i++) {
            String dbField = 'Answer_' + i + '__c';
            String mapped = EditorNotificationsService.mapDBToField('Answer_', dbField);
            String unmapped = EditorNotificationsService.mapFieldToDB('Answer_', mapped);
            System.assertEquals(dbField, unmapped, 'roundtripped mapping matches for: ' + dbField);
            dbToPage += dbField + ' => ' + mapped + '\n';
        }

        System.debug(dbToPage);
    }

    @IsTest
    public static void testReadNewQuestionnaire() {

        Map<String, Object> toClient = EditorNotificationsService.load(TestSubscriptionUtils.getTestAccountId());
        Map<Object, Object> q = (Map<Object, Object>) toClient.get('RECORD');

        System.debug(toClient.keySet());

        System.assertEquals(new List<String>(), toClient.get('prop.labelsRequired'), 'no fields needing questions for save when account has no AU fields populated');

        System.assertEquals(false, q.get('Lock__c'), 'New questionnaire is unlocked');
        System.assertNotEquals(null, q.get('Status__c'), 'Status is populated');

        System.assertEquals(null, q.get('Question_1__c'), 'Question is not sent to client');
        System.assertEquals(null, q.get('Answer_1__c'), 'Answer 1 is empty');

        System.assertEquals(false, toClient.get('prop.lastYearPresent'), 'last year property is present and correct');

        System.assertEquals('AU', toClient.get('prop.countryCode'), 'country code is populated');
        List<Map<String, String>> countries = (List<Map<String, String>>) toClient.get('prop.countries');
        System.assertNotEquals(0, countries.size(), 'countries options are populated');

    }
//Commented out due to removal of Mapping
//  @IsTest
//  public static void testReadWriteNewWithAccountMappingsAU() {
//   
//        // checking pre-loading of answers from Account and writes back to account after save
//
//        Id accountId = TestSubscriptionUtils.getTestAccountId();
//
//        update new Account(Id = accountId,
//                Registered_BAS_Agent__c = true, // Q 13 in custom meta-data //Update from Q 10
//              //  AFSL_Number__c = '3456' // Q 24 in custom meta-data //Update from Q 18
//        );
//
//       Map<String, Object> toClient = EditorNotificationsService.load(accountId);
//        Map<Object, Object> q = (Map<Object, Object>) toClient.get('RECORD');
//
//        System.debug(toClient.keySet());
//
//        System.assertEquals('AU', toClient.get('prop.countryCode'), 'country code is populated');
//
//       // System.assertEquals(new List<String>{
//       //         'Answer_12__c', 'Answer_23__c' //Updated from 'Answer_9__c', 'Answer_17__c'
//       // }, toClient.get('prop.labelsRequired'),
//       //         'client sees two (translated) fields needing questions for save');
//
//        // client sees request for labels and supplies them
//        toClient.put('LABEL-MAPPINGS', new Map<Object, Object>{
//                'Answer_12__c' => 'Who is your daddy?', //Updated from 'Answer_9__c'
//               // 'Answer_23__c' => 'Who is your momma?' //Updated from 'Answer_17__c'
//        });
//
//        // apex translates client fields back to db field here i.e. in db fields will match custom meta-data listed above
//        EditorNotificationsService.save(toClient, accountId);
//
//        Questionnaire_Response__c qAfterSave = [select Id, Question_13__c, Question_24__c from Questionnaire_Response__c];
//        System.debug(qAfterSave);
//        System.assertEquals('Who is your daddy?', qAfterSave.Question_13__c, 'Question for pre-populated answer was saved'); //Updated from 'Answer_10__c'
//        System.assertEquals('Who is your momma?', qAfterSave.Question_24__c, 'Question for pre-populated answer was saved'); //Updated from 'Answer_18__c'
//
//    }
//AE Commented out as this is not Required - Fields not Mapped in Metadata @26/04/2018
//Test Class no longer Relevant No NZ Fields Mapped to Account 
//    @IsTest
//    public static void testReadWriteNewWithAccountMappingsNZ() {
//
//        // checking pre-loading of answers from Account and writes back to account after save
//
//       Id accountId = TestSubscriptionUtils.getTestAccountId();
//
//        update new Account(Id = accountId,
//                PersonOtherCountryCode = 'NZ',
//                Qualified_Auditor__c = true, // Q 11 in custom meta-data
//                Insolvency_Practitioner__c = true // Q 17 in custom meta-data
//        );
//
//        Map<String, Object> toClient = EditorNotificationsService.load(accountId);
//        Map<Object, Object> q = (Map<Object, Object>) toClient.get('RECORD');
//
//        System.debug(toClient.keySet());
//
//        System.assertEquals('NZ', toClient.get('prop.countryCode'), 'country code is populated');
//
//        // NOTE: these tests will break if an admin changes the mappings i.e. always requires dev support
//        System.assertEquals(new List<String>{
//                'Answer_10__c', 'Answer_16__c'
//        }, toClient.get('prop.labelsRequired'),
//                'client sees two (translated) fields needing questions for save');
//
//        System.assertEquals('Yes', q.get('Answer_10__c'), 'mapped question was pre-loaded');
//        System.assertEquals('Yes', q.get('Answer_16__c'), 'mapped question was pre-loaded');
//
//        // client changes the pre-loaded questions
//        setAnswer(toClient, 'Answer_10__c', 'No', 'Who is your daddy?');
//        setAnswer(toClient, 'Answer_16__c', 'No', 'Who is your momma?');
//
//        // apex translates client fields back to db field here i.e. in db fields will match custom meta-data listed above
//        EditorNotificationsService.save(toClient, accountId);
//
//        Questionnaire_Response__c qAfterSave = [select Id, Question_11__c, Answer_11__c, Question_17__c, Answer_17__c from Questionnaire_Response__c];
//        System.debug(qAfterSave);
//        System.assertEquals('Who is your daddy?', qAfterSave.Question_11__c, 'Question for pre-populated answer was saved');
//        System.assertEquals('No', qAfterSave.Answer_11__c, 'Answer for pre-populated answer was saved');
//        System.assertEquals('Who is your momma?', qAfterSave.Question_17__c, 'Question for pre-populated answer was saved');
//        System.assertEquals('No', qAfterSave.Answer_17__c, 'Answer for pre-populated answer was saved');
//
//        Account nzAccountAfterWrite = getAccountQueryForAllMappedFields(accountId);
//        System.assertEquals(true, nzAccountAfterWrite.Insolvency_Practitioner__c, 'Answer for pre-populated answer was not saved to Account');
//        System.assertEquals(true, nzAccountAfterWrite.Qualified_Auditor__c, 'Answer for pre-populated answer was not saved to Account');
//
//    }

    private static Account getAccountQueryForAllMappedFields(Id accountId) {
        String accountQuery = 'select ';
        for (String fieldName : EditorNotificationsService.AU_ACCOUNT_FIELDS.values()) {
            accountQuery = accountQuery + fieldName + ',';
        }
        for (String fieldName : EditorNotificationsService.NZ_ACCOUNT_FIELDS.values()) {
            accountQuery = accountQuery + fieldName + ',';
        }
        accountQuery = accountQuery.substringBeforeLast(',') + ' from Account where Id = \'' + accountId + '\'';
        return Database.query(accountQuery);
    }

    @IsTest
    public static void testWriteNewQuestionnaireAUAU() {
        // where account country is AU and questionnaire country is Australia

        String accountId = TestSubscriptionUtils.getTestAccountId();

        Map<String, Object> fromClient = EditorNotificationsService.load(accountId);
        Map<Object, Object> q = (Map<Object, Object>) fromClient.get('RECORD');

        setAnswer(fromClient, 'Answer_1__c', '45', 'How many hours did you work?');
        setAnswer(fromClient, 'Answer_2__c', 'Other', 'Are you a member of another org?');
        setAnswer(fromClient, 'Answer_3__c', 'taking a break', 'Why are you exempt?');

        // surrounding the insertion of 5-5
        setAnswer(fromClient, 'Answer_5__c', 'other', 'I am also a member of');
        setAnswer(fromClient, 'Answer_5-5__c', 'the boy scouts', 'Enter other');
        setAnswer(fromClient, 'Answer_6__c', 'Yes', 'Registered Company Auditor?');

        // the last questions in AU, have been troublesome
        setAnswer(fromClient, 'Answer_32__c', 'Yes', 'Are you a Principal or Partner of an Accounting Practice?'); //Previously ('Answer_27__c')
        setAnswer(fromClient, 'Answer_33__c', true, 'Provided services in NZ?'); //Previously ('Answer_28__c')
        setAnswer(fromClient, 'Answer_34__c', true, 'Provided services in AU?'); //Previously ('Answer_29__c')
        setAnswer(fromClient, 'Answer_35__c', true, 'Provided services in rest of the world?'); //Previously ('Answer_30__c')

        // questions replicated to Account object: NOTE these depend on the Custom Metadata Type
        setAnswer(fromClient, 'Answer_16__c', 'Yes', 'Are you a registered Bankruptcy Guru');
        setAnswer(fromClient, 'Answer_22__c', 'Howard Jones', 'Full Name of AFSL guy');
        setAnswer(fromClient, 'Answer_23__c', '1234', 'AFSL Number');

        EditorNotificationsService.save(fromClient, accountId);

        fromClient = EditorNotificationsService.load(accountId);
        q = (Map<Object, Object>) fromClient.get('RECORD');
        for (Object k : q.keySet()) {
            system.debug('re-read after save: ' + k + ' => ' + q.get(k));
        }
        System.assertEquals('45', q.get('Answer_1__c'), 'Answer 1 is populated');
        System.assertEquals('Yes', q.get('Answer_32__c'), 'Answer 27 is populated and translated');
        System.assertEquals(true, q.get('Answer_33__c'), 'Answer 28 is populated and translated'); //was ('Answer_27__c')
        System.assertEquals(true, q.get('Answer_34__c'), 'Answer 29 is populated and translated'); //was ('Answer_28__c')
       //System.assertEquals(true, q.get('Answer_35__c'), 'Answer 30 is populated and translated'); //was ('Answer_29__c')

        Account auAccountAfterWrite = getAccountQueryForAllMappedFields(accountId);

        // testing account fields with various types i.e. boolean, number, string
        System.assertEquals(true, auAccountAfterWrite.Registered_Trustee_in_Bankruptcy__c,
                'trustee field in account was updated');
        //System.assertEquals('1234', auAccountAfterWrite.AFSL_Number__c,
          //      'AFSL number in account was updated');
       // System.assertEquals('Howard Jones', auAccountAfterWrite.Full_Name_of_AFSL__c,
           //     'AFSL full name in account was updated');
    }

    @IsTest
    public static void testWriteNewQuestionnaireAUNZ() {
        // where account country is AU and questionnaire country is New Zealand

        String accountId = TestSubscriptionUtils.getTestAccountId();

        // simulate changing residential country for questionnaire. see testWriteNewCountry below for more assertions around this
        Map<String, Object> fromClient = EditorNotificationsService.load(accountId);
        fromClient.put('DIRTY', new Map<Object, Object>{
                'prop.countryCode' => true
        });
        setAnswer(fromClient, 'prop.countryCode', 'NZ', 'Country');
        System.assertEquals('nz', EditorNotificationsService.save(fromClient, accountId), 'suffix returned is lowercase');

        // reload and simulate client setting and sending answers
        fromClient = EditorNotificationsService.load(accountId);
        Map<Object, Object> q = (Map<Object, Object>) fromClient.get('RECORD');

        setAnswer(fromClient, 'Answer_1__c', '45', 'How many hours did you work?');
        setAnswer(fromClient, 'Answer_2__c', 'Other', 'Are you a member of another org?');
        setAnswer(fromClient, 'Answer_3__c', 'taking a break', 'Why are you exempt?');
        //AE Commented out as this is not Required - Fields not Mapped in Metadata @26/04/2018
        // questions displayed by the NZ page that have account mappings - //AE Not Required, No Account Mapping 
        //setAnswer(fromClient, 'Answer_10__c', 'Yes', 'Auditor?');
        //setAnswer(fromClient, 'Answer_16__c', 'Yes', 'Insolvency Practitioner?');

        EditorNotificationsService.save(fromClient, accountId);

        fromClient = EditorNotificationsService.load(accountId);
        q = (Map<Object, Object>) fromClient.get('RECORD');
        for (Object k : q.keySet()) {
            system.debug('re-read after save: ' + k + ' => ' + q.get(k));
        }
        System.assertEquals('45', q.get('Answer_1__c'), 'Answer 1 is populated');

        Account auAccountAfterWrite = getAccountQueryForAllMappedFields(accountId);

        // testing AU account fields with various types i.e. boolean, number, string are not changed
        System.assertEquals(false, auAccountAfterWrite.Registered_Trustee_in_Bankruptcy__c, 'account field not updated');
       // System.assertEquals(NULL, auAccountAfterWrite.AFSL_Number__c, 'account field not updated');
        //AE Commented out as this is not Required - Fields not Mapped in Metadata @26/04/2018
        // testing NZ account fields with various types i.e. boolean, number, string are not changed - //AE Not Required, No Account Mapping 
     //   System.assertEquals(false, auAccountAfterWrite.Qualified_Auditor__c, 'account field not updated');
     //   System.assertEquals(false, auAccountAfterWrite.Insolvency_Practitioner__c, 'account field not updated');
    }

    @IsTest
    public static void testWriteLockedQuestionnaire() {

        String accountId = TestSubscriptionUtils.getTestAccountId();

        Map<String, Object> fromClient = EditorNotificationsService.load(accountId);
        Map<Object, Object> q = (Map<Object, Object>) fromClient.get('RECORD');

        setAnswer(fromClient, 'prop.confirmed', true, 'Check this box to confirm! Changes cannot be made afterwards');

        EditorNotificationsService.save(fromClient, accountId);

        fromClient = EditorNotificationsService.load(accountId);
        q = (Map<Object, Object>) fromClient.get('RECORD');
        system.debug('after confirmation save: ' + fromClient);

        System.assertEquals(true, fromClient.get('prop.readOnly'), 'After a confirmation the page should be read only');
        System.assertEquals('Submitted', q.get('Status__c'), 'After a confirmation the status should be Submitted');
        System.assertEquals(true, q.get('Lock__c'), 'After a confirmation the lock field should be true');
    }


    @IsTest
    public static void testWriteNewCountry() {

        String accountId = TestSubscriptionUtils.getTestAccountId();

        update new Account(Id = accountId,
                // set some AU mapped answers
                Registered_BAS_Agent__c = true, // Q 10 in custom meta-data
                AFSL_Number__c = '3456', // Q 18 in custom meta-data
                // set some NZ mapped answers
                Qualified_Auditor__c = true, // Q 11 in custom meta-data
                Insolvency_Practitioner__c = true // Q 17 in custom meta-data
        );

        Questionnaire_Response__c q2 = new Questionnaire_Response__c(Status__c = 'Draft', Account__c = accountId,
                Year__c = String.valueOf(EditorNotificationsService.getCurrentYear()),
                Question_1__c = 'How Many Hours', Answer_1__c = '345',
                Question_6__c = 'Auditor?', Answer_6__c = 'Yes',
                // questions above #6 are country specific
                Question_7__c = 'Legend?', Answer_7__c = 'Yes');
        insert q2;

        Map<String, Object> fromClient = EditorNotificationsService.load(accountId);
        Map<Object, Object> q = (Map<Object, Object>) fromClient.get('RECORD');
        fromClient.put('DIRTY', new Map<Object, Object>{
                'prop.countryCode' => true
        });

        setAnswer(fromClient, 'prop.countryCode', 'NZ', 'Country');

        System.assertEquals('nz', EditorNotificationsService.save(fromClient, accountId), 'suffix returned is lowercase');

        // check cleaning process worked i.e. integration test assertions
        q2 = [
                select Id, Residential_Country__c,
                        Question_1__c, Answer_1__c, Question_6__c, Answer_6__c, Question_7__c, Answer_7__c,
                        Answer_10__c, Answer_11__c, Answer_17__c, Answer_18__c
                from Questionnaire_Response__c
                where Id = :q2.Id
        ];
        System.debug(q2);
        System.assertEquals('How Many Hours', q2.Question_1__c, 'Common questions were not cleaned');
        System.assertEquals('345', q2.Answer_1__c, 'Common answers were not cleaned');
        System.assertNotEquals(null, q2.Question_6__c, 'Common questions were not cleaned');
        System.assertNotEquals(null, q2.Answer_6__c, 'Common answers were not cleaned');
        // here's the common vs country boundary
        System.assertEquals(null, q2.Question_7__c, 'Country questions were cleaned');
        System.assertEquals(null, q2.Answer_7__c, 'Country answers were cleaned');

        System.assertEquals('New Zealand', q2.Residential_Country__c, 'Country in Questionnaire was changed');

        // the NZ mapped answers should not be pre-loaded since the Account and Questionnaire countries no longer match
        System.assertEquals(null, q2.Answer_11__c, 'NZ mapped questions not pre-loaded');
        System.assertEquals(null, q2.Answer_17__c, 'NZ mapped questions not pre-loaded');

    }

    // this emulates what a client editor does in its onchange handler
    private static void setAnswer(Map<String, Object> fromClient, String answerName, Object answerValue, String label) {
        // when the client sees a change, it updates the sobject map
        Map<Object, Object> record = (Map<Object, Object>) fromClient.get('RECORD');
        if (EditorPageUtils.isPropertyField(answerName)) {
            fromClient.put(answerName, answerValue);
        } else {
            record.put(answerName, answerValue);
        }
        // and writes the label was used for the field changed
        Map<Object, Object> mappings = (Map<Object, Object>) fromClient.get('LABEL-MAPPINGS');
        if (mappings == NULL) {
            mappings = new Map<Object, Object>();
            fromClient.put('LABEL-MAPPINGS', mappings);
        }
        mappings.put(answerName, label);
        // and writes a dirty flag for the field changed 
        Map<Object, Object> dirty = (Map<Object, Object>) fromClient.get('DIRTY');
        if (dirty == NULL) {
            dirty = new Map<Object, Object>();
            fromClient.put('DIRTY', dirty);
        }
        dirty.put(answerName, true);
    }

}