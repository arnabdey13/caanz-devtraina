/**
    Developer: WDCi (Lean)
    Development Date: 05/04/2016
    Task #: Enrollment wizard - Controller for Course selection

    Change History:
    PAN:6009 26/03/2019 - WDCi Lean: add late enrollment date and late enrollment charge for student
    PAN:6223 16/05/2019 - WDCi Lean: location selection for CASM
**/

public without sharing class luana_EnrolmentWizardCourseController extends luana_EnrolmentWizardObject{
    
    luana_EnrolmentWizardController stdController;

    //PAN:6223
    public String selectedLocationId {set; get;}
    //public List<LuanaSMS__Course__c> availableCourses {set; get;}
    public boolean enableLocSelection {private set; get;}

    public luana_EnrolmentWizardCourseController(luana_EnrolmentWizardController stdController){
        this.stdController = stdController;
        
        //PAN:6223 - start
        selectedLocationId = '';
        enableLocSelection = false;

        if(String.isNotBlank(this.stdController.selectedProductType) && this.stdController.selectedProductType == luana_EnrolmentConstants.PRODUCTTYPE_MASTERCLASS.toLowerCase()){
            enableLocSelection= true;
        }

        //PAN:6223 - end
        
    }
    
    public luana_EnrolmentWizardCourseController(){
       
    }

    //PAN:6223 
    public List<LuanaSMS__Course__c> getAvailableCourses(){
        List<LuanaSMS__Course__c> availableCourses = new List<LuanaSMS__Course__c>();

        if(this.stdController.selectedProgramOffering != null){
            if(this.stdController.selectedProductType != luana_EnrolmentConstants.PRODUCTTYPE_MASTERCLASS.toLowerCase()){
                availableCourses = this.stdController.selectedProgramOffering.LuanaSMS__Courses__r;
            } else {
                system.debug('test :: ' + selectedLocationId); 

                if(String.isNotBlank(selectedLocationId)){
                    for(LuanaSMS__Course__c course : this.stdController.selectedProgramOffering.LuanaSMS__Courses__r){
                        if(course.CASM_Course_Delivery_Location__c == selectedLocationId){
                            availableCourses.add(course);
                        }
                    }
                }
            }
        }

        return availableCourses;
    }

    //PAN:6223 act as a refresh function for location picklist
    public void refreshCourses(){

    }
    
    //PAN:6223
    public List<SelectOption> getLocations(){

        List<SelectOption> opts = new List<SelectOption>();
        SelectOption defaultOpt = new SelectOption('', '-- Please select one --');
        opts.add(defaultOpt);

        Set<Id> locIds = new Set<Id>();
        if(this.stdController.selectedProductType == luana_EnrolmentConstants.PRODUCTTYPE_MASTERCLASS.toLowerCase()){
            for(LuanaSMS__Course__c course : this.stdController.selectedProgramOffering.LuanaSMS__Courses__r){
                if(!locIds.contains(course.CASM_Course_Delivery_Location__c)){
                    SelectOption locOpt = new SelectOption(course.CASM_Course_Delivery_Location__c, course.CASM_Course_Delivery_Location__r.Name);
                    opts.add(locOpt);

                    locIds.add(course.CASM_Course_Delivery_Location__c);
                }
            }
        }

        return opts;
    }
    
    public PageReference courseNext(){
        
        if(stdController.selectedCourseId != null && stdController.selectedCourseId != '' && stdController.selectedProgramOffering != null){
            
            for(LuanaSMS__Course__c course : stdController.selectedProgramOffering.LuanaSMS__Courses__r){
                if(course.Id == stdController.selectedCourseId){
                    stdController.selectedCourse = course;
                    
                    //PAN:6009 apply late enrolment fee for non-manual enrolment, and hide the employer token section
                    if(!stdController.isManualEnrolment() && stdController.selectedCourse.Enrolment_End_Date__c < System.today()){
                        stdController.initLateEnrolmentFee();
                        stdController.selectedProgramOffering.Support_Employment_Token__c = false;
                    }

                    break;
                }
            }
            
            PageReference pageRef;
            stdController.skipValidation = true;
            if(stdController.selectedProgramOffering.Require_Subject_Selection__c){
                pageRef = Page.luana_EnrolmentWizardSubject;
            } else {
                stdController.populateSubjectOptions();
                stdController.getSubjectNextPage();
                
                pageRef = stdController.getTargetDetailsPage(stdController.selectedProgramOffering.Wizard_Details_Page_Name__c);
            }
            
            return pageRef;
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please select a course below.'));
        }
        
        return null;
    }
    
    public PageReference courseBack(){
        
        PageReference pageRef = Page.luana_EnrolmentWizardProgram;
        stdController.skipValidation = true;
        
        return pageRef;
    }
    
    
    public List<SelectOption> getCourseOptions(){
        
        List<SelectOption> options = new List<SelectOption>();
        
        if(stdController.selectedProgramOffering != null){
            for(LuanaSMS__Course__c course : stdController.selectedProgramOffering.LuanaSMS__Courses__r){
                SelectOption option = new SelectOption(course.Id, course.Name);
                                
                options.add(option); 
            }
        }
        
        return options;
    }
            
}