public class luana_PrintResultComponentCtl {
    
    public Id currentSPId {get; private set;}
    public Id currentContId {get; private set;}
    
    public List<LuanaSMS__Student_Program_Subject__c> spsList {get; set;}
    public List<AAS_Student_Assessment_Section_Item__c> examSASIList {get; set;}
    public List<tempSAS> nonExamSASList {get; set;}
    public List<tempSAS> examSASList {get; set;}
    public LuanaSMS__Student_Program__c currSpObj {get; set;}
    
    public Boolean hasHeaderVal;
    public Boolean getHasHeaderVal() {return hasHeaderVal;}
    public void setHasHeaderVal(Boolean hasHeader){
        hasHeaderVal = hasHeader;
    }
    
    public Boolean viewPDF {get; set;}
    
    public Boolean hasContains {get; set;}
    
    public luana_PrintResultComponentCtl(){
        
        Id userId = UserInfo.getUserId();
        System.debug('****userId:: ' + userId);
        
        hasContains = false;
        
        currentSPId = ApexPages.currentPage().getParameters().get('id');
        //viewPDF = Boolean.valueOf(ApexPages.currentPage().getParameters().get('isPDF'));
        System.debug('*****currentSPId:: ' + currentSPId);
        System.debug('****Is PDF:: ' + viewPDF);
        currSpObj = new LuanaSMS__Student_Program__c();
        
        Set<Id> latestSAId = new Set<Id>();
        
        for(LuanaSMS__Student_Program__c sp: [Select Id, Name, Result_Publish__c, LuanaSMS__Contact_Student__c, Transcript_Status__c, LuanaSMS__Contact_Student__r.Name, LuanaSMS__Contact_Student__r.Member_ID__c, LuanaSMS__Contact_Student__r.OtherAddress, 
                                                LuanaSMS__Contact_Student__r.OtherStreet, LuanaSMS__Contact_Student__r.OtherCity, LuanaSMS__Contact_Student__r.OtherState, LuanaSMS__Contact_Student__r.OtherPostalCode, LuanaSMS__Contact_Student__r.OtherCountry,
                                                LuanaSMS__Contact_Student__r.LastName, LuanaSMS__Contact_Student__r.FirstName, LuanaSMS__Status__c, LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.Product_Type__c, LuanaSMS__Course__r.Description__c,
                                                (Select Id, Score__c, Result__c from LuanaSMS__Student_Program_Subjects__r), (Select Id, createdDate, AAS_Course_Assessment__r.AAS_Final_Result_Release_Exam__c from Student_Assessments__r Where AAS_Course_Assessment__r.AAS_Final_Result_Release_Exam__c = true Order by createdDate DESC limit 1)
                                                from LuanaSMS__Student_Program__c Where Id =: currentSPId]){
            
            currSpObj = sp;
            
            spsList = new List<LuanaSMS__Student_Program_Subject__c>();                                    
            if(!sp.LuanaSMS__Student_Program_Subjects__r.isEmpty() && sp.Result_Publish__c){
                
                for(LuanaSMS__Student_Program_Subject__c sps: sp.LuanaSMS__Student_Program_Subjects__r){
                    spsList.add(sps);
                }
            }
            
            currentContId = sp.LuanaSMS__Contact_Student__c;
            
            for(AAS_Student_Assessment__c latestSA: sp.Student_Assessments__r){
                latestSAId.add(latestSA.Id);
                
                hasContains = true;
            }
        }
        
        nonExamSASList = new List<tempSAS>();
        examSASList = new List<tempSAS>();
        examSASIList = new List<AAS_Student_Assessment_Section_Item__c>();
        for(AAS_Student_Assessment_Section__c sas: [Select Id, AAS_Total_Final_Mark__c, AAS_Result__c, RecordType.Name, AAS_Student_Assessment__r.CreatedDate, 
                                                    (Select Id, Name, AAS_SASI_Result__c, AAS_Assessment_Schema_Section_Item__r.Name from Student_Assessment_Section_Items__r Order by AAS_Assessment_Schema_Section_Item__r.Name ASC)
                                                    from AAS_Student_Assessment_Section__c Where AAS_Student_Assessment__r.AAS_Student_Program_Subject__c in: spsList and AAS_Student_Assessment__c in: latestSAId]){
            if(sas.RecordType.Name =='Blackboard Assessment SAS' || sas.RecordType.Name =='Workshop Assessment SAS'){ 
                nonExamSASList.add(new tempSAS('Non-examination', sas));
            }else if(sas.RecordType.Name =='Exam Assessment SAS'){
                examSASList.add(new tempSAS('Examination', sas));
                
                for(AAS_Student_Assessment_Section_Item__c sasi: sas.Student_Assessment_Section_Items__r){
                    examSASIList.add(sasi);
                }
            }
        }
    }
    
    public class tempSAS {
        public String recordTypeName {get; set;}
        public AAS_Student_Assessment_Section__c sas {get; set;}
        
        public tempSAS(String rtName, AAS_Student_Assessment_Section__c sasObj) {
            recordTypeName = rtName;
            sas = sasObj;
        }
    }
}