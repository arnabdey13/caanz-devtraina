public class EditorSubscriptionsProgressBarService {

    @AuraEnabled
    public static Map<String, Object> load() {
        return load(EditorPageUtils.getUsersAccountId());
    }

    @TestVisible
    // the client expects only boolean values so this method translates the varying database values into booleans
    private static Map<String, Object> load(Id accountId) {

        Map<String, Object> result = new Map<String, Object> {'RECORD' => null};

        Account a = [
                select Id, Membership_Class__c, PersonOtherCountryCode
                from Account
                where Id = :accountId
        ];

        Boolean notificationsNotRequired = a.Membership_Class__c == 'Provisional' && a.PersonOtherCountryCode == 'NZ';

        result.put('prop.Notifications',  !notificationsNotRequired);

        return result;
    }

}