// Marked as without sharing as this class will need to check for existing members
global without sharing class RegistrationRedirector {
	// Global variables
	public String strDebug {get;set;}	
	public Boolean bShowHeader {get;set;}		
	public static String endpoint = 'https://charteredaccountantsanz.tfaforms.net/rest';		
	// Page Variables 
	public String strRecordTypeParam {get;set;}	
	public String strAccountIdParam {get;set;}		
	public String strEmail {get;set;}
	public String strFirstName {get;set;}
	public String strLastName {get;set;}
	public String resBody {get;set;}
	// Internal Variables 	
	private Id userRecordId {get;set;}	
	private String strFormId {get;set;}
	public static String strMessage {get;set;}
	// Constructor
	public RegistrationRedirector() {	

	}
	@RemoteAction
	global static Boolean validateUser(String firstName, String lastName, String email) { 
		Boolean bReturnValue = true;
		// Check if applicant is already an existing user
		if(email != null && firstName != null && lastName != null && email != '' && firstName != '' && lastName != '') {	
			// Validate against FirstName + LastName + Email
			List<Account> membersList = new List<Account>();
			membersList = [select Id, FirstName, LastName, PersonEmail from Account 
							where FirstName = :firstName and LastName = :lastName
							and PersonEmail = :email and IsPersonAccount = true];
																	
			if(membersList.size()>0) {
				strMessage = 'You may already have an account with us. Please contact customer service for assistance';
				// addPageMessage(ApexPages.severity.WARNING, strMessage);
				bReturnValue = false;
			}
			else {
				// addPageMessage('OK');
			}				
		}
		else {
			strMessage = 'Incomplete details provided';
			// addPageMessage(ApexPages.severity.ERROR, strMessage);
			bReturnValue = false;
		}
		return bReturnValue;
	}
	public PageReference showError() {
		strMessage = 'You may already have an account with us. Please contact customer service for assistance';
		addPageMessage(ApexPages.severity.ERROR, strMessage);
		return null;
	}
	// Redirect and Login User	
	public PageReference RedirectAndLogin() {
		PageReference pref = null;
		Boolean bReturnValue = true;
		// Read URL parameters	
		Map<String, String> mapPageParameters = ApexPages.currentPage().getParameters();	
		// Record Type parameter	
		strRecordTypeParam = (mapPageParameters.get('RecordType') != null)?
			(String) mapPageParameters.get('RecordType'):null;		
		if(strRecordTypeParam == null) {
			strMessage = 'RecordType not provided';
			addPageMessage(ApexPages.severity.ERROR, strMessage);
			bReturnValue = false;
		}	
		// Account Id parameter	
		strAccountIdParam = (mapPageParameters.get('AccountId') != null)?
			(String) mapPageParameters.get('AccountId'):null;		
		if(strAccountIdParam == null) {
			strMessage = 'AccountId not provided';
			addPageMessage(ApexPages.severity.ERROR, strMessage);
			bReturnValue = false;
		}
		if(bReturnValue == false) // exit and show errors
			return null;	
		else {
			if(strAccountIdParam != null) {
				Schema.DescribeSObjectResult r = Application__c.sObjectType.getDescribe();
				String keyPrefix = r.getKeyPrefix();
				system.debug('###RecordType: ' + EncodingUtil.urlDecode(strRecordTypeParam, 'UTF-8'));
				String strStartURL = '/' + keyPrefix + '/e?retURL=/' + keyPrefix + 
										'&r=' + String.valueOf(Math.random()) + // randomizer
										'&Account=' + strAccountIdParam;	
				RecordType rt = [select Id, Name, DeveloperName from RecordType 
									where Name = : EncodingUtil.urlDecode(strRecordTypeParam, 'UTF-8')
									and  sObjectType = 'Application__c' limit 1];			
				if(rt != null && rt.Name != null) {
					strStartURL += '&RecordType=' + EncodingUtil.urlEncode(rt.Name, 'UTF-8'); // Record Type Name
					strStartURL += '&RecordType=' + rt.Id; // Record Type Name		
				}										
				// Determine if page is viewed from COMMUNITIES PORTAL OR ANONYMOUSLY
				bShowHeader = (ApplicationOverride.IsCommunityProfileUser())? true:false;
				strStartURL += '&IsPortal=' + String.valueOf(bShowHeader);													
				List<Account> createdAccountList = new List<Account>();
				createdAccountList = [select Id, Member_Of__c, PersonContactId, FirstName, LastName, 
											PersonEmail, Customer_ID__c from Account where Id = :strAccountIdParam limit 1];	
				if(createdAccountList.size()>0)	{									
					List<User> existinPortalUsers = new List<User>();
					existinPortalUsers = [select Id from User where ContactId = :createdAccountList[0].PersonContactId];		
					if(existinPortalUsers.size() == 0) {
						User u = new User();
						u = createCommunityMemberUser(strAccountIdParam); // create portal user
						return Site.login(u.username, 'password', strStartURL);
					}
					else {
						pref = new PageReference(strStartURL);
						addPageMessage(strStartURL);
						return pref;						
					}
				}
				else {
					pref = new PageReference(strStartURL);
					addPageMessage(strStartURL);
					return pref;
				}
			}
		}	
		return pref;
	}
	// Create communities portal user	
	public User createCommunityMemberUser(String accountId) {
		List<Account> createdAccountList = new List<Account>();
		createdAccountList = [select Id, Member_Of__c, PersonContactId, FirstName, LastName, 
									PersonEmail, Customer_ID__c from Account where Id = :accountId limit 1];
		User u = new User();	
		if(createdAccountList.size()>0) {
			Account account = createdAccountList[0];
			
			u.EmailEncodingKey = 'ISO-8859-1';
			u.LanguageLocaleKey = 'en_US';
			u.FirstName = account.FirstName; 
			u.LastName = account.LastName;
			u.Email = account.PersonEmail; 
			u.ContactId = account.PersonContactId;
			
			String UserHexCode = encodingUtil.convertToHex(Crypto.generateDigest('SHA1', 
				blob.valueOf(String.valueOf(Crypto.getRandomInteger())))).subString(0,8);
			u.CommunityNickname = UserHexCode;
			u.Alias = UserHexCode;
			
			//u.FederationIdentifier = account.Customer_ID__c; // Customer_ID__c;
			// System.debug('### FederationIdentifier: ' + u.FederationIdentifier);
			u.ProfileId = ProfileCache.getId('NZICA Community Login User');
			
			Boolean isMemberOfNzica = (account.Member_Of__c!='NZICA')? false:true;
			u.TimeZoneSidKey = (isMemberOfNzica)? 'Pacific/Auckland':'Australia/Sydney';
			u.LocaleSidKey = (isMemberOfNzica)?'en_NZ':'en_AU';
			u.UserName = account.PersonEmail;
			//u.UserName = account.Customer_ID__c + ((isMemberOfNzica)?'@nzica.co.nz':'@caanz.com');

			Database.insert(u);
		}
		return u;
		// Error handling
	}	  
	// Generate Form Assembly URL
	public PageReference generateFormURL() {
		PageReference pref = null;	
		// Default Values		
		Map<String, String> mapPageParameters = ApexPages.currentPage().getParameters();		
		strRecordTypeParam = (mapPageParameters.get('RecordType') != null)?
			(String) mapPageParameters.get('RecordType'):null;		
		if(strRecordTypeParam == null) {
			strMessage = 'Please provide application type';
			addPageMessage(ApexPages.severity.ERROR, strMessage);
			return null;
		}
				
		// Read configuration settings
		Map<String, Application_Form__c> mapApplicationFormSettings = Application_Form__c.getAll();
		if(mapApplicationFormSettings.size() == 0) {
			addErrorMessage('Custom settings not defined'); 
		}	
		for(String s:mapApplicationFormSettings.keySet()) {
			Application_Form__c setting = mapApplicationFormSettings.get(s);
			if(setting.Name != null && setting.Record_Type__c!= null) {
				if((setting.Name).toLowerCase() == 'registration' && (setting.Record_Type__c).toLowerCase() == 'registration') {
					strFormId = setting.Form_ID__c; // FORM ID	 	
					break;
				}
			}
		}
		if(strFormId == null) addErrorMessage('Form Assembly Page not defined in custom settings'); 			
			
		// Build parameters	
		String strParameters = 'SessionId=' + UserInfo.getSessionId(); // Session ID
		RecordType rt = [select Id, Name, DeveloperName from RecordType 
							where Name = : EncodingUtil.urlDecode(strRecordTypeParam, 'UTF-8')
							and  sObjectType = 'Application__c' limit 1];			
		if(rt != null && rt.Name != null) {
			strParameters += '&RecordType=' + EncodingUtil.urlEncode(rt.Name, 'UTF-8'); // Record Type Name
			strParameters += '&RecordTypeId=' + rt.Id; // Record Type Name		
		}	
		strParameters += '&r=' + String.valueOf(Math.random()); // randomizer
		system.debug('strFormId: ' + strFormId);	
		system.debug('strParameters: ' + strParameters);  
		// addPageMessage(strParameters); 	
		// Build PageReference
		if(strFormId != '') {
			callFormAssembly(strFormId, strParameters);		
		}
		else {
			addErrorMessage('Application form not found. Please contact your administrator.');
		}		
		return null;		
	}	
	// Execute web service call 
	public void callFormAssembly(String strFormID, String strRequestParameters) {
		HTTPResponse response;		
		String strURL;
		PageReference pref = ApexPages.currentPage();
		HttpRequest request = new HttpRequest();
		request.setMethod('GET');
		request.setTimeout(60000); // timeout
		// Build URL
		if(pref.getParameters().get('tfa_next') == null) {
			strURL = endpoint + '/forms/view/' + strFormID;
		}
		else {
			strURL = endpoint + pref.getParameters().get('tfa_next');
		}
		// Add parameters
		if(strRequestParameters != null && strRequestParameters.length()>0) {
			strURL = strURL + '?' + strRequestParameters;
		} 
		system.debug('strURL: ' + strURL);	
		// addPageMessage('strURL: ' + strURL);	
		request.setEndpoint(strURL);
		Http http = new Http();
        try {			
			response = http.send(request); // Execute web service call 
			resBody = response.getBody();
		} 
        catch(System.CalloutException e) {
            system.debug('CalloutException: ' + e);
            addErrorMessage(e);
        }		        
	}		
	// Page Messages
	public static void addPageMessage(ApexPages.severity severity, Object objMessage) {
		ApexPages.Message pMessage = new ApexPages.Message(severity, String.valueOf(objMessage));
		ApexPages.addMessage(pMessage);
	}
	public static void addPageMessage(Object objMessage) {
		addPageMessage(ApexPages.severity.INFO, objMessage);
	}		
	public static void addErrorMessage(Object objMessage) {
		addPageMessage(ApexPages.severity.ERROR, objMessage);
	}		
}