/* 
    Developer: WDCi (KH)
    Development Date: 09/11/2016
    Task #: AAS borderline remark for exam and sub-exam  
*/

public with sharing class AAS_BorderlineRemarkController {

    public AAS_Course_Assessment__c courseAssessment {get; set;}
    public String minMark {get; set;}
    public String maxMark {get; set;}
    
    public Boolean validToProcess {get; set;}
    
    public String selectedRecordType{get;set;}
    public String searchTypeVal {get; set;}
    
    public Integer numberOfBorderlineRecord {get; set;}
    public boolean hasNoRecords {get; private set;}
    public boolean hasReviewed {get; private set;}
    public boolean showButton {get; private set;}
    public boolean hasExceedFullMark {get; private set;}
    public String exceedFullMarkReportId {get; set;}
    public String sasRecordTypeId;
    
    public String urlSelectedType {get; set;}
    
    private List<AAS_Student_Assessment_Section__c> searchResults {get; set;}
    
    public AAS_BorderlineRemarkController(ApexPages.StandardController controller) {
        
        if(!test.isRunningTest()){
            List<String> addFieldName = new List<String>{'AAS_Cohort_Adjustment_Exam__c', 'AAS_Course__r.LuanaSMS__Program_Offering__r.IsCapstone__c', 'AAS_Module_Passing_Mark__c',
                                                        'AAS_Borderline_Remark__c', 'AAS_Cohort_Adjustment_Supp_Exam__c',  'AAS_Borderline_Remark_Supp_Exam__c'};
            controller.addFields(addFieldName);
        }
        courseAssessment = (AAS_Course_Assessment__c) controller.getRecord();
        
        numberOfBorderlineRecord = 0;
        hasNoRecords = false;
        hasReviewed = false;
        showButton = false;
        hasExceedFullMark = false;
        
        urlSelectedType = apexpages.currentpage().getparameters().get('type');

        selectedRecordType = urlSelectedType + '_SAS';

        //Module change
        Double borderlineRange = 0;
        List<AAS_Settings__c> aasSets = AAS_Settings__c.getall().values();
        for(AAS_Settings__c assSetting: aasSets){
            if(assSetting.Name == 'Default Borderline Range'){
                borderlineRange = Integer.ValueOf(assSetting.Value__c);
            }
        }
        
        Double passingMark = 0;
        for(AAS_Assessment_Schema_Section__c ass: [Select Id, RecordTypeId, RecordType.Name, AAS_Passing_Mark__c from AAS_Assessment_Schema_Section__c 
                                                    Where AAS_Course_Assessment__c =: courseAssessment.Id and RecordType.DeveloperName =: urlSelectedType limit 1]){
            passingMark = ass.AAS_Passing_Mark__c;                             
        }
        if(!courseAssessment.AAS_Course__r.LuanaSMS__Program_Offering__r.IsCapstone__c){
            if(selectedRecordType == 'Exam_Assessment_SAS'){
                searchTypeVal = 'By Exam Mark';  
            }else if(selectedRecordType == 'Supp_Exam_Assessment_SAS'){
                searchTypeVal = 'By Supp Exam Mark';
            }
            minMark = String.valueOf(passingMark + borderlineRange);
            maxMark = String.ValueOf(passingMark);
        }else{
            if(selectedRecordType == 'Exam_Assessment_SAS'){
                searchTypeVal = 'By Module Mark (Non Exam + Exam)';            
            }else if(selectedRecordType == 'Supp_Exam_Assessment_SAS'){
                searchTypeVal = 'By Module Mark (Non Exam + SuppExam)';
            }
            minMark = String.valueOf(courseAssessment.AAS_Module_Passing_Mark__c + borderlineRange);
            maxMark = String.ValueOf(courseAssessment.AAS_Module_Passing_Mark__c);
        }
        
        doValidation();
        
    }
    
    public void doValidation(){
        
        //Reset the hasRecord flag to remove the record link
        hasReviewed = false;
        showButton = false;
        if(selectedRecordType == 'Exam_Assessment_SAS'){
            if(courseAssessment.AAS_Cohort_Adjustment_Exam__c){
                validToProcess = true;
            }else{
                validToProcess = false;
                addPageMessage(ApexPages.severity.ERROR, 'You are not allow to continue in this step. Please complete the Cohort Adjustment first.');
            }
            if(courseAssessment.AAS_Borderline_Remark__c){
                addPageMessage(ApexPages.severity.WARNING, 'Borderline Remark process was completed before. If you wish to continue, previous checked Remark flag will not be undone. You will have to remove them manually.');
            }
        }else if(selectedRecordType == 'Supp_Exam_Assessment_SAS'){
            if(courseAssessment.AAS_Cohort_Adjustment_Supp_Exam__c){
                validToProcess = true;
            } else{
                validToProcess = false;
                addPageMessage(ApexPages.severity.ERROR, 'You are not allow to continue in this step. Please complete the Cohort Adjustment first.');
            }
            if(courseAssessment.AAS_Borderline_Remark_Supp_Exam__c){
                addPageMessage(ApexPages.severity.WARNING, 'Borderline Remark process was completed before. If you wish to continue, previous checked Remark flag will not be undone. You will have to remove them manually.');
            }
        }
    }
    
    public void doNext(){
        try{
            hasReviewed = true;
            searchResults = new List<AAS_Student_Assessment_Section__c>();
            
            String filterValue ='';
            if(!courseAssessment.AAS_Course__r.LuanaSMS__Program_Offering__r.IsCapstone__c){
                filterValue = '(AAS_Total_Final_Mark__c >=' + Decimal.ValueOf(minMark) + ' and AAS_Total_Final_Mark__c < ' + Decimal.ValueOf(maxMark) + ')';
            }else{
                if(selectedRecordType == 'Exam_Assessment_SAS'){
                    filterValue = '(AAS_Student_Assessment__r.AAS_Total_Module_Mark_Non_Exam_Exam__c >=' + Decimal.ValueOf(minMark) + ' and AAS_Student_Assessment__r.AAS_Total_Module_Mark_Non_Exam_Exam__c < ' + Decimal.ValueOf(maxMark) + ')';
                }else if(selectedRecordType == 'Supp_Exam_Assessment_SAS'){
                    filterValue = '(AAS_Student_Assessment__r.AAS_Total_Module_Mark_Non_Exam_Supp_Exam__c >=' + Decimal.ValueOf(minMark) + ' and AAS_Student_Assessment__r.AAS_Total_Module_Mark_Non_Exam_Supp_Exam__c < ' + Decimal.ValueOf(maxMark) + ')';
                }
            }
            
            String sasQuery = 'Select Id, AAS_Total_Calculated_Mark__c, AAS_Student_Assessment__c, AAS_Total_Final_Mark__c, AAS_Exceed_Full_Mark_Count__c, RecordTypeId, '+
                                            'AAS_Student_Assessment__r.AAS_Total_Module_Mark_Non_Exam_Exam__c, AAS_Student_Assessment__r.AAS_Total_Module_Mark_Non_Exam_Supp_Exam__c '+
                                            'from AAS_Student_Assessment_Section__c '+
                                            'Where AAS_Student_Assessment__r.AAS_Course_Assessment__c = \'' + courseAssessment.Id + '\' and RecordType.DeveloperName = \'' + selectedRecordType + '\' and ';

            searchResults = database.query(sasQuery + filterValue);
            
            if(!searchResults.isEmpty()){
                hasNoRecords = false;
                showButton = true;

                addPageMessage(ApexPages.severity.INFO, 'Total ' + searchResults.size() + ' record/s found within the range. Please press the \'Mark Remark Flag\' button to set the \'Remark\' flag to \'True\'');
            }else{
                hasNoRecords = true;
                showButton = false;
                
                addPageMessage(ApexPages.severity.INFO, 'Total ' + searchResults.size() + ' record/s found within the range. Please press on the \'Done\' button to set \'Borderline Remark\' at Course Assessment as True to indicate that Borderline Remark is completed.');
            }
            //doComplete();
        
        }Catch(Exception exp){
            addPageMessage(ApexPages.severity.ERROR, 'Error processing. Please try again later or contact your system administrator. Error: ' + exp.getMessage() + ' - ' + exp.getStackTraceString());
        }
    }
   
    public pageReference doRemark(){
        Savepoint sp = Database.setSavepoint();
        try{
            
            if(validToProcess){
                if(selectedRecordType != null){
                    
                    for(AAS_Student_Assessment_Section__c sas: searchResults){
                        if(sas.AAS_Exceed_Full_Mark_Count__c > 0){
                            hasExceedFullMark = true;
                        }
                        sasRecordTypeId = sas.RecordTypeId;
                    }
                    
                    if(!hasExceedFullMark){
                        numberOfBorderlineRecord = searchResults.size();
                        if(!searchResults.isEmpty()){
                            
                            List<AAS_Student_Assessment_Section__c> sasList = new List<AAS_Student_Assessment_Section__c>();
                            for(AAS_Student_Assessment_Section__c sas: searchResults){
                                sas.AAS_Remark__c = true;
                                sasList.add(sas);
                            }
                            update sasList;
                            doComplete();
                            
                            PageReference redirectPage = new PageReference('/'+courseAssessment.Id);
                            redirectPage.setRedirect(true);
                            return redirectPage;
                        }
                    }else{
                        validToProcess = false;
                        hasReviewed = false;
                        addPageMessage(ApexPages.severity.ERROR, 'Error found! One of the record has exceeded full mark, please refer to <a href="/'+exceedFullMarkReportId+sasRecordTypeId.subString(0, 15)+'" target="_blank">here</a> to fix the record manually.');
                    }
                    

                    
                }else{
                    addPageMessage(ApexPages.severity.WARNING, 'Please select Type.');
                }
            
            }
        }Catch(Exception exp){
            Database.rollback(sp);
            addPageMessage(ApexPages.severity.ERROR, 'Error processing. Please try again later or contact your system administrator. Error: ' + exp.getMessage() + ' - ' + exp.getStackTraceString());
        }
        
        return null;
    }
    
    /*Change to show the report in CA custom link    
    public PageReference getReport(){
        
        PageReference retURL = new PageReference('/' + borderlineReportId+sasRecordTypeId.subString(0, 15));
        retURL.setRedirect(true);
        return retURL;
    }
    */
    
    public PageReference doCancel(){
        PageReference returnPage = new PageReference('/'+courseAssessment.Id);
        return returnPage;
    }
    
    public PageReference doComplete(){
        //update ca Borderlink remark
        if(selectedRecordType == 'Exam_Assessment_SAS'){
            courseAssessment.AAS_Borderline_Remark__c = true;
        }else if(selectedRecordType == 'Supp_Exam_Assessment_SAS'){
            courseAssessment.AAS_Borderline_Remark_Supp_Exam__c = true;
        }
        update courseAssessment;
        
        PageReference returnPage = new PageReference('/'+courseAssessment.Id);
        return returnPage;
    }
    
    public List<SelectOption> getRecordTypeOptions() {
        
        Schema.DescribeFieldResult statusFieldDescription = RecordType.DeveloperName.getDescribe();
        List<SelectOption> statusOptions = new list<SelectOption>();
        
        //May need to change the query filter to handle supp exam in future
        for(AAS_Assessment_Schema_Section__c ass: [Select Id, RecordType.Name, RecordType.developerName from AAS_Assessment_Schema_Section__c 
                                                    Where AAS_Course_Assessment__c =: courseAssessment.Id and RecordType.DeveloperName = 'Exam_Assessment']){
            statusOptions.add(new SelectOption(ass.RecordType.developerName+'_SAS', ass.RecordType.Name));
        }
        
 
        return statusOptions;
    }
    
    public void addPageMessage(ApexPages.severity sev, String msg){
        ApexPages.Message warningmsg = new ApexPages.Message(sev, msg);
        ApexPages.addmessage(warningmsg);
    }
    
  
}