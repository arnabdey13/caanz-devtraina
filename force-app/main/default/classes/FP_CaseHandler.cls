/*
    Developer: WDCi (Vin)
    Date: 8/Oct/2018
    Task #: CAANZ - Flexipath   
*/

public with sharing class FP_CaseHandler{
    
    public static void upsertGainedCompetencyArea(List<Case> caseList){
        
        List<FP_Gained_Competency_Area__c> newGCAUpsert = new List<FP_Gained_Competency_Area__c>();
        
        for(Case caseRequestCA : caseList) {
            //Construct External Id for GCA. This will be used for upsert operation later
            FP_Gained_Competency_Area__c gca = new FP_Gained_Competency_Area__c(FP_External_Id__c=caseRequestCA.ContactId+'|'+caseRequestCA.FP_Requested_Competency_Area__c, FP_Contact__c=caseRequestCA.ContactId, FP_Competency_Area__c=caseRequestCA.FP_Requested_Competency_Area__c,  FP_Case__c=caseRequestCA.Id);
            newGCAUpsert.add(gca);
            
        }
        //Upsert the list of related GCA
        upsert newGCAUpsert FP_External_Id__c;
    }
}