/**
 * Developer: WDCi (LKoh)
 * Date: 22-10-2018
 * Task #: PAN5340 - Flexipath SPS Competency Area related test class
**/
@isTest
public class FP_SPSCompetencyArea_TEST {

    static testMethod void validateSPSCompetencyArea() {
        
        Map<String, SObject> assetMap = FP_Test_UTIL.assetGenerator('SPSCA');
        
        FP_Relevant_Competency_Area__c rca1 = FP_Test_UTIL.createRCA(assetMap.get('ca1').Id, 'Gains', assetMap.get('subject1').Id);
        insert rca1;
        
        // Custom Metadata that contains the acceptable SPS Outcome
        List<FP_PositiveOutcome__mdt> checkMD = [SELECT Id, MasterLabel, Positive_Outcome__c FROM FP_PositiveOutcome__mdt WHERE Positive_Outcome__c = true AND MasterLabel = 'Competency achieved/pass'];
        system.assert(checkMD != null && checkMD.size() > 0, 'Could not find Positive Outcome Custom MetaData with matching MasterLabel');
                        
        test.startTest();
        
        // Create SPS that fulfill the SPSCompetencyAreaHandler requirement
        LuanaSMS__Student_Program_Subject__c sps1 = FP_Test_UTIL.createSPS(assetMap.get('subject1').Id, assetMap.get('cont1').Id, checkMD[0].MasterLabel);
        insert sps1;
        
        test.stopTest();
	}    
}