/**
* @author           WDCi-LKoh
* @date             11/06/2019
* @group            Competency Automation
* @description      Trigger Handler for ApplicationTrigger
* @change-history   #FIX003 -   WDCi-Lkoh   -   05/09/2019  -   Fix for issue with the Pathway evaluation assessment
* @change-history   #FIX006 -   WDCi-Lkoh   -   10/09/2019  -   Improved Pathway evaluation assessment
*/
public with sharing class wdci_ApplicationHandler {

    public static void initFlow(List<Application__c> newList, List<Application__c> oldList, Map<Id, Application__c> oldMap, boolean isInsert, boolean isUpdate, boolean isDelete, boolean isUndelete, boolean isBefore, boolean isAfter) {
        
        system.debug('Entering initFlow: ' +newList+ ' : ' +oldList+ ' : ' +oldMap+ ' : ' +isInsert+ ' : ' +isUpdate+ ' : ' +isDelete+ ' : ' +isUndelete+ ' : ' +isBefore+ ' : ' +isAfter);
        if (isAfter) {
            if (isInsert) {
                evaluateCompetency(newList);
            } else if (isUpdate) {
                List<Application__c> appListToProcess = new List<Application__c>();
                for (Application__c app : newList) {                    
                    if (oldMap.get(app.Id).Resumption_Stage_Number__c != app.Resumption_Stage_Number__c) {
                        appListToProcess.add(app);
                    }
                }
                if (appListToProcess.size() > 0) {
                    evaluateCompetency(appListToProcess);
                }
            }
        }
        system.debug('Exiting initFlow');
    }

    public static void evaluateCompetency(List<Application__c> applicationList) {

        system.debug('Entering evaluateCompetency: ' +applicationList);
        Set<Id> allUniversityIdSet = new Set<Id>();
        Set<Id> applicationIdSet = new Set<Id>();
        Set<Id> studentIdSet = new Set<Id>();

        for (Application__c app : applicationList) {
            applicationIdSet.add(app.Id);
            studentIdSet.add(app.Account__c);
        }

        // Contact Map for the Student involved
        Map<Id, Contact> personAccountContactMap = wdci_CompetencyUtil.mapPersonAccountContact(studentIdSet);        

        // Student Paper involved for the referenced Applications (1 Student Paper per unique UniSubject on the application)
        Map<Id, Map<Id, FT_Student_Paper__c>> studentPaperMap = wdci_CompetencyUtil.mapStudentPaper(applicationIdSet);

        system.debug('studentPaperMap: ' +studentPaperMap);

        Set<Id> allStudentPaperIdSet = new Set<Id>();
        Map<Id, Set<Id>> universityMap = new Map<Id, Set<Id>>(); // Map of the University involved for each Application
        for (Id applicationId : studentPaperMap.keySet()) {
            Map<Id, FT_Student_Paper__c> studentPaperMapForTheApplication = studentPaperMap.get(applicationId);
            for (Id uniSubjectId : studentPaperMapForTheApplication.keySet()) {

                FT_Student_Paper__c currentStudentPaper = studentPaperMapForTheApplication.get(uniSubjectId);
                allStudentPaperIdSet.add(currentStudentPaper.Id);

                allUniversityIdSet.add(currentStudentPaper.FT_Education_History__r.University_Degree__r.University__c);
                if (universityMap.containsKey(applicationId)) {
                    universityMap.get(applicationId).add(currentStudentPaper.FT_Education_History__r.University_Degree__r.University__c);
                } else {
                    Set<Id> universityIdSet = new Set<Id>();
                    universityIdSet.add(currentStudentPaper.FT_Education_History__r.University_Degree__r.University__c);
                    universityMap.put(applicationId, universityIdSet);
                }
            }
        }

        Map<String, FT_Paper_Gained_Competency__c> paperGainedCompetencyMap = wdci_CompetencyUtil.mapPaperGainedCompetency(allStudentPaperIdSet);

        // Pathway involved for the referenced Universities
        Map<Id, List<FT_Pathway__c>> pathwayMap = wdci_CompetencyUtil.mapPathway(allUniversityIdSet);

        // Subjects involved for the referenced Universities
        Map<Id, List<FT_University_Subject__c>> uniSubjectMap = wdci_CompetencyUtil.mapUniSubjectMk2(allUniversityIdSet);   // #FIX006
        // Map<Id, List<FT_University_Subject__c>> uniSubjectMap = wdci_CompetencyUtil.mapUniSubject(allUniversityIdSet);

        // Existing Gained Competency Area for the students involved
        Map<Id, Map<Id, FP_Gained_Competency_Area__c>> gainedCompetencyAreaMap = wdci_CompetencyUtil.mapGainedCompetencyArea(personAccountContactMap.values());        

        Map<Application__c, Map<Id, FT_Pathway__c>> fulfilledPathwayMap = new Map<Application__c, Map<Id, FT_Pathway__c>>();
        Map<String, Map<Id, FT_Student_Paper__c>> studentPaperContributionMap = new Map<String, Map<Id, FT_Student_Paper__c>>();

        // Evaluate all the related Application against the possible Pathway to evaluate which Pathway are fulfilled
        for (Application__c application : applicationList) {
            if (universityMap.containsKey(application.Id)) {
                for (Id universityID : universityMap.get(application.Id)) {
                    if (pathwayMap.containsKey(universityID)) {
                        for (FT_Pathway__c potentialPathway : pathwayMap.get(universityID)) {
                            if (potentialPathway.FT_Active__c) {    // #FIX003
                                string logicStringToEvaluate = potentialPathway.FT_Pathway__c;

                                // system.debug('Unprocessed Logic String: ' +logicStringToEvaluate);                    
                                Map<Id, FT_Student_Paper__c> studentPaperContributingToThisPathway = new Map<Id, FT_Student_Paper__c>();

                                // We need to substitute the Subject Code + Subject Name on the Pathway
                                for (FT_University_Subject__c uniSubject : uniSubjectMap.get(universityID)) {

                                    // system.debug('uniSubject: ' +uniSubject);

                                    /* Lean - use code below due to matching issue
                                    if (logicStringToEvaluate.contains(uniSubject.FT_Pathway_Subject_Code__c)) {

                                        // system.debug('logicStringContains: ' +uniSubject.FT_Pathway_Subject_Code__c);                            
                                        Boolean studentHaveTakenTheSubject = false;
                                        Map<Id, FT_Student_Paper__c> studentPaperMapForTheApplication = studentPaperMap.get(application.Id);


                                        // If the Student have Student Paper with the same Uni Subject, then he/she has taken this particular Pathway required subject
                                        if (studentPaperMapForTheApplication.containsKey(uniSubject.Id)) {
                                            FT_Student_Paper__c studentPaperWithMatchingUniSubject = studentPaperMapForTheApplication.get(uniSubject.Id);
                                            studentHaveTakenTheSubject = true;
                                            studentPaperContributingToThisPathway.put(studentPaperWithMatchingUniSubject.Id, studentPaperWithMatchingUniSubject);
                                        }
                                        logicStringToEvaluate = logicStringToEvaluate.replace(uniSubject.FT_Pathway_Subject_Code__c, String.valueOf(studentHaveTakenTheSubject));                            
                                    }*/

                                    boolean isMatched = wdci_CompetencyUtil.isCodeMatches(logicStringToEvaluate, uniSubject.FT_Pathway_Subject_Code__c);
                                    
                                    if(isMatched){
                                        Boolean studentHaveTakenTheSubject = false;
                                        Map<Id, FT_Student_Paper__c> studentPaperMapForTheApplication = studentPaperMap.get(application.Id);


                                        // If the Student have Student Paper with the same Uni Subject, then he/she has taken this particular Pathway required subject
                                        if (studentPaperMapForTheApplication.containsKey(uniSubject.Id)) {
                                            FT_Student_Paper__c studentPaperWithMatchingUniSubject = studentPaperMapForTheApplication.get(uniSubject.Id);
                                            studentHaveTakenTheSubject = true;
                                            studentPaperContributingToThisPathway.put(studentPaperWithMatchingUniSubject.Id, studentPaperWithMatchingUniSubject);
                                        }

                                        logicStringToEvaluate = wdci_CompetencyUtil.replacePatternAll(logicStringToEvaluate, uniSubject.FT_Pathway_Subject_Code__c, String.valueOf(studentHaveTakenTheSubject));
                                    }
                                }
                                // system.debug('Currently on pathway: ' +potentialPathway.Id);                                
                                // system.debug('logicStringToEvaluate: ' +logicStringToEvaluate);
                                // system.debug('APEX CPU TIME USED: ' +Limits.getCpuTime());
                                // system.debug('APEX CPU TIME LIMIT LEFT: ' +Limits.getLimitCpuTime());
                                wdci_CompetencyUtil.LogicString ls = new wdci_CompetencyUtil.LogicString();
                                Boolean pathwayFulfilled = ls.LogicString(logicStringToEvaluate);
                                system.debug('Application: ' +application.Id+ ' - University: ' +universityID+ ' - Pathway: ' +potentialPathway.Name+ ' - Processed Logic String: ' +logicStringToEvaluate+ ' - Result: ' +pathwayFulfilled);

                                if (pathwayFulfilled) {
                                    // Map the pathway and the Application
                                    if (fulfilledPathwayMap.containsKey(application)) {
                                        fulfilledPathwayMap.get(application).put(potentialPathway.Id, potentialPathway);
                                    } else {
                                        Map<Id, FT_Pathway__c> newPathwayMap = new Map<Id, FT_Pathway__c>();
                                        newPathwayMap.put(potentialPathway.Id, potentialPathway);
                                        fulfilledPathwayMap.put(application, newPathwayMap);
                                    }

                                    // Map the application + pathway and the Uni Subject that contributed to the success of the pathway
                                    String key = application.Id + ':' + potentialPathway.Id;
                                    studentPaperContributionMap.put(key, studentPaperContributingToThisPathway);
                                }
                            }
                        }
                    }
                }
            }
        }
        system.debug('studentPaperContributionMap: ' +studentPaperContributionMap);

        // Generate the gained competency area and the paper gained competency for the Application Student based on the Pathway fulfilled
        if (!fulfilledPathwayMap.isEmpty()) {

            List<FP_Gained_Competency_Area__c> newGainedCompetencyAreaList = new List<FP_Gained_Competency_Area__c>();

            // Generate the Gained Competency Area if necessary
            for (Application__c application : fulfilledPathwayMap.keySet()) {

                Contact contactForCurrentApplication = personAccountContactMap.get(application.Account__c);
                Map<Id, FT_Pathway__c> fulfilledPathwayMapForApplication = fulfilledPathwayMap.get(application);

                for (Id pathwayId : fulfilledPathwayMapForApplication.keySet()) {

                    FT_Pathway__c fullFilledPathway = fulfilledPathwayMapForApplication.get(pathwayId);
                    
                    // There is a Gained Competency Area for the Student
                    if (gainedCompetencyAreaMap.containsKey(contactForCurrentApplication.Id)) {

                        // Check if the Gained Competency Area includes the matching Competency Area as the Pathway
                        if (gainedCompetencyAreaMap.get(contactForCurrentApplication.Id).containsKey(fullFilledPathway.FT_Competency_Area__c)) {
                            // An existing Gained Competency Area already exist
                            system.debug('Existing GCA: ' +gainedCompetencyAreaMap.get(contactForCurrentApplication.Id).get(fullFilledPathway.FT_Competency_Area__c));
                        } else {
                            FP_Gained_Competency_Area__c newGainedCompetencyArea = wdci_ApplicationHandler.generateGainedCompetencyArea(fullFilledPathway.FT_Competency_Area__c, contactForCurrentApplication.Id, fullFilledPathway.Id);
                            newGainedCompetencyAreaList.add(newGainedCompetencyArea);
                            
                            gainedCompetencyAreaMap.get(contactForCurrentApplication.Id).put(fullFilledPathway.FT_Competency_Area__c, newGainedCompetencyArea);
                        }
                    } else {
                        // No Gained Competency Area yet for the Student, generate new Gained Competency Area Map
                        Map<Id, FP_Gained_Competency_Area__c> newGainedCompetencyAreaMap = new Map<Id, FP_Gained_Competency_Area__c>();
                        FP_Gained_Competency_Area__c newGainedCompetencyArea = wdci_ApplicationHandler.generateGainedCompetencyArea(fullFilledPathway.FT_Competency_Area__c, contactForCurrentApplication.Id, fullFilledPathway.Id);
                        newGainedCompetencyAreaList.add(newGainedCompetencyArea);

                        newGainedCompetencyAreaMap.put(fullFilledPathway.FT_Competency_Area__c, newGainedCompetencyArea);
                        gainedCompetencyAreaMap.put(contactForCurrentApplication.Id, newGainedCompetencyAreaMap);
                    }
                }
            }

            system.debug('gainedCompetencyAreaMap: ' +gainedCompetencyAreaMap);
            system.debug('newGainedCompetencyAreaList: ' +newGainedCompetencyAreaList);
            if (newGainedCompetencyAreaList.size() > 0) insert newGainedCompetencyAreaList;                        

            // Generate the Paper Gained Competency
            List<FT_Paper_Gained_Competency__c> newPaperGainedCompetencyList = new List<FT_Paper_Gained_Competency__c>();
            for (Application__c application : fulfilledPathwayMap.keySet()) {
                if (studentPaperMap.containsKey(application.Id)) {
                    Map<Id, FT_Student_Paper__c> currentStudentPaperMap = studentPaperMap.get(application.Id);                                                                      // Student Paper related to this Application
                    Map<Id, FP_Gained_Competency_Area__c> currentGainedCompetencyAreaMap = gainedCompetencyAreaMap.get(personAccountContactMap.get(application.Account__c).Id);        // Gained Competency Area related to this Application Student 
                    Map<Id, FT_Pathway__c> currentFulfilledPathwayMap = fulfilledPathwayMap.get(application);                                                                       // Pathway that was fulfilled in this Application

                    for (Id pathwayId : currentFulfilledPathwayMap.keySet()) {

                        FT_Pathway__c currentFulfilledPathway = currentFulfilledPathwayMap.get(pathwayId);
                        String currentStudentPaperContributionKey = application.Id + ':' +pathwayId;

                        if (studentPaperContributionMap.containsKey(currentStudentPaperContributionKey)) {

                            // Map of the Student Paper that contributed to the success of this Pathway
                            Map<Id, FT_Student_Paper__c> currentStudentPaperContributionMap = studentPaperContributionMap.get(currentStudentPaperContributionKey);                            
                            
                            // Gained Competency Area related to the pathway
                            if (currentGainedCompetencyAreaMap.containsKey(currentFulfilledPathway.FT_Competency_Area__c)) {
                                FP_Gained_Competency_Area__c currentGainedCompetencyArea = currentGainedCompetencyAreaMap.get(currentFulfilledPathway.FT_Competency_Area__c);

                                for (Id currentStudentPaperID : currentStudentPaperContributionMap.keySet()) {

                                    // Check if there is already an existing Paper Gained Competency for the Student Paper and Competence Area Gained
                                    String paperGainedCompetencyKey = currentStudentPaperID+ ':' +currentGainedCompetencyArea.Id;
                                    if (paperGainedCompetencyMap.containsKey(paperGainedCompetencyKey)) {
                                        // Existing Paper Gained Competency already exist
                                    } else {
                                        // Paper Gained Competency linked with the Gained Competency Area and the Student Paper that contributed to it
                                        FT_Paper_Gained_Competency__c newPaperGainedCompetency = new FT_Paper_Gained_Competency__c();
                                        newPaperGainedCompetency.FT_Competence_Area_Gained__c = currentGainedCompetencyArea.Id;
                                        newPaperGainedCompetency.FT_Student_Paper__c = currentStudentPaperID;
                                        newPaperGainedCompetencyList.add(newPaperGainedCompetency);
                                    }                                    
                                }                                                            
                            }
                        }                        
                    }
                }
            }            
            system.debug('newPaperGainedCompetencyList: ' +newPaperGainedCompetencyList);
            if (newPaperGainedCompetencyList.size() > 0) insert newPaperGainedCompetencyList;            
        }

        // Re-Evaluating the Gained Competency Area for the Application to remove the ones no longer valid
        system.debug('Re-evaluating Gained Competency Area');
        system.debug('gainedCompetencyAreaMap: ' +gainedCompetencyAreaMap);
        if (gainedCompetencyAreaMap.size() > 0) {
            Set<FP_Gained_Competency_Area__c> gainedCompetencyAreaToReevaluate = new Set<FP_Gained_Competency_Area__c>();
            for (Map<Id, FP_Gained_Competency_Area__c> gcaMap :gainedCompetencyAreaMap.values()) {
                gainedCompetencyAreaToReevaluate.addAll(gcaMap.values());
            }
            
            List<Id> gcaIdList = new List<Id>();
            for (FP_Gained_Competency_Area__c gca : gainedCompetencyAreaToReevaluate) {
                gcaIdList.add(gca.Id);
            }
            system.debug('gcaIdList: ' +gcaIdList);

            evaluateGainedCompetencyAreaForInvalidMembers(gcaIdList);
        }
        system.debug('Exiting evaluateCompetency');        
    }

    public static FP_Gained_Competency_Area__c generateGainedCompetencyArea(Id competencyAreaId, Id studentContactId, Id pathwayId) {
        system.debug('Entering generateGainedCompetencyArea: ' +studentContactId+ ' | ' +competencyAreaId);
        FP_Gained_Competency_Area__c newGainedCompetencyArea = new FP_Gained_Competency_Area__c();
        newGainedCompetencyArea.FP_Competency_Area__c = competencyAreaId;
        newGainedCompetencyArea.FP_Contact__c = studentContactId;
        newGainedCompetencyArea.FP_External_Id__c = studentContactId + '|' + competencyAreaId;
        newGainedCompetencyArea.FT_Pathway__c = pathwayId;
        system.debug('Exiting generateGainedCompetencyArea: ' +newGainedCompetencyArea);
        return newGainedCompetencyArea;
    }

    public static void evaluateGainedCompetencyAreaForInvalidMembers(List<Id> gainedCompetencyAreaIdList) {

        system.debug('Entering evaluateGainedCompetencyAreaForInvalidMembers: ' +gainedCompetencyAreaIdList);

        List<FP_Gained_Competency_Area__c> potentialRelatedGainedCompetencyArea = [SELECT Id, FT_Pathway__c, FT_Pathway__r.FT_University__c, FT_Pathway_Information__c, (SELECT Id, FT_Subject_Code__c, FT_Subject_Name__c, FT_Student_Paper__r.FT_University_Subject__c FROM Paper_Gained_Competency__r) FROM FP_Gained_Competency_Area__c WHERE Id IN :gainedCompetencyAreaIdList AND FT_Pathway__c != null];        
        
        Set<Id> allUniversityIDSet = new Set<Id>();
        for (FP_Gained_Competency_Area__c gca : potentialRelatedGainedCompetencyArea) {
            allUniversityIDSet.add(gca.FT_Pathway__r.FT_University__c);
        }
        // Build the University Subject Map
        Map<Id, List<FT_University_Subject__c>> uniSubjectMap = wdci_CompetencyUtil.mapUniSubjectMk2(allUniversityIDSet);   // #FIX006
        // Map<Id, List<FT_University_Subject__c>> uniSubjectMap = wdci_CompetencyUtil.mapUniSubject(allUniversityIDSet);
        

        // These Gained Competency Area need to be rechecked if they are still valid, otherwise they will need to be removed
        List<FP_Gained_Competency_Area__c> gcaToBeDeleted = new List<FP_Gained_Competency_Area__c>();
        for (FP_Gained_Competency_Area__c gainedCompetencyArea : potentialRelatedGainedCompetencyArea) {
            String logicStringToEvaluate = gainedCompetencyArea.FT_Pathway_Information__c;
            
            // We need to substitute the Subject Code + Subject Name on the Pathway
            /* Lean - use code below due to matching issue
            for (FT_University_Subject__c uniSubject : uniSubjectMap.get(gainedCompetencyArea.FT_Pathway__r.FT_University__c)) {                        
                if (logicStringToEvaluate.contains(uniSubject.FT_Pathway_Subject_Code__c)) {
                    
                    Boolean studentHaveTakenTheSubject = false;
                    for (FT_Paper_Gained_Competency__c paperGainedCompetency : gainedCompetencyArea.Paper_Gained_Competency__r) {                            
                        if (paperGainedCompetency.FT_Student_Paper__r.FT_University_Subject__c == uniSubject.Id) {
                            studentHaveTakenTheSubject = true;
                        }
                    }
                    logicStringToEvaluate = logicStringToEvaluate.replace(uniSubject.FT_Pathway_Subject_Code__c, String.valueOf(studentHaveTakenTheSubject));                        
                }
            }*/

            for (FT_University_Subject__c uniSubject : uniSubjectMap.get(gainedCompetencyArea.FT_Pathway__r.FT_University__c)) {

                boolean isMatched = wdci_CompetencyUtil.isCodeMatches(logicStringToEvaluate, uniSubject.FT_Pathway_Subject_Code__c);

                if(isMatched){
                    Boolean studentHaveTakenTheSubject = false;
                    for (FT_Paper_Gained_Competency__c paperGainedCompetency : gainedCompetencyArea.Paper_Gained_Competency__r) {                            
                        if (paperGainedCompetency.FT_Student_Paper__r.FT_University_Subject__c == uniSubject.Id) {
                            studentHaveTakenTheSubject = true;
                        }
                    }
                    logicStringToEvaluate = wdci_CompetencyUtil.replacePatternAll(logicStringToEvaluate, uniSubject.FT_Pathway_Subject_Code__c, String.valueOf(studentHaveTakenTheSubject));
                }
            }


            // The evaluate the logic string
            wdci_CompetencyUtil.LogicString ls = new wdci_CompetencyUtil.LogicString();
            Boolean pathwayFulfilled = ls.LogicString(logicStringToEvaluate);
            
            if (!pathwayFulfilled) {
                // This Gained Competency Area is no longer valid as the Student does not have the prequisite Subjects for it
                gcaToBeDeleted.add(gainedCompetencyArea);
            }
        }
        if (gcaToBeDeleted.size() > 0) {
            // Delete the related paper gained compentecy first
            List<FT_Paper_Gained_Competency__c> paperGainedCompetencyToBeDeleted = new List<FT_Paper_Gained_Competency__c>();
            for (FP_Gained_Competency_Area__c gca : gcaToBeDeleted) {
                for (FT_Paper_Gained_Competency__c paperGainedCompetency : gca.Paper_Gained_Competency__r) {
                    paperGainedCompetencyToBeDeleted.add(paperGainedCompetency);
                }
            }
            if (paperGainedCompetencyToBeDeleted.size() > 0) delete paperGainedCompetencyToBeDeleted;
            // Then delete the Gained Competency Area themselves
            delete gcaToBeDeleted;
        }
        system.debug('Exiting evaluateGainedCompetencyAreaForInvalidMembers');
    }
}