public with sharing class ff_SampleService {

    @AuraEnabled
    public static List<Map<String, Object>> search(
            String objectName, String fieldName, String searchTerm, Map<String, Object> searchContext) {
        System.debug(searchTerm);
        System.debug(searchContext);
        List<Map<String, Object>> result = new List<Map<String, Object>>();
        result.add(new Map<String, Object>{
                'id' => 'id1', 'label1' => 'Google', 'label2' => 'Palo Alto', 'position' => 0
        });
        result.add(new Map<String, Object>{
                'id' => 'id2', 'label1' => 'Apple', 'label2' => 'San Jose', 'position' => 1
        });
        return result;
    }

    @AuraEnabled
    public static ff_ReadData load() {
        return getService().handler.load(null);
    }

    @AuraEnabled
    public static Id uploadChunk(Id parentId, String attachmentId, String fileName, String chunk, String contentType) {
        ff_SampleService service = getService();
        Id accountId = getAccountId();

        List<Attachment> attachments = [SELECT Id from Attachment WHERE ParentId = :accountId ORDER BY LastModifiedDate DESC];
        if (attachments.size() > 5) {
            // limit records created so that org doesn't fill up
            return attachments.get(0).Id;
        } else {
            return service.handler.uploadChunk(parentId, attachmentId, fileName, chunk, contentType);
        }
    }

    @AuraEnabled
    public static ff_WriteResult save(String writeData) {
        return getService().handler.save(writeData);
    }

    @AuraEnabled
    public static ff_WriteResult deleteRecord(Id id) {
        return getService().handler.deleteRecord(id);
    }

    private static Id getAccountId() {
        return [SELECT Id FROM Account ORDER BY Name DESC LIMIT 1].Id;
    }

    private static ff_SampleService getService() {
        Campaign c = [SELECT Id FROM Campaign ORDER BY Name DESC LIMIT 1];
        return new ff_SampleService(getAccountId(), c.Id);
    }

    private final ff_ServiceHandler handler;

    private ff_SampleService(Id accountId, Id campaignId) {
        handler = new ff_ServiceHandler(new RecordIdProviderImpl(accountId, campaignId), new LoaderImpl(), new SaverImpl());
    }

    public class RecordIdProviderImpl implements ff_ServiceHandler.RecordIdProvider {

        Id accountId;
        Id campaignId;
        Set<Id> contactIds = new Set<Id>();

        public RecordIdProviderImpl(Id accountId, Id campaignId) {
            this.accountId = accountId;
            this.campaignId = campaignId;
            for (Contact c : [SELECT Id FROM Contact WHERE AccountId = :accountId]) {
                contactIds.add(c.Id);
            }
        }
        public Map<Schema.sObjectType, Set<Id>> getContext() {
            Map<Schema.sObjectType, Set<Id>> result = new Map<Schema.sObjectType, Set<Id>>();
            result.put(Account.getSObjectType(), new Set<Id>{
                    accountId
            });
            result.put(Contact.getSObjectType(), contactIds);
            result.put(Campaign.getSObjectType(), new Set<Id>{
                    campaignId
            });
            return result;
        }
    }

    public class LoaderImpl extends ff_ServiceHandler.LoadHandler {

        public override Map<String, List<ff_Service.SObjectWrapper>> loadRecordsForRead(Map<Schema.sObjectType, Set<Id>> recordIds) {
            List<Account> acc = [
                    SELECT Name, Type
                    FROM Account
                    WHERE Id IN :recordIds.get(Account.getSObjectType())
            ];
            List<ff_Service.SObjectWrapper> accountWrappers = new List<ff_Service.SObjectWrapper>();
            accountWrappers.add(new ff_Service.SObjectWrapper(acc, true));

            // the sample form has 2 contact lists.
            // The first holds the single contact being inserted by the main form fields
            // The second holds the multiple contacts edited by the SampleContactList
            List<ff_Service.SObjectWrapper> contactWrappers = new List<ff_Service.SObjectWrapper>();

            // a list of 0 contacts as a placeHolder for the main form contact
            contactWrappers.add(new ff_Service.SObjectWrapper(new List<SObject>(), true));

            List<Contact> contacts = [
                    SELECT FirstName, LastName, Title, Birthdate, Salutation, Department,
                            Account.Id, Account.Name
                    FROM Contact
                    WHERE AccountId IN :recordIds.get(Account.getSObjectType())
            ];

            // a list of N contacts for a list control
            contactWrappers.add(new ff_Service.SObjectWrapper(contacts, false));

            List<CampaignMember> members = [
                    SELECT Id, Contact.FirstName, Contact.LastName, Campaign.Name
                    FROM CampaignMember
                    WHERE Contact.AccountId IN :recordIds.get(Account.getSObjectType())
            ];
            List<ff_Service.SObjectWrapper> memberWrappers = new List<ff_Service.SObjectWrapper>();
            memberWrappers.add(new ff_Service.SObjectWrapper(members, false));

            return new Map<String, List<ff_Service.SObjectWrapper>>{
                    // note: keys are always SObject type
                    'Account' => accountWrappers,
                    'Contact' => contactWrappers,
                    'CampaignMember' => memberWrappers
            };
        }

        public override Map<String, List<ff_Service.CustomWrapper>> loadWrappersForRead(Map<Schema.sObjectType, Set<Id>> recordIds) {
            Map<String, ff_WrapperSource> sources = new Map<String, ff_WrapperSource>();

            sources.put('Contact.Salutation', new ff_WrappedFieldDescribeSource(Contact.Salutation));
            sources.put('Account.Type', new ff_WrappedFieldDescribeSource(Account.Type));

            Map<String, ff_WrapperSource> countryAndStates = new ff_WrappedCountryStateSources().getSources();
            sources.put('User.Countries', countryAndStates.get('countries'));
            sources.put('User.States', countryAndStates.get('states'));

            // only one account id so extract and use it to create a single wrapper source
            Id accountId = new List<Id>(recordIds.get(Account.getSObjectType())).get(0);
            sources.put('Account.Attachments', new ff_WrappedAttachmentSource(accountId));
            // if there is more than a single attachment parent in the form, repeat the lines above for each

            // include attachments for CampaignMember records as well
            // TODO create a bulkified attachment source that loads attachment wrappers for a set of ids
            for (Contact c : [
                    SELECT Id
                    FROM Contact
                    WHERE AccountId IN :recordIds.get(Account.getSObjectType())
            ]) {
                sources.put('Contact' + c.Id + '.Attachments', new ff_WrappedAttachmentSource(c.Id));
            }

            return ff_ServiceHandler.loadWrappers(sources);
        }
    }

    private static Set<SObjectType> SUPPORTED = new Set<SObjectType>{
            Contact.getSObjectType(),
            CampaignMember.getSObjectType()
    };

    public class SaverImpl extends ff_ServiceHandler.SaveHandler {

        public override List<sObject> processUpserts(RecordIdProvider idProvider, List<SObject> upsertRecords, List<SObject> otherRecords) {

            // "upsert" can be SFDC upsert if there is an external id or just plain update using provided ids.
            // The method of finding records and insert/update/upsert is form specific.

            // "other" records are those that are not upserted in some way.
            if (otherRecords.size() == 1) {
                if (!SUPPORTED.contains(otherRecords.get(0).getSObjectType())) {
                    throw new SObjectException('Unsupported Write request');
                }
            } else {
                throw new SObjectException('Form should only provide a single contact');
            }

            if (otherRecords.get(0).getSObjectType() == Contact.getSObjectType()) {

                Contact c = (Contact) otherRecords.get(0);
                Id accountId = ff_ServiceHandler.getProvidedId(idProvider, Account.getSObjectType(), 0); // don't trust any id from client

                if (c.Account == null) {
                    // no account is present when the insert comes from SampleContactList.
                    c.AccountId = accountId; // don't trust ids from client
                } else {
                    // main form save and Contact list need nested account to be updated
                    // and fk to be set in the Contact ready for insert

                    Account a = c.Account;
                    a.Id = accountId; // don't trust ids from client
                    update a; // upsert not required for this sample form since we always know the account id

                    // change the Contact FK to be the updated Account
                    c.Account = null;
                    c.AccountId = a.Id;
                }

            } else {
                // eager insert from SampleCampaignMemberList control needs a Campaign Id
                CampaignMember cm = (CampaignMember) otherRecords.get(0);
                // need to set the allowed campaign id from the id provider i.e. don't trust client id
                cm.CampaignId = ff_ServiceHandler.getProvidedId(idProvider, Campaign.getSObjectType(), 0);

                // also clear out all other member records to allow this insert to succeed
                for (List<CampaignMember> cms : [select Id from CampaignMember where CampaignId = :cm.CampaignId]) {
                    delete cms;
                }
            }
            return otherRecords;
        }

        public override sObject enhanceResult(RecordIdProvider idProvider, SObject record) {
            System.debug('enhancing: ' + record);
            if (record.getSObjectType() == CampaignMember.getSObjectType()) { // was an eager create
                // same select list as loader above so that eagerly created records display the same way
                return [
                        SELECT Id, Contact.FirstName, Contact.LastName, Campaign.Name
                        FROM CampaignMember
                        WHERE Id = :record.Id
                ];

            } else if (record.getSObjectType() == Contact.getSObjectType()) {
                //////////////////// below is not all used for real world services.
                // This is just to stop the tests from creating lots of records

                Id accountId = ff_ServiceHandler.getProvidedId(idProvider, Account.getSObjectType(), 0);
                // no need to suppress kevin adds since he is deleted by the test
                Contact original = suppressNContacts(accountId, 'Donald', record, 2);

                if (original == null) {

                    // this is the real world enhance code that is typical. In this case it's supporting updates
                    // of Contacts in the ContactList component
                    return [
                            SELECT FirstName, LastName, Title, Birthdate, Salutation, Department,
                                    Account.Id, Account.Name
                            FROM Contact
                            WHERE Id = :record.Id
                    ];
                } else {
                    return original;
                }

                ////////////////////

            } else {
                // don't ignore this, must return default (which is the record with Id only)
                return super.enhanceResult(idProvider, record);
            }
        }

    }

    private static Contact suppressNContacts(Id accountId, String name, SObject c, Integer max) {
        List<Contact> withName = [SELECT Id, FirstName FROM Contact WHERE Id = :c.Id];
        if (withName.size() > 0 && withName.get(0).firstName == name) {
            List<Contact> duplicates = [
                    SELECT Id
                    FROM Contact
                    WHERE AccountId = :accountId AND FirstName = :name
            ];
            if (duplicates.size() >= max) {
                // then we just added a 2nd dupe
                delete c; // remove the added record
                return duplicates.get(0); // return the first record
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}