/**
 * The main class for converting NZTM co-ordinates
 * to lat/longitude
 * 
 * @author Clayton Maxwell
 * @date   15/10/2017
 **/

public class SegmentationNZTM {

	// Define statics
	public static final decimal NZTM_A    = 6378137.0;
	public static final decimal NZTM_RF_GRS80   = 298.257222101; //GRS80 Inverse flattening between equatorial and polar.
	public static final decimal NZTM_RF_WGS84 = 298.257223563; //Inverse flattening.
	public static final decimal NZTM_RF = NZTM_RF_GRS80;
	public static final decimal NZTM_CM   = 173;
	public static final decimal NZTM_OLAT = 0.0; 
	public static final decimal NZTM_SF   = 0.9996; 
	public static final decimal NZTM_FE   = 1600000.0;
	public static final decimal NZTM_FN   = 10000000.0;
				
	// Defining some class variables
	public static decimal meridian = NZTM_CM/(180/Math.PI); // Central meridian
	public static decimal scalef = NZTM_SF;   // Scale factor
	public static decimal orglat = NZTM_OLAT/(180/Math.PI);   // Origin latitude
	public static decimal falsee = NZTM_FE;   // False easting
	public static decimal falsen = NZTM_FN;   // False northing
	public static decimal utomk = 1.0;    // Unit to metre conversion //Note: Might just be utom
	public static decimal utom = 1.0;
	public static decimal a = NZTM_A;
	public static decimal rf = NZTM_RF;
	public static decimal f = 1.0/NZTM_RF;
	public static decimal e2 = 2.0*f - f*f;
	public static decimal ep2 = e2/( 1.0 - e2 );
	public static decimal om;
	

    
	// LatLong Object to hold latitude and longitude
	// SegmentationNZTM.getLatLong(X, Y)
	public static Map<String, decimal> getLatLong(decimal easting, decimal northing) {
			Map<String, decimal> result = new Map<String, decimal>();
			decimal latitude;
	 		decimal longitude;
        
			decimal lt = NZTM_OLAT/(180/Math.PI);
			decimal e4 = e2*e2;
			decimal e6 = e4*e2;

			decimal a0 = 1 - (e2/4.0) - (3.0*e4/64.0) - (5.0*e6/256.0);
			decimal a2 = (3.0/8.0) * (e2+e4/4.0+15.0*e6/128.0);
			decimal a4 = (15.0/256.0) * (e4 + 3.0*e6/4.0);
			decimal a6 = 35.0*e6/3072.0;

			// Init the om
			om = a * (a0 * lt - a2 * Math.sin(2 * lt) + a4 * Math.sin(4 * lt) - a6 * Math.sin(6 * lt));
					
					
			//geod(easting, northing);
			decimal cn1  =  (northing - falsen)*utom/scalef + om;
			decimal n  = f/(2.0-f);
			decimal n2 = n*n;
			decimal n3 = n2*n;
			decimal n4 = n2*n2;
			
			decimal g = a*(1.0-n)*(1.0-n2)*(1+9.0*n2/4.0+225.0*n4/64.0);
			decimal sig = cn1/g;
			
			decimal fphi = sig + (3.0*n/2.0 - 27.0*n3/32.0)*Math.sin(2.0*sig) +
					(21.0*n2/16.0 - 55.0*n4/32.0)*Math.sin(4.0*sig) +
					(151.0*n3/96.0) * Math.sin(6.0*sig) +
					(1097.0*n4/512.0) * Math.sin(8.0*sig);
			
			decimal slt = Math.sin(fphi);
			decimal clt = Math.cos(fphi);
			decimal eslt = (1.0-e2*slt*slt);
			decimal eta = a/Math.sqrt(eslt);
			decimal rho = eta * (1.0 - e2) / eslt;
			decimal psi = eta/rho;
			 
			decimal e = (easting-falsee)*utom;
			decimal x = e/(eta*scalef);
			decimal x2 = x*x;
			decimal t = slt/clt;
			decimal t2 = t*t;
			decimal t4 = t2*t2;  
			
			// Calculate latitude
			decimal trm1 = 1.0/2.0;
			decimal trm2 = ((-4.0*psi + 9.0*(1-t2))*psi + 12.0*t2)/24.0;
			decimal trm3 = ((((8.0*(11.0-24.0*t2)*psi - 
									12.0*(21.0-71.0*t2))*psi + 
									15.0*((15.0*t2-98.0)*t2+15))*psi +
									180.0*((-3.0*t2+5.0)*t2))*psi + 360.0*t4)/720.0;
			decimal trm4 = (((1575.0*t2+4095.0)*t2+3633.0)*t2+1385.0)/40320.0;
			
			decimal latitude_r = fphi+(t*x*e/(scalef*rho))*(((trm4*x2-trm3)*x2+trm2)*x2-trm1);
			latitude = latitude_r * 180.0 / Math.PI;
			
			// Calculate longitude
			trm1 = 1.0;
			trm2 = (psi+2.0*t2)/6.0;
			trm3 = (((-4.0*(1.0-6.0*t2)*psi +
							 (9.0-68.0*t2))*psi +
							 72.0*t2)*psi +
							 24.0*t4)/120.0;
			trm4 = (((720.0*t2+1320.0)*t2+662.0)*t2+61.0)/5040.0;
			
			decimal longitude_r = meridian - (x/clt)*(((trm4*x2-trm3)*x2+trm2)*x2-trm1);
			longitude = longitude_r * 180.0 / Math.PI;
			
        	result.put('Latitude', latitude);
        	result.put('Longitude', longitude);
			return result;
	}
}