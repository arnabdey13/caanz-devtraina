/**
    Developer: WDCi (Lean)
    Development Date: 05/04/2016
    Task #: Enrollment wizard - Controller for Details Masterclass
**/

public without sharing class luana_EnrolmentWizardDetailsMCController extends luana_EnrolmentWizardObject{
	
	luana_EnrolmentWizardController stdController;
    
    public luana_EnrolmentWizardDetailsMCController(luana_EnrolmentWizardController stdController){
        this.stdController = stdController;
		
    }
    
    public luana_EnrolmentWizardDetailsMCController(){
		
    }
    
    public PageReference detailsMCNext(){
		
		if(stdController.workingStudentProgram.Accept_terms_and_conditions__c){
    		PageReference pageRef = stdController.getDetailsNextPage();
    		stdController.skipValidation = true;
    		
    		return pageRef;
    		
    	} else {
    		ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please accept the Terms and Conditions.'));
            
            return null;
    	}
    }
    
    public PageReference detailsMCBack(){
        
        PageReference pageRef;
        stdController.skipValidation = true;
        
        if(stdController.selectedProgramOffering.Require_Subject_Selection__c){
            pageRef = Page.luana_EnrolmentWizardSubject;
        } else {
        	stdController.selectedCourseId = null;
            stdController.selectedCourse = null;
            
            pageRef = Page.luana_EnrolmentWizardCourse;
        }
        
        return pageRef;
    }
    
}