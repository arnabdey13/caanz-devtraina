/**
    Developer: WDCi (Terry)
    Development Date:21/11/2017
    Task: Test class for AAS_LockResultController
    
    This test class will use actual course data for testing.
    
    
**/
@isTest(seeAllData=true)
private class AAS_Test_AAS_LockResultController {
    
    
    
    static testMethod void testAASLockResult() {
        List<LuanaSMS__Course__c> cList = [SELECT Id FROM LuanaSMS__Course__c WHERE RecordType.Name='Accredited Module' LIMIT 1];
        if(!cList.isEmpty()) {
            Test.startTest();
            AAS_Course_Assessment__c ca = new AAS_Course_Assessment__c(AAS_Course__c=cList.get(0).Id, Name='TestLckRes');
            insert ca;
            ApexPages.StandardController sc = new ApexPages.StandardController(ca);    
            AAS_LockResultController cosLR = new AAS_LockResultController(sc);
            cosLR.doValidation();
            cosLR.doCancel();
            cosLR.doCommit();
            ca.AAS_Pre_Final_Adjustment__c=true;
            upsert ca;
            ApexPages.StandardController sc1 = new ApexPages.StandardController(ca);    
            AAS_LockResultController cosLR1 = new AAS_LockResultController(sc1);
            cosLR1.doValidation();
        
            Test.stopTest();        
        }

    }
}