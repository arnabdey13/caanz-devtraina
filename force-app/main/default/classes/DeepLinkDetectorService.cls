public with sharing class DeepLinkDetectorService {

    @AuraEnabled
    public static String load() {
        return load(EditorPageUtils.getUsersAccountId());
    }

    @TestVisible
    private static String load(Id accountId) {
        Account account = [
                SELECT Id, Registration_Destination_URL__c
                FROM Account
                WHERE Id = :accountId
        ];

        String destination = account.Registration_Destination_URL__c;
        if (String.isEmpty(destination)) {
            return null;
        } else {
            account.Registration_Destination_URL__c = null;
            update account;
            return destination;
        }
    }
}