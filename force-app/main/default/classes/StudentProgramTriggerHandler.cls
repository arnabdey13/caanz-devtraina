/***********************************************************************************************************************************************************************
Name: StudentProgramTriggerHandler 
============================================================================================================================== 
Purpose: Trigger Handler for Merging of Multiple Triggers of LuanaSMS__Student_Program__c. Below are the list of Triggers Merged
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR                 DATE          DETAIL     Description 
1.0         Vinay               20/02/2019    Created    Trigger Handler for Merging of Multiple Triggers of LuanaSMS__Student_Program__c. Below are the list of Triggers Merged
                                                            * luana_StudentProgramAction
                                                            * luana_StudentProgramDeleteLog                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/
public with sharing class StudentProgramTriggerHandler extends TriggerHandler{
    //Context Variable Collections after Type Casting
    List<LuanaSMS__Student_Program__c> newList = (List<LuanaSMS__Student_Program__c>)Trigger.new;
    List<LuanaSMS__Student_Program__c> oldList = (List<LuanaSMS__Student_Program__c>)Trigger.old;
    Map<Id,LuanaSMS__Student_Program__c> oldMap = (Map<Id,LuanaSMS__Student_Program__c>)Trigger.oldMap;
    Map<Id,LuanaSMS__Student_Program__c> newMap = (Map<Id,LuanaSMS__Student_Program__c>)Trigger.newMap;

    //Constructor.
    public StudentProgramTriggerHandler() {

    }

    public override void beforeInsert(){
        //Provide a Random Number for every Student Program.
        luana_StudentProgramHandler.setRandomNumber(newList);

        //set duplicate key(combination of student and course) which is used to identify duplicate enrollment.
        luana_StudentProgramHandler.setDuplicateKey(newList);

        //check Duplicate Enrolment using duplicate key and if enforceduplicateenrollment is true(which is default)
        luana_StudentProgramHandler.duplicateEnrolmentCheck(newList);

        //Record Type of Student Program will be set based on the Course they have enrolled for.
        luana_StudentProgramHandler.setRecordType(newList);
        
        //Set default flag for CASM SP
        luana_StudentProgramHandler.setCASMFlag(newList);

        //Module Intial Enrollment value(true/false) will be updated based on the Student Program recordtype, status and earlier Student Programs.
        luana_StudentProgramHandler.setInitialReenrolmentFlag(newList, TRUE);

        //Update Student Program Status from Module Final Status with the Maped value defined in 'Luana_ReferenceTable__mdt'.
        luana_StudentProgramHandler.translateFinalModuleStatusToStatus(newList);
        
        //Update Student Program Module Final Status from Status if it is In Progress.
        luana_StudentProgramHandler.inProgressStatus(newList);
    }
                    
    public override void beforeUpdate(){
        //Module Intial Enrollment value(true/false) will be updated based on the Student Program recordtype, status and earlier Student Programs.
        luana_StudentProgramHandler.setInitialReenrolmentFlag(newList, FALSE);

        //Update Student Program Status from Module Final Status with the Maped value defined in 'Luana_ReferenceTable__mdt'.
        luana_StudentProgramHandler.translateFinalModuleStatusToStatus(newList);

        //Update Student Program Module Final Status from Status if it is In Progress.
        luana_StudentProgramHandler.inProgressStatus(newList);

        //auto sets Final Module Status when Supp_Exam_Paid__c becomes TRUE from previously FALSE value.
        luana_StudentProgramHandler.suppExamPaid(newList, oldMap);
    }       
    
    public override void beforeDelete(){
        //Deleted Student Program Records will be Archived into Luana Log sObject.
        list<LuanaSMS__Luana_Log__c> luanaLogList = new list<LuanaSMS__Luana_Log__c>();
        for(LuanaSMS__Student_Program__c sp : oldList) {
            luanaLogList.add(new LuanaSMS__Luana_Log__c(LuanaSMS__Level__c = 'Info', LuanaSMS__Log_Message__c = JSON.serializePretty(sp), LuanaSMS__Reference__c = 'LuanaSMS__Student_Program__c', LuanaSMS__Status__c = 'Completed', LuanaSMS__Type__c = 'Record Deletion'));
        }
        if(luanaLogList != null && !luanaLogList.isEmpty()){
            insert luanaLogList;
        }
    }
            
    public override void afterInsert(){
        //auto assignment of attandence for sessions related to courses of student program.
        luana_StudentProgramHandler.spAutoAssignment(newList, oldMap);
    }

    public override void afterUpdate(){
        //update Student Unit Of Study status based on the Student Program status
        luana_StudentProgramHandler.updateStuUnitOfStudy(newList);

        //update Student Program Completion Date with the End Date of Course associated with Student Program.
        luana_StudentProgramHandler.updateAPCourseEndDate(newList);
        
        //will reiniate the existing earliest enrolment as initial = true if the current one is cancelled and changed from initial = true to false
        luana_StudentProgramHandler.reassignIntialEnrolmentFlag(newList, oldList);
        
        //grant elearn hours if Grant_eLearn_CPD_Hours__c is True or changed to true.
        luana_StudentProgramHandler.grantELearnCPDHours(newList, oldList);

        //grant elearn hours if Grant_Workshop_CPD_Hours__c is True or changed to true.
        luana_StudentProgramHandler.grantWorkshopCPDHours(newList, oldList);

        //auto assignment of attandence for sessions related to courses of student program.
        luana_StudentProgramHandler.spAutoAssignment(newList, oldMap);
    }
    
    public override void afterDelete(){
        
    }
    
    public override void afterUndelete(){
        //check Duplicate Enrolment using duplicate key and if enforceduplicateenrollment is true(which is default)
        luana_StudentProgramHandler.duplicateEnrolmentCheck(newList);
    }
}