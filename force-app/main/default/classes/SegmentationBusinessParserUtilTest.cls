@isTest
public class SegmentationBusinessParserUtilTest {
  
 
     @testSetup 
    static void setup() {
        
        Reference_Main_Practices__c  deloitte = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Deloitte'
            , Search_Name__c     = 'Deloitte'
            , Full_Name__c       = 'Deloitte'
            , Practice_Size__c   = 'Large Firm'
        );
      
		Reference_Main_Practices__c  pwc = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'PricewaterhouseCoopers;PWC; PwC; PriceWaterHouseCoopers; PriceWaterhouseCoopers; PricewaterhousCoopers; Pricewaterhousecoopers'
            , Search_Name__c     = 'PricewaterhouseCoopers'
            , Full_Name__c       = 'PricewaterhouseCoopers'
            , Practice_Size__c   = 'Large Firm'
        );
        
		Reference_Main_Practices__c  kpmg = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'KPMG'
            , Search_Name__c     = 'KPMG'
            , Full_Name__c       = 'KPMG'
            , Practice_Size__c   = 'Large Firm'
        );
        
		Reference_Main_Practices__c  ey = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Ernst and Young; EY; Ernst & Young'
            , Search_Name__c     = 'Ernst and Young'
            , Full_Name__c       = 'Ernst and Young'
            , Practice_Size__c   = 'Large Firm'
        );
        
		Reference_Main_Practices__c  bdo = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'BDO'
            , Search_Name__c     = 'BDO'
            , Full_Name__c       = 'BDO'
            , Practice_Size__c   = 'Large Firm'
        );
        
		Reference_Main_Practices__c  vincents = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Vincents Chartered Accountants; Vincents'
            , Search_Name__c     = 'Vincents Chartered Accountants'
            , Full_Name__c       = 'Vincents Chartered Accountants'
            , Practice_Size__c   = 'Large Firm'
        );
        
        insert new List<Reference_Main_Practices__c>{pwc, deloitte, kpmg, ey, bdo, vincents};
            
    }
    
            
    // Method to test the Pattern Matching
    static testMethod void validatePatternMatchingBig4EY() {
        	SegmentationBusinessParserUtil parser = new SegmentationBusinessParserUtil();
			System.debug('What Practice ' + parser.whatMainPractice('Ernst & Young'));

        	System.debug('What Practice ' + parser.whatMainPractice('Ernst and Young'));
        	System.debug('What Practice ' + parser.whatMainPractice('Ernst & Young'));
            System.assertEquals('Ernst and Young', parser.whatMainPractice('Ernst and Young')) ;
        	System.assertEquals('Ernst and Young', parser.whatMainPractice('Ernst & Young Denmark')) ;
        	System.assertEquals('Ernst and Young', parser.whatMainPractice('Cap Gemini Ernst and Young Australia Pty Ltd')) ;
            System.assertEquals('Ernst and Young', parser.whatMainPractice('EY')) ;
            System.assertEquals('Ernst and Young', parser.whatMainPractice('EY Sweeney'));
        	System.assertEquals('Ernst and Young', parser.whatMainPractice('EY (NSW)'));
        	// False Matches for EY
        	System.assertNotEquals('Ernst and Young', parser.whatMainPractice('Preston Eye Clinic'));
            System.assertNotEquals('Ernst and Young', parser.whatMainPractice('Market EYE'));
           	System.assertNotEquals('Ernst and Young', parser.whatMainPractice('Preston EYE Clinic'));
            System.assertNotEquals('Ernst and Young', parser.whatMainPractice('ABBEY'));
        	System.assertNotEquals('Ernst and Young', parser.whatMainPractice('BROOKVALE M C Kimmorley and Company Pty Ltd'));
        	System.assertNotEquals('Ernst and Young', parser.whatMainPractice('Peter Bailey & Associates Pty Ltd'));
        	System.assertNotEquals('Ernst and Young', parser.whatMainPractice('SYDNEY Tax & Accounting Partners'));
        	System.assertNotEquals('Ernst and Young', parser.whatMainPractice('Robert A. Casey & Co'));
        
   } 
    
    static testMethod void validatePatternMatchingBig4PWC() { 
        	SegmentationBusinessParserUtil parser = new SegmentationBusinessParserUtil();
        	System.debug('What Practice ' + parser.whatMainPractice('PWC'));
        	System.assertEquals('PricewaterhouseCoopers', parser.whatMainPractice('PricewaterhouseCoopers')) ;
            System.assertEquals('PricewaterhouseCoopers', parser.whatMainPractice('PriceWaterhouseCoopers')) ;
        	System.assertEquals('PricewaterhouseCoopers', parser.whatMainPractice('PriceWaterHouseCoopers')) ;
            System.assertEquals('PricewaterhouseCoopers', parser.whatMainPractice('PwC')) ;
        	System.assertEquals('PricewaterhouseCoopers', parser.whatMainPractice('PWC')) ;
        	System.assertEquals('PricewaterhouseCoopers', parser.whatMainPractice('PwC UK')) ;
        	System.assertEquals('PricewaterhouseCoopers', parser.whatMainPractice('PwC Consulting')) ;

    } 
    
    static testMethod void validatePatternMatchingBig4Deloitte() {
        	SegmentationBusinessParserUtil parser = new SegmentationBusinessParserUtil();
        	System.debug('What Practice ' + parser.whatMainPractice('Deloitte'));
            System.assertEquals('Deloitte', parser.whatMainPractice('Deloitte'));
            System.assertEquals('Deloitte', parser.whatMainPractice('Deloitte (NSW)')) ;
        	System.assertEquals('Deloitte', parser.whatMainPractice('Deloitte Touche Tohmatsu'));
        	System.assertEquals('Deloitte', parser.whatMainPractice('Deloitte and Touche'));
        	System.assertEquals('Deloitte', parser.whatMainPractice('Touche - Deloitte'));
 	} 
   
    
    static testMethod void validatePatternMatchingBig4KPMG() {    
        	
        	SegmentationBusinessParserUtil parser = new SegmentationBusinessParserUtil();
        	System.debug('What Practice ' + parser.whatMainPractice('KPMG'));
            System.assertEquals('KPMG', parser.whatMainPractice('KPMG')) ;
        	System.assertEquals('KPMG', parser.whatMainPractice('KPMG Auckland')) ;
        	System.assertEquals('KPMG', parser.whatMainPractice('KPMG - Melbourne')) ;
        	System.assertEquals('KPMG', parser.whatMainPractice('KPMG-Melbourne')) ;
        	System.assertEquals('KPMG', parser.whatMainPractice('(XYZ) KPMG-Melbourne')) ;
    }
    
    
     // Method to test the Pattern Matching
    static testMethod void validatePatternMatchingLargeBDO() {
        	SegmentationBusinessParserUtil parser = new SegmentationBusinessParserUtil();
        	System.debug('What Practice ' + parser.whatMainPractice('BDO'));
            System.assertEquals('BDO', parser.whatMainPractice('BDO')) ;
            System.assertEquals('BDO', parser.whatMainPractice('BDO Auckland')) ;
           	System.assertEquals('BDO', parser.whatMainPractice('(QR)BDO')) ;
        	System.assertEquals('BDO', parser.whatMainPractice('(QR)BDO Northland')) ;
        	System.assertNotEquals('BDO', parser.whatMainPractice('')) ;
 
    }
    
     // Method to test the Pattern Matching
    static testMethod void validatePatternMatchingOther() {
        	SegmentationBusinessParserUtil parser = new SegmentationBusinessParserUtil();
        	System.debug('What Practice ' + parser.whatMainPractice('Vincents Chartered Accountants'));
            System.assertEquals('Vincents Chartered Accountants', parser.whatMainPractice('Vincents Chartered Accountants')) ;
        	System.assertEquals('Vincents Chartered Accountants', parser.whatMainPractice('Vincents')) ;
    }
    
    
    static testMethod void validateMainPractice() {
		SegmentationBusinessParserUtil parser = new SegmentationBusinessParserUtil();
        Reference_Main_Practices__c main = parser.mainPracticeRef('Ernst & Young');
        System.debug('Main Practice');
        System.debug (main);
		System.assertEquals('Ernst and Young', main.Search_Name__c) ;
    }
   
    
    static testMethod void stripNoise() {
		System.assertEquals('ERNST AND YOUNG', SegmentationBusinessParserUtil.stripBusNoise('Ernst And Young Prop Ltd')) ;
    }
    
    
    
  
/**    
    Baumgartner Super
BENTLEYS Bentleys

BM Accounting BM Accounting Ltd
                BM and Y Accountants Pty Ltd
                BM and Y MANDURAH
                BM Financial Group


                Boyce
                Boyce  A N
Boyce Chartered Accountants    Boyce Chartered Accountants
Boyce Chartered Accountants    Boyce Chartered Accountants
                BOYCE Family Office



Crowe Horwath                Crowe Horwath YARRA JUNCTION
                Crowe Howarth


                DKM
DKM Group        DKM Group

                Duns
                Duns  M H
Duns Ltd              Duns Ltd

Ernst and Young               Ernst & Young Wellington
                Ernst &Young Tokyo
                Ernst Young

HLB Mann Judd HLB Mann Judd Wealth Management (NSW) Pty Ltd
                HLBMJ (SE QLD) Financial Planning Pty Ltd


                ICL Accountants Pty Ltd
ICL Chartered Accountants          ICL Chartered Accountants


BM Accounting JBM Accounting Pty Ltd


Hall Chadwick    Maxim Hall Chadwick Accountants Pty Ltd

McCulloch and Partners                McCulloch and Partners
                Mcculloch and Partners


Bentleys              McLean Delmo Bentleys


                Mitchell and Co
Mitchell and Partners     Mitchell and Partners
                Mitchell Group (NSW) Pty Ltd
                Mitchell McCleary
                Mitchell Partners
                Mitchell Partners Consulting
                Mitchell Partners Consulting


MSI Global Alliance         MSI Ragg Weir
MSI Global Alliance         MSI Taylor
MSI Global Alliance         MSI Taylor Wealth Management Pty Ltd


PricewaterhouseCoopers            PricewaterhousCoopers
                Pricewaterhouse Cooper New Zealand
                PricewaterhouseCoopers Ltda.


Allan Hall              Walter Allan Hall Pty Ltd

                Worrells Forensic Accountants Pty Ltd
Worrells Solvency            Worrells Solvency and Forensic Accountants

YCG Accountants             YCG Accountants, Auditors & Advisors
                YCG Audit Services Pty Ltd

**/
    
/**    
    static testMethod void testParserRecognitionAll() {
     	SegmentationBusinessParserUtil parser = new SegmentationBusinessParserUtil();
        
		List <Reference_CAANZ_Accounting_Companies__c> prctToUpdate = new List <Reference_CAANZ_Accounting_Companies__c> ();
        List <Reference_CAANZ_Accounting_Companies__c> prcts =  [Select ID, Company_Name__c, Company_Type__c, Recognised_Company__c From Reference_CAANZ_Accounting_Companies__c ];
        
        // get all practices       
        for(Reference_CAANZ_Accounting_Companies__c practice : prcts ) {
        	Reference_CAANZ_Accounting_Companies__c recogPractice = new Reference_CAANZ_Accounting_Companies__c();
            recogPractice.ID = practice.ID;
            recogPractice.Recognised_Company__c = parser.whatMainPractice(practice.Company_Name__c);
            if (recogPractice.Recognised_Company__c != NULL)
                prctToUpdate.add(recogPractice);
        }
    	update prctToUpdate;
    }
**/
    
}