/**
    Developer: WDCi (kh)
    Development Date:13/12/2016
    Task: Test class for AAS_FinalAdjustmentController
**/
@isTest(seeAllData=false)
private class AAS_Test_FinalAdjustmentController {

    

    final static String contextLongName = 'AAS_Test_FinalAdjustmentController';
    final static String contextShortName = 'adac';
    
    public static AAS_Course_Assessment__c courseAss;
    public static AAS_Student_Assessment__c sa;
    public static AAS_Student_Assessment_Section__c sas;
    
    public static List<AAS_Student_Assessment_Section_Item__c> sasiList;
    
    public static void initial(){
        
        Luana_DataPrep_Test dataPrepUtil = new Luana_DataPrep_Test();
        AAS_DataPrep_Test aasdataPrepUtil = new AAS_DataPrep_Test();
        
        insert aasdataPrepUtil.defauleCustomSetting();
        
        Map<String, Id> recordTypeMap = aasdataPrepUtil.getRecordTypeMap('Exam');
        
        
        //Create all the luana custom setting
        insert dataPrepUtil.prepLuanaExtensionSettingCustomSettings();
        insert dataPrepUtil.createLuanaConfigurationCustomSetting();
        
        //Course
        Map<String, SObject> basicAssetMap = aasdataPrepUtil.createBasicAssets(contextLongName, contextShortName, dataPrepUtil, aasdataPrepUtil);
        LuanaSMS__Program_Offering__c po = new LuanaSMS__Program_Offering__c();
        po = (LuanaSMS__Program_Offering__c)basicAssetMap.get('PROGRAMOFFERRING1');

        LuanaSMS__Course__c courseAM = aasdataPrepUtil.createLuanaCourse(po.Id, dataPrepUtil);
        courseAM.LuanaSMS__Allow_Online_Enrolment__c = true;
        insert courseAM;
        
        //CA
        courseAss = aasdataPrepUtil.createCourseAssessment(courseAM.Id, 'CA_' + contextShortName);
        courseAss.AAS_Module_Passing_Mark__c = 40;
        courseAss.AAS_Non_Exam_Data_Loaded__c = true;
        courseAss.AAS_Exam_Data_Loaded__c = true;
        courseAss.AAS_Cohort_Adjustment_Exam__c = true;
        insert courseAss;
        
        //ASS
        AAS_Assessment_Schema_Section__c ass = aasdataPrepUtil.createAsessmentSchemaSection(courseAss.Id, 'ASS_' + contextShortName, recordTypeMap.get('AAS_Assessment_Schema_Section__c:Exam_Assessment'));
        ass.AAS_Passing_Mark__c = 40;
        ass.AAS_Cohort_Adjustment__c = 2;
        insert ass;
        
        List<AAS_Assessment_Schema_Section_Item__c> assiList = new List<AAS_Assessment_Schema_Section_Item__c>();
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA111 Q #1'));
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA111 Q #2'));
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA111 Q #3'));
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA111 Q #4'));
        insert assiList;
        
        //SA
        sa = aasdataPrepUtil.createStudentAssessment(courseAss.Id);
        insert sa;
        

        
        //SAS
        sas = aasdataPrepUtil.createStudentAssessmentSection(sa.Id, recordTypeMap.get('AAS_Student_Assessment_Section__c:Exam_Assessment_SAS'));
        sas.AAS_Assessment_Schema_Section__c = ass.Id;
        sas.AAS_Adjustment__c = 1;
        insert sas;
        
        
        
        //SASI
        sasiList = new List<AAS_Student_Assessment_Section_Item__c>();
        
        for(AAS_Assessment_Schema_Section_Item__c assi: assiList){
            AAS_Student_Assessment_Section_Item__c sasi = aasdataPrepUtil.createStudentAssessmentSectionItem(sas.Id, recordTypeMap.get('AAS_Student_Assessment_Section_Item__c:Exam_Assessment_SASI'), assi.Id);
            sasi.AAS_Raw_Mark__c = 9;
            sasi.AAS_Adjustment__c = 0.75;
            //sasi.AAS_Special_Consideration_Adjustment__c = 0;//hk 9april17
            sasi.AAS_Full_Mark__c = 20;
            sasiList.add(sasi);
        }
        insert sasiList;
        
        List<AAS_Student_Assessment_Section_Item__c> sasiResult = [Select Id, AAS_Rounded_Part__c, AAS_Eligible_for_Addition__c, AAS_Eligible_for_Deduction__c, AAS_Final_Mark__c, AAS_Calculated_Mark__c, AAS_Raw_Mark__c, AAS_Full_Mark__c, AAS_Weight__c, AAS_Adjustment__c, AAS_Student_Assessment_Section__c from AAS_Student_Assessment_Section_Item__c Where AAS_Student_Assessment_Section__c =: sas.Id];
        System.debug('***sasi: ' + sasiResult);
        
        AAS_Student_Assessment_Section__c sasResult = [Select Id, AAS_Rolled_Up_Sum__c, AAS_Rounded_Sum__c, AAS_Adjustment__c from AAS_Student_Assessment_Section__c where id =:  sas.Id];
        System.debug('***sas: ' + sasResult);
        
        AAS_Student_Assessment__c saResult = [Select Id, AAS_Total_Module_Mark_Non_Exam_Exam__c from AAS_Student_Assessment__c where id =:  sa.Id];
        System.debug('***sa: ' + saResult);
    }
    
    public static testMethod void test_NoBorderline() { 
    
        initial();
        
        Test.startTest();
            try{
                PageReference pageRef1 = Page.AAS_ModuleAdjustment;
                pageRef1.getParameters().put('id', String.valueOf(courseAss.Id));
                pageRef1.getParameters().put('type', String.valueOf('Exam_Assessment'));
                Test.setCurrentPage(pageRef1);
                
                ApexPages.StandardController sc1 = new ApexPages.StandardController(courseAss);
                AAS_ModuleAdjustmentController finalAdj = new AAS_ModuleAdjustmentController(sc1);
            }Catch(Exception exp){
                Boolean expectedExceptionThrown =  exp.getMessage().contains('You are not allow to continue in this step. Please complete the Cohort Adjustment & Borderline remark processes first.') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true, exp.getMessage());
            }
        Test.stopTest();
    }       
    
    public static testMethod void test_NoRecordFoundMain() { 
    
        initial();
        
        //sa.AAS_Final_Adjustment__c = 2;
        
        Test.startTest();
            try{
                PageReference pageRef1 = Page.AAS_ModuleAdjustment;
                pageRef1.getParameters().put('id', String.valueOf(courseAss.Id));
                pageRef1.getParameters().put('type', String.valueOf('Exam_Assessment'));
                Test.setCurrentPage(pageRef1);
                
                ApexPages.StandardController sc1 = new ApexPages.StandardController(courseAss);
                AAS_ExamAdjustmentController finalAdj = new AAS_ExamAdjustmentController(sc1);
                
                finalAdj.getResults();
                finalAdj.doComplete();
                finalAdj.getRecordTypeOptions();
                finalAdj.doCancel();
                
            }Catch(Exception exp){
                Boolean expectedExceptionThrown =  exp.getMessage().contains('No record found. Please press \'Done\' to indicate that Final Adjustment is completed and exit.') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true, exp.getMessage());
            }
        Test.stopTest();
    } 
    
    public static testMethod void test_RecordFound_Main() { 
    
        initial();
        
        Test.startTest();
        
            PageReference pageRef1 = Page.AAS_ModuleAdjustment;
            pageRef1.getParameters().put('id', String.valueOf(courseAss.Id));
            pageRef1.getParameters().put('type', String.valueOf('Exam_Assessment'));
            Test.setCurrentPage(pageRef1);
            
            ApexPages.StandardController sc1 = new ApexPages.StandardController(courseAss);
            AAS_ModuleAdjustmentController finalAdj = new AAS_ModuleAdjustmentController(sc1);
            
            finalAdj.getResults();
            finalAdj.doComplete();
            finalAdj.getRecordTypeOptions();
            finalAdj.doCancel();
            finalAdj.doCommit();
            
            AAS_ExamAdjustmentController examAdj = new AAS_ExamAdjustmentController(sc1);
            examAdj.getResults();
            examAdj.doComplete();
            examAdj.getRecordTypeOptions();
            examAdj.doCancel();
            examAdj.doCommit();
            
            List<AAS_Course_Assessment__c> caResultCheck = [Select Id, AAS_Final_Adjustment_Exam__c from AAS_Course_Assessment__c Where Id =: courseAss.Id];
            System.assertEquals(caResultCheck[0].AAS_Final_Adjustment_Exam__c, true, 'Final Adjustment Exam is checked'); 
          
            
        Test.stopTest();
    }     
      
    
    public static testMethod void test_FinalSupExamMain() { 
    
        initial();
        
        Test.startTest();
            
            PageReference pageRef1 = Page.AAS_ModuleAdjustment;
            pageRef1.getParameters().put('id', String.valueOf(courseAss.Id));
            pageRef1.getParameters().put('type', String.valueOf('Supp_Exam_Assessment'));
            Test.setCurrentPage(pageRef1);
            
            ApexPages.StandardController sc1 = new ApexPages.StandardController(courseAss);
            AAS_ModuleAdjustmentController finalAdj = new AAS_ModuleAdjustmentController(sc1);
            
            finalAdj.getResults();
            finalAdj.doComplete();
            finalAdj.getRecordTypeOptions();
            finalAdj.doCancel();
            
        Test.stopTest();
    }                 
          
    public static testMethod void test_ModuleAdjustmentExamMain() { 
    
        initial();
        
        Test.startTest();
            
            PageReference pageRef1 = Page.AAS_ModuleAdjustment;
            pageRef1.getParameters().put('id', String.valueOf(courseAss.Id));
            pageRef1.getParameters().put('type', String.valueOf('Exam_Assessment'));
            Test.setCurrentPage(pageRef1);
            
            ApexPages.StandardController sc1 = new ApexPages.StandardController(courseAss);
            AAS_ModuleAdjustmentController finalAdj = new AAS_ModuleAdjustmentController(sc1);
            
            finalAdj.getResults();
            finalAdj.doComplete();
            finalAdj.getRecordTypeOptions();
            finalAdj.doCancel();
            
        Test.stopTest();
    }  
    
    
    
    public static testMethod void test_ExamAdjustmentExamMain() { 
    
        initial();
        
        // LKoh :: 16-05-2017
        // Note :: Added to fix the coverage issue
        courseAss.RecordTypeId = Schema.SObjectType.AAS_Course_Assessment__c.getRecordTypeInfosByName().get('Capstone Module').getRecordTypeId();
        update courseAss;
        
        Test.startTest();
            
            PageReference pageRef1 = Page.AAS_ModuleAdjustment;
            pageRef1.getParameters().put('id', String.valueOf(courseAss.Id));
            pageRef1.getParameters().put('type', String.valueOf('Exam_Assessment'));
            Test.setCurrentPage(pageRef1);
            
            ApexPages.StandardController sc1 = new ApexPages.StandardController(courseAss);
            AAS_ExamAdjustmentController finalAdj = new AAS_ExamAdjustmentController(sc1);
            
            finalAdj.getResults();
            finalAdj.doComplete();
            finalAdj.getRecordTypeOptions();
            finalAdj.doCancel();
            finalAdj.doCommit();
            
        Test.stopTest();
    } 

    // LKoh :: 16-05-2017
    // Note :: Added to fix the coverage issue
    public static testMethod void test_ExamAdjustmentExamMain2() { 
    
        initial();
        
        courseAss.RecordTypeId = Schema.SObjectType.AAS_Course_Assessment__c.getRecordTypeInfosByName().get('Technical Module').getRecordTypeId();
        update courseAss;
        
        Test.startTest();
            
            PageReference pageRef1 = Page.AAS_ModuleAdjustment;
            pageRef1.getParameters().put('id', String.valueOf(courseAss.Id));
            pageRef1.getParameters().put('type', String.valueOf('Exam_Assessment'));
            Test.setCurrentPage(pageRef1);
            
            ApexPages.StandardController sc1 = new ApexPages.StandardController(courseAss);
            AAS_ExamAdjustmentController finalAdj = new AAS_ExamAdjustmentController(sc1);
            
            finalAdj.getResults();
            finalAdj.doComplete();
            finalAdj.getRecordTypeOptions();
            finalAdj.doCancel();
            finalAdj.doCommit();
            
        Test.stopTest();
    }
    
    //KHLau :: 16-05-2017
    public static testMethod void test_AAS_FinalAdjustmentController(){
        
        initial();
        
        courseAss.RecordTypeId = Schema.SObjectType.AAS_Course_Assessment__c.getRecordTypeInfosByName().get('Technical Module').getRecordTypeId();
        update courseAss;
        
        Test.startTest();
            ApexPages.StandardController sc1 = new ApexPages.StandardController(courseAss);
            AAS_FinalAdjustmentController finalAdj = new AAS_FinalAdjustmentController(sc1);
        Test.stopTest();
    }
}