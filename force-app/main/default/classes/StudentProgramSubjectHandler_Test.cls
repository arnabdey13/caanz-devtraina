@isTest
public with sharing class StudentProgramSubjectHandler_Test {
      public static Luana_DataPrep_Test dataPrep;
    private static String classNamePrefixLong = 'StudentProgramSubjectTriggerHandler_Test';
    private static String classNamePrefixShort = 'spsth';

    static testMethod void testStudentProgramSubjectTrigger() {
        list<Profile> profileList = [Select Id from Profile where name = 'CAANZ Service Desk'];
        list<PermissionSet> luanaAdminPermissionList = [Select Id from PermissionSet where name = 'Luana_AdminPermission'];
        System.debug('What is the profile id ' + profileList);
        list<UserRole> portalRoleList = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        User u;
        if(profileList != null && !profileList.isEmpty()){
            u = new User( ProfileId = profileList[0].Id,
                            Username = 'testtermsconditions1234423@charteredaccountantsanz.com',
                            Alias = 'batman',
                            Email='testtermsconditions1234423@charteredaccountantsanz.com',
                            EmailEncodingKey='UTF-8',
                            Firstname='Bruce',
                            Lastname='Wayne',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_NZ',
                            TimeZoneSidKey='Pacific/Auckland');
            if(portalRoleList != null && !portalRoleList.isEmpty()){
                u.UserRoleId = portalRoleList[0].Id;
            }            
            insert u;
        }        
        PermissionSetAssignment permissonSetAssign;
        if(luanaAdminPermissionList != null && !luanaAdminPermissionList.isEmpty()){
            permissonSetAssign = new PermissionSetAssignment(PermissionSetId = luanaAdminPermissionList[0].Id, AssigneeId = u.Id);
            insert permissonSetAssign;
        }
        System.runas(u){
            //initialize
            dataPrep = new Luana_DataPrep_Test();
            
            //Create all the custom setting
            insert dataPrep.prepLuanaExtensionSettingCustomSettings();
            insert dataPrep.createLuanaConfigurationCustomSetting();
            
            //Create traning org
            LuanaSMS__Training_Organisation__c trainingOrg = dataPrep.createNewTraningOrg(classNamePrefixLong, classNamePrefixLong, classNamePrefixShort, 'St 123', 'Wollonggong', '2244');
            insert trainingOrg;
            
            //Create Product
            List<Product2> prodList = new List<Product2>();
            prodList.add(dataPrep.createNewProduct('FIN_AU_' + classNamePrefixLong, 'AU0001'));
            prodList.add(dataPrep.createNewProduct('FIN_NZ_' + classNamePrefixLong, 'NZ0001'));
            prodList.add(dataPrep.createNewProduct('FIN_INT_' + classNamePrefixLong, 'INT0001'));
            insert prodList;
            
            //Create program
            LuanaSMS__Program__c newProgrm = dataPrep.createNewProgram(classNamePrefixShort, classNamePrefixLong, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
            insert newProgrm;

            //Create Program Offerings
            List<LuanaSMS__Program_Offering__c> progList = new List<LuanaSMS__Program_Offering__c>();
            LuanaSMS__Program_Offering__c poAM1 = dataPrep.createNewProgOffering('PO_AM_1' + classNamePrefixLong, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), newProgrm.Id, trainingOrg.Id, prodList[0].Id,prodList[1].Id, prodList[2].Id, 1, 1);
            poAM1.IsCapstone__c = true;
            poAM1.Product_Type__c = 'CA Program';
            progList.add(poAM1);            
            insert progList;
            
            //Create courses
            List<LuanaSMS__Course__c> courseList = new List<LuanaSMS__Course__c>();
            LuanaSMS__Course__c c = dataPrep.createNewCourse('Graduate Diploma of Chartered Accounting', progList[0].Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
            courseList.add(c);
            LuanaSMS__Course__c c1 = dataPrep.createNewCourse('Graduate Diploma of Chartered Accounting 1', progList[0].Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('PPP'), 'Running');
            courseList.add(c1);
            LuanaSMS__Course__c c2 = dataPrep.createNewCourse('Graduate Diploma of Chartered Accounting 1', progList[0].Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Program'), 'Running');
            courseList.add(c2);
            LuanaSMS__Course__c c3 = dataPrep.createNewCourse('Graduate Diploma of Chartered Accounting 1', progList[0].Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Foundation'), 'Running');
            courseList.add(c3);
            insert courseList;
            
            List<Account> memberAccounts = new List<Account>();
            for(integer i = 0; i < 3; i ++){
                Account mAccount = dataPrep.generateNewApplicantAcc('Zac_' + classNamePrefixShort + '_' + i, classNamePrefixLong, 'Full_Member');
                //May need to add email to generateNewApplicantAcc
                mAccount.PersonEmail = classNamePrefixLong + i + '@example.com';
                mAccount.Member_Id__c = '1234' + i;
                mAccount.Affiliated_Branch_Country__c = 'Australia';
                mAccount.Membership_Class__c = 'Full';
                mAccount.Communication_Preference__c= 'Home Phone';
                mAccount.PersonHomePhone= '1234';
                mAccount.PersonOtherStreet= '83 Saggers Road';
                mAccount.PersonOtherCity='JITARNING';
                mAccount.PersonOtherState='Western Australia';
                mAccount.PersonOtherCountry='Australia';
                mAccount.PersonOtherPostalCode='6365'; 
                
                memberAccounts.add(mAccount);
            }           
            
            insert memberAccounts;               
            
            List<Contact> contactList = [SELECT Id, AccountId FROM Contact WHERE AccountID IN : memberAccounts];
            
            List<LuanaSMS__Student_Program__c> studentProgramsList = new List<LuanaSMS__Student_Program__c>();
            for(integer i = 0; i < 3; i ++){
                for(LuanaSMS__Course__c courseRec : courseList){
                    LuanaSMS__Student_Program__c sp = dataPrep.createNewStudentProgram(dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), 
                                                        contactList[i].id, courseRec.Id, 'Australia', 'Completed');      
                    
                    studentProgramsList.add(sp);
                }
            }            
            system.debug('***size of Student program list-->'+studentProgramsList.size());        
            insert studentProgramsList;
        
            Map<String, SObject> assetMap = FP_Test_UTIL.assetGenerator('SPSCA');
            
            FP_Relevant_Competency_Area__c rca1 = FP_Test_UTIL.createRCA(assetMap.get('ca1').Id, 'Gains', assetMap.get('subject1').Id);
            insert rca1;
            
            // Custom Metadata that contains the acceptable SPS Outcome
            List<FP_PositiveOutcome__mdt> checkMD = [SELECT Id, MasterLabel, Positive_Outcome__c FROM FP_PositiveOutcome__mdt WHERE Positive_Outcome__c = TRUE];
            system.assert(checkMD != null && checkMD.size() > 0, 'Could not find Positive Outcome Custom MetaData with matching MasterLabel');
                            
            test.startTest();
            
            // Create SPS that fulfill the SPSCompetencyAreaHandler requirement
            LuanaSMS__Student_Program_Subject__c sps1 = FP_Test_UTIL.createSPS(assetMap.get('subject1').Id, assetMap.get('cont1').Id, checkMD[0].MasterLabel);
            sps1.LuanaSMS__Student_Program__c = studentProgramsList[0].Id;
            insert sps1;

            update sps1;

            delete sps1;

            undelete sps1;

            list<PermissionSet> muteProcessesPermissionList = [Select Id from PermissionSet where name = 'Mute_Triggers_Processes'];
            PermissionSetAssignment mutePermissonSetAssign;
            if(muteProcessesPermissionList != null && !muteProcessesPermissionList.isEmpty())
                mutePermissonSetAssign = new PermissionSetAssignment(PermissionSetId = muteProcessesPermissionList[0].Id, AssigneeId = u.Id);
            
            if(mutePermissonSetAssign != null)
                insert mutePermissonSetAssign;

            update sps1;
            
            test.stopTest();
        }
  }
}