public with sharing class QPRFormAssemblyRedirect {
    						  
 
        // Generate Form Assembly URL
        public PageReference generateFormURL() {
          
             PageReference pref = null;
            String redirectURL ='';
            String encodedURLParam='';
            String texttoEncode='';
            String encodedURL ='';
            String encodedURLText ='';
           String urlAction =ApexPages.currentPage().getParameters().get('formaction');
           try{
               List<QPR_Config__mdt> kevalues = [select QPR_Key_Value__c from QPR_Config__mdt where QPR_Key__c='FormAssemblyKey']; 
                for( QPR_Config__mdt qprConfigobj : kevalues)
                {
                    String keyValue=qprConfigobj.QPR_Key_Value__c;
            if(urlAction =='au_savenresume'||urlAction =='nz_savenresume')
            {
              //  QPR_Config__c__mdt obj= QPR_Config__c.getValues('FormAssemblyKey'); 
              //  String keyValue=obj.QPR_Key_Value__c; 
          
                
                    Blob key = Blob.valueOf(keyValue);
               	 texttoEncode =urlAction;
                 encodedURLParam = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES128', key, Blob.valueOf(texttoEncode)));
                  encodedURLText=EncodingUtil.URLEncode(encodedURLParam,'UTF-8');

			 redirectURL = Url.getSalesforceBaseURL().toExternalForm()+ '/MyCA/qprformmessage?id='+encodedURLText;
 				
 				//redirectURL ='https://devtraina-charteredaccountantsanz.cs6.force.com/MyCA/qprformmessage';
                
                
        			}
       	
            else if(urlAction =='au_savensubmit'||urlAction =='nz_savensubmit')
            {
              //  QPR_Config__c obj= QPR_Config__c.getValues('FormAssemblyKey'); 
               // String keyValue=obj.QPR_Key_Value__c; 
                Blob key = Blob.valueOf(keyValue);
               	 texttoEncode =urlAction;
                 encodedURLParam = EncodingUtil.base64Encode(Crypto.encryptWithManagedIV('AES128', key, Blob.valueOf(texttoEncode)));
                 //encodedURLText= EncodingUtil.base64Encode(encodedCipherText);
                 encodedURLText=EncodingUtil.URLEncode(encodedURLParam,'UTF-8');
              	 redirectURL =  Url.getSalesforceBaseURL().toExternalForm()+'/MyCA/qprformmessage?id='+encodedURLText;
              //  redirectURL = Url.getSalesforceBaseURL().toExternalForm()+'/'+'MyCa/FA_formSubmitted';         
            }
           }
            }
            catch(exception e){
                system.debug('exception Message-->'+e.getMessage());
                system.debug('exception stack trace-->'+e.getStackTraceString());
            }
             	//redirectURL = 'https://devtraina-charteredaccountantsanz.cs6.force.com/MyCA/FA_formSubmitted';
         //  redirectURL ='https://devtraina-charteredaccountantsanz.cs6.force.com/customer/FA_SaveNResume';
         //    
             
PageReference componentUrl = new PageReference(redirectURL);
            
             componentUrl.setRedirect(true);
                    return componentUrl;
        }
    }