/*
 * @author          WDCi (LKoh)
 * @date            24-June-2019
 * @description     Test class for the wdci_UniSubjectPathwayController class
 * @changehistory
 */
 @isTest
public class wdci_UniSubjectPathwayTest {

    @TestSetup
    static void generateAssets() {

        wdci_TestUtil.generateAsset();
    }

    @isTest static void testUniSubjectPathwayController() {

        List<FT_University_Subject__c> uniSubjectList = [SELECT Id FROM FT_University_Subject__c];
        system.debug('uniSubjectList: ' +uniSubjectList);

        Test.startTest();

        List<FT_Pathway__c> relatedPathway = wdci_UniSubjectPathwayController.grabRelatedPathway(uniSubjectList[0].Id);
        system.debug('relatedPathway: ' +relatedPathway);

        Test.stopTest();
    }
}