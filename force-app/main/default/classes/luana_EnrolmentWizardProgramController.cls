/**
    Developer: WDCi (Lean)
    Development Date: 05/04/2016
    Task #: Enrollment wizard - Controller for Product Type selection
    
    Change History:
    LCA-1169 06/02/2018 - WDCi Lean: add capstone flag
**/

public without sharing class luana_EnrolmentWizardProgramController extends luana_EnrolmentWizardObject {
    
    luana_EnrolmentWizardController stdController;
    
    public luana_EnrolmentWizardProgramController(luana_EnrolmentWizardController stdController){
        this.stdController = stdController;
        
    }
    
    public luana_EnrolmentWizardProgramController(){
        
    }
    
    public PageReference programNext(){
        if(stdController.selectedProgramOfferingId != null && stdController.programOfferings != null){
            
            for(LuanaSMS__Program_Offering__c programOffering : stdController.programOfferings){
                if(programOffering.Id == stdController.selectedProgramOfferingId){
                    stdController.selectedProgramOffering = programOffering;
                    
                    //reset the value
                    stdController.selectedCourseId = null;
                    stdController.selectedCourse = null;
                    
                    break;
                }
            }
            
            //LCA-5, checking pass all subject
            if(stdController.selectedProgramOffering.isCapstone__c){
                stdController.isSelectedCapstone = stdController.selectedProgramOffering.isCapstone__c;
            	                
                if(!luana_EnrolmentUtil.eligibleForCapstone(stdController.custCommConId, stdController.studentAccreditedProgramSubjects, stdController.linkedSubjectId)){ //LCA-581, LCA-1169 - added subject param
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'You have not completed the necessary prerequisites or corequisites to be able to enrol into this module/course. If you require assistance, please contact the Service Centre.'));
                    
                    return null;
                }
            }
            
            PageReference pageRef = Page.luana_EnrolmentWizardCourse;
            stdController.skipValidation = true;
            
            return pageRef;
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please select a program offering below.'));
        }
        
        return null;
    }
    
    public PageReference programBack(){
        return null;
    }
    
    public List<SelectOption> getProgramOfferingOptions(){
        
        List<SelectOption> options = new List<SelectOption>();
        
        if(stdController.programOfferings != null){
            for(LuanaSMS__Program_Offering__c programOffering : stdController.programOfferings){
                
                
                system.debug('programOffering.LuanaSMS__Courses__r: ' + programOffering.Id + ' - ' + programOffering.LuanaSMS__Courses__r);
                SelectOption option;
                if(programOffering.LuanaSMS__Courses__r.size() == 0){
                    option = new SelectOption(programOffering.Id, programOffering.Name + ' - No Available Courses', true);
                }else{
                    option = new SelectOption(programOffering.Id, programOffering.Name, false);
                    options.add(option); //LCA-148 - do not show program offering without course
                }
                
                //options.add(option); //LCA-148 - do not show program offering without course
            }
        }
        
        return options;
    }
    
}