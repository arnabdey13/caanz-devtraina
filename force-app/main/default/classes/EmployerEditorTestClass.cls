@isTest
/*
    Test class for EmployerEditor
    ======================================================
    Changes:
    	Apr 2016	Davanti - JC	Created
    	Aug 2016	Davanti - RN	Changed Is_CPP_Provided__c to false (defaults to true)
*/
public class EmployerEditorTestClass {
    
    private static Account getFullMemberAccountObject(){
        Account FullMemberAccountObject = TestObjectCreator.createFullMemberAccount();
        FullMemberAccountObject.Member_Of__c = 'NZICA' ;
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        return FullMemberAccountObject;
    }
    
    static testMethod void empListTestMethod(){
        Account BusinessAccountObject = TestObjectCreator.createBusinessAccount();
        BusinessAccountObject.BillingStreet = '8 Nelson Street';
        BusinessAccountObject.Member_Of__c = 'NZICA' ;
        BusinessAccountObject.Status__c = 'Active' ;
        insert BusinessAccountObject;
        
        Account FullMemberAccountObject = getFullMemberAccountObject();
        Test.startTest() ;
        insert FullMemberAccountObject; // Future method
        Test.stopTest() ;
        // Check Results
        Id FullMemberContactId = [Select id FROM Contact WHERE AccountId =: FullMemberAccountObject.Id].Id;
        List<User> User_List = [Select FederationIdentifier FROM User WHERE contactId=:FullMemberContactId];
        System.assertEquals(1, User_List.size(), 'User_List.size' );
        System.assertNotEquals(null, User_List[0].FederationIdentifier, 'FederationIdentifier' );
        system.runAs(User_List[0]) {
            EmployerEditorClass empClass = new EmployerEditorClass();
            EmployerEditorClass.returnCurrntUserMember() ;
            List<Employment_History__c> empLoadList = EmployerEditorClass.EmpList();
            System.assertEquals(0, empLoadList.size(), 'empLoadList.size' );
            //Creating new Employment History record.
            Employment_History__c EmploymentHistoryObject = TestObjectCreator.createEmploymentHistory();         
            EmploymentHistoryObject.Job_Title__c = 'SFDC Dev Opps Team' ;
            EmploymentHistoryObject.Is_CPP_Provided__c = false;
            EmploymentHistoryObject.Employee_Start_Date__c = System.today() - 20 ;
            EmploymentHistoryObject.Employee_End_Date__c = System.today() - 2 ;
            EmployerEditorClass.getAccountupdatedlist(EmploymentHistoryObject, BusinessAccountObject.Id) ;
            
            List<Employment_History__c> empInsertList = EmployerEditorClass.EmpList();
            System.assertEquals(1, empInsertList.size(), 'empLoadList.size after insert' );
            
            Employment_History__c  empRecord = EmployerEditorClass.getEmpRecordDetail(empInsertList[0].Id) ;
            System.assertEquals('SFDC Dev Opps Team', empRecord.Job_Title__c, 'empLoadList Job Title' );
            
            EmployerEditorClass.updateEmpRecord(empInsertList[0]) ;
            List<Account> accList = EmployerEditorClass.findByName('Test', 'NZICA') ;
            EmployerEditorClass.getStatusPickListClass() ;
        }
        
    }
}