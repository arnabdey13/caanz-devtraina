/**
Developer: akopec
Date:	27/03/2018

Change History:
**/
@isTest(seeAllData=false)
public class SegmentationMaintenanceTest {

    public static Account memberAccount {get; set;}
    public static Account businessAccount {get; set;} 
    public static Employment_History__c eh {get; set;}	
    public static SegmentationDataGenTest testDataGen;
    private static boolean debug = true;
 
    ////////////////////// Main Practice Lookup Prep ///////////////////////////////////////////////////////////////
    @testSetup 
    static void setup() {
        
        Reference_Main_Practices__c  deloitte = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Deloitte'
            , Search_Name__c     = 'Deloitte'
            , Full_Name__c       = 'Deloitte'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            = 'Deloitte'
        );
      
		Reference_Main_Practices__c  pwc = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'PricewaterhouseCoopers;PWC; PwC; PriceWaterHouseCoopers; PriceWaterhouseCoopers; PricewaterhousCoopers; Pricewaterhousecoopers'
            , Search_Name__c     = 'PricewaterhouseCoopers'
            , Full_Name__c       = 'PricewaterhouseCoopers'
            , Practice_Size__c   = 'Large Firm'
			, Big4__c            = 'PWC'
        );
        
		Reference_Main_Practices__c  kpmg = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'KPMG'
            , Search_Name__c     = 'KPMG'
            , Full_Name__c       = 'KPMG'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            = 'KPMG'
        );
        
		Reference_Main_Practices__c  ey = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Ernst and Young; EY; Ernst & Young'
            , Search_Name__c     = 'Ernst and Young'
            , Full_Name__c       = 'Ernst and Young'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            = 'EY'
        );
        
		Reference_Main_Practices__c  bdo = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'BDO'
            , Search_Name__c     = 'BDO'
            , Full_Name__c       = 'BDO'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            =  NULL
        );
        
		Reference_Main_Practices__c  gt = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Grant Thornton'
            , Search_Name__c     = 'Grant Thornton'
            , Full_Name__c       = 'Grant Thornton'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            =  NULL
        );        
        
        
		Reference_Main_Practices__c  vincents = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Vincents Chartered Accountants; Vincents'
            , Search_Name__c     = 'Vincents Chartered Accountants'
            , Full_Name__c       = 'Vincents Chartered Accountants'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            =  NULL
        );
        
        Reference_Main_Practices__c  boroughs = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Boroughs'
            , Search_Name__c     = 'Boroughs'
            , Full_Name__c       = 'Boroughs'
            , Practice_Size__c   = 'Medium Firm'
            , Big4__c            =  NULL
        );
        
        insert new List<Reference_Main_Practices__c>{pwc, deloitte, kpmg, ey, bdo, gt, vincents, boroughs};  
            
             
		Segmentation_Settings__c setting = new Segmentation_Settings__c();        
        setting.Name = 'SegmentationMembersUpdate';
		setting.Active__c = TRUE;
		insert setting;
  
    }
    
    
    public static void createMidMemberCurrentEmpBig4AU(){
		memberAccount = SegmentationDataGenTest.genFullMemberAustralia('Brown', NULL, 'brown1@test.co.au', Date.valueOf('1975-05-12') );
        insert memberAccount;
		businessAccount = SegmentationDataGenTest.genPracticeAU( 'PricewaterhouseCoopers', 'Chartered Accounting', 'Brisbane', 25);
        insert businessAccount;
		eh = SegmentationDataGenTest.genCurrentEmploymentHistory(memberAccount.Id, businessAccount.Id, 'Manager', 'Tax Manager');  
		insert eh;   
    }
 
    public static void createMemberDirectorLargePracticeAU(){
		memberAccount = SegmentationDataGenTest.genFullMemberAustralia('Brown', 'Director', 'brown2@test.co.au', Date.valueOf('1965-05-12') );
        insert memberAccount;
		businessAccount = SegmentationDataGenTest.genPracticeAU( 'BDO', 'Chartered Accounting', 'Perth', 10);
        insert businessAccount;
		eh = SegmentationDataGenTest.genCurrentEmploymentHistory(memberAccount.Id, businessAccount.Id, 'Director', 'Tax Director');  
		insert eh;   
    }
    
    ///////////////////////  Test /////////////////////////////////////////////////////////////////////////////////////////////////////////////



	// Test 
	// Case 1a. New records - No Segmentation     
	private static testMethod void newMemberNoEmploymentAU_Case1(){    

        createMidMemberCurrentEmpBig4AU();
		createMemberDirectorLargePracticeAU();

		List<Segmentation__c> segmentsBefore  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c ];
        System.debug('Senior Member Segmentation Before :' + segmentsBefore);
        System.assertEquals(0, segmentsBefore.size());
        
        Test.startTest();         
        SegmentationMaintenance.refreshNewMembers();
       	Test.stopTest();

        
		List<Segmentation__c> segmentsNew  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c ];
        System.debug('Senior Member Segmentation after :' + segmentsNew);
        System.assertEquals(2, segmentsNew.size());
	}   


    
	private static testMethod void newMemberNoEmploymentAU_Case2(){    

        createMidMemberCurrentEmpBig4AU();
		createMemberDirectorLargePracticeAU();

		List<Segmentation__c> segmentsBefore  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c ];
        System.debug('Senior Member Segmentation Before :' + segmentsBefore);
        System.assertEquals(0, segmentsBefore.size());
        
        Test.startTest();         
        SegmentationMaintenance.refreshAllMembers();
       	Test.stopTest();

        
		List<Segmentation__c> segmentsNew  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c ];
        System.debug('Senior Member Segmentation after :' + segmentsNew);
        System.assertEquals(2, segmentsNew.size());
	}   
     

    private static testMethod void SeniorMemberAccountAU_Case1(){
		if (debug) System.debug( 'TEST PROCESSING - SegmentationMaintenanceTest');
		if (debug) System.debug( 'SeniorMemberAccountAU_Case1');

		Account SeniorMemberAccountAU = SegmentationDataGenTest.genFullMemberAustralia('Turner', 'Tax Manager', 'brown1@test.co.au', Date.valueOf('1977-05-12') );
        insert SeniorMemberAccountAU; 
        Account big4BusinessAccountAU = SegmentationDataGenTest.genPracticeAU( 'Deloitte', 'Chartered Accounting', 'Sydney', 25);
		insert big4BusinessAccountAU;        
        Employment_History__c histEmpSeniorMemberAU = SegmentationDataGenTest.genCurrentEmploymentHistory(SeniorMemberAccountAU.Id, big4BusinessAccountAU.Id, 'Tax Manager', 'Manager');  
		insert histEmpSeniorMemberAU; 

        Segmentation__c seniorMemberAUSeg = new Segmentation__c();
        seniorMemberAUSeg.Account__c = SeniorMemberAccountAU.Id;
        seniorMemberAUSeg.Organisation_Type__c = 'Education';
        seniorMemberAUSeg.Business_Segmentation_MS__c = 'Public Team Player';
		insert seniorMemberAUSeg;

        List<Segmentation__c> segmentsBefore  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{SeniorMemberAccountAU.Id}];
        System.debug('Senior Member Segmentation before :' + segmentsBefore);

        Test.startTest();         
		SegmentationMaintenance.refreshAllExistingSegments();
       	Test.stopTest();

        
        List<Segmentation__c> segmentsAfter  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{SeniorMemberAccountAU.Id}];
        System.debug('Senior Member Segmentation after refresh all segments :' + segmentsAfter);
		System.assertEquals('Practice', segmentsAfter[0].Organisation_Type__c);
		System.assertEquals('Large Practice Leader', segmentsAfter[0].Business_Segmentation_MS__c);
    }
 
    
	private static testMethod void SeniorMemberAccountAU_Case2(){
		if (debug) System.debug( 'TEST PROCESSING - SegmentationMaintenanceTest');
		if (debug) System.debug( 'SeniorMemberAccountAU_Case2');

		Account SeniorMemberAccountAU = SegmentationDataGenTest.genFullMemberAustralia('Turner', 'Tax Manager', 'brown1@test.co.au', Date.valueOf('1977-05-12') );
        insert SeniorMemberAccountAU; 
        Account big4BusinessAccountAU = SegmentationDataGenTest.genPracticeAU( 'Deloitte', 'Chartered Accounting', 'Sydney', 25);
		insert big4BusinessAccountAU;        
        Employment_History__c histEmpSeniorMemberAU = SegmentationDataGenTest.genCurrentEmploymentHistory(SeniorMemberAccountAU.Id, big4BusinessAccountAU.Id, 'Tax Manager', 'Manager');  
		insert histEmpSeniorMemberAU; 

        Segmentation__c seniorMemberAUSeg = new Segmentation__c();
        seniorMemberAUSeg.Account__c = SeniorMemberAccountAU.Id;
        seniorMemberAUSeg.Organisation_Type__c = 'Education';
		insert seniorMemberAUSeg;

        List<Segmentation__c> segmentsBefore  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{SeniorMemberAccountAU.Id}];
        System.debug('Senior Member Segmentation before :' + segmentsBefore);
        
        Test.startTest();         
        SegmentationMaintenance.refreshAllMembers();
       	Test.stopTest();
        
        List<Segmentation__c> segmentsAfter  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{SeniorMemberAccountAU.Id}];
        System.debug('Senior Member Segmentation after :' + segmentsAfter);
		System.assertEquals('Practice', segmentsAfter[0].Organisation_Type__c); 
    }
    

    
     private static testMethod void DuplicateSegments_Case1(){
		if (debug) System.debug( 'TEST PROCESSING - SegmentationMaintenanceTest');
		if (debug) System.debug( 'DuplicateSegments_Case1_TestRun');

		Account SeniorMemberAccountAU = SegmentationDataGenTest.genFullMemberAustralia('Turner', 'Tax Manager', 'brown1@test.co.au', Date.valueOf('1977-05-12') );
        insert SeniorMemberAccountAU; 
        Account big4BusinessAccountAU = SegmentationDataGenTest.genPracticeAU( 'Deloitte', 'Chartered Accounting', 'Sydney', 25);
		insert big4BusinessAccountAU;        
        Employment_History__c histEmpSeniorMemberAU = SegmentationDataGenTest.genHistoricalEmploymentHistory(SeniorMemberAccountAU.Id, big4BusinessAccountAU.Id, 'Tax Manager', 'Manager');  
		insert histEmpSeniorMemberAU; 

        Segmentation__c seniorMemberAUSeg = new Segmentation__c();
        seniorMemberAUSeg.Account__c = SeniorMemberAccountAU.Id;
        seniorMemberAUSeg.Organisation_Type__c = 'Education';
		insert seniorMemberAUSeg;
         
        List<Segmentation__c> segmentsBefore1  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c, LastModifiedDate
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{SeniorMemberAccountAU.Id}];
         
        System.debug('Senior Member Segmentation after first insert :' + segmentsBefore1);         
         
        Segmentation__c duplicateSeg = new Segmentation__c();
        duplicateSeg.Account__c = SeniorMemberAccountAU.Id;
        duplicateSeg.Organisation_Type__c = 'Practice';
		insert duplicateSeg;
 
        List<Segmentation__c> segmentsBefore2  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c, LastModifiedDate
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{SeniorMemberAccountAU.Id}];
        
        System.assertEquals(2, segmentsBefore2.size());
        System.debug('Senior Member Segmentation before dedup :' + segmentsBefore2); 
        
        Test.startTest();  
        SegmentationMaintenance.removeDuplicates(true);
		Test.stopTest();
         
        List<Segmentation__c> segmentsAfterDedup  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c, LastModifiedDate
                                           		 	from Segmentation__c                                                  
                                                 	where Account__c IN : new List<Id>{SeniorMemberAccountAU.Id}];
		System.assertEquals(2, segmentsAfterDedup.size());
    }
    
    
	private static testMethod void DuplicateSegments_Case2(){
		if (debug) System.debug( 'TEST PROCESSING - SegmentationMaintenanceTest');
		if (debug) System.debug( 'DuplicateSegments_Case2_FullRun');

		Account SeniorMemberAccountAU = SegmentationDataGenTest.genFullMemberAustralia('Turner', 'Tax Manager', 'brown1@test.co.au', Date.valueOf('1977-05-12') );
        insert SeniorMemberAccountAU; 
        Account big4BusinessAccountAU = SegmentationDataGenTest.genPracticeAU( 'Deloitte', 'Chartered Accounting', 'Sydney', 25);
		insert big4BusinessAccountAU;        
        Employment_History__c histEmpSeniorMemberAU = SegmentationDataGenTest.genHistoricalEmploymentHistory(SeniorMemberAccountAU.Id, big4BusinessAccountAU.Id, 'Tax Manager', 'Manager');  
		insert histEmpSeniorMemberAU; 

        Segmentation__c seniorMemberAUSeg = new Segmentation__c();
        seniorMemberAUSeg.Account__c = SeniorMemberAccountAU.Id;
        seniorMemberAUSeg.Organisation_Type__c = 'Education';
		insert seniorMemberAUSeg;
         
        List<Segmentation__c> segmentsBefore1  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c, LastModifiedDate
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{SeniorMemberAccountAU.Id}];
         
        System.debug('Senior Member Segmentation after first insert :' + segmentsBefore1);         
         
        Segmentation__c duplicateSeg = new Segmentation__c();
        duplicateSeg.Account__c = SeniorMemberAccountAU.Id;
        duplicateSeg.Organisation_Type__c = 'Practice';
		insert duplicateSeg;
 
        List<Segmentation__c> segmentsBefore2  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c, LastModifiedDate
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{SeniorMemberAccountAU.Id}];
        
        System.assertEquals(2, segmentsBefore2.size());
        System.debug('Senior Member Segmentation before dedup :' + segmentsBefore2); 
        
        Test.startTest();  
        SegmentationMaintenance.removeDuplicates(false);
		Test.stopTest();
         
        List<Segmentation__c> segmentsAfterDedup  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c, LastModifiedDate
                                           		 	from Segmentation__c                                                  
                                                 	where Account__c IN : new List<Id>{SeniorMemberAccountAU.Id}];
		System.assertEquals(1, segmentsAfterDedup.size());
    }


    
	private static testMethod void SeniorMemberAccountAU_Case3(){
		if (debug) System.debug( 'TEST PROCESSING - SegmentationMaintenanceTest');
		if (debug) System.debug( 'SeniorMemberAccountAU_Case2');

		Account SeniorMemberAccountAU = SegmentationDataGenTest.genFullMemberAustralia('Turner', 'Tax Manager', 'brown1@test.co.au', Date.valueOf('1977-05-12') );
        insert SeniorMemberAccountAU; 
        Account big4BusinessAccountAU = SegmentationDataGenTest.genPracticeAU( 'Deloitte', 'Chartered Accounting', 'Sydney', 25);
		insert big4BusinessAccountAU;        
        Employment_History__c histEmpSeniorMemberAU = SegmentationDataGenTest.genCurrentEmploymentHistory(SeniorMemberAccountAU.Id, big4BusinessAccountAU.Id, 'Tax Manager', 'Manager');  
		insert histEmpSeniorMemberAU; 

        Segmentation__c seniorMemberAUSeg = new Segmentation__c();
        seniorMemberAUSeg.Account__c = SeniorMemberAccountAU.Id;
        seniorMemberAUSeg.Organisation_Type__c = 'Education';
        seniorMemberAUSeg.Business_Segmentation_MS__c = 'Public Team Player';
		insert seniorMemberAUSeg;

        List<Segmentation__c> segmentsBefore  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c, LastModifiedDate
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{SeniorMemberAccountAU.Id}];
        System.debug('Senior Member Segmentation before :' + segmentsBefore);

        String lastModDate = DateTime.now().addDays(1).format('yyyy-MM-dd\'T\'hh:mm:ss\'z\'');      
        System.debug(lastModDate);
        
        Test.startTest();         
		SegmentationMaintenance.refreshOldExistingSegments(lastModDate, 1);
       	Test.stopTest();
        
        List<Segmentation__c> segmentsAfter  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c, LastModifiedDate
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{SeniorMemberAccountAU.Id}];
        System.debug('Senior Member Segmentation after :' + segmentsAfter);
		System.assertEquals('Practice', segmentsAfter[0].Organisation_Type__c); 
    }
   
    
}