/**
    Developer: WDCi (kh)
    Development Date:21/03/2016
    Task: Luana Test class for luana_MemberDataIntegrationHandler and luana_NonMemberDataIntegrationHandler
**/
@isTest(seeAllData=false)
private class luana_MemberDataIntegrationHandler_Test {
    
    public static Luana_DataPrep_Test dataPrep;
    public static Account acc;
    
    private static String classNamePrefixLong = 'luana_MemberDataIntegrationHandler_Test';
    private static String classNamePrefixShort = 'lmdih';
    private static map<String, Id> commProfIdMap {get; set;}
    public static Account memberAccount {get; set;}
    
    public static void prepareSampleData(String recTypeString){
        
        //initialize
        dataPrep = new Luana_DataPrep_Test();
        
        //Create all the custom setting
        insert dataPrep.prepLuanaExtensionSettingCustomSettings();
        insert dataPrep.createLuanaConfigurationCustomSetting();
        
        //Create user with Member and Employer community access
        memberAccount = dataPrep.generateNewApplicantAcc('Joe_' + classNamePrefixShort, classNamePrefixLong, recTypeString);
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
       
        memberAccount.personEmail = 'joe_1_'+classNamePrefixShort +'@gmail.com';
        
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
        
    }

    public static testMethod void testCreateUserWithCustCommStep1_1() { 
        Test.startTest();
            prepareSampleData('Full_Member');
            memberAccount.Membership_Class__c = 'Full';
            memberAccount.Communication_Preference__c= 'Home Phone';
            memberAccount.PersonHomePhone= '1234';
            memberAccount.PersonOtherStreet= '83 Saggers Road';
            memberAccount.PersonOtherCity='JITARNING';
            memberAccount.PersonOtherState='Western Australia';
            memberAccount.PersonOtherCountry='Australia';
            memberAccount.PersonOtherPostalCode='6365';  
            
             
            insert memberAccount;
        Test.stopTest();
        
        List<User> newUsers = new List<User>();
            
        //User user = dataPrep.generateNewApplicantUser(classNamePrefixLong+'_1', classNamePrefixshort+'_1', memberAccount, commProfIdMap.get('Customer Community Login User'));
        //insert user;//LCA-921
            
        User user = [Select Id, Email, FirstName, LastName, UserName, Name, isActive from User Where Email =: 'joe_1_' + classNamePrefixShort+'@gmail.com' limit 1];    
        newUsers.add(user);

        
        //To outside the Test is because the PermissionSet is create by using furture method  
        Boolean hasNonMemberCommPerm = false;
        for(PermissionSetAssignment psa: [SELECT AssigneeId, Assignee.Name, PermissionSetId, PermissionSet.Name FROM PermissionSetAssignment Where AssigneeId =: user.Id]){
            if(psa.PermissionSet.Name == 'Luana_NonMemberCommunityPermission'){
                hasNonMemberCommPerm = true;
            }
        }
        System.assertEquals(hasNonMemberCommPerm, false, 'Test 1.1 fail: Expect no \'Luana_NonMemberCommunityPermission\' Permission Set will assign to the user.');  
        
        
        luana_NonMemberDataIntegrationHandler.updateNonMemberPermSet(newUsers);
    }

    /*public static testMethod void testCreateUserWithNZICACommStep1_2() { 
        
        prepareSampleData();
        
        Test.startTest(); 
            User user = dataPrep.generateNewApplicantUser('Black', acc, commProfIdMap.get('NZICA Community Login User'));
        Test.stopTest(); 

        //To outside the Test is because the PermissionSet is create by using furture method        
        Boolean hasNonMemberCommPerm = false;
        for(PermissionSetAssignment psa: [SELECT AssigneeId, Assignee.Name, PermissionSetId, PermissionSet.Name FROM PermissionSetAssignment Where AssigneeId =: user.Id]){
            if(psa.PermissionSet.Name == 'Luana_NonMemberCommunityPermission'){
                hasNonMemberCommPerm = true;
            }
        }
        System.assertEquals(hasNonMemberCommPerm, true, 'Test 1.2 fail: Expect \'Luana_NonMemberCommunityPermission\' Permission Set assigned to the user.');    
             
    }*/ 
  
    public static testMethod void testUpdateAccountStep2_1() { 
        Test.startTest();
            prepareSampleData('Non_member');
            //memberAccount.Membership_Class__c = 'Full';
            memberAccount.Communication_Preference__c= 'Home Phone';
            memberAccount.PersonHomePhone= '1234';
            memberAccount.PersonOtherStreet= '83 Saggers Road';
            memberAccount.PersonOtherCity='JITARNING';
            memberAccount.PersonOtherState='Western Australia';
            memberAccount.PersonOtherCountry='Australia';
            memberAccount.PersonOtherPostalCode='6365';  
             insert memberAccount;
        Test.stopTest();
        
        //User user = dataPrep.generateNewApplicantUser(classNamePrefixLong +'_2', classNamePrefixshort+'_2', memberAccount, commProfIdMap.get('Customer Community Login User'));
        //insert user;//LCA-921
   
        List<User> user = [Select Id, Email, FirstName, LastName, UserName, Name from User Where Email =: 'joe_1_' + classNamePrefixShort+'@gmail.com' limit 1];   
        System.assertEquals(1, user.size(), 'Expect 1 user created');
        
        Account updatePA = [Select Id, RecordTypeId, Membership_Class__c from Account Where Id =: memberAccount.Id limit 1];
        updatePA.RecordTypeId = dataPrep.getAccRTMap().get('Full_Member');
        updatePA.Membership_Class__c = 'Full';
        
        update updatePA;

/*
This will not be able to test already because it is run in future method also            
        //To outside the Test is because the PermissionSet is create by using furture method        
        Boolean hasMemberCommPerm = false;
        for(PermissionSetAssignment psa: [SELECT AssigneeId, Assignee.Name, PermissionSetId, PermissionSet.Name FROM PermissionSetAssignment Where AssigneeId =: user.Id]){
            if(psa.PermissionSet.Name == 'Luana_MemberCommunityPermission'){
                hasMemberCommPerm = true;
            }
        }
        System.assertEquals(hasMemberCommPerm, true, 'Test 2.1 fail: Expect \'Luana_MemberCommunityPermission\' Permission Set assigned to the user.');    
*/        
    }
    
    /*public static testMethod void testUpdateAccountStep2_2() { 
        
        prepareSampleData();

        User user = dataPrep.generateNewApplicantUser('Black', acc, commProfIdMap.get('NZICA Community Login User'));

        Test.startTest(); 
            
            Account updatePA = [Select Id, RecordTypeId, Membership_Class__c from Account Where Id =: acc.Id limit 1];
            updatePA.RecordTypeId = dataPrep.getAccRTMap().get('Non_member');
            
            update updatePA;

        Test.stopTest(); 
        
        //To outside the Test is because the PermissionSet is create by using furture method        
        Boolean hasNonMemberCommPerm = false;
        for(PermissionSetAssignment psa: [SELECT AssigneeId, Assignee.Name, PermissionSetId, PermissionSet.Name FROM PermissionSetAssignment Where AssigneeId =: user.Id]){
            if(psa.PermissionSet.Name == 'Luana_NonMemberCommunityPermission'){
                hasNonMemberCommPerm = true;
            }
        }
        System.assertEquals(hasNonMemberCommPerm, true, 'Test 2.2 fail: Expect \'Luana_NonMemberCommunityPermission\' Permission Set assigned to the user.');    
    }*/
    
    public static testMethod void testUpdateAccountStep2_3() { 
        Test.startTest();
            prepareSampleData('Full_Member');
            memberAccount.Membership_Class__c = 'Provisional';
            memberAccount.Communication_Preference__c= 'Home Phone';
            memberAccount.PersonHomePhone= '1234';
            memberAccount.PersonOtherStreet= '83 Saggers Road';
            memberAccount.PersonOtherCity='JITARNING';
            memberAccount.PersonOtherState='Western Australia';
            memberAccount.PersonOtherCountry='Australia';
            memberAccount.PersonOtherPostalCode='6365';  
            insert memberAccount;
        Test.stopTest();
        
        //User user = dataPrep.generateNewApplicantUser(classNamePrefixLong +'_3', classNamePrefixshort+'_3', memberAccount, commProfIdMap.get('Customer Community Login User'));
        //insert user;
        List<User> user = [Select Id, Email, FirstName, LastName, UserName, Name from User Where Email =: 'joe_1_' + classNamePrefixShort+'@gmail.com' limit 1];   
        System.assertEquals(1, user.size(), 'Expect no user created');
        
        //Test.startTest(); 
            
        /*LCA-921, wont be able to run this line, because it is future method
        Account updatePA = [Select Id, RecordTypeId, Membership_Class__c from Account Where Id =: memberAccount.Id limit 1];
        updatePA.RecordTypeId = dataPrep.getAccRTMap().get('Full_Member');
        updatePA.Membership_Class__c = 'Provisional';
        
        update updatePA;
        */

        //Test.stopTest(); 
        
        //To outside the Test is because the PermissionSet is create by using furture method        
/*      LCA-921, wont be able to run this line, because it is future method  
        Boolean hasMemberCommPerm = false;

        for(PermissionSetAssignment psa: [SELECT AssigneeId, Assignee.Name, PermissionSetId, PermissionSet.Name FROM PermissionSetAssignment Where AssigneeId =: user.Id]){
            if(psa.PermissionSet.Name == 'Luana_MemberCommunityPermission'){
                hasMemberCommPerm = true;
            }
        }
*/
    }
    
    //LCA-502
    public static testMethod void testUpdateAccountStep2_4() { 
        
        /*prepareSampleData();
        
        User user = dataPrep.generateNewApplicantUser(classNamePrefixLong +'_4', classNamePrefixshort+'_4', memberAccount, commProfIdMap.get('Customer Community Login User'));
        insert user;
        
        Account updatePA = [Select Id, RecordTypeId, Membership_Class__c from Account Where Id =: memberAccount.Id limit 1];

        Test.startTest(); 
            luana_MemberDataIntegrationHandler.processedRecordIds = new Map<Id, Id>();
            updatePA.Membership_Type__c = 'Past Member';
            
            update updatePA;
        Test.stopTest(); 
        
        //To outside the Test is because the PermissionSet is create by using furture method        
        Boolean hasMemberCommPerm = false;
        for(PermissionSetAssignment psa: [SELECT AssigneeId, Assignee.Name, PermissionSetId, PermissionSet.Name FROM PermissionSetAssignment Where AssigneeId =: user.Id]){
            if(psa.PermissionSet.Name == 'Luana_MemberCommunityPermission'){
                hasMemberCommPerm = true;
            }
        }
        //System.assertEquals(hasMemberCommPerm, false, 'Test 2.1 fail: Expect \'Luana_MemberCommunityPermission\' Permission Set removed from the user.');    
        */
    }
    
/*    
    public static testMethod void testUpdateAccountStep3_1() { 
        
        prepareSampleData();

        User user = dataPrep.generateNewApplicantUser('Black', acc, commProfIdMap.get('NZICA Community Login User'));

        Test.startTest(); 
            
            Account updatePA = [Select Id, RecordTypeId, Membership_Class__c from Account Where Id =: acc.Id limit 1];
            updatePA.RecordTypeId = dataPrep.getAccRTMap().get('Full_Member');
            updatePA.Membership_Class__c = 'Provisional';
            
            update updatePA;
            
            //Expect the user already have the MemberCommunityPermission after this line
            
            Account updatePAAgain = [Select Id, RecordTypeId, Membership_Class__c from Account Where Id =: acc.Id limit 1];
            updatePAAgain.RecordTypeId = dataPrep.getAccRTMap().get('Full_Member');
            updatePAAgain.Membership_Class__c = 'Full';
           
            update updatePAAgain;
        Test.stopTest(); 
        
        //To outside the Test is because the PermissionSet is create by using furture method        
        Boolean hasNonMemberCommPerm = false;
        Boolean hasMemberCommPerm = false;

        for(PermissionSetAssignment psa: [SELECT AssigneeId, Assignee.Name, PermissionSetId, PermissionSet.Name FROM PermissionSetAssignment Where AssigneeId =: user.Id]){
            if(psa.PermissionSet.Name == 'Luana_NonMemberCommunityPermission'){
                hasNonMemberCommPerm = true;
            }
            
            if(psa.PermissionSet.Name == 'Luana_MemberCommunityPermission'){
                hasMemberCommPerm = true;
            }
        }
        System.assertEquals(hasNonMemberCommPerm, true, 'Test 3.1 fail: Expect \'Luana_NonMemberCommunityPermission\' Permission Set assigned to the user.');    
        System.assertEquals(hasNonMemberCommPerm, true, 'Test 3.1 fail: Expect no \'Luana_MemberCommunityPermission\' Permission Set assign to the user.');
        
    }
    
    public static testMethod void testUpdateAccountStep3_2() { 
        
        prepareSampleData();

        User user = dataPrep.generateNewApplicantUser('Black', acc, commProfIdMap.get('NZICA Community Login User'));

        Test.startTest(); 
            
            Account updatePA = [Select Id, RecordTypeId, Membership_Class__c from Account Where Id =: acc.Id limit 1];
            updatePA.RecordTypeId = dataPrep.getAccRTMap().get('Full_Member');
            updatePA.Membership_Class__c = 'Provisional';
            
            update updatePA;
            
            //Expect the user already have the MemberCommunityPermission after this line
            
            Account updatePAAgain = [Select Id, RecordTypeId, Membership_Class__c from Account Where Id =: acc.Id limit 1];
            updatePAAgain.RecordTypeId = dataPrep.getAccRTMap().get('Non_member');
           
            update updatePAAgain;
        Test.stopTest(); 
        
        //To outside the Test is because the PermissionSet is create by using furture method        
        Boolean hasNonMemberCommPerm = false;
        Boolean hasMemberCommPerm = false;

        for(PermissionSetAssignment psa: [SELECT AssigneeId, Assignee.Name, PermissionSetId, PermissionSet.Name FROM PermissionSetAssignment Where AssigneeId =: user.Id]){
            if(psa.PermissionSet.Name == 'Luana_NonMemberCommunityPermission'){
                hasNonMemberCommPerm = true;
            }
            
            if(psa.PermissionSet.Name == 'Luana_MemberCommunityPermission'){
                hasMemberCommPerm = true;
            }
        }
        System.assertEquals(hasNonMemberCommPerm, true, 'Test 3.1 fail: Expect \'Luana_NonMemberCommunityPermission\' Permission Set assigned to the user.');    
        System.assertEquals(hasNonMemberCommPerm, true, 'Test 3.1 fail: Expect no \'Luana_MemberCommunityPermission\' Permission Set assign to the user.');
        
    }
    */
}