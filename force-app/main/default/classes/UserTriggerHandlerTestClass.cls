@isTest(SeeAllData=false)
public class UserTriggerHandlerTestClass {
    
    private static Account getFullMemberAccountObject(){
        Account FullMemberAccountObject = TestObjectCreator.createFullMemberAccount();
        return FullMemberAccountObject;
    }
    
    static testMethod void userTriggerTestMethod(){
        Account FullMemberAccountObject = getFullMemberAccountObject();
        Test.startTest();
        insert FullMemberAccountObject; // Future method
        Test.stopTest();
        // Check Results
        Id FullMemberContactId = [Select id FROM Contact WHERE AccountId =: FullMemberAccountObject.Id].Id;
        List<User> User_List = [Select Email, UserName, AccountId, FederationIdentifier FROM User WHERE contactId=:FullMemberContactId];
        System.assertEquals(1, User_List.size(), 'User_List.size' );
        System.assertNotEquals(null, User_List[0].FederationIdentifier, 'FederationIdentifier' );
        Profile p = [SELECT Id FROM Profile WHERE Name='CAANZ System Admin'];
        String randomEmail = 'caanzadminuserintestmethod' + TestObjectCreator.getRandomNumber(563563748) +'@google.com';
        User u = new User(Alias = 'standt', Email=randomEmail, 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName=randomEmail);
        insert u ;
        system.runAs(u) {
            User_List[0].Email = 'jackdaniel' + TestObjectCreator.getRandomNumber(6363) +'@google.com';
            update User_List[0];           
            UserTriggerHandlerClass.onBeforeInsert(new List<User>{User_List[0]}) ;
            UserTriggerHandlerClass.onAfterInsert(new List<User>{User_List[0]}) ;
            UserTriggerHandlerClass.onAfterUpdate(new List<User>{User_List[0]}, null) ;
        }
    }
    
    /*@isTest(SeeAllData=true)
    static  void userTriggerEmailUpdatetMethod(){
        Account personAcc= new Account();
        for(Account personAccRec : [SELECT PersonEmail, Id, isPersonAccount FROM Account WHERE PersonEmail != null AND isPersonAccount= true LIMIT 1]){
            personAcc = personAccRec ;                       
        }
        if(String.isNotEmpty(personAcc.Id)){
            personAcc.PersonEmail = 'jackdanielPersonAcc' + TestObjectCreator.getRandomNumber(73834) +'@google.com';
            update personAcc;
        }                      
    }*/
    
}