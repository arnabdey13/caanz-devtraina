/*
    API for FindCA - Find CA - Search Public Practitioner
    November 2017   CAANZ - akopec  

    Example: /services/apexrest/FindCA/v1.0/Nearest?MemberType=Business+Valuation+Specialist&Latitude=-36.8804549&Longitude=174.7407973&Limit=20
			/services/apexrest/FindCA/v1.0/Nearest?MemberType=Forensic+Accounting+Specialist&Latitude=-33.8804549&Longitude=151.7407973&Limit=20
			/services/apexrest/FindCA/v1.0/Nearest?Latitude=-36.8804549&Longitude=174.7407973&Limit=20

	R01 akopec 23 Jan 2018 added  Firstname, Preffered, Middle and Lastname to the results
	R02 akopec 6 Feb 2018  added upper limit on number of returned results
	R03 akopec 17 April 2018  removed limitation on displaying Qualified Auditors email and phone
	R04 akopec 21 Nov 2018 Forensic Accounting
*/
@RestResource(urlMapping='/FindCA/v1.0/Nearest/*')
global without sharing class FindCANearest {

    private static final String STR_MEMBERTYPE_QUALIFIED_AUDITORS = 'Qualified Auditor';
    private static final String STR_MEMBERTYPE_INSOLVENCY_PRACTICE = 'Insolvency Practitioner';
    private static final String STR_MEMBERTYPE_SMSF_SPECIALIST = 'SMSF Specialist';
    private static final String STR_MEMBERTYPE_BUSINESS_VALUATION_SPECIALIST = 'Business Valuation Specialist';
    private static final String STR_MEMBERTYPE_FINANCIAL_PLANNING_SPECIALIST = 'Financial Planning Specialist';
    private static final String STR_MEMBERTYPE_FORENSIC_ACCOUNTING_SPECIALIST = 'Forensic Accounting Specialist';   // RO4
    private static final String STR_MEMBERTYPE_CPP_HELD = 'Public Practitioner';

       
	public static String  strSelectedMemberTypeOption {get; set;}
	public static Integer intInputLimit {get; set;}
    public static Integer intInputRadius {get; set;}
    public static Decimal decInputLatitude {get; set;}
    public static Decimal decInputLongitude {get; set;}

    @HttpGet
	global static List<CA> doGet() {
        String  vMemberType    = '';
        Integer vLimit         = 0;
        Integer vRadius        = 0;
        Decimal vLatitude      = null;
        Decimal vLongitude     = null;

        if (!String.isBlank(RestContext.request.params.get('MemberType')))
        	vMemberType    = RestContext.request.params.get('MemberType');
        if (!String.isBlank(RestContext.request.params.get('Limit')))
             try {
            		vLimit = Integer.valueOf(RestContext.request.params.get('Limit'));
                 } catch(Exception e) {
                     System.debug('Conversion Error Limit: ' + e.getMessage());
                 }
        
        if (!String.isBlank(RestContext.request.params.get('Radius')))
             try {
            		vRadius = Integer.valueOf(RestContext.request.params.get('Radius'));
                 } catch(Exception e) {
                     System.debug('Conversion Error Limit: ' + e.getMessage());
                 }

        
        if (!String.isBlank(RestContext.request.params.get('Latitude')) &&
            !String.isBlank(RestContext.request.params.get('Longitude'))
           )       
             try {
                 vLatitude   = Decimal.valueOf(RestContext.request.params.get('Latitude'));
                 vLongitude  = Decimal.valueOf(RestContext.request.params.get('Longitude'));
             } catch(Exception e) {
                 System.debug('Conversion Error Lat/Long: ' + e.getMessage());
             }
    
        System.debug('Request Parameters:' + vMemberType + ',' + vLimit + ',' + vLatitude + ',' + vLongitude );     
		return executeSearchCA( vMemberType, vLatitude, vLongitude, vLimit, vRadius);
    }
    
    private static boolean IsValidMemberType(String vMemberType) {
      List<FindCA_API_Member_Type__mdt> members = [Select Label, Country__c, Member_Type__c From FindCA_API_Member_Type__mdt where Member_Type__c = :vMemberType];
      if (members.size()>0) 
          return true;
      else
          return false;
  	 }
    
    
    // List of search results
    public static List<CA> executeSearchCA(String vMemberType, Decimal vLatitude, Decimal vLongitude, Integer vLimit, Integer vRadius )
    {    
        String query;
        Integer outputLimit = 200;	// R02 Limit
        Integer counter = 0;		// R02 Limit
        
        strSelectedMemberTypeOption = vMemberType;
		intInputLimit               = vLimit;
        intInputRadius              = vRadius;
		decInputLatitude			= vLatitude;
		decInputLongitude			= vLongitude;
            
       	// when no Limit and no Radius - defaults to Limit = 20
        if (vRadius==0 && vLimit== 0)
            intInputLimit = 20;
        
        List<CA> listCAs = new List<CA>(); 
        
        if (string.isNotBlank(vMemberType) && IsValidMemberType(vMemberType) )
        	query = getQuery(false);
        else
			query = getQueryAllMemberTypes();
        
        try {
            	List<sObject> sobjList = Database.query(query);
            	for(SObject sobj : sobjList){
                    CA newCA = new CA((Account)sobj, strSelectedMemberTypeOption);
                    // R02 add counter to check if we reached the limit
                    if (counter < outputLimit) {					// R02
                        listCAs.add(newCA);
                        counter += 1;								// R02
                    }												// R02
                    else											// R02
                        break;										// R02  
                }
                System.debug(logginglevel.DEBUG, '##### listCAs: ' + listCAs);
				return listCAs;
		} 
		catch (exception e){
			return null;
		}
	}
    

    // Constructs and returns a query string for All Member Types - Only Geography
    private static String getQueryAllMemberTypes(){
    	
    	String strQuery;
    	Integer index = 0;
  	
		String strSelectedMemberTypeOptionSafe = String.escapeSingleQuotes(strSelectedMemberTypeOption);

    	strQuery = 'SELECT Id, FirstName, Preferred_Name__c, Middle_Name__c, LastName, Display_Name__c, Primary_Employer_Company_Name__c, Affiliated_Branch__c, Affiliated_Branch_Country__c ';
    	strQuery += ', PersonOtherPhone, PersonEmail, Website, Primary_Employer__r.Website';
        strQuery += ', BV__c, SMSF__c, FP_Specialisation__c, Forensic_Accounting_Specialisation__c, Qualified_Auditor__c, QA_Date_of_suspension__c, Insolvency_Practitioner__c, NZAIP_Date_of_suspension__c ';
        strQuery += ', IsPersonAccount, Primary_Employer__c';
        strQuery += ', BillingStreet, BillingCity, BillingStateCode, BillingPostalCode';
        strQuery += ', Primary_Employer__r.BillingAddress, Primary_Employer__r.BillingStreet, Primary_Employer__r.BillingCity, Primary_Employer__r.BillingStateCode, Primary_Employer__r.BillingPostalCode';
        strQuery += ', Primary_Employer__r.BillingLatitude, Primary_Employer__r.BillingLongitude, Primary_Employer__r.BillingGeocodeAccuracy ';        
        strQuery += ', ShippingStreet, ShippingCity, ShippingStateCode, ShippingPostalCode';
        strQuery += ', Primary_Employer__r.ShippingAddress, Primary_Employer__r.ShippingStreet, Primary_Employer__r.ShippingCity, Primary_Employer__r.ShippingStateCode, Primary_Employer__r.ShippingPostalCode';
		strQuery += ', Primary_Employer__r.ShippingLatitude, Primary_Employer__r.ShippingLongitude, Primary_Employer__r.ShippingGeocodeAccuracy ';
        strQuery += ', QA_Conditions_imposed__c';
        strQuery += ', BV_Areas_of_Practice__c, Forensic_Accounting_Areas_of_Practice__c '; 
        strQuery += ', BillingLatitude,	BillingLongitude,	BillingGeocodeAccuracy ';  
        strQuery += ', ShippingLatitude, ShippingLongitude,	ShippingGeocodeAccuracy ';  
              	
    	strQuery += ' FROM Account WHERE';
        strQuery += ' IsPersonAccount = true';
    	strQuery += ' AND Status__c = \'Active\'';
    	strQuery += ' AND Opt_out_of_Find_an_Accountant_register__c = false';
        
        // add All Member Types 
        strQuery += ' AND ( ';     
        strQuery += ' (CPP__c = true AND Find_CA_Opt_In__c = true) OR ';
		strQuery += ' (Qualified_Auditor__c = true AND QA_Date_of_suspension__c = null) OR ';
        strQuery += ' (Insolvency_Practitioner__c = true AND NZAIP_Date_of_suspension__c = null ) OR ';
		strQuery += ' (BV__c = true OR SMSF__c = true OR FP_Specialisation__c = true OR Forensic_Accounting_Specialisation__c = true ) ';   //R04
        strQuery += ' ) '; 
        
        if (decInputLatitude != null && decInputLongitude != null) {
            if (intInputLimit > 0) {
            	strQuery += ' AND Primary_Employer__r.BillingLatitude != null AND Primary_Employer__r.BillingLongitude != null ';
            	strQuery += ' ORDER BY DISTANCE (Primary_Employer__r.BillingAddress,  GEOLOCATION(' + decInputLatitude + ', ' + decInputLongitude + '), \'km\') ';  
                strQuery += ' LIMIT ' + intInputLimit;
            } else if ( intInputRadius > 0 ) {
            	strQuery += ' AND Primary_Employer__r.BillingLatitude != null AND Primary_Employer__r.BillingLongitude != null ';
            	strQuery += ' AND DISTANCE (Primary_Employer__r.BillingAddress,  GEOLOCATION(' + decInputLatitude + ', ' + decInputLongitude + '), \'km\') < ' + intInputRadius ;    
        	} 
        } else
            // stop processing
            strQuery += ' LIMIT = 0 ';
        
        System.debug(logginglevel.INFO, '##### strQuery: ' + strQuery);
        return strQuery;
    }
        
 
    
    // Constructs and returns a query string.
    private static String getQuery(Boolean bCount){
    	
    	String strQuery;
    	Integer index = 0;
    	List<String> listNames = new List<String>();
    	
		String strSelectedMemberTypeOptionSafe = String.escapeSingleQuotes(strSelectedMemberTypeOption);

    	
    	if(bCount){
    		strQuery = 'SELECT COUNT(Id) size';
    	}
    	else{	
			strQuery = 'SELECT Id, FirstName, Preferred_Name__c, Middle_Name__c, LastName, Display_Name__c, Primary_Employer_Company_Name__c, Affiliated_Branch__c, Affiliated_Branch_Country__c ';
    		strQuery += ', PersonOtherPhone, PersonEmail, Website, Primary_Employer__r.Website';
            strQuery += ', BV__c, SMSF__c, FP_Specialisation__c, Forensic_Accounting_Specialisation__c, Qualified_Auditor__c, QA_Date_of_suspension__c, Insolvency_Practitioner__c, NZAIP_Date_of_suspension__c ';            
            strQuery += ', IsPersonAccount, Primary_Employer__c';
            strQuery += ', BillingStreet, BillingCity, BillingStateCode, BillingPostalCode';
            strQuery += ', Primary_Employer__r.BillingAddress, Primary_Employer__r.BillingStreet, Primary_Employer__r.BillingCity, Primary_Employer__r.BillingStateCode, Primary_Employer__r.BillingPostalCode';
            strQuery += ', Primary_Employer__r.BillingLatitude, Primary_Employer__r.BillingLongitude, Primary_Employer__r.BillingGeocodeAccuracy ';
            strQuery += ', ShippingStreet, ShippingCity, ShippingStateCode, ShippingPostalCode';
            strQuery += ', Primary_Employer__r.ShippingAddress, Primary_Employer__r.ShippingStreet, Primary_Employer__r.ShippingCity, Primary_Employer__r.ShippingStateCode, Primary_Employer__r.ShippingPostalCode';
			strQuery += ', Primary_Employer__r.ShippingLatitude, Primary_Employer__r.ShippingLongitude, Primary_Employer__r.ShippingGeocodeAccuracy ';
            strQuery += ', QA_Conditions_imposed__c';
            strQuery += ', BV_Areas_of_Practice__c, Forensic_Accounting_Areas_of_Practice__c ';
            strQuery += ', BillingLatitude,	BillingLongitude,	BillingGeocodeAccuracy ';  
            strQuery += ', ShippingLatitude, ShippingLongitude,	ShippingGeocodeAccuracy ';  
    	}
    		
    	strQuery += ' FROM Account WHERE';
    	strQuery += ' Status__c = \'Active\'';
    	strQuery += ' AND Opt_out_of_Find_an_Accountant_register__c = false';
        	
    	if(strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_QUALIFIED_AUDITORS){
	        strQuery += ' AND ((IsPersonAccount = true AND Membership_Type__c = \'Member\') OR IsPersonAccount = false)';
	        strQuery += ' AND Qualified_Auditor__c = true';
	        strQuery += ' AND QA_Date_of_suspension__c = null';
	        strQuery += ' AND QA_Date_of_de_recognition__c = null';
    	}
    	else if(strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_INSOLVENCY_PRACTICE){
    		strQuery += ' AND Membership_Type__c IN (\'Member\', \'NZAIP\', \'NMP\')';
	        strQuery += ' AND IsPersonAccount = true';
	        strQuery += ' AND Insolvency_Practitioner__c = true';
	        strQuery += ' AND NZAIP_Date_of_suspension__c = null';
	        strQuery += ' AND NZAIP_Date_of_de_recognition__c = null';
    	}
    	else if(strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_SMSF_SPECIALIST
            || strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_BUSINESS_VALUATION_SPECIALIST
            || strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_FINANCIAL_PLANNING_SPECIALIST
            || strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_FORENSIC_ACCOUNTING_SPECIALIST     // R04
        ){
        	if(Label.FIND_CA_OPT_IN.equalsIgnoreCase('true')) // if this feature is 'on' include opt in as a condition, otherwise ignore
        		strQuery += ' AND Find_CA_Opt_In__c = true';
        	strQuery += ' AND Membership_Type__c = \'Member\'';
	        strQuery += ' AND IsPersonAccount = true';
	        strQuery += ' AND (NOT Financial_Category__c LIKE \'%Retired%\')';
	        if(strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_SMSF_SPECIALIST)
	            strQuery += ' AND SMSF__c = true';
	        else if(strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_BUSINESS_VALUATION_SPECIALIST)
	            strQuery += ' AND BV__c = true';
	        else if(strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_FINANCIAL_PLANNING_SPECIALIST)
	            strQuery += ' AND FP_Specialisation__c = true';
	        else if(strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_FORENSIC_ACCOUNTING_SPECIALIST)  // R04
	            strQuery += ' AND Forensic_Accounting_Specialisation__c = true';                       // R04
        }
        else if(strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_CPP_HELD){
        	strQuery += ' AND Find_CA_Opt_In__c = true';
        	strQuery += ' AND Membership_Type__c = \'Member\'';
	        strQuery += ' AND IsPersonAccount = true';
	        strQuery += ' AND CPP__c = true';
        }
           
        if (decInputLatitude != null && decInputLongitude != null) {
            if (intInputLimit > 0) {
            	strQuery += ' AND Primary_Employer__r.BillingLatitude != null AND Primary_Employer__r.BillingLongitude != null ';
            	strQuery += ' ORDER BY DISTANCE (Primary_Employer__r.BillingAddress,  GEOLOCATION(' + decInputLatitude + ', ' + decInputLongitude + '), \'km\') ';  
                strQuery += ' LIMIT ' + intInputLimit;
            } else if ( intInputRadius > 0 ) {
            	strQuery += ' AND Primary_Employer__r.BillingLatitude != null AND Primary_Employer__r.BillingLongitude != null ';
            	strQuery += ' AND DISTANCE (Primary_Employer__r.BillingAddress,  GEOLOCATION(' + decInputLatitude + ', ' + decInputLongitude + '), \'km\') < ' + intInputRadius ;    
        	}
        } else
            // stop processing
            strQuery += ' LIMIT = 0 ';
        
		System.debug(logginglevel.INFO, '##### strQuery: ' + strQuery);
        return strQuery;
    }
    
    
    
    // ====== INTERNAL CLASS ======     
    global class CA{
    	
    	//private Account act {get; set;}
    	private String strSelectedMemberType;
        
        global String Name;
        global String First;
        global String Preferred;
        global String Middle;
        global String Last;
        global String Company;
        global String BusinessAddress;
        global String Phone;
        global String Email;
        global String CompanyWebsite;
        global String SpecialConditions;
        global String Specialisation;
        global String Specialties;

        global Double Latitude;
        global Double Longitude;
        global String GeocodeAccuracy;
        
		public CA(Account account, String strSelectedMemberTypeOption) {
            
             System.debug(' Account' + account);
			//act = account;
			strSelectedMemberType = strSelectedMemberTypeOption;
            
            Name = account.Display_Name__c;
            First = account.FirstName;
			Preferred = account.Preferred_Name__c;
			Middle = account.Middle_Name__c;
			Last = account.LastName;
            Company = account.Primary_Employer_Company_Name__c;
            BusinessAddress = getBusinessAddress(account); 
            
            // R03 Phone = ((strSelectedMemberType != STR_MEMBERTYPE_QUALIFIED_AUDITORS)? account.PersonOtherPhone : null) ;
            // R03 Email = ((strSelectedMemberType != STR_MEMBERTYPE_QUALIFIED_AUDITORS)? account.PersonEmail : null) ;
            Phone = ((strSelectedMemberType == STR_MEMBERTYPE_QUALIFIED_AUDITORS  && account.isPersonAccount == FALSE)? account.Phone : account.PersonOtherPhone) ;
            Email = account.PersonEmail;
            SpecialConditions = (string.isNotBlank(account.QA_Conditions_imposed__c) ? account.QA_Conditions_imposed__c : null) ;
            
            CompanyWebsite = getCompanyWebsite(account);
            Specialisation = getSpecialisation(account);
            Specialties    = getSpecialties(account);
            
            Latitude        = (account.Primary_Employer__r.BillingLatitude!=null  ? account.Primary_Employer__r.BillingLatitude        : account.Primary_Employer__r.ShippingLatitude) ;
            Longitude       = (account.Primary_Employer__r.BillingLatitude!=null  ? account.Primary_Employer__r.BillingLongitude       : account.Primary_Employer__r.ShippingLongitude) ;
            GeocodeAccuracy = (account.Primary_Employer__r.BillingLatitude!=null  ? account.Primary_Employer__r.BillingGeocodeAccuracy : account.Primary_Employer__r.ShippingGeocodeAccuracy) ;

		}
        
               
        public String getBusinessAddress(Account act){
                String strBusinessAddress = '';
                if(act.IsPersonAccount){
                    if(String.isNotBlank(act.Primary_Employer__c)){
                        if(String.isNotBlank(act.Primary_Employer__r.BillingCity)){
                            strBusinessAddress = capInitialLetters(act.Primary_Employer__r.BillingStreet);
                            strBusinessAddress += ', ' + capInitialLetters(act.Primary_Employer__r.BillingCity);
                            if(act.Affiliated_Branch_Country__c == 'Australia'){
                                if (string.isNotBlank(act.Primary_Employer__r.BillingStateCode))
                                	strBusinessAddress += ', ' + act.Primary_Employer__r.BillingStateCode.toUpperCase();
                            }
                            strBusinessAddress += ', ' + act.Primary_Employer__r.BillingPostalCode;
                        }
                        else if(String.isNotBlank(act.Primary_Employer__r.ShippingCity)){
                            strBusinessAddress = capInitialLetters(act.Primary_Employer__r.ShippingStreet);
                            strBusinessAddress += ', ' + capInitialLetters(act.Primary_Employer__r.ShippingCity);
                            if(act.Affiliated_Branch_Country__c == 'Australia')
                                strBusinessAddress += ', ' + act.Primary_Employer__r.ShippingStateCode.toUpperCase();
                            strBusinessAddress += ', ' + act.Primary_Employer__r.ShippingPostalCode;
                    	}
                    }
                }
                else{
                    if(String.isNotBlank(act.BillingCity)){
                        strBusinessAddress = capInitialLetters(act.BillingStreet);
                        strBusinessAddress += ', ' + capInitialLetters(act.BillingCity);
                        if(act.Affiliated_Branch_Country__c == 'Australia') {
                            if (string.isNotBlank(act.BillingStateCode))
                            	strBusinessAddress += ', ' + act.BillingStateCode.toUpperCase();
                        }
                        strBusinessAddress += ', ' + act.BillingPostalCode;
                    }
                    else if(String.isNotBlank(act.ShippingCity)){
                        strBusinessAddress = capInitialLetters(act.ShippingStreet);
                        strBusinessAddress += ', ' + capInitialLetters(act.ShippingCity);
                        if(act.Affiliated_Branch_Country__c == 'Australia')
                            strBusinessAddress += ', ' + act.ShippingStateCode.toUpperCase();
                        strBusinessAddress += ', ' + act.ShippingPostalCode;
                    }
                }
                return strBusinessAddress;
        }

        
        public String getCompanyWebsite(Account act){
            if(strSelectedMemberType != STR_MEMBERTYPE_QUALIFIED_AUDITORS){
                if(act.IsPersonAccount){
                    if(string.isNotBlank(act.Primary_Employer__r.Website)){
                        if(!act.Primary_Employer__r.Website.startsWithignoreCase('http://') && !act.Primary_Employer__r.Website.startsWithignoreCase('https://')){
                            return 'http://' + act.Primary_Employer__r.Website;        
                        }
                    }
                    return act.Primary_Employer__r.Website;
                }
                else{
                    if(string.isNotBlank(act.Website)){
                        if(!act.Website.startsWithignoreCase('http://') && !act.Website.startsWithignoreCase('https://')){
                            return 'http://' + act.Website;        
                        }
                    }
                    return act.Website;
                }
            }
            else{
                return null;
            }
        }
        
        
        public String getSpecialisation(Account act) { 
       		String vListSpec = '';
           
            if (act.BV__c == true )
                vListSpec += (string.isNotBlank(vListSpec) ? ', Business Valuation Specialist' : 'Business Valuation Specialist') ;
            if (act.SMSF__c == true )
                vListSpec += (string.isNotBlank(vListSpec) ? ', SMSF Specialist' : ' SMSF Specialist') ;
            if (act.FP_Specialisation__c == true )
                vListSpec += (string.isNotBlank(vListSpec) ? ', Financial Planning Specialist' : 'Financial Planning Specialist') ;
            if (act.Forensic_Accounting_Specialisation__c == true )                                                                      // R04
                vListSpec += (string.isNotBlank(vListSpec) ? ', Forensic Accounting Specialist' : 'Forensic Accounting Specialist') ;    // R04
            if (act.Qualified_Auditor__c == true && act.QA_Date_of_suspension__c == null)
                vListSpec += (string.isNotBlank(vListSpec) ? ', Qualified Auditor' : ' Qualified Auditor') ; 
            if (act.Insolvency_Practitioner__c == true && act.NZAIP_Date_of_suspension__c == null)
                vListSpec += (string.isNotBlank(vListSpec) ? ', Insolvency Practitioner' : 'Insolvency Practitioner') ;
   			
            return vListSpec;   
        }
        
        public String getSpecialties(Account act) {     
            // return (string.isNotBlank(act.Accounting_Specialties__c) ? act.Accounting_Specialties__c : null) ;
            String strOutput = '';
            Boolean first = true;
            
            // Only one of the below 3 options can happen at a time
            
            // BV Areas of Practice for BV Searches
            // removed need for selection: if (String.isNotBlank(act.BV_Areas_of_Practice__c) && strSelectedMemberType == STR_MEMBERTYPE_BUSINESS_VALUATION_SPECIALIST ) {
            if (String.isNotBlank(act.BV_Areas_of_Practice__c) && act.BV__c == true && strSelectedMemberType == STR_MEMBERTYPE_BUSINESS_VALUATION_SPECIALIST  ) {
                for (String entry : act.BV_Areas_of_Practice__c.split(';') ) {
                    if (first) {
                        strOutput += entry;
                        first = false;
                    }
                    else
                        strOutput += ', ' + entry;
                }
            }
           
            // R04 Forensic Areas of Practice for Forensic Searches
            if (String.isNotBlank(act.Forensic_Accounting_Areas_of_Practice__c) && act.Forensic_Accounting_Specialisation__c == true  && strSelectedMemberType == STR_MEMBERTYPE_FORENSIC_ACCOUNTING_SPECIALIST ) {
                for (String entry : act.Forensic_Accounting_Areas_of_Practice__c.split(';') ) {
                    if (first) {
                        strOutput += entry;
                        first = false;
                    }
                    else
                        strOutput += ', ' + entry;
                }
            }
            

/**
            // SMSF Areas of Practice for SMSF Searches 
            if (String.isNotBlank(act.SMSF_Areas_of_Practice__c) && strSelectedMemberType == STR_MEMBERTYPE_SMSF_SPECIALIST ) {
                for (String entry : act.SMSF_Areas_of_Practice__c.split(';') ) {
                    if (first) {
                        strOutput += entry;
                        first = false;
                    }
                    else
                        strOutput += ', ' + entry;
                }
            }
            
**/
            
            return strOutput;
        }

        
    private String capInitialLetters(String strInput){
            String strOutput = '';
            //for(String str : strInput.split('\\W+')){
            for(String str : strInput.split('\\s')){
                strOutput += str.toLowerCase().capitalize() + ' ';
            }
            return strOutput.substring(0, strOutput.length() - 1);
        }
    }
}