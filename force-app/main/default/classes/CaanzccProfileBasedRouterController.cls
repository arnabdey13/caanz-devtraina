/*------------------------------------------------------------------------------------
Author:        Sumit Gupta
Company:       Salesforce
Description:   Profile based router component controller

History
Date            Author             Comments
--------------------------------------------------------------------------------------
02-05-2018     Sumit Gupta       	Initial Release
------------------------------------------------------------------------------------*/
public with sharing class CaanzccProfileBasedRouterController {

	@AuraEnabled
	public static String getCurrentUserProfile() {
		return [Select Name from Profile where Id =: UserInfo.getProfileId()].Name;
	}
}