public without sharing class WithoutSharing {
	// Called from ApplicationWizardController.cls
	public static List<Specified_Practical_Experience__c> getSpecifiedPracticalExperienceList(Id ApplicationId){
		return [Select 
                Name
                , s.Start_Date__c
                , s.Hours_per_week__c
                , s.Finish_date__c
                , s.Approved_Training_Employer__c
                , s.Approved_Training_Employer__r.Name
                , s.Application__c
                , s.Mentors_NZICA_ID__c
             From Specified_Practical_Experience__c s where s.Application__c=:ApplicationId order by CreatedDate desc];
	}
	
	// Called from ApplicationWizardController.cls
	public static void upsertSpecifiedPracticalExperienceList(
	List<Specified_Practical_Experience__c> SpecifiedPracticalExperience_List
	){
		upsert SpecifiedPracticalExperience_List;
	}
	
	// Called from PortalSelfRegistration.cls
	public static void updateAccountOnRegistration(List<Account> Account_List){
		update Account_List;
	}
	
	// Called from ManageMembership.cls & ApplicationWizardController.cls
	public static List<Account> queryAccountObject(String queryString){
		return Database.query( queryString );
	}
	
	// Called from ManageMembership.cls
	public static List<Account> queryAccountObject(String queryString, Set<String> Id_Set){
		// Can expand this if "order by" or "Limit" are required: Closing statements
		return Database.query( queryString + ':Id_Set');
	}
	
	// Called from ManageMembership.cls
	public static List<Database.Upsertresult> upsertEmploymentHistoryList( List<Employment_History__c> EmploymentHistory_List ){
		return Database.upsert(EmploymentHistory_List, false);
	}
}