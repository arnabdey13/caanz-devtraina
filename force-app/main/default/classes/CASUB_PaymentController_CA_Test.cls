/*------------------------------------------------------------------------------------
Author:        Mayukhman
Company:       Tech Mahindra
Description:   Test class for most of the Controllers - CASUB_PaymentController_CA 

History
Date            Author             Comments
--------------------------------------------------------------------------------------
07-05-2019     Mayukhman         Initial Release
------------------------------------------------------------------------------------*/
@isTest 
public class CASUB_PaymentController_CA_Test {
    private static User currentUser;
    private static Account currentAccount;
    private static Order currentOrder;
    private static OrderItem currentOrderItem;
    private static Pricebook2 currentPricebook2;
    private static PricebookEntry currentPricebookEntry;
    private static Product2 currentProduct;
    private static Subscription__c currentSubscription;
    private static Subscription__c updatedSubscription;
    private static Questionnaire_Response__c QuestionniareResponse;
    
    static {
        currentAccount = CASUB_PaymentController_CA_Test.createFullMemberAccount();
        currentAccount.Affiliated_Branch_Country__c = 'Australia';
        insert currentAccount;
        
        currentSubscription = CASUB_PaymentController_CA_Test.createSubscriptionRecord();
        updatedSubscription = CASUB_PaymentController_CA_Test.updateSubscriptionRecord();
        currentProduct = CASUB_PaymentController_CA_Test.createProductRecord();
        currentPricebook2 = CASUB_PaymentController_CA_Test.createPricebook2();
        currentPricebookEntry = CASUB_PaymentController_CA_Test.createPricebookRecord();
        currentOrder = CASUB_PaymentController_CA_Test.createOrderRecord();
        currentOrderItem = CASUB_PaymentController_CA_Test.createOrderItemRecord();
        currentOrder.Status = 'Activated';
        update currentOrder;
      //  currentSubscription.Contact_Details_Status__c='completed';
      //  update currentSubscription;
        
    }
    
    private static User getCurrentUser(){
        Test.startTest(); Test.stopTest(); // Future method needs to run to create the user.
        return currentUser = [SELECT Name FROM User WHERE AccountId =: currentAccount.Id];
    }
    
    // <Create FullMemberAccount>
    public static Account createFullMemberAccount(){
        Account FullMemberAccountObject = new Account();
        FullMemberAccountObject.Salutation = 'Mr.';
        FullMemberAccountObject.FirstName = 'TestFName';
        FullMemberAccountObject.PersonEmail= 'Full_Member'+ CASUB_PaymentController_CA_Test.getRandomNumber(910) +'@gmail.com';
        FullMemberAccountObject.LastName = 'Test'+ CASUB_PaymentController_CA_Test.getRandomNumber(910);
        FullMemberAccountObject.RecordTypeId = RecordTypeCache.getId(FullMemberAccountObject, 'Full_Member');
        FullMemberAccountObject.Communication_Preference__c= 'Home Phone';
        FullMemberAccountObject.PersonHomePhone= '1234';
        FullMemberAccountObject.PersonOtherStreet= '83 Saggers Road';
        FullMemberAccountObject.PersonOtherCity='JITARNING';
        FullMemberAccountObject.PersonOtherState='Western Australia';
        FullMemberAccountObject.PersonOtherCountry='Australia';
        FullMemberAccountObject.PersonOtherPostalCode='6365';  
        return FullMemberAccountObject;
    }
    
    public static product2 createProductRecord(){        
        //insert a new product
        Product2 p = new Product2();
        p.Name = ' Test Product ';
        p.Description='Test Product Entry 1';
        p.productCode = 'ABC';
        p.isActive = true;
        insert p;
        return p;
    }
    public static Pricebook2 createPricebook2(){        
        //define the standart price for the product
        Pricebook2 standardPb = new Pricebook2(
            Id = Test.getStandardPricebookId(),
            IsActive = true
        );
        return standardPb;
    }
    public static PricebookEntry createPricebookRecord(){
        PricebookEntry standardPrice = new PricebookEntry();
        standardPrice.Pricebook2Id = currentPricebook2.Id;
        standardPrice.Product2Id = currentProduct.Id;
        standardPrice.UnitPrice = 1;
        standardPrice.IsActive = true;
        standardPrice.UseStandardPrice = false;
        insert standardPrice;        
        return standardPrice;
    }
    
    public static Order createOrderRecord(){
        Id RecordTypeIdOrder = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        Order ord = new Order();
        ord.Name = 'Test Order ';
        ord.Status = 'Draft';
        ord.EffectiveDate = system.today();
        ord.EndDate = system.today() + 4;
        ord.AccountId = currentAccount.id;
        ord.Recordtypeid = RecordTypeIdOrder;
        ord.Pricebook2Id = currentPricebook2.Id;
        insert ord;
        return ord;
    }
    public static OrderItem createOrderItemRecord(){
        OrderItem ordItem = new OrderItem();
        ordItem.OrderId = currentOrder.id;
        ordItem.Quantity = 24;
        ordItem.UnitPrice = 240;
        ordItem.Product2id = currentProduct.id;
        ordItem.PricebookEntryId=currentPricebookEntry.id;
        insert ordItem;
        return ordItem;
    }
    
    public static Subscription__c createSubscriptionRecord(){
        Subscription__c subscription = new Subscription__c();
        subscription.Account__c = currentAccount.Id;
        subscription.Contact_Details_Status__c = 'Pending';
        subscription.Obligation_Status__c = 'Pending';
        subscription.Sales_Order_Status__c = 'Pending';
        subscription.Year__c = '2019';
        insert subscription;
        return subscription;
    }
    
    public static Subscription__c updateSubscriptionRecord(){
        Subscription__c newSubscription = currentSubscription;
        Subscription__c updateSubscription = new Subscription__c(Id=newSubscription.Id);
        updateSubscription.Sales_Order_Status__c = 'completed';
        update updateSubscription;
        return updateSubscription;
    } 
    
     // <Create Questionnaire Response>
    public static Questionnaire_Response__c createQuestionnaireResponseRecord(){
        Questionnaire_Response__c QuestionnaireResponse = new Questionnaire_Response__c();
        QuestionnaireResponse.Question_1__c= 'Question1';
        QuestionnaireResponse.Question_2__c='Question1';
        QuestionnaireResponse.Question_3__c='Question1';
        QuestionnaireResponse.Question_4__c='Question1';
        QuestionnaireResponse.Question_5__c='Question1';
        QuestionnaireResponse.Question_6__c='Question1';
        QuestionnaireResponse.Question_7__c='Question1';
        QuestionnaireResponse.Question_8__c='Question1';
        QuestionnaireResponse.Question_9__c='Question1';
        QuestionnaireResponse.Question_10__c='Question1';
        QuestionnaireResponse.Question_11__c='Question1';
        QuestionnaireResponse.Question_12__c='Question1';
        QuestionnaireResponse.Question_13__c='Question1';
        QuestionnaireResponse.Question_14__c='Question1';
        QuestionnaireResponse.Question_15__c='Question1';
        QuestionnaireResponse.Question_16__c='Question1';
        QuestionnaireResponse.Question_17__c='Question1';
        QuestionnaireResponse.Question_18__c='Question1';
        QuestionnaireResponse.Question_19__c='Question1';
        QuestionnaireResponse.Question_20__c='Question1';
        QuestionnaireResponse.Question_21__c='Question1';
        QuestionnaireResponse.Question_22__c='Question1';
        QuestionnaireResponse.Question_23__c='Question1';
        QuestionnaireResponse.Question_24__c='Question1';
        QuestionnaireResponse.Question_25__c='Question1';
        QuestionnaireResponse.Question_26__c='Question1';
        QuestionnaireResponse.Question_27__c='Question1';
        QuestionnaireResponse.Question_28__c='Question1';
        QuestionnaireResponse.Question_29__c='Question1';
        QuestionnaireResponse.Question_30__c='Question1'; 
        QuestionnaireResponse.Account__c=currentAccount.Id;
        QuestionnaireResponse.Answer_1__c='Answer1';
        QuestionnaireResponse.Answer_2__c='Answer1';
        QuestionnaireResponse.Answer_3__c='Answer1';
        QuestionnaireResponse.Answer_4__c='Answer1';
        QuestionnaireResponse.Answer_5__c='Answer1';
        QuestionnaireResponse.Answer_6__c='Answer1';
        QuestionnaireResponse.Answer_7__c='Answer1';
        QuestionnaireResponse.Answer_8__c='Answer1';
        QuestionnaireResponse.Answer_9__c='Answer1';
        QuestionnaireResponse.Answer_10__c='Answer1';
        QuestionnaireResponse.Answer_11__c='Answer1';
        QuestionnaireResponse.Answer_12__c='Answer1';
        QuestionnaireResponse.Answer_13__c='Answer1';
        QuestionnaireResponse.Answer_14__c='Answer1';
        QuestionnaireResponse.Answer_15__c='Answer1';
        QuestionnaireResponse.Answer_16__c='Answer1';
        QuestionnaireResponse.Answer_17__c='Answer1';
        QuestionnaireResponse.Answer_18__c='Answer1';
        QuestionnaireResponse.Answer_19__c='Answer1';
        QuestionnaireResponse.Answer_20__c='Answer1';
        QuestionnaireResponse.Answer_21__c='Answer1';
        QuestionnaireResponse.Answer_22__c='Answer1';
        QuestionnaireResponse.Answer_23__c='Answer1';
        QuestionnaireResponse.Privacy_Policy__c=true;
        QuestionnaireResponse.Name='Test Questionnaire Response';
        //QuestionnaireResponse.CreatedDate=System.today();
        QuestionnaireResponse.Residential_Country__c = 'New Zealand';
        QuestionnaireResponse.Year__c = '2019';
        return QuestionnaireResponse;
    }
    
    public static Integer getRandomNumber(Integer size){
          Double d = math.random() * size;
          return d.intValue();
     }
    
    @isTest static void test_CASUB_PaymentController() {
        System.runAs(getCurrentUser()) {
            CASUB_Payment_Controller getController = new CASUB_Payment_Controller();
            getController = CASUB_Payment_Controller.getUserRelatedOrder();
            System.assertNotEquals(getController, null);
        }
    }
    @isTest static void test_CASUB_PaymentControllerAsSystemAdmin() {
            QuestionniareResponse = CASUB_PaymentController_CA_Test.createQuestionnaireResponseRecord();
            insert QuestionniareResponse;
            CASUB_Payment_Controller.updateSubscriptionPayment(updatedSubscription, currentAccount.Id);
        CASUB_Payment_Controller.getEncryptedParam('', '');
    }
    
}