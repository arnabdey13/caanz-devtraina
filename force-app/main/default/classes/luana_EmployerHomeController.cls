/**
    Change Histroy:
    LCA-487 07/07/2016 - WDCi Lean: update authentication redirection to custom login page
**/

public class luana_EmployerHomeController {
    
    public Id custCommConId {set; get;}
    public Id custCommAccId {set; get;}

    public String selectedEmployer {get; set;}
    public List<Payment_Token__c> paymentTokens {get; set;}
    
    //public Map<Id, List<Id>> emplomentMembersMap = new Map<id, List<Id>>(); Lean 21/04 - not being used, not sure why
    
    public Date dateFrom {set; get;}
    public Date dateTo {set; get;}
    public String memberName {set; get;}
    public String memberId {set; get;}
    public String courseName {set; get;}
    
    boolean resetPage = true;
    
    private String ptURLParamValue = 'ptid';
    public String selectedPTId;
    public String validPaymentTokenSelectedId;
    
    List<Employment_History__c> employmentHistories;
    
    public luana_EmployerHomeController() {
        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId()); 
       
        //custCommConId = '003p0000003PlpE';
        //custCommAccId = '001p0000004NC69AAG';
        

        custCommConId = commUserUtil.custCommConId;       
        custCommAccId = commUserUtil.custCommAccId;  
        
        paymentTokens = new List<Payment_Token__c>();
        
        employmentHistories = luana_EmploymentUtil.getUserActiveEmploymentHistory(custCommAccId);
        
        selectedEmployer = null;
        
        if(ApexPages.currentPage().getParameters().containsKey(ptURLParamValue)){
            selectedPTId = ApexPages.currentPage().getParameters().get(ptURLParamValue);
            
            Map<Id, String> employmentHistStatusMap = new Map<Id, String>();
            for(Employment_History__c eh: employmentHistories){
                employmentHistStatusMap.put(eh.Employer__c, eh.Status__c);
            }
            for(Payment_Token__c pt: [select Id, Employer__c, Employment_Token__c, Ext_Id__c from Payment_Token__c Where Id =: selectedPTId]){
                if(employmentHistStatusMap.containsKey(pt.Employer__c)){
                    if(employmentHistStatusMap.get(pt.Employer__c) == 'Current'){
                        selectedEmployer = pt.Employer__c;
                        validPaymentTokenSelectedId = pt.Id;
                        
                        currentPage = 1;
                    }
                }
            }
        }
        
        resetPage = true;
        
    }
    
    public PageReference forwardToAuthPage(){
        if(UserInfo.getUserType() == 'Guest'){
            //Fix for issue LCA-327 & LCA-328
            //return new PageReference('/CommunitiesLogin');
            return new PageReference(URL.getSalesforceBaseUrl().toExternalForm()+'/employer/luana_Login?startURL=/employer/luana_EmployerHome'); //LCA-487
        }
        else{
            return null;
        }
    }
    
    public List<SelectOption> getEmployerOptions(){
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        
        //KH change on 5 April 2016 - Include Status checking 
        for(Employment_History__c empHistory : employmentHistories){
            options.add(new SelectOption(empHistory.Employer__c, empHistory.Employer__r.Name));
        }
        
        /* Lean 21/04 - not being used, not sure why
        for(Employment_History__c eh: employerHistories){
            if(emplomentMembersMap.containsKey(eh.Employer__c)){
                emplomentMembersMap.get(eh.Employer__c).add(eh.Member__c);
            }else{
                List<Id> newIds = new List<Id>();
                newIds.add(eh.Member__c);
                emplomentMembersMap.put(eh.Employer__c, newIds);
            }
        }*/
        
        return options;
    }
    
    //Change for issue LCA-193, to set the setCon variable
    public boolean hasRelatedBulkCode {public get; private set;}
    
    public PageReference doSearch(){
        setCon = null;
        
        if(resetPage){
            currentPage = 1;
        }
        
        if(selectedEmployer == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please select an employer.'));
            hasRelatedBulkCode= false;
        }else{
            hasRelatedBulkCode = true;
        }
        
        return null;
    }
    
    /*
    public PageReference getRelatedBulkCode(){

        paymentTokens = [Select Id, Code__c, revoke__c, Course__c, Course__r.Name, Course__r.LuanaSMS__Status__c, Contact__r.Account.Member_ID__c, Date_Used__c, Description__c, Employer__c, Employer__r.Name, Expiry_Date__c, Contact__c, Employer__r.BillingCity, 
                                Contact__r.Name, Student_Program__c, Student_Program__r.Name from Payment_Token__c Where Employer__c =: selectedEmployer Order by Course__r.LuanaSMS__Start_Date__c LIMIT 500];
        return null;
    }*/
    
    
    public String revokedBulkCodeId {get; set;}
    
    public PageReference revokePaymentToken(){
        
        try{
            List<Payment_Token__c> paymentTokensUpdateList = new List<Payment_Token__c>();
            for(Payment_Token__c pt: (List<Payment_Token__c>)setCon.getRecords()){
                if(pt.Id == revokedBulkCodeId){
                    Payment_Token__c ptUpdate = new Payment_Token__c(Id = pt.Id);
                    ptUpdate.revoke__c = true;
                    
                    paymentTokensUpdateList.add(ptUpdate);
                }
            }
            
            update paymentTokensUpdateList;
            
            resetPage = false;
            PageReference pageRef = doSearch();
            resetPage = true;
            
            return pageRef;
            
        }catch(Exception exp){
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Unexpected error found please contact support. ' + exp.getMessage()));
        }
        return null;
    } 
    
    //KH chnage on issue LCA-193, include pagination
    public List<Payment_Token__c> getRelatedBulkCodes(){
        try{
            List<Payment_Token__c> pts = new List<Payment_Token__c>();
            if(setCon != null){
                for(Payment_Token__c pt: (List<Payment_Token__c>)setCon.getRecords()){
                    pts.add(pt);
                }
            }
            return pts;
        }catch(Exception exp){
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Unexpected error found please contact support. ' + exp.getMessage()));
        }
        return null;
    }
        
    public Integer noOfRecords{get; set;}
    public Integer size{get;set;}
    public Integer totalNumOfPage {get; set;}
    Integer currentPage;
    
    public ApexPages.StandardSetController setCon {
        get{
            if(selectedEmployer != null){
                if(setCon == null){
                    
                    Luana_Extension_Settings__c noPerPage = Luana_Extension_Settings__c.getValues('Default_Pagination_No_Per_Page');
                    Integer numberPerPageSize = Integer.valueOf(noPerPage.value__c);
                    size = numberPerPageSize;
                    
                    string queryString = getFilterQuery();
                    
                    setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
                    
                    setCon.setPageSize(size);
                    noOfRecords = setCon.getResultSize();
                    
                    setCon.setPageNumber(currentPage);
                    
                    Decimal block = decimal.valueOf(noOfRecords).divide(integer.valueOf(size), 2, System.RoundingMode.UP); 
       
                    totalNumOfPage = integer.valueOf(block.round(System.RoundingMode.CEILING));
                    
                      
                }
            }
            return setCon;
        }
        set;
    }
    
    public void next(){
        
        if(setCon.getHasNext()){
            setCon.next();
        }
        
        currentPage = setCon.getPageNumber();
    }
    
    public void previous(){
        
        if(setCon.getHasPrevious()){
            setCon.previous();
        }
        
        currentPage = setCon.getPageNumber();
    }
    
    public void first(){
        
        setCon.first();
        currentPage = setCon.getPageNumber();
    }
    
    public void last(){
        
        setCon.last();
        currentPage = setCon.getPageNumber();

    }
        
    public String getFilterQuery(){
        
        String whereStr = '';
        
        Map<String, String> filterMap = new Map<String, String>();

        filterMap.put('Employer__c', 'Employer__c = ');
        filterMap.put('Contact__r.Account.Member_ID__c', 'Contact__r.Account.Member_ID__c like ');
        filterMap.put('Contact__r.Name', 'Contact__r.Name like ');
        filterMap.put('Course__r.Name', 'Course__r.Name like ');
        filterMap.put('Id', 'Id = ');
        
        Map<String, String> filterValue = new Map<String, String>();
                   
        if(selectedEmployer != null && selectedEmployer != ''){
            filterValue.put('Employer__c', '\'' + selectedEmployer + '\'');
        }else{
            filterValue.put('Employer__c', null);
        }
        if(memberName != null && memberName != ''){
            filterValue.put('Contact__r.Name', '\'%'+String.escapeSingleQuotes(memberName)+'%\'');
        }else{
            filterValue.put('Contact__r.Name', null);
        }
        
        if(memberId !=null && memberId != ''){
            filterValue.put('Contact__r.Account.Member_ID__c', '\'%'+String.escapeSingleQuotes(memberId)+'%\'');
        }else{
            filterValue.put('Contact__r.Account.Member_ID__c', null);
        }
        
        if(courseName !=null && courseName != ''){
            filterValue.put('Course__r.Name', '\'%'+String.escapeSingleQuotes(courseName)+'%\'');
        }else{
            filterValue.put('Course__r.Name', null);
        }
        
        if(validPaymentTokenSelectedId != null && validPaymentTokenSelectedId != ''){
            filterValue.put('Id', '\''+validPaymentTokenSelectedId+'\'');
        }else{
            filterValue.put('Id', null);
        }
        /**/
        
        
        String dateSearch;
        if(dateFrom != null && dateTo == null){
            String fDate = datetime.newInstance(dateFrom.year(), dateFrom.month(),dateFrom.day()).format('yyyy-MM-dd');
            dateSearch = '(Date_Used__c >= ' + fDate + ')';
        } else if(dateTo != null && dateFrom == null){
            String tDate = datetime.newInstance(dateTo.year(), dateTo.month(),dateTo.day()).format('yyyy-MM-dd');
            dateSearch = '(Date_Used__c <= ' + tDate + ')';
        }else if(dateTo != null && dateFrom != null){
            String fDate = datetime.newInstance(dateFrom.year(), dateFrom.month(),dateFrom.day()).format('yyyy-MM-dd');
            String tDate = datetime.newInstance(dateTo.year(), dateTo.month(),dateTo.day()).format('yyyy-MM-dd');
            dateSearch = '(Date_Used__c >= ' + fDate + ' and Date_Used__c <= ' + tDate + ')';
        }else{
            dateSearch = null;
        }
        
        String filterStr = prepareFilterStr(filterMap, filterValue, dateSearch);
        
        string queryStr = 'Select Id, Eligible_For_Revoke__c, Code__c, revoke__c, Course__c, Student_Program__r.Opt_In__c, Course__r.Name, Course__r.LuanaSMS__Status__c, Contact__r.Account.Member_ID__c, '+
                                            'Date_Used__c, Description__c, Employer__c, Employer__r.Name, Expiry_Date__c, Contact__c, Employer__r.BillingCity, '+
                                            'Contact__r.Name, Student_Program__c, Student_Program__r.Name, Ext_Id__c from Payment_Token__c ' + filterStr + ' Order by Date_Used__c DESC NULLS Last LIMIT 10000';
        
        System.debug('***queryStr: ' + queryStr);
        return queryStr;
    }
    
    public String prepareFilterStr(Map<String, String> filterMap, Map<String, String> filterValue, String dateSearch){
        
        String whereStr = '';
        
        List<String> filters = new List<String>();
        if(!filterMap.isEmpty()){
            for(String strField: filterMap.keySet()){
                if(filterValue.get(strField) != null){
                    filters.add(filterMap.get(strField) + filterValue.get(strField));
                }
            }
            
        }
        if(dateSearch != null){
            filters.add(dateSearch);
        }
        for(String filterStr: filters){
            whereStr += filterStr + ' and ';
        }
        
        if(whereStr.length() > 0){
            return ' Where ' + whereStr.subString(0, whereStr.lastIndexOf('and'));
        }else{
            return '';
        } 
        
        return null;
    }
    
    public PageReference doNothing(){
        return null;
    }
}