@isTest
private class luana_URLRewriterTest{

    static testMethod void validateHelloWorld() {
        
        Test.startTest();
        
        luana_URLRewriter reWriter = new luana_URLRewriter();
        
        PageReference pageTest = new PageReference('/luana_memberhome');
        
        system.assert(reWriter.mapRequestUrl(pageTest) == null, 'This should return null');
        
        PageReference pageTest2 = new PageReference('/001XXXXXXXXXXXX/e?nooverride=1');
        
        system.assert(reWriter.mapRequestUrl(pageTest2) != null, 'This shouldnt return null');
        
        system.assert(reWriter.generateUrlFor(null) == null, 'This should return null');
        
        Test.stopTest();
    }
    
}