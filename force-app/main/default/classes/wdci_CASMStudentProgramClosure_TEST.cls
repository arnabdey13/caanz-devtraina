/**
* @author WDCi-Lean
* @date 18/06/2019
* @group Test Class
* @description Test class for wdci_CASMStudentProgramClosure_BATCH and wdci_CASMStudentProgramClosure_SCHED
* @change-history
*/

@isTest(seeAllData=false)
private class wdci_CASMStudentProgramClosure_TEST {
    
    private static String classNamePrefixLong = 'wdci_CASMSP';
    private static String classNamePrefixShort = 'wcsp';

    public static Luana_DataPrep_Test testDataGenerator;
    public static Account memberAccount {get; set;}

    public static LuanaSMS__Training_Organisation__c trainingOrg {get; set;}
    public static LuanaSMS__Program__c progrm {get; set;}
    public static List<LuanaSMS__Course__c> courseAMList {get; set;}

    @TestSetup
    static void initData(){
        Luana_Extension_Settings__c batchSizeSetting = new Luana_Extension_Settings__c(Name = wdci_CASMStudentProgramClosure_BATCH.CASM_JOB_BATCHSIZE_KEY, Value__c = '25');
        Luana_Extension_Settings__c dateTypeSetting = new Luana_Extension_Settings__c(Name = wdci_CASMStudentProgramClosure_BATCH.CASM_JOB_DATE_TYPE_KEY, Value__c = 'LSRD');
        Luana_Extension_Settings__c lastRunDateSetting = new Luana_Extension_Settings__c(Name = wdci_CASMStudentProgramClosure_BATCH.CASM_JOB_LASTRUNDATE_KEY, Value__c = '2019-06-01 00:00:00');

        insert new List<Luana_Extension_Settings__c>{batchSizeSetting, dateTypeSetting, lastRunDateSetting};

        testDataGenerator = new Luana_DataPrep_Test();
        
        //Create all the custom setting
        insert testDataGenerator.prepLuanaExtensionSettingCustomSettings();
        insert testDataGenerator.createLuanaConfigurationCustomSetting();
        
        Map<String, SObject> eduAssets = testDataGenerator.createEducationAssets();                
                        
        //Create user with Member and Employer community access
        memberAccount = testDataGenerator.generateNewApplicantAcc('JoeTest_' + classNamePrefixShort, classNamePrefixLong, 'Full_Member');
        memberAccount.Member_Id__c = '99999';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';        
        memberAccount.Assessible_for_CA_Program__c = false;
        memberAccount.PersonEmail = 'joe_1_' + classNamePrefixShort +'@gmail.com';
        memberAccount.Communication_Preference__c= 'Home Phone';
        memberAccount.PersonHomePhone= '1234';
        memberAccount.PersonOtherStreet= '83 Saggers Road';
        memberAccount.PersonOtherCity='JITARNING';
        memberAccount.PersonOtherState='Western Australia';
        memberAccount.PersonOtherCountry='Australia';
        memberAccount.PersonOtherPostalCode='6365';
        
        insert memberAccount;

        memberAccount= [SELECT Id, PersonContactId, PersonEmail FROM Account WHERE Id = :memberAccount.Id];

        edu_Education_History__c edu_asset = testDataGenerator.createEducationHistory(null, memberAccount.PersonContactId, true, eduAssets.get('universityDegreeJoin1').Id);
        insert edu_asset;
        
        // once the valid Education History is in place, only then can we set the Accessible for CA Program to true
        memberAccount.Assessible_for_CA_Program__c = true;        
        update memberAccount;


    }

    private static void setupMasterData(){

        Id poModuleRTId = Schema.SObjectType.LuanaSMS__Program_Offering__c.getRecordTypeInfosByName().get('Accredited Module').getRecordTypeId();
        Id poCASMRTId = Schema.SObjectType.LuanaSMS__Program_Offering__c.getRecordTypeInfosByName().get('CASM').getRecordTypeId();

        Id courseModuleRTId = Schema.SObjectType.LuanaSMS__Course__c.getRecordTypeInfosByName().get('Accredited Module').getRecordTypeId();
        Id courseCASMRTId = Schema.SObjectType.LuanaSMS__Course__c.getRecordTypeInfosByName().get('CASM').getRecordTypeId();

        testDataGenerator = new Luana_DataPrep_Test();

        memberAccount = [SELECT Id, PersonContactId, PersonEmail FROM Account WHERE Member_Id__c = '99999'];

        //Create traning org
        trainingOrg = testDataGenerator.createNewTraningOrg(classNamePrefixLong, classNamePrefixLong, classNamePrefixShort, 'St 123', 'Wollonggong', '2244');
        insert trainingOrg;       
        
        //Create program
        progrm = testDataGenerator.createNewProgram(classNamePrefixShort, classNamePrefixLong, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        insert progrm;

        //Create multiple subjects
        List<LuanaSMS__Subject__c> subjList = new List<LuanaSMS__Subject__c>();
        subjList.add(testDataGenerator.createNewSubject('TAX NZ_' + classNamePrefixShort, 'TAX NZ_' + classNamePrefixLong, 'TAX NZ_' + classNamePrefixShort, 1, 'Module'));
        subjList.add(testDataGenerator.createNewSubject('Capstone_' + classNamePrefixShort, 'Capstone_' + classNamePrefixLong, 'Capstone', 1, 'Module'));
        subjList.add(testDataGenerator.createNewSubject('FIN_' + classNamePrefixShort, 'FIN_' + classNamePrefixLong, 'FIN_' + classNamePrefixShort, 55, 'Module'));        
        insert subjList;

        LuanaSMS__Program_Offering__c finPO = testDataGenerator.createNewProgOffering('FIN', poModuleRTId, progrm.Id, trainingOrg.Id, null, null, null, 0, 0);
        LuanaSMS__Program_Offering__c capPO = testDataGenerator.createNewProgOffering('CAP', poModuleRTId, progrm.Id, trainingOrg.Id, null, null, null, 0, 0);
        LuanaSMS__Program_Offering__c taxNzPO = testDataGenerator.createNewProgOffering('TAX NZ', poModuleRTId, progrm.Id, trainingOrg.Id, null, null, null, 0, 0);
        LuanaSMS__Program_Offering__c casmFinPO = testDataGenerator.createNewProgOffering('CASM FIN', poCASMRTId, progrm.Id, trainingOrg.Id, null, null, null, 0, 0);

        List<LuanaSMS__Program_Offering__c> amPos = new List<LuanaSMS__Program_Offering__c>{finPO, capPO, taxNzPO, casmFinPO};
        insert amPos;

        List<Product_Permission__c> prodPermissions = new List<Product_Permission__c>();
        for(LuanaSMS__Program_Offering__c ampo: amPos){
            prodPermissions.add(new Product_Permission__c(Program_Offering__c = ampo.Id, Grant_Permission__c = 'Member Community', Member_Criteria__c = 'Assessible for CA Program'));
        }
        insert prodPermissions;

        List<LuanaSMS__Program_Offering_Subject__c> posList = new List<LuanaSMS__Program_Offering_Subject__c>();
        for(LuanaSMS__Program_Offering__c poObj: amPos){
            for(LuanaSMS__Subject__c sub: subjList){
                if(poObj.Name =='TAX NZ' + classNamePrefixShort && sub.name =='TAX NZ_' + classNamePrefixShort){
                    posList.add(testDataGenerator.createNewProgOffSubject(poObj.Id, sub.Id, 'Elective'));
                }else if(poObj.Name =='CAP' + classNamePrefixShort && sub.name =='CAP_' + classNamePrefixShort){
                    posList.add(testDataGenerator.createNewProgOffSubject(poObj.Id, sub.Id, 'Core'));
                }else if(poObj.Name =='FIN' + classNamePrefixShort && sub.name =='FIN_' + classNamePrefixShort){
                    posList.add(testDataGenerator.createNewProgOffSubject(poObj.Id, sub.Id, 'Core'));
                }
            }
        }
        insert posList;

        //Create Delivery Location
        List<LuanaSMS__Delivery_Location__c> devLocations = new List<LuanaSMS__Delivery_Location__c>();
        devLocations.add(testDataGenerator.createNewDeliveryLocationRecord('Australia - South Australia - Adelaide_' + classNamePrefixShort, 'Australia - South Australia - Adelaide_' +classNamePrefixLong, trainingOrg.Id, '1000', 'Australia'));
        devLocations.add(testDataGenerator.createNewDeliveryLocationRecord('United Kingdom - London_' + classNamePrefixShort, 'United Kingdom - London_' +classNamePrefixLong, trainingOrg.Id, '1200', 'United Kingdom'));
        
        //PAN:6226
        LuanaSMS__Delivery_Location__c casmDL = testDataGenerator.createNewDeliveryLocationRecord('CASM Australia - South Australia - Adelaide_' + classNamePrefixShort, 'CASM Australia - South Australia - Adelaide_' +classNamePrefixLong, trainingOrg.Id, '1000', 'Australia');
        casmDL.Is_CASM_Location__c = true;
        devLocations.add(casmDL);

        insert devLocations;

        //Create course
        courseAMList = new List<LuanaSMS__Course__c>();
        for(LuanaSMS__Program_Offering__c ampo: amPos){
            for(integer i = 0; i < 2; i++){

                if(ampo.Name.startsWith('CASM')){
                    //PAN:6226
                    LuanaSMS__Course__c amCourse = testDataGenerator.createNewCourse(ampo.Name +'_11'+i, ampo.Id, courseCASMRTId, 'Running');
                    amCourse.LuanaSMS__Allow_Online_Enrolment__c = true;
                    amCourse.End_Date__c = System.today().addMonths(1);
                    amCourse.CASM_Course_Delivery_Location__c = casmDL.Id;

                    courseAMList.add(amCourse);
                } else {
                    LuanaSMS__Course__c amCourse = testDataGenerator.createNewCourse(ampo.Name +'_11'+i, ampo.Id, courseModuleRTId, 'Running');
                    amCourse.LuanaSMS__Allow_Online_Enrolment__c = true;
                    amCourse.End_Date__c = System.today().addMonths(1);
                    courseAMList.add(amCourse);
                }
            }
        }
        
        insert courseAMList;

        List<Course_Delivery_Location__c> cdls = new List<Course_Delivery_Location__c>();
        for(LuanaSMS__Course__c courseAM: courseAMList){
            for(LuanaSMS__Delivery_Location__c dl: devLocations){
                cdls.add(testDataGenerator.createCourseDeliveryLocation(courseAM.Id, dl.Id, 'Exam Location'));
                
                Course_Delivery_Location__c wlCDL = testDataGenerator.createCourseDeliveryLocation(courseAM.Id, dl.Id, 'Workshop Location');
                wlCDL.Availability_Day__c = 'Saturday;Sunday';
                cdls.add(wlCDL);
            }
        }

        insert cdls;
    }

    @IsTest
    private static void testSchedulingSuccess(){
        
        Test.startTest();

        String jobId = System.schedule('Test_wdci_CASMStudentProgramClosure', '0 0 0 1 12 ? ' + System.today().year(), new wdci_CASMStudentProgramClosure_SCHED());

        Test.stopTest();

        List<CronTrigger> schedJobs = [SELECT Id FROM CronTrigger where Id =: jobId];
        System.debug(schedJobs);

        System.assert(!schedJobs.isEmpty(), 'The job should be scheduled.');
        
    }

    @IsTest
    private static void testSchedulingFail(){
        
        //test without config to simulate failure
        delete [select id from Luana_Extension_Settings__c];

        Test.startTest();

        String jobId = System.schedule('Test_wdci_CASMStudentProgramClosure', '0 0 0 1 12 ? ' + System.today().year(), new wdci_CASMStudentProgramClosure_SCHED());

        Test.stopTest();

        List<LuanaSMS__Luana_Log__c> errorLogs = [SELECT Id FROM LuanaSMS__Luana_Log__c];
        System.assert(!errorLogs.isEmpty(), 'The job should have error.');
        
    }

    @IsTest
    private static void testCASMBatchJobWithLastRunDate(){
        setupMasterData();

        
        Luana_Extension_Settings__c dateTypeSetting = Luana_Extension_Settings__c.getValues(wdci_CASMStudentProgramClosure_BATCH.CASM_JOB_DATE_TYPE_KEY);
        dateTypeSetting.Value__c = 'LRD';

        update dateTypeSetting;

        Id spModuleRTId = Schema.SObjectType.LuanaSMS__Student_Program__c.getRecordTypeInfosByName().get('Accredited Module').getRecordTypeId();
        Id spCASMRTId = Schema.SObjectType.LuanaSMS__Student_Program__c.getRecordTypeInfosByName().get('CASM').getRecordTypeId();

        LuanaSMS__Course__c courseAM;
        LuanaSMS__Course__c courseMasterClass;

        for(LuanaSMS__Course__c course : [select id, Name, RecordType.DeveloperName from LuanaSMS__Course__c where Name like '%FIN%']){
            if(course.RecordType.DeveloperName == 'CASM'){
                courseMasterClass = course;
            } else {
                courseAM = course;
            }
        }

        //create module sp
        LuanaSMS__Student_Program__c newModuleSP = testDataGenerator.createNewStudentProgram(spModuleRTId, memberAccount.PersonContactId , courseAM.Id, 'Australia', 'In Progress');
        newModuleSP.LuanaSMS__Auto_Student_Program_Subject_Creation__c = true;
        newModuleSP.Workshop_Broadcasted__c = true;
        newModuleSP.Paid__c = true;

        insert newModuleSP;

        //create casm sp
        LuanaSMS__Student_Program__c newCASMSP = testDataGenerator.createNewStudentProgram(spCASMRTId, memberAccount.PersonContactId , courseMasterClass.Id, 'Australia', 'In Progress');
        newCASMSP.LuanaSMS__Auto_Student_Program_Subject_Creation__c = true;
        newCASMSP.Workshop_Broadcasted__c = true;
        newCASMSP.Paid__c = true;
        newCASMSP.Related_Student_Program__c = newModuleSP.Id;

        insert newCASMSP;

        //set course result release date
        courseAM.Result_Release_Date__c = System.today().addDays(-1);
        update courseAM;

        Test.startTest();
        Luana_Extension_Settings__c batchSizeSetting = Luana_Extension_Settings__c.getValues(wdci_CASMStudentProgramClosure_BATCH.CASM_JOB_BATCHSIZE_KEY);

        wdci_CASMStudentProgramClosure_BATCH batchJob = new wdci_CASMStudentProgramClosure_BATCH();
        Database.executeBatch(batchJob, Integer.valueOf(batchSizeSetting.Value__c));

        Test.stopTest();

        newCASMSP = [select Id, LuanaSMS__Status__c from LuanaSMS__Student_Program__c where Id =: newCASMSP.Id];

        System.assert(newCASMSP.LuanaSMS__Status__c == 'Completed', 'The CASM student program should have been marked as completed.');
    }

    @IsTest
    private static void testCASMBatchJobWithLastSuccessfulRunDate(){
        setupMasterData();

        Id spModuleRTId = Schema.SObjectType.LuanaSMS__Student_Program__c.getRecordTypeInfosByName().get('Accredited Module').getRecordTypeId();
        Id spCASMRTId = Schema.SObjectType.LuanaSMS__Student_Program__c.getRecordTypeInfosByName().get('CASM').getRecordTypeId();

        LuanaSMS__Course__c courseAM;
        LuanaSMS__Course__c courseMasterClass;

        for(LuanaSMS__Course__c course : [select id, Name, RecordType.DeveloperName from LuanaSMS__Course__c where Name like '%FIN%']){
            if(course.RecordType.DeveloperName == 'CASM'){
                courseMasterClass = course;
            } else {
                courseAM = course;
            }
        }

        //create module sp
        LuanaSMS__Student_Program__c newModuleSP = testDataGenerator.createNewStudentProgram(spModuleRTId, memberAccount.PersonContactId , courseAM.Id, 'Australia', 'In Progress');
        newModuleSP.LuanaSMS__Auto_Student_Program_Subject_Creation__c = true;
        newModuleSP.Workshop_Broadcasted__c = true;
        newModuleSP.Paid__c = true;

        insert newModuleSP;

        //create casm sp
        LuanaSMS__Student_Program__c newCASMSP = testDataGenerator.createNewStudentProgram(spCASMRTId, memberAccount.PersonContactId , courseMasterClass.Id, 'Australia', 'In Progress');
        newCASMSP.LuanaSMS__Auto_Student_Program_Subject_Creation__c = true;
        newCASMSP.Workshop_Broadcasted__c = true;
        newCASMSP.Paid__c = true;
        newCASMSP.Related_Student_Program__c = newModuleSP.Id;

        insert newCASMSP;

        //set course result release date
        courseAM.Result_Release_Date__c = System.today().addDays(-1);
        update courseAM;

        Test.startTest();
        Luana_Extension_Settings__c batchSizeSetting = Luana_Extension_Settings__c.getValues(wdci_CASMStudentProgramClosure_BATCH.CASM_JOB_BATCHSIZE_KEY);

        wdci_CASMStudentProgramClosure_BATCH batchJob = new wdci_CASMStudentProgramClosure_BATCH();
        Database.executeBatch(batchJob, Integer.valueOf(batchSizeSetting.Value__c));

        Test.stopTest();

        newCASMSP = [select Id, LuanaSMS__Status__c from LuanaSMS__Student_Program__c where Id =: newCASMSP.Id];

        System.assert(newCASMSP.LuanaSMS__Status__c == 'Completed', 'The CASM student program should have been marked as completed.');
    }

    @IsTest
    private static void testInitErrorLog(){
        
        memberAccount = [SELECT Id, PersonContactId, PersonEmail FROM Account WHERE Member_Id__c = '99999'];
        
        Test.startTest();

        String jobId = System.schedule('Test_wdci_CASMStudentProgramClosure', '0 0 0 1 12 ? ' + System.today().year(), new wdci_CASMStudentProgramClosure_SCHED());
        LuanaSMS__Luana_Log__c sampleError = wdci_CASMStudentProgramClosure_BATCH.initErrorLog(memberAccount.Id, 'sample error', System.now(), jobId);

        Test.stopTest();

        System.assert(String.isNotBlank(sampleError.LuanaSMS__Parent_Id__c), 'The parent id should have value.');

    }
}