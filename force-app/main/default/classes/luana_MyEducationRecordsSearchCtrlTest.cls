/*
    Developer: WDCi (Lean)
    Development Date: 13/04/2016
    Task #: Test luana_MyEducationRecordsSearch for enrolment wizard
    
    Change History:
    LCA-555 WDCi - KH: 30-06-2016 Include doEdit and doDelete methods test
    LCA-921 23/08/2019 WDCi - KH: Add person account email
    LCA-1070 15/Dec/2016 WDCi - Lean: change date search from Date Commenced to Date Completed
*/

@isTest(seeAllData=false)
private class luana_MyEducationRecordsSearchCtrlTest {
    
    private static String classNamePrefixLong = 'luana_MyEducationRecordsSearchCtrlTest';
    private static String classNamePrefixShort = 'lmers';
    private static map<String, Id> commProfIdMap {get; set;}
    public static Account memberAccount {get; set;}
    public static User memberUser {get; set;}
    
    public static Luana_DataPrep_Test dataPrep;
    
    public static void initial(){
        //initialize
        dataPrep = new Luana_DataPrep_Test();
        
        //Create all the custom setting
        insert dataPrep.prepLuanaExtensionSettingCustomSettings();
        insert dataPrep.createLuanaConfigurationCustomSetting();
        
        //Create user with Member and Employer community access
        memberAccount = dataPrep.generateNewApplicantAcc('Joe_' + classNamePrefixShort, classNamePrefixLong, 'Full_Member');
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.PersonEmail = 'joe_1_' +classNamePrefixShort+'@gmail.com';//LCA-921
        memberAccount.Communication_Preference__c= 'Home Phone';
        memberAccount.PersonHomePhone= '1234';
        memberAccount.PersonOtherStreet= '83 Saggers Road';
        memberAccount.PersonOtherCity='JITARNING';
        memberAccount.PersonOtherState='Western Australia';
        memberAccount.PersonOtherCountry='Australia';
        memberAccount.PersonOtherPostalCode='6365';  
        insert memberAccount;
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
    }
    
    public static void setup(){
        
        //LCA-921
        memberUser = [Select Id, Email, FirstName, LastName, UserName, Name, ContactId from User Where Email =: 'joe_1_' + classNamePrefixShort+'@gmail.com' limit 1];
        //memberUser = testDataGenerator.generateNewApplicantUser(classNamePrefixLong, classNamePrefixshort, memberAccount, commProfIdMap.get('Customer Community Login User'));
        //insert memberUser;
        
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
       
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defEmployerPermSetId.value__c));
        list<PermissionSetAssignment> permissAssign = [Select Id from PermissionSetAssignment where AssigneeId =: memberUser.Id AND (PermissionSetId =: defMemberPermSetId.value__c OR PermissionSetId =: defEmployerPermSetId.value__c)];
        System.runAs(memberUser){
            if(permissAssign == null)
                insert psas;
        }
        
        
        luana_CommUserUtil userUtil = new luana_CommUserUtil(memberUser.Id);
        
        //Create 5 Education Records
        List<Education_Record__c> erlist = new List<Education_Record__c>();
        for(integer i = 0; i < 5; i++){
            Education_Record__c newER = dataPrep.createNewEducationRecords(memberUser.ContactId, System.today().addDays(i), 'Course_' + i +'_'+classNamePrefixShort, 'University ABC');
            newER.Date_completed__c = System.today().addDays(i); //LCA-1070
            
            erlist.add(newER);
        }
        
        insert erlist;
    }
    
    static testMethod void testSearchEducationRecords() { 

        Test.startTest();
            initial();
        Test.stopTest();
        setup();
        
        System.RunAs(memberUser){
                            
            luana_MyEducationRecordsSearchController searchCtrl = new luana_MyEducationRecordsSearchController();
            
            searchCtrl.getTemplateName();
            
            //Commenced Date filter check
            searchCtrl.dateFrom = System.today();
            searchCtrl.dateTo = System.today().addDays(1);
            searchCtrl.doSearch();
            
            //test navigation
            searchCtrl.doNew();
            
            searchCtrl.doEdit();
            searchCtrl.doDelete();
            
            System.assertEquals(searchCtrl.educationRecords.size(), 2, 'Expect 2 record found, but return ' + searchCtrl.educationRecords.size()); 
            
        }
        
    }
    
}