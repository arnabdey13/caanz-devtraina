/**
*   Project:            CAANZ Internal
*   Project Name:       Segmentation
*   Project Task Name:  Account trigger Handler After Update
*   Developer:          akopec
*   Initial Date:       October 2017
*   Revisions           Fix null pointer debugging error
*        Rev 2          Call Address Change when changing from Non Member to Member
*   
*   Description:        Change to Account Address, triggers updates to Account and Geo Segmentation for the correspondent Account ID. 
**/

public without sharing class SegmentationAddressTriggerHandler {

    // Protection from Workflows and other Triggers
    @TestVisible private static Boolean alreadyProcessedSegments = false;
    //private static Boolean alreadyProcessedAccounts = false;
    public static String AddressTypeChange;
    public static Boolean debugFlag = false;
    /*********************************************************************************************
    * formatSafe
    * @description  utility to format address for QAS search based on Country
    *               encapsulate all address types: Residential, Mailing, Billing, Shipping
    *               It is "Safe" as it takes care of nulls
    * @param        String Country          - Country - full name
    * @param        String Street           - the street address to search
    * @param        String City             - the city
    * @param        String State            - State (AU Only)
    * @param        String Postcode         - Postal Code
    *
    **********************************************************************************************/ 
    private static String formatSafe(String Country, String Street, String City, String State, String Postcode) {
        String safeAddress='';
        if (debugFlag) System.debug('Country:' + Country + 'Street:' + Street + ' City:' + City + ' State:' + ' Postcode:' + Postcode);
        if ( Country==Label.CountryAustralia)
            safeAddress = (string.isNotBlank(Street)   ? Street + ', ' : '') +
                          (string.isNotBlank(City)     ? City + ' '    : '') +
                          (string.isNotBlank(State)    ? State + ' '   : '') +
                          (string.isNotBlank(Postcode) ? Postcode + ' ': '');
        else if ( Country==Label.CountryNewZealand)
            safeAddress = (string.isNotBlank(Street)   ? Street + ', ' : '') +
                          (string.isNotBlank(City)     ? City + ' '    : '') +
                          (string.isNotBlank(Postcode) ? Postcode + ' ': '');
              
        return safeAddress.trim();
    }
 
    /*********************************************************************************************
    * formatAddress
    * @description  utility to format address for search
    *               uses the correct selection of fields per address type
    * @param        Account acct            - instance of the Account
    * @param        String addressType      - address types: Residential, Mailing, Billing, Shipping
    *
    **********************************************************************************************/ 
    public static String formatAddress(Account acct, String addressType) {
        String formattedAddress;
        String Country;
        String AddrType;
           
        if (addressType==Label.AddressResidential) {
            Country = acct.PersonOtherCountry;
            AddrType = 'RES';
            formattedAddress = formatSafe(acct.PersonOtherCountry, acct.PersonOtherStreet, acct.PersonOtherCity, acct.PersonOtherStateCode, acct.PersonOtherPostalCode);
        }     
        if (addressType==Label.AddressMailing) {
            Country = acct.PersonMailingCountry;
            AddrType = 'MAL';
            formattedAddress = formatSafe(acct.PersonMailingCountry, acct.PersonMailingStreet, acct.PersonMailingCity, acct.PersonMailingStateCode, acct.PersonMailingPostalCode);
        } 
        if (addressType==Label.AddressBilling) {
            Country = acct.BillingCountry;
            AddrType = 'BIL';
            formattedAddress = formatSafe(acct.BillingCountry, acct.BillingStreet, acct.BillingCity, acct.BillingStateCode, acct.BillingPostalCode);
        }
        if (addressType==Label.AddressShipping) {
            Country = acct.ShippingCountry;
            AddrType = 'SHP';
            formattedAddress = formatSafe(acct.ShippingCountry, acct.ShippingStreet, acct.ShippingCity, acct.ShippingStateCode, acct.ShippingPostalCode);
        }
        AddressTypeChange = AddrType;
        
        if (String.isBlank(Country))
            return ''; 
 
        if (!String.isBlank(Country) && Country != Label.CountryAustralia && Country!=Label.CountryNewZealand)
            return 'Overseas';       
                             
        if  (Country == Label.CountryAustralia )
            return Label.CountryAU+':' + AddrType + ':' + formattedAddress;
        
        if  (Country == Label.CountryNewZealand )
            return Label.CountryNZ+':' + AddrType + ':' +  formattedAddress;
        
        return '';
    }
    
    
    public static String getGeoAccuracy( String Verification) {
       if (String.isBlank(Verification)) 
           return NULL;
        
        if (Verification.equalsIgnoreCase('Verified'))
            return Label.Address;
        
        if (Verification.equalsIgnoreCase('PremisesPartial'))
            return Label.Block;
        
        if (Verification.equalsIgnoreCase('InteractionRequired'))
            return Label.Street;
        
        if (Verification.equalsIgnoreCase('Multiple'))
            return Label.Street;
        
        if (Verification.equalsIgnoreCase('StreetPartial'))
            return Label.Street;
        return null;
    }
    
    public static String getConfLevel( String Verification) {
       if (String.isBlank(Verification)) 
           return NULL;
        
        if (Verification.equalsIgnoreCase('Verified'))
            return Label.GeoConfidenceLevelHigh;
        
        if (Verification.equalsIgnoreCase('PremisesPartial'))
            return Label.GeoConfidenceLevelMed_High;
        
        if (Verification.equalsIgnoreCase('InteractionRequired'))
            return Label.GeoConfidenceLevelMed;
        
        if (Verification.equalsIgnoreCase('Multiple'))
            return Label.GeoConfidenceLevelMed;
        
        if (Verification.equalsIgnoreCase('StreetPartial'))
            return Label.GeoConfidenceLevelMed;
        return null;
    }
    
    public static Boolean getConfigAddressUpdateStatus(String ConfigName ) {
        Segmentation_Settings__c Config = Segmentation_Settings__c.getInstance(ConfigName);
        if (Config != null)
            return Config.Active__c;
        else
            return FALSE; 
    }
    
    
    public static Boolean getConfigAddressUpdateDebug(String ConfigName ) {
        Segmentation_Settings__c Config = Segmentation_Settings__c.getInstance(ConfigName);
        if (Config != null)
            return Config.Active__c;
        else
            return FALSE; 
    }
    
    public static ID getNZMeshblockID(String Meshblock ) {
        List <Reference_Geography_New_Zealand__c> MeshId = [SELECT Id, Name FROM Reference_Geography_New_Zealand__c WHERE Name = : Meshblock LIMIT 1];
        if (!MeshId.isEmpty())
            return MeshId[0].Id;
        else
            return null; 
    }
    
    public static ID getAUSA1ID(String Meshblock ) {
        List <Reference_Meshblock_Australia__c> MeshSA1 = [SELECT Id, Name,  MB_CODE_2016__c, SA1_MAINCODE__c, SA1_7DIGIT__c, SA1_ID__c FROM Reference_Meshblock_Australia__c WHERE Name = : Meshblock LIMIT 1];
        if (!MeshSA1.isEmpty())
            return MeshSA1[0].SA1_ID__c;         

       return null; 
    }
    
    
    // Rev 2 start
    // static call to determine a list of Accounts that will require update of the segmentation object when Change of Membership    
     public static Map<Id, String> handleSegmentsAfterMembershipChange(List<Account> newList, Map<Id, Account> newMap, List<Account> oldList, Map<Id, Account> oldMap){     
        if(getConfigAddressUpdateDebug('SegmentationAddressTriggerDebug' )) 
            debugFlag=true; //Turn on debugging        
        if (debugFlag) System.debug('Started debugging in Address Trigger Handler');
         
        Map<Id, String> SegAcctIdsToUpdate = new Map<Id, String>{};
        String vAddress;
         
        // don't run it again (workflows could trigger it again)
        if (alreadyProcessedSegments) {
            if (debugFlag) System.debug('Already Processed');
            return SegAcctIdsToUpdate;
         }
         
        for(Account acc: newList){
            if(acc.isPersonAccount){
                if (debugFlag) System.debug('Address: ' + newList[0].PersonOtherStreet);
                
                // Case X New Member changed from Non Member without changing address
                if  ( oldMap != null 
                        && oldMap.containsKey(acc.Id)  
                        && !String.isBlank(acc.Membership_Type__c) 
                        && acc.Membership_Type__c == Label.AccountMembershipTypeMember 
                        && oldMap.get(acc.Id).Membership_Type__c == Label.AccountMembershipTypeNon_Member )
                { 
                if (debugFlag) System.debug('I am in Case X Membership Change and Not Address');  
                vAddress = formatAddress(acc, Label.AddressResidential);
                if (vAddress!='Overseas')
                    SegAcctIdsToUpdate.put(acc.Id, vAddress); 
                }  
            }
        }
        System.debug('After Process - SegAcctToUpdate:'+ SegAcctIdsToUpdate); 
        // Mark as processed if not empty
        if ( SegAcctIdsToUpdate.size() > 0) {
            alreadyProcessedSegments = true; }
        return SegAcctIdsToUpdate;       
     }
    // Rev 2 end    
    
    
     // static call to determine a list of Accounts that will require update of the segmentation object 
     public static Map<Id, String> handleSegmentsAfterUpdate(List<Account> newList, Map<Id, Account> newMap, List<Account> oldList, Map<Id, Account> oldMap){ 
        
        if(getConfigAddressUpdateDebug(Label.SegmentSettSegmentationAddressTriggerDebug)) 
            debugFlag=true; //Turn on debugging 
        
        if (debugFlag) System.debug('I start debugging in Trigger Handler');
         
        Map<Id, String> SegmentAcctIdsToUpdate = new Map<Id, String>{};
        String newAddress;
         
        // don't run it again (workflows could trigger it again)
        if (alreadyProcessedSegments) {
            if (debugFlag) System.debug('Already Processed');
            return SegmentAcctIdsToUpdate;
         }
        
        for(Account acc: newList){
            // different set of Account Addresses for Person and Business

            if(acc.isPersonAccount){
                if (debugFlag) System.debug('I am Person Account');
                // do not waste calls to web service on account types that do not require segmentation
                if ( acc.Membership_Type__c == Label.AccountMembershipTypeStudent_Affiliate || acc.Membership_Type__c == Label.AccountMembershipTypeNon_Member || String.isBlank(acc.Membership_Type__c) )
                    return new Map<Id, String>();
                
                // R01 fix null pointer error
                if (debugFlag) {
                    if (oldList != NULL)
                        System.debug('Old Address: ' + oldList[0].PersonOtherStreet);
                }
                if (debugFlag) System.debug('New Address: ' + newList[0].PersonOtherStreet);
                
                // Case 1. New residential address is populated and different than previously
                if ( ( oldMap == null && !String.isBlank(acc.PersonOtherStreet) ) ||
                     ( oldMap != null && oldMap.containsKey(acc.Id)  
                                      && !String.isBlank(acc.PersonOtherStreet) 
                                      && oldMap.get(acc.Id).PersonOtherStreet != acc.PersonOtherStreet ))
                { 
                    if (debugFlag) System.debug('I am in Case 1');
                    newAddress = formatAddress(acc, Label.AddressResidential);
                    if (newAddress!='Overseas')
                        SegmentAcctIdsToUpdate.put(acc.Id, newAddress); 
                }
                
                // Case 2. Current & previous residential address is empty and New Mailing is different than previously
                else if ( (oldMap == null &&  String.isBlank(acc.PersonOtherStreet) 
                                          && !String.isBlank(acc.PersonMailingStreet) ) ||
                          (oldMap != null && oldMap.containsKey(acc.Id)  
                                          &&  String.isBlank(acc.PersonOtherStreet )
                                          &&  String.isBlank(oldMap.get(acc.Id).PersonOtherStreet)
                                          && !String.isBlank(acc.PersonMailingStreet)                 
                                          && oldMap.get(acc.Id).PersonMailingStreet != acc.PersonMailingStreet ) )
                {                             
                    newAddress = formatAddress(acc, Label.AddressMailing);
                    if (debugFlag) System.debug('I am in Case 2');
                    if (newAddress!='Overseas')
                        SegmentAcctIdsToUpdate.put(acc.Id, newAddress);       
                }
                
                // Case 3. New residential address is empty but wasn't empty before and Previous segmentation was based on residential address, 
                // which is now empty so use the current Mailing even if Mailing did not change - Update only no Insert possible
                else if (oldMap != null && oldMap.containsKey(acc.Id) 
                                        &&  String.isBlank(acc.PersonOtherStreet)
                                        && !String.isBlank(acc.PersonMailingStreet)
                                        && !String.isBlank(oldMap.get(acc.Id).PersonOtherStreet) ) {   
                    newAddress = formatAddress(acc, Label.AddressMailing);
                    if (debugFlag) System.debug('I am in Case 3');
                    if (newAddress!='Overseas')
                        SegmentAcctIdsToUpdate.put(acc.Id, newAddress);       
                }
                
                // Case 4. Everything is empty now and wasn't before - reset geo segmentation to nothing
                else if (oldMap != null && oldMap.containsKey(acc.Id) 
                                        &&  String.isBlank(acc.PersonOtherStreet)
                                        &&  String.isBlank(acc.PersonMailingStreet)
                                        && (!String.isBlank(oldMap.get(acc.Id).PersonOtherStreet) || !String.isBlank(oldMap.get(acc.Id).PersonMailingStreet) ) ) 
                {   
                    // new address is empty
                    if (debugFlag) System.debug('I am in Case 4');
                    SegmentAcctIdsToUpdate.put(acc.Id, newAddress);       
                }
                

            } else // ####################  logic for Businesses #######################                
            {
                // Case 1. New Billing address is populated and different than previously
                if ( ( oldMap == null && !String.isBlank(acc.BillingStreet) ) ||
                     ( oldMap != null && oldMap.containsKey(acc.Id) 
                                      && !String.isBlank(acc.BillingStreet) 
                                      && oldMap.get(acc.Id).BillingStreet != acc.BillingStreet ) )
                { 
                    if (debugFlag) System.debug('I am in Case 1 for Business');
                    newAddress = formatAddress(acc, Label.AddressBilling);
                    if (debugFlag) System.debug('formatted newAddress:'+ newAddress);
                    if (newAddress!='Overseas')
                        SegmentAcctIdsToUpdate.put(acc.Id, newAddress); 
                }     
                
                // Case 2. New Shipping address is populated and different than previously
                else if ( (oldMap == null &&  String.isBlank(acc.BillingStreet) 
                                          && !String.isBlank(acc.ShippingStreet) ) ||
                         ( oldMap != null && oldMap.containsKey(acc.Id) 
                                          &&  String.isBlank(acc.BillingStreet) 
                                          && !String.isBlank(acc.ShippingStreet) 
                                          && oldMap.get(acc.Id).ShippingStreet != acc.ShippingStreet ) )
                { 
                    if (debugFlag) System.debug('I am in Case 2 for Business');
                    newAddress = formatAddress(acc, Label.AddressShipping);
                    if (debugFlag) System.debug('formatted newAddress:'+ newAddress);
                    if (newAddress!='Overseas')
                        SegmentAcctIdsToUpdate.put(acc.Id, newAddress); 
                }

            }       
        }
        System.debug('After Process - SegmentsAcctYoUpdate:'+ SegmentAcctIdsToUpdate);
        // Mark as processed if not empty
        if ( SegmentAcctIdsToUpdate.size() > 0) {
            alreadyProcessedSegments = true;
        }
        return SegmentAcctIdsToUpdate;      
    } 
    
}