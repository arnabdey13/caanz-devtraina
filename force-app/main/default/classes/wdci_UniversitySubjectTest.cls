/*
 * @author          WDCi (LKoh)
 * @date            24-June-2019
 * @description     Test class for the wdci_UniversitySubjectHandler Trigger Handler
 * @changehistory
 */
@isTest
public class wdci_UniversitySubjectTest {

    @TestSetup
    static void generateAssets() {

        wdci_TestUtil.generateAsset();
    }

    @isTest static void testUniversitySubjectHandler() {

        Test.startTest();
        // Updating an existing University Subject that is related to a Pathway will trigger a custom trigger error
        List<FT_University_Subject__c> checkUniSubject = [SELECT Id, FT_Subject_Code__c FROM FT_University_Subject__c WHERE FT_Subject_Code__c = 'BUSS1030'];
        try {
            update checkUniSubject;
        } catch (Exception ex) {            
            system.debug('Expected Exception: ' +ex);
        }

        Test.stopTest();
    }
}