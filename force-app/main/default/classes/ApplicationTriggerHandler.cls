/*
* Handler for Application trigger
======================================================
History 
April 2016      Davanti     Created for PASA.   
*/
public without sharing class ApplicationTriggerHandler extends TriggerHandler{ 
    
    List<Application__c> newList = (List<Application__c>)Trigger.new;
    List<Application__c> oldList = (List<Application__c>)Trigger.old;
    Map<Id,Application__c> oldMap = (Map<Id,Application__c>)Trigger.oldMap;
    Map<Id,Application__c> newMap = (Map<Id,Application__c>)Trigger.newMap;        
    
    
    public override void beforeInsert(){ 
        
        for(Application__c a:newList) {
            if(a.Application_Status__c == 'Submitted for Assessment' || 
               a.Application_Status__c == 'Approved' ||
               a.Application_Status__c == 'Declined' ||
               a.Application_Status__c == 'Escalated for Approval') a.Lock__c = true;
            else
                a.Lock__c = false;
        }
    }
    
    public override void afterInsert() {
        // Because applications are the detail of an Account, we need to share the account.
        // For all new applications where the Migration Agent is not null,
        // Share the account owner of the applications, with the Agent.
        
        // This map is a map of the Applications Account ID to  Migration Agent ID -- it is the final map used to share
        // Note -- this implicitly assumes that there will only  be one share per account!
        Map<ID, ID> accountsToShareWithAgent = new Map<ID, ID>();
        
        for (Application__c application: newList) {
            if (application.Migration_Agent__c != null) {
                accountsToShareWithAgent.put(application.Account__c, application.Migration_Agent__c);
            }
        }
        
        List<AccountShare> accountShares =  createSharesForAccounts(accountsToShareWithAgent, 'Read');
        
        //Create!
        for (AccountShare debugItem: accountShares) {
            System.debug('Sharing: User id:' + debugItem.UserOrGroupId + ' Accountid: ' + debugItem.AccountId);
        }
        try {
            Database.insert(accountShares);
        } catch (Exception e) {
            //do nothing... just continue
        }
        
    }
    
    //This is not strictly needed for the business -- but makes testing a heck of a lot easier.
    public override void afterUpdate() {
    
     System.debug('In after update of application trigger.');
       
    
         //Handling Application status changes 
         ApplicationTriggerClass.checkApplicationChanges(oldMap,newMap); 
         
        // Because applications are the detail of an Account, we need to share the account.
        // For all new applications where the Migration Agent is not null,Share the account owner of the applications, with the Agent.                                  
        // This map is a map of the Applications Account ID to  Migration Agent ID -- it is the final map used to share
        // Note -- this implicitly assumes that there will only  be one share per account!
        Map<ID, ID> accountsToShareWithAgent = new Map<ID, ID>();        
        for (Application__c application: newMap.values()) {
            Application__c oldApplication = oldMap.get(application.Id);
            System.debug('Check application.  New migration agent is:' + application.Migration_Agent__c + 'old is: ' + oldApplication.Migration_Agent__c);
            if (application.Migration_Agent__c != null && oldApplication.Migration_Agent__c == null) {
                accountsToShareWithAgent.put(application.Account__c, application.Migration_Agent__c);
            }
        }
        
        List<AccountShare> accountShares =  createSharesForAccounts(accountsToShareWithAgent, 'Read');
        
        //Create!
        for (AccountShare debugItem: accountShares) {
            System.debug('Sharing: User id:' + debugItem.UserOrGroupId + ' Accountid: ' + debugItem.AccountId);
        }
        try {
            Database.insert(accountShares);
        } catch (Exception e) {
            System.debug('Error sharing account: ' + e.getMessage());
        }        
             
    }
    
    //     Create a list of Account Shares for the map of accounts to share.
    private List<AccountShare> createSharesForAccounts(Map<ID, ID> accountsToShare,String accessLevel ) {
      List<AccountShare> shareList = new List<AccountShare>();
      Map<ID, ID> agentToAgentUser = getAccountsToUsersMap(accountsToShare.values());
       for (ID applicantsAccount: accountsToShare.keySet()) {
               AccountShare newShare = new AccountShare();
               newShare.AccountId = applicantsAccount;
               newShare.AccountAccessLevel = accessLevel;
               newShare.CaseAccessLevel = 'None';
              //newShare.ContactAccessLevel = 'None';
               newShare.OpportunityAccessLevel = 'None';
              ID agentAccount = accountsToShare.get(applicantsAccount);
              newShare.UserOrGroupId = agentToAgentUser.get(agentAccount);
              newShare.RowCause = Schema.AccountShare.RowCause.Manual;
              shareList.add(newShare);
           }
      return shareList;
    }
    
    // Given a List of Accounts  -- return a map of Account to User.
    // The map is Map<AccountID, UserID>
    public static Map<ID, ID> getAccountsToUsersMap(List<Id> accounts) {
        Map<ID, ID> accountsToUsers = new Map<ID,ID>();
        
        List<User> users = [select ID, AccountId from User where AccountId in :accounts];
        //Go through this to create a map of Accounts to users.
        for (User user: users) {
            accountsToUsers.put(user.AccountId, user.id);
        }
        return accountsToUsers;
    }    
}