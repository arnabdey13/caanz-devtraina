/*
    Developer: WDCi (kh)
    Development Date:6/09/2016
    Task: PPP Workshop register controller
    
    Change History:
    LCA-986 20/09/2016: WDCi KH - PPP - Workshop Session Management Issue
*/
public class luana_PPPWorkshopRegisterController {
    
    public Id custCommConId {get; private set;}
    public Id custCommAccId {get; private set;}
    
    public List<wSession> workshopRegList {get; set;}
    
    public String wsSelected {get; set;}
    public String selectedTopicKey {set; get;}
    
    public boolean hasOverlapSession {private set; get;}
    public boolean hasDuplicateTopic {private set; get;}
    
    public Map<Id, Course_Delivery_Location__c> workshopLocationMap {get; private set;}
    public String selectedCourseId {set; get;}
    public String selectedWorkshopLocation1Id {set; get;}
    
    public LuanaSMS__Student_Program__c workingStudentProgram {set; get;}
    
    public luana_PPPWorkshopRegisterController(){
        
        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId()); 
        this.custCommConId = commUserUtil.custCommConId;       
        this.custCommAccId = commUserUtil.custCommAccId;
        
        hasOverlapSession = false;
        hasDuplicateTopic = false;
        
        
        workshopLocationMap = new Map<Id, Course_Delivery_Location__c>();
        selectedCourseId = getCourseIdFromURL();
        
        workingStudentProgram = new LuanaSMS__Student_Program__c();
    }
    
    
    public PageReference doBooking(){
        
        Savepoint sp = Database.setSavepoint();
        
        try{
            
            if(!String.isBlank(workingStudentProgram.Specify_Assistance_Details__c)){
                LuanaSMS__Student_Program__c updateSP = new LuanaSMS__Student_Program__c(Id = getStudentProgramIdFromURL());
                updateSP.Specify_Assistance_Details__c = workingStudentProgram.Specify_Assistance_Details__c;
                updateSP.Do_you_require_assistance__c = 'Yes';
                
                update updateSP;
            }          
                        
            hasOverlapSession = false;
            
            List<LuanaSMS__Attendance2__c> atts = new List<LuanaSMS__Attendance2__c>();
            
            if(!workshopRegList.isEmpty()){
                boolean foundSession = false;
                
                for(wSession ws: workshopRegList){
                    if(ws.topicKey == selectedTopicKey){
                        for(LuanaSMS__Session__c validSess: ws.sessionList){
                            LuanaSMS__Attendance2__c newAttendance = new LuanaSMS__Attendance2__c();
                            newAttendance.LuanaSMS__Student_Program__c = getStudentProgramIdFromURL();
                            newAttendance.LuanaSMS__Session__c = validSess.Id;
                            newAttendance.LuanaSMS__Contact_Student__c = custCommConId;
                            newAttendance.LuanaSMS__Type__c = 'Student';
                            
                            atts.add(newAttendance);
                            
                            foundSession = true;
                        }
                    }
                }
                
                if(foundSession){
                    insert atts;
                        
                    PageReference pageRef = Page.luana_MyPPPEnrolment;
                    pageRef.setAnchor('workshoplocation');
                    pageRef.getParameters().put('spid', getStudentProgramIdFromURL());
                    
                    
                    return pageRef;
                } else {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please select a workshop session'));
                    return null;
                }
            } else {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'No PPP workshop is available for this location'));
                return null;
            }
            
        } catch(Exception exp){
            
            if(exp.getMessage().contains('The student has overlapping attendance')){
                hasOverlapSession = true;
            } if(exp.getMessage().contains('duplicates value on record with id')){
                hasDuplicateTopic = true;
            } 
            if(selectedTopicKey == null || selectedTopicKey == ''){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please select a workshop session'));
            } 
            else {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Error registering the session: ' + exp.getMessage()));
            }
            
            Database.rollback(sp);
            return null;
        }
        
    }
    
    public PageReference doBack(){
        
        PageReference pageRef = Page.luana_MyPPPEnrolment;
        pageRef.setAnchor('workshoplocation');
        pageRef.getParameters().put('spid', getStudentProgramIdFromURL());
        
        return pageRef;
    }
    
    public String getTemplateName(){
        return luana_NetworkUtil.getTemplateName();
    }
        
    private String getStudentProgramIdFromURL(){
        if(ApexPages.currentPage().getParameters().containsKey('spid')){
            return ApexPages.currentPage().getParameters().get('spid');
        }
        return null;
    }
    
    public class wSession {
        public Boolean isSelected {get; set;}
        public Boolean isAvailable {get; set;}
        public String groupdedSessionName {get; set;}
        public String seatEnroled {get; set;}
        public String seatStatus {get; set;}
        public String topicKey {get; set;}
        
        public List<LuanaSMS__Session__c> sessionList {get; set;}
        
        public wSession(Boolean avail, String sName, String se, String ss, String tkey, List<LuanaSMS__Session__c> sList){
            isSelected = false;
            isAvailable = avail;
            groupdedSessionName = sName;
            seatEnroled = se;
            seatStatus = ss;
            topicKey = tkey;
            
            sessionList = sList;
        }
    }
    
    
    //LCA-5, Phase 3 
    public List<SelectOption> getWorkshopLocationOptions(Id masterCourseId){
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--None--'));
        List<Course_Delivery_Location__c> courseDeliveryLocations = luana_DeliveryLocationUtil.getCourseWorkshopLocation(masterCourseId); //LCA-698

        for(Course_Delivery_Location__c dl: courseDeliveryLocations){
            if(dl.Type__c == 'Workshop Location'){
                SelectOption option = new SelectOption(dl.Id, dl.Name);
                options.add(option); 
                workshopLocationMap.put(dl.Id, dl);
            }
        }
        return options;
    }
    
    public List<SelectOption> getWorkshop1LocationOptions(){
        
        //LCA-986 Change on 20/09/2016 to combine all the CDL in one place
        Luana_Extension_Settings__c pppMaster = Luana_Extension_Settings__c.getValues('PPP_Master');
        List<LuanaSMS__Course__c> masterPPPCourse = [Select Id from LuanaSMS__Course__c Where LuanaSMS__Program_Offering__c =: pppMaster.value__c and luanaSMS__Status__c = 'Running' limit 1];
        if(!masterPPPCourse.isEmpty()){
            return getWorkshopLocationOptions(masterPPPCourse[0].Id);
        }else{
            return null;
        }
    }
    
    private String getCourseIdFromURL(){
        if(ApexPages.currentPage().getParameters().containsKey('courseid')){
            return ApexPages.currentPage().getParameters().get('courseid');
            
        }
        return null;
    }
    
    public String counter {get; set;}
    
    public void optionSelected() {
        
        //LCA-986
        /*LuanaSMS__Student_Program__c tempSP = new LuanaSMS__Student_Program__c();
        tempSP.Id = getStudentProgramIdFromURL();
        tempSP.Workshop_Location_Preference_1__c = selectedWorkshopLocation1Id;
        update tempSP;
        */

        workshopRegList = new List<wSession>();
        
        if(selectedWorkshopLocation1Id != null){
            List<LuanaSMS__Session__c> sessions = [Select Id, name, Maximum_Capacity__c, Seats_Availability__c, Topic_Key__c, LuanaSMS__Start_Time__c, LuanaSMS__End_Time__c, 
                                                    Session_End_Time_Venue__c,  Session_Start_Time_Venue__c, LuanaSMS__Number_of_enrolled_students__c, LuanaSMS__Status__c, 
                                                    LuanaSMS__Venue__c, LuanaSMS__Venue__r.Name, Venue_Timezone__c, Course_Delivery_Location__r.Course__c, Course_Delivery_Location__r.Course__r.LuanaSMS__Program_Offering__r.Course_Session_Close_Before_Days__c 
                                                    from LuanaSMS__Session__c Where Course_Delivery_Location__c =: selectedWorkshopLocation1Id and LuanaSMS__Status__c = 'Running' order by LuanaSMS__Start_Time__c ASC];
            System.debug('***sessions:: ' + sessions);

            Map<String, List<LuanaSMS__Session__c>> groupedSessions = new Map<String, List<LuanaSMS__Session__c>>(); 
            
            //Group the session                                      
            for(LuanaSMS__Session__c sess: sessions){
                
                if(groupedSessions.containsKey(sess.Topic_Key__c)){
                    groupedSessions.get(sess.Topic_Key__c).add(sess);
                }else{
                    List<LuanaSMS__Session__c> newSessList = new List<LuanaSMS__Session__c>();
                    newSessList.add(sess);
                    groupedSessions.put(sess.Topic_Key__c, newSessList);
                }
            }
            //check has sufficient session for each group
            Set<String> groupWithSufficentSessionKey = new Set<String>();
            for(String sKey: groupedSessions.keySet()){
                
                Integer sessCounter = 1;
                Map<String, Integer> groupWithInsufficentSession = new Map<String, Integer>();
                for(LuanaSMS__Session__c groupedSess: groupedSessions.get(sKey)){
                    if(groupWithInsufficentSession.containsKey(groupedSess.Topic_Key__c)){
                        groupWithInsufficentSession.put(groupedSess.Topic_Key__c, groupWithInsufficentSession.get(groupedSess.Topic_Key__c)+1);
                        groupWithSufficentSessionKey.add(groupedSess.Topic_Key__c);
                    }else{
                        groupWithInsufficentSession.put(groupedSess.Topic_Key__c, 1);
                    }
                }

                if(groupWithInsufficentSession.get(sKey) < 2){
                    groupedSessions.remove(sKey);
                }
            }
            
            //check the session start date is before the no. of days stated in Course_Session_Close_Before_Days__c
            Set<String> hasSessionBeforeCheckDays = new Set<String>();
            for(String currentKey: groupWithSufficentSessionKey){
                for(LuanaSMS__Session__c groupedSess: groupedSessions.get(currentKey)){
                    
                    Integer cSessionCloseBeforeDays;
                    if(groupedSess.Course_Delivery_Location__r.Course__r.LuanaSMS__Program_Offering__r.Course_Session_Close_Before_Days__c == null){
                        cSessionCloseBeforeDays = 0;
                    }else{
                        cSessionCloseBeforeDays = Integer.valueOf(groupedSess.Course_Delivery_Location__r.Course__r.LuanaSMS__Program_Offering__r.Course_Session_Close_Before_Days__c);
                    }
                    
                    if(groupedSess.LuanaSMS__Start_Time__c < System.today().addDays(cSessionCloseBeforeDays)){
                        hasSessionBeforeCheckDays.add(currentKey);
                    }
                }
            }
            for(String tKeyToRemove: hasSessionBeforeCheckDays){
                groupedSessions.remove(tKeyToRemove);
            }
            
            Integer workshopCounter = 0;
            
            for(String sKey: groupedSessions.keySet()){
                workshopCounter++;
                    
                Integer sessionCounter = 0;
                String seatEnrolStr = '';
                String seatStatusStr = '';
                String venueName = '';
                String venueTimezone = '';
                
                Decimal leastNoMaxCap;
                String leastSessionId;
                Decimal seatAvailable;
                
                for(LuanaSMS__Session__c groupSess: groupedSessions.get(sKey)){
                    if(seatAvailable == null){
                        seatAvailable = groupSess.Maximum_Capacity__c - groupSess.LuanaSMS__Number_of_enrolled_students__c;
                        leastSessionId = groupSess.Id;
                    } else {
                        if((groupSess.Maximum_Capacity__c - groupSess.LuanaSMS__Number_of_enrolled_students__c) < seatAvailable){
                            seatAvailable = groupSess.Maximum_Capacity__c - groupSess.LuanaSMS__Number_of_enrolled_students__c;
                            leastSessionId = groupSess.Id;
                        }
                    }
                }
                
                for(LuanaSMS__Session__c groupSess: groupedSessions.get(sKey)){
                    sessionCounter++;
                    
                    //Find the least No of Maximum Capacity
                    /*if(leastNoMaxCap == null){
                        leastNoMaxCap = groupSess.Maximum_Capacity__c;
                        leastSessionId = groupSess.Id;
                    }else{
                        if(groupSess.Maximum_Capacity__c < leastNoMaxCap){
                            leastNoMaxCap = groupSess.Maximum_Capacity__c;
                            leastSessionId = groupSess.Id;
                        }
                    }*/
                    
                    //TODO find the least max cap session to display the seat enroled field in visualforce page
                    if(groupSess.Id == leastSessionId){
                        seatEnrolStr = groupSess.LuanaSMS__Number_of_enrolled_students__c +'/'+groupSess.Maximum_Capacity__c;
                        
                        seatAvailable = groupSess.Maximum_Capacity__c - groupSess.LuanaSMS__Number_of_enrolled_students__c;
                        
                        System.debug('****run here0::::' + seatEnrolStr + ' - '+ groupSess.Seats_Availability__c);
                        if(groupSess.Seats_Availability__c > 20){
                            seatStatusStr = 'Available';
                        } else if((groupSess.Seats_Availability__c <= 20) && (groupSess.Seats_Availability__c > 0)){
                            seatStatusStr = 'Limited seat';
                        } else if(groupSess.Seats_Availability__c == 0){
                            seatStatusStr = 'Full';
                        }   
                    }
                }
                
                System.debug('****run here2::::' + seatStatusStr);
                
                workshopRegList.add(new wSession(seatAvailable <= 0 ? false: true, 'Workshop Session #' + workshopCounter, seatEnrolStr, seatStatusStr, sKey, groupedSessions.get(sKey)));
                sessionCounter = 1; 
            }
        }
    }
}