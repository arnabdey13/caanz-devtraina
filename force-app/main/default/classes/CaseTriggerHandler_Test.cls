/*------------------------------------------------------------------------------------
Author:        Mayukhman
Company:       Tech Mahindra
Description:   Test class for most of the Controllers - CaseTriggerHandler 

History
Date            Author             Comments
--------------------------------------------------------------------------------------
24-07-2019     Mayukhman         Initial Release
------------------------------------------------------------------------------------*/
@isTest 
public class CaseTriggerHandler_Test {
    private static Account currentAccount;
    private static Case currentCase;
    private static Case currentPortalCase;
    static {
        currentAccount = CaseTriggerHandler_Test.createFullMemberAccount();
        insert currentAccount;
        currentCase = CaseTriggerHandler_Test.createCase();
        insert currentCase;
        currentCase.Type = 'Complaints';
        currentCase.Sub_Type__c = 'About CA ANZ';
        update currentCase;
        currentPortalCase = CaseTriggerHandler_Test.createPortalCase();
        insert currentPortalCase;
    }
    
    
    // <Create FullMemberAccount>
    public static Account createFullMemberAccount(){
        Account FullMemberAccountObject = new Account();
        FullMemberAccountObject.Salutation = 'Mr.';
        FullMemberAccountObject.FirstName = 'TestFName';
        FullMemberAccountObject.PersonEmail= 'Full_Member'+ CaseTriggerHandler_Test.getRandomNumber(910) +'@gmail.com';
        FullMemberAccountObject.LastName = 'Test'+ CaseTriggerHandler_Test.getRandomNumber(910);
        FullMemberAccountObject.RecordTypeId = RecordTypeCache.getId(FullMemberAccountObject, 'Full_Member');
        FullMemberAccountObject.Communication_Preference__c= 'Home Phone';
        FullMemberAccountObject.PersonHomePhone= '1234';
        FullMemberAccountObject.PersonOtherStreet= '83 Saggers Road';
        FullMemberAccountObject.PersonOtherCity='JITARNING';
        FullMemberAccountObject.PersonOtherState='Western Australia';
        FullMemberAccountObject.PersonOtherCountry='Australia';
        FullMemberAccountObject.PersonOtherPostalCode='6365';  
        return FullMemberAccountObject;
    }
    public static Case createCase(){
        Id recordTypeID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case').getRecordTypeId();
        Case CAANZMSTCase = new Case();
        CAANZMSTCase.AccountId = currentAccount.Id;
        CAANZMSTCase.Subject = 'Test';
        CAANZMSTCase.RecordTypeId = recordTypeID;
        return CAANZMSTCase;
    }
    public static Case createPortalCase(){
        Id recordTypeID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Portal Case').getRecordTypeId();
        Case CAANZMSTCase = new Case();
        CAANZMSTCase.AccountId = currentAccount.Id;
        CAANZMSTCase.Subject = 'Test 2';
        CAANZMSTCase.Type = 'Complaints';
        CAANZMSTCase.Sub_Type__c = 'About a Member';
        CAANZMSTCase.RecordTypeId = recordTypeID;
        CAANZMSTCase.Origin = 'MyCA';
        return CAANZMSTCase;
    }
    public static Integer getRandomNumber(Integer size){
        Double d = math.random() * size;
        return d.intValue();
    }
    
    @isTest static void test_CaseTriggerHandler() {
        
    }
}