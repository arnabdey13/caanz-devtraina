@IsTest(seeAllData=false)
private class ff_WrapperSourceTests {

    @IsTest
    private static void testCountriesAndStates() {
        Map<String, ff_WrapperSource> countryAndStates = new ff_WrappedCountryStateSources().getSources();
        System.assertEquals(2, countryAndStates.size(), 'two picklists i.e. countries and states');
        for (ff_WrapperSource src : countryAndStates.values()) {
            System.debug(src.getWrappers());
        }
        // TODO check states are parented by countries
    }

    @IsTest
    private static void testDescribe() {

        // picklist
        ff_WrappedFieldDescribeSource src1 = new ff_WrappedFieldDescribeSource(Account.Type);
        System.debug(src1.getWrappers());

        // checkbox
        ff_WrappedFieldDescribeSource src2 = new ff_WrappedFieldDescribeSource(Contact.HasOptedOutOfEmail);
        System.debug(src2.getWrappers());

        // dependent picklist
        Map<String, Set<String>> valueToParentMap = new Map<String, Set<String>>();
        valueToParentMap.put('Customer - Direct', new Set<String>{
                'Customers'
        });
        valueToParentMap.put('Customer - Channel', new Set<String>{
                'Customers'
        });
        ff_WrappedFieldDescribeSource src3 = new ff_WrappedFieldDescribeSource(Account.Type, valueToParentMap);
        System.debug(src3.getWrappers());

        // checkbox translated to Yes/No values
        ff_WrappedFieldDescribeSource src4 = new ff_WrappedFieldDescribeSource(Contact.HasOptedOutOfEmail, 'Yes', 'No');
        System.debug(src4.getWrappers());

    }

    @IsTest
    private static void testRichText() {
        ff_RichTextStore__c rt = new ff_RichTextStore__c(Name__c = 'foo');
        insert rt;
        ff_WrappedRichtextSource src1 = new ff_WrappedRichtextSource('foo');
        System.debug(src1.getWrappers());
    }

}