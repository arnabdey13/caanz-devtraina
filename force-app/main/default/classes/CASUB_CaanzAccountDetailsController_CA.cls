/*------------------------------------------------------------------------------------
Author:        Paras Prajapati
Company:       Salesforce
Description:   Address details component controller

History
Date            Author             Comments
--------------------------------------------------------------------------------------
03-25-2019     Paras Prajapati         Initial Release
05-06-2019     Mayukhman               Update the Subscription based Concession
------------------------------------------------------------------------------------*/
public with sharing class CASUB_CaanzAccountDetailsController_CA {
    private static List<String> FIELDS_ACCOUNT;
    private static List<String> FIELDS_CONTACT;
    private static Map<String, Schema.SObjectType> GLOBAL_DESCRIBE;
    
    static{
        FIELDS_ACCOUNT = new List<String>{
            'Member_ID__c', 'Salutation', 'FirstName', 'LastName', 'Middle_Name__c', 'Designation__c', 'Gender__c',
                'Financial_Category__c', 'Membership_Class__c', 'Membership_Approval_Date__c', 
                'Preferred_Name__c','Opt_in_for_LinkedIn_Learning__c','NMP__c'
                };
                    FIELDS_CONTACT = new List<String>{
                        'Email', 'MobilePhone', 'HomePhone','OtherPhone', 'Birthdate'
                            };
                                GLOBAL_DESCRIBE = Schema.getGlobalDescribe();
    }
    
    @AuraEnabled
    public static String getAccountData(){
        try{
            return JSON.serialize(getPersonAccountData(new PersonAccount()));
        } catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }
    }
    
    /***
*  Add the values in ConfigurationSetUp Class that need required for us to send to 
*/
    public class ConfigurationSetUp{
        @AuraEnabled public List<CASUB_Concession_Setting__mdt> listOf_CASUB_Concession_Setting;
    }
    
    @AuraEnabled
    public static String saveAccountData(String accountDataJSONString){
        try{
            System.debug('accountDataJSONString' + JSON.serialize(accountDataJSONString));
            
            PersonAccount personAccount = (PersonAccount)JSON.deserialize(accountDataJSONString, PersonAccount.class);
            System.debug('user' + personAccount.user);
            System.debug('account' + personAccount.account);
            System.debug('personAccount' + personAccount.contact); 
            update personAccount.user; 
            update personAccount.contact; 
            update personAccount.account; 
            return JSON.serialize(personAccount);
        } catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }
    }
    
    private static PersonAccount getPersonAccountData(PersonAccount personAccount){
        String query = 'SELECT Id, PersonContactId, ' + String.join(FIELDS_ACCOUNT, ',');
        query +=  ',Person' + String.join(FIELDS_CONTACT, ',Person') + ' FROM Account';
        query += ' WHERE Id IN (SELECT AccountId FROM User WHERE Id =' + '\'' + UserInfo.getUserId() + '\')';
        System.debug('*** Person Account read - '+query);
        List<Account> accounts = Database.query(query);
        System.debug('*** accounts read - '+accounts);
        if(!accounts.isEmpty()){
            personAccount.setAccount(accounts.get(0));
        }
        System.debug('*** personAccount read - '+personAccount);
        return personAccount;
    }
    
    private class PersonAccount{
        public User user;
        public Account account;
        public Contact contact;
        public Map<String, FieldDescribe> accountFields, contactFields;
        public String profileName;
        public PersonAccount(){
            user = [SELECT Id, CommunityNickname FROM User WHERE Id =: UserInfo.getUserId()]; 
            account = new Account();
            contact = new Contact();
            profileName= [Select Name from Profile where Id =: userinfo.getProfileid()].Name;
            accountFields = new Map<String, FieldDescribe>();
            contactFields = new Map<String, FieldDescribe>();
            addAccountFields();
            addContactFields();
        }
        
        public void setAccount(Account account){
            for(String fname : FIELDS_ACCOUNT){
                this.account.put(fname, account.get(fname));
            }
            for(String fname : FIELDS_CONTACT){
                contact.put(fname, account.get('Person' + fname));
            }
            this.account.Id = account.Id;
            contact.Id = account.PersonContactId;
        }
        
        public void addAccountFields(){
            Map<String, Schema.SObjectField> fieldsMap = GLOBAL_DESCRIBE.get('Account').getDescribe().fields.getMap();
            for(String fname : FIELDS_ACCOUNT){
                if(fieldsMap.containsKey(fname)){
                    accountFields.put(fname, new FieldDescribe(fieldsMap.get(fname).getDescribe()));
                }
            }
        }
        
        public void addContactFields(){
            Map<String, Schema.SObjectField> fieldsMap = GLOBAL_DESCRIBE.get('Contact').getDescribe().fields.getMap();
            for(String fname : FIELDS_CONTACT){
                if(fieldsMap.containsKey(fname)){
                    contactFields.put(fname, new FieldDescribe(fieldsMap.get(fname).getDescribe()));
                }
            }
        }
    }
    
    public class FieldDescribe{
        public Boolean isAccessible, isEditable;
        public Schema.DescribeFieldResult describe;
        public FieldDescribe(Schema.DescribeFieldResult describe){
            this.describe = describe;
            isAccessible = describe.isAccessible();
            isEditable = isAccessible && describe.isUpdateable();
            //isAccessible = isEditable = true;
        }
    }
    
    public class FinancialCategoryOptions {
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String value;
        public FinancialCategoryOptions( String label, String value ) {
            this.label = label;
            this.value = value;
        }
    }
    
    @AuraEnabled
    public static List<FinancialCategoryOptions> getFinancialCategoryPicklistValues() {
        String objectName = 'Account';
        String fieldName = 'Financial_Category__c';
        List<FinancialCategoryOptions> financialCategoryOptions = new List<FinancialCategoryOptions>();
        for ( PicklistEntry entry : Schema.getGlobalDescribe().get( objectName ).getDescribe().fields.getMap().get( fieldName ).getDescribe().getPicklistValues() ) {
            financialCategoryOptions.add( new FinancialCategoryOptions( entry.getLabel(), entry.getValue() ) );
        }
        return financialCategoryOptions;
    }
    
    @AuraEnabled
    public static CASUB_Concession_Setting__mdt getConcessionConfigData() {
        CASUB_Concession_Setting__mdt casubConcessionSetting = new CASUB_Concession_Setting__mdt();
        List<CASUB_Concession_Setting__mdt> casub_Concession_SettingList = new List<CASUB_Concession_Setting__mdt>();
        casub_Concession_SettingList = [Select MasterLabel,CASUB_Allowed_Concession__c,CASUB_Concession_Show__c,CASUB_Not_Allowed_Concession__c from CASUB_Concession_Setting__mdt where MasterLabel='CASUB_Concession_Config'];
        if(casub_Concession_SettingList!=null && casub_Concession_SettingList.size()>0){
            casubConcessionSetting = casub_Concession_SettingList.get(0);
        }
        return casubConcessionSetting;

    }
    
    @AuraEnabled
    public static String saveFinancialCategory(String personAccountId,String financialCategory){
        try{
            Account account =  new account();
            account.id = personAccountId;
            account.Financial_Category__c = financialCategory;
            update account;
            System.debug('Updated Account' + account);
            /* Update by Mayukh for User story ARTTB-344-  To update concession on Step 1 in Subscription object */
            String accIdVal = personAccountId;
            String currentYear = CASUB_MainComponent_Controller.getCurrentYear();
            
            List<Subscription__c> existingSubscription = new List<Subscription__c>([SELECT Account__c, Fiscal_Year__c, Contact_Details_URL__c, Contact_Details_Status__c, Obligation_URL__c, Obligation_Status__c, Payment_URL__c, 
                                                                                    Sales_Order_Status__c, Sales_Order__c,  Contact_Details_Sub_Component__c, Obligation_Sub_Components__c, 
                                                                                    Year__c, Member_Age__c, Id, Name ,Concession__c
                                                                                    From Subscription__c Where Year__c =: currentYear AND Account__c =: accIdVal LIMIT 1]);
            
            System.debug('existingSubscription===' + existingSubscription);
            if(existingSubscription.size() > 0){
                if(financialCategory == 'Low income concession'  || financialCategory == 'Career break concession' || financialCategory == 'Retired' ){
                    existingSubscription[0].Contact_Details_Sub_Component__c = 'Concessions';
                    existingSubscription[0].Concession__c = financialCategory;
                } 
                else{
                    existingSubscription[0].Contact_Details_Sub_Component__c = '';
                    existingSubscription[0].Concession__c = financialCategory;
                }
                update existingSubscription;
            }
            
            /* End of code */
            return 'Account Updated';
        } catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }
    }
}