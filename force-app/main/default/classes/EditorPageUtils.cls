public class EditorPageUtils {
    
    public static SObject getRecordFromSaveRequest(SObject toSave, Map<String, Object> properties) {
        Map<Object, Object> fromClient = (Map<Object, Object>)properties.get('RECORD');
        System.debug('fromClient: '+fromClient);
        System.debug('dirty: '+properties.get('DIRTY'));
        
        if (toSave.Id == null) {
            throw new SObjectException('Id for record to be saved not provided');
        }
        
        Map<Object, Object> dirty = (Map<Object, Object>) properties.get('DIRTY');
        if (dirty != NULL) {
            for (Object f : dirty.keySet()) { 
                String fieldName = (String) f;
                if (fieldName.toLowerCase() != 'id' && !isPropertyField(fieldName)) {
                    System.debug('loading sobject: '+fieldName+' -> '+ fromClient.get(fieldName)+ ' ('+getType(fromClient.get(fieldName))+')');
                    toSave.put(fieldName, fromClient.get(fieldName));
                }
            }
        }    
        return toSave;
    }
    
    public static boolean isPropertyField(String fieldName) {
        system.debug('***fieldName***' + fieldName + '---'  + fieldName.startsWith('prop.'));
        return fieldName.startsWith('prop.');
    }
    
    public static Id getUsersAccountId() {
        CommunityAccountOverride__c accountOverride = CommunityAccountOverride__c.getInstance(UserInfo.getUserId());
        if (accountOverride.AccountId__c == null) {
            User currentUser = [select AccountId from User where Id = 
                                :UserInfo.getUserId()];
            return currentUser.AccountId;
        } else {
            return accountOverride.AccountId__c;
        }
    }
    
    // read the countries and states by parsing a static resource containing the Address.settings file from the meta-data api
    public static List<CountryPicklistSetting> getCountriesAndStates() {        
	    StaticResource src = [SELECT Body FROM StaticResource where Name = 'AddressPicklists'];    
		List<CountryPicklistSetting> countries = EditorPageUtils.parseCountriesMetaData(src.Body.toString());
        countries.sort();
        return countries;
    }
        
    //// UTILS FNS

    public static List<CountryPicklistSetting> parseCountriesMetaData(String toParse) {
        DOM.Document doc = new DOM.Document();      
        doc.load(toParse);    
        DOM.XMLNode root = doc.getRootElement();
        List<CountryPicklistSetting> countries = new List<CountryPicklistSetting>();
        for (DOM.XmlNode countryNode : root.getChildElements()[0].getChildElements()) {
            CountryPicklistSetting country = translateXmlToCountry(countryNode);
            if (country != null) { // inactive countries are returned as nulls
	            countries.add(translateXmlToCountry(countryNode));
            }
        }
        return countries;
    }
    
    // Parse through the XML, determine the author and the characters
    private static CountryPicklistSetting translateXmlToCountry(DOM.XmlNode countryNode) {
        CountryPicklistSetting country = new CountryPicklistSetting();
        
        List<SortableList> unsortedStates = new List<SortableList>();

        for (DOM.XmlNode child : countryNode.getChildElements()) {
            if (child.getName() == 'active') {
                if (child.getText() == 'false') {
                    return null;
                }
            } else if (child.getName() == 'label') {
                country.setLabel(child.getText());
            } else if (child.getName() == 'isoCode') {
                country.setCode(child.getText());
            } else if (child.getName() == 'states') {
                
                List<String> state = new List<String>(2);
		        for (DOM.XmlNode stateNode : child.getChildElements()) {
		            if (stateNode.getName() == 'active') {
                        if (stateNode.getText() == 'false') {
                            state = null;
                            break;
                        }
                    } else if (stateNode.getName() == 'integrationValue') {
                        state.set(0, stateNode.getText());
                    } else if (stateNode.getName() == 'label') {
                        state.set(1, stateNode.getText());
                    }
                }
                if (state != null) { // inactive states are null 
                    unsortedStates.add(new SortableList(1, state));
                }                                
            }
        }

        unsortedStates.sort();
        for (SortableList s : unsortedStates) {
            country.addState(s.data);
        }
        
        return country;
    } 
    
    // util class for sorting lists of Objects. you provide an index to a String element in the list for sorting
    private class SortableList implements Comparable {
        private Integer sortIndex;
        private List<String> data;
        private SortableList(Integer index, List<String> data) {
            this.sortIndex = index;
            this.data = data;
        }
        public Integer compareTo(Object compareTo) {
            SortableList other = (SortableList) compareTo;
            String thisVal = this.data.get(sortIndex);
            String otherVal = other.data.get(sortIndex);
            return thisVal.compareTo(otherVal);
    	}
    }
        
    public static string getType(Object o) {
        if(o==null) return '';              // we can't say much about null with our current techniques
        if(o instanceof SObject)            return ((SObject)o).getSObjectType().getDescribe().getName()+''; 
        if(o instanceof Boolean)            return 'Boolean';
        if(o instanceof Id)                 return 'Id';
        if(o instanceof String)             return 'String';
        return 'Object';                    // actually we can't detect maps and sets and maps
    }
}