/**
* @author Steve Buikhuizen, Jannis Bott
* @date 14/11/2016
*
* @group FormsFramework
*
* @description  Generic handler that implements the common processes of read and write.
*               It is made specific by the implementation objects received in its constructor
*/
global with sharing class ff_ServiceHandler {

    private RecordIdProvider recordIdProvider;
    private LoadHandler loader;
    private SaveHandler saver;

    /**
    * @description Constructor
    * @param RecordIdProvider is used to restrict which recordscan be read or written
    * @param Loader is used to supply all data based on the record ids
    */
    global ff_ServiceHandler(RecordIdProvider recordIdProvider, LoadHandler loader, SaveHandler saver) {
        this.recordIdProvider = recordIdProvider;
        this.loader = loader;
        this.saver = saver;
    }

    /**
    * @description Generic load method that creates ReadData object via concrete Loader implementation
    * @params TODO will be used for query generation
    */
    global ff_ReadData load(List<ff_Service.CustomWrapper> params) {
        Map<Schema.sObjectType, Set<Id>> contextIds = recordIdProvider.getContext();

        ff_ReadData result = new ff_ReadData();
        result.records = loader.loadRecordsForRead(contextIds);
        result.wrappers = loader.loadWrappersForRead(contextIds);
        result.properties = loader.loadPropertiesForRead(contextIds);

        return result;
    }

    /**
    * @description Generic upload method used by any concrete service that supports uploads
    * @param parentId the record to which the file should be attached
    * @param attachmentId the attachment id if not adding the first chunk
    * @param fileName the name of the attachment being saved
    * @param chunk the file contents chunk
    * @param contentType the file Content Type sent from the client
    */
    global Id uploadChunk(Id parentId, String attachmentId, String fileName, String chunk, String contentType) {
        Map<Schema.sObjectType, Set<Id>> contextIds = recordIdProvider.getContext();
        Set<Id> allIds = new Set<Id>();
        for (Set<Id> objIds: contextIds.values()) {
            allIds.addAll(objIds);
        }
        if (!allIds.contains(parentId)) {
            throw new AuraHandledException('Access not allowed to parent record');
        }
        System.debug('Uploading ' + fileName + ' to parent: ' + parentId);

        if (attachmentId.length() == 0) {
            Attachment a = new Attachment(
                    ParentId = parentId,
                    Body = EncodingUtil.base64Decode(EncodingUtil.urlDecode(chunk, 'UTF-8')),
                    Name = fileName,
                    ContentType = contentType);
            insert a;
            return a.Id;
        } else {
            appendToFile(attachmentId, EncodingUtil.urlDecode(chunk, 'UTF-8'));
            return Id.valueOf(attachmentId);
        }
    }

    private static void appendToFile(Id fileId, String chunkBase64) {
        Attachment a = [
                SELECT Id, Body
                FROM Attachment
                WHERE Id = :fileId
        ];
        String existingBody = EncodingUtil.base64Encode(a.Body);
        a.Body = EncodingUtil.base64Decode(existingBody + chunkBase64);
        update a;
    }

    private List<SObject> filterIds(ff_WriteData writeData, String prefix, Boolean match) {
        List<SObject> result = new List<SObject>();
        for (String id: writeData.records.keySet()) {
            if (id.startsWith(prefix)) {
                if (match) {
                    result.add(writeData.records.get(id));
                }
            } else {
                if (!match) {
                    result.add(writeData.records.get(id));
                }
            }
        }
        return result;
    }

    private static final Map<StatusCode, String> ERROR_MESSAGES = new Map<StatusCode, String>{
            StatusCode.REQUIRED_FIELD_MISSING => 'This field is required'
    };

    @TestVisible
    private void loadResults(ff_WriteData writeData, List<SObject> saved, ff_WriteResult result, List<Database.SaveResult> saveResults) {
        System.debug('save results: ' + saveResults);
        Integer position = 0;
        for (Database.SaveResult sr: saveResults) {
            SObject savedObject = saved.get(position);
            String savedObjectKey;
            for (String k : writeData.records.keySet()) {
                if (writeData.records.get(k) == savedObject) {
                    savedObjectKey = k;
                    break;
                }
            }
            if (savedObjectKey == NULL) {
                throw new SObjectException('Could not match the saved record');
            } else {
                if (sr.isSuccess()) {
                    result.results.put(savedObjectKey, savedObject);
                } else {
                    List<String> recordErrors = new List<String>();
                    for (Database.Error e : sr.getErrors()) {
                        String errorMessage = ERROR_MESSAGES.containsKey(e.getStatusCode()) ? ERROR_MESSAGES.get(e.getStatusCode()) : e.getMessage();
                        if (e.getFields() == NULL || e.getFields().size() == 0) {
                            recordErrors.add(errorMessage);
                        } else {
                            Map<String, String> fieldErrors = new Map<String, String>();
                            for (String field: e.getFields()) {
                                fieldErrors.put(field, errorMessage);
                            }
                            result.fieldErrors.put(savedObjectKey, fieldErrors);
                        }
                    }
                    if (recordErrors.size() > 0) {
                        result.recordErrors.put(savedObjectKey, recordErrors);
                    }
                }
            }
            position++;
        }
    }

    global ff_WriteResult save(String writeData) {
        // working around http://salesforce.stackexchange.com/questions/55464/sobject-array-parameter-in-lightning-causes-internal-salesforce-com-error-in-ape
        ff_WriteData wd = (ff_WriteData) JSON.deserialize(writeData, ff_WriteData.class);
        return save(wd);
    }

    global ff_WriteResult save(ff_WriteData writeData) {
        System.debug('write data: ' + writeData);
        ff_WriteResult result = new ff_WriteResult();

        Map<Schema.sObjectType, Set<Id>> contextIds = recordIdProvider.getContext();

        List<SObject> toUpsert = filterIds(writeData, 'UPSERT-', true);
        List<SObject> toSave = filterIds(writeData, 'UPSERT-', false);

        // TODO need concrete services to be able to implement custom validation and provide that to the write data
        // WriteResult has fieldErrors and recordErrors for validation errors so could populate these in processUpserts
        // and skip all further work if they are present

        if (toSave != null) {
            toSave = saver.processUpserts(recordIdProvider, toUpsert, toSave);

            List<SObject> toInsert = filterIds(writeData, 'INSERT-', true);
            if (toInsert.size() > 0) {
                System.debug('Inserting: ' + toInsert);
                List<Database.SaveResult> insertResults = saver.doInsert(toInsert);
                System.debug('Insert results: ' + insertResults);
                loadResults(writeData, toInsert, result, insertResults);
            }

            Set<SObject> saveSet = new Set<SObject>(toSave);
            saveSet.removeAll(filterIds(writeData, 'INSERT-', true));
            List<SObject> toUpdate = new List<SObject>();
            for (SObject obj : saveSet) {
                Set<Id> allowedIds = contextIds.get(obj.getSObjectType());
                if (saver.isUpdateAllowed(recordIdProvider, obj)) {
                    toUpdate.add(obj);
                } else {
                    throw new UnsupportedOperationException('Attempting an update of an inaccessible record: ' + obj.getSObjectType() + ' vs ' + obj.Id);
                }
            }
            if (toUpdate.size() > 0) {
                toUpdate = saver.prepareUpdates(recordIdProvider, toUpdate);
                System.debug('Updating: ' + toUpdate);
                List<Database.SaveResult> updateResults = saver.doUpdate(toUpdate);
                loadResults(writeData, toUpdate, result, updateResults);
            }

            // allow query/transform for save results
            saver.enhance(recordIdProvider, result);

        }
        return result;
    }

    global ff_WriteResult deleteRecord(Id id) {
        System.debug('deleting: ' + id);
        ff_WriteResult result = new ff_WriteResult();

        if (saver.isDeleteAllowed(recordIdProvider, id)) {
            SObject record = id.getSobjectType().newSObject(id);
            Database.DeleteResult deleteResult = saver.doDelete(id);
            if (!deleteResult.success) {
                List<String> errors = new List<String>();
                for (Database.error e : deleteResult.errors) {
                    errors.add(e.getMessage());
                }
                result.recordErrors.put(id, errors);
            }
        } else {
            result.recordErrors.put(id, new List<String>{
                    'Record delete refused'
            });
        }

        return result;
    }

    /**
    * @description Interface that must be implemted to restrict record ids for load and save
    */
    global interface RecordIdProvider {
        Map<Schema.sObjectType, Set<Id>> getContext();
    }

        /**
    * @author Steve Buikhuizen
    *
    * @group FormsFramework
    *
    * @description Utility method for reading ids from a provider
    */
    global static Id getProvidedId(RecordIdProvider provider, Schema.sObjectType type, Integer position) {
        Map<Schema.sObjectType, Set<Id>> context = provider.getContext();
        List<Id> idsSorted = new List<Id>(context.get(type));
        idsSorted.sort();
        return idsSorted.get(position);
    }

    global abstract class LoadHandler {
        global virtual Map<String, List<ff_Service.SObjectWrapper>> loadRecordsForRead(Map<Schema.sObjectType, Set<Id>> recordIds) {
            return null;
        }
        global virtual Map<String, List<ff_Service.CustomWrapper>> loadWrappersForRead(Map<Schema.sObjectType, Set<Id>> recordIds) {
            return null;
        }
        global virtual Map<String, Object> loadPropertiesForRead(Map<Schema.sObjectType, Set<Id>> recordIds) {
            return null;
        }
    }

    global abstract class SaveHandler {
        // default upsert handling makes no changes to the upsert records.
        // concrete handlers should
        // 1: check the expected number of upsert/other records
        // 2: ensure ids for all upsert records
        // 3: overwrite the upsert records in the other records so that ids are present (unless an external id already exists)
        global virtual List<sObject> processUpserts(RecordIdProvider idProvider, List<SObject> upsertRecords, List<SObject> otherRecords) {
            return otherRecords;
        }
        // default update preparation makes no changes
        global virtual List<sObject> prepareUpdates(RecordIdProvider idProvider, List<SObject> updateRecords) {
            return updateRecords;
        }
        // default insert operation just returns the results
        global virtual List<Database.SaveResult> doInsert(List<SObject> toSave) {
            return Database.insert(toSave, false);
        }
        // default update predicate. uses id provider to check id is present
        global virtual Boolean isUpdateAllowed(RecordIdProvider idProvider, SObject obj) {
            Set<Id> allowedIds = idProvider.getContext().get(obj.getSObjectType());
            return allowedIds.contains(obj.Id);
        }
        // default update operation just returns the results
        global virtual List<Database.SaveResult> doUpdate(List<SObject> toSave) {
            return Database.update(toSave, false);
        }
        // default update operation just returns the results
        global virtual Database.DeleteResult doDelete(Id toDelete) {
            return Database.delete(toDelete, false);
        }
        // the last step in a write operation, loop through data saved and decorate
        global virtual void enhance(RecordIdProvider idProvider, ff_WriteResult result) {
            for (String key : result.results.keyset()) {
                result.results.put(key, this.enhanceResult(idProvider, result.results.get(key)));
            }
        }
        // default cleanses the saved record of all fields except id to reduce data leaking back to client on update e.g. fields populated by triggers/workflow etc
        global virtual sObject enhanceResult(RecordIdProvider idProvider, SObject record) {
            return record.getSObjectType().newSObject(record.Id);
        }
        // default allows delete of any id in the id provider
        global virtual Boolean isDeleteAllowed(RecordIdProvider idProvider, Id id) {
            SObjectType sot = id.getSobjectType();
            Set<Id> idsForType = idProvider.getContext().get(sot);
            if (idsForType == null) {
                System.debug('Default delete check. No ids found: ' + sot);
                return false;
            } else {
                System.debug('Default delete check for ' + id + ' in ' + idsForType);
                return idsForType.contains(id);
            }
        }
    }


    /**
    * @description utility method that is used by concrete implementations to compose WrapperSources
    */
    global static Map<String, List<ff_Service.CustomWrapper>> loadWrappers(Map<String, ff_WrapperSource> sources) {
        Map<String, List<ff_Service.CustomWrapper>> result = new Map<String, List<ff_Service.CustomWrapper>>();
        for (String wrapperKey : sources.keySet()) {
            result.put(wrapperKey, sources.get(wrapperKey).getWrappers());
        }
        return result;
    }

///////// DEPRECATED CODE ///////// TODO remove this once in a new org

    /**
    * @description OLD Interface, DO NOT USE
    * @deprecated
    */
    global interface Loader {
        Map<String, List<ff_Service.SObjectWrapper>> loadRecordsForRead(Map<Schema.sObjectType, Set<Id>> recordIds);
        Map<String, List<ff_Service.CustomWrapper>> loadWrappersForRead(Map<Schema.sObjectType, Set<Id>> recordIds);
        Map<String, Object> loadPropertiesForRead(Map<Schema.sObjectType, Set<Id>> recordIds);

        Map<Id, sObject> loadForSave(Map<Schema.sObjectType, Set<Id>> recordIds, ff_WriteData writeData);
        SObject prepareRelatedRecord(SObject related, Map<Schema.sObjectType, Set<Id>> recordIds);
    }

    global ff_ServiceHandler(RecordIdProvider recordIdProvider, Loader loader) {
        throw new UnsupportedOperationException('Loader interface is deprecated');
    }

}