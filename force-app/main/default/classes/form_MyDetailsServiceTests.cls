@IsTest(seeAllData=false)
private class form_MyDetailsServiceTests {

    private static final Id recordTypeBusiness = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();

    @IsTest
    private static void employment_EmployerSearch() {
        List<Account> nzicaAccounts = new TestDataAccountBuilder('Business Account')
                .name('Acme').status('Active').memberOf('NZICA')
                .billingAddress('Unit 1, 50 Customhouse Quay', 'Wellington', null, '6011', 'NZ')
                .create(3, true);
        List<Id> accountIds = new List<Id>();
        for (Account a : nzicaAccounts) {
            accountIds.add(a.Id);
        }
        Test.setFixedSearchResults(accountIds);

        Id accountId = TestSubscriptionUtils.getTestAccountId();

        System.runAs(getMemberUser()) {
            form_MyDetailsService.RecordIdProviderImpl idProvider = new form_MyDetailsService.RecordIdProviderImpl(accountId);
            List<Map<String, Object>> searchResults = new form_MyDetailsService.RelaxedSharingEmployerHistory().searchBusinessAccounts(idProvider, 'foo');
            System.assertEquals(3, searchResults.size(), '3 of the 5 accounts returned');
        }
    }

    @IsTest
    private static void employment_EmployerCurrentInsertPrimary() {
        // testing for correct behaviour when saving current/primary employers
        // this covers SFS-622 where the default value on the Primary_Employer__c field was creating bad UX for user

        Account employer = new TestDataAccountBuilder('Business Account')
                .name('Deloitte').status('Active').memberOf('NZICA')
                .billingAddress('Unit 1, 50 Customhouse Quay', 'Wellington', null, '6011', 'NZ')
                .create(1, true).get(0);

        System.runAs(getMemberUser()) {

            Test.startTest();

            SObject h1 = insertEmployer(employer.Id, 'Current', false);
            System.assertEquals(false, h1.get('Primary_Employer__c'),
                    'Primary employer field not checked because was not sent by the form');

            SObject h2 = insertEmployer(employer.Id, 'Current', true);
            System.assertEquals(true, h2.get('Primary_Employer__c'),
                    'Primary employer field is checked because it was sent by the form');

            SObject h3 = insertEmployer(employer.Id, 'Current', true);
            System.assertEquals(true, h3.get('Primary_Employer__c'),
                    'Primary employer field is checked because it was sent by the form');

            // notice that h2 is expected to have been unchecked when h3 was inserted
            checkPrimaries(new Map<Id, Boolean>{
                    h1.Id => false,
                    h2.Id => false,
                    h3.Id => true
            });

            // update the second and make it primary

            /* disabled to workaround governor limits in UAT/Prod. Needs to be tested in a separate method

            form_MyDetailsService service = new form_MyDetailsService(TestSubscriptionUtils.getTestAccountId());
            ff_WriteData wd = new ff_WriteData();
            wd.records.put(h2.Id, new Employment_History__c(
                    Id = h2.Id,
                    Primary_Employer__c = true
            ));
            ff_WriteResult result = service.handler.save(wd);
            System.assertEquals(0, result.fieldErrors.size(), 'no errors during save');
            System.assertEquals(0, result.recordErrors.size(), 'no errors during save');

            // notice that h2 is expected to have been unchecked when h3 was inserted
            checkPrimaries(new Map<Id, Boolean>{
                    h1.Id => false,
                    h2.Id => true,
                    h3.Id => false
            });
            */

            Test.stopTest();
        }
    }

    private static void checkPrimaries(Map<Id, Boolean> expected) {
        form_MyDetailsService service = new form_MyDetailsService(TestSubscriptionUtils.getTestAccountId());
        ff_ReadData withHistoryRecord = service.handler.load(null);
        List<ff_Service.SObjectWrapper> histories = withHistoryRecord.records.get('Employment_History__c');
        System.assertEquals(expected.size(), histories.get(0).sObjectList.size(), 'All 3 inserted records are returned');
        Integer count = 0;
        for (SObject history : histories.get(0).sObjectList) {
            System.debug(count + ': checking ' + history);
            System.assertEquals(expected.get(history.Id), history.get('Primary_Employer__c'),
                    'history record was updated as expected');
            count++;
        }
    }

    private static SObject insertEmployer(Id employerId, String status, Boolean primary) {
        String tempId = 'INSERT-Employment_History__c0';
        ff_WriteData wd = new ff_WriteData();
        Employment_History__c history = new Employment_History__c(
                // member id not passed by client, populated by saver impl
                Employer__c = employerId,
                Job_Title__c = 'Mail Room Deputy Assistant',
                Employee_Start_Date__c = System.today(),
                Status__c = status
        );
        if (status == 'Closed') {
            history.Employee_End_Date__c = System.today();
        }
        if (primary) {
            history.Primary_Employer__c = true;
        }
        wd.records.put(tempId, history);

        form_MyDetailsService service = new form_MyDetailsService(TestSubscriptionUtils.getTestAccountId());
        ff_WriteResult result = service.handler.save(wd);
        System.assertEquals(0, result.fieldErrors.size(), 'no errors during save');
        System.assertEquals(0, result.recordErrors.size(), 'no errors during save');
        return result.results.get(tempId);
    }

    @IsTest
    private static void employment_EmployerOptIn() {
        Account employer = new TestDataAccountBuilder('Business Account')
                .name('Deloitte').status('Active').memberOf('NZICA')
                .billingAddress('Unit 1, 50 Customhouse Quay', 'Wellington', null, '6011', 'NZ')
                .create(1, true).get(0);

        Id accountId = TestSubscriptionUtils.getTestAccountId();

        System.runAs(getMemberUser()) {
            String tempId = 'INSERT-Employment_History__c2';
            ff_WriteData wd = new ff_WriteData();
            wd.records.put(tempId,
                    new Employment_History__c(
                            // member id not passed by client, populated by saver impl
                            Employer__c = employer.Id,
                            Job_Title__c = 'Mail Room Deputy Assistant',
                            Employee_Start_Date__c = System.today(),
                            Employee_End_Date__c = System.today(),
                            Status__c = 'Closed',
                            Work_Hours__c = 'Full Time'
                    ));

            form_MyDetailsService service = new form_MyDetailsService(accountId);
            ff_WriteResult result = service.handler.save(wd);
            System.assertEquals(0, result.fieldErrors.size(), 'no errors during save');
            System.assertEquals(0, result.recordErrors.size(), 'no errors during save');

            Id newHistoryId = result.results.get(tempId).Id;

            service = new form_MyDetailsService(accountId);
            ff_ReadData withHistoryRecord = service.handler.load(null);
            List<ff_Service.SObjectWrapper> histories = withHistoryRecord.records.get('Employment_History__c');

            // checking security access to history records
            System.assertEquals(1, histories.get(0).sObjectList.size(),
                    'created record can be read after create');

            // ensure member can update employment history records
            service = new form_MyDetailsService(accountId);
            wd = new ff_WriteData();
            wd.records.put(newHistoryId, new Employment_History__c(
                    Id = newHistoryId,
                    Job_Title__c = 'Mail Room Boss Person'));
            result = service.handler.save(wd);
            System.debug('Update result: ' + result);
            System.assertEquals(0, result.fieldErrors.size(), 'no field errors from update');
            System.assertEquals(0, result.recordErrors.size(), 'no record errors from update');

            // check that update was saved
            service = new form_MyDetailsService(accountId);
            ff_ReadData withHistoryUpdate = service.handler.load(null);
            histories = withHistoryUpdate.records.get('Employment_History__c');
            System.assertEquals('Mail Room Boss Person', histories.get(0).sObjectList.get(0).get('Job_Title__c'),
                    'update was made and is seen in subsequent read');

            // ensure member can delete a history record
            service = new form_MyDetailsService(accountId);
            result = service.handler.deleteRecord(newHistoryId);
            System.debug('Delete result: ' + result);
            System.assertEquals(0, result.fieldErrors.size(), 'no field errors from delete');
            System.assertEquals(0, result.recordErrors.size(), 'no record errors from delete');
        }
    }

    private static User getMemberUser() {
        Id contactId = [SELECT Id FROM Contact WHERE AccountId = :TestSubscriptionUtils.getTestAccountId()].Id;
        return [select Id from User where ContactId = :contactId];
    }

    @testSetup static void createTestData() {
        TestSubscriptionUtils.createTestData();

        Environment_Ids__c ownerId = new Environment_Ids__c(
                Name = 'Employer_Opt_Out_Owner', Id__c = UserInfo.getUserId());
        System.debug('Inserting custom setting: ' + ownerId);
        insert ownerId;

        TestDataFFRichTextBuilder.multiple(RICH_TEXT_NAMES, 'fake content', true);
    }

    private static final List<String> RICH_TEXT_NAMES = new List<String>{
            'MyDetails.PersonalInformation',
            'MyDetails.ResidentialAddress',
            'MyDetails.MailingAddress'
    };
}