public class AnalyticsTrackerService {

    private static final Set<String> ENV_CONFIG = new Set<String>{'asset', 'env'};

    @AuraEnabled
    public static Map<String, String> load() {
        return load(EditorPageUtils.getUsersAccountId());
    }

    @TestVisible
    private static Map<String, String> load(Id accountId) {
        Map<String, String> result = new Map<String, String>();

        for (Analytics_Config__mdt config : [select DeveloperName, Value__c, Mapping__c from Analytics_Config__mdt]) {
            if (ENV_CONFIG.contains(config.DeveloperName)) {
                result.put(config.DeveloperName, config.Value__c);
            } else {
                result.put(config.Value__c, config.Mapping__c);
            }
        }

        if (result.get('asset') != NULL) {

            // user specific config
            result.put('userId', UserInfo.getUserId());
            result.put('userName', UserInfo.getUserName());

            Account account = [select Id, Membership_Type__c from Account where Id = :accountId];
            result.put('membershipType', account.Membership_Type__c);

            return result;
        } else {
            // no data when no asset to bootstrap DTM client
            return new Map<String, String>();
        }

    }
    
    
}