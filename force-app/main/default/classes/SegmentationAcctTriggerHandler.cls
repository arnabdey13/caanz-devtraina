/**
*	Project:			CAANZ Internal
*	Project Name: 		Segmentation
*	Project Task Name: 	Account trigger Handler After Update
*	Developer: 			akopec
*	Initial Date: 		May 2017
*	
*   Description: 		Change to Account Address, triggers update of the Segmentation Geo Confidence Flag to Low (Low-Med)
*						for the correspondent Account ID. To minimize impact a futrue method executed for the final update.
*
**/
public without sharing class SegmentationAcctTriggerHandler {
    
    // Protection from Workflows and other Triggers
    private static Boolean alreadyProcessed = false;
       
    // return confidence = "Low" unless difference in postcodes is smaller equal 5 than return "Low-Med"
   	@TestVisible private static String calcAddressChangeConfidence (String newPostalCode, String oldPostalCode){
   		String status = 'Low';
   		if ( !String.isBlank(newPostalCode) && !String.isBlank(oldPostalCode) ) {
   			try {
   				if ( math.abs ( Integer.valueof(newPostalCode) - Integer.valueof (oldPostalCode) ) <= 5 )
                	status = 'Low-Med'; 
   			}
            // ignore if not numeric and assume "Low" confidence
            catch ( Exception e) {}
   		}
   		return status;
   	}
   
     // static call to determine a list of Accounts that will require update of segmentation object	
     public static Map<Id, String> handleAfterUpdate(List<Account> newList, Map<Id, Account> newMap, List<Account> oldList, Map<Id, Account> oldMap){ 

        Map<Id, String> SegmentAcctIdsToUpdate = new Map<Id, String>{};
        String newConfidenceFlag = '';
        
        // don't run it again (workflows could trigger it again)
        if (alreadyProcessed) {
            return SegmentAcctIdsToUpdate;
         }
         
         
        for(Account acc: newList){
            // different set of Account Addresses for Person and Business
        	if(acc.isPersonAccount){
                // Logic for residentail address changes only if residential address is or was populated
                if (oldMap.containsKey(acc.Id) && (   !String.isBlank(oldMap.get(acc.Id).PersonOtherPostalCode) 
                                                   || !String.isBlank(acc.PersonOtherPostalCode) )
                    						   && oldMap.get(acc.Id).PersonOtherPostalCode != acc.PersonOtherPostalCode ) { 
                                                   
					newConfidenceFlag = calcAddressChangeConfidence (acc.PersonOtherPostalCode , oldMap.get(acc.Id).PersonOtherPostalCode);
                   	SegmentAcctIdsToUpdate.put(acc.Id, newConfidenceFlag);  
        		}
                // Logic for mailing address changes only if residential address is not populated
                // Typically Non Members and Students
                if (oldMap.containsKey(acc.Id) && String.isBlank(oldMap.get(acc.Id).PersonOtherPostalCode)
                    						   && String.isBlank(acc.PersonOtherPostalCode )
                    						   && ( !String.isBlank(oldMap.get(acc.Id).PersonMailingPostalCode) 
                                                 || !String.isBlank(acc.PersonMailingPostalCode) )                  
                    						   && oldMap.get(acc.Id).PersonMailingPostalCode != acc.PersonMailingPostalCode ) {
                                                   
                    newConfidenceFlag = calcAddressChangeConfidence (acc.PersonMailingPostalCode , oldMap.get(acc.Id).PersonMailingPostalCode);
                   	SegmentAcctIdsToUpdate.put(acc.Id, newConfidenceFlag);      
        		}
            }
            // Add else logic for Businesses			
        }
        
       // Mark as processed if not empty
       if ( SegmentAcctIdsToUpdate.size() > 0) {
          	alreadyProcessed = true;
        }
         
       	return SegmentAcctIdsToUpdate;   	
    }
    
    // the actual update of Segmentation Object based on already defined list	
    @future 
    public static void handleAfterUpdateAsync(Map <Id, String> SegmentAcctToUpdate){         
                
        if ( SegmentAcctToUpdate.isEmpty()) {
           return;   
        }

            
		// do not update records that currently do not have geo segmentation (rare), Geo_Confidence_Flag__c != NULL
        List<Segmentation__c> segmentsUpdate  = [select Id, Geo_Confidence_Flag__c, Account__c
        										   from Segmentation__c 
        										  where Account__c IN :SegmentAcctToUpdate.keySet()
        										    and Geo_Confidence_Flag__c != NULL ];
        
        if ( segmentsUpdate.isEmpty())
            return; 
        
        // we want to update only the Geo Confidence Flag and do not touch other fields
        List<Segmentation__c> segmentsToUpdateList = new List <Segmentation__c>();
				
        for ( Segmentation__c seg : segmentsUpdate){
            Segmentation__c newSegment = new Segmentation__c();
            newSegment.Id = seg.Id;
            newSegment.Geo_Confidence_Flag__c = SegmentAcctToUpdate.get(seg.Account__c);
            segmentsToUpdateList.add(newSegment);
        }
        
        if (segmentsToUpdateList.size() > 0 )
        	update segmentsToUpdateList;      
    } 
}