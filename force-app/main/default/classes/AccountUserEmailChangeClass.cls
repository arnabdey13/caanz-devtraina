public without sharing class AccountUserEmailChangeClass {
    
    public static Boolean isEmailUpdateRunning = true;
    
    public static void personAccEmailUpdate(List<Account> personAccList, Map<Id, Account> mapContactOld) {
        system.debug('***IN personAccEmailUpdate METHOD***'+AccountUserEmailChangeClass.isEmailUpdateRunning);
        if(AccountUserEmailChangeClass.isEmailUpdateRunning){
            AccountUserEmailChangeClass.isEmailUpdateRunning = false ;
            Map<Id, String> contactIdEmailMap = new Map<Id, String>();
            List<User> userUpdateList = new List<User>();
            for(Account conRec : personAccList){
                if(String.isNotEmpty(conRec.PersonEmail) && conRec.PersonEmail != mapContactOld.get(conRec.Id).PersonEmail){
                    contactIdEmailMap.put(conRec.PersonContactId, conRec.PersonEmail);
                }
            }
            system.debug('***IN contactIdEmailMap***'+ contactIdEmailMap);
            if(!contactIdEmailMap.isEmpty()){           
                for(User userRec : [SELECT ContactId FROM User WHERE ContactId IN : contactIdEmailMap.keySet()]){
                    userRec.Email = contactIdEmailMap.get(userRec.ContactId);
                    userUpdateList.add(userRec);
                }            
            }
            if(!userUpdateList.isEmpty()){
                Database.update(userUpdateList);            
            }
        }
    }
}