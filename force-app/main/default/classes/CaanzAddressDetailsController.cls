/*------------------------------------------------------------------------------------
Author:        Sumit Gupta
Company:       Salesforce
Description:   Address details component controller

History
Date            Author             Comments
--------------------------------------------------------------------------------------
16-05-2018     Sumit Gupta       	Initial Release
------------------------------------------------------------------------------------*/
public with sharing class CaanzAddressDetailsController {

    private static Integer DEFAULT_SEARCH_LIMIT = 10;

	@AuraEnabled
    public static String getAddressData(Id contactId){
    	return JSON.serialize(new AddressData(getPersonAccountData(contactId)));
    }

    @AuraEnabled
    public static String getCountryAndStateOptions(){
        return JSON.serialize(getCountryStateOptions());
    }

    @AuraEnabled
    public static String saveAddressData(String addressDataJSONString){
    	AddressData addressData = (AddressData)JSON.deserialize(addressDataJSONString, AddressData.class);
    	if(addressData.account != null) update addressData.account; 
    	if(addressData.contact != null) update addressData.contact; 
    	return JSON.serialize(addressData);
    }

    @AuraEnabled
    public static String searchAddress(String searchTerm, String countryCode, Integer searchLimit){
        searchLimit = searchLimit != null ? searchLimit : DEFAULT_SEARCH_LIMIT;
        return !Test.isRunningTest() ? JSON.serialize(EDQAddressService.searchAddress(searchTerm, countryCode, searchLimit)) : '';
    }

    @AuraEnabled
    public static String formatAddress(String addressId, String countryCode){
        return JSON.serialize(EDQAddressService.formatAddress(addressId, countryCode));
    }

    private static Contact getPersonAccountData(Id contactId){
    	if(contactId == null){
    		contactId = [SELECT ContactId FROM User WHERE Id =: UserInfo.getUserId()].ContactId;
    	}
    	if(contactId == null) return null;
    	return [
    		SELECT OtherStreet, OtherCity, OtherState, OtherCountry, OtherStateCode, OtherCountryCode, OtherPostalCode,
    			MailingStreet, MailingCity, MailingState, MailingCountry, MailingStateCode, MailingCountryCode, MailingPostalCode,
    			Account.Mailing_and_Residential_is_the_same__c, Account.Mailing_Company_Name__c
    		FROM Contact WHERE Id =: contactId LIMIT 1
    	];
    }

    private static List<Country> getCountryStateOptions() {
        List<Country> countries = getCountryPicklistValues();
        List<State> states = getStatePicklistValues();
        Map<Integer, Country> countriesMap = new Map<Integer, Country>();
        for (State state : states) {
            for(Integer index : state.getCountryIndexes()){
                if(index < countries.size()){
                    countries.get(index).addState(state);
                }
            }
            state.validFor = null;
        } 
        return countries;
    }

    /*private static List<CountryPicklistSetting> getCountryAndStateValues(){
        Map<String, String> validCountryCodes = new Map<String, String>(), validStateCodes = new Map<String, String>();
        List<CountryPicklistSetting> countryStateOptions = new List<CountryPicklistSetting>();

        for(Schema.PicklistEntry f : User.Countrycode.getDescribe().getPicklistValues()){
            validCountryCodes.put(f.getLabel(), f.getValue()); validCountryCodes.put(f.getValue(), f.getValue());
        }
        for(Schema.PicklistEntry f : User.statecode.getDescribe().getPicklistValues()){
            validStateCodes.put(f.getLabel(), f.getValue()); validStateCodes.put(f.getValue(), f.getValue());
        }
        for(CountryPicklistSetting setting : EditorPageUtils.getCountriesAndStates()){
            String countryCode = setting.getCountryCode(), countryLabel = setting.getCountryLabel();
            if(validCountryCodes.containsKey(countryCode) || validCountryCodes.containsKey(countryLabel)){
                CountryPicklistSetting newSetting = new CountryPicklistSetting();
                newSetting.setCode(validCountryCodes.containsKey(countryCode) ? validCountryCodes.get(countryCode) : validCountryCodes.get(countryLabel));
                newSetting.setLabel(setting.getCountryLabel());
                for(List<String> stateValues : setting.getStates()){
                    String stateCode = stateValues.get(0), stateLabel = stateValues.get(1);
                    if(validStateCodes.containsKey(stateCode) || validStateCodes.containsKey(stateLabel)){
                        stateCode = validStateCodes.containsKey(stateCode) ? validStateCodes.get(stateCode) : validStateCodes.get(stateLabel);
                        newSetting.addState(new List<String> { stateCode, stateLabel });
                    }
                }
                countryStateOptions.add(newSetting);
            }
        }
        return countryStateOptions;
    }*/

    private static final String base64Chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' + 'abcdefghijklmnopqrstuvwxyz' + '0123456789+/';

    private static List<Country> getCountryPicklistValues() {
        return (List<Country>) JSON.deserialize(JSON.serialize(User.CountryCode.getDescribe().getPicklistValues()), List<Country>.class);
    }

    private static List<State> getStatePicklistValues() {
        return (List<State>) JSON.deserialize(JSON.serialize(User.StateCode.getDescribe().getPicklistValues()), List<State>.class);
    }

    public class AddressData{
        Account account;
        Contact contact;

        public AddressData(Contact contact){
            this.account = new Account(
                Id = contact.Account.Id, 
                Mailing_and_Residential_is_the_same__c = contact.Account.Mailing_and_Residential_is_the_same__c,
                Mailing_Company_Name__c = contact.Account.Mailing_Company_Name__c
            );  
            this.contact = new Contact(
                Id = contact.Id, OtherStreet = contact.OtherStreet, OtherCity = contact.OtherCity, 
                OtherState = contact.OtherState, OtherStateCode = contact.OtherStateCode, 
                OtherCountry = contact.OtherCountry, OtherCountryCode = contact.OtherCountryCode, 
                OtherPostalCode = contact.OtherPostalCode, MailingStreet = contact.MailingStreet, 
                MailingCity = contact.MailingCity, MailingState = contact.MailingState, 
                MailingStateCode = contact.MailingStateCode, MailingCountry = contact.MailingCountry, 
                MailingCountryCode = contact.MailingCountryCode, MailingPostalCode = contact.MailingPostalCode
            );
        }
    }

    private class Country {
        public String label;
        public String value;
        public List<State> states;

        public void addState(State state){
            if(states == null) states = new List<State>(); 
            states.add(state);
        }
    }

    private class State {
        public String label;
        public String value;
        public String validFor;

        private List<Integer> getCountryIndexes(){
            List<Integer> indexes = new List<Integer>();
            String validForBits = base64ToBits(validFor);
            for (Integer i = 0; i < validForBits.length(); i++) {
                String bit = validForBits.mid(i, 1);
                if (bit == '1') indexes.add(i); 
            }
            return indexes;
        }

        private String base64ToBits(String validFor) {
            if(String.isEmpty(validFor)) return '';
            String validForBits = '';
            for (Integer i = 0; i < validFor.length(); i++) {
                String thisChar = validFor.mid(i, 1);
                Integer val = base64Chars.indexOf(thisChar);
                String bits = decimalToBinary(val).leftPad(6, '0');
                validForBits += bits;
            }
            return validForBits;
        }

        private String decimalToBinary(Integer val) {
            String bits = '';
            while (val > 0) {
                Integer remainder = Math.mod(val, 2);
                val = Integer.valueOf(Math.floor(val / 2));
                bits = String.valueOf(remainder) + bits;
            }
            return bits;
        }
    }
}