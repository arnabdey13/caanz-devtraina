/**
    Developer: WDCi (Terry)
    Development Date: 27/02/2019
    Task #: Controller for Luana Request Management
    
    Change History:
    TEQSA change 26/02/2019: Include flag to Hide AM SP from Community
**/
public without sharing class luana_RequestController {
    Id custCommConId;
    Id custCommAccId;
    
    public Case cs {set; get;}
    public List<SPWrapper> spWrapList {set; get;}
    public Id selectedSPId {set; get;}
    public Id selectedPOId {set; get;}
    public Id selectedCAId {set; get;}
    public Attachment att {get; set;}
    
    //LCA-376 support multiple upload
    public Attachment additionalAtt1 {get; set;}
    public Attachment additionalAtt2 {get; set;}
    public Attachment additionalAtt3 {get; set;}
    public Attachment additionalAtt4 {get; set;}
    
    private Map<String, Id> termNCondMap = new Map<String, Id>();
        
    public luana_RequestController(ApexPages.StandardController controller) {
        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId()); 
        this.custCommConId = commUserUtil.custCommConId;       
        this.custCommAccId = commUserUtil.custCommAccId;  
        
        //For issue LCA-293 add assignment rule
        AssignmentRule AR = new AssignmentRule();
        AR = [select id, Name from AssignmentRule where SobjectType = 'Case' and Active = true];
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId= AR.id;
        
        cs = new Case();
        cs.Status = 'New';
        cs.Origin = 'Portal';
        cs.ContactId = custCommConId;
        
        cs.setOptions(dmlOpts);
        att = new Attachment();
        
        Set<String> termCondName = new Set<String>();
        termCondName.add('T&C: Discontinuation Additional Info');
        termCondName.add('T&C: Discontinuation T&C');
        termCondName.add('T&C: Exemption Additional Info');
        termCondName.add('T&C: Exemption T&C');
        termCondName.add('T&C: Competency Area T&C');
        
        for(Terms_and_Conditions__c termNConds: [Select Id, Name from Terms_and_Conditions__c Where Name in: termCondName]){
            termNCondMap.put(termNConds.Name, termNConds.Id);
        }
        
        //LCA-344
        if(ApexPages.currentPage().getParameters().containsKey('poid')){
            selectedPOId = ApexPages.currentPage().getParameters().get('poid');
        }
        
        //LCA-376
        additionalAtt1 = new Attachment();
        additionalAtt2 = new Attachment();
        additionalAtt3 = new Attachment();
        additionalAtt4 = new Attachment();
    }
    
    public void uploadAttToCase(Case cs) {
        
        List<Attachment> newAtts = new List<Attachment>();
        
        if(cs != null){
            if(att.body != null){
                Attachment newAtt = att.clone(false, true);
                newAtt.ParentId = cs.Id;
                
                newAtts.add(newAtt);
            }
            
            if(additionalAtt1.body != null){
                Attachment newAtt = additionalAtt1.clone(false, true);
                newAtt.ParentId = cs.Id;
                
                newAtts.add(newAtt);
            }
            
            if(additionalAtt2.body != null){
                Attachment newAtt = additionalAtt2.clone(false, true);
                newAtt.ParentId = cs.Id;
                
                newAtts.add(newAtt);
            }
            
            if(additionalAtt3.body != null){
                Attachment newAtt = additionalAtt3.clone(false, true);
                newAtt.ParentId = cs.Id;
                
                newAtts.add(newAtt);
            }
            
            if(additionalAtt4.body != null){
                Attachment newAtt = additionalAtt4.clone(false, true);
                newAtt.ParentId = cs.Id;
                
                newAtts.add(newAtt);
            }
            
            if(newAtts.size() > 0){
                
                insert newAtts;
                
                att = new Attachment();
                additionalAtt1 = new Attachment();
                additionalAtt2 = new Attachment();
                additionalAtt3 = new Attachment();
                additionalAtt4 = new Attachment();
            }
            
            
        }
    }
        
    public PageReference SaveME() {
        if(cs.Accept_Terms_And_Condition__c){
            RecordType rt = new RecordType(Name=luana_RequestConstants.REQ_EXEMPTION_RT_NAME);
            cs.RecordType = rt;
            cs.Subject = 'Module Exemption Request';            
            cs.Module__c = selectedPOId;

            insert cs;
            uploadAttToCase(cs);
            
            //Fix for issue LCA-396
            insertTermNCondAttachment(new List<String>{'T&C: Exemption T&C'}, cs.Id, cs.Accept_Terms_and_Condition__c);
            
            return Page.luana_MyRequest;
        }else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure that the "Accept Terms and Conditions" are checked.'));
            return null;
        }
    }
    
    // FlexiPath - Request Competency Area
    public PageReference SaveCA() {
    
        //if(cs.Accept_Terms_And_Condition__c){

            //uploadAttToCase(cs);
            
            if (att.body != null) {
                
                FP_Competency_Area__c caRecord = [SELECT Id, Name FROM FP_Competency_Area__c WHERE Id = :selectedCAId];                
                
                RecordType rt = new RecordType(Name=luana_RequestConstants.REQ_COMPETENCY_RT_NAME);
                cs.RecordType = rt;
                cs.Subject = 'Competence Area Request: ' +caRecord.Name;            
                cs.FP_Requested_Competency_Area__c = selectedCAId;
                insert cs;
                
                uploadAttToCase(cs);
                //insertTermNCondAttachment(new List<String>{'T&C: Competency Area T&C'}, cs.Id, cs.Accept_Terms_and_Condition__c); 
                return Page.luana_MyRequest;
            } else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure that the suppporting document is attached.'));
                return null;      
            }

            
        /* }else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure that the "Accept Terms and Conditions" are checked.'));
            return null;
        } */ 
    }
    
    public PageReference SaveDW() {
        //LCA-405 remove the 'Accept Terms and Condition' checkbox
        if(cs.Accept_Fee__c){
            RecordType rt = new RecordType(Name=luana_RequestConstants.REQ_DIS_RT_NAME);
            cs.RecordType = rt;
            cs.Subject = 'Module Cancellation/Discontinuation Request';
            cs.Student_Program__c = selectedSPId;
            
            insert cs;
            uploadAttToCase(cs);
            
            //Fix for issue LCA-396, change again on LCA-405
            insertTermNCondAttachment(new List<String>{'T&C: Discontinuation Additional Info'}, cs.Id, true);
            
            return Page.luana_MyRequest;
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure that the "Accept Fee" are checked.'));
            return null;
        }
    }
    
    public PageReference SaveOOAE() {
        // LCA-285
        //if(att.body != null){
            RecordType rt = new RecordType(Name=luana_RequestConstants.REQ_OOAE_RT_NAME);
            cs.RecordType = rt;
            cs.Subject = 'Out of Approved Employment Request';                        
            //cs.Student_Program__c = selectedSPId; //LCA-316/LCA-321
            cs.Module__c = selectedPOId; //LCA-316/LCA-321
            
            insert cs;
            uploadAttToCase(cs);
            return Page.luana_MyRequest;
        //} else {
            
        //    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Supporting document is required.'));
        //    return null;
        //}
    }
    
    public PageReference SaveSC() {
        RecordType rt = new RecordType(Name=luana_RequestConstants.REQ_SC_RT_NAME);
        cs.RecordType = rt;
        cs.Subject = 'Special Consideration Request';                                   
        cs.Student_Program__c = selectedSPId;
        
        insert cs;
        uploadAttToCase(cs);
        return Page.luana_MyRequest;
    }
    
    public PageReference SaveRA() {
        RecordType rt = new RecordType(Name=luana_RequestConstants.REQ_ASSISTANCE_RT_NAME);
        cs.RecordType = rt;
        cs.Subject = 'Request Assistance Request';                                    
        cs.Student_Program__c = selectedSPId;

        insert cs;
        uploadAttToCase(cs);
        return Page.luana_MyRequest;
    }
    
    public List<SelectOption> getMyModuleEnrolments() {
        List<SelectOption> options = new List<SelectOption>();
        //LCA-436, add extra filter logic
        // TEQSA change: Include flag to Hide AM SP from Community
        for(LuanaSMS__Student_Program__c sp : [SELECT Id, LuanaSMS__Course__r.Name, Name FROM LuanaSMS__Student_Program__c WHERE Hide_from_Community__c=false and LuanaSMS__Contact_Student__c =: custCommConId AND 
                                               RecordType.DeveloperName =: luana_EnrolmentConstants.RECORDTYPE_STUDENTPROGRAM_ACCREDITEDMODULE and 
                                               (LuanaSMS__Course__r.Special_Consideration_Close_Date__c  > TODAY OR LuanaSMS__Course__r.Special_Consideration_Close_Date__c =null) ]) {
            SelectOption option = new SelectOption(sp.Id, sp.LuanaSMS__Course__r.Name + ' (' + sp.Name + ')');
            options.add(option); 
        }
        return options;
    }
    
    // FlexiPath project - Request Competency Area
    public List<SelectOption> getActiveCompetencyAreas() {
        List<SelectOption> options = new List<SelectOption>();
        for(FP_Competency_Area__c ca : [SELECT Id, Name FROM FP_Competency_Area__c WHERE FP_Active__c = true AND FP_Parent_Competency_Area__c = null AND FP_Display_Only__c = FALSE ORDER BY Name ASC]) {
            SelectOption option = new SelectOption(ca.Id, ca.Name);
            options.add(option); 
        }
        return options;
    }
    
    public List<SelectOption> getMyInProgressModuleEnrolments() {
        List<SelectOption> options = new List<SelectOption>();
        // TEQSA change: Include flag to Hide AM SP from Community
        for(LuanaSMS__Student_Program__c sp : [SELECT Id, LuanaSMS__Course__r.Name, Name FROM LuanaSMS__Student_Program__c WHERE Hide_from_Community__c=false and LuanaSMS__Contact_Student__c =: custCommConId AND LuanaSMS__Status__c = 'In Progress' AND RecordType.DeveloperName =: luana_EnrolmentConstants.RECORDTYPE_STUDENTPROGRAM_ACCREDITEDMODULE]) {
            SelectOption option = new SelectOption(sp.Id, sp.LuanaSMS__Course__r.Name + ' (' + sp.Name + ')');
            options.add(option); 
        }
        return options;
    }
    
    public List<SelectOption> getModulePOs() {
        List<SelectOption> options = new List<SelectOption>();
        for(LuanaSMS__Program_Offering__c po : [SELECT Id, Name FROM LuanaSMS__Program_Offering__c WHERE RecordType.DeveloperName =: luana_EnrolmentConstants.RECORDTYPE_PROGRAMOFFERING_ACCREDITEDMODULE]) {
            // LCA-560
            //if(po.Name != 'CAP') {
                SelectOption option = new SelectOption(po.Id, po.Name);
                options.add(option); 
            //}
            
            
        }
        return options;
    }
    
    public List<SPWrapper> getSPWList() {
    
        spWrapList = new List<SPWrapper>();
        for(LuanaSMS__Student_Program__c sp : [SELECT Id, LuanaSMS__Course__r.Name FROM LuanaSMS__Student_Program__c WHERE LuanaSMS__Contact_Student__c =: custCommConId AND RecordType.Name =: luana_EnrolmentConstants.RECORDTYPE_STUDENTPROGRAM_ACCREDITEDMODULE]) {
            spWrapList.add(new SPWrapper(false, sp));
        }    
        return spWrapList;
    }
    
    class SPWrapper {
        public Boolean isSelected {set; get;}
        public LuanaSMS__Student_Program__c sp {set; get;}
        
        public SPWrapper(Boolean isSelected, LuanaSMS__Student_Program__c sp) {
            this.isSelected = isSelected;
            this.sp = sp;
        }
    }
    
    public void insertTermNCondAttachment(List<String> termNConNames, Id parentId, Boolean accepted){
        if(accepted){
        
            List<Attachment> atts = new List<Attachment>();
            for(String str: termNConNames){
                Attachment newAtt = tcAttachment(str).clone(false, true);
                newAtt.ParentId = parentId;
                atts.add(newAtt);
            }
            insert atts;
        }
    }
    
    
    public Attachment getDiscontinuationAdditionalInfo(){
        return tcAttachment('T&C: Discontinuation Additional Info');
    }
    
    public Attachment getDiscontinuationTNC(){
        return tcAttachment('T&C: Discontinuation T&C');
    }
    
    public Attachment getExemptionAdditionalInfo(){
        return tcAttachment('T&C: Exemption Additional Info');
    }
    
    public Attachment getExemptionTNC(){
        return tcAttachment('T&C: Exemption T&C');
    }
    
    public Attachment getCompetencyAreaTNC(){
        return tcAttachment('T&C: Competency Area T&C');
    }
    
    public Attachment tcAttachment(String tcName){
    
        List<Attachment> attachments = [Select Id, Name, Body from Attachment Where ParentId =: termNCondMap.get(tcName) limit 1];
        
        if(!attachments.isEmpty()){
            return attachments[0];
        }else{
            return null;
        }
    }
    
    public String getDomainUrl(){
        return luana_NetworkUtil.getCommunityPath();
    }
}