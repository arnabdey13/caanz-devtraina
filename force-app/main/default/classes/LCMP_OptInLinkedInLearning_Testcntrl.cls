@isTest
public class LCMP_OptInLinkedInLearning_Testcntrl {

    private static User currentUser;
    private static Account currentAccount;
    private static Document currentDocument;
      static {
        currentAccount = CASUB_PaymentController_CA_Test.createFullMemberAccount();
        currentAccount.Affiliated_Branch_Country__c = 'Australia';
        insert currentAccount;
        
        currentDocument= LCMP_OptInLinkedInLearning_Testcntrl.createDocument();
    }

    private static User getCurrentUser(){
        Test.startTest(); Test.stopTest(); // Future method needs to run to create the user.
        return currentUser = [SELECT Name FROM User WHERE AccountId =: currentAccount.Id];
    }
    
    // <Create FullMemberAccount>
    public static Account createFullMemberAccount(){
        Account FullMemberAccountObject = new Account();
        FullMemberAccountObject.Salutation = 'Mr.';
        FullMemberAccountObject.FirstName = 'TestFName';
        FullMemberAccountObject.PersonEmail= 'Full_Member'+ LCMP_OptInLinkedInLearning_Testcntrl.getRandomNumber(910) +'@gmail.com';
        FullMemberAccountObject.LastName = 'Test'+ LCMP_OptInLinkedInLearning_Testcntrl.getRandomNumber(910);
        FullMemberAccountObject.RecordTypeId = RecordTypeCache.getId(FullMemberAccountObject, 'Full_Member');
        FullMemberAccountObject.Communication_Preference__c= 'Home Phone';
        FullMemberAccountObject.PersonHomePhone= '1234';
        FullMemberAccountObject.PersonOtherStreet= '83 Saggers Road';
        FullMemberAccountObject.PersonOtherCity='JITARNING';
        FullMemberAccountObject.PersonOtherState='Western Australia';
        FullMemberAccountObject.PersonOtherCountry='Australia';
        FullMemberAccountObject.PersonOtherPostalCode='6365';  
        return FullMemberAccountObject;
    }
    public static Document createDocument(){        
        Document documentObj = new Document();
        documentObj.Body = Blob.valueOf('Test Document Text');
        documentObj.ContentType = 'application/pdf';
        documentObj.DeveloperName = 'my_document';
        documentObj.IsPublic = true;
        documentObj.Name = 'LIL';
        documentObj.FolderId = [select id from folder where name = 'Communities Shared Document Folder' limit 1].id;
        insert documentObj;
        return documentObj;
    }
     public static Integer getRandomNumber(Integer size){
          Double d = math.random() * size;
          return d.intValue();
     }
     
        @isTest static void testCMP_OptInLinkedInLearning() {
        System.runAs(getCurrentUser()) {
         LCMP_OptInLinkedInLearning.getInitialData();
         String result = LCMP_OptInLinkedInLearning.updateAccount(currentAccount.Id, 'newEmail@ignore.com');
        system.assertEquals('SUCCESS', result);
        }
        }
        }