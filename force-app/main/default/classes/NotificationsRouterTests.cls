@IsTest(seeAllData=false)
public class NotificationsRouterTests {

    @IsTest
    public static void testNotificationsSkip() {
        System.assertEquals(false, NotificationsRouter.getMemberData(TestSubscriptionUtils.getTestAccountId()).get('prop.skipNotifications'),
                'skip is present');
    }

    @IsTest
    public static void testCountryCodes() {

        Id accountId = TestSubscriptionUtils.getTestAccountId();

        System.debug(NotificationsRouter.getMemberData(accountId));

        System.assertEquals('au', NotificationsRouter.getMemberData(accountId).get('prop.countryCode'),
                'lowercase country code is returned');

        setCountryOnQuestionnaire(accountId, 'NZ');

        System.assertEquals('nz', NotificationsRouter.getMemberData(accountId).get('prop.countryCode'),
                'updated lowercase country code is returned');

        setCountryOnQuestionnaire(accountId, 'NL');

        System.assertEquals('other', NotificationsRouter.getMemberData(accountId).get('prop.countryCode'),
                'lowercase other is returned for non rugby playing nations');
    }

    private static void setCountryOnQuestionnaire(Id accountId, String countryCode) {
        Map<String, Object> fromClient = EditorNotificationsService.load(accountId);
        Map<Object, Object> q = (Map<Object, Object>) fromClient.get('RECORD');
        fromClient.put('DIRTY', new Map<Object, Object>{'prop.countryCode' => true});
        setAnswer(fromClient, 'prop.countryCode', countryCode);
        EditorNotificationsService.save(fromClient, accountId);
    }

    // emulate how the Lightning code handles onChange events and keep tests DRY 
    private static void setAnswer(Map<String, Object> properties, String field, Object value) {
        Map<Object, Object> record = (Map<Object, Object>) properties.get('RECORD');
        properties.put('DIRTY', new Map<Object, Object>{
                field => true
        });
        if (EditorPageUtils.isPropertyField(field)) {
            properties.put(field, value);
        } else {
            record.put(field, value);
        }
    }

    @testSetup static void createTestData() {
        TestSubscriptionUtils.createTestData();
    }

}