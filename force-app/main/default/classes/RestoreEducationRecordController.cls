public with sharing class RestoreEducationRecordController {
    
    
    @AuraEnabled 
    public static void restoreEducationRecord(ID educationRecordId){
        try{
        List<ID> educationBackupRecordID = new List<ID>();
        List<Education_Record__c> educationRecordList = new List<Education_Record__c>();
        List<CPD_Log_Backup__c> backupEducationRecordList = new List<CPD_Log_Backup__c>();
        backupEducationRecordList = [Select IsDeleted, Name, CurrencyIsoCode,Contact_Student__c, Award_as_Presenter__c, Bridging__c, 
                                           CAHDP__c,Community_User_Entry__c, Country__c, Date_Commenced__c, Date_completed__c, Degree_Country__c, 
                                           Degree_Other__c, Degree__c, Enter_university_instituition__c, Event_Course_name__c, Ext_Id__c, 
                                           Other_State__c, Primary_Qualification__c, Provider_State__c, Qualifying_hours__c, Qualifying_hours_type__c, 
                                           Specialist_hours_code__c, State__c, Training_and_development__c, University_professional_organisation__c, 
                                           Verified__c, Other_Type__c, Type__c, Resource_Booking__c, Professional_Competency__c, Type_of_Activity__c, 
                                           Migrated__c, CPD_Contact_and_Summary_Relate__c, CPD_End_Date_is_before_Triennium_Start__c, CPD_Summary__c,
                                           Description_Of_Activity__c, Linked_To_Current_Triennium__c, Triennium_Start_Date__c, Triennium_Year__c, 
                                           University_or_Institution__c, CPD_Processed__c, Dummy_field_to_trigger_update__c FROM CPD_Log_Backup__c where ID =:educationRecordId];
        
        if(!backupEducationRecordList.isEmpty()){
            educationRecordList = createEducationRecord(backupEducationRecordList);
        }   
         system.debug('Before insert');
         system.debug('List size'+educationRecordList.size());
        insert educationRecordList;
        delete backupEducationRecordList;
        }
        catch(Exception e){
            throw new AuraHandledException('== Error =='+ e.getMessage());
            
        }
        
        
        
         
    }
    
    public static List<Education_Record__c> createEducationRecord(List<CPD_Log_Backup__c> backupEducationRecordList){
        List<Education_Record__c> educationRecordList = new List<Education_Record__c>();
        for(CPD_Log_Backup__c educationBackupdata : backupEducationRecordList){
            Education_Record__c educationOriginalRecord = new Education_Record__c();
            //educationBackupRecord.IsDeleted = educationRecord.IsDeleted;
            //educationBackupRecord.Name = educationRecord.Name;
            educationOriginalRecord.CurrencyIsoCode = educationBackupdata.CurrencyIsoCode;
            educationOriginalRecord.Contact_Student__c = educationBackupdata.Contact_Student__c;
            educationOriginalRecord.Award_as_Presenter__c = educationBackupdata.Award_as_Presenter__c;
            educationOriginalRecord.Bridging__c = educationBackupdata.Bridging__c;
            educationOriginalRecord.CAHDP__c = educationBackupdata.CAHDP__c;
            educationOriginalRecord.Community_User_Entry__c = educationBackupdata.Community_User_Entry__c;
            educationOriginalRecord.Country__c = educationBackupdata.Country__c;
            educationOriginalRecord.Date_Commenced__c = educationBackupdata.Date_Commenced__c;
            educationOriginalRecord.Date_completed__c = educationBackupdata.Date_completed__c;
            educationOriginalRecord.Degree_Country__c = educationBackupdata.Degree_Country__c;
            educationOriginalRecord.Degree_Other__c = educationBackupdata.Degree_Other__c;
            educationOriginalRecord.Degree__c = educationBackupdata.Degree__c;
            educationOriginalRecord.Enter_university_instituition__c = educationBackupdata.Enter_university_instituition__c;
            educationOriginalRecord.Event_Course_name__c = educationBackupdata.Event_Course_name__c;
            educationOriginalRecord.Ext_Id__c = educationBackupdata.Ext_Id__c;
            educationOriginalRecord.Other_State__c = educationBackupdata.Other_State__c;
            educationOriginalRecord.Primary_Qualification__c = educationBackupdata.Primary_Qualification__c;
            educationOriginalRecord.Provider_State__c = educationBackupdata.Provider_State__c;
            educationOriginalRecord.Qualifying_hours__c = educationBackupdata.Qualifying_hours__c;
            educationOriginalRecord.Qualifying_hours_type__c = educationBackupdata.Qualifying_hours_type__c;
            educationOriginalRecord.Specialist_hours_code__c = educationBackupdata.Specialist_hours_code__c;
            educationOriginalRecord.State__c = educationBackupdata.Other_State__c;
            educationOriginalRecord.Training_and_development__c = educationBackupdata.Training_and_development__c;
            educationOriginalRecord.University_professional_organisation__c = educationBackupdata.University_professional_organisation__c;
            educationOriginalRecord.Verified__c = educationBackupdata.Verified__c;
            educationOriginalRecord.Other_Type__c = educationBackupdata.Other_Type__c;
            educationOriginalRecord.Type__c = educationBackupdata.Type__c;
            educationOriginalRecord.Resource_Booking__c = educationBackupdata.Resource_Booking__c;
            educationOriginalRecord.Professional_Competency__c = educationBackupdata.Professional_Competency__c;
            educationOriginalRecord.Type_of_Activity__c = educationBackupdata.Type_of_Activity__c;
            educationOriginalRecord.Migrated__c = educationBackupdata.Migrated__c;
            //educationOriginalRecord.CPD_Contact_and_Summary_Relate__c = educationBackupdata.CPD_Contact_and_Summary_Relate__c;
            //educationOriginalRecord.CPD_End_Date_is_before_Triennium_Start__c = educationBackupdata.CPD_End_Date_is_before_Triennium_Start__c;
            educationOriginalRecord.CPD_Summary__c = educationBackupdata.CPD_Summary__c;
            educationOriginalRecord.Description_Of_Activity__c = educationBackupdata.Description_Of_Activity__c;
            //educationOriginalRecord.Linked_To_Current_Triennium__c = educationBackupdata.Linked_To_Current_Triennium__c;
            //educationOriginalRecord.Triennium_Start_Date__c = educationBackupdata.Triennium_Start_Date__c;
            //educationOriginalRecord.Triennium_Year__c = educationBackupdata.Triennium_Year__c;
            //educationOriginalRecord.University_or_Institution__c = educationBackupdata.University_or_Institution__c;
            educationOriginalRecord.CPD_Processed__c = educationBackupdata.CPD_Processed__c;
            educationOriginalRecord.Dummy_field_to_trigger_update__c = educationBackupdata.Dummy_field_to_trigger_update__c;
            educationRecordList.add(educationOriginalRecord);
        }
        return educationRecordList;
    }

}