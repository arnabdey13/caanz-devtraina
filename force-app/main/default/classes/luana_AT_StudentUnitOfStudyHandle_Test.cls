/*
    Developer: WDCi (KH)
    Date: 30/08/2016
    Task #: Test class for luana_SuppExamBroadcastController
    
*/
@isTest(seeAllData=false)
private class luana_AT_StudentUnitOfStudyHandle_Test {
    
    final static String contextLongName = 'luana_AT_StudentUnitOfStudyHandle_Test';
    final static String contextShortName = 'lsuos';
    private static map<String, Id> commProfIdMap {get; set;}
    
    public static User memberUser {get; set;}
    public static Account memberAccount {get; set;}
    public static luana_CommUserUtil userUtil {get; set;} 
    
    public static LuanaSMS__Course__c course1;
    public static LuanaSMS__Course__c course2;
    public static LuanaSMS__Course__c course3;
    public static LuanaSMS__Course__c course4;
    
    public static LuanaSMS__Session__c newSession1;
    public static LuanaSMS__Session__c newSession2;
    public static LuanaSMS__Session__c newSession3;
    
    public static Luana_DataPrep_Test dataPrep {get; set;}
    public static List<LuanaSMS__Student_Program__c> stuPrograms {get; set;}
    
    static void initial(){
        //initialize
        dataPrep = new Luana_DataPrep_Test();
        
        //Create all the custom setting
         
        insert dataPrep.createLuanaConfigurationCustomSetting();
        
        List<Luana_Extension_Settings__c> customSessingList = dataPrep.prepLuanaExtensionSettingCustomSettings();
        customSessingList.add(new Luana_Extension_Settings__c(Name = 'AdobeConnect_URL', Value__c = '  https://connect-staging.charteredaccountantsanz.com'));
        insert customSessingList;
        
        //Create user with Member and Employer community access
        memberAccount = dataPrep.generateNewApplicantAcc('Joe_' + contextShortName, contextLongName, 'Full_Member');
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.Assessible_for_CA_Program__c = true;
        memberAccount.PersonEmail = 'joe_1_' + contextShortName +'@gmail.com';//LCA-921
        insert memberAccount;
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
    }
    
    static void setup(){
        
        //LCA-921
        memberUser = [Select Id, Email, FirstName, LastName, UserName, Name from User Where Email =: 'joe_1_' + contextShortName+'@gmail.com' limit 1];
        //memberUser = dataPrep.generateNewApplicantUser(contextLongName, contextShortName, memberAccount, commProfIdMap.get('Customer Community Login User'));
        //insert memberUser;
        
        userUtil = new luana_CommUserUtil(memberUser.Id);
        
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
        Luana_Extension_Settings__c defContriPermSetId = Luana_Extension_Settings__c.getValues('Default_ContCom_PermSet_Id');
       
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defEmployerPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defContriPermSetId.value__c));
        
        System.runAs(memberUser){
            insert psas;
        }
        
        LuanaSMS__Training_Organisation__c trainingOrg = dataPrep.createNewTraningOrg(contextLongName, contextLongName, contextShortName, 'Virtual Street 123', 'Queensland', '4551');
        insert trainingOrg;
        LuanaSMS__Program__c program = dataPrep.createNewProgram(contextShortName, contextLongName, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        insert program;
        LuanaSMS__Program_Offering__c po = dataPrep.createNewProgOffering('po' + contextShortName, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), program.Id, trainingOrg.Id, null, null, null, 1, 1);
        insert po;
        
        List<LuanaSMS__Course__c> courses = new List<LuanaSMS__Course__c>();
        course1 = dataPrep.createNewCourse('Graduate Diploma of Workshop1 Accounting_' + contextShortName, po.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Program'), 'Running');
        course1.LuanaSMS__Allow_Online_Enrolment__c = true;
        course1.Supp_Exam_Enrolment_End_Date__c = System.today().addMonths(3);
        courses.add(course1);
        
        insert courses;
        
        stuPrograms = new List<LuanaSMS__Student_Program__c>();
        LuanaSMS__Student_Program__c newStudProg1 = dataPrep.createNewStudentProgram(dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), userUtil.custCommConId, course1.Id, '', 'Fail');
        newStudProg1.Paid__c = true;
        newStudProg1.Supp_Exam_Broadcasted__c = false;
        newStudProg1.Sup_Exam_Enrolled__c = false;
        newStudProg1.Supp_Exam_Special_Consideration_Approved__c = 'Approved';
        newStudProg1.Supp_Exam_Eligible__c = false;
        stuPrograms.add(newStudProg1);
        
        insert stuPrograms;
        
/*        List<Account> venues = new List<Account>();
        Account venueAccount1 = dataPrep.generateNewBusinessAcc(contextShortName+ 'Test Venue', 'Venue', null, null, null, null, 'Sunshine Coast Office', 'Active');
        venueAccount1.billingCity = 'city';
        venueAccount1.billingPostalCode = '1234';
        venueAccount1.billingCountry = 'Malaysia';
        venues.add(venueAccount1);
        
        Account venueAccount2 = dataPrep.generateNewBusinessAcc(contextShortName+ 'Test Venue', 'Venue', null, null, null, null, 'Sunshine Coast Office', 'Active');
        venueAccount2.billingCity = 'city';
        venueAccount2.billingPostalCode = '1234';
        venueAccount2.billingCountry = 'Australia';
        venueAccount2.billingState = 'New South Wales';
        venues.add(venueAccount2);
        insert venues;
        
        List<LuanaSMS__Room__c> rooms = new List<LuanaSMS__Room__c>();
        LuanaSMS__Room__c room1 = dataPrep.newRoom(contextShortName, contextLongName, venueAccount1.Id, 20, 'Exam');
        LuanaSMS__Room__c room2 = dataPrep.newRoom(contextShortName, contextLongName, venueAccount2.Id, 20, 'Exam');
        LuanaSMS__Room__c room3 = dataPrep.newRoom(contextShortName, contextLongName, venueAccount3.Id, 20, 'Exam');
        rooms.add(room1);
        rooms.add(room2);
        rooms.add(room3);
        insert rooms;
        
        List<LuanaSMS__Session__c> sessions = new List<LuanaSMS__Session__c>();
        newSession1 = dataPrep.newSession(contextShortName, contextLongName, 'Exam', 20, venueAccount1.Id, room1.Id, null, system.now().addHours(1), system.now().addHours(2));
        newSession1.Topic_Key__c = contextShortName+ '_T1';
        sessions.add(newSession1);
        
        newSession2 = dataPrep.newSession(contextShortName, contextLongName, 'Supplementary_Exam', 20, venueAccount2.Id, room2.Id, null, system.now().addHours(1), system.now().addHours(2));
        newSession2.Topic_Key__c = contextShortName+ '_T1';
        sessions.add(newSession2);
        
        newSession3 = dataPrep.newSession(contextShortName, contextLongName, 'Supplementary_Exam', 20, venueAccount3.Id, room3.Id, null, system.now().addHours(1), system.now().addHours(2));
        newSession3.Topic_Key__c = contextShortName+ '_T1';
        sessions.add(newSession3);
        
        insert sessions;
        
        List<LuanaSMS__Attendance2__c> attendances = new List<LuanaSMS__Attendance2__c>();
        LuanaSMS__Attendance2__c att1 = dataPrep.createAttendance(userUtil.custCommConId, 'Exam', newStudProg1.Id, newSession1.Id);
        attendances.add(att1);
        LuanaSMS__Attendance2__c att2 = dataPrep.createAttendance(userUtil.custCommConId, 'Supplementary_Exam', newStudProg1.Id, newSession2.Id);
        attendances.add(att2);
        
        LuanaSMS__Attendance2__c att3 = dataPrep.createAttendance(userUtil.custCommConId, 'Exam', newStudProg2.Id, newSession1.Id);
        attendances.add(att3);
        LuanaSMS__Attendance2__c att4 = dataPrep.createAttendance(userUtil.custCommConId, 'Supplementary_Exam', newStudProg2.Id, newSession2.Id);
        attendances.add(att4);
        
        LuanaSMS__Attendance2__c att5 = dataPrep.createAttendance(userUtil.custCommConId, 'Supplementary_Exam', newStudProg3.Id, newSession3.Id);
        attendances.add(att5);
        insert attendances;
*/        
    }
    
     /*
        Success allocation test from CDL
    */
    static testMethod void testCreateSUOS() {
        Test.startTest();
            initial();
            
        Test.stopTest();
        
        setup();
        
        LuanaSMS__Student_Unit_of_Study__c  newSuos = new LuanaSMS__Student_Unit_of_Study__c();
        newSuos.LuanaSMS__Student_Program__c = stuPrograms[0].Id;
        newSuos.Name = 'Test_SUOS_'+contextShortName+'_1';
        
        insert newSuos;
        
    }
}