/**
 * Porter Stemmer.
 *
 * Porter Stemmer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Porter Stemmer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Porter Stemmer.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This is a simple implementation of the Porter stemming algorithm, defined here:
 * <a href='http://tartarus.org/martin/PorterStemmer/def.txt'>http://tartarus.org/martin/PorterStemmer/def.txt</a>
 * <p>
 * This implementation has not been tuned for high performance on large amounts of text. It is
 * a simple (naive perhaps) implementation of the rules.
 **/

// Gives A - String myChar = String.fromCharArray( new List<integer> { 65 } );

public class RecognitionPorterStemmer {

    /**
     * @param word the word to stem
     * @return the stem of the word, in lowercase.
     */
    public String stemWord(String word) {
        String stem = word.toLowerCase();
        if (stem.length() < 3) return stem;
        stem = stemStep1a(stem);
        stem = stemStep1b(stem);
        stem = stemStep1c(stem);
        stem = stemStep2(stem);
        stem = stemStep3(stem);
        stem = stemStep4(stem);
        stem = stemStep5a(stem);
        stem = stemStep5b(stem);
        return stem;
    }

	@TestVisible String stemStep1a(String input) {
        // SSES -> SS
        if (input.endsWith('sses')) {
            return input.substring(0, input.length() - 2);
        }
        // IES  -> I
        if (input.endsWith('ies')) {
            return input.substring(0, input.length() - 2);
        }
        // SS   -> SS
        if (input.endsWith('ss')) {
            return input;
        }
        // S    ->
        if (input.endsWith('s')) {
            return input.substring(0, input.length() - 1);
        }
        return input;
    }

	@TestVisible String stemStep1b(String input) {
        // (m>0) EED -> EE
        if (input.endsWith('eed')) {
            String stem = input.substring(0, input.length() - 1);
            String letterTypes = getLetterTypes(stem);
            Integer m = getM(letterTypes);
            if (m > 0) return stem;
            return input;
        }
        // (*v*) ED  ->
        if (input.endsWith('ed')) {
            String stem = input.substring(0, input.length() - 2);
            String letterTypes = getLetterTypes(stem);
            if (letterTypes.contains('v')) {
                return step1b2(stem);
            }
            return input;
        }
        // (*v*) ING ->
        if (input.endsWith('ing')) {
            String stem = input.substring(0, input.length() - 3);
            String letterTypes = getLetterTypes(stem);
            if (letterTypes.contains('v')) {
                return step1b2(stem);
            }
            return input;
        }
        return input;
    }

	@TestVisible String step1b2(String input) {
        // AT -> ATE
        if (input.endsWith('at')) {
            return input + 'e';
        }
        // BL -> BLE
        else if (input.endsWith('bl')) {
            return input + 'e';
        }
        // IZ -> IZE
        else if (input.endsWith('iz')) {
            return input + 'e';
        } else {
            // (*d and not (*L or *S or *Z))
            // -> single letter
            String lastDoubleConsonant = getLastDoubleConsonant(input);
            if (lastDoubleConsonant != '' &&
                    lastDoubleConsonant != 'l'
                    && lastDoubleConsonant != 's'
                    && lastDoubleConsonant != 'z') {
                return input.substring(0, input.length() - 1);
            }
            // (m=1 and *o) -> E
            else {
                String letterTypes = getLetterTypes(input);
                Integer m = getM(letterTypes);
                if (m == 1 && isStarO(input)) {
                    return input + 'e';
                }

            }
        }
        return input;
    }

	@TestVisible String stemStep1c(String input) {
        if (input.endsWith('y')) {
            String stem = input.substring(0, input.length() - 1);
            String letterTypes = getLetterTypes(stem);
            if (letterTypes.contains('v')) return stem + 'i';
        }
        return input;
    }

	@TestVisible String stemStep2(String input) {
        String[] s1 = new String[]{
                'ational',
                'tional',
                'enci',
                'anci',
                'izer',
                'bli', // the published algorithm specifies abli instead of bli.
                'alli',
                'entli',
                'eli',
                'ousli',
                'ization',
                'ation',
                'ator',
                'alism',
                'iveness',
                'fulness',
                'ousness',
                'aliti',
                'iviti',
                'biliti',
                'logi'
        };
        String[] s2 = new String[]{
                'ate',
                'tion',
                'ence',
                'ance',
                'ize',
                'ble', 
                'al',
                'ent',
                'e',
                'ous',
                'ize',
                'ate',
                'ate',
                'al',
                'ive',
                'ful',
                'ous',
                'al',
                'ive',
                'ble',
                'log'
        };
        // (m>0) ATIONAL ->  ATE
        // (m>0) TIONAL  ->  TION
        for (Integer i = 0; i < s1.size(); i++) {
            if (input.endsWith(s1[i])) {
                String stem = input.substring(0, input.length() - s1[i].length());
                String letterTypes = getLetterTypes(stem);
                Integer m = getM(letterTypes);
                if (m > 0) return stem + s2[i];
                return input;
            }
        }
        return input;
    }

	@TestVisible String stemStep3(String input) {
        String[] s1 = new String[]{
                'icate',
                'ative',
                'alize',
                'iciti',
                'ical',
                'ful',
                'ness'
        };
        String[] s2 = new String[]{
                'ic',
                '',
                'al',
                'ic',
                'ic',
                '',
                ''
        };
        // (m>0) ICATE ->  IC
        // (m>0) ATIVE ->
        for (Integer i = 0; i < s1.size(); i++) {
            if (input.endsWith(s1[i])) {
                String stem = input.substring(0, input.length() - s1[i].length());
                String letterTypes = getLetterTypes(stem);
                Integer m = getM(letterTypes);
                if (m > 0) return stem + s2[i];
                return input;
            }
        }
        return input;

    }

	@TestVisible String stemStep4(String input) {
        String[] suffixes = new String[]{
                'al',
                'ance',
                'ence',
                'er',
                'ic',
                'able',
                'ible',
                'ant',
                'ement',
                'ment',
                'ent',
                'ion',
                'ou',
                'ism',
                'ate',
                'iti',
                'ous',
                'ive',
                'ize'
        };
        // (m>1) AL    ->
        // (m>1) ANCE  ->
        for(String suffix : suffixes) {
            if (input.endsWith(suffix)) {
                String stem = input.substring(0, input.length() - suffix.length());
                String letterTypes = getLetterTypes(stem);
                Integer m = getM(letterTypes);
                if (m > 1) {
                    if (suffix.equals('ion')) {
                        if (stem.charAt(stem.length() - 1) == 's'.charAt(0) || stem.charAt(stem.length() - 1) == 't'.charAt(0)) {
                            return stem;
                        }
                    } else {
                        return stem;
                    }
                }
                return input;
            }
        }
        return input;
    }

	@TestVisible String stemStep5a(String input) {
        if (input.endsWith('e')) {
            String stem = input.substring(0, input.length() - 1);
            String letterTypes = getLetterTypes(stem);
            Integer m = getM(letterTypes);
            // (m>1) E     ->
            if (m > 1) {
                return stem;
            }
            // (m=1 and not *o) E ->
            if (m == 1 && !isStarO(stem)) {
                return stem;
            }
        }
        return input;
    }

	@TestVisible String stemStep5b(String input) {
        // (m > 1 and *d and *L) -> single letter
        String letterTypes = getLetterTypes(input);
        Integer m = getM(letterTypes);
        if (m > 1 && input.endsWith('ll')) {
            return input.substring(0, input.length() - 1);
        }
        return input;
    }

	@TestVisible String getLastDoubleConsonant(String input) {
        if (input.length() < 2) return '';
        String lastLetter = String.fromCharArray( new List<integer> { input.charAt(input.length() - 1) } );
        String penultimateLetter = String.fromCharArray( new List<integer> { input.charAt(input.length() - 2) } );
        if (lastLetter == penultimateLetter && getLetterType('', lastLetter) == 'c') {
            return lastLetter;
        }
        return '';
    }

    // *o  - the stem ends cvc, where the second c is not W, X or Y (e.g.
    //                                                              -WIL, -HOP)
	@TestVisible boolean isStarO(String input) {
        if (input.length() < 3) return false;
        String lastLetter = String.fromCharArray( new List<integer> { input.charAt(input.length() - 1) } );
        if (lastLetter == 'w' || lastLetter == 'x' || lastLetter == 'y') return false;

        String secondToLastLetter = String.fromCharArray( new List<integer> { input.charAt(input.length() - 2) } );
        String thirdToLastLetter  = String.fromCharArray( new List<integer> { input.charAt(input.length() - 3) } );
    	String fourthToLastLetter = ( input.length() == 3) ? '' : String.fromCharArray( new List<integer> { input.charAt(input.length() - 4) } );
		
        return getLetterType(secondToLastLetter, lastLetter) == 'c'
                && getLetterType(thirdToLastLetter, secondToLastLetter) == 'v'
                && getLetterType(fourthToLastLetter, thirdToLastLetter) == 'c';
    }

	@TestVisible String getLetterTypes(String input) {
        String letterTypes = '';
        String previousLetter='';
        for (Integer i = 0; i < input.length(); i++) {
            String letter = String.fromCharArray( new List<integer> { input.charAt(i) } );
            if (i>0)
            	previousLetter =  String.fromCharArray( new List<integer> { input.charAt(i - 1) } ); 
            String letterType = getLetterType(previousLetter, letter);
            if (letterTypes.length() == 0 || letterTypes.charAt(letterTypes.length() - 1) != letterType.charAt(0) ) {
                letterTypes += letterType;
            }
        }
        return letterTypes;
    }

	@TestVisible Integer getM(String letterTypes) {
        if (letterTypes.length() < 2) return 0;
        if (letterTypes.charAt(0) == 'c'.charAt(0)) return (letterTypes.length() - 1) / 2;
        return letterTypes.length() / 2;
    }

	@TestVisible String getLetterType(String previousLetter, String letter) {
        if (letter == 'a' || letter == 'e' || letter == 'i' || letter == 'o' || letter == 'u')
			return 'v';
        else if (letter == 'y') {
			if (previousLetter == '' || getLetterType('', previousLetter) == 'v')
            	return 'c';
        	return 'v';
        }
        else
			return 'c';
    }
}