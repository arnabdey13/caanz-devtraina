/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ProfileCache_test {
	private static Id SystemAdministratorProfileId = ProfileCache.getId('System Administrator');
	
	static testMethod void ProfileCache_testGetProfileByName() {
		Profile SystemAdministratorProfile = ProfileCache.getProfile('System Administrator');
		System.assertNotEquals(null, SystemAdministratorProfile);
	}
	
	static testMethod void ProfileCache_testGetProfileById() {
		Profile SystemAdministratorProfile = ProfileCache.getProfile( SystemAdministratorProfileId );
		System.assertNotEquals(null, SystemAdministratorProfile);
	}
	
	static testMethod void ProfileCache_testUnableToFindProfileByName() {
		String FakeProfileName = 'UnableToFindProfile';
		try{
			ProfileCache.getProfile( FakeProfileName );
		}
		catch(Exception ex){
			System.assertEquals( 'Unable to find Profile for '+FakeProfileName, ex.getMessage(), 'Error for FakeProfileName' );
			return;
		}
		System.assert(false, 'An error should have been throw!');
	}
	
	static testMethod void ProfileCache_testUnableToFindProfileById() {
		Id FakeProfileId = '00e90990001JzUPAA0';
		try{
			ProfileCache.getProfile( FakeProfileId );
		}
		catch(Exception ex){
			System.assert( ex.getMessage().startsWith('Unable to find Profile for ' + FakeProfileId) );
			return;
		}
		System.assert(false, 'An error should have been throw!');
	}
}