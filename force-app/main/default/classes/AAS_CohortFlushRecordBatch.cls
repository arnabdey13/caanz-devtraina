global class AAS_CohortFlushRecordBatch implements Database.Batchable<sObject>{
    
    List<SObject> flushBatchSObjList = new List<SObject>();
    List<SObject> updateBatchSObjList = new List<SObject>();
    Boolean updateBatches = false;
    AAS_Course_Assessment__c courseAssessment;
    AAS_Assessment_Schema_Section__c assObj;

    global AAS_CohortFlushRecordBatch(List<SObject> fSobjList, List<SObject> uSobjList, Boolean callFromMain, AAS_Course_Assessment__c ca, AAS_Assessment_Schema_Section__c ass){
        flushBatchSObjList = fSobjList;
        updateBatchSObjList = uSobjList;
        
        updateBatches = callFromMain;
        courseAssessment = ca;
        assObj = ass;
    }
    
    global List<SObject> start(Database.BatchableContext BC) {
        return flushBatchSObjList;
        
    }
    
    global void executeBatch(Database.BatchableContext BC, integer scope){
        
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        try{
            
            for(SObject sobj: scope){
                AAS_Student_Assessment_Section_Item__c newObjScope = (AAS_Student_Assessment_Section_Item__c)sobj;
                newObjScope.AAS_Adjustment__c = null;
            }
            System.debug('*********size:: ' + flushBatchSObjList.size() + ' - ' + scope.size());
            database.update(scope,true);
            
        }Catch(Exception exp){
            
            throw exp;
        }
    }

    global void finish(Database.BatchableContext BC) {
        
        if(updateBatches){
            //Call the batchable class to update the record in batch
            AAS_CohortUpdateBatch updateSASIBatch = New AAS_CohortUpdateBatch(updateBatchSObjList, courseAssessment, assObj);
            database.executeBatch(updateSASIBatch, 200);
            
        }else{
            //Will run here because of the Cohort update fail and flush the data again
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            AsyncApexJob aaj = [Select TotalJobItems, Status, NumberOfErrors, JobType, JobItemsProcessed, ExtendedStatus, CreatedById, CreatedBy.Email, CompletedDate From AsyncApexJob WHERE id = :BC.getJobId()];//get the job Id
            
            List<String> eTemplateName = new List<String>();
            eTemplateName.add('AAS_Fail_Cohort_Adjustment_Notification');
            List<EmailTemplate> eTemplates = [Select id, name, developerName, Subject, HtmlValue, Body from EmailTemplate Where DeveloperName in: eTemplateName];
            
            String [] email = new String[] {aaj.CreatedBy.Email};
            
            //below code will send an email to User about the status
            mail.setToAddresses(email);
            mail.setSenderDisplayName('AAS Cohort Processing Status');
            
            for(EmailTemplate et: eTemplates){
                if(et.developerName == 'AAS_Fail_Cohort_Adjustment_Notification'){
                    mail.setSubject(processUpdateEmailTemplateSubject(et.Subject, courseAssessment.Name));
                    mail.setPlainTextBody(processUpdateEmailTemplateHTMLValue(et.HtmlValue, courseAssessment.Name, courseAssessment.Id, aaj.ExtendedStatus));
                }
            }
            
            Messaging.sendEmail(new Messaging.Singleemailmessage [] {mail});
            
            AAS_Course_Assessment__c ca = new AAS_Course_Assessment__c();
            ca.Id = courseAssessment.Id;
            ca.AAS_Cohort_Batch_Processing__c = false;
            ca.AAS_Cohort_Adjustment_Exam__c = false;
            
            update ca;
            
            AAS_Assessment_Schema_Section__c assTemp = new AAS_Assessment_Schema_Section__c();
            assTemp.Id = assObj.Id;
            assTemp.AAS_Cohort_Adjustment__c = 0;
            
            update assTemp;
        }

    }
    
    public String processUpdateEmailTemplateSubject(String emailTemplateSubject, String caName){
        String subject = emailTemplateSubject;
        subject = subject.replace('{!AAS_Course_Assessment__c.Name}', caName);
        return subject;
    }
    
    public String processUpdateEmailTemplateHTMLValue(String emailTemplateHTMLValue, String caName, String caId, String errorMessage){
        String htmlValue = emailTemplateHTMLValue;
        htmlValue = htmlValue.replace('{!AAS_Course_Assessment__c.Name}', caName);
        
        String caURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + caId;
        htmlValue = htmlValue.replace('{!AAS_Course_Assessment__c.Link}', caURL);
        if(errorMessage != null){
            htmlValue = htmlValue.replace('Error Message:', 'Error Message: <br/>' + errorMessage);
        }
        return htmlValue;
    }
    
    public String processUpdateEmailTemplateBody(String emailTemplateBody, String caName, String caId, String errorMessage){
        String body = emailTemplateBody;
        body = body.replace('{!AAS_Course_Assessment__c.Name}', caName);
        
        String caURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + caId;
        body = body.replace('{!AAS_Course_Assessment__c.Link}', caURL);
        if(errorMessage != null){
            body = body.replace('Error Message:', 'Error Message: \n' + errorMessage);
        }
        return body;
    }

}