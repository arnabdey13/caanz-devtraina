/**
* @author           WDCi-LKoh
* @date             20/06/2019
* @group            Competency Automation
* @description      Trigger Handler for FT_University_Subject__c
* @change-history   #FIX001 - WDCi-LKoh - 04/09/2019 - Fixed the issue with related pathway blocking users from adjusting University Subject
*/
public with sharing class wdci_UniversitySubjectHandler {
    
    public static void initFlow(List<FT_University_Subject__c> newList, List<FT_University_Subject__c> oldList, Map<Id, FT_University_Subject__c> oldMap, boolean isInsert, boolean isUpdate, boolean isDelete, boolean isUndelete, boolean isBefore, boolean isAfter) {
        
        system.debug('Entering initFlow: ' +newList+ ' : ' +oldList+ ' : ' +oldMap+ ' : ' +isInsert+ ' : ' +isUpdate+ ' : ' +isDelete+ ' : ' +isUndelete+ ' : ' +isBefore+ ' : ' +isAfter);
        if (isBefore) {
            if (isUpdate) {
                checkForRelatedPathway(newList, oldMap);
            }
        }
        system.debug('Exiting initFlow');
    }

    public static void checkForRelatedPathway(List<FT_University_Subject__c> uniSubjectList, Map<Id, FT_University_Subject__c> oldMap) {

        system.debug('Entering checkForRelatedPathway: ' +uniSubjectList);

        Set<Id> uniSubjectIdSet = new Set<Id>();
        for (FT_University_Subject__c uniSubject : uniSubjectList) {
            FT_University_Subject__c previousUniSubject = oldMap.get(uniSubject.Id);
            if (previousUniSubject.FT_Pathway_Subject_Code__c != uniSubject.FT_Pathway_Subject_Code__c) {
                uniSubjectIdSet.add(uniSubject.Id);
            }
        }

        Map<Id, List<FT_Pathway__c>> pathwayMap = wdci_UniversitySubjectHandler.mapPathway(uniSubjectIdSet);

        for (FT_University_Subject__c uniSubject : uniSubjectList) {

            FT_University_Subject__c previousUniSubject = oldMap.get(uniSubject.Id);
            // #FIX001
            if (pathwayMap.containsKey(uniSubject.Id) && pathwayMap.get(uniSubject.Id).size() > 0) {
                
                for (FT_Pathway__c existingPathway : pathwayMap.get(uniSubject.Id)) {
                    /* Lean - use code below due to matching issue
                    if (existingPathway.FT_Pathway__c.contains(previousUniSubject.FT_Pathway_Subject_Code__c)) {
                        uniSubject.addError('There are related Pathways to this University Subject, please remove them before proceeding');
                        system.debug('University Subject with error: ' +uniSubject);
                    }*/

                    boolean isMatched = wdci_CompetencyUtil.isCodeMatches(existingPathway.FT_Pathway__c, previousUniSubject.FT_Pathway_Subject_Code__c);

                    if(isMatched){
                        uniSubject.addError('There are related Pathways to this University Subject, please remove them before proceeding');
                        system.debug('University Subject with error: ' +uniSubject);
                    }                   
                }
            }
        }
        system.debug('Exiting checkForRelatedPathway');
    }

    public static Map<Id, List<FT_Pathway__c>> mapPathway(Set<Id> uniSubjectIdSet) {
        
        system.debug('Entering mapPathway: ' +uniSubjectIdSet);

        Map<Id, List<FT_Pathway__c>> relatedPathwayMap = new Map<Id, List<FT_Pathway__c>>();
        List<FT_University_Subject__c> universitySubjectList = [SELECT Id, FT_Pathway_Subject_Code__c, FT_University__c, FT_Inactive__c FROM FT_University_Subject__c WHERE Id IN :uniSubjectIdSet];
        
        Set<Id> universityIdSet = new Set<Id>();
        for (FT_University_Subject__c uniSubject : universitySubjectList) {
            universityIdSet.add(uniSubject.FT_University__c);
        }

        List<FT_Pathway__c> relatedPathwayList = [SELECT Id, Name, FT_Pathway__c, FT_University__c FROM FT_Pathway__c WHERE FT_University__c IN :universityIdSet];
        Map<Id, List<FT_Pathway__c>> pathwayMap = new Map<Id, List<FT_Pathway__c>>();
        for (FT_Pathway__c pathway : relatedPathwayList) {
            if (pathwayMap.containsKey(pathway.FT_University__c)) {
                pathwayMap.get(pathway.FT_University__c).add(pathway);
            } else {
                List<FT_Pathway__c> newPathwayList = new List<FT_Pathway__c>();
                newPathwayList.add(pathway);
                pathwayMap.put(pathway.FT_University__c, newPathwayList);
            }
        }

        /* Lean - use new code below due to matching issue
        for (FT_University_Subject__c currentUniSubject : universitySubjectList) {
            List<FT_Pathway__c> relatedPathwayToThisUniSubject = new List<FT_Pathway__c>();
            if (pathwayMap.containsKey(currentUniSubject.FT_University__c)) {
                for (FT_Pathway__c relatedPathway : pathwayMap.get(currentUniSubject.FT_University__c)) {
                    if (!String.isBlank(relatedPathway.FT_Pathway__c) 
                        && !String.isBlank(currentUniSubject.FT_Pathway_Subject_Code__c) 
                        && relatedPathway.FT_Pathway__c.contains(currentUniSubject.FT_Pathway_Subject_Code__c)) 
                    {
                        relatedPathwayToThisUniSubject.add(relatedPathway);
                    }
                }
            }
            relatedPathwayMap.put(currentUniSubject.Id, relatedPathwayToThisUniSubject);
        }*/

        for (FT_University_Subject__c currentUniSubject : universitySubjectList) {
            List<FT_Pathway__c> relatedPathwayToThisUniSubject = new List<FT_Pathway__c>();
            if (pathwayMap.containsKey(currentUniSubject.FT_University__c)) {
                for (FT_Pathway__c relatedPathway : pathwayMap.get(currentUniSubject.FT_University__c)) {
                    if (!String.isBlank(relatedPathway.FT_Pathway__c) 
                        && !String.isBlank(currentUniSubject.FT_Pathway_Subject_Code__c))
                    {
                        boolean isMatched = wdci_CompetencyUtil.isCodeMatches(relatedPathway.FT_Pathway__c, currentUniSubject.FT_Pathway_Subject_Code__c);

                        if(isMatched){
                            relatedPathwayToThisUniSubject.add(relatedPathway);
                        }
                    }
                }
            }
            relatedPathwayMap.put(currentUniSubject.Id, relatedPathwayToThisUniSubject);
        }

        system.debug('Exiting mapPathway: ' +relatedPathwayMap);
        return relatedPathwayMap;
    }    
}