global class AAS_UpdateBatch implements Database.Batchable<sObject>{
    
    List<SObject> batchSObjList = new List<SObject>();
    
    global AAS_UpdateBatch(List<SObject> sobjList){
        batchSObjList = sobjList;
    }
    
    global List<SObject> start(Database.BatchableContext BC) {
        return batchSObjList;
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        try{
            database.update(scope,true);
        }Catch(Exception exp){
            
            throw exp;
        }
    }

    global void finish(Database.BatchableContext BC) {
        // todo delete the batch job
    }

}