@isTest
private class testQuestionnaireOverride {
	static testMethod void unitTestQuestionnaireOverride() {		
		// Create member
		Account a = TestObjectCreator.createFullMemberAccount();
		a.Create_Portal_User__c = true;
		insert a;					
		Questionnaire_Response__c qr = new Questionnaire_Response__c();
		qr.Lock__c = true;
		qr.Account__c = a.Id; 
		ApexPages.standardController con = new ApexPages.standardController(qr);
		QuestionnaireOverride q = new QuestionnaireOverride(con);
		database.insert(qr);	
		Test.startTest();
		Test.setCurrentPage(Page.QuestionnaireOverride);
		q = new QuestionnaireOverride(con);
		q.modeEdit();
		q.generateFormURL();
		q.addPageMessage('test');
		q.callFormAssembly('strFormID', 'String strRequestParameters');
		q.strDebug = 'strDebug';
		QuestionnaireOverride.IsCommunityProfileUser();
		Test.stopTest();
	}
	@testSetup static void createTestData() {
		// Custom settings
		Application_Form__c af1 = new Application_Form__c();
		af1.Name = 'Questionnaire (read only)';
		af1.Form_ID__c = '1';
		af1.ReadOnly__c = false;
		af1.Record_Type__c = 'n/a';
		Application_Form__c af2 = new Application_Form__c();
		af2.Name = 'Questionnaire (editable)';
		af2.Form_ID__c = '2';
		af2.ReadOnly__c = true;
		af2.Record_Type__c = 'n/a';
		List<Application_Form__c> settings = new List<Application_Form__c>();
		settings.add(af1);
		settings.add(af2);
		database.insert(settings);		
	}
}