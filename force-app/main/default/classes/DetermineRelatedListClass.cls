public with sharing class DetermineRelatedListClass {
	// Pass the Application retrieved from controller - should contain the recordTypeId
	public static List<String> getRelatedList(String strRecordType) {
		List<String> relatedListNameList = new List<String>();
		Http h = new Http();
		
        // Get the base url if sandbox or production - needs to be added as a remote site
        String sfdcBaseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        // Build the url
        if(strRecordType != null) {		
            String finalUrl = sfdcBaseUrl + '/services/data/v32.0/sobjects/Application__c/describe/layouts/' + 
                EncodingUtil.urlEncode(strRecordType, 'UTF-8');
            finalUrl = finalUrl.replace('http://', 'https://');
            // Make a HTTP Get Request
            HttpRequest req = new HttpRequest();
            req.SetEndPoint(finalUrl);
            req.setMethod('GET');
            req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
            HttpResponse res = h.send(req);
            // Parse the json for the related list api names using pageLayoutJSON class
            system.debug('## res.getBody() =>' + res.getBody());
            if(res.getStatusCode() == 200) { 
                pageLayoutJSON obj = pageLayoutJSON.parse(res.getBody());
                //system.debug('## obj.relatedLists => ' + obj.relatedLists);
                if(obj!=null && obj.relatedLists!=null && obj.relatedLists.size()>0) {
                    Integer rlSize = obj.relatedLists.size();
                    for(Integer i=0; i < rlSize; i++) {
                        relatedListNameList.add(obj.relatedLists.get(i).name);
                    }
                }
            }
        }
		
		return relatedListNameList;
	}
    // PASA Overload method with additional parameter
    // Pass the Application retrieved from controller - should contain the recordTypeId
	public static List<String> getRelatedList(String strRecordType, String strObjectType) {
		List<String> relatedListNameList = new List<String>();
		Http h = new Http();
		if(!Test.isRunningTest()) {      
			// Get the base url if sandbox or production - needs to be added as a remote site
	        String sfdcBaseUrl = URL.getSalesforceBaseUrl().toExternalForm();
			// Build the url
			if(strRecordType != null) {		
				String finalUrl = sfdcBaseUrl + '/services/data/v32.0/sobjects/' + strObjectType + '/describe/layouts/' + 
									EncodingUtil.urlEncode(strRecordType, 'UTF-8');
				finalUrl = finalUrl.replace('http://', 'https://');
				// Make a HTTP Get Request
				HttpRequest req = new HttpRequest();
				req.SetEndPoint(finalUrl);
				req.setMethod('GET');
				req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
				HttpResponse res = h.send(req);
				// Parse the json for the related list api names using pageLayoutJSON class
				system.debug('## res.getBody() =>' + res.getBody());
				if(res.getStatusCode() == 200) { 
					pageLayoutJSON obj = pageLayoutJSON.parse(res.getBody());
					//system.debug('## obj.relatedLists => ' + obj.relatedLists);
					if(obj!=null && obj.relatedLists!=null && obj.relatedLists.size()>0) {
						Integer rlSize = obj.relatedLists.size();
						for(Integer i=0; i < rlSize; i++) {
							relatedListNameList.add(obj.relatedLists.get(i).name);
						}
					}
				}
			}
		}
		return relatedListNameList;
	}
}