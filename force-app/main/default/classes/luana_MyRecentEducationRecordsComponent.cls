/**
    Developer: WDCi (Lean)
    Development Date: 12/04/2016
    Task #: Controller for luana_MyEducationHistory vf component
    
    Change History
    LCA-704 01-Jul-2016 WDCi - KH add Date_completed__c to the query
    LCA-1070 22/Feb/2017 WDCi - Lean: change the sorting of education record from Date_Commenced__c to date completed 
**/

public without sharing class luana_MyRecentEducationRecordsComponent {
    
    public string contactId;
    
    public luana_MyRecentEducationRecordsComponent (){
        
    }
    
    public void setContactId(String contactId){
        this.contactId = contactId;
    }
    
    public String getContactId(){
        return this.contactId;
    }
    
    public List<Education_Record__c> getEducations() {
         return [SELECT Id, Name, Enter_university_instituition__c, Specialist_hours_code__c, Community_User_Entry__c, Verified__c, Date_completed__c, Date_Commenced__c, Event_Course_name__c, Qualifying_hours__c, Qualifying_hours_type__c, Contact_Student__c, CAHDP__c FROM Education_Record__c WHERE Contact_Student__c =: getContactId() ORDER BY Date_completed__c DESC LIMIT 3]; //LCA-1070
    
    }
}