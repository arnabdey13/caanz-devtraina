/**
* @author Steve Buikhuizen, Jannis Bott
* @date 14/11/2016
*
* @group FormsFramework
*
* @description  Common code used in wrapper production
*/
global abstract with sharing class ff_WrapperSource {

    /**
    * @description All sources must override this method to be used in the general load process.
    */
    global abstract List<ff_Service.CustomWrapper> getWrappers();
}