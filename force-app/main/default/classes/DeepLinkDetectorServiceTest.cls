@IsTest
private class DeepLinkDetectorServiceTest {

    static testMethod void testLoadWithoutNavigation() {
        String destination = DeepLinkDetectorService.load(TestSubscriptionUtils.getTestAccountId());
        System.assertEquals(null, destination, 'test members have no deep link by default');
    }

    static testMethod void testLoadWithNavigation() {
        String destination = '/myProfile';
        Account a = new Account(Id = TestSubscriptionUtils.getTestAccountId(),
                Registration_Destination_URL__c = destination);
        update a;

        String destinationReturned = DeepLinkDetectorService.load(TestSubscriptionUtils.getTestAccountId());
        System.assertEquals(destination, destinationReturned, 'destination is return after first load');

        destinationReturned = DeepLinkDetectorService.load(TestSubscriptionUtils.getTestAccountId());
        System.assertEquals(null, destinationReturned, 'destination was removed after first load');
    }

    @testSetup static void createTestData() {
        TestSubscriptionUtils.createTestData();
    }

}