/*
    Developer: WDCi (KH)
    Date: 29/June/2016
    Task #: Test class for luana_ExamBroadcastController
    
    Change History
    LCA-921             23/08/2016      WDCi KH:    KH add person account email
    PAN:6222            21/05/2019      WDCi LKoh:  Fixed issue with validation on the Account

*/
@isTest(seeAllData=false)
private class luana_AT_ExamBroadcast_Test {

    private static String classNamePrefixLong = 'luana_AT_ExamBroadcast_Test';
    private static String classNamePrefixShort = 'laebt';
    public static Luana_DataPrep_Test testDataGenerator;
    private static map<String, Id> commProfIdMap {get; set;}
    
    public static User memberUser {get; set;}
    
    public static Account memberAccount {get; set;}
    public static luana_CommUserUtil userUtil {get; set;} 
    public static LuanaSMS__Course__c courseV;
    
    public static List<LuanaSMS__Student_Program__c> newStudentPrograms;
    public static LuanaSMS__Session__c newSession1;
    public static LuanaSMS__Session__c newSession2;
    public static LuanaSMS__Session__c newSession3;
    
    public static void initial(){
        //initialize
        testDataGenerator = new Luana_DataPrep_Test();
        
        //Create all the custom setting
        insert testDataGenerator.prepLuanaExtensionSettingCustomSettings();
        insert testDataGenerator.createLuanaConfigurationCustomSetting();
        
        // PAN:6222
        Map<String, SObject> eduAssets = testDataGenerator.createEducationAssets();

        //Create user with Member and Employer community access
        memberAccount = testDataGenerator.generateNewApplicantAcc('Joe_' + classNamePrefixShort, classNamePrefixLong, 'Full_Member');
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.Assessible_for_CA_Program__c = false;
        memberAccount.PersonEmail = 'joe_1_' + classNamePrefixShort+'@gmail.com';//LCA-921
        memberAccount.Create_Portal_User__c = false;
        memberAccount.Communication_Preference__c= 'Home Phone';
        memberAccount.PersonHomePhone= '1234';
        memberAccount.PersonOtherStreet= '83 Saggers Road';
        memberAccount.PersonOtherCity='JITARNING';
        memberAccount.PersonOtherState='Western Australia';
        memberAccount.PersonOtherCountry='Australia';
        memberAccount.PersonOtherPostalCode='6365';
        
        insert memberAccount;

        // PAN:6222 - We can't have the account be set to be assessible for CA Program upon insert as we need the Education History for the Account to do so
        List<Account> checkAccount = [SELECT Id, PersonContactId FROM Account WHERE Id = :memberAccount.Id];
        edu_Education_History__c edu_asset = testDataGenerator.createEducationHistory(null, checkAccount[0].PersonContactId, true, eduAssets.get('universityDegreeJoin1').Id);
        insert edu_asset;
        
        // once the valid Education History is in place, only then can we set the Accessible for CA Program to true
        memberAccount.Assessible_for_CA_Program__c = true;        
        update memberAccount;

        
        System.debug('***memberAccount: ' + memberAccount + ' - ' + UserInfo.getProfileId());
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
    }

    
    //Setup method
    static void setup(String runSeqKey){
        
        memberUser = [Select Id, Email, FirstName, LastName, UserName, Name from User Where Email =: 'joe_1_' + classNamePrefixShort+'@gmail.com' limit 1];
        System.debug('****users: ' + memberUser);
        
        userUtil = new luana_CommUserUtil(memberUser.Id);
        
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
       
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defEmployerPermSetId.value__c));
        
        System.runAs(memberUser){
            insert psas;
        }
        
        LuanaSMS__Training_Organisation__c trainingOrg = testDataGenerator.createNewTraningOrg(classNamePrefixShort, classNamePrefixLong, classNamePrefixShort, 'Virtual Street 123', 'Queensland', '4551');
        insert trainingOrg;
        LuanaSMS__Program__c program = testDataGenerator.createNewProgram(classNamePrefixShort, classNamePrefixLong, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        insert program;
        LuanaSMS__Program_Offering__c po = testDataGenerator.createNewProgOffering('po' + classNamePrefixShort, testDataGenerator.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), program.Id, trainingOrg.Id, null, null, null, 1, 1);
        insert po;
        courseV = testDataGenerator.createNewCourse('Graduate Diploma of Virtual Accounting_' + classNamePrefixShort, po.Id, testDataGenerator.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Program'), 'Running');
        courseV.LuanaSMS__Allow_Online_Enrolment__c = true;
        insert courseV;
        
        List<Account> venues = new List<Account>();
        Account venueAccount1 = testDataGenerator.generateNewBusinessAcc(classNamePrefixShort + '_Test Venue_2'+runSeqKey, 'Venue', null, null, null, null, 'Sunshine Coast Office', 'Active');
        venueAccount1.billingCity = 'city';
        venueAccount1.billingPostalCode = '1234';
        venueAccount1.billingCountry = 'Malaysia';
        venueAccount1.Ext_Id__c = classNamePrefixShort+'_'+runSeqKey+'_1';
        venues.add(venueAccount1);
        
        Account venueAccount2 = testDataGenerator.generateNewBusinessAcc(classNamePrefixShort + '_Test Venue_2'+runSeqKey, 'Venue', null, null, null, null, 'Sunshine Coast Office', 'Active');
        venueAccount2.billingCity = 'city';
        venueAccount2.billingPostalCode = '1234';
        venueAccount2.billingCountry = 'Australia';
        venueAccount2.billingState = 'New South Wales';
        venueAccount1.Ext_Id__c = classNamePrefixShort+'_'+runSeqKey+'_2';
        venues.add(venueAccount2);
        insert venues;
        
        List<LuanaSMS__Room__c> rooms = new List<LuanaSMS__Room__c>();
        LuanaSMS__Room__c room1 = testDataGenerator.newRoom(classNamePrefixShort, classNamePrefixLong+'_1', venueAccount1.Id, 20, 'Classroom', runSeqKey+'_1');
        LuanaSMS__Room__c room2 = testDataGenerator.newRoom(classNamePrefixShort, classNamePrefixLong+'_2', venueAccount2.Id, 20, 'Classroom', runSeqKey+'_2');
        rooms.add(room1);
        rooms.add(room2);
        insert rooms;
        
        
        newStudentPrograms = new List<LuanaSMS__Student_Program__c>();
        Id studProgAccModuleId = testDataGenerator.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module');
        

        LuanaSMS__Student_Program__c newStudProg = testDataGenerator.createNewStudentProgram(studProgAccModuleId, userUtil.custCommConId, courseV.Id, 'Australia', 'In Progress');
        newStudProg.Paid__c = TRUE;
        newStudentPrograms.add(newStudProg);
            
        insert newStudentPrograms;
        
        
        List<LuanaSMS__Session__c> sessions = new List<LuanaSMS__Session__c>();
        newSession1 = testDataGenerator.newSession(classNamePrefixShort, classNamePrefixLong, 'Exam', 20, venueAccount1.Id, room1.Id, null, system.now().addHours(1), system.now().addHours(2));
        newSession1.Topic_Key__c = classNamePrefixShort + '_T1';
        sessions.add(newSession1);
        
        newSession2 = testDataGenerator.newSession(classNamePrefixShort, classNamePrefixLong, 'Exam', 20, venueAccount2.Id, room2.Id, null, system.now().addHours(1), system.now().addHours(2));
        newSession2.Topic_Key__c = classNamePrefixShort + '_T1';
        sessions.add(newSession2);

        newSession3 = testDataGenerator.newSession(classNamePrefixShort, classNamePrefixLong, 'Exam', 20, venueAccount2.Id, room2.Id, null, null, null);
        newSession3.Topic_Key__c = classNamePrefixShort + '_T1';
        sessions.add(newSession3);

        insert sessions;
        
        
  }

  /*Scenario - A course has sent out a virtual broadcast to the student enrolled in it.
   Everything is set up correctly and the job should complete successfully.
  */
    static testMethod void testBroadcastSuccess() {
        
        
        
        Test.startTest();
            initial();
        Test.stopTest();
        
        setup('1');
        List<LuanaSMS__Attendance2__c> atts = new List<LuanaSMS__Attendance2__c>();
        LuanaSMS__Attendance2__c att1 = testDataGenerator.createAttendance(userUtil.custCommConId, 'Exam', newStudentPrograms[0].Id, newSession1.Id);
        LuanaSMS__Attendance2__c att2 = testDataGenerator.createAttendance(userUtil.custCommConId, 'Exam', newStudentPrograms[0].Id, newSession2.Id);
        atts.add(att1);
        atts.add(att2);
        insert atts;
    
        PageReference pageRef = Page.luana_ExamBroadcastWizard;
        pageRef.getParameters().put('id', courseV.id);
        
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController lvcbt_sc = new ApexPages.StandardController(courseV);
        luana_ExamBroadcastController lvcbt_e = new luana_ExamBroadcastController(lvcbt_sc);
        lvcbt_e.doBroadcast();
            
    }
  
    
    /*
        Exam broadcast without attendance
    */
    static testMethod void testBroadcastWithoutAttendance() {
        
        
        Test.startTest();
            initial();
        Test.stopTest();
        
        setup('2');
        PageReference pageRef = Page.luana_ExamBroadcastWizard;
        pageRef.getParameters().put('id', courseV.id);
        
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController lvcbt_sc = new ApexPages.StandardController(courseV);
        luana_ExamBroadcastController lvcbt_e = new luana_ExamBroadcastController(lvcbt_sc);
        lvcbt_e.doBroadcast();
        
        for(String warningMsg: lvcbt_e.warningMessage){
            System.assertEquals(warningMsg.contains('Please create exam attendance for this Student Program record:'), true, 'Warning message for create exam attendance missing');
        }
    }
   
    
    /*
        Exam broadcast without start time & end time
    */
    static testMethod void testBroadcastWithoutStartEndTime() {
        Test.startTest();
            initial();
        Test.stopTest();
        
        setup('3');
        List<LuanaSMS__Attendance2__c> atts = new List<LuanaSMS__Attendance2__c>();
        LuanaSMS__Attendance2__c att1 = testDataGenerator.createAttendance(userUtil.custCommConId, 'Exam', newStudentPrograms[0].Id, newSession3.Id);
        atts.add(att1);
        insert atts;
        
        PageReference pageRef = Page.luana_ExamBroadcastWizard;
        pageRef.getParameters().put('id', courseV.id);
        
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController lvcbt_sc = new ApexPages.StandardController(courseV);
        luana_ExamBroadcastController lvcbt_e = new luana_ExamBroadcastController(lvcbt_sc);
        lvcbt_e.doBroadcast();
    }
   
    
    /*
        Test main broadcast controller
    */
    static testMethod void testMainBroadcastController(){
        
        Test.startTest();
            initial();
        Test.stopTest();
        setup('4');
        ApexPages.StandardController sc = new ApexPages.StandardController(courseV);
        luana_MainBroadcastController mainBroadcast = new luana_MainBroadcastController(sc);
        mainBroadcast.getItems();
        mainBroadcast.setType('Virtual_Classes');
        mainBroadcast.doRedirect();
        mainBroadcast.back();
        mainBroadcast.setType('Exam_Details');
        mainBroadcast.doRedirect();
        mainBroadcast.back();
        mainBroadcast.setType('Wrokshop_Details');
        mainBroadcast.doRedirect();
        
        mainBroadcast.redirectBack();
        mainBroadcast.getType();
    }
   
    
    /*Scenario - Someone tries to broadcast from a non-existant course
    The page will give them an error and be unable to run the job.
    
    static testmethod void testBroadcastFailNoCourse(){
      
      setup();
        
      //Main Test Area
        Test.startTest();
        PageReference pageRef = Page.luana_VirtualClassBroadcastWizard;
        
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController lvcbt_sc = new ApexPages.StandardController(courseV);
        luana_VirtualClassBroadcastController lvcbt_e = new luana_VirtualClassBroadcastController(lvcbt_sc);
        Test.stopTest();
        
        
        List<Apexpages.Message> messages = ApexPages.getMessages();
        boolean found = false;
        for(Apexpages.Message msg : messages){
          if (msg.getDetail().contains('Invalid course. Please try again later or contact your system administrator.')){
            found = true;
          }
        }
        System.assert(found);
      
    }*/
}