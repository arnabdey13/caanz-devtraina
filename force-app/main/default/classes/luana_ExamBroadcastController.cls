/*
    Developer: WDCi (KH)
    Date: 28/06/2016
    Task #: Luana implementation - Exam
*/

public with sharing class luana_ExamBroadcastController {
    
    public boolean enabled {private set; public get;}
    public boolean hasError {private set; public get;}
    public String buttonLocation {private set; public get;}
    
    public LuanaSMS__Course__c course {private set; public get;}
    
    private String targetCourseId {set; get;}
    
    public List<LuanaSMS__Attendance2__c> attlist {get; set;}
    public Map<String, List<LuanaSMS__Student_Program__c>> notAttendanceMap {get; set;}
    public Map<String, List<LuanaSMS__Attendance2__c>> invalidAttMap {get; set;}
    public List<LuanaSMS__Attendance2__c> validSPs {get; set;}
    
    public List<String> warningMessage {get; set;}

    public luana_ExamBroadcastController (ApexPages.StandardController controller) {

      enabled = false;
      hasError = false;
      targetCourseId = getCourseIdFromUrl();
      buttonLocation = 'bottom';
      
      notAttendanceMap = new Map<String, List<LuanaSMS__Student_Program__c>>();  
      warningMessage = new List<String>();
      
      //Check all SP has attendance or not
      validSPs = new List<LuanaSMS__Attendance2__c>();
      for(LuanaSMS__Student_Program__c sp: [select Id, Name, (select Name, LuanaSMS__Student_Program__c, LuanaSMS__Session__r.LuanaSMS__Venue__c, LuanaSMS__Session__r.LuanaSMS__Room__c,LuanaSMS__Session__r.LuanaSMS__Venue__r.Name, LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress, LuanaSMS__Session__r.Name, LuanaSMS__Session__r.LuanaSMS__Start_Time__c, LuanaSMS__Session__r.LuanaSMS__End_Time__c from LuanaSMS__Attendances__r Where Record_Type_Name__c = 'Exam') from LuanaSMS__Student_Program__c where LuanaSMS__Course__c =: targetCourseId and Exam_Broadcasted__c = false and LuanaSMS__Status__c = 'In Progress' and Paid__c = true]){
          if(sp.LuanaSMS__Attendances__r.isEmpty()){
              if(notAttendanceMap.containsKey('emptyAttendance')){
                  notAttendanceMap.get('emptyAttendance').add(sp); 
              }else{
                  List<LuanaSMS__Student_Program__c> newSPs = new List<LuanaSMS__Student_Program__c>();
                  newSPs.add(sp);
                  notAttendanceMap.put('emptyAttendance', newSPs);
                  
              }
          }else{
              for(LuanaSMS__Attendance2__c att: sp.LuanaSMS__Attendances__r){
                  validSPs.add(att);
              }
          }
      }
      
      //Get attendance from SP list
      invalidAttMap = new Map<String, List<LuanaSMS__Attendance2__c>>(); 
      String urlCode = String.valueof(URL.getSalesforceBaseUrl().toExternalForm());
      
      Map<Id, String> venueMap = new Map<Id, String>();
      Map<Id, String> sessionMap = new Map<Id, String>();
      for(LuanaSMS__Attendance2__c att: validSPs){   
          
          String billingAddress;
          if(att.LuanaSMS__Session__r.LuanaSMS__Venue__c != null && !String.isBlank(att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getStreet()) && 
              !String.isBlank(att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getCity()) && 
              !String.isBlank(att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getPostalCode()) &&
              !String.isBlank(att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getCountry())){
              
              if(att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getCountry() != 'Australia'){
                  billingAddress = att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getStreet()+ ' ' +
                   att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getCity()+ ' ' +
                    att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getPostalCode()+ ' ' +
                     att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getCountry();
              }else{
                  if(!String.isBlank(att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getState())){
                      billingAddress = att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getStreet()+ ' ' +
                       att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getCity()+ ' ' +
                        att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getPostalCode()+ ' ' +
                         att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getState()+ ' ' +
                          att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getCountry();
                  }
              }
          }
          
          if(!venueMap.containsKey(att.LuanaSMS__Session__r.LuanaSMS__Venue__c) && att.LuanaSMS__Session__r.LuanaSMS__Venue__c != null){
              venueMap.put(att.LuanaSMS__Session__r.LuanaSMS__Venue__c, att.LuanaSMS__Session__r.LuanaSMS__Venue__r.Name);
              validateAttendanceRecord(att, 'emptyBillingAdd', billingAddress);
          }
          
          if(!sessionMap.containsKey(att.LuanaSMS__Session__c) && att.LuanaSMS__Session__c != null){
              sessionMap.put(att.LuanaSMS__Session__c, att.LuanaSMS__Session__r.Name);
              validateAttendanceRecord(att, 'emptyVenue', att.LuanaSMS__Session__r.LuanaSMS__Venue__c);
              validateAttendanceRecord(att, 'emptyStartTime', String.ValueOf(att.LuanaSMS__Session__r.LuanaSMS__Start_Time__c));
              validateAttendanceRecord(att, 'emptyEndTime', String.ValueOf(att.LuanaSMS__Session__r.LuanaSMS__End_Time__c));
              validateAttendanceRecord(att, 'emptyRoom', att.LuanaSMS__Session__r.LuanaSMS__Room__c);
          }
      }
      
      if(notAttendanceMap.containsKey('emptyAttendance')){
          for(LuanaSMS__Student_Program__c failSP: notAttendanceMap.get('emptyAttendance')){
              warningMessage.add('Please create exam attendance for this Student Program record: <a href="'+urlCode+'/'+failSP.id+'"target="_blank">'+failSP.Name+'</a>');
          }
      }
      
      if(invalidAttMap.containsKey('emptyBillingAdd')){
          for(LuanaSMS__Attendance2__c failAtt: invalidAttMap.get('emptyBillingAdd')){
              warningMessage.add('Please create full address for this Venue record: <a href="'+urlCode+'/'+failAtt.LuanaSMS__Session__r.LuanaSMS__Venue__c+'" target="_blank">'+failAtt.LuanaSMS__Session__r.LuanaSMS__Venue__r.Name+'</a>');
          }
      }
      
      if(invalidAttMap.containsKey('emptyVenue')){
          for(LuanaSMS__Attendance2__c failAtt: invalidAttMap.get('emptyVenue')){
              warningMessage.add('Please provide Venue for this Session: <a href="'+urlCode+'/'+failAtt.LuanaSMS__Session__c+'" target="_blank">'+failAtt.LuanaSMS__Session__r.Name+'</a>');
          }
      }
      
      if(invalidAttMap.containsKey('emptyRoom')){
          for(LuanaSMS__Attendance2__c failAtt: invalidAttMap.get('emptyRoom')){
              warningMessage.add('Please provide Room for this Session: <a href="'+urlCode+'/'+failAtt.LuanaSMS__Session__c+'" target="_blank">'+failAtt.LuanaSMS__Session__r.Name+'</a>');
          }
      }
      
      if(invalidAttMap.containsKey('emptyStartTime')){
          for(LuanaSMS__Attendance2__c failAtt: invalidAttMap.get('emptyStartTime')){
              warningMessage.add('Please provide Start Time for this Session: <a href="'+urlCode+'/'+failAtt.LuanaSMS__Session__c+'" target="_blank">'+failAtt.LuanaSMS__Session__r.Name+'</a>');
          }
      }
      
      if(invalidAttMap.containsKey('emptyEndTime')){
          for(LuanaSMS__Attendance2__c failAtt: invalidAttMap.get('emptyEndTime')){
              warningMessage.add('Please provide End Time for this Session: <a href="'+urlCode+'/'+failAtt.LuanaSMS__Session__c+'" target="_blank">'+failAtt.LuanaSMS__Session__r.Name+'</a>');
          }
      }
      
      if(getCourseIdFromUrl() != null){
        try{
          course = [select Id, Name from LuanaSMS__Course__c where Id =: targetCourseId];
          if(invalidAttMap.isEmpty() && notAttendanceMap.isEmpty()){
              hasError = false;
          }else{
              hasError = true;
          }
          enabled = true;
          
        } catch(Exception exp){
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Error retrieving course. Please try again later or contact your system administrator. Error: ' + exp.getMessage()));
        }
      } else {
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Invalid course. Please try again later or contact your system administrator.'));
      }
    }
    
    public Map<String, List<LuanaSMS__Attendance2__c>> validateAttendanceRecord(LuanaSMS__Attendance2__c att, String typeOfError, String frieldToCheck){
        if(frieldToCheck == null){
            if(invalidAttMap.containsKey(typeOfError)){
                invalidAttMap.get(typeOfError).add(att);
            }else{
                List<LuanaSMS__Attendance2__c> newAtts = new List<LuanaSMS__Attendance2__c>();
                newAtts.add(att);
                invalidAttMap.put(typeOfError, newAtts);
            }
        }
        return invalidAttMap;
    }
    
    private String getCourseIdFromUrl(){
        if(ApexPages.currentPage().getParameters().containsKey('id')){
            return ApexPages.currentPage().getParameters().get('id');
            
        }
        
        return null;
    }
    
    public PageReference doBroadcast(){
        try{
            
            Database.executeBatch(new luana_ExamBroadcastAsync(targetCourseId), 200);
            
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'The job has been submitted.'));
        
        } catch(Exception exp){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Error submitting job: ' + exp.getMessage()));
        }
        enabled = false;
        return null;
    }
    
}