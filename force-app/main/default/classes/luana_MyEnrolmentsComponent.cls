/* 
    Developer: WDCi (Lean)
    Development Date: 15/04/2016
    Task #: luana_MyEnrolmentsComponent for luana_MyEnrolments vf component LCA-247
    
    Change History
    LCA-665     20/06/2016 - WDCi KH:       include credit transfer for capstone enrolment
    LCA-669     21/06/2016 - WDCi KH:       remove credit transfer enrol button
    LCA-829     16/08/2016 - WDCi KH:       change the logic for display the enrol & view button in the community
    LCA-580     08/03/2017 - WDCi Terry:    Backstop
    LCA-1169    06/02/2018 - WDCi Lean:     replace CAPSTONE subject name with checkbox
    LCA-1161    07/03/2018 - WDCi LKoh:     remove reference to the Student Program Status from the current logic for displaying Enrol and View button, replaced with the new logic that checks on custom metadata
                                            add extra column in My Enrolment page 
    LCA-1161    16/03/2018 - WDCi KH:       Update the Community display status in MyEnrolment Page
	PAN:5340    11/10/2018 - WDCi Lean:     add validation for flexipath
    TEQSA change 26/02/2019: Include flag to Hide AM SP from Community
	PAN:6353    05/07/2019 - WDCi LKoh:     added method to support redirection to separate page for displaying Visa and Address fields
*/

public without sharing class luana_MyEnrolmentsComponent {
    
    public string contactId;
    public boolean enabledView;
    
    public Map<String, String> coursesName {public get; private set;}
    
    //LCA-392
    public Map<Id, String> hasExemptedSubjectIdMap {get; set;}
    public Map<Id, Boolean> hasExemptedSubject {get; set;}
    
    //LCA-665
    public Map<Id, Boolean> hasTransferredSubject {get; set;}
    
    //LCA-669
    public Map<Id, Boolean> finalCTSubject {get; set;}
    
    //LCA-829
    public Date currentDate; 
    
    //LCA-1161
    private Map<String, Boolean> viewEnabled;
    private Map<String, Boolean> enrolEnabled;
    private Map<String, String> referenceAction;
    public Map<Id, Boolean> enableViewMap {get;set;}
    public Map<Id, Boolean> enableEnrolMap {get;set;}
    public Boolean communityDisplayEnabled {get; set;}
    
    private Map<Id, boolean> eligibleFlexipathSubjects; //PAN:5340
    
    public luana_MyEnrolmentsComponent(){
        //LCA-392
        hasExemptedSubjectIdMap = new Map<Id, String>();
        hasExemptedSubject = new Map<Id, Boolean>();
        
        //LCA-665
        hasTransferredSubject = new Map<Id, Boolean>();
        
        //LCA-669
        finalCTSubject = new Map<Id, Boolean>();
        
        currentDate = System.today();
        
        //LCA-1161
        Luana_Extension_Settings__c luanaExtCS = Luana_Extension_Settings__c.getValues('CommunityDisplayStatus');
        if(luanaExtCS.Value__c == 'True'){
            communityDisplayEnabled = true;
        }else{
            communityDisplayEnabled = false;
        }
    }
    
    public void setContactId(String contactId){
        this.contactId = contactId;
    }
    
    public String getContactId(){
        return this.contactId;
    }
    
    public void setEnabledView(boolean enabledView){
        this.enabledView = enabledView;
    }
    
    public boolean getEnabledView(){
        return this.enabledView;
    }
    
    public Map<String, List<luana_JoinedEnrolment>> getJoinedSPSs() {
        
        //LCA-392
        hasExemptedSubjectIdMap = new Map<Id, String>();
        hasExemptedSubject = new Map<Id, Boolean>();
        
        //LCA-665
        hasTransferredSubject = new Map<Id, Boolean>();
        
        Map<Id, LuanaSMS__Student_Program_Subject__c> uniqueModuleSPSMap = getUniqueModuleSPSMap();
        
        Map<String, List<luana_JoinedEnrolment>> joinedSPSMap = new Map<String, List<luana_JoinedEnrolment>>();
        
        //LCA-1161, the metadata map contains the map required to determine if the View or Enrol button will be visible
        populateMetadataMap();
        
        eligibleFlexipathSubjects = luana_EnrolmentUtil.eligibleFlexipathSubjects(getContactId()); //PAN:5340
        
        for(LuanaSMS__Student_Program_Subject__c sps : getMasterSPSList()) {
            
            //LCA-392
            if(sps.LuanaSMS__Outcome_National__c == 'Recognition of prior learning granted'){
                if(sps.Module_Exemption_Case__c != null){
                    hasExemptedSubjectIdMap.put(sps.Id, sps.Module_Exemption_Case__c);
                }else{
                    hasExemptedSubjectIdMap.put(sps.Id, 'empty');
                }
                hasExemptedSubject.put(sps.Id, true);
            }else{
                hasExemptedSubjectIdMap.put(sps.Id, '');
                hasExemptedSubject.put(sps.Id, false);
            }
            
            //LCA-665
            if(sps.LuanaSMS__Outcome_National__c == 'Credit transfer/national recognition'){
                hasTransferredSubject.put(sps.Id, true);
            }else{
                hasTransferredSubject.put(sps.Id, false);
            }
            
            
            if(joinedSPSMap.containsKey(sps.LuanaSMS__Student_Program__c)){
                List<luana_JoinedEnrolment> joinedEnrolments = joinedSPSMap.get(sps.LuanaSMS__Student_Program__c);
                
                joinedSPSMap.put(sps.LuanaSMS__Student_Program__c, populateJoinedEnrolments(uniqueModuleSPSMap, joinedEnrolments, sps));
            } else {
                List<luana_JoinedEnrolment> joinedEnrolments = new List<luana_JoinedEnrolment>();
                joinedSPSMap.put(sps.LuanaSMS__Student_Program__c, populateJoinedEnrolments(uniqueModuleSPSMap, joinedEnrolments, sps));
            }
            
        }
        
        //this is to disable capstone if student hasn't passed all the subjects 
        //LCA-665, pass in extra hasTransferredSubject param
        setCapstoneEnrolmentFlag(joinedSPSMap, hasExemptedSubject, hasTransferredSubject);
        
        coursesName = getCourseName(joinedSPSMap.keySet());
        
        System.debug('***joinedSPSMap::: ' + joinedSPSMap);
        return joinedSPSMap;
    }
    
    //LCA-665, pass in extra hasTransferredSubject param
    public void setCapstoneEnrolmentFlag(Map<String, List<luana_JoinedEnrolment>> joinedSPSMap, Map<Id, Boolean> hasExemptedSubject, Map<Id, Boolean> hasTransferredSubject){
        integer exemptedCounter =0;
        //LCA-665
        integer transferredCounter =0;
        
        //LCA-669
        finalCTSubject = new Map<Id, Boolean>();
        
        
        System.debug('***joinedSPSMap1: ' + joinedSPSMap);
        System.debug('***hasExemptedSubject: ' + hasExemptedSubject);
        System.debug('***hasTransferredSubject: ' + hasTransferredSubject);
        
        for(String studProgId : joinedSPSMap.keySet()){
            
            boolean enableCapstone = false;
            integer passedSubjectsCount = 0;
            integer capstoneSubjectsCount = 0;
            integer studProjSubjectsCount = joinedSPSMap.get(studProgId).size();
            
            for(luana_JoinedEnrolment je : joinedSPSMap.get(studProgId)){
                
                if(hasExemptedSubject.containsKey(je.spsId)){
                    if(hasExemptedSubject.get(je.spsId)){
                        exemptedCounter++;
                    }
                }
            
                //if(je.subName == luana_EnrolmentConstants.SUBJECT_CAP){ //LCA-1169 replace capstone flag
                if(je.isCapstone){ //LCA-1169
                    capstoneSubjectsCount ++;
                    finalCTSubject.put(je.spsId, false);//LCA-669
                } else {
                //LCA-665, do not use passing score
                //    if(je.score >= luana_EnrolmentConstants.SUBJECT_PASSING_SCORE){
                //        passedSubjectsCount ++;
                //    }else{
                        if(hasTransferredSubject.containsKey(je.spsId)){//LCA-665
                            if(hasTransferredSubject.get(je.spsId)){
                                transferredCounter++;
                                finalCTSubject.put(je.spsId, true);//LCA-669
                            }else{
                                finalCTSubject.put(je.spsId, false);//LCA-669
                            }
                        }
                //    }
                }
            }
            
            System.debug('***counter1:: ' + passedSubjectsCount +' - '+ exemptedCounter +' - '+ transferredCounter);
            System.debug('***counter2:: ' + studProjSubjectsCount + ' - ' +capstoneSubjectsCount);

            //LCA-665, add transferredCounter
            if((passedSubjectsCount + exemptedCounter + transferredCounter) >= (studProjSubjectsCount - capstoneSubjectsCount)){
                enableCapstone = true;
            }
            
            //LCA-580 Backstop Enrolment
            //Query Allow Backstop Enrolment Flag
            List<Account> accList = [SELECT Allow_Backstop_Enrolment__c FROM Account WHERE PersonContactId=: getContactId()];
            System.debug('***allowBackstop accList: ' + accList);
            boolean allowBackstop = false;
            if(accList.size() > 0)
                allowBackstop = accList.get(0).Allow_Backstop_Enrolment__c;
            if(allowBackstop)
                enableCapstone = true;  
            
            for(luana_JoinedEnrolment je : joinedSPSMap.get(studProgId)){
                //if(je.subName == luana_EnrolmentConstants.SUBJECT_CAP && !enableCapstone && je.isEnrolEnabled) //LCA-1169 replace capstone flag
                if(je.isCapstone && !enableCapstone && je.isEnrolEnabled){ //LCA-1169
                    System.debug('***update capstone:::: ' + je.isCapstone + ' - ' + !enableCapstone + ' - ' + je.isEnrolEnabled);
                    je.isEnrolEnabled = false;
                    je.enrolMode = luana_EnrolmentConstants.ACTION_MODE_ENROL_OFF;
                    je.actionUrl = '';
                }
                
            }
            
            System.debug('***joinedSPSMap2: ' + joinedSPSMap);
        }
    }
    
    
    //LCA-829, check re-enrolment for date
    // LCA-1161 note the currentStatus is not used at the moment for logic consideration by isReEnrolable
    public Boolean isEnrolable(Map<Id, Boolean> enableViewMap, Map<Id, Boolean> enableEnrolMap, String spsId, String poId, String courseId){
        
        // 2nd LCA-1161 change, isReenrolable have been disabled completely and will now simply return true
        // note: disabling the isReenrolable logic will have effect on the way the wrapper class are constructed, for example the IsPaid field value differ depending on the outcome of isReenrolable
        System.debug('*****spsId: ' + spsId);
        //LCA-116 KH update re-enrol logic
                        
        if(enableViewMap.get(spsId) && enableEnrolMap.get(spsId)){          
            return true;
        }else if(enableViewMap.get(spsId) && !enableEnrolMap.get(spsId)){           
            return false;
        }else if(!enableViewMap.get(spsId) && enableEnrolMap.get(spsId)){           
            return true;
        }
        return false;
    }
    
    public List<luana_JoinedEnrolment> populateJoinedEnrolments(Map<Id, LuanaSMS__Student_Program_Subject__c> uniqueModuleSPSMap, List<luana_JoinedEnrolment> joinedEnrolments, LuanaSMS__Student_Program_Subject__c sps){
        
        system.debug('uniqueModuleSPSMap: ' +uniqueModuleSPSMap);
        system.debug('sps: ' +sps);

        if(uniqueModuleSPSMap.containsKey(sps.LuanaSMS__Subject__r.Id)) {
                        
            LuanaSMS__Student_Program_Subject__c modSPS = uniqueModuleSPSMap.get(sps.LuanaSMS__Subject__r.Id);
            
            //LCA-1161
            Id pOId = modSPS.LuanaSMS__Student_Program__r.LuanaSMS__Course__r.LuanaSMS__Program_Offering__c;                    
            
            // LCA-1161
            // map to check the right for access to 'Enrol' or 'View' button for SPS under Accredited Module
                
            if(referenceAction.get(modSPS.LuanaSMS__Student_Program__r.Transcript_Status__c) == 'View'){
                enableEnrolMap.put(sps.Id, false);
                enableViewMap.put(sps.Id, true);
            }else if(referenceAction.get(modSPS.LuanaSMS__Student_Program__r.Transcript_Status__c) == 'Enrol'){
                enableEnrolMap.put(sps.Id, true);
                enableViewMap.put(sps.Id, false);
            }else if(referenceAction.get(modSPS.LuanaSMS__Student_Program__r.Transcript_Status__c) == 'Based on Re-enrol Date'){
                enableEnrolMap.put(sps.Id, true);
                enableViewMap.put(sps.Id, true);
            }else{
                enableEnrolMap.put(sps.Id, false);
                enableViewMap.put(sps.Id, false);
            }
            
            //LCA-1161 KH 16/03/2018  
            if(referenceAction.get(modSPS.LuanaSMS__Student_Program__r.Transcript_Status__c) == 'Based on Re-enrol Date'){
                if(modSPS.LuanaSMS__Student_Program__r.LuanaSMS__Course__r.Re_enrol_Date__c != null){
                    if(modSPS.LuanaSMS__Student_Program__r.LuanaSMS__Course__r.Re_enrol_Date__c > System.today()){
                        //View   
                        joinedEnrolments.add(new luana_JoinedEnrolment(sps.Id,
                                                            modSPS.LuanaSMS__Subject__r.Name,
                                                            modSPS.LuanaSMS__Subject__r.Id,
                                                            modSPS.LuanaSMS__Student_Program__r.LuanaSMS__Course__r.Name,
                                                            modSPS.LuanaSMS__Student_Program__r.LuanaSMS__Status__c,
                                                            modSPS.Result__c,
                                                            modSPS.merit__c,
                                                            modSPS.Score__c,
                                                            modSPS.LuanaSMS__Student_Program__r.Paid__c,
                                                            modSPS.LuanaSMS__Student_Program__r.Id,
                                                            false, 
                                                            luana_EnrolmentConstants.ACTION_MODE_VIEW,
                                                            getViewURL() + '?spid=' + modSPS.LuanaSMS__Student_Program__r.Id,
                                                            modSPS.LuanaSMS__Student_Program__r.Transcript_Status__c,  //LCA-1161 KH 16/03/2018 old value is Final_Module_Status__c
                                                            modSPS.LuanaSMS__Subject__r.Is_Capstone__c //LCA-1169
                                                            ));    
                    }else{
                        //Enrol
                        System.debug('**Re-enrol: ' + modSPS.LuanaSMS__Subject__r.Name + ' - '+ modSPS.LuanaSMS__Student_Program__r.Paid__c + ' - ' + modSPS.LuanaSMS__Student_Program__r.Sup_Exam_Enrolled__c);
                    
                        //LCA-1161 Start here - extra logic to handle display view or enrol button
                        date systemToday = System.today();
                        Boolean hasAvailableCourses = false;
                        Integer enrolMode = 0;
                        String actionUrl = '';
                        String courseName = '';
                        Boolean paidStatus;
                        List<LuanaSMS__Course__c> coursesPerPO = [Select Id, Name from LuanaSMS__Course__c Where 
                                                                LuanaSMS__Status__c = 'Running' and LuanaSMS__Allow_Online_Enrolment__c = true and 
                                                                (Enrolment_Start_Date__c <=: systemToday and Enrolment_End_Date__c >=: systemToday) and 
                                                                LuanaSMS__Program_Offering__c =: poId and Id !=: modSPS.LuanaSMS__Student_Program__r.LuanaSMS__Course__c];
                        System.debug('**coursesPerPO: ' + coursesPerPO);
                        
                        if(!coursesPerPO.isEmpty()){
                            hasAvailableCourses = true;
                            enrolMode = luana_EnrolmentConstants.ACTION_MODE_ENROL_ON;
                            // PAN:6353
                            actionUrl = (sps.LuanaSMS__Student_Program__r.LuanaSMS__Course__r.LuanaSMS__Allow_Online_Enrolment__c ? redirectToPreValidationPage(sps.Id) : '');
                            // actionUrl = (sps.LuanaSMS__Student_Program__r.LuanaSMS__Course__r.LuanaSMS__Allow_Online_Enrolment__c ? getEnrolURL('CA Module') + '&linkedsubjectid=' + sps.LuanaSMS__Subject__r.Id : '');
                        }else{
                            hasAvailableCourses = false;
                            enrolMode = luana_EnrolmentConstants.ACTION_MODE_VIEW;
                            enableViewMap.put(sps.Id, true);
                            actionUrl = getViewURL() + '?spid=' + modSPS.LuanaSMS__Student_Program__r.Id;
                     //       courseName = modSPS.LuanaSMS__Student_Program__r.LuanaSMS__Course__r.Name;
                     //       paidStatus = modSPS.LuanaSMS__Student_Program__r.Paid__c;
                        }
                        
                        //LCA-1161 End here
                        
                        joinedEnrolments.add(new luana_JoinedEnrolment(sps.Id,
                                                                sps.LuanaSMS__Subject__r.Name,
                                                                sps.LuanaSMS__Subject__r.Id,
                                                                //modSPS.LuanaSMS__Student_Program__r.LuanaSMS__Course__r.Name,
                                                                courseName,
                                                                modSPS.LuanaSMS__Student_Program__r.LuanaSMS__Status__c,
                                                                modSPS.Result__c,
                                                                modSPS.Merit__c,
                                                                modSPS.Score__c,
                                                                //modSPS.LuanaSMS__Student_Program__r.Paid__c,
                                                                paidStatus,
                                                                sps.LuanaSMS__Student_Program__r.Id,
                                                                true,
                                                                luana_EnrolmentConstants.ACTION_MODE_ENROL_ON,
                                                                // PAN:6353
                                                                (sps.LuanaSMS__Student_Program__r.LuanaSMS__Course__r.LuanaSMS__Allow_Online_Enrolment__c ? redirectToPreValidationPage(sps.Id) : ''),
                                                                // (sps.LuanaSMS__Student_Program__r.LuanaSMS__Course__r.LuanaSMS__Allow_Online_Enrolment__c ? getEnrolURL('CA Module') + '&linkedsubjectid=' + sps.LuanaSMS__Subject__r.Id : ''),                                                                
                                                                modSPS.LuanaSMS__Student_Program__r.Transcript_Status__c, //LCA-1161 KH 16/03/2018 old value is Final_Module_Status__c
                                                                modSPS.LuanaSMS__Subject__r.Is_Capstone__c //LCA-1169
                                                                ));
                        System.debug('##**1: ' + joinedEnrolments);                                         
                    }
                }else{
                    //If is re-enrol and the re-enrol date is empty show View   
                    joinedEnrolments.add(new luana_JoinedEnrolment(sps.Id,
                                                        modSPS.LuanaSMS__Subject__r.Name,
                                                        modSPS.LuanaSMS__Subject__r.Id,
                                                        modSPS.LuanaSMS__Student_Program__r.LuanaSMS__Course__r.Name,
                                                        modSPS.LuanaSMS__Student_Program__r.LuanaSMS__Status__c,
                                                        modSPS.Result__c,
                                                        modSPS.merit__c,
                                                        modSPS.Score__c,
                                                        modSPS.LuanaSMS__Student_Program__r.Paid__c,
                                                        modSPS.LuanaSMS__Student_Program__r.Id,
                                                        false, 
                                                        luana_EnrolmentConstants.ACTION_MODE_VIEW,
                                                        getViewURL() + '?spid=' + modSPS.LuanaSMS__Student_Program__r.Id,
                                                        modSPS.LuanaSMS__Student_Program__r.Transcript_Status__c,  //LCA-1161 KH 16/03/2018 old value is Final_Module_Status__c
                                                        modSPS.LuanaSMS__Subject__r.Is_Capstone__c //LCA-1169
                                                        ));  
                }
            } else if(referenceAction.get(modSPS.LuanaSMS__Student_Program__r.Transcript_Status__c) == 'View'){
                System.debug('**View: ' + modSPS.LuanaSMS__Subject__r.Name + ' - '+ modSPS.LuanaSMS__Student_Program__r.Paid__c + ' - ' + modSPS.LuanaSMS__Student_Program__r.Sup_Exam_Enrolled__c); 
                joinedEnrolments.add(new luana_JoinedEnrolment(sps.Id,
                                                        modSPS.LuanaSMS__Subject__r.Name,
                                                        modSPS.LuanaSMS__Subject__r.Id,
                                                        modSPS.LuanaSMS__Student_Program__r.LuanaSMS__Course__r.Name,
                                                        modSPS.LuanaSMS__Student_Program__r.LuanaSMS__Status__c,
                                                        modSPS.Result__c,
                                                        modSPS.merit__c,
                                                        modSPS.Score__c,
                                                        modSPS.LuanaSMS__Student_Program__r.Paid__c,
                                                        modSPS.LuanaSMS__Student_Program__r.Id,
                                                        false, 
                                                        luana_EnrolmentConstants.ACTION_MODE_VIEW,
                                                        getViewURL() + '?spid=' + modSPS.LuanaSMS__Student_Program__r.Id,
                                                        modSPS.LuanaSMS__Student_Program__r.Transcript_Status__c,  //LCA-1161 KH 16/03/2018 old value is Final_Module_Status__c
                                                        modSPS.LuanaSMS__Subject__r.Is_Capstone__c //LCA-1169
                                                        )); 
                                                        
            } else if(referenceAction.get(modSPS.LuanaSMS__Student_Program__r.Transcript_Status__c) == 'Enrol'){
                //Enrol
                System.debug('**Enrol: ' + modSPS.LuanaSMS__Subject__r.Name + ' - '+ modSPS.LuanaSMS__Student_Program__r.Paid__c + ' - ' + modSPS.LuanaSMS__Student_Program__r.Sup_Exam_Enrolled__c);
                
                //LCA-1161 Start here - extra logic to handle display view or enrol button
                date systemToday = System.today();
                Boolean hasAvailableCourses = false;
                Integer enrolMode = 0;
                String actionUrl = '';
                String courseName = '';
                Boolean paidStatus;
                List<LuanaSMS__Course__c> coursesPerPO = [Select Id, Name from LuanaSMS__Course__c Where 
                                                        LuanaSMS__Status__c = 'Running' and LuanaSMS__Allow_Online_Enrolment__c = true and 
                                                        (Enrolment_Start_Date__c <=: systemToday and Enrolment_End_Date__c >=: systemToday) and 
                                                        LuanaSMS__Program_Offering__c =: poId and Id !=: modSPS.LuanaSMS__Student_Program__r.LuanaSMS__Course__c];
                
                
                if(!coursesPerPO.isEmpty()){
                    hasAvailableCourses = true;
                    enrolMode = luana_EnrolmentConstants.ACTION_MODE_ENROL_ON;
                    // PAN:6353
                    actionUrl = (sps.LuanaSMS__Student_Program__r.LuanaSMS__Course__r.LuanaSMS__Allow_Online_Enrolment__c ? redirectToPreValidationPage(sps.Id) : '');
                    // actionUrl = (sps.LuanaSMS__Student_Program__r.LuanaSMS__Course__r.LuanaSMS__Allow_Online_Enrolment__c ? getEnrolURL('CA Module') + '&linkedsubjectid=' + sps.LuanaSMS__Subject__r.Id : '');
                    courseName = (isEnrolable(enableViewMap, enableEnrolMap, sps.Id, poId, modSPS.LuanaSMS__Student_Program__r.LuanaSMS__Course__c) ? '' : modSPS.LuanaSMS__Student_Program__r.LuanaSMS__Course__r.Name);
                    paidStatus = (isEnrolable(enableViewMap, enableEnrolMap, sps.Id, poId, modSPS.LuanaSMS__Student_Program__r.LuanaSMS__Course__c) ? false : modSPS.LuanaSMS__Student_Program__r.Paid__c);
                    
                }else{
                    hasAvailableCourses = false;
                    enrolMode = luana_EnrolmentConstants.ACTION_MODE_VIEW;
                    enableViewMap.put(sps.Id, true);
                    actionUrl = getViewURL() + '?spid=' + modSPS.LuanaSMS__Student_Program__r.Id;
                    courseName = modSPS.LuanaSMS__Student_Program__r.LuanaSMS__Course__r.Name;
                    paidStatus = modSPS.LuanaSMS__Student_Program__r.Paid__c;
                }
                
                
                
                //LCA-1161 End here
                
                joinedEnrolments.add(new luana_JoinedEnrolment(sps.Id,
                                                        sps.LuanaSMS__Subject__r.Name,
                                                        sps.LuanaSMS__Subject__r.Id,
                                                        //modSPS.LuanaSMS__Student_Program__r.LuanaSMS__Course__r.Name,
                                                        courseName,
                                                        modSPS.LuanaSMS__Student_Program__r.LuanaSMS__Status__c,
                                                        modSPS.Result__c,
                                                        modSPS.Merit__c,
                                                        modSPS.Score__c,
                                                        //modSPS.LuanaSMS__Student_Program__r.Paid__c,
                                                        paidStatus,
                                                        sps.LuanaSMS__Student_Program__r.Id,
                                                        true,
                                                        luana_EnrolmentConstants.ACTION_MODE_ENROL_ON,
                                                        // PAN:6353
                                                        (sps.LuanaSMS__Student_Program__r.LuanaSMS__Course__r.LuanaSMS__Allow_Online_Enrolment__c ? redirectToPreValidationPage(sps.Id) : ''),
                                                        // (sps.LuanaSMS__Student_Program__r.LuanaSMS__Course__r.LuanaSMS__Allow_Online_Enrolment__c ? getEnrolURL('CA Module') + '&linkedsubjectid=' + sps.LuanaSMS__Subject__r.Id : ''),
                                                        modSPS.LuanaSMS__Student_Program__r.Transcript_Status__c, //LCA-1161 KH 16/03/2018 old value is Final_Module_Status__c
                                                        modSPS.LuanaSMS__Subject__r.Is_Capstone__c //LCA-1169
                                                        ));   
            }
        
       
        } else {
        	
        	boolean flexipathEligible = (eligibleFlexipathSubjects.containsKey(sps.Id) ? eligibleFlexipathSubjects.get(sps.Id) : false); //PAN:5340
            // LCA-1161
            // non accredited module SPS are automatically ignored for consideration with custom metadata (treated as always true)
            enableViewMap.put(sps.Id, true);
            enableEnrolMap.put(sps.Id, flexipathEligible);
            
            
            joinedEnrolments.add(new luana_JoinedEnrolment(sps.Id,
                                                        sps.LuanaSMS__Subject__r.Name,
                                                        sps.LuanaSMS__Subject__r.Id,
                                                        '',
                                                        '',
                                                        '',
                                                        '',
                                                        null,
                                                        false,
                                                        sps.LuanaSMS__Student_Program__r.Id,
                                                        (sps.LuanaSMS__Student_Program__r.LuanaSMS__Course__r.LuanaSMS__Allow_Online_Enrolment__c && flexipathEligible ? true : false),
                                                        (sps.LuanaSMS__Student_Program__r.LuanaSMS__Course__r.LuanaSMS__Allow_Online_Enrolment__c && flexipathEligible ? luana_EnrolmentConstants.ACTION_MODE_ENROL_ON : luana_EnrolmentConstants.ACTION_MODE_ENROL_OFF),
                                                        //TODO need to handle this better to identify the producttype
                                                        // PAN:6353
                                                        (sps.LuanaSMS__Student_Program__r.LuanaSMS__Course__r.LuanaSMS__Allow_Online_Enrolment__c && flexipathEligible ? redirectToPreValidationPage(sps.Id) : ''),
                                                        // (sps.LuanaSMS__Student_Program__r.LuanaSMS__Course__r.LuanaSMS__Allow_Online_Enrolment__c && flexipathEligible ? getEnrolURL('CA Module') + '&linkedsubjectid=' + sps.LuanaSMS__Subject__r.Id : ''),
                                                        sps.LuanaSMS__Student_Program__r.Transcript_Status__c, //LCA-1161 KH 16/03/2018 old value is Final_Module_Status__c
                                                        sps.LuanaSMS__Subject__r.Is_Capstone__c //LCA-1169
                                                        ));      
        }
        
        return joinedEnrolments;
    }

    public List<LuanaSMS__Student_Program_Subject__c> getMasterSPSList() {
        return [SELECT LuanaSMS__Subject__r.Name, LuanaSMS__Student_Program__r.Final_Module_Status__c, LuanaSMS__Subject__r.Id, Module_Exemption_Case__c, LuanaSMS__Outcome_National__c, LuanaSMS__Student_Program__r.LuanaSMS__Course__r.Re_enrol_Date__c, LuanaSMS__Student_Program__r.Sup_Exam_Enrolled__c, Result__c, Score__c, Merit__c, Id, LuanaSMS__Student_Program__c, LuanaSMS__Student_Program__r.Paid__c, LuanaSMS__Student_Program__r.LuanaSMS__Course__c, LuanaSMS__Student_Program__r.LuanaSMS__Course__r.Name, LuanaSMS__Student_Program__r.LuanaSMS__Course__r.LuanaSMS__Allow_Online_Enrolment__c,  
                LuanaSMS__Subject__r.Is_Capstone__c, //LCA-1169
                LuanaSMS__Student_Program__r.Transcript_Status__c //LCA-1161
                FROM  LuanaSMS__Student_Program_Subject__c 
                WHERE LuanaSMS__Student_Program__r.Hide_From_Community__c = false // TEQSA change: Include flag to Hide AM SP from Community
                AND LuanaSMS__Student_Program__r.LuanaSMS__Contact_Student__c =: getContactId() AND
                LuanaSMS__Student_Program__r.Hide_From_Community__c = false AND //Exclude these enrolments
                LuanaSMS__Student_Program__r.RecordType.DeveloperName =: luana_EnrolmentConstants.RECORDTYPE_STUDENTPROGRAM_ACCREDITEDPROGRAM 
                ];
    }
    
    public Map<Id, LuanaSMS__Student_Program_Subject__c> getUniqueModuleSPSMap() {
        Map<Id, LuanaSMS__Student_Program_Subject__c> uniqueModuleSPSMap = new Map<Id, LuanaSMS__Student_Program_Subject__c>();
        
        // LCA-1161
        // Replaced the query with a new version that does not reference the Student Program Status, as that would be handled by the custom metadata based logic map
        // and added new field to the query list 'Final_Module_Status__c'
        for( LuanaSMS__Student_Program_Subject__c sps : [SELECT LuanaSMS__Student_Program__r.Final_Module_Status__c, 
                    Result__c, Score__c, Merit__c, LuanaSMS__Subject__r.Name, LuanaSMS__Subject__r.Id, LuanaSMS__Student_Program__r.Id, LuanaSMS__Student_Program__r.LuanaSMS__Course__r.Re_enrol_Date__c, LuanaSMS__Student_Program__r.LuanaSMS__Course__r.LuanaSMS__Program_Offering__c, LuanaSMS__Student_Program__r.Sup_Exam_Enrolled__c, LuanaSMS__Student_Program__r.Paid__c, LuanaSMS__Student_Program__r.LuanaSMS__Course__r.Name, LuanaSMS__Student_Program__r.LuanaSMS__Status__c,  
                    LuanaSMS__Subject__r.Is_Capstone__c, //LCA-1169
                    LuanaSMS__Student_Program__r.Transcript_Status__c //LCA-1161                    
                    FROM LuanaSMS__Student_Program_Subject__c 
                    WHERE   LuanaSMS__Student_Program__r.Hide_From_Community__c = false // TEQSA change: Include flag to Hide AM SP from Community
                    AND LuanaSMS__Student_Program__r.RecordType.DeveloperName =: luana_EnrolmentConstants.RECORDTYPE_STUDENTPROGRAM_ACCREDITEDMODULE AND 
                    LuanaSMS__Student_Program__r.LuanaSMS__Contact_Student__c=: getContactId() AND
                    LuanaSMS__Student_Program__r.LuanaSMS__Status__c != 'Withdrawn/Cancelled' AND
                    //LCA-387 LuanaSMS__Student_Program__r.LuanaSMS__Status__c != 'Fail' AND
                    LuanaSMS__Student_Program__r.LuanaSMS__Status__c != 'Discontinued' AND
                    LuanaSMS__Student_Program__r.LuanaSMS__Status__c != 'Cancelled'
                    ORDER BY LuanaSMS__Student_Program__r.LuanaSMS__Enrolment_Date__c ASC]) {
             
            uniqueModuleSPSMap.put(sps.LuanaSMS__Subject__r.Id, sps);
        }            
        return uniqueModuleSPSMap;        
    }
    
    public Map<String, String> getCourseName(Set<String> spIds){
        Map<String, String> courseNames = new Map<String, String>();
        
        for(LuanaSMS__Student_Program__c sp : [select Id, LuanaSMS__Course__r.Name from LuanaSMS__Student_Program__c where id in: spIds]){
            courseNames.put(sp.Id, sp.LuanaSMS__Course__r.Name);
        }
        
        return courseNames;
    }
    
    public String getViewURL(){
        
        return luana_NetworkUtil.getCommunityPath() + '/luana_MyEnrolmentView';
    }
    
    public String getEnrolURL(String productType){
        //TODO LCA-247 need to handle enrolment properly for member community in the future
        return luana_NetworkUtil.getCommunityPath() + '/luana_EnrolmentWizardLanding?producttype=' + EncodingUtil.urlEncode(productType, 'UTF-8');
    }
    
    // LCA-1161
    // New logic for checking the custom metadata for the availability of View or Enrol button for each Final Module Status
    public void populateMetadataMap() {
        
        //viewEnabled = new Map<String, Boolean>();
        //enrolEnabled = new Map<String, Boolean>();
        enableViewMap = new Map<Id, Boolean>();
        enableEnrolMap = new Map<Id, Boolean>();
        
        referenceAction = new Map<String, String>();
        
        
        List<Luana_ReferenceTable__mdt> referenceTableList = [SELECT Id, MasterLabel, DeveloperName, Action__c FROM Luana_ReferenceTable__mdt];
        
        for (Luana_ReferenceTable__mdt reference : referenceTableList) {
            
            referenceAction.put(reference.MasterLabel, reference.Action__c);
            
        }
    }

    // PAN:6353
    public String redirectToPreValidationPage(Id spsId) {
        
        String redirectionURLString = luana_NetworkUtil.getCommunityPath() + '/wdci_EnrolmentPreValidationPage?spsId=' +spsId;
        return redirectionURLString;
    }
}