public with sharing class PortalForgotPassword {
    public String username {get; set;}
	public String param {get; set;}
    public PortalForgotPassword() {}
    public User userRecord {get;set;}
	public Id getUserId(String strUsername) {
		userRecord = [select Preferred_Name__c, FirstName, Email from User where Username = :strUsername];
		return userRecord.Id;
	}
	public void replaceTemplateVariables(String strUsername, String strPassword) {
		system.debug('strUsername: ' + strUsername);
		system.debug('strPassword: ' + strPassword);
		String strQuery = 'select Id, Subject, HtmlValue from EmailTemplate where ';
		if(test.isRunningTest()) 
			strQuery += 'DeveloperName = \'Communities_Forgot_Password_Email_HTML_2\'';
		else 
			strQuery += 'DeveloperName = \'Communities_Forgot_Password_Email_HTML\'';
		List<EmailTemplate> emailContents = database.query(strQuery);
		if(emailContents.size()>0) {
			EmailTemplate eTemplate = emailContents[0];
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			mail.setToAddresses(new String[]{userRecord.Email});
			mail.setSubject(eTemplate.Subject);
			String emailBody = eTemplate.HtmlValue;
			if(emailBody.indexOf('username:') != -1){
				emailBody = emailBody.replace('username:', 'username: ' + strUsername);
			}
			if(emailBody.indexOf('password:') != -1){
				emailBody = emailBody.replace('password:', 'password: ' + strPassword);
			}
			if(
			emailBody.indexOf('{!CODE.Account.Preferred_Name__c}') != -1 && 
			userRecord.Preferred_Name__c != null
			){
				emailBody = emailBody.replace('{!CODE.Account.Preferred_Name__c}', 
					userRecord.Preferred_Name__c);
			}
			mail.setHtmlBody(emailBody);
			Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {mail});
		}
	}
  	public PageReference forgotPassword() {
  		PageReference pr = Page.PortalForgotPasswordConfirm;
  		pr.setRedirect(true);
  		if(UserInfo.getUserType() != 'Guest') {
  			addPageMessage(ApexPages.severity.ERROR, 'You are already logged in. Click <a href="/home/home.jsp"><span style="font-size:12px;">here</span></a> to go to the home page');
  			return null;
  		}
  		try {
	  		Id pId = getUserId(username);
	  		if(pId != null) {
	  			system.debug('### reset User Id: ' + pId);
	  			System.ResetPasswordResult result = System.resetPassword(pId, false);
	  			System.debug('Temporary Password: ' + result.getPassword());
	  			replaceTemplateVariables(username, result.getPassword());
	  		}

  		}
  		catch(Exception e) {
  			system.debug('### Error: ' + e);
  			addPageMessage(ApexPages.severity.ERROR, e);
  			return null;
  		}
  		return pr;
  	}
    public PageReference register() {
    	PageReference pref = new PageReference('/PortalSelfRegistration?type=' + param);
    	pref.setRedirect(true);
    	return pref;
    }
	// Add Page Messages
	public void addPageMessage(ApexPages.severity severity, Object objMessage) {
		ApexPages.Message pMessage = new ApexPages.Message(severity, String.valueOf(objMessage));
		ApexPages.addMessage(pMessage);
	}
	public void addPageMessage(Object objMessage) {
		addPageMessage(ApexPages.severity.INFO, objMessage);
	}
}