/*
    Developer: WDCi (Lean)
    Date: 15/Aug/2016
    Task #: LCA-802 Luana implementation - Wizard for CASM workshop allocation
    
    Change History:
    LCA-866 17/Aug/2016 - WDCi Lean: create course session
*/

public without sharing class luana_MasterclassAllocationWizardCtrl {
    
    public LuanaSMS__Course__c course {private set; public get;}
    public List<luana_WorkshopCourse> workshopCourses {private set; public get;}
    
    public boolean enabled {private set; public get;}
    
    public List<luana_ErrorRecordWrapper> sessionErrorRecords {private set; get;}
    public List<luana_ErrorRecordWrapper> attendanceErrorRecords {private set; get;}
    
    public Map<String, List<LuanaSMS__Student_Program__c>> courseStudentPrograms {private set; get;}
    
    public integer successCount {private set; get;}
    public integer errorCount {private set; get;}
    
    private String targetCourseId {set; get;}
    
    private Luana_Extension_Settings__c luanaReportConfig {set; get;}
    private Luana_Extension_Settings__c luanaDefaultSeriesCountConfig {set; get;}
    private Luana_Extension_Settings__c luanaDefaultMaxSeatConfig {set; get;}
    
    private String sessionCASMRTId;
    private Map<String, String> sessionNames;
    
    public luana_MasterclassAllocationWizardCtrl (ApexPages.StandardController controller) {
        
        sessionErrorRecords = new List<luana_ErrorRecordWrapper>();
        attendanceErrorRecords = new List<luana_ErrorRecordWrapper>();
        
        successCount = 0;
        errorCount = 0;
        
        targetCourseId = getCourseIdFromUrl();
        
        sessionCASMRTId = [select id from RecordType where SObjectType = 'LuanaSMS__Session__c' and DeveloperName =: luana_SessionConstants.RECORDTYPE_SESSION_CASM].Id;
        init();
    }
    
    private String getCourseIdFromUrl(){
        if(ApexPages.currentPage().getParameters().containsKey('id')){
            return ApexPages.currentPage().getParameters().get('id');
            
        }
        
        return null;
    }
    
    public void init(){
        enabled = false;
        
     	sessionNames = new Map<String, String>();
     	courseStudentPrograms = new Map<String, List<LuanaSMS__Student_Program__c>>();
     	
     	//even though we know there is only 1 course, we still use list here for future enhancement and more dynamic
     	workshopCourses = new List<luana_WorkshopCourse>(); 
     	
     	luanaReportConfig = Luana_Extension_Settings__c.getValues('CASM Allocation Report Params');
        luanaDefaultSeriesCountConfig = Luana_Extension_Settings__c.getValues('CASM Allocation Default Series');
        luanaDefaultMaxSeatConfig = Luana_Extension_Settings__c.getValues('CASM Allocation Default Max Seat');
        
        if(luanaReportConfig != null && luanaDefaultSeriesCountConfig != null && luanaDefaultMaxSeatConfig != null){
            if(targetCourseId != null){
            	
            	try{
                    //get course name, also work as validation to make sure course is valid
                    course = [select Id, Name from LuanaSMS__Course__c where Id =: targetCourseId];
                    
                    System.debug('*****getStudentProgramQuery()::: ' + getStudentProgramQuery());
                    
                    //group the students by workshop location preference and day
                    for(LuanaSMS__Student_Program__c studProg : Database.query(getStudentProgramQuery())){
                    	
                    	System.debug('*****studProg::: ' + studProg);
                        
                        if(studProg.LuanaSMS__Attendances__r.isEmpty()){
                        	
                        	if(courseStudentPrograms.containsKey(studProg.LuanaSMS__Course__c)){
                                List<LuanaSMS__Student_Program__c> existingStudProgs = courseStudentPrograms.get(studProg.LuanaSMS__Course__c);
                                existingStudProgs.add(studProg);
                                
                                courseStudentPrograms.put(studProg.LuanaSMS__Course__c, existingStudProgs);
                                
                            } else {
                                List<LuanaSMS__Student_Program__c> newStudProgs = new List<LuanaSMS__Student_Program__c>();
                                newStudProgs.add(studProg);
                                
                                courseStudentPrograms.put(studProg.LuanaSMS__Course__c, newStudProgs);
                                
                            }
                        }
                    }
                    
                    if(!courseStudentPrograms.isEmpty()){
                    	
                    	//prepare empty key for session without topic_key__c
                        String emptyGroupHash = system.now().format('yyyyMMddHHmmssSSS');
                        integer emptyGroupCounter = 1;
                        
                        //we don't multiple courses here, so single level is good enough to handle the grouping
                        luana_WorkshopCourse workshopCourse = new luana_WorkshopCourse(course.Id, course.Name, course.Name, new List<luana_WorkshopSessionGroup>(), courseStudentPrograms.get(course.Id).size());
                        
                        Map<String, luana_WorkshopSessionGroup> sessionGroupsByKey = new Map<String, luana_WorkshopSessionGroup>();
                        
                        for(LuanaSMS__Course_Session__c courseSession : Database.query(getCourseSessionQuery())){
                        	integer maxCap = courseSession.LuanaSMS__Session__r.Maximum_Capacity__c == null ? null : courseSession.LuanaSMS__Session__r.Maximum_Capacity__c.intValue();
                            integer xSeries = courseSession.LuanaSMS__Session__r.X_of_Series__c == null ? null : courseSession.LuanaSMS__Session__r.X_of_Series__c.intValue();
                            
                            if(courseSession.LuanaSMS__Session__r.Topic_Key__c != null && sessionGroupsByKey.containsKey(courseSession.LuanaSMS__Session__r.Topic_Key__c)){
                                luana_WorkshopSessionGroup existingWsg = sessionGroupsByKey.get(courseSession.LuanaSMS__Session__r.Topic_Key__c);
                                
                                existingWsg.sessions.add(courseSession);
                                
                                sessionGroupsByKey.put(courseSession.LuanaSMS__Session__r.Topic_Key__c, existingWsg);
                            } else {
                                List<LuanaSMS__Course_Session__c> sList = new List<LuanaSMS__Course_Session__c>();
                                sList.add(courseSession);
                                
                                String groupKey;
                                if(courseSession.LuanaSMS__Session__r.Topic_Key__c != null){
                                    groupKey = courseSession.LuanaSMS__Session__r.Topic_Key__c;
                                } else {
                                    groupKey = emptyGroupHash + '_' + emptyGroupCounter;
                                    emptyGroupCounter ++;
                                }
                                
                                luana_WorkshopSessionGroup newWsg = new luana_WorkshopSessionGroup(workshopCourse.id, (courseSession.LuanaSMS__Session__r.Topic_Key__c != null ? courseSession.LuanaSMS__Session__r.Topic_Key__c : null), (courseSession.LuanaSMS__Session__r.Topic_Key__c != null ? courseSession.LuanaSMS__Session__r.Topic_Key__c : 'Empty Group'), sList, maxCap, maxCap, xSeries, (courseSession.LuanaSMS__Session__r.LuanaSMS__Number_of_enrolled_students__c >= courseSession.LuanaSMS__Session__r.Maximum_Capacity__c ? false : true), false);
                                
                                sessionGroupsByKey.put(groupKey, newWsg);
                                
                                
                                System.debug('****sList: ' + sList);
                            }
                        }
                        
                        //add all existing session list
                        workshopCourse.sessionGroups.addAll(sessionGroupsByKey.values());
                        
                        //define an empty grouping and add into the list
                        luana_WorkshopSessionGroup emptyWsg = new luana_WorkshopSessionGroup(workshopCourse.id, null, 'Define details for new session', new List<LuanaSMS__Course_Session__c>(), 0, Integer.valueOf(luanaDefaultMaxSeatConfig.Value__c), Integer.valueOf(luanaDefaultSeriesCountConfig.Value__c), true, true);
                                        
                        workshopCourse.sessionGroups.add(emptyWsg);
                                        
                        workshopCourses.add(workshopCourse);
                        
                        enabled = true;
                        
                    } else {
                        addPageMessage(ApexPages.severity.INFO, 'No outstanding enrolment to be allocated for this course.');
                    }
                    
            	} catch(Exception exp){
                    addPageMessage(ApexPages.severity.ERROR, 'Error retrieving course. Please try again later or contact your system administrator. Error: ' + exp.getMessage() + ' - ' + exp.getStackTraceString());
                }
            	
            }else {
                addPageMessage(ApexPages.severity.ERROR, 'Invalid course. Please try again later or contact your system administrator.');
            }
        } else {
            addPageMessage(ApexPages.severity.ERROR, 'Missing configuration \'CASM Allocation Default Max Seat\', \'CASM Allocation Report Params\' and \'CASM Allocation Default Series\'. Please try again later or contact your system administrator.');
        }
    }
    
    public PageReference doSave(){
    	
    	boolean hasDataError = false;
        String newGroupHash = system.now().format('yyyyMMddHHmmss');
        
        List<LuanaSMS__Attendance2__c> newWorkshopAttendance = new List<LuanaSMS__Attendance2__c>();
        List<LuanaSMS__Attendance2__c> newWorkshopAttendanceWithRoom = new List<LuanaSMS__Attendance2__c>();
        List<LuanaSMS__Course_Session__c> newCourseSessions = new List<LuanaSMS__Course_Session__c>(); //LCA-866
        
        Map<String, List<LuanaSMS__Session__c>> newSessionGroups = new Map<String, List<LuanaSMS__Session__c>>();
        Map<String, List<LuanaSMS__Attendance2__c>> newSessionGroupAttendances = new Map<String, List<LuanaSMS__Attendance2__c>>();
        
        sessionErrorRecords = new List<luana_ErrorRecordWrapper>();
        attendanceErrorRecords = new List<luana_ErrorRecordWrapper>();
        
        
        for(luana_WorkshopCourse workshopCourse : workshopCourses){
        	integer studentSequence = 0;
        	//step#1: we assign student to existing session first with fill up
        	
	        for(luana_WorkshopSessionGroup wsg : workshopCourse.sessionGroups){
	        	if(wsg.fillUp && !wsg.isNew && courseStudentPrograms.containsKey(workshopCourse.id) && wsg.sessions != null && !wsg.sessions.isEmpty()){
	        		List<LuanaSMS__Student_Program__c> studentPrograms = courseStudentPrograms.get(workshopCourse.id);
                    
                    for(;studentSequence < studentPrograms.size();){
                    	LuanaSMS__Student_Program__c studentProg = studentPrograms.get(studentSequence);
                        
                        List<LuanaSMS__Attendance2__c> tempAttendances = new List<LuanaSMS__Attendance2__c>();
                        List<LuanaSMS__Attendance2__c> tempAttendancesWithRoom = new List<LuanaSMS__Attendance2__c>();
                        
                        for(LuanaSMS__Course_Session__c courseSession : wsg.sessions){
                            //step#1.1: we need to make sure that all the session in the series/group has enough seat to fill in
                            if((courseSession.LuanaSMS__Session__r.LuanaSMS__Number_of_enrolled_students__c + studentSequence) < courseSession.LuanaSMS__Session__r.Maximum_Capacity__c){
                                LuanaSMS__Attendance2__c newAttendance = new LuanaSMS__Attendance2__c();
                                //attendance mapping here
                                newAttendance.LuanaSMS__Contact_Student__c = studentProg.LuanaSMS__Contact_Student__c;
                                newAttendance.LuanaSMS__Session__c = courseSession.LuanaSMS__Session__r.Id;
                                newAttendance.LuanaSMS__Student_Program__c = studentProg.Id;
                                newAttendance.LuanaSMS__Type__c = 'Student';
                                newAttendance.LuanaSMS__Start_time__c = courseSession.LuanaSMS__Session__r.LuanaSMS__Start_Time__c;
                                newAttendance.LuanaSMS__End_time__c = courseSession.LuanaSMS__Session__r.LuanaSMS__End_Time__c;
                                
                                if(courseSession.LuanaSMS__Session__r.LuanaSMS__Room__c != null){
                                    tempAttendancesWithRoom.add(newAttendance);
                                } else {
                                    tempAttendances.add(newAttendance);
                                }
                                
                                sessionNames.put(courseSession.LuanaSMS__Session__r.Id, courseSession.LuanaSMS__Session__r.Name);
                            } else {
                                break;
                            }
                        }
                        
                        if(wsg.sessions.size() == (tempAttendances.size() + tempAttendancesWithRoom.size())){
                            newWorkshopAttendance.addAll(tempAttendances);
                            newWorkshopAttendanceWithRoom.addAll(tempAttendancesWithRoom);
                            
                            studentSequence ++;
                        } else {
                            break;
                        }
                    }
	        	}
	        	
	        }
	        
	        //step#2: we then only assign to new session
            integer groupCount = 1;
            
            for(luana_WorkshopSessionGroup wsg : workshopCourse.sessionGroups){
                if(wsg.fillUp && wsg.isNew && courseStudentPrograms.containsKey(workshopCourse.id)){
                	List<LuanaSMS__Student_Program__c> studentPrograms = courseStudentPrograms.get(workshopCourse.id);
                	
                	Decimal remainingStudentsToAllocate = Decimal.valueOf(studentPrograms.size() - studentSequence);
                	
                	if(remainingStudentsToAllocate > 0){
                        Decimal avgCap = Decimal.valueOf(wsg.avgSeat == null || wsg.avgSeat == 0 ? wsg.maxSeat : wsg.avgSeat);
                        Decimal maxCap = Decimal.valueOf(wsg.maxSeat == null ? 0 : wsg.maxSeat);
                     	
                     	if(maxCap == 0){
                            addPageMessage(ApexPages.severity.ERROR, 'The \'Max. Seat\' for new session cannot be zero or empty. Please verify the details again.');
                            hasDataError = true;
                            
                            break;
                        } else if(avgCap > maxCap){
                            addPageMessage(ApexPages.severity.ERROR, 'The \'Avg. Seat\' for new session cannot be more than \'Max. Set\'. Please verify the details again.');
                            hasDataError = true;
                            
                            break;
                            
                        } else {
                        	//we need to find out the total # of new groups to be created
                            Decimal noOfNewSessionsGroup = (remainingStudentsToAllocate < avgCap ? 1 : (remainingStudentsToAllocate/avgCap));
                            noOfNewSessionsGroup = noOfNewSessionsGroup.round(system.RoundingMode.CEILING);
                            
                            //prepare the new sessions and group them by key e.g, 20160701010000_01DXXXXXXXXXXXX_Saturday_G1
                            for(integer i = 0; i < noOfNewSessionsGroup; i ++){
                                for(integer j = 0; j < wsg.noOfSeries; j ++){
                                	String extId = newGroupHash + '_' + workshopCourse.Id + '_G' + (groupCount + i) + '_' + (j + 1);
                                	
                                    LuanaSMS__Session__c newSession = new LuanaSMS__Session__c();
                                    
                                    newSession.Name = course.Name + ' - G' + (groupCount + i) + '-' + (j + 1);
                                    newSession.RecordTypeId = sessionCASMRTId;
                                    newSession.X_of_Series__c = wsg.noOfSeries;
                                    newSession.Maximum_Capacity__c = wsg.maxSeat;
                                    newSession.Topic_Key__c = course.Name + '_G' + (groupCount + i);                                  
                                    newSession.LuanaSMS__External_Id__c = extId;
                                                                        
                                    if(newSessionGroups.containsKey(newSession.Topic_Key__c)){
                                        newSessionGroups.get(newSession.Topic_Key__c).add(newSession);
                                    } else {
                                        newSessionGroups.put(newSession.Topic_Key__c, new List<LuanaSMS__Session__c>{newSession});
                                    }
                                    
                                    //LCA-866 create course session to link course and session
                                    LuanaSMS__Course_Session__c newCourseSession = new LuanaSMS__Course_Session__c();
                                    newCourseSession.LuanaSMS__Course__c = course.Id;
                                    newCourseSession.LuanaSMS__Session__r = new LuanaSMS__Session__c(LuanaSMS__External_Id__c = extId);
                                    
                                    newCourseSessions.add(newCourseSession);
                                    
                                }
                            }
                            
                            system.debug('newSessionGroups :: ' + newSessionGroups.keySet());
                            
                            integer newGroupStudentCount = 0; //counter for number of student added to a new group
                            
                            //loop through list of new group and allocated students to the series of sessions
                            for(String key : newSessionGroups.keySet()){
                            	if(key.contains(course.Name)){
                            		for(;studentSequence < studentPrograms.size();){
                                        LuanaSMS__Student_Program__c studentProg = studentPrograms.get(studentSequence);
                                        
                                        if(newGroupStudentCount < avgCap){
                                            for(LuanaSMS__Session__c session : newSessionGroups.get(key)){
                                                
                                                LuanaSMS__Attendance2__c newSessionAttendance = new LuanaSMS__Attendance2__c();
                                                //attendance mapping here
                                                newSessionAttendance.LuanaSMS__Contact_Student__c = studentProg.LuanaSMS__Contact_Student__c;
                                                newSessionAttendance.LuanaSMS__Student_Program__c = studentProg.Id;
                                                newSessionAttendance.LuanaSMS__Type__c = 'Student';
                                                
                                                if(newSessionGroupAttendances.containsKey(session.LuanaSMS__External_Id__c)){
                                                    newSessionGroupAttendances.get(session.LuanaSMS__External_Id__c).add(newSessionAttendance);
                                                } else {
                                                    newSessionGroupAttendances.put(session.LuanaSMS__External_Id__c, new List<LuanaSMS__Attendance2__c>{newSessionAttendance});
                                                }
                                            }
                                            
                                            newGroupStudentCount ++;
                                            studentSequence ++;
                                        } else {
                                            newGroupStudentCount = 0;
                                            break;
                                        }
                                    }
                            	}
                            }
                        }   
                	}
                }
                
                groupCount ++;
            }
            
            if(hasDataError){
                break;
            }
        }
        
        if(!hasDataError){
            //TODO dml here
            Savepoint sp = Database.setSavepoint();
            
            try{
            	Set<Id> succeededSessionIds = new Set<Id>();
                Set<Id> succeededStudentIds = new Set<Id>();
                Set<Id> failedStudentIds = new Set<Id>();
                
                List<LuanaSMS__Session__c> newWorkshopSessions = new List<LuanaSMS__Session__c>();
                for(List<LuanaSMS__Session__c> newSessions : newSessionGroups.values()){
                    newWorkshopSessions.addAll(newSessions);
                }
                
                //ensure all sessions can be created before hand  
                insert newWorkshopSessions;
                
                for(LuanaSMS__Session__c sr : newWorkshopSessions){
                    if(sr.Id != null){
                        sessionNames.put(sr.Id, sr.Name);
                    }
                }
                
                //LCA-866 ensure all course session can be created before hand as we dont want to lost the relationship
                insert newCourseSessions;
                
                List<LuanaSMS__Attendance2__c> newWorkshopAttendanceForNewSession = new List<LuanaSMS__Attendance2__c>();
                for(String key : newSessionGroups.keySet()){
                    List<LuanaSMS__Attendance2__c> newSessionAttendances = newSessionGroupAttendances.get(key);
                    List<LuanaSMS__Session__c> newSessions = newSessionGroups.get(key);
                    
                    system.debug('newSessionAttendances :: ' + newSessionAttendances);
                    system.debug('newSessions :: ' + newSessions);
                    
                    for(LuanaSMS__Session__c newSession : newSessionGroups.get(key)){
                        if(newSession.Id != null){ //we will skip the attendance if the new session is failed to be created
                            for(LuanaSMS__Attendance2__c newSessionAttendance : newSessionGroupAttendances.get(newSession.LuanaSMS__External_Id__c)){
                                newSessionAttendance.LuanaSMS__Session__c = newSession.Id;
                                newWorkshopAttendanceForNewSession.add(newSessionAttendance);
                            }
                        } else {
                            for(LuanaSMS__Attendance2__c newSessionAttendance : newSessionGroupAttendances.get(newSession.LuanaSMS__External_Id__c)){
                                failedStudentIds.add(newSessionAttendance.LuanaSMS__Contact_Student__c);
                            }
                        }
                    }
                }
                
                //dml for attendance for session with room - not sure why salesforce requires us to provide the layout required field. that's why we have to split the dml here
                for(List<SObject> pagedNewAttendances : luana_ListUtil.getPageIterable(newWorkshopAttendanceWithRoom, 600)){
                    Database.SaveResult[] saveResults = Database.insert(pagedNewAttendances, false);
                    
                    system.debug('saveResults with room::' + saveResults);
                    
                    integer attCounter = 0;
                    for(Database.SaveResult sr : saveResults){
                        LuanaSMS__Attendance2__c resultAtt = (LuanaSMS__Attendance2__c)pagedNewAttendances.get(attCounter);
                        
                        system.debug('saveResult with room ::' + sr.isSuccess() + ' - ' + sr.getErrors());
                        
                        if(sr.isSuccess()){
                            succeededStudentIds.add(resultAtt.LuanaSMS__Contact_Student__c);
                            succeededSessionIds.add(resultAtt.LuanaSMS__Session__c);
                        } else {
                            luana_ErrorRecordWrapper errorRecord = new luana_ErrorRecordWrapper(null, resultAtt, 'Attendace', sr.getErrors()[0].getMessage());
                            attendanceErrorRecords.add(errorRecord);
                            
                            failedStudentIds.add(resultAtt.LuanaSMS__Contact_Student__c);
                        }
                        
                        attCounter ++;
                    }
                }
                
                //dml for attendance for session without room
                newWorkshopAttendance.addAll(newWorkshopAttendanceForNewSession);
                
                for(List<SObject> pagedNewAttendances : luana_ListUtil.getPageIterable(newWorkshopAttendance, 600)){
                    Database.SaveResult[] saveResults = Database.insert(pagedNewAttendances, false);
                    
                    system.debug('saveResults without room ::' + saveResults);
                    
                    integer attCounter = 0;
                    for(Database.SaveResult sr : saveResults){
                        LuanaSMS__Attendance2__c resultAtt = (LuanaSMS__Attendance2__c)pagedNewAttendances.get(attCounter);
                        
                        system.debug('saveResult without room ::' + sr.isSuccess() + ' - ' + sr.getErrors());
                        
                        if(sr.isSuccess()){
                            succeededStudentIds.add(resultAtt.LuanaSMS__Contact_Student__c);
                            succeededSessionIds.add(resultAtt.LuanaSMS__Session__c);
                        } else {
                            luana_ErrorRecordWrapper errorRecord = new luana_ErrorRecordWrapper(null, resultAtt, 'Attendance', sr.getErrors()[0].getMessage());
                            attendanceErrorRecords.add(errorRecord);
                            
                            failedStudentIds.add(resultAtt.LuanaSMS__Contact_Student__c);
                        }
                        
                        attCounter ++;
                    }
                }
                
                successCount = succeededStudentIds.size();
                errorCount = failedStudentIds.size();
                
                if(successCount > 0){
                
                    String detailsMsg = '';
                    for(Id sessionId : succeededSessionIds){
                        detailsMsg += '<a href="' + luanaReportConfig.Value__c + String.valueOf(sessionId).substring(0, 15) + '" target="_blank">' + sessionNames.get(sessionId) + '</a><br/>';
                    }
                    
                    addPageMessage(ApexPages.severity.INFO, successCount + ' student program/s are allocated successfully. Please see the reports below for details.');
                    addPageMessage(ApexPages.severity.INFO, detailsMsg);
                }
                
                if(errorCount > 0){
                    addPageMessage(ApexPages.severity.ERROR, 'There are ' + errorCount + ' student program/s failed to be allocated.');
                }
                
                init();
                
            } catch(Exception exp){
                
                addPageMessage(ApexPages.severity.ERROR, 'Error creating workshop sessions/attendances. Please try again later or contact your system administrator. Error: ' + exp.getMessage() + ' - ' + exp.getStackTraceString());
                Database.rollback(sp);
                
            }
            
        }
        
    	return null;
    }
    
    public void addPageMessage(ApexPages.severity sev, String msg){
    	ApexPages.Message warningmsg = new ApexPages.Message(sev, msg);
        ApexPages.addmessage(warningmsg);
    }
    
    private String getStudentProgramQuery(){
    	return 'select Id, Name, LuanaSMS__Course__c, LuanaSMS__Contact_Student__c, (select Id, Name from LuanaSMS__Attendances__r where RecordType.DeveloperName = \'' + luana_SessionConstants.RECORDTYPE_ATTENDANCE_CASM + '\') from LuanaSMS__Student_Program__c where LuanaSMS__Course__c =: targetCourseId and Paid__c = true and LuanaSMS__Status__c = \'In Progress\' order by Random_Number__c asc';
    }
    
    private String getCourseSessionQuery(){
    	return 'select Id, Name, LuanaSMS__Course__c, LuanaSMS__Session__r.Id, LuanaSMS__Session__r.Name, LuanaSMS__Session__r.LuanaSMS__Number_of_enrolled_students__c, LuanaSMS__Session__r.Maximum_Capacity__c, LuanaSMS__Session__r.LuanaSMS__Start_Time__c, LuanaSMS__Session__r.LuanaSMS__End_Time__c, LuanaSMS__Session__r.X_of_Series__c, LuanaSMS__Session__r.Day_of_Week__c, LuanaSMS__Session__r.Topic_Key__c, LuanaSMS__Session__r.LuanaSMS__Venue__r.Name, LuanaSMS__Session__r.LuanaSMS__Room__c, LuanaSMS__Session__r.LuanaSMS__Room__r.Name from LuanaSMS__Course_Session__c where LuanaSMS__Course__c =: targetCourseId and LuanaSMS__Session__r.RecordType.DeveloperName = \'' + luana_SessionConstants.RECORDTYPE_SESSION_CASM + '\' order by LuanaSMS__Session__r.Topic_Key__c, LuanaSMS__Session__r.LuanaSMS__Start_Time__c asc nulls last';
    }
    
    class luana_ErrorRecordWrapper{
        
        public String errorMessage {set;get;}
        public LuanaSMS__Attendance2__c attendance {set; get;}
        public LuanaSMS__Session__c session {set; get;}
        public String objectType {set;get;}
        
        public luana_ErrorRecordWrapper(LuanaSMS__Session__c session, LuanaSMS__Attendance2__c attendance, String objectType, String errorMessage){
            this.session = session;
            this.attendance = attendance;
            this.objectType = objectType;
            this.errorMessage = errorMessage;
        }
    }
    
    class luana_WorkshopCourse {
        
        public String id {set; get;}
        public String courseId {set; get;}
        public String displayName {set; get;}
        public String name {set; get;}
        public List<luana_WorkshopSessionGroup> sessionGroups {set; get;}
        public integer noOfStudents {set; get;}
        
        public luana_WorkshopCourse(String courseId, String name, String displayName, List<luana_WorkshopSessionGroup> sessionGroups, integer noOfStudents){
            this.id = courseId;
            
            this.courseId = courseId;
            this.name = name;
            this.displayName = displayName;
            this.sessionGroups = sessionGroups;
            this.noOfStudents = noOfStudents;
        }
    }
    
    class luana_WorkshopSessionGroup {
        
        public String parentId {set; get;}
        public String groupKey {set; get;}
        public String name {set; get;}
        public List<LuanaSMS__Course_Session__c> sessions {set; get;}
        public integer avgSeat {set; get;}
        public integer maxSeat {set; get;}
        public integer noOfSeries {set; get;}
        public boolean fillUp {set; get;}
        public boolean isNew {set; get;}
        
        public luana_WorkshopSessionGroup(String parentId, String groupKey, String name, List<LuanaSMS__Course_Session__c> sessions, integer avgSeat, integer maxSeat, integer noOfSeries, boolean fillUp, boolean isNew){
            this.parentId = parentId;
            this.groupKey = groupKey;
            this.name = name;
            this.sessions = sessions;
            this.avgSeat = avgSeat;
            this.maxSeat = maxSeat;
            this.noOfSeries = noOfSeries;
            this.fillUp = fillUp;
            this.isNew = isNew;
        }
        
        public boolean getHasExistingSession(){
            if(sessions != null && sessions.size() > 0){
                return true;
            } else {
                return false;
            }
        }
        
    }
}