/*
    Developer: WDCi (Vin)
    Date: 9/Oct/2018
    Task #: CAANZ - Flexipath   
*/

public with sharing class FP_SPSCompetencyAreaHandler{
    
    @future
    public static void createGainedCompetencyArea(List<Id> spsList){
        
        Set<String> spsOutcomeSet= new Set<String>();
        Set<Id> subjectSet = new Set<Id>();
        
        //Get SPS with positive outcome by referring to the custom metadata
        for (FP_PositiveOutcome__mdt translationRecord : [SELECT Id, MasterLabel, Positive_Outcome__c FROM FP_PositiveOutcome__mdt]) {
            if (translationRecord.Positive_Outcome__c == TRUE) spsOutcomeSet.add(translationRecord.MasterLabel.toLowerCase());
        }
        
        List<LuanaSMS__Student_Program_Subject__c> spsPositiveList= [SELECT Id, LuanaSMS__Subject__c, LuanaSMS__Contact_Student__c FROM LuanaSMS__Student_Program_Subject__c WHERE Id IN : spsList AND LuanaSMS__Outcome_National__c IN: spsOutcomeSet];
        
        // Build a Map for Student and Subject
        Map<Id, Set<Id>> stuSubjectSet = new Map<Id, Set<Id>>();
        for(LuanaSMS__Student_Program_Subject__c sps : spsPositiveList) {
            subjectSet.add(sps.LuanaSMS__Subject__c);
        }
        
        // Build a Map for Subjects with set of gains Competency Area
        List<FP_Relevant_Competency_Area__c> subjectRCAList = [SELECT Id, FP_Type__c, FP_Subject__c, FP_Competency_Area__c FROM FP_Relevant_Competency_Area__c WHERE FP_Subject__c IN: subjectSet AND FP_Competency_Area__r.FP_Active__c = TRUE AND FP_Type__c = 'Gains'];
        
        Map<Id, Set<Id>> subjectCASet = new Map<Id, Set<Id>>();
        
        for(FP_Relevant_Competency_Area__c rca : subjectRCAList) {
            if(subjectCASet.get(rca.FP_Subject__c) != null) {
                subjectCASet.get(rca.FP_Subject__c).add(rca.FP_Competency_Area__c);
            }else{
                Set<Id> newCASet = new Set<Id>();
                newCASet.add(rca.FP_Competency_Area__c);
                subjectCASet.put(rca.FP_Subject__c, newCASet);
            }
        }
        
        List<FP_Gained_Competency_Area__c> newGCAUpsert = new List<FP_Gained_Competency_Area__c>();
        for(LuanaSMS__Student_Program_Subject__c sps : spsPositiveList){
            for(Id subjectCA: subjectCASet.keySet()){
                if(sps.LuanaSMS__Subject__c == subjectCA){
                    for(Id compAreaId : subjectCASet.get(subjectCA)){
                        FP_Gained_Competency_Area__c gca = new FP_Gained_Competency_Area__c(FP_External_Id__c=sps.LuanaSMS__Contact_Student__c+'|'+compAreaId, FP_Contact__c=sps.LuanaSMS__Contact_Student__c, FP_Competency_Area__c=compAreaId, FP_Student_Program_Subject__c=sps.Id);
                        newGCAUpsert.add(gca);
                    }
                }
            }
        }
        
        upsert newGCAUpsert FP_External_Id__c;
    }

}