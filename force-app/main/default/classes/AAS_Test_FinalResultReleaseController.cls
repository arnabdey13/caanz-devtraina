/**
    Developer: WDCi (kh)
    Development Date:13/12/2016
    Task: Test class for AAS_FinalResultReleaseController
    
    Change History:	
	LCA-1176            13/03/2018      WDCi LKoh:  added coverage
**/
@isTest(seeAllData=false)
private class AAS_Test_FinalResultReleaseController {

    final static String contextLongName = 'AAS_Test_FinalResultReleaseController';
    final static String contextShortName = 'frr';
    
    public static AAS_Course_Assessment__c courseAss;
    public static AAS_Course_Assessment__c courseAss2;
    public static AAS_Student_Assessment__c sa;
    public static AAS_Student_Assessment_Section__c sas;
    
    public static List<AAS_Student_Assessment_Section_Item__c> sasiList;
    
    public static List<LuanaSMS__Student_Program__c> spList;
    private static List<Account> memberAccounts;
    private static List<Contact> contactList;
    
    public static LuanaSMS__Student_Program__c newStudProg1;
    public static LuanaSMS__Student_Program__c newStudProg2;
    public static LuanaSMS__Student_Program__c newStudProg3;
    public static LuanaSMS__Student_Program__c newStudProg4;
    
    public static User memberUser {get; set;}
    public static Account memberAccount {get; set;}
    private static map<String, Id> commProfIdMap {get; set;}
    public static luana_CommUserUtil userUtil {get; set;}
    
    public static Luana_DataPrep_Test dataPrepUtil {get; set;}
    
    public static void initialMember(String runSeqNo){
        //initialize
        dataPrepUtil = new Luana_DataPrep_Test();
        
        //Create all the custom setting
         
        insert dataPrepUtil.createLuanaConfigurationCustomSetting();
        
        List<Luana_Extension_Settings__c> customSessingList = dataPrepUtil.prepLuanaExtensionSettingCustomSettings();
        customSessingList.add(new Luana_Extension_Settings__c(Name = 'AdobeConnect_URL', Value__c = '  https://connect-staging.charteredaccountantsanz.com'));
        insert customSessingList;
        
        //Create user with Member and Employer community access
        memberAccount = dataPrepUtil.generateNewApplicantAcc('Joe_' + contextShortName, contextLongName, 'Full_Member');
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.Assessible_for_CA_Program__c = true;
        memberAccount.PersonEmail = 'joe_'+runSeqNo+'_' + contextShortName +'@gmail.com';//LCA-921
        insert memberAccount;
        
        System.debug('******memberAccount:: ' + memberAccount);
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
    }
    
    public static void initial(Boolean isCapstone, String runSeqKey){

        memberUser = [Select Id, Email, FirstName, LastName, UserName, Name from User Where Email =: 'joe_'+runSeqKey+'_' + contextShortName+'@gmail.com' limit 1];

        userUtil = new luana_CommUserUtil(memberUser.Id);
        
        AAS_DataPrep_Test aasdataPrepUtil = new AAS_DataPrep_Test();
        
        insert aasdataPrepUtil.defauleCustomSetting();
        
        Map<String, Id> recordTypeMap = aasdataPrepUtil.getRecordTypeMap('Exam');
        
        //Course
        Map<String, SObject> basicAssetMap = aasdataPrepUtil.createBasicAssets(contextLongName, contextShortName, dataPrepUtil, aasdataPrepUtil);
        LuanaSMS__Program_Offering__c poAM = new LuanaSMS__Program_Offering__c();
        poAM = (LuanaSMS__Program_Offering__c)basicAssetMap.get('PROGRAMOFFERRING1');

        LuanaSMS__Course__c courseAM = aasdataPrepUtil.createLuanaCourse(poAM.Id, dataPrepUtil);
        courseAM.LuanaSMS__Allow_Online_Enrolment__c = true;
        insert courseAM;
        
        // LKoh :: 15-05-2017
        // Note :: fix for the failing test class due to newly introduced restriction on Course Assessment needing unique Course
        List<LuanaSMS__Course__c> courseAMList = aasdataPrepUtil.createGenericLuanaAMCourse(1, poAM.Id, dataPrepUtil);
        insert courseAMList[0];
        
        List<LuanaSMS__Course__c> course = [Select Id, Name, LuanaSMS__Program_Offering__r.IsCapstone__c from LuanaSMS__Course__c Where Id=:courseAM.Id];
        System.debug('***course:: ' + course);
        
        List<LuanaSMS__Program_Offering__c> po = [Select Id, Name, IsCapstone__c from LuanaSMS__Program_Offering__c Where Id=:poAM.Id];
        System.debug('***po:: ' + po);
        
        
        List<LuanaSMS__Student_Program__c> stuPrograms = new List<LuanaSMS__Student_Program__c>();
        newStudProg1 = dataPrepUtil.createNewStudentProgram(dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), userUtil.custCommConId, courseAM.Id, '', 'Fail');
        newStudProg1.Paid__c = true;
        newStudProg1.Supp_Exam_Broadcasted__c = false;
        newStudProg1.Sup_Exam_Enrolled__c = false;
        newStudProg1.Supp_Exam_Special_Consideration_Approved__c = 'Approved';
        newStudProg1.Supp_Exam_Eligible__c = false;
        stuPrograms.add(newStudProg1);
        insert stuPrograms;
        
        LuanaSMS__Subject__c sub = dataPrepUtil.createNewSubject('FIN_' + contextShortName, 'FIN_' + contextLongName, 'FIN_' + contextShortName, 55, 'Module');
        insert sub;
        
        //Create student Program Subject
        LuanaSMS__Student_Program_Subject__c sps = dataPrepUtil.createNewStudentProgramSubject(userUtil.custCommConId, newStudProg1.Id, sub.Id);
        insert sps;
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.AAS_Course_Assessment__c; 
        Map<String,Schema.RecordTypeInfo> courseAssessmentRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        System.debug('********recordType: ' + courseAssessmentRecordTypeInfo);
        
        
        Id rtTechId = courseAssessmentRecordTypeInfo.get('Technical Module').getRecordTypeId();
        Id rtCapId = courseAssessmentRecordTypeInfo.get('Capstone Module').getRecordTypeId();
        
        // LKoh :: 15-05-2017
        system.debug('## TEST :: rtTechId : ' +rtTechId);
        system.debug('## TEST :: rtCapId : ' +rtCapId);
        
        //CA
        courseAss = aasdataPrepUtil.createCourseAssessment(courseAM.Id, 'CA_' + contextShortName);
        courseAss.AAS_Module_Passing_Mark__c = 40;
        courseAss.AAS_Non_Exam_Data_Loaded__c = true;
        courseAss.AAS_Exam_Data_Loaded__c = true;
        courseAss.AAS_Cohort_Adjustment_Exam__c = true;
        courseAss.AAS_Borderline_Remark__c = true;
        courseAss.RecordTypeId = rtTechId;
        insert courseAss;
        
        // LKoh :: 15-05-2017
        // courseAss2 = aasdataPrepUtil.createCourseAssessment(courseAM.Id, 'CA_' + contextShortName);
        courseAss2 = aasdataPrepUtil.createCourseAssessment(courseAMList[0].Id, 'CA_' + contextShortName);
        courseAss2.AAS_Module_Passing_Mark__c = 40;
        courseAss2.AAS_Non_Exam_Data_Loaded__c = true;
        courseAss2.AAS_Exam_Data_Loaded__c = true;
        courseAss2.AAS_Cohort_Adjustment_Exam__c = true;
        courseAss2.AAS_Borderline_Remark__c = true;
        courseAss.RecordTypeId = rtCapId;
        insert courseAss2;
        

        //ASS
        AAS_Assessment_Schema_Section__c ass = aasdataPrepUtil.createAsessmentSchemaSection(courseAss.Id, 'ASS_' + contextShortName, recordTypeMap.get('AAS_Assessment_Schema_Section__c:Exam_Assessment'));
        ass.AAS_Passing_Mark__c = 40;
        ass.AAS_Cohort_Adjustment__c = 2;
        insert ass;
        
        List<AAS_Assessment_Schema_Section_Item__c> assiList = new List<AAS_Assessment_Schema_Section_Item__c>();
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA117 Q #1'));
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA117 Q #2'));
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA117 Q #3'));
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA117 Q #4'));
        insert assiList;
        
        //SA - 
        sa = aasdataPrepUtil.createStudentAssessment(courseAss.Id);
        sa.AAS_Student_Program__c = newStudProg1.Id;
        sa.AAS_Final_Result__c = 'Pass';
        insert sa;
        
        AAS_Student_Assessment__c sa2 = aasdataPrepUtil.createStudentAssessment(courseAss2.Id);
        sa2.AAS_Student_Program__c = newStudProg1.Id;
        sa2.AAS_Final_Result__c = 'Fail';
        insert sa2;

        //SAS
        sas = aasdataPrepUtil.createStudentAssessmentSection(sa.Id, recordTypeMap.get('AAS_Student_Assessment_Section__c:Exam_Assessment_SAS'));
        sas.AAS_Assessment_Schema_Section__c = ass.Id;
        sas.AAS_Adjustment__c = 1;
        insert sas;
        
        AAS_Student_Assessment_Section__c sas2 = aasdataPrepUtil.createStudentAssessmentSection(sa2.Id, recordTypeMap.get('AAS_Student_Assessment_Section__c:Exam_Assessment_SAS'));
        sas2.AAS_Assessment_Schema_Section__c = ass.Id;
        sas2.AAS_Adjustment__c = 1;
        insert sas2;
        
        //SASI
        sasiList = new List<AAS_Student_Assessment_Section_Item__c>();
        
        for(AAS_Assessment_Schema_Section_Item__c assi: assiList){
            AAS_Student_Assessment_Section_Item__c sasi = aasdataPrepUtil.createStudentAssessmentSectionItem(sas.Id, recordTypeMap.get('AAS_Student_Assessment_Section_Item__c:Exam_Assessment_SASI'), assi.Id);
            sasi.AAS_Raw_Mark__c = 9;
            sasi.AAS_Adjustment__c = 0.75;
            //sasi.AAS_Special_Consideration_Adjustment__c = 0;
            sasiList.add(sasi);
        }
        insert sasiList;
        
        List<AAS_Student_Assessment_Section_Item__c> sasiList2 = new List<AAS_Student_Assessment_Section_Item__c>();
        
        for(AAS_Assessment_Schema_Section_Item__c assi: assiList){
            AAS_Student_Assessment_Section_Item__c sasi2 = aasdataPrepUtil.createStudentAssessmentSectionItem(sas2.Id, recordTypeMap.get('AAS_Student_Assessment_Section_Item__c:Exam_Assessment_SASI'), assi.Id);
            sasi2.AAS_Raw_Mark__c = 12;
            sasi2.AAS_Adjustment__c = 0.75;
            //sasi2.AAS_Special_Consideration_Adjustment__c = 0;
            sasiList2.add(sasi2);
        }
        insert sasiList2;
        
    }
 
    
    public static testMethod void test_FinalCalNotDone() { 
        Test.startTest();
            initialMember('3');
        Test.stopTest();
        
        initial(true, '3');

        try{
            PageReference pageRef1 = Page.AAS_FinalResultRelease;
            pageRef1.getParameters().put('id', String.valueOf(courseAss.Id));
            pageRef1.getParameters().put('type', String.valueOf('Exam_Assessment'));
            Test.setCurrentPage(pageRef1);
            
            ApexPages.StandardController sc1 = new ApexPages.StandardController(courseAss);
            AAS_FinalResultReleaseController finalResultRel = new AAS_FinalResultReleaseController(sc1);
            
            finalResultRel.toLandingPage();
            finalResultRel.doCommit();
            finalResultRel.doRedirect();
            finalResultRel.doCancel();
            finalResultRel.updateFinalResultReleaseBatchProcessingFlag(courseAss, true, true);
            finalResultRel.doWebserviceCallout();
            
        }Catch(Exception exp){
            Boolean expectedExceptionThrown =  exp.getMessage().contains('You are not allow to continue in this step. Please complete the Cohort Adjustment, Borderline Remark, Final Adjusment and Final Result Calculation processes first.') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true, exp.getMessage());
        }
        
    }  
    
    public static testMethod void test_FinalResRelDoneBefore() { 
        
        Test.startTest();
            initialMember('2');
        Test.stopTest();
        
        initial(true, '2');
        
        courseAss.AAS_Final_Adjustment_Exam__c = true;
        courseAss.AAS_Final_Result_Calculation_Exam__c = true;
        courseAss.AAS_Final_Result_Release_Exam__c= true;
        update courseAss;

        try{
            PageReference pageRef1 = Page.AAS_FinalResultRelease;
            pageRef1.getParameters().put('id', String.valueOf(courseAss.Id));
            pageRef1.getParameters().put('type', String.valueOf('Exam_Assessment'));
            Test.setCurrentPage(pageRef1);
            
            ApexPages.StandardController sc1 = new ApexPages.StandardController(courseAss);
            AAS_FinalResultReleaseController finalResultRel = new AAS_FinalResultReleaseController(sc1);
        }Catch(Exception exp){
            Boolean expectedExceptionThrown =  exp.getMessage().contains('You are not allow to continue in this step. Please complete the Cohort Adjustment, Borderline Remark, Final Adjusment and Final Result Calculation processes first.') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true, exp.getMessage());
        }
    }  
    
    
    public static testMethod void test_FinalResRelPassMain() { 
        
        Test.startTest();
            initialMember('1');
        Test.stopTest();
        
        initial(true, '1');
        
        courseAss.AAS_Final_Adjustment_Exam__c = true;
        courseAss.AAS_Final_Result_Calculation_Exam__c = true;
        update courseAss;
        
        PageReference pageRef1 = Page.AAS_FinalResultRelease;
        pageRef1.getParameters().put('id', String.valueOf(courseAss.Id));
        pageRef1.getParameters().put('type', String.valueOf('Exam_Assessment'));
        Test.setCurrentPage(pageRef1);
        
        ApexPages.StandardController sc1 = new ApexPages.StandardController(courseAss);
        AAS_FinalResultReleaseController finalResultRel = new AAS_FinalResultReleaseController(sc1);
        
        // LCA-1176
        finalResultRel.courseDummy.Result_Release_Date__c = system.today();
        
        finalResultRel.doCommit();
        finalResultRel.doCancel();
        
        finalResultRel.isUpdateDone = true;
        finalResultRel.doRedirect();
  //      finalResultRel.checkBatchCompletion();
  
  		// LCA-1176
  		finalResultRel.getReleaseDateOptions(); 
    } 
    
    public static testMethod void test_FinalResRelFailMain() { 
        
        Test.startTest();
            initialMember('0');
        Test.stopTest();
        
        initial(true, '0');
        
        courseAss.AAS_Final_Adjustment_Exam__c = true;
        courseAss.AAS_Final_Result_Calculation_Exam__c = true;
        courseAss.AAS_Final_Result_Release_Processing__c = true;
        update courseAss;
        
        PageReference pageRef1 = Page.AAS_FinalResultRelease;
        pageRef1.getParameters().put('id', String.valueOf(courseAss2.Id));
        pageRef1.getParameters().put('type', String.valueOf('Exam_Assessment'));
        Test.setCurrentPage(pageRef1);
        
        ApexPages.StandardController sc1 = new ApexPages.StandardController(courseAss2);
        AAS_FinalResultReleaseController finalResultRel = new AAS_FinalResultReleaseController(sc1);
        
        finalResultRel.doCommit();
        finalResultRel.doCancel();
        
        finalResultRel.isUpdateDone = false;
        finalResultRel.checkCommitStatus = false;
        finalResultRel.doRedirect();
   //     finalResultRel.checkBatchCompletion(); 
   
        // LKoh :: 15-05-2017
        ApexPages.StandardController sc2 = new ApexPages.StandardController(courseAss);
        AAS_FinalResultReleaseController finalResultRel2 = new AAS_FinalResultReleaseController(sc2);
    }
    
}