@isTest(seeAllData = false)
public class ConnectedAppPluginHandler_Test {

    public static User currentUser;
    public static Account currentAccount;
    static {
        currentAccount = CASUB_MainComponent_Controller_CA_Test.createFullMemberAccount();
        currentAccount.Affiliated_Branch_Country__c = 'Australia';
        insert currentAccount;
        
        Account acc = [Select PersonContactId From Account Where Id = :currentAccount.Id];
        
        Profile p = [SELECT Id FROM Profile WHERE Name = 'CAANZ CCH Community Member' LIMIT 1];
            User u = new User(
            FirstName = 'TestFirstName',
            LastName = 'TestLastName',
            Email = 'test@user.com',
            Username = 'test@user.com',
            Alias = 'TestPA', 
            TimeZoneSidKey = 'GMT', 
            LocaleSidKey = 'en_US', 
            EmailEncodingKey = 'UTF-8', 
            ProfileId = p.Id, 
            LanguageLocaleKey = 'en_US',
            ContactId = acc.PersonContactId);
            insert u;

    }

    public static User getCurrentUser() {
        return [SELECT Id, FirstName, LastName, Email,AccountId, Contact.AccountId, Profile.Name, Name,
                Account.Affiliated_Branch_Country__c FROM User WHERE AccountId =: currentAccount.Id];

    }

   public static Integer getRandomNumber(Integer size) {
        Double d = math.random() * size;
        return d.intValue();
    }

   /* Test method for LinkedIn Learning - 01*/
   @isTest static void customAttributesWithout_Segmentation_LinkedInLearning() {
        
            ConnectedApplication conApp = [Select Id, Name from ConnectedApplication 
                                            where Name = 'LinkedInLearning' limit 1];
            Map<String,String> mapstr = new Map<String,String>();
            Test.startTest();
       		ConnectedAppPluginHandler  cap = new ConnectedAppPluginHandler();
       		currentUser = getCurrentUser();
            Map<String,String> respMapStr = cap.customAttributes(currentUser.Id,conApp.Id,mapstr,NULL);
            Test.stopTest();
            
       
    }
    
   /* Test method for LinkedIn Learning - 02*/ 
   @isTest static void customAttributesWith_Segmentation_LinkedInLearning() {
        
            ConnectedApplication conApp = [Select Id, Name from ConnectedApplication 
                                            where Name = 'LinkedInLearning' limit 1];
            Map<String,String> mapstr = new Map<String,String>();
         
         	Segmentation__c seg = new Segmentation__c();
         	seg.Account__c = currentAccount.Id;
         	seg.Name = 'Test Name';
         	seg.Organisation_Type__c = 'Commerce';
         	seg.Career_Stage__c = 'Team Player';
            insert seg;
         	
            Test.startTest();
       		currentUser = getCurrentUser();
         	Segmentation__c segm = [Select Id, Career_Stage__c,Account__r.Membership_Class__c, Organisation_Type__c From 
                                         Segmentation__c where Account__c =: currentUser.Contact.AccountId
                                         order by createdDate desc limit 1];
            
         
            ConnectedAppPluginHandler  cap = new ConnectedAppPluginHandler();
            Map<String,String> linkedIn = cap.customAttributes(currentUser.Id,conApp.Id,mapstr,NULL);
            Test.stopTest();
            
        
    }
    
    /* Test method for NetSuite*/
     @isTest static void customAttributes_NetSuite() {
        
            ConnectedApplication conApp = [Select Id, Name from ConnectedApplication 
                                            where Name = 'NetSuite' limit 1];
            Map<String,String> mapstr = new Map<String,String>();
            Test.startTest();
       		ConnectedAppPluginHandler  cap = new ConnectedAppPluginHandler();
       		currentUser = getCurrentUser();
            Map<String,String> respMapStr = cap.customAttributes(currentUser.Id,conApp.Id,mapstr,NULL);
            Test.stopTest();
            
       
    }
}