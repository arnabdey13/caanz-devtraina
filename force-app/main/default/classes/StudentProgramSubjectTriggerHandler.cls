/***********************************************************************************************************************************************************************
Name: StudentProgramSubjectTriggerHandler 
============================================================================================================================== 
Purpose: Trigger Handler for Merging of Multiple Triggers of LuanaSMS__Student_Program_Subject__c. Below are the list of Triggers Merged
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR                 DATE          DETAIL     Description 
1.0         Vinay               26/02/2019    Created    Trigger Handler for Merging of Multiple Triggers of LuanaSMS__Student_Program_Subject__c. Below are the list of Triggers Merged
                                                            * GenerateCAProgramSUOS
                                                            * luana_SPSDeleteLog
                                                            * FP_SPSCompetencyArea                                                            
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/
public with sharing class StudentProgramSubjectTriggerHandler extends TriggerHandler{
    //Context Variable Collections after Type Casting
    List<LuanaSMS__Student_Program_Subject__c> newList = (List<LuanaSMS__Student_Program_Subject__c>)Trigger.new;
    List<LuanaSMS__Student_Program_Subject__c> oldList = (List<LuanaSMS__Student_Program_Subject__c>)Trigger.old;
    Map<Id,LuanaSMS__Student_Program_Subject__c> oldMap = (Map<Id,LuanaSMS__Student_Program_Subject__c>)Trigger.oldMap;
    Map<Id,LuanaSMS__Student_Program_Subject__c> newMap = (Map<Id,LuanaSMS__Student_Program_Subject__c>)Trigger.newMap;

    //Constructor
    public StudentProgramSubjectTriggerHandler() {

    }

    public override void beforeInsert(){
        //Creates Student Unit Of Study and associate with Student Program Subject.
        StudentProgramSubjectHandler.GenerateCAProgramSUOS(newList);
    }
                    
    public override void beforeUpdate(){
        
    }       
    
    public override void beforeDelete(){
        //Deleted Student Program Subject Records will be Archived into Luana Log sObject.
        list<LuanaSMS__Luana_Log__c> luanaLogList = new list<LuanaSMS__Luana_Log__c>();

        for(LuanaSMS__Student_Program_Subject__c p : oldList) {
            luanaLogList.add(new LuanaSMS__Luana_Log__c(LuanaSMS__Level__c = 'Info', LuanaSMS__Log_Message__c = JSON.serializePretty(p), LuanaSMS__Reference__c = 'LuanaSMS__Student_Program_Subject__c', LuanaSMS__Status__c = 'Completed', LuanaSMS__Type__c = 'Record Deletion'));
        }
        
        if(luanaLogList != null && !luanaLogList.isEmpty()){
            insert luanaLogList;
        }
    }
            
    public override void afterInsert(){
        list<Id> spsIdsList = new list<Id>();
        spsIdsList.addAll(newMap.KeySet());
        //Created GainedCompetencyArea for Positive Outcome Student Program Subjects and this is a asyncronus process(future method).
        FP_SPSCompetencyAreaHandler.createGainedCompetencyArea(spsIdsList);        
    }

    public override void afterUpdate(){
        list<Id> spsIdsList = new list<Id>();
        spsIdsList.addAll(newMap.KeySet());
        //Created GainedCompetencyArea for Positive Outcome Student Program Subjects and this is a asyncronus process(future method).
        FP_SPSCompetencyAreaHandler.createGainedCompetencyArea(spsIdsList);
    }
    
    public override void afterDelete(){
        
    }
    
    public override void afterUndelete(){
        
    }
}