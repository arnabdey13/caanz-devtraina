@isTest(seeAllData=false)
public class RecognitionMatchKeyRefreshTest {

   ////////////////////// Main Practice Lookup Prep ///////////////////////////////////////////////////////////////
    @testSetup 
    static void setup() {
             
		// insert some businesses  
		Account big4BusinessAccountNZ = SegmentationDataGenTest.genPracticeNewZealand( 'Ernst & Young', 'Chartered Accounting', 'Auckland', 25);
		insert big4BusinessAccountNZ;
		Account smallBusinessAccount = SegmentationDataGenTest.genPracticeNewZealand( 'Bob Accounting', 'Chartered Accounting', 'Wellington', 1);
		insert smallBusinessAccount;
        Account midTierBusinessAccount = SegmentationDataGenTest.genPracticeNewZealand( 'BDO', 'Chartered Accounting', 'Hamilton', 15);
        insert midTierBusinessAccount;  
        Account big4BusinessAccountAU = SegmentationDataGenTest.genPracticeAU( 'Deloitte', 'Chartered Accounting', 'Sydney', 25);
		insert big4BusinessAccountAU;
        Account smallBusinessAccountAU = SegmentationDataGenTest.genPracticeAU( 'Young and Co', 'Chartered Accounting', 'Brisbane', 2);
		insert smallBusinessAccountAU;
    }

	// Test 
	private static testMethod void runBatchUpdate_1(){
		System.debug( 'Starting Batch');
        
        Test.startTest();           
        RecognitionMatchKeyRefresh objClass = new RecognitionMatchKeyRefresh(TRUE);
		Database.executeBatch (objClass, 10);
        
		Test.stopTest();
		List<Reference_Recognition_CAANZ_Repository__c> repositoryAfter = [	Select ID, Account__c, Company_Name__c, 
                                                                          	BillingStreet__c, BillingCity__c, BillingPostalCode__c, BillingDPID__c,  
																			ShippingStreet__c, ShippingCity__c, ShippingPostalCode__c, ShippingDPID__c, Phone__c,
                                                                           	BusNameFuzzy__c, BusSearch_Key__c, MATCH_KEY__c, Match_Score__c, AddressSearch_Key__c
                                                                          	From Reference_Recognition_CAANZ_Repository__c ];

        for (Reference_Recognition_CAANZ_Repository__c rep : repositoryAfter) {
			System.debug(rep);
        }
        System.assert(repositoryAfter.size()==5);
    }
    

	private static testMethod void runBatchUpdate_2(){
		System.debug( 'Starting Batch');
        
        Test.startTest();           
        List<Account> businessAccounts = [select Id, Name , BillingCountry, BillingStreet, BillingCity, BillingPostalCode, Billing_DPID__c,
										  ShippingCountry, ShippingStreet, ShippingCity, ShippingPostalCode, Shipping_DPID__c, Phone
										  from Account Where Name like 'B%'];

        List <Id> keysToProcess = new List<Id>();
        for (Account bus : businessAccounts)
            keysToProcess.add(bus.Id);
        
        RecognitionMatchKeyRefresh objClass = new RecognitionMatchKeyRefresh(keysToProcess);
		Database.executeBatch (objClass, 5);
        
		Test.stopTest();	
		List<Reference_Recognition_CAANZ_Repository__c> repositoryAfter = [	Select ID, Account__c, Company_Name__c, 
                                                                          	BillingStreet__c, BillingCity__c, BillingPostalCode__c, BillingDPID__c,  
																			ShippingStreet__c, ShippingCity__c, ShippingPostalCode__c, ShippingDPID__c, Phone__c,
                                                                           	BusNameFuzzy__c, BusSearch_Key__c, MATCH_KEY__c, Match_Score__c, AddressSearch_Key__c
                                                                          	From Reference_Recognition_CAANZ_Repository__c ];

        for (Reference_Recognition_CAANZ_Repository__c rep : repositoryAfter) {
			System.debug(rep);
            if (rep.Company_Name__c=='Bob Accounting')
            	System.assertEquals('bobaccount', rep.BusNameFuzzy__c);
            if (rep.Company_Name__c=='BDO')
            	System.assertEquals('bdo', rep.BusNameFuzzy__c);
        }
        System.assert(repositoryAfter.size()==2);
    }

}