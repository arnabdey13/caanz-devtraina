@IsTest(seeAllData=false)
private class ff_WrappedAttachmentSourceTests {

    @IsTest
    static void testLoad() {
        Id accountId = [select Id from Account LIMIT 1].Id;
        ff_WrappedAttachmentSource src = new ff_WrappedAttachmentSource(accountId);
        List<ff_Service.AttachmentWrapper> wrappers = (List<ff_Service.AttachmentWrapper>) src.getWrappers();
        System.debug(wrappers);
        System.assertEquals(3, wrappers.size(),
                'de-duped attachments should return CV list, one Resume list + the empty parent wrapper');
        Set<Integer> counts = new Set<Integer>();
        for (ff_Service.AttachmentWrapper w : wrappers) {
            counts.add(w.attachments.size());
        }
        System.assertEquals(new Set<Integer>{
                0, 1, 2
        }, counts, 'cv count = 2, resume count = 1, parent wrapper = 0');
    }

    @TestSetup
    static void seed() {
        Account a = new ff_TestDataAccountBuilder().name('Acme')
                .billingAddress('1 Market St', 'San Francisco', 'California', '94105', 'United States')
                .create(1, true)
                .get(0);

        // these two should de-dupe based on matching name
        attach(a.Id, 'CV:test-page1.txt');
        attach(a.Id, 'CV:test-page1.txt');

        // this has same prefix so will be in result
        attach(a.Id, 'CV:test-page2.txt');

        // single different prefix so will be in result
        attach(a.Id, 'Resume:test.txt');
        attach(a.Id, 'Resume:test.txt');

    }

    private static void attach(Id parent, String fileName) {
        Attachment a1 = new Attachment(ParentId = parent,
                Name = fileName,
                Body = Blob.valueOf('dGhpcyBpcyBhIHRlc3QgZmlsZSBmb3IgdXBsb2Fk'),
                ContentType = 'text/plain');
        insert a1;
    }
}