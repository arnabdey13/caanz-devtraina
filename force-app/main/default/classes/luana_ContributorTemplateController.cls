/*
    Developer: WDCi (KH)
    Date: 28/06/2016
    Task #: Contributor template controller
    
*/
public class luana_ContributorTemplateController {
    
    public String userPhotoUrl {get; set;}
    private Id communityId;
    
    public luana_ContributorTemplateController(){

        for(ConnectApi.community comm: ConnectApi.Communities.getCommunities().communities){
            if(comm.urlPathPrefix == 'member'){
                communityId = comm.Id;
            }
        }
        
        ConnectApi.Photo userProfPhoto = ConnectApi.UserProfiles.getPhoto(communityId, UserInfo.getUserId());
        
        userPhotoUrl = userProfPhoto.largePhotoUrl;
    }
    
}