public class QPRSurveyRelatedContactTriggerHandler extends TriggerHandler {
     //Context Variable Collections after Type Casting
    List<QPR_Survey_Related_Contact__c> newList = (List<QPR_Survey_Related_Contact__c>)Trigger.new;
    List<QPR_Survey_Related_Contact__c> oldList = (List<QPR_Survey_Related_Contact__c>)Trigger.old;
    Map<Id,QPR_Survey_Related_Contact__c> oldMap = (Map<Id,QPR_Survey_Related_Contact__c>)Trigger.oldMap;
    Map<Id,QPR_Survey_Related_Contact__c> newMap = (Map<Id,QPR_Survey_Related_Contact__c>)Trigger.newMap;        
   
    public static Boolean isTrigExec =true;
    //After Insert Trigger 
    public override void afterInsert() {
        
    }
    public override void afterUpdate(){
                           updateQPRSurveyRelatedContact(newList);
                              }
    public void updateQPRSurveyRelatedContact(List<QPR_Survey_Related_Contact__c> newQPRSurveyRelatedAddressList)
    {
        List<QPR_Survey_Related_Contact__c> delRelatedContacts = new List<QPR_Survey_Related_Contact__c>();
         List<ID> delRelatedContactID = new List<ID>();
        if (isTrigExec ==true)
        {
             for(QPR_Survey_Related_Contact__c qprRelatedContacts : newQPRSurveyRelatedAddressList){
                  if(qprRelatedContacts.RemoveFlag__c!=null)
                 {
                 if(qprRelatedContacts.RemoveFlag__c==true)
                 { delRelatedContactID.add(qprRelatedContacts.Id); }
                 }
             }
            if(!delRelatedContactID.isEmpty())
            {
                delRelatedContacts= [SELECT Id, Name FROM QPR_Survey_Related_Contact__c where Id in :delRelatedContactID];
                 delete delRelatedContacts;
            }
            isTrigExec =false;
        }
    }
     public override void beforeInsert(){}
     public override void beforeUpdate(){}
     public override void afterDelete(){}
     public override void afterUndelete(){}
    
}