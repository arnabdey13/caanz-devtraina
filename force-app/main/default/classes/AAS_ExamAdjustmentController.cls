/* 
    Developer: WDCi (KH)
    Development Date: 03/04/2017
    Task #: AAS Exam mark adjustment for exam 
*/
public with sharing class AAS_ExamAdjustmentController {
    
    AAS_ModuleExamAdjustmentController shareCtl = new AAS_ModuleExamAdjustmentController();
    
    public AAS_Course_Assessment__c courseAssessment {get; set;}
    public static String selectedRecordType{get;set;}
    
    public Transient List<FinalAdjustmentWrapper> finalAdjustmentWrapperList {get; set;}
    private List<FinalAdjustmentWrapper> commitRecords {get; set;}
    
    public String urlSelectedType {get; set;}
    public Boolean validToProcess {get; set;}
    public Boolean readyToCommit {get; set;}
    public Boolean hasRecords {get; set;}
    
    //public String finalAdjReportId {get; set;}
    public String exceedFullMarkReportId {get; set;}
    public Boolean hasExceedFullMark {get; set;}
    public String sasRecordTypeId;
    
    public String totalMarkLabel {get; set;}
    //public Map<Id, Double> totalMarkValueMap {get; set;}
    
    public AAS_ExamAdjustmentController(ApexPages.StandardController controller) {
        
        if(!test.isRunningTest()){
            List<String> addFieldName = new List<String>{'AAS_Course__c', 'AAS_Module_Passing_Mark__c', 'AAS_Course__r.Name', 'AAS_Exam_Adjustment__c', 'AAS_Cohort_Adjustment_Exam__c', 'AAS_Final_Adjustment_Exam__c', 'AAS_Final_Adjustment_Supp_Exam__c', 
                                                         'AAS_Borderline_Remark__c', 'AAS_Borderline_Remark_Supp_Exam__c', 'AAS_Cohort_Adjustment_Supp_Exam__c', 'RecordTypeId'};
            controller.addFields(addFieldName);
        }
        courseAssessment = (AAS_Course_Assessment__c) controller.getRecord();

        urlSelectedType = apexpages.currentpage().getparameters().get('type');
        selectedRecordType = urlSelectedType + '_SAS';
        
        doValidation();
        //validToProcess = shareCtl.validationAction(courseAssessment, 'Exam_Assessment_SAS');
        
        readyToCommit= false;
        commitRecords = new List<FinalAdjustmentWrapper>();
        hasExceedFullMark = false;
        hasRecords = true;
        
        //Set the default value to display in the UI for cohort and passing value
        List<AAS_Settings__c> aasSets = AAS_Settings__c.getall().values();
        for(AAS_Settings__c assSetting: aasSets){
            
            if(assSetting.Name == 'Exceed Full Mark Report Id'){
                exceedFullMarkReportId = assSetting.Value__c +'?pv0='+String.ValueOf(courseAssessment.Id).subString(0,15)+'&pv1=';
            }
        }
        
        if(selectedRecordType == 'Exam_Assessment_SAS'){
            totalMarkLabel = 'Total Module Mark (Non Exam + Exam)';
        }
        //totalMarkValueMap = new Map<Id, Double>();
    }

    
    public void doQuery(){
        
        readyToCommit = false;
        commitRecords = new List<FinalAdjustmentWrapper>();
        
        System.debug('**********::::1: ' + selectedRecordType + ' - '+ courseAssessment.Id);
        
        Transient List<AAS_Student_Assessment_Section__c> sasList = [Select Id, Name, AAS_Exceed_Full_Mark_Count__c, AAS_Student_Assessment__c, AAS_Total_Final_Mark__c, 
                AAS_Assessment_Schema_Section__r.AAS_Passing_Mark__c ,
                AAS_Student_Assessment__r.AAS_Total_Module_Mark_Non_Exam_Exam__c, AAS_Student_Assessment__r.AAS_Total_Module_Mark_Non_Exam_Supp_Exam__c,
                AAS_Student_Assessment__r.AAS_Student_Program__c, AAS_Student_Assessment__r.AAS_Student_Program__r.LuanaSMS__Contact_Student__r.Member_ID__c,
                AAS_Student_Assessment__r.AAS_Student_Program__r.LuanaSMS__Contact_Student__r.Name, AAS_Assessment_Schema_Section__r.RecordType.developerName,
                AAS_Student_Assessment__r.AAS_Student_Program__r.Name, AAS_Student_Assessment__r.AAS_Course_Assessment__r.AAS_Module_Passing_Mark__c, RecordType.DeveloperName
                from AAS_Student_Assessment_Section__c 
                Where RecordType.DeveloperName =: selectedRecordType and AAS_Student_Assessment__r.AAS_Course_Assessment__c =: courseAssessment.Id];    

        //and AAS_Student_Assessment__r.AAS_Final_Adjustment__c = null
        //AAS_Assessment_Schema_Section__c
        //AAS_Total_Final_Mark__c
        System.debug('********sasList:: ' + sasList);
        
        //The checking of 0.5,1.5,-0.5 is done in the wrapper class, in here we just need to set the value to the wrapper class
        Transient List<FinalAdjustmentWrapper> tempFinalAdjustmentWrapperList = new List<FinalAdjustmentWrapper>();
        for(AAS_Student_Assessment_Section__c sas: sasList){
            tempFinalAdjustmentWrapperList.add(new FinalAdjustmentWrapper(sas));
            
            if(sas.AAS_Exceed_Full_Mark_Count__c > 0){
                hasExceedFullMark = true;
            }
            sasRecordTypeId = sas.RecordTypeId;
            
            //if(sas.RecordType.DeveloperName  == 'Exam_Assessment_SAS'){
            //   totalMarkValueMap.put(sas.AAS_Student_Assessment__r.Id, sas.AAS_Student_Assessment__r.AAS_Total_Module_Mark_Non_Exam_Exam__c);
            //}
        }
        
        //System.debug('****totalMarkValueMap::: ' + totalMarkValueMap + ' - ' + tempFinalAdjustmentWrapperList);
        
        Integer limitNum = 200;
        Integer counterLimit = 1;
        for(FinalAdjustmentWrapper faw: tempFinalAdjustmentWrapperList){
            if(faw.finalAdjustMark != 0){
                if(counterLimit <= limitNum){
                    finalAdjustmentWrapperList.add(faw);//Display the record in visualforce page, only show 200 records
                }
                counterLimit++;
                
                readyToCommit = true;
                
                commitRecords.add(faw);//Set into this list again because the previous list the most will only contains 200 records
            }
        }
        
        System.debug('******:: ' + finalAdjustmentWrapperList.size());
        
        if(!finalAdjustmentWrapperList.isEmpty()){
            hasRecords = true;
            if(commitRecords.size() <= limitNum){
                addPageMessage(ApexPages.severity.INFO, 'Total ' + finalAdjustmentWrapperList.size() +' record/s found.');
            }else{
                addPageMessage(ApexPages.severity.INFO, 'Total ' + commitRecords.size() +' records found, table below will only show maximum ' + limitNum + ' records.');
            }
        }else{
            hasRecords = false;
            addPageMessage(ApexPages.severity.INFO, 'No record found. Please press \'Done\' to indicate that Exam Adjustment is completed and exit.');
        }
         
    }
    
    public PageReference doCommit(){
        Savepoint sp = Database.setSavepoint();
        try{
            if(!hasExceedFullMark){

                Map<Id, Boolean> sasAddOrDeductMap = new Map<Id, Boolean>();
                for(FinalAdjustmentWrapper faw: commitRecords){
                    if(faw.finalAdjustMark > 0){
                        sasAddOrDeductMap.put(faw.stuAssSec.Id, true);
                    }else{
                        sasAddOrDeductMap.put(faw.stuAssSec.Id, false);
                    }
                }
                System.debug('****sasIds:::: ' + sasAddOrDeductMap);
                
                //do a soql to get all related SASI records
                Map<Id, Integer> sasNumToDevidedMap = new Map<Id, Integer>();
                List<AAS_Student_Assessment_Section_Item__c> sasiList = [Select id, Name, AAS_Student_Assessment_Section__c, AAS_Eligible_for_Addition__c, AAS_Eligible_for_Deduction__c from AAS_Student_Assessment_Section_Item__c 
                                                                    Where AAS_Student_Assessment_Section__c in: sasAddOrDeductMap.keySet() and (AAS_Eligible_for_Addition__c = true or AAS_Eligible_for_Deduction__c = true)];

                //Set num of eligible Add/Deduct for each SAS and SASI for update
                List<AAS_Student_Assessment_Section_Item__c> validSASIList = new List<AAS_Student_Assessment_Section_Item__c>();
                for(AAS_Student_Assessment_Section_Item__c sasi: sasiList){
                    
                    if(sasAddOrDeductMap.containsKey(sasi.AAS_Student_Assessment_Section__c)){                                                    
                        if(sasAddOrDeductMap.get(sasi.AAS_Student_Assessment_Section__c)){ 
                            //check is eligible for addidtion
                            if(sasi.AAS_Eligible_for_Addition__c){
                                if(sasNumToDevidedMap.containsKey(sasi.AAS_Student_Assessment_Section__c)){
                                    integer tempCurrentAmount = sasNumToDevidedMap.get(sasi.AAS_Student_Assessment_Section__c);
                                    sasNumToDevidedMap.put(sasi.AAS_Student_Assessment_Section__c, tempCurrentAmount + 1);
                                }else{
                                    sasNumToDevidedMap.put(sasi.AAS_Student_Assessment_Section__c, 1);
                                }
                                validSASIList.add(sasi);
                            }
                        }else{
                            //check is eligible for deduction
                            if(sasi.AAS_Eligible_for_Deduction__c){
                                if(sasNumToDevidedMap.containsKey(sasi.AAS_Student_Assessment_Section__c)){
                                    integer tempCurrentAmount = sasNumToDevidedMap.get(sasi.AAS_Student_Assessment_Section__c);
                                    sasNumToDevidedMap.put(sasi.AAS_Student_Assessment_Section__c, tempCurrentAmount + 1);
                                }else{
                                    sasNumToDevidedMap.put(sasi.AAS_Student_Assessment_Section__c, 1);
                                }
                                validSASIList.add(sasi);
                            }
                        }
                    }
                }
                
                //Do final calculation based on the amount of eligibility and the Adjusted amount
                Map<Id, Decimal> calculatedAdjustmentMap = new Map<Id, Decimal>();
                for(FinalAdjustmentWrapper faw: commitRecords){
                    if(sasNumToDevidedMap.containsKey(faw.stuAssSec.Id)){
                        Decimal devidedAdjMark = faw.finalAdjustMark / sasNumToDevidedMap.get(faw.stuAssSec.Id);
                        calculatedAdjustmentMap.put(faw.stuAssSec.Id, devidedAdjMark);
                    }
                }
                
                //Do SASI update
                List<AAS_Student_Assessment_Section_Item__c> sasiUpdateList = new List<AAS_Student_Assessment_Section_Item__c>();
                for(AAS_Student_Assessment_Section_Item__c sasi: validSASIList){
                    if(calculatedAdjustmentMap.containsKey(sasi.AAS_Student_Assessment_Section__c)){
                        sasi.AAS_Exam_Adjustment__c = calculatedAdjustmentMap.get(sasi.AAS_Student_Assessment_Section__c);
                        sasiUpdateList.add(sasi);
                    }
                }
                
                update sasiUpdateList;
                
                addPageMessage(ApexPages.severity.Info, 'Committed success.');
                readyToCommit= false;
                
                doUpdateCA();
                /*AAS_Course_Assessment__c ca = new AAS_Course_Assessment__c();
                ca.Id = courseAssessment.Id;
                if(selectedRecordType.equals('Exam_Assessment_SAS')){
                    ca.AAS_Exam_Adjustment__c = true;
                }
                update ca;*/
                
                PageReference returnPage = new PageReference('/'+courseAssessment.Id);
                return returnPage;
            }else{
                addPageMessage(ApexPages.severity.ERROR, 'Error found! One of the record has exceeded full mark, please refer to <a href="/'+exceedFullMarkReportId+sasRecordTypeId.subString(0, 15)+'" target="_blank">here</a> to fix the record manually.');
            }
        }Catch(Exception exp){
            Database.rollback(sp);
            addPageMessage(ApexPages.severity.ERROR, 'Committed fail. Please try again later or contact your system administrator. Error: ' + exp.getMessage() + ' - ' + exp.getStackTraceString());
        }
        return null;
    }
    
    public void doValidation(){
        validToProcess = shareCtl.validationAction(courseAssessment, 'Exam_Assessment_SAS');
    }
    
    // returns a list of wrapper objects for the sObjects in the current page set
    public void getResults() {
        finalAdjustmentWrapperList = new List<FinalAdjustmentWrapper>();
        doQuery();
    }
    
    public PageReference doCancel(){
        return shareCtl.cancelAction(courseAssessment);
    }
    
    public PageReference doComplete(){
        doUpdateCA();
        
        PageReference returnPage = new PageReference('/'+courseAssessment.Id);
        return returnPage;
    }
    
    public List<SelectOption> getRecordTypeOptions() {
        
        return shareCtl.getRecordTypeOptions(courseAssessment, 'Exam_Assessment');
    }
    
    public void addPageMessage(ApexPages.severity sev, String msg){
        ApexPages.Message warningmsg = new ApexPages.Message(sev, msg);
        ApexPages.addmessage(warningmsg);
    }   
    
    public void doUpdateCA(){
        AAS_Course_Assessment__c ca = new AAS_Course_Assessment__c();
        ca.Id = courseAssessment.Id;
        if(selectedRecordType.equals('Exam_Assessment_SAS')){
            ca.AAS_Exam_Adjustment__c = true;
        }
        update ca;
    }
    
    public class FinalAdjustmentWrapper{
        
        public AAS_Student_Assessment_Section__c stuAssSec {get; set;}
        public Double finalAdjustMark {get; set;}
        
        public FinalAdjustmentWrapper(AAS_Student_Assessment_Section__c sas){
            stuAssSec = sas;
          
            if(sas.AAS_Assessment_Schema_Section__r.AAS_Passing_Mark__c != null && sas.AAS_Assessment_Schema_Section__r.RecordType.developerName +'_SAS' == 'Exam_Assessment_SAS'){
                if(sas.AAS_Assessment_Schema_Section__r.AAS_Passing_Mark__c - sas.AAS_Total_Final_Mark__c == 0.5){ 
                    finalAdjustMark = 0.5;
                }else if(sas.AAS_Assessment_Schema_Section__r.AAS_Passing_Mark__c - sas.AAS_Total_Final_Mark__c == 1.5){
                    finalAdjustMark = -0.5;
                }else if(sas.AAS_Assessment_Schema_Section__r.AAS_Passing_Mark__c - sas.AAS_Total_Final_Mark__c == 1){
                    finalAdjustMark = -1;
                }else{
                    finalAdjustMark = 0;
                }
                 
            }
            
        }
    }
}