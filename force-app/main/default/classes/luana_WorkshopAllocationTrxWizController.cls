/*
    Developer: WDCi (Lean)
    Date: 06/Jul/2016
    Task #: LCA-710 Luana implementation - Wizard for captsone workshop allocation
*/

public with sharing class luana_WorkshopAllocationTrxWizController {
    
    public boolean enabled {private set; public get;}
    public List<LuanaSMS__Attendance2__c> selectedAttendances {set; get;}
    
    public boolean saveIgnoreError {private set; public get;}
    public boolean hasMaxSeatError {private set; public get;}
    
    private ApexPages.StandardSetController standardController;
    
    public luana_WorkshopAllocationTrxWizController(ApexPages.StandardSetController standardController){
        this.standardController = standardController;
        
        List<LuanaSMS__Attendance2__c> attIds = (List<LuanaSMS__Attendance2__c>) standardController.getSelected();
        selectedAttendances = [select Id, Name, LuanaSMS__Attendance_Status__c, LuanaSMS__Session__r.Topic_Key__c, LuanaSMS__Session__c from LuanaSMS__Attendance2__c where id in: attIds];
        enabled = true;
        saveIgnoreError = false;
        hasMaxSeatError = false;
    }
    
    public PageReference initiateSave(){
        List<LuanaSMS__Attendance2__c> attendancesToUpdate = new List<LuanaSMS__Attendance2__c>();
        Map<String, String> attNames = new Map<String, String>();
        Map<String, Decimal> sessionAttendanceCount = new Map<String, Decimal>();
        
        hasMaxSeatError = false;
        
        for(LuanaSMS__Attendance2__c attendance : selectedAttendances){
            if(attendance.LuanaSMS__Attendance_Status__c == null){
                attendancesToUpdate.add(new LuanaSMS__Attendance2__c(Id = attendance.Id, LuanaSMS__Session__c = attendance.LuanaSMS__Session__c, LuanaSMS__Start_Time__c = null, LuanaSMS__End_Time__c = null));
                attNames.put(attendance.Id, attendance.Name);
            }
        }
        
        if(!saveIgnoreError){
            
            for(LuanaSMS__Attendance2__c attendance : attendancesToUpdate){
                
                if(sessionAttendanceCount.containsKey(attendance.LuanaSMS__Session__c)){
                    sessionAttendanceCount.put(attendance.LuanaSMS__Session__c, sessionAttendanceCount.get(attendance.LuanaSMS__Session__c) + 1);
                } else {
                    sessionAttendanceCount.put(attendance.LuanaSMS__Session__c, 1);
                }
            }
            
            for(LuanaSMS__Session__c session : [select Id, Name, Maximum_Capacity__c, LuanaSMS__Number_of_enrolled_students__c from LuanaSMS__Session__c where id in: sessionAttendanceCount.keySet()]){
                if(sessionAttendanceCount.containsKey(session.Id)){
                    if((sessionAttendanceCount.get(session.Id) + session.LuanaSMS__Number_of_enrolled_students__c) > session.Maximum_Capacity__c){
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'The number of students to be allocated to the session ' + session.Name + ' has exceeded the maximum capacity.'));
                        hasMaxSeatError = true;
                    }
                }
            }
            
            if(hasMaxSeatError){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'Please click \'Save & ignore\' if you still would like to proceed.'));
                
                return null;
            }
        }
        
        if(attendancesToUpdate.size() > 0 && !hasMaxSeatError){
            
            boolean hasError = false;
            
            integer counter = 0;
            for(Database.SaveResult sr : Database.update(attendancesToUpdate, false)){
                if(!sr.isSuccess()){
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error updating attendance ' + (attNames.containsKey(attendancesToUpdate.get(counter).Id) ? attNames.get(attendancesToUpdate.get(counter).Id) : attendancesToUpdate.get(counter).Id) + '. Error: ' + sr.getErrors()[0].getMessage()));
                    hasError = true;
                }
                
                counter ++;
            }
            
            if(hasError){
                return null;
            }
        }
        
        return standardController.cancel();
    }
    
    public PageReference doSaveAndIgnore(){
        
        saveIgnoreError = true;
        return initiateSave();
    }
    
    public PageReference doSave(){
        
        saveIgnoreError = false;
        return initiateSave();
    }
    
}