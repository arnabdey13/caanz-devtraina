/**
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Author:         Sudheendra GS
Organisation:   TechMahindra
Role:           Developer
Title:          SoapLoginClass_Test.cls
Test Class:     N/A
Version:        0.0
Since:          25/04/2019
Description:    This class contains code related to Unit Testing and test coverage of class SoapLoginClass 

History
<Date>          <Authors Name>          <Brief Description of Change>
25/04/2019      Sudheendra GS           Class Created.                                       
                                        This class contains code related to Unit Testing and test coverage of SoapLoginClass.
26/06/2019      Rakesh Murugan          Modified.
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
**/

@isTest
private class SoapLoginClass_Test {

	private static String SERVER_URL = URL.getSalesforceBaseUrl().toExternalForm();
	private static String SOAP_PATH = '/services/Soap/u/26.0/';
	private static String ORG_ID = UserInfo.getOrganizationId();
	private static String SESSION_VALUE = 'SESSION_ID_REMOVED';

	@isTest 
	static void testCallout() {
		Test.startTest();
			Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorSoap()); 
			String[] actualValue = SoapLoginClass.login();
			String[] expectedValue = new String[]{SERVER_URL + SOAP_PATH + ORG_ID.substring(0, ORG_ID.length() - 3), SESSION_VALUE};
        Test.stopTest();
        
        System.debug('expectedValue: ' + expectedValue);
        System.debug('actualValue: ' + actualValue);
        System.assertEquals(expectedValue, actualValue);

    }
}