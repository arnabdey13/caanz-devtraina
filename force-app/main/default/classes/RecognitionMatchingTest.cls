@isTest(seeAllData=false)
public class RecognitionMatchingTest {
   ////////////////////// Main Practice Lookup Prep ///////////////////////////////////////////////////////////////
    @testSetup 
    static void setup() {
        
        Reference_Main_Practices__c  deloitte = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Deloitte'
            , Search_Name__c     = 'Deloitte'
            , Full_Name__c       = 'Deloitte'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            = 'Deloitte'
        );
      
		Reference_Main_Practices__c  pwc = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'PricewaterhouseCoopers;PWC; PwC; PriceWaterHouseCoopers; PriceWaterhouseCoopers; PricewaterhousCoopers; Pricewaterhousecoopers'
            , Search_Name__c     = 'PricewaterhouseCoopers'
            , Full_Name__c       = 'PricewaterhouseCoopers'
            , Practice_Size__c   = 'Large Firm'
			, Big4__c            = 'PWC'
        );
        
		Reference_Main_Practices__c  kpmg = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'KPMG'
            , Search_Name__c     = 'KPMG'
            , Full_Name__c       = 'KPMG'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            = 'KPMG'
        );
        
		Reference_Main_Practices__c  ey = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Ernst and Young; EY; Ernst & Young'
            , Search_Name__c     = 'Ernst and Young'
            , Full_Name__c       = 'Ernst and Young'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            = 'EY'
        );
        
		Reference_Main_Practices__c  bdo = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'BDO'
            , Search_Name__c     = 'BDO'
            , Full_Name__c       = 'BDO'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            =  NULL
        );
        
		Reference_Main_Practices__c  gt = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Grant Thornton'
            , Search_Name__c     = 'Grant Thornton'
            , Full_Name__c       = 'Grant Thornton'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            =  NULL
        );        
        
        
		Reference_Main_Practices__c  vincents = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Vincents Chartered Accountants; Vincents'
            , Search_Name__c     = 'Vincents Chartered Accountants'
            , Full_Name__c       = 'Vincents Chartered Accountants'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            =  NULL
        );
        
        Reference_Main_Practices__c  boroughs = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Boroughs'
            , Search_Name__c     = 'Boroughs'
            , Full_Name__c       = 'Boroughs'
            , Practice_Size__c   = 'Medium Firm'
            , Big4__c            =  NULL
        );
        
        insert new List<Reference_Main_Practices__c>{pwc, deloitte, kpmg, ey, bdo, gt, vincents, boroughs};  
            
		// insert some businesses  
		Account big4BusinessAccountNZ = SegmentationDataGenTest.genPracticeNewZealand( 'Ernst & Young', 'Chartered Accounting', 'Auckland', 25);
		insert big4BusinessAccountNZ;
		Account smallBusinessAccount = SegmentationDataGenTest.genPracticeNewZealand( 'Bob Accounting', 'Chartered Accounting', 'Wellington', 1);
		insert smallBusinessAccount;
        Account midTierBusinessAccount = SegmentationDataGenTest.genPracticeNewZealand( 'BDO', 'Chartered Accounting', 'Hamilton', 15);
        insert midTierBusinessAccount;  
        Account big4BusinessAccountAU = SegmentationDataGenTest.genPracticeAU( 'Deloitte', 'Chartered Accounting', 'Sydney', 25);
		insert big4BusinessAccountAU;
        Account smallBusinessAccountAU = SegmentationDataGenTest.genPracticeAU( 'Young and Co', 'Chartered Accounting', 'Brisbane', 2);
		insert smallBusinessAccountAU;
    }

	// Test 
	private static testMethod void singleEntryBig4_Case1(){
		System.debug( 'Matching Case 1');
        
        List <Reference_Recognition_CAANZ_Repository__c> busToUpdate = new List <Reference_Recognition_CAANZ_Repository__c>();             
 		Account businessAccount = [select id, Name , BillingCountry, BillingStreet, BillingCity, BillingPostalCode, Billing_DPID__c,
										  ShippingCountry, ShippingStreet, ShippingCity, ShippingPostalCode, Shipping_DPID__c, Phone
                                   from Account where Name = 'Ernst & Young'];

        Reference_Recognition_CAANZ_Repository__c result = RecognitionMatching.getMatchKeys(businessAccount);
        busToUpdate.add(result);  
        if (busToUpdate != null && busToUpdate.size()>0) {
            Database.upsert(busToUpdate); 
            System.debug('List Size '+ busToUpdate.size());
        }
   
        List<Reference_Recognition_CAANZ_Repository__c> repositoryAfter = [	Select ID, Account__c, Company_Name__c, 
                                                                          	BillingStreet__c, BillingCity__c, BillingPostalCode__c, BillingDPID__c,  
																			ShippingStreet__c, ShippingCity__c, ShippingPostalCode__c, ShippingDPID__c, Phone__c,
                                                                           	BusNameFuzzy__c, BusSearch_Key__c, MATCH_KEY__c, Match_Score__c, AddressSearch_Key__c
                                                                          	From Reference_Recognition_CAANZ_Repository__c ];

        for (Reference_Recognition_CAANZ_Repository__c rep : repositoryAfter) {
			System.debug(rep);

            System.assertEquals('ernstyoung', rep.BusNameFuzzy__c);
            System.assertEquals('E652', rep.BusSearch_Key__c);
            System.assertEquals(100001, rep.MATCH_KEY__c);
            System.assertEquals('Main', rep.AddressSearch_Key__c);
            System.assertEquals(0, rep.Match_Score__c);
        }
    }
  
	private static testMethod void matchSingleEntryBig4_Case2(){
		System.debug( 'Matching Case 2');
        
        List <Reference_Recognition_CAANZ_Repository__c> busToUpdate = new List <Reference_Recognition_CAANZ_Repository__c>();             
 		Account businessAccount = [select id, Name , BillingCountry, BillingStreet, BillingCity, BillingPostalCode, Billing_DPID__c,
										  ShippingCountry, ShippingStreet, ShippingCity, ShippingPostalCode, Shipping_DPID__c, Phone
                                   from Account where Name = 'Ernst & Young'];

        Reference_Recognition_CAANZ_Repository__c result = RecognitionMatching.getMatchKeys(businessAccount);
        busToUpdate.add(result);  
        if (busToUpdate != null && busToUpdate.size()>0) {
            Database.upsert(busToUpdate); 
            System.debug('List Size '+ busToUpdate.size());
        }
   
		Account big4AccountNZ = SegmentationDataGenTest.genPracticeNewZealand( 'Ernst & Young', 'Chartered Accounting', 'Auckland', 15);
        Reference_Recognition_CAANZ_Repository__c match = RecognitionMatching.getMatchKeys(big4AccountNZ);

		System.debug(match);

        System.assertEquals('ernstyoung', match.BusNameFuzzy__c);
        System.assertEquals('E652', match.BusSearch_Key__c);
        System.assertEquals(100001, match.MATCH_KEY__c);
        System.assertEquals('Main', match.AddressSearch_Key__c);
        System.assertEquals(100, match.Match_Score__c);
       
    }
    
	private static testMethod void matchSingleEntryBig4_Case3(){
		System.debug( 'Matching Case 3');
        
        List <Reference_Recognition_CAANZ_Repository__c> busToUpdate = new List <Reference_Recognition_CAANZ_Repository__c>();             
 		Account businessAccount = [select id, Name , BillingCountry, BillingStreet, BillingCity, BillingPostalCode, Billing_DPID__c,
										  ShippingCountry, ShippingStreet, ShippingCity, ShippingPostalCode, Shipping_DPID__c, Phone
                                   from Account where Name = 'Ernst & Young'];

        Reference_Recognition_CAANZ_Repository__c result = RecognitionMatching.getMatchKeys(businessAccount);
        busToUpdate.add(result);  
        if (busToUpdate != null && busToUpdate.size()>0) {
            Database.upsert(busToUpdate); 
            System.debug('List Size '+ busToUpdate.size());
        }
   
		Account big4AccountNZ = SegmentationDataGenTest.genPracticeNewZealand( 'Ernst & Young', 'Chartered Accounting', 'Auckland', 15);
        Reference_Recognition_CAANZ_Repository__c match = RecognitionMatching.getMatchKeys(big4AccountNZ);

		System.debug(match);
        System.assertEquals(100001, match.MATCH_KEY__c);

        System.debug('TEST: matchSingleEntryBig4_Case3: Matching Key:' + String.valueOf(match.MATCH_KEY__c));
		List<Reference_Recognition_CAANZ_Repository__c> matchedRecords = RecognitionMatching.getRecordByMatchKey( 'MATCH_KEY__c', match.MATCH_KEY__c);
        System.assert(matchedRecords.size() > 0);
        for (Reference_Recognition_CAANZ_Repository__c recMatch : matchedRecords) {
			System.debug(' Record by Match Key:' + recMatch);
			System.assertEquals('ernstyoung', recMatch.BusNameFuzzy__c);
        }        
    }

    
	// Company_Name__c=Ernst & Young, BillingStreet__c=123 Main Street, BillingCity__c=AUCKLAND, BillingPostalCode__c=1234, BillingDPID__c=12345678
	// BusNameFuzzy__c=ernstyoung, BusSearch_Key__c=E652, MATCH_KEY__c=100001, Match_Score__c=0, AddressSearch_Key__c=Main}, 
	
	// Company_Name__c=Bob Accounting, BillingStreet__c=123 Main Street, BillingCity__c=WELLINGTON, BillingPostalCode__c=1234, BillingDPID__c=12345678, 
	// BusNameFuzzy__c=bobaccount, BusSearch_Key__c=B100, MATCH_KEY__c=100002, Match_Score__c=0, AddressSearch_Key__c=Main}, 
	
	// Company_Name__c=BDO, BillingStreet__c=123 Main Street, BillingCity__c=HAMILTON, BillingPostalCode__c=1234, BillingDPID__c=12345678, 
	// BusNameFuzzy__c=bdo, BusSearch_Key__c=B300, MATCH_KEY__c=100003, Match_Score__c=0, AddressSearch_Key__c=Main
    
	private static testMethod void processMultipleEntries_Case4(){
		System.debug( 'Matching Case 4 - Multiple Entries');
		List<sObject> sobjList;
            
        String  queryBase = ' Select ID, Account__c, Company_Name__c, BillingStreet__c, BillingCity__c, BillingPostalCode__c, BillingDPID__c, ' ;        
		queryBase += ' ShippingStreet__c, ShippingCity__c, ShippingPostalCode__c, ShippingDPID__c, Phone__c, BusNameFuzzy__c, BusSearch_Key__c, ';
		queryBase += ' MATCH_KEY__c, Match_Score__c, AddressSearch_Key__c From Reference_Recognition_CAANZ_Repository__c ';

        RecognitionMatching.addToRepository( ' LIMIT 3');
		sobjList = Database.query(queryBase);  
        System.assert(sobjList.size()==3);
		System.debug(sobjList);
        
        RecognitionMatching.rmFromRepository( ' WHERE Company_Name__c = \'Bob Accounting\' ' );
		sobjList = Database.query(queryBase);  
        System.assert(sobjList.size()==2);
		System.debug(sobjList);
        
		RecognitionMatching.clearRepository();
		sobjList = Database.query(queryBase);  
        System.assert(sobjList.size()==0);
    }
    
    
    
	private static testMethod void matchExistingEntry_Case5(){
		System.debug( 'Matching Case 5 - Duplicate Entries');
		List<sObject> sobjList;
        
		String  queryBase = ' Select ID, Account__c, Company_Name__c, BillingStreet__c, BillingCity__c, BillingPostalCode__c, BillingDPID__c, ' ;        
		queryBase += ' ShippingStreet__c, ShippingCity__c, ShippingPostalCode__c, ShippingDPID__c, Phone__c, BusNameFuzzy__c, BusSearch_Key__c, ';
		queryBase += ' MATCH_KEY__c, Match_Score__c, AddressSearch_Key__c From Reference_Recognition_CAANZ_Repository__c ';

        RecognitionMatching.addToRepository( 'where Name = \'Ernst & Young\' LIMIT 1 ');
		
        sobjList = Database.query(queryBase);  
        System.assert(sobjList.size()==1);
		System.debug('matchExistingEntry_Case5: First Test:' + sobjList);

		RecognitionMatching.addToRepository( 'where Name = \'Ernst & Young\' LIMIT 1 ');
		
        sobjList = Database.query(queryBase);  
        System.assert(sobjList.size()==1);
		System.debug('matchExistingEntry_Case5: Second Test:' + sobjList);
	}
    
}