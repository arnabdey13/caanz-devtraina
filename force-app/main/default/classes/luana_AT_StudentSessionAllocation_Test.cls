/*
    Developer: WDCi (Carter)
    Date: 06/June/2016
    Task #: Test class for luana_StudentSessionAllocationController
    
    Change History
    01-July-2016 WDCi - KH: Update the SP with Status = 'In Progress' and Paid = true in line(160-161) & line(198-199)
    14-July-2016 WDCi - KH: Increase test class coverage and include resource booking checking
    08-Aug-2016 WDCi - KH: Add person account email
    
*/

@isTest(seeAllData=false)
private class luana_AT_StudentSessionAllocation_Test {

    final static String contextShortName = 'ssact';
    final static String contextLongName = 'luana_StudentSessionAllocationCtlTest';
    final static String deliveryTeamPermSetName = 'Luana_DeliveryTeamPermission';
    
    //TODO - Change to private where applicable
    public static Luana_DataPrep_Test dataPrep;
    public static PermissionSet deliveryTeamPermSet;
    public static Profile userProfile;
    public static User deliveryUser;
    public static List<LuanaSMS__Luana_Configuration__c> luanaSMSConfig;
    public static List<Luana_Extension_Settings__c> luanaExtensionConfig;
    public static LuanaSMS__Training_Organisation__c trainingOrg;
    public static LuanaSMS__Program__c program;
    public static LuanaSMS__Program_Offering__c poAM;
    public static LuanaSMS__Program_Offering__c poAP;
    public static List<LuanaSMS__Subject__c> subjList;
    public static List<LuanaSMS__Program_Offering_Subject__c> posList;
    public static List<LuanaSMS__Program_Offering_Subject__c> posAMList;
    public static LuanaSMS__Course__c courseAP;
    public static LuanaSMS__Course__c courseAM;
    public static LuanaSMS__Delivery_Location__c devLocation;
    public static Course_Delivery_Location__c courseDevLoc;
    public static Account venueAccount;
    public static LuanaSMS__Room__c room1;
    public static LuanaSMS__Session__c session1;
    public static LuanaSMS__Session__c session2;
    public static List<Account> memberAccounts;
    public static List<LuanaSMS__Student_Program__c> newStudentPrograms;
    public static List<LuanaSMS__Session__c> sessions;
    
    //Used to initialise all the variables
    static void setup(String runSeqKey){
         
        dataPrep = new Luana_DataPrep_Test();
        
        deliveryTeamPermSet = [select id from PermissionSet where Name =: deliveryTeamPermSetName];
        
        userProfile = dataPrep.getProfileByName('CAANZ Standard User');
        
        deliveryUser = dataPrep.newInternalUser(contextShortName, contextLongName, userProfile.Id);
        insert deliveryUser;
        
        //PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId = deliveryUser.Id, PermissionSetId = deliveryTeamPermSet.Id);
        //insert psa;
        
        luanaSMSConfig = dataPrep.createLuanaConfigurationCustomSetting();
        insert luanaSMSConfig;
        
        luanaExtensionConfig = dataPrep.prepLuanaExtensionSettingCustomSettings();
        luanaExtensionConfig.add(new Luana_Extension_Settings__c(Name = 'Exam Allocation Report Params', Value__c = '000000000000000'));
        insert luanaExtensionConfig;
        
        trainingOrg = dataPrep.createNewTraningOrg(contextShortName, contextShortName + 'my org', contextShortName + 'myorg', 'test street', 'test location', '5400');
        insert trainingOrg;
        
        program = dataPrep.createNewProgram(contextShortName, contextLongName, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        insert program;
        
        poAM = dataPrep.createNewProgOffering('PO_AM_' + contextShortName, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), program.Id, trainingOrg.Id, null, null, null, 1, 1);
        insert poAM;
        
        poAP = dataPrep.createNewProgOffering('PO_AP_' + contextShortName, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Program'), program.Id, trainingOrg.Id, null, null, null, 1, 1);
        insert poAP;
        
        //Create multiple subjects
        subjList = new List<LuanaSMS__Subject__c>();
        subjList.add(dataPrep.createNewSubject('TAX AU_' + contextShortName, 'TAX AU_' + contextShortName, 'TAX AU_' + contextShortName, 1, 'Module'));
        subjList.add(dataPrep.createNewSubject('TAX NZ_' + contextShortName, 'TAX NZ_' + contextShortName, 'TAX NZ_' + contextShortName, 1, 'Module'));
        subjList.add(dataPrep.createNewSubject('AAA_' + contextShortName, 'AAA_' + contextShortName, 'AAA_' + contextShortName, 1, 'Module'));
        subjList.add(dataPrep.createNewSubject('Cap_' + contextShortName, 'Capstone_' + contextShortName, 'Cap_' + contextShortName, 1, 'Module'));
        subjList.add(dataPrep.createNewSubject('FIN_' + contextShortName, 'FIN_' + contextShortName, 'FIN_' + contextShortName, 55, 'Module'));
        subjList.add(dataPrep.createNewSubject('MAAF_' + contextShortName, 'MAAF_' + contextShortName, 'MAAF_' + contextShortName, 60, 'Module'));
        insert subjList;
        
        //Create multiple Program Offering Subject
        posList = new List<LuanaSMS__Program_Offering_Subject__c>();
        for(LuanaSMS__Subject__c sub: subjList){
            if(sub.name =='TAX AU_' + contextShortName || sub.name =='TAX NZ_' + contextShortName){
                posList.add(dataPrep.createNewProgOffSubject(poAP.Id, sub.Id, 'Elective'));
            }else{
                posList.add(dataPrep.createNewProgOffSubject(poAP.Id, sub.Id, 'Core'));
            }
        }
        insert posList;
                
        posAMList = new List<LuanaSMS__Program_Offering_Subject__c>();
        for(LuanaSMS__Subject__c sub: subjList){
            if(sub.name =='TAX AU_' + contextShortName || sub.name =='TAX NZ_' + contextShortName){
                posAMList.add(dataPrep.createNewProgOffSubject(poAM.Id, sub.Id, 'Elective'));
            }else{
                posAMList.add(dataPrep.createNewProgOffSubject(poAM.Id, sub.Id, 'Core'));
            }
        }
        insert posAMList;
        
        //Create course
        courseAP = dataPrep.createNewCourse('Graduate Diploma of Chartered Accounting_' + contextShortName, poAP.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Program'), 'Running');
        courseAP.LuanaSMS__Allow_Online_Enrolment__c = true;
        
        courseAM = dataPrep.createNewCourse('AAA216', poAM.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        courseAM.LuanaSMS__Allow_Online_Enrolment__c = true;
        
        insert new List<LuanaSMS__Course__c>{courseAP, courseAM};
        
        devLocation = dataPrep.createNewDeliveryLocationRecord('Australia - South Australia - Adelaide_' + contextShortName, 'Australia - South Australia - Adelaide_' + contextShortName, trainingOrg.Id, '1000', 'Australia');
        insert devLocation;
        
        courseDevLoc = dataPrep.createCourseDeliveryLocation(courseAM.Id, devLocation.Id, 'Exam Location');
        insert courseDevLoc;
        
        venueAccount = dataPrep.generateNewBusinessAcc(contextShortName + 'Test Venue_'+runSeqKey, 'Venue', null, null, null, null, 'Sunshine Coast Office', 'Active');
        venueAccount.Ext_Id__c = contextShortName +'_'+runSeqKey;
        insert venueAccount;
        
        room1 = dataPrep.newRoom(contextShortName, contextLongName, venueAccount.Id, 20, 'Classroom', runSeqKey);
        insert room1;
        
        sessions = new List<LuanaSMS__Session__c>();
        sessions.add(dataPrep.newSession(contextShortName, contextLongName, 'Exam', 20, venueAccount.Id, room1.Id, courseDevLoc.Id, system.now().addHours(1), system.now().addHours(2))); //e.g, 1pm - 2pm
        sessions.add(dataPrep.newSession(contextShortName, contextLongName, 'Exam', 20, venueAccount.Id, room1.Id, courseDevLoc.Id, system.now().addHours(3), system.now().addHours(4))); //e.g, 3pm - 4pm        
    
        insert sessions;
        
        LuanaSMS__Resource__c res1 = dataPrep.createResource('Test Res1', contextShortName + 'res1');
        insert res1;
        
        List<LuanaSMS__Resource_Booking__c> resBookings = new List<LuanaSMS__Resource_Booking__c>();

        memberAccounts = new List<Account>();
        for(integer i = 0; i < 5; i ++){
            Account memberAccount = dataPrep.generateNewApplicantAcc('Joe_' + contextShortName + '_' + i, contextLongName, 'Full_Member');
            memberAccount.Member_Id__c = '12345' + i;
            memberAccount.Affiliated_Branch_Country__c = 'Australia';
            memberAccount.Membership_Class__c = 'Full';
            memberAccount.PersonEmail = 'joe_'+i+'_'+contextShortName+'@gmail.com';//LCA-921 add person account email
            memberAccount.Communication_Preference__c= 'Home Phone';
            memberAccount.PersonHomePhone= '1234';
            memberAccount.PersonOtherStreet= '83 Saggers Road';
            memberAccount.PersonOtherCity='JITARNING';
            memberAccount.PersonOtherState='Western Australia';
            memberAccount.PersonOtherCountry='Australia';
            memberAccount.PersonOtherPostalCode='6365'; 
            memberAccounts.add(memberAccount);
        }
        insert memberAccounts;
        
        for(Contact contact : [select Id from Contact where AccountId in: memberAccounts]){
            for(LuanaSMS__Session__c ses: sessions){
                resBookings.add(dataPrep.createResourceBooking(contact.Id, res1.Id, ses.Id));
            }
        }
        insert resBookings;       
    }
    
    /*
        Update attendance start time & end time by session start time & end time
    */
    static testMethod void testUpdateAttendaceTimes() {
        
        setup('1');
        newStudentPrograms = new List<LuanaSMS__Student_Program__c>();
        Id studProgAccModuleId = dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module');
        
        for(Contact contact : [select Id from Contact where AccountId in: memberAccounts]){

            LuanaSMS__Student_Program__c newStudProg = dataPrep.createNewStudentProgram(studProgAccModuleId, contact.Id, courseAM.Id, 'Australia', 'In Progress');
            newStudProg.LuanaSMS__Auto_Student_Program_Subject_Creation__c = true;
            newStudProg.Exam_Location__c = courseDevLoc.Id;
            newStudProg.Paid__c = true;
            
            newStudentPrograms.add(newStudProg);
        }
        insert newStudentPrograms;
        Test.startTest();
        //Main Test Area
        
        PageReference pageRef = Page.luana_StudentSessionAllocationWizard;
        pageRef.getParameters().put('id', CourseAM.id);
        pageRef.getParameters().put('cdlid', courseDevLoc.id);
        
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController lssac_sc = new ApexPages.StandardController(CourseAM);
        luana_StudentSessionAllocationController lssac_e = new luana_StudentSessionAllocationController(lssac_sc);
        lssac_e.doSave();
        Test.stopTest();
        
        List<LuanaSMS__Attendance2__c> attendees = [SELECT Id, LuanaSMS__Session__c, LuanaSMS__End_time__c, LuanaSMS__Start_time__c FROM LuanaSMS__Attendance2__c];
        System.assertEquals(attendees.size(), memberAccounts.size());
        
        List<LuanaSMS__Session__c> updateSes = new List<LuanaSMS__Session__c>();
        for(LuanaSMS__Session__c ses: sessions){
            
            ses.LuanaSMS__Start_time__c = system.now().addDays(1);
            ses.LuanaSMS__End_time__c = system.now().addDays(1);
            
            updateSes.add(ses);
        }
        
        update updateSes;
        
    
    }
    
    /*
        Success allocation test from CDL
    */
    static testMethod void testSuccessAllocationFromCDL() {
        
        setup('2');
        newStudentPrograms = new List<LuanaSMS__Student_Program__c>();
        Id studProgAccModuleId = dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module');
        
        for(Contact contact : [select Id from Contact where AccountId in: memberAccounts]){

            LuanaSMS__Student_Program__c newStudProg = dataPrep.createNewStudentProgram(studProgAccModuleId, contact.Id, courseAM.Id, 'Australia', 'In Progress');
            newStudProg.LuanaSMS__Auto_Student_Program_Subject_Creation__c = true;
            newStudProg.Exam_Location__c = courseDevLoc.Id;
            newStudProg.Paid__c = true;
            
            newStudentPrograms.add(newStudProg);
        }
        insert newStudentPrograms;
        
        Test.startTest();
            
            //Main Test Area
            PageReference pageRef = Page.luana_StudentSessionAllocationWizard;
            pageRef.getParameters().put('id', CourseAM.id);
            pageRef.getParameters().put('cdlid', courseDevLoc.id);
            
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController lssac_sc = new ApexPages.StandardController(CourseAM);
            luana_StudentSessionAllocationController lssac_e = new luana_StudentSessionAllocationController(lssac_sc);
            lssac_e.doSave();
        Test.stopTest();
        
        List<LuanaSMS__Attendance2__c> attendees = [SELECT Id FROM LuanaSMS__Attendance2__c];
        System.assertEquals(attendees.size(), memberAccounts.size());
    }
    
    /*
        Fail allocation test from CDL
    */
    static testMethod void testFailAllocationFromCDL() {
        Test.startTest();
        setup('3');
        
        newStudentPrograms = new List<LuanaSMS__Student_Program__c>();
        Id studProgAccModuleId = dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module');
        
        for(Contact contact : [select Id from Contact where AccountId in: memberAccounts]){

            LuanaSMS__Student_Program__c newStudProg = dataPrep.createNewStudentProgram(studProgAccModuleId, contact.Id, courseAM.Id, 'Australia', 'In Progress');
            newStudProg.LuanaSMS__Auto_Student_Program_Subject_Creation__c = true;
            newStudProg.Exam_Location__c = courseDevLoc.Id;
            newStudProg.Paid__c = false;
            
            newStudentPrograms.add(newStudProg);
        }
        insert newStudentPrograms;
        
        //Main Test Area
        
        PageReference pageRef = Page.luana_StudentSessionAllocationWizard;
        pageRef.getParameters().put('id', CourseAM.id);
        pageRef.getParameters().put('cdlid', courseDevLoc.id);
        
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController lssac_sc = new ApexPages.StandardController(CourseAM);
        luana_StudentSessionAllocationController lssac_e = new luana_StudentSessionAllocationController(lssac_sc);
        lssac_e.doSave();
        Test.stopTest();
        
        List<LuanaSMS__Attendance2__c> attendees = [SELECT Id FROM LuanaSMS__Attendance2__c];
        System.assertEquals(attendees.size(), 0);
    }
    
    /*
        Success allocation test from Course
    */
    static testMethod void testSuccessAllocationFromCourse() {
        
        setup('4');
        newStudentPrograms = new List<LuanaSMS__Student_Program__c>();
        Id studProgAccModuleId = dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module');
        
        for(Contact contact : [select Id from Contact where AccountId in: memberAccounts]){

            LuanaSMS__Student_Program__c newStudProg = dataPrep.createNewStudentProgram(studProgAccModuleId, contact.Id, courseAM.Id, 'Australia', 'In Progress');
            newStudProg.LuanaSMS__Auto_Student_Program_Subject_Creation__c = true;
            newStudProg.Exam_Location__c = courseDevLoc.Id;
            newStudProg.Paid__c = true;
            
            newStudentPrograms.add(newStudProg);
        }
        
        insert newStudentPrograms;
        
        Test.startTest();
            
            //Main Test Area
            PageReference pageRef = Page.luana_StudentSessionAllocationWizard;
            pageRef.getParameters().put('id', CourseAM.id);
            
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController lssac_sc = new ApexPages.StandardController(CourseAM);
            luana_StudentSessionAllocationController lssac_e = new luana_StudentSessionAllocationController(lssac_sc);
            lssac_e.doSave();
            
        Test.stopTest();
        
        List<LuanaSMS__Attendance2__c> attendees = [SELECT Id FROM LuanaSMS__Attendance2__c];
        System.assertEquals(attendees.size(), memberAccounts.size());
    }
    
    /*
        Fail allocation test from Course - SP unpaid
    */
    static testMethod void testFailAllocationFromCourse() {
        //Main Test Area
        Test.startTest();

        setup('5');
        
        newStudentPrograms = new List<LuanaSMS__Student_Program__c>();
        Id studProgAccModuleId = dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module');
        
        for(Contact contact : [select Id from Contact where AccountId in: memberAccounts]){

            LuanaSMS__Student_Program__c newStudProg = dataPrep.createNewStudentProgram(studProgAccModuleId, contact.Id, courseAM.Id, 'Australia', 'In Progress');
            newStudProg.LuanaSMS__Auto_Student_Program_Subject_Creation__c = true;
            newStudProg.Exam_Location__c = courseDevLoc.Id;
            newStudProg.Paid__c = false;
            
            newStudentPrograms.add(newStudProg);
        }
        
        insert newStudentPrograms;
        
        PageReference pageRef = Page.luana_StudentSessionAllocationWizard;
        pageRef.getParameters().put('id', CourseAM.id);
        
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController lssac_sc = new ApexPages.StandardController(CourseAM);
        luana_StudentSessionAllocationController lssac_e = new luana_StudentSessionAllocationController(lssac_sc);
        lssac_e.doSave();
        Test.stopTest();
        
        List<LuanaSMS__Attendance2__c> attendees = [SELECT Id FROM LuanaSMS__Attendance2__c];
        System.assertEquals(attendees.size(), 0);
    }
    
    /*
        Student program without exam location set test from CDL
    */
    static testMethod void testFailNoCourseLocationFromCDL() {
        
        //Main Test Area
        Test.startTest();
        setup('6');
                
        newStudentPrograms = new List<LuanaSMS__Student_Program__c>();
        Id studProgAccModuleId = dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module');
        
        for(Contact contact : [select Id from Contact where AccountId in: memberAccounts]){

            LuanaSMS__Student_Program__c newStudProg = dataPrep.createNewStudentProgram(studProgAccModuleId, contact.Id, courseAM.Id, 'Australia', 'In Progress');
            newStudProg.LuanaSMS__Auto_Student_Program_Subject_Creation__c = true;
            
            newStudentPrograms.add(newStudProg);
        }
        
        insert newStudentPrograms;
        
        
        PageReference pageRef = Page.luana_StudentSessionAllocationWizard;
        pageRef.getParameters().put('id', CourseAM.id);
        pageRef.getParameters().put('cdlid', courseDevLoc.id);
        
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController lssac_sc = new ApexPages.StandardController(CourseAM);
        luana_StudentSessionAllocationController lssac_e = new luana_StudentSessionAllocationController(lssac_sc);
        lssac_e.doSave();
        Test.stopTest();
        
        List<LuanaSMS__Attendance2__c> attendees = [SELECT Id FROM LuanaSMS__Attendance2__c];
        System.assertEquals(attendees.size(), 0);
    }
    
    /*
        Student program without exam location set test from course
    */
    static testMethod void testFailNoCourseLocationFromCourse() {
        
        Test.startTest();
        setup('7');
                
        newStudentPrograms = new List<LuanaSMS__Student_Program__c>();
        Id studProgAccModuleId = dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module');
        
        for(Contact contact : [select Id from Contact where AccountId in: memberAccounts]){

            LuanaSMS__Student_Program__c newStudProg = dataPrep.createNewStudentProgram(studProgAccModuleId, contact.Id, courseAM.Id, 'Australia', 'In Progress');
            newStudProg.LuanaSMS__Auto_Student_Program_Subject_Creation__c = true;
            
            newStudentPrograms.add(newStudProg);
        }
        
        insert newStudentPrograms;
        
        //Main Test Area

        PageReference pageRef = Page.luana_StudentSessionAllocationWizard;
        pageRef.getParameters().put('id', CourseAM.id);
        
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController lssac_sc = new ApexPages.StandardController(CourseAM);
        luana_StudentSessionAllocationController lssac_e = new luana_StudentSessionAllocationController(lssac_sc);
        lssac_e.doSave();
        Test.stopTest();
        
        List<LuanaSMS__Attendance2__c> attendees = [SELECT Id FROM LuanaSMS__Attendance2__c];
        System.assertEquals(attendees.size(), 0);
    }
}