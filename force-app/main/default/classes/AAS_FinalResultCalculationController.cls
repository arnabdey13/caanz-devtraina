/* 
    Developer: WDCi (KH)
    Development Date: 02/12/2016
    Task #: AAS Final Result Calculaton for exam 
*/
public with sharing class AAS_FinalResultCalculationController {

    public AAS_Course_Assessment__c courseAssessment {get; set;}
    public static String selectedRecordType{get;set;}
    public String urlSelectedType {get; set;}
    public Boolean validToProcess {get; set;}
    
    public Boolean isCapstone;

    ID batchprocessid;
    public boolean isUpdateDone {get;set;}
    public boolean isUpdateDoneWithError {get;set;}
    public boolean checkCommitStatus {get; set;}

    public AAS_FinalResultCalculationController(ApexPages.StandardController controller) {
        
        if(!test.isRunningTest()){
            List<String> addFieldName = new List<String>{'AAS_Course__c', 'AAS_Course__r.Name', 'AAS_Cohort_Adjustment_Exam__c', 'AAS_Final_Adjustment_Exam__c', 'AAS_Final_Adjustment_Supp_Exam__c', 
                                                        'AAS_Borderline_Remark__c', 'AAS_Borderline_Remark_Supp_Exam__c', 'AAS_Cohort_Adjustment_Supp_Exam__c', 'AAS_Final_Result_Release_Exam__c',
                                                        'AAS_Final_Result_Release_Supp_Exam__c', 'AAS_Final_Result_Calculation_Exam__c', 'AAS_Course__r.LuanaSMS__Program_Offering__r.IsCapstone__c'};
            controller.addFields(addFieldName);
        }
        courseAssessment = (AAS_Course_Assessment__c) controller.getRecord();
        
        isCapstone = courseAssessment.AAS_Course__r.LuanaSMS__Program_Offering__r.IsCapstone__c;
        
        validToProcess = true;
        
        urlSelectedType = apexpages.currentpage().getparameters().get('type');
        selectedRecordType = urlSelectedType + '_SAS';
        doValidation();
         
        isUpdateDone= false;
        isUpdateDoneWithError = false;
        checkCommitStatus= false; 
    }
    
    public void doValidation(){
        
        if(selectedRecordType == 'Exam_Assessment_SAS'){
            if(courseAssessment.AAS_Cohort_Adjustment_Exam__c && courseAssessment.AAS_Borderline_Remark__c && courseAssessment.AAS_Final_Adjustment_Exam__c){
                if(courseAssessment.AAS_Final_Result_Calculation_Exam__c){
                    addPageMessage(ApexPages.severity.WARNING, 'Final Result Calculation was completed before, if you wish to continue old data will be lost.');
                }
            }else{
                validToProcess = false;
                addPageMessage(ApexPages.severity.ERROR, 'You are not allow to continue in this step. Please complete the Cohort Adjustment, Borderline Remark and Final Adjusment processes first.');
            }
        }
    }
    
    public void doCalculate(){    
        Savepoint sp = Database.setSavepoint();
        try{
            List<AAS_Student_Assessment__c> updateSAList = new List<AAS_Student_Assessment__c>();
            Set<AAS_Student_Assessment__c> updateSASet = new Set<AAS_Student_Assessment__c>();
            Map<String, Set<String>> sasResultMap = new Map<String, Set<String>>();
            
            for(AAS_Student_Assessment_Section__c sas: [Select Id, AAS_Result__c, RecordType.DeveloperName, AAS_Student_Assessment__c, AAS_Student_Assessment__r.AAS_Final_Result__c, 
                                                                AAS_Student_Assessment__r.AAS_Final_Release_Result__c
                                                                from AAS_Student_Assessment_Section__c 
                                                                Where AAS_Student_Assessment__r.AAS_Course_Assessment__c =: courseAssessment.Id]){
                
                if(isCapstone){
                    
                    updateSASet.add(new AAS_Student_Assessment__c(Id = sas.AAS_Student_Assessment__c, AAS_Final_Result__c = sas.AAS_Student_Assessment__r.AAS_Final_Release_Result__c));
                }else{
                    if(sasResultMap.containsKey(sas.AAS_Student_Assessment__c)){
                        sasResultMap.get(sas.AAS_Student_Assessment__c).add(sas.AAS_Result__c);
                    }else{
                        Set<String> tempResult = new Set<String>();
                        tempResult.add(sas.AAS_Result__c);
                        sasResultMap.put(sas.AAS_Student_Assessment__c, tempResult);
                    }
                    
                    sasResultMap.get(sas.AAS_Student_Assessment__c).add(sas.AAS_Student_Assessment__r.AAS_Final_Release_Result__c);
                }   
            }
           
            System.debug('****sasResultMap:: ' + sasResultMap);
            
            if(!isCapstone){
                for(String saId: sasResultMap.keySet()){
                    if(sasResultMap.get(saId).contains('Fail')){
                        updateSAList.add(new AAS_Student_Assessment__c(Id = saId, AAS_Final_Result__c = 'Fail'));
                    }else{
                        updateSAList.add(new AAS_Student_Assessment__c(Id = saId, AAS_Final_Result__c = 'Pass'));
                    }
                }
            }else{
                for(AAS_Student_Assessment__c sa: updateSASet){
                    updateSAList.add(sa);
                }
            }
            
            courseAssessment.AAS_Final_Result_Calculation_Exam__c = true;
            update courseAssessment;
            
            System.debug('****updateSAList:: ' + updateSAList);
            checkCommitStatus = true;
            
            //batch format change temp commented this batch function out this is not used currently
            //AAS_UpdateBatch updateSABatch = New AAS_UpdateBatch(updateSAList);
            //batchprocessid= database.executeBatch(updateSABatch);
            
        }Catch(Exception exp){
            Database.rollback(sp);
            addPageMessage(ApexPages.severity.ERROR, 'Error processing. Please try again later or contact your system administrator. Error: ' + exp.getMessage() + ' - ' + exp.getStackTraceString());
        }
    }

    
    public void checkBatchCompletion(){

        List<AsyncApexJob> aajs = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =: batchprocessid and Status = 'Completed'];
        
        System.debug('**********aajs: ' + aajs);
        
        if(!aajs.isEmpty()){
            for(AsyncApexJob aaj: aajs){
                if(aaj.NumberOfErrors > 0){
                    isUpdateDone = false;
                    isUpdateDoneWithError = true;
                }else{
                    isUpdateDone = true;
                    isUpdateDoneWithError = false;
                }
            }
            checkCommitStatus = false;

        }
    }
    
    public PageReference doRedirect(){
        if(isUpdateDone){
            PageReference returnPage = new PageReference('/'+courseAssessment.Id);
            return returnPage;
        }else{
            if(!checkCommitStatus){
                 addPageMessage(ApexPages.severity.ERROR, 'Error found during Calculation process. Please try again later or contact your system administrator.');
            }
            return null;
        }
    }
    
    public PageReference doCancel(){
        PageReference returnPage = new PageReference('/'+courseAssessment.Id);
        return returnPage;
    }
    
    public void addPageMessage(ApexPages.severity sev, String msg){
        ApexPages.Message warningmsg = new ApexPages.Message(sev, msg);
        ApexPages.addmessage(warningmsg);
    }   
    
}