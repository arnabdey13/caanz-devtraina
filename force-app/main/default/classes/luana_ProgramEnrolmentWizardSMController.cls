/* 
    Developer: WDCi (Lean)
    Date: 02/Aug/2016
    Task #: LCA-822 Luana implementation - Wizard for Non AU program enrolment
*/

public without sharing class luana_ProgramEnrolmentWizardSMController {
    
    luana_ProgramEnrolmentWizardController stdController;
        
    public luana_ProgramEnrolmentWizardSMController(luana_ProgramEnrolmentWizardController stdController){
        this.stdController = stdController;
        
        init();
    }
    
    public void init(){
    	
    }
    
    public PageReference summaryNext(){
    	
    	return null;
    	
    }
    
    public PageReference summaryBack(){
    	
    	return null;
    }
}