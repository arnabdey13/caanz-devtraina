@IsTest
private class TestAppFormsReferences {


    @testSetup static void createTestData() {
        AppFormsTestUtil.createMemberAccount(); 
    }

    private static User getMemberUser() {
        Id contactId = [SELECT Id FROM Contact WHERE AccountId = :TestSubscriptionUtils.getTestAccountId()].Id;
        return [select Id from User where ContactId = :contactId];
    }

     
    @IsTest
    private static void testReferences() {
    
       Id accountId = TestSubscriptionUtils.getTestAccountId();   
       
        AppFormsDriverController service = new AppFormsDriverController(accountId ,'Special_Admissions');
        ff_WriteData wd = new ff_WriteData();
        wd.records.put('INSERT-Application_References__c1', new Application_References__c(        
                        External_Reference_Email__c= 'abc@123.com'));                        
        ff_WriteResult result = service.handler.save(wd); 
                        
        wd = new ff_WriteData();
        wd.records.put('INSERT-Application_References__c2', new Application_References__c(        
                        Internal_Member_Id__c= '12345'));  
         service = new AppFormsDriverController(accountId ,'Special_Admissions');
                ff_WriteResult result1 = service.handler.save(wd); 
                System.debug(result1);
        //System.assertEquals(1, result1.recordErrors.size(), 'No Matching Refernces Found');
       
    }
    
    
}