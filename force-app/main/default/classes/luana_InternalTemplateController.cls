/* 
    Developer: WDCi (Lean)
    Date: 12/May/2016
    Task #: Luana implementation - Wizard for Late Enrolment
*/
public with sharing class luana_InternalTemplateController {
    
    public String userPhotoUrl {get; set;}
    public Boolean isMember {get; private set;}
    
    public luana_InternalTemplateController(){
        
        luana_CommUserUtil conUser = new luana_CommUserUtil(UserInfo.getUserId());
        
        isMember = false;
        
        //LCA-346
        userPhotoUrl = conUser.getUserPhoto(Network.getNetworkId(), UserInfo.getUserId());
    }
        
    public String getRetURL(){
        if(ApexPages.currentPage().getParameters().containsKey('retURL')){
            return EncodingUtil.urlDecode(ApexPages.currentPage().getParameters().get('retURL'), 'UTF-8');
            
        }
        
        return '/';
    }
    
}