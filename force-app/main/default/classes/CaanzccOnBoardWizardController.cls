/*------------------------------------------------------------------------------------
Author:        Sumit Gupta
Company:       Salesforce
Description:   Address details component controller

History
Date            Author             Comments
--------------------------------------------------------------------------------------
10-05-2018     Sumit Gupta       	Initial Release
------------------------------------------------------------------------------------*/
public with sharing class CaanzccOnBoardWizardController {

    @AuraEnabled
    public static String getWizardData(){
    	User currentUser = [SELECT Id, ContactId, AccountId, OnBoarding_Required__c FROM User WHERE Id =: UserInfo.getUserId()];
        return JSON.serialize(currentUser);
    }

    @AuraEnabled
    public static void acceptTermsAndConditions(){
    	update new User(
    		Id = UserInfo.getUserId(), OnBoarding_Required__c = false, 
    		TermsAndConditions_Status__c = 'Accepted', TermsAndConditions_UpdatedOn__c = DateTime.now()
    	);
    }
    	
    @AuraEnabled
    public static void rejectTermsAndConditions(){
    	update new User(
    		Id = UserInfo.getUserId(), OnBoarding_Required__c = true, 
    		TermsAndConditions_Status__c = 'Rejected', TermsAndConditions_UpdatedOn__c = DateTime.now()
    	);
    }
}