/**
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Author:         Sudheendra GS
Organisation:   TechMahindra
Role:           Developer
Title:          SoapLoginClass.cls
Test Class:     SoapLoginClass_Test.cls
Version:        0.0
Since:          25/04/2019
Description:    This Class is used to login to Salesforce with User Credentials via API to get Server URL and Session Id.

History
<Date>          <Authors Name>          <Brief Description of Change>
25/04/2019      Sudheendra GS           Class Created.                                       
                                        This Class is used to login to Salesforce with User Credentials via API to get Server 
                                        URL and Session Id.
26/06/2019      Rakesh Murugan          Updated to use NamedCredentials, as opposed to CustomSettings.
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
**/

public class SoapLoginClass{    
    
    //Method for to prepare Parameters required for Login and call the login method.
    public static String[] login() {
        System.debug('');
        System.debug('DEBUG - Entered SoapLoginClass');            

        //Make the API call with the SOAP Body and get the response.
        String outCallResultLogin = makeHTTPCall();
        System.debug('----- - outCallResultLogin: ' + outCallResultLogin);        

        //Get the Server URL and Session ID from the Response.
        String serverUrl = getValueFromXMLString(outCallResultLogin, 'serverUrl');      
        String session = getValueFromXMLString(outCallResultLogin, 'sessionId'); 
         
        System.debug('----- - serverUrl: ' + serverUrl);
        System.debug('----- - session: ' + session);
        System.debug('DEBUG - Exited SoapLoginClass');       
        System.debug('');

        return new String[]{serverUrl, session};
    }

    private static string makeHTTPCall() {
        System.debug('');
        System.debug('DEBUG - Entered makeHTTPCall');            
        
        HttpRequest reqLLogin = new HttpRequest();
        reqLLogin.setTimeout(60000);
        reqLLogin.setEndpoint('callout:FormAssembly_User_NamedCred/services/Soap/u/45.0');  
        reqLLogin.setMethod('POST');
        reqLLogin.setHeader('SFDC_STACK_DEPTH', '1');
        reqLLogin.setHeader('SOAPAction','DoesNotMatter'); 
        reqLLogin.setHeader('Accept','text/xml');  
        reqLLogin.setHeader('Content-type','text/xml');    
        reqLLogin.setHeader('charset','UTF-8');
        
        //Prepare SOAP Body for Callout.
        String username = '{!$Credential.UserName}';
        String password = '{!$Credential.Password}';
        String httpBody = String.format(Label.loginTemplate, new String[]{ username , password });
        reqLLogin.setBody(httpBody);

        System.debug('----- - httpBody: ' + httpBody);
        System.debug('----- - reqLLogin: ' + reqLLogin);

        String responseBody = null;
        try {
            Http hLLogin = new Http();
            HttpResponse resLLogin = hLLogin.send(reqLLogin);
            System.debug('----- - resLLogin: ' + resLLogin);
            
            responseBody = resLLogin.getBody();
            System.debug('----- - responseBody: ' + responseBody);

        } catch(System.CalloutException e) {
            System.debug('----- - Callout error: '+ e);
        }

        System.debug('DEBUG - Exited makeHTTPCall');
        System.debug('');

        return responseBody;       
    }

    //Parsing the Response to get the keyField(Session Id) Value.
    private static string getValueFromXMLString(string xmlString, string keyField){
        String valueFound = '';
        if (xmlString.contains('<' + keyField + '>') && xmlString.contains('</' + keyField + '>')) {
            try {
                valueFound = xmlString.substringBetween('<' + keyField + '>', '</' + keyField + '>');
            } catch (exception e) {
                system.debug('Error in getValueFromXMLString.  Details: ' + e.getMessage() + ' keyfield: ' + keyfield);
            }            
        }
        return valueFound;
    }

    /**
    //Method for to prepare Parameters required for Login and call the login method.
    public static String[] login(string userName, string password, string loginURL) {
        System.debug('');
        System.debug('DEBUG - Entered SoapLoginClass');       
        System.debug('----- - userName: ' + userName);
        System.debug('----- - password: ' + password);
        System.debug('----- - loginURL: ' + loginURL);
        
        //Calling the Customlabel where loginTemplate has been stored.
        String loginTemplate = Label.loginTemplate;
        System.debug('----- - loginTemplate: ' + loginTemplate);        

        //Prepare SOAP Body for Callout.
        String bodyToSendLogin = String.format(loginTemplate, new String[]{UserName, password});
        System.debug('----- - bodyToSendLogin: ' + bodyToSendLogin);        

        //Make the API call with the SOAP Body and get the response.
        String outCallResultLogin = makeHTTPCall(loginURL, bodyToSendLogin);
        System.debug('----- - outCallResultLogin: ' + outCallResultLogin);        

        //Get the Server URL and Session ID from the Response.
        String serverUrl = getValueFromXMLString(outCallResultLogin, 'serverUrl');      
        String session = getValueFromXMLString(outCallResultLogin, 'sessionId'); 
        String paramvalue = EncodingUtil.base64Encode(Blob.valueOf(session));
         
        System.debug('----- - serverUrl: ' + serverUrl);
        System.debug('----- - session: ' + session);
        System.debug('----- - paramvalue: ' + paramvalue);
        System.debug('DEBUG - Exited SoapLoginClass');       

        return new String[]{serverUrl, session};
        //End of XML Generation for Partner API SOAP Login Call
    }

    //Method Making Http call with Soap Body.
    private static string makeHTTPCall(string endPoint, string soapBody){
        System.debug('');
        System.debug('DEBUG - Entered makeHTTPCall');  

        HttpRequest reqLLogin = new HttpRequest();
        reqLLogin.setTimeout(60000);
        reqLLogin.setEndpoint(endPoint);  
        reqLLogin.setMethod('POST');
        reqLLogin.setHeader('SFDC_STACK_DEPTH', '1');
        reqLLogin.setHeader('SOAPAction','DoesNotMatter'); 
        reqLLogin.setHeader('Accept','text/xml');  
        reqLLogin.setHeader('Content-type','text/xml');    
        reqLLogin.setHeader('charset','UTF-8');
        reqLLogin.setBody(soapBody);

        System.debug('DEBUG - reqLLogin: ' + reqLLogin);

        String outCallResultLogin = null;
        try {
            Http hLLogin = new Http();
            HttpResponse resLLogin = hLLogin.send(reqLLogin);
            System.debug('DEBUG - resLLogin: ' + resLLogin);
            
            outCallResultLogin = resLLogin.getBody();
            System.debug('DEBUG - outCallResultLogin: ' + outCallResultLogin);

        } catch(System.CalloutException e) {
            System.debug('Callout error: '+ e);
        }
        
        System.debug('DEBUG - Exited makeHTTPCall');       
        return outCallResultLogin;
    }
    **/    
}