/***********************************************************************************************************************************************************************
Name: TaskTriggerHandler 
============================================================================================================================== 
Purpose: 
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR                 DATE          DETAIL     Description 
1.0        Mayukhman Pathak       10/07/2019     Created    
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/
public class TaskTriggerHandler extends TriggerHandler{
    //Context Variable Collections after Type Casting
    List<Task> newList = (List<Task>)Trigger.new;
    List<Task> oldList = (List<Task>)Trigger.old;
    Map<Id,Task> oldMap = (Map<Id,Task>)Trigger.oldMap;
    Map<Id,Task> newMap = (Map<Id,Task>)Trigger.newMap;    
    
    public TaskTriggerHandler (){     
    }
    
    public override void afterInsert(){ 
        updateCaseInitialResponseTime(newMap);        
    }
    public override void afterUpdate(){  
        updateCaseInitialResponseTime(newMap);
    }
    
    /*public override void beforeUpdate(){  
    }
    
    public override void beforeInsert(){              
        
    }
    
    public override void afterDelete(){
        
    }
    
    public override void afterUndelete(){
        
    }
*/
    /* Method to Update Intial Reponse Time in Case*/
    private void updateCaseInitialResponseTime(Map<Id,Task> newMap){
        Set<Id> caseIds = new Set<Id>();
        List<Case> updateListCase = new List<Case>();
        List<Task> lstTask = new List<Task>([Select Id,WhatId,What.Type,Status,TaskSubtype  From Task 
                                             Where Id =: newMap.keySet() AND What.Type = 'Case' AND TaskSubtype != 'Email' AND 
                                             (Status =: 'In Progress' OR Status =: 'Completed' )]);
        if(lstTask.size() > 0){
            for(Task tsk: lstTask) 
                caseIds.add(tsk.WhatId);
            
            for(Case cse : [Select Id,RecordTypeId,RecordType.DeveloperName,Initial_Response_Time__c from Case where Id IN: caseIds]){
                if(cse.RecordType.DeveloperName == 'Case' && cse.Initial_Response_Time__c == null){
                    cse.Initial_Response_Time__c = System.now();
                    updateListCase.add(cse);
                }
            }
            if(updateListCase.size() > 0){            
                try{
                    update updateListCase;
                }
                catch(Exception ae){
                    system.debug('Exception : ' + ae );
                }
            }
        }
    }
}