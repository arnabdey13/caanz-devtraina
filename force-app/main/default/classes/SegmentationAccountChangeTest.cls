/**
    Developer: akopec
    Date:	27/03/2018
    
    Change History:
**/
@isTest(seeAllData=false)
public class SegmentationAccountChangeTest {

        ////////////////////// Main Practice Lookup Prep ///////////////////////////////////////////////////////////////
    @testSetup 
    static void setup() {
        
        Reference_Main_Practices__c  deloitte = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Deloitte'
            , Search_Name__c     = 'Deloitte'
            , Full_Name__c       = 'Deloitte'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            = 'Deloitte'
        );
      
		Reference_Main_Practices__c  pwc = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'PricewaterhouseCoopers;PWC; PwC; PriceWaterHouseCoopers; PriceWaterhouseCoopers; PricewaterhousCoopers; Pricewaterhousecoopers'
            , Search_Name__c     = 'PricewaterhouseCoopers'
            , Full_Name__c       = 'PricewaterhouseCoopers'
            , Practice_Size__c   = 'Large Firm'
			, Big4__c            = 'PWC'
        );
        
		Reference_Main_Practices__c  kpmg = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'KPMG'
            , Search_Name__c     = 'KPMG'
            , Full_Name__c       = 'KPMG'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            = 'KPMG'
        );
        
		Reference_Main_Practices__c  ey = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Ernst and Young; EY; Ernst & Young'
            , Search_Name__c     = 'Ernst and Young'
            , Full_Name__c       = 'Ernst and Young'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            = 'EY'
        );
        
		Reference_Main_Practices__c  bdo = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'BDO'
            , Search_Name__c     = 'BDO'
            , Full_Name__c       = 'BDO'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            =  NULL
        );
        
		Reference_Main_Practices__c  gt = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Grant Thornton'
            , Search_Name__c     = 'Grant Thornton'
            , Full_Name__c       = 'Grant Thornton'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            =  NULL
        );        
        
        
		Reference_Main_Practices__c  vincents = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Vincents Chartered Accountants; Vincents'
            , Search_Name__c     = 'Vincents Chartered Accountants'
            , Full_Name__c       = 'Vincents Chartered Accountants'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            =  NULL
        );
        
        Reference_Main_Practices__c  boroughs = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Boroughs'
            , Search_Name__c     = 'Boroughs'
            , Full_Name__c       = 'Boroughs'
            , Practice_Size__c   = 'Medium Firm'
            , Big4__c            =  NULL
        );
        
        insert new List<Reference_Main_Practices__c>{pwc, deloitte, kpmg, ey, bdo, gt, vincents, boroughs};  
         
		Segmentation_Settings__c setting = new Segmentation_Settings__c();        
        setting.Name = 'SegmentationMembersUpdate';
		setting.Active__c = TRUE;
		insert setting;
    
        
    }
    
    private static boolean debug = true;

 	// Case 1.  Senior AU Member, previously closed employment
    //			Now Partner in Deloitte
	private static testMethod void SeniorMemberAccountAU_Case1(){
		if (debug) System.debug( 'TEST PROCESSING - SegmentationMembersTriggerHandlerTest');
		if (debug) System.debug( 'SeniorMemberAccountAU_Case1');

		Account SeniorMemberAccountAU = SegmentationDataGenTest.genFullMemberAustralia('Turner', 'Tax Manager', 'brown1@test.co.au', Date.valueOf('1977-05-12') );
        insert SeniorMemberAccountAU; 
        Account big4BusinessAccountAU = SegmentationDataGenTest.genPracticeAU( 'Deloitte', 'Chartered Accounting', 'Sydney', 25);
		insert big4BusinessAccountAU;        
        Employment_History__c histEmpSeniorMemberAU = SegmentationDataGenTest.genHistoricalEmploymentHistory(SeniorMemberAccountAU.Id, big4BusinessAccountAU.Id, 'Tax Manager', 'Manager');  
		insert histEmpSeniorMemberAU; 

        Segmentation__c seniorMemberAUSeg = new Segmentation__c();
        seniorMemberAUSeg.Account__c = SeniorMemberAccountAU.Id;
        seniorMemberAUSeg.Organisation_Type__c = 'Education';
		insert seniorMemberAUSeg;
        
        Test.startTest();         
        SegmentationMembersTriggerHandlerTest.forceTriggerDuringTest = true;
        
		Account seniorMember = [select id, Name from Account where LastName = 'Turner'];
        System.debug('Senior Member before :' + seniorMember);
        List<Employment_History__c> eh = [Select Id, Name, Member__c, Employer__c, Job_Title__c, Status__c From Employment_History__c where Member__c=:seniorMember.Id];
        System.debug('Senior Member Employment before :' + eh);
        List<Segmentation__c> segmentsBefore  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{seniorMember.Id}];
        System.debug('Senior Member Segmentation before :' + segmentsBefore);
		// now apply changes        
        Account businessAccount = [select id,name from Account where Name = 'Deloitte'];
		System.debug('Deloitte Business in Test: ' + businessAccount);
        Employment_History__c empHist = SegmentationDataGenTest.genCurrentEmploymentHistory(seniorMember.Id, businessAccount.Id, 'Director', 'Partner');  
		insert empHist;
		eh = [Select Id, Name, Member__c, Employer__c, Job_Title__c, Status__c From Employment_History__c where Member__c=:seniorMember.Id];
        System.debug('Senior Member Employment after :' + eh);
		Test.stopTest();
        
        List<Segmentation__c> segmentsAfter  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{seniorMember.Id}];

        for (Segmentation__c seg : segmentsAfter) {
			System.debug('Senior Member Segmentation After:' + seg);
            System.assertEquals('Practice', seg.Organisation_Type__c);
            System.assertEquals('Big 4', seg.Firm_Type__c);
            System.assertEquals('Large Practice Executive', seg.Business_Segmentation_MS__c);
            System.assertEquals('Executive', seg.Career_Stage__c); 
            System.assertEquals('Large', seg.Firm_Office_Size__c); 
        }
    }

    
      // Case 2. No Employment
    
	private static testMethod void YoungMemberBig4NZ_Case2(){
		if (debug) System.debug( 'YoungMemberBig4NZ C2 No Employment');
        
		Account JuniorMemberAccountNZ = SegmentationDataGenTest.genFullMemberNewZealand('Smith', 'Senior Accountant', 'smith1@test.co.nz', Date.valueOf('1987-05-12') );
        insert JuniorMemberAccountNZ;       

        Segmentation__c juniorMemberSeg = new Segmentation__c();
        juniorMemberSeg.Account__c = JuniorMemberAccountNZ.Id;
        juniorMemberSeg.Business_Segmentation_MS__c = 'SMP Team Player';
        juniorMemberSeg.Organisation_Type__c = 'Not For Profit';
		insert juniorMemberSeg;
        Account big4BusinessAccountNZ = SegmentationDataGenTest.genPracticeNewZealand( 'Ernst & Young', 'Chartered Accounting', 'Auckland', 25);
		insert big4BusinessAccountNZ;

        Test.startTest();         
		SegmentationMembersTriggerHandlerTest.forceTriggerDuringTest = true; 
        
		Account JuniorMember = [select id,name from Account where LastName = 'Smith'];
		System.debug('Junior Member in Test: ' + JuniorMember);
 		Account businessAccount = [select id,name from Account where Name = 'Ernst & Young'];
		System.debug('Mid Tier Business in Test: ' + businessAccount);
		Employment_History__c empHist = SegmentationDataGenTest.genHistoricalEmploymentHistory(JuniorMember.Id, businessAccount.Id, 'Accountant', 'Senior Accountant');  
		insert empHist;   

		Test.stopTest();
        
        List<Segmentation__c> segmentsAfter  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{JuniorMember.Id}];

        for (Segmentation__c seg : segmentsAfter) {
			System.debug(seg);
            System.assertEquals(NULL, seg.Organisation_Type__c);
            System.assertEquals(NULL, seg.Firm_Type__c);
            System.assertEquals('Member No Employment', seg.Business_Segmentation_MS__c);
            System.assertEquals('Team Player', seg.Career_Stage__c); 
            System.assertEquals(NULL, seg.Firm_Office_Size__c); 
        }
    }
    
    
   // Case 3. Previously No Employment, now Big 4
    
	private static testMethod void YoungMemberBig4NZ_Case3(){
		if (debug) System.debug( 'YoungMemberBig4NZ C3 Changed Employment');
         
		Account JuniorMemberAccountNZ = SegmentationDataGenTest.genFullMemberNewZealand('Smith', 'Senior Accountant', 'smith1@test.co.nz', Date.valueOf('1987-05-12') );
        insert JuniorMemberAccountNZ;  
        Segmentation__c juniorMemberSeg = new Segmentation__c();
        juniorMemberSeg.Account__c = JuniorMemberAccountNZ.Id;
        juniorMemberSeg.Business_Segmentation_MS__c = 'Member No Employment';
        juniorMemberSeg.Organisation_Type__c = 'Not For Profit';
		insert juniorMemberSeg;
        Account big4BusinessAccountNZ = SegmentationDataGenTest.genPracticeNewZealand( 'Ernst & Young', 'Chartered Accounting', 'Auckland', 25);
		insert big4BusinessAccountNZ;
  
		Test.startTest();
        SegmentationMembersTriggerHandlerTest.forceTriggerDuringTest = true;
        
		Account JuniorMember = [select id,name from Account where LastName = 'Smith'];
		System.debug('Junior Member in Test: ' + JuniorMember);
 		Account businessAccount = [select id,name from Account where Name = 'Ernst & Young'];
		System.debug('Mid Tier Business in Test: ' + businessAccount);
		Employment_History__c empHist = SegmentationDataGenTest.genCurrentEmploymentHistory(JuniorMember.Id, businessAccount.Id, 'Accountant', 'Senior Accountant');  
		insert empHist;   
        
		Test.stopTest();	        
        List<Segmentation__c> segmentsAfter  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{JuniorMember.Id}];

        for (Segmentation__c seg : segmentsAfter) {
			System.debug(seg);
            System.assertEquals('Practice', seg.Organisation_Type__c);
            System.assertEquals('Big 4', seg.Firm_Type__c);
            System.assertEquals('Large Practice Team Player', seg.Business_Segmentation_MS__c);
            System.assertEquals('Team Player', seg.Career_Stage__c); 
            System.assertEquals('Large', seg.Firm_Office_Size__c); 
        }
    }
    
    
    // Case 7.  Senior AU Member, previously closed employment
    //			Now re-opened - end date cleared   
	private static testMethod void SeniorMemberEmpChangeAU_Case7(){
		if (debug) System.debug( 'TEST PROCESSING - SegmentationMembersTriggerHandlerTest');
		if (debug) System.debug( 'SeniorMemberEmpChangeAU_Case7');
        
		Account SeniorMemberAccountAU = SegmentationDataGenTest.genFullMemberAustralia('Turner', 'Tax Manager', 'brown1@test.co.au', Date.valueOf('1977-05-12') );
        insert SeniorMemberAccountAU; 
        Account big4BusinessAccountAU = SegmentationDataGenTest.genPracticeAU( 'Deloitte', 'Chartered Accounting', 'Sydney', 25);
		insert big4BusinessAccountAU;        
        Employment_History__c histEmpSeniorMemberAU = SegmentationDataGenTest.genHistoricalEmploymentHistory(SeniorMemberAccountAU.Id, big4BusinessAccountAU.Id, 'Tax Manager', 'Manager');  
		insert histEmpSeniorMemberAU; 

        Segmentation__c seniorMemberAUSeg = new Segmentation__c();
        seniorMemberAUSeg.Account__c = SeniorMemberAccountAU.Id;
        seniorMemberAUSeg.Organisation_Type__c = 'Education';
		insert seniorMemberAUSeg;
        
        Test.startTest();    
		SegmentationMembersTriggerHandlerTest.forceTriggerDuringTest = true;
        
		Account seniorMember = [select id, Name from Account where LastName = 'Turner'];
        System.debug('Senior Member before :' + seniorMember);
        Employment_History__c eh = [Select Id, Name, Member__c, Employer__c, Job_Title__c, Status__c, Primary_Employer__c, Employee_Start_Date__c, Employee_End_Date__c 
                                          From Employment_History__c where Member__c=:seniorMember.Id][0];
        System.debug('Senior Member Employment before :' + eh);
        List<Segmentation__c> segmentsBefore  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{seniorMember.Id}];
        System.debug('Senior Member Segmentation before :' + segmentsBefore);
        eh.Status__c = 'Current';
		eh.Primary_Employer__c = true;
        eh.Employee_End_Date__c = NULL;
		update eh;   
		Test.stopTest();
        
        List<Segmentation__c> segmentsAfter  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{seniorMember.Id}];

        for (Segmentation__c seg : segmentsAfter) {
			System.debug('Senior Member Segmentation After Emp Change:' + seg);
            System.assertEquals('Practice', seg.Organisation_Type__c);
            System.assertEquals('Big 4', seg.Firm_Type__c);
            System.assertEquals('Large Practice Leader', seg.Business_Segmentation_MS__c);
            System.assertEquals('Leader', seg.Career_Stage__c); 
            System.assertEquals('Large', seg.Firm_Office_Size__c); 
        }
    }

     // Case 8.  Senior AU Member, previously closed employment retired
	private static testMethod void SeniorMemberRetiredAU_Case8(){
		if (debug) System.debug( 'TEST PROCESSING - SegmentationMembersTriggerHandlerTest');
		if (debug) System.debug( 'SeniorMemberRetiredAU_Case8');
        
		Account SeniorMemberAccountAU = SegmentationDataGenTest.genFullMemberAustralia('Turner', 'Tax Manager', 'brown1@test.co.au', Date.valueOf('1977-05-12') );
        insert SeniorMemberAccountAU; 
        Account big4BusinessAccountAU = SegmentationDataGenTest.genPracticeAU( 'Deloitte', 'Chartered Accounting', 'Sydney', 25);
		insert big4BusinessAccountAU;        
        Employment_History__c histEmpSeniorMemberAU = SegmentationDataGenTest.genHistoricalEmploymentHistory(SeniorMemberAccountAU.Id, big4BusinessAccountAU.Id, 'Tax Manager', 'Manager');  
		insert histEmpSeniorMemberAU; 

        Segmentation__c seniorMemberAUSeg = new Segmentation__c();
        seniorMemberAUSeg.Account__c = SeniorMemberAccountAU.Id;
        seniorMemberAUSeg.Organisation_Type__c = 'Education';
		insert seniorMemberAUSeg;
        
        Test.startTest();        
		SegmentationMembersTriggerHandlerTest.forceTriggerDuringTest = true;
        
		Account seniorMember = [select id, LastName, Status__c, Membership_Type__c, Financial_Category__c from Account where LastName = 'Turner'];
        System.debug('Senior Member before :' + seniorMember);

        List<Segmentation__c> segmentsBefore  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{seniorMember.Id}];
        System.debug('Senior Member Segmentation before :' + segmentsBefore);
		// now apply changes  
		seniorMember.Financial_Category__c = 'Honorary Retired';
        update seniorMember;
		Test.stopTest();
        
        List<Segmentation__c> segmentsAfter  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{seniorMember.Id}];

        for (Segmentation__c seg : segmentsAfter) {
			System.debug('Senior Member Retired Segmentation After:' + seg);
            System.assertEquals(NULL, seg.Organisation_Type__c);
            System.assertEquals(NULL, seg.Firm_Type__c);
            System.assertEquals('Retired or Semi-Retired', seg.Business_Segmentation_MS__c);
            System.assertEquals(NULL, seg.Career_Stage__c); 
            System.assertEquals(NULL, seg.Firm_Office_Size__c); 
        }
    }



	// Case 9.  Senior Overseas Member, retired
	private static testMethod void SeniorMemberRetiredOverseas_Case9(){
		if (debug) System.debug( 'TEST PROCESSING - SegmentationMembersTriggerHandlerTest');
		if (debug) System.debug( 'SeniorMemberRetiredOverseas_Case9');

        Account overseasMemberAccount = SegmentationDataGenTest.genFullMemberOverseas('Kwong', 'Accountant', 'kwong@test.co.hk', Date.valueOf('1981-05-12') );
        insert overseasMemberAccount;
        
        Test.startTest();
		SegmentationMembersTriggerHandlerTest.forceTriggerDuringTest = true;
        
		Account seniorMember = [select id, LastName, Status__c, Membership_Type__c, Financial_Category__c from Account where LastName = 'Kwong'];
        System.debug('Senior Member before :' + seniorMember);

        List<Segmentation__c> segmentsBefore  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{seniorMember.Id}];
        System.debug('Senior Member Segmentation before :' + segmentsBefore);
		// now apply changes  
		seniorMember.Financial_Category__c = 'Honorary Retired';
        update seniorMember;
		Test.stopTest();
        
        List<Segmentation__c> segmentsAfter  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{seniorMember.Id}];

        for (Segmentation__c seg : segmentsAfter) {
			System.debug('Senior Overseas Member Retired Segmentation After:' + seg);
            System.assertEquals(NULL, seg.Organisation_Type__c);
            System.assertEquals(NULL, seg.Firm_Type__c);
            System.assertEquals('Retired or Semi-Retired', seg.Business_Segmentation_MS__c);
            System.assertEquals(NULL, seg.Career_Stage__c); 
            System.assertEquals(NULL, seg.Firm_Office_Size__c); 
        }
    }
    
    // Case 10.  Senior AU Member moves Overseas
	private static testMethod void SeniorMemberOverseas_Case10(){
		if (debug) System.debug( 'TEST PROCESSING - SegmentationMembersTriggerHandlerTest');
		if (debug) System.debug( 'SeniorMemberOverseas_Case10');
        
		Account SeniorMemberAccountAU = SegmentationDataGenTest.genFullMemberAustralia('Turner', 'Tax Manager', 'brown1@test.co.au', Date.valueOf('1977-05-12') );
        insert SeniorMemberAccountAU; 

        Segmentation__c seniorMemberAUSeg = new Segmentation__c();
        seniorMemberAUSeg.Account__c = SeniorMemberAccountAU.Id;
        seniorMemberAUSeg.Organisation_Type__c = 'Education';
		insert seniorMemberAUSeg;
        
        Test.startTest(); 
		SegmentationMembersTriggerHandlerTest.forceTriggerDuringTest = true;
        
		Account seniorMember = [select id, PersonOtherCountry from Account where LastName = 'Turner'];
        System.debug('Senior Member before :' + seniorMember);
        List<Segmentation__c> segmentsBefore  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{seniorMember.Id}];
        System.debug('Senior Member Segmentation before :' + segmentsBefore);
        seniorMember.PersonOtherCountry = 'Hong Kong';
        seniorMember.PersonMailingCountry = 'Hong Kong';
		update seniorMember;
 
		Test.stopTest();    
        List<Segmentation__c> segmentsAfter  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{seniorMember.Id}];

        for (Segmentation__c seg : segmentsAfter) {
			System.debug('Senior Overseas Member Segmentation After:' + seg);
            System.assertEquals('Overseas', seg.Business_Segmentation_MS__c); 
        }
    }
    
}