/*
    API for FindCA - Find CA - Search Public Practitioner
    October 2017   CAANZ - akopec  

    Example: /services/apexrest/FindCA/v1.0/Public?Branch=New+South+Wales&MemberType=Business+Valuation+Specialist&Postcode=206
    Example: /services/apexrest/FindCA/v1.0/Public?BranchCountry=Australia&Name=Kate+Barker&MemberType=Business+Valuation+Specialist
    Example: /services/apexrest/FindCA/v1.0/Public?BranchCountry=Australia&MemberType=Forensic+Accounting+Specialist

	// R01 akopec Jan 2018 search by Name returns closest matches first
    Example: /services/apexrest/FindCA/v1.0/Public?BranchCountry=Australia&Name=Kate+Barker
	// R02 akopec Feb 2018 introduce upper limit on number of returned results
	// RO3 akopec 21/11/2018 introduced Forensic_Accounting_Specialisation
*/
@RestResource(urlMapping='/FindCA/v1.0/Public/*')
global with sharing class FindCAPublic {

    private static final String STR_MEMBERTYPE_QUALIFIED_AUDITORS = 'Qualified Auditor';
    private static final String STR_MEMBERTYPE_INSOLVENCY_PRACTICE = 'Insolvency Practitioner';
    private static final String STR_MEMBERTYPE_SMSF_SPECIALIST = 'SMSF Specialist';
    private static final String STR_MEMBERTYPE_BUSINESS_VALUATION_SPECIALIST = 'Business Valuation Specialist';
    private static final String STR_MEMBERTYPE_FINANCIAL_PLANNING_SPECIALIST = 'Financial Planning Specialist';
    private static final String STR_MEMBERTYPE_FORENSIC_ACCOUNTING_SPECIALIST = 'Forensic Accounting Specialist';   // RO3
    private static final String STR_MEMBERTYPE_CPP_HELD = 'Public Practitioner';
    private static final String STR_NAME = 'Name';
        
    public static String strSelectedBranchCountryOption {get; set;}
	public static String strSelectedBranchOption {get; set;}
    public static String strSelectedPostcodeOption {get; set;}
	public static String strSelectedMemberTypeOption {get; set;}
	public static String strInputName {get; set;}
    public static String strInputBusName {get; set;}
    
  
    @HttpGet
	global static List<CA> doGet() {
    	String vBranchCountry = '';
        String vBranch        = '';
        String vMemberType    = '';
        String vPostcode      = '';
        String vName          = '';
        String vBusName       = '';
        
        if (!String.isBlank(RestContext.request.params.get('BranchCountry')))
        	vBranchCountry = RestContext.request.params.get('BranchCountry');
        if (!String.isBlank(RestContext.request.params.get('Branch')))
        	vBranch        = RestContext.request.params.get('Branch');
        if (!String.isBlank(RestContext.request.params.get('Postcode')))
        	vPostcode        = RestContext.request.params.get('Postcode');
        if (!String.isBlank(RestContext.request.params.get('MemberType')))
        	vMemberType    = RestContext.request.params.get('MemberType');
        if (!String.isBlank(RestContext.request.params.get('Name')))
        	vName          = RestContext.request.params.get('Name');
         if (!String.isBlank(RestContext.request.params.get('BusName')))
        	vBusName       = RestContext.request.params.get('BusName');
        
        System.debug('Request Parameters:' + vBranchCountry + ',' + vBranch + ',' + vPostcode + ',' + vMemberType + ',' + vName + ',' + vBusName );     
		return executeSearchCA( vBranchCountry, vBranch, vPostcode, vMemberType, vName, vBusName);
    }
    
    private static boolean IsValidMemberType(String vMemberType) {
      List<FindCA_API_Member_Type__mdt> members = [Select Label, Country__c, Member_Type__c From FindCA_API_Member_Type__mdt where Member_Type__c = :vMemberType];
      if (members.size()>0) 
          return true;
      else
          return false;
  	 }
    
    
    // List of search results
    //  FindCAPublic.searchCA( 'Australia', 'New South Wales', String vMemberType, String vName)
    public static List<CA> executeSearchCA( String vBranchCountry,String vBranch,String vPostcode, String vMemberType,String vName,String vBusName)
    {    
        String query;
        Integer outputLimit = 200;	// R02 Limit
        Integer counter = 0;		// R02 Limit
        
		strSelectedBranchCountryOption = vBranchCountry;
		strSelectedBranchOption        = vBranch;
		strSelectedPostcodeOption	   = vPostcode;
		strSelectedMemberTypeOption    = vMemberType;
		strInputName                   = vName;
		strInputBusName                = vBusName;
        
        List<CA> listCAs = new List<CA>();
        List<CAWrapper> listCAWraps = new List<CAWrapper>(); 
     
        if (string.isNotBlank(vMemberType) && IsValidMemberType(vMemberType) )
			query = getQuery(false);
        else
			query = getQueryAllMemberTypes();
        
        try {
            	List<sObject> sobjList = Database.query(query);           
                for(SObject sobj : sobjList){
                    CA newCA = new CA((Account)sobj, strSelectedMemberTypeOption);
                    
                    // R01 if search by Name use comparable wrapper class
                    if(String.isNotBlank(strInputName)){				// R01
                        CAWrapper newCAWrapper = new CAWrapper(newCA);	// R01
                        listCAWraps.add(newCAWrapper);					// R01
                    } else
                        // R02 add counter to check if we reached the limit
                        if (counter < outputLimit) {					// R02
                            listCAs.add(newCA);
                            counter += 1;								// R02
                        }												// R02
						else											// R02
							break;										// R02 
                }
            	// R01 start sort the CA Wrapper list and change it back to CA
                if(String.isNotBlank(strInputName)){					
                    listCAWraps.sort();
                    for (CAWrapper CAWrap : listCAWraps )
                    	//R02 start add counter to check if we reached the limit
                        if (counter < outputLimit) {					// R02
							listCAs.add((CA)CAWrap.res);
                            counter += 1;								// R02
                        }												// R02
						else											// R02
							break;										// R02
                } 
				// R01 end
                return listCAs;
		} 
		catch (exception e){
			return null;
		}
	}
    
    
    
    // Helper method that splits a string by spaces
    private static List<String> listCANames(String strParam) {
        return String.escapeSingleQuotes(strParam).split('\\W+');
    }
   
    
    // Constructs and returns a query string for All Member Types - Only Geography
    private static String getQueryAllMemberTypes(){
    	
    	String strQuery;
    	Integer index = 0;
    	List<String> listNames = new List<String>();
    	
		String strSelectedBranchCountryOptionSafe = String.escapeSingleQuotes(strSelectedBranchCountryOption);
		String strSelectedBranchOptionSafe = String.escapeSingleQuotes(strSelectedBranchOption);
        String strSelectedPostcodeOptionSafe = String.escapeSingleQuotes(strSelectedPostcodeOption);
		String strSelectedMemberTypeOptionSafe = String.escapeSingleQuotes(strSelectedMemberTypeOption);
		String strInputNameSafe = String.escapeSingleQuotes(strInputName);
    	String strInputBusNameSafe = String.escapeSingleQuotes(strInputBusName);

    	strQuery = 'SELECT Id, FirstName, Preferred_Name__c, Middle_Name__c, LastName, Display_Name__c, Primary_Employer_Company_Name__c, Affiliated_Branch__c, Affiliated_Branch_Country__c ';
    	strQuery += ', PersonOtherPhone, PersonEmail, Phone, Website, Primary_Employer__r.Website';
        strQuery += ', BV__c, SMSF__c, FP_Specialisation__c, Forensic_Accounting_Specialisation__c, Qualified_Auditor__c, QA_Date_of_suspension__c, Insolvency_Practitioner__c, NZAIP_Date_of_suspension__c ';
        strQuery += ', IsPersonAccount, Primary_Employer__c';
        strQuery += ', BillingStreet, BillingCity, BillingStateCode, BillingPostalCode';
        strQuery += ', Primary_Employer__r.BillingAddress, Primary_Employer__r.BillingStreet, Primary_Employer__r.BillingCity, Primary_Employer__r.BillingStateCode, Primary_Employer__r.BillingPostalCode';
        strQuery += ', Primary_Employer__r.BillingLatitude, Primary_Employer__r.BillingLongitude, Primary_Employer__r.BillingGeocodeAccuracy ';        
        strQuery += ', ShippingStreet, ShippingCity, ShippingStateCode, ShippingPostalCode';
        strQuery += ', Primary_Employer__r.ShippingAddress, Primary_Employer__r.ShippingStreet, Primary_Employer__r.ShippingCity, Primary_Employer__r.ShippingStateCode, Primary_Employer__r.ShippingPostalCode';
		strQuery += ', Primary_Employer__r.ShippingLatitude, Primary_Employer__r.ShippingLongitude, Primary_Employer__r.ShippingGeocodeAccuracy ';
        strQuery += ', QA_Conditions_imposed__c';
        strQuery += ', BV_Areas_of_Practice__c, Forensic_Accounting_Areas_of_Practice__c ';  //FP_Areas_of_Practice__c, SMSF_Areas_of_Practice__c ' 
        strQuery += ', BillingLatitude,	BillingLongitude,	BillingGeocodeAccuracy ';  
        strQuery += ', ShippingLatitude, ShippingLongitude,	ShippingGeocodeAccuracy ';  
              	
    	strQuery += ' FROM Account WHERE';
        strQuery += ' IsPersonAccount = true';
    	strQuery += ' AND Status__c = \'Active\'';
    	strQuery += ' AND Opt_out_of_Find_an_Accountant_register__c = false';
        // strQuery += ' AND Membership_Type__c IN (\'Member\', \'NZAIP\', \'NMP\')';
        // strQuery += ' AND (NOT Financial_Category__c LIKE \'%Retired%\')'
        
        // add All Member Types 
        strQuery += ' AND ( ';     
        strQuery += ' (CPP__c = true AND Find_CA_Opt_In__c = true) OR ';
		strQuery += ' (Qualified_Auditor__c = true AND QA_Date_of_suspension__c = null) OR ';
        strQuery += ' (Insolvency_Practitioner__c = true AND NZAIP_Date_of_suspension__c = null ) OR ';
		strQuery += ' (BV__c = true OR SMSF__c = true OR FP_Specialisation__c = true OR Forensic_Accounting_Specialisation__c = true ) ';   //R03
        strQuery += ' ) '; 
        
        // R02 check for attacks
        if(String.isBlank(strSelectedBranchCountryOption)){
			// stop processing, it is not coming from the interface
            strQuery += ' LIMIT = 0 ';
            return strQuery;
        }
            
        if(String.isNotBlank(strSelectedBranchCountryOptionSafe))
            strQuery += ' AND Affiliated_Branch_Country__c = \'' + strSelectedBranchCountryOptionSafe + '\'';
        if(String.isNotBlank(strSelectedBranchOptionSafe)){
        	if(strSelectedBranchCountryOptionSafe == 'Overseas' && strSelectedBranchOptionSafe == 'Other')
        		strQuery += ' AND Affiliated_Branch__c != \'UK\'';
        	else
        		strQuery += ' AND Affiliated_Branch__c = \'' + strSelectedBranchOptionSafe + '\'';
        }
        if(String.isNotBlank(strSelectedPostcodeOptionSafe))
            strQuery += ' AND Primary_Employer__r.BillingPostalCode LIKE \'' + strSelectedPostcodeOptionSafe + '%\' ';
               
        if(String.isNotBlank(strInputNameSafe)){
        	listNames = listCANames(strInputNameSafe);
        	if(listNames.size() > 0){
	        	strQuery += ' AND (';
	        	for(String strPart : listNames){
	        		index++;
	        		strQuery += ' FirstName LIKE \'%' + strPart + '%\'OR LastName LIKE \'%' + strPart + '%\' OR Display_Name__c LIKE \'%' + strPart + '%\'';
	        		if(index != listNames.size())
	        			strQuery += ' OR';
	        	}
	        	strQuery += ')';
        	}
        }
        
		if(String.isNotBlank(strInputBusNameSafe)){
			strQuery += ' AND (Primary_Employer_Company_Name__c LIKE \'%' + strInputBusNameSafe + '%\') ';
        }
        
        //System.debug(logginglevel.INFO, '##### strQuery: ' + strQuery);
        return strQuery;
    }
        
 
    
    // Constructs and returns a query string.
    private static String getQuery(Boolean bCount){
    	
    	String strQuery;
    	Integer index = 0;
    	List<String> listNames = new List<String>();
    	
		String strSelectedBranchCountryOptionSafe = String.escapeSingleQuotes(strSelectedBranchCountryOption);
		String strSelectedBranchOptionSafe = String.escapeSingleQuotes(strSelectedBranchOption);
        String strSelectedPostcodeOptionSafe = String.escapeSingleQuotes(strSelectedPostcodeOption);
		String strSelectedMemberTypeOptionSafe = String.escapeSingleQuotes(strSelectedMemberTypeOption);
		String strInputNameSafe = String.escapeSingleQuotes(strInputName);
        String strInputBusNameSafe = String.escapeSingleQuotes(strInputBusName);
    	
    	if(bCount){
    		strQuery = 'SELECT COUNT(Id) size';
    	}
    	else{	
			strQuery = 'SELECT Id, FirstName, Preferred_Name__c, Middle_Name__c, LastName, Display_Name__c, Primary_Employer_Company_Name__c, Affiliated_Branch__c, Affiliated_Branch_Country__c ';
    		strQuery += ', PersonOtherPhone, PersonEmail, Phone, Website, Primary_Employer__r.Website';
            strQuery += ', BV__c, SMSF__c, FP_Specialisation__c, Qualified_Auditor__c, Forensic_Accounting_Specialisation__c, QA_Date_of_suspension__c, Insolvency_Practitioner__c, NZAIP_Date_of_suspension__c ';            
            strQuery += ', IsPersonAccount, Primary_Employer__c';
            strQuery += ', BillingStreet, BillingCity, BillingStateCode, BillingPostalCode';
            strQuery += ', Primary_Employer__r.BillingAddress, Primary_Employer__r.BillingStreet, Primary_Employer__r.BillingCity, Primary_Employer__r.BillingStateCode, Primary_Employer__r.BillingPostalCode';
            strQuery += ', Primary_Employer__r.BillingLatitude, Primary_Employer__r.BillingLongitude, Primary_Employer__r.BillingGeocodeAccuracy ';
            strQuery += ', ShippingStreet, ShippingCity, ShippingStateCode, ShippingPostalCode';
            strQuery += ', Primary_Employer__r.ShippingAddress, Primary_Employer__r.ShippingStreet, Primary_Employer__r.ShippingCity, Primary_Employer__r.ShippingStateCode, Primary_Employer__r.ShippingPostalCode';
			strQuery += ', Primary_Employer__r.ShippingLatitude, Primary_Employer__r.ShippingLongitude, Primary_Employer__r.ShippingGeocodeAccuracy ';
            strQuery += ', QA_Conditions_imposed__c';
            strQuery += ', BV_Areas_of_Practice__c, Forensic_Accounting_Areas_of_Practice__c ';          // FP_Areas_of_Practice__c, SMSF_Areas_of_Practice__c ';
            strQuery += ', BillingLatitude,	BillingLongitude,	BillingGeocodeAccuracy ';  
            strQuery += ', ShippingLatitude, ShippingLongitude,	ShippingGeocodeAccuracy ';  

    	}
    		
    	strQuery += ' FROM Account WHERE';
    	strQuery += ' Status__c = \'Active\'';
    	strQuery += ' AND Opt_out_of_Find_an_Accountant_register__c = false';
        	
    	if(strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_QUALIFIED_AUDITORS){
	        strQuery += ' AND ((IsPersonAccount = true AND Membership_Type__c = \'Member\') OR IsPersonAccount = false)';
	        strQuery += ' AND Qualified_Auditor__c = true';
	        strQuery += ' AND QA_Date_of_suspension__c = null';
	        strQuery += ' AND QA_Date_of_de_recognition__c = null';
    	}
    	else if(strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_INSOLVENCY_PRACTICE){
    		strQuery += ' AND Membership_Type__c IN (\'Member\', \'NZAIP\', \'NMP\')';
	        strQuery += ' AND IsPersonAccount = true';
	        strQuery += ' AND Insolvency_Practitioner__c = true';
	        strQuery += ' AND NZAIP_Date_of_suspension__c = null';
	        strQuery += ' AND NZAIP_Date_of_de_recognition__c = null';
    	}
    	else if(strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_SMSF_SPECIALIST
            || strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_BUSINESS_VALUATION_SPECIALIST
            || strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_FINANCIAL_PLANNING_SPECIALIST
            || strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_FORENSIC_ACCOUNTING_SPECIALIST     // R03
        ){
        	if(Label.FIND_CA_OPT_IN.equalsIgnoreCase('true')) // if this feature is 'on' include opt in as a condition, otherwise ignore
        		strQuery += ' AND Find_CA_Opt_In__c = true';
        	strQuery += ' AND Membership_Type__c = \'Member\'';
	        strQuery += ' AND IsPersonAccount = true';
	        strQuery += ' AND (NOT Financial_Category__c LIKE \'%Retired%\')';
	        if(strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_SMSF_SPECIALIST)
	            strQuery += ' AND SMSF__c = true';
	        else if(strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_BUSINESS_VALUATION_SPECIALIST)
	            strQuery += ' AND BV__c = true';
	        else if(strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_FINANCIAL_PLANNING_SPECIALIST)
	            strQuery += ' AND FP_Specialisation__c = true';
	        else if(strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_FORENSIC_ACCOUNTING_SPECIALIST)  // R03
	            strQuery += ' AND Forensic_Accounting_Specialisation__c = true';                       // R03
            
        }
        else if(strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_CPP_HELD){
        	strQuery += ' AND Find_CA_Opt_In__c = true';
        	strQuery += ' AND Membership_Type__c = \'Member\'';
	        strQuery += ' AND IsPersonAccount = true';
	        strQuery += ' AND CPP__c = true';
        }
        
        // R02 check for attacks
        if(String.isBlank(strSelectedBranchCountryOption)){
			// stop processing, it is not coming from the interface
            strQuery += ' LIMIT = 0 ';
            return strQuery;
        }
        
    	if(String.isNotBlank(strSelectedBranchCountryOptionSafe))
            strQuery += ' AND Affiliated_Branch_Country__c = \'' + strSelectedBranchCountryOptionSafe + '\'';
        if(String.isNotBlank(strSelectedBranchOptionSafe)){
        	if(strSelectedBranchCountryOptionSafe == 'Overseas' && strSelectedBranchOptionSafe == 'Other')
        		strQuery += ' AND Affiliated_Branch__c != \'UK\'';
        	else
        		strQuery += ' AND Affiliated_Branch__c = \'' + strSelectedBranchOptionSafe + '\'';
        }
        
        if(String.isNotBlank(strSelectedPostcodeOptionSafe))
            strQuery += ' AND ( Primary_Employer__r.BillingPostalCode LIKE \'' + strSelectedPostcodeOptionSafe + '%\' OR BillingPostalCode LIKE \'' + strSelectedPostcodeOptionSafe + '%\')';
        
        
        if(String.isNotBlank(strInputNameSafe)){
        	listNames = listCANames(strInputNameSafe);
        	if(listNames.size() > 0){
	        	strQuery += ' AND (';
	        	for(String strPart : listNames){
	        		index++;
	        		strQuery += ' FirstName LIKE \'%' + strPart + '%\'OR LastName LIKE \'%' + strPart + '%\' OR Display_Name__c LIKE \'%' + strPart + '%\'';
	        		if(index != listNames.size())
	        			strQuery += ' OR';
	        	}
	        	strQuery += ')';
        	}
        }
		
		if(String.isNotBlank(strInputBusNameSafe)){
			strQuery += ' AND (Primary_Employer_Company_Name__c LIKE \'%' + strInputBusNameSafe + '%\') ';
        }
        
		System.debug(logginglevel.INFO, '##### strQuery: ' + strQuery);
        return strQuery;
    }
    
    
    
    // ====== INTERNAL CLASS ======     
    global class CA{
    	
    	//private Account act {get; set;}
    	private String strSelectedMemberType;
        private String strSelectedName;
        
        global String Name;
        global String First;
        global String Preferred;
        global String Middle;
        global String Last;
        global String Company;
        global String BusinessAddress;
        global String Phone;
        global String Email;
        global String CompanyWebsite;
        global String SpecialConditions;
        global String Specialisation;
        global String Specialties;
        global Integer Weight=0;

        global Double Latitude;
        global Double Longitude;
        global String GeocodeAccuracy;
        
		public CA(Account account, String strSelectedMemberTypeOption) {
			//act = account;
            strSelectedMemberType = ((strSelectedMemberTypeOption != 'undefined')? strSelectedMemberTypeOption : '') ;
            
            Name = account.Display_Name__c;
			First = account.FirstName;
			Preferred = account.Preferred_Name__c;
			Middle = account.Middle_Name__c;
			Last = account.LastName;
            
            if (String.isNotBlank(strInputName))
           	 Weight = getNameMatchWeight(strInputName, First, Last, Preferred);
            
            Company = account.Primary_Employer_Company_Name__c;
            BusinessAddress = getBusinessAddress(account); 
            
            Phone = ((strSelectedMemberType == STR_MEMBERTYPE_QUALIFIED_AUDITORS  && account.isPersonAccount == FALSE)? account.Phone : account.PersonOtherPhone) ;
            Email = account.PersonEmail;
            SpecialConditions = (string.isNotBlank(account.QA_Conditions_imposed__c) ? account.QA_Conditions_imposed__c : '') ;
            
            CompanyWebsite = getCompanyWebsite(account);
            Specialisation = getSpecialisation(account);
            Specialties    = getSpecialties(account);
            
            // for Qualified Auditors companies display business address
            if (strSelectedMemberType == STR_MEMBERTYPE_QUALIFIED_AUDITORS  && account.isPersonAccount == FALSE) {
            	Latitude        = (account.BillingLatitude!=null  ? account.BillingLatitude        : account.ShippingLatitude) ;
            	Longitude       = (account.BillingLatitude!=null  ? account.BillingLongitude       : account.ShippingLongitude) ;
            	GeocodeAccuracy = (account.BillingLatitude!=null  ? account.BillingGeocodeAccuracy : account.ShippingGeocodeAccuracy) ;
        	}
            else {
            	Latitude        = (account.Primary_Employer__r.BillingLatitude!=null  ? account.Primary_Employer__r.BillingLatitude        : account.Primary_Employer__r.ShippingLatitude) ;
            	Longitude       = (account.Primary_Employer__r.BillingLatitude!=null  ? account.Primary_Employer__r.BillingLongitude       : account.Primary_Employer__r.ShippingLongitude) ;
            	GeocodeAccuracy = (account.Primary_Employer__r.BillingLatitude!=null  ? account.Primary_Employer__r.BillingGeocodeAccuracy : account.Primary_Employer__r.ShippingGeocodeAccuracy) ;
        	}
		}
        
               
        public String getBusinessAddress(Account act){
                String strBusinessAddress = '';
                if(act.IsPersonAccount){
                    if(String.isNotBlank(act.Primary_Employer__c)){
                        if(String.isNotBlank(act.Primary_Employer__r.BillingCity)){
                            strBusinessAddress = capInitialLetters(act.Primary_Employer__r.BillingStreet);
                            strBusinessAddress += ', ' + capInitialLetters(act.Primary_Employer__r.BillingCity);
                            if(act.Affiliated_Branch_Country__c == 'Australia'){
                                if (string.isNotBlank(act.Primary_Employer__r.BillingStateCode))
                                	strBusinessAddress += ', ' + act.Primary_Employer__r.BillingStateCode.toUpperCase();
                            }
                            strBusinessAddress += ', ' + act.Primary_Employer__r.BillingPostalCode;
                        }
                        else if(String.isNotBlank(act.Primary_Employer__r.ShippingCity)){
                            strBusinessAddress = capInitialLetters(act.Primary_Employer__r.ShippingStreet);
                            strBusinessAddress += ', ' + capInitialLetters(act.Primary_Employer__r.ShippingCity);
                            if(act.Affiliated_Branch_Country__c == 'Australia')
                                strBusinessAddress += ', ' + act.Primary_Employer__r.ShippingStateCode.toUpperCase();
                            strBusinessAddress += ', ' + act.Primary_Employer__r.ShippingPostalCode;
                    	}
                    }
                }
                else{
                    if(String.isNotBlank(act.BillingCity)){
                        strBusinessAddress = capInitialLetters(act.BillingStreet);
                        strBusinessAddress += ', ' + capInitialLetters(act.BillingCity);
                        if(act.Affiliated_Branch_Country__c == 'Australia') {
                            if (string.isNotBlank(act.BillingStateCode))
                            	strBusinessAddress += ', ' + act.BillingStateCode.toUpperCase();
                        }
                        strBusinessAddress += ', ' + act.BillingPostalCode;
                    }
                    else if(String.isNotBlank(act.ShippingCity)){
                        strBusinessAddress = capInitialLetters(act.ShippingStreet);
                        strBusinessAddress += ', ' + capInitialLetters(act.ShippingCity);
                        if(act.Affiliated_Branch_Country__c == 'Australia')
                            strBusinessAddress += ', ' + act.ShippingStateCode.toUpperCase();
                        strBusinessAddress += ', ' + act.ShippingPostalCode;
                    }
                }
                return strBusinessAddress;
        }

        
        public String getCompanyWebsite(Account act){
            if(act.IsPersonAccount){
                if(string.isNotBlank(act.Primary_Employer__r.Website)){
                    if(!act.Primary_Employer__r.Website.startsWithignoreCase('http://') && !act.Primary_Employer__r.Website.startsWithignoreCase('https://')){
                        return 'http://' + act.Primary_Employer__r.Website;        
                    }
                }
                return act.Primary_Employer__r.Website;
            }
            else{
                if(string.isNotBlank(act.Website)){
                    if(!act.Website.startsWithignoreCase('http://') && !act.Website.startsWithignoreCase('https://')){
                        return 'http://' + act.Website;        
                    }
                }
                return act.Website;
            }
        }

        
        public String getSpecialisation(Account act) { 
       		String vListSpec = '';
           
            if (act.BV__c == true )
                vListSpec += (string.isNotBlank(vListSpec) ? ', Business Valuation Specialist' : 'Business Valuation Specialist') ;
            if (act.SMSF__c == true )
                vListSpec += (string.isNotBlank(vListSpec) ? ', SMSF Specialist' : ' SMSF Specialist') ;
            if (act.FP_Specialisation__c == true )
                vListSpec += (string.isNotBlank(vListSpec) ? ', Financial Planning Specialist' : 'Financial Planning Specialist') ;  
            if (act.Forensic_Accounting_Specialisation__c == true )                                                                      // R03
                vListSpec += (string.isNotBlank(vListSpec) ? ', Forensic Accounting Specialist' : 'Forensic Accounting Specialist') ;    // R03   
            if (act.Qualified_Auditor__c == true && act.QA_Date_of_suspension__c == null)
                vListSpec += (string.isNotBlank(vListSpec) ? ', Qualified Auditor' : ' Qualified Auditor') ; 
            if (act.Insolvency_Practitioner__c == true && act.NZAIP_Date_of_suspension__c == null)
                vListSpec += (string.isNotBlank(vListSpec) ? ', Insolvency Practitioner' : 'Insolvency Practitioner') ;
   			
            return vListSpec;   
        }
        
        public String getSpecialties(Account act) {     
            // return (string.isNotBlank(act.Accounting_Specialties__c) ? act.Accounting_Specialties__c : null) ;
            String strOutput = '';
            Boolean first = true;
            
            // Only one of the below 3 options can happen at a time
            
            // BV Areas of Practice for BV Searches
            // removed need for selection: if (String.isNotBlank(act.BV_Areas_of_Practice__c) && strSelectedMemberType == STR_MEMBERTYPE_BUSINESS_VALUATION_SPECIALIST ) {
            if (String.isNotBlank(act.BV_Areas_of_Practice__c) && act.BV__c == true && strSelectedMemberType == STR_MEMBERTYPE_BUSINESS_VALUATION_SPECIALIST  ) {
                for (String entry : act.BV_Areas_of_Practice__c.split(';') ) {
                    if (first) {
                        strOutput += entry;
                        first = false;
                    }
                    else
                        strOutput += ', ' + entry;
                }
            }

            // R03 Forensic Areas of Practice for Forensic Searches
            if (String.isNotBlank(act.Forensic_Accounting_Areas_of_Practice__c) && act.Forensic_Accounting_Specialisation__c == true  && strSelectedMemberType == STR_MEMBERTYPE_FORENSIC_ACCOUNTING_SPECIALIST ) {
                for (String entry : act.Forensic_Accounting_Areas_of_Practice__c.split(';') ) {
                    if (first) {
                        strOutput += entry;
                        first = false;
                    }
                    else
                        strOutput += ', ' + entry;
                }
            }         
            
            
            
            
/**            
            // FP Areas of Practice for FP Searches
            if (String.isNotBlank(act.FP_Areas_of_Practice__c) && strSelectedMemberType == STR_MEMBERTYPE_FINANCIAL_PLANNING_SPECIALIST ) {
                for (String entry : act.FP_Areas_of_Practice__c.split(';') ) {
                    if (first) {
                        strOutput += entry;
                        first = false;
                    }
                    else
                        strOutput += ', ' + entry;
                }
            }
            
            // SMSF Areas of Practice for SMSF Searches 
            if (String.isNotBlank(act.SMSF_Areas_of_Practice__c) && strSelectedMemberType == STR_MEMBERTYPE_SMSF_SPECIALIST ) {
                for (String entry : act.SMSF_Areas_of_Practice__c.split(';') ) {
                    if (first) {
                        strOutput += entry;
                        first = false;
                    }
                    else
                        strOutput += ', ' + entry;
                }
            }
**/            
            return strOutput;
        }
        
    private String capInitialLetters(String strInput){
            String strOutput = '';
            //for(String str : strInput.split('\\W+')){
            for(String str : strInput.split('\\s')){
                strOutput += str.toLowerCase().capitalize() + ' ';
            }
            return strOutput.substring(0, strOutput.length() - 1);
        }
        
    // R01 calculate weight on matched elements
	private Integer getNameMatchWeight(String strInputName, String First, String Last, String Preferred){  
        Integer score = 0;
		List<String> words = new List<String>();
        words = strInputName.split('\\W+');
		if(words.size() > 0){
			for(String word : words){
                if (First.containsIgnoreCase(word))
                    score += 10;
                if (Preferred.containsIgnoreCase(word))
                    score += 10;
                if (Last.containsIgnoreCase(word)) {
                    score += 30;
                    if (Last.equalsIgnoreCase(word) )
						score += 10;                    
                }
			}
        }
   		return score;
	}
        
}
    
    
    global class CAWrapper implements Comparable {

    public CA res;
    
    // Constructor
    public CAWrapper(CA obj) {
        res = obj;
    }
    // Compare results based on the opportunity amount.
    global Integer compareTo(Object compareTo) {
        // Cast argument to CAWrapper
        CAWrapper compareToCA = (CAWrapper)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (res.Weight > compareToCA.res.Weight) {
            // Set return value to a positive value.
            returnValue = -1;
        } else if (res.Weight < compareToCA.res.Weight) {
            // Set return value to a negative value.
            returnValue = 1;
        }
        return returnValue;       
    }
}
}