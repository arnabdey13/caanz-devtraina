/*
    Developer: WDCi (Lean)
    Date: 07/June/2016
    Task #: Test class for luana_MyVirtualClassNewController
    
    Change History:
    LCA-553/LCA-901 10/08/2016 - WDCi Lean: remove noncommunity check
    LCA-921 23/08/2016 WDCi KH: Add person account email
*/

@isTest
private class luana_AT_MyVirtualClassNew_Test {

    final static String contextShortName = 'mvcnt';
    final static String contextLongName = 'luana_AT_MyVirtualClassNew_Test';
    
    //TODO - Change to private where applicable
    static Luana_DataPrep_Test dataPrep;
    static List<LuanaSMS__Luana_Configuration__c> luanaSMSConfig;
    static List<Luana_Extension_Settings__c> luanaExtensionConfig;
    static LuanaSMS__Training_Organisation__c trainingOrg;
    static LuanaSMS__Program__c program;
    static LuanaSMS__Program_Offering__c poAM;
    static LuanaSMS__Program_Offering__c poAP;
    static List<LuanaSMS__Subject__c> subjList;
    static List<LuanaSMS__Program_Offering_Subject__c> posList;
    static List<LuanaSMS__Program_Offering_Subject__c> posAMList;
    static LuanaSMS__Course__c courseAP;
    static LuanaSMS__Course__c courseAM;
    static LuanaSMS__Delivery_Location__c devLocation;
    static Course_Delivery_Location__c courseDevLoc;
    static Account venueAccount;
    static LuanaSMS__Room__c room1;
    static LuanaSMS__Session__c session1;
    static LuanaSMS__Session__c session2;
    static List<Account> memberAccounts;
    static List<LuanaSMS__Student_Program__c> newStudentPrograms;
    
    static LuanaSMS__Course_Session__c coursesession1;
    static LuanaSMS__Course_Session__c coursesession2;
    
    static User admin;
    
    //Used to initialise all the variables
    static void setup(String runSeqKey){
         
        dataPrep = new Luana_DataPrep_Test();
        Profile adminProfile = dataPrep.getProfileByName('System Administrator');
        admin = dataPrep.newInternalUser(contextShortName, contextShortName + 'admin', adminProfile.Id);
        insert admin;
        
        luanaSMSConfig = dataPrep.createLuanaConfigurationCustomSetting();
        insert luanaSMSConfig;
        
        luanaExtensionConfig = dataPrep.prepLuanaExtensionSettingCustomSettings();
        luanaExtensionConfig.add(new Luana_Extension_Settings__c(Name = 'Exam Allocation Report Params', Value__c = '000000000000000'));
        insert luanaExtensionConfig;
        
        trainingOrg = dataPrep.createNewTraningOrg(contextShortName, contextShortName + 'my org', contextShortName + 'myorg', 'test street', 'test location', '5400');
        insert trainingOrg;
        
        program = dataPrep.createNewProgram(contextShortName, contextLongName, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        insert program;
        
        poAM = dataPrep.createNewProgOffering('PO_AM_' + contextShortName, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), program.Id, trainingOrg.Id, null, null, null, 1, 1);
        insert poAM;
        
        poAP = dataPrep.createNewProgOffering('PO_AP_' + contextShortName, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Program'), program.Id, trainingOrg.Id, null, null, null, 1, 1);
        insert poAP;
        
        //Create multiple subjects
        subjList = new List<LuanaSMS__Subject__c>();
        subjList.add(dataPrep.createNewSubject('TAX AU_' + contextShortName, 'TAX AU_' + contextShortName, 'TAX AU_' + contextShortName, 1, 'Module'));
        subjList.add(dataPrep.createNewSubject('TAX NZ_' + contextShortName, 'TAX NZ_' + contextShortName, 'TAX NZ_' + contextShortName, 1, 'Module'));
        subjList.add(dataPrep.createNewSubject('AAA_' + contextShortName, 'AAA_' + contextShortName, 'AAA_' + contextShortName, 1, 'Module'));
        subjList.add(dataPrep.createNewSubject('Cap_' + contextShortName, 'Capstone_' + contextShortName, 'Cap_' + contextShortName, 1, 'Module'));
        subjList.add(dataPrep.createNewSubject('FIN_' + contextShortName, 'FIN_' + contextShortName, 'FIN_' + contextShortName, 55, 'Module'));
        subjList.add(dataPrep.createNewSubject('MAAF_' + contextShortName, 'MAAF_' + contextShortName, 'MAAF_' + contextShortName, 60, 'Module'));
        insert subjList;
        
        //Create multiple Program Offering Subject
        posList = new List<LuanaSMS__Program_Offering_Subject__c>();
        for(LuanaSMS__Subject__c sub: subjList){
            if(sub.name =='TAX AU_' + contextShortName || sub.name =='TAX NZ_' + contextShortName){
                posList.add(dataPrep.createNewProgOffSubject(poAP.Id, sub.Id, 'Elective'));
            }else{
                posList.add(dataPrep.createNewProgOffSubject(poAP.Id, sub.Id, 'Core'));
            }
        }
        insert posList;
                
        posAMList = new List<LuanaSMS__Program_Offering_Subject__c>();
        for(LuanaSMS__Subject__c sub: subjList){
            if(sub.name =='TAX AU_' + contextShortName || sub.name =='TAX NZ_' + contextShortName){
                posAMList.add(dataPrep.createNewProgOffSubject(poAM.Id, sub.Id, 'Elective'));
            }else{
                posAMList.add(dataPrep.createNewProgOffSubject(poAM.Id, sub.Id, 'Core'));
            }
        }
        insert posAMList;
        
        //Create course
        courseAP = dataPrep.createNewCourse('Graduate Diploma of Chartered Accounting_' + contextShortName, poAP.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Program'), 'Running');
        courseAP.LuanaSMS__Allow_Online_Enrolment__c = true;
        
        courseAM = dataPrep.createNewCourse('AAA216', poAM.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        courseAM.LuanaSMS__Allow_Online_Enrolment__c = true;
        
        insert new List<LuanaSMS__Course__c>{courseAP, courseAM};
        
        devLocation = dataPrep.createNewDeliveryLocationRecord('Australia - South Australia - Adelaide_' + contextShortName, 'Australia - South Australia - Adelaide_' + contextShortName, trainingOrg.Id, '1000', 'Australia');
        insert devLocation;
        
        courseDevLoc = dataPrep.createCourseDeliveryLocation(courseAM.Id, devLocation.Id, 'Exam Location');
        insert courseDevLoc;
        
        venueAccount = dataPrep.generateNewBusinessAcc(contextShortName + 'Test Venue'+'_'+runSeqKey, 'Venue', null, null, null, null, 'Sunshine Coast Office', 'Active');
        venueAccount.Ext_Id__c = contextShortName +'_'+runSeqKey;
        insert venueAccount;
        
        room1 = dataPrep.newRoom(contextShortName, contextLongName, venueAccount.Id, 20, 'Classroom', runSeqKey);
        insert room1;
        
        session1 = dataPrep.newSession(contextShortName, contextLongName, 'Virtual', 20, venueAccount.Id, room1.Id, null, system.now().addHours(1), system.now().addHours(2)); //e.g, 1pm - 2pm
        session1.Topic_Key__c = contextShortName + '_T1';
        
        session2 = dataPrep.newSession(contextShortName, contextLongName, 'Virtual', 20, venueAccount.Id, room1.Id, null, system.now().addHours(3), system.now().addHours(4)); //e.g, 3pm - 4pm        
        session2.Topic_Key__c = contextShortName + '_T1';
        insert new List<LuanaSMS__Session__c>{session1, session2};
        
        coursesession1 = dataPrep.createCourseSession(courseAM.Id, session1.Id);
        coursesession2 = dataPrep.createCourseSession(courseAM.Id, session2.Id);
        
        insert new List<LuanaSMS__Course_Session__c>{coursesession1, coursesession2};
        
        memberAccounts = new List<Account>();
        Account memberAccount = dataPrep.generateNewApplicantAcc('Joe_' + contextShortName, contextLongName, 'Full_Member');
        memberAccount.Member_Id__c = '9999991';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.PersonEmail = contextShortName + '_joe9999991@example.com.donotsend';
        memberAccounts.add(memberAccount);
        memberAccount.Communication_Preference__c= 'Home Phone';
        memberAccount.PersonHomePhone= '1234';
        memberAccount.PersonOtherStreet= '83 Saggers Road';
        memberAccount.PersonOtherCity='JITARNING';
        memberAccount.PersonOtherState='Western Australia';
        memberAccount.PersonOtherCountry='Australia';
        memberAccount.PersonOtherPostalCode='6365';  

        
        insert memberAccounts;
        
        
    }
    
    /*
        success vc registration
    */
    static testMethod void testSuccessVCRegistration() {
        
        Test.startTest();
        setup('1');
        
        Test.stopTest();
        
        User memberUser = new User();
        
         for(User usr: [select id,ContactId,Username, LastName, Email, Alias, CommunityNickname, TimeZoneSidKey, LocaleSidKey, EmailEncodingKey, ProfileId, LanguageLocaleKey  from User where isActive=true and ContactId != null limit 1]){
            system.debug('user :: ' + usr);
            memberUser = usr;
        }
        
        system.runAs(admin){
            String permissionSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id').Value__c;
            PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = permissionSetId, AssigneeId = memberUser.Id);
            list<PermissionSetAssignment> permissAssign = [Select Id from PermissionSetAssignment where AssigneeId =: memberUser.Id AND PermissionSetId =: permissionSetId];
            if(permissAssign == null)
                insert psa;
        }
        
        newStudentPrograms = new List<LuanaSMS__Student_Program__c>();
        Id studProgAccModuleId = dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module');
        Account BusinessAccountObject = TestObjectCreator.createBusinessAccount();
       
        insert BusinessAccountObject;
        
        Contact ContactObject = TestObjectCreator.createContact();
        
        ContactObject.AccountId = BusinessAccountObject.Id; // portal account owner must have a role
      
        insert ContactObject;
        
        LuanaSMS__Student_Program__c newStudProg = dataPrep.createNewStudentProgram(studProgAccModuleId, memberUser.ContactId, courseAM.Id, 'Australia', 'In Progress');
        newStudProg.LuanaSMS__Auto_Student_Program_Subject_Creation__c = true;
        newStudProg.Exam_Location__c = courseDevLoc.Id;
        newStudProg.LuanaSMS__Contact_Student__c=ContactObject.Id;
        newStudentPrograms.add(newStudProg);
        
        insert newStudentPrograms;
        
        system.runAs(memberUser){
            
            PageReference pageRef = Page.luana_MyVirtualClassNew;
            pageRef.getParameters().put('spid', newStudProg.id);
            
            Test.setCurrentPage(pageRef);
            
            luana_MyVirtualClassNewController mvcnt_e = new luana_MyVirtualClassNewController();
            
            mvcnt_e.getDomainUrl();
            mvcnt_e.getIsMemberCommunity();
            //LCA-901 mvcnt_e.getIsNonMemberCommunity();
            mvcnt_e.getIsInternal();
            mvcnt_e.getTemplateName();
            mvcnt_e.doCancel();
            
            mvcnt_e.selectedSession = session1.Id;
            
            mvcnt_e.doSave();
            
        }
        
        List<LuanaSMS__Attendance2__c> createdAttendances = [select id from LuanaSMS__Attendance2__c where LuanaSMS__Contact_Student__c =: ContactObject.Id];
        system.assert(0 == createdAttendances.size(), true);
                
    }
    
    /*
        duplicate vc registration
    */
    static testMethod void testDuplicateVCRegistration() {
        
        Test.startTest();
        setup('2');
        
        Test.stopTest();
        
       User memberUser = new User();
        
       
        for(User usr: [select id,ContactId,Username, LastName, Email, Alias, CommunityNickname, TimeZoneSidKey, LocaleSidKey, EmailEncodingKey, ProfileId, LanguageLocaleKey  from User where isActive=true and ContactId != null limit 1]){
            system.debug('user :: ' + usr);
            memberUser = usr;
        }
     
        system.runAs(admin){
            String permissionSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id').Value__c;
            PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = permissionSetId, AssigneeId = memberUser.Id);
            list<PermissionSetAssignment> permissAssign = [Select Id from PermissionSetAssignment where AssigneeId =: memberUser.Id AND PermissionSetId =: permissionSetId];
            if(permissAssign == null)
                insert psa;
        }
        
        newStudentPrograms = new List<LuanaSMS__Student_Program__c>();
        Id studProgAccModuleId = dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module');
       Account BusinessAccountObject = TestObjectCreator.createBusinessAccount();
       
       
        insert BusinessAccountObject;
        
        Contact ContactObject = TestObjectCreator.createContact();
       
        ContactObject.AccountId = BusinessAccountObject.Id; // portal account owner must have a role
       
        insert ContactObject;
        
        LuanaSMS__Student_Program__c newStudProg = dataPrep.createNewStudentProgram(studProgAccModuleId, memberUser.ContactId, courseAM.Id, 'Australia', 'In Progress');
        newStudProg.LuanaSMS__Auto_Student_Program_Subject_Creation__c = true;
        newStudProg.Exam_Location__c = courseDevLoc.Id;
        newStudProg.LuanaSMS__Contact_Student__c=ContactObject.Id;
        newStudentPrograms.add(newStudProg);
        
        insert newStudentPrograms;
        
        LuanaSMS__Attendance2__c existingAttendance = new LuanaSMS__Attendance2__c();
        existingAttendance.LuanaSMS__Student_Program__c = newStudProg.Id;
        existingAttendance.LuanaSMS__Session__c = session1.Id;
        existingAttendance.LuanaSMS__Contact_Student__c = ContactObject.Id;
        existingAttendance.LuanaSMS__Type__c = 'Student';
        
        insert existingAttendance;
            
        system.runAs(memberUser){
            PageReference pageRef = Page.luana_MyVirtualClassNew;
            pageRef.getParameters().put('spid', newStudProg.id);
            
            Test.setCurrentPage(pageRef);
            
            luana_MyVirtualClassNewController mvcnt_e = new luana_MyVirtualClassNewController();
            
            mvcnt_e.getDomainUrl();
            mvcnt_e.getIsMemberCommunity();
            //LCA-901 mvcnt_e.getIsNonMemberCommunity();
            mvcnt_e.getIsInternal();
            mvcnt_e.getTemplateName();
            mvcnt_e.doCancel();
            
            mvcnt_e.selectedSession = session2.Id;
            
            mvcnt_e.doSave();
            
        }
       
        List<LuanaSMS__Attendance2__c> createdAttendances = [select id from LuanaSMS__Attendance2__c where LuanaSMS__Contact_Student__c =: ContactObject.Id];
        system.assert(createdAttendances.size()>0, true);
       
    }
    
    /*
        transfer vc registration
    */
    static testMethod void testTransferVCRegistration() {
        
        Test.startTest();
        setup('3');
        
        Test.stopTest();
        
        User memberUser = new User();
        
        for(User usr: [select id,ContactId,Username,UserType,LastName, Email, Alias, CommunityNickname, TimeZoneSidKey, LocaleSidKey, EmailEncodingKey, ProfileId, LanguageLocaleKey  from User where isActive=true and ContactId != null and UserType='CSPLitePortal'  limit 1 ]){
            system.debug('user :: ' + usr);
            memberUser = usr;
        }
        
        system.runAs(admin){
            String permissionSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id').Value__c;
            PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = permissionSetId, AssigneeId = memberUser.Id);
            list<PermissionSetAssignment> permissAssign = [Select Id from PermissionSetAssignment where AssigneeId =: memberUser.Id AND PermissionSetId =: permissionSetId];
            if(permissAssign == null)
                insert psa;
        }
         Account BusinessAccountObject = TestObjectCreator.createBusinessAccount();
        
       
        insert BusinessAccountObject;
        
        Contact ContactObject = TestObjectCreator.createContact();
        
        ContactObject.AccountId = BusinessAccountObject.Id; // portal account owner must have a role
       
        insert ContactObject;
        
        newStudentPrograms = new List<LuanaSMS__Student_Program__c>();
        Id studProgAccModuleId = dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module');
        LuanaSMS__Student_Program__c newStudProg = dataPrep.createNewStudentProgram(studProgAccModuleId, memberUser.ContactId, courseAM.Id, 'Australia', 'In Progress');
        newStudProg.LuanaSMS__Auto_Student_Program_Subject_Creation__c = true;
        newStudProg.Exam_Location__c = courseDevLoc.Id;
        newStudProg.LuanaSMS__Contact_Student__c=ContactObject.Id;
        newStudentPrograms.add(newStudProg);
        
        insert newStudentPrograms;
        
       
 
        LuanaSMS__Attendance2__c existingAttendance = new LuanaSMS__Attendance2__c();
        existingAttendance.LuanaSMS__Student_Program__c = newStudProg.Id;
        existingAttendance.LuanaSMS__Session__c = session1.Id;
        existingAttendance.LuanaSMS__Contact_Student__c = memberUser.ContactId;
        existingAttendance.LuanaSMS__Type__c = 'Student';
        
        insert existingAttendance;
        
        PageReference pageRef = Page.luana_MyVirtualClassNew;
            pageRef.getParameters().put('spid', newStudProg.id);
            pageRef.getParameters().put('cancelattendanceid', existingAttendance.id);
            
            Test.setCurrentPage(pageRef);
            
         system.runAs(memberUser){
        
            
            
            luana_MyVirtualClassNewController mvcnt_e = new luana_MyVirtualClassNewController();
            
            mvcnt_e.getDomainUrl();
            mvcnt_e.getIsMemberCommunity();
            //LCA-901 mvcnt_e.getIsNonMemberCommunity();
            mvcnt_e.getIsInternal();
            mvcnt_e.getTemplateName();
            mvcnt_e.doCancel();
            
            mvcnt_e.selectedSession = session2.Id;
            
            mvcnt_e.doSave();
            
            
            
        }
        
        List<LuanaSMS__Attendance2__c> createdAttendances = [select id from LuanaSMS__Attendance2__c where LuanaSMS__Contact_Student__c =: memberUser.ContactId];
        system.assert(1 == createdAttendances.size(), 'There should be 1 virtual attendance');
    }
}