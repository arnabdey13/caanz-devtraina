public with sharing class AppFormsRecordTypeController {
    
    @AuraEnabled
    public static Map<String,List<Application_Categories__mdt>> getApplicationTypes(String communityName) {
        //public static Map<String,List<Application_Categories__mdt>> getApplicationTypes() {
        
        set<String> recTyps = new set<String>{'Provisional_Member','IPP_Provisional_Member','Special_Admissions'};
        List<AggregateResult> draftapplications = [Select count(Id),RecordType.DeveloperName rc FROM Application__c WHERE RecordType.DeveloperName IN:recTyps
                                                       AND Application_Status__c IN ('Draft') group by RecordType.DeveloperName];
        
        Set<String> existingDraft = new Set<String>();
        if(draftapplications!=null && draftapplications.size()>0){
            
            for(AggregateResult res:draftapplications) {                
                existingDraft.add(String.valueOf(res.get('rc')));            
            }
        }
        
        
        List<Application_Categories__mdt> items = [Select Id,MasterLabel,DeveloperName,Sub_Category__c,Parent_Category__c,
                                                   Landing_URL__c,MyCA__c,Customer__c
                                                   FROM Application_Categories__mdt  where (DeveloperName NOT IN:existingDraft)];
        
        List<SortingWrapper> appsList = new List<SortingWrapper>();
        
        for(Application_Categories__mdt app:items){
            appsList.add(new SortingWrapper(app));
        }
        
        appsList.sort();
        
        Map<String,List<Application_Categories__mdt>> categoryMap = new Map<String,List<Application_Categories__mdt>>();
        List<Application_Categories__mdt> subcateg;
        
        String myCA = System.Label.Community_Prefix_CCH;
        String customer = System.Label.Community_Prefix;
        
        for(SortingWrapper scateg:appsList){
            Application_Categories__mdt categ = scateg.appSubCateg;
            
            if(communityName == myCA){
                if(categ.MyCA__c){                  
                    if(categoryMap.containsKey(categ.Parent_Category__c)) {
                        subcateg = categoryMap.get(categ.Parent_Category__c); 
                        subcateg.add(categ);
                    }
                    else {
                        subcateg = new List<Application_Categories__mdt>();
                        subcateg.add(categ);
                    }
                    categoryMap.put(String.valueOf(categ.Parent_Category__c),subcateg);
                }
            }
            
            if(communityName == customer){
               if(categ.Customer__c){
                    if(categoryMap.containsKey(categ.Parent_Category__c)) {
                        subcateg = categoryMap.get(categ.Parent_Category__c); 
                        subcateg.add(categ);
                    }
                    else {
                        subcateg = new List<Application_Categories__mdt>();
                        subcateg.add(categ);
                    }
                    categoryMap.put(String.valueOf(categ.Parent_Category__c),subcateg);
                }
                
            }
        }
        
        return categoryMap;
    }
}