/*
    Developer: WDCi (Leo)
    Date: 15/02/2017
    Task#: CASM Bulk Enrolment Wizard - controller class for the Wizard page
*/
public class luana_BulkEnrolmentController {

    public Map<String, Luana_Extension_Settings__c> luanaConfigs;
    public String custCommCountry;
    public luana_CommUserUtil commUserUtil;

    private Boolean debug = true;
    private static Set<String> validStatusForEmployee = new Set<String>{'In Progress'};
    private static String validMainCourseStatus = 'Running';
    private static String studentProgramDefaultStatus = 'In Progress';
    private static String employmentTokenName = 'Bulk Employment Token - ' + String.valueOf(system.today());
    private static Set<String> statusForExistingSPToConsider = new Set<String>{'In Progress'};
    
    public Boolean error {get;set;}
    private Map<String, String> urlParameterMap;    
    
    private Integer listSize=100;
    private String queryString;       
        
    // Dynamic Title and Descriptions   
    public String VFFooter {get;set;}
    public String headerTitle {get;set;}
    public String headerDescription {get;set;}
    public String headerDescriptionPage1 {get;set;}
    public String headerDescriptionPage2 {get;set;}
    public String headerDescriptionPage3 {get;set;}
    public String headerDescriptionPage3New {get;set;}
    public String headerDescriptionPage4 {get;set;}
    public String headerDescriptionPage5 {get;set;}
    public String searchNote1 {get;set;}
    public String searchNote2 {get;set;}
    public String autoAttendanceHelpText {get;set;}
    
    public LuanaSMS__Course__c activeSubCourse {get;set;}
    public List<SelectOption> mainCourseOptions {get;set;}
    public Id mainCourseID {get;set;}
    
    // various record type ID that will be used in the logic for account (business and personal), as well as course record types
    Id companyRecordTypeID;
    Id employeeRecordTypeID;
    Id accreditedModuleRecordTypeID;
    Id casmRecordTypeID;    

    private Map<Id, Id> studentContactToMainCourseSPMap;

    public String companyNameSearch {get;set;}
    public String companyCustomerIDSearch {get;set;}
    public String companyMemberIDSearch {get;set;}    
    public List<SelectOption> companyAccountOptions {get;set;}
    public List<Account> companyAccountList {get;set;}    
    public String selectedCompanyAccount {get;set;}
    public Account selectedCompany {get;set;}
    public Boolean companySearchNoResult {get;set;}

    public String employeeFirstNameSearch {get;set;}
    public String employeeLastNameSearch {get;set;}
    public String employeeMemberIDSearch {get;set;}
    public String employeeCustomerIDSearch {get;set;}
    public String employeeEmailSearch {get;set;}
    
    private List<Account> employeePAList;
    public Map<Id, wrappedObject> employeeSearchWrapperMap;
    private Map<Id, String> personContactIdToAffiliatedBranchCountryMap;
    public List<wrappedObject> woToCreate {get;set;}
    
    public String currencyKey;
    
    public List<LuanaSMS__Student_Program__c> spToCreate {get;set;}
    
    private Map<Id, Employment_Token__c> employmentTokenMap {get;set;}
    public List<SelectOption> employmentTokenOptions {get;set;}
    public String selectedEmploymentToken {get;set;}
    public Boolean useNewEmploymentToken {get;set;}
    
    // variables for creating new Employment Token
    public Employment_Token__c newEmploymentToken {get;set;}
    public String employmentTokenCode {get;set;}
        
    // checkbox to govern if the Welcome Email will be sent for all the generated Student Program
    public Boolean welcomeEmailToBeSent {get;set;}
    
    // variable to determine if the Welcome Email checkbox can be selected or not
    public Boolean disableWelcomeEmailCheckbox {get;set;}
    
    // checkbox to govern if the Auto Attendance will be checked for the newly generated Student Program
    public Boolean autoAttendance {get;set;}
    
    // Partial Success variables
    public Integer successCount {get;set;}
    public Integer errorCount {get;set;}
    private Map<LuanaSMS__Student_Program__c, String> errorMessageMap;    
    
    public Boolean enableDone {get;set;} 
    
    // Constructors //
    // ============ //        
    public luana_BulkEnrolmentController() {        
        initialize();
        
    }
    
    public ApexPages.StandardSetController con {        
        get {
            if (con == null) {                
                searchEmployee();
            }
            if (debug) system.debug('standard set controller con: '+con);          
            return con;
        }
        set;        
    }
    
    // Basic Methods to construct and initialize //
    // ========================================= //    
    public class wrappedObject {
        // public LuanaSMS__Student_Program__c storedObject {get;set;}
        public Account storedObject {get;set;}
        public Boolean selected {get;set;}
                
        public wrappedObject(Account pa) {
            storedObject = pa;
            selected = false;
        }
    }
        
    private void initialize() {
                        
        urlParameterMap = ApexPages.currentPage().getParameters();
        
        if ((urlParameterMap.containsKey('id') && !(String.isBlank(urlParameterMap.get('id')))) || activeSubCourse != NULL) {
            
            luanaConfigs = Luana_Extension_Settings__c.getAll();
            /**
            commUserUtil = new luana_CommUserUtil(UserInfo.getUserId());
            custCommCountry = commUserUtil.custCommCountry;
            
            if (debug) {
                system.debug('commUserUtil: ' +commUserUtil);
                system.debug('custCommCountry: ' +custCommCountry);
            }
            **/
            
            error = FALSE;
            companySearchNoResult = FALSE;
            
            // Populating record type ID for Account
            companyRecordTypeID = recordTypeRetriever(Account.SObjectType, 'Business Account');
            employeeRecordTypeID = recordTypeRetriever(Account.SObjectType, 'Member');
            
            if (companyRecordTypeID == NULL || employeeRecordTypeID == NULL) {
                error = TRUE;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error while retrieving Account record types'));
            }
            
            // Populating record type ID for Course
            // Map<String, Id> courseRecordTypeMap = recordTypeMapper('LuanaSMS__Course__c');            
            accreditedModuleRecordTypeID = recordTypeRetriever(LuanaSMS__Course__c.SObjectType, 'Accredited Module');
            casmRecordTypeID = recordTypeRetriever(LuanaSMS__Course__c.SObjectType, 'CASM');
            
            if (accreditedModuleRecordTypeID == NULL || casmRecordTypeID == NULL) {
                error = TRUE;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error while retrieving Course record types'));
            }
            
            if (activeSubCourse == NULL) activeSubCourse = [SELECT Id, Name, LuanaSMS__Program_Offering__c, Visibility__c, Number_of_active_and_paid_students__c, Maximum_Enrolment__c from LuanaSMS__Course__c WHERE Id = :urlParameterMap.get('id') LIMIT 1];
            List<Program_Offering_Related__c> porLinks = [SELECT Id, Child_Program_Offering__c, Parent_Program_Offering__c, Type__c FROM Program_Offering_Related__c WHERE Child_Program_Offering__c = :activeSubCourse.LuanaSMS__Program_Offering__c];
            
            // Generate the list of valid Main Course to select from, sorted by Start Date in descending fashion
            // valid Main Course satisfy the following criteria
            // 1. RecordType = 'Accredited Module'
            // 2. the Main Course is linked with the Sub Course (the course in which the wizard is started) via program offering related record            
            List<LuanaSMS__Course__c> mainCourseList = new List<LuanaSMS__Course__c>();
            if (porLinks != NULL && !(porLinks.isEmpty())) {
                mainCourseList = [SELECT Id, Name, LuanaSMS__Start_Date__c FROM LuanaSMS__Course__c WHERE LuanaSMS__Program_Offering__c = :porLinks[0].Parent_Program_Offering__c AND LuanaSMS__Status__c = :validMainCourseStatus AND RecordTypeID = :accreditedModuleRecordTypeID ORDER BY Name DESC];
            } else {
                error = true;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'No Program Offering Related record found for the course'));
            }                        
            
            if (mainCourseList .isEmpty()) {
                error = TRUE;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'No Main Course matching the required criteria found'));
            }
            
            if (debug) {
                system.debug('activeSubCourse: ' +activeSubCourse);
                system.debug('porLinks: ' +porLinks);
                system.debug('mainCourseList: ' +mainCourseList);
            }
            
            // Populating the valid main courses into the list
            mainCourseOptions = new List<SelectOption>();
            for (LuanaSMS__Course__c mainCourse : mainCourseList) {
                SelectOption newCourseOption = new SelectOption(mainCourse.Id, mainCourse.Name);                
                mainCourseOptions.add(newCourseOption);
            }
            
            // Check for the CASM Visibility
            // if it's type public then show the checkbox for send email in the 4th page
            // if it's type private then disable it
            // if it's type empty/null then default to allow the checkbox in the 4th page
            if (activeSubCourse.Visibility__c == 'Public') {
                disableWelcomeEmailCheckbox = false;
            } else if (activeSubCourse.Visibility__c == 'Private') {
                disableWelcomeEmailCheckbox = true;
            } else {
                disableWelcomeEmailCheckbox = false; 
            }
            
            if (debug) system.debug('disableWelcomeEmailCheckbox: ' + disableWelcomeEmailCheckbox);

            // Initializing variables            
            studentContactToMainCourseSPMap = new Map<Id, Id>();
            employeeSearchWrapperMap = new Map<Id, wrappedObject>();            
            spToCreate = new List<LuanaSMS__Student_Program__c>();
            companyNameSearch = '';
            companyCustomerIDSearch = '';
            companyMemberIDSearch = '';
            companyAccountOptions = new List<SelectOption>();
            companyAccountList = new List<Account>();
            selectedCompanyAccount = '';

            selectedEmploymentToken = '';
            employmentTokenOptions = new List<SelectOption>();
            employmentTokenMap = new Map<Id, Employment_Token__c>();
            useNewEmploymentToken = false;                          

            welcomeEmailToBeSent = false;            
            autoAttendance = false;
            enableDone = false;
            
            // Initializing the Dynamic Title values
            headerTitle = 'CASM Bulk Enrolment Wizard';
        
            headerDescription = 'This is a wizard for bulk enrolment of the employees under one company into a Public/Private CASM course using an employment token. </br></br>';
            
            headerDescriptionPage1 = 'Step 1 : Select an Accredited Module Course and a company. This is to filter and get a list of employees under the selected company whom are enroled into the selected module course. </br>';
            headerDescriptionPage2 = 'Step 2 : Select the employees to enrol into CASM course. Use Search for Employee to search for a particular employee to perform enrolment. Previous selection under Select Employee/s for Bulk Enrolment will be erased after each search filter. </br>';
            headerDescriptionPage3 = 'Step 3 : Select an active employment token or create a new employment token. </br>';
            headerDescriptionPage3New = 'Step 3a : Create new employment token. </br>';
            headerDescriptionPage4 = 'Step 4 : Select \'Send Welcome Email\' to send welcome email to employee. This selection is only available for Public CASM course. Select \'Auto Assign Employees to Current Session\' to auto create attendance/s for Course Sessions added under the CASM course. Click on \'Confirm\' to enrol employees to the CASM course with the selected Employment Token. </br>';
            headerDescriptionPage5 = 'Step 5 : Enrolment result will be displayed. Click Done to exit. </br>';
    
            searchNote1 = 'Note: the search will not show Employee already enrolled in ' +activeSubCourse.Name+ '</br>';
            searchNote2 = 'Search result will only return a maximum of 200 records </br>';
            
            autoAttendanceHelpText = autoAttendanceHelpText = 'Please ensure that all session under the CASM course have sufficient seats before checking the checkbox. ' +
                      'To check the seat, go to Sessions under CASM Course, and check the maximum capacity.';
        } else {
            // Error, the page was called without a Course Id to identify the Sub Course
            error = true;
        }        
    }
    
    private Id recordTypeRetriever(SObjectType sObjectType, String recordTypeName) {
        
        // Generate a map of tokens for all the Record Types for the desired object
        Map<String,Schema.RecordTypeInfo> recordTypeInfo = sObjectType.getDescribe().getRecordTypeInfosByName();

        if(!recordTypeInfo.containsKey(recordTypeName)) return null;            

        //Retrieve the record type id by name
        return recordTypeInfo.get(recordTypeName).getRecordTypeId();
    }
    
    public List<wrappedObject> getWrappedEmployees() {
        
        List<wrappedObject> woList = new List<wrappedObject>();
        
        for (Account acc : (List<Account>)con.getRecords()) {
            woList.add(employeeSearchWrapperMap.get(acc.Id));                              
        }
        return woList;
    }
    
    // Action methods //
    // ============== //
    
    public void searchCompany() {
        
        // generate list of all the valid student programs employer for the currently selected Main Course
        List<LuanaSMS__Student_Program__c> validSP = [SELECT Id, LuanaSMS__Contact_Student__c FROM LuanaSMS__Student_Program__c WHERE LuanaSMS__Course__c = :mainCourseID AND LuanaSMS__Status__c IN :validStatusForEmployee AND Paid__c = TRUE];
        Set<Id> validContactIdSet = new Set<Id>();        
        for (LuanaSMS__Student_Program__c sp : validSP) {
            validContactIdSet.add(sp.LuanaSMS__Contact_Student__c);            
        }
        List<Account> validPAList = [SELECT Id, PersonContactId, Primary_Employer__c FROM Account WHERE PersonContactId IN :validContactIdSet];
        Set<Id> validEmployerIdSet = new Set<Id>();
        for (Account acc : validPAList) {
            if (acc.Primary_Employer__c != NULL) validEmployerIdSet.add(acc.Primary_Employer__c);
        }
        
        companyAccountOptions = new List<SelectOption>();
        String companySearchTerm = '';
        if (!String.isBlank(companyNameSearch)) {
            companySearchTerm += ' AND Name LIKE ' +'\'%'+ String.escapeSingleQuotes(companyNameSearch) +'%\'';
        }        
        if (!String.isBlank(companyCustomerIDSearch)) {            
            companySearchTerm += ' AND Customer_ID__c = \'' + String.escapeSingleQuotes(companyCustomerIDSearch) + '\'';
        }
        if (!String.isBlank(companyMemberIDSearch)) {            
            companySearchTerm += ' AND Member_ID__c = \'' + String.escapeSingleQuotes(companyMemberIDSearch) + '\'';
        }
        String queryString = 'SELECT Id, Name, Customer_ID__c, Member_ID__c FROM Account WHERE Id IN :validEmployerIdSet AND RecordTypeId = \'' + companyRecordTypeID + '\'' + companySearchTerm;
        queryString += ' ORDER BY Name ASC NULLS last';
        if (debug) system.debug('queryString: ' +queryString);
        
        // if (!String.isBlank(companySearchTerm)) {
            companyAccountList = Database.query(queryString);
            
            if (companyAccountList.isEmpty()) {
                companySearchNoResult = TRUE;
            } else {
                companySearchNoResult = FALSE;
            }
            
            if (debug) system.debug('companyAccountList: ' +companyAccountList);
        // }        
    }        
    
    public void searchEmployee() {
                
        // Do not include enrolment that are in progress or paid at the CASM course
        List<LuanaSMS__Student_Program__c> existingSP = [SELECT Id, Name, LuanaSMS__Contact_Student__c FROM LuanaSMS__Student_Program__c WHERE LuanaSMS__Course__c = :activeSubCourse.Id AND LuanaSMS__Status__c IN :statusForExistingSPToConsider];
        
        if (debug) system.debug('existingSP: ' +existingSP);
        
        Set<Id> contactIDSetToAvoid = new Set<Id>();
        for (LuanaSMS__Student_Program__c sp : existingSP) {
            contactIDSetToAvoid.add(sp.LuanaSMS__Contact_Student__c);
        }
        
        if (debug) system.debug('contactIDSetToAvoid: ' +contactIDSetToAvoid);      

        // Checks for all Student Program that satisfy following criteria
        // 1. they must have PAID status
        // 2. they must be studying in the course selected as the main course for the wizard
        // 3. they must have status that are within what described in validStatusForEmployee
        // 4. their Contact Student ID must not be among the ones that have been enrolled or paid for as queried above 
        // List<LuanaSMS__Student_Program__c> validSP = [SELECT Id, LuanaSMS__Contact_Student__c FROM LuanaSMS__Student_Program__c WHERE LuanaSMS__Course__c = :mainCourseID AND LuanaSMS__Status__c IN :validStatusForEmployee AND Paid__c = TRUE];
        List<LuanaSMS__Student_Program__c> validSP = [SELECT Id, LuanaSMS__Contact_Student__c FROM LuanaSMS__Student_Program__c WHERE LuanaSMS__Course__c = :mainCourseID AND LuanaSMS__Status__c IN :validStatusForEmployee AND Paid__c = TRUE AND LuanaSMS__Contact_Student__c NOT IN :contactIDSetToAvoid];
        Set<Id> validContactIdSet = new Set<Id>();        
        for (LuanaSMS__Student_Program__c sp : validSP) {
            validContactIdSet.add(sp.LuanaSMS__Contact_Student__c);
            studentContactToMainCourseSPMap.put(sp.LuanaSMS__Contact_Student__c, sp.Id);
        }
        
        // Then check for all Person Account that satisfy the following criteria
        // 1. they had contact in the Student Program above
        // 2. they must be employed by the Employer selected in the 1st page of the Wizard 
        List<Account> validPAList = [SELECT Id, PersonContactId, Primary_Employer__c FROM Account WHERE PersonContactId IN :validContactIdSet AND Primary_Employer__c = :selectedCompanyAccount];
        if (debug) {
            system.debug('validSP: ' +validSP);
            system.debug('validPAList: ' +validPAList);
            system.debug('validContactIdSet: ' +validContactIdSet);
        }                
        
        String searchTerm = '';
        if (!String.isBlank(employeeFirstNameSearch)) {
            searchTerm += ' AND FirstName';
            
            searchTerm += ' LIKE \'%' + String.escapeSingleQuotes(employeeFirstNameSearch) + '%\'';         
        }
        
        if (!String.isBlank(employeeLastNameSearch)) {
            searchTerm += ' AND LastName';
            
            searchTerm += ' LIKE \'%' + String.escapeSingleQuotes(employeeLastNameSearch) + '%\'';          
        }
        
        if (!String.isBlank(employeeMemberIDSearch)) {
            searchTerm += ' AND Member_ID__c = \'' + String.escapeSingleQuotes(employeeMemberIDSearch) + '\'';            
        }
        
        if (!String.isBlank(employeeCustomerIDSearch)) {
            searchTerm += ' AND Customer_ID__c = \'' + String.escapeSingleQuotes(employeeCustomerIDSearch) + '\'';
        }
        
        if (!String.isBlank(employeeEmailSearch)) {
            searchTerm += ' AND PersonEmail';
            
            searchTerm += ' LIKE \'%' + String.escapeSingleQuotes(employeeEmailSearch) + '%\'';                     
        }
        
        try {            
            queryString = 'SELECT Id, FirstName, LastName, Member_ID__c, Customer_ID__c, PersonEmail, PersonContactId, Affiliated_Branch_Country__c FROM Account WHERE Id IN :validPAList AND RecordTypeId = \'' + employeeRecordTypeID + '\'' + searchTerm + ' ORDER BY FirstName ASC NULLS last LIMIT 200';
            if (debug) {
                system.debug('queryString: ' +queryString);
                system.debug('employeeRecordTypeID: ' +employeeRecordTypeID);
            }

            employeePAList = new List<Account>();
            employeePAList = Database.query(queryString);
            if (debug) system.debug('employeePAList: ' +employeePAList);

            con = new ApexPages.StandardSetController(employeePAList);
            con.setPageSize(listSize);
            
            // Store the result List into a separate wrapper map
            employeeSearchWrapperMap = new Map<Id, wrappedObject>();
            
            // Also store the map to reference the Person Account via the Person Contact Id to be used during Payment Token process
            personContactIdToAffiliatedBranchCountryMap = new Map<Id, String>();
            
            for (Account acc : employeePAList) {                
                employeeSearchWrapperMap.put(acc.Id, new wrappedObject(acc));
                // ## personContactIdToAffiliatedBranchCountryMap.put(acc.PersonContactId, acc.Affiliated_Branch_Country__c);
            }            
            
        } catch(QueryException e) {
            if (debug) system.debug('Exception during searchEmployee: ' +e);
            ApexPages.addMessages(e);
        }        
    }
    
    // Check for all the available valid Employment token for the Employer
    public void checkEmploymentToken() {
        employmentTokenMap = new Map<Id, Employment_Token__c>();
        Date cutoffDate = system.today();
        // List<Employment_Token__c> validEmploymentTokens = [SELECT Id, Name, Expiry_Date__c FROM Employment_Token__c WHERE Active__c = TRUE AND Employer__c = :selectedCompanyAccount AND (Expiry_Date__c = NULL OR Expiry_Date__c > :cutoffDate) AND (Related_Course__c = :mainCourseID OR Related_Course__c = NULL)];
        List<Employment_Token__c> validEmploymentTokens = [SELECT Id, Name, Expiry_Date__c, Token_Code__c FROM Employment_Token__c WHERE Active__c = TRUE AND Employer__c = :selectedCompanyAccount AND (Expiry_Date__c = NULL OR Expiry_Date__c >= :cutoffDate) AND (Related_Course__c = :activeSubCourse.Id OR Related_Course__c = NULL)];
        
        if (debug) {
            system.debug('cutoffDate: ' +cutoffDate);
            system.debug('mainCourseID: ' +mainCourseID);
            system.debug('validEmploymentTokens: ' +validEmploymentTokens );
        }
        
        employmentTokenOptions = new List<SelectOption>();
        for (Employment_Token__c et : validEmploymentTokens) {
            employmentTokenMap.put(et.Id, et);
            SelectOption newETOption = new SelectOption(et.Id, et.Name);
            employmentTokenOptions.add(newETOption);
        }
                
        SelectOption newETOption = new SelectOption('new', '--New Employment Token--');
        employmentTokenOptions.add(newETOption);        
    }
    
    public void toggleNewEmploymentTokenPanel() {
        // Toggles the Employment Token creation panel
        if (useNewEmploymentToken) {
            useNewEmploymentToken = false;
        } else {
            useNewEmploymentToken = true;
        }
    }
    
    // Navigation Methods //
    // ================== //
    
    public PageReference cancel() {
        PageReference backToCourse = new PageReference('/' + activeSubCourse.Id);
        return backToCourse;
    }
    
    public PageReference nextToPage2() {
        
        if (debug) system.debug('selectedCompanyAccount: |' +selectedCompanyAccount+ '|');
        
        if (selectedCompanyAccount != null && selectedCompanyAccount != '') {
            // Improvement, if the user already selected a company before and did not change it when returning to Page 1 then it should not be resetting the employment token
            
            searchEmployee();
            PageReference nextPR = Page.luana_BulkEnrolmentPage2;
            return nextPR;        
        } else {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select a company after searching for one'));
            return NULL;
            
                        
        }
    }
    
    public PageReference nextToPage3() {
        Boolean employeeSelectionError = false;
        
        checkEmploymentToken();
        newEmploymentToken = new Employment_Token__c();
        
        // Check to make sure there are some employee selected in the map
        Boolean employeeSelected = false;
        woToCreate = new List<wrappedObject>();
        if (!employeeSearchWrapperMap.isEmpty()) {
            for (wrappedObject wo : employeeSearchWrapperMap.values()) {                
                if (wo.selected == true) {
                    employeeSelected = true;
                    woToCreate.add(wo);
                }
            }
        }        
        
        if (employeeSelected == false) {
            employeeSelectionError = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please add eligible Employee to the selection list for enrollment'));
        } else {
            // generate the Student Programs to enroll based on the students selected in the bucket
            
            spToCreate = new List<LuanaSMS__Student_Program__c>();
            for (wrappedObject se : woToCreate) {
                LuanaSMS__Student_Program__c newSP = new LuanaSMS__Student_Program__c();
                newSP.LuanaSMS__Course__c = activeSubCourse.Id;
                newSP.LuanaSMS__Contact_Student__c = se.storedObject.PersonContactId;
                newSP.Paid__c = true;
                newSP.LuanaSMS__Status__c = studentProgramDefaultStatus;
                if (studentContactToMainCourseSPMap.containsKey(se.storedObject.PersonContactId)) newSP.Related_Student_Program__c = studentContactToMainCourseSPMap.get(se.storedObject.PersonContactId);
                
                spToCreate.add(newSP);
            }
        }
        
        if (employeeSelectionError == false) {
            PageReference nextPR = Page.luana_BulkEnrolmentPage3;        
            return nextPR;
        } else {            
            return null;
        }
    }
    
    public PageReference nextToPage3or4() {
        if (selectedEmploymentToken == 'new') {
            useNewEmploymentToken = true;
            PageReference page3New = Page.luana_BulkEnrolmentPage3New;
            return page3New;
        } else {
            useNewEmploymentToken = false;
            return nextToPage4();
        }
    }
    
    public PageReference nextToPage4() {
        
        // Check for the Employment Token
        Boolean tokenError = false;
        if (useNewEmploymentToken) {
            // check if the New Employment Token detail fields are filled in
            if (newEmploymentToken.Token_Code__c == NULL || (newEmploymentToken.Token_Code__c != NULL && String.isBlank(newEmploymentToken.Token_Code__c))) {
                tokenError = true;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please enter Token Code for the new Employment Token'));                                          
            }
            
            if (newEmploymentToken.Cancellation_Fee__c == NULL) {
                newEmploymentToken.Cancellation_Fee__c = 0;
            }
            
            if (newEmploymentToken.Discounted_Price__c == NULL) {
                newEmploymentToken.Discounted_Price__c = 0;
            }                        

            // fill in the remaining details for the new Employment Token if no error occurred
            if (tokenError == false) {
                newEmploymentToken.Name = employmentTokenName;
                newEmploymentToken.Active__c = true;
                newEmploymentToken.Employer__c = selectedCompanyAccount;
                newEmploymentToken.Related_Course__c = activeSubCourse.Id;              
            }
            employmentTokenCode = newEmploymentToken.Token_Code__c;
        } else {
            // check if there is an Employment Token selected
            if (selectedEmploymentToken == NULL || String.isBlank(selectedEmploymentToken)) {
                tokenError = true;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please select an Employment Token'));
            }
            employmentTokenCode = employmentTokenMap.get(selectedEmploymentToken).Token_Code__c;
        }
        
        if (tokenError) {
            return null;
        } else {
            // create the Employment Token if using new Employment Token, otherwise simply use the selected Token ID
            if (useNewEmploymentToken) {
                Savepoint tokenCreationSP = Database.setSavepoint();
                try {
                    insert newEmploymentToken;
                } catch (Exception e) {
                    
                    system.debug('e message: ' +e.getMessage());
                    
                    if (e.getMessage().contains('Token_Code__c duplicates value on record')) {
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error: Duplicate Token Code found in the system. Please use a new Token Code.'));                     
                    } else {
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error when creating New Employment Token: ' +e));
                    }                   
                    Database.rollback(tokenCreationSP);
                    return null;
                }
                
            }
            
            PageReference nextPR = Page.luana_BulkEnrolmentPage4;
            return nextPR;            
        }               
    }
    
    public PageReference confirmEnrollment() {
        
        if (debug) system.debug('useNewEmploymentToken: ' + useNewEmploymentToken);
        
        enableDone = false;
        Savepoint sp = Database.setSavepoint();
        // Boolean confirmationError = false;
        try {
            // Stop the user from creating the Student Programs if there are not enough enrolment space in the Course using the latest value for the activeSubCourse
            LuanaSMS__Course__c checkSubCourse = [SELECT Id, Maximum_Enrolment__c, Number_of_active_and_paid_students__c FROM LuanaSMS__Course__c WHERE Id = :activeSubCourse.Id LIMIT 1];
            if (checkSubCourse.Maximum_Enrolment__c - checkSubCourse.Number_of_active_and_paid_students__c <= 0) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'Insufficient seats for CASM enrolment. Please increase the number of seats at the Course > Maximum Enrolment.'));
                return null;
            } else {
                // check if the Welcome Email checkbox is selected, and update the Student Programs to be created accordingly
                for (LuanaSMS__Student_Program__c sProg : spToCreate) {
                    // sProg.Welcome_Email_Sent__c = welcomeEmailToBeSent; 
                    if (welcomeEmailToBeSent) {
                        // if the Welcome_Email_Sent__c is not checked, this allows the workflow to send the email
                        sProg.Welcome_Email_Sent__c = false;
                    } else {
                        // if the Welcome_Email_Sent__c is checked, this stops the workflow from sending the email
                        sProg.Welcome_Email_Sent__c = true;
                    }
                    
                    sProg.LuanaSMS__Auto_Attendance_Creation__c = autoAttendance;                
                }
    
                // insert the new Student Programs record
                // insert spToCreate;
                
                // Partial Success handling
                Database.SaveResult[] insertResults = Database.insert(spToCreate, false);
                errorMessageMap = new Map<LuanaSMS__Student_Program__c, String>();
                List<LuanaSMS__Student_Program__c> successfullSP = new List<LuanaSMS__Student_Program__c>();
                errorCount = 0; 
                successCount = 0;
                
                integer counter = 0;
                for(Database.SaveResult sr : insertResults){
                    if (!sr.isSuccess()) {
                        errorMessageMap.put(spToCreate.get(counter), sr.getErrors()[0].getMessage());                    
    
                        errorCount ++;
                    } else {
                        successCount ++;
                        successfullSP.add(spToCreate.get(counter));
                    }
                    counter ++;
                }
                
                if (successCount > 0) {                 

                    Set<Id> successfullSPContact = new Set<Id>();
                    for (LuanaSMS__Student_Program__c sProg : successfullSP) {
                        successfullSPContact.add(sProg.LuanaSMS__Contact_Student__c);
                    }

                    Map<Id, String> personContactToAffiliatedBranchCountryMap = new Map<Id, String>();
                    for  (Account acc : [SELECT Id, PersonContactId, Affiliated_Branch_Country__c FROM Account WHERE PersonContactId IN :successfullSPContact]) {
                        personContactToAffiliatedBranchCountryMap.put(acc.PersonContactId, acc.Affiliated_Branch_Country__c);
                    }
                    
                    if (debug) system.debug('personContactToAffiliatedBranchCountryMap: ' +personContactToAffiliatedBranchCountryMap);

                    List<Payment_Token__c> paymentTokenToGenerate = new List<Payment_Token__c>();

                    for (LuanaSMS__Student_Program__c sProg : successfullSP) {

                        // generate the Payment Token for each of the Student Program
                        Payment_Token__c newPaymentToken = new Payment_Token__c();
                        newPaymentToken.Employer__c = selectedCompanyAccount;
                        newPaymentToken.Student_Program__c = sProg.Id;
                        newPaymentToken.Contact__c = sProg.LuanaSMS__Contact_Student__c;
                        newPaymentToken.Course__c = activeSubCourse.Id;
                        newPaymentToken.Expiry_Date__c = system.today();
                        newPaymentToken.Date_Used__c = system.today();
        
                        if (useNewEmploymentToken) {
                            newPaymentToken.Employment_Token__c = newEmploymentToken.Id;
                        } else {
                            newPaymentToken.Employment_Token__c = selectedEmploymentToken;
                        }                                                                   
                        
                        currencyKey = 'Currency_' + personContactToAffiliatedBranchCountryMap.get(sProg.LuanaSMS__Contact_Student__c);
                        
                        if (debug) {
                            system.debug('sprog contact student: ' +sProg.LuanaSMS__Contact_Student__c);
                            system.debug('currencyKey: ' +currencyKey);
                            system.debug('luanaConfigs: ' +luanaConfigs);
                        }
                        
                        if (luanaConfigs.containsKey(currencyKey)) {
                            if(luanaConfigs.get(currencyKey).Value__c == 'INT'){
                                newPaymentToken.Token_Currency__c = 'AUD';
                            } else {
                                newPaymentToken.Token_Currency__c = luanaConfigs.get(currencyKey).Value__c;
                            }
                        }
        
                        paymentTokenToGenerate.add(newPaymentToken);
                        
                    }
                    insert paymentTokenToGenerate;
                    
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, successCount + ' student program/s were allocated successfully.<br/>'));             
                }
                
                if (errorCount > 0) {
                    String detailsMsg = '';
                    for(LuanaSMS__Student_Program__c spKey : errorMessageMap.keySet()){
                        detailsMsg += '<a href="/' + String.valueOf(spKey.Id).substring(0, 15) + '" target="_blank">' + spKey.Name + '</a><br/>';
                    }
                    
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'There are ' + errorCount + ' student program/s that failed to be allocated. Please see the reports below for details.'));
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, detailsMsg));
                }
                enableDone = true;
            }           
        } catch (Exception e) {
            // confirmationError = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Exception during final confirmation: ' +e));
            Database.rollback(sp);            
        }                
        
        PageReference page5 = Page.luana_BulkEnrolmentPage5;
        return page5;
        /**
        if (confirmationError) {            
            return null;
        } else {            
            PageReference backToCourse = new PageReference('/' + activeSubCourse.Id);
            return backToCourse;
        }
        **/
    }
    
    public PageReference pageDone() {
        PageReference backToCourse = new PageReference('/' + activeSubCourse.Id);
        return backToCourse;
    }
    

    public PageReference backToPage1() {
        initialize();
        PageReference backPR = Page.luana_BulkEnrolmentPage1;
        return backPR;        
    }

    public PageReference backToPage2() {
        PageReference backPR = Page.luana_BulkEnrolmentPage2;
        return backPR;        
    }
    
    public PageReference backToPage3() {
        newEmploymentToken = new Employment_Token__c();
        checkEmploymentToken();
        PageReference backPR = Page.luana_BulkEnrolmentPage3;
        return backPR;       
    }
    
    // Pagination navigation
    public void Previous() {               
        con.previous();
    }
    
    public void Next() {
        if (debug) system.debug('Next button engaged');               
        con.next();        
    }
    
    public void Beginning() {
        con.first();
    }
    
    public void End() {                
        con.last();
    }
    
    public Boolean hasNext {        
        get {
            if(debug) system.debug('con.getHasNext() '+con.getHasNext());
            return con.getHasNext();
        }        
        set;
    }

    public Boolean hasPrevious {
        get {
            if (debug) system.debug('con.getHasPrevious() '+con.getHasPrevious());
            return con.getHasPrevious();
        }
        set;
    }
    
    public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set;
    }
}