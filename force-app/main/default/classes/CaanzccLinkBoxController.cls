/*------------------------------------------------------------------------------------
Author:        Sumit Gupta
Company:       Salesforce
Description:   Address details component controller

History
Date            Author             Comments
--------------------------------------------------------------------------------------
16-05-2018     Sumit Gupta          Initial Release
------------------------------------------------------------------------------------*/
public with sharing class CaanzccLinkBoxController {
    
    @AuraEnabled
    public static String getUserPermissions(List<String> customPermissions){
        List<CustomPermission> customPermissionRecords = [
            SELECT Id, DeveloperName, (
                SELECT Id FROM SetupEntityAccessItems 
                WHERE ParentId IN: getPermissionSetIds() OR ParentId =: UserInfo.getProfileId()
            ) 
            FROM CustomPermission WHERE DeveloperName IN : customPermissions
        ];
        return JSON.serialize(getUserPermissions(customPermissionRecords));
    }

    private static Set<String> getUserPermissions(List<CustomPermission> permissions){
    	Set<String> userPermissions = new Set<String>();
    	for(CustomPermission permission : permissions){
    		if(!permission.SetupEntityAccessItems.isEmpty()){
    			userPermissions.add(permission.DeveloperName);
    		}
    	}
    	return userPermissions;
    }

    private static Set<Id> getPermissionSetIds(){
        Set<Id> psIds = new Set<Id>();
        for(PermissionSetAssignment ps : [SELECT PermissionSetId FROM PermissionSetAssignment 
            WHERE AssigneeId =: UserInfo.getUserId()]){
            psIds.add(ps.PermissionSetId);
        }
        return psIds;
    }
}