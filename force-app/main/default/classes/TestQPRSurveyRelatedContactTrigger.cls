@isTest
public class TestQPRSurveyRelatedContactTrigger {
      public static Id questionnaireAU = Schema.SObjectType.QPR_Questionnaire__c.getRecordTypeInfosByName().get('QPR AUS').getRecordTypeId();

@testSetup
     Static void setupTestData(){
           
             // Create member
        Account a = TestObjectCreator.createBusinessAccount();
        insert a;
        
        // Create Review
        Review__c r = new Review__c();
        r.Practice_Name__c = a.Id;
        insert r;
        
        // Create QPR Questionnaire
        QPR_Questionnaire__c oneQuestionnaire = new QPR_Questionnaire__c();
        oneQuestionnaire.RecordTypeId = questionnaireAU;
        oneQuestionnaire.Practice__c = a.Id;
        oneQuestionnaire.Practice_ID__c = '12345';
        oneQuestionnaire.Review_Code__c = r.Id;
        insert oneQuestionnaire;
         
         //insert QPR Questionnaire related address
         QPR_Survey_Related_Contact__c relatedContact  = new QPR_Survey_Related_Contact__c();
         relatedContact.QPR_Survey__c = oneQuestionnaire.Id;
         relatedContact.Name_of_Partner_s_or_Equivalent__c	 = 'Test AB1';
         relatedContact.Other_Description__c = 'Test AB2';
         insert relatedContact;
    }
@isTest
    public static void testQPRSurveyRelatedContact(){
        QPR_Survey_Related_Contact__c relatedContact = [select ID, RemoveFlag__c from QPR_Survey_Related_Contact__c where Name_of_Partner_s_or_Equivalent__c = 'Test AB1' and Other_Description__c ='Test AB2'];
        relatedContact.RemoveFlag__c= True;
       	update relatedContact;
        
    }
    
}