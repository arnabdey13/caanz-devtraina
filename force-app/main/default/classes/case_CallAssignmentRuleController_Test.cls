/*------------------------------------------------------------------------------------
Author:        Mayukhman
Company:       Tech Mahindra
Description:   Test class for most of the Controllers - case_CallAssignmentRuleController 

History
Date            Author             Comments
--------------------------------------------------------------------------------------
24-07-2019     Mayukhman         Initial Release
------------------------------------------------------------------------------------*/
@isTest 
public class case_CallAssignmentRuleController_Test {
    private static Account currentAccount;
    private static Case currentCase;
    static {
        currentAccount = case_CallAssignmentRuleController_Test.createFullMemberAccount();
        insert currentAccount;
        currentCase = case_CallAssignmentRuleController_Test.createCase();
        insert currentCase;
    }
    
    
    // <Create FullMemberAccount>
    public static Account createFullMemberAccount(){
        Account FullMemberAccountObject = new Account();
        FullMemberAccountObject.Salutation = 'Mr.';
        FullMemberAccountObject.FirstName = 'TestFName';
        FullMemberAccountObject.PersonEmail= 'Full_Member'+ case_CallAssignmentRuleController_Test.getRandomNumber(910) +'@gmail.com';
        FullMemberAccountObject.LastName = 'Test'+ case_CallAssignmentRuleController_Test.getRandomNumber(910);
        FullMemberAccountObject.RecordTypeId = RecordTypeCache.getId(FullMemberAccountObject, 'Full_Member');
        FullMemberAccountObject.Communication_Preference__c= 'Home Phone';
        FullMemberAccountObject.PersonHomePhone= '1234';
        FullMemberAccountObject.PersonOtherStreet= '83 Saggers Road';
        FullMemberAccountObject.PersonOtherCity='JITARNING';
        FullMemberAccountObject.PersonOtherState='Western Australia';
        FullMemberAccountObject.PersonOtherCountry='Australia';
        FullMemberAccountObject.PersonOtherPostalCode='6365';  
        return FullMemberAccountObject;
    }
    public static Case createCase(){
        Id recordTypeID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case').getRecordTypeId();
        Case CAANZMSTCase = new Case();
        CAANZMSTCase.AccountId = currentAccount.Id;
        CAANZMSTCase.Subject = 'Test';
        CAANZMSTCase.Type = 'Complaints';
        CAANZMSTCase.Sub_Type__c = 'About CA ANZ';
        CAANZMSTCase.RecordTypeId = recordTypeID;
        return CAANZMSTCase;
    }
    public static Integer getRandomNumber(Integer size){
        Double d = math.random() * size;
        return d.intValue();
    }
    
    @isTest static void test_case_CallAssignmentRuleController() {
        case_CallAssignmentRuleController.callCaseAssignmentRule(currentCase.Id);
        List<Case> lstCase = new List<Case>();
        lstCase.add(currentCase);
        case_CallAssignmentRuleController.callCaseAssignmentRulePB(lstCase);
    }
}