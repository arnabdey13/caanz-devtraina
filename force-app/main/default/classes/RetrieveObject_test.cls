@isTest
public class RetrieveObject_test 
{
static testMethod void validateRetrieveObject()
{
    RetrieveObject.objectData od = new RetrieveObject.objectData();
    
    //Account test
    Account testAccount = new Account(Name='TestAccount_RetrieveObject', Member_ID__c = 'TEST123'
                                     ,BillingStreet='1 Test Street'); //DN comply with validation rule
    insert testAccount;
    
    od = RetrieveObject.GetObject(testAccount.Member_ID__c,'', 'account');
    system.assertEquals(true, od.bExists);
    
    //User test
     User testUser = new User(ProfileId ='00e90000001JzUV', Username = 'testUserRetrieveObject@icaa.com.au',Email='julian.redwood@charteredaccountants.com.au',Alias ='JRTest',CommunityNickname = 'julian redwood', firstname ='TestUser',LastName = 'RetrieveObject',LocaleSidKey='en_AU',LanguageLocaleKey='en_US',TimeZoneSidKey='Australia/Sydney',EmailEncodingKey='ISO-8859-1');
     insert testUser;
    
    od = RetrieveObject.GetObject('',testUser.ID,'user');
    system.assertEquals(true, od.bExists);
    
    //Bad test
        od = RetrieveObject.GetObject('123','','user');
        system.assertEquals(false, od.bExists);
}
    
}