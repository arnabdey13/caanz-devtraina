@IsTest @TestVisible
private with sharing class TestDataDegreeBuilder {

    private String name;
    private String status;
    private String type;

    @TestVisible
    private TestDataDegreeBuilder(String name) {
        this.name = name;
    }

    @TestVisible
    private TestDataDegreeBuilder status(String status) {
        this.status = status;
        return this;
    }

    @TestVisible
    private TestDataDegreeBuilder type(String type) {
        this.type = type;
        return this;
    }

    @TestVisible
    private List<edu_Degree__c> create(Integer count, Boolean persisted) {
        List<edu_Degree__c> results = new List<edu_Degree__c>();
        for (Integer i = 0; i < count; i++) {
            results.add(new edu_Degree__c(
                    Degree_Name__c = this.name,
                    Degree_Type__c = this.type,
                    Status__c = this.status));
        }
        if (persisted) insert results;
        return results;
    }

}