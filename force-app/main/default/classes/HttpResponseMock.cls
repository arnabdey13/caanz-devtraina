@isTest
global class HttpResponseMock {

    global class HttpResponseSuccess implements HttpCalloutMock {
        global HTTPResponse respond(HTTPRequest request) {

            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"count":1,"results":[{"suggestion":"147 Ross Street, FOREST LODGE  NSW 2037","matched":[],"format":"https://api.edq.com/capture/address/v2/format?country=AUS&id=700AUS-aOAUSHArgBwAAAAAIAwEAAAAAK.6m0AAgAAAAAAAAAAD..2QAAAAA.....wAAAAAAAAAAAAAAAAAxNDcgUm9zcyBTdHJlZXQA"}]}');
            response.setStatusCode(200);
            System.debug('MOCK RESPONSE ' + response);
            System.debug('MOCK RESPONSE BODY ' + response.getBody());
            System.debug('MOCK RESPONSE HEADER KEYS' + response.getHeaderKeys());

            return response;
        }
    }

    global class HttpResponseSuccessQASFormat implements HttpCalloutMock {
        global HTTPResponse respond(HTTPRequest request) {

            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"address":[{"addressLine1":"147 Ross St"},{"addressLine2":""},{"addressLine3":""},{"locality":"PORT MELBOURNE"},{"province":"VIC"},{"postalCode":"3207"},{"country":"AUSTRALIA"}],"components":[{"deliveryPointId1":"30717717"},{"streetNumber1":"147"},{"street1":"Ross St"},{"locality1":"PORT MELBOURNE"},{"province1":"Victoria"},{"provinceCode1":"VIC"},{"postalCode1":"3207"},{"country1":"AUSTRALIA"},{"countryISO1":"AUS"}]}');
            response.setStatusCode(200);
            System.debug('MOCK RESPONSE ' + response);
            System.debug('MOCK RESPONSE BODY ' + response.getBody());
            System.debug('MOCK RESPONSE HEADER KEYS' + response.getHeaderKeys());

            return response;
        }
    }

    global class HttpResponseFailure implements HttpCalloutMock {
        global HTTPResponse respond(HTTPRequest request) {

            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"Data" : "Not found", "Message" : "Error"}');
            response.setStatusCode(404);

            return response;
        }
    }
}