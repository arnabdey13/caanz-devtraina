/**
    Developer: WDCi (KH)
    Development Date: 20/03/2017
    Task #: Enrollment wizard - Controller for Details LLL
    
**/

public without sharing class luana_EnrolmentWizardDetailsLLController extends luana_EnrolmentWizardObject{
    
    luana_EnrolmentWizardController stdController;
    
    public luana_EnrolmentWizardDetailsLLController(luana_EnrolmentWizardController stdController){
        this.stdController = stdController;
    }
    
    public luana_EnrolmentWizardDetailsLLController(){
    }

    
    public PageReference detailsLL1Next(){
        boolean hasError = false;
        
        try{
        
            if(!stdController.workingStudentProgram.Accept_terms_and_conditions__c){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please agree to the Terms and Conditions before proceeding'));
                hasError = true;
            }
                     
            if(!hasError){
                 PageReference pageRef = stdController.getDetailsNextPage();
                stdController.skipValidation = true;
                
                return pageRef;
            }
        
        }Catch(Exception exp){
        
        }
        return null;
    }
    
    public PageReference detailsLL1Back(){
    
        PageReference pageRef;
        stdController.skipValidation = true;
        
        if(stdController.selectedProgramOffering.Require_Subject_Selection__c){
            pageRef = Page.luana_EnrolmentWizardSubject;
        } else {
            stdController.selectedCourseId = null;
            stdController.selectedCourse = null;
            
            pageRef = Page.luana_EnrolmentWizardCourse;
        }
        
        return pageRef;
    }
}