public class luana_EmployerTempleteController {
    
    public String userPhotoUrl {get; set;}
    private Id communityId;
    
    public luana_EmployerTempleteController(){

        for(ConnectApi.community comm: ConnectApi.Communities.getCommunities().communities){
            if(comm.urlPathPrefix == 'member'){
                communityId = comm.Id;
            }
        }
        
        ConnectApi.Photo userProfPhoto = ConnectApi.UserProfiles.getPhoto(communityId, UserInfo.getUserId());
        
        userPhotoUrl = userProfPhoto.largePhotoUrl;
    }

}