/**
    Developer: WDCi (Lean)
    Development Date: 05/04/2016
    Task #: Enrollment wizard - Controller for Details CA Module
    
    Change History:
    LCA-611 09/06/2016 - added logic to display OOAE case url based on community/user
    LCA-706 28/06/2016 - WDCi Lean: added workshop location validation
    LCA-720 11/07/2016 - WDCi Lean: to list relationship with status Current only
**/

public without sharing class luana_EnrolmentWizardDetailsCMController extends luana_EnrolmentWizardObject{
    
    luana_EnrolmentWizardController stdController;
    
    //public Account personAccountInfo {get; set;}
    public List<Employment_History__c> eHistories {get; set;}
    public List<Relationship__c> relationships {get; set;}
    public Set<String> mentorList {get; set;}

    //LCA-469 Phase 3
    public List<LuanaSMS__Course__c> courseList {private set; public get;}
    
    //LCA-87
    public String outOfEmploymentMessage {private set; public get;}
    
    //LCA-407
    public Boolean hasEmployerAndMentor {private set; public get;}

    //LCA-611
    String ooaeCaseRecordTypeId;
    
    public luana_EnrolmentWizardDetailsCMController(luana_EnrolmentWizardController stdController){
        this.stdController = stdController;
        
        eHistories = [Select Id, Employer__c, Employer__r.Name, Primary_Employer__c, Employer__r.BillingStreet, Employer__r.BillingState, 
                            Employer__r.BillingPostalCode, Employer__r.BillingCity, Employer__r.BillingCountry, Member__c from Employment_History__c Where Primary_Employer__c = true and Member__c =: stdController.custCommAccId];
                
        relationships = [Select Id, Name, Primary_Relationship__c, Account__c, Account__r.Name, Member__c, Member__r.Name, Reciprocal_Relationship__c 
                            from Relationship__c 
                            Where (((Reciprocal_Relationship__c = 'Mentee' and Member__c =: stdController.custCommAccId)) or
                            ((Primary_Relationship__c = 'Mentee' and Account__c =: stdController.custCommAccId))) and Status__c = 'Current' //LCA-720
                         ];  
                                                               
        mentorList = new Set<String>();
        for(Relationship__c r: relationships){
            if(r.Reciprocal_Relationship__c == 'Mentee' && r.Member__c == stdController.custCommAccId){
                mentorList.add(r.Account__r.Name);
            }else if(r.Primary_Relationship__c == 'Mentee' && r.Account__c == stdController.custCommAccId){
                mentorList.add(r.Member__r.Name);
            }
        }
        if(!mentorList.isEmpty() && !eHistories.isEmpty()){
            hasEmployerAndMentor = true;
        }else{
            hasEmployerAndMentor = false;
            stdController.workingStudentProgram.Employment_and_mentor_details_correct__c = 'No';
        }
        
        //LCA_611
        for(RecordType rt : [select id from RecordType where SObjectType = 'Case' and Name =: luana_RequestConstants.REQ_OOAE_RT_NAME]){
            ooaeCaseRecordTypeId = rt.Id;
        }
    }
    
    public luana_EnrolmentWizardDetailsCMController(){
        
    }
    
    public PageReference detailsCMNext(){
        
        
        
        boolean hasError = false;
        
        //LCA-5
        if(stdController.isSelectedCapstone){
        
            if(stdController.selectedWorkshopLocation1Id == null){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure the "Please select your 1st workshop location preference" is selected.'));
                hasError = true;
            }else{
                stdController.workingStudentProgram.Workshop_Location_Preference_1__c = stdController.selectedWorkshopLocation1Id;
            }
            if(stdController.selectedDayOfWeek1 == null){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure the "Please select your 1st day of the week preference" is selected.'));
                hasError = true;
            }else{
                stdController.workingStudentProgram.Day_of_the_Week_Preference_1__c = stdController.selectedDayOfWeek1;
            }

            if(stdController.selectedWorkshopLocation2Id == null){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure the "Please select your 2st workshop location preference" is selected.'));
                hasError = true;
            }else{
                stdController.workingStudentProgram.Workshop_Location_Preference_2__c = stdController.selectedWorkshopLocation2Id;
            }
            if(stdController.selectedDayOfWeek2 == null){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure the "Please select your 2st day of the week preference" is selected.'));
                hasError = true;
            }else{
                stdController.workingStudentProgram.Day_of_the_Week_Preference_2__c = stdController.selectedDayOfWeek2;
            }
            
            //LCA-706
            if(stdController.selectedWorkshopLocation1Id != null && stdController.selectedDayOfWeek1 != null && stdController.selectedWorkshopLocation1Id == stdController.selectedWorkshopLocation2Id && stdController.selectedDayOfWeek1 == stdController.selectedDayOfWeek2){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure your 1st workshop preference is different to your 2nd workshop preference.'));
                hasError = true;
            }
            
            if(stdController.workingStudentProgram.Line_of_service__c == null){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure the "Line of service" is selected.'));
                hasError = true;
            }
            if(stdController.workingStudentProgram.Organisation_type__c == null){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure the "Organisation type" is selected.'));
                hasError = true;
            }
        }
        
        if(!stdController.workingStudentProgram.Accept_terms_and_conditions__c){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure that you are agreed with the Terms and Conditions by checking on the checkbox'));
            hasError = true;
        }
        
        if(stdController.selectedExamLocationId == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure the "Please select your exam location preference" is selected.'));
            hasError = true;
        } else {
            //LCA-469
            String examLocationCountry = '';
            stdController.examLocationFeePrice = new PricebookEntry();
            
            stdController.prefExamLocation = stdController.examLocationMap.get(stdController.selectedExamLocationId).Delivery_Location__r.LuanaSMS__Delivery_Location_Name__c;
            examLocationCountry = stdController.examLocationMap.get(stdController.selectedExamLocationId).Delivery_Location__r.LuanaSMS__Country__c;
            
            if(stdController.luanaConfigs.containsKey(stdController.examLocationFeeProductKey) && stdController.luanaConfigs.containsKey(stdController.examLocationFreeCountryKey) && stdController.luanaConfigs.containsKey(stdController.pricebookKey)){
                if(!stdController.luanaConfigs.get(stdController.examLocationFreeCountryKey).Value__c.contains(examLocationCountry)){
                    
                    for(PricebookEntry pbe : [select id, UnitPrice, product2.NetSuite_Internal_Id__c, product2.NetSuite_Pricing_Level__c from PricebookEntry where Pricebook2Id =: stdController.luanaConfigs.get(stdController.pricebookKey).Value__c and Product2Id =: stdController.luanaConfigs.get(stdController.examLocationFeeProductKey).Value__c]){
                        stdController.examLocationFeePrice = pbe;
                    }
                    if(stdController.examLocationFeePrice == null || stdController.examLocationFeePrice.Id == null){
                        ApexPages.Message warningmsg = new ApexPages.Message(ApexPages.severity.FATAL,'Unfortunately, we are not able to determine the exam location fee. Please try again later or contact our support if the problem persists.');
                        ApexPages.addmessage(warningmsg);
                        
                        hasError = true;
                    }
                }   
            } else {
                ApexPages.Message warningmsg = new ApexPages.Message(ApexPages.severity.FATAL,'Unfortunately, we are not able to determine the exam location fee. Please try again later or contact our support if the problem persists.');
                ApexPages.addmessage(warningmsg);
                
                hasError = true;
            }
        }
        
        
        if(stdController.workingStudentProgram.Do_you_require_assistance__c == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure the "Do you require assistance?" is selected.'));
            hasError = true;
        }
        
        if(stdController.workingStudentProgram.Employment_and_mentor_details_correct__c == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure the "Are your employment and mentor details correct?" is selected.'));
            hasError = true;
        } else {
            //LCA-87
            if(stdController.workingStudentProgram.Employment_and_mentor_details_correct__c == 'No' && stdController.workingStudentProgram.Out_of_Approved_Employment__c == null){
                ApexPages.Message warningmsg = new ApexPages.Message(ApexPages.severity.FATAL,'Do any of the following statements apply to you?: You must enter a value');
                ApexPages.addmessage(warningmsg);
                hasError = true;
            }
        }
        
        if(!hasError){
            //LCA-469 Phase 3
            if(stdController.assistanceAtt.body != null){
                stdController.luanaAttachmentCache.addAttachmentCache(new List<Attachment>{stdController.assistanceAtt}, 'Case');
                stdController.assistanceAtt = new Attachment();
            } 
            
            //LCA-466 Phase 3
            stdController.selectedExamLocation = stdController.examLocationMap.get(stdController.selectedExamLocationId).Delivery_Location__r.LuanaSMS__Delivery_Location_Name__c;
            
            if(stdController.examLocationFeePrice != null && stdController.examLocationFeePrice.Id != null){
                stdController.totalAmount += stdController.examLocationFeePrice.unitPrice;
            }
            
            PageReference pageRef = stdController.getDetailsNextPage();
            stdController.skipValidation = true;
            
            return pageRef;
            
        } else {
            //LCA-469 Phase 3
            stdController.assistanceAtt = new Attachment();
            return null;
        }
        
        
        
        return null;
    }
    
    public List<SelectOption> dayOfWeekPref1Options {get; set;}
    public List<SelectOption> dayOfWeekPref2Options {get; set;}
    //LCA-5
    public void getDayofWeekPref1WSLoc(){
        
        dayOfWeekPref1Options = new List<SelectOption>();
        dayOfWeekPref1Options.add(new SelectOption('', '--None--'));

        if(stdController.selectedWorkshopLocation1Id != null && stdController.workshopLocationMap.containsKey(stdController.selectedWorkshopLocation1Id)){
            String availableDates = stdController.workshopLocationMap.get(stdController.selectedWorkshopLocation1Id).Availability_Day__c;
            
            for(String availDay: availableDates.split(';')){
                SelectOption option = new SelectOption(availDay,availDay);
                dayOfWeekPref1Options.add(option); 
            }
            
        }
    }
    public void getDayofWeekPref2WSLoc(){
        
        dayOfWeekPref2Options = new List<SelectOption>();
        dayOfWeekPref2Options.add(new SelectOption('', '--None--'));

        if(stdController.selectedWorkshopLocation2Id != null && stdController.workshopLocationMap.containsKey(stdController.selectedWorkshopLocation2Id)){
            String availableDates = stdController.workshopLocationMap.get(stdController.selectedWorkshopLocation2Id).Availability_Day__c;
            
            for(String availDay: availableDates.split(';')){
                SelectOption option = new SelectOption(availDay,availDay);
                dayOfWeekPref2Options.add(option); 
            }
            
        }
    }
    
    public PageReference detailsCMBack(){
        
        PageReference pageRef;
        stdController.skipValidation = true;
        
        if(stdController.selectedProgramOffering.Require_Subject_Selection__c){
            pageRef = Page.luana_EnrolmentWizardSubject;
        } else {
            stdController.selectedCourseId = null;
            stdController.selectedCourse = null;
            
            pageRef = Page.luana_EnrolmentWizardCourse;
        }
        
        return pageRef;
    }
    
    //LCA-87
    public void determineOutOfEmployment(){
        //LCA-611 added logic to display different url
        String hostUrl;
        
        if(luana_NetworkUtil.isInternal()){
            hostUrl = '/500/e?RecordType=' + ooaeCaseRecordTypeId + '&ent=Case';
        }else{
            hostUrl = '/member/luana_RequestOOAE?poid=' + stdController.selectedProgramOfferingId;
        }

        if(stdController.workingStudentProgram.Out_of_Approved_Employment__c == 'Yes'){
            outOfEmploymentMessage = 'You are currently Out of Approved Employment and are required to complete the <a href="/customer/Application?RecordType=Employment%20Details%20Form%20(EDF)" target="_blank">Employment Details Form</a>. You may continue with enrolment.'; //, CA-X changes - Terry 21st Jun however this must be submitted to us before you start your next module.';
        } else if(stdController.workingStudentProgram.Out_of_Approved_Employment__c == 'No'){
            outOfEmploymentMessage = 'To update your employer and mentor details, please complete and submit the <a href="/customer/Application?RecordType=Employment%20Details%20Form%20(EDF)" target="_blank">Employment Details Form</a>. This should be submitted before the start of the Module so that we may verify that you are engaged in approved practical experience.';
        } else {
            outOfEmploymentMessage = '';
        }
    }
    
    //LCA-469 Phase 3
    public void reloadPage(){
        
    }
    
}