@isTest
private class ProfessionalConductTriggersTest{

static testMethod void NZPCFlagTest(){              
        
    RecordType rt = [select id,Name from RecordType where SobjectType='Conduct_Support__c' and Name='NZ' Limit 1];        
    Account a = new Account(Name = 'Test NZ Member', Member_Of__c = 'NZICA'
                           ,BillingStreet='1 Test Street'); //DN comply with validation rule
    insert a;
        
    Conduct_Support__c pc = new Conduct_Support__c(Member__c = a.Id, RecordTypeId = rt.Id, Complaint__c = true);
    insert pc;
    
    Account insertedAccount = [select Professional_Conduct_Flag_NZ_Checkbox__c FROM Account WHERE Id = :a.id];    
       
    // Test 1 - Check NZ PC Flag is set on Account
    System.assertEquals(true,insertedAccount.Professional_Conduct_Flag_NZ_Checkbox__c);
    
    Conduct_Support__c insertedPC = [select Complaint__c FROM Conduct_Support__c WHERE Id = :pc.id];
    
    insertedPC.complaint__c = false;
    update insertedPC;
    
    Account updatedAccount = [select Professional_Conduct_Flag_NZ_Checkbox__c FROM Account WHERE Id = :a.id];
	
    // Test 2 - Check NZ PC Flag is not set on Account
    System.assertEquals(false,updatedAccount.Professional_Conduct_Flag_NZ_Checkbox__c);     
               
}
    
static testMethod void AUPCFlagTest(){              
        
    RecordType rt = [select id,Name from RecordType where SobjectType='Conduct_Support__c' and Name='AU' Limit 1];
        
    Account a = new Account(Name = 'Test AU Member', Member_Of__c = 'ICAA'
                                                  ,BillingStreet='1 Test Street'); //DN comply with validation rule
    insert a;
            
   	Conduct_Support__c pc = new Conduct_Support__c(Member__c = a.Id, RecordTypeId = rt.Id);
    insert pc;
        
    Account insertedAccount = [select Professional_Conduct_Flag_AU_Checkbox__c FROM Account WHERE Id = :a.id];    
       
   // Test 1 - Check AU PC Flag is not set on Account
    System.assertEquals(true,insertedAccount.Professional_Conduct_Flag_AU_Checkbox__c);

	Conduct_Support__c insertedPC = [select Overall_Status__c FROM Conduct_Support__c WHERE Id = :pc.id];
    
    insertedPC.Overall_Status__c = 'Closed';
    update insertedPC;
    
    Account updatedAccount = [select Professional_Conduct_Flag_AU_Checkbox__c FROM Account WHERE Id = :a.id];
	
    // Test 2 - Check AU PC Flag is not set on Account
    System.assertEquals(false,updatedAccount.Professional_Conduct_Flag_AU_Checkbox__c);         
        
}
    static testMethod void FineCostFlagTest(){
        
    RecordType rt = [select id,Name from RecordType where SobjectType='Conduct_Support__c' and Name='AU' Limit 1];        
    
    Account a = new Account(Name = 'Test AU Member', Member_Of__c = 'ICAA'
                                                  ,BillingStreet='1 Test Street'); //DN comply with validation rule
    insert a;
        
    Conduct_Support__c pc = new Conduct_Support__c(Member__c = a.Id, RecordTypeId = rt.Id);
    insert pc;
        
    Professional_Conduct_Tribunal__c pct = new Professional_Conduct_Tribunal__c(Professional_Conduct__c = pc.Id);
    insert pct;
        
    Fine_Cost__c fc = new Fine_Cost__c(Professional_Conduct__c = pc.Id, Professional_Conduct_Tribunal__c = pct.Id);
    insert fc;
        
    Account insertedAccount = [select Unpaid_Fine_Cost_Flag_Checkbox__c FROM Account WHERE Id = :a.id];
        
    // Test 1 - Check Fine Cost Flag is set on Account
    System.assertEquals(true,insertedAccount.Unpaid_Fine_Cost_Flag_Checkbox__c);
        
    Fine_Cost__c insertedFC = [select Cost_Recovery_Status__c FROM Fine_Cost__c WHERE Id = :fc.id];
    
    insertedFC.Cost_Recovery_Status__c = 'Paid in Full';
    update insertedFC;
    
    Account updatedAccount = [select Unpaid_Fine_Cost_Flag_Checkbox__c FROM Account WHERE Id = :a.id];
	
    // Test 2 - Check AU PC Flag is not set on Account
    System.assertEquals(false,updatedAccount.Unpaid_Fine_Cost_Flag_Checkbox__c);   
       
    }
        
}