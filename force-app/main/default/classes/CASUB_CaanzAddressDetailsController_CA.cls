/*------------------------------------------------------------------------------------
Author:        Paras  Prajapati 
Company:       Salesforce
Description:   Address details component controller - This class is clone of CaanzAddressDetailsController apex, 
			   with upgrade version, has issue with setStorable for New component for Subs.

History
Date            Author             Comments
--------------------------------------------------------------------------------------
27-03-2019     Paras Prajapati       	Initial Release
------------------------------------------------------------------------------------*/
public with sharing class CASUB_CaanzAddressDetailsController_CA {
    
    
    private static Integer DEFAULT_SEARCH_LIMIT = 10;
    
    @AuraEnabled
    public static String getAddressData(Id contactId){
        Contact contact = getPersonAccountData(contactId);
        CASUB_Address_Notification_CA_Setting__mdt casub_Address_Notification_CA_Setting = getAddressNotificationSettingData(contact.OtherCountry);
        System.debug('casub_Address_Notification_CA_Setting' + casub_Address_Notification_CA_Setting);
        return JSON.serialize(new AddressData(contact,casub_Address_Notification_CA_Setting));
    }
    
    @AuraEnabled(cacheable=true)
    public static String getCountryAndStateOptions(){
        return JSON.serialize(getCountryStateOptions());
    }
    
    @AuraEnabled
    public static String saveDeclarationStatement(String personAccountId,Boolean confirmationChecked){
        try{
            Account account =  new account();
            account.id = personAccountId;
            account.NZICA_confirmation__pc = confirmationChecked;
            update account;
            System.debug('Updated Account' + account);
            return 'Account Updated';
        } catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }
    }
    
    @AuraEnabled
    public static String saveAddressData(String addressDataJSONString){
        AddressData addressData = (AddressData)JSON.deserialize(addressDataJSONString, AddressData.class);
        if(addressData.account != null) {
            
            update addressData.account;            
            /* Mayukh - Add code to update Subscription on Address change in Step 2 */
            String currentYear = getCurrentYear();
            List<Subscription__c> userPersonAccountSubs = new List<Subscription__c>([SELECT Account__c, Fiscal_Year__c, Contact_Details_URL__c, Contact_Details_Status__c, Obligation_URL__c, Obligation_Status__c, Payment_URL__c, 
                                                                                     Sales_Order_Status__c, Sales_Order__c,  Contact_Details_Sub_Component__c, Obligation_Sub_Components__c, 
                                                                                     Year__c, Member_Age__c, Id, Name,Concession__c
                                                                                     From Subscription__c Where Year__c =: currentYear AND Account__c =: addressData.account.Id LIMIT 1]);
            if(userPersonAccountSubs.size() > 0){
                userPersonAccountSubs[0].Contact_Details_Status__c = 'Pending';
                userPersonAccountSubs[0].Obligation_Status__c = 'Pending';
                update userPersonAccountSubs;
            }
            /* End of Code*/
        }
        
        if(addressData.contact != null) update addressData.contact; 
        return JSON.serialize(addressData);
    }
    
    @AuraEnabled
    public static String searchAddress(String searchTerm, String countryCode, Integer searchLimit){
        searchLimit = searchLimit != null ? searchLimit : DEFAULT_SEARCH_LIMIT;
        searchTerm = string.escapeSingleQuotes(searchTerm);
        return !Test.isRunningTest() ? JSON.serialize(EDQAddressService.searchAddress(searchTerm, countryCode, searchLimit)) : '';
    }
    
    @AuraEnabled(cacheable=true)
    public static String formatAddress(String addressId, String countryCode){
        return JSON.serialize(EDQAddressService.formatAddress(addressId, countryCode));
    }
    
    private static Contact getPersonAccountData(Id contactId){
        if(contactId == null){
            contactId = [SELECT ContactId FROM User WHERE Id =: UserInfo.getUserId()].ContactId;
        }
        if(contactId == null) return null;
        return [
            SELECT OtherStreet, OtherCity, OtherState, OtherCountry, OtherStateCode, OtherCountryCode, OtherPostalCode,
            MailingStreet, MailingCity, MailingState, MailingCountry, MailingStateCode, MailingCountryCode, MailingPostalCode,
            Account.Mailing_and_Residential_is_the_same__c, Account.Mailing_Company_Name__c,Account.NZICA_confirmation__pc
            FROM Contact WHERE Id =: contactId LIMIT 1
        ];
    }
    
    private static CASUB_Address_Notification_CA_Setting__mdt getAddressNotificationSettingData(String residentialCountry){
        CASUB_Address_Notification_CA_Setting__mdt casub_Address_Notification_CA_Setting= new CASUB_Address_Notification_CA_Setting__mdt();
        List<CASUB_Address_Notification_CA_Setting__mdt> listOfCASUB_Address_Notification_CA_Setting = [SELECT MasterLabel,Confirmation_Message__c,
                                                                                                        Is_Active__c,Regulatory_Requirements_Web_Link__c 
                                                                                                        from CASUB_Address_Notification_CA_Setting__mdt	
                                                                                                        where MasterLabel=:residentialCountry LIMIT 1];
            if(listOfCASUB_Address_Notification_CA_Setting!=null && listOfCASUB_Address_Notification_CA_Setting.size()>0){
                casub_Address_Notification_CA_Setting = listOfCASUB_Address_Notification_CA_Setting.get(0);
            }
        return casub_Address_Notification_CA_Setting;
    }
    
    private static List<Country> getCountryStateOptions() {
        List<Country> countries = getCountryPicklistValues();
        List<State> states = getStatePicklistValues();
        Map<Integer, Country> countriesMap = new Map<Integer, Country>();
        for (State state : states) {
            for(Integer index : state.getCountryIndexes()){
                if(index < countries.size()){
                    countries.get(index).addState(state);
                }
            }
            state.validFor = null;
        } 
        return countries;
    }
    
        
    private static final String base64Chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' + 'abcdefghijklmnopqrstuvwxyz' + '0123456789+/';
    
    private static List<Country> getCountryPicklistValues() {
        return (List<Country>) JSON.deserialize(JSON.serialize(User.CountryCode.getDescribe().getPicklistValues()), List<Country>.class);
    }
    
    private static List<State> getStatePicklistValues() {
        return (List<State>) JSON.deserialize(JSON.serialize(User.StateCode.getDescribe().getPicklistValues()), List<State>.class);
    }
    
    public class AddressData{
        Account account;
        Contact contact;
        CASUB_Address_Notification_CA_Setting__mdt casub_Address_Notification_CA_Setting;
        
        public AddressData(Contact contact,CASUB_Address_Notification_CA_Setting__mdt casub_Address_Notification_CA_Setting){
            this.account = new Account(
                Id = contact.Account.Id, 
                Mailing_and_Residential_is_the_same__c = contact.Account.Mailing_and_Residential_is_the_same__c,
                Mailing_Company_Name__c = contact.Account.Mailing_Company_Name__c,
                NZICA_confirmation__pc = contact.Account.NZICA_confirmation__pc
            );  
            this.contact = new Contact(
                Id = contact.Id, OtherStreet = contact.OtherStreet, OtherCity = contact.OtherCity, 
                OtherState = contact.OtherState, OtherStateCode = contact.OtherStateCode, 
                OtherCountry = contact.OtherCountry, OtherCountryCode = contact.OtherCountryCode, 
                OtherPostalCode = contact.OtherPostalCode, MailingStreet = contact.MailingStreet, 
                MailingCity = contact.MailingCity, MailingState = contact.MailingState, 
                MailingStateCode = contact.MailingStateCode, MailingCountry = contact.MailingCountry, 
                MailingCountryCode = contact.MailingCountryCode, MailingPostalCode = contact.MailingPostalCode
            );
            this.casub_Address_Notification_CA_Setting = new CASUB_Address_Notification_CA_Setting__mdt(
                MasterLabel = casub_Address_Notification_CA_Setting.MasterLabel,
                Confirmation_Message__c=casub_Address_Notification_CA_Setting.Confirmation_Message__c,
                Is_Active__c=casub_Address_Notification_CA_Setting.Is_Active__c,
                Regulatory_Requirements_Web_Link__c=casub_Address_Notification_CA_Setting.Regulatory_Requirements_Web_Link__c
            );
        }
    }
    
    public static String getCurrentYear() {
        String currentYear='';
        List<CASUB_Mandatory_Notification_Config__mdt> casub_Mandatory_Notification_ConfigList = [select Current_Year__c from CASUB_Mandatory_Notification_Config__mdt where DeveloperName = 'Current_Year'];
        if(casub_Mandatory_Notification_ConfigList!=null && casub_Mandatory_Notification_ConfigList.size()>0){
            currentYear = casub_Mandatory_Notification_ConfigList.get(0).Current_Year__c;             
        }
        return currentYear;
    }
    private class Country {
        public String label;
        public String value;
        public List<State> states;
        
        public void addState(State state){
            if(states == null) states = new List<State>(); 
            states.add(state);
        }
    }
    
    private class State {
        public String label;
        public String value;
        public String validFor;
        
        private List<Integer> getCountryIndexes(){
            List<Integer> indexes = new List<Integer>();
            String validForBits = base64ToBits(validFor);
            for (Integer i = 0; i < validForBits.length(); i++) {
                String bit = validForBits.mid(i, 1);
                if (bit == '1') indexes.add(i); 
            }
            return indexes;
        }
        
        private String base64ToBits(String validFor) {
            if(String.isEmpty(validFor)) return '';
            String validForBits = '';
            for (Integer i = 0; i < validFor.length(); i++) {
                String thisChar = validFor.mid(i, 1);
                Integer val = base64Chars.indexOf(thisChar);
                String bits = decimalToBinary(val).leftPad(6, '0');
                validForBits += bits;
            }
            return validForBits;
        }
        
        private String decimalToBinary(Integer val) {
            String bits = '';
            while (val > 0) {
                Integer remainder = Math.mod(val, 2);
                val = Integer.valueOf(Math.floor(val / 2));
                bits = String.valueOf(remainder) + bits;
            }
            return bits;
        }
    }
}