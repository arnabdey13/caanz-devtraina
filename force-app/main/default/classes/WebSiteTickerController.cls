public with sharing class WebSiteTickerController {
	List<Ticker> listTickers = new List<Ticker>();
	public List<Ticker> getWebsiteTickers()
	{
		for(Website_Ticker__c w:[select Name, Link__c, Background_Image__c 
					  from Website_Ticker__c where Active__c = true 
					  order by Order__c asc
					  limit 200]) {
			Ticker t = new Ticker();
			t.websiteTicker = w;
			t.strCaption = '<a href="' + w.Link__c + '" target="_top" style="color:#FFF;text-decoration: none;border:0!important;">' + w.Name + '</a>'; 
			listTickers.add(t);
		}
		return listTickers;
	}
	public class Ticker {
		public String strCaption {get;set;}
		public Website_Ticker__c websiteTicker {get;set;}
	}
}