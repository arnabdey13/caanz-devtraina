/*
    Developer: WDCi (Lean)
    Date: 22/Mar/2016
    Task #: Luana implementation
    
    Change History:
    LCA-615 08/06/2016: WDCi Lean - this is obsolete
*/

@isTest
private class luana_AccreditedProgramControllerTest {
    /*LCA-615
    private static String classNamePrefixLong = 'luana_AccreditedProgramControllerTest';
    private static String classNamePrefixShort = 'lapc';
    private static map<String, Id> commProfIdMap {get; set;}
    public static Account memberAccount {get; set;}
    
    static testMethod void testViewPage(){
        Test.startTest();
        
        Luana_DataPrep_Test dataPrep = new Luana_DataPrep_Test();
        
        //Create traning org
        LuanaSMS__Training_Organisation__c trainingOrg = dataPrep.createNewTraningOrg(classNamePrefixLong, classNamePrefixLong, classNamePrefixShort, 'St 123', 'Wollonggong', '2244');
        insert trainingOrg;

        //Create program
        LuanaSMS__Program__c progrm = dataPrep.createNewProgram(classNamePrefixShort, classNamePrefixLong, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        insert progrm;
        
        List<Product2> prodList = new List<Product2>();
        prodList.add(dataPrep.createNewProduct('FIN_AU', 'AU0001'));
        prodList.add(dataPrep.createNewProduct('FIN_NZ', 'NZ0001'));
        prodList.add(dataPrep.createNewProduct('FIN_INT', 'INT0001'));
        insert prodList;
        
        //create price book entry
        /\\* Lean - 20/05/2016 - disabling to avoid lock contention
        List<PricebookEntry> pbes = new List<PricebookEntry>();
        pbes.add(dataPrep.createPricebookEntry(Test.getStandardPricebookId(), prodList[0].Id, 1300));
        pbes.add(dataPrep.createPricebookEntry(Test.getStandardPricebookId(), prodList[1].Id, 1300));
        pbes.add(dataPrep.createPricebookEntry(Test.getStandardPricebookId(), prodList[2].Id, 1300));
        *\\/
        
        //Create Program Offering
        LuanaSMS__Program_Offering__c po = dataPrep.createNewProgOffering('PO_AP_' + classNamePrefixLong, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Program'), progrm.Id, trainingOrg.Id, prodList[0].Id,prodList[1].Id, prodList[2].Id, 1, 1);
        insert po;
        
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(progrm);
        luana_AccreditedProgramController ctrl = new luana_AccreditedProgramController(stdCtrl);
        
        //Cannot test this from test class
        //system.assert(ctrl.doNewPO() != null, 'This shouldnt be null. ');
        
        Test.stopTest();
    }
    LCA-615*/
}