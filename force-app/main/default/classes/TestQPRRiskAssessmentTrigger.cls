@isTest
public class TestQPRRiskAssessmentTrigger {
    @testSetup
    Static void setupTestData(){
         RecordType typeBusiness = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Business_Account' LIMIT 1];
        RecordType typeMember = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Full_Member' LIMIT 1];
        List<Account> listAccounts = new List<Account>();
        Account personAccount = new Account();
        personAccount.RecordType = typeMember;
        personAccount.FirstName = 'Test';
        personAccount.LastName = 'Practice Contact';
        personAccount.Member_Of__c = 'ICAA';
        personAccount.Gender__c = 'Male';
        personAccount.Salutation = 'Mr';
        personAccount.PersonEmail = 'testpracticecontact@example.com';
        personAccount.Create_Portal_User__c = false;
        personAccount.Communication_Preference__c= 'Home Phone';
        personAccount.PersonHomePhone= '1234';
        personAccount.PersonOtherStreet= '83 Saggers Road';
        personAccount.PersonOtherCity='JITARNING';
        personAccount.PersonOtherState='Western Australia';
        personAccount.PersonOtherCountry='Australia';
        personAccount.PersonOtherPostalCode='6365'; 
        personAccount.PersonMailingCountry='Australia';
           personAccount.Affiliated_Branch_Country__c='Australia';
          personAccount.Is_QPR_Referral_Contact__c=true;
        listAccounts.add(personAccount);
          Account acct = new Account();
        acct.RecordType = typeBusiness;
        acct.Name = 'Test Account.ABC';
        acct.Type = 'Unallocated';
        acct.Phone = '6421771557';
        acct.Member_Of__c = 'ICAA';
        acct.BillingStreet = '33 Customhouse Quay';
        acct.BillingCity = 'Wellington';
        acct.BillingCountry = 'New Zealand';
        acct.Review_Contact__c = personAccount.Id;
        

        listAccounts.add(acct);   
        insert listAccounts;
        List<QPR_Risk_Assessment__c> qprRiskAssessmentList  = new List<QPR_Risk_Assessment__c>();
        QPR_Risk_Assessment__c qprRiskAssessmentRecord1  = new QPR_Risk_Assessment__c();
        qprRiskAssessmentRecord1.Risk_Driver__c = 'Monitoring';
       	qprRiskAssessmentRecord1.Risk_Weighting_1_5__c =2;
        qprRiskAssessmentRecord1.Risk_Assessment_Date__c = Date.Today();
  	    qprRiskAssessmentRecord1.Risk_Summary__c ='test qprref trigger1';
         qprRiskAssessmentRecord1.Account__c = acct.Id;
        qprRiskAssessmentList.add(qprRiskAssessmentRecord1);
        QPR_Risk_Assessment__c qprRiskAssessmentRecord2  = new QPR_Risk_Assessment__c();
        qprRiskAssessmentRecord2.Risk_Driver__c = 'Monitoring';
       	qprRiskAssessmentRecord2.Risk_Weighting_1_5__c =3;
        qprRiskAssessmentRecord2.Risk_Assessment_Date__c = Date.Today();
       qprRiskAssessmentRecord2.Risk_Summary__c ='test qprref trigger2';
         qprRiskAssessmentRecord2.Member_Name__c = personAccount.Id;
        qprRiskAssessmentList.add(qprRiskAssessmentRecord2);
        
        insert qprRiskAssessmentList;
    }
 @isTest
    Static void testCreateQPRRiskAssessment(){
        List<QPR_Risk_Assessment__c> qprRiskAssessmentList  = new List<QPR_Risk_Assessment__c>();
        Account personAccount = [SELECT Id, FirstName, LastName FROM Account WHERE PersonEmail = 'testpracticecontact@example.com' LIMIT 1];
        Account businessAccount = [SELECT Id, FirstName, LastName FROM Account WHERE Name = 'Test Account.ABC' LIMIT 1];
           Date currentDate  =Date.Today();
       for( QPR_Risk_Assessment__c  qprRiskAssessmentRecord1 : [SELECT Id, Risk_Summary__c, Member_Name__c 
                                                  FROM QPR_Risk_Assessment__c WHERE Risk_Assessment_Date__c = :currentDate  and Risk_Weighting_1_5__c =2 and Risk_Driver__c = 'Monitoring' LIMIT 1])
       {
             qprRiskAssessmentRecord1.Member_Name__c = personAccount.Id;
             qprRiskAssessmentList.add( qprRiskAssessmentRecord1);
           update qprRiskAssessmentRecord1;
       }
        QPR_Risk_Assessment__c qprRiskAssessment1  = new QPR_Risk_Assessment__c();
        qprRiskAssessment1.Risk_Driver__c = 'Notification';
       	qprRiskAssessment1.Risk_Weighting_1_5__c =2;
        qprRiskAssessment1.Risk_Assessment_Date__c = Date.Today();
        qprRiskAssessment1.Member_Name__c = personAccount.Id;
        qprRiskAssessmentList.add(qprRiskAssessment1);
        QPR_Risk_Assessment__c qprRiskAssessment_2  = new QPR_Risk_Assessment__c();
        qprRiskAssessment_2.Risk_Driver__c = 'Notification';
       	qprRiskAssessment_2.Risk_Weighting_1_5__c =2;
        qprRiskAssessment_2.Risk_Assessment_Date__c = Date.Today();
        qprRiskAssessment_2.Account__c = personAccount.Id;
        qprRiskAssessmentList.add(qprRiskAssessment_2);
        upsert  qprRiskAssessmentList;
    }
     @isTest
    Static void testUpdateQPRRiskAssessment(){
        List<QPR_Risk_Assessment__c> qprRiskAssessmentList  = new List<QPR_Risk_Assessment__c>();
        Account personAccount = [SELECT Id, FirstName, LastName FROM Account WHERE PersonEmail = 'testpracticecontact@example.com' LIMIT 1];
        Account businessAccount = [SELECT Id, FirstName, LastName FROM Account WHERE Name = 'Test Account.ABC' LIMIT 1];
          Date currentDate  =Date.Today();
     for( QPR_Risk_Assessment__c  qprRiskAssessmentRecord2 : [SELECT Id, Risk_Summary__c, Member_Name__c 
                                                  FROM QPR_Risk_Assessment__c WHERE Risk_Assessment_Date__c = :currentDate  and Risk_Weighting_1_5__c =3 and Risk_Driver__c = 'Monitoring' LIMIT 1])  {
              qprRiskAssessmentRecord2.Account__c = businessAccount.Id;
         qprRiskAssessmentList.add( qprRiskAssessmentRecord2);
          
        QPR_Risk_Assessment__c qprRiskAssessmentRecord_6  = new QPR_Risk_Assessment__c();
        qprRiskAssessmentRecord_6.Risk_Driver__c = 'Notification';
       	qprRiskAssessmentRecord_6.Risk_Weighting_1_5__c =3;
        qprRiskAssessmentRecord_6.Risk_Assessment_Date__c = Date.Today();
        qprRiskAssessmentRecord_6.Member_Name__c = businessAccount.Id;
        qprRiskAssessmentList.add(qprRiskAssessmentRecord_6);
          //        qprRiskAssessmentList.add( qprRiskAssessmentRecord1);
       }
          QPR_Risk_Assessment__c qprRiskAssessmentRecord_5  = new QPR_Risk_Assessment__c();
        qprRiskAssessmentRecord_5.Risk_Driver__c = 'Notification';
       	qprRiskAssessmentRecord_5.Risk_Weighting_1_5__c =3;
        qprRiskAssessmentRecord_5.Risk_Assessment_Date__c = Date.Today();
        qprRiskAssessmentRecord_5.Member_Name__c = personAccount.Id;
        qprRiskAssessmentList.add(qprRiskAssessmentRecord_5);
       
     
        upsert  qprRiskAssessmentList;
    }
}