/***********************************************************************************************************************************************************************
Name: Account_CPDMemberHandler
============================================================================================================================== 
Purpose: This Class contains the code related to CPD and Member Functionality invoked from the AccountTriggerHandler 
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR                 DATE          DETAIL     Description 
1.0        Rama Krishna    04/02/2019    Created          This Class contains the code related to CPD Functionality invoked from the AccountTriggerHandler 
                                                             
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/
public class Account_CPDMemberHandler{
      public static List<Account> accounts;
      // The following method Updates Unpaid Finecost and Professional conduct Flags for member account records
      public static void CPDMemberChanges(List<Account> newAccountList,Map<Id,Account> oldAccountMap){                        
            Set<Id> UnpaidFineCost_Set = new Set<Id>();
            Set<Id> AUPC_Set = new Set<Id>();
            Set<Id> NZPC_Set = new Set<Id>();           
            Set<String> AccountId_Set = new Set<String>();
            
            for(Account AccountObj : newAccountList){                
                if(AccountObj.RollUpFromProfessionalConductAndFineCost__c){                                    
                    AccountObj.RollUpFromProfessionalConductAndFineCost__c = false;
                    AccountId_Set.add(AccountObj.Id);                                               
                }
               
            }
                         
            if(AccountId_Set.size() > 0){            
                List<Conduct_Support__c> PC_List = [Select Id, Member__c, Country__c, Overall_Status__c, Complaint__c, Outcome__c FROM Conduct_Support__c WHERE Member__c IN : AccountId_Set];          
                List<Professional_Conduct_Tribunal__c> PCT_List = [Select Id FROM Professional_Conduct_Tribunal__c WHERE Professional_Conduct__c IN : PC_List];            
                List<Appeal_Tribunal__c> AT_List = [Select Id FROM Appeal_Tribunal__c WHERE Professional_Conduct__c IN : PC_List];                        
                List<Fine_Cost__c> Fine_Cost_List = [SELECT Id, Professional_Conduct_Tribunal__c, Appeal_Tribunal__c, Cost_Recovery_Status__c, Member_Formula__c
                     FROM Fine_Cost__c
                     WHERE (Professional_Conduct_Tribunal__c IN : PCT_List OR
                     Appeal_Tribunal__c IN : AT_List) AND
                     Cost_Recovery_Status__c != :Label.CostRecoveryStatusPaid_in_Full ];     
                                                                                                      
                for(Conduct_Support__c PC : PC_List){        
                    if(PC.Country__c == Label.CountryAU && PC.Overall_Status__c == Label.StatusOpen && PC.Member__c != null){            
                        AUPC_Set.add(PC.Member__c);            
                    }
                    else if(PC.Country__c == Label.CountryNZ && PC.Complaint__c == true && PC.Member__c != null
                    ){
                        NZPC_Set.add(PC.Member__c);
                    }                   
                }
                
                for(Fine_Cost__c FineCost : Fine_Cost_List){            
                    system.debug('********fine cost********' + FineCost);
                    if(FineCost.Member_Formula__c != null){       
                        UnpaidFineCost_Set.add(FineCost.Member_Formula__c);                
                    }                        
                }
                
                for(Account AccountObj : newAccountList){
                    // Sets Unpaid Fine Cost Flag to true for Account record
                    if(UnpaidFineCost_Set.contains(AccountObj.Id)){            
                        AccountObj.Unpaid_Fine_Cost_Flag_Checkbox__c = true;            
                    }
                    else{     
                    // Sets Unpaid Fine Cost Flag to false for Account record       
                        AccountObj.Unpaid_Fine_Cost_Flag_Checkbox__c = false;            
                    }
                    
                    if(AUPC_Set.contains(AccountObj.Id)){  
                    //Updates Professional Conduct Flag to true for Account record for the Country Australia          
                        AccountObj.Professional_Conduct_Flag_AU_Checkbox__c = true;            
                    } 
                    else{     
                    //Updates Professional Conduct Flag to false for Account record for the Country Australia       
                        AccountObj.Professional_Conduct_Flag_AU_Checkbox__c = false;            
                    }
                    
                    if(NZPC_Set.contains(AccountObj.Id)){  
                     //Updates Professional Conduct Flag to true for Account record for the Country NewZealand          
                        AccountObj.Professional_Conduct_Flag_NZ_Checkbox__c = true; 
                        //SYSTEM.DEBUG('SFDC DEBUG AccountObj.Professional_Conduct_Flag_NZ_Checkbox__c = true');                     
                    } 
                    else{   
                    //Updates Professional Conduct Flag to false for Account record for the Country NewZealand         
                         AccountObj.Professional_Conduct_Flag_NZ_Checkbox__c = false;            
                         //SYSTEM.DEBUG('SFDC DEBUG AccountObj.Professional_Conduct_Flag_NZ_Checkbox__c = false');                
                    }         
                }               
            }
            
            //set Account Specialisations from CPD Summary records
            if(!accounts.isEmpty()){                
                CPDSummaryHandler.setAccontSpecialisations(accounts);
            }
            
        }
      //Create missing account summary records and update summary specialisations
       public static void updateSummarySpecialisations(List<Account> newAccountList,Map<Id,Account> oldAccountMap){
            //List<Account> accounts = CPDSummaryHandler.filterSummaryAccounts(newAccountList);
            if(!accounts.isEMpty()){
                //Update summary specialisations
                List<Account> updatableSummaryAccounts = new List<Account>();
                for(Account acc : accounts){
                    //Check if account specialisations changed
                    if(CPDSummaryHandler.isAccountSpecialisationsChanged(acc, oldAccountMap.get(acc.Id))){
                        updatableSummaryAccounts.add(acc);
                    }
                }
                if(!updatableSummaryAccounts.isEmpty()){
                    CPDSummaryHandler.updateSummarySpecialisations(updatableSummaryAccounts);
                }
                //Create missing account summary records
                CPDSummaryHandler.createMissingCPDSummaryRecords(accounts);
            }    
        }
        
}