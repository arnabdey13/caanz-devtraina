/*
    Developer: WDCi (KH)
    Date: 30/08/2016
    Task #: Test class for luana_SuppExamBroadcastController
    
*/
@isTest(seeAllData=false)
private class luana_AT_SuppExamSearcCtl_Test {
    
    final static String contextLongName = 'luana_AT_SuppExamSearcCtl_Test';
    final static String contextShortName = 'lases';
    private static map<String, Id> commProfIdMap {get; set;}
    
    public static User memberUser {get; set;}
    public static Account memberAccount {get; set;}
    public static luana_CommUserUtil userUtil {get; set;} 
    
    public static LuanaSMS__Course__c course1;
    public static LuanaSMS__Course__c course2;
    public static LuanaSMS__Course__c course3;
    public static LuanaSMS__Course__c course4;
    
    public static LuanaSMS__Session__c newSession1;
    public static LuanaSMS__Session__c newSession2;
    public static LuanaSMS__Session__c newSession3;
    
    public static Luana_DataPrep_Test dataPrep {get; set;}
    
    static void initial(){
        //initialize
        dataPrep = new Luana_DataPrep_Test();
        
        //Create all the custom setting
         
        insert dataPrep.createLuanaConfigurationCustomSetting();
        
        List<Luana_Extension_Settings__c> customSessingList = dataPrep.prepLuanaExtensionSettingCustomSettings();
        customSessingList.add(new Luana_Extension_Settings__c(Name = 'AdobeConnect_URL', Value__c = '  https://connect-staging.charteredaccountantsanz.com'));
        customSessingList.add(new Luana_Extension_Settings__c(Name = 'Supp Exam Search Report Params', Value__c = '000000000000000'));
        
        insert customSessingList;
        
        //Create user with Member and Employer community access
        memberAccount = dataPrep.generateNewApplicantAcc('Joe_' + contextShortName, contextLongName, 'Full_Member');
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.Assessible_for_CA_Program__c = true;
        memberAccount.PersonEmail = 'joe_1_' + contextShortName +'@gmail.com';//LCA-921
        insert memberAccount;
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
    }
    
    static void setup(String runSeqKey){
        
        //LCA-921
        memberUser = [Select Id, Email, FirstName, LastName, UserName, Name from User Where Email =: 'joe_1_' + contextShortName+'@gmail.com' limit 1];
        //memberUser = dataPrep.generateNewApplicantUser(contextLongName, contextShortName, memberAccount, commProfIdMap.get('Customer Community Login User'));
        //insert memberUser;
        
        userUtil = new luana_CommUserUtil(memberUser.Id);
        
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
        Luana_Extension_Settings__c defContriPermSetId = Luana_Extension_Settings__c.getValues('Default_ContCom_PermSet_Id');
       
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defEmployerPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defContriPermSetId.value__c));
        
        System.runAs(memberUser){
            insert psas;
        }
        
        LuanaSMS__Training_Organisation__c trainingOrg = dataPrep.createNewTraningOrg(contextLongName, contextLongName, contextShortName, 'Virtual Street 123', 'Queensland', '4551');
        insert trainingOrg;
        LuanaSMS__Program__c program = dataPrep.createNewProgram(contextShortName, contextLongName, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        insert program;
        LuanaSMS__Program_Offering__c po = dataPrep.createNewProgOffering('po' + contextShortName, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), program.Id, trainingOrg.Id, null, null, null, 1, 1);
        insert po;
        
        List<LuanaSMS__Course__c> courses = new List<LuanaSMS__Course__c>();
        course1 = dataPrep.createNewCourse('Graduate Diploma of Workshop1 Accounting_' + contextShortName, po.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        course1.LuanaSMS__Allow_Online_Enrolment__c = true;
        course1.Supp_Exam_Eligible_Score__c = 10;
        course1.Supp_Exam_End_Range__c = 50;
        courses.add(course1);
        
        insert courses;
        
        List<LuanaSMS__Student_Program__c> stuPrograms = new List<LuanaSMS__Student_Program__c>();
        LuanaSMS__Student_Program__c newStudProg1 = dataPrep.createNewStudentProgram(dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), userUtil.custCommConId, course1.Id, '', 'Fail');
        newStudProg1.Paid__c = true;
        newStudProg1.Supp_Exam_Broadcasted__c = false;
        newStudProg1.Sup_Exam_Enrolled__c = false;
        newStudProg1.Supp_Exam_Eligible__c = false;
        newStudProg1.Supp_Exam_Special_Consideration_Approved__c = null;
        stuPrograms.add(newStudProg1);
        insert stuPrograms;
        
        LuanaSMS__Subject__c sub = dataPrep.createNewSubject('ATDAT', 'ATDAT', 'ATDAT', 10, 'ATDAT');
        insert sub;
        
        LuanaSMS__Student_Program_Subject__c sps = dataPrep.createNewStudentProgramSubject(userUtil.custCommConId, stuPrograms[0].Id, sub.Id);
        sps.score__c = 40.0;
        sps.LuanaSMS__Outcome_National__c = 'Competency not achieved/fail';
        insert sps;

        List<Account> venues = new List<Account>();
        Account venueAccount1 = dataPrep.generateNewBusinessAcc(contextShortName+ 'Test Venue_'+runSeqKey+'_1', 'Venue', null, null, null, null, 'Sunshine Coast Office', 'Active');
        venueAccount1.billingCity = 'city';
        venueAccount1.billingPostalCode = '1234';
        venueAccount1.billingCountry = 'Malaysia';
        venueAccount1.Ext_Id__c = contextShortName+'_'+runSeqKey+'_1';
        venues.add(venueAccount1);
        
        Account venueAccount2 = dataPrep.generateNewBusinessAcc(contextShortName+ 'Test Venue_'+runSeqKey+'_2', 'Venue', null, null, null, null, 'Sunshine Coast Office', 'Active');
        venueAccount2.billingCity = 'city';
        venueAccount2.billingPostalCode = '1234';
        venueAccount2.billingCountry = 'Australia';
        venueAccount2.billingState = 'New South Wales';
        venueAccount2.Ext_Id__c = contextShortName+'_'+runSeqKey+'_2';
        venues.add(venueAccount2);
        
        insert venues;
        
        List<LuanaSMS__Room__c> rooms = new List<LuanaSMS__Room__c>();
        LuanaSMS__Room__c room1 = dataPrep.newRoom(contextShortName, contextLongName, venueAccount1.Id, 20, 'Exam', runSeqKey+'_1');
        LuanaSMS__Room__c room2 = dataPrep.newRoom(contextShortName, contextLongName, venueAccount2.Id, 20, 'Exam', runSeqKey+'_2');
        rooms.add(room1);
        rooms.add(room2);
        insert rooms;
        
        List<LuanaSMS__Session__c> sessions = new List<LuanaSMS__Session__c>();
        newSession1 = dataPrep.newSession(contextShortName, contextLongName, 'Exam', 20, venueAccount1.Id, room1.Id, null, system.now().addHours(1), system.now().addHours(2));
        newSession1.Topic_Key__c = contextShortName+ '_T1';
        sessions.add(newSession1);
        
        newSession2 = dataPrep.newSession(contextShortName, contextLongName, 'Supplementary_Exam', 20, venueAccount2.Id, room2.Id, null, system.now().addHours(1), system.now().addHours(2));
        newSession2.Topic_Key__c = contextShortName+ '_T1';
        sessions.add(newSession2);
        
        insert sessions;
        
        List<LuanaSMS__Attendance2__c> attendances = new List<LuanaSMS__Attendance2__c>();
        LuanaSMS__Attendance2__c att1 = dataPrep.createAttendance(userUtil.custCommConId, 'Exam', newStudProg1.Id, newSession1.Id);
        attendances.add(att1);
        LuanaSMS__Attendance2__c att2 = dataPrep.createAttendance(userUtil.custCommConId, 'Supplementary_Exam', newStudProg1.Id, newSession2.Id);
        attendances.add(att2);

        insert attendances;
        
        Case newCase = dataPrep.createCase('Support', 'New', dataPrep.getRecordTypeIdMap('Case').get('Request_Special_Consideration'), 'General Enquiry', 'Supplementary Exam Request', 'Phone');
        newCase.Student_Program__c = stuPrograms[0].id;
        insert newCase;
    }
    
    
    /*
        Broadcast supplementary success
    */
    static testMethod void testSuppExamSearch(){
        
        Test.startTest();
            initial();
        Test.stopTest();
        
        setup('1');
        
        //Create and link mock http class
        Test.setMock(HttpCalloutMock.class, new luana_AT_VCImportWizardMockHttp_Test());
        //Main Test Area
       
        PageReference pageRef = Page.luana_SearchSuppExams;
        
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController suppExamSearch = new ApexPages.StandardController(course1);
        luana_SuppExamSearchCtl suppExamCtl = new luana_SuppExamSearchCtl(suppExamSearch); 
        
        suppExamCtl.getStuProgramSubjs();
        suppExamCtl.first();
        suppExamCtl.last();
        suppExamCtl.previous();
        suppExamCtl.next();
        suppExamCtl.redirectBack();
        suppExamCtl.doUpdate();
       
        
    }
    

}