/**
* @author           WDCi-LKoh
* @date             26/06/2019
* @group            Education History
* @description      Trigger Handler for EducationHistoryTrigger
* @change-history
*/
public with sharing class wdci_EducationHistoryHandler {

    public static void initFlow(List<edu_Education_History__c> newList, List<edu_Education_History__c> oldList, Map<Id, edu_Education_History__c> oldMap, boolean isInsert, boolean isUpdate, boolean isDelete, boolean isUndelete, boolean isBefore, boolean isAfter) {
        
        system.debug('Entering initFlow: ' +newList+ ' : ' +oldList+ ' : ' +oldMap+ ' : ' +isInsert+ ' : ' +isUpdate+ ' : ' +isDelete+ ' : ' +isUndelete+ ' : ' +isBefore+ ' : ' +isAfter);
        if (isBefore) {
            if (isDelete) {
                deleteExistingStudentPaper(oldMap);
            }
        }
        system.debug('Exiting initFlow');
    }

    public static void deleteExistingStudentPaper(Map<Id, edu_Education_History__c> eduHistoryMap) {

        // if the Education History have any Student Paper linked to it, delete these Student Papers
        Set<Id> eduHistoryIdSet = eduHistoryMap.keySet();

        List<FT_Student_Paper__c> relatedStudentPaperList = [SELECT Id, FT_Education_History__c FROM FT_Student_Paper__c WHERE FT_Education_History__c IN :eduHistoryIdSet];
        if (relatedStudentPaperList.size() > 0) {
            delete relatedStudentPaperList;
        }
    }
}