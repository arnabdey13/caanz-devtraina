/*------------------------------------------------------------------------------------
Author:        Sumit Gupta
Company:       Salesforce
Description:   Content kb trigger handler

History
Date            Author             Comments
--------------------------------------------------------------------------------------
04-06-2018     Sumit Gupta          Initial Release
------------------------------------------------------------------------------------*/
public with sharing class ContentKBTriggerHandler {
/*
    private static String OBJECT_TYPE_KNOWLEDGE = 'KnowledgeArticleVersion';
    private static String DATA_CATEGORY_GROUP_TOPIC = 'Topic';
    private static String DATA_CATEGORY_GROUP_SUBTOPIC = 'SubTopic';
    private static String DATA_CATEGORY_GROUP_LOCATION = 'Location';

    public static Set<String> DATA_CATEGORY_LIST_TOPIC;
    public static Set<String> DATA_CATEGORY_LIST_SUBTOPIC;
    public static Set<String> DATA_CATEGORY_LIST_LOCATION; 

    private static String REGEXP_SPLCHARS = '[^0-9a-zA-Z]+';

    static{
        DATA_CATEGORY_LIST_TOPIC = new Set<String>();
        DATA_CATEGORY_LIST_SUBTOPIC = new Set<String>();
        DATA_CATEGORY_LIST_LOCATION = new Set<String>();

        List<DataCategoryGroupSObjectTypePair> pairs = new List<DataCategoryGroupSObjectTypePair>{
            //getDataCategoryGroupSObjectTypePair(DATA_CATEGORY_GROUP_TOPIC),
            getDataCategoryGroupSObjectTypePair(DATA_CATEGORY_GROUP_SUBTOPIC),
            getDataCategoryGroupSObjectTypePair(DATA_CATEGORY_GROUP_LOCATION)
        };
        List<DescribeDataCategoryGroupStructureResult> categoryGroups = Schema.describeDataCategoryGroupStructures(pairs, false);
        for(DescribeDataCategoryGroupStructureResult categoryGroup : categoryGroups){
            setDataCategories(categoryGroup.getName(), categoryGroup.getTopCategories());
        }
    }

    private static DataCategoryGroupSObjectTypePair getDataCategoryGroupSObjectTypePair(String categoryGroupName){
        DataCategoryGroupSObjectTypePair pair = new DataCategoryGroupSObjectTypePair();
        pair.setSobject(OBJECT_TYPE_KNOWLEDGE);
        pair.setDataCategoryGroupName(categoryGroupName);
        return pair;
    }

    private static void setDataCategories(String categoryGroupName, List<Schema.DataCategory> topCategories){
        Set<String> categories = categoryGroupName == DATA_CATEGORY_GROUP_TOPIC ? DATA_CATEGORY_LIST_TOPIC : 
            categoryGroupName == DATA_CATEGORY_GROUP_SUBTOPIC ?  DATA_CATEGORY_LIST_SUBTOPIC : DATA_CATEGORY_LIST_LOCATION;

        for(Schema.DataCategory category : topCategories){
            categories.add(category.getName());
            setDataCategories(categoryGroupName, category.getChildCategories());
        }
    }

    public static void createArticles(List<Content_KB__c> contents){
        List<Content__kav> articles = new List<Content__kav>();
        Map<String, ContentArticle> contentArticlesMap = new Map<String, ContentArticle>();

        for(Content_KB__c content : contents){
            ContentArticle contentArticle = new ContentArticle(content);
            articles.add(contentArticle.article);
            contentArticlesMap.put(content.Name, contentArticle);
        }
        insert articles;

        Set<Id> knowledgeArticleIds = new Set<Id>();
        List<Content__DataCategorySelection> dataCategories = new List<Content__DataCategorySelection>();

        for(Content__kav article : [SELECT Id, Title, KnowledgeArticleId, IsVisibleInCsp FROM Content__kav WHERE Id IN : articles]){
            ContentArticle contentArticle = contentArticlesMap.get(article.Title);
            contentArticle.updateArticle(article);
            dataCategories.addAll(contentArticle.dataCategories);
            //add to publish list
            if(article.IsVisibleInCsp) {
                knowledgeArticleIds.add(article.KnowledgeArticleId);
            }
        }
        if(!dataCategories.isEmpty()){
            insert dataCategories;
        }
        //publish articles
        if(!knowledgeArticleIds.isEmpty()){
            publishArticles(knowledgeArticleIds, true);
        }
    }

    public static List<Content_KB__c> getUpdatableArticleContents(List<Content_KB__c> newContents, Map<Id, Content_KB__c> oldContents){
        List<Content_KB__c> contents = new List<Content_KB__c>();
        for(Content_KB__c newContent : newContents){
            Content_KB__c oldContent = oldContents.get(newContent.Id);
            if(newContent.Article_ID__c != null && newContent.Published_Date__c != null &&
                newContent.Article_ID__c == oldContent.Article_ID__c && oldContent.Published_Date__c == newContent.Published_Date__c){
                contents.add(newContent);
            }
        }
        return contents;
    }

    public static void updateArticles(List<Content_KB__c> contents){
        Map<Id, Content_KB__c> contentsMap = new Map<Id, Content_KB__c>();
        for(Content_KB__c content : contents){
            contentsMap.put(content.Article_ID__c, content);
        }
        Set<Id> knowledgeArticleIds = new Set<Id>();
        Map<Id, Content__kav> articlesMap = new Map<Id, Content__kav>();
        List<Content__DataCategorySelection> dataCategories = new List<Content__DataCategorySelection>();

        for(Content__kav article : [SELECT Id, KnowledgeArticleId, PublishStatus, IsVisibleInCsp FROM Content__kav WHERE Id IN : contentsMap.keySet()]){
            ContentArticle contentArticle = new ContentArticle(contentsMap.get(article.Id));
            //unpublish article
            if(article.PublishStatus == 'Online'){
                article.Id = KbManagement.PublishingService.editOnlineArticle(article.KnowledgeArticleId, true);
            }
            articlesMap.put(article.Id, contentArticle.updateArticle(article));
            dataCategories.addAll(contentArticle.dataCategories);
            //add to publish list
            if(article.IsVisibleInCsp) {
                knowledgeArticleIds.add(article.KnowledgeArticleId);
            }
        }
        //update articles
        if(!articlesMap.isEmpty()){
            update articlesMap.values();
        }
        //delete existing data categories
        delete [SELECT Id FROM Content__DataCategorySelection WHERE ParentId IN : articlesMap.keySet()];
        //insert new data categories
        if(!dataCategories.isEmpty()){
            insert dataCategories;
        }
        //publish articles
        if(!knowledgeArticleIds.isEmpty() && !Test.isRunningTest()){
            publishArticles(knowledgeArticleIds, true);
        }
    }

    @future
    private static void publishArticles(Set<Id> knowledgeArticleIds, Boolean flagAsNew){
        Set<Id> pendingIds = new Set<Id>(knowledgeArticleIds);
        for(Id knowledgeArticleId : knowledgeArticleIds){
            KbManagement.PublishingService.publishArticle(knowledgeArticleId, flagAsNew);
            pendingIds.remove(knowledgeArticleId);
            if(Limits.getDMLRows() > 9000){
                publishArticles(pendingIds, flagAsNew); return;
            }
        }
    }

    /*@future
    private static void publishArticles(Map<Id, Id> articleIdsMap, Boolean flagAsNew){
        for(Id knowledgeArticleId : articleIdsMap.values()){
            KbManagement.PublishingService.publishArticle(knowledgeArticleId, flagAsNew);
        }
        Map<Id, Content__kav> articlesMap = new Map<Id, Content__kav>();
        for(Content__kav article : [SELECT Id, LastPublishedDate FROM Content__kav WHERE Id IN : articleIdsMap.keySet()]){
            articlesMap.put(article.Id, article);
        }
        List<Content_KB__c> updatableContents = new List<Content_KB__c>();
        for(Content_KB__c content : [SELECT Article_ID__c, Published_Date__c FROM Content_KB__c WHERE Article_ID__c IN : articleIdsMap.keySet()]){
            if(articlesMap.containsKey(content.Article_ID__c)){
                Content__kav article = articlesMap.get(content.Article_ID__c);
                content.Published_Date__c = String.valueOf(article.LastPublishedDate);
                updatableContents.add(content);
            }
        }
        if(!updatableContents.isEmpty()){
            update updatableContents;
        }
    }*/
/*
    public class ContentArticle{
        public Content_KB__c content;
        public Content__kav article;
        public List<Content__DataCategorySelection> dataCategories;

        public ContentArticle(Content_KB__c content){
            this.content = content; dataCategories = new List<Content__DataCategorySelection>();
            createArticle(); addDataCategories();
        }

        private void createArticle(){
            article = new Content__kav(
                Title = content.Name, 
                Summary = content.Summary__c, 
                Article_Link__c = content.Article_Link__c, 
                UrlName = content.Name.replaceAll(REGEXP_SPLCHARS, ''),
                IsVisibleInCsp = content.Active__c
            );
        }

        private void addDataCategories(){
            if(String.isNotBlank(content.Topic__c)){
                addDataCategories(DATA_CATEGORY_GROUP_TOPIC, DATA_CATEGORY_LIST_TOPIC, content.Topic__c.split(';'));
            }
            if(String.isNotBlank(content.Sub_Topic__c)){
                addDataCategories(DATA_CATEGORY_GROUP_SUBTOPIC, DATA_CATEGORY_LIST_SUBTOPIC, content.Sub_Topic__c.split(';'));
            }
            if(String.isNotBlank(content.Country__c)){
                addDataCategories(DATA_CATEGORY_GROUP_LOCATION, DATA_CATEGORY_LIST_LOCATION, content.Country__c.split(';'));
            }
        }

        public void addDataCategories(String dataCategoryGroupName, Set<String> validDataCategories, List<String> dataCategories){
            for(String dataCategoryName : dataCategories){
                dataCategoryName = dataCategoryName.trim();
                if(validDataCategories.contains(dataCategoryName)){
                    addDataCategories(dataCategoryGroupName, dataCategoryName);
                }
            }
        }

        public void addDataCategories(String dataCategoryGroupName, String dataCategoryName){
            dataCategories.add(new Content__DataCategorySelection(DataCategoryGroupName = dataCategoryGroupName, DataCategoryName = dataCategoryName)); 
        }

        public Content__kav updateArticle(Content__kav newArticle){
            article.Id = newArticle.Id;
            updateContent();
            updateDataCategories();
            return article;
        }

        public void updateContent(){
            content.Article_ID__c = article.Id;
        }

        private void updateDataCategories(){
            for(Content__DataCategorySelection dataCategory : dataCategories){
                dataCategory.ParentId = article.Id;
            }
        }
    }*/
}