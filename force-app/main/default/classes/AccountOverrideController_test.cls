/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AccountOverrideController_test {
	private static Account FullMemberAccountObjectInDB; // Person Account
	private static Id FullMemberContactIdInDB;
	//private static User PersonAccountPortalUserObjectInDB;
	static{
		insertFullMemberAccountObject();
		//insertPersonAccountPortalUserObject();
	}
	private static void insertFullMemberAccountObject(){
		FullMemberAccountObjectInDB = TestObjectCreator.createFullMemberAccount();
		//## Required Relationships
		//## Additional fields and relationships / Updated fields
		FullMemberAccountObjectInDB.Create_Portal_User__c = false;
		insert FullMemberAccountObjectInDB;
		FullMemberContactIdInDB = [Select id from Contact where AccountId=:FullMemberAccountObjectInDB.Id].Id;
	}
	
	/*private static void insertPersonAccountPortalUserObject(){
		PersonAccountPortalUserObjectInDB = TestObjectCreator.createPersonAccountPortalUser();
		//## Required Relationships
		PersonAccountPortalUserObjectInDB.ContactId = FullMemberContactIdInDB;
		PersonAccountPortalUserObjectInDB.Profileid = ProfileCache.getId('NZICA Community Login User');
		//## Additional fields and relationships / Updated fields
		insert PersonAccountPortalUserObjectInDB;
	}*/
	private static User getPersonAccountPortalUserObjectInDB(){
		Test.startTest();
		Test.stopTest(); // Future method needs to run to create the user.
		
		List<User> PersonAccountPortalUserObject_List = [Select Name from User 
			where AccountId=:FullMemberAccountObjectInDB.Id];
		System.assertEquals(1, PersonAccountPortalUserObject_List.size(), 'PersonAccountPortalUserObject_List.size' );
		return PersonAccountPortalUserObject_List[0];
	}
	
	//******************************************************************************************
	//                             TestMethods
	//******************************************************************************************
	static testMethod void AccountOverrideController_testViewPageForInternalUser() {
		ApexPages.StandardController Ctrl = new ApexPages.StandardController( FullMemberAccountObjectInDB );
		PageReference PR = Page.AccountViewOverride;
		PR.getParameters().put('id', FullMemberAccountObjectInDB.Id );
		Test.setCurrentPage( PR );
		AccountOverrideController CtrlExt = new AccountOverrideController( Ctrl );
		
		PageReference ViewPage = CtrlExt.gotoViewPage();
		System.assertNotEquals( null, ViewPage );
		System.assert( ViewPage.getUrl().startsWith('/'+ FullMemberAccountObjectInDB.Id), 'getUrl' );
	}
	
	static testMethod void AccountOverrideController_testViewPageForPortalUser() {
		User PersonAccountPortalUserObjectInDB = getPersonAccountPortalUserObjectInDB();
		System.runAs(PersonAccountPortalUserObjectInDB) {
			ApexPages.StandardController Ctrl = new ApexPages.StandardController( FullMemberAccountObjectInDB );
			PageReference PR = Page.AccountViewOverride;
			PR.getParameters().put('id', FullMemberAccountObjectInDB.Id );
			Test.setCurrentPage( PR );
			AccountOverrideController CtrlExt = new AccountOverrideController( Ctrl );
			
			PageReference ViewPage = CtrlExt.gotoViewPage();
			System.assertNotEquals( null, ViewPage );
			System.assertEquals( '/'+ Schema.sObjectType.Application__c.getKeyPrefix() +'/o', ViewPage.getUrl(), 'getUrl' );
		}
	}
	
	static testMethod void AccountOverrideController_testEditPageForInternalUser() {
		ApexPages.StandardController Ctrl = new ApexPages.StandardController( FullMemberAccountObjectInDB );
		PageReference PR = Page.AccountEditOverride;
		PR.getParameters().put('id', FullMemberAccountObjectInDB.Id );
		Test.setCurrentPage( PR );
		AccountOverrideController CtrlExt = new AccountOverrideController( Ctrl );
		
		PageReference EditPage = CtrlExt.gotoEditPage();
		System.assertNotEquals( null, EditPage );
		System.assert( EditPage.getUrl().startsWith('/'+ FullMemberAccountObjectInDB.Id + '/e'), 'getUrl' );
		// /001O000000LjtdbIAB/e?nooverride=1&retURL=%2F001O000000LjtdbIAB
	}
	
	static testMethod void AccountOverrideController_testEditPageForPortalUser() {
		User PersonAccountPortalUserObjectInDB = getPersonAccountPortalUserObjectInDB();
		System.runAs(PersonAccountPortalUserObjectInDB) {
			String FakeRecordId = 'Hu4rewxSIeWt';
			ApexPages.StandardController Ctrl = new ApexPages.StandardController( FullMemberAccountObjectInDB );
			PageReference PR = Page.AccountEditOverride;
			PR.getParameters().put('id', FullMemberAccountObjectInDB.Id );
			PR.getParameters().put('retURL', FakeRecordId );
			Test.setCurrentPage( PR );
			AccountOverrideController CtrlExt = new AccountOverrideController( Ctrl );
			
			PageReference EditPage = CtrlExt.gotoEditPage();
			System.assertNotEquals( null, EditPage );
			System.assertEquals( FakeRecordId, EditPage.getUrl(), 'getUrl' );
		}
	}
}