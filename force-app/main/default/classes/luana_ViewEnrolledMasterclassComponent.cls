/*
    Developer: WDCi (Lean)
    Development Date: 18/08/2016
    Task #: LCA-802 display CASM workshop/webinar session
    
    Change History:
    PAN:6222            20/05/2019      WDCi LKoh:  added session venue info and room info to the table for Masterclass, filtered the masterclass attendance to only show attendance for SP that are in progress and paid
*/

public without sharing class luana_ViewEnrolledMasterclassComponent {
    
    public String parentStudentProgramId;
    public String contactId;
    public String userTimezone {private set; get;}
    public String selectedAttendanceId {set; get;}
    
    public boolean enabledView;
        
    public luana_ViewEnrolledMasterclassComponent(){
        userTimezone = UserInfo.getTimeZone().getID();
    }
    
    public void setContactId(String contactId){
        this.contactId = contactId;
    }
    
    public String getContactId(){
        return this.contactId;
    }
    
    public void setParentStudentProgramId(String parentStudentProgramId){
        this.parentStudentProgramId = parentStudentProgramId;
    }
    
    public String getParentStudentProgramId(){
        return this.parentStudentProgramId;
    }
        
    public void setEnabledView(boolean enabledView){
        this.enabledView = enabledView;
    }
    
    public boolean getEnabledView(){
        return this.enabledView;
    }
    
    public List<LuanaSMS__Student_Program__c> getEnrolledMasterclasses(){
        return [Select Id, Name, Related_Student_Program__c, Related_Student_Program__r.LuanaSMS__Course__r.Name, LuanaSMS__Course__r.Name,
            LuanaSMS__Status__c, LuanaSMS__Program_Commencement_Date__c,LuanaSMS__Enrolment_Date__c
            from LuanaSMS__Student_Program__c Where LuanaSMS__Status__c != 'Cancelled' AND LuanaSMS__Status__c != 'Withdrawn/Cancelled' AND Related_Student_Program__c =: getParentStudentProgramId() and LuanaSMS__Contact_Student__c =: getContactId()];
    }
    
    // PAN:6222
    public List<LuanaSMS__Attendance2__c> getMasterclassAttendances(){
        return [Select Id, Name, CreatedDate, LuanaSMS__Attendance_Status__c, Adobe_Connect_Url__c, LuanaSMS__Start_time__c, LuanaSMS__End_time__c, LuanaSMS__Session__r.Name, LuanaSMS__Session__r.RecordType.DeveloperName, LuanaSMS__Session__r.Session_Start_Time_Venue__c, LuanaSMS__Session__r.Session_End_Time_Venue__c, 
        LuanaSMS__Session__r.LuanaSMS__Venue__r.Name, LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingStreet, LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingCity, LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingState, LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingPostalCode, LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingCountry,
        LuanaSMS__Session__r.LuanaSMS__Room__r.Name, LuanaSMS__Session__r.LuanaSMS__Room__c
        from LuanaSMS__Attendance2__c where LuanaSMS__Contact_Student__c =: getContactId() and LuanaSMS__Student_Program__r.Related_Student_Program__c =: getParentStudentProgramId() and Masterclass_Broadcasted__c = true
		AND LuanaSMS__Student_Program__r.LuanaSMS__Status__c = 'In Progress' AND LuanaSMS__Student_Program__r.Paid__c = true
		order by LuanaSMS__Start_time__c asc nulls last];
    }
    
    public PageReference doLaunchAdobeSession(){
        try{
            String cookie;
            luana_AdobeConnectUtil acUtil = new luana_AdobeConnectUtil();
            cookie = acUtil.getSessionCookie();
            if(acUtil.doExtLogin(getContactId(), cookie)){
                
                String adobeUrl;
                String attName;
                for(LuanaSMS__Attendance2__c attendance : getMasterclassAttendances()){
                    attName = attendance.Name;
                    if(attendance.Id == selectedAttendanceId){
                        adobeUrl = attendance.Adobe_Connect_Url__c;
                    }
                }
                if(adobeUrl != null){
                    PageReference pageRef = new PageReference(adobeUrl + '?session=' + cookie);
                    return pageRef;
                }else{
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Error connecting to adobe. Please contact support: adobe connection url is invalid (' + attName + ')'));
                    return null;        
                }
            }else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Error connecting to adobe. Please contact support: adobe login fail (' + getContactId() + ')'));
                return null; 
            }
            
            
            
        } catch(Exception exp){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Error connecting to adobe, Please contact support: ' + exp.getMessage()));
            return null;
        }
    }
}