/*------------------------------------------------------------------------------------
Author:        Sumit Gupta
Company:       Salesforce
Description:   Address details component controller

History
Date            Author             Comments
--------------------------------------------------------------------------------------
02-06-2018     Sumit Gupta          Initial Release
------------------------------------------------------------------------------------*/
public with sharing class CaanzAccountDetailsController {

    private static List<String> FIELDS_ACCOUNT;
    private static List<String> FIELDS_CONTACT;
    private static Map<String, Schema.SObjectType> GLOBAL_DESCRIBE;

    static{
        FIELDS_ACCOUNT = new List<String>{
            'Member_ID__c', 'Salutation', 'FirstName', 'LastName', 'Middle_Name__c', 'Designation__c', 'Gender__c',
            'Financial_Category__c', 'Membership_Class__c', 'Membership_Approval_Date__c', 
            'Preferred_Name__c'
        };
        FIELDS_CONTACT = new List<String>{
            'Email', 'MobilePhone', 'HomePhone','OtherPhone', 'Birthdate'
        };
        GLOBAL_DESCRIBE = Schema.getGlobalDescribe();
    }
    
    @AuraEnabled
    public static String getAccountData(){
        try{
           return JSON.serialize(getPersonAccountData(new PersonAccount()));
        } catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }
    }

    @AuraEnabled
    public static String saveAccountData(String accountDataJSONString){
        try{
            PersonAccount personAccount = (PersonAccount)JSON.deserialize(accountDataJSONString, PersonAccount.class);
            /** Added Code  - By Paras On 11 June 2019 
             * Code commented - As Email Update is not worked in the current Order
             */ 
            // update personAccount.user; update personAccount.account; update personAccount.contact; 
            /**
             * Swapped the order of Update of Account and Contact
             */ 
            update personAccount.user;  update personAccount.contact; update personAccount.account;
            return JSON.serialize(personAccount);
        } catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }
    }

    private static PersonAccount getPersonAccountData(PersonAccount personAccount){
        String query = 'SELECT Id, PersonContactId, ' + String.join(FIELDS_ACCOUNT, ',');
        query +=  ',Person' + String.join(FIELDS_CONTACT, ',Person') + ' FROM Account';
        query += ' WHERE Id IN (SELECT AccountId FROM User WHERE Id =' + '\'' + UserInfo.getUserId() + '\')';
        System.debug('*** Person Account read - '+query);
        List<Account> accounts = Database.query(query);
        if(!accounts.isEmpty()){
            personAccount.setAccount(accounts.get(0));
        }
        return personAccount;
    }

    private class PersonAccount{
        public User user;
        public Account account;
        public Contact contact;
        public Map<String, FieldDescribe> accountFields, contactFields;

        public PersonAccount(){
            user = [SELECT Id, CommunityNickname FROM User WHERE Id =: UserInfo.getUserId()]; 
            account = new Account();
            contact = new Contact();
            accountFields = new Map<String, FieldDescribe>();
            contactFields = new Map<String, FieldDescribe>();
            addAccountFields();
            addContactFields();
        }

        public void setAccount(Account account){
            for(String fname : FIELDS_ACCOUNT){
                this.account.put(fname, account.get(fname));
            }
            for(String fname : FIELDS_CONTACT){
                contact.put(fname, account.get('Person' + fname));
            }
            this.account.Id = account.Id;
            contact.Id = account.PersonContactId;
        }

        public void addAccountFields(){
            Map<String, Schema.SObjectField> fieldsMap = GLOBAL_DESCRIBE.get('Account').getDescribe().fields.getMap();
            for(String fname : FIELDS_ACCOUNT){
                if(fieldsMap.containsKey(fname)){
                    accountFields.put(fname, new FieldDescribe(fieldsMap.get(fname).getDescribe()));
                }
            }
        }

        public void addContactFields(){
            Map<String, Schema.SObjectField> fieldsMap = GLOBAL_DESCRIBE.get('Contact').getDescribe().fields.getMap();
            for(String fname : FIELDS_CONTACT){
                if(fieldsMap.containsKey(fname)){
                    contactFields.put(fname, new FieldDescribe(fieldsMap.get(fname).getDescribe()));
                }
            }
        }
    }

    public class FieldDescribe{
        public Boolean isAccessible, isEditable;
        public Schema.DescribeFieldResult describe;

        public FieldDescribe(Schema.DescribeFieldResult describe){
            this.describe = describe;
            isAccessible = describe.isAccessible();
            isEditable = isAccessible && describe.isUpdateable();
            //isAccessible = isEditable = true;
        }
    }
}