/*
    Developer: WDCi (Lean)
    Date: 23/May/2016
    Task #: Luana implementation - Virtual Class
*/

global with sharing class luana_VirtualClassBroadcastAsync implements Database.Batchable<sObject>, Database.Stateful {
    
    String courseId;
    String errorRecords;
    boolean hasError;
    
    global luana_VirtualClassBroadcastAsync(String courseId){
		this.courseId = courseId;
		this.errorRecords = '';
		this.hasError = false;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
    	
    	return Database.getQueryLocator('select Id, Name, Ready_to_Broadcast__c from LuanaSMS__Student_Program__c where Paid__c = true and LuanaSMS__Status__c = \'In Progress\' and LuanaSMS__Course__c = \'' + courseId + '\'');
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> scope){
    	List<LuanaSMS__Student_Program__c> studProgsToBroadcast = new List<LuanaSMS__Student_Program__c>();
    	
    	for(SObject sobj : scope){
    		LuanaSMS__Student_Program__c studentProgram = (LuanaSMS__Student_Program__c)sobj;
    		
    		studentProgram.Ready_to_Broadcast__c = true;
    		studProgsToBroadcast.add(studentProgram);
    	}
    	    	
    	integer counter = 0;
    	for(Database.SaveResult sr : Database.update(studProgsToBroadcast, false)){
    		if(!sr.isSuccess()){
    			errorRecords += studProgsToBroadcast.get(counter) + ' - ' + sr.getErrors()[0].getMessage() + '\n';
    			hasError = true;
    		}
    		
    		counter ++;
    	}
    	
    }
    
    global void finish(Database.BatchableContext bc){
    	
    	LuanaSMS__Luana_Log__c errorLog = new LuanaSMS__Luana_Log__c();
    	errorLog.LuanaSMS__Notify_User__c = false;
    	errorLog.LuanaSMS__Related_Course__c = courseId;
	    errorLog.LuanaSMS__Parent_Id__c = courseId;
	    
    	if(hasError){
	    	errorLog.LuanaSMS__Status__c = 'Failed';
	    	errorLog.LuanaSMS__Level__c = 'Fatal';
	    	errorLog.LuanaSMS__Log_Message__c = 'Error broadcasting to the following record/s:\n\n' + errorRecords;
	    	
    	} else {
    		errorLog.LuanaSMS__Status__c = 'Completed';
	    	errorLog.LuanaSMS__Level__c = 'Info';
    	}
    	
    	insert errorLog;
    	
    }
}