/**
    Developer: WDCi (kh)
    Development Date:17/03/2016
    Task: Library for Luana Test class
    
    Change History
    LCA-700     24/06/2016 - WDCi KH:   To add in create resource & resource booking object methods
    LCA-511     29/06/2016 - WDCi KH:   To add in create attendance method
    Test class  11/07/2016 - WDCi KH:   include 'Education Record Default University' custom setting in line 45
    LCA-921     23/08/2016 - WDCi KH:   To add personAccount email field
    PAN:5340    11/10/2018 - WDCi Lean: new custom settings for flexipath and objects
    PAN:5340    18/10/2018 - WDCi LKoh: new Education History generation method for use in test classes and prepackage them into a 
    PAN:6226    06/06/2019 - WDCi Lean: enhanced code coverage
    PAN:6258    01/07/2019 - WDCi LKoh: support for Subject Access
**/
@isTest
public class Luana_DataPrep_Test {
    
    public List<Luana_Extension_Settings__c> prepLuanaExtensionSettingCustomSettings() {
        List<Luana_Extension_Settings__c> csList = new List<Luana_Extension_Settings__c>();
        
        csList.add(new Luana_Extension_Settings__c(Name = 'Pricebook_Australia', Value__c = Test.getStandardPricebookId()));
        csList.add(new Luana_Extension_Settings__c(Name = 'Pricebook_New Zealand', Value__c = Test.getStandardPricebookId()));
        csList.add(new Luana_Extension_Settings__c(Name = 'Pricebook_Overseas', Value__c = Test.getStandardPricebookId()));
        
        csList.add(new Luana_Extension_Settings__c(Name = 'Currency_Australia', Value__c = 'AUD'));
        csList.add(new Luana_Extension_Settings__c(Name = 'Currency_New Zealand', Value__c = 'NZD'));
        csList.add(new Luana_Extension_Settings__c(Name = 'Currency_Overseas', Value__c = 'INT'));
        
        csList.add(new Luana_Extension_Settings__c(Name = 'AUD', Value__c = 'Australian Dollar (AUD)'));
        csList.add(new Luana_Extension_Settings__c(Name = 'INT', Value__c = 'Australian Dollar (AUD)'));
        csList.add(new Luana_Extension_Settings__c(Name = 'NZD', Value__c = 'New Zealand Dollar (NZD)'));

        csList.add(new Luana_Extension_Settings__c(Name = 'ExamLocationFreeCountry_Australia', Value__c = 'Australia,New Zealand, Malaysia'));
        csList.add(new Luana_Extension_Settings__c(Name = 'ExamLocationFreeCountry_New Zealand', Value__c = 'Australia,New Zealand, Malaysia'));
        csList.add(new Luana_Extension_Settings__c(Name = 'ExamLocationFreeCountry_Overseas', Value__c = 'Australia,New Zealand, Malaysia'));
        
        csList.add(new Luana_Extension_Settings__c(Name = 'Default_Pagination_No_Per_Page', Value__c = '10'));
        
        csList.add(new Luana_Extension_Settings__c(Name = 'CommunityDisplayStatus', Value__c = 'true'));
        
        PermissionSet memberPermSet = [SELECT Id FROM PermissionSet WHERE Name='Luana_MemberCommunityPermission'];
        PermissionSet employerPermSet = [SELECT Id FROM PermissionSet WHERE Name='Luana_EmployerCommunityPermission'];
        PermissionSet nonMemberPermSet = [SELECT Id FROM PermissionSet WHERE Name='Luana_NonMemberCommunityPermission'];
        PermissionSet contributorPermSet = [SELECT Id FROM PermissionSet WHERE Name='Luana_ContributorCommunityPermission'];

        csList.add(new Luana_Extension_Settings__c(Name = 'Default_Member_PermSet_Id', Value__c = String.valueOf(memberPermSet.Id).left(15)));
        csList.add(new Luana_Extension_Settings__c(Name = 'Default_Employer_PermSet_Id', Value__c = String.valueOf(employerPermSet.Id).left(15)));
        csList.add(new Luana_Extension_Settings__c(Name = 'Default_NonMember_PermSet_Id', Value__c = String.valueOf(nonMemberPermSet.Id).left(15)));
        csList.add(new Luana_Extension_Settings__c(Name = 'Default_ContCom_PermSet_Id', Value__c = String.valueOf(contributorPermSet.Id).left(15)));
        
        csList.add(new Luana_Extension_Settings__c(Name = 'Education Record Default University', Value__c = 'Chartered Accountants Australia and New Zealand'));
        
        //PAN:5340
        DateTime tempDate = system.now().addDays(10);
        csList.add(new Luana_Extension_Settings__c(Name = luana_EnrolmentConstants.CS_FLEXIPATH_CUTOFF_DATE, Value__c = tempDate.format('yyyy-MM-dd')));
        
        //PAN:6226
        csList.add(new Luana_Extension_Settings__c(Name = 'AttendanceMarking_Workshop', Value__c = 'true'));
        
        return csList;
       
        
    }
    
    public List<LuanaSMS__Luana_Configuration__c> createLuanaConfigurationCustomSetting(){
        List<LuanaSMS__Luana_Configuration__c> luanaConfs = new List<LuanaSMS__Luana_Configuration__c>();
        
        luanaConfs.add(new LuanaSMS__Luana_Configuration__c(Name = 'Luana Configuration', LuanaSMS__Enable_AutoCreditTransfer__c = true, LuanaSMS__Batchable_Size__c = 1000));
        return luanaConfs;
    }
    
    public User generateNewApplicantUser(String longContext, String shortContext, Account acc, Id profId) {

        Contact con = [SELECT Id FROM Contact WHERE AccountId =: acc.Id];
        if(con == null)
            return null;
        else {
            User newUser = new User( 
                profileId = profId,
                username = longContext + '@example.com', 
                email = longContext + '@example.com', 
                emailencodingkey = 'UTF-8', 
                localesidkey = 'en_US', 
                languagelocalekey = 'en_US', 
                timezonesidkey = 'Australia/Sydney', 
                alias = shortContext, 
                lastname = longContext + 'lastname', 
                contactId = con.id
            );
            return newUser;
        }                    
    }
    public Account generateNewApplicantAcc(String firstName, String context, String type) {        
        Account acc = new Account(
            
            RecordTypeId=getRecordTypeIdMap('Account').get(type),
            LastName= (context != null && context.length() > 15 ? context.substring(0, 15) : context),
            Preferred_Name__c = context,
            FirstName=(firstName != null && firstName.length() > 15 ? firstName.substring(0, 20) : firstName),
            Gender__c = 'Male',
            Member_Of__c = 'NZICA',
            Salutation = 'Mr',
            Membership_Class__c = 'Full',
            Create_Portal_User__c =true
        );
        return acc;
    }

    public List<Account> generateNewApplicantAccBulk(Integer j,String firstName, String context,Id typeId) {        
       List<Account> memAccList = new List<Account>();
       for(Integer i=1;i<=j;i++){      
           Account acc = new Account();
           acc.RecordTypeId=typeId;
           acc.LastName= (context != null && context.length() > 15 ? context.substring(0, 15) : context)+i;
           acc.Preferred_Name__c = context+i;
           acc.FirstName=(firstName != null && firstName.length() > 15 ? firstName.substring(0, 20) : firstName)+i;
           acc.Gender__c = 'Male';
           acc.Member_Of__c = 'NZICA';
           acc.Salutation = 'Mr';
           memAccList.add(acc);          
        }
        return memAccList;
    }
    
    public Account generateNewBusinessAcc(String contextName, String type, String accType, String employerToken, String phone, String memberOf, String bStreet, String status){
        Account acc = new Account(
            RecordTypeId=getRecordTypeIdMap('Account').get(type),
            Name = contextName,
            Type = accType,
            Phone = phone,
            Member_Of__c = memberOf,
            BillingStreet = bStreet,
            Status__c = status,
            Employer_Token__c = employerToken
        );
        
        return acc;
    }
    
    /*LCA-518, not used
    public Contact generateNewContact(String context, Account businessAcc){
        Contact cont = new Contact(
            LastName = context,
            AccountId = businessAcc.Id
        );
        insert cont;
        return cont;
    }*/
    
    public Map<String, Id> getAccRTMap() {
        Map<String, Id> accRTMap = new Map<String, Id>();
        
        for(RecordType rt : [SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType='Account']) {
            accRTMap.put(rt.DeveloperName, rt.Id);
        }
        return accRTMap;   
    }
    
    public Id getCustCommProfId() {
        Id custCommProfId;
        for(Profile prof : [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User']) {
           custCommProfId = prof.Id;
        }
        return custCommProfId;   
    }
    
    /*LCA-518, all insert method should do in the test class
    public User generateFullAccount(String firstName, String lastName, String memberType){
        Account businessAcc = generateNewBusinessAcc('Price Waterhouse', 'Business_Account', '11111111');
        Account memberAcc = generateNewApplicantAcc(firstName, lastName, memberType);
        
        generateNewContact(lastName, businessAcc);
        
        User memberUser = generateNewApplicantUser(lastName, memberAcc, getCustCommProfId());
        
        List<User> users = [Select Id, Name, ContactId, Contact.AccountId from User Where Contact.AccountId =: memberAcc.Id];
        
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
        //Luana_Extension_Settings__c defNonMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_NonMember_PermSet_Id');
       
        
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defEmployerPermSetId.value__c));
        //psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defNonMemberPermSetId.value__c));
        
        System.runAs(memberUser){
            insert psas;
        }
        
        //Check user accessbility to Member or Employer Community
        List<PermissionSetAssignment> hasMemberAccessCheck = [SELECT AssigneeId, Assignee.Name, PermissionSetId, PermissionSet.Name FROM PermissionSetAssignment Where (PermissionSet.Name = 'Luana_MemberCommunityPermission') and AssigneeId =: memberUser.Id];
        System.assertEquals(hasMemberAccessCheck.isEmpty(), false, 'Expect ' + user.Name + ' has acccess to \'Community - Member\' but it is not');  
        
        List<PermissionSetAssignment> hasEmployerAccessCheck = [SELECT AssigneeId, Assignee.Name, PermissionSetId, PermissionSet.Name FROM PermissionSetAssignment Where (PermissionSet.Name = 'Luana_EmployerCommunityPermission') and AssigneeId =: memberUser.Id];
        System.assertEquals(hasMemberAccessCheck.isEmpty(), false, 'Expect ' + user.Name + ' has acccess to \'Community - Empoyer\' but it is not'); 
                    
        return memberUser;
    }
    */
    
    public Map<String, Id> getRecordTypeIdMap(String sobj){
        Map<String, Id> recordTypeMap = new Map<String, Id>();
        
        /*PAN:5340 Lean - disabled, changed to describe method below to save soql
        for(RecordType rt: [SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType=:sobj]){
            recordTypeMap.put(rt.DeveloperName, rt.Id);
        }*/        
        
        Schema.SObjectType objType = Schema.getGlobalDescribe().get(sobj);
        Schema.DescribeSobjectResult objTypeDesc = objType.getDescribe();
        
        for(Schema.RecordTypeInfo recordTypeInfo : objTypeDesc.getRecordTypeInfos()){
            recordTypeMap.put(recordTypeInfo.getDeveloperName(), recordTypeInfo.getRecordTypeId());
        }
        
        return recordTypeMap;
    }
    
    public LuanaSMS__Course__c createNewCourse(String courseName, Id poId, Id recordTypeId, String courseStatus){
        LuanaSMS__Course__c course = new LuanaSMS__Course__c();
        course.RecordTypeId = recordTypeId;
        course.Name = courseName;
        course.LuanaSMS__Program_Offering__c = poId;
        course.LuanaSMS__Start_Date__c = System.today();
        course.Enrolment_Start_Date__c = System.today().addDays(-1);
        course.Enrolment_End_Date__c = System.today().addDays(10);
        course.LuanaSMS__Allow_Online_Enrolment__c = true;
        course.LuanaSMS__Status__c = courseStatus;
        
        return course;
    }
    
    //LCA-518, fixed
    public Product2 createNewProduct(String contextName, String nsId){
        Product2 p = new Product2();
        p.Name = contextName;
        p.ProductCode = contextName;
        p.NetSuite_Internal_Id__c = nsId;
        
        return p;
    }
    
    public Pricebook2 createNewCustomPricebook(String name){
        Pricebook2 customPB = new Pricebook2();
        customPB.Name=name;
        customPB.isActive=true;
        
        return customPB;
    }
    
    
    public PricebookEntry createPricebookEntry(Id pbId, Id pId, Double unitPrice){
        PricebookEntry pbe = new PricebookEntry();
        pbe.Pricebook2Id = pbId;
        pbe.Product2Id = pId;
        pbe.UnitPrice = unitPrice;
        pbe.IsActive = true;
        
        return pbe;
    }
    
    public LuanaSMS__Program_Offering__c createNewProgOffering(String contextName, Id recordTypeId, Id progId, Id tOrgId, Id auProdId, Id nzProdId, Id intProdId, Integer noOfRequiredMin, Integer noOfMax){
        LuanaSMS__Program_Offering__c po = new LuanaSMS__Program_Offering__c();
        po.Name = contextName;
        po.RecordTypeId = recordTypeId;
        po.LuanaSMS__Program__c = progId;
        po.LuanaSMS__Training_Organisation__c = tOrgId;
        po.LuanaSMS__Number_of_Required_Electives__c = noOfRequiredMin;
        po.Maximum_Number_of_Required_Electives__c = noOfMax;
        po.AU_Product__c = auProdId;
        po.NZ_Product__c = nzProdId;
        po.INT_Product__c = intProdId;
        po.AU_Product_Supp_Exam__c = auProdId;
        po.NZ_Product_Supp_Exam__c = nzProdId;
        po.INT_Product_Supp_Exam__c = intProdId;
        
        return po;
    }
    
    //LCA-518, fixed
    public LuanaSMS__Subject__c createNewSubject(String contextName, String subjContextName, String subjContextId, double normialHrs, String subjectFlag){
        LuanaSMS__Subject__c subj = new LuanaSMS__Subject__c();
        subj.Name = contextName;
        subj.LuanaSMS__Subject_Name__c = subjContextName;
        subj.LuanaSMS__Subject_Id__c = subjContextId;
        subj.LuanaSMS__Nominal_Hours__c = normialHrs;
        subj.LuanaSMS__Subject_Flag__c = subjectFlag;
        
        return subj;
    }
    
    public LuanaSMS__Program_Offering_Subject__c createNewProgOffSubject(Id poId, Id subId, String essential){
        LuanaSMS__Program_Offering_Subject__c pos = new LuanaSMS__Program_Offering_Subject__c();
        pos.LuanaSMS__Program_Offering__c = poId;
        pos.LuanaSMS__Subject__c = subId;
        pos.LuanaSMS__Nominal_Hours__c = 2;
        pos.LuanaSMS__Essential__c = essential;
        pos.LuanaSMS__Completion_Requirement__c = 'Optional';
        
        return pos; 
    }
    
    public LuanaSMS__Training_Organisation__c createNewTraningOrg(String contextName, String contextOrgName, String contextOrgId, String addFirstLine, String addLoc, String postCode){
        LuanaSMS__Training_Organisation__c to = new LuanaSMS__Training_Organisation__c();
        to.Name = contextName;
        to.LuanaSMS__Training_Organisation_Name__c = contextOrgName;
        to.LuanaSMS__Training_Organisation_Id__c = contextOrgId;
        to.LuanaSMS__Address_First_Line__c = addFirstLine;
        to.LuanaSMS__Address_Location__c = addLoc;
        to.LuanaSMS__Postcode__c = postCode;
        
        return to;
    }
    
    public LuanaSMS__Program__c createNewProgram(String shortContextName, String longContextName, String progStatus, String progLevelEdu, String progRecog){
        LuanaSMS__Program__c prog = new LuanaSMS__Program__c();
        prog.Name = shortContextName;
        prog.LuanaSMS__Program_Name__c = longContextName;
        prog.LuanaSMS__Program_Id__c = shortContextName;
        prog.Program_Status__c = progStatus;
        prog.LuanaSMS__Program_Level_of_Education__c = progLevelEdu;
        prog.LuanaSMS__Program_Recognition__c = progRecog;

        return prog;
    }
    
    public LuanaSMS__Student_Program__c createNewStudentProgram(Id recordTypeId, Id contId, Id courseId, String residenceCountry, String statusValue){
        Map<String, LuanaSMS__Student_Program__c> spRecordTypeMap = new Map<String, LuanaSMS__Student_Program__c>();
        
        LuanaSMS__Student_Program__c sp = new LuanaSMS__Student_Program__c();
        sp.RecordTypeId = recordTypeId;
        sp.LuanaSMS__Contact_Student__c = contId;
        sp.LuanaSMS__Course__c = courseId;
        sp.LuanaSMS__Auto_Student_Program_Subject_Creation__c = true;
        //sp.LuanaSMS__Status__c = 'In Progress';
        sp.LuanaSMS__Status__c = statusValue;
        //sp.Please_select_your_country_of_residence2__c = residenceCountry;
        return sp;
    }
    
    public LuanaSMS__Student_Program_Subject__c createNewStudentProgramSubject(Id contId, Id spId, Id subId){
        
        LuanaSMS__Student_Program_Subject__c sps = new LuanaSMS__Student_Program_Subject__c();

        sps.LuanaSMS__Contact_Student__c = contId;
        sps.LuanaSMS__Student_Program__c = spId;
        sps.LuanaSMS__Subject__c = subId;
        sps.LuanaSMS__Activity_Start_Date__c = System.today();
        sps.LuanaSMS__Activity_End_Date__c = System.today().addMonths(1);
        
        return sps;
    }
    
    public boolean errorMessageCheck(String expectedMsg){
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean matchErrorMsg = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains(expectedMsg)){
                matchErrorMsg = true;
            }
        }
        return matchErrorMsg;     
    }
    
    //LCA-518, fixed hardcored value
    public Employment_History__c createNewEmploymentHistory(Id memberId, Id employerId, String status, String jobTitle){
        Employment_History__c eh = new Employment_History__c();
        eh.Member__c = memberId;
        eh.Employer__c = employerId;
        eh.Status__c = status;
        eh.Nominated_Employee__c = true;
        eh.Primary_Employer__c = true;
        eh.Job_Title__c = jobTitle;
        eh.Employee_Start_Date__c = system.today();
        //eh.Is_CPP_Provided__c = false;
        
        if(status == 'Closed'){
            eh.Employee_End_Date__c = system.today().addDays(1);
        }
        
        return eh;
    }
    
    public Payment_Token__c createNewPaymentToken(Id empId, Id courseId, Date expiryDate, Boolean revoked){
        
        Payment_Token__c pt = new Payment_Token__c();
        pt.Employer__c = empId;
        pt.Course__c = courseId;
        pt.Expiry_Date__c = expiryDate;
        pt.Revoke__c = revoked;
        
        return pt;
    }
    
    public Education_Record__c createNewEducationRecords(Id contId, Date commDate, String eventCourseName, String universityInst){
        
        Education_Record__c er = new Education_Record__c();
        er.Contact_Student__c = contId;
        er.Date_Commenced__c = commDate;
        er.Event_Course_name__c = eventCourseName;
        er.Enter_university_instituition__c = universityInst;
        
        return er;
    }
    
    //LCA-518, fix the hardcoded value
    public LuanaSMS__Delivery_Location__c createNewDeliveryLocationRecord(String shortContextName, String longContextName, Id traningOrgId, String postCodeValue, String countryValue){
    
        LuanaSMS__Delivery_Location__c dl = new LuanaSMS__Delivery_Location__c();
        dl.Name = shortContextName;
        dl.LuanaSMS__Delivery_Location_Name__c = longContextName;
        dl.LuanaSMS__Training_Organisation__c = traningOrgId;
        dl.LuanaSMS__Postcode__c = postCodeValue;
        dl.LuanaSMS__Country__c = countryValue;
        
        return dl;
    }
    
    //LCA-518, no Fix
    public LuanaSMS__Program_Subject__c createProgramSubject(LuanaSMS__Program__c progObj, LuanaSMS__Subject__c subjObj, String essential){
        
        LuanaSMS__Program_Subject__c ps = new LuanaSMS__Program_Subject__c();
        ps.LuanaSMS__Essential__c = essential;
        ps.LuanaSMS__Program__c = progObj.Id;
        ps.LuanaSMS__Subject__c = subjObj.Id;
        ps.LuanaSMS__External_Id__c = progObj.Name +' - '+subjObj.Name;
        
        return ps;
    }
    
    //LCA-518, no Fix
    public Relationship__c createRelationship(Id accountId, Id memberId, String status, String primaryRel, String reciprocalRel, Date startdate, Date enddate){
        
        Relationship__c r = new Relationship__c();
        r.Account__c = accountId;
        r.Member__c = memberId;
        r.Status__c = status;
        r.Primary_Relationship__c = primaryRel;
        r.Reciprocal_Relationship__c = reciprocalRel;
        r.Relationship_Start_Date__c = startDate;
        r.Relationship_End_Date__c = endDate;
        
        return r;
    }
    
    //LCA-518, Fixed    
    public Attachment addAttachmentToParent(Id parentId, String context) {  
        Blob b = Blob.valueOf('Test Data');  
          
        Attachment attachment = new Attachment();  
        attachment.ParentId = parentId;  
        attachment.Name = context;  
        attachment.Body = b;  
          
        return attachment;  
    }  
    
    //LCA-518, Fixed with min required field
    public Case createCase(String type, String status, Id recordTypeId, String subType, String subject, String origin){
    
        Case caseObj = new Case();
            caseObj.Type = type;
            caseObj.Sub_Type__c = subType;
            caseObj.Status = status;
            caseObj.Origin = origin;
            caseObj.RecordTypeId = recordTypeId;
            caseObj.Subject = subject;
        return caseObj;
    }
    
    //LCA-518, Fixed    
    public Terms_and_Conditions__c createTermAndCond(String context, String type){
        Terms_and_Conditions__c tnc = new Terms_and_Conditions__c();
        tnc.Name = context;
        tnc.Type__c = type;
        
        return tnc;
    }
    
    public List<Terms_and_Conditions__c> createDefaultTNCs(){
        List<Terms_and_Conditions__c> tncs = new List<Terms_and_Conditions__c>();
        tncs.add(createTermAndCond('CA Module', 'Attachment'));
        tncs.add(createTermAndCond('CA Program AU', 'Attachment'));
        tncs.add(createTermAndCond('CA Program NZ', 'Attachment'));
        tncs.add(createTermAndCond('CA Program', 'Attachment'));
        tncs.add(createTermAndCond('Foundation', 'Attachment'));
        tncs.add(createTermAndCond('T&C: Discontinuation Additional Info', 'Attachment'));
        tncs.add(createTermAndCond('T&C: Discontinuation T&C', 'Attachment'));
        tncs.add(createTermAndCond('T&C: Exemption Additional Info', 'Attachment'));
        tncs.add(createTermAndCond('T&C: Exemption T&C', 'Attachment'));
        return tncs;
    }
    
    //LCA-518, Fixed    
    public Course_Delivery_Location__c createCourseDeliveryLocation(Id courseId, Id devLocId, String type){
        Course_Delivery_Location__c cdl = new Course_Delivery_Location__c();
        cdl.Course__c = courseId;
        cdl.Delivery_Location__c = devLocId;
        cdl.Availability_Day__c = 'Monday';
        cdl.Type__c = type;
         
        return cdl;
    }
    
    //LCA-518, Fixed
    public Employment_Token__c createEmplToken(String longContextName, Id accId, String tokenCodeValue, String shortContextName, date expDate, boolean activeVal){
        Employment_Token__c et = new Employment_Token__c();
        et.Name = longContextName;
        et.Employer__c = accId;
        et.Token_Code__c = tokenCodeValue + '_' + shortContextName;
        et.Expiry_Date__c = expDate;
        et.Active__c = activeVal;
        
        return et;
    }
    
    public List<Comment_Read_Status__c> createCommentReadStatus(Id caseId){
        List<Comment_Read_Status__c> crsList = new List<Comment_Read_Status__c>();
        Comment_Read_Status__c crs = new Comment_Read_Status__c();
        crs.Case__c = caseId;
        crsList.add(crs);
        return crsList;
    }
    
    public User newInternalUser(String shortContext, String longContext, String profileId){
        User newUser = new User( 
            profileId = profileId,
            username = longContext + '@example.com', 
            email = longContext + '@example.com', 
            emailencodingkey = 'UTF-8', 
            localesidkey = 'en_US', 
            languagelocalekey = 'en_US', 
            timezonesidkey = 'Australia/Sydney', 
            alias = shortContext, 
            lastname = longContext + 'lastname'
        );
        
        return newUser;
    }
    
    public Profile getProfileByName(String profileName){
        
        return [SELECT Id, Name FROM Profile WHERE Name =: profileName];
    }
    
    public LuanaSMS__Room__c newRoom(String shortContext, String longContext, String venueId, Integer capacity, String type, String runKey){
        return new LuanaSMS__Room__c(
            Name = longContext+'_'+runKey,
            Room_Capacity__c = capacity,
            Type__c = type,
            LuanaSMS__Venue__c = venueId//,
            //LuanaSMS__External_Id__c = shortContext + '_' + runKey
        );
    } 
    
    public LuanaSMS__Session__c newSession(String shortContext, String longContext, String type, Integer capacity, String venueId, String roomId, String courseDeliveryLocationId, DateTime startTime, DateTime endTime){
        return new LuanaSMS__Session__c(
            Name = longContext,
            RecordTypeId = getRecordTypeIdMap('LuanaSMS__Session__c').get(type),
            Maximum_Capacity__c = capacity,
            LuanaSMS__Venue__c = venueId,
            LuanaSMS__Room__c = roomId,
            Course_Delivery_Location__c = courseDeliveryLocationId,
            LuanaSMS__Start_Time__c = startTime,
            LuanaSMS__End_Time__c = endTime
        );
    }
    
    public LuanaSMS__Course_Session__c createCourseSession(String courseId, String sessionId){
        
        return new LuanaSMS__Course_Session__c(LuanaSMS__Course__c = courseId, LuanaSMS__Session__c = sessionId);
    }
    
    public LuanaSMS__Resource__c createResource(String name, String extId){
        LuanaSMS__Resource__c res = new LuanaSMS__Resource__c();
        res.Name = name;
        res.LuanaSMS__External_Id__c = extId;
        
        return res;
    }
    
    public LuanaSMS__Resource_Booking__c createResourceBooking(Id contId, Id resId, Id sessId){
    
        LuanaSMS__Resource_Booking__c rb = new LuanaSMS__Resource_Booking__c();
        rb.LuanaSMS__Contact_Trainer_Assessor__c = contId;
        rb.LuanaSMS__Resource__c = resId;
        rb.LuanaSMS__Session__c = sessId;
        
        return rb;
    }
    
    public LuanaSMS__Attendance2__c createAttendance(Id contId, String type, Id spId, Id sessId){
        
        LuanaSMS__Attendance2__c att = new LuanaSMS__Attendance2__c();
        att.LuanaSMS__Contact_Student__c = contId;
        att.RecordTypeId = getRecordTypeIdMap('LuanaSMS__Attendance2__c').get(type);
        att.LuanaSMS__Student_Program__c = spId;
        att.LuanaSMS__Session__c = sessId;
        
        return att;
    }
    
    //PAN:5340
    public FP_Competency_Area__c createCompetencyArea(String name, boolean isActive){
        
        FP_Competency_Area__c compArea = new FP_Competency_Area__c(Name = name, FP_Active__c = isActive);
        
        return compArea;
    }
    
    //PAN:5340
    public FP_Relevant_Competency_Area__c createRelevantCompetencyArea(String competencyAreaId, String subjectId, String type){
        
        FP_Relevant_Competency_Area__c relCompArea = new FP_Relevant_Competency_Area__c(
            FP_Competency_Area__c = competencyAreaId,
            FP_Subject__c = subjectId,
            FP_Type__c = type
        );
        
        return relCompArea;
    }

    // PAN:6258
    public FP_Relevant_Competency_Area__c createRelevantCompetencyAreaMk2(String competencyAreaId, String subjectAccessId, String subjectId, String type){
        
        FP_Relevant_Competency_Area__c relCompArea = new FP_Relevant_Competency_Area__c(
            FP_Competency_Area__c = competencyAreaId,
            FP_Subject__c = subjectId,
            FT_Subject_Access__c = subjectAccessId,
            FP_Type__c = type
        );
        
        return relCompArea;
    }

    // PAN:6258
    public FT_Subject_Access__c createSubjectAccess(String name, String subjectId) {

        FT_Subject_Access__c subjectAccess = new FT_Subject_Access__c(
            Name = name,
            FT_Subject__c = subjectId,
            FT_Active__c = true
        );
        return subjectAccess;
    }
    
    //PAN:5340
    public FP_Gained_Competency_Area__c createGainedCompetencyArea(String competencyAreaId, String contactId, String spsId, String caseId){
        
        FP_Gained_Competency_Area__c gainedCompArea = new FP_Gained_Competency_Area__c(
            FP_Competency_Area__c = competencyAreaId,
            FP_Contact__c = contactId,
            FP_Student_Program_Subject__c = spsId,
            FP_Case__c = caseId
        );
        
        return gainedCompArea;
    }

    //PAN:5340
    public edu_University__c createUniversity(String acronym, String country, String state, String status, String name, String currencyISOCode) {

        edu_University__c university = new edu_University__c(
            Acronym__c = acronym,
            Country__c = country,
            State__c = state,
            Status__c = status,
            University_Name__c = name,
            CurrencyIsoCode = currencyISOCode
        );
        return university;
    }

    // PAN:5340
    public edu_Degree__c createDegree(String degreeName, String degreeType, String status, String currencyISOCode) {

        edu_Degree__c degree = new edu_Degree__c(
            Degree_Name__c = degreeName,
            Degree_Type__c = degreeType,
            Status__c = status,
            CurrencyIsoCode = currencyISOCode
        );
        return degree;
    }

    // PAN:5340
    public edu_University_Degree_Join__c createUniversityDegreeJoin(Id degreeId, String searchField, Id universityId) {

        edu_University_Degree_Join__c udj = new edu_University_Degree_Join__c(
            Degree__c = degreeId,
            Search_Field__c = searchField,
            University__c = universityId            
        );
        return udj;
    }

    // PAN:5340
    public edu_Education_History__c createEducationHistory(Id applicationId, Id contactId, Boolean evidenceSighted, Id uniDegreeJoinId) {

        String yearCommenced = String.valueOf(system.today().addYears(-4).year());
        String yearFinished = String.valueOf(system.today().addYears(-1).year());
        
        edu_Education_History__c educationHistory = new edu_Education_History__c(
            Application__c = applicationId,
            FP_Contact__c = contactId,
            FP_Evidence_Sighted__c = evidenceSighted,
            University_Degree__c = uniDegreeJoinId,
            FP_Year_of_Commence__c = yearCommenced,
            FP_Year_of_Finish__c = yearFinished
        );
        return educationHistory;
    }
    
    // PAN:5340 - University and Degree assets needed to clear to create Education History
    public Map<String, SObject> createEducationAssets() {
        Map<String, SObject> assetMap = new Map<String, SObject>();
        edu_University__c uni_asset1 = createUniversity('UNIMELB', 'Australia', 'Victoria', 'Approved', 'University of Melbourne', 'AUD');
        insert uni_asset1;
        assetMap.put('university1', uni_asset1);

        edu_Degree__c degree_asset1 = createDegree('Bachelor of Science', 'Undergraduate', 'Approved', 'AUD');
        insert degree_asset1;
        assetMap.put('degree1', degree_asset1);
        
        edu_University_Degree_Join__c udj_asset1 = createUniversityDegreeJoin(degree_asset1.Id, null, uni_asset1.Id);
        insert udj_asset1;
        assetMap.put('universityDegreeJoin1', udj_asset1);
        
        return assetMap;
    }
    
    public Map<String, SObject> createEducationAssets(String uniName, String uniCode, String degreeName) {
        Map<String, SObject> assetMap = new Map<String, SObject>();
        edu_University__c uni_asset1 = createUniversity(uniCode, 'Australia', 'Victoria', 'Approved', uniName, 'AUD');
        insert uni_asset1;
        assetMap.put('university2', uni_asset1);

        edu_Degree__c degree_asset1 = createDegree(degreeName, 'Undergraduate', 'Approved', 'AUD');
        insert degree_asset1;
        assetMap.put('degree2', degree_asset1);
        
        edu_University_Degree_Join__c udj_asset1 = createUniversityDegreeJoin(degree_asset1.Id, null, uni_asset1.Id);
        insert udj_asset1;
        assetMap.put('universityDegreeJoin2', udj_asset1);
        
        return assetMap;
    }     
}