@isTest
public class SegmentationDataGenTest {
/**
    Developer: akopec
    Development Date: 01/2018
    Task: Helper class to generate data for Segmentation Testing
    
**/

    //   genFullMemberNewZealand('Smith', 'Senior Accountant', 'smith1@test.co.nz', Date.valueOf('1980-05-12') )
    public  static Account genFullMemberNewZealand(String Last, String JobTitle, String email, Date DOB ) {       
        Account accountPersonNZ = new Account(
            RecordTypeId = RecordTypeCache.getId('Account', 'Full_Member')
            , Salutation = 'Mr'
            , FirstName = 'John'
            , LastName = Last
            , Preferred_Name__c = 'Johnny'
            , Gender__c = 'Male'
            , PersonBirthdate = DOB
            , Membership_Type__c = 'Member'
            , Membership_Class__c = 'Full'
            , Designation__c = 'CA'
            , Member_Of__c = 'NZICA'
            , Membership_Approval_Date__c = Date.valueOf('2010-01-29:00:00')
            , Financial_Category__c = 'Standard fee applies'
            , Status__c = 'Active'
            , Assessible_for_CAF_Program__c = false
            , Job_Title__c = JobTitle
            , PersonOtherPhone = '1234567'
            , PersonEmail = email
            // NZ attributes
            , PersonOtherCountry = 'New Zealand'
            , PersonMailingCountry = 'New Zealand'
            , Affiliated_Branch_Country__c = 'New Zealand'
            , Affiliated_Branch__c = 'Auckland'
             ,Communication_Preference__c= 'Home Phone'
            ,PersonHomePhone= '1234'
            ,PersonOtherStreet= '83 Saggers Road'
            ,PersonOtherCity='JITARNING'
            ,PersonOtherPostalCode='6365'
        );
        return accountPersonNZ;
 }
    
    
    //   genFullMemberNewZealand('Smith', 'Senior Accountant', 'smith1@test.co.nz', Date.valueOf('1980-05-12') )
    public  static Account genFullMemberWithAddressAU(String Last, String JobTitle, String email, Date DOB ) {       
        Account accountPersonAU = new Account(
            RecordTypeId = RecordTypeCache.getId('Account', 'Full_Member')
            , Salutation = 'Mr'
            , FirstName = 'John'
            , LastName = Last
            , Preferred_Name__c = 'Johnny'
            , Gender__c = 'Male'
            , PersonBirthdate = DOB
            , Membership_Type__c = 'Member'
            , Membership_Class__c = 'Full'
            , PersonOtherStreet = '1610 Pacific Hwy'
            , PersonOtherCity = 'Wahroonga'
            , PersonOtherState = 'New South Wales'
            , PersonOtherPostalCode = '2076'
            , Designation__c = 'CA'
            , Member_Of__c = 'ICAA'
            , Membership_Approval_Date__c = Date.valueOf('2010-01-29:00:00')
            , Financial_Category__c = 'Standard fee applies'
            , Status__c = 'Active'
            , Assessible_for_CAF_Program__c = false
            , Job_Title__c = JobTitle
            , PersonOtherPhone = '1234567'
            , PersonEmail = email
            , PersonOtherCountry = 'Australia'
            , PersonMailingCountry = 'Australia'
            , Affiliated_Branch_Country__c = 'Australia'
            , Affiliated_Branch__c = 'New South Wales'
        );
        return accountPersonAU;
 }
    
   
    //   genFRetiredMemberNewZealand('Smith', 'Senior Accountant', 'smith1@test.co.nz', Date.valueOf('1980-05-12') )
    public  static Account genFRetiredMemberNewZealand(String Last, String JobTitle, String email, Date DOB ) {       
        Account accountPersonNZ = new Account(
            RecordTypeId = RecordTypeCache.getId('Account', 'Full_Member')
            , Salutation = 'Mr'
            , FirstName = 'John'
            , LastName = Last
            , Preferred_Name__c = 'Johnny'
            , Gender__c = 'Male'
            , PersonBirthdate = DOB
            , Membership_Type__c = 'Member'
            , Membership_Class__c = 'Full'
            , Designation__c = 'CA'
            , Member_Of__c = 'NZICA'
            , Membership_Approval_Date__c = Date.valueOf('1970-01-29:00:00')
            , Financial_Category__c = 'Honorary Retired'
            , Status__c = 'Active'
            , Assessible_for_CAF_Program__c = false
            , Job_Title__c = JobTitle
            , PersonOtherPhone = '1234567'
            , PersonEmail = email
            // NZ attributes
            , PersonOtherCountry = 'New Zealand'
            , PersonMailingCountry = 'New Zealand'
            , Affiliated_Branch_Country__c = 'New Zealand'
            , Affiliated_Branch__c = 'Auckland'
             ,Communication_Preference__c= 'Home Phone'
            ,PersonHomePhone= '1234'
            ,PersonOtherStreet= '83 Saggers Road'
            ,PersonOtherCity='JITARNING'
            ,PersonOtherPostalCode='6365' 
            
            
            
        );
        return accountPersonNZ;
 }

    //   genSpecialistNewZealand('Smith', 'Senior Accountant', 'smith1@test.co.nz', Date.valueOf('1980-05-12') )    
    public static Account genSpecialistNewZealand(String Last, String JobTitle, String email, Date DOB ) {       
        Account accountPersonNZ = new Account(
            RecordTypeId = RecordTypeCache.getId('Account', 'Full_Member')
            , Salutation = 'Mr'
            , FirstName = 'John'
            , LastName = Last
            , Preferred_Name__c = 'Johnny'
            , Gender__c = 'Male'
            , PersonBirthdate = DOB
            , Membership_Type__c = 'Member'
            , Membership_Class__c = 'Full'
            , Designation__c = 'CA'
            , Member_Of__c = 'NZICA'
            , Membership_Approval_Date__c = Date.valueOf('2000-01-29:00:00')
            , Financial_Category__c = 'Standard fee applies'
            , Status__c = 'Active'
            , Assessible_for_CAF_Program__c = false
            , Job_Title__c = JobTitle
            , PersonOtherPhone = '1234567'
            , PersonEmail = email
            // NZ attributes
            , PersonOtherCountry = 'New Zealand'
            , PersonMailingCountry = 'New Zealand'
            , Affiliated_Branch_Country__c = 'New Zealand'
            , Affiliated_Branch__c = 'Auckland'
            // Qualification
            , Qualified_Auditor__c = true
            , QA_Date_of_suspension__c = null
            , QA_Date_of_de_recognition__c = null
            , CPP_Picklist__c = 'Full'
        );
        return accountPersonNZ;
 }
    
    
    // genPracticeNewZealand( 'Ernst & Young', 'Chartered Accounting', 'Auckland', 15)
    public static Account genPracticeNewZealandOld( String BusName, String BusType, String BusCity, Decimal CPPCount)   {
        Account accountBusinessNZ = new Account(
            RecordTypeId = RecordTypeCache.getId('Account', 'Business_Account')
            , Name = BusName
            , Status__c = 'Active'
            , Member_ID__c = '12345'
            , Type = BusType
            , BillingStreet = 'Main Street'
            , BillingCity = BusCity
            , BillingCountry = 'New Zealand'
            , BillingPostalCode = '1234'
            , Member_Of__c = 'NZICA'
            , Website = 'www.ey.co.nz'
            , Affiliated_Branch_Country__c = 'New Zealand'
            , Affiliated_Branch__c = 'Auckland'

        );
        
        return accountBusinessNZ;
    }        
    
        public static Account genPracticeNewZealand( String BusName, String BusType, String BusCity, Decimal CPPCount)   {    
        Account accountBusinessNZ = new Account(
            RecordTypeId = RecordTypeCache.getId('Account', 'Business_Account')
            , Name = BusName
            , Status__c = 'Active'
            , Member_ID__c = '12345'
            , Type = BusType
            , BillingStreet = '123 Main Street'
            , BillingCity = BusCity
            , BillingCountry = 'New Zealand'
            , BillingPostalCode = '1234' 
            , Billing_DPID__c = '12345678'
            , ShippingCountry = 'New Zealand'
            , ShippingStreet = '123 Main Street'
            , ShippingCity = 'Auckland'
            , ShippingPostalCode = '2034'
            , Shipping_DPID__c = '12345678'
            , Member_Of__c = 'NZICA'
            , Phone = '0299887766'
            , Website = 'www.big.co.nz'
            , Affiliated_Branch_Country__c = 'New Zealand'
            , Affiliated_Branch__c = 'Auckland'

        ); 
        return accountBusinessNZ;
    }   
    
    

    public static Account genFullMemberAustralia(String Last, String JobTitle, String email, Date DOB  ) {      
        Account accountPersonAU = new Account( 
            RecordTypeId = RecordTypeCache.getId('Account', 'Full_Member')
            , Salutation = 'Mr'
            , FirstName = 'Robert'
            , LastName = Last
            , Preferred_Name__c = 'Bob'
            , Gender__c = 'Male'
            , PersonBirthdate = DOB
            , Membership_Type__c = 'Member'
            , Membership_Class__c = 'Full'
            , Designation__c = 'CA'
            , Member_Of__c = 'ICAA'
            , Membership_Approval_Date__c = Date.valueOf('2005-01-29:00:00')
            , Financial_Category__c = 'Standard fee applies'
            , Status__c = 'Active'
            , Assessible_for_CAF_Program__c = false
            , Job_Title__c = JobTitle
            , PersonOtherPhone = '1234567'
            , PersonEmail = email     
            // AU attributes
            , PersonOtherCountry = 'Australia'
            , PersonMailingCountry = 'Australia'
            , Affiliated_Branch_Country__c = 'Australia'
            , Affiliated_Branch__c = 'New South Wales'
            
            ,Communication_Preference__c= 'Home Phone'
            ,PersonHomePhone= '1234'
            ,PersonOtherStreet= '83 Saggers Road'
            ,PersonOtherCity='JITARNING'
            ,PersonOtherState='Western Australia'
           
            ,PersonOtherPostalCode='6365'
        );
        return accountPersonAU;
    }   
    
    public static Account genNonMemberAustralia(String Last, String JobTitle, String email, Date DOB  ) {      
        Account accountPersonAU = new Account( 
            RecordTypeId = RecordTypeCache.getId('Account', 'Non_member')
            , Salutation = 'Mr'
            , FirstName = 'Robert'
            , LastName = Last
            , Preferred_Name__c = 'Bob'
            , Gender__c = 'Male'
            , PersonBirthdate = DOB
            , Membership_Type__c = 'Non Member'
            , Member_Of__c = 'ICAA'
            , Status__c = 'Active'
            , Job_Title__c = JobTitle
            , PersonOtherPhone = '1234567'
            , PersonEmail = email     
            , Assessible_for_CAF_Program__c = false
            // AU attributes
            , PersonOtherCountry = 'Australia'
            , PersonMailingCountry = 'Australia'
            , Affiliated_Branch_Country__c = 'Australia'
            //, Affiliated_Branch__c = 'New South Wales'
        );
        return accountPersonAU;
    }
    
    public static Account genProvMemberAustralia(String Last, String JobTitle, String email, Date DOB  ) {      
        Account accountPersonAU = new Account( 
            RecordTypeId = RecordTypeCache.getId('Account', 'Full_Member')
            , Salutation = 'Mr'
            , FirstName = 'Robert'
            , LastName = Last
            , Preferred_Name__c = 'Bob'
            , Gender__c = 'Male'
            , PersonBirthdate = DOB
            , Membership_Type__c = 'Member'
            , Membership_Class__c = 'Provisional'
            , Designation__c = 'CA'
            , Member_Of__c = 'ICAA'
            , Membership_Approval_Date__c = Date.valueOf('2017-01-29:00:00')
            , Financial_Category__c = 'Standard fee applies'
            , Status__c = 'Active'
            , Assessible_for_CAF_Program__c = false
            , Job_Title__c = JobTitle
            , PersonOtherPhone = '1234567'
            , PersonEmail = email     
            // AU attributes
            , PersonOtherCountry = 'Australia'
            , PersonMailingCountry = 'Australia'
            , Affiliated_Branch_Country__c = 'Australia'
            , Affiliated_Branch__c = 'New South Wales'
             ,Communication_Preference__c= 'Home Phone'
            ,PersonHomePhone= '1234'
            ,PersonOtherStreet= '83 Saggers Road'
            ,PersonOtherCity='JITARNING'
            ,PersonOtherState='Western Australia'
           
            ,PersonOtherPostalCode='6365'
        );
        return accountPersonAU;
    }
 
    public static Account genSpecialistAustralia(String Last, String JobTitle, String email, Date DOB  ) {
        Account accountPersonNZ = new Account(
            RecordTypeId = RecordTypeCache.getId('Account', 'Full_Member')
            , Salutation = 'Mr'
            , FirstName = 'Susan'
            , LastName = Last
            , Preferred_Name__c = 'Suzi'
            , Gender__c = 'Male'
            , PersonBirthdate = DOB
            , Membership_Type__c = 'Member'
            , Membership_Class__c = 'Full'
            , Designation__c = 'CA'
            , Member_Of__c = 'ICAA'
            , Membership_Approval_Date__c = Date.valueOf('2005-01-29:00:00')
            , Financial_Category__c = 'Standard fee applies'
            , Status__c = 'Active'
            , Assessible_for_CAF_Program__c = false
            , Job_Title__c = JobTitle
            , PersonOtherPhone = '1234567'
            , PersonEmail = email   
            // AU attributes
            , PersonOtherCountry = 'Australia'
            , PersonMailingCountry = 'Australia'
            , Affiliated_Branch_Country__c = 'Australia'
            , Affiliated_Branch__c = 'New South Wales'  
            ,PersonHomePhone= '1234'
            ,PersonOtherStreet= '83 Saggers Road'
            ,PersonOtherCity='JITARNING'
            ,PersonOtherPostalCode='6365' 
            
                      
            // Specialists (SMSF, BV, FP) attributes
            , SMSF__c = true
            , BV__c = true
            , FP_Specialisation__c = true
            , BV_Areas_of_Practice__c =  'Tax;Family Law'
            , FP_Areas_of_Practice__c =  'TBA'
            , SMSF_Areas_of_Practice__c =  'TBA'  
            // Public Practitioner attributes
            , CPP_Picklist__c = 'Full'
            
        );
        return accountPersonNZ;
    }  
    
    // genPracticeAU( 'Ernst & Young', 'Chartered Accounting', 'Sydney', 25)
    public static Account genPracticeAU( String BusName, String BusType, String BusCity, Decimal CPPCount)   {    
        Account accountBusinessAU = new Account(
            RecordTypeId = RecordTypeCache.getId('Account', 'Business_Account')
            , Name = BusName
            , Status__c = 'Active'
            , Member_ID__c = '12345'
            , Type = BusType
            , BillingStreet = '55 George Street'
            , BillingCity = BusCity
            , BillingCountry = 'Australia'
            , BillingState = 'New South Wales'
            , BillingPostalCode = '1234' 
            , Billing_DPID__c = '12345678'
            , ShippingCountry = 'Australia'
            , ShippingStreet = '456 Pitt Street'
            , ShippingCity = 'Sydney'
            , ShippingState = 'New South Wales'
            , ShippingPostalCode = '2034'
            , Shipping_DPID__c = '12345678'
            , Member_Of__c = 'ICAA'
            , Phone = '0299887766'
            , Website = 'www.big.co.au'
            , Affiliated_Branch_Country__c = 'Australia'
            , Affiliated_Branch__c = 'Sydney'

        ); 
        return accountBusinessAU;
    }        
      
    // genCorporateAU( 'ANZ', 'Commerce', 'Sydney', 15)
    public static Account genCorporateAU( String BusName, String BusType, String BusCity, Decimal CPPCount)   {    
        Account accountBusinessAU = new Account(
            RecordTypeId = RecordTypeCache.getId('Account', 'Business_Account')
            , Name = BusName
            , Status__c = 'Active'
            , Member_ID__c = '12345'
            , Type = BusType
            , BillingStreet = '110 Collins Street'
            , BillingCity = BusCity
            , BillingCountry = 'Australia'
            , BillingState = 'Victoria'
            , BillingPostalCode = '3034'
            , ShippingCountry = 'Australia'
            , ShippingStreet = '110 Collins Street'
            , ShippingCity = 'Melbourne'
            , ShippingState = 'Victoria'
            , ShippingPostalCode = '3034'
            , Member_Of__c = 'ICAA'
            , Website = 'www.big.co.au'
            , Affiliated_Branch_Country__c = 'Australia'
            , Affiliated_Branch__c = 'Melbourne'

        ); 
        return accountBusinessAU;
    }        
    
    
    // Overseas Member
    public static Account genFullMemberOverseas(String Last, String JobTitle, String email, Date DOB  ) {      
        Account accountPersonHK = new Account( 
            RecordTypeId = RecordTypeCache.getId('Account', 'Full_Member')
            , Salutation = 'Mr'
            , FirstName = 'Bruce'
            , LastName = Last
            , Preferred_Name__c = 'Bruce'
            , Gender__c = 'Male'
            , PersonBirthdate = DOB
            , Membership_Type__c = 'Member'
            , Membership_Class__c = 'Full'
            , Designation__c = 'CA'
            , Member_Of__c = 'ICAA'
            , Membership_Approval_Date__c = Date.valueOf('2009-01-29:00:00')
            , Financial_Category__c = 'Standard fee applies'
            , Status__c = 'Active'
            , Assessible_for_CAF_Program__c = false
            , Job_Title__c = JobTitle
            , PersonOtherPhone = '1234567'
            , PersonEmail = email     
            // AU attributes
            , PersonOtherCountry = 'Hong Kong'
            , PersonMailingCountry = 'Hong Kong'
            , Affiliated_Branch_Country__c = 'Overseas'
            , Affiliated_Branch__c = NULL
            ,Communication_Preference__c= 'Home Phone'
            ,PersonHomePhone= '1234'
            ,PersonOtherStreet= '83 Saggers Road'
            ,PersonOtherCity='JITARNING'
            
            ,PersonOtherPostalCode='6365'
        );
        return accountPersonHK;
    }
 
    // genPracticeOverseas( 'Ernst & Young', 'Chartered Accounting', 'Hong Kong', 0)
    public static Account genPracticeOverseas( String BusName, String BusType, String BusCity, Decimal CPPCount)   {    
        Account accountBusinessAU = new Account(
            RecordTypeId = RecordTypeCache.getId('Account', 'Business_Account')
            , Name = BusName
            , Status__c = 'Active'
            , Member_ID__c = '12345'
            , Type = BusType
            , BillingStreet = 'Main Street'
            , BillingCity = BusCity
            , BillingCountry = 'Hong Kong'
            , BillingState = NULL
            , BillingPostalCode = '1234'
            , ShippingCountry = 'Hong Kong'
            , ShippingStreet = '123 Street'
            , ShippingCity = 'Hong Kong'
            , ShippingState = NULL
            , ShippingPostalCode = '1234'
            , Member_Of__c = 'ICAA'
            , Website = 'www.bigBus.co.hk'
            , Affiliated_Branch_Country__c = 'Overseas'
            , Affiliated_Branch__c = 'Hong Kong'

        ); 
        return accountBusinessAU;
    }      
 
    
    public static Employment_History__c genCurrentEmploymentHistory(Id memberId, Id employerId, String jobClass, String jobTitle){
        Employment_History__c eh = new Employment_History__c();
        eh.Member__c = memberId;
        eh.Employer__c = employerId;
        eh.Status__c = 'Current';
        eh.Primary_Employer__c = true;
        eh.Job_Title__c = jobTitle;
        eh.Job_Class__c = jobClass;
        eh.Director__c = False; 
        eh.Partner__c = False;  
        eh.Principal__c = False; 
        eh.Employee_Start_Date__c = system.today().addDays(-100);
        eh.Employee_End_Date__c = null;
        return eh;
    }   

    public static Employment_History__c genTodayEmploymentHistory(Id memberId, Id employerId, String jobClass, String jobTitle){
        Employment_History__c eh = new Employment_History__c();
        eh.Member__c = memberId;
        eh.Employer__c = employerId;
        eh.Status__c = 'Current';
        eh.Primary_Employer__c = true;
        eh.Job_Title__c = jobTitle;
        eh.Job_Class__c = jobClass;
        eh.Director__c = False; 
        eh.Partner__c = False;  
        eh.Principal__c = False; 
        eh.Employee_Start_Date__c = system.today();
        eh.Employee_End_Date__c = null;
        return eh;
    }  
    
    
    // genCurrentEmploymentHistoryDirector(memberAccount.Id, businessAccount.Id, 'Accountant', 'Senior Accountant')  
    public static Employment_History__c genCurrentEmploymentHistoryDirector(Id memberId, Id employerId, String jobClass, String jobTitle){
        Employment_History__c eh = new Employment_History__c();
        eh.Member__c = memberId;
        eh.Employer__c = employerId;
        eh.Status__c = 'Current';
        eh.Primary_Employer__c = true;
        eh.Job_Title__c = jobTitle;
        eh.Job_Class__c = jobClass;
        eh.Director__c = true; 
        eh.Partner__c = true;  
        eh.Principal__c = False; 
        eh.Employee_Start_Date__c = system.today().addDays(-100);
        eh.Employee_End_Date__c = null;
        return eh;
    } 
    
    public static Employment_History__c genHistoricalEmploymentHistory(Id memberId, Id employerId,  String jobClass, String jobTitle){
        Employment_History__c eh = new Employment_History__c();
        eh.Member__c = memberId;
        eh.Employer__c = employerId;
        eh.Status__c = 'Closed';
        eh.Primary_Employer__c = false;
        eh.Job_Title__c = jobTitle;
        eh.Job_Class__c = jobClass;
        eh.Director__c = False; 
        eh.Partner__c = False;  
        eh.Principal__c = False; 
        eh.Employee_Start_Date__c = system.today().addDays(-500);
        eh.Employee_End_Date__c = system.today().addDays(-100);
        return eh;
    }   
     

    public Map<String, Id> getRecordTypeIdMap(String sobj){
        Map<String, Id> recordTypeMap = new Map<String, Id>();
        for(RecordType rt: [SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType=:sobj]){
            recordTypeMap.put(rt.DeveloperName, rt.Id);
        }
        return recordTypeMap;
    }
    

    public Employment_History__c createNewEmploymentHistory(Id memberId, Id employerId, String status, String jobTitle){
        Employment_History__c eh = new Employment_History__c();
        eh.Member__c = memberId;
        eh.Employer__c = employerId;
        eh.Status__c = status;
        eh.Nominated_Employee__c = true;
        eh.Primary_Employer__c = true;
        eh.Job_Title__c = jobTitle;
        eh.Employee_Start_Date__c = system.today();
        
        if(status == 'Closed'){
            eh.Employee_End_Date__c = system.today().addDays(1);
        }
        return eh;
    }    
}