@isTest(SeeAllData=true)
public class TestQPRFormAssemblyRedirect {
@isTest
    public static void testPageRedirection(){
        Test.setCurrentPageReference(new PageReference('QPRFormAssemblyRedirect'));
		System.currentPageReference().getParameters().put('formaction','au_savenresume');
        QPRFormAssemblyRedirect qprObj_1 = new QPRFormAssemblyRedirect();
        qprObj_1.generateFormURL();
        System.currentPageReference().getParameters().put('formaction','au_savensubmit');
        QPRFormAssemblyRedirect qprObj_2 = new QPRFormAssemblyRedirect();
        qprObj_2.generateFormURL();
        System.currentPageReference().getParameters().put('formaction','nz_savenresume');
        QPRFormAssemblyRedirect qprObj_3 = new QPRFormAssemblyRedirect();
        qprObj_3.generateFormURL();
        System.currentPageReference().getParameters().put('formaction','nz_savensubmit');
        QPRFormAssemblyRedirect qprObj_4 = new QPRFormAssemblyRedirect();
        qprObj_4.generateFormURL();

        
    }
}