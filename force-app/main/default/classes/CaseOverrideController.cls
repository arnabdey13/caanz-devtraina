public with sharing class CaseOverrideController {
	public PageReference gotoTabPage(){
		if( UserInfo.getProfileId()==ProfileCache.getId('NZICA Community Login User') ){
			return Page.CaseTab;
		}
		else{
			String currentPageUrl = ApexPages.currentPage().getUrl();
			currentPageUrl = currentPageUrl.replace('/apex/CaseTabOverride',
				'/'+ Schema.sObjectType.Case.getKeyPrefix() +'/o');
			currentPageUrl = currentPageUrl.replace('save_new=1','');
			PageReference PR = new PageReference( currentPageUrl );
			PR.getParameters().put('nooverride','1');
			// https://cs5.salesforce.com/500/o?tsid=02u90000000k85w // Changing app
			
			return PR;
		}
	}
}