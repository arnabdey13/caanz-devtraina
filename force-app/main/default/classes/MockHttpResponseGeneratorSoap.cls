/***********************************************************************************************************************************************************************
Name: MockHttpResponseGeneratorSoap 
============================================================================================================================== 
Purpose: This class contains code related to MockResponse of class SoapLoginClass 
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR           DATE          DETAIL     Description 
1.0     Sudheendra G S    26/04/2019    Created     This class contains code related to MockResponse of class SoapLoginClass.
                                                             
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/
@isTest
global class MockHttpResponseGeneratorSoap implements HttpCalloutMock {

    private static String SERVER_URL = URL.getSalesforceBaseUrl().toExternalForm();
    private static String SOAP_PATH = '/services/Soap/u/26.0/';
	private static String ORG_ID = UserInfo.getOrganizationId().substring(0, UserInfo.getOrganizationId().length() - 3);    

    
    global HTTPResponse respond(HTTPRequest req) {
        // System.assertEquals(SERVER_URL + SOAP_PATH, req.getEndpoint());
        System.assertEquals('POST', req.getMethod());
        
        // Create a Mock response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        //res.setBody('{"<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns="urn:partner.soap.sforce.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><loginResponse><result><metadataServerUrl>https://charteredaccountantsanz--devTrainA.cs6.my.salesforce.com/services/Soap/m/26.0/00DN0000000RCwr</metadataServerUrl><passwordExpired>false</passwordExpired><sandbox>true</sandbox><serverUrl>https://charteredaccountantsanz--devTrainA.cs6.my.salesforce.com/services/Soap/u/26.0/00DN0000000RCwr</serverUrl><sessionId>SESSION_ID_REMOVED</sessionId><userId>00590000004D1QcAAK</userId><userInfo><accessibilityMode>false</accessibilityMode><currencySymbol xsi:nil="true"/><orgAttachmentFileSizeLimit>5242880</orgAttachmentFileSizeLimit><orgDefaultCurrencyIsoCode xsi:nil="true"/><orgDisallowHtmlAttachments>false</orgDisallowHtmlAttachments><orgHasPersonAccounts>true</orgHasPersonAccounts><organizationId>00DN0000000RCwrMAG</organizationId><organizationMultiCurrency>true</organizationMultiCurrency><organizationName>Chartered Accountants Australia  New Zealand</organizationName><profileId>00e90000001JzUPAA0</profileId><roleId>00E90000000dkLFEAY</roleId><sessionSecondsValid>7200</sessionSecondsValid><userDefaultCurrencyIsoCode>AUD</userDefaultCurrencyIsoCode><userEmail>sudheendra.gs@charteredaccountantsanz.com</userEmail><userFullName>FormAssembly Integration</userFullName><userId>00590000004D1QcAAK</userId><userLanguage>en_US</userLanguage><userLocale>en_NZ</userLocale><userName>formassembly@charteredaccountantsanz.com.devtraina</userName><userTimeZone>Pacific/Auckland</userTimeZone><userType>Standard</userType><userUiSkin>Theme3</userUiSkin></userInfo></result></loginResponse></soapenv:Body></soapenv:Envelope>"}');
        res.setBody('{"<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns="urn:partner.soap.sforce.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><loginResponse><result><metadataServerUrl>'+SERVER_URL+'/services/Soap/m/26.0/'+ORG_ID+'</metadataServerUrl><passwordExpired>false</passwordExpired><sandbox>true</sandbox><serverUrl>'+SERVER_URL+'/services/Soap/u/26.0/'+ORG_ID+'</serverUrl><sessionId>SESSION_ID_REMOVED</sessionId><userId>00590000004D1QcAAK</userId><userInfo><accessibilityMode>false</accessibilityMode><currencySymbol xsi:nil="true"/><orgAttachmentFileSizeLimit>5242880</orgAttachmentFileSizeLimit><orgDefaultCurrencyIsoCode xsi:nil="true"/><orgDisallowHtmlAttachments>false</orgDisallowHtmlAttachments><orgHasPersonAccounts>true</orgHasPersonAccounts><organizationId>'+ORG_ID+'MAG</organizationId><organizationMultiCurrency>true</organizationMultiCurrency><organizationName>Chartered Accountants Australia  New Zealand</organizationName><profileId>00e90000001JzUPAA0</profileId><roleId>00E90000000dkLFEAY</roleId><sessionSecondsValid>7200</sessionSecondsValid><userDefaultCurrencyIsoCode>AUD</userDefaultCurrencyIsoCode><userEmail>sudheendra.gs@charteredaccountantsanz.com</userEmail><userFullName>FormAssembly Integration</userFullName><userId>00590000004D1QcAAK</userId><userLanguage>en_US</userLanguage><userLocale>en_NZ</userLocale><userName>formassembly@charteredaccountantsanz.com.devtraina</userName><userTimeZone>Pacific/Auckland</userTimeZone><userType>Standard</userType><userUiSkin>Theme3</userUiSkin></userInfo></result></loginResponse></soapenv:Body></soapenv:Envelope>"}');
        res.setStatusCode(200);
        
        return res;
    }
}