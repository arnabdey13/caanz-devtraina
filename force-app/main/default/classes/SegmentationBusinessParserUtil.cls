public class SegmentationBusinessParserUtil{

	private static Map<String, List <Reference_Main_Practices__c> > lkp;
    private static Boolean lkpLoaded = false;
	private static Boolean DebugFlag = false;
    private static Boolean MatchDebugFlag = false;
	    
	/***
	* Utilities for business Names Matching
	***/
	public SegmentationBusinessParserUtil() {
		lkp = LoadMainPraticesLookup();
        lkpLoaded = true;
		if (DebugFlag )
			for ( String entryKey : lkp.keySet() ) {
				System.debug('Key: ' + entryKey + ' Values: ' + lkp.get(entryKey) );          
			}  
	}

    
	
	/***
	* Often there are multiple patterns in recognition
	* For example PriceWaterhouseCoopers could have Pattern = 'PWC, PwC, PriceWaterhouseCoopers, pricewaterhousecoopers' etc
	* This function splits patterns into a collection of single patterns
	* 
	* Also splits multiwords into a Collection of word
	* 
	***/
	private static List<String> delimitedStringToList(String Patterns, String delimiter) { 
		if (String.isEmpty(Patterns)) 
			return new List<String>(); 
        
		List<String> ptrns = Patterns.split(delimiter);
		List<String> cleanPatterns = new List<String>();
		for(String pattern : ptrns) {
			cleanPatterns.add(pattern.trim());
		}
		return cleanPatterns;
	} 

    
	/***
    * LoadLookupMap
    *  Map: 
    *  Key representing a single token of the pattern
    *  Value: List of Names (Reference Objects) for that Key
    *  The same key can represent two or more different Objects
    *  A single token part of the Matching Pattern becomes a key
    *
    *  Example: Key    Matched Names
    *           Ernst, <'Ernst And Young'>
    *           Young, <'Ernst And Young'>, < Young And Old Associates> 
    * 		          
    *
    ***/
    private static Map<String, List<Reference_Main_Practices__c>> LoadMainPraticesLookup() {
		Map<String, List<Reference_Main_Practices__c>> mp = new Map<String, List<Reference_Main_Practices__c>>();   
		List <Reference_Main_Practices__c> prcts =  [Select Id, Matching_Tokens__c, Search_Name__c, Full_Name__c, Practice_Size__c, Big4__c From Reference_Main_Practices__c ];
     
        // get a list matching patterns e.g. PWC = <'PWC, PwC, PriceWaterhouseCoopers, pricewaterhousecoopers'>
        for(Reference_Main_Practices__c practice : prcts ) {
			if (DebugFlag )
            	System.debug('Adding new Practice to lookup: ' + practice.Search_Name__c );   
            // split String containing multiple matching patterns delimited by ; into a List of single patterns delimited by commma
            List <String> patterns = delimitedStringToList(practice.Matching_Tokens__c, ';');
        	for (String pat: patterns) {
            	// Strip unwanted noise from pattern, we do not want the noise to become key
            	String pattern = stripCommonNoise(pat); 
                pattern = stripAccountingNoise(pattern);
            	// Split each pattern into a set of tokens (words) delimited by space	
                for (String tok : delimitedStringToList(pattern, ' ') ) {
					String token = tok.trim();
					if (String.isEmpty(token.trim())) 
	        			continue; 
            		// if the key already exists for that token, add practice to the existing List otherwise add to a new list
            		if (mp.containsKey(token) ) {
                        List<Reference_Main_Practices__c> currListForKey = mp.get(token);
                        currListForKey.add(practice);
                        mp.put(token, currListForKey);
                    } 
                    else {
                        List<Reference_Main_Practices__c> newListForKey = new List<Reference_Main_Practices__c>();
                        newListForKey.add(practice);
                        if (DebugFlag )
                        	System.debug('Adding new Key: ' + token + ' Values: ' + newListForKey );   
                        mp.put(token, newListForKey);
                    }
                }
        	}
        }
        
        if (DebugFlag ) {
			Set <String> Allkeys = mp.keySet();
        	for (String key : Allkeys)
				System.debug('Lookup: ' + key);             
        }
        return mp;
    }
    
    

    public Map<String, List<Reference_Main_Practices__c>> getLookup() {
        return lkp;
    }
    
    // obsolete
    private boolean isMatch(String companyName, String MatchPattern ) {
        if ( !companyName.containsIgnoreCase(MatchPattern) )
            return false;
        
        if ( companyName.equals(MatchPattern)                  ||
             companyName.startsWith(MatchPattern + ' ')        ||
             companyName.contains(' ' + MatchPattern + ' ')    ||
             companyName.endsWith(' ' + MatchPattern)
           ) 
            return true;
        else
            return false;
    }
    
    
    /***
    * Main Practice record
    ***/
    public Reference_Main_Practices__c mainPracticeRef(String busname) {
 
		// clean the common noise from the name to allow for clean spliting
        String company = stripCommonNoise(busname);
		
		List<String> tokens = delimitedStringToList(company, ' ');
		for(String token : tokens) {
			if ( lkp.containsKey(token) ) {
				if (MatchDebugFlag )
					System.debug('Recognised key ' + token);
                
				List<Reference_Main_Practices__c> prcts = lkp.get(token);
				for (Reference_Main_Practices__c practice : prcts) {
					if (MatchDebugFlag )
						System.debug('Token Search Name: ' + practice.Search_Name__c + ' company: ' + company);
                    
					List <String> patterns = delimitedStringToList(practice.Matching_Tokens__c, ';');
					for (String pattern: patterns) {
                        if (MatchDebugFlag )
								System.debug('For Pattern:' + pattern + ' and Company to match ' + busname);
                        
                        if (company.contains(pattern) || busname.contains(pattern) ) {
							if (MatchDebugFlag )
                            	System.debug('Matched:' + practice.Matching_Tokens__c);
							return practice;
						}
						else {
							if (MatchDebugFlag )
								System.debug('Not Matched:' + practice.Matching_Tokens__c + ' company: ' + company);
						}
					}

				}
			}
        }
        return (new Reference_Main_Practices__c() );
    }
    
    

	/***
    * Get the Name of the Main Practice 
    ***/
    public String whatMainPractice(String busname) {
         // clean the common noise from the name to allow for clean spliting
         String company = stripCommonNoise(busname);
        
        
    	 List<String> tokens = delimitedStringToList(company, ' ');
    	 for(String token : tokens) {
			if ( lkp.containsKey(token) ) {
				if (MatchDebugFlag )
					System.debug('Recognised key ' + token);
                
				List<Reference_Main_Practices__c> prcts = lkp.get(token);
				for (Reference_Main_Practices__c practice : prcts) {
					if (MatchDebugFlag )
						System.debug('Token Search Name: ' + practice.Search_Name__c + ' company: ' + company);
                    
					List <String> patterns = delimitedStringToList(practice.Matching_Tokens__c, ';');
					for (String pattern: patterns) {
                        if (MatchDebugFlag )
								System.debug('For Pattern:' + pattern + ' and Company to match ' + busname);
                        
                        if (company.contains(pattern) || busname.contains(pattern) ) {
							if (MatchDebugFlag )
                            	System.debug('Matched:' + practice.Matching_Tokens__c);
							return practice.Search_Name__c;
						}
						else {
							if (MatchDebugFlag )
								System.debug('Not Matched:' + practice.Matching_Tokens__c + ' company: ' + company);
						}
					}

				}
			}
        }
        return '';
    }
 
	  

    /***
	*Strip common noise from the companyname
	***/
	
	public static String stripCommonNoise(String BusName)
	{
		if(String.isEmpty(BusName))
			return null;
	
		BusName=BusName.replace('-',' ');
        BusName=BusName.replace(',',' ');
        BusName=BusName.replace('(',' ');
        BusName=BusName.replace(')',' ');
        
        BusName=BusName.remove(' And');
		BusName=BusName.remove(' and');
		BusName=BusName.remove(' AND');
        BusName=BusName.remove(' &');
        
        BusName=BusName.remove(' Co.');
        
       	BusName=BusName.remove(' Pty');
		BusName=BusName.remove(' pty');
		BusName=BusName.remove(' Ltd');
		BusName=BusName.remove(' Ltd.');
		BusName=BusName.remove(' ltd');
		BusName=BusName.remove(' ltd.');
        BusName=BusName.remove('Limited');
	
		return(BusName);
    }
    
    public static String stripAccountingNoise(String BusName)
	{
		if(String.isEmpty(BusName))
			return null;
	
		BusName=BusName.remove('Accountancy');
        BusName=BusName.remove('Accountants');
        BusName=BusName.remove('Accountant');
		BusName=BusName.remove('Accounting');
        
        BusName=BusName.remove('Advisers');
        BusName=BusName.remove('Advisors');
        BusName=BusName.remove('Advisory');
        BusName=BusName.remove('Adviser');
        BusName=BusName.remove('Advisor');
        
        BusName=BusName.remove('Asia');
        BusName=BusName.remove('Pacific');       

        
		return(BusName);
    }
    

	/***
	*Strip typical business name noise from the companyname
	***/
	
	public static String stripBusNoise(String BusName)
	{
		if(String.isEmpty(BusName))
			return null;
	
		BusName=BusName.toUpperCase();
		
		BusName=BusName.remove(' PROP LTD');
        
		BusName=BusName.remove(' PTY');
		BusName=BusName.remove(' PTY.');
		BusName=BusName.remove(' LTD');
		BusName=BusName.remove(' LTD.');
		BusName=BusName.remove(' LIMITED');
		BusName=BusName.remove(' COMPANY');
		BusName=BusName.remove(' COMP ');
		BusName=BusName.remove(' CO ');
		BusName=BusName.remove(' INC');
		
		BusName=BusName.remove(' PROPRIETARY');
		BusName=BusName.remove(' PROP ');
		BusName=BusName.remove(' CORPORATION');
		BusName=BusName.remove(' CORP ');
		BusName=BusName.remove(' ASSOCIATION');
		
		BusName=BusName.remove(' AUSTRALIA');
		BusName=BusName.remove(' AUST');
		BusName=BusName.remove(' NEW ZEALAND');
		BusName=BusName.remove(' NZ ');
		BusName=BusName.remove(' ASIA');
		BusName=BusName.remove(' INTERNATIONAL');
		BusName=BusName.remove(' (AUST)');
		BusName=BusName.remove(' (AUS)');
	
		BusName=BusName.remove(' ACT');
		BusName=BusName.remove(' QLD');
		BusName=BusName.remove(' SA');
		BusName=BusName.remove(' NSW');
		BusName=BusName.remove(' NT');
		BusName=BusName.remove(' TAS');
		BusName=BusName.remove(' VIC');
		BusName=BusName.remove(' WA');
		
		BusName=BusName.remove(' (ACT)');
		BusName=BusName.remove(' (QLD)');
		BusName=BusName.remove(' (SA)');
		BusName=BusName.remove(' (NSW)');
		BusName=BusName.remove(' (NT)');
		BusName=BusName.remove(' (TAS)');
		BusName=BusName.remove(' (VIC)');
		BusName=BusName.remove(' (WA)');
		
		BusName=BusName.remove(' (SYDNEY)');
		BusName=BusName.remove(' (MELBOURNE)');
		BusName=BusName.remove(' (BRISANE)');
		BusName=BusName.remove(' (AUCKLAND)');
		BusName=BusName.remove(' (WELLINGTON)');
		BusName=BusName.remove(' (PERTH)');
		BusName=BusName.remove(' (ADELAIDE)');
		BusName=BusName.remove(' (CANBERRA)');
		
		BusName=BusName.remove(' SYDNEY');
		BusName=BusName.remove(' MELBOURNE');
		BusName=BusName.remove(' BRISANE');
		BusName=BusName.remove(' AUCKLAND');
		BusName=BusName.remove(' WELLINGTON');
		BusName=BusName.remove(' PERTH');
		BusName=BusName.remove(' ADELAIDE');
		BusName=BusName.remove(' CANBERRA');
		
		BusName=BusName.remove(' THE');
		BusName=BusName.remove('THE ');
		BusName=BusName.remove(' OF');
		
		return(BusName);
	}
	
}