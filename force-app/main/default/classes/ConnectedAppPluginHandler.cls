/*------------------------------------------------------------------------------------
Author:        Sumit Pandey
Company:       Tech Mahindra
Description:   Custom Connected App Handler - Can be use to add setup/non-setup custom attribute(In SAML assertion)

History
Date            Author             Comments
--------------------------------------------------------------------------------------
24-09-2019      Sumit Pandey       Initial Release
------------------------------------------------------------------------------------*/
global with sharing class ConnectedAppPluginHandler extends Auth.ConnectedAppPlugin
{
    global override Map<String,String> customAttributes(Id userId, Id connectedAppId, Map<String,String>
                                                        formulaDefinedAttributes, Auth.InvocationContext context)         
    {  
        /* get Account Id so that we can query Segmentation Record for LinkedIn */
        User usr = [Select Id, Email, Name, FirstName, LastName, Profile.Name,Contact.AccountId, 
                    Account.Affiliated_Branch_Country__c,AccountId From User where Id =:userId];
        
        String ConnectedAppName = getConnecteAppName(connectedAppId);
	
        /* Check if Connected App is LinkedIn */
        if(ConnectedAppName == 'LinkedInLearning'){
           formulaDefinedAttributes =  getLinkedInSpecificAttribute(usr);
           
        }
        /* Check if Connected App is NetSuite */
        else if(ConnectedAppName == 'NetSuite'){
           formulaDefinedAttributes =  getNetSuiteSpecificAttribute(usr);
        }
        
        return formulaDefinedAttributes;     
    }

    /* Return LinkedIn Connected App Specific Attribute */
    global Map<String,String> getLinkedInSpecificAttribute(User usr){

         Map<String,String> formulaDefinedAttributes = new Map<String,String>();

         List<Segmentation__c> seg = [Select Id, Career_Stage__c,Account__r.Membership_Class__c, Organisation_Type__c From 
                                         Segmentation__c where Account__c =: usr.Contact.AccountId
                                         order by createdDate desc limit 1];

            formulaDefinedAttributes.put('FirstName',usr.FirstName);
            formulaDefinedAttributes.put('LastName',usr.LastName);
            formulaDefinedAttributes.put('Email',usr.Email);
            formulaDefinedAttributes.put('Assign Groups',usr.Profile.Name == 'CAANZ CCH Community Provisional Member' ? 
                                         'CA Provisional':'CA Full Member');
            
            if(!seg.isEmpty()){
                /* Set Value for Career Stage */
                if(String.isEmpty(seg.get(0).Career_Stage__c)){
                    formulaDefinedAttributes.put('Career Stage', seg.get(0).Account__r.Membership_Class__c == 'Provisional' ? 
                                                 'Provisional' : 'Team Player');
                    
                }
                else{
                    formulaDefinedAttributes.put('Career Stage', seg.get(0).Career_Stage__c);
                }
                
                /* Set Value for Organization Type */
                if(!String.isEmpty(seg.get(0).Organisation_Type__c)){
                    formulaDefinedAttributes.put('Organization Type', seg.get(0).Organisation_Type__c == 'Unallocated' ? 
                                                 'Commerce' : seg.get(0).Organisation_Type__c);
                }
                else{
                    formulaDefinedAttributes.put('Organization Type','Commerce');
                }
            }
            else{
                formulaDefinedAttributes.put('Career Stage','Team Player');
                formulaDefinedAttributes.put('Organization Type','Commerce');
            }

            return formulaDefinedAttributes;

    }

    /* Return NetSuite Connected App Specific Attribute */
    global Map<String,String> getNetSuiteSpecificAttribute(User usr){
        Map<String,String> formulaDefinedAttributes = new Map<String,String>();
        if(usr.Account.Affiliated_Branch_Country__c == 'New Zealand'){
                formulaDefinedAttributes.put('1','https://test.salesforce.com');
        }
        else{
                formulaDefinedAttributes.put('3','https://login.salesforce.com');
        }
        return formulaDefinedAttributes;
    }
    
    /* Return ConnectedApp Name */
    global String getConnecteAppName(Id connectedAppId){
        ConnectedApplication conApp = [Select Id, Name From ConnectedApplication 
                                       where Id =:connectedAppId limit 1];
        return conApp.Name;
   }
    
    
}