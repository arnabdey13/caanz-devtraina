/*
    Change History:
    LCA-887 04/08/2016 - WDCi Lean: display user timezone next to date field
            29/03/2018 - WDCi KH: include case Publish__c field into getRequests() method's SOQL query
*/

public with sharing class luana_MyRequestHome {
    Id custCommConId;
    Id custCommAccId;
    Case c;
    
    public Id selectedCaseForView {get; set;}
    public String custCommCaseURL {get; set;}
    public String viewCaseId {get; set;}
    
    public Map<Id, Boolean> hasUnreadComment {get; private set;}
    
    public String userTimeZone {private set;get;}
    public Boolean isMember {get; private set;} // Flexipath Project - to show Competency Area Request in MyRequest, for Non-Member
    
    public luana_MyRequestHome(ApexPages.StandardController controller) {
        c = (Case)controller.getRecord();
        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId()); 
       
        this.custCommConId = commUserUtil.custCommConId;       
        this.custCommAccId = commUserUtil.custCommAccId;  
        custCommCaseURL = luana_NetworkUtil.getCommunityPath() +'/luana_RequestView?csId='; //LCA-347
        
        hasUnreadComment = new Map<Id, Boolean>();
        
        userTimeZone = UserInfo.getTimeZone().getID(); //LCA-887
        
        //Flexipath Project - to show Competency Area Request in MyRequest, for Non-Member
        if(commUserUtil.hasAccessToMember){ 
          isMember = true;
                
        } else if(commUserUtil.hasAccessToNonMember){
            isMember = false;            
        }
        //Flexipath Project - to show Competency Area Request in MyRequest, for Non-Member
    }
    
    //This method seem like not using
    /*public PageReference viewCase() {
        return new PageReference(Url.getSalesforceBaseUrl().toExternalForm() + '/customer/s/case/' + selectedCaseForView);
    }*/
    
    
    public List<Case> getRequests() {
        // Temp solution for go live
        //LCA-394 add in caseNumber
        
        List<Case> reqCase = [SELECT Id, Student_Program__c, Subject, CaseNumber, Type, RecordType.Name, Status, CreatedDate, Total_Public_Comment_Unread__c, Publish__c FROM Case WHERE 
            ContactId =: custCommConId AND 
            (RecordType.Name =: luana_RequestConstants.REQ_DIS_RT_NAME OR 
            RecordType.Name =: luana_RequestConstants.REQ_EXEMPTION_RT_NAME OR
            RecordType.Name =: luana_RequestConstants.REQ_COMPETENCY_RT_NAME OR
            RecordType.Name =: luana_RequestConstants.REQ_OOAE_RT_NAME OR
            RecordType.Name =: luana_RequestConstants.REQ_SC_RT_NAME OR
            RecordType.Name =: luana_RequestConstants.REQ_ASSISTANCE_RT_NAME) ORDER BY CreatedDate DESC LIMIT 500];
        
        for(Case c: reqCase){
            if(c.Total_Public_Comment_Unread__c > 0){
                hasUnreadComment.put(c.Id, true);
            }else{
                hasUnreadComment.put(c.Id, false);
            }
        }
        
        //List<Comment_Read_Status__c> reqComReadStatus = [Select Id, Name, Case__c from Comment_Read_Status__c Where Case__c in: reqCase];
        System.debug('***reqCase::: ' + reqCase);
        return reqCase;
    
    }
    
    
    //Fox for LCA-394: Phase 3
    public PageReference doDeleteCommentReadStatus(){
        List<Comment_Read_Status__c> deleteCrs = new List<Comment_Read_Status__c>();
        for(Comment_Read_Status__c crs: [Select Id, Name, Case__c from Comment_Read_Status__c Where Case__c =: viewCaseId]){
            deleteCrs.add(crs);
        }
        delete deleteCrs;
        
        PageReference viewReqPage = new PageReference(luana_NetworkUtil.getCommunityPath() +'/luana_RequestView?csId=' + viewCaseId);
        viewReqPage.setRedirect(true);
        return viewReqPage;
    }
    
}