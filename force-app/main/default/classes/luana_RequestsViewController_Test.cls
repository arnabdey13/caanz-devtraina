/**
    Developer: WDCi (kh)
    Development Date:28/03/2016
    Task: Luana Test class for luana_RequestsViewController
    
    Change History
    LCA-921 23/08/2019 WDCi - KH: Add person account email
	PAN:5340 22/10/2018 - WDCi LKoh: fix to comply with new validation in the Assessible_for_CA_Program__c field
**/
@isTest(seeAllData=false)
private class  luana_RequestsViewController_Test {
    
    public static Account member;
    public static User memberUser;
    public static Luana_DataPrep_Test dataPrep;
    public static luana_CommUserUtil userUtil {get; set;} 
    
    private static String classNamePrefixLong = 'luana_RequestsViewController_Test';
    private static String classNamePrefixShort = 'lrvc';
    
    public static List<Case> newCases;
    
    //LCA-921
    public static void initial(){
        //Initialize
        dataPrep = new Luana_DataPrep_Test();
        
        //Create all the custom setting
        insert dataPrep.prepLuanaExtensionSettingCustomSettings();
        insert dataPrep.createLuanaConfigurationCustomSetting();
        
        // PAN:5340 - University and Degree asset needed to create Education History
        Map<String, SObject> eduAssets = dataPrep.createEducationAssets();                
        
        member = dataPrep.generateNewApplicantAcc('Jeo',classNamePrefixLong, 'Applicant');
        member.Member_Id__c = '12345';
        member.Affiliated_Branch_Country__c = 'Australia';
        member.Membership_Class__c = 'Full';        
        // PAN:5340 - new validation in place will cause attempt to set Assessibility for CA Program and CA Foundation to fail without prequisite Education History        
        // member.Assessible_for_CA_Program__c = true;
        member.Assessible_for_CA_Program__c = false;
        member.PersonEmail = 'joe_1_' +classNamePrefixShort+'@gmail.com';//LCA-921
        
        insert member;
        
        // PAN:5340
        List<Account> checkAccount = [SELECT Id, PersonContactId FROM Account WHERE Id = :member.Id];        
        edu_Education_History__c edu_asset = dataPrep.createEducationHistory(null, checkAccount[0].PersonContactId, true, eduAssets.get('universityDegreeJoin1').Id);
        insert edu_asset;
        
        // once the valid Education History is in place, only then can we set the Accessible for CA Program to true
        member.Assessible_for_CA_Program__c = true;
        member.Membership_Class__c = 'Provisional';
        update member;
        // end of PAN:5340        
    }
    
    public static void setup(){
        //LCA-921
        memberUser = [Select Id, Email, FirstName, LastName, UserName, Name, ContactId from User Where Email =: 'joe_1_' + classNamePrefixShort+'@gmail.com' limit 1];
        //memberUser = testDataGenerator.generateNewApplicantUser(classNamePrefixLong, classNamePrefixshort, memberAccount, commProfIdMap.get('Customer Community Login User'));
        //insert memberUser;
        
        userUtil = new luana_CommUserUtil(memberUser.Id);
        
        newCases = new List<Case>();
        newCases.add(dataPrep.createCase('CA Program', 'New', dataPrep.getRecordTypeIdMap('Case').get('Module_Cancellation_Discontinue'), 'Exam', 'CA Program Exam', 'Phone'));
        newCases.add(dataPrep.createCase('CA Program', 'New', dataPrep.getRecordTypeIdMap('Case').get('Module_Exemption'), 'Exam', 'CA Program Exam', 'Phone'));
        newCases.add(dataPrep.createCase('CA Program', 'New', dataPrep.getRecordTypeIdMap('Case').get('Out_of_Approved_Employment'), 'Exam', 'CA Program Exam', 'Phone'));
        newCases.add(dataPrep.createCase('CA Program', 'New', dataPrep.getRecordTypeIdMap('Case').get('Request_Special_Consideration'), 'Exam', 'CA Program Exam', 'Phone'));
        newCases.add(dataPrep.createCase('CA Program', 'New', dataPrep.getRecordTypeIdMap('Case').get('Request_for_Assistance'), 'Exam', 'CA Program Exam', 'Phone'));
        insert newCases;
    
        List<Attachment> newAttch = new List<Attachment>();
        for(Case c: newCases){
            newAttch.add(dataPrep.addAttachmentToParent(c.Id, classNamePrefixLong + '-Attachment'));
        }
        insert newAttch;
    }
    
    public static testMethod void testController() { 
        
        Test.startTest();
            initial();
        Test.stopTest();
        setup();
            
        for(Case c: newCases){
            PageReference pageRef = Page.luana_RequestView;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('csId', c.Id);
            
            luana_RequestsViewController  rvc = new luana_RequestsViewController ();
            rvc.fieldSetMembers = SObjectType.Case.FieldSets.DW.getFields();
            
            rvc.getCase();
            rvc.getAttachment();
            
            rvc.back();
        }
            
        System.runAs(memberUser){
            for(Case c: newCases){
                PageReference pageRef = Page.luana_RequestView;
                Test.setCurrentPage(pageRef);
                ApexPages.currentPage().getParameters().put('csId', c.Id);
                
                luana_RequestsViewController  rvc = new luana_RequestsViewController ();
                rvc.fieldSetMembers = SObjectType.Case.FieldSets.DW.getFields();
                
                rvc.getCase();
                rvc.getAttachment();
                
                rvc.getRelatedComment();
                rvc.insertNewCaseComment();
                
                rvc.back();
            }
        }
    }
}