public class RecognitionMatching {

public static Set<String> MATCH_KEYS = new Set<String> {'ADD_KEY', 'BUS_KEY','BUS_ADD_KEY'};

//private static Set<String> FUZZY_MATCH_KEYS = new Set<String> {'BusSearch_Key__c', 'BusNameSearch_Key__c', 'AddressSearch_Key__c'};
private static Set<String> FUZZY_MATCH_KEYS = new Set<String> {'BusNameFuzzy__c', 'BillingCity__c', 'ShippingCity__c', 'AddressSearch_Key__c', 'BillingPostalCode__c', 'ShippingPostalCode__c'};
private static Set<String> EXACT_MATCH_KEYS = new Set<String> {'BusSearch_Key__c', 'BillingDPID__c', 'ShippingDPID__c', 'Phone__c', 'BillingStreet__c', 'ShippingStreet__c', 'AddressSearch_Key__c'};
      
public static Map<Id, Map<String, String>> results = new Map<Id, Map<String, String>>();
public static Map<Integer, Reference_Recognition_CAANZ_Repository__c> matchResults;
     
public static RecognitionJaroWinkler jaroWinkler = new RecognitionJaroWinkler();
    
public static Boolean debugFine = true;
public static Boolean debug = true;
    
    /**
     * Main Recognition Function - returns a set of Match Keys
     * 
     * creates a new entry to the repository
     * generates a set fields for exact and fuzzy matching
     *  - removes noise from Business Names (Legal Noise, Locality Noise, Business Registration noise and other common noise)
     *  - generates soundex Keys
     *  - generates stemming keys
     * generates a set of Search keys that will be used for searching prospects
     * queries repository and returns a set of prospects - candidates for matching
     * performs grouping and calculates Matching score
     * qualifies records as matches 
     * returns best match key (if match found) or new key if match not found
     * 
     **/
    
    public static Reference_Recognition_CAANZ_Repository__c getMatchKeys(Account bus) {
        matchResults = new Map<Integer, Reference_Recognition_CAANZ_Repository__c>();

        // create a clean version of the new record for Repository including searching keys    
        Reference_Recognition_CAANZ_Repository__c newEntry = createNewEntry(bus);
        // check if existing entries if already exist
        newEntry = getExistingAccount(newEntry, bus.Id);
        if (newEntry.MATCH_KEY__c != null)
			return newEntry;            
           
        // get search keys only
		Map<String, String> searchKeys = getSearchKeys(newEntry);
        // run a search and return list of all matching prospects
        List<Reference_Recognition_CAANZ_Repository__c> matchProspects = getMatchProspects(searchKeys);

		// attempt grouping between new entry and prospects        
        runGrouping(newEntry, matchProspects);
        if (debug) System.debug(matchResults);
        
        // append key from the matched record or a new key
		newEntry = appendMatchedKeys(newEntry);        
       return newEntry;
    }
        
	public static Reference_Recognition_CAANZ_Repository__c createNewEntry(Account acct) {
		Reference_Recognition_CAANZ_Repository__c rep = new Reference_Recognition_CAANZ_Repository__c();
        
        rep.Company_Name__c = acct.Name;
        rep.Account__c = acct.Id;
        rep.BillingCountry__c = acct.BillingCountry;
        rep.BillingStreet__c = acct.BillingStreet;
        rep.BillingCity__c = RecognitionBusinessUtil.stdCity(acct.BillingCity);
        rep.BillingPostalCode__c = acct.BillingPostalCode;
        rep.BillingDPID__c = acct.Billing_DPID__c;
        rep.ShippingCountry__c = acct.ShippingCountry;
        rep.ShippingCity__c = RecognitionBusinessUtil.stdCity(acct.ShippingCity);
        rep.ShippingPostalCode__c = acct.ShippingPostalCode;
        rep.ShippingStreet__c = acct.ShippingStreet;
        rep.ShippingDPID__c = acct.Shipping_DPID__c;
        rep.Phone__c = acct.Phone;
        rep.AddressSearch_Key__c = RecognitionBusinessUtil.getStreet(acct.BillingStreet);  
        
        rep.BusSearch_Key__c = RecognitionBusinessUtil.genBusSearchKey(acct.Name);
        String fuzzyBusName = RecognitionBusinessUtil.genBusNameStem(acct.Name);
        if (fuzzyBusName.length() > 20)
        	rep.BusNameFuzzy__c = fuzzyBusName.substring(0,19);
        else
        	rep.BusNameFuzzy__c = fuzzyBusName;
        
        //rep.AddressSearch_Key__c = RecognitionBusinessUtil.genAddressSearchKey(acct.Name);
        //rep.AddressSearch_Key__c = acct.BillingCity;
            
        return rep;
    }
    
	/**
	 * retrieve keys from matched record
	 * if no match take the next available key
	 * append key to the new entry
	 **/     
    public static Reference_Recognition_CAANZ_Repository__c appendMatchedKeys(Reference_Recognition_CAANZ_Repository__c newEntry) {
		List<Integer> lscores = new List<Integer>(matchResults.keySet());
        if (debug) System.debug('appendMatchedKeys: Matched Scores:' +  lscores );
        if (lscores.size() > 0){
        	lscores.sort();
        	Integer maxScore = lscores[lscores.size()-1];
        	//String bus_key =  (String) matchResults.get(maxScore).get('BUS_KEY__c');
        	Decimal match_key = (Decimal)  matchResults.get(maxScore).get('MATCH_KEY__c');
            if (debug) System.debug('appendMatchedKeys: MatchedKey:' +  match_key );
            if ( match_key == NULL || match_key == 0 )
				match_key = getNextKey('MATCH_KEY');            
			newEntry.MATCH_KEY__c  = match_key;
			newEntry.Match_Score__c  = maxScore;
            if (debug) System.debug('appendMatchedKeys: Matched Prospect: Id:' +  (String) matchResults.get(maxScore).get('Company_Name__c') + ' Score' + maxScore + ' MATCH_KEY ' + match_key );
        }
		else {
            if (debugFine) System.debug('appendMatchedKeys: No Match'); {
				newEntry.MATCH_KEY__c  = getNextKey('MATCH_KEY');
				newEntry.Match_Score__c  = 0;
            }
        }    
        return newEntry;
    }
    
	public static Map<String, String> getSearchKeys(Reference_Recognition_CAANZ_Repository__c rep) {    
		Map<String, String> searchKeys = new Map <String, String>();
        searchKeys.put('BusSearch_Key__c', rep.BusSearch_Key__c);
		searchKeys.put('Phone__c', rep.Phone__c);
		//searchKeys.put('BusNameSearch_Key__c', rep.BusNameSearch_Key__c);
		searchKeys.put('BillingDPID__c', rep.BillingDPID__c);
		searchKeys.put('ShippingDPID__c', rep.ShippingDPID__c);
            
        return searchKeys;
    }    
    
      
    /**
     * Query the repository for Account - we do not another entry if already exists
     * TODO we need to confirm that the existing BUS_KEY still makes sense if change is dramatic - e.g. change of the city
     *      Add logic to compare old and new city etc.
     **/        
	public static Reference_Recognition_CAANZ_Repository__c getExistingAccount(Reference_Recognition_CAANZ_Repository__c newEntry, Id AccountId) {
        List<Reference_Recognition_CAANZ_Repository__c> refList = [ Select ID, MATCH_KEY__c From Reference_Recognition_CAANZ_Repository__c where Account__c = : AccountId];
        if (refList.size() > 0) {
            newEntry.MATCH_KEY__c = (Decimal)refList[0].get('MATCH_KEY__c');
            newEntry.Id = (Id)refList[0].get('Id');
            return newEntry; 
        }
        else
            return newEntry;
   }
    
    /**
     * Query the repository for all combinations of the search keys
     * Returns a list of prospects for matching
     **/    
	public static List<Reference_Recognition_CAANZ_Repository__c> getMatchProspects(Map<String, String> mapSearchKeys) {
        String strQuery;
        String baseQry;
        // List of all search results
        List<Reference_Recognition_CAANZ_Repository__c> searchProspects = new List<Reference_Recognition_CAANZ_Repository__c>();      
        baseQry =  ' Select ID, MATCH_KEY__c, BUS_KEY__c, Account__c, Company_Name__c, BillingStreet__c, BillingCity__c, BillingPostalCode__c, BillingDPID__c, BusSearch_Key__c, BusNameFuzzy__c, ';
		baseQry += ' AddressFuzzy__c, ShippingCountry__c, ShippingStreet__c, ShippingCity__c, ShippingDPID__c, ShippingPostalCode__c, Phone__c, AddressSearch_Key__c From Reference_Recognition_CAANZ_Repository__c ';
		
        // generate and execute a separate search query for each search key
        Set<String> searchKeys = mapSearchKeys.keySet();
        for (String searchKey : searchKeys) {           
			strQuery = baseQry + ' WHERE ' + searchKey + ' = \'' + mapSearchKeys.get(searchKey) + '\'';    
            List<sObject> sobjList = Database.query(strQuery);
            // add individual search results to all results bucket
            for(SObject sobj : sobjList){
                Reference_Recognition_CAANZ_Repository__c ref = (Reference_Recognition_CAANZ_Repository__c)sobj;
                searchProspects.add(ref);                   
           } 
        }
        return searchProspects;
   }
    
	public static void runGrouping(Reference_Recognition_CAANZ_Repository__c newEntry,
        					List<Reference_Recognition_CAANZ_Repository__c> prospects){
                   
        String newEntryExactKeys;
        String newEntryFuzzyKeys;  
        String newEntryDebugKeys = (String)newEntry.Id;
        String prospectDebugKeys;
          
		Map<String, String> newEntryExact = new Map<String, String>();                        
		for (String eMatchKey : EXACT_MATCH_KEYS) {
            newEntryExact.put( eMatchKey, (String)newEntry.get(eMatchKey));
        }
        if (debugFine) System.debug('New Entry Key List: ' + newEntry.get('Company_Name__c') + ' ' + newEntryExact);

		Map<String, String> newEntryFuzzy = new Map<String, String>();                                                   
		for (String fMatchKey : FUZZY_MATCH_KEYS) {
            newEntryFuzzy.put( fMatchKey, (String)newEntry.get(fMatchKey));
        }
        if (debugFine) System.debug('New Entry Fuzzy Key List: ' + newEntryFuzzy);
                                
        // try to get a match score for each prospect                        
		for (Integer idx=0; idx<prospects.size(); idx++) {
			if (debugFine) System.debug('Prospect:' + prospects[idx].get('Id'));
            Map<String, String> prospectMatchExact = new Map<String, String>();
            for (String eMatchKey : EXACT_MATCH_KEYS) {
				prospectMatchExact.put(eMatchKey, (String)prospects[idx].get(eMatchKey));
            }
			if (debugFine) System.debug('New Entry Key List: ' + newEntry.get('Company_Name__c') + ' ' +  newEntryExact);
			if (debugFine) System.debug('Prospect Exat Match List: ' + (String)prospects[idx].get('Company_Name__c') + ' ' +  prospectMatchExact);
 
            Map<String, String> prospectMatchFuzzy = new Map<String, String>();                        
			for (String fMatchKey : FUZZY_MATCH_KEYS) {
				prospectDebugKeys += ':' + fMatchKey + '=' + prospects[idx].get(fMatchKey) + ':';
                // Temporary
                if (fMatchKey == 'BusNameFuzzy__c')
					prospectMatchFuzzy.put(fMatchKey, RecognitionBusinessUtil.genBusNameStem((String) prospects[idx].get('Company_Name__c')) );
                else
					prospectMatchFuzzy.put(fMatchKey, (String)prospects[idx].get(fMatchKey));          
        	}
			if (debugFine) System.debug('New Entry Fuzzy Key List: '  + newEntry.get('Company_Name__c')  + ' ' +  newEntryFuzzy);
			if (debugFine) System.debug('Prospect Fuzzy Match: ' + (String)prospects[idx].get('Company_Name__c') + ' ' + prospectMatchFuzzy);
            
            Integer matchScore = calcMatch(newEntryExact, prospectMatchExact, newEntryFuzzy, prospectMatchFuzzy);            
            if (matchScore>0) {
				if (!matchResults.containsKey(matchScore))
            		matchResults.put( matchScore, prospects[idx]);
             }
    	}                                 
	}

    
    // {'BusSearch_Key__c', 'BillingDPID__c', 'ShippingDPID__c', 'Phone__c'};
    // {'BusNameFuzzy__c', 'BillingCity__c', 'ShippingCity__c'};
	private static Integer calcMatch(Map<String, String> newEntryExact, Map<String, String> prospectMatchExact, 
                                  	 Map<String, String> newEntryFuzzy, Map<String, String> prospectMatchFuzzy ) {
                                      
        Boolean DPID_Match = false;
        Boolean Phone_Match = false;
        Boolean BusKey_Match = false;
		Boolean Address_Match = false;
		Boolean Street_Match = false;
        Decimal CityMatch = 0;
        Decimal BusName_Match = 0;
                                      
		for (String eMatchKey : EXACT_MATCH_KEYS) { 
            if (eMatchKey == 'BillingDPID__c')
        		if ( isEqualString(newEntryExact.get('BillingDPID__c'), prospectMatchExact.get('BillingDPID__c')) || 
                     isEqualString(newEntryExact.get('BillingDPID__c'), prospectMatchExact.get('ShippingDPID__c')))
                	DPID_Match = true;
            if (eMatchKey == 'ShippingDPID__c')
        		if (isEqualString(newEntryExact.get('ShippingDPID__c'), prospectMatchExact.get('ShippingDPID__c')) ||
                   isEqualString( newEntryExact.get('ShippingDPID__c'), prospectMatchExact.get('BillingDPID__c')))
                	DPID_Match = true;
            
            if (eMatchKey == 'BusSearch_Key__c')
        		if (isEqualString(newEntryExact.get(eMatchKey), prospectMatchExact.get(eMatchKey)) )
                	BusKey_Match = true;
            
            if (eMatchKey == 'Phone__c')
        		if (isEqualString(newEntryExact.get(eMatchKey), prospectMatchExact.get(eMatchKey)) )
                	Phone_Match = true;  
            
			if (eMatchKey == 'BillingStreet__c')
        		if (isEqualString(newEntryExact.get(eMatchKey), prospectMatchExact.get(eMatchKey)) || 
                    (newEntryExact.get(eMatchKey).contains(prospectMatchExact.get(eMatchKey)) && prospectMatchExact.get(eMatchKey).length() > 7 && !prospectMatchExact.get(eMatchKey).contains(' BOX ')) ||
                    (prospectMatchExact.get(eMatchKey).contains(newEntryExact.get(eMatchKey)) && newEntryExact.get(eMatchKey).length() > 7 && !prospectMatchExact.get(eMatchKey).contains(' BOX '))
                   )
                	Address_Match = true;  
 
			if (eMatchKey == 'ShippingStreet__c')
        		if (isEqualString(newEntryExact.get(eMatchKey), prospectMatchExact.get(eMatchKey)) || 
                    (newEntryExact.get(eMatchKey).contains(prospectMatchExact.get(eMatchKey)) && prospectMatchExact.get(eMatchKey).length() > 7 && !prospectMatchExact.get(eMatchKey).contains(' BOX ')) ||
                    (prospectMatchExact.get(eMatchKey).contains(newEntryExact.get(eMatchKey)) && newEntryExact.get(eMatchKey).length() > 7 && !prospectMatchExact.get(eMatchKey).contains(' BOX '))
                   )
                	Address_Match = true;  
            
            if (eMatchKey == 'AddressSearch_Key__c')
        		if (isEqualString(newEntryExact.get(eMatchKey), prospectMatchExact.get(eMatchKey)) )
                	Street_Match = true;
                 
        }
		// for fuzzy matching use Jaro Winkler to calculate similarity   
		for (String fMatchKey : FUZZY_MATCH_KEYS) { 
            if (fMatchKey == 'BusNameFuzzy__c')
				BusName_Match = RecognitionMatching.jaroWinkler.similarity(newEntryFuzzy.get(fMatchKey), prospectMatchFuzzy.get(fMatchKey));               
        
			if (fMatchKey == 'BillingCity__c') {
                Decimal newCityMatch = 0.0;
                newCityMatch =  RecognitionMatching.jaroWinkler.similarity(newEntryFuzzy.get('BillingCity__c'), prospectMatchFuzzy.get('BillingCity__c'));   
                if ( newCityMatch > CityMatch)
                    CityMatch = newCityMatch;
                newCityMatch =  RecognitionMatching.jaroWinkler.similarity(newEntryFuzzy.get('BillingCity__c'), prospectMatchFuzzy.get('ShippingCity__c'));
                if ( newCityMatch > CityMatch)
                    CityMatch = newCityMatch; 
           }
            
			if (fMatchKey == 'ShippingCity__c') {
                Decimal newCityMatch = 0.0;
                newCityMatch =  RecognitionMatching.jaroWinkler.similarity(newEntryFuzzy.get('ShippingCity__c'), prospectMatchFuzzy.get('ShippingCity__c'));   
                if ( newCityMatch > CityMatch)
                    CityMatch = newCityMatch;
                newCityMatch =  RecognitionMatching.jaroWinkler.similarity(newEntryFuzzy.get('ShippingCity__c'), prospectMatchFuzzy.get('BillingCity__c'));
                if ( newCityMatch > CityMatch)
                    CityMatch = newCityMatch;      
           }
 
        }
		// safety when shipping city much e.g. Care of different from billing city
		System.debug( 'Equal City:' + isEqualString(newEntryFuzzy.get('BillingCity__c'), newEntryFuzzy.get('ShippingCity__c')) );
		System.debug( 'Equal Postcode:' + isEqualString (newEntryFuzzy.get('BillingPostalCode__c'), newEntryFuzzy.get('ShippingPostalCode__c'), 2) );                                         
                                         
        if (  ( !isEqualString(newEntryFuzzy.get('BillingCity__c'), newEntryFuzzy.get('ShippingCity__c')) && 
                !isEqualString (newEntryFuzzy.get('BillingPostalCode__c'), newEntryFuzzy.get('ShippingPostalCode__c'), 2) ) 
            || 
              (!isEqualString(prospectMatchFuzzy.get('BillingCity__c'), prospectMatchFuzzy.get('ShippingCity__c')) && 
               !isEqualString( prospectMatchFuzzy.get('BillingPostalCode__c'), prospectMatchFuzzy.get('ShippingPostalCode__c'), 2) )
           ){
			CityMatch = 0;
            Address_Match = false;
			DPID_Match = false;
           }

        return getMatchScore( DPID_Match, Phone_Match, BusKey_Match, Address_Match, Street_Match, CityMatch, BusName_Match ); 
	}
      
	private static Integer getMatchScore( Boolean DPID_Match, Boolean Phone_Match, Boolean BusKey_Match, Boolean Address_Match, Boolean Street_Match, Decimal CityMatch, Decimal BusName_Match ) {
    	Integer score;        
     
         if (    DPID_Match  && Phone_Match && BusName_Match>=0.9)
			score = 100;            
        else if (DPID_Match && Phone_Match && BusKey_Match) 
			score = 95;
        else if (Address_Match && Phone_Match && BusKey_Match) 
			score = 90;
        else if (DPID_Match && Phone_Match && BusName_Match>=0.7) 
            score = 85;
        else if (Address_Match && Phone_Match && BusName_Match>=0.7) 
            score = 85;
        else if (DPID_Match && BusName_Match>=0.8) 
            score = 80;
        else if (Phone_Match && BusName_Match >=0.8 && CityMatch > 0.8)
            score = 75;
        else if (BusName_Match >=0.9 && !Phone_Match && !DPID_Match && !Street_Match && CityMatch == 1) 
            score = 70;
        else if (BusKey_Match && CityMatch == 1 && Street_Match) 
            score = 65;           
        //else if (BusName_Match >=0.8 && CityMatch > 0.8) 
        //    score = 60; 
		else
            score = 0;
        
        
         // Rules for matching
        if ( score > 0 ) {
			if (debug) System.debug('Match!!! : Score: ' + score + ' BusKey_Match: ' + BusKey_Match + ' DPIDMatch: ' + DPID_Match + ' BusNameMatch: ' + BusName_Match + ' CityMatch: ' + CityMatch + ' Phone:' + Phone_Match + ' Address:' + Address_Match);
        } else {
			if (debug) System.debug('No Match: BusKey_Match: ' + BusKey_Match + ' DPIDMatch: ' + DPID_Match + ' BusNameMatch: ' + BusName_Match + ' CityMatch: ' + CityMatch + ' Phone:' + Phone_Match + ' Address:' + Address_Match);
        }
        return score;
    }

     // helper function
     public static Boolean isEqualString(String left, String right) {
        if (left == null || right == null )
            return false;
         
        if (left.trim().length() < 2)
            return false;
        if (left.equals(right))
            return true; 
        return false;
     }

    
	public static Boolean isEqualString(String left, String right, Integer minLimit) {
        if (left == null || right == null )
            return false;
        if (minLimit < 1) 
            return false;
        if (left.trim().length() < minLimit || right.trim().length() < minLimit)
            return false;
        if (left.substring(0, minLimit).equals(right.substring(0, minLimit)))
            return true; 
        return false;
     }

    public static Decimal getNextKey(String typeKey) {
       
        //AggregateResult[] MaxResults = [Select MAX(BUS_KEY__c) max_bus, MAX(ADD_KEY__c) max_add, MAX(MATCH_KEY__c) max_key FROM Reference_Recognition_CAANZ_Repository__c];
 		AggregateResult[] MaxResults = [Select MAX(MATCH_KEY__c) max_key FROM Reference_Recognition_CAANZ_Repository__c];
        // Decimal maxBus = (Decimal) MaxResults[0].get('max_bus');
		// Decimal maxAdd = (Decimal) MaxResults[0].get('max_add');
		Decimal maxKey = (Decimal) MaxResults[0].get('max_key');
        
        if (typeKey=='MATCH_KEY') {
            if (maxKey==NULL)
                maxKey = 100000;
            return maxKey+1;            
        }
/**
        else if (typeKey=='BUS_KEY') {
            if (maxBus==NULL)
                maxBus = 100000;
            return maxBus+1;
        }
        else if (typeKey=='ADD_KEY') {
            if (maxAdd==NULL)
                maxAdd = 100000;
            return maxAdd+1;
        }
**/
        else
            return 0;
    }
 
    public static List<Reference_Recognition_CAANZ_Repository__c> getRecordByMatchKey( String Key, Decimal Value )
    {    
        List<Reference_Recognition_CAANZ_Repository__c> results = new List<Reference_Recognition_CAANZ_Repository__c>();
        String strQuery;
        strQuery =  ' Select ID, Account__c, Company_Name__c, BillingStreet__c, BillingCity__c, BillingPostalCode__c, BillingDPID__c, '; 
        strQuery += ' ShippingStreet__c, ShippingCity__c, ShippingPostalCode__c, ShippingDPID__c, Phone__c, ';
        strQuery += ' BusNameFuzzy__c, BusSearch_Key__c, MATCH_KEY__c, Match_Score__c, AddressSearch_Key__c '; 
       	// strQuery += ' FROM Reference_Recognition_CAANZ_Repository__c WHERE ' + Key + ' = \'' + Value + '\''; 
       	strQuery += ' FROM Reference_Recognition_CAANZ_Repository__c WHERE ' + Key + ' = ' + Value ; 
        
		if (debug) System.debug('RecognitionMatching: getRecordByMatchKey: strQuery ' + strQuery);       
        try {	
            List<sObject> sobjList = Database.query(strQuery);           
            for(SObject sobj : sobjList){
                Reference_Recognition_CAANZ_Repository__c ref = (Reference_Recognition_CAANZ_Repository__c)sobj;
                results.add(ref);
            } 
            return results;
        } 
		catch (exception e){
			return new List<Reference_Recognition_CAANZ_Repository__c>();
		}
	}
    
    
/**
* Repository Maintenance Utils
**/ 
    
    // '  Where isPersonAccount = false And Type like \'%Account%\' And BillingCountry = \'Australia\' and Status__c = \'Active\' And Name like \'%Deloitte%\' And BillingCity like \'%CANBERRA%\' LIMIT 10';
	public static void addToRepository( String whereQuery ){
     	String  queryBase =  'Select ID, Name, BillingCountry, BillingStreet, BillingCity, BillingPostalCode, Billing_DPID__c, Type, ';
        queryBase += ' ShippingCountry, ShippingStreet, ShippingCity,  ShippingPostalCode, Shipping_DPID__c, Phone ';
        queryBase += ' From Account ';      
        List<sObject> sobjList = Database.query(queryBase + whereQuery);         
        processSmallBatchWriteMode( (List<Account>) sobjList);   
    }
 
	public static void clearRepository( ){
        List <Reference_Recognition_CAANZ_Repository__c> toDelete = [Select Id From Reference_Recognition_CAANZ_Repository__c]  ;    
		delete toDelete;        
    }
    
    public static void rmFromRepositoryById( List<Id> practicesToDelete ){
		List <Reference_Recognition_CAANZ_Repository__c> toDelete = [Select Id From Reference_Recognition_CAANZ_Repository__c where ID in :practicesToDelete]  ;
    }    

    // String where = ' Where BillingCountry__c = \'Australia\' and Company_Name__c like \'%Deloitte%\' And BillingCity__c like \'%ADELAIDE%\'';
	public static void rmFromRepository( String whereQuery ){
        List <Reference_Recognition_CAANZ_Repository__c> toDelete = new List <Reference_Recognition_CAANZ_Repository__c>();
        String queryBase = ' Select Id From Reference_Recognition_CAANZ_Repository__c ';
        List<sObject> sobjList = Database.query(queryBase + whereQuery);           
            for(SObject sobj : sobjList){
                Reference_Recognition_CAANZ_Repository__c ref = (Reference_Recognition_CAANZ_Repository__c)sobj;
                toDelete.add(ref);
            } 
		delete toDelete;           
    }
    
     public static void processSmallBatchWriteMode(List<Account> busList) { 
		if ( busList.size()> 200 )
            return;
        for (Account bus : busList) {
			Reference_Recognition_CAANZ_Repository__c result = getMatchKeys(bus);
            try {
                upsert result;
            } catch (DmlException e) {
                // Process exception
            }
        }
    }
    
}