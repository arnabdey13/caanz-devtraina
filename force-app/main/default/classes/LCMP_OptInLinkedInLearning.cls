/*------------------------------------------------------------------------------------
    Author:        Sumit Pandey
    Company:       Tech Mahindra
    Description:   This Controller is used for Lightning Component 'LCMP_OptInLinkedInLearning' which is
                    placed on MyCA community page.
                    When the Component is initialized , method getLoadData() will be invoked and fetch current user profile and 
                    Custom Metadata and Based on the User Profile LinkedIn Learning will be displayed.


    History
    Date            Author             Comments
--------------------------------------------------------------------------------------
    04-01-2019      Sumit Pandey       Initial Release
------------------------------------------------------------------------------------*/

public with sharing class LCMP_OptInLinkedInLearning {
    
    @AuraEnabled
    public static dataWrapper getInitialData(){
        //Profile pr = [Select Id, Name from Profile where Id =: UserInfo.getProfileId()];
        Opt_in_for_LinkedIn_Learning__mdt optMet = new Opt_in_for_LinkedIn_Learning__mdt();
        
        if(!Test.isRunningTest()){
        optMet = [select id,MasterLabel, DeveloperName,URLPrefix__c, 
                                                        Disclaimar_Year__c,IDPLink__c,FAQLink__c
                                                        from Opt_in_for_LinkedIn_Learning__mdt where 
                                                        DeveloperName = 'DisclaimarYearCollectionDoc' limit 1];
            
        
        }
        List<Document> lstDocument = [Select Id, Name From Document where Name = 'LIL'];
        
        dataWrapper dt = new dataWrapper();
        //dt.profileName = pr.Name;
        dt.optInCusMet = optMet;
        dt.fileId = lstDocument.get(0).Id;
        dt.accountData = CASUB_CaanzAccountDetailsController_CA.getAccountData();
        return dt;
    }
    
    /*@AuraEnabled
    public static String getAccountData(){
        String accountData = CASUB_CaanzAccountDetailsController_CA.getAccountData();
        return accountData;
    }*/
    
    @AuraEnabled
    public static string updateAccount(string recordId, string emailToUpdate){
        system.debug('==Current User Id=='+userinfo.getUserId());
        Account acc = [Select Id,PersonEmail,Opt_in_for_LinkedIn_Learning__c 
                       from Account where Id =: recordId];
        try{
            acc.Opt_in_for_LinkedIn_Learning__c = true;
            if(acc.PersonEmail != emailToUpdate){
                acc.PersonEmail = emailToUpdate;
            }
            update acc;
            return 'SUCCESS';
        }
        catch(exception ex){
            throw new AuraHandledException(ex.getMessage());
        }
    }
    
    public class dataWrapper{
        //@AuraEnabled public  string profileName;
        @AuraEnabled public  Opt_in_for_LinkedIn_Learning__mdt optInCusMet;
        @AuraEnabled public  String fileId;
        @AuraEnabled public  string accountData;
    }
    
}