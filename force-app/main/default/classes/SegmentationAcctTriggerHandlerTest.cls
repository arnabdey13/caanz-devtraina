/**
*	Project:			CAANZ Internal
*	Project Name:		Segmentation
*	Project Task Name:	Unit Test of the Account trigger Handler After Update
*	Developer:			akopec
*	Initial Date:		May 2017
*	
**/
@isTest(seeAllData=false)
public class SegmentationAcctTriggerHandlerTest {
	  
    
    private static Map<Id, Account> oldMap;
	private static Map<Id, Account> newMap;
    private static List <Account> newList;
    private static List <Account> oldList;
    private static Boolean initiated = false;    
    private static Boolean debugFlag = true;    
    
    private static List<Reference_Geography_Australia__c> geo_references;
	private	static List<Segmentation__c> segments;
        
    
    private static void initAccountMap () {
        // prepare Person Accounts for testing        
        List <Account> newAccounts = new List <Account>();
        // create a list of 2 Accounts for John Doe    	
        newAccounts = createPersonAccounts ( 'John', 'Doe', 2 );
        insert newAccounts;  
        List<ID> idList = new List<ID>(new Map<Id, Account>(newAccounts).keySet());
        List <Account> accounts = [SELECT Id, Name, isPersonAccount, PersonOtherPostalCode, PersonMailingPostalCode FROM Account WHERE ID = :idList ]; 
        
        // prepare old and new lists and maps
        oldMap = new Map<ID, Account>{} ;
        for (Account acct : accounts)
            oldMap.put(acct.ID, acct);      	
        newMap = oldMap.deepClone();
	}
        
    
    private static void init () {
        if (initiated)
            return;
        
        // initiate Account Objects and corresponding old and new Maps
        initAccountMap ();
     
        // initiate Geo Reference Objects
        List <String> refs_AU = new List <String>();
        refs_AU.add('12102141111');
        refs_AU.add('12102142222');
       	geo_references = createGeoReferences (refs_AU);
        
        // initiate Segmentation Objects 2 accounts and 2 geographies
		segments = createSegmentations ();
        
        initiated = true; 
    }
    
 
    private static void debugMaps () 
    {
			for ( ID oldIdKey : oldMap.keyset()) {
  	          System.debug('oldMap Values ' + oldMap.get(oldIdKey).PersonOtherPostalCode);
              System.debug('oldMap Person Account :' + oldMap.get(oldIdKey).isPersonAccount);  
          	}
        
            for ( ID newIdKey : newMap.keyset()) {
  	          System.debug('newMap Values ' + newMap.get(newIdKey).PersonOtherPostalCode);
              System.debug('New List is Person Account :' + newMap.get(newIdKey).isPersonAccount);
          	}  
        
        	System.debug('Old Map: ' + oldMap);
            System.debug('New Map: ' + newMap);
            System.debug('Old List:' + oldList);
            System.debug('New List:' + newList);
    }
    
       
    /**
     * Prepare a generic List of Accounts
     * baseLastname  - generic Lastname
     * baseFirstname - generic Firstname
     * count - number of iterations
     *
     * Salutation, Home Phone and email are necessary because of the validation rules
    **/
    private static List<Account> createPersonAccounts (	String baseFirstname, String baseLastname, Integer count) {
                                                      
     	List <Account> accounts = new List<Account>();
		RecordType personAccountRecordType =  [SELECT Id, IsPersonType FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account' and IsPersonType = true];                                               
        Long basePhone = 61298760000L;
                                                                                                         
        for (Integer x=0 ; x< count; x++)
        { 
            Account acct = (Account)Account.sObjectType.newSObject(null, true);
            acct.RecordTypeId = personAccountRecordType.Id;
            acct.Salutation = 'Mr';
        	acct.FirstName = baseFirstname + String.valueOf(x);
        	acct.LastName  = baseLastname +  String.valueOf(x);
			acct.PersonEmail = baseFirstname + baseLastname + String.valueOf(x) + '@gmail.com.dev';
            acct.PersonHomePhone = String.valueOf( basePhone + x );
            acct.PersonOtherCountry = 'Australia';
        	acct.PersonOtherPostalCode = '2000';
        	acct.PersonMailingPostalCode = '2000';
        	acct.Status__c = 'Active';

            accounts.add(acct);
        }
        return accounts;
     }
    
    
     private static List<Reference_Geography_Australia__c> createGeoReferences (List<String> ref_names) {
                                                      
     	List <Reference_Geography_Australia__c> geo_refs_AU = new List <Reference_Geography_Australia__c>();
        Reference_Geography_Australia__c ref_AU;
        
        for (String ref : ref_names) {
            ref_AU = new Reference_Geography_Australia__c(Name=ref);
            geo_refs_AU.add(ref_AU);
        }         
        insert geo_refs_AU;
        List<ID> idList = new List<ID>(new Map<Id, Reference_Geography_Australia__c>(geo_refs_AU).keySet());
        List <Reference_Geography_Australia__c> refs = [SELECT Id, Name FROM Reference_Geography_Australia__c WHERE ID = :idList ];  
         
        return refs;
     }

    
     // create List of Segmentation objects, take a different geo ref for different account person
     // 2 persons, 2 references
     private static List<Segmentation__c> createSegmentations () {
                                                      
     	List <Segmentation__c> segmentsToAdd = new List <Segmentation__c>();
        Segmentation__c seg;
        Integer counter = 0;
   
        for (ID acctID : oldMap.keySet() ) {
            seg = new Segmentation__c(Name=geo_references[counter].ID);
        	seg.Geo_AU_SA1__c = geo_references[counter].ID;
        	seg.Account__c = acctID;
        	seg.Geo_Confidence_Flag__c = 'High';
            counter++;
            segmentsToAdd.add(seg);
        }
		insert segmentsToAdd;
        List<ID> idList = new List<ID>(new Map<Id, Segmentation__c>(segmentsToAdd).keySet());
        List <Segmentation__c> segs = [SELECT Id, Name, Geo_AU_SA1__c, Account__c, Geo_Confidence_Flag__c FROM Segmentation__c WHERE ID = :idList ];  
         
        return segs;
      }
        
    
	private static void testScenario(String newPostalCode, String oldPostalCode, String newMailingCode, String oldMailingCode) 
    {
    		for ( ID newIdKey : newMap.keyset()) {
  	          newMap.get(newIdKey).PersonOtherPostalCode = newPostalCode;
          	}
        	for ( ID oldIdKey : oldMap.keyset()) {
  	          oldMap.get(oldIdKey).PersonOtherPostalCode = oldPostalCode;
          	}

        	for ( ID newIdKey : newMap.keyset()) {
  	          newMap.get(newIdKey).PersonMailingPostalCode = newMailingCode;
          	}       
        	for ( ID oldIdKey : oldMap.keyset()) {
  	          oldMap.get(oldIdKey).PersonMailingPostalCode = oldMailingCode;
          	}

        	newList = new List<Account>(newMap.values());
          	oldList = new List<Account>(oldMap.values());
        
        	if (debugFlag)
				debugMaps () ;
    }
    
    
    
    // testScenario(String newPostalCode, String oldPostalCode, String newMailingCode, String oldMailingCode)   
    public static testMethod void testStaticAfterUpdateOther1() {
		
        Map<Id, String> results = new Map<Id, String>{};
        init ();
        
        if (debugFlag)
            System.debug('Debugging Other Postal Addresses');
        ////////////////////////////////////// Testing Other Postal Changes /////////////////////////////////////
        // test small changes to Other postal
        // new 2070 old 2072
        
        testScenario('2070', '2072' , '2050', '');
        results = SegmentationAcctTriggerHandler.handleAfterUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('Smaller Other Postal Change Map Results: ' + results);
        System.assert(results.size() > 0);
        for (ID resId : results.keyset() ){
            System.assertEquals(results.get(resId), 'Low-Med'); 
        } 	
    }
        
    public static testMethod void testStaticAfterUpdateOther2() {
		
        Map<Id, String> results = new Map<Id, String>{};
        init ();
        // test bigger changes to Other postal
        //  new '3000' old 2072
        testScenario('3000', '2072' , '2050', '');
        results = SegmentationAcctTriggerHandler.handleAfterUpdate(newList, newMap, oldList, oldMap) ;
        System.assert(results.size() > 0);
        System.debug('Empty Other Postal Change Map Results: ' + results );
        for (ID resId : results.keyset() ){
            System.assertEquals(results.get(resId), 'Low');
        }
    }
        
    public static testMethod void testStaticAfterUpdateOther3() {
		
        Map<Id, String> results = new Map<Id, String>{};
        init ();
        // test bigger changes to Other postal make it null
        //  new '' old 2072
        testScenario('', '2072' , '2050', '');
        results = SegmentationAcctTriggerHandler.handleAfterUpdate(newList, newMap, oldList, oldMap) ;
        System.assert(results.size() > 0);
        System.debug('Empty Other Postal Change Map Results: ' + results );
        for (ID resId : results.keyset() ){
            System.assertEquals(results.get(resId), 'Low');
        }
    }
     
    public static testMethod void testStaticAfterUpdateOther4() {
		
        Map<Id, String> results = new Map<Id, String>{};
        init ();
        // test bigger changes to Other postal the same as above but different order
        //  new '2070' old empty
        testScenario('2070', '' , '2050', '');
        results = SegmentationAcctTriggerHandler.handleAfterUpdate(newList, newMap, oldList, oldMap) ;
        System.assert(results.size() > 0);
        System.debug('Empty Other Postal Change Map Results: ' + results );
        for (ID resId : results.keyset() ){
            System.assertEquals(results.get(resId), 'Low');
        }
        
        // test it again to trigger test on Processed Already
        testScenario('', '2070' , '2050', '');
        results = SegmentationAcctTriggerHandler.handleAfterUpdate(newList, newMap, oldList, oldMap) ;
        System.debug('Trigger the Already Processed ' + results );
        System.assert(results.size() == 0);      
 
    }
    
    public static testMethod void testStaticAfterUpdateOther5() {
		
        Map<Id, String> results = new Map<Id, String>{};
        init ();
        
        // test incorrect postcodes changes to Other postal 
        //  new '20MZ' old 2000
        testScenario('20MZ', '2000' , '2050', '');
        results = SegmentationAcctTriggerHandler.handleAfterUpdate(newList, newMap, oldList, oldMap) ;
        System.assert(results.size() > 0);
        System.debug('Incorrect Other Postal Map Results: ' + results );
        for (ID resId : results.keyset() ){
            System.assertEquals(results.get(resId), 'Low'); 
        }
        
    }
      
    ///////////////////////  Tests for Mailing //////////////////////////
    
	public static testMethod void testStaticAfterUpdateMailing1() {
		
        Map<Id, String> results = new Map<Id, String>{};
        init ();
        
        if (debugFlag)
            System.debug('Debugging Mailing Postal Addresses');
        ////////////////////////////////////// Testing Mailing Postal Changes /////////////////////////////////////
        // The same test on Mailing when Other Address is null
        // test small changes to Mailing postal
        // new 2050 old 2052       
        testScenario('', '' , '2050', '2052');
        results = SegmentationAcctTriggerHandler.handleAfterUpdate(newList, newMap, oldList, oldMap) ;
        System.assert(results.size() > 0);
        System.debug('Smaller Mailing Change Map Results: ' + results);
        for (ID resId : results.keyset() ){
            System.assertEquals(results.get(resId), 'Low-Med'); 	
        } 
    }

   	public static testMethod void testStaticAfterUpdateMailing2() {
		
        Map<Id, String> results = new Map<Id, String>{};
        init ();
        // test bigger changes to Mailing postal
        // new 2150 old 2052       
        testScenario('', '' , '2150', '2052');
        results = SegmentationAcctTriggerHandler.handleAfterUpdate(newList, newMap, oldList, oldMap) ;
        System.assert(results.size() > 0);
        System.debug('Bigger Mailing Change Map Results: ' + results);
        for (ID resId : results.keyset() ){
            System.assertEquals(results.get(resId), 'Low'); 	
        } 
    }
    
   	public static testMethod void testStaticAfterUpdateMailing3() {
		
        Map<Id, String> results = new Map<Id, String>{};
        init ();
        
        // test empty Mailing postal
        // new '' old 2052       
        testScenario('', '' , '', '2052');
        results = SegmentationAcctTriggerHandler.handleAfterUpdate(newList, newMap, oldList, oldMap) ;
        System.assert(results.size() > 0);
        System.debug('Empty Mailing Change Map Results: ' + results);
        for (ID resId : results.keyset() ){
            System.assertEquals(results.get(resId), 'Low'); 	
        } 	
    }
        
  	public static testMethod void testStaticAfterUpdateMailing4() {
		
        Map<Id, String> results = new Map<Id, String>{};
        init (); 
    	// test empty Mailing postal
        // new 2150 old ''       
        testScenario('', '' , '2150', '');
        results = SegmentationAcctTriggerHandler.handleAfterUpdate(newList, newMap, oldList, oldMap) ;
        System.assert(results.size() > 0);
        System.debug('Empty Mailing Change Map Results: ' + results);
        for (ID resId : results.keyset() ){
            System.assertEquals(results.get(resId), 'Low'); 	
        }
    }
     
   	public static testMethod void testStaticAfterUpdateMailing5() {
		
        Map<Id, String> results = new Map<Id, String>{};
        init ();
        // Test incorrect postcode to trigger exception
        // new 20MM old 2052       
        testScenario('', '' , '20MM', '2052');
        results = SegmentationAcctTriggerHandler.handleAfterUpdate(newList, newMap, oldList, oldMap) ;
        System.assert(results.size() > 0);
        System.debug('Incorrect Mailing Postcode Map Results: ' + results);
        for (ID resId : results.keyset() ){
            System.assertEquals(results.get(resId), 'Low'); 	
        } 	

    }   
       
    
    public static testMethod void testStaticAfterUpdateFuture() {
		Test.startTest();
        Map<Id, String> results = new Map<Id, String>{};
        init ();
        
        if (debugFlag) {
            System.debug('Debugging Future Method');
         	System.debug('Geo Reference AU ' + geo_references);
            System.debug('Segmentation Before ' + segments);
        }
        ////////////////////////////////////// Testing Future Postal Changes /////////////////////////////////////
     
        testScenario('2070', '2069' , '2050', '2052');
        results = SegmentationAcctTriggerHandler.handleAfterUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('Smaller Other Postal Future Method Map Results: ' + results);
        for (ID resId : results.keyset() ){
            System.assertEquals(results.get(resId), 'Low-Med'); 	
        } 	
            
        SegmentationAcctTriggerHandler.handleAfterUpdateAsync(results);
        
        Test.stopTest();	
            
        List<Segmentation__c> segmentsAfter  = [ select Id, Geo_Confidence_Flag__c, Account__c
                                           		 from Segmentation__c 
                                                 where Account__c IN :oldMap.keySet() ];
        if (debugFlag) {
            System.debug('Segmentation After ' + segmentsAfter);
        }
        for (Segmentation__c seg : segmentsAfter) {
            System.assertEquals(seg.Geo_Confidence_Flag__c, 'Low-Med'); 
        }
    }
        
        
	// Test the actual trigger itself - perform Account update
    public static testMethod void testTriggerAfterUpdateFuture() {
		
        Test.startTest();
        init ();

        List<Account> acctToUpdateList = new List <Account>();
				
        for ( ID acctId : oldMap.keySet()){
            Account newAcct = new Account();
            newAcct.Id = acctId;
            newAcct.PersonOtherPostalCode = '4000';
            acctToUpdateList.add(newAcct);
        }
        
        if (acctToUpdateList.size() > 0 )
        	update acctToUpdateList;
        
        Test.stopTest();	
            
        List<Segmentation__c> segmentsAfter  = [ select Id, Geo_Confidence_Flag__c, Account__c
        								    	from Segmentation__c 
        									  	where Account__c IN :oldMap.keySet() ];
        
        
        for (Segmentation__c seg : segmentsAfter) {
                System.assertEquals(seg.Geo_Confidence_Flag__c, 'Low'); 
        }
        
        if (debugFlag) {
            System.debug('Segmentation After Trigger fire ' + segmentsAfter);
        }
        
    }  
    
    
     // test empty update - Account does not have corresponding Segmentation
     public static testMethod void testTriggerAfterUpdateEmpty() {
		Test.startTest();
        // initiate Accounts Only (no Segments)
        initAccountMap (); 
        List<Account> acctToUpdateList = new List <Account>();
				
        for ( ID acctId : oldMap.keySet()){
            Account newAcct = new Account();
            newAcct.Id = acctId;
            newAcct.PersonOtherPostalCode = '4000';
            acctToUpdateList.add(newAcct);
        }
        if (acctToUpdateList.size() > 0 )
        	update acctToUpdateList;
        Test.stopTest();	
            
        List<Segmentation__c> segmentsAfter  = [ select Id, Geo_Confidence_Flag__c, Account__c
                                           		 from Segmentation__c 
                                                 where Account__c IN :oldMap.keySet() ];
        System.assert( segmentsAfter.isEmpty());
    }    
    
    
    
/**    
      public static testMethod void testPostcodeCalculation() {
          
           // Test static calculation of the distance between postcodes
          System.assertEquals(SegmentationAcctTriggerHandler.calcAddressChangeConfidence ('2000', '2010'), 'Low');
          System.assertEquals(SegmentationAcctTriggerHandler.calcAddressChangeConfidence ('2000', '2003'), 'Low-Med');
          System.assertEquals(SegmentationAcctTriggerHandler.calcAddressChangeConfidence ('2000', '20MM'), 'Low');
          System.assertEquals(SegmentationAcctTriggerHandler.calcAddressChangeConfidence ('20MM', '2000'), 'Low');
          System.assertEquals(SegmentationAcctTriggerHandler.calcAddressChangeConfidence ('',     '2000'), 'Low');
          System.assertEquals(SegmentationAcctTriggerHandler.calcAddressChangeConfidence ('2000', ''    ), 'Low');
          
      }
**/  

}