/**
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Author:         Unknown
Organisation:   Davanti Consulting
Role:           Unknown
Title:          ApplicationOverride.cls
Test Class:     Unknown
Version:        0.0
Since:          25/02/2015
Description:    Unknown

History
<Date>          <Authors Name>          <Brief Description of Change>
25/02/2015      Unknown                 Class Created
25/04/2019      Sudheendra GS           Changes for ARTTA-677. 
26/06/2019      Rakesh Murugan          Changes for ARTTA-677. 
                                        Page modified to use the "sessionId" variable, as opposed to "$Api.Session_ID", in all
                                        AJAX calls between Browser and Salesforce.
                                        Note: "$Api.Session_ID" corresponds to the logged-in user's session details. With the 
                                        introduction of API Client Whitelisting, these AJAX calls will fail. To circumvent this 
                                        issue, "$Api.Session_ID" is replaced with "sessionId", which corresponds to the session 
                                        details of a generic user.
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
**/

public without sharing class ApplicationOverride {
    //Global variables
    public String strDebug {get;set;}
    public Boolean bShowHeader {get;set;}       
    public static String endpoint = 'https://charteredaccountantsanz.tfaforms.net/rest';    
    public Boolean bIsPortalUser {get;set;}
    public Boolean bIsView {get;set;}
    public String strURL {get;set;}
    public Application__c application {get;set;}
    public User currentUser {get;set;}
    public Contact currentUserContact {get;set;}
    public String strRecordTypeParam {get; set;} // Record Type parameter
    public String strRecordTypeParamId {get; set;} // Record Type ID parameter
    public String strApplicationIDParam {get;set;} // Application ID parameter
    public String resBody {get;set;} // form body
    
    //rmurugan_20190626
    public  String  sessionId{get;private set;} // Parameter for Session ID

    //Start PASA change
    public String strJsonApplicationAttachments {get;set;} //A JSON string of application attachments
    //End PASA change
        
    // Internal variables
    private String strApplicantId = '';
    private String strMigrationAccountId = ''; //Start PASA change
    private String strMemberOf = '';
    private String strFormId = '';
    private String strParameters = '';
    private String strContext = '';

    private Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
    // Constructor
    public ApplicationOverride(ApexPages.standardController con) {
        //TEMPORARY FIX
        Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=11');  
        //TEMPORARY FIX
        Map<String, String> mapPageParameters = ApexPages.currentPage().getParameters();        
        strRecordTypeParam = (mapPageParameters.get('RecordType') != null)?
            (String) mapPageParameters.get('RecordType'):null;
        strRecordTypeParamId = (mapPageParameters.get('RecordTypeId') != null)?
            (String) mapPageParameters.get('RecordTypeId'):null;            
        application = (Application__c) con.getRecord(); 
        
        //DN 20160316 DEBUG
        system.debug('###ApplicationOverride:constructor:con.getId()='+con.getId());
        
        if(con.getId() != null) {
            application = [select Id, RecordTypeId, Lock__c from Application__c where Id = :con.getId() limit 1];
            strRecordTypeParam = application.RecordTypeId;  
        }  
        
        //DN 20160316 DEBUG
        system.debug('###ApplicationOverride:constructor:application='+application);
        system.debug('###ApplicationOverride:constructor:strRecordTypeParam='+strRecordTypeParam);
        // /DN
 
        // Determine context: NEW / EDIT / VIEW / LOCKED
        String currentRequestURL = URL.getCurrentRequestUrl().toExternalForm();
        bIsView = true; // Default to VIEW
        strContext = 'VIEW';    
        if(application.Id == null) { // NEW
            bIsView = false;    
            strContext = 'NEW';
        }
        else if(application.Id != null && application.Lock__c) { // LOCKED
            bIsView = true;
            strContext = 'LOCKED';
        }
        else if((application.Id != null && currentRequestURL.indexOf('/e') != -1 && !application.Lock__c) ||
                (currentRequestURL.indexOf('mode=edit') != -1) ) { // EDIT
            bIsView = false; 
            strContext = 'EDIT';
        }
        // Determine if page is viewed from COMMUNITIES PORTAL OR ANONYMOUSLY
        bIsPortalUser = IsCommunityProfileUser();
        bShowHeader = (bIsPortalUser)? true:false;
                
        // addPageMessage('bIsView: ' + bIsView);   
        system.debug('bIsView: ' + bIsView);       
        
        //DN 20160316 DEBUG
        system.debug('###ApplicationOverride:constructor:strContext='+strContext);

        //rmurugan_20190626
        setSessionDetails();
    }
    
    // Application EDIT override
    public PageReference modeEdit() {
        PageReference pref = Page.Application; //DN20160316 reverted
        pref.getParameters().put('id', application.Id);
        pref.getParameters().put('mode', 'edit');
        if(!bIsPortalUser) pref.getParameters().put('nooverride', '1');
        if(ApexPages.currentPage().getParameters().get('retURL') != null) 
            pref.getParameters().put('retURL', ApexPages.currentPage().getParameters().get('retURL'));
        pref.setRedirect(true);
        return pref;
    }
    
    // Generate Form Assembly URL
    public PageReference generateFormURL() {
        PageReference pref = null;
        system.debug('bIsPortalUser: ' + bIsPortalUser);        
        
        currentUser = [select Id, ContactId, AccountId from User where Id = :UserInfo.getUserId() limit 1];
        
        if(currentUser.ContactId != null) 
            currentUserContact = [select Id, AccountId, Account.Member_Of__c, Account.Is_Migration_Agent__c from Contact where Id = :currentUser.ContactId limit 1];   

        // INTERNAL USER
        if(!bIsPortalUser && UserInfo.getUserType() != 'Guest') {       
            if(strContext == 'NEW') {   // NEW          
                String strPrefix = globalDescribe.get('Application__c').getDescribe().getkeyprefix();
                pref = new PageReference('/' + strPrefix + '/e');   
            }                   
            if(strContext == 'EDIT' || (strContext == 'LOCKED' && ApexPages.currentPage().getParameters().get('mode') == 'edit'))  // EDIT
                pref = new PageReference('/' + application.Id + '/e');
            if(strContext == 'VIEW'|| (strContext == 'LOCKED' && ApexPages.currentPage().getParameters().get('mode') != 'edit')) // VIEW
                pref = new PageReference('/' + application.Id); 
            Set<String> setExcludedParams = new Set<String> {'sfdc.override', 'save_new', 'nooverride'};                
            for(String s:ApexPages.currentPage().getParameters().keySet()) {
                String val = ApexPages.currentPage().getParameters().get(s);
                if(s != null && val != null && !setExcludedParams.contains(s)) {    
                    if(pref != null && pref.getParameters() != null) pref.getParameters().put(s, val);
                }
            }           
            if(pref != null && pref.getParameters() != null) 
                pref.getParameters().put('nooverride', '1');
            return pref;
        }

        // Read configuration settings
        Map<String, Application_Form__c> mapApplicationFormSettings = Application_Form__c.getAll();
        if(mapApplicationFormSettings.size() == 0) {
            addErrorMessage('Custom settings not defined');
        }       
        if(currentUserContact != null && currentUserContact.AccountId != null && UserInfo.getUserType() != 'Guest') 
             strApplicantId = currentUserContact.AccountId; // Applicant Id
        if(UserInfo.getUserType() == 'Guest') {  
            Map<String, String> mapPageParameters = ApexPages.currentPage().getParameters();        
            strApplicantId = (mapPageParameters.get('AccountId') != null)?
                (String) mapPageParameters.get('AccountId'):null; // Applicant Id
        }
        if(currentUserContact != null && currentUserContact.AccountId != null && currentUserContact.Account.Member_Of__c != null) 
            strMemberOf = currentUserContact.Account.Member_Of__c;  // Member Of
        String employmentHistory = '';      
        if(currentUserContact != null && currentUserContact.AccountId != null) { // Employment History
            Integer i = 0;
            for(Employment_History__c emp:[select Id, Member__c from Employment_History__c 
                                                where Member__c = :currentUser.AccountId 
                                                order by Employee_Start_Date__c desc]) {
                employmentHistory += '&Employment_History__c' + i + '=' + emp.Id;
                i++;
            }
        }   
        system.debug('### strRecordTypeParam: ' + strRecordTypeParam);     
        system.debug('### strRecordTypeParamId: ' + strRecordTypeParamId);      
        RecordType rt = [select Id, Name, DeveloperName from RecordType 
                        where (Name = :strRecordTypeParam or Id = :strRecordTypeParamId or
                        Id = :strRecordTypeParam or Name = :strRecordTypeParamId)
                        and SObjectType = 'Application__c'
                         limit 1];                

        for(String s:mapApplicationFormSettings.keySet()) {
            Application_Form__c setting = mapApplicationFormSettings.get(s);
            if(setting.ReadOnly__c == bIsView && setting.Record_Type__c == rt.DeveloperName) {
                strFormId = setting.Form_ID__c; // FORM ID
                // addPageMessage('strFormId: ' + strFormId);   
                system.debug('### strFormId: ' + strFormId);    
                break;
            }
        }
        // Determine Related List IDs
        List<String> listRelationshipNames = new List<String>();  
        listRelationshipNames = DetermineRelatedListClass.getRelatedList(strRecordTypeParam, 'Application__c');
        // Build Application Query
        if(application.Id != null) {
            String strQuery = 'select Id, RecordTypeId, Account__c, Lock__c'; //PASA Change Account__c
            if(listRelationshipNames.size()>0) { // if additional related list exists
                for(String s:listRelationshipNames) {
                    strQuery += ', (select Id from ' + String.escapeSingleQuotes(s) + ')';
                }
            }
            strQuery += ' from Application__c where Id = \'' + String.escapeSingleQuotes(application.Id) + '\' limit 1';
            system.debug('### strQuery: ' + strQuery);
            // addPageMessage(strQuery);
            application = Database.query(strQuery);
            
            //PASA Change
            //If the account is a account.Is_Migration_Agent__c then use the application applicant as applicant
            // and not the portal user
            if (currentUserContact.Account.Is_Migration_Agent__c) {
                strApplicantId = application.Account__c;
            }
        }
        
        //PASA Change
        //Add in the migration agent acount id.      
        if(UserInfo.getUserType() != 'Guest'){
            if (currentUserContact.Account.Is_Migration_Agent__c) {
                strMigrationAccountId = currentUserContact.AccountId;
            }
        }
        
        // Build parameters 
        strParameters = 'SessionId=' + UserInfo.getSessionId(); // Session ID
        strParameters += '&r=' + String.valueOf(Math.random()); // randomizer
        strParameters += '&IsPortal=' + String.valueOf(bIsPortalUser); // portal user
        if(strApplicantId != '') strParameters += '&Account=' + strApplicantId; // Applicant    
        if(strMigrationAccountId != '') strParameters += '&MigrationAccount=' + strMigrationAccountId ; // Migration Agent Account
        if(strMemberOf != '') strParameters += '&MemberOf=' + strMemberOf;  // Member Of    
        if(rt != null && rt.Name != null) 
            strParameters += '&RecordType=' + EncodingUtil.urlEncode(rt.Name, 'UTF-8'); // Record Type Name
        if(strRecordTypeParam != null && strRecordTypeParam != '') 
            strParameters += '&RecordTypeId=' + EncodingUtil.urlEncode(strRecordTypeParam, 'UTF-8'); // Record Type Id
        if(employmentHistory != null && employmentHistory != '') strParameters += employmentHistory; // employment history                  
        if(application.Id != null) {
            strParameters += '&Application__c=' + application.Id; // Application ID             
            // Add related list parameters
            if(listRelationshipNames.size()>0) { // if additional related list exists           
                for(String s:listRelationshipNames) {
                    if(application.getSObjects(s) != null && application.getSObjects(s).size()>0) {
                        Integer i = 0;
                        for(sObject item:application.getSObjects(s)) {
                            strParameters += '&' + s + String.valueOf(i) + '=' + item.get('Id'); // ID of related list item
                            i++;
                        }
                    }                   
                }
            }
        }
        
        //Start PASA change
        // Get a list of attachments.. this is then put into javascript and can be used to show in page.
        List<Attachment> lstApplicationAttachments = [SELECT id, name FROM Attachment where ParentId = :application.Id];
        strJsonApplicationAttachments = JSON.serializePretty(lstApplicationAttachments);
        //End PASA change

        system.debug('strFormId: ' + strFormId);    
        system.debug('strParameters: ' + strParameters);  
        // addPageMessage(strParameters);   
        // Build PageReference
        if(strFormId != '') {
            callFormAssembly(strFormId, strParameters);     
        } else {
            addErrorMessage('Application form not found. Please contact your administrator.');
            system.debug('###Error: Formassembly form not found. Check custom settings.');
            /* TODO: remove this lines when fully migrated to FormAssembly forms */
            if(application != null && application.Id != null)
                return new PageReference('/apex/ApplicationEditOverride?Id=' + application.Id); 
            else 
                return new PageReference('/apex/ApplicationNewOverride');
           /* END:TODO */   
        }
        return null;
    }
    
    // Determine if page is viewed from portal
    public static Boolean IsCommunityProfileUser() {
        Boolean bReturnValue = false;
        // addPageMessage('User Type: ' + UserInfo.getUserType());

        //Start PASA update.  Removed the following and added the new type of PowerCustomerSuccess
        //This is because the Agent is customercummunit plus.
        //if(UserInfo.getUserType() == 'CspLitePortal')
        if(UserInfo.getUserType() == 'CspLitePortal' || UserInfo.getUserType() == 'PowerCustomerSuccess')
            bReturnValue = true;
        //End PASA update
        /*
        if(UserInfo.getUserType() == 'CspLitePortal') 
            bReturnValue = true;
        /*
        String strCurrentProfileId = UserInfo.getProfileId();
        for(Profile p:[select Id, Name from Profile where UserLicense.LicenseDefinitionKey 
                        like '%Customer_Community%' limit :(Limits.getLimitQueryRows() - Limits.getQueryRows())]) {
            if(p.Id == strCurrentProfileId) {
                bReturnValue = true; 
                break;
            }
        }
        */  
        return bReturnValue;    
    }
    
    // Execute web service call 
    public void callFormAssembly(String strFormID, String strRequestParameters) {
        
        system.debug('');
        system.debug('---> Entered callFormAssembly');
        system.debug('strFormID: ' + strFormID);
        system.debug('strRequestParameters: ' + strRequestParameters);

        HTTPResponse response;      
        String strURL;
        PageReference pref = ApexPages.currentPage();
        HttpRequest request = new HttpRequest();
        request.setMethod('GET');
        request.setTimeout(60000); // timeout
        
        // Build URL
        if(pref.getParameters().get('tfa_next') == null) {
            strURL = endpoint + '/forms/view/' + strFormID;
        } else {
            strURL = endpoint + pref.getParameters().get('tfa_next');
        }
        
        // Add parameters
        if(strRequestParameters != null && strRequestParameters.length()>0) {
            strURL = strURL + '?' + strRequestParameters;
        } 
        system.debug('strURL: ' + strURL);  
        
        // addPageMessage('strURL: ' + strURL); 
        request.setEndpoint(strURL);
        Http http = new Http();
        try {
            if(!Test.isRunningTest()) {            
                response = http.send(request); // Execute web service call 
                resBody = response.getBody();
                // system.debug('resBody@@@@@@@'+resBody);         
            }
        } catch(System.CalloutException e) {
            //addPageMessage(e);
            system.debug('CalloutException: ' + e);
            addErrorMessage('ERROR:  ' + e);
        }               
    }

    // Page Messages
    public void addPageMessage(ApexPages.severity severity, Object objMessage) {
        ApexPages.Message pMessage = new ApexPages.Message(severity, String.valueOf(objMessage));
        ApexPages.addMessage(pMessage);
    }

    public void addPageMessage(Object objMessage) {
        addPageMessage(ApexPages.severity.INFO, objMessage);
    }

    public void addErrorMessage(Object objMessage) {
        addPageMessage(ApexPages.severity.ERROR, objMessage);
    }

    //rmurugan_20190626
    public void setSessionDetails() {
        System.debug('');
        System.debug('DEBUG - Entered setSessionDetails');
        System.debug('----- - sessionId: ' + sessionId); 
        String sessId = null;

        /**
        String baseURL = URL.getSalesforceBaseUrl().toExternalForm(); //Get the base URL.
        System.debug('DEBUG - Base URL: ' + baseURL); 
        String loginUrl = baseURL + '/services/Soap/u/45.0';
        System.debug('DEBUG - LoginUrl: ' + loginUrl);    

        //Calling Custom Setting in order to get Form Assembly credentials and Login Url
        FormAssemblyCredentials__c FC = FormAssemblyCredentials__c.getInstance('Form');    
        System.debug('DEBUG - FC: ' + FC);
        
        //Calling the SoapLoginClass class.
        List<String> stSesId = SoapLoginClass.login(FC.UserName__c, FC.Password__c, loginUrl);
        System.debug('DEBUG - stSesId: ' + stSesId);
        **/

        //Calling the SoapLoginClass class.
        List<String> stSesId = SoapLoginClass.login();
        System.debug('----- - stSesId: ' + stSesId);

        if( stSesId != Null && !stSesId.isEmpty() ) {
            sessId = stSesId[1]; 
            sessionId = sessId;
        }
        
        System.debug('----- - sessionId: ' + sessionId); 
        System.debug('DEBUG - Exited setSessionDetails');       
        System.debug('');
    }
}