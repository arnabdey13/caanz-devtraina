/**
    Developer: WDCi (Lean)
    Development Date: 18/05/2016
    Task #: Controller for luana_MyVirtualClassNew vf page LCA-499
    
    Change History:
    LCA-626 09/06/2016: WDCi Lean - add timezone next to datetime field
    LCA-642 14/06/2016: WDCi KH - change timezone to always Australia/Sydney time
    LCA-887 04/08/2016 - WDCi Lean: append user timezone to date/time field, remove conversion as we now depends on user profile
**/

public without sharing class luana_MyVirtualClassNewController {
    
    public Id custCommConId {get; private set;}
    public Id custCommAccId {get; private set;}
    
    public LuanaSMS__Student_Program__c selectedSP {public get; private set;}
    
    public boolean isValid {public get; private set;}
    public boolean isMember {public get; private set;}
    
    public List<LuanaSMS__Course_Session__c> eligibleVirtualClasses {private set; public get;}
    public String selectedSession {set; get;}
    
    public boolean hasOverlapSession {private set; get;}
    
    //LCA-571
    LuanaSMS__Attendance2__c cancelAttendance;
    Set<Id> sessionIdToExclues;
    public boolean hasDuplicateTopic {private set; get;}
    
    //LCA-626
    public String userTimezone {get; private set;}
    
    //LCA-887 no longer required conversion
    //LCA-642
    //public Map<Id, String> vcStartTime {get; set;}
    //public Map<Id, String> vcEndTime {get; set;}
    //LCA-887 end
    
    public luana_MyVirtualClassNewController(){
        
        //test data
        //luana_CommUserUtil commUserUtil = new luana_CommUserUtil('005N0000002Nw25');
        
        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId()); 
        this.custCommConId = commUserUtil.custCommConId;       
        this.custCommAccId = commUserUtil.custCommAccId;
        
        isValid = false;
        hasOverlapSession = false;
        
        //LCA-887 no longer required conversion
        //LCA-642
        //vcStartTime = new Map<Id, String>();
        //vcEndTime = new Map<Id, String>();
        //LCA-887 end
        
        //LCA-571 to handle vc transfer
        hasDuplicateTopic = false;
        sessionIdToExclues = new Set<Id>();
        if(getCancelAttendanceIdFromURL() != null){
            for(LuanaSMS__Attendance2__c attendance : [select Id, LuanaSMS__Session__c from LuanaSMS__Attendance2__c where id =: getCancelAttendanceIdFromURL() and LuanaSMS__Contact_Student__c =: this.custCommConId]){
                cancelAttendance = attendance;
                sessionIdToExclues.add(attendance.LuanaSMS__Session__c);
            }
        }
        
        if(getStudentProgramIdFromURL() != null){
            try{
                //get the sp
                selectedSP = [Select Id, Name, Paid__c, LuanaSMS__Status__c, Exam_Location__c, Exam_Location__r.Name, Opt_In__c, LuanaSMS__Course__c, 
                (select Id, Name, LuanaSMS__Attendance_Status__c, LuanaSMS__Session__c, LuanaSMS__Session__r.Topic_Key__c from LuanaSMS__Attendances__r where LuanaSMS__Session__r.RecordType.DeveloperName =: luana_SessionConstants.RECORDTYPE_SESSION_VIRTUAL)  
                from LuanaSMS__Student_Program__c Where Id =: getStudentProgramIdFromURL() and LuanaSMS__Contact_Student__c =: custCommConId];
                
                //gather the registered virtual class session key
                Set<String> registeredSessionKey = new Set<String>();
                for(LuanaSMS__Attendance2__c attendance : selectedSP.LuanaSMS__Attendances__r){
                    if(attendance.LuanaSMS__Session__r.Topic_Key__c != ''){
                        if(cancelAttendance == null || cancelAttendance.Id != attendance.Id){
                            registeredSessionKey.add(attendance.LuanaSMS__Session__r.Topic_Key__c);
                        }
                    }
                }
                
                //look for the available virtual class by course
                eligibleVirtualClasses = new List<LuanaSMS__Course_Session__c>();
                for(LuanaSMS__Course_Session__c virtualSession : [select id, LuanaSMS__Course__c, LuanaSMS__Session__c, LuanaSMS__Session__r.Name, LuanaSMS__Session__r.LuanaSMS__Start_Time__c, LuanaSMS__Session__r.LuanaSMS__End_Time__c, LuanaSMS__Session__r.Maximum_Capacity__c, LuanaSMS__Session__r.LuanaSMS__Number_of_enrolled_students__c, LuanaSMS__Session__r.Seats_Availability__c, LuanaSMS__Session__r.Show_Seats__c from LuanaSMS__Course_Session__c 
                    where LuanaSMS__Session__r.Topic_Key__c not in: registeredSessionKey and LuanaSMS__Course__c =: selectedSP.LuanaSMS__Course__c and LuanaSMS__Session__r.RecordType.DeveloperName =: luana_SessionConstants.RECORDTYPE_SESSION_VIRTUAL and LuanaSMS__Session__r.Registration_Start_Date__c <=: system.today() and LuanaSMS__Session__r.Registration_End_Date__c >=: system.today() 
                    and LuanaSMS__Session__c not in: sessionIdToExclues //LCA-571
                    order by LuanaSMS__Session__r.LuanaSMS__Start_Time__c asc nulls last
                ]){
                    
                    eligibleVirtualClasses.add(virtualSession);
                    
                    //LCA-887 no longer required conversion
                    //LCA-642 start
                    //if(virtualSession.LuanaSMS__Session__r.LuanaSMS__Start_Time__c != null){
                    //    vcStartTime.put(virtualSession.Id, virtualSession.LuanaSMS__Session__r.LuanaSMS__Start_Time__c.format('dd/MM/yyyy hh:mm:ss a', 'Australia/Sydney'));
                    //}else{
                    //    vcStartTime.put(virtualSession.Id, '');
                    //}
                    //if(virtualSession.LuanaSMS__Session__r.LuanaSMS__End_Time__c != null){
                    //    vcEndTime.put(virtualSession.Id, virtualSession.LuanaSMS__Session__r.LuanaSMS__End_Time__c.format('dd/MM/yyyy hh:mm:ss a', 'Australia/Sydney'));
                    //}else{
                    //    vcEndTime.put(virtualSession.Id, '');
                    //}
                    //LCA-642 end
                    //LCA-887 end
                }
                
                isMember = luana_NetworkUtil.isMemberCommunity();
                
                isValid = true;
                
                //replaced by lca-642
                //userTimezone = System.now().format('z', 'Australia/Sydney'); //LCA-887
                userTimezone = UserInfo.getTimeZone().getID(); //LCA-626, reused LCA-887
                
            } catch(Exception exp){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'You do not have the permission to view the record. Please try again later or contact our support if the problem persists.'));
            }
            
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Invalid request. Please try again later or contact our support if the problem persists.'));
        }
    }
    
    private String getStudentProgramIdFromURL(){
        if(ApexPages.currentPage().getParameters().containsKey('spid')){
            return ApexPages.currentPage().getParameters().get('spid');
            
        }
        
        return null;
    }
    
    private String getCancelAttendanceIdFromURL(){
        if(ApexPages.currentPage().getParameters().containsKey('cancelattendanceid')){
            return ApexPages.currentPage().getParameters().get('cancelattendanceid');
            
        }
        
        return null;
    }
    
    public PageReference doSave(){
        
        Savepoint sp = Database.setSavepoint();
        
        try{
            hasOverlapSession = false;
            
            if(selectedSession != null && selectedSession != ''){
                
                if(cancelAttendance != null && cancelAttendance.Id != null){
                    delete cancelAttendance;
                }
                
                LuanaSMS__Attendance2__c newAttendance = new LuanaSMS__Attendance2__c();
                newAttendance.LuanaSMS__Student_Program__c = getStudentProgramIdFromURL();
                newAttendance.LuanaSMS__Session__c = selectedSession;
                newAttendance.LuanaSMS__Contact_Student__c = custCommConId;
                newAttendance.LuanaSMS__Type__c = 'Student';
                
                insert newAttendance;
                
                PageReference pageRef = Page.luana_MyEnrolmentView;
                pageRef.setAnchor('virtualclass');
                pageRef.getParameters().put('spid', getStudentProgramIdFromURL());
                
                return pageRef;
            } else {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please select a session.'));
            }
            
        } catch(Exception exp){
            
            if(exp.getMessage().contains('The student has overlapping attendance')){
                hasOverlapSession = true;
            } if(exp.getMessage().contains('duplicates value on record with id')){
                hasDuplicateTopic = true;
            } else {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Error registering the session: ' + exp.getMessage()));
            }
            
            Database.rollback(sp);
        }
        
        return null;
    }
    
    public PageReference doCancel(){
        
        PageReference pageRef = Page.luana_MyEnrolmentView;
        pageRef.setAnchor('virtualclass');
        pageRef.getParameters().put('spid', getStudentProgramIdFromURL());
        
        return pageRef;
    }
    
    public String getDomainUrl(){
        return luana_NetworkUtil.getCommunityPath();
    }
    
    public boolean getIsMemberCommunity(){
        return luana_NetworkUtil.isMemberCommunity();
    }
    
    /*LCA-901
    public boolean getIsNonMemberCommunity(){
        return luana_NetworkUtil.isNonMemberCommunity();
    }*/
    
    public boolean getIsInternal(){
        return luana_NetworkUtil.isInternal();
    }
    
    public String getTemplateName(){
        return luana_NetworkUtil.getTemplateName();
        
        /*LCA-901
        if(getIsMemberCommunity()){
            return 'luana_MemberTemplate';
        } else if(getIsNonMemberCommunity()){
            return 'luana_NonMemberTemplate';
        } else if(getIsInternal()){
            return 'luana_InternalTemplate';
        }
        
        return '';*/
    }
}