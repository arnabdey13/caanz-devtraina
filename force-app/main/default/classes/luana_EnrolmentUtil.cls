/**
    Developer: WDCi (Lean)
    Development Date: 03/06/2016
    Task #: A shared util to validate student capstone enrolment (LCA-581)
    
    Change History:
    LCA-665     20/06/2016 - WDCi KH:       include credit transfer for capstone enrolment
    LCA-822     03/08/2016 - WDCi Lean:     make isAutoSubjectSelection as shared method
    LCA-1169    06/02/2018 - WDCi Lean:     added capstone logic
    PAN:5340    11/10/2018 - WDCi Lean:     add pre-requisite validation flexipath
    PAN:6258    28/06/2019 - WDCi LKoh:     added support for Subject Access on eligibleFlexipathSubjects
**/

public without sharing class luana_EnrolmentUtil {
    
    /* PAN:5340
     * @param contactId contact student id
     * @return Map<Id, boolean> AP SPS flexipath eligibility status
     */
    public static Map<Id, boolean> eligibleFlexipathSubjects(String contactId){
    	
    	String flexipathCutoffDateStr = Luana_Extension_Settings__c.getValues(luana_EnrolmentConstants.CS_FLEXIPATH_CUTOFF_DATE).Value__c;
    	DateTime cutoffDate = luana_EnrolmentUtil.parseDatetime(flexipathCutoffDateStr);
    	
    	String capCourseId = Luana_Extension_Settings__c.getValues(luana_EnrolmentConstants.CS_DEFAULT_CA_PROGRAM_COURSE_ID).Value__c;    	
    	
    	List<LuanaSMS__Student_Program_Subject__c> studentAPSPS = [Select Id, LuanaSMS__Subject__c, LuanaSMS__Subject__r.Name, LuanaSMS__Student_Program__c, LuanaSMS__Student_Program__r.CreatedDate, LuanaSMS__Student_Program__r.LuanaSMS__Course__c
                                                    from LuanaSMS__Student_Program_Subject__c 
                                                    Where LuanaSMS__Student_Program__r.RecordType.developerName =: luana_EnrolmentConstants.RECORDTYPE_STUDENTPROGRAM_ACCREDITEDPROGRAM and 
                                                    LuanaSMS__Contact_Student__c =: contactId];
    	
    	Set<Id> subjectIds = new Set<Id>();
    	
    	for(LuanaSMS__Student_Program_Subject__c apsps : studentAPSPS){
    		subjectIds.add(apsps.LuanaSMS__Subject__c);
    	}

        // PAN:6258
        List<FP_Relevant_Competency_Area__c> checkRCA = [SELECT Id, FP_Competency_Area__c, FP_Type__c, FP_Subject__c, FT_Subject_Access__c, FT_Subject_Access__r.FT_Subject__c 
                                                         FROM FP_Relevant_Competency_Area__c 
                                                         WHERE FT_Subject_Access__r.FT_Subject__c in :subjectIds];

        Map<Id, Map<Id, Set<Id>>> subjCAsMap = new Map<Id, Map<Id, Set<Id>>>();
    	for (FP_Relevant_Competency_Area__c subjCA : [SELECT Id, FP_Competency_Area__c, FP_Type__c, FP_Subject__c, FT_Subject_Access__c, FT_Subject_Access__r.FT_Subject__c 
                                                      FROM FP_Relevant_Competency_Area__c 
                                                      WHERE FT_Subject_Access__r.FT_Subject__c in :subjectIds 
                                                      AND FP_Type__c =: luana_EnrolmentConstants.COMPETENCY_AREA_TYPE_REQUIRES 
                                                      AND FP_Competency_Area__r.FP_Active__c = true 
                                                      AND FT_Subject_Access__r.FT_Active__c = true])
        {
            if (subjCA.FT_Subject_Access__r.FT_Subject__c != null) {
                if (subjCAsMap.containsKey(subjCA.FT_Subject_Access__r.FT_Subject__c)) {
                    if (subjCAsMap.get(subjCA.FT_Subject_Access__r.FT_Subject__c).containsKey(subjCA.FT_Subject_Access__c)) {
                        subjCAsMap.get(subjCA.FT_Subject_Access__r.FT_Subject__c).get(subjCA.FT_Subject_Access__c).add(subjCA.FP_Competency_Area__c);
                    } else {
                        subjCAsMap.get(subjCA.FT_Subject_Access__r.FT_Subject__c).put(subjCA.FT_Subject_Access__c, new Set<Id>{subjCA.FP_Competency_Area__c});
                    }
                } else {
                    Map<Id, Set<Id>> newSAToCAMap = new Map<Id, Set<Id>>();
                    newSAToCAMap.put(subjCA.FT_Subject_Access__c, new Set<Id>{subjCA.FP_Competency_Area__c});
                    subjCAsMap.put(subjCA.FT_Subject_Access__r.FT_Subject__c, newSAToCAMap);
                }
            }    		
    	}

        Set<Id> studentGainedCAIds = new Set<Id>();
    	for(FP_Gained_Competency_Area__c gainedCA : [select Id, FP_Competency_Area__c from FP_Gained_Competency_Area__c where FP_Contact__c =: contactId ]){
    		studentGainedCAIds.add(gainedCA.FP_Competency_Area__c);
    	}
    	
        system.debug('subjCAsMap: ' +subjCAsMap);
        system.debug('studentAPSPS: ' +studentAPSPS);
        system.debug('capCourseId: ' +capCourseId);

    	Map<Id, boolean> eligibleSubjects = new Map<Id, boolean>();
    	for(LuanaSMS__Student_Program_Subject__c apsps : studentAPSPS){
    		
    		if(cutoffDate != null && apsps.LuanaSMS__Student_Program__r.CreatedDate >= cutoffDate && subjCAsMap.containsKey(apsps.LuanaSMS__Subject__c) && apsps.LuanaSMS__Student_Program__r.LuanaSMS__Course__c == capCourseId){
                
                // PAN:6258
                eligibleSubjects.put(apsps.Id, false);
                Map<Id, Set<Id>> requiredCAbySA = subjCAsMap.get(apsps.LuanaSMS__Subject__c);
                for (Id subjectAccessId : requiredCAbySA.keySet()) {                    
                    Set<Id> requiredCAs = requiredCAbySA.get(subjectAccessId);                    
                    if (studentGainedCAIds.containsAll(requiredCAs)) {
                        eligibleSubjects.put(apsps.Id, true);
                    }   
                }
    		} else {
    			eligibleSubjects.put(apsps.Id, true);
    		}
    	}
        system.debug('eligibleSubjects: ' +eligibleSubjects);
    	return eligibleSubjects;
    }
    
    /*
     * @param datetimeStr date/time value in string, supported format is yyyy-MM-dd HH:mm:ss
     */
    public static DateTime parseDatetime(String datetimeStr){
    	
    	if(datetimeStr != null){
	    	String [] datetimeInfo = datetimeStr.split(' ');
	    	
	    	if(datetimeInfo.size() > 0){
	    		
	    		String [] dateInfo = datetimeInfo[0].split('-');
	    		String [] timeInfo;
	    		
	    		if(datetimeInfo.size() == 2){
	    			timeInfo = datetimeInfo[1].split(':');
	    		}
	    		
	    		if(dateInfo.size() == 3){
		    		if(timeInfo != null && timeInfo.size() == 3){
		    			return DateTime.newInstance(Integer.valueOf(dateInfo[0]), Integer.valueOf(dateInfo[1]), Integer.valueOf(dateInfo[2]), Integer.valueOf(timeInfo[0]), Integer.valueOf(timeInfo[1]), Integer.valueOf(timeInfo[2]));
		    		} else {
		    			return DateTime.newInstance(Integer.valueOf(dateInfo[0]), Integer.valueOf(dateInfo[1]), Integer.valueOf(dateInfo[2]));
		    		}
	    		}
	    		
	    	}
    	}
    	
    	return null;
    }
    
    public static boolean eligibleForCapstone(String contactId, List<LuanaSMS__Student_Program_Subject__c> studentAccreditedProgramSubjects, String targetCapstoneSubjectId){
        /*LCA-665
        List<LuanaSMS__Student_Program_Subject__c> userAMSPSs = [Select Id, LuanaSMS__Subject__c, LuanaSMS__Subject__r.Name, score__c, LuanaSMS__Program_Offering_Subject__c, LuanaSMS__Program_Offering_Subject__r.LuanaSMS__Program_Offering__c, LuanaSMS__Student_Program__r.RecordType.Name
                                                                from LuanaSMS__Student_Program_Subject__c 
                                                                Where LuanaSMS__Student_Program__r.RecordType.developerName =: luana_EnrolmentConstants.RECORDTYPE_STUDENTPROGRAM_ACCREDITEDMODULE and 
                                                                LuanaSMS__Contact_Student__c =: contactId];
        */
        
        integer passedSubjCount = 0;
        integer hasExemptedSubjCounter = 0;
        integer hasTransferredSubjCounter = 0; //LCA-665
        integer capstoneSubjCount = 0; //LCA-1169
        integer eligbleSPSCount = 0; //LCA-1169
        
        /*LCA-665
        for(LuanaSMS__Student_Program_Subject__c sps: userAMSPSs){
            if(sps.LuanaSMS__Subject__r.Name != luana_EnrolmentConstants.SUBJECT_CAP && sps.score__c >= luana_EnrolmentConstants.SUBJECT_PASSING_SCORE){
                passedSubjCount ++;
            }
        }*/
        
        //LCA-1169 - we need to find out the sp that contains the target capstone subject
        Set<Id> spIds = new Set<Id>();
        if(targetCapstoneSubjectId != null && !String.isBlank(targetCapstoneSubjectId)){
	        Id capstoneSubjectId = Id.valueOf(targetCapstoneSubjectId);
	        for(LuanaSMS__Student_Program_Subject__c apSPS: studentAccreditedProgramSubjects){
	        	if(apSPS.LuanaSMS__Subject__c == capstoneSubjectId){
	        		spIds.add(apSPS.LuanaSMS__Student_Program__c);
	        	}
	        }
	        
	        for(LuanaSMS__Student_Program_Subject__c apSPS: studentAccreditedProgramSubjects){
	        	if(spIds.contains(apSPS.LuanaSMS__Student_Program__c)){ //LCA-1169 - we only consider if the sps has the same parent as target capstone subject
	        		
		            //KH update this code in 09/06/2016, for production deployment remove 'apSPS.Module_Exemption_Case__c != null' checking
		            if(apSPS.LuanaSMS__Outcome_National__c == 'Recognition of prior learning granted'){
		                hasExemptedSubjCounter++;
		            }
		            
		            //LCA-665
		            if(apSPS.LuanaSMS__Outcome_National__c == 'Credit transfer/national recognition'){
		                hasTransferredSubjCounter++;
		            }
		            
		            if(apSPS.LuanaSMS__Subject__r.Is_Capstone__c){
		            	capstoneSubjCount ++;
		            }
		            
		            eligbleSPSCount ++;
	        	}
	        }
        }
        
        system.debug('passedSubjCount :: ' + passedSubjCount);
        system.debug('hasExemptedSubjCounter :: ' + hasExemptedSubjCounter);
        system.debug('capstoneSubjCount :: ' + capstoneSubjCount);
        system.debug('eligbleSPSCount :: ' + eligbleSPSCount);
        system.debug('hasTransferredSubjCounter :: ' + hasTransferredSubjCounter);
        
        //LCA-665, include transfer counter
        //if((passedSubjCount + hasExemptedSubjCounter) >= studentAccreditedProgramSubjects.size() - 1 - hasTransferredSubjCounter){ //LCA-1169
        
        //LCA-1169 substitute the capstone count
        if((passedSubjCount + hasExemptedSubjCounter) >= eligbleSPSCount - capstoneSubjCount - hasTransferredSubjCounter){
            return true;
        }
        
        //LCA-580 Backstop Enrolment
        //Query Allow Backstop Enrolment Flag
        List<Account> accList = [SELECT Allow_Backstop_Enrolment__c FROM Account WHERE PersonContactId=: contactId];
        boolean allowBackstop = false;
        if(accList.size() > 0)
            allowBackstop = accList.get(0).Allow_Backstop_Enrolment__c;
        if(allowBackstop)
            return true; 
        
        return false;
    }
    
    /*LCA-1169 obsolete
    public static boolean eligibleForCapstone(String contactId, List<LuanaSMS__Student_Program_Subject__c> studentAccreditedProgramSubjects){
        List<LuanaSMS__Student_Program_Subject__c> userAMSPSs = [Select Id, LuanaSMS__Subject__c, LuanaSMS__Subject__r.Name, score__c, LuanaSMS__Program_Offering_Subject__c, LuanaSMS__Program_Offering_Subject__r.LuanaSMS__Program_Offering__c, LuanaSMS__Student_Program__r.RecordType.Name
                                                                from LuanaSMS__Student_Program_Subject__c 
                                                                Where LuanaSMS__Student_Program__r.RecordType.developerName =: luana_EnrolmentConstants.RECORDTYPE_STUDENTPROGRAM_ACCREDITEDMODULE and 
                                                                LuanaSMS__Contact_Student__c =: contactId];
        
        integer passedSubjCount = 0;
        integer hasExemptedSubjCounter = 0;
        integer hasTransferredSubjCounter = 0; //LCA-665
        
        /\*LCA-665
        for(LuanaSMS__Student_Program_Subject__c sps: userAMSPSs){
            if(sps.LuanaSMS__Subject__r.Name != luana_EnrolmentConstants.SUBJECT_CAP && sps.score__c >= luana_EnrolmentConstants.SUBJECT_PASSING_SCORE){
                passedSubjCount ++;
            }
        }*\/
        
        integer capstoneSubjCount = 0; //LCA-1169
        
        for(LuanaSMS__Student_Program_Subject__c apSPS: studentAccreditedProgramSubjects){
            //KH update this code in 09/06/2016, for production deployment remove 'apSPS.Module_Exemption_Case__c != null' checking
            if(apSPS.LuanaSMS__Outcome_National__c == 'Recognition of prior learning granted'){
                hasExemptedSubjCounter++;
            }
            
            //LCA-665
            if(apSPS.LuanaSMS__Outcome_National__c == 'Credit transfer/national recognition'){
                hasTransferredSubjCounter++;
            }
            
            if(apSPS.LuanaSMS__Subject__r.Is_Capstone__c){
            	capstoneSubjCount ++;
            }
        }
        
        system.debug('passedSubjCount :: ' + passedSubjCount);
        system.debug('hasExemptedSubjCounter :: ' + hasExemptedSubjCounter);
        
        //LCA-665, include transfer counter
        //if((passedSubjCount + hasExemptedSubjCounter) >= studentAccreditedProgramSubjects.size() - 1 - hasTransferredSubjCounter){ //LCA-1169
        
        //LCA-1169 substitute the capstone count
        if((passedSubjCount + hasExemptedSubjCounter) >= studentAccreditedProgramSubjects.size() - capstoneSubjCount - hasTransferredSubjCounter){
            return true;
        }
        
        //LCA-580 Backstop Enrolment
        //Query Allow Backstop Enrolment Flag
        List<Account> accList = [SELECT Allow_Backstop_Enrolment__c FROM Account WHERE PersonContactId=: contactId];
        boolean allowBackstop = false;
        if(accList.size() > 0)
            allowBackstop = accList.get(0).Allow_Backstop_Enrolment__c;
        if(allowBackstop)
            return true; 
        
        return false;
    }*/
    
    //LCA-822
    public static boolean isAutoSubjectSelection(String option){
        if(option != null && option == 'Auto Selection'){
            return true;
        } else {
            return false;
        }
    }
}