/**
    Developer: WDCi (KH)
    Development Date: 21/03/2017
    Task #: Controller for luana_MyRecentLifelongLearningCourses vf component
    
**/
public without sharing class luana_LifelongLearningCoursesController {
    
    public Id custCommConId {get; private set;}
    public Id custCommAccId {get; private set;}
    
    public luana_LifelongLearningCoursesController(){
        
        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId()); 
        
        this.custCommConId = commUserUtil.custCommConId;       
        this.custCommAccId = commUserUtil.custCommAccId;
    }
}