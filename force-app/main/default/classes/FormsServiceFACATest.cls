/**
 * @Author: Jannis Bott
 * @Date: 11/11/2016
 * @Description: This is the test class for the controller that serves the Find a CA component for the community
 */
@isTest(seeAllData=false)
public with sharing class FormsServiceFACATest {

    @testSetup static void createTestData() {
        TestSubscriptionUtils.createTestData();
        List<Account> accounts = [SELECT Id From Account];
        accounts[0].Status__c = 'Active';
        accounts[0].Opt_out_of_Find_an_Accountant_register__c = false;
        accounts[0].Membership_Type__c = 'Member';
        update accounts;
    }

    testMethod static void testLoadIntegration() {
        List<Account> accounts = [Select Id FROM Account];
        if (!accounts.isEmpty()) {
            accounts[0].CPP_Picklist__c = 'Full';
            update accounts;
        }
        System.assertEquals(false, FormsServiceFACA.load(accounts[0].Id).isEmpty(),
                'The load method should return data.');
    }

    testMethod static void testSaveIntegration() {
        List<Account> accounts1 = [Select Id FROM Account];
        FormsServiceFACA.save(true, accounts1[0].Id);
        List<Account> accounts2 = [Select Find_CA_Opt_In__c FROM Account];

        System.assertEquals(1, accounts2.size(),
                'The save method should not return more than one record.');
        System.assertEquals(true, accounts2[0].Find_CA_Opt_In__c,
                'The account field Find_CA_Opt_In__c should be true');
    }

    testMethod static void testQualifiedAuditorPositive() {

        List<Account> accounts = [
                SELECT Find_CA_Opt_In__c,
                        Membership_Type__c,
                        Status__c,
                        IsPersonAccount,
                        Insolvency_Practitioner__c,
                        Qualified_Auditor__c,
                        QA_Date_of_suspension__c,
                        QA_Date_of_de_recognition__c,
                        Opt_out_of_Find_an_Accountant_register__c,
                        Financial_Category__c,
                        BV__c,
                        SMSF__c,
                        FP_Specialisation__c,
                        NZAIP_Date_of_suspension__c,
                        NZAIP_Date_of_de_recognition__c,
                        CPP__c
                FROM Account
                WHERE Status__c = 'Active'
                AND Opt_out_of_Find_an_Accountant_register__c = false
        ];
        if (!accounts.isEmpty()) {
            accounts[0].Qualified_Auditor__c = true;
            accounts[0].QA_Date_of_suspension__c = null;
            accounts[0].QA_Date_of_de_recognition__c = null;
            update accounts;
        }
        System.assertNotEquals(false, FormsServiceFACA.isQualifiedAuditor(accounts[0]),
                'The return type for qualified auditor should be true');
    }

    testMethod static void testQualifiedAuditorNegative() {

        List<Account> accounts = [
                SELECT Find_CA_Opt_In__c,
                        Membership_Type__c,
                        Status__c,
                        IsPersonAccount,
                        Insolvency_Practitioner__c,
                        Qualified_Auditor__c,
                        QA_Date_of_suspension__c,
                        QA_Date_of_de_recognition__c,
                        Opt_out_of_Find_an_Accountant_register__c,
                        Financial_Category__c,
                        BV__c,
                        SMSF__c,
                        FP_Specialisation__c,
                        NZAIP_Date_of_suspension__c,
                        NZAIP_Date_of_de_recognition__c,
                        CPP__c
                FROM Account
                WHERE Status__c = 'Active'
                AND Opt_out_of_Find_an_Accountant_register__c = false
        ];
        if (!accounts.isEmpty()) {
            accounts[0].Qualified_Auditor__c = false;
            accounts[0].QA_Date_of_suspension__c = System.today();
            accounts[0].QA_Date_of_de_recognition__c = null;
            update accounts;
        }

        System.assertNotEquals(true, FormsServiceFACA.isQualifiedAuditor(accounts[0]),
                'The return type for qualified auditor should be false');
    }

    testMethod static void testInsolvencyPractitionerPositive() {

        List<Account> accounts = [
                SELECT Find_CA_Opt_In__c,
                        Membership_Type__c,
                        Status__c,
                        IsPersonAccount,
                        Insolvency_Practitioner__c,
                        Qualified_Auditor__c,
                        QA_Date_of_suspension__c,
                        QA_Date_of_de_recognition__c,
                        Opt_out_of_Find_an_Accountant_register__c,
                        Financial_Category__c,
                        BV__c,
                        SMSF__c,
                        FP_Specialisation__c,
                        NZAIP_Date_of_suspension__c,
                        NZAIP_Date_of_de_recognition__c,
                        CPP__c
                FROM Account
                WHERE Status__c = 'Active'
                AND Opt_out_of_Find_an_Accountant_register__c = false
        ];
        if (!accounts.isEmpty()) {
            accounts[0].Membership_Type__c = 'NZAIP';
            accounts[0].Insolvency_Practitioner__c = true;
            accounts[0].NZAIP_Date_of_suspension__c = null;
            accounts[0].NZAIP_Date_of_de_recognition__c = null;
            update accounts;
        }



        System.assertNotEquals(false, FormsServiceFACA.isInsolvencyPractitioner(accounts[0]),
                'The return type for insolvency practitioner should be true');
    }

    testMethod static void testInsolvencyPractitionerNegative() {

        List<Account> accounts = [
                SELECT Find_CA_Opt_In__c,
                        Membership_Type__c,
                        Status__c,
                        IsPersonAccount,
                        Insolvency_Practitioner__c,
                        Qualified_Auditor__c,
                        QA_Date_of_suspension__c,
                        QA_Date_of_de_recognition__c,
                        Opt_out_of_Find_an_Accountant_register__c,
                        Financial_Category__c,
                        BV__c,
                        SMSF__c,
                        FP_Specialisation__c,
                        NZAIP_Date_of_suspension__c,
                        NZAIP_Date_of_de_recognition__c,
                        CPP__c
                FROM Account
                WHERE Status__c = 'Active'
                AND Opt_out_of_Find_an_Accountant_register__c = false
        ];
        if (!accounts.isEmpty()) {
            accounts[0].Membership_Type__c = 'NZAIP';
            accounts[0].Insolvency_Practitioner__c = false;
            accounts[0].NZAIP_Date_of_suspension__c = System.today();
            accounts[0].NZAIP_Date_of_de_recognition__c = null;
            update accounts;
        }
        System.assertNotEquals(true, FormsServiceFACA.isInsolvencyPractitioner(accounts[0]),
                'The return type for insolvency practitioner should be false');
    }

    testMethod static void testBusinessValuationSpecialistPositive() {

        List<Account> accounts = [
                SELECT Find_CA_Opt_In__c,
                        Membership_Type__c,
                        Status__c,
                        IsPersonAccount,
                        Insolvency_Practitioner__c,
                        Qualified_Auditor__c,
                        QA_Date_of_suspension__c,
                        QA_Date_of_de_recognition__c,
                        Opt_out_of_Find_an_Accountant_register__c,
                        Financial_Category__c,
                        BV__c,
                        SMSF__c,
                        FP_Specialisation__c,
                        NZAIP_Date_of_suspension__c,
                        NZAIP_Date_of_de_recognition__c,
                        CPP__c
                FROM Account
                WHERE Status__c = 'Active'
                AND Opt_out_of_Find_an_Accountant_register__c = false
        ];
        if (!accounts.isEmpty()) {
            accounts[0].BV__c = true;
            update accounts;
        }
        System.assertNotEquals(false, FormsServiceFACA.isBusinessValuationSpecialist(accounts[0]),
                'The return type for business valuation speacialist should be true');
    }

    testMethod static void testBusinessValuationSpecialistNegative() {

        List<Account> accounts = [
                SELECT Find_CA_Opt_In__c,
                        Membership_Type__c,
                        Status__c,
                        IsPersonAccount,
                        Insolvency_Practitioner__c,
                        Qualified_Auditor__c,
                        QA_Date_of_suspension__c,
                        QA_Date_of_de_recognition__c,
                        Opt_out_of_Find_an_Accountant_register__c,
                        Financial_Category__c,
                        BV__c,
                        SMSF__c,
                        FP_Specialisation__c,
                        NZAIP_Date_of_suspension__c,
                        NZAIP_Date_of_de_recognition__c,
                        CPP__c
                FROM Account
                WHERE Status__c = 'Active'
                AND Opt_out_of_Find_an_Accountant_register__c = false
        ];
        if (!accounts.isEmpty()) {
            accounts[0].BV__c = false;
            update accounts;
        }
        System.assertNotEquals(true, FormsServiceFACA.isBusinessValuationSpecialist(accounts[0]),
                'The return type for business valuation speacialist should be false');
    }

    testMethod static void testFinancialPlanningSpecialistPositive() {

        List<Account> accounts = [
                SELECT Find_CA_Opt_In__c,
                        Membership_Type__c,
                        Status__c,
                        IsPersonAccount,
                        Insolvency_Practitioner__c,
                        Qualified_Auditor__c,
                        QA_Date_of_suspension__c,
                        QA_Date_of_de_recognition__c,
                        Opt_out_of_Find_an_Accountant_register__c,
                        Financial_Category__c,
                        BV__c,
                        SMSF__c,
                        FP_Specialisation__c,
                        NZAIP_Date_of_suspension__c,
                        NZAIP_Date_of_de_recognition__c,
                        CPP__c
                FROM Account
                WHERE Status__c = 'Active'
                AND Opt_out_of_Find_an_Accountant_register__c = false
        ];
        if (!accounts.isEmpty()) {
            accounts[0].FP_Specialisation__c = true;
            update accounts;
        }
        System.assertNotEquals(false, FormsServiceFACA.isFinancialPlanningSpecialist(accounts[0]),
                'The return type for financial planning speacialist should be true');
    }

    testMethod static void testFinancialPlanningSpecialistNegative() {

        List<Account> accounts = [
                SELECT Find_CA_Opt_In__c,
                        Membership_Type__c,
                        Status__c,
                        IsPersonAccount,
                        Insolvency_Practitioner__c,
                        Qualified_Auditor__c,
                        QA_Date_of_suspension__c,
                        QA_Date_of_de_recognition__c,
                        Opt_out_of_Find_an_Accountant_register__c,
                        Financial_Category__c,
                        BV__c,
                        SMSF__c,
                        FP_Specialisation__c,
                        NZAIP_Date_of_suspension__c,
                        NZAIP_Date_of_de_recognition__c,
                        CPP__c
                FROM Account
                WHERE Status__c = 'Active'
                AND Opt_out_of_Find_an_Accountant_register__c = false
        ];
        if (!accounts.isEmpty()) {
            accounts[0].FP_Specialisation__c = false;
            update accounts;
        }
        System.assertNotEquals(true, FormsServiceFACA.isFinancialPlanningSpecialist(accounts[0]),
                'The return type for financial planning speacialist should be false');
    }

    testMethod static void testSMSFSpecialistPositive() {

        List<Account> accounts = [
                SELECT Find_CA_Opt_In__c,
                        Membership_Type__c,
                        Status__c,
                        IsPersonAccount,
                        Insolvency_Practitioner__c,
                        Qualified_Auditor__c,
                        QA_Date_of_suspension__c,
                        QA_Date_of_de_recognition__c,
                        Opt_out_of_Find_an_Accountant_register__c,
                        Financial_Category__c,
                        BV__c,
                        SMSF__c,
                        FP_Specialisation__c,
                        NZAIP_Date_of_suspension__c,
                        NZAIP_Date_of_de_recognition__c,
                        CPP__c
                FROM Account
                WHERE Status__c = 'Active'
                AND Opt_out_of_Find_an_Accountant_register__c = false
        ];
        if (!accounts.isEmpty()) {
            accounts[0].SMSF__c = true;
            update accounts;
        }
        System.assertNotEquals(false, FormsServiceFACA.isSMSFSpecialist(accounts[0]),
                'The return type for SMSF speacialist should be true');
    }

    testMethod static void testSMSFSpecialistNegativee() {

        List<Account> accounts = [
                SELECT Find_CA_Opt_In__c,
                        Membership_Type__c,
                        Status__c,
                        IsPersonAccount,
                        Insolvency_Practitioner__c,
                        Qualified_Auditor__c,
                        QA_Date_of_suspension__c,
                        QA_Date_of_de_recognition__c,
                        Opt_out_of_Find_an_Accountant_register__c,
                        Financial_Category__c,
                        BV__c,
                        SMSF__c,
                        FP_Specialisation__c,
                        NZAIP_Date_of_suspension__c,
                        NZAIP_Date_of_de_recognition__c,
                        CPP__c
                FROM Account
                WHERE Status__c = 'Active'
                AND Opt_out_of_Find_an_Accountant_register__c = false
        ];
        if (!accounts.isEmpty()) {
            accounts[0].SMSF__c = false;
            update accounts;
        }
        System.assertNotEquals(true, FormsServiceFACA.isSMSFSpecialist(accounts[0]),
                'The return type for SMSF speacialist should be false');
    }

    testMethod static void testCPPPositive() {

        List<Account> accounts = [
                SELECT Id
                FROM Account
                WHERE Status__c = 'Active'
                AND Opt_out_of_Find_an_Accountant_register__c = false
        ];
        if (!accounts.isEmpty()) {
            accounts[0].CPP_Picklist__c = 'Full';
            update accounts;
        }
        List<Account> accounts2 = [
                SELECT Find_CA_Opt_In__c,
                        Membership_Type__c,
                        Status__c,
                        IsPersonAccount,
                        Insolvency_Practitioner__c,
                        Qualified_Auditor__c,
                        QA_Date_of_suspension__c,
                        QA_Date_of_de_recognition__c,
                        Opt_out_of_Find_an_Accountant_register__c,
                        Financial_Category__c,
                        BV__c,
                        SMSF__c,
                        FP_Specialisation__c,
                        NZAIP_Date_of_suspension__c,
                        NZAIP_Date_of_de_recognition__c,
                        CPP__c
                FROM Account
                WHERE Status__c = 'Active'
                AND Opt_out_of_Find_an_Accountant_register__c = false
        ];
        System.assertNotEquals(false, FormsServiceFACA.isCPP(accounts2[0]),
                'The return type for CPP should be true');
    }

    testMethod static void testCPPNegative() {
        List<Account> accounts = [
                SELECT Id
                FROM Account
                WHERE Status__c = 'Active'
                AND Opt_out_of_Find_an_Accountant_register__c = false
        ];
        if (!accounts.isEmpty()) {
            accounts[0].CPP_Picklist__c = null;
            update accounts;
        }
        List<Account> accounts2 = [
                SELECT Find_CA_Opt_In__c,
                        Membership_Type__c,
                        Status__c,
                        IsPersonAccount,
                        Insolvency_Practitioner__c,
                        Qualified_Auditor__c,
                        QA_Date_of_suspension__c,
                        QA_Date_of_de_recognition__c,
                        Opt_out_of_Find_an_Accountant_register__c,
                        Financial_Category__c,
                        BV__c,
                        SMSF__c,
                        FP_Specialisation__c,
                        NZAIP_Date_of_suspension__c,
                        NZAIP_Date_of_de_recognition__c,
                        CPP__c
                FROM Account
                WHERE Status__c = 'Active'
                AND Opt_out_of_Find_an_Accountant_register__c = false
        ];
        System.assertNotEquals(true, FormsServiceFACA.isCPP(accounts2[0]),
                'The return type for CPP should be false');
    }
}