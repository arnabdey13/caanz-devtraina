/*------------------------------------------------------------------------------------
Author:        Sumit Gupta
Company:       Salesforce
Description:   Product recommendations component controller

History
Date            Author             Comments
--------------------------------------------------------------------------------------
10-05-2018     Sumit Gupta       	Initial Release
------------------------------------------------------------------------------------*/
public with sharing class CaanzProductRecommendationsController {

	private static String QUERY_BASE = 'SELECT Id, Name, ProductCode, Description, Product_URL__c, NS_Format__c, NS_SubTopic__c, NS_Location__c FROM Product2 WHERE IsActive = true';
	private static Decimal LIMIT_DEFAULT = 100;

    private static Map<String, String> ICON_MAPPING = new Map<String, String>{
        'greyIcon' => 'standard:team_member', 'orangeIcon' => 'custom:custom113', 'blueIcon' => 'standard:topic'
    };

    private static Map<String, String> ICON_COLOR_MAPPING = new Map<String, String>{
        'conference' => 'grey', 'workshop' => 'grey', 'seminar' => 'grey',
        'seminarseries' => 'grey', 'onsitelearning' => 'grey', 'blendedProgram' => 'grey',
        'webinar' => 'orange', 'recordedwebinar' => 'orange', 'self-paced' => 'orange', 'recordedevent' => 'orange',
        'networking' => 'blue', 'sporting' => 'blue', 'ceremonial' => 'blue', 'caprogrammasterclass' => 'blue'
    };

	@AuraEnabled
    public static String getRecommendations(Decimal limitQuery, Boolean sortUniqueFirst){
        List<Recommendation> recommendations = new List<Recommendation>();
    	try{
    		if(limitQuery == null) limitQuery = LIMIT_DEFAULT;
    		List<Product2> products = getProducts(sortUniqueFirst ? LIMIT_DEFAULT : limitQuery);
    		recommendations = getRecommendations(sortUniqueFirst ? sortUniqueProductsFirst(products, limitQuery) : products);
    	} catch(Exception ex){
 			System.debug(ex.getMessage());
    	} 
        return JSON.serialize(recommendations);
    }

    private static List<Product2> getProducts(Decimal limitQuery){
    	String query = QUERY_BASE + ' AND NS_SubTopic__c != null AND NS_SubTopic__c INCLUDES (\'' + String.join(getApplicableTopics(), '\',\'') + '\')';
    	query += getAccountFilterCriteria() + ' ORDER BY NS_Format__c LIMIT ' + limitQuery;
    	return Database.query(query);
    } 

    private static List<Product2> sortUniqueProductsFirst(List<Product2> products, Decimal limitQuery){
    	Map<String, Product2> sortedProductsMap = new Map<String, Product2>();
    	List<Product2> otherProducts = new List<Product2>();
    	for(Product2 prod : products){
    		if(!sortedProductsMap.containsKey(prod.NS_SubTopic__c)){
    			sortedProductsMap.put(prod.NS_SubTopic__c, prod);
    		} else if(otherProducts.size() <= limitQuery){
    			otherProducts.add(prod);
    		}
    		if(sortedProductsMap.size() == limitQuery){
    			break;
    		}
    	}
    	List<Product2> sortedProducts = sortedProductsMap.values();
    	if(sortedProducts.size() < limitQuery){
    		for(Product2 prod : otherProducts){
    			sortedProducts.add(prod);
    			if(sortedProducts.size() == limitQuery){
    				break;
    			}
    		}
    	}
    	return sortedProducts;
    }

    private static List<Recommendation> getRecommendations(List<Product2> products){
    	List<Recommendation> recommendations = new List<Recommendation>();
    	for(Product2 prod : products){
    		recommendations.add(new Recommendation(prod));
    	}
    	return recommendations;
    }

    private static List<String> getApplicableTopics(){
    	List<String> topics = new List<String>();
    	for(My_Preference__c myPreference : getMyPreferences()){
    		topics.add(myPreference.Name);
    	}
    	return topics;
    }

    private static String getAccountFilterCriteria(){
    	String criteria = '';
    	Account acc = getCurrentUserAccount();
    	if(acc != null){
    		if(String.isNotBlank(acc.Affiliated_Branch_Country__c)){
	    		criteria += ' AND NS_Country__c = \'' + acc.Affiliated_Branch_Country__c + '\'';
	    	}
	    	if(!acc.Segmentations__r.isEmpty()){
	    		Segmentation__c seg = acc.Segmentations__r[0];
	    		if(String.isNotBlank(seg.Career_Stage__c)){
	    			criteria += ' AND (NS_CareerPhase__c = null OR NS_CareerPhase__c = \'' + seg.Career_Stage__c + '\')';
	    		}
	    		if(String.isNotBlank(seg.Firm_Type__c)){
	    			criteria += ' AND (NS_FirmType__c = null OR NS_FirmType__c = \'' + seg.Firm_Type__c + '\')';
	    		}
	    		if(String.isNotBlank(seg.Organisation_Type__c)){
	    			criteria += ' AND (NS_OrgType__c = null OR NS_OrgType__c = \'' + seg.Organisation_Type__c + '\')';
	    		}
	    	}
    	}
    	return criteria;
    }

    private static Account getCurrentUserAccount(){
    	List<Account> accounts = [
    		SELECT Affiliated_Branch_Country__c, 
    		(SELECT Id, Career_Stage__c, Firm_Type__c, Organisation_Type__c FROM Segmentations__r LIMIT 1) 
    		FROM Account WHERE Id IN (SELECT AccountId FROM User WHERE Id =: UserInfo.getUserId()) LIMIT 1
    	];
    	return !accounts.isEmpty() ? accounts.get(0) : null;
    }

    private static List<My_Preference__c> getMyPreferences(){
    	return [
    		SELECT Name FROM My_Preference__c 
    		WHERE User__c =: UserInfo.getUserId()
    		AND (Type__c != 'Boolean' OR Value__c = 'true')
    	];
    }

    public class Recommendation{
    	Product2 product;
    	String icon, iconClass;

    	public Recommendation(Product2 product){
    		this.product = product;
    		getIcon(getIconClass(getFormat()));
    	}

        private void getIcon(String iconClass){
            icon = ICON_MAPPING.containsKey(iconClass) ? ICON_MAPPING.get(iconClass) : 'standard:product';
        }

        private String getIconClass(String format){
            return iconClass = (ICON_COLOR_MAPPING.containsKey(format) ? ICON_COLOR_MAPPING.get(format) : 'transparent') + 'Icon';
        }

        private String getFormat(){
            return (product.NS_Format__c != null ? product.NS_Format__c : '').toLowerCase().replaceAll(' ', '');
        }
    }
}