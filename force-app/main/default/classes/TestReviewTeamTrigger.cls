@isTest
Public Class TestReviewTeamTrigger{
/************************************************
*
*Author: Callum MacErlich (Davanti Consulting)
*Description: Test class for ReviewTeamTrigger
*History:
*02/12/2016 - Initial version - Callum MacErlich
*
************************************************/


@testSetup

    Static void setupTestData(){
        //Method to set up test data to be used in this test class.
        List<User> listUsers = new List<User>();
        Profile profileQPR = [SELECT Id, Name FROM Profile WHERE Name = 'CAANZ Business Management User' LIMIT 1];
        PermissionSet permQPRAU = [SELECT Id, Name FROM PermissionSet WHERE Name = 'QPR_AU_Permission_set' LIMIT 1];

        Profile profileReviewer = [SELECT Id, Name FROM Profile WHERE Name = 'QPR Reviewer User' LIMIT 1];
        PermissionSet permReviewer = [SELECT Id, Name FROM PermissionSet WHERE Name = 'QPR_Reviewer_AU' LIMIT 1];
        //Create Account and Review to associate Reviewer_Note__c to.
        RecordType typeBusiness = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Business_Account' LIMIT 1];
        RecordType typeMember = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Full_Member' LIMIT 1];

        List<Account> listAccounts = new List<Account>();

        Account personAccount = new Account();
        personAccount.RecordType = typeMember;
        personAccount.FirstName = 'Test';
        personAccount.LastName = 'Practice Contact';
        personAccount.Member_Of__c = 'ICAA';
        personAccount.Gender__c = 'Male';
        personAccount.Salutation = 'Mr';
        personAccount.PersonEmail = 'testpracticecontact@davanti.co.nz';
        personAccount.Create_Portal_User__c = false;
        personAccount.Communication_Preference__c= 'Home Phone';
        personAccount.PersonHomePhone= '1234';
        personAccount.PersonOtherStreet= '83 Saggers Road';
        personAccount.PersonOtherCity='JITARNING';
        personAccount.PersonOtherState='Western Australia';
        personAccount.PersonOtherCountry='Australia';
        personAccount.PersonOtherPostalCode='6365'; 

        listAccounts.add(personAccount);

        Account acct = new Account();
        acct.RecordType = typeBusiness;
        acct.Name = 'Test Account';
        acct.Type = 'Unallocated';
        acct.Phone = '6421771557';
        acct.Member_Of__c = 'ICAA';
        acct.BillingStreet = '33 Customhouse Quay';
        acct.BillingCity = 'Wellington';
        acct.BillingCountry = 'New Zealand';
        acct.Review_Contact__c = personAccount.Id;
        

        listAccounts.add(acct);    

        Account reviewer = new Account();
        reviewer.RecordType = typeMember;
        reviewer.FirstName = 'Reviewer';
        reviewer.LastName = 'LastName';
        reviewer.Member_Of__c = 'ICAA';
        reviewer.Gender__c = 'Female';
        reviewer.Salutation = 'Ms';
        reviewer.PersonEmail = 'testreviewer@example.com';
        reviewer.Create_Portal_User__c = False;
        
        reviewer.Communication_Preference__c= 'Home Phone';
        reviewer.PersonHomePhone= '1234';
        reviewer.PersonOtherStreet= '83 Saggers Road';
        reviewer.PersonOtherCity='JITARNING';
        reviewer.PersonOtherState='Western Australia';
        reviewer.PersonOtherCountry='Australia';
        reviewer.PersonOtherPostalCode='6365';  
        reviewer.Reviewer__c=True;
        listAccounts.add(reviewer);

        insert listAccounts;

        Review__c review = new Review__c();
        review.Practice_Name__c = acct.Id;
        review.Review_Country__c = 'AU';
        review.Process_Stage__c = 'AU';
        review.Person_Account__c = acct.Id; 
        review.Legacy_Quality_Review_ID__c = 'R-12345';       

        insert review;

        Review_Team__c reviewTeam = new Review_Team__c();
        reviewTeam.Review__c = review.Id;
        reviewTeam.Reviewer__c = reviewer.Id;


        insert reviewTeam;

    }
    
    @isTest
    Static void testCreateReviewTeam(){
        Review__c review  = [SELECT Id, Name FROM Review__c WHERE Legacy_Quality_Review_ID__c = 'R-12345' LIMIT 1];
        Account reviewer = [SELECT Id, FirstName, LastName FROM Account WHERE PersonEmail = 'testreviewer@example.com' LIMIT 1];
        
        Review_Team__c reviewTeam = new Review_Team__c();
        reviewTeam.Review__c = review.Id;
        reviewTeam.Reviewer__c = reviewer.Id;

       Test.startTest();
        insert reviewTeam;
       Test.stopTest();   
    }


}