global class LinkedinHandler implements Auth.RegistrationHandler{

    global Void updateUser(Id userId, Id portalId, Auth.UserData data) { system.debug('updating user******');}
    global User createUser(Id portalId, Auth.UserData data) {system.debug('creating user**********');
        return (User)[SELECT Id FROM User WHERE email = :data.email LIMIT 1][0];
    }
}