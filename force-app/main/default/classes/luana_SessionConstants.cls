/**
    Developer: WDCi (Lean)
    Development Date: 18/05/2016
    Task #: Controller for luana_MyVirtualClassNew vf page LCA-499
    
    Change History:
    LCA-710 04/Jul/2016 - WDCi Lean: added new workshop record type
    LCA-802 16/Aug/2016 - WDCi Lean: add new CASM record type
**/

public with sharing class luana_SessionConstants {
    
    public static final String RECORDTYPE_SESSION_EXAM = 'Exam';
    public static final String RECORDTYPE_SESSION_VIRTUAL = 'Virtual';
    public static final String RECORDTYPE_SESSION_WORKSHOP = 'Workshop';
    public static final String RECORDTYPE_SESSION_SUPP_EXAM = 'Supplementary_Exam';
    public static final String RECORDTYPE_SESSION_CASM = 'CASM';
    
    public static final String RECORDTYPE_ATTENDANCE_EXAM = 'Exam';
    public static final String RECORDTYPE_ATTENDANCE_VIRTUAL = 'Virtual';
    public static final String RECORDTYPE_ATTENDANCE_WORKSHOP = 'Workshop';
    public static final String RECORDTYPE_ATTENDANCE_SUPP_EXAM = 'Supplementary_Exam';
    public static final String RECORDTYPE_ATTENDANCE_CASM = 'CASM';
}