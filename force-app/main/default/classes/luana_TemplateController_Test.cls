/**
    Developer: WDCi (KH)
    Development Date:14/04/2016
    Task: Luana Test class for luana_MemberTemplateController
    
    Change History:
    LCA-701 11/07/2016 WDCi - KH: to include luana_ContributorTemplateController test class
    LCA-921 23/08/2019 WDCi - KH: Add person account email
    
**/
@isTest(SeeAllData=true)
private class luana_TemplateController_Test {
    
    private static String classNamePrefixLong = 'luana_TemplateController_Test';
    private static String classNamePrefixShort = 'ltct';
    private static map<String, Id> commProfIdMap {get; set;}
    
    public static User memberUser {get; set;}
    public static Account memberAccount {get; set;}
    public static luana_CommUserUtil userUtil {get; set;} 
    public static Luana_DataPrep_Test testDataGenerator;
    
    //LCA-921
    public static void initial(){
        //initialize
        testDataGenerator = new Luana_DataPrep_Test();
        
        //Create user with Member and Employer community access
        memberAccount = testDataGenerator.generateNewApplicantAcc('Joe_' + classNamePrefixShort, classNamePrefixLong, 'Full_Member');
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.Assessible_for_CA_Program__c = false;
        memberAccount.PersonEmail = 'joe_1_' +classNamePrefixShort+'@gmail.com';//LCA-921
        memberAccount.Communication_Preference__c= 'Home Phone';
        memberAccount.PersonHomePhone= '1234';
        memberAccount.PersonOtherStreet= '83 Saggers Road';
        memberAccount.PersonOtherCity='JITARNING';
        memberAccount.PersonOtherState='Western Australia';
        memberAccount.PersonOtherCountry='Australia';
        memberAccount.PersonOtherPostalCode='6365'; 
        insert memberAccount;
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
        
    }
    
    public static void setup(){
        
        //LCA-921
        memberUser = [Select Id, Email, FirstName, LastName, UserName, Name, ContactId from User Where Email =: 'joe_1_' + classNamePrefixShort+'@gmail.com' limit 1];
        //memberUser = testDataGenerator.generateNewApplicantUser(classNamePrefixLong, classNamePrefixshort, memberAccount, commProfIdMap.get('Customer Community Login User'));
        //insert memberUser;
        
        userUtil = new luana_CommUserUtil(memberUser.Id);
        
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
       
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defEmployerPermSetId.value__c));
        list<PermissionSetAssignment> permissAssign = [Select Id from PermissionSetAssignment where AssigneeId =: memberUser.Id AND (PermissionSetId =: defMemberPermSetId.value__c OR PermissionSetId =: defEmployerPermSetId.value__c)];
        System.runAs(memberUser){
            if(permissAssign == null)
                insert psas;
        }
    }
    
    public static testMethod void testLink(){
        Test.startTest();
            initial();
        Test.stopTest();
        setup();
        
        system.runAs(memberUser){
            luana_MemberTemplateController memberTemplate = new luana_MemberTemplateController();
            luana_EmployerTempleteController employerMemberTemplate = new luana_EmployerTempleteController();
            luana_InternalTemplateController intTemplate = new luana_InternalTemplateController();
            
            //LCA-701
            luana_ContributorTemplateController contTemplate = new luana_ContributorTemplateController();
            
            PageReference pageRef = Page.luana_StudentSessionAllocationWizard;
            pageRef.getParameters().put('retURL', 'something');
            Test.setCurrentPage(pageRef);

            intTemplate.getRetURL();
        }
    }
}