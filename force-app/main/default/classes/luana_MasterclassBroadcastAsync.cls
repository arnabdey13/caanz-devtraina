/*
    Developer: WDCi (Lean)
    Date: 29/Jul/2016
    Task #: LCA-867 Luana implementation - Masterclass broadcast
    
    Change History:
    LCA-802 18/08/2016 - WDCi Lean: to support allocation broadcast
*/

global with sharing class luana_MasterclassBroadcastAsync implements Database.Batchable<sObject>, Database.Stateful{
    
    public final static String TYPE_OPENING = 'opening';
    public final static String TYPE_ALLOCATION = 'allocation';
    
    String courseId;
    String errorRecords;
    String broadcastType;
    boolean hasError;
    
    global luana_MasterclassBroadcastAsync(String courseId, String broadcastType){
        this.courseId = courseId;
        this.errorRecords = '';
        this.broadcastType = broadcastType;
        this.hasError = false;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        
        if(broadcastType == TYPE_OPENING){
            return Database.getQueryLocator('select Id, Name, Masterclass_Broadcasted__c from LuanaSMS__Student_Program__c where Paid__c = true and LuanaSMS__Status__c = \'In Progress\' and Masterclass_Broadcasted__c = false and LuanaSMS__Course__c = \'' + courseId + '\'');
        
        } else if(broadcastType == TYPE_ALLOCATION){
            return Database.getQueryLocator('select Id, LuanaSMS__Student_Program__c from LuanaSMS__Attendance2__c Where LuanaSMS__Student_Program__c in '+ 
                                        '(Select Id from LuanaSMS__Student_Program__c where LuanaSMS__Course__c = \'' + courseId + '\' and Masterclass_Broadcasted__c = false '+ 
                                            'and LuanaSMS__Status__c = \'In Progress\' and Paid__c = true)' + 
                                        'and (LuanaSMS__Attendance2__c.Record_Type_Name__c = \'' + luana_SessionConstants.RECORDTYPE_ATTENDANCE_CASM + '\' OR LuanaSMS__Attendance2__c.Record_Type_Name__c = \'' + luana_SessionConstants.RECORDTYPE_ATTENDANCE_VIRTUAL + '\')');
        }
        
        return null;
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> scope){
        
        if(broadcastType == TYPE_OPENING){
            List<LuanaSMS__Student_Program__c> studProgsToBroadcast = new List<LuanaSMS__Student_Program__c>();
            
            for(SObject sobj : scope){
                LuanaSMS__Student_Program__c studentProgram = (LuanaSMS__Student_Program__c)sobj;
                
                studentProgram.Masterclass_Broadcasted__c = true;
                studProgsToBroadcast.add(studentProgram);
            }
                    
            integer counter = 0;
            for(Database.SaveResult sr : Database.update(studProgsToBroadcast, false)){
                if(!sr.isSuccess()){
                    errorRecords += studProgsToBroadcast.get(counter) + ' - ' + sr.getErrors()[0].getMessage() + '\n';
                    hasError = true;
                }
                
                counter ++;
            }
        } else if(broadcastType == TYPE_ALLOCATION){
            List<LuanaSMS__Attendance2__c> attendancesToBroadcast = new List<LuanaSMS__Attendance2__c>();
            
            for(SObject sobj : scope){
                LuanaSMS__Attendance2__c att = (LuanaSMS__Attendance2__c)sobj;
                att.Masterclass_Broadcasted__c = true;
                
                attendancesToBroadcast.add(att);
            }
            
            integer counter = 0;
            for(Database.SaveResult sr : Database.update(attendancesToBroadcast, false)){
                if(!sr.isSuccess()){
                    errorRecords += attendancesToBroadcast.get(counter) + ' - ' + sr.getErrors()[0].getMessage() + '\n';
                    hasError = true;
                }
                
                counter ++;
            }
        }
        
    }
    
    global void finish(Database.BatchableContext bc){
        
        LuanaSMS__Luana_Log__c errorLog = new LuanaSMS__Luana_Log__c();
        errorLog.LuanaSMS__Notify_User__c = false;
        errorLog.LuanaSMS__Related_Course__c = courseId;
        errorLog.LuanaSMS__Parent_Id__c = courseId;
        
        if(hasError){
            errorLog.LuanaSMS__Status__c = 'Failed';
            errorLog.LuanaSMS__Level__c = 'Fatal';
            errorLog.LuanaSMS__Log_Message__c = 'Error broadcasting masterclass to the following record/s:\n\n' + errorRecords;
            
        } else {
            errorLog.LuanaSMS__Status__c = 'Completed';
            errorLog.LuanaSMS__Level__c = 'Info';
        }
        
        insert errorLog;
        
    }
    
}