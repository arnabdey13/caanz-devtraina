/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class DeclarationOverrideController_Test {
	private static Account FullMemberAccountObjectInDB; // Person Account - Full Member
	private static Account MemberAccountObjectInDB;// Person Account - Member
	private static Id FullMemberContactIdInDB;
	private static Id MemberContactIdInDB;
	//private static User PersonAccountPortalUserObjectInDB;
	private static Declaration__c DeclarationObjectInDB;
	static{
		insertFullMemberAccountObject();
		insertMemberAccountObject();
		//insertPersonAccountPortalUserObject();
		insertDeclarationObject();
	}
	private static void insertFullMemberAccountObject(){
		FullMemberAccountObjectInDB = TestObjectCreator.createFullMemberAccount();
		//## Required Relationships
		//## Additional fields and relationships / Updated fields
		FullMemberAccountObjectInDB.Membership_Class__c = 'Full';
		system.debug(FullMemberAccountObjectInDB);
		insert FullMemberAccountObjectInDB;
		FullMemberContactIdInDB = [Select id from Contact where AccountId=:FullMemberAccountObjectInDB.Id].Id;
	}
	private static void insertMemberAccountObject(){
		MemberAccountObjectInDB = TestObjectCreator.createFullMemberAccount();
		//## Required Relationships
		//## Additional fields and relationships / Updated fields
		system.debug(MemberAccountObjectInDB);
		insert MemberAccountObjectInDB;
		MemberContactIdInDB = [Select id from Contact where AccountId=:MemberAccountObjectInDB.Id].Id;
	}
	private static void insertDeclarationObject(){
		DeclarationObjectInDB = TestObjectCreator.createDeclaration();
		//## Required Relationships
		DeclarationObjectInDB.Member_Name__c = FullMemberAccountObjectInDB.Id;
		//## Additional fields and relationships / Updated fields
		system.debug(DeclarationObjectInDB);
		insert DeclarationObjectInDB;
	}
	private static User getFullPersonAccountPortalUserObjectInDB(){
		Test.startTest();
		Test.stopTest(); // Future method needs to run to create the user.
		
		List<User> PersonAccountPortalUserObject_List = [Select Name from User 
			where AccountId=:FullMemberAccountObjectInDB.Id];
		System.assertEquals(1, PersonAccountPortalUserObject_List.size(), 'PersonAccountPortalUserObject_List.size' );
		return PersonAccountPortalUserObject_List[0];
	}
	private static User getPersonAccountPortalUserObjectInDB(){
		Test.startTest();
		Test.stopTest(); // Future method needs to run to create the user.
		
		List<User> PersonAccountPortalUserObject_List = [Select Name from User 
			where AccountId=:MemberAccountObjectInDB.Id];
		System.assertEquals(1, PersonAccountPortalUserObject_List.size(), 'PersonAccountPortalUserObject_List.size' );
		return PersonAccountPortalUserObject_List[0];
	}
	//******************************************************************************************
	//                             TestMethods
	//******************************************************************************************
	/*static testMethod void AccountOverrideController_testNewPageForInternalUser() {
		ApexPages.StandardController Ctrl = new ApexPages.StandardController( new Declaration__c() );
		PageReference PR = Page.DeclarationNewOverride;
		Test.setCurrentPage( PR );
		DeclarationOverrideController CtrlExt = new DeclarationOverrideController( Ctrl );
		
		PageReference NewPage = CtrlExt.gotoNewPage();
		System.assertNotEquals( null, NewPage );
		System.assert( NewPage.getUrl().startsWith('/'+ Schema.sObjectType.Declaration__c.getKeyPrefix() + '/e'), 'getUrl:' + NewPage.getUrl() );
	}*/
	
	static testMethod void DeclarationOverrideController_testNewPageForPortalUser() {
		//User FullPersonAccountPortalUserObjectInDB = getFullPersonAccountPortalUserObjectInDB();
		Account FullMemberAccountObject = TestObjectCreator.createPersonAccount();
        Test.startTest();
        FullMemberAccountObject.PersonEmail = 'PersonAccDecOverridet2345' + TestObjectCreator.getRandomNumber(80119) +'@gmail.com';
        insert FullMemberAccountObject; // Future method
        Test.stopTest();
        Id FullMemberContactId = [Select id FROM Contact WHERE AccountId =: FullMemberAccountObject.Id].Id;
        User FullPersonAccountPortalUserObjectInDB = [Select FederationIdentifier FROM User WHERE contactId=:FullMemberContactId];
		System.runAs(FullPersonAccountPortalUserObjectInDB)  {
			ApexPages.StandardController Ctrl = new ApexPages.StandardController( new Declaration__c() );
			PageReference PR = Page.ApplicationNewOverride;
			Test.setCurrentPage( PR );
			DeclarationOverrideController CtrlExt = new DeclarationOverrideController( Ctrl );
			
			PageReference NewPage = CtrlExt.gotoNewPage();
			System.assertNotEquals( null, NewPage );
		}
	}
	
	static testMethod void DeclarationOverrideController_testEditPageForUserWithAccess() {
		//User FullPersonAccountPortalUserObjectInDB = getFullPersonAccountPortalUserObjectInDB();
        Account FullMemberAccountObject = TestObjectCreator.createPersonAccount();
        Test.startTest();
        FullMemberAccountObject.PersonEmail = 'PersonAccDecOverridet667344' + TestObjectCreator.getRandomNumber(809) +'@gmail.com';
        insert FullMemberAccountObject; // Future method
        Test.stopTest();
        Id FullMemberContactId = [Select id FROM Contact WHERE AccountId =: FullMemberAccountObject.Id].Id;
        User FullPersonAccountPortalUserObjectInDB = [Select FederationIdentifier FROM User WHERE contactId=:FullMemberContactId];
		System.runAs(FullPersonAccountPortalUserObjectInDB) {
			ApexPages.StandardController Ctrl = new ApexPages.StandardController( DeclarationObjectInDB );
			PageReference PR = Page.DeclarationEditOverride;			
			Test.setCurrentPage( PR );
			DeclarationOverrideController CtrlExt = new DeclarationOverrideController( Ctrl );
			
			PageReference EditPage = CtrlExt.gotoEditPage();
			System.assertNotEquals( null, EditPage );
			System.assertNotEquals( '/apex/declarationwizardnoaccess', EditPage.getUrl(), 'getUrl' );
		}
	}
	
	static testMethod void DeclarationOverrideController_testEditPageForUserWithNoAccess() {
		//User PersonAccountPortalUserObjectInDB = getPersonAccountPortalUserObjectInDB();
        Account FullMemberAccountObject = TestObjectCreator.createPersonAccount();
        Test.startTest();
        FullMemberAccountObject.PersonEmail = 'PersonAccDecOverridet2454' + TestObjectCreator.getRandomNumber(809) +'@gmail.com';
        insert FullMemberAccountObject; // Future method
        Test.stopTest();
        Id FullMemberContactId = [Select id FROM Contact WHERE AccountId =: FullMemberAccountObject.Id].Id;
        User PersonAccountPortalUserObjectInDB = [Select FederationIdentifier FROM User WHERE contactId=:FullMemberContactId];
		System.runAs(PersonAccountPortalUserObjectInDB) {
			ApexPages.StandardController Ctrl = new ApexPages.StandardController( DeclarationObjectInDB );
			PageReference PR = Page.DeclarationEditOverride;			
			Test.setCurrentPage( PR );
			DeclarationOverrideController CtrlExt = new DeclarationOverrideController( Ctrl );
			
			PageReference EditPage = CtrlExt.gotoEditPage();
			System.assertNotEquals( null, EditPage );
			System.assertEquals( '/apex/declarationwizardaccessdenied', EditPage.getUrl(), 'getUrl' );
		}
	}
	
	/*static testMethod void AccountOverrideController_testEditPageForPortalUser() {
		ApplicationObjectInDB.Application_Status__c = 'Draft';
		update ApplicationObjectInDB;
		User PersonAccountPortalUserObjectInDB = getPersonAccountPortalUserObjectInDB();
		System.runAs(PersonAccountPortalUserObjectInDB) {
			ApexPages.StandardController Ctrl = new ApexPages.StandardController( ApplicationObjectInDB );
			PageReference PR = Page.ApplicationEditOverride;
			PR.getParameters().put('id', ApplicationObjectInDB.Id );
			Test.setCurrentPage( PR );
			ApplicationOverrideController CtrlExt = new ApplicationOverrideController( Ctrl );
			
			PageReference EditPage = CtrlExt.gotoEditPage();
			System.assertNotEquals( null, EditPage );
			System.assertEquals( '/apex/applicationwizardpage0', EditPage.getUrl(), 'getUrl' );
		}
	}
	
	static testMethod void AccountOverrideController_testTabPageForInternalUser() {
		ApplicationOverrideController Ctrl = new ApplicationOverrideController();
		
		PageReference TabPage = Ctrl.gotoTabPage();
		System.assertNotEquals( null, TabPage );
		System.assert( TabPage.getUrl().startsWith('/'+ Schema.sObjectType.Application__c.getKeyPrefix() + '/o'), 'getUrl:' + TabPage.getUrl() );
	}
	
	static testMethod void ApplicationOverrideController_testTabPageForPortalUser() {
		User PersonAccountPortalUserObjectInDB = getPersonAccountPortalUserObjectInDB();
		System.runAs(PersonAccountPortalUserObjectInDB) {
			ApplicationOverrideController Ctrl = new ApplicationOverrideController();
			
			PageReference TabPage = Ctrl.gotoTabPage();
			System.assertNotEquals( null, TabPage );
			System.assert( TabPage.getUrl().startsWith('/'+ Schema.sObjectType.Application__c.getKeyPrefix() + '/o'), 'getUrl:' + TabPage.getUrl() );
		}
	}
	
	static testMethod void ApplicationOverrideController_testTabPageForPortalUserWhoIsAMemberOfICAA() {
		User PersonAccountPortalUserObjectInDB = getPersonAccountPortalUserObjectInDB();
		FullMemberAccountObjectInDB.Member_Of__c='ICAA';
		update FullMemberAccountObjectInDB;
		System.runAs(PersonAccountPortalUserObjectInDB) {
			ApplicationOverrideController Ctrl = new ApplicationOverrideController();
			
			PageReference TabPage = Ctrl.gotoTabPage();
			System.assertEquals( null, TabPage );
		}
	}*/
}