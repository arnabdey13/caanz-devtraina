@isTest
public with sharing class AppFormsTestUtil {

 private static final String MEMBER_ID = '12345';
 
 public static Account createBusinessAccount(){
        Account acc= new Account(
        Name = 'Test Business Account',
        RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Business_Account').getRecordTypeId(),
        Member_Of__c='NZICA',
        BillingStreet = 'Unit 1, 50 Customhouse Quay, Wellington Central',
        BillingCity = 'WELLINGTON',
        BillingState = '',
        BillingPostalCode ='6011',
        BillingCountryCode = 'NZ',
        Status__c ='Active');
        return acc;
    }

 public static Application__c createApplication(Id ApplicantId,String Status,Id rectype) {
    
        Application__c app = new Application__c();        
        app.recordtypeid=rectype;
        app.Account__c=ApplicantId;
        app.Adjudged_Bankrupt__c='No';
        app.Application_Status__c=Status;        
        app.Lock__c=true;
        app.Convicted_of_any_crime__c='No'; 
        app.Subject_to_disciplinary_proceedings__c='No';
        app.Prohibited_from_company_management__c='No';
        app.Reciprocal_Application__c='No';
        app.College__c='Chartered Accountants';
        app.Reference_s_email__c = 'kat@gmail.com';
        app.Reference_s_First_name__c='testAB';
        app.Reference_s_Last__c='kavy';
        app.Reference_s_phone_number__c='9000000009';
        app.Reference_2_Company__c='ilfs';
        app.Reference_2_Email__c='test@gmail.com';
        app.Reference_2_first_name__c='warner';
        app.reference_2_last_name__c='DY';
        app.Reference_2_member_number__c='123445';
        app.reference_2_phone_number__c='9000990909';
        app.Reference_2_Role__c='manage';
        app.Reference_s_member_number__c='123456';
        app.Subject_to_disciplinary_by_education__c='No';
        app.Accounting_Bodies__c = 'ICAI (India)'; 
    
    return app;
        
  }
  
   public static Application_References__c createReference(Id testApplication,String Type,Id memberId){
   
    Map<String, Schema.RecordTypeInfo> referencesRectypes = Schema.SObjectType.Application_References__c.getRecordTypeInfosByDeveloperName(); 
    Application_References__c ref = new Application_References__c();
   
        if(Type == 'Internal'){
        ref = new Application_References__c(        
        Application__c = testApplication,
        Reference_Type__c = type,
        RecordTypeId = referencesRectypes.get(type).getRecordTypeId(), 
        Member__c =  memberId );
       }
       
        if(Type == 'External'){
        ref = new Application_References__c(        
        Application__c = testApplication,
        Reference_Type__c = type,
        RecordTypeId = referencesRectypes.get(type).getRecordTypeId(), 
        External_Reference_Email__c = 'Myexternal@123.com',
        Membership_Body__c  = 'The American Institute of Certified Public Accountants (AICPA)',
        Membership_Number__c='123456');
       }
       
       
        return ref;
   }

    public static void createMemberAccount() {
        Account a = new Account(
                RecordTypeId = RecordTypeCache.getId('Account', 'PersonAccount'),
                FirstName = 'Test', LastName = 'User',
                Salutation = 'Mr',
                PersonEmail = 'apex-tests@testingutil.com.nz.au',
                Membership_Class__c = 'Full',
                Membership_Type__c ='Member',
                Member_ID__c = MEMBER_ID,
                Member_Of__c = 'NZICA',
                Communication_Preference__c = 'Home Phone',
                PersonHomePhone = '0416334889',
                PersonOtherCountry = 'Australia',
                PersonOtherStreet= '83 Saggers Road',
                PersonOtherCity='JITARNING',                 
                PersonOtherPostalCode='6365' 
             );
        insert a;
    }
    
    public static Id getTestAccountId() {
        return [select Id from Account where PersonHomePhone = '0416334889' LIMIT 1].Id;
    } 
    
}