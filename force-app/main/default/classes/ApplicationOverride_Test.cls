/***********************************************************************************************************************************************************************
Name: ApplicationOverride_Test
============================================================================================================================== 
Purpose: This class contains code related to Unit Testing and test coverage of class ApplicationOverride 
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR           DATE          DETAIL     Description 
1.0     Sudheendra G S    3/05/2019    Created     This class contains code related to Unit Testing and test coverage of class SoapLoginClass.
                                                             
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/

@isTest
public class ApplicationOverride_Test{
    private static Account FullMemberAccountObjectInDB; // Person Account
    private static Id FullMemberContactIdInDB;
    private static Application__c ApplicationObjectInDB;
    private static User PersonAccountPortalUserObjectInDB;
    
    //In
    static{
        insertTestData();        
    }    
    
    private static void insertTestData(){
        Id RecordTypeIdContact = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Full Membership').getRecordTypeId();

        FullMemberAccountObjectInDB = TestObjectCreator.createFullMemberAccount();
        insert FullMemberAccountObjectInDB;
        
        FullMemberContactIdInDB = [Select id from Contact where AccountId=:FullMemberAccountObjectInDB.Id].Id;
        
        ApplicationObjectInDB = TestObjectCreator.createApplication();        
        ApplicationObjectInDB.Account__c = FullMemberAccountObjectInDB.Id;        
        ApplicationObjectInDB.recordtypeid=RecordTypeIdContact;
        ApplicationObjectInDB.Adjudged_Bankrupt__c='No';
        ApplicationObjectInDB.Application_Status__c='Applied';        
        ApplicationObjectInDB.Lock__c=true;
        ApplicationObjectInDB.Convicted_of_any_crime__c='No'; 
        ApplicationObjectInDB.Subject_to_disciplinary_proceedings__c='No';
        ApplicationObjectInDB.Prohibited_from_company_management__c='No';
        ApplicationObjectInDB.Reciprocal_Application__c='No';
        ApplicationObjectInDB.College__c='Chartered Accountants';
        ApplicationObjectInDB.Reference_s_email__c = 'kat@gmail.com';
        ApplicationObjectInDB.Reference_s_First_name__c='testAB';
        ApplicationObjectInDB.Reference_s_Last__c='kavy';
        ApplicationObjectInDB.Reference_s_phone_number__c='9000000009';
        ApplicationObjectInDB.Reference_2_Company__c='ilfs';
        ApplicationObjectInDB.Reference_2_Email__c='test@gmail.com';
        ApplicationObjectInDB.Reference_2_first_name__c='warner';
        ApplicationObjectInDB.reference_2_last_name__c='DY';
        ApplicationObjectInDB.Reference_2_member_number__c='123445';
        ApplicationObjectInDB.reference_2_phone_number__c='9000990909';
        ApplicationObjectInDB.Reference_2_Role__c='manage';
        ApplicationObjectInDB.Reference_s_member_number__c='123456';
        ApplicationObjectInDB.Subject_to_disciplinary_by_education__c='No';
        insert ApplicationObjectInDB;
        system.debug('AppId====='+ApplicationObjectInDB);

     
       
    }     

    /*
    testmethod private static void myTestGetSet() {

        // Instantiate the standard controller
        Application__c app = new Application__c();
        Apexpages.StandardController sc = new Apexpages.standardController(app);
        // Instantiate the extension
        ApplicationOverride appOvr= new ApplicationOverride(sc);
        
        //calling methods 
        PageReference testPageResults = appOvr.modeEdit();
        PageReference testPage = appOvr.generateFormURL();
        appOvr.callFormAssembly('219','test');
        ApplicationOverride.IsCommunityProfileUser();    
        
        //create custom settings 
        Application_Form__c af =new Application_Form__c();    
        af.Name ='Full Membership (editable)';
        af.Form_ID__c='219';
        af.Record_Type__c='Full_Membership';
        insert af; 
        system.debug('CS1====='+af);
        
        
        FormAssemblyCredentials__c FC = new FormAssemblyCredentials__c();
        FC.Name='Form';
        FC.UserName__c= 'test@gmail.com';
        FC.Password__c='abc@1234#';
        insert FC; 
        system.debug('CS2====='+FC );
        string LoginUrl='https://devtraina-charteredaccountantsanz.cs6.force.com/services/Soap/u/45.0';
        
        Application__c application = [select Id, RecordTypeId, Lock__c from Application__c where Id = :ApplicationObjectInDB.id limit 1];
        Apexpages.StandardController sc1 = new Apexpages.standardController(application);
        ApplicationOverride AppRide= new ApplicationOverride(sc1);
      
         //Inserting Portal User 
        Profile Portalprofile = [Select Id from Profile where name = 'NZICA Community Login User' limit 1];
        System.debug('What is the profile id ' + Portalprofile );
      
        User user1 = new User(
        Username = 'test12345test@test.com',
        ContactId = FullMemberContactIdInDB,
        ProfileId = portalProfile.Id,
        Alias = 'test123',
        Email = 'test12345@test.com',
        EmailEncodingKey = 'UTF-8',
        LastName = 'Kumar',
        CommunityNickname = 'test12345',
        TimeZoneSidKey = 'America/Los_Angeles',
        LocaleSidKey = 'en_US',
        LanguageLocaleKey = 'en_US'
        );
       
        insert user1 ;
        system.debug('user1===='+user1 ); 
    }
    */
   
    testmethod private static void myTestGetSet() {

        insertTestData();
        Id RecordTypeIdContact = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Full Membership').getRecordTypeId();    
 
         //create custom settings 
        Application_Form__c af =new Application_Form__c();    
        af.Name ='Full Membership (editable)';
        af.Form_ID__c='219';
        af.Record_Type__c='Full_Membership';
        insert af; 

        //Inserting Portal User 
        Profile Portalprofile = [Select Id from Profile where name = 'NZICA Community Login User' limit 1];
        //Profile Portalprofile = [select Id,name from Profile where UserType='PowerCustomerSuccess' limit 1];
        User user1 = new User(
        Username = 'testApplicationOverride_Test@testApplicationOverride_Test.com',
        ContactId = FullMemberContactIdInDB,
        ProfileId = portalProfile.Id,
        Alias = 'test123',
        Email = 'testApplicationOverride_Test@test.com',
        EmailEncodingKey = 'UTF-8',
        LastName = 'Kumar',
        CommunityNickname = 'testApplicationOverride_Test',
        TimeZoneSidKey = 'America/Los_Angeles',
        LocaleSidKey = 'en_US',
        LanguageLocaleKey = 'en_US'
        );
      
        System.debug('What is the profile id ' + Portalprofile );      

        system.runAs(user1){ 
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorSoap());

            // Instantiate the standard controller
            PageReference appVFPage = Page.Application;
            Test.setCurrentPage(appVFPage);
            ApexPages.currentPage().getParameters().put('RecordTypeId',RecordTypeIdContact);
            ApexPages.currentPage().getParameters().put('RecordType','Full Membership');
            ApexPages.currentPage().getParameters().put('strDebug','testing');

            Application__c app = new Application__c();
            ApexPages.StandardController sc = new ApexPages.standardController(app);

            // Instantiate the extension
            ApplicationOverride appOvr= new ApplicationOverride(sc);
            appOvr.strURL='testValue';
            appOvr.strApplicationIDParam='testValue';
            appOvr.resBody='testValue';
        
            Application__c application = [select Id, RecordTypeId, Lock__c from Application__c where Id = :ApplicationObjectInDB.id limit 1];
            Apexpages.StandardController sc1 = new Apexpages.standardController(application);
            ApplicationOverride AppRide= new ApplicationOverride(sc1);
            system.debug('user1===='+user1 + ' -UserType - ' + UserInfo.getUserType());   

            //calling methods 
            PageReference testPageResults = appOvr.modeEdit();
            PageReference testPage = appOvr.generateFormURL();
            appOvr.callFormAssembly('219','test');
            ApplicationOverride.IsCommunityProfileUser(); 
            appOvr.addPageMessage('INFO' + UserInfo.getUserType());
            appOvr.addPageMessage(UserInfo.getUserType());
            appOvr.addErrorMessage(UserInfo.getUserType());
            Test.stopTest();
        }

    }

    testmethod private static void nonPortalUserTest() {

        insertTestData();        

        // Instantiate the standard controller
        Application__c app = new Application__c();
        Apexpages.StandardController sc = new Apexpages.standardController(app);
        // Instantiate the extension
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorSoap()); 
        ApplicationOverride appOvr= new ApplicationOverride(sc);
        
      
        Application__c application = [select Id, RecordTypeId, Lock__c from Application__c where Id = :ApplicationObjectInDB.id limit 1];
        Apexpages.StandardController sc1 = new Apexpages.standardController(application);
        ApplicationOverride AppRide= new ApplicationOverride(sc1);

        //calling methods 
        PageReference testPage = appOvr.generateFormURL();

         Test.stopTest();
    }    
    
}