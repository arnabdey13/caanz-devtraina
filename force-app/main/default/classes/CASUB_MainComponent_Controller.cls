/*------------------------------------------------------------------------------------
Author:        Mayukhman
Company:       Tech M
Description:   This Controller will be used by CASUB Stories, related to load currentYear

History
Date            Author             Comments
--------------------------------------------------------------------------------------
03-05-2019      Mayukhman          Initial Release
------------------------------------------------------------------------------------*/
public with sharing class CASUB_MainComponent_Controller {
    /* Method to conditionaly load/create Subscription related to Person Account for Current year */
    @AuraEnabled
    public static Subscription__c loadSubscriptionInformation(){
        Subscription__c existingSubscription;
        User userRec = [SELECT AccountId FROM User WHERE ID =: UserInfo.getUserId()] ;
        String accIdVal = userRec.AccountId;
        String currentYear = getCurrentYear();
        
        List<Account> userPersonAccount = new List<Account>([Select Id,Name,
                                                             (SELECT Account__c, Fiscal_Year__c, Contact_Details_URL__c, Contact_Details_Status__c, Obligation_URL__c, Obligation_Status__c, Payment_URL__c, 
                                                              Sales_Order_Status__c, Sales_Order__c,  Contact_Details_Sub_Component__c, Obligation_Sub_Components__c, 
                                                              Year__c, Member_Age__c, Id, Name,Concession__c
                                                              From Subscriptions__r Where Year__c =: currentYear LIMIT 1)
                                                             From Account Where Id =: accIdVal]);
        if(userPersonAccount.size() > 0){
            for(Subscription__c subs : userPersonAccount[0].Subscriptions__r){
                if(subs != null)
                    existingSubscription = subs;
            }
        }
        
        return existingSubscription;
    }
    
    /* Method to update Subscription based on user selection of Step in UI*/
    @AuraEnabled
    public static Subscription__c updateSubscriptionInformation(Integer activeProgressItemIndex,sobject userSubscriptionToUpdate){
        boolean isValid = false;
        Subscription__c validateSubscription = new Subscription__c();
        User userRec = [SELECT AccountId FROM User WHERE ID =: UserInfo.getUserId()] ;
        String accIdVal = userRec.AccountId; 
        if(userSubscriptionToUpdate == null){
            createSubscriptionInformation();
        }
        String currentYear = getCurrentYear();
        List<Account> userPersonAccount = new List<Account>([Select Id,Name,Financial_Category__c,
                                                             (SELECT Id, Name, Account__c, Fiscal_Year__c, Contact_Details_URL__c, 
                                                              Contact_Details_Status__c, Obligation_URL__c, Obligation_Status__c, Payment_URL__c, 
                                                              Sales_Order_Status__c, Sales_Order__c, Contact_Details_Sub_Component__c, Obligation_Sub_Components__c, Year__c, Member_Age__c 
                                                              FROM Subscriptions__r WHERE Year__c =: currentYear LIMIT 1)
                                                             From Account Where Id =: accIdVal]);
                
        if(userPersonAccount.size() > 0){
            for(Subscription__c sub : userPersonAccount[0].Subscriptions__r){
                if(sub != null)
                    validateSubscription = sub;
            }
            //Condition for updating subscription based on the user Next button input
            //Condition when User click next in Step 1 and Contact_Details_Status__c == 'Pending'
            if(activeProgressItemIndex == 0 &&
               validateSubscription.Contact_Details_Status__c == 'Pending'
              ){
                  validateSubscription.Contact_Details_Status__c = 'Completed'; 
                  if(userPersonAccount[0].Financial_Category__c == 'Low income concession'  || userPersonAccount[0].Financial_Category__c == 'Career break concession' || userPersonAccount[0].Financial_Category__c == 'Retired' ){
                      validateSubscription.Contact_Details_Sub_Component__c = 'Concessions';
                      validateSubscription.Concession__c = userPersonAccount[0].Financial_Category__c;                      
                  } 
                  else{
                      validateSubscription.Contact_Details_Sub_Component__c = '';
                      validateSubscription.Concession__c = userPersonAccount[0].Financial_Category__c; 
                  }
                  validateSubscription = updateSubscriptionInformation(validateSubscription);
              }
            //Condition when User click next in Step 2 and Obligation_Status__c == 'Pending'
            else if(activeProgressItemIndex == 1 &&
                    validateSubscription.Obligation_Status__c == 'Pending' ){
                        validateSubscription.Obligation_Status__c = 'Completed'; 
                        validateSubscription = updateSubscriptionInformation(validateSubscription);
                    }
        }
        return validateSubscription;
    }
    @AuraEnabled
    public static Subscription__c createSubscriptionInformation(){
        String currentYear = CASUB_MainComponent_Controller.getCurrentYear();
        Subscription__c newSubscription = new Subscription__c();
        Order personAccountOrder = new Order();
        User userRec = [SELECT AccountId FROM User WHERE ID =: UserInfo.getUserId()] ;
        String accIdVal = userRec.AccountId;
        Id recordTypeSubscription = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        
        List<Account> userPersonAccount = new List<Account>([Select Id,Name,Financial_Category__c,
                                                             (SELECT Id, OrderNumber,TotalAmount,CurrencyISOCode FROM Orders WHERE (Status = 'Activated' OR Status = 'Pending')  AND RecordTypeId =: recordTypeSubscription ORDER BY CreatedDate DESC  LIMIT 1) 
                                                             From Account Where Id =: accIdVal]);
        if(userPersonAccount.size() > 0){
            for(Order odr : userPersonAccount[0].Orders){
                if(odr != null)
                    personAccountOrder = odr;
            }
        }
        newSubscription = new Subscription__c(); 
        newSubscription.Account__c = accIdVal;
        newSubscription.Contact_Details_Status__c = 'Pending';
        newSubscription.Obligation_Status__c = 'Pending';
        newSubscription.Sales_Order_Status__c = 'Pending';
        newSubscription.Year__c = currentYear;
        newSubscription.Sales_Order__c = personAccountOrder.Id;
        if(userPersonAccount[0].Financial_Category__c == 'Low income concession'  || userPersonAccount[0].Financial_Category__c == 'Career break concession' || userPersonAccount[0].Financial_Category__c == 'Retired' ){
            newSubscription.Contact_Details_Sub_Component__c = 'Concessions';
        } 
        try{
            insert newSubscription;
        }catch(DmlException e) {
            throw new AuraHandledException(e.getMessage());
        }catch(Exception e){
            throw new AuraHandledException(e.getMessage());
        }
        finally {
        }
        return newSubscription;
    }
    public static Subscription__c updateSubscriptionInformation(Subscription__c userSubscription){
        update userSubscription;
        return userSubscription;
    }
    
    public static String getCurrentYear() {
        String currentYear='';
        List<CASUB_Mandatory_Notification_Config__mdt> casub_Mandatory_Notification_ConfigList = [select Current_Year__c from CASUB_Mandatory_Notification_Config__mdt where DeveloperName = 'Current_Year'];
        if(casub_Mandatory_Notification_ConfigList!=null && casub_Mandatory_Notification_ConfigList.size()>0){
            currentYear = casub_Mandatory_Notification_ConfigList.get(0).Current_Year__c;             
        }
        return currentYear;
    }
}