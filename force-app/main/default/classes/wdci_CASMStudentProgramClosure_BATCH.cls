/**
* @author WDCi-Lean
* @date 18/06/2019
* @group CASM
* @description Batch job to close casm student program
* @change-history
*/

global class wdci_CASMStudentProgramClosure_BATCH implements Database.batchable<SObject>, Database.Stateful {

    public static final String CASM_JOB_DATE_TYPE_KEY = 'CASM Complete SP Job Date Type';
    public static final String CASM_JOB_LASTRUNDATE_KEY = 'CASM Complete SP Job Last Run Date';
    public static final String CASM_JOB_BATCHSIZE_KEY = 'CASM Complete SP Job Batch Size';

    Datetime lastRunDate;
    Datetime newLastRunDate;
    String dateType;
    boolean hasError;

    Luana_Extension_Settings__c dateTypeSetting;
    Luana_Extension_Settings__c lastRunDateSetting;

    global wdci_CASMStudentProgramClosure_BATCH() {
        //get value from luana extension setting
        dateTypeSetting = Luana_Extension_Settings__c.getValues(CASM_JOB_DATE_TYPE_KEY);
        lastRunDateSetting = Luana_Extension_Settings__c.getValues(CASM_JOB_LASTRUNDATE_KEY);

        //default the new last run date to now, this will be decided later in finish()
        newLastRunDate = System.now();
        hasError = false;

        //check if the job is using last run date or last successful run date
        if(dateTypeSetting != null && String.isNotBlank(dateTypeSetting.Value__c)){
            dateType = dateTypeSetting.Value__c;
        } else {
            dateType = 'LRD'; //default to last run date if no setting found
        }

        //we will only proceed if there is last run date specified
        if(lastRunDateSetting != null && String.isNotBlank(lastRunDateSetting.Value__c)){
            lastRunDate = Datetime.valueOf(lastRunDateSetting.Value__c);

        } else {
            String error = 'Could not find \'' + CASM_JOB_LASTRUNDATE_KEY + '\' config in Luana_Extension_Setting__c.';

            System.NoDataFoundException ndfe = new NoDataFoundException();
            ndfe.setMessage(error);

            throw ndfe;
        }

    }

    global Database.QueryLocator start(Database.BatchableContext bc){
        //search for casm student programs
        String dataQuery = 'SELECT id, LuanaSMS__Status__c, Related_Student_Program__c FROM LuanaSMS__Student_Program__c ' +
                        'WHERE RecordType.DeveloperName = \'CASM\' ' + 
                        'AND LuanaSMS__Status__c = \'In Progress\' ' + 
                        'AND Related_Student_Program__r.Result_Publish__c = true ' + 
                        'AND Related_Student_Program__c != \'\' ' + 
                        'AND Related_Student_Program__r.LastModifiedDate >=: lastRunDate ';

        return Database.getQueryLocator(dataQuery);
    }

    global void execute(Database.BatchableContext bc, List<SObject> scope){

        List<LuanaSMS__Luana_Log__c> errorLogs = new List<LuanaSMS__Luana_Log__c>();

        try{
            List<LuanaSMS__Student_Program__c> casmSPList = (List<LuanaSMS__Student_Program__c>) scope;

            //set the casm sp to completed
            for(LuanaSMS__Student_Program__c casmSP : casmSPList){
                System.debug('wdci_CASMStudentProgramClosure_BATCH.execute - casmSP :: ' + casmSP);
                
                casmSP.LuanaSMS__Override_Completion_Rule_Manual__c = true;
                casmSP.LuanaSMS__Status__c = 'Completed';
            }
        
            Integer counter = 0;
            for(Database.SaveResult sr : Database.update(casmSPList, false)){
                if(!sr.isSuccess()){

                    //define error record
                    hasError = true;
                    errorLogs.add(wdci_CASMStudentProgramClosure_BATCH.initErrorLog(casmSPList.get(counter).Id, sr.getErrors()[0].getMessage(), lastRunDate, bc.getJobId()));
                }

                counter ++;
            }

        } catch (Exception exp){
            //something is going wrong, define error record
            hasError = true;
            errorLogs.add(wdci_CASMStudentProgramClosure_BATCH.initErrorLog(null, 'Problem updating CASM student program. Error: ' + exp.getLineNumber() + ' - ' + exp.getMessage(), lastRunDate, bc.getJobId()));

        } finally {
            //insert error record if there is any
            insert errorLogs;
        }
    }

    global void finish(Database.BatchableContext bc){
        
        //decide if we want to store last run date or last successful run date
        if(hasError && dateType == 'LSRD'){
            newLastRunDate = lastRunDate;
        }

        System.debug('wdci_CASMStudentProgramClosure_BATCH.finish - hasError :: ' + hasError);
        System.debug('wdci_CASMStudentProgramClosure_BATCH.finish - dateType :: ' + dateType);
        System.debug('wdci_CASMStudentProgramClosure_BATCH.finish - newLastRunDate :: ' + newLastRunDate);

        //update custom setting with latest date value
        if(lastRunDateSetting != null){
            lastRunDateSetting.Value__c = newLastRunDate.format('yyyy-MM-dd HH:mm:ss');
            update lastRunDateSetting;
        }

        

    }

    public static LuanaSMS__Luana_Log__c initErrorLog(Id recordId, String errorMsg, Datetime lsr, Id jobId){

        LuanaSMS__Luana_Log__c newLog = new LuanaSMS__Luana_Log__c();
        newLog.LuanaSMS__Status__c = 'Failed';
        newLog.LuanaSMS__Type__c = 'CASM Student Program Closure Batch Job';
        newLog.LuanaSMS__Level__c = 'Error';
        newLog.LuanaSMS__Log_Message__c = errorMsg;

        if(String.isNotBlank(recordId)){
            newLog.LuanaSMS__Related_Student_Program__c = recordId;
            newLog.LuanaSMS__Parent_Id__c = recordId;
        }

        newLog.LuanaSMS__Reference__c = jobId.getSobjectType().getDescribe().getLabel() + ' - Id: ' + jobId;
        newLog.LuanaSMS__Notify_User__c = true;

        return newLog;
    }

}