/*
    Developer: WDCi (KH)
    Date: 24/June/2016
    Task #: Test class for luana_ActivateNMUser
    
    Change History
    LCA-921 23/08/2016 WDCi-KH: To add person account email
*/

@isTest
private class luana_ActivateNMUser_Test {

    public static Luana_DataPrep_Test dataPrep;
    public static Account acc;
    
    private static String classNamePrefixLong = 'luana_ActivateNMUser_Test';
    private static String classNamePrefixShort = 'lanmu2';
    private static map<String, Id> commProfIdMap {get; set;}
    public static Account nonMemberAcc {get; set;}
    
    public static void prepareSampleData(){
        
        //initialize
        dataPrep = new Luana_DataPrep_Test();
        
        //Create all the custom setting
        insert dataPrep.prepLuanaExtensionSettingCustomSettings();
        insert dataPrep.createLuanaConfigurationCustomSetting();
        
        //Create nonMember account with Member and Employer community access
        nonMemberAcc = dataPrep.generateNewApplicantAcc('Joe_' + classNamePrefixShort, classNamePrefixLong, 'Non_member');
        nonMemberAcc.Member_Id__c = '12345';
        nonMemberAcc.Affiliated_Branch_Country__c = 'Australia';
        nonMemberAcc.Member_Of__c = 'NZICA';
        
        
    }

    public static testMethod void testCreateNonMemberWithoutCAF() { 
        
        Test.startTest(); 
            prepareSampleData();
            nonMemberAcc.Assessible_for_CAF_Program__c = false;
            nonMemberAcc.PersonEmail = 'joe_1_'+classNamePrefixShort+'@gmail.com';//LCA-921 include person account email
            try {
                insert nonMemberAcc; 
            }catch (DmlException ex) {
                System.assertEquals(StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION, ex.getDmlType(0));
                System.assertEquals('You can\'t enrol this Person in CA Foundations. Please ensure that the Community User is created.', ex.getDmlMessage(0));
            }
        Test.stopTest();
        
        List<User> newUsers = new List<User>();

        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
        
        //User user = dataPrep.generateNewApplicantUser(classNamePrefixLong+'_1', classNamePrefixshort+'_1', nonMemberAcc, commProfIdMap.get('Customer Community Login User'));
        //insert user;
        
        //LCA-921
        User user = [Select Id, Email, FirstName, LastName, UserName, Name from User Where Email =: 'joe_1_'+classNamePrefixShort+'@gmail.com' limit 1];
        newUsers.add(user);
        
        nonMemberAcc.Assessible_for_CAF_Program__c = true;
        update nonMemberAcc;
    }
    
    public static testMethod void testCreateNonMemberWithCAF() { 
    
        prepareSampleData();
        nonMemberAcc.Assessible_for_CAF_Program__c = true;
        nonMemberAcc.PersonEmail = 'joe_2_'+classNamePrefixShort+'@gmail.com';//LCA-921 include person account email
        try {
            insert nonMemberAcc; 
        }catch (DmlException ex) {
            System.assertEquals(StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION, ex.getDmlType(0));
            System.assertEquals('You can\'t enrol this Person in CA Foundations. Please ensure that the Community User is created.', ex.getDmlMessage(0));
        }
        
    }
}