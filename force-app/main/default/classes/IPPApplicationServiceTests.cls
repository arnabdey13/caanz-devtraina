/********************************************       
Description: TestClass for IPPApplicationService Class
Author: RXP 
Created: Jun-2019
********************************************/
@IsTest(seeAllData=false)
private class IPPApplicationServiceTests {

    private static final Id recordTypeBusiness = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();

    // most employer tests with variations are in form_MyDetailsServiceTests.cls
    // tests for employer CRUD using Unknown_Employer__c are all here

    @IsTest
    private static void testEmployerOptInCRUD() {
        // same test as in My Details service but needs to cover using the IPPApplicationService instead
        Account employer = new TestDataAccountBuilder('Business Account')
                .name('Deloitte').memberOf('NZICA').status('Active')
                .billingAddress('Unit 1, 50 Customhouse Quay, Wellington Central', 'WELLINGTON', '', '6011', 'NZ')
                .create(1, true).get(0);

        Id accountId = TestSubscriptionUtils.getTestAccountId();

        System.runAs(getMemberUser()) {
            ff_WriteData wd = new ff_WriteData();
            wd.records.put('INSERT-Employment_History__c2',
                    new Employment_History__c(
                            // member id not passed by client, populated by saver impl
                            Employer__c = employer.Id,
                            Job_Title__c = 'Mail Room Deputy Assistant',
                            Employee_Start_Date__c = System.today(),
                            Employee_End_Date__c = System.today(),
                            Status__c = 'Closed',
                            Work_Hours__c = 'Full Time'
                    ));

            // reset governor limits since the add, edit and delete are invoked in different requests
            // when invoked in real-world use
            
            IPPApplicationService service = new IPPApplicationService(accountId);
            ff_WriteResult result = service.handler.save(wd);
            System.debug('opt-in insert result: ' + result);
            System.assertEquals(0, result.fieldErrors.size(), 'no errors during create');
            System.assertEquals(0, result.recordErrors.size(), 'no errors during create');
            System.debug('queries used in create: ' + Limits.getQueries());

            Test.startTest();
            
            service = new IPPApplicationService(accountId);
            ff_ReadData withHistoryRecord = service.handler.load(null);
            System.debug(withHistoryRecord);
            List<ff_Service.SObjectWrapper> histories = withHistoryRecord.records.get('Employment_History__c');
            System.debug(histories);
            // checking security access to history records
            System.assertEquals(1, histories.get(0).sObjectList.size(),
                    'created record can be read after create');

            Employment_History__c history = (Employment_History__c) histories.get(0).sObjectList.get(0);

            wd = new ff_WriteData();
            wd.records.put(history.Id,
                    new Employment_History__c(
                            Id = history.Id,
                            Status__c = 'Current'
                    ));
            System.debug('wd for update: ' + wd);

            service = new IPPApplicationService(accountId);
            result = service.handler.save(wd);
            System.debug('edit result: ' + result);
            System.assertEquals(0, result.fieldErrors.size(), 'no errors during edit');
            System.assertEquals(0, result.recordErrors.size(), 'no errors during edit');
            System.debug('queries used in edit: ' + Limits.getQueries());

            service = new IPPApplicationService(accountId);
            result = service.handler.deleteRecord(history.Id);
            System.debug('delete result: ' + result);
            System.assertEquals(0, result.fieldErrors.size(), 'no errors during delete');
            System.assertEquals(0, result.recordErrors.size(), 'no errors during delete');
            System.debug('queries used in delete: ' + Limits.getQueries());

            Test.stopTest();

        }
    }

    @IsTest
    private static void testPrimaryEmployerSwitch() {

        Account employer = new TestDataAccountBuilder('Business Account')
                .name('Deloitte').memberOf('NZICA').status('Active')
                .billingAddress('Unit 1, 50 Customhouse Quay, Wellington Central', 'WELLINGTON', '', '6011', 'NZ')
                .create(1, true).get(0);

        Account optedOutEmployer = new Account(
                Name = 'CAANZ',
                BillingStreet = 'Unit 1, 50 Customhouse Quay, Wellington Central',
                BillingCity = 'WELLINGTON',
                BillingState = '',
                BillingPostalCode = '6011',
                BillingCountryCode = 'NZ',
                Billing_DPID__c = 'DP123',
                PersonBillingAddressValidatedByQAS__c = true
        );

        Id accountId = TestSubscriptionUtils.getTestAccountId();

        System.runAs(getMemberUser()) {

            // first create an opted-in employer and save
            ff_WriteData wd = new ff_WriteData();
            wd.records.put('INSERT-Employment_History__c1',
                    new Employment_History__c(
                            // member id not passed by client, populated by saver impl
                            Employer__c = employer.Id,
                            Job_Title__c = 'Mail Room Deputy Assistant',
                            Employee_Start_Date__c = System.today(),
                            Employee_End_Date__c = System.today(),
                            Status__c = 'Current',
                            Primary_Employer__c = true,
                            Work_Hours__c = 'Full Time'
                    ));
            IPPApplicationService service = new IPPApplicationService(accountId);
            ff_WriteResult result1 = service.handler.save(wd);
            SObject history1 = result1.results.values().get(0);
            String history1Id = history1.Id;

            Test.startTest(); // now start the test

            // opted out employer should remove the other opted out primary
            wd = new ff_WriteData();
            wd.records.put('INSERT-Employment_History__c2',
                    new Employment_History__c(
                            // member id not passed by client, populated by saver impl
                            Employer__r = optedOutEmployer,
                            Job_Title__c = 'Mail Room Deputy Assistant',
                            Employee_Start_Date__c = System.today(),
                            Employee_End_Date__c = System.today(),
                            Status__c = 'Current',
                            Primary_Employer__c = true,
                            Work_Hours__c = 'Full Time'
                    ));
            service = new IPPApplicationService(accountId);
            ff_WriteResult result2 = service.handler.save(wd);
            SObject history2 = result2.results.values().get(0);
            String history2Id = (String) history2.get('Employer_Name_at_Commencement__c');

            // These lines were commented out in May 2018, to prevent 'Too many SOQL queries' error
           // checkPrimaries(accountId, new Map<String, Boolean>{
           //         history1Id => false, // should have been cleared by ProvApp calling MyDetails
           //         history2Id => true // latest is primary now
           // });

            wd = new ff_WriteData();
            wd.records.put('INSERT-Employment_History__c3',
                    new Employment_History__c(
                            // member id not passed by client, populated by saver impl
                            Employer__r = optedOutEmployer,
                            Job_Title__c = 'Mail Room Deputy Assistant',
                            Employee_Start_Date__c = System.today(),
                            Employee_End_Date__c = System.today(),
                            Status__c = 'Current',
                            Primary_Employer__c = true,
                            Work_Hours__c = 'Full Time'
                    ));
            service = new IPPApplicationService(accountId);
            ff_WriteResult result3 = service.handler.save(wd);
            SObject history3 = result3.results.values().get(0);
            String history3Id = (String) history3.get('Employer_Name_at_Commencement__c');

            Test.stopTest();
            
          
        }
    }

    private static void checkPrimaries(Id accountId, Map<String, Boolean> expected) {
        IPPApplicationService service = new IPPApplicationService(accountId);
        ff_ReadData withHistoryRecord = service.handler.load(null);
        List<SObject> histories = withHistoryRecord.records.get('Employment_History__c').get(0).sObjectList;

        System.debug('expected: ' + expected);
        for (SObject h : histories) {
            String historyId = getHistoryId(h);
            System.debug('checking: ' + historyId);
            System.assertEquals(expected.get(historyId), h.get('Primary_Employer__c'));
        }
    }

    private static String getHistoryId(SObject history) {
        if (history.Id == NULL) {
            return (String) history.get('Employer_Name_at_Commencement__c');
        } else {
            return history.Id;
        }
    }

    @IsTest
    private static void testEmployerOptOutCRUD() {
        // tests for opt-out which causes the service to save records to Unknown_Employer__c instead of Employer_History__c
        // this includes the ability to be able to load data from both objects but return only Employer_History__c to the client
        // there are some tricky edge cases when switching from opt-in to opt-out in an update, so testing needs to be more robust there

        Id accountId = TestSubscriptionUtils.getTestAccountId();

        System.runAs(getMemberUser()) {
            ff_WriteData wd = new ff_WriteData();
            Account optedOutEmployer = new Account(
                    Name = 'CAANZ',
                    BillingStreet = 'Unit 1, 50 Customhouse Quay, Wellington Central',
                    BillingCity = 'WELLINGTON',
                    BillingState = '',
                    BillingPostalCode = '6011',
                    BillingCountryCode = 'NZ',
                    Billing_DPID__c = 'DP123',
                    PersonBillingAddressValidatedByQAS__c = true
            );
            wd.records.put('INSERT-Employment_History__c2',
                    new Employment_History__c(
                            // member id not passed by client, populated by saver impl
                            Employer__r = optedOutEmployer,
                            Job_Title__c = 'Mail Room Deputy Assistant',
                            Employee_Start_Date__c = System.today(),
                            Employee_End_Date__c = System.today(),
                            Status__c = 'Current',
                            Primary_Employer__c = true,
                            Work_Hours__c = 'Full Time'
                    ));

            // reset governor limits since the add, edit and delete are invoked in different requests
            // when invoked in real-world use
            Test.startTest();

            // CREATE
            IPPApplicationService service = new IPPApplicationService(accountId);
            ff_WriteResult result = service.handler.save(wd);
            System.debug('opt-out insert result: ' + result);
            System.assertEquals(0, result.fieldErrors.size(), 'no errors during create');
            System.assertEquals(0, result.recordErrors.size(), 'no errors during create');
            System.debug('queries used in create: ' + Limits.getQueries());

            // READ
            service = new IPPApplicationService(accountId);
            ff_ReadData withHistoryRecord = service.handler.load(null);
            System.debug(withHistoryRecord);
            List<ff_Service.SObjectWrapper> histories = withHistoryRecord.records.get('Employment_History__c');
            System.debug('employer histories post-insert: ' + histories);
            System.assertEquals(1, histories.get(0).sObjectList.size(),
                    'opted out created record can be read after create');
            // sending id in Employer_Name_at_Commencement__c is a workaround to the mismatching SObject and Id types.
            // It's a hack but it's the least evil quick solution to the problem of using showing different sobject data
            // in a single list control
            System.assertNotEquals(NULL, histories.get(0).sObjectList.get(0).get('Employer_Name_at_Commencement__c'),
                    'Id has been sent to client in Employer_Name_at_Commencement__c field');

            // UPDATE
            Employment_History__c history = (Employment_History__c) histories.get(0).sObjectList.get(0);

            wd = new ff_WriteData();
            wd.records.put(history.Employer_Name_at_Commencement__c, // client list control moves id out of this field into Id before value change events start
                    new Employment_History__c(
                            // Employer_Name_at_Commencement__c field used for id, simulating the client list control/driver
                            Employer_Name_at_Commencement__c = history.Employer_Name_at_Commencement__c,
                            Status__c = 'Closed',
                            Primary_Employer__c = NULL // simulate what the client sends when this field hides/clears itself
                    ));
            System.debug('wd for update: ' + wd);

            service = new IPPApplicationService(accountId);
            result = service.handler.save(wd);
            System.debug('edit result: ' + result);
            System.assertEquals(0, result.fieldErrors.size(), 'no errors during edit');
            System.assertEquals(0, result.recordErrors.size(), 'no errors during edit');
            System.assertEquals(false, result.results.values().get(0).get('Primary_Employer__c'),
                    'The NULL value from the client has been translated into false');
            System.debug('queries used in edit: ' + Limits.getQueries());

            // These lines were commented out in May 2018, to prevent 'Too many SOQL queries' error
            // READ AFTER UPDATE
            //service = new IPPApplicationService(accountId);
            //withHistoryRecord = service.handler.load(null);
            //System.debug(withHistoryRecord);
            //histories = withHistoryRecord.records.get('Employment_History__c');
            //System.debug('employer histories post-update: ' + histories);

            Test.stopTest(); // working around governor limits for this test which fails in UAT but not in dev sandboxes (out of sync)

            // NOTE: the client supresses any change to the employer once a record is created. This simplifies
            // the update code since it doesn't have to cater for switching from one sobject to another and changing the id

            // NOTE: when opting out, the service does not check for matches against existing business accounts. It previously
            // did this but it's not required for opt-out since a CA Assessor will clean up the data anyway

            // TODO test primary field auto-switch across opt in/out

            // DELETE
            service = new IPPApplicationService(accountId);
            result = service.handler.deleteRecord(history.Employer_Name_at_Commencement__c); // work-around/client sends back this id for delete
            System.debug('delete result: ' + result);
            System.assertEquals(0, result.fieldErrors.size(), 'no errors during delete');
            System.assertEquals(0, result.recordErrors.size(), 'no errors during delete');
            System.debug('queries used in delete: ' + Limits.getQueries());

        }
    }

    

    /*@IsTest
    static void testEducationSaveMode3() {

        Id accountId = TestSubscriptionUtils.getTestAccountId();
       
        IPPApplicationService service = new IPPApplicationService(accountId);
        ff_WriteResult result = service.handler.save(wd);
        System.debug(result);
        System.assertEquals(0, result.fieldErrors.size(), 'no errors during save');
        System.assertEquals(0, result.recordErrors.size(), 'no errors during save');

        // checking legacy education field updates
        Application__c application = getApplicationWithLegacyFields(accountId);
        
        Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body'); 
        ContentVersion newfile = new ContentVersion(
            Title='Header_Picture1', 
            PathOnClient ='/Header_Picture1.jpg',
            VersionData = bodyBlob, 
            origin = 'H'
        );
        insert newfile;
        
        ContentVersion cVersion = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :newfile.Id LIMIT 1];
        
        
        ContentDocumentLink contentlink = new ContentDocumentLink();
        contentlink.LinkedEntityId = application.id;
        contentlink.contentdocumentid = cVersion.contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink; 
        
        IPPApplicationService.UpdateFiles(new List<String>{cVersion.contentdocumentid}, new List<String>{'CV'}, application.id);
        
       
    }*/

    private static Application__c getApplicationWithLegacyFields(Id accountId) {
        return [
                SELECT Id, // other means opt out for uni and degree
                        Location_of_Provider__c, Location_of_Provider_Graduate__c,
                        // 8 undergrad fields from page layout
                        University_Australia_Undergraduate__c, University_New_Zealand_Undergraduate__c, Other_University_Undergraduate__c,
                        Undergraduate_AU__c, Undergraduate_AU_other__c, Undergraduate_nz__c, Undergraduate_nz_other__c,
                        // 8 postgrad fields from page layout
                        University_Australia_Graduate__c, University_New_Zealand_Graduate__c, Other_University_Graduate__c,
                        Graduate_au__c, Graduate_au_other__c, Graduate_nz__c, Graduate_nz_other__c
                FROM Application__c
                WHERE Account__c = :accountId
        ];
    }

    

    @testSetup static void createTestData() {
        TestSubscriptionUtils.createTestData();

        TestDataDegreeJoinBuilder.joinedApproved('Otago University', 'New Zealand', 'Bachelor of Accounting', 'Undergraduate');

        TestDataFFRichTextBuilder.multiple(RICH_TEXT_NAMES, 'fake content', true);
    }

    private static User getMemberUser() {
        Id contactId = [SELECT Id FROM Contact WHERE AccountId = :TestSubscriptionUtils.getTestAccountId()].Id;
        return [select Id from User where ContactId = :contactId];
    }

    private static final List<String> RICH_TEXT_NAMES = new List<String>{
            'ProvApp.Stage6.StatementPrivacyLink',
            'ProvApp.Stage0.InternationalAgreements',
            'ProvApp.Stage1.AddressContent',
            'ProvApp.Stage0.AssistanceRequired',
            'ProvApp.Stage6.StatementPrivacyStatement',
            'ProvApp.Stage5.LegalDisciplinaryByStatutory',
            'ProvApp.Stage5.LegalDisciplinaryByTertiary',
            'ProvApp.Stage5.Legal',
            'ProvApp.Stage4.Mentoring',
            'ProvApp.Stage6.StatementDeclaration',
            'ProvApp.Stage5.LegalBankruptcy',
            'ProvApp.Stage3.Education',
            'ProvApp.Stage7.Submitted',
            'ProvApp.Stage4.ApprovedEmployment',
            'ProvApp.Stage4.OutOfApprovedEmployment',
            'ProvApp.Stage5.LegalConviction',
            'ProvApp.Stage4.MentorSearch',
            'ProvApp.Stage4.EmploymentDetails',
            'ProvApp.Stage2.NZICARESIDENT',
            'ProvApp.Stage6.StatementEUGDPRStatement',
            // PAN:5340                
            'ProvApp.StageCheckList.RequiredDocument', 
            'ProvApp.StageCheckList.RequiredDocumentNote',
            'ProvApp.StageCheckList.AdditionalInfo',
            'ProvApp.StageCheckList.AdditionalInfoNote',
            'ProvApp.StageCheckList.ProvisionalMembershipObligations',
            'ProvApp.StageCheckList.ProvisionalMembershipObligationsNote',
            'ProvApp.StageCheckList.AdditionalInfo_MenterMembershipNumber',
            'ProvApp.StageCheckList.Certification',
            'ProvApp.StageCheckList.RequiredText',
            'ProvApp.StageCheckList.Optional',
            'Application.ProvisionalMembershipObligations',
            'EducationHistory.YearOfCommence',
            'EducationHistory.YearOfFinish',
            'Application.SubjectToDisciplinaryByCompany',
            'ProvApp.Stage5.NotToManageCorporation',
            'Status.opts',
            'Type.opts',
            // end of PAN:5340
            //for IPP
            'IPP.StageCheckList.RequiredText',
            'IPP.StageCheckList.RequiredDocumentNote',
            'IPP.StageCheckList.ImportantInfo',
            'IPP.StageCheckList.Certification',
            'IPP.StageCheckList.GoodStanding',
            'IPP.Stage1.AddressContent',
            'IPP.Stage7.Submitted'
    };
    
}