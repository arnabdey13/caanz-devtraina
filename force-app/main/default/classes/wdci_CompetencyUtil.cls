/**
* @author           WDCi-LKoh
* @date             11/06/2019
* @group            Competency Automation
* @description      Utility Class for methods used to handle Competency Automation
* @change-history   #FIX003 -   WDCi-Lkoh   -   05/09/2019  -   Fix for issue with the Pathway evaluation assessment
* @change-history   #FIX006 -   WDCi-Lkoh   -   10/09/2019  -   Improved Pathway method
*/
public with sharing class wdci_CompetencyUtil {
        
    // Maps out University Subject related to the provided Universities    

    // #FIX006
    public static Map<Id, List<FT_University_Subject__c>> mapUniSubjectMk2(Set<Id> universityIDSet) {

        system.debug('Entering mapUniSubjectMk2: ' +universityIDSet);
        Map<Id, List<FT_University_Subject__c>> uniSubjectMap = new Map<Id, List<FT_University_Subject__c>>();
        
        // This method sorts the University Subject based on the University Subject's FT Pathway Subject Code length
        List<FT_University_Subject__c> uniSubjectList = [SELECT Id, FT_University__c, FT_Pathway_Subject_Code__c, FT_Subject_Code__c, FT_Inactive__c, FT_External_ID__c 
                                                        FROM FT_University_Subject__c WHERE FT_University__c IN :universityIDSet ORDER BY FT_Pathway_Subject_Code_Length__c DESC, FT_Subject_Code__c];
        for (FT_University_Subject__c uniSubject : uniSubjectList) {
            if (uniSubjectMap.containsKey(uniSubject.FT_University__c)) {
                uniSubjectMap.get(uniSubject.FT_University__c).add(uniSubject);
            } else {
                List<FT_University_Subject__c> newUniSubjectList = new List<FT_University_Subject__c>();
                newUniSubjectList.add(uniSubject);
                uniSubjectMap.put(uniSubject.FT_University__c, newUniSubjectList);
            }            
        }
        system.debug('Exiting mapUniSubjectMk2: ' +uniSubjectMap);
        return uniSubjectMap;
    }

    // Maps out Student Paper related to the provided applications
    public static Map<Id, Map<Id, FT_Student_Paper__c>> mapStudentPaper(Set<Id> applicationIDSet) {

        system.debug('Entering mapStudentPaper: ' +applicationIDSet);
        Map<Id, Map<Id, FT_Student_Paper__c>> studentPaperMap = new Map<Id, Map<Id, FT_Student_Paper__c>>();

        List<FT_Student_Paper__c> studentPaperList = [SELECT Id, FT_Education_History__c, FT_External_Id__c, FT_Passed_Credited__c, FT_Uni_Subject_Code__c, FT_University_Subject__c, 
                                                     FT_Education_History__r.Application__c, FT_Education_History__r.University_Degree__r.University__c 
                                                     FROM FT_Student_Paper__c WHERE FT_Education_History__r.Application__c IN :applicationIDSet];
        for (FT_Student_Paper__c studentPaper : studentPaperList) {
            if (studentPaperMap.containsKey(studentPaper.FT_Education_History__r.Application__c)) {
                studentPaperMap.get(studentPaper.FT_Education_History__r.Application__c).put(studentPaper.FT_University_Subject__c, studentPaper);
            } else {
                Map<Id, FT_Student_Paper__c> newStudentPaperMap = new Map<Id, FT_Student_Paper__c>();
                newStudentPaperMap.put(studentPaper.FT_University_Subject__c, studentPaper);
                studentPaperMap.put(studentPaper.FT_Education_History__r.Application__c, newStudentPaperMap);
            }
        }
        system.debug('Exiting mapStudentPaper: ' +studentPaperMap);
        return studentPaperMap;
    }

    public static Map<String, FT_Paper_Gained_Competency__c> mapPaperGainedCompetency(Set<Id> studentPaperIDSet) {

        system.debug('Entering mapPaperGainedCompetency: ' +studentPaperIDSet);
        Map<String, FT_Paper_Gained_Competency__c> paperGainedCompetencyMap = new Map<String, FT_Paper_Gained_Competency__c>();

        List<FT_Paper_Gained_Competency__c> paperGainedCompetencyList = [SELECT Id, FT_Student_Paper__c, FT_Competence_Area_Gained__c, FT_External_Id__c	FROM FT_Paper_Gained_Competency__c WHERE FT_Student_Paper__c IN :studentPaperIDSet];
        for (FT_Paper_Gained_Competency__c paperGainedCompetency : paperGainedCompetencyList) {
            String mapKey = paperGainedCompetency.FT_Student_Paper__c+ ':' +paperGainedCompetency.FT_Competence_Area_Gained__c;
            paperGainedCompetencyMap.put(mapKey, paperGainedCompetency);
        }

        system.debug('Exiting mapPaperGainedCompetency: ' +paperGainedCompetencyMap);
        return paperGainedCompetencyMap;
    }

    /**
    * @description Maps out Gained Competency Area for Students based on the Competency Area
    */    
    public static Map<Id, Map<Id, FP_Gained_Competency_Area__c>> mapGainedCompetencyArea(List<Contact> studentContactList) {

        system.debug('Entering mapGainedCompetencyArea: ' +studentContactList);
        Map<Id, Map<Id, FP_Gained_Competency_Area__c>> gainedCompetencyAreaMap = new Map<Id, Map<Id, FP_Gained_Competency_Area__c>>();

        List<FP_Gained_Competency_Area__c> gainedCompetencyAreaList = [SELECT Id, FP_Case__c, FP_Competency_Area__c, FP_Contact__c, FP_External_Id__c, FP_Student_Program_Subject__c                                                                      
                                                                      FROM FP_Gained_Competency_Area__c WHERE FP_Contact__c IN :studentContactList];
        for (FP_Gained_Competency_Area__c gainedCompetencyArea : gainedCompetencyAreaList) {
            if (gainedCompetencyAreaMap.containsKey(gainedCompetencyArea.FP_Contact__c)) {
                gainedCompetencyAreaMap.get(gainedCompetencyArea.FP_Contact__c).put(gainedCompetencyArea.FP_Competency_Area__c, gainedCompetencyArea);
            } else {
                Map<Id, FP_Gained_Competency_Area__c> newGainedCompetencyAreaMap = new Map<Id, FP_Gained_Competency_Area__c>();
                newGainedCompetencyAreaMap.put(gainedCompetencyArea.FP_Competency_Area__c, gainedCompetencyArea);
                gainedCompetencyAreaMap.put(gainedCompetencyArea.FP_Contact__c, newGainedCompetencyAreaMap);
            }
        }

        system.debug('Exiting mapGainedCompetencyArea: ' +gainedCompetencyAreaMap);
        return gainedCompetencyAreaMap;
    }

    public static Map<Id, List<FT_Pathway__c>> mapPathway(Set<Id> universityIDSet) {

        system.debug('Entering mapPathway: ' +universityIDSet);
        Map<Id, List<FT_Pathway__c>> pathwayMap = new Map<Id, List<FT_Pathway__c>>();
        
        // #FIX003  - added FT_Active__c
        List<FT_Pathway__c> pathwayList = [SELECT Id, Name, FT_Active__c, FT_Competency_Area__c, FT_External_Id__c, FT_Pathway__c, FT_University__c 
                                          FROM FT_Pathway__c WHERE FT_University__c IN :universityIDSet];
        for (FT_Pathway__c pathway : pathwayList) {
            if (pathwayMap.containsKey(pathway.FT_University__c)) {
                pathwayMap.get(pathway.FT_University__c).add(pathway);
            } else {
                List<FT_Pathway__c> newPathwayList = new List<FT_Pathway__c>();
                newPathwayList.add(pathway);
                pathwayMap.put(pathway.FT_University__c, newPathwayList);
            }
        }
        system.debug('Exiting mapPathway: ' +pathwayMap);
        return pathwayMap;
    }

    /** 
    public static Map<Id, edu_Education_History__c> mapEducationHistory(Set<Id> educationHistoryIDSet) {

        system.debug('Entering mapEducationHistory: ' +educationHistoryIDSet);
        Map<Id, edu_Education_History__c> educationHistoryMap = new Map<Id, edu_Education_History__c>();

        List<edu_Education_History__c> educationHistoryList  = [SELECT Id, FT_Additional_Remark__c, Application__c, FP_Contact__c, University_Degree__c, FT_My_eQuals__c,
                                                               University_Degree__r.University__c, University_Degree__r.Degree__c
                                                               FROM edu_Education_History__c WHERE Id IN :educationHistoryIDSet];
        for (edu_Education_History__c educationHistory : educationHistoryList) {
            educationHistoryMap.put(educationHistory.Id, educationHistory);
        }
        system.debug('Exiting mapEducationHistory: ' +educationHistoryMap);
        return educationHistoryMap;
    }
    */

    public static Map<Id, Contact> mapPersonAccountContact(Set<Id> personAccountIDSet) {
        List<Contact> contactList = [SELECT Id, AccountId, isPersonAccount FROM Contact WHERE AccountId IN :personAccountIDSet AND isPersonAccount = true];
        Map<Id, Contact> contactMap = new Map<Id, Contact>();
        for (Contact ct : contactList) {
            contactMap.put(ct.AccountId, ct);
        }
        return contactMap;
    }

    // ===== Logic Parser Methods =====

    public class LogicString {

        /**
        * @description Logic String
        */
        String ls;
        
        /**
        * @description Boolean Value
        */
        Boolean bv;
        
        /**
         * @description Constructor
         * @param logicString String of criteria logic e.g, 1 OR 2 AND (3 AND 4)
         */
        public Boolean LogicString (String logicString) {
            
            system.debug('Entering LogicString: ' +logicString);
            ls = logicString;
            bv = false;

            formatLogic();
            bv = evaluateLogic(ls);
            system.debug('Exiting LogicString: ' +bv);
            return bv;
        }
        
        /**
         * @description Replace logic word with proper logic syntax, e.g, & = &&, AND = &&.
         * @return void
         */
        private void formatLogic() {

            ls = ls.toUpperCase();
            ls = ls.deleteWhitespace();
            ls = ls.replace('&', '&&');
            ls = ls.replace('AND', '&&');
            ls = ls.replace('|', '||');
            ls = ls.replace('OR', '||');
            ls = ls.replace('NOT', '!');
        }                
        
        /**
         * @description Evaluate the outcome of a logic string, e.g, false && true = false.
         * @param x String of criteria logic
         * @return Boolean
         */
        private Boolean evaluateLogic(String x) {            
            x = simplifyLogic(x);
            if (!isSimpleExpressionLogic(x)) {
                if (x.contains('&&'))
                    return logicAND(x.substringBefore('&&'), x.substringAfter('&&'));
                if (x.contains('||'))
                    return logicOR(x.substringBefore('||'), x.substringAfter('||'));
                if (x.startsWith('!')) 
                    return !evaluateLogic(x.substring(1));
                return false;
            }
            return Boolean.valueOf(x);
        }
        
        /**
         * @description Split or chunk complex logic into simple logic. E.g, (true && false) || (true && true) = 1. true && false, 2. true && true.
         * @String x String of criteria logic
         * @return String
         */
        private String simplifyLogic(String x) {

            x = x.trim();
            while (x.contains('(')) {
                String subLogic = x.substringAfterLast('(').substringBefore(')');
                x = x.replace('(' + subLogic + ')', String.valueOf(evaluateLogic(subLogic)));
            }
            return x;
        }
        
        /**
         * @description Validate if string of logic is only true/false.
         * @param x String of logic
         * @return Boolean
         */
        private Boolean isSimpleExpressionLogic(String x) {

            if ((x == 'true') || (x == 'false'))
                return true;
            else
                return false;
        }
        
        /**
         * @description Evaluate outcome between x and y using AND condition.
         * @param x simple string of logic
         * @param y simple string of logic
         * @return Boolean
         */
        private Boolean logicAND(String x, String y) {
            return evaluateLogic(x) && evaluateLogic(y);
        }
        
        /**
         * @description Evaluate outcome between x and y using OR condition.
         * @param x simple string of logic
         * @param y simple string of logic
         * @return Boolean
         */
        private Boolean logicOR(String x, String y) {
            return evaluateLogic(x) || evaluateLogic(y);
        }
    }

    public static boolean isCodeMatches(String codesLogicStr, String code){

        String codeRegex = '\\b(' + code + '$)|(' + code + '\\))|(' + code + ' AND)|(' + code + ' and)|(' + code + ' OR)|(' + code + ' or)\\b';
        Pattern codePattern = Pattern.compile(codeRegex);
        Matcher codeMatcher = codePattern.matcher(codesLogicStr);

        if(codeMatcher.find()){
            return true;
        } else {
            return false;
        }
    }

    public static String replacePatternAll(String codesLogicStr, String code, String replacement){

        codesLogicStr = replacePatternOne(codesLogicStr, code, replacement);
        codesLogicStr = replacePatternTwo(codesLogicStr, code, replacement);
        codesLogicStr = replacePatternThree(codesLogicStr, code, replacement);
        codesLogicStr = replacePatternFour(codesLogicStr, code, replacement);

        return codesLogicStr;
    }

    public static String replacePatternOne(String codesLogicStr, String code, String replacement){
        String codeRegex = '\\b' + code + '$\\b';
        Pattern codePattern = Pattern.compile(codeRegex);
        Matcher codeMatcher = codePattern.matcher(codesLogicStr);

        if(codeMatcher.find()){
            codesLogicStr = codeMatcher.replaceAll(replacement);
        }

        return codesLogicStr;
    }

    public static String replacePatternTwo(String codesLogicStr, String code, String replacement){
        String codeRegex = '\\b' + code + '\\b\\)';
        Pattern codePattern = Pattern.compile(codeRegex);
        Matcher codeMatcher = codePattern.matcher(codesLogicStr);

        if(codeMatcher.find()){
            codesLogicStr = codeMatcher.replaceAll(replacement + ')');
        }

        return codesLogicStr;
    }

    public static String replacePatternThree(String codesLogicStr, String code, String replacement){
        String codeRegex = '\\b(' + code + ' AND)|(' + code + ' and)\\b';
        Pattern codePattern = Pattern.compile(codeRegex);
        Matcher codeMatcher = codePattern.matcher(codesLogicStr);

        if(codeMatcher.find()){
            codesLogicStr = codeMatcher.replaceAll(replacement + ' AND');
        }

        return codesLogicStr;
    }

    public static String replacePatternFour(String codesLogicStr, String code, String replacement){
        String codeRegex = '\\b(' + code + ' OR)|(' + code + ' or)\\b';
        Pattern codePattern = Pattern.compile(codeRegex);
        Matcher codeMatcher = codePattern.matcher(codesLogicStr);

        if(codeMatcher.find()){
            codesLogicStr = codeMatcher.replaceAll(replacement + ' OR');
        }

        return codesLogicStr;
    }

    // ===== END Of Logic Parser Methods =====
}