@isTest
public class QPRSearchCloneClassTest {
    
    private static Account getFullMemberAccountObject(){       
        Account FullMemberAccountObject = TestObjectCreator.createFullMemberAccount();
        FullMemberAccountObject.Member_Of__c = 'NZICA' ;
        FullMemberAccountObject.Financial_Category__c = 'Life' ;
        FullMemberAccountObject.FP_Specialisation__c = true ;
        FullMemberAccountObject.Registered_BAS_Agent__c = true ;
        FullMemberAccountObject.Reviewer_of_Second_Tier_Companies__c = true ;
        FullMemberAccountObject.FP_Subscriber__c = true ;
        FullMemberAccountObject.SMSF__c = true ;
        FullMemberAccountObject.Registered_Company_Auditor__c = true ;
        FullMemberAccountObject.SMSF_Auditor__c = true ;
        FullMemberAccountObject.Registered_Company_Liquidator__c = true ;
        FullMemberAccountObject.BV__c = true ;
        FullMemberAccountObject.Financial_Planner__c = true ;
        FullMemberAccountObject.Registered_Trustee_in_Bankruptcy__c = true ;
        FullMemberAccountObject.Registered_Tax_Agent__c = true ;
        FullMemberAccountObject.Licensee_of_AFSL__c = true ;
        FullMemberAccountObject.Authorised_Representative_of_an_AFSL__c = true ;
        FullMemberAccountObject.CPP_Picklist_AU__c = 'Full';
        FullMemberAccountObject.Status__c = 'Active';
        FullMemberAccountObject.PersonOtherCity = 'Sydney';
        FullMemberAccountObject.PersonOtherPostalCode = '1010';
        FullMemberAccountObject.PersonEmail = 'jai.chaturvedi6327t32@davanti.co.nz';
        // FullMemberAccountObject.Last_Review_Date_member__c = System.today() - 400 ;
        return FullMemberAccountObject;
    }
    
    static testMethod void searchReportTestMethod(){
        Account businessAccountObject = TestObjectCreator.createBusinessAccount();
        businessAccountObject.BillingStreet = '8 Nelson Street';
        businessAccountObject.Member_Of__c = 'NZICA' ;
        businessAccountObject.Status__c = 'Active' ;
        businessAccountObject.Affiliated_Branch_Country__c = 'Australia' ; 
        businessAccountObject.Affiliated_Branch__c = 'New South Wales' ; 
        businessAccountObject.BillingCity = 'Australia' ;
        businessAccountObject.BillingPostalCode = '1010';
        businessAccountObject.CPP_Picklist_AU__c = 'Full' ; 
        businessAccountObject.Affiliated_Branch_Country__c = 'Australia' ;
        businessAccountObject.Last_Review_Date_1__c = System.today() - 400 ;
        businessAccountObject.Practice_Information_Questionnaire_Date__c = System.today() - 400 ;
        businessAccountObject.Status__c = 'Active';
        businessAccountObject.Has_open_review__c = false ;
        businessAccountObject.Has_open_survey__c = false;
        
        insert businessAccountObject;
        
        Account FullMemberAccountObject = getFullMemberAccountObject();
        Test.startTest() ;
        insert FullMemberAccountObject; // Future method
        //insert userRecord ;
        Test.stopTest() ;
        
        Employment_History__c EmploymentHistoryObject = TestObjectCreator.createEmploymentHistory();
        EmploymentHistoryObject.Job_Title__c = 'SFDC Dev Opps Team' ;
        EmploymentHistoryObject.Employee_Start_Date__c = System.today() - 20 ;
        EmploymentHistoryObject.Employer__c = businessAccountObject.Id;
        EmploymentHistoryObject.Member__c = FullMemberAccountObject.Id;
        EmploymentHistoryObject.Status__c = 'Current';
        EmploymentHistoryObject.Is_CPP_Provided__c = true;
        insert EmploymentHistoryObject;
        
        
        // Check Results
        //User FullMemberContactId = [Select id FROM User WHERE Id =: userRecord.Id];
        
        //system.runAs(FullMemberContactId) {
        PageReference pageRef = Page.QPRSearchPage;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdAcc = new ApexPages.StandardController(businessAccountObject);
        QPRSearchCloneClass controller = new QPRSearchCloneClass(stdAcc);
        controller.selectedCountry = 'Australia';
        controller.FPpecialisation = true ;
        controller.RegisteredBASAgent = true ;
        controller.ReviewerofSecondTier = true ;
        controller.FPSubscriber = true ;
        controller.SMSFSpecialisation = true ;
        controller.RegisteredCompanyAuditorAustralia = true ;
        controller.SMSFAuditorAustralia = true ;
        controller.RegisteredCompanyLiquidator = true ;
        controller.BVSpecialisation = true ;
        controller.FinancialPlanner = true ;
        controller.RegisteredTrusteeinBankruptcy = true ;
        controller.RegisteredTaxAgent = true ;
        controller.LicenseeofAFSL = true ;
        controller.AuthorisedRepresentativeofAFSL = true ;
        controller.numberofcppholders = '1';
        controller.mailingPostalCode = '1010' ;
        controller.mailingCity = 'Australia';
        controller.lastReviewedDate = '1';
        
        List<SelectOption> selectedaffiliatedBranch = new List<SelectOption>();
        selectedaffiliatedBranch.add(new SelectOption('New South Wales','New South Wales'));
        controller.selectedaffiliatedBranch = selectedaffiliatedBranch;
        
        Account acc = [SELECT CPP_Employment_History_Count__c,BillingPostalCode ,BillingCity, Affiliated_Branch_Country__c
                       FROM Account WHERE Id =:businessAccountObject.id ];
        system.debug('****acc****'+acc);
        controller.testObjectString = '{"New Zealand":["Auckland","Canterbury","Central Bay of Plenty","Coastal Bay of Plenty","Gisborne - East Coast","Hawkes Bay","Manawatu","Marlborough","Nelson","Northland","Otago","Southland","Taranaki","Waikato","Wairarapa","Wellington","Westland","Whanganui"],"Australia":["Adelaide","Brisbane","Canberra","Perth","Melbourne","Sydney","Australian Capital Territory","Northern Territory","New South Wales","Queensland","South Australia","Tasmania","Victoria","Western Australia"],"Overseas":["UK","Other"]}' ;
        
        
        controller.initLoadMethod();
        controller.updateAusPanel();
        controller.fetchPracticeData();
        QPRSearchCloneClass.getRecords(acc.Id);
        
        //Covering ReviewTriggerHandler part also. Avoiding the extra class for small patch of code coverage.
        List<Review__c> rList = new List<Review__c>();
        Review__c r = new Review__c();
        r.Practice_Name__c = acc.Id;
        rList.add(r) ;
        Review__c r2 = new Review__c();
        r2.Practice_Name__c = FullMemberAccountObject.Id;
        r2.Person_Account__c = FullMemberAccountObject.Id;
        rList.add(r2) ;
        insert rList;     
        
    }
    
    static testMethod void searchReportTestMethod2(){
        Account FullMemberAccountObject = getFullMemberAccountObject();
        Test.startTest() ;
        FullMemberAccountObject.PersonEmail = 'jai.chaturvedimacmamc6265365327624347625637632@davanti.co.nz';
        insert FullMemberAccountObject; // Future method
        //insert userRecord ;
        //Test.stopTest() ;removed for QPR Prod deployment 3 Dec 2016
        
        Account businessAccountObject = TestObjectCreator.createBusinessAccount();
        businessAccountObject.BillingStreet = '8 Nelson Street';
        businessAccountObject.Member_Of__c = 'NZICA' ;
        businessAccountObject.Status__c = 'Active' ;
        businessAccountObject.Affiliated_Branch_Country__c = 'Australia' ; 
        businessAccountObject.Affiliated_Branch__c = 'New South Wales' ; 
        businessAccountObject.BillingCity = 'Australia' ;
        businessAccountObject.BillingPostalCode = '1010';
        businessAccountObject.CPP_Picklist_AU__c = 'Full' ; 
        businessAccountObject.Affiliated_Branch_Country__c = 'Australia' ;
        businessAccountObject.APE_Approval_Date__c = System.today() - 400 ;
        //businessAccountObject.Practice_Information_Questionnaire_Date__c = System.today() - 400 ;
        businessAccountObject.Status__c = 'Active';
        businessAccountObject.Has_open_review__c = false ;
        businessAccountObject.Has_open_survey__c = false;
        businessAccountObject.Member_ID__c = '527638737';
        businessAccountObject.Review_Contact__c = FullMemberAccountObject.Id;
        insert businessAccountObject;
        
        
        
        List<Employment_History__c> empList = new List<Employment_History__c>();
        /*Employment_History__c EmploymentHistoryObject = TestObjectCreator.createEmploymentHistory();
EmploymentHistoryObject.Job_Title__c = 'SFDC Dev Opps Team' ;
EmploymentHistoryObject.Employee_Start_Date__c = System.today() - 20 ;
EmploymentHistoryObject.Employer__c = businessAccountObject.Id;
EmploymentHistoryObject.Member__c = FullMemberAccountObject.Id;
EmploymentHistoryObject.Status__c = 'Current';
EmploymentHistoryObject.Is_CPP_Provided__c = true;
empList.add(EmploymentHistoryObject);*/
        
        Employment_History__c EmploymentHistoryObject2 = TestObjectCreator.createEmploymentHistory();
        EmploymentHistoryObject2.Job_Title__c = 'SFDC Dev Opps Team' ;
        EmploymentHistoryObject2.Employee_Start_Date__c = System.today() - 20 ;
        EmploymentHistoryObject2.Employee_End_Date__c = System.today() + 20 ;
        EmploymentHistoryObject2.Employer__c = businessAccountObject.Id;
        EmploymentHistoryObject2.Member__c = FullMemberAccountObject.Id;
        EmploymentHistoryObject2.Status__c = 'Closed';
        EmploymentHistoryObject2.Is_CPP_Provided__c = true;
        empList.add(EmploymentHistoryObject2);
        
        insert empList;
        
        
        // Check Results
        //User FullMemberContactId = [Select id FROM User WHERE Id =: userRecord.Id];
        
        //system.runAs(FullMemberContactId) {
        PageReference pageRef = Page.QPRSearchPage;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdAcc = new ApexPages.StandardController(businessAccountObject);
        QPRSearchCloneClass controller = new QPRSearchCloneClass(stdAcc);
        controller.selectedCountry = 'Australia';
        controller.FPpecialisation = true ;
        controller.RegisteredBASAgent = true ;
        controller.ReviewerofSecondTier = true ;
        controller.FPSubscriber = true ;
        controller.SMSFSpecialisation = true ;
        controller.RegisteredCompanyAuditorAustralia = true ;
        controller.SMSFAuditorAustralia = true ;
        controller.RegisteredCompanyLiquidator = true ;
        controller.BVSpecialisation = true ;
        controller.FinancialPlanner = true ;
        controller.RegisteredTrusteeinBankruptcy = true ;
        controller.RegisteredTaxAgent = true ;
        controller.LicenseeofAFSL = true ;
        controller.AuthorisedRepresentativeofAFSL = true ;
        //controller.numberofcppholders = '1';
        controller.mailingPostalCode = '1010' ;
        controller.mailingCity = 'Sydney';
        controller.lastReviewedDate = '1';
        
        List<SelectOption> selectedaffiliatedBranch = new List<SelectOption>();
        selectedaffiliatedBranch.add(new SelectOption('New South Wales','New South Wales'));
        controller.selectedaffiliatedBranch = selectedaffiliatedBranch;
        
        Account acc = [SELECT CPP_Employment_History_Count__c,BillingPostalCode ,BillingCity, Affiliated_Branch_Country__c, Review_Contact__c,PersonEmail,
                       Review_Contact__r.PersonEmail, Member_ID__c, Has_open_survey__c,IsPersonAccount
                       FROM Account 
                       WHERE Id =:businessAccountObject.id ];
        system.debug('****acc****'+acc);
        controller.testObjectString = '{"New Zealand":["Auckland","Canterbury","Central Bay of Plenty","Coastal Bay of Plenty","Gisborne - East Coast","Hawkes Bay","Manawatu","Marlborough","Nelson","Northland","Otago","Southland","Taranaki","Waikato","Wairarapa","Wellington","Westland","Whanganui"],"Australia":["Adelaide","Brisbane","Canberra","Perth","Melbourne","Sydney","Australian Capital Territory","Northern Territory","New South Wales","Queensland","South Australia","Tasmania","Victoria","Western Australia"],"Overseas":["UK","Other"]}' ;
        
        
        controller.initLoadMethod();
        controller.fetchPracticeData();
        controller.resetpage();
        controller.selectedPracticeRecord = businessAccountObject.Id ;
        //Test.startTest(); removed for QPR Prod deployment 3 Dec 2016
        controller.sendSurvey();
        Test.stopTest();
        // }
        
    }
    
    static testMethod void searchReportTestMethodNZ(){
        Account businessAccountObject = TestObjectCreator.createBusinessAccount();
        businessAccountObject.BillingStreet = '8 Nelson Street';
        businessAccountObject.Member_Of__c = 'NZICA' ;
        businessAccountObject.Status__c = 'Active' ;
        businessAccountObject.Affiliated_Branch_Country__c = 'New Zealand' ; 
        businessAccountObject.Affiliated_Branch__c = 'Auckland' ; 
        businessAccountObject.BillingCity = 'Auckland' ;
        businessAccountObject.BillingPostalCode = '1010';
        businessAccountObject.CPP_Picklist_AU__c = 'Full' ; 
        businessAccountObject.Last_Review_Date_1__c = System.today() - 400 ;
        businessAccountObject.Practice_Information_Questionnaire_Date__c = System.today() - 400 ;
        businessAccountObject.Status__c = 'Active';
        businessAccountObject.Has_open_review__c = false ;
        businessAccountObject.Has_open_survey__c = false;
        businessAccountObject.APE__c = true;
        
        insert businessAccountObject;
        
        Account FullMemberAccountObject = getFullMemberAccountObject();
        Test.startTest() ;
        insert FullMemberAccountObject; // Future method
        //insert userRecord ;
        Test.stopTest() ;
        
        Employment_History__c EmploymentHistoryObject = TestObjectCreator.createEmploymentHistory();
        EmploymentHistoryObject.Job_Title__c = 'SFDC Dev Opps Team New Zealand' ;
        EmploymentHistoryObject.Employee_Start_Date__c = System.today() - 20 ;
        EmploymentHistoryObject.Employer__c = businessAccountObject.Id;
        EmploymentHistoryObject.Member__c = FullMemberAccountObject.Id;
        EmploymentHistoryObject.Status__c = 'Current';
        EmploymentHistoryObject.Is_CPP_Provided__c = false;
        insert EmploymentHistoryObject;
        
        
        // Check Results
        //User FullMemberContactId = [Select id FROM User WHERE Id =: userRecord.Id];
        
        //system.runAs(FullMemberContactId) {
        PageReference pageRef = Page.QPRSearchPage;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdAcc = new ApexPages.StandardController(businessAccountObject);
        QPRSearchCloneClass controller = new QPRSearchCloneClass(stdAcc);
        controller.selectedCountry = 'New Zealand';
        
        controller.numberofcppholders = '1';
        controller.mailingPostalCode = '1010;1212' ;
        controller.mailingCity = 'Auckland';
        controller.lastReviewedDate = '1';
        controller.selectedAplhabet = 'T';
        
        List<SelectOption> selectedaffiliatedBranch = new List<SelectOption>();
        selectedaffiliatedBranch.add(new SelectOption('Auckland','Manawatu'));
        controller.selectedaffiliatedBranch = selectedaffiliatedBranch;
        
        Account acc = [SELECT CPP_Employment_History_Count__c,BillingPostalCode ,BillingCity, Affiliated_Branch_Country__c
                       FROM Account WHERE Id =:businessAccountObject.id ];
        system.debug('****acc****'+acc);
        controller.testObjectString = '{"New Zealand":["Auckland","Canterbury","Central Bay of Plenty","Coastal Bay of Plenty","Gisborne - East Coast","Hawkes Bay","Manawatu","Marlborough","Nelson","Northland","Otago","Southland","Taranaki","Waikato","Wairarapa","Wellington","Westland","Whanganui"],"Australia":["Adelaide","Brisbane","Canberra","Perth","Melbourne","Sydney","Australian Capital Territory","Northern Territory","New South Wales","Queensland","South Australia","Tasmania","Victoria","Western Australia"],"Overseas":["UK","Other"]}' ;
        
        
        controller.initLoadMethod();
        controller.updateAusPanel();
        controller.fetchPracticeData();
        //QPRSearchCloneClass.getRecords(acc.Id);
        
        //Covering ReviewTriggerHandler part also. Avoiding the extra class for small patch of code coverage.
        List<Review__c> rList = new List<Review__c>();
        Review__c r = new Review__c();
        r.Practice_Name__c = acc.Id;
        rList.add(r) ;
        Review__c r2 = new Review__c();
        r2.Practice_Name__c = FullMemberAccountObject.Id;
        r2.Person_Account__c = FullMemberAccountObject.Id;
        rList.add(r2) ;
        insert rList;     
        
    }
    
}