/**
    Developer: WDCi (kh)
    Development Date:24/11/2016
    Task: Test class for AAS_BorderlineRemarkController
**/
@isTest(seeAllData=false)
private class AAS_Test_BorderlineRemarkController {

    final static String contextLongName = 'AAS_Test_BorderlineRemarkController_Test';
    final static String contextShortName = 'abr';
    
    public static AAS_Course_Assessment__c courseAss;
    public static AAS_Student_Assessment__c sa;
    public static AAS_Student_Assessment_Section__c sas;
    
    public static List<AAS_Student_Assessment_Section_Item__c> sasiList;
    
    public static void initial(Boolean isCapstone){
        
        Luana_DataPrep_Test dataPrepUtil = new Luana_DataPrep_Test();
        AAS_DataPrep_Test aasdataPrepUtil = new AAS_DataPrep_Test();
        
        insert aasdataPrepUtil.defauleCustomSetting();
        
        Map<String, Id> recordTypeMap = aasdataPrepUtil.getRecordTypeMap('Exam');
        
        //Training Org
        LuanaSMS__Training_Organisation__c trainOrg = dataPrepUtil.createNewTraningOrg(contextLongName, contextLongName, contextShortName, 'address line 1', 'address loc 1', '5000');
        insert trainOrg;
        
        //Program
        LuanaSMS__Program__c prog = dataPrepUtil.createNewProgram(contextShortName, contextLongName, 'Confirmed', 'Graduate diploma', 'Nationally accredited qualification specified in a national training package');
        insert prog;
        
         //Product
        List<Product2> prodList = new List<Product2>();
        prodList.add(dataPrepUtil.createNewProduct('TEST_AU_' + contextShortName, 'AU0001'));
        prodList.add(dataPrepUtil.createNewProduct('TEST_NZ_' + contextShortName, 'NZ0001'));
        prodList.add(dataPrepUtil.createNewProduct('TEST_INT_' + contextShortName, 'INT0001'));
        insert prodList;
        
        //Program Offerings
        LuanaSMS__Program_Offering__c poAM = dataPrepUtil.createNewProgOffering('PO_AM_' + contextShortName, dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), prog.Id, trainOrg.Id, prodList[0].Id, prodList[1].Id, prodList[2].Id, 1, 1);
        if(isCapstone){
            poAM.IsCapstone__c = true;
        }
        insert poAM;
        System.debug('***poAM: ' + poAM);
        
        //Course
        LuanaSMS__Course__c courseAM;
        if(isCapstone){
            courseAM = dataPrepUtil.createNewCourse('CAP216', poAM.Id, dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        }else{
            courseAM = dataPrepUtil.createNewCourse('AAA216', poAM.Id, dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        }
        courseAM.LuanaSMS__Allow_Online_Enrolment__c = true;
        insert courseAM;
        System.debug('***courseAM1: ' + courseAM);
        System.debug('***courseAM2: ' + courseAM.LuanaSMS__Program_Offering__r.IsCapstone__c);
        
        //CA
        courseAss = aasdataPrepUtil.createCourseAssessment(courseAM.Id, 'CA_' + contextShortName);
        courseAss.AAS_Module_Passing_Mark__c = 50;
        
        insert courseAss;
        System.debug('***courseAss1: ' + courseAss.AAS_Course__r.LuanaSMS__Program_Offering__r.IsCapstone__c);
        
        //ASS
        AAS_Assessment_Schema_Section__c ass = aasdataPrepUtil.createAsessmentSchemaSection(courseAss.Id, 'ASS_' + contextShortName, recordTypeMap.get('AAS_Assessment_Schema_Section__c:Exam_Assessment'));
        ass.AAS_Passing_Mark__c = 40;
        ass.AAS_Cohort_Adjustment__c = 2;
        insert ass;
        
        List<AAS_Assessment_Schema_Section_Item__c> assiList = new List<AAS_Assessment_Schema_Section_Item__c>();
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA001 Q #1'));
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA001 Q #2'));
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA001 Q #3'));
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA001 Q #4'));
        insert assiList;
        
        //SA
        sa = aasdataPrepUtil.createStudentAssessment(courseAss.Id);
        insert sa;
        
        //SAS
        sas = aasdataPrepUtil.createStudentAssessmentSection(sa.Id, recordTypeMap.get('AAS_Student_Assessment_Section__c:Exam_Assessment_SAS'));
        insert sas;
        
        //SASI
        sasiList = new List<AAS_Student_Assessment_Section_Item__c>();
        for(AAS_Assessment_Schema_Section_Item__c assi: assiList){
            AAS_Student_Assessment_Section_Item__c sasi = aasdataPrepUtil.createStudentAssessmentSectionItem(sas.Id, recordTypeMap.get('AAS_Student_Assessment_Section_Item__c:Exam_Assessment_SASI'), assi.Id);
            sasi.AAS_Raw_Mark__c = 9;
            sasiList.add(sasi);
        }
        insert sasiList;
          
    }
    
   public static testMethod void test_BorderlineMain() { 
    
        initial(false);
        
        Test.startTest();
            
            
            //before Cohort adjustment
            PageReference pageRef1 = Page.AAS_BorderlineRemark;
            pageRef1.getParameters().put('id', String.valueOf(courseAss.Id));
            pageRef1.getParameters().put('type', String.valueOf('Exam_Assessment'));
            Test.setCurrentPage(pageRef1);
            
            ApexPages.StandardController sc1 = new ApexPages.StandardController(courseAss);
            AAS_BorderlineRemarkController borderlineRemark1 = new AAS_BorderlineRemarkController(sc1);
            
            
            //after cohort adjustment
            courseAss.AAS_Cohort_Adjustment_Exam__c = true;
            update courseAss;
            
            PageReference pageRef2 = Page.AAS_BorderlineRemark;
            pageRef2.getParameters().put('id', String.valueOf(courseAss.Id));
            pageRef2.getParameters().put('type', String.valueOf('Exam_Assessment'));
            Test.setCurrentPage(pageRef2);
            
            ApexPages.StandardController sc2 = new ApexPages.StandardController(courseAss);
            AAS_BorderlineRemarkController borderlineRemark2 = new AAS_BorderlineRemarkController(sc2);
            borderlineRemark2.doNext();
            borderlineRemark2.doRemark();
            
            //Test one of the assi has exceed the full mark
            List<AAS_Student_Assessment_Section_Item__c> updateSASIList = new List<AAS_Student_Assessment_Section_Item__c>();
            for(AAS_Student_Assessment_Section_Item__c sasi: [Select Id from AAS_Student_Assessment_Section_Item__c Where AAS_Student_Assessment_Section__c =: sas.Id limit 1]){
                sasi.AAS_Raw_Mark__c = 21;
                updateSASIList.add(sasi);
            }
            update updateSASIList;
            
            PageReference pageRef3 = Page.AAS_BorderlineRemark;
            pageRef3.getParameters().put('id', String.valueOf(courseAss.Id));
            pageRef3.getParameters().put('type', String.valueOf('Exam_Assessment'));
            Test.setCurrentPage(pageRef3);
            
            ApexPages.StandardController sc3 = new ApexPages.StandardController(courseAss);
            AAS_BorderlineRemarkController borderlineRemark3 = new AAS_BorderlineRemarkController(sc3);
            
            borderlineRemark3.doNext();
            borderlineRemark3.doRemark();
            
        Test.stopTest();
    }

   
    public static testMethod void test_Borderline_isCapstone() { 
    
        initial(true);
        Test.startTest();
            /*
                before Cohort adjustment
            */
            PageReference pageRef1 = Page.AAS_BorderlineRemark;
            pageRef1.getParameters().put('id', String.valueOf(courseAss.Id));
            pageRef1.getParameters().put('type', String.valueOf('Exam_Assessment'));
            Test.setCurrentPage(pageRef1);
            
            ApexPages.StandardController sc1 = new ApexPages.StandardController(courseAss);
            AAS_BorderlineRemarkController borderlineRemark1 = new AAS_BorderlineRemarkController(sc1);
            
            /*
                after cohort adjustment
            */
            
            courseAss.AAS_Cohort_Adjustment_Exam__c = true;
            update courseAss;
            
            System.debug('****:courseAss2: ' + courseAss.AAS_Course__r.LuanaSMS__Program_Offering__r.IsCapstone__c);
            
            PageReference pageRef2 = Page.AAS_BorderlineRemark;
            pageRef2.getParameters().put('id', String.valueOf(courseAss.Id));
            pageRef2.getParameters().put('type', String.valueOf('Exam_Assessment'));
            Test.setCurrentPage(pageRef2);
            
            ApexPages.StandardController sc2 = new ApexPages.StandardController(courseAss);
            AAS_BorderlineRemarkController borderlineRemark2 = new AAS_BorderlineRemarkController(sc2);
            borderlineRemark2.doNext();
            borderlineRemark2.doRemark();
            
            borderlineRemark2.getRecordTypeOptions();
            borderlineRemark2.doCancel();
            
        Test.stopTest();
    }
    
    public static testMethod void test_Borderline_SuppExam_Main() { 
    
        initial(false);
        
        Test.startTest();
            
            
            //before Cohort adjustment
            PageReference pageRef1 = Page.AAS_BorderlineRemark;
            pageRef1.getParameters().put('id', String.valueOf(courseAss.Id));
            pageRef1.getParameters().put('type', String.valueOf('Supp_Exam_Assessment'));
            Test.setCurrentPage(pageRef1);
            
            ApexPages.StandardController sc1 = new ApexPages.StandardController(courseAss);
            AAS_BorderlineRemarkController borderlineRemark1 = new AAS_BorderlineRemarkController(sc1);
            
            
            //after cohort adjustment
            courseAss.AAS_Cohort_Adjustment_Exam__c = true;
            update courseAss;
            
            PageReference pageRef2 = Page.AAS_BorderlineRemark;
            pageRef2.getParameters().put('id', String.valueOf(courseAss.Id));
            pageRef2.getParameters().put('type', String.valueOf('Supp_Exam_Assessment'));
            Test.setCurrentPage(pageRef2);
            
            ApexPages.StandardController sc2 = new ApexPages.StandardController(courseAss);
            AAS_BorderlineRemarkController borderlineRemark2 = new AAS_BorderlineRemarkController(sc2);
            borderlineRemark2.doNext();
            borderlineRemark2.doRemark();
            
        Test.stopTest();
    }
}