@isTest
public class QuestionnaireListControllerTest {
    
    static testMethod void fetchQPRQuestionnaireTestMethod(){
        QPR_Questionnaire__c qpr = new QPR_Questionnaire__c(Status__c = 'Draft', Practice_ID__c = '457627');       
        insert qpr;
        List<QPR_Questionnaire__c> qprList = QuestionnaireListController.fetchQPRQuestionnaire('Draft');
        system.assertEquals(qprList[0].Status__c, 'Draft') ;
         system.assertEquals(qprList.size(), 1) ;
    }
}