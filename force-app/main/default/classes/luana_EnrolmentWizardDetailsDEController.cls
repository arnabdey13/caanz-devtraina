/**
    Developer: WDCi (Lean)
    Development Date: 05/04/2016
    Task #: Enrollment wizard - Controller for Details default
**/

public without sharing class luana_EnrolmentWizardDetailsDEController extends luana_EnrolmentWizardObject{
	
	luana_EnrolmentWizardController stdController;
    
    public luana_EnrolmentWizardDetailsDEController(luana_EnrolmentWizardController stdController){
        this.stdController = stdController;
		
    }
    
    public luana_EnrolmentWizardDetailsDEController(){
		
    }
    
    public PageReference detailsDefaultNext(){
    	
    	PageReference pageRef = Page.luana_EnrolmentWizardSummary;
    	stdController.skipValidation = true;
    	
        return pageRef;
    }
    
    public PageReference detailsDefaultBack(){
        
		PageReference pageRef = Page.luana_EnrolmentWizardSubject;
    	stdController.skipValidation = true;
    	
        return pageRef;
    }
    
}