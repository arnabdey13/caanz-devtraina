/**
 THIS IS A REDUNDANT CLASS - SUPERSEDED BY DeclarationsWizardController_Test
 * 
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class DeclarationWizardController_Test {
/* THIS IS A REDUNDANT CLASS - SUPERSEDED BY DeclarationsWizardController_Test
	private static Account FullMemberAccountObjectInDB; // Person Account - Full Member
	private static Account MemberAccountObjectInDB;// Person Account - Member
	private static Id FullMemberContactIdInDB;
	private static Id MemberContactIdInDB;
	//private static User PersonAccountPortalUserObjectInDB;
	private static Declaration__c DeclarationObjectInDB;
	static{
		insertFullMemberAccountObject();
		insertMemberAccountObject();
		insertDeclarationObject();
	}
	private static void insertFullMemberAccountObject(){
		FullMemberAccountObjectInDB = TestObjectCreator.createFullMemberAccount();
		//## Required Relationships
		//## Additional fields and relationships / Updated fields
		FullMemberAccountObjectInDB.Membership_Class__c = 'Full';
		system.debug(FullMemberAccountObjectInDB);
		insert FullMemberAccountObjectInDB;
		FullMemberContactIdInDB = [Select id from Contact where AccountId=:FullMemberAccountObjectInDB.Id].Id;
	}
	private static void insertMemberAccountObject(){
		MemberAccountObjectInDB = TestObjectCreator.createFullMemberAccount();
		//## Required Relationships
		//## Additional fields and relationships / Updated fields
		system.debug(MemberAccountObjectInDB);
		insert MemberAccountObjectInDB;
		MemberContactIdInDB = [Select id from Contact where AccountId=:MemberAccountObjectInDB.Id].Id;
	}
	private static void insertDeclarationObject(){
		DeclarationObjectInDB = TestObjectCreator.createDeclaration();
		//## Required Relationships
		DeclarationObjectInDB.Member_Name__c = FullMemberAccountObjectInDB.Id;
		//## Additional fields and relationships / Updated fields
		system.debug(DeclarationObjectInDB);
		insert DeclarationObjectInDB;
	}
	static DeclarationWizardController getDeclarationWizardController(Declaration__c Declaration, PageReference PageRef){
		ApexPages.StandardController Ctrl = new ApexPages.StandardController( Declaration );
		Test.setCurrentPage( pageRef );
		return new DeclarationWizardController( Ctrl );
	}
	private static User getFullPersonAccountPortalUserObjectInDB(){
        FullMemberAccountObjectInDB = TestObjectCreator.createFullMemberAccount();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
		Test.startTest();
        insert FullMemberAccountObjectInDB; // Future method
		Test.stopTest(); // Future method needs to run to create the user.
		
		List<User> PersonAccountPortalUserObject_List = [Select Name from User 
			where AccountId=:FullMemberAccountObjectInDB.Id];
		System.assertEquals(1, PersonAccountPortalUserObject_List.size(), 'PersonAccountPortalUserObject_List.size' );
		return PersonAccountPortalUserObject_List[0];
	}
	private static User getPersonAccountPortalUserObjectInDB(){
		Test.startTest();
		Test.stopTest(); // Future method needs to run to create the user.
		
		List<User> PersonAccountPortalUserObject_List = [Select Name from User 
			where AccountId=:MemberAccountObjectInDB.Id];
		System.assertEquals(1, PersonAccountPortalUserObject_List.size(), 'PersonAccountPortalUserObject_List.size' );
		return PersonAccountPortalUserObject_List[0];
	}
	
	//******************************************************************************************
	//                             TestMethods
	//******************************************************************************************
	//
	static testMethod void DeclarationWizardController_testFullMembershipDeclarationWizardPage1SaveAndContinue() {
		//User FullPersonAccountPortalUserObjectInDB = getFullPersonAccountPortalUserObjectInDB();
        Account FullMemberAccountObject = TestObjectCreator.createPersonAccount();
        Test.startTest();
        insert FullMemberAccountObject; // Future method
        Test.stopTest();
         Id FullMemberContactId = [Select id FROM Contact WHERE AccountId =: FullMemberAccountObject.Id].Id;
        User FullPersonAccountPortalUserObjectInDB = [Select FederationIdentifier FROM User WHERE contactId=:FullMemberContactId];
		System.runAs(FullPersonAccountPortalUserObjectInDB) {
			DeclarationWizardController CtrlExt = getDeclarationWizardController(
				new Declaration__c(), Page.Declarationwizardpage1
			);
			
			PageReference PR = CtrlExt.saveAndContinuePage1(); // Save and Continue button
			System.assertNotEquals( null, PR );
			System.assert( PR.getUrl().contains('declarationwizardpage2'), PR.getUrl() );
		}
	}
	static testMethod void DeclarationWizardController_testFullMembershipDeclarationWizardPage1SaveAndExit() {
		//User FullPersonAccountPortalUserObjectInDB = getFullPersonAccountPortalUserObjectInDB();
        Account FullMemberAccountObject = TestObjectCreator.createPersonAccount();
        Test.startTest();
        FullMemberAccountObject.PersonEmail = 'PersonAccoun89t' + TestObjectCreator.getRandomNumber(89) +'@gmail.com';
        insert FullMemberAccountObject; // Future method
        Test.stopTest();
         Id FullMemberContactId = [Select id FROM Contact WHERE AccountId =: FullMemberAccountObject.Id].Id;
        User FullPersonAccountPortalUserObjectInDB = [Select FederationIdentifier FROM User WHERE contactId=:FullMemberContactId];
		System.runAs(FullPersonAccountPortalUserObjectInDB) {
			Declaration__c d = new Declaration__c();
			d.Member_Name__c = FullMemberAccountObjectInDB.Id;
			d.Declaration_Status__c = 'In Progress';
			d.Declaration_Completed__c = false;
			DeclarationWizardController CtrlExt = getDeclarationWizardController(
				d, Page.Declarationwizardpage1
			);
			
			PageReference PR = CtrlExt.saveAndExitPage1(); // Save and Continue button
		}
	}

	static testMethod void DeclarationWizardController_testFullMembershipDeclarationWizardSavePage2() {
		//User FullPersonAccountPortalUserObjectInDB = getFullPersonAccountPortalUserObjectInDB();
        Account FullMemberAccountObject = TestObjectCreator.createFullMemberAccount();
        Test.startTest();
        FullMemberAccountObject.PersonEmail = 'PersonAccount' + TestObjectCreator.getRandomNumber(09) +'@gmail.com';
        insert FullMemberAccountObject; // Future method
        Test.stopTest();
         Id FullMemberContactId = [Select id FROM Contact WHERE AccountId =: FullMemberAccountObject.Id].Id;
        User FullPersonAccountPortalUserObjectInDB = [Select FederationIdentifier FROM User WHERE contactId=:FullMemberContactId];
		System.runAs(FullPersonAccountPortalUserObjectInDB) {
			//DeclarationObjectInDB.Declaration_Completed__c = true;
			//update DeclarationObjectInDB;
			//system.debug(DeclarationObjectInDB);
			DeclarationWizardController CtrlExt = getDeclarationWizardController(
				DeclarationObjectInDB, Page.Declarationwizardpage2
			);
			
			PageReference PR = CtrlExt.savePage2(); // Save and Continue button
			System.assertEquals( null, PR );
		}
	}
	
	static testMethod void DeclarationWizardController_testFullMembershipDeclarationWizardPage1New1SavePage1() {
		//User FullPersonAccountPortalUserObjectInDB = getFullPersonAccountPortalUserObjectInDB()
        Account FullMemberAccountObject = TestObjectCreator.createPersonAccount();
        Test.startTest();
        FullMemberAccountObject.PersonEmail = 'PersonAccount' + TestObjectCreator.getRandomNumber(85) +'@gmail.com';
        insert FullMemberAccountObject; // Future method
        Test.stopTest();
         Id FullMemberContactId = [Select id FROM Contact WHERE AccountId =: FullMemberAccountObject.Id].Id;
        User FullPersonAccountPortalUserObjectInDB = [Select FederationIdentifier FROM User WHERE contactId=:FullMemberContactId];
		System.runAs(FullPersonAccountPortalUserObjectInDB) {
			DeclarationWizardController CtrlExt = getDeclarationWizardController(new Declaration__c(),
				 Page.Declarationwizardpage1
			);			
			PageReference PR = CtrlExt.savePage1(); // Save and Continue button
			System.assertNotEquals( null, PR );
			System.assert( PR.getUrl().contains('declarationwizardpage2'), PR.getUrl() );
		}
	}
	static testMethod void DeclarationWizardController_testFullMembershipDeclarationWizardPage2Save() {
		//User FullPersonAccountPortalUserObjectInDB = TestObjectCreator.createPersonAccount() ; //getFullPersonAccountPortalUserObjectInDB()
        Account FullMemberAccountObject = TestObjectCreator.createPersonAccount();
        Test.startTest();
        FullMemberAccountObject.PersonEmail = 'PersonAccount649' + TestObjectCreator.getRandomNumber(465) +'@gmail.com';
        insert FullMemberAccountObject; // Future method
        Test.stopTest();
         Id FullMemberContactId = [Select id FROM Contact WHERE AccountId =: FullMemberAccountObject.Id].Id;
        User FullPersonAccountPortalUserObjectInDB = [Select FederationIdentifier FROM User WHERE contactId=:FullMemberContactId];
		System.runAs(FullPersonAccountPortalUserObjectInDB) {
			DeclarationWizardController CtrlExt = getDeclarationWizardController(
				DeclarationObjectInDB, Page.Declarationwizardpage2
			);
			PageReference PR = CtrlExt.savePage2(); // Save and Continue button
			System.assertEquals( null, PR );
			//System.assertEquals( '/'+ Schema.sObjectType.Application__c.getKeyPrefix() +'/o', PR.getUrl(), 'getUrl' );
		}
	}
	static testMethod void DeclarationWizardController_testFullMembershipDeclarationWizardbackToPage1() {
        //User FullPersonAccountPortalUserObjectInDB = TestObjectCreator.createPersonAccount() ; //getFullPersonAccountPortalUserObjectInDB()
        Account FullMemberAccountObject = TestObjectCreator.createPersonAccount();
        Test.startTest();
        FullMemberAccountObject.PersonEmail = 'PersonFullAccount' + TestObjectCreator.getRandomNumber(849) +'@gmail.com';
        insert FullMemberAccountObject; // Future method
        Test.stopTest();
         Id FullMemberContactId = [Select id FROM Contact WHERE AccountId =: FullMemberAccountObject.Id].Id;
        User FullPersonAccountPortalUserObjectInDB = [Select FederationIdentifier FROM User WHERE contactId=:FullMemberContactId];
		System.runAs(FullPersonAccountPortalUserObjectInDB) {
			DeclarationWizardController CtrlExt = getDeclarationWizardController(new Declaration__c(),
				 Page.Declarationwizardpage1
			);			
			PageReference PR = CtrlExt.backToPage1(); // Save and Continue button
			System.assertNotEquals( null, PR );
			System.assert( PR.getUrl().contains('declarationwizardpage1'), PR.getUrl() );
		}
	}

	static testMethod void DeclarationWizardController_testFullMembershipDeclarationIsNull() {
		//User FullPersonAccountPortalUserObjectInDB = TestObjectCreator.createPersonAccount() ; //getFullPersonAccountPortalUserObjectInDB()
        Account FullMemberAccountObject = TestObjectCreator.createPersonAccount();
        Test.startTest();
        FullMemberAccountObject.PersonEmail = 'PersonNullAcct5378738' + TestObjectCreator.getRandomNumber(18565469) +'@gmail.com';
        insert FullMemberAccountObject; // Future method
        Test.stopTest();
         Id FullMemberContactId = [Select id FROM Contact WHERE AccountId =: FullMemberAccountObject.Id].Id;
        User FullPersonAccountPortalUserObjectInDB = [Select FederationIdentifier FROM User WHERE contactId=:FullMemberContactId];
		System.runAs(FullPersonAccountPortalUserObjectInDB) {
			DeclarationWizardController CtrlExt = getDeclarationWizardController(new Declaration__c(),
				 Page.Declarationwizardpage1
			);			
			PageReference PR = CtrlExt.savePage2(); // Save and Continue button
			System.assertEquals( null, PR );
			//System.assert( PR.getUrl().contains('declarationwizardpage1'), PR.getUrl() );
		}
	}
*/
}