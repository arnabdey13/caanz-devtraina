global class PasswordResetBatch implements Database.Batchable<sObject> {
//  Type:       Batch Apex
//  Purpose:	Bulk reset of user passwords for Customer Community
//  Execute via Developer Console as follows:
//		String profileId = '00e90000001ZOnz';
//  	ID batchprocessid = Database.executeBatch(new PasswordResetBatch(profileId));
//  History:

    global final String query;
    
    global PasswordResetBatch(String profileId){
		query = 'SELECT Id, Username FROM User WHERE isActive = TRUE AND ProfileId = \'' + profileId + '\'';
	}

	global Database.QueryLocator start(Database.BatchableContext BC){
		return Database.getQueryLocator(query);
	}

    global void execute(Database.BatchableContext BC, list<User> usersToReset) {
		// Set EmailHeader option to send password resets
		Database.DMLOptions dlo = new Database.DMLOptions();
        dlo.EmailHeader.triggerUserEmail = true;

        // Reset password for this user - System.resetPassword(userId, sendUserEmail)
        for (User userToReset : usersToReset) {
            System.resetPassword(userToReset.Id, true);
            System.debug('password reset for ' + userToReset.Username);
        }
    }
        
	global void finish(Database.BatchableContext BC) {
        // Get the ID of the AsyncApexJob representing this batch job
        // from Database.BatchableContext.
        // Query the AsyncApexJob object to retrieve the current job's information.
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                          TotalJobItems, CreatedBy.Email
                          FROM AsyncApexJob WHERE Id =
                          :BC.getJobId()];
        // Send an email to the Apex job's submitter notifying of job completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
            mail.setToAddresses(toAddresses);
        mail.setSubject('Password Reset ' + a.Status);
        mail.setPlainTextBody
            ('The batch Apex job processed ' + a.TotalJobItems +
             ' batches with '+ a.NumberOfErrors + ' failures.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });    
    }
}