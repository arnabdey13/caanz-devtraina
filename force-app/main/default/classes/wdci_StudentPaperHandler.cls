/**
* @author WDCi-LKoh
* @date 11/06/2019
* @group Competency Automation
* @description Trigger Handler for FT_Student_Paper__c
* @change-history
* 
*/
public with sharing class wdci_StudentPaperHandler {
    
    public static void initFlow(List<FT_Student_Paper__c> newList, List<FT_Student_Paper__c> oldList, Map<Id, FT_Student_Paper__c> oldMap, boolean isInsert, boolean isUpdate, boolean isDelete, boolean isUndelete, boolean isBefore, boolean isAfter) {
        
        system.debug('Entering initFlow: ' +newList+ ' : ' +oldList+ ' : ' +oldMap+ ' : ' +isInsert+ ' : ' +isUpdate+ ' : ' +isDelete+ ' : ' +isUndelete+ ' : ' +isBefore+ ' : ' +isAfter);
        if (isAfter) {
            if (isInsert) {
                // evaluateCompetency(newList, isDelete);
            } else if (isDelete) {
                // evaluateCompetency(oldList, isDelete);
            }
        }
        system.debug('Exiting initFlow');
    }

    /** OBSOLETE, DONE BY wdci_ApplicationHandler
    public static void evaluateCompetency(List<FT_Student_Paper__c> studentPaperList, Boolean isDelete) {

        system.debug('Entering evaluateCompetency: ' +studentPaperList+ ' : ' +isDelete);
        Set<Id> allUniversityIDSet = new Set<Id>();
        Set<Id> educationHistoryIDSet = new Set<Id>();

        Set<Id> studentContactIDSet = new Set<Id>();
        if (isDelete) {
            for (FT_Student_Paper__c studentPaper : studentPaperList) {
                educationHistoryIDSet.add(studentPaper.FT_Education_History__c);
            }

            List<edu_Education_History__c> relatedEducationHistories = [SELECT Id, University_Degree__r.University__c, FP_Contact__c FROM edu_Education_History__c WHERE Id IN :educationHistoryIDSet];
            for (edu_Education_History__c educationHistory : relatedEducationHistories) {
                studentContactIDSet.add(educationHistory.FP_Contact__c);
                allUniversityIDSet.add(educationHistory.University_Degree__r.University__c);
            }

            List<FP_Gained_Competency_Area__c> potentialRelatedGainedCompetencyArea = [SELECT Id, FT_Pathway__c, FT_Pathway__r.FT_University__c, FT_Pathway_Information__c, (SELECT Id, FT_Subject_Code__c, FT_Subject_Name__c, FT_Student_Paper__r.FT_University_Subject__c FROM Paper_Gained_Competency__r) FROM FP_Gained_Competency_Area__c WHERE FP_Contact__c IN :studentContactIDSet AND FT_Pathway__c != null];
            
            // Build the University Subject Map
            Map<Id, List<FT_University_Subject__c>> uniSubjectMap = wdci_CompetencyUtil.mapUniSubject(allUniversityIDSet);

            // These Gained Competency Area need to be rechecked if they are still valid, otherwise they will need to be removed
            List<FP_Gained_Competency_Area__c> gcaToBeDeleted = new List<FP_Gained_Competency_Area__c>();
            for (FP_Gained_Competency_Area__c gainedCompetencyArea : potentialRelatedGainedCompetencyArea) {
                String logicStringToEvaluate = gainedCompetencyArea.FT_Pathway_Information__c;
                
                // We need to substitute the Subject Code + Subject Name on the Pathway
                for (FT_University_Subject__c uniSubject : uniSubjectMap.get(gainedCompetencyArea.FT_Pathway__r.FT_University__c)) {                        
                    if (logicStringToEvaluate.contains(uniSubject.FT_Pathway_Subject_Code__c)) {
                        
                        Boolean studentHaveTakenTheSubject = false;
                        for (FT_Paper_Gained_Competency__c paperGainedCompetency : gainedCompetencyArea.Paper_Gained_Competency__r) {                            
                            if (paperGainedCompetency.FT_Student_Paper__r.FT_University_Subject__c == uniSubject.Id) {
                                studentHaveTakenTheSubject = true;
                            }
                        }
                        logicStringToEvaluate = logicStringToEvaluate.replace(uniSubject.FT_Pathway_Subject_Code__c, String.valueOf(studentHaveTakenTheSubject));                        
                    }
                }

                // The evaluate the logic string
                wdci_CompetencyUtil.LogicString ls = new wdci_CompetencyUtil.LogicString();
                Boolean pathwayFulfilled = ls.LogicString(logicStringToEvaluate);
                
                if (!pathwayFulfilled) {
                    // This Gained Competency Area is no longer valid as the Student does not have the prequisite Subjects for it
                    gcaToBeDeleted.add(gainedCompetencyArea);
                }
            }
            if (gcaToBeDeleted.size() > 0) {
                // Delete the related paper gained compentecy first
                List<FT_Paper_Gained_Competency__c> paperGainedCompetencyToBeDeleted = new List<FT_Paper_Gained_Competency__c>();
                for (FP_Gained_Competency_Area__c gca : gcaToBeDeleted) {
                    for (FT_Paper_Gained_Competency__c paperGainedCompetency : gca.Paper_Gained_Competency__r) {
                        paperGainedCompetencyToBeDeleted.add(paperGainedCompetency);
                    }
                }
                if (paperGainedCompetencyToBeDeleted.size() > 0) delete paperGainedCompetencyToBeDeleted;
                // Then delete the Gained Competency Area themselves
                delete gcaToBeDeleted;
            }            
        } else {            
            for (FT_Student_Paper__c studentPaper : studentPaperList) {
                educationHistoryIDSet.add(studentPaper.FT_Education_History__c);
            }
            
            Map<Id, Set<Id>> universityMap = new Map<Id, Set<Id>>(); // Map of the University involved for each Application

            Map<Id, edu_Education_History__c> educationHistoryMap = wdci_CompetencyUtil.mapEducationHistory(educationHistoryIDSet);
            for (edu_Education_History__c educationHistory : educationHistoryMap.values()) {
                allUniversityIDSet.add(educationHistory.University_Degree__r.University__c);
                applicationIDSet.add(educationHistory.Application__c);
                if (universityMap.containsKey(educationHistory.Application__c)) {
                    universityMap.get(educationHistory.Application__c).add(educationHistory.University_Degree__r.University__c);
                } else {
                    Set<Id> newUniversityIDSet = new Set<Id>();
                    newUniversityIDSet.add(educationHistory.University_Degree__r.University__c);
                    universityMap.put(educationHistory.Application__c, newUniversityIDSet);
                }
            }

            // Pathway involved for the referenced Universities
            Map<Id, List<FT_Pathway__c>> pathwayMap = wdci_CompetencyUtil.mapPathway(allUniversityIDSet);

            // Subjects involved for the referenced Universities
            Map<Id, List<FT_University_Subject__c>> uniSubjectMap = wdci_CompetencyUtil.mapUniSubject(allUniversityIDSet);        
            
            // Student Paper involved for the referenced Applications (1 Student Paper per unique UniSubject on the application)
            Map<Id, Map<Id, FT_Student_Paper__c>> studentPaperMap = wdci_CompetencyUtil.mapStudentPaper(applicationIDSet);

            // Evaluate all the related Application
            for (Id applicationID : applicationIDSet) {
                for (Id universityID : universityMap.get(applicationID)) {
                    List<FT_Pathway__c> potentialPathwayList = pathwayMap.get(universityID);
                    if (potentialPathwayList != null) {
                        for (FT_Pathway__c potentialPathway : potentialPathwayList) {
                            string logicStringToEvaluate = potentialPathway.FT_Pathway__c;

                            // We need to substitute the Subject Code + Subject Name on the Pathway
                            for (FT_University_Subject__c uniSubject : uniSubjectMap.get(universityID)) {                        
                                if (logicStringToEvaluate.contains(uniSubject.FT_Pathway_Subject_Code__c)) {
                                    
                                    Boolean studentHaveTakenTheSubject = false;
                                    Map<Id, FT_Student_Paper__c> studentPaperMapForTheApplication = studentPaperMap.get(applicationID);
                                    // If the Student have Student Paper with the same Uni Subject, then he/she has taken this particular Pathway required subject
                                    if (studentPaperMapForTheApplication != null && studentPaperMapForTheApplication.containsKey(uniSubject.Id))
                                        studentHaveTakenTheSubject = true;

                                    logicStringToEvaluate = logicStringToEvaluate.replace(uniSubject.FT_Pathway_Subject_Code__c, String.valueOf(studentHaveTakenTheSubject));
                                }
                            }
                            system.debug('Application: ' +applicationID+ ' - University: ' +universityID+ ' - Pathway: ' +potentialPathway.Name+ ' - Processed Logic String: ' +logicStringToEvaluate);
                        }
                    }                
                }
            }
            system.debug('Exiting evaluateCompetency');
            
        }
    }
    */
}