/*
* Test class for AccountInvocableMethodHandler

======================================================
History 
Aug 201		RN Davanti 	Created
*/
@isTest
private class AccountInvocableMethodHandlerTest {
    
    private static testMethod void processAccountRequestTest(){
        
        // Set up Accounts and a child account.
        Account memberAccountObject = TestObjectCreator.createFullMemberAccount();
        
        Account busAccountObject = TestObjectCreator.createBusinessAccount();
        Account busAccountObject2 = TestObjectCreator.createBusinessAccount();
        
        List<Account> listAccount = new list<Account>();
        listAccount.add(memberAccountObject);
        listAccount.add(busAccountObject);
        
        insert listAccount;
        
        busAccountObject2.ParentId = listAccount[1].Id;
        insert busAccountObject2;		
        
        
        Test.startTest();
        
        // Create a relationship which should invoke AccountInvocableMethodHandler
        Relationship__c rel = TestObjectCreator.createRelationship();
        rel.Member__c = listAccount[0].Id;
        rel.Account__c = listAccount[1].Id;
        rel.Reciprocal_Relationship__c = 'PI Contact';
        rel.Primary_Relationship__c = 'PI Contact';
        rel.Status__c = 'Current';
        rel.Apply_PI_Contact_to_child_businesses__c = true;
        
        insert rel;
        
        busAccountObject.Apply_PI_Contact_to_child_businesses__c = true;
        busAccountObject.Review_Contact__c = listAccount[0].Id;
        
        update busAccountObject;
        
        list<AccountInvocableMethodHandler.AccountRequest> listRequest = new list<AccountInvocableMethodHandler.AccountRequest>();
        listRequest.add(new AccountInvocableMethodHandler.AccountRequest(listAccount[1], 'createPIRelationship'));
        
        AccountInvocableMethodHandler.processAccountRequest(listRequest);
        
        Test.stopTest();
    }
    
    private static testMethod void processAccountRequestEmploymentHistoryTest(){
        
        Account accountEmployer = TestObjectCreator.createBusinessAccount();
        Account accountMember = TestObjectCreator.createFullMemberAccount();
        insert new List<Account>{accountEmployer, accountMember};
            
        Employment_History__c eh= TestObjectCreator.createEmploymentHistory();
        eh.Member__c=accountMember.Id;
        eh.Employer__c=accountEmployer.id;
        eh.Is_CPP_Provided__c=false;
        insert eh;
        
        Relationship__c r=TestObjectCreator.createRelationship();
        r.Account__c=eh.Member__c;
        r.Member__c=eh.Employer__c;
        r.Status__c='Current';
        r.Primary_Relationship__c='PI Contact';
        
        test.startTest();
        insert r;
        test.stopTest();
        
        Account a=[SELECT Review_Contact__c FROM Account WHERE Id=:eh.Employer__c];
        system.assertEquals(eh.Member__c,a.Review_Contact__c);
    }
}