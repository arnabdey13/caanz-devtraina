/* 
    Developer: WDCi (Lean)
    Date: 02/Aug/2016
    Task #: LCA-822 Luana implementation - Wizard for Non AU program enrolment
*/

public without sharing class luana_ProgramEnrolmentWizardSUController {
    
    luana_ProgramEnrolmentWizardController stdController;
    List<luana_ProgramOfferingSubjectWrapper> programOfferingSubjects;
    
    public luana_ProgramEnrolmentWizardSUController(luana_ProgramEnrolmentWizardController stdController){
        this.stdController = stdController;
        
		init();
    }
    
    public void init(){
    	
    }
    
    public PageReference subjectNext(){
    	
    	Integer countElective = 0;
        stdController.selectedProgramOfferingSubjects = new List<luana_ProgramOfferingSubjectWrapper>();
        
        if(stdController.selectedCourse != null && stdController.selectedProgramOffering != null){
            for(luana_ProgramOfferingSubjectWrapper selectedSubject: programOfferingSubjects) {
                if(selectedSubject.isSelected) {
                    if(!selectedSubject.isCore){
                        countElective ++;
                    }
                    
                    stdController.selectedProgramOfferingSubjects.add(selectedSubject);
                    
                }
            }
        }
        
        if(countElective >= stdController.selectedProgramOffering.LuanaSMS__Number_of_Required_Electives__c) {
        	if(stdController.maxElective != null && countElective > stdController.maxElective){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'You can only select maximum ' + stdController.maxElective + ' elective/s.'));
                return null;
                
            } else {
            	
            	if(createRecords()){
            		return Page.luana_ProgramEnrolmentWizardSummary;
            	}
            }
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'You must select at least ' + stdController.selectedProgramOffering.LuanaSMS__Number_of_Required_Electives__c + ' elective/s before you can proceed.'));
        }
        
    	return null;
    	
    }
    
    public PageReference subjectBack(){
    	
    	PageReference pageRef = Page.luana_ProgramEnrolmentWizardCourse;
        
        return pageRef;
    }
    
    public boolean createRecords(){
    	
    	Attachment termsAndConditionAtt;
        
        for(Terms_and_Conditions__c termAndCon : [Select Id, Name from Terms_and_Conditions__c Where Name =: stdController.selectedProductType order by createddate desc limit 1]){
			for(Attachment att : [Select Id, Name, Body from Attachment Where ParentId =: termAndCon.Id order by createddate desc limit 1]){
				termsAndConditionAtt = att.clone(false, true);
		  	}
        }
        
        if(termsAndConditionAtt != null){
        	
        	Savepoint sp = Database.setSavepoint();
        	
        	try{
		    	LuanaSMS__Student_Program__c newStudentProgram = new LuanaSMS__Student_Program__c();
		    	newStudentProgram.LuanaSMS__Contact_Student__c = stdController.custCommConId;
		        newStudentProgram.LuanaSMS__Course__c = stdController.selectedCourse.Id;
		        newStudentProgram.LuanaSMS__Enrolment_Date__c =  System.today();
		        newStudentProgram.LuanaSMS__Status__c = luana_EnrolmentConstants.STUDENTPROGRAM_STATUS_INPROGRESS;
		        
		        insert newStudentProgram;
	        	
	        	List<LuanaSMS__Student_Program_Subject__c> newSPSs = new List<LuanaSMS__Student_Program_Subject__c>();
	        	for(luana_ProgramOfferingSubjectWrapper selectedPOS : stdController.selectedProgramOfferingSubjects){
	        		
	        		LuanaSMS__Student_Program_Subject__c newSPS = selectedPOS.sps.clone(false, true);
	        		
	        		newSPS.LuanaSMS__Program_Offering_Subject__c = selectedPOS.pos.Id;
                	newSPS.LuanaSMS__Subject__c = selectedPOS.pos.LuanaSMS__Subject__c;
                	newSPS.LuanaSMS__Contact_Student__c = stdController.custCommConId;
                	newSPS.LuanaSMS__Student_Program__c = newStudentProgram.Id;
                	
                	newSPSs.add(newSPS);
	        		
	        	}
	        	
	        	insert newSPSs;
	        	
	        	termsAndConditionAtt.ParentId = newStudentProgram.Id;
	        	
	        	insert termsAndConditionAtt;
	        	
	        	return true;
	        	
        	} catch(Exception exp){
        		Database.rollback(sp);
        		
        		if(exp.getMessage().contains(luana_EnrolmentConstants.STUDENTPROGRAM_DUPLICATE_CHECK_ERROR)){
                    //errMsg = 'You have already enrolled to the same course previously. Please contact our support if you would like to proceed with the enrollment.';
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'You have already enrolled to the same course previously. Please contact our support if you would like to proceed with the enrollment.'));
                } else {
                    //errMsg = 'Problem enroling course: ' + exp.getMessage() + '. Please try again later or contact our support if the problem persists.';
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Problem enroling course: ' + exp.getMessage() + '. Please try again later or contact our support if the problem persists.'));
                }
        	}
        } else {
        	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Could not find the related terms and conditions document. Please try again later or contact our support if the problem persists.'));
        }
        
        return false;
        
    }
    
    public List<luana_ProgramOfferingSubjectWrapper> getSubjectOptions() {
    	
    	if(programOfferingSubjects == null || programOfferingSubjects.size() == 0 || (programOfferingSubjects != null && programOfferingSubjects.get(0).pos.LuanaSMS__Program_Offering__c != stdController.selectedProgramOffering.Id)){
            programOfferingSubjects = new List<luana_ProgramOfferingSubjectWrapper>();
            
            if(stdController.selectedCourseId != null && stdController.selectedCourse != null && stdController.selectedProgramOffering != null){
                
                boolean autoSelection = luana_EnrolmentUtil.isAutoSubjectSelection(stdController.selectedProgramOffering.Subject_Selection_Option__c);
                
                for(LuanaSMS__Program_Offering_Subject__c pos : stdController.selectedProgramOffering.LuanaSMS__Program_Offering_Subjects__r) {
                	
                	LuanaSMS__Student_Program_Subject__c sps = new LuanaSMS__Student_Program_Subject__c(LuanaSMS__Subject__c = pos.LuanaSMS__Subject__c, LuanaSMS__Contact_Student__c = stdController.custCommConId);
                	
                    if(pos.LuanaSMS__Essential__c == 'Core'){
                        programOfferingSubjects.add(new luana_ProgramOfferingSubjectWrapper(true, true, true, pos, sps));
                    } else {
                        programOfferingSubjects.add(new luana_ProgramOfferingSubjectWrapper((autoSelection ? true : false), false, (autoSelection ? true : false) , pos, sps));
                    }
                }
            }
        }
        
        return programOfferingSubjects;
    }
    
    
}