/*------------------------------------------------------------------------------------
Author:        Paras Prajapati
Company:       Salesforce
Description:   Test class for most of the Controllers - CASUB_CaanzcAccountDetailsController_CA 

History
Date            Author             Comments
--------------------------------------------------------------------------------------
04-01-2019     Paras Prajapati          Initial Release
------------------------------------------------------------------------------------*/
@isTest public class CASUB_AccountDetailsController_CA_Test {
    private static User currentUser;
    private static Account currentAccount;
    private static Contact currentContact;
    private static Subscription__c subscription;
    private static My_Preference__c myPreference;
    
    static {
        currentAccount = CASUB_AccountDetailsController_CA_Test.createFullMemberAccount(); 
        currentAccount.Affiliated_Branch_Country__c = 'Australia';
        insert currentAccount;
        subscription = CASUB_AccountDetailsController_CA_Test.createSubscriptionRecord();
        insert subscription;
        System.debug('subscription' + subscription);
        currentContact = [SELECT Id FROM Contact WHERE AccountId =: currentAccount.Id];
    }
    
    private static User getCurrentUser(){
        Test.startTest(); Test.stopTest(); // Future method needs to run to create the user.
        return currentUser = [SELECT Name FROM User WHERE AccountId =: currentAccount.Id];
    }
    
    // <Create FullMemberAccount>
    public static Account createFullMemberAccount(){
        Account FullMemberAccountObject = new Account();
        FullMemberAccountObject.Salutation = 'Mr.';
        FullMemberAccountObject.FirstName = 'TestFName';
        FullMemberAccountObject.PersonEmail= 'Full_Member'+ CASUB_AccountDetailsController_CA_Test.getRandomNumber(910) +'@gmail.com';
        FullMemberAccountObject.LastName = 'Test'+ CASUB_AccountDetailsController_CA_Test.getRandomNumber(910);
        FullMemberAccountObject.RecordTypeId = RecordTypeCache.getId(FullMemberAccountObject, 'Full_Member');
        FullMemberAccountObject.Communication_Preference__c= 'Home Phone';
        FullMemberAccountObject.PersonHomePhone= '1234';
        FullMemberAccountObject.PersonOtherStreet= '83 Saggers Road';
        FullMemberAccountObject.PersonOtherCity='JITARNING';
        FullMemberAccountObject.PersonOtherState='Western Australia';
        FullMemberAccountObject.PersonOtherCountry='Australia';
        FullMemberAccountObject.PersonOtherPostalCode='6365';  
        return FullMemberAccountObject;
    }
    
    public static Subscription__c createSubscriptionRecord(){
        Subscription__c subscription = new Subscription__c();
        subscription.Account__c = currentAccount.Id;
        subscription.Contact_Details_Status__c = 'Pending';
        subscription.Obligation_Status__c = 'Pending';
        subscription.Sales_Order_Status__c = 'Pending';
        subscription.Year__c = '2019';
        return subscription;
    }
    
    public static Integer getRandomNumber(Integer size){
          Double d = math.random() * size;
          return d.intValue();
     }
    
    @isTest static void test_CaanzAccountDetailsController() {
        System.runAs(getCurrentUser()) {
            String data = CASUB_CaanzAccountDetailsController_CA.getAccountData();
            CASUB_CaanzAccountDetailsController_CA.getFinancialCategoryPicklistValues();
            CASUB_CaanzAccountDetailsController_CA.getConcessionConfigData();
            try{
                String personAccountId = currentAccount.Id;
                String financialCategory = 'Low income concession';
                CASUB_CaanzAccountDetailsController_CA.saveFinancialCategory(personAccountId,financialCategory); 
                CASUB_CaanzAccountDetailsController_CA.saveAccountData(data);
            } catch(Exception ex){
                System.debug(ex.getMessage());
            }
        }
    }
}