public with sharing class EditorUpdatePreferencesService {

    // TODO define an object and a set of fields that are allowed to be read and written, some can be read only

    @AuraEnabled
    public static Map<String, Object> load() {
        return load(EditorPageUtils.getUsersAccountId());
    }

    private static final Set<String> BOOLEAN_FIELDS = new Set<String> {
            'PersonHasOptedOutOfEmail',
            'PersonHasOptedOutOfMail__pc',
            'PersonDoNotCall',
            'PersonHasOptedOutOfSMS__pc',
            'Bulk_pay__pc',
            'Marketing_Promo_Opt_Out__pc'
    };

    private static final Set<String> PICKLIST_FIELDS = new Set<String> {
            'NL_News_Events_Local_NZICA_Office__pc', // regional office events
            'NL_In_The_Know__pc', // insight NZ
            'NL_Institute_News__pc', // insight AU
            'NL_CPD_Information__pc',
            'NL_Special_Interest_Group__pc',
            'NL_Library_eNews__pc',
            'NL_Asia_Insight__pc',
            'NL_Affinity_Members_Programme__pc',
            'NL_CA_Tax_Bulletin__pc', // Tax News
            'Tax_News_NZ__pc',
            'NL_Superannuation_Bulletin__pc', // Superannuation News
            'NL_eLearning_Update__pc',
            'NL_Accounting_Audit_News_Today_ANT__pc',
            'NL_CA_Program_Candidate__pc',
            'NL_Educator_e_news__pc', // Academic News
            'CP_Member_Surveys__pc', // Member Surveys
            'CP_GAA_Export__pc', // GAA Transfer
            'CP_Training_Development_Events__pc', // CPD Events
            'CP_Member_Benefits__pc', // Affinity Members Programme TODO should this be NL_Affinity_Members_Programme__pc
            'CP_American_Express__pc',
            'CP_HCF__pc',
            'Business_Valuation_Community_Newsletter__pc',
            'Forensic_Accounting_Community_Newsletter__pc',
            'IT_Community_Newsletter__pc',
            'NL_Acuity_Newsletter__pc',
            'NL_Acuity_eNewsletter__pc',
            'NL_Insight_UK__pc'
    };

    public static String getQuery(String accountId) {
        String fieldList = '';
        for (String f : BOOLEAN_FIELDS) {
            fieldList += f + ',';
        }
        for (String f : PICKLIST_FIELDS) {
            fieldList += f + ',';
        }
        return 'SELECT '+fieldList.substringBeforeLast(',') + ' FROM Account WHERE Id = \''+accountId+'\'';
    }

    @TestVisible
    // the client expects only boolean values so this method translates the varying database values into booleans
    private static Map<String, Object> load(Id accountId) {

        Account a = Database.query(getQuery(accountId));

        // the client only sees boolean values for its checkboxes so this is where the conversion is done
        Map<String, Boolean> record = new Map<String, Boolean>();
        for (String f : BOOLEAN_FIELDS) {
            record.put(f, (Boolean) a.get(f));
        }
        for (String f : PICKLIST_FIELDS) {
            String value = (String) a.get(f);
            if (value != null) {
                record.put(f, value.equals('Yes'));
            }
        }

        return new Map<String, Object> {'RECORD' => record};
    }

    @TestVisible
    // convert a boolean value from the client into the correct value for the data nodel
    private static Object translateType(String fieldName, Boolean fieldValue) {
        if (BOOLEAN_FIELDS.contains(fieldName)) {
            return fieldValue;
        } else if (PICKLIST_FIELDS.contains(fieldName)) {
            return ((Boolean)fieldValue)?'Yes':'No';
        } else {
            throw new SObjectException('invalid field: '+fieldName);
        }
    }

    @AuraEnabled
    public static Map<String, Object> save(Map<String, Object> properties) {
        return save(properties, EditorPageUtils.getUsersAccountId());
    }

    @TestVisible
    // despite receiving String,Boolean - the client returns Object, Object in json so save has to adjust for that.
    private static Map<String, Object> save(Map<String, Object> properties, Id accountId) {
        // record is Object,Object because it comes back via json
        Map<Object,Object> record = (Map<Object, Object>) properties.get('RECORD');
        // translate the values from client booleans into correct data model types
        Map<Object, Object> translated = new Map<Object, Object>();
        for (Object f : record.keySet()) {
            String fieldName = (String) f;
            Boolean fieldValue = (Boolean) record.get(fieldName);
            if (fieldValue != NULL) {
                translated.put(fieldName, translateType(fieldName, fieldValue));
            }
        }
        // now pass the translated values to the sobject translator
        properties.put('RECORD', translated);
        Account toSave = (Account) EditorPageUtils.getRecordFromSaveRequest(new Account(Id=accountId), properties);
        System.debug('toSave: '+toSave);
        update toSave;
        return null;
    }

}