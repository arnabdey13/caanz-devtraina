public class QPRSearchReportQueueable implements Queueable{
    
    private List<QPR_Questionnaire__c> recordGoingToProcessList;
    private List<QPR_Questionnaire__c> recordsRemainingToProcessList ;
    
    public QPRSearchReportQueueable( List<QPR_Questionnaire__c> recordGoingToProcessList){
        
        this.recordGoingToProcessList = recordGoingToProcessList;
        //this.recordsRemainingToProcessList = recordsRemainingToProcessList;
    }
    
    public void execute(QueueableContext context){
        if(!recordGoingToProcessList.isEmpty()){
            Database.insert(recordGoingToProcessList, false) ;
        }
        
        /*List<QPR_Questionnaire__c> recordGoingToProcessListTemp = new List<QPR_Questionnaire__c>();
List<QPR_Questionnaire__c> recordsRemainingToProcessListTemp = new List<QPR_Questionnaire__c>();
recordsRemainingToProcessListTemp.addAll(recordsRemainingToProcessList);

Integer count = 0 ;
if (!recordsRemainingToProcessList.isEmpty()) { 
for(QPR_Questionnaire__c q : recordsRemainingToProcessList) {
if(count <= 20){
recordGoingToProcessListTemp.add(q);
if(recordsRemainingToProcessListTemp.size() >= count){
recordsRemainingToProcessListTemp.remove(count);
}

count++ ;
}else{
break ;
}            
}
QPRSearchReportQueueable updateJob = new QPRSearchReportQueueable(recordGoingToProcessListTemp, recordsRemainingToProcessListTemp);
System.enqueueJob(updateJob);
} */       
    }
    
    /*public static void splitList(List<QPR_Questionnaire__c> qprList){
List<QPR_Questionnaire__c> recordGoingToProcessList = new List<QPR_Questionnaire__c>();
List<QPR_Questionnaire__c> recordsRemainingToProcessList = new List<QPR_Questionnaire__c>();
recordsRemainingToProcessList.addAll(qprList);

Integer count = 0 ;
if (!qprList.isEmpty()) { 
for(QPR_Questionnaire__c q : qprList) {
if(count <= 20){
recordGoingToProcessList.add(qprList[count]);
recordsRemainingToProcessList.remove(count);
count++ ;
}else{
break ;
}            
}
QPRSearchReportQueueable updateJob = new QPRSearchReportQueueable(recordGoingToProcessList, recordsRemainingToProcessList);
System.enqueueJob(updateJob);
//splitList(qprListTemp); 
}        
}*/
    
    public static void splitList(List<QPR_Questionnaire__c> qprList){
        List<QPR_Questionnaire__c> ret = new List<QPR_Questionnaire__c>() ;
        integer i = 0;
        system.debug('***qprList***'+qprList);
        for ( QPR_Questionnaire__c o: qprList ){
            ret.add(o);
            if ( i == 20 ){
                QPRSearchReportQueueable updateJob = new QPRSearchReportQueueable(ret);
                System.enqueueJob(updateJob);
                ret.clear();
                i=0;
            }
            i++;
        }
        if(!ret.isEmpty()){
            QPRSearchReportQueueable updateJob = new QPRSearchReportQueueable(ret);
            System.enqueueJob(updateJob);
        }
        
    }
    
}