/*
 * @author          WDCi (LKoh)
 * @date            24-June-2019
 * @description     Test class for the wdci_ApplicationHandler class
 * @changehistory
//PAN:6539 - Edy: Changes for Contact lookup > Master Detail in Education History
 */
@isTest
public class wdci_ApplicationTest {
    
    @TestSetup
    static void generateAssets() {

        wdci_TestUtil.generateAsset();
    }

    @isTest static void testApplicationHandler() {

        String currentYear = String.valueOf(system.today().year());
        String educationHistoryCommencedYear = String.valueOf(system.today().addYears(-3).year());
        String educationHistoryEndYear = String.valueOf(system.today().addYears(-1).year());

        List<Account> studentAccount = [SELECT Id, personcontactid FROM Account WHERE Member_ID__c = '3053164']; ////PAN:6539 - Edy
        List<edu_University_Degree_Join__c> uniDegreeJoin = [SELECT Id FROM edu_University_Degree_Join__c];
        List<FT_University_Subject__c> uniSubject = [SELECT Id FROM FT_University_Subject__c];

        // Application
        Application__c testApplication = wdci_TestUtil.generateApplication(studentAccount[0].Id, 'Chartered Accountants', 'Draft', 'Standard');
        insert testApplication;

        List<Application__c> checkApplication = [SELECT Id, Furthest_Stage_Number__c FROM Application__c WHERE Id = :testApplication.Id];
        system.debug('checkApplication: ' +checkApplication);

        // Education History
        edu_Education_History__c testEducationHistory = wdci_TestUtil.generateEducationHistory(testApplication.Id, studentAccount[0].personcontactid, uniDegreeJoin[0].Id, educationHistoryCommencedYear, educationHistoryEndYear); //PAN:6539 - Edy
        insert testEducationHistory;

        // Student Paper
        FT_Student_Paper__c testStudentPaper = wdci_TestUtil.generateStudentPaper(uniSubject[0].Id, testEducationHistory.Id, true);

        system.debug('testStudentPaper: ' +testStudentPaper);

        insert testStudentPaper;        
    
        Test.startTest();
        
        testApplication.Resumption_Stage_Number__c = 0;
        update(testApplication);

        Test.stopTest();
    }

    @isTest static void testRegexPattern() {
        String code = 'Law12';

        String raw = 'Law12 AND Law12 OR Law12 AND (Law12) Law12 (Law12 v2 AND (Law12) AND Law12 v22 OR Law12 v11) Law12 (Law12)';
        String expected = 'true AND true OR true AND (true) Law12 (Law12 v2 AND (true) AND Law12 v22 OR Law12 v11) Law12 (true)';

        system.assert(wdci_CompetencyUtil.isCodeMatches(raw, code), 'This should be true.');
        
        String result = wdci_CompetencyUtil.replacePatternAll(raw, code, 'true');
        system.assertEquals(expected, result, 'The result is not matched.');

    }
}