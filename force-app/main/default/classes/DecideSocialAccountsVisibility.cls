public class DecideSocialAccountsVisibility {
    @AuraEnabled
    public static Boolean displaylinktofacebook () {
        Id loggedinuserid = [Select id from User where id =: UserInfo.getUserId()].id;
        List<ThirdPartyAccountLink> tpalinkList = [Select id, Provider, RemoteIdentifier, UserId from ThirdPartyAccountLink where userid =: loggedinuserid AND provider = 'facebook'];
        if(tpalinkList != null && tpalinkList.size() > 0) {
            system.debug('returning true facebook');
            return true;
        }
        system.debug('returning false facebook');
        return false;
    }
    
    @AuraEnabled
    public static Boolean displaylinkedinlinkapex () {
        Id loggedinuserid = [Select id from User where id =: UserInfo.getUserId()].id;
        List<ThirdPartyAccountLink> tpalinkList1 = [Select id, Provider, RemoteIdentifier, UserId from ThirdPartyAccountLink where userid =: loggedinuserid AND provider = 'linkedin'];
        system.debug('tpalinkList1: '+tpalinkList1);
        system.debug('tpalinkList1 size: '+tpalinkList1.size());
        if(tpalinkList1 != null && tpalinkList1.size() > 0) {
            system.debug('returning true linkedin');
            return true;
        }
        system.debug('returning false linkedin');
        return false;
    }
    
    @AuraEnabled
    public static Boolean unlinkUser() {
        //select loggedin user
        Id loggedinuserid = [Select id from User where id =: UserInfo.getUserId()].id;
        Id authid = [Select id, developername from AuthProvider where developername = 'facebook'].id;
        system.debug('$$$$$loggedinuserid is'+loggedinuserid );
        system.debug('$$$$$$authid is'+authid );
        List<ThirdPartyAccountLink> tpalinkList = [Select id, Provider, RemoteIdentifier, UserId from ThirdPartyAccountLink where userid =: loggedinuserid AND provider = 'facebook'];
        PageReference pageRef = new PageReference('');
        system.debug('$$$$$$tpalinkList is'+tpalinkList );
        if(tpalinkList != null && tpalinkList.size() > 0) {
            system.debug('details here: '+authid  + ' ' +tpalinkList[0].provider + ' '+tpalinkList[0].userid + ' ' +tpalinkList[0].RemoteIdentifier);
            Boolean revoked = Auth.AuthToken.revokeAccess(authid ,tpalinkList[0].provider,tpalinkList[0].userid,tpalinkList[0].RemoteIdentifier);
            if(revoked)
                return true;
            return null;
        }
        else 
            return null;
    }

    @AuraEnabled
    public static Boolean unlinkLinkedinUserCallApex() {
        //select loggedin user
        Id loggedinuserid = [Select id from User where id =: UserInfo.getUserId()].id;
        Id authid = [Select id, developername from AuthProvider where developername = 'linkedin'].id;
        system.debug('****************loggedinuserid is'+loggedinuserid );
        system.debug('*************authid is'+authid );
        List<ThirdPartyAccountLink> tpalinkList = [Select id, Provider, RemoteIdentifier, UserId from ThirdPartyAccountLink where userid =: loggedinuserid AND provider = 'linkedin'];
        PageReference pageRef = new PageReference('');
        system.debug('*************tpalinkList is'+tpalinkList );
        if(tpalinkList != null && tpalinkList.size() > 0) {
            system.debug('details here: '+authid  + ' ' +tpalinkList[0].provider + ' '+tpalinkList[0].userid + ' ' +tpalinkList[0].RemoteIdentifier);
            Boolean revoked = Auth.AuthToken.revokeAccess(authid ,tpalinkList[0].provider,tpalinkList[0].userid,tpalinkList[0].RemoteIdentifier);
            //pageRef = new PageReference('https://devmanu-charteredaccountantsanz.cs6.force.com/napili/s/');
            //pageRef.setRedirect(true);
            if(revoked)
                return true;
            return null;
        }
        else 
            return null;
    }
}