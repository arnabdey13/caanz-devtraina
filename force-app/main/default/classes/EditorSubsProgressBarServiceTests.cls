@IsTest
public with sharing class EditorSubsProgressBarServiceTests {

    @IsTest
    public static void testAUUserNotifications() {
        Map<String, Object> properties = (Map<String, Object>) EditorSubscriptionsProgressBarService.load(TestSubscriptionUtils.getTestAccountId());
        System.assertEquals(true,properties.get('prop.Notifications'), 'enabled for AU users');
    }

    @IsTest
    public static void testNZProvisionalUserNotifications() {
        update new Account(Id=TestSubscriptionUtils.getTestAccountId(), PersonOtherCountryCode = 'NZ', Membership_Class__c = 'Provisional');
        Map<String, Object> properties = (Map<String, Object>) EditorSubscriptionsProgressBarService.load(TestSubscriptionUtils.getTestAccountId());
        System.assertEquals(false,properties.get('prop.Notifications'), 'disabled for provisional NZ users');
    }

    @testSetup static void createTestData() {
        TestSubscriptionUtils.createTestData();
    }


}