public class luana_PrintResultCtl {
    
    public Boolean savePDFFlag{get; set;}
    public Id currentSPId {get; private set;}
    public Id currentContId {get; private set;}
    public Boolean isCommunityUser {get; set;}
    public Boolean isWithoutHeader { get; set; }
    public Boolean viewPDF {get; set;}
    
    public Boolean viewAsPDF {get; set;}
    public Boolean isPDF {get; set;}
    
    public luana_PrintResultCtl(){
        Id userId = UserInfo.getUserId();
        System.debug('****userId:: ' + userId);
        
        savePDFFlag = false;
        viewAsPDF = false;
        isPDF = false;
        
        List<User> users = [select Id, IsActive, Profile.Name, UserRole.Name, UserType from User Where Id =: userId];
        if(users[0].UserType == 'CSPLitePortal'){
            isCommunityUser = true;
            isWithoutHeader = true;
        }else{
            isCommunityUser = false;
        }
        System.debug('***ApexPages.currentPage():: ' + ApexPages.currentPage());
        currentSPId = ApexPages.currentPage().getParameters().get('id');
        currentContId = ApexPages.currentPage().getParameters().get('cid');
        isWithoutHeader = Boolean.valueOf(ApexPages.currentPage().getParameters().get('h'));
        //viewPDF = Boolean.valueOf(ApexPages.currentPage().getParameters().get('isPDF'));
    }
    
    
    public void savePDF(){
        System.debug('***save pdf::: ');
        if (!savePDFFlag){
            viewAsPDF = true;
            Document_Repository__c dr = new Document_Repository__c();
            dr.Contact__c = currentContId;
            dr.Is_Public__c = true;
            dr.Student_Program__c = currentSPId;
            dr.Name = 'Result ' + Datetime.now().format('yyyy-MM-dd HH:mm');
            insert dr;
            
            PageReference pagePdf;
            if(isCommunityUser){
                pagePdf = new PageReference('/member/luana_PrintResult?id='+currentSPId+'&cid='+currentContId+'&h='+isWithoutHeader); 
            }else{
                pagePdf = new PageReference('/apex/luana_Printresult?id='+currentSPId+'&cid='+currentContId+'&h='+isWithoutHeader); 
            }       
            //System.debug('***run here dr 2::: ' + pagePdf.getContentAsPDF());
            Blob pdfPageBlob; 
            
            if(Test.isRunningTest()) { 
              pdfPageBlob = blob.valueOf('Unit.Test');
            } else {
              pdfPageBlob = pagePdf.getContentAsPDF();
            }
            
            //pdfPageBlob = pagePdf.getContentAsPDF(); 
            System.debug('***run here dr 3::: ');
            Attachment a = new Attachment(); 
            a.Body = pdfPageBlob; 
            a.ParentID = dr.Id;
            a.Name = 'Result ' + Datetime.now().format('yyyy-MM-dd HH:mm') +'.pdf'; 
            insert a; 
      
            System.debug('***run here a::: ' + a);
           
            savePDFFlag=true;
        }
    }        
}