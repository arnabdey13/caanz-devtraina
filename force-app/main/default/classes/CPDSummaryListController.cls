public class CPDSummaryListController {
    
    @AuraEnabled
    public static List<CPD_Summary__c> getCPDSummariesForUser() {
        
        List<CPD_Summary__c> cpdSummaries;
        

        List<User> userContactIds = [select ContactId  from User where Id = :userInfo.getUserId() limit 1]; 

        if (userContactIds != null && !userContactIds.isEmpty()) {
            //Start PASA change
                system.debug('### Retrieving CPD Summaries records');
                cpdSummaries = [select id, Current_Triennium__c,
                                Triennium_Start_Date__c,Triennium_End_Date__c,
                                Current_Year_Start_Date__c,Current_Year_End_Date__c,
                                CPD_Earned_This_Triennium__c, Required_Total_Triennium_Hours__c,        //Triennium Totals
                                CPD_Earned_This_Year__c, Required_Total_Annual_Hours__c,                //Annual Totals     
                                Total_Formal_Verifiable_Hours__c, Maximum_Triennium_Informal_Hours__c,  //Triennium Verifiable Totals   
                                Total_Exempt_Hours__c, Total_Formal_Exempt_Hours__c,                    //Exempted Hours    
                                Year_1_Date_Range__c, Year_2_Date_Range__c, Year_3_Date_Range__c,       //Annual Date Range Text                               
                                Qualifying_Informal_Hours_Y1__c, Formal_Verifiable_Hours_Y1__c,         //Year 1 hours
                                Qualifying_Informal_Hours_Y2__c, Formal_Verifiable_Hours_Y2__c,         //Year 2 hours
                                Qualifying_Informal_Hours_Y3__c, Formal_Verifiable_Hours_Y3__c,         //Year 3 hours                             
                                toLabel(Specialisation_1__c), Specialisation_Hours_1__c,                         //Specialisation Type & Hours                              
                                toLabel(Specialisation_2__c), Specialisation_Hours_2__c,                         //Specialisation Type & Hours                              
                                toLabel(Specialisation_3__c), Specialisation_Hours_3__c,                         //Specialisation Type & Hours                              
                                toLabel(Specialisation_4__c), Specialisation_Hours_4__c,                         //Specialisation Type & Hours                              
                                toLabel(Specialisation_5__c), Specialisation_Hours_5__c,                         //Specialisation Type & Hours                              
                                toLabel(Specialisation_6__c), Specialisation_Hours_6__c,                         //Specialisation Type & Hours                              
                                toLabel(Specialisation_7__c), Specialisation_Hours_7__c,                         //Specialisation Type & Hours                              
                                toLabel(Specialisation_8__c), Specialisation_Hours_8__c                          //Specialisation Type & Hours                              
                       from CPD_Summary__c
                       where (Contact_Student__c = :userContactIds[0].ContactId) Order By Triennium_Start_Date__c
                       limit 5000];
            }
        return cpdSummaries;
    }
    
    @AuraEnabled
    public static List<Education_Record__c> getEducationRecordsForUser() {
        
        List<Education_Record__c> educationRecords;
        

        List<User> userContactIds = [select ContactId  from User where Id = :userInfo.getUserId() limit 1]; //PASA change Is_Migration_Agent__c

        if (userContactIds != null && !userContactIds.isEmpty()) {
                system.debug('### Retrieving Education records');
                educationRecords = [select Id, Name, toLabel(Specialist_hours_code__c), Community_User_Entry__c, 
                                    Date_Commenced__c, Event_Course_name__c, Qualifying_hours__c, Qualifying_hours_type__c,                                    
                                    CAHDP__c, Date_completed__c, Country__c, State__c, University_professional_organisation__c, 
                                    University_professional_organisation__r.Name, Enter_university_instituition__c, Verified__c,
                                    University_or_Institution__c, Linked_To_Current_Triennium__c,
                                    Bridging__c, Training_and_development__c, 
                                    Degree_Country__c, Degree__c, 
                                    Degree_Other__c, Professional_Competency__c, Type_of_Activity__c,Status__c            
                                    from Education_Record__c
                       where (Contact_Student__c = :userContactIds[0].ContactId) Order By Date_completed__c
                       limit 5000];
            }
        return educationRecords;
    }
    
      @AuraEnabled
    public static Integer CPDLogPendingConfirmation() {
        Integer count=0;

          List<Education_Record__c> educationRecords;
        List<User> userContactIds = [select ContactId  from User where Id = :userInfo.getUserId() limit 1]; //PASA change Is_Migration_Agent__c

             if (userContactIds != null && !userContactIds.isEmpty()) {
                system.debug('### Retrieving Education records');
                educationRecords = [select Id, Name, Status__c,Contact_Student__c       
                                    from Education_Record__c
                       where (Contact_Student__c = :userContactIds[0].ContactId and Status__c='Awaiting Confirmation') 
                       limit 5000];
            }
        for(Education_Record__c educationRecord: educationRecords)
        {
            count++;
        }
			return count;
    }
      @AuraEnabled
    public static void deleteEducationRecordByID(List<ID> idList) {
        List<Education_Record__c> educationRecordList = new List<Education_Record__c>();
        educationRecordList = [Select Id FROM Education_Record__c where ID IN:idList];
        try{
            delete educationRecordList;
        }catch(Exception e){
            throw new AuraHandledException('== Error =='+ e.getMessage());
        }
            
    }
    
}