@IsTest(seeAllData=false)
public with sharing class PrintableNotificationsControllerTests {

    @testSetup static void createTestData() {
        TestSubscriptionUtils.createTestData();
    }

    @IsTest
    public static void testPrintThisYear() {
        PageReference pageRef = Page.PrintableNotifications;
        Test.setCurrentPage(pageRef);

        Questionnaire_Response__c qr = fake(EditorNotificationsService.getCurrentYear());
        PrintableNotificationsController controller = new PrintableNotificationsController();

        System.assertNotEquals(null, controller.year, 'a year is returned');
        Questionnaire_Response__c response = controller.getAccountResponse(TestSubscriptionUtils.getTestAccountId());
        System.assertEquals(qr.Id,response.Id, 'The fake response is returned');
        System.assertEquals(1, controller.getAnswers(response).size(), 'the single answer from the fake response is returned');
    }

    @IsTest
    public static void testPrintLastYear() {
        PageReference pageRef = Page.PrintableNotifications;
        pageRef.getParameters().put('y','p');
        Test.setCurrentPage(pageRef);

        Questionnaire_Response__c qr = fake(EditorNotificationsService.getCurrentYear()-1);
        PrintableNotificationsController controller = new PrintableNotificationsController();

        System.assertNotEquals(null, controller.year, 'a year is returned');
        Questionnaire_Response__c response = controller.getAccountResponse(TestSubscriptionUtils.getTestAccountId());
        System.assertEquals(qr.Id,response.Id, 'The fake response is returned');
        System.assertEquals(1, controller.getAnswers(response).size(), 'the single answer from the fake response is returned');
    }

    private static Questionnaire_Response__c fake(Integer year) {
        Questionnaire_Response__c qr = new Questionnaire_Response__c();
        qr.Lock__c = true;
        qr.Account__c = TestSubscriptionUtils.getTestAccountId();
        qr.Residential_Country__c = 'New Zealand';
        qr.Year__c = String.valueOf(year);
        qr.Question_1__c = 'Who will win the Super Rugby Comp?';
        qr.Answer_1__c = 'Otago, naturally!';
        insert qr;
        return qr;
    }
}