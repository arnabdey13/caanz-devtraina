public class QPRFormMessage {
   public String formAction{get;set;}
    public boolean showResume{get;set;}
	public boolean showSave{get;set;}
    public String test{get;set;}
public void validateRequest()
{
     //   test = Url.getSalesforceBaseURL().toExternalForm();
     test = Url.getSalesforceBaseURL().toExternalForm();
       test=formAction;
    try{
       showResume=false;
        showSave=false;
          String keyValue='';
              //  QPR_Config__c obj= QPR_Config__c.getValues('FormAssemblyKey'); 
       // String keyValue=obj.QPR_Key_Value__c; 
             List<QPR_Config__mdt> kevalues = [select QPR_Key_Value__c from QPR_Config__mdt where QPR_Key__c='FormAssemblyKey']; 
                for( QPR_Config__mdt qprConfigobj : kevalues)
                {
                keyValue=   qprConfigobj.QPR_Key_Value__c;
                }
        Blob key = Blob.valueOf(keyValue);
        string encodedid = ApexPages.currentPage().getParameters().get('id');
 		Blob blobData = EncodingUtil.base64Decode(encodedid);    
		Blob decryptedBlob = Crypto.decryptWithManagedIV('AES128', key, blobData);    
        string decodedId = decryptedBlob.toString();
          test=decodedId;
        formAction = decodedId;
    
        if(formAction=='au_savenresume'||formAction=='nz_savenresume')
        {
            showResume =true;
        }
        else if (formAction=='au_savensubmit'||formAction=='nz_savensubmit')
        {
            showSave =true;
        }
        
    }    
	catch(Exception e )
	{
system.debug('Exception: ' + e.getStackTraceString());              
    }
}
    
    
}