/**
 * Developer: WDCi (LKoh)
 * Date: 09-10-2018
 * Task #: PAN5340 - Flexipath Gained Competency Area UI    

Update 20190819 (Edy - #PAN6549) - add with sharing

**/
public with sharing class FP_GainedCompetencyArea_CTRL {
    
    public Id passedContactID{get;set;}
    public Id passedAccountID{get;set;}
    
    public List<CompetencyAreaWrapper> competencyAreaWrapperList{get;set;}
    public List<EducationHistoryWrapper> educationHistoryWrapperList{get;set;}
    
    public Boolean selectAll {get;set;}
    
    public Account currentAccount{get;set;}
    public Boolean disableCAFCheckbox{get;set;}
    public Boolean disableCACheckbox{get;set;}
    private Map<Id, FP_Gained_Competency_Area__c> gainedCompetencyAreaMap;        
    public Map<Id, String> associatedModuleMap{get;set;}
    
    public Map<Id, List<Attachment>> attachmentMap{get;set;}
        
    public Boolean editable{get;set;}
    public Boolean deletable{get;set;}
    public Boolean creatable{get;set;}

    public String SelectedEduHistoryId{get; set;}        
    
    // Text to be displayed
    public String assessibilityMessage{get;set;}
    public String capRequirementTitle{get;set;}
    public String cafRequirementTitle{get;set;}
    public String capText{get;set;}
    public String cafText{get;set;}
    public String titleText{get;set;}
    public String subtitleText{get;set;}

    public FP_GainedCompetencyArea_CTRL() {
        gainedCompetencyAreaMap = new Map<Id, FP_Gained_Competency_Area__c>();
        associatedModuleMap = new Map<Id, String>();
        competencyAreaWrapperList = new List<CompetencyAreaWrapper>();
        attachmentMap = new Map<Id, List<Attachment>>();
        
        // Text message
        // assessibilityMessage = '<b><i><u>Requirement</u></i></b><br/>1. Student has completed Degree<br/>2. Student has Degree In Progress<br/>3. Student is a Provisional Member';
        cafRequirementTitle = '<b><i><u>For CA Foundations Requirements are:</u></i></b><br/>';
        capRequirementTitle = '<br/><b><i><u>For CA Program Requirements are:</u></i></b><br/>';
        capText = '';
        cafText = '';
        subtitleText = 'Competence Area Gained';
    }
    
    private void initialize() {
        
        if (passedContactID != null || passedAccountID != null) {
            if (passedContactID != null) {
                Contact currentContact = [SELECT Id, AccountID FROM Contact WHERE Id = :passedContactID];
                currentAccount = [SELECT Id, FirstName, LastName, Membership_Class__c, Assessible_for_CAF_Program__c, Assessible_for_CA_Program__c FROM Account WHERE Id = :currentContact.AccountID];
                passedAccountID = currentAccount.Id;
            } else {
                currentAccount = [SELECT Id, FirstName, LastName, Membership_Class__c, Assessible_for_CAF_Program__c, Assessible_for_CA_Program__c FROM Account WHERE Id = :passedAccountID];
                passedContactID = [SELECT Id, AccountID FROM Contact WHERE AccountID = :passedAccountID].Id;
            }
            
            if (currentAccount.Assessible_for_CAF_Program__c) disableCAFCheckbox = true;
            if (currentAccount.Assessible_for_CA_Program__c) disableCACheckbox = true;
            
            // Populate the text for the Title with the Account Name
            titleText = currentAccount.FirstName + ' ' + currentAccount.LastName;
            
            // Populate the Gained Competency Area map - used to indicate which Competency Area already attained by the user
            for (FP_Gained_Competency_Area__c gac : [SELECT Id, FP_Competency_Area__c FROM FP_Gained_Competency_Area__c WHERE FP_Contact__c = :passedContactID]) {
                gainedCompetencyAreaMap.put(gac.FP_Competency_Area__c, gac);
            }
            
            // Check the FLS for the User
            editable = Schema.sObjectType.edu_Education_History__c.fields.FP_Contact__c.isUpdateable();
            deletable = edu_Education_History__c.sObjectType.getDescribe().isDeletable();
            creatable = Schema.sObjectType.edu_Education_History__c.fields.FP_Contact__c.isCreateable();
            
        } else {
            // this should throw error as we should not be able to call this component without linked Account.
        }
    }
    
    public List<CompetencyAreaWrapper> getCompetencyAreaWrappers() {

        loadCompetencyAreaData();
        
        return competencyAreaWrapperList;
    }
    
    public List<EducationHistoryWrapper> getEducationHistoryWrappers() {
        
        initialize(); // We have to initialize it here as it would not work in the Constructor due to the passed IDs not being populated yet during Constructor runtime
        LoadEduHistoryData();
                
        return educationHistoryWrapperList;
    }

    public PageReference saveRecords() {
        
        // Initializing the anchor if we need to revert the changes back
        Savepoint sp = Database.setSavepoint();
        try {
            // Updating Person Account record
            update currentAccount;
            
            List<FP_Gained_Competency_Area__c> gcaToUpsert = new List<FP_Gained_Competency_Area__c>();
            // Updating Gained Competency Area records
            for (CompetencyAreaWrapper caw : competencyAreaWrapperList) {
                if(caw.caChecked) {                                                            
                    FP_Gained_Competency_Area__c newGCA = new FP_Gained_Competency_Area__c();
                    newGCA.FP_Competency_Area__c = caw.competencyAreaID;
                    newGCA.FP_Contact__c = passedContactId;
                    String externalIDString = newGCA.FP_Contact__c + '|' + newGCA.FP_Competency_Area__c;                
                    newGCA.FP_External_Id__c = externalIDString;
                    gcaToUpsert.add(newGCA);
                }                
            }
            upsert gcaToUpsert FP_External_Id__c;
            
            return new ApexPages.StandardController(currentAccount).view();
        } catch (Exception e) {
            // Revert all changes done if any error occurs.
            Database.rollback(sp);
            system.debug('Exception: ' +e);
            system.debug('Exception stack trace: ' +e.getStackTraceString());
            system.debug('Exception message: ' +e.getMessage());
            system.debug('Exception linenumber: ' +e.getLineNumber());
            
            if (e.getMessage().contains('The student must have a valid completed degree education history and is currently a Provisional Member.') ||
               e.getMessage().contains('The student must have a valid commenced degree education history.')) {
                // skip these as there are already error message feed for these scenario
            } else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to save, due to: ' +e.getMessage(), ''));
            }

            return null;
        }        
    }
    
    public void deleteEduHistory() {
        
        // if for any reason we are missing the reference 
        if (SelectedEduHistoryId == null) {            
            return;
        }
        
        // find the account record within the collection
        edu_Education_History__c tobeDeleted = null;
        for(EducationHistoryWrapper ehw : educationHistoryWrapperList)
            if (ehw.ehRecord.Id == SelectedEduHistoryId) {
                tobeDeleted = ehw.ehRecord;
                break;
            }
        
        //if account record found delete it
        if (tobeDeleted != null) {
            Delete tobeDeleted;
        }
        
        //refresh the data
        LoadEduHistoryData();
    }
    
    public void loadCompetencyAreaData() {
        
        competencyAreaWrapperList = new List<CompetencyAreaWrapper>();
        Set<Id> caIdSet = new Set<Id>();
        for (FP_Competency_Area__c caItem : [SELECT Id, Name, FP_Description__c, FP_Display_Only__c FROM FP_Competency_Area__c WHERE FP_Active__c = true AND FP_Parent_Competency_Area__c = null ORDER BY FP_Display_Only__c ASC, Name ASC]) {
            
            Boolean competencyAreaGained = false;
            if (gainedCompetencyAreaMap.containsKey(caItem.Id)) competencyAreaGained = true;
            
            CompetencyAreaWrapper newCAWrapper = new CompetencyAreaWrapper(caItem.Id, caItem.Name, caItem.FP_Description__c, caItem.FP_Display_Only__c, competencyAreaGained);
            competencyAreaWrapperList.add(newCAWrapper);
            caIdSet.add(caItem.Id);
            // associatedModuleMap.put(caItem.Id, '');
        }
        
        Map<Id, Set<String>> competencyAreaSubjectIDMap = new Map<Id, Set<String>>();
        
        // Build the map of Subject ID against the Competency Area ID
        for (FP_Relevant_Competency_Area__c rca : [SELECT Id, FP_Competency_Area__c, FP_Competency_Area__r.Name, FP_Competency_Area__r.FP_Display_Only__c, FP_Subject__r.LuanaSMS__Subject_Id__c FROM FP_Relevant_Competency_Area__c WHERE FP_Type__c = 'Requires' AND FP_Competency_Area__c IN :caIdSet order BY FP_Subject__r.LuanaSMS__Subject_Id__c]) {
                                                    
            if (competencyAreaSubjectIDMap.containsKey(rca.FP_Competency_Area__c)) {
                competencyAreaSubjectIDMap.get(rca.FP_Competency_Area__c).add(rca.FP_Subject__r.LuanaSMS__Subject_Id__c);
            } else {
                Set<String> newSet = new Set<String>();
                newSet.add(rca.FP_Subject__r.LuanaSMS__Subject_Id__c);
                competencyAreaSubjectIDMap.put(rca.FP_Competency_Area__c, newSet);                
            }            
        }
        
        if (!competencyAreaSubjectIDMap.isEmpty()) {
            for (Id competencyAreaId : caIdSet) {               
                String subjectIdString = '';
                if (competencyAreaSubjectIDMap.containsKey(competencyAreaId)) {
                    Set<String> currentSet = competencyAreaSubjectIDMap.get(competencyAreaId);
                    for (String sId : currentSet) {
                        subjectIdString += sId + ', ';
                    }
                }                
                if (subjectIdString.endsWith(', ')) subjectIdString = subjectIdString.removeEnd(', ');
                associatedModuleMap.put(competencyAreaId, subjectIdString);
            }                        
        } 
    }
    
    public void loadEduHistoryData() {
        educationHistoryWrapperList = new List<EducationHistoryWrapper>();
        attachmentMap = new Map<Id, List<Attachment>>();
        // for (edu_Education_History__c eh : [SELECT Id, FP_Contact__c, Application__c, Degree_Name__c, Degree_Status__c, University_Name__c, University_Status__c FROM edu_Education_History__c WHERE FP_Contact__c = :passedContactID]) {
        Set<Id> eduHistorySet = new Set<Id>();
        for (edu_Education_History__c eh : [SELECT Id, FP_Contact__c, Application__c, Degree_Name__c, Degree_Status__c, University_Name__c, University_Status__c, FP_Year_of_Commence__c, FP_Year_of_Finish__c, FP_Evidence_Sighted__c FROM edu_Education_History__c WHERE FP_Contact__c = :passedContactID]) {
            eduHistorySet.add(eh.Id);
            EducationHistoryWrapper newEHWrapper = new EducationHistoryWrapper(eh);
            educationHistoryWrapperList.add(newEHWrapper);
        }
        
        // Search for all the related Attachment
        for (Attachment  att : [SELECT Id, Name, ParentId FROM Attachment WHERE ParentId IN :eduHistorySet]) {
            if (attachmentMap.containsKey(att.ParentId)) {
                attachmentMap.get(att.ParentId).add(att);
            } else {
                List<Attachment> attachmentList = new List<Attachment>();
                attachmentList.add(att);
                attachmentMap.put(att.ParentId, attachmentList);
            }
        }
        
        // Insert empty attachment List to the Attachment Map for Education History with no attachment
        for (EducationHistoryWrapper ehw : educationHistoryWrapperList) {
            if (!attachmentMap.containsKey(ehw.ehRecord.Id)) attachmentMap.put(ehw.ehRecord.Id, new List<Attachment>());
        }
        
        system.debug('attachmentMap: ' +attachmentMap);
    }
    
    @TestVisible
    class EducationHistoryWrapper {
        
        public edu_Education_History__c ehRecord {get;set;}        
        
        // public EducationHistoryWrapper(edu_Education_History__c eh, Boolean editEnabled, Boolean deleteEnabled) {
        public EducationHistoryWrapper(edu_Education_History__c eh) {
            ehRecord = eh;            
        }
    }
    
    public PageReference cancel() {
        return new ApexPages.StandardController(currentAccount).view();
    }

    @TestVisible
    class CompetencyAreaWrapper {

        public Boolean caChecked {get;set;}

        public Id competencyAreaID {get;set;}
        public String competencyAreaName {get;set;}
        public String competencyAreaDescription {get;set;}
        public Boolean selectionDisabled {get;set;}
        public Boolean displayOnly {get;set;}

        // public CompetencyAreaWrapper(FP_Competency_Area__c ca, Map<Id, FP_Gained_Competency_Area__c> gcaMap) {
        public CompetencyAreaWrapper(Id caId, String caName, String caDescription, Boolean forDisplay, Boolean caGained) {
            
            competencyAreaID = caId;
            competencyAreaName = caName;
            competencyAreaDescription = caDescription;
            
            selectionDisabled = false;
            if (caGained) {
                caChecked = true;
                selectionDisabled = true;
            } else {
                caChecked = false;
                selectionDisabled = false;
            }
            
            if (forDisplay) {
                displayOnly = true;
            } else {
                displayOnly = false;
            }
        }
    }
}