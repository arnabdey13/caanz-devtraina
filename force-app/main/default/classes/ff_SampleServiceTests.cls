@IsTest(seeAllData=false)
private class ff_SampleServiceTests {

    @IsTest
    private static void testSearch() {
        List<Map<String, Object>> results = ff_SampleService.search('Account', 'Name', 'Tes', null);
        System.assertEquals(2, results.size(), 'two results returned');
    }

    @IsTest
    private static void testUpload() {
        Id accountId = [SELECT Id FROM Account LIMIT 1].Id;
        Id attachmentId = ff_SampleService.uploadChunk(accountId, '', 'foo.txt', '', 'text/plain');
        System.debug(attachmentId);
    }

    @IsTest
    private static void testLoadAndSave() {
        ff_ReadData rd = ff_SampleService.load();
        System.debug(rd);

        ff_WriteData wd = new ff_WriteData();
        wd.records = new Map<String, SObject>{
                'INSERT-Contact1' => new Contact(
                        FirstName = 'Brendon',
                        LastName = 'McCullum'
                )
        };
        ff_WriteResult wr = ff_SampleService.save(JSON.serialize(wd));
        System.assertEquals(0, wr.fieldErrors.size(), 'No field errors during save');
        System.assertEquals(0, wr.recordErrors.size(), 'No record errors during save');
    }

    private static final String ACCOUNT_NAME = 'Test Account';
    private static final String CAMPAIGN_NAME = 'Test Campaign';

    @TestSetup
    private static void setup() {
        new ff_TestDataAccountBuilder().name(ACCOUNT_NAME)
                .billingAddress('1 Market St', 'San Francisco', 'California', '94105', 'United States')
                .create(1, true);
        new ff_TestDataCampaignBuilder().name(CAMPAIGN_NAME).create(1, true);
    }
}