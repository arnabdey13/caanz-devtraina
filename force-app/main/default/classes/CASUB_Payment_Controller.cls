/*------------------------------------------------------------------------------------
Author:        Mayukhman
Company:       Tech Mahindra
Description:   This Controller will be used by CASUB Stories, related to Payment.
History
Date            Author             Comments
--------------------------------------------------------------------------------------
24-04-2019      Mayukhman     Initial Release
------------------------------------------------------------------------------------*/
public with sharing class CASUB_Payment_Controller {
    private static List<String> FIELDS_ACCOUNT;
    private static List<String> FIELDS_CONTACT;
    private static List<String> FIELDS_SUBSCRIPTION;
    
    static{
        FIELDS_ACCOUNT = new List<String>{'Membership_Class__c','BASC__c','Customer_ID__c'};
            FIELDS_CONTACT = new List<String>{'Email', 'MobilePhone', 'HomePhone','OtherPhone', 'Birthdate','OtherCountry'};
                FIELDS_SUBSCRIPTION = new List<String>{'Name', 'Account__c', 'Fiscal_Year__c', 'Contact_Details_URL__c', 
                    'Contact_Details_Status__c', 'Obligation_URL__c', 'Obligation_Status__c', 'Payment_URL__c', 
                    'Sales_Order_Status__c', 'Sales_Order__c', 'Contact_Details_Sub_Component__c', 'Obligation_Sub_Components__c', 'Year__c', 'Member_Age__c'};
                        }
    
    @AuraEnabled public Order userRelatedOrder{get;set;}
    @AuraEnabled public List<OrderItem> userRelatedOrderItems{get;set;}
    @AuraEnabled public Account personAccount{get;set;}
    @AuraEnabled public Subscription__c subscription{get;set;}
    
    /* Method to get the person account information and related Order and order item */
    @AuraEnabled
    public static CASUB_Payment_Controller getUserRelatedOrder(){
        
        CASUB_Payment_Controller paymentContrl = new CASUB_Payment_Controller();
        
        /* Code to get the User's related order and order products */
        Id recordTypeSubscription = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        User userRec = [SELECT AccountId FROM User WHERE ID =: UserInfo.getUserId()] ;
        String accIdVal = userRec.AccountId ;
        List<Order> lstOrder = new List<Order>([SELECT Id, OrderNumber,TotalAmount,AccountId,CurrencyISOCode,NetSuite_Order_ID__c,Netsuite_Revenue_Status__c  FROM ORDER WHERE (Status = 'Activated' OR Status = 'Pending')  AND RecordTypeId =: recordTypeSubscription AND AccountId =: accIdVal ORDER BY CreatedDate DESC LIMIT 1 ]);
        if(lstOrder.size() > 0){
            paymentContrl.userRelatedOrder = lstOrder[0];
            paymentContrl.userRelatedOrderItems = [SELECT Id, Product2Id,Product2.Name,Description,CurrencyISOCode,Product2.CurrencyISOCode,Product2.Description, OrderId, PricebookEntryId, OriginalOrderItemId, AvailableQuantity, Quantity, UnitPrice, 
                                                   ListPrice, TotalPrice, NetSuite_Pricing_Level__c, NetSuite_Product_ID__c
                                                   FROM OrderItem WHERE OrderId =: lstOrder[0].Id];  
        }
        else
            paymentContrl.userRelatedOrderItems = new List<OrderItem>();
        paymentContrl.personAccount = getPersonAccountData(new Account());
        String personAccountId = paymentContrl.personAccount.Id;    
        paymentContrl.subscription = getSubscriptionRecord(personAccountId);   
        return paymentContrl;
    }
    
    
    @AuraEnabled
    public static Subscription__c updateSubscriptionPayment(sobject userSubscriptionToUpdate, string personAccountId){
        Subscription__c subscription = (Subscription__c) userSubscriptionToUpdate;
        system.debug('==userSubscriptionToUpdate==' + userSubscriptionToUpdate);
        update subscription;
        
        if(personAccountId != null && personAccountId != ''){
            
            String currentYear = getCurrentYear();
            List<Questionnaire_Response__c> questionnaireResponseList = new List<Questionnaire_Response__c>([Select Id,Account__c,Status__c,Lock__c ,Date__c
                                                                                                             from Questionnaire_Response__c 
                                                                                                             where Account__c =: personAccountId AND Year__c = : currentYear]);
            if(questionnaireResponseList.size() > 0){
                questionnaireResponseList[0].Status__c = 'Submitted';
                questionnaireResponseList[0].Lock__c = true;
                questionnaireResponseList[0].Date__c = System.today();
                
                update questionnaireResponseList;
            }
        }
        return subscription;
    }
    @AuraEnabled
    public static String getEncryptedParam(string customerId,string salesOrderId){
        String finalString = '';
        // Value in system|country|salesOrder|memberKey|utcMilliseconds
        string systemParam;
        if (runningInASandbox()) 
            systemParam = System.Label.CASUB_Encryption_SandboxParam;
        else
            systemParam = System.Label.CASUB_Encryption_ProductionParam;
        string countryParam = [Select Id,Name,Netsuite_site_destination__c from User where Id=:UserInfo.getUserId()].Netsuite_site_destination__c;
        string salesOrder = salesOrderId;
        string customerKey = customerId;
        
        finalString = systemParam + '|' + countryParam + '|' + salesOrder + '|' + customerKey;
        
        Blob beforeblob = Blob.valueOf(finalString);
        string param64Encode = EncodingUtil.base64Encode(beforeblob);
        String paramURLencoded = EncodingUtil.urlEncode(param64Encode, 'UTF-8');
        return paramURLencoded;
    }
    /*
    public static string encrypt(string plainText){
        CASUB_NetSuite_PaymentEncryption__c encryptCodeCS = CASUB_NetSuite_PaymentEncryption__c.getInstance('NetSuiteKey');

        String string256Key = encryptCodeCS.Secret_Key__c;//Store this in Custom settings
        blob key = EncodingUtil.base64Decode(string256Key);
        Blob initializationVector = Blob.valueOf(encryptCodeCS.Initialization_Vector__c);
        blob textBlob = blob.valueOf(plainText);
        blob encryptedData = Crypto.encrypt('AES256', key, initializationVector, textBlob);                                            
        return EncodingUtil.base64Encode(encryptedData); 
	
    }
    
    public static string encryptWithManagedIV(string plainText){
        CASUB_NetSuite_PaymentEncryption__c encryptCodeCS = CASUB_NetSuite_PaymentEncryption__c.getInstance('NetSuiteKey');

        String string256Key = encryptCodeCS.Secret_Key__c;//Store this in Custom settings
        blob key = EncodingUtil.base64Decode(string256Key);
        blob textBlob = blob.valueOf(plainText);
        blob encryptedData = Crypto.encryptWithManagedIV('AES256', key, textBlob);                                            
        return EncodingUtil.base64Encode(encryptedData); 
           
    }
*/
    /* Get person account related to login User*/
    private static Account getPersonAccountData(Account personAccount){
        String query = 'SELECT Id, PersonContactId, ' + String.join(FIELDS_ACCOUNT, ',');
        query +=  ',Person' + String.join(FIELDS_CONTACT, ',Person') + ' FROM Account';
        query += ' WHERE Id IN (SELECT AccountId FROM User WHERE Id =' + '\'' + UserInfo.getUserId() + '\')';
        System.debug('*** Person Account read - '+query);
        List<Account> accounts = Database.query(query);
        if(!accounts.isEmpty()){
            return accounts[0];
        }
        else
            return null;        
    }
    public static Boolean runningInASandbox() {
        return [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }
    private static Subscription__c getSubscriptionRecord(String personAccountId){    
        
        String currentYear = getCurrentYear();
        String query = 'SELECT Id, ' + String.join(FIELDS_SUBSCRIPTION, ',');
        query += ' FROM Subscription__c';
        query += ' WHERE Account__c =' + '\'' + personAccountId + '\'';
        query += ' AND Year__c = '+ '\'' + currentYear + '\'';
        query += ' LIMIT 1';
        System.debug('*** Subscription__c  read - '+query);
        List<Subscription__c> subscriptions = Database.query(query);
        if(subscriptions.size() > 0)
        return subscriptions.get(0);
        else
        return null;
    }
    
    public static String getCurrentYear() {
        String currentYear='';
        List<CASUB_Mandatory_Notification_Config__mdt> casub_Mandatory_Notification_ConfigList = [select Current_Year__c from CASUB_Mandatory_Notification_Config__mdt where DeveloperName = 'Current_Year'];
        if(casub_Mandatory_Notification_ConfigList!=null && casub_Mandatory_Notification_ConfigList.size()>0){
            currentYear = casub_Mandatory_Notification_ConfigList.get(0).Current_Year__c;             
        }
        return currentYear;
    }
}