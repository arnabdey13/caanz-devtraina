/**
    Developer: WDCi (kh)
    Development Date:24/11/2016
    Task: Test class for AAS_DataUploadFormatManualController
    

**/
@isTest(seeAllData=false)
private class AAS_Test_DataUploadFormatManualCtl {

    final static String contextLongName = 'AAS_Test_DataUploadFormatManualCtl_Test';
    final static String contextShortName = 'adum';
    
    public static AAS_Course_Assessment__c courseAss;
    
    static void initial(){
        
        Luana_DataPrep_Test dataPrepUtil = new Luana_DataPrep_Test();
        AAS_DataPrep_Test aasdataPrepUtil = new AAS_DataPrep_Test();
        
        Map<String, Id> recordTypeMap = aasdataPrepUtil.getRecordTypeMap('Exam');
        
        //Training Org
        LuanaSMS__Training_Organisation__c trainOrg = dataPrepUtil.createNewTraningOrg(contextLongName, contextLongName, contextShortName, 'address line 1', 'address loc 1', '5000');
        insert trainOrg;
        
        //Program
        LuanaSMS__Program__c prog = dataPrepUtil.createNewProgram(contextShortName, contextLongName, 'Confirmed', 'Graduate diploma', 'Nationally accredited qualification specified in a national training package');
        insert prog;
        
         //Product
        List<Product2> prodList = new List<Product2>();
        prodList.add(dataPrepUtil.createNewProduct('FIN_AU_' + contextShortName, 'AU0001'));
        prodList.add(dataPrepUtil.createNewProduct('FIN_NZ_' + contextShortName, 'NZ0001'));
        prodList.add(dataPrepUtil.createNewProduct('FIN_INT_' + contextShortName, 'INT0001'));
        insert prodList;
        
        //Program Offerings
        LuanaSMS__Program_Offering__c poAM = dataPrepUtil.createNewProgOffering('PO_AM_' + contextShortName, dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), prog.Id, trainOrg.Id, null, null, null, 1, 1);
        insert poAM;
        
        //Course
        LuanaSMS__Course__c courseAM = dataPrepUtil.createNewCourse('AAA216', poAM.Id, dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        courseAM.LuanaSMS__Allow_Online_Enrolment__c = true;
        insert courseAM;
        
        //CA
        courseAss = aasdataPrepUtil.createCourseAssessment(courseAM.Id, 'CA_' + contextShortName);
        insert courseAss;
        
        //ASS
        AAS_Assessment_Schema_Section__c ass = aasdataPrepUtil.createAsessmentSchemaSection(courseAss.Id, 'ASS_' + contextShortName, recordTypeMap.get('AAS_Assessment_Schema_Section__c:Exam_Assessment'));
        ass.AAS_Passing_Mark__c = 40;
        ass.AAS_Cohort_Adjustment__c = 2;
        insert ass;
        
        List<AAS_Assessment_Schema_Section_Item__c> assiList = new List<AAS_Assessment_Schema_Section_Item__c>();
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA111 Q #1'));
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA111 Q #2'));
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA111 Q #3'));
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA111 Q #4'));
        insert assiList;
        
        //SA
        AAS_Student_Assessment__c sa = aasdataPrepUtil.createStudentAssessment(courseAss.Id);
        insert sa;
        
        //SAS
        AAS_Student_Assessment_Section__c sas = aasdataPrepUtil.createStudentAssessmentSection(sa.Id, recordTypeMap.get('AAS_Student_Assessment_Section__c:Exam_Assessment_SAS'));
        insert sas;
        
        //SASI
        List<AAS_Student_Assessment_Section_Item__c> sasiList = new List<AAS_Student_Assessment_Section_Item__c>();
        for(AAS_Assessment_Schema_Section_Item__c assi: assiList){
            sasiList.add(aasdataPrepUtil.createStudentAssessmentSectionItem(sas.Id, recordTypeMap.get('AAS_Student_Assessment_Section_Item__c:Exam_Assessment_SASI'), assi.Id));
        }
        insert sasiList;
        
        
    }
    
    public static testMethod void testMain1() { 
        
        initial();
        
        Test.startTest();
            
            apexpages.currentpage().getparameters().put('id', courseAss.Id);
            AAS_DataUploadFormatManualController controller = new AAS_DataUploadFormatManualController() ;
            
            //pageRef.getParameters().put('id', courseAss.Id);
            
        Test.stopTest();
    }
    
 
}