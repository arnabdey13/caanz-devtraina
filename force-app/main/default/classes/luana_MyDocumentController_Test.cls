/**
    Developer: WDCi (kh)
    Development Date:13/07/2016
    Task: Luana Test class for luana_MyDocumentController
    
    Change History
    //LCA-921 23/08/2019 WDCi - KH: Add person account email
**/
@isTest(seeAllData=false)
public class luana_MyDocumentController_Test{
    
    private static String classNamePrefixLong = 'luana_MyDocumentController_Test';
    private static String classNamePrefixShort = 'lmdc';
    private static map<String, Id> commProfIdMap {get; set;}
    
    public static User memberUser {get; set;}
    public static Account memberAccount {get; set;}
    public static luana_CommUserUtil userUtil {get; set;} 
    public static Luana_DataPrep_Test testDataGenerator;    
    
    //LCA-921
    public static void initial(){
        //initialize
        testDataGenerator = new Luana_DataPrep_Test();
        
        //Create all the custom setting
        insert testDataGenerator.prepLuanaExtensionSettingCustomSettings();
        insert testDataGenerator.createLuanaConfigurationCustomSetting();
        
        //Create user with Member and Employer community access
        memberAccount = testDataGenerator.generateNewApplicantAcc('Joe_' + classNamePrefixShort, classNamePrefixLong, 'Full_Member');
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.Assessible_for_CA_Program__c = true;
        memberAccount.PersonEmail = 'joe_1_' +classNamePrefixShort+'@gmail.com';//LCA-921
        insert memberAccount;
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
    }
    
    public static void setup(){

        //LCA-921
        memberUser = [Select Id, Email, FirstName, LastName, UserName, Name from User Where Email =: 'joe_1_' + classNamePrefixShort+'@gmail.com' limit 1];
        //memberUser = testDataGenerator.generateNewApplicantUser(classNamePrefixLong, classNamePrefixshort, memberAccount, commProfIdMap.get('Customer Community Login User'));
        //insert memberUser;
        
        userUtil = new luana_CommUserUtil(memberUser.Id);
        
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
       
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defEmployerPermSetId.value__c));
        
        System.runAs(memberUser){
            insert psas;
        }
    }
    
    static testMethod void testCAMasterEnrolmentWizardTest() {
        
        Test.startTest();
            initial();
        Test.stopTest();
        
        setup();
        
        //Test CA Program
        system.runAs(memberUser){
            luana_MyDocumentController myDoc = new luana_MyDocumentController();
            
            myDoc.forwardToAuthPage();
            myDoc.getDomainUrl();
            myDoc.getDocRepoAttachments();
        }
        
    }
}