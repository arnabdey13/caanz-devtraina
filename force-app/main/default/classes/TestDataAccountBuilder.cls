@IsTest @TestVisible
private class TestDataAccountBuilder {

    private Id recordTypeId;
    private String recordTypeName;
    private String name;
    private String salutation;
    private String firstName;
    private String lastName;
    private String email;
    private String status;
    private String memberOf;
    private String website;
    private String type;
    private Boolean gaa;
    private String membershipClass;
    private String designation;
    private String memberId;

    private List<String> billingAddress;
    private String billingAddressDPId;
    private Boolean billingAddressValidated;

    @TestVisible
    private TestDataAccountBuilder(String recordType) {
        this.recordTypeName = recordType;
        this.recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(recordType).getRecordTypeId();
    }

    @TestVisible
    private TestDataAccountBuilder name(String name) {
        this.name = name;
        return this;
    }

    @TestVisible
    private TestDataAccountBuilder salutation(String salutation) {
        this.salutation = salutation;
        return this;
    }

    @TestVisible
    private TestDataAccountBuilder firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    @TestVisible
    private TestDataAccountBuilder lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    @TestVisible
    private TestDataAccountBuilder email(String email) {
        this.email = email;
        return this;
    }

    @TestVisible
    private TestDataAccountBuilder type(String type) {
        this.type = type;
        return this;
    }

    @TestVisible
    private TestDataAccountBuilder status(String status) {
        this.status = status;
        return this;
    }

    @TestVisible
    private TestDataAccountBuilder memberOf(String memberOf) {
        this.memberOf = memberOf;
        return this;
    }

    @TestVisible
    private TestDataAccountBuilder website(String website) {
        this.website = website;
        return this;
    }

    @TestVisible
    private TestDataAccountBuilder gaa(Boolean gaa) {
        this.gaa = gaa;
        return this;
    }

    @TestVisible
    private TestDataAccountBuilder membershipClass(String membershipClass) {
        this.membershipClass = membershipClass;
        return this;
    }

    @TestVisible
    private TestDataAccountBuilder designation(String designation) {
        this.designation = designation;
        return this;
    }

    @TestVisible
    private TestDataAccountBuilder memberId(String memberId) {
        this.memberId = memberId;
        return this;
    }

    @TestVisible
    private TestDataAccountBuilder billingAddress(String street, String city, String state, String postCode, String countryCode) {
        this.billingAddress = new List<String>{
                street, city, state, postCode, countryCode
        };
        return this;
    }
    @TestVisible
    private TestDataAccountBuilder billingAddressQAS(String dpId, Boolean validated) {
        this.billingAddressDPId = dpId;
        this.billingAddressValidated = validated;
        return this;
    }

    @TestVisible
    private List<Account> create(Integer count, Boolean persisted) {
        List<Account> results = new List<Account>();
        
        
        for (Integer i = 0; i < count; i++) {
        Account a;
            
            if(this.recordTypeName == 'Business Account'){
            a = new Account(
                    RecordTypeId = this.recordTypeId,
                    Membership_Class__c = this.membershipClass,
                    Designation__c = this.designation,
                    Member_ID__c = memberId,
                    Status__c = this.status,
                    Member_Of__c = this.memberOf,
                    Website = this.website,
                    BillingStreet = this.billingAddress[0],
                    BillingCity = this.billingAddress[1],
                    BillingState = this.billingAddress[2],
                    BillingPostalCode = this.billingAddress[3],
                    BillingCountryCode = this.billingAddress[4],
                    
                    /*PersonOtherStreet= this.billingAddress[0],
                    PersonOtherCity= this.billingAddress[1],
                    PersonOtherState= this.billingAddress[2],
                    PersonOtherPostalCode= this.billingAddress[3],
                    //BillingCountryCode = this.billingAddress[4], 
                    PersonOtherCountry='Australia', */
                    
                    Billing_DPID__c = this.billingAddressDPId
            );
            }
            
            if(this.recordTypeName == 'Person Account'){
            a = new Account(
                    RecordTypeId = this.recordTypeId,
                    Membership_Class__c = this.membershipClass,
                    Designation__c = this.designation,
                    Member_ID__c = memberId,
                    Status__c = this.status,
                    Member_Of__c = this.memberOf,
                    Website = this.website,
                    BillingStreet = this.billingAddress[0],
                    BillingCity = this.billingAddress[1],
                    BillingState = this.billingAddress[2],
                    BillingPostalCode = this.billingAddress[3],
                    BillingCountryCode = this.billingAddress[4],
                    
                    PersonOtherStreet= this.billingAddress[0],
                    PersonOtherCity= this.billingAddress[1],
                    PersonOtherState= this.billingAddress[2],
                    PersonOtherPostalCode= this.billingAddress[3],
                    //BillingCountryCode = this.billingAddress[4], 
                    PersonOtherCountry='Australia', 
                    
                    Billing_DPID__c = this.billingAddressDPId
            );
            }
            
            
            if (this.salutation != null) {
                a.Salutation = this.salutation;
            }
            if (this.billingAddressValidated != null) {
                a.PersonBillingAddressValidatedByQAS__c = this.billingAddressValidated;
            }
            if (this.name != null){
                a.Name = this.name + (count == 1 ? '' : '' + i);
            }
            if (this.firstName != null){
                a.FirstName = this.firstName;
            }
            if(this.firstName != null){
                a.LastName = this.lastName;
            }
            if(this.email != null){
                a.PersonEmail = this.email;
            }
            if(this.gaa != null){
                a.GAA_member__c = this.gaa;
            }
            if(this.type != null){
                a.Type = this.type;
            }
            results.add(a);
        }
        System.debug(results);
        if (persisted) insert results;
        return results;
    }

}