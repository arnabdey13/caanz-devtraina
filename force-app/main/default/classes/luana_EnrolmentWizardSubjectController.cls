/**
    Developer: WDCi (Lean)
    Development Date: 05/04/2016
    Task #: Enrollment wizard - Controller for Subject selection
**/

public without sharing class luana_EnrolmentWizardSubjectController extends luana_EnrolmentWizardObject{
    
    luana_EnrolmentWizardController stdController;
    
    public List<String> selectedSubjectIds {set; get;}
    
    public luana_EnrolmentWizardSubjectController(luana_EnrolmentWizardController stdController){
        this.stdController = stdController;
        selectedSubjectIds = new List<String>();
        
    }
    
    public luana_EnrolmentWizardSubjectController(){
        
    }
    
    public PageReference subjectNext(){
        
        /*Integer countElective = 0;
        stdController.selectedProgramOfferingSubjects = new List<LuanaSMS__Program_Offering_Subject__c>();
        
        if(stdController.selectedCourse != null && stdController.selectedProgramOffering != null){
            for(luana_ProgramOfferingSubjectWrapper selectedSubject: programOfferingSubjects) {
                if(selectedSubject.isSelected) {
                    if(!selectedSubject.isCore){
                        countElective ++;
                    }
                    
                    stdController.selectedProgramOfferingSubjects.add(selectedSubject.pos);
                }
            }
        }
        
        if(countElective >= stdController.selectedProgramOffering.LuanaSMS__Number_of_Required_Electives__c) {
            
            stdController.setProgramOfferingPricing();
            
            if(stdController.selectedPricebookEntry != null){
                
                PageReference pageRef = stdController.getTargetDetailsPage(stdController.selectedProgramOffering.Wizard_Details_Page_Name__c);
                stdController.skipValidation = true;
                
                return pageRef;
            } else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Unfortunately, we are not able to determine the course price. Please try again later or contact our support if the problem persists.'));
            }
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'You must select at least ' + stdController.selectedProgramOffering.LuanaSMS__Number_of_Required_Electives__c + ' elective/s before you can proceed.'));
        }
        
        return null;*/
        
        stdController.skipValidation = true;
        return stdController.getSubjectNextPage();
    }
    
    public PageReference subjectBack(){
        
        stdController.selectedCourseId = null;
        stdController.selectedCourse = null;
                    
        PageReference pageRef = Page.luana_EnrolmentWizardCourse;
        stdController.skipValidation = true;
                
        return pageRef;
    }
    
    public List<luana_ProgramOfferingSubjectWrapper> getSubjectOptions() {
    
        /*if(programOfferingSubjects == null || (programOfferingSubjects != null && programOfferingSubjects.get(0).pos.LuanaSMS__Program_Offering__c != stdController.selectedProgramOffering.Id)){
            programOfferingSubjects = new List<luana_ProgramOfferingSubjectWrapper>();
            
            if(stdController.selectedCourseId != null && stdController.selectedCourse != null && stdController.selectedProgramOffering != null){
                
                boolean autoSelection = stdController.isAutoSubjectSelection(stdController.selectedProgramOffering.Subject_Selection_Option__c);
                
                for(LuanaSMS__Program_Offering_Subject__c pos : stdController.selectedProgramOffering.LuanaSMS__Program_Offering_Subjects__r) {
                    if(pos.LuanaSMS__Essential__c == 'Core'){
                        programOfferingSubjects.add(new luana_ProgramOfferingSubjectWrapper(true, true, true, pos));
                    } else {
                        programOfferingSubjects.add(new luana_ProgramOfferingSubjectWrapper((autoSelection ? true : false), false, (autoSelection ? true : false) , pos));
                    }
                }
            }
        }
        
        return programOfferingSubjects;*/
        return stdController.populateSubjectOptions();
    }
    
}