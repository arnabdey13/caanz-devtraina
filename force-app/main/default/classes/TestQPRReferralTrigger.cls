@isTest
public class TestQPRReferralTrigger {
    @testSetup
    Static void setupTestData(){
         RecordType typeBusiness = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Business_Account' LIMIT 1];
        RecordType typeMember = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Full_Member' LIMIT 1];
        List<Account> listAccounts = new List<Account>();
        Account personAccount = new Account();
        personAccount.RecordType = typeMember;
        personAccount.FirstName = 'Test';
        personAccount.LastName = 'Practice Contact';
        personAccount.Member_Of__c = 'ICAA';
        personAccount.Gender__c = 'Male';
        personAccount.Salutation = 'Mr';
        personAccount.PersonEmail = 'testpracticecontact@example.com';
        personAccount.Create_Portal_User__c = false;
        personAccount.Communication_Preference__c= 'Home Phone';
        personAccount.PersonHomePhone= '1234';
        personAccount.PersonOtherStreet= '83 Saggers Road';
        personAccount.PersonOtherCity='JITARNING';
        personAccount.PersonOtherState='Western Australia';
        personAccount.PersonOtherCountry='Australia';
        personAccount.PersonOtherPostalCode='6365'; 
        personAccount.PersonMailingCountry='Australia';
        personAccount.Affiliated_Branch_Country__c='Australia';
          personAccount.Is_QPR_Referral_Contact__c=true;
        listAccounts.add(personAccount);
          Account acct = new Account();
        acct.RecordType = typeBusiness;
        acct.Name = 'Test Account.ABC';
        acct.Type = 'Unallocated';
        acct.Phone = '6421771557';
        acct.Member_Of__c = 'ICAA';
        acct.BillingStreet = '33 Customhouse Quay';
        acct.BillingCity = 'Wellington';
        acct.BillingCountry = 'New Zealand';
        acct.Review_Contact__c = personAccount.Id;
        

        listAccounts.add(acct);   
        insert listAccounts;
        List<QPR_Referral__c> qprReferralList  = new List<QPR_Referral__c>();
        QPR_Referral__c qprReferralRecord1  = new QPR_Referral__c();
        qprReferralRecord1.Referral_Body__c = 'CA Board';
       	qprReferralRecord1.Review_referred_date__c =Date.Today();
        qprReferralRecord1.Referral_due_date__c = Date.Today();
       qprReferralRecord1.Other_Referral_Body__c ='test qprref trigger1';
      //   qprReferralRecord1.Referred_Account__c =acct.Id;
        qprReferralList.add(qprReferralRecord1);
        QPR_Referral__c qprReferralRecord2  = new QPR_Referral__c();
        qprReferralRecord2.Referral_Body__c = 'CA Board';
       	qprReferralRecord2.Review_referred_date__c =Date.Today();
        qprReferralRecord2.Referral_due_date__c = Date.Today();
       qprReferralRecord2.Other_Referral_Body__c ='test qprref trigger2';
        //qprReferralRecord2.Referral_Contact__c=personAccount.Id;
        qprReferralList.add(qprReferralRecord2);
        
        insert qprReferralList;
    }
 @isTest
    Static void testCreateQPRReferral(){
        List<QPR_Referral__c> qprReferralList  = new List<QPR_Referral__c>();
        Account personAccount = [SELECT Id, FirstName, LastName FROM Account WHERE PersonEmail = 'testpracticecontact@example.com' LIMIT 1];
        Account businessAccount = [SELECT Id, FirstName, LastName FROM Account WHERE Name = 'Test Account.ABC' LIMIT 1];
           
       for( QPR_Referral__c qprReferralRecord1 : [SELECT Id, Other_Referral_Body__c, Referral_Contact__c 
                                                  FROM QPR_Referral__c WHERE Other_Referral_Body__c ='test qpref trigger1' LIMIT 1])
       {
            qprReferralRecord1.Referral_Contact__c = personAccount.Id;
             qprReferralList.add(qprReferralRecord1);
       }
        QPR_Referral__c qprReferral  = new QPR_Referral__c();
        qprReferral.Referral_Body__c = 'CA Board';
       	qprReferral.Review_referred_date__c =Date.Today();
        qprReferral.Referral_due_date__c = Date.Today();
        qprReferral.Referral_Contact__c = personAccount.Id;
        qprReferralList.add(qprReferral);
       
        upsert  qprReferralList;
    }
     @isTest
    Static void testUpdateQPRReferral(){
        List<QPR_Referral__c> qprReferralList  = new List<QPR_Referral__c>();
        Account personAccount = [SELECT Id, FirstName, LastName FROM Account WHERE PersonEmail = 'testpracticecontact@example.com' LIMIT 1];
        Account businessAccount = [SELECT Id, FirstName, LastName FROM Account WHERE Name = 'Test Account.ABC' LIMIT 1];
     
       for(QPR_Referral__c qprReferralRecord2 : [SELECT Id, Other_Referral_Body__c, Referred_Account__c
                                                 FROM QPR_Referral__c WHERE Other_Referral_Body__c ='test qprref trigger2' LIMIT 1])
       {
             qprReferralRecord2.Referred_Account__c = businessAccount.Id;
          update qprReferralRecord2;
          //  qprReferralList.add(qprReferralRecord2);
       }
      QPR_Referral__c qprReferralRecord_5  = new QPR_Referral__c();
        qprReferralRecord_5.Referral_Body__c = 'CA Board';
       	qprReferralRecord_5.Review_referred_date__c =Date.Today();
        qprReferralRecord_5.Referral_due_date__c = Date.Today();
        qprReferralRecord_5.Referred_Account__c = businessAccount.Id;
        qprReferralList.add(qprReferralRecord_5);
        upsert  qprReferralList;
    }
}