/*
    Developer: WDCi (KH)
    Date: 28/April/2016
    Task #: Test class for luana_SObjectOverrideController
    
    Change History
    LCA-921 23/08/2019 WDCi - KH: Add person account email
*/

@isTest(SeeAllData=false)
private class  luana_SObjectOverrideCtlTest {
    
    public static Account member;
    public static User memberUser;
    public static Luana_DataPrep_Test dataPrep;
    public static luana_CommUserUtil userUtil {get; set;} 
    
    public static List<Attachment> attachments {get; set;}
    
    private static String classNamePrefixLong = 'luana_SObjectOverrideCtlTest';
    private static String classNamePrefixShort = 'lsoc';
    private static map<String, Id> commProfIdMap {get; set;}
    public static Account memberAccount {get; set;}
    
    //LCA-921
    public static void initial(){
        //initialize
        dataPrep = new Luana_DataPrep_Test();
        
        //Create all the custom setting
        insert dataPrep.prepLuanaExtensionSettingCustomSettings();
        insert dataPrep.createLuanaConfigurationCustomSetting();
        
        //Create user with Member and Employer community access
        memberAccount = dataPrep.generateNewApplicantAcc('Joe_' + classNamePrefixShort, classNamePrefixLong, 'Full_Member');
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.PersonEmail = 'joe_1_' +classNamePrefixShort+'@gmail.com';//LCA-921
        memberAccount.Communication_Preference__c= 'Home Phone';
        memberAccount.PersonHomePhone= '1234';
        memberAccount.PersonOtherStreet= '83 Saggers Road';
        memberAccount.PersonOtherCity='JITARNING';
        memberAccount.PersonOtherState='Western Australia';
        memberAccount.PersonOtherCountry='Australia';
        memberAccount.PersonOtherPostalCode='6365'; 
        insert memberAccount;
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
    }
    
    public static void setup(){
        //LCA-921
        memberUser = [Select Id, Email, FirstName, LastName, UserName, Name, ContactId from User Where Email =: 'joe_1_' + classNamePrefixShort+'@gmail.com' limit 1];
        //memberUser = testDataGenerator.generateNewApplicantUser(classNamePrefixLong, classNamePrefixshort, memberAccount, commProfIdMap.get('Customer Community Login User'));
        //insert memberUser;
        
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
       
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defEmployerPermSetId.value__c));
        list<PermissionSetAssignment> permissAssign = [Select Id from PermissionSetAssignment where AssigneeId =: memberUser.Id AND (PermissionSetId =: defMemberPermSetId.value__c OR PermissionSetId =: defEmployerPermSetId.value__c)];
        System.runAs(memberUser){
            if(permissAssign == null)
                insert psas;
        }

        userUtil = new luana_CommUserUtil(memberUser.Id);
    }
    
    static testMethod void testViewPageStdController(){
        
        
        Test.startTest();
            initial();            
        Test.stopTest();
        
        setup();
            
        //Create program
        LuanaSMS__Program__c progrm = dataPrep.createNewProgram(classNamePrefixShort+'_1', classNamePrefixLong+'_1', 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        insert progrm;
        
        System.runAs(memberUser){
                    
            ApexPages.StandardController sc = new ApexPages.StandardController(progrm);
            
            luana_SObjectOverrideController soCtl = new luana_SObjectOverrideController(sc);
            
            
            PageReference pageRef = Page.luana_OverrideProgramOfferingList;
            Test.setCurrentPage(pageRef);
            
            soCtl.gotoViewPage();
            soCtl.gotoEditPage();
            soCtl.gotoNewPage();
            //soCtl.gotoListPage();
        }
    }
    
    static testMethod void testViewPageStdSetController(){
        Test.startTest();
            initial();            
        Test.stopTest();
        
        setup();
        
        //Create program
        List<LuanaSMS__Program__c> newProgs = new List<LuanaSMS__Program__c>();
        LuanaSMS__Program__c progrm = dataPrep.createNewProgram(classNamePrefixShort+'_2', classNamePrefixLong+'_2', 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        newProgs.add(progrm);
        insert newProgs;
        
        System.runAs(memberUser){
                    
            ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(newProgs);
            luana_SObjectOverrideController soCtl = new luana_SObjectOverrideController(stdSetController);        
                    
            PageReference pageRef = Page.luana_OverrideProgramOfferingList;
            Test.setCurrentPage(pageRef);
            
            soCtl.gotoViewPage();
            soCtl.gotoEditPage();
            soCtl.gotoNewPage();
            soCtl.gotoListPage();
        }
    }
    
    static testMethod void testViewPageNonMember(){
    
        Test.startTest();
            initial();            
        Test.stopTest();
        
        setup();
        
        //Create program
        List<LuanaSMS__Program__c> newProgs = new List<LuanaSMS__Program__c>();
        LuanaSMS__Program__c progrm = dataPrep.createNewProgram(classNamePrefixShort+'_3', classNamePrefixLong+'_3', 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        newProgs.add(progrm);
        insert newProgs;


        PageReference pageRef = Page.luana_OverrideProgramOfferingList;
        pageRef.getParameters().put('id', String.valueOf(newProgs[0].Id));
        
        Test.setCurrentPage(pageRef);
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(newProgs);
        luana_SObjectOverrideController soCtl = new luana_SObjectOverrideController(stdSetController);

        soCtl.gotoViewPage();
        soCtl.gotoEditPage();
        soCtl.gotoNewPage();
        soCtl.gotoListPage();

    }
}