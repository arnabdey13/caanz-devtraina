// without sharing is required to find the email template in the save method
// this is safe since the accountId is always chosen by apex and not the client
// an alternative might be to use system.runAs when sending email
public without sharing class EditorUpdateDetailsService {

    // TODO use FLS to ensure fields being saved are valid

    private static Set<String> CONCESSIONS = new Set<String>{'Retired','Low income concession', 'Career break concession'};

    @AuraEnabled
    public static Map<String, Object> load() {
        return load(EditorPageUtils.getUsersAccountId());
    }

    // only these fields are sent to the client during load
    private static final Set<String> CLIENT_FIELDS = new Set<String>{
            'FirstName', 'LastName', 'PersonEmail', 'Member_ID__c', 'Financial_Category__c', 'Membership_Class__c',
            'PersonOtherStreet', 'PersonOtherCity', 'PersonOtherState', 'PersonOtherPostalCode', 'PersonOtherCountryCode',
            'NZICA_confirmation__pc'
    };

    @TestVisible
    private static Map<String, Object> load(Id accountId) {
        Map<String, Object> properties = new Map<String, Object>();

        Account a = [
                select Id, FirstName, LastName, PersonEmail, Membership_Class__c,
                        Member_ID__c, Financial_Category__c, CPP_Picklist__c,
                        PersonOtherStreet, PersonOtherCity, PersonOtherState, PersonOtherPostalCode, PersonOtherCountryCode, 
                        NZICA_confirmation__pc
                from Account
                where Id = :accountId
        ];

        // client sees object as generic map i.e. JSON so translate the sobject into a map so that tests can read/write
        Map<Object, Object> accountAsJSON = new Map<Object, Object>();
        for (String f : CLIENT_FIELDS) {
            accountAsJSON.put(f, a.get(f));
        }

        String concession = (String) accountAsJSON.get('Financial_Category__c');
        if (concession == NULL || !CONCESSIONS.contains(concession)) {
            accountAsJSON.put('Financial_Category__c', 'None');
        }

        properties.put('RECORD', accountAsJSON);
        properties.put('prop.CPP', isCPPFull(a));
        properties.put('prop.countriesAndStates', EditorPageUtils.getCountriesAndStates());
        return properties;
    }

    @AuraEnabled
    public static void save(Map<String, Object> properties) {
        save(properties, EditorPageUtils.getUsersAccountId());
    }

    @TestVisible
    // returns boolean indicating a Case was created to make test assertions simple
    private static boolean save(Map<String, Object> properties, Id accountId) {

        Account accountBeforeUpdate = [
                select CPP_Picklist__c, PersonOtherCountryCode, Financial_Category__c
                from Account
                where Id = :accountId
        ];
        Boolean cppWasFull = isCPPFull(accountBeforeUpdate);

        Map<Object, Object> dirtyFields = (Map<Object, Object>) properties.get('DIRTY');
        if (dirtyFields == null) { // npe protection
            dirtyFields = new Map<Object, Object>();
        }

        // convert 'None' from picklists into correct value for the sobject
        // note this is before the translation to Account but we are updating the input to the translation
        Map<Object, Object> accountFromClient = (Map<Object, Object>) properties.get('RECORD');

        if (dirtyFields.get('PersonOtherState') != null) {
            if (accountFromClient.get('PersonOtherState') == 'None') {
                accountFromClient.put('PersonOtherState', NULL);
            }
        }
        
        if (dirtyFields.get('NZICA_confirmation__pc') != null) {
            if (accountFromClient.get('NZICA_confirmation__pc') == True) {
                accountFromClient.put('NZICA_confirmation__pc', True);
            } else {
                accountFromClient.put('NZICA_confirmation__pc', False);
            }
        }
               
        if (dirtyFields.get('Financial_Category__c') != null) {
            if (accountFromClient.get('Financial_Category__c') == 'None') {
                String category = 'Standard fee applies';
                // maintain the current value of category when the client sends None i.e. don't overwrite with NULL
                if (accountBeforeUpdate.Financial_Category__c != NULL && !CONCESSIONS.contains(accountBeforeUpdate.Financial_Category__c)) {
                    category = accountBeforeUpdate.Financial_Category__c;
                }
                accountFromClient.put('Financial_Category__c', category);
            }
        }

        Account toSave = (Account) EditorPageUtils.getRecordFromSaveRequest(new Account(Id = accountId), properties);

        Boolean cppWasRelinquished = cppWasFull && dirtyFields.get('prop.CPP') == true && properties.get('prop.CPP') == false;
        if (cppWasRelinquished) {
            toSave.CPP_Removal_Date__c = System.today();
            toSave.CPP_Picklist__c = 'Removed';
        }

        System.debug('toSave: ' + toSave);
        update toSave;

        if (cppWasRelinquished) {

            Set<String> cppConfirmFields = new Set<String>{
                    'prop.CPP', 'prop.CPPRelinquishReason', 'prop.PracticeCeasedDate', 'prop.CPPConfirmIndemnity', 'prop.ConfirmCPP'
            };

            Map<Object, Object> mappings = (Map<Object, Object>) properties.get('LABEL-MAPPINGS');
            if (mappings == null) {
                throw new SObjectException('mappings missing');
            }
            String description = 'Confirmation Responses:\n\n';
            for (String q : cppConfirmFields) {
                String labelUsed = (String) mappings.get(q);
                Object answer = properties.get(q);
                description += labelUsed + ' => ' + answer + '\n';
            }

            Id queueId = [
                    select Queue.Id
                    from QueueSobject
                    where Queue.Name = 'Enrolment & Admissions Group'
            ].Queue.Id;
            Id contactId = [select PersonContactId from Account where Id = :accountId].PersonContactId;
            Case c = new Case(
                    OwnerId = queueId,
                    AccountId = accountId,
                    ContactId = contactId,
                    Status = 'New',
                    Priority = 'Medium',
                    Type = 'Membership',
                    Sub_Type__c = 'Applications',
                    Category__c = '--None--',
                    Origin = 'Web',
                    Subject = 'CPP Relinquishment',
                    Description = description);
            insert c;
        }

        return cppWasRelinquished;
    }

    private static boolean isCPPFull(Account a) {
        return a.CPP_Picklist__c == 'Full';
    }

}