public with sharing class RecordTypeCache {
	private static Map<String, RecordType> currentCache = null;
	
	static {
		refreshCache();
	}
	
	public static RecordType getRecordType(sObject sObjectType, String developerName) {
		return getRecordType( String.valueOf(sObjectType.getSObjectType()), developerName);
	}
	public static RecordType getRecordType(String sObjectType, String developerName){
		if(currentCache == null){
			refreshCache();
		}
		RecordType result = currentCache.get(developerName + '%%' + sObjectType);
		if (result == null) {
			refreshCache();
			result = currentCache.get(developerName + '%%' + sObjectType);
		}
		if (result == null) {
			throw new NewException('Unable to find RecordType for ' + sObjectType + '.' + developerName);
		}
		return result;
	}
	
	public static RecordType getRecordType(Id recordTypeId) {
		if (currentCache == null) {
			refreshCache();
		}
		
		RecordType result = currentCache.get(recordTypeId);
		if (result == null) {
			refreshCache();
			result = currentCache.get(recordTypeId);
		}
		if (result == null) {
			throw new NewException('Unable to find RecordType for ' + recordTypeId);
		}
		return result;
	}
	
	/**
	 * Get the recordTypeId for the given sObjectType and developerName (Case Sensitive)
	*/
	public static Id getId(sObject sObjectType, String developerName) {
		return getId( String.valueOf(sObjectType.getSObjectType()), developerName);
	}
	public static Id getId(String sObjectType, String developerName) {
		RecordType rt = getRecordType(sObjectType, developerName);
		if (rt != null) {
			return rt.id;
		} 
		return null;
	}
	
	private static void refreshCache() {
		Map<String, RecordType> newCache = new Map<String, RecordType>();
		
		for (RecordType rt : [ select id, name, developerName, SObjectType from RecordType where isActive=true]) {
			newCache.put(rt.developerName + '%%' + rt.SObjectType, rt);
			newCache.put(rt.Id, rt);
		}
		currentCache = newCache;
	}
}