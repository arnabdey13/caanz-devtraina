/**
    Developer: akopec
    Date:	27/03/2018
    
    Change History:
**/
@isTest(seeAllData=false)
public class SegmentationMembersTriggerHandlerTest {

	public static Boolean forceTriggerDuringTest = false;
    
    public static Account memberAccount {get; set;}
	public static Account specialistAccount {get; set;}
    public static Account businessAccount {get; set;} 
    public static Account businessAccountHist {get; set;}   
    public static Employment_History__c eh {get; set;}
    public static List<Employment_History__c> ehList {get; set;}    
    private static Boolean initiated = false;  

    public static SegmentationDataGenTest testDataGen;
    private static boolean debug = true;
 

    ////////////////////// Main Practice Lookup Prep ///////////////////////////////////////////////////////////////
    @testSetup 
    static void setup() {
        
        Reference_Main_Practices__c  deloitte = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Deloitte'
            , Search_Name__c     = 'Deloitte'
            , Full_Name__c       = 'Deloitte'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            = 'Deloitte'
        );
      
		Reference_Main_Practices__c  pwc = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'PricewaterhouseCoopers;PWC; PwC; PriceWaterHouseCoopers; PriceWaterhouseCoopers; PricewaterhousCoopers; Pricewaterhousecoopers'
            , Search_Name__c     = 'PricewaterhouseCoopers'
            , Full_Name__c       = 'PricewaterhouseCoopers'
            , Practice_Size__c   = 'Large Firm'
			, Big4__c            = 'PWC'
        );
        
		Reference_Main_Practices__c  kpmg = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'KPMG'
            , Search_Name__c     = 'KPMG'
            , Full_Name__c       = 'KPMG'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            = 'KPMG'
        );
        
		Reference_Main_Practices__c  ey = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Ernst and Young; EY; Ernst & Young'
            , Search_Name__c     = 'Ernst and Young'
            , Full_Name__c       = 'Ernst and Young'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            = 'EY'
        );
        
		Reference_Main_Practices__c  bdo = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'BDO'
            , Search_Name__c     = 'BDO'
            , Full_Name__c       = 'BDO'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            =  NULL
        );
        
		Reference_Main_Practices__c  gt = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Grant Thornton'
            , Search_Name__c     = 'Grant Thornton'
            , Full_Name__c       = 'Grant Thornton'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            =  NULL
        );        
        
        
		Reference_Main_Practices__c  vincents = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Vincents Chartered Accountants; Vincents'
            , Search_Name__c     = 'Vincents Chartered Accountants'
            , Full_Name__c       = 'Vincents Chartered Accountants'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            =  NULL
        );
        
        Reference_Main_Practices__c  boroughs = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Boroughs'
            , Search_Name__c     = 'Boroughs'
            , Full_Name__c       = 'Boroughs'
            , Practice_Size__c   = 'Medium Firm'
            , Big4__c            =  NULL
        );
        
        insert new List<Reference_Main_Practices__c>{pwc, deloitte, kpmg, ey, bdo, gt, vincents, boroughs};  
            
             
		Segmentation_Settings__c setting = new Segmentation_Settings__c();        
        setting.Name = 'SegmentationMembersUpdate';
		setting.Active__c = TRUE;
		insert setting;
  
    }
    
    
	private static void debugMaps (List <Account> newList, Map<Id, Account> newMap, List <Account> oldList, Map<Id, Account>  oldMap) 
    {
        	System.debug('Old Map: ' + oldMap);
            System.debug('New Map: ' + newMap);
            System.debug('Old List:' + oldList);
            System.debug('New List:' + newList);
    }
 
	private static void debugEHMaps (List <Employment_History__c> newList, Map<Id, Employment_History__c> newMap, 
                                     List <Employment_History__c> oldList, Map<Id, Employment_History__c>  oldMap) 
    {
        	System.debug('Old Map: ' + oldMap);
            System.debug('New Map: ' + newMap);
            System.debug('Old List:' + oldList);
            System.debug('New List:' + newList);
    }
 

    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////   
    ////////////////////////////////////////////////////////////////// TESTS ////////////////////////////////////////////////////////////////////////////////////////////////
    
    ///////////////////////  Early Career Tests With Employment /////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Case 0. Not a member anymore     
	private static testMethod void nonMemberAU_Case0(){    
        Map<Id, Account> oldMap = new Map<Id, Account>{} ;
		Map<Id, Account> newMap = new Map<Id, Account>{} ;
     
		Account acct = SegmentationDataGenTest.genFullMemberNewZealand('Smith', 'Senior Accountant', 'smith1@test.co.nz', Date.valueOf('1987-05-12') );
        insert acct;
        Account oldMemberAccountAU = [SELECT Id, Name, isPersonAccount, Membership_Type__c, Membership_Class__c, Status__c, PersonOtherCountry
                                      FROM Account WHERE Id = :acct.Id]; 

        Account newMemberAccountAU  = oldMemberAccountAU.clone(true, true, false, false);
        newMemberAccountAU.Membership_Type__c = 'Non Member';
        newMemberAccountAU.Membership_Class__c = NULL;
        
        // prepare old and new lists and maps
        oldMap.put(oldMemberAccountAU.Id, oldMemberAccountAU);
        newMap.put(newMemberAccountAU.Id, newMemberAccountAU);
        
		List <Account> newList = new List<Account>(newMap.values());
		List <Account> oldList = new List<Account>(oldMap.values());
        
		if (debug) debugMaps(newList, newMap, oldList, oldMap);
 
		Map<Id, String> results = SegmentationMembersTriggerHandler.handleSegmentsAfterAccountUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('Test Non Member Record: ' + results);
        System.assert(results.size() > 0);
       	Set<Id> keys = results.keySet();
        for (Id key : keys)
			System.assertEquals('Case 0', results.get(key) );
	}   
    
    // Case 1a. New record     
	private static testMethod void newMemberNoEmploymentAU_Case1(){    
        Map<Id, Account> oldMap;
		Map<Id, Account> newMap = new Map<Id, Account>{} ;
     
		Account acct = SegmentationDataGenTest.genFullMemberNewZealand('Smith', 'Senior Accountant', 'smith1@test.co.nz', Date.valueOf('1987-05-12') );
        insert acct;
        Account newMemberAccountAU = [SELECT Id, Name, isPersonAccount, Membership_Type__c, Membership_Class__c, Status__c, PersonOtherCountry
                                      FROM Account WHERE Id = :acct.Id]; 
        
        // prepare old and new lists and maps
		newMap.put(newMemberAccountAU.Id, newMemberAccountAU);
            
		List <Account> oldList;
		List <Account> newList = new List<Account>(newMap.values());
        
		if (debug) debugMaps(newList, newMap, oldList, oldMap);
 
		Map<Id, String> results = SegmentationMembersTriggerHandler.handleSegmentsAfterAccountUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('Test New Member Record: ' + results);
        System.assert(results.size() > 0);
       	Set<Id> keys = results.keySet();
        for (Id key : keys)
			System.assertEquals('Case 1', results.get(key) );
	}   
    

	// Case 1b. Change Membership Type                    
	private static testMethod void newMemberNoEmploymentAU_Case1b(){ 
        Map<Id, Account> oldMap = new Map<Id, Account>{} ;
		Map<Id, Account> newMap = new Map<Id, Account>{} ;
		Account acct = SegmentationDataGenTest.genNonMemberAustralia('Smith', 'Senior Accountant', 'smith1@test.co.nz', Date.valueOf('1987-05-12'));
        insert acct;
        Account nonMemberAccountAU = [SELECT Id, Name, isPersonAccount, Membership_Type__c, Membership_Class__c, Status__c, PersonOtherCountry
                                      FROM Account WHERE Id = :acct.Id]; 
        
		System.debug('Old:' + nonMemberAccountAU);
        Account newMemberAccountAU  = nonMemberAccountAU.clone(true, true, false, false);
        newMemberAccountAU.Membership_Type__c = 'Member';
        // prepare old and new lists and maps
        oldMap.put(nonMemberAccountAU.Id, nonMemberAccountAU);
		newMap.put(newMemberAccountAU.Id, newMemberAccountAU);
            
		List <Account> newList = new List<Account>(newMap.values());
		List <Account> oldList = new List<Account>(oldMap.values());
		if (debug) debugMaps(newList, newMap, oldList, oldMap);
 
		Map<Id, String> results = SegmentationMembersTriggerHandler.handleSegmentsAfterAccountUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('Test Change Member Type: ' + results);
        System.assert(results.size() > 0);
       	Set<Id> keys = results.keySet();
        for (Id key : keys)
			System.assertEquals('Case 1', results.get(key) ); 
	}    

    
    // Case 2. Empty Membership Type                    
	private static testMethod void noMembershipTypeAU_Case2(){ 
        Map<Id, Account> oldMap = new Map<Id, Account>{} ;
		Map<Id, Account> newMap = new Map<Id, Account>{} ;
		Account acct = SegmentationDataGenTest.genFullMemberNewZealand('Smith', 'Senior Accountant', 'smith1@test.co.nz', Date.valueOf('1987-05-12') );
        insert acct;
        Account nonMemberAccountAU = [SELECT Id, Name, isPersonAccount, Membership_Type__c, Membership_Class__c, Status__c, PersonOtherCountry
                                      FROM Account WHERE Id = :acct.Id]; 
        
        Account newMemberAccountAU  = nonMemberAccountAU.clone(true, true, false, false);
        newMemberAccountAU.Membership_Type__c = '';
        // prepare old and new lists and maps
        oldMap.put(nonMemberAccountAU.Id, nonMemberAccountAU);
		newMap.put(newMemberAccountAU.Id, newMemberAccountAU);
            
		List <Account> newList = new List<Account>(newMap.values());
		List <Account> oldList = new List<Account>(oldMap.values());
		if (debug) debugMaps(newList, newMap, oldList, oldMap);
 
		Map<Id, String> results = SegmentationMembersTriggerHandler.handleSegmentsAfterAccountUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('Test Change Member Type: ' + results);
        System.assert(results.size() > 0); 
	}    
 
	// Case 3. Change in Membership Class
	private static testMethod void changeMembershipClassAU_Case3(){ 
        Map<Id, Account> oldMap = new Map<Id, Account>{} ;
		Map<Id, Account> newMap = new Map<Id, Account>{} ;
		Account acct = SegmentationDataGenTest.genProvMemberAustralia('Smith', 'Senior Accountant', 'smith1@test.co.nz', Date.valueOf('1987-05-12') );
        insert acct;
        Account provMemberAccountAU = [SELECT Id, Name, isPersonAccount, Membership_Type__c, Membership_Class__c, Status__c, PersonOtherCountry
                                      FROM Account WHERE Id = :acct.Id]; 
        
        Account newMemberAccountAU  = provMemberAccountAU.clone(true, true, false, false);
        newMemberAccountAU.Membership_Class__c = 'Full';
        // prepare old and new lists and maps
        oldMap.put(provMemberAccountAU.Id, provMemberAccountAU);
		newMap.put(newMemberAccountAU.Id, newMemberAccountAU);
            
		List <Account> newList = new List<Account>(newMap.values());
		List <Account> oldList = new List<Account>(oldMap.values());
		if (debug) debugMaps(newList, newMap, oldList, oldMap);
 
		Map<Id, String> results = SegmentationMembersTriggerHandler.handleSegmentsAfterAccountUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('Test Change Member Type: ' + results);
        System.assert(results.size() > 0); 
		Set<Id> keys = results.keySet();
        for (Id key : keys)
			System.assertEquals('Case 3', results.get(key) ); 	
	} 


	// Case 4. Removed Membership Class
	private static testMethod void removedMembershipClassAU_Case4(){ 
        Map<Id, Account> oldMap = new Map<Id, Account>{} ;
		Map<Id, Account> newMap = new Map<Id, Account>{} ;
		Account acct = SegmentationDataGenTest.genProvMemberAustralia('Smith', 'Senior Accountant', 'smith1@test.co.nz', Date.valueOf('1987-05-12') );
        insert acct;
        Account provMemberAccountAU = [SELECT Id, Name, isPersonAccount, Membership_Type__c, Membership_Class__c, Status__c, PersonOtherCountry
                                      FROM Account WHERE Id = :acct.Id]; 
        
        Account newMemberAccountAU  = provMemberAccountAU.clone(true, true, false, false);
        newMemberAccountAU.Membership_Class__c = '';
        // prepare old and new lists and maps
        oldMap.put(provMemberAccountAU.Id, provMemberAccountAU);
		newMap.put(newMemberAccountAU.Id, newMemberAccountAU);
            
		List <Account> newList = new List<Account>(newMap.values());
		List <Account> oldList = new List<Account>(oldMap.values());
		if (debug) debugMaps(newList, newMap, oldList, oldMap);
 
		Map<Id, String> results = SegmentationMembersTriggerHandler.handleSegmentsAfterAccountUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('Test Change Member Type: ' + results);
        System.assert(results.size() > 0); 
		Set<Id> keys = results.keySet();
        for (Id key : keys)
			System.assertEquals('Case 4', results.get(key) ); 	
	} 
    
	 // Case 5. Change in Status
	private static testMethod void resignedMemberAU_Case5(){ 
        Map<Id, Account> oldMap = new Map<Id, Account>{} ;
		Map<Id, Account> newMap = new Map<Id, Account>{} ;
		Account acct = SegmentationDataGenTest.genProvMemberAustralia('Smith', 'Senior Accountant', 'smith1@test.co.nz', Date.valueOf('1987-05-12') );
        insert acct;
        Account provMemberAccountAU = [SELECT Id, Name, isPersonAccount, Membership_Type__c, Membership_Class__c, Status__c, PersonOtherCountry
                                      FROM Account WHERE Id = :acct.Id]; 
        
        Account newMemberAccountAU  = provMemberAccountAU.clone(true, true, false, false);
        newMemberAccountAU.Status__c = 'Resigned';
        // prepare old and new lists and maps
        oldMap.put(provMemberAccountAU.Id, provMemberAccountAU);
		newMap.put(newMemberAccountAU.Id, newMemberAccountAU);
            
		List <Account> newList = new List<Account>(newMap.values());
		List <Account> oldList = new List<Account>(oldMap.values());
		if (debug) debugMaps(newList, newMap, oldList, oldMap);
 
		Map<Id, String> results = SegmentationMembersTriggerHandler.handleSegmentsAfterAccountUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('Test Change Member Type: ' + results);
        System.assert(results.size() > 0); 
		Set<Id> keys = results.keySet();
        for (Id key : keys)
			System.assertEquals('Case 5', results.get(key) ); 	
	} 
    
	// Case 6. Financial Category Financial_Category__c
	private static testMethod void retiredMemberAU_Case6(){ 
        Map<Id, Account> oldMap = new Map<Id, Account>{} ;
		Map<Id, Account> newMap = new Map<Id, Account>{} ;
		Account acct = SegmentationDataGenTest.genProvMemberAustralia('Smith', 'Senior Accountant', 'smith1@test.co.nz', Date.valueOf('1987-05-12') );
        insert acct;
        Account provMemberAccountAU = [SELECT Id, Name, isPersonAccount, Membership_Type__c, Membership_Class__c, Status__c, PersonOtherCountry, Financial_Category__c
                                      FROM Account WHERE Id = :acct.Id]; 
        
        Account newMemberAccountAU  = provMemberAccountAU.clone(true, true, false, false);
        newMemberAccountAU.Financial_Category__c = 'Honorary Retired';
        // prepare old and new lists and maps
        oldMap.put(provMemberAccountAU.Id, provMemberAccountAU);
		newMap.put(newMemberAccountAU.Id, newMemberAccountAU);
            
		List <Account> newList = new List<Account>(newMap.values());
		List <Account> oldList = new List<Account>(oldMap.values());
		if (debug) debugMaps(newList, newMap, oldList, oldMap);
 
		Map<Id, String> results = SegmentationMembersTriggerHandler.handleSegmentsAfterAccountUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('Test Change Member Type: ' + results);
        System.assert(results.size() > 0); 
		Set<Id> keys = results.keySet();
        for (Id key : keys)
			System.assertEquals('Case 6', results.get(key) ); 	
	} 
	
    // Case 7. Country changed to or from Overseas    
    private static testMethod void countryChangeMember_Case7(){ 
        Map<Id, Account> oldMap = new Map<Id, Account>{} ;
		Map<Id, Account> newMap = new Map<Id, Account>{} ;
		Account acct = SegmentationDataGenTest.genFullMemberNewZealand('Smith', 'Senior Accountant', 'smith1@test.co.nz', Date.valueOf('1987-05-12') );
        insert acct;
        Account oldMemberAccountAU = [SELECT Id, Name, isPersonAccount, Membership_Type__c, Membership_Class__c, Status__c, PersonOtherCountry, Financial_Category__c
                                      FROM Account WHERE Id = :acct.Id]; 
        
        Account newMemberAccountAU  = oldMemberAccountAU.clone(true, true, false, false);
        newMemberAccountAU.PersonOtherCountry = 'Hong Kong';
        // prepare old and new lists and maps
        oldMap.put(oldMemberAccountAU.Id, oldMemberAccountAU);
		newMap.put(newMemberAccountAU.Id, newMemberAccountAU);
            
		List <Account> newList = new List<Account>(newMap.values());
		List <Account> oldList = new List<Account>(oldMap.values());
		if (debug) debugMaps(newList, newMap, oldList, oldMap);
 
		Map<Id, String> results = SegmentationMembersTriggerHandler.handleSegmentsAfterAccountUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('Test Change Member Type: ' + results);
        System.assert(results.size() > 0); 
		Set<Id> keys = results.keySet();
        for (Id key : keys)
			System.assertEquals('Case 7', results.get(key) ); 	
	} 
  
	// Case 1a.  Employment History
    // No Employment - Change to Employment			
	private static testMethod void SeniorMemberEHNew_Case1a(){
		if (debug) System.debug( 'SEG: SegmentationMembersTriggerHandler: SeniorMemberEHNew_Case1a');
        Map<Id, Employment_History__c> oldMap;
		Map<Id, Employment_History__c> newMap = new Map<Id, Employment_History__c>{} ;
            
		Account SeniorMemberAccountAU = SegmentationDataGenTest.genFullMemberAustralia('Turner', 'Tax Manager', 'brown1@test.co.au', Date.valueOf('1977-05-12') );
        insert SeniorMemberAccountAU; 
        Account big4BusinessAccountAU = SegmentationDataGenTest.genPracticeAU( 'Deloitte', 'Chartered Accounting', 'Sydney', 25);
		insert big4BusinessAccountAU; 
        Employment_History__c histEmpSeniorMemberAU = SegmentationDataGenTest.genCurrentEmploymentHistory(SeniorMemberAccountAU.Id, big4BusinessAccountAU.Id, 'Tax Manager', 'Manager');  
		insert histEmpSeniorMemberAU;
        
        // prepare old and new lists and maps
		newMap.put(histEmpSeniorMemberAU.Id, histEmpSeniorMemberAU);
            
		List <Employment_History__c> newList = new List<Employment_History__c>(newMap.values());
		List <Employment_History__c> oldList;
		// if (debug) debugMaps(newList, newMap, oldList, oldMap);
 
		Map<Id, String> results = SegmentationMembersTriggerHandler.handleSegmentsAfterEHUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('Test Change Employment: ' + results);
        System.assert(results.size() > 0); 
		Set<Id> keys = results.keySet();
        for (Id key : keys){
            			System.debug('Before Assert case 1a ' + results + ' Key ' + results.get(key) );
						System.assertEquals('Case 1', results.get(key) );             
        }
    }
	
	// Case 1b.  Employment History Change  
  	private static testMethod void SeniorMemberEHChange_Case1b(){
		if (debug) System.debug( 'SEG: SegmentationMembersTriggerHandler: SeniorMemberEHChange_Case1b');
        Map<Id, Employment_History__c> oldMap = new Map<Id, Employment_History__c>{} ;
		Map<Id, Employment_History__c> newMap = new Map<Id, Employment_History__c>{} ;
            
		Account memberAccount = SegmentationDataGenTest.genFullMemberNewZealand('Smith', 'Senior Accountant', 'smith1@test.co.nz', Date.valueOf('1987-05-12') );
		insert memberAccount;
		Account big4BusinessAccountNZ = SegmentationDataGenTest.genPracticeNewZealand( 'Ernst & Young', 'Chartered Accounting', 'Auckland', 25);
		insert big4BusinessAccountNZ;
        Employment_History__c histEmpSeniorMemberAU = SegmentationDataGenTest.genCurrentEmploymentHistory(memberAccount.Id, big4BusinessAccountNZ.Id, 'Tax Manager', 'Manager');  
		insert histEmpSeniorMemberAU;

        // close the existing employment
		Employment_History__c oldEH = [Select Id, Name, Member__c, Employer__c, Job_Title__c, Status__c, Primary_Employer__c, Employee_Start_Date__c, Employee_End_Date__c 
                                       From Employment_History__c where Member__c=:memberAccount.Id][0];        

		// mark old as closed
        oldEH.Status__c = 'Closed';
        oldEH.Primary_Employer__c = false;
        oldEH.Employee_End_Date__c = system.today().addDays(-1);
        
        update oldEH;
 		// get a new Employment History object
        Account smallBusinessAccount = SegmentationDataGenTest.genPracticeNewZealand( 'Bob Accounting', 'Chartered Accounting', 'Wellington', 1);
		insert smallBusinessAccount;
        Employment_History__c newEH = SegmentationDataGenTest.genCurrentEmploymentHistory(memberAccount.Id, smallBusinessAccount.Id, 'Tax Manager', 'Manager');  
		insert newEH;

		if (debug) System.debug('DEBUG: After Data Preperation Old Employer '+ oldEH.Employer__c);
 		if (debug) System.debug('DEBUG: After Data Preperation New Employer '+ newEH.Employer__c);
            
        // prepare old and new lists and maps. The new listing now contains both old and new employment details
        oldMap = NULL;
        List <Employment_History__c> oldList;
		
        newMap.put(newEH.Id, newEH);
		List <Employment_History__c> newList = new List<Employment_History__c>(newMap.values());

		if (debug) System.debug('DEBUG: Maps');
		if (debug) debugEHMaps(newList, newMap, oldList, oldMap);
        
		Map<Id, String> results = SegmentationMembersTriggerHandler.handleSegmentsAfterEHUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('Test Change Employer: ' + results);
        System.assert(results.size() > 0); 
		Set<Id> keys = results.keySet();
        for (Id key : keys)
			System.assertEquals('Case 1', results.get(key) ); 	
    }

    
    // Case 2.  Job Title Change
  	private static testMethod void SeniorMemberEHJobTitleChange_Case2(){
		if (debug) System.debug( 'SEG: SegmentationMembersTriggerHandler: SeniorMemberEHJobTitleChange_Case2');
        Map<Id, Employment_History__c> oldMap = new Map<Id, Employment_History__c>{} ;
		Map<Id, Employment_History__c> newMap = new Map<Id, Employment_History__c>{} ;
            
		Account memberAccount = SegmentationDataGenTest.genFullMemberNewZealand('Smith', 'Senior Accountant', 'smith1@test.co.nz', Date.valueOf('1987-05-12') );
		insert memberAccount;
		Account big4BusinessAccountNZ = SegmentationDataGenTest.genPracticeNewZealand( 'Ernst & Young', 'Chartered Accounting', 'Auckland', 25);
		insert big4BusinessAccountNZ;
        Employment_History__c histEmpSeniorMemberAU = SegmentationDataGenTest.genCurrentEmploymentHistory(memberAccount.Id, big4BusinessAccountNZ.Id, 'Accountant', 'Accountant');  
		insert histEmpSeniorMemberAU;
        // close the existing employment
		Employment_History__c oldEH = [Select Id, Name, Member__c, Employer__c, Job_Title__c, Status__c, Primary_Employer__c, Employee_Start_Date__c, Employee_End_Date__c 
                                       From Employment_History__c where Member__c=:memberAccount.Id][0];        

		Employment_History__c newEH = oldEH.clone(true, true, false, false);
        newEH.Job_Title__c = 'Tax Manager';
        newEH.Job_Class__c = 'Manager';
 
        // prepare old and new lists and maps
        oldMap.put(oldEH.Id, oldEH);
		newMap.put(newEH.Id, newEH);
            
		List <Employment_History__c> newList = new List<Employment_History__c>(newMap.values());
		List <Employment_History__c> oldList = new List<Employment_History__c>(oldMap.values());
		// if (debug) debugMaps(newList, newMap, oldList, oldMap);
 
		Map<Id, String> results = SegmentationMembersTriggerHandler.handleSegmentsAfterEHUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('Test Change Member Type: ' + results);
        System.assert(results.size() > 0); 
		Set<Id> keys = results.keySet();
        for (Id key : keys)
			System.assertEquals('Case 2', results.get(key) ); 	
    }
    
    // Case 3.  Employment History Changes to Status  
  	private static testMethod void SeniorMemberEHStatusChange_Case3(){
		if (debug) System.debug( 'SEG: SegmentationMembersTriggerHandler: SeniorMemberEHStatusChange_Case3');
        Map<Id, Employment_History__c> oldMap = new Map<Id, Employment_History__c>{} ;
		Map<Id, Employment_History__c> newMap = new Map<Id, Employment_History__c>{} ;
            
		Account memberAccount = SegmentationDataGenTest.genFullMemberNewZealand('Smith', 'Senior Accountant', 'smith1@test.co.nz', Date.valueOf('1987-05-12') );
		insert memberAccount;
		Account big4BusinessAccountNZ = SegmentationDataGenTest.genPracticeNewZealand( 'Ernst & Young', 'Chartered Accounting', 'Auckland', 25);
		insert big4BusinessAccountNZ;
        Employment_History__c histEmpSeniorMemberAU = SegmentationDataGenTest.genCurrentEmploymentHistory(memberAccount.Id, big4BusinessAccountNZ.Id, 'Tax Manager', 'Manager');  
		insert histEmpSeniorMemberAU;
        // close the existing employment
		Employment_History__c oldEH = [Select Id, Name, Member__c, Employer__c, Job_Title__c, Status__c, Primary_Employer__c, Employee_Start_Date__c, Employee_End_Date__c 
                                       From Employment_History__c where Member__c=:memberAccount.Id][0];        

		Employment_History__c newEH = oldEH.clone(true, true, false, false);
        newEH.Status__c = 'Closed';
        newEH.Primary_Employer__c = false;
 
        // prepare old and new lists and maps
        oldMap.put(oldEH.Id, oldEH);
		newMap.put(newEH.Id, newEH);
            
		List <Employment_History__c> newList = new List<Employment_History__c>(newMap.values());
		List <Employment_History__c> oldList = new List<Employment_History__c>(oldMap.values());
		// if (debug) debugMaps(newList, newMap, oldList, oldMap);
 
		Map<Id, String> results = SegmentationMembersTriggerHandler.handleSegmentsAfterEHUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('Test Change Member Type: ' + results);
        System.assert(results.size() > 0); 
		Set<Id> keys = results.keySet();
        for (Id key : keys)
			System.assertEquals('Case 3', results.get(key) ); 	
    }
    
    // Case 3a.  Employment History Changes to End Date  
  	private static testMethod void SeniorMemberEHStatusChange_Case3a(){
		if (debug) System.debug( 'SEG: SegmentationMembersTriggerHandler: SeniorMemberEHStatusChange_Case3a');
        Map<Id, Employment_History__c> oldMap = new Map<Id, Employment_History__c>{} ;
		Map<Id, Employment_History__c> newMap = new Map<Id, Employment_History__c>{} ;
            
		Account memberAccount = SegmentationDataGenTest.genFullMemberNewZealand('Smith', 'Senior Accountant', 'smith1@test.co.nz', Date.valueOf('1987-05-12') );
		insert memberAccount;
		Account big4BusinessAccountNZ = SegmentationDataGenTest.genPracticeNewZealand( 'Ernst & Young', 'Chartered Accounting', 'Auckland', 25);
		insert big4BusinessAccountNZ;
        Employment_History__c histEmpSeniorMemberAU = SegmentationDataGenTest.genCurrentEmploymentHistory(memberAccount.Id, big4BusinessAccountNZ.Id, 'Tax Manager', 'Manager');  
		insert histEmpSeniorMemberAU;
        // close the existing employment
		Employment_History__c oldEH = [Select Id, Name, Member__c, Employer__c, Job_Title__c, Status__c, Primary_Employer__c, Employee_Start_Date__c, Employee_End_Date__c 
                                       From Employment_History__c where Member__c=:memberAccount.Id][0];        

		Employment_History__c newEH = oldEH.clone(true, true, false, false);
        newEH.Employee_End_Date__c = system.today().addDays(-1);
 
        // prepare old and new lists and maps
        oldMap.put(oldEH.Id, oldEH);
		newMap.put(newEH.Id, newEH);
            
		List <Employment_History__c> newList = new List<Employment_History__c>(newMap.values());
		List <Employment_History__c> oldList = new List<Employment_History__c>(oldMap.values());
		// if (debug) debugMaps(newList, newMap, oldList, oldMap);
 
		Map<Id, String> results = SegmentationMembersTriggerHandler.handleSegmentsAfterEHUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('Test Change Member Type: ' + results);
        System.assert(results.size() > 0); 
		Set<Id> keys = results.keySet();
        for (Id key : keys)
			System.assertEquals('Case 3', results.get(key) ); 	
    }
    
    
     // Case 3b.  Employment History Changes to End Date  - re-opened
  	private static testMethod void SeniorMemberEHStatusChange_Case3b(){
		if (debug) System.debug( 'SEG: SegmentationMembersTriggerHandler: SeniorMemberEHStatusChange_Case3b');
        Map<Id, Employment_History__c> oldMap = new Map<Id, Employment_History__c>{} ;
		Map<Id, Employment_History__c> newMap = new Map<Id, Employment_History__c>{} ;
            
		Account memberAccount = SegmentationDataGenTest.genFullMemberNewZealand('Smith', 'Senior Accountant', 'smith1@test.co.nz', Date.valueOf('1987-05-12') );
		insert memberAccount;
		Account big4BusinessAccountNZ = SegmentationDataGenTest.genPracticeNewZealand( 'Ernst & Young', 'Chartered Accounting', 'Auckland', 25);
		insert big4BusinessAccountNZ;
   		Employment_History__c empHist = SegmentationDataGenTest.genHistoricalEmploymentHistory(memberAccount.Id, big4BusinessAccountNZ.Id, 'Accountant', 'Senior Accountant');  
		insert empHist;

        // close the existing employment
		Employment_History__c oldEH = [Select Id, Name, Member__c, Employer__c, Job_Title__c, Status__c, Primary_Employer__c, Employee_Start_Date__c, Employee_End_Date__c 
                                       From Employment_History__c where Member__c=:memberAccount.Id][0];        

		Employment_History__c newEH = oldEH.clone(true, true, false, false);
        newEH.Employee_End_Date__c = NULL;
 
        // prepare old and new lists and maps
        oldMap.put(oldEH.Id, oldEH);
		newMap.put(newEH.Id, newEH);
            
		List <Employment_History__c> newList = new List<Employment_History__c>(newMap.values());
		List <Employment_History__c> oldList = new List<Employment_History__c>(oldMap.values());
		// if (debug) debugMaps(newList, newMap, oldList, oldMap);
 
		Map<Id, String> results = SegmentationMembersTriggerHandler.handleSegmentsAfterEHUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('Test Change Member Type: ' + results);
        System.assert(results.size() > 0); 
		Set<Id> keys = results.keySet();
        for (Id key : keys)
			System.assertEquals('Case 3', results.get(key) ); 	
    }

 
	private static testMethod void ProvToFullMemberAccountAU_Update(){
		if (debug) System.debug( 'TEST PROCESSING - SegmentationMembersTriggerHandlerTest');
		if (debug) System.debug( 'ProvToFullMemberAccountAU_Update');

        Account provMemberAccountAU = SegmentationDataGenTest.genProvMemberAustralia('Lee', 'Accountant', 'lee@test.co.au', Date.valueOf('1991-05-12') );
        insert provMemberAccountAU;
        Account smallBusinessAccountAU = SegmentationDataGenTest.genPracticeAU( 'Young and Co', 'Chartered Accounting', 'Brisbane', 2);
		insert smallBusinessAccountAU;
		Employment_History__c currEmpProvMemberAU = SegmentationDataGenTest.genCurrentEmploymentHistory(provMemberAccountAU.Id, smallBusinessAccountAU.Id, 'Accountant', 'Accountant');  
		insert currEmpProvMemberAU;
        // create segmentation object
        Segmentation__c provMemberSeg = new Segmentation__c();
        provMemberSeg.Account__c = provMemberAccountAU.Id;
        provMemberSeg.Organisation_Type__c = 'Practice';
        provMemberSeg.Business_Segmentation_MS__c = 'Provisional SMP';
		insert provMemberSeg;
        
        Test.startTest(); 
                
		Account provMember = [select id, Membership_Type__c, Membership_Class__c from Account where LastName = 'Lee'];
        System.debug('Provisional Member before :' + provMember);
        List<Employment_History__c> eh = [Select Id, Name, Member__c, Employer__c, Job_Title__c, Status__c From Employment_History__c where Member__c=:provMember.Id];
        System.debug('Provisional Member Employment before :' + eh);
        List<Segmentation__c> segmentsBefore  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{provMember.Id}];
        System.debug('Provisional Member Segmentation before :' + segmentsBefore);
		provMember.Membership_Class__c = 'Full';
        update provMember;
        Map <Id, String> change = new Map <Id, String>();
        change.put(provMember.Id, 'Case 1');
		SegmentationMembersTriggerHandler.handleSegmentsAfterUpdateAsync(change) ;         
		Test.stopTest();
        
        List<Segmentation__c> segmentsAfter  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{provMember.Id}];

        for (Segmentation__c seg : segmentsAfter) {
			System.debug('Provisional Member Segmentation After:' + seg);
            System.assertEquals('Practice', seg.Organisation_Type__c);
            System.assertEquals('Small Firm', seg.Firm_Type__c);
            System.assertEquals('SMP Team Player', seg.Business_Segmentation_MS__c);
            System.assertEquals('Team Player', seg.Career_Stage__c); 
            System.assertEquals('Small', seg.Firm_Office_Size__c); 
        }
    }
    
	private static testMethod void ProvToNonMemberAccountAU_Update(){
		if (debug) System.debug( 'TEST PROCESSING - SegmentationMembersTriggerHandlerTest');
		if (debug) System.debug( 'ProvToFullMemberAccountAU_Case6');

        Account provMemberAccountAU = SegmentationDataGenTest.genProvMemberAustralia('Lee', 'Accountant', 'lee@test.co.au', Date.valueOf('1991-05-12') );
        insert provMemberAccountAU;
        Account smallBusinessAccountAU = SegmentationDataGenTest.genPracticeAU( 'Young and Co', 'Chartered Accounting', 'Brisbane', 2);
		insert smallBusinessAccountAU;
		Employment_History__c currEmpProvMemberAU = SegmentationDataGenTest.genCurrentEmploymentHistory(provMemberAccountAU.Id, smallBusinessAccountAU.Id, 'Accountant', 'Accountant');  
		insert currEmpProvMemberAU;
        // create segmentation object
        Segmentation__c provMemberSeg = new Segmentation__c();
        provMemberSeg.Account__c = provMemberAccountAU.Id;
        provMemberSeg.Organisation_Type__c = 'Practice';
        provMemberSeg.Business_Segmentation_MS__c = 'Provisional SMP';
		insert provMemberSeg;
        
        Test.startTest(); 
                
		Account provMember = [select id, Membership_Type__c, Membership_Class__c from Account where LastName = 'Lee'];
        System.debug('Provisional Member before :' + provMember);
        List<Employment_History__c> eh = [Select Id, Name, Member__c, Employer__c, Job_Title__c, Status__c From Employment_History__c where Member__c=:provMember.Id];
        System.debug('Provisional Member Employment before :' + eh);
        List<Segmentation__c> segmentsBefore  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{provMember.Id}];
        System.debug('Provisional Member Segmentation before :' + segmentsBefore);
		provMember.Membership_Type__c = 'Non Member';
        update provMember;
        Map <Id, String> change = new Map <Id, String>();
        change.put(provMember.Id, 'Case 2');
		SegmentationMembersTriggerHandler.handleSegmentsAfterUpdateAsync(change) ;         
		Test.stopTest();
        
        List<Segmentation__c> segmentsAfter  = [ select Id, Account__c, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c, Career_Stage__c
                                           		 from Segmentation__c 
                                                 where Account__c IN : new List<Id>{provMember.Id}];

        for (Segmentation__c seg : segmentsAfter) {
			System.debug('Provisional Member Segmentation After:' + seg);
            System.assertEquals(NULL, seg.Organisation_Type__c);
            System.assertEquals(NULL, seg.Firm_Type__c);
            System.assertEquals(NULL, seg.Business_Segmentation_MS__c);
            System.assertEquals(NULL, seg.Career_Stage__c); 
            System.assertEquals(NULL, seg.Firm_Office_Size__c); 
        }
    }
}