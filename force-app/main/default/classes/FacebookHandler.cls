global class FacebookHandler implements Auth.RegistrationHandler{

    global Void updateUser(Id userId, Id portalId, Auth.UserData data) { }
    global User createUser(Id portalId, Auth.UserData data) {
        List<User> usrs =[SELECT Id FROM User WHERE email = :data.email LIMIT 1];
        if(usrs!=null&&!usrs.isEmpty()) return usrs[0]; else return null;
    }
}