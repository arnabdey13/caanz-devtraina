@IsTest
private class TestAppFormsDriverController {


    @testSetup static void createTestData() {
        TestSubscriptionUtils.createTestData();
        TestDataDegreeJoinBuilder.joinedApproved('Otago University', 'New Zealand', 'Bachelor of Accounting', 'Undergraduate');
        TestDataFFRichTextBuilder.multiple(RICH_TEXT_NAMES, 'fake content', true);
        //AppFormsTestUtil.createMemberAccount(); 
    }

    private static User getMemberUser() {
        Id contactId = [SELECT Id FROM Contact WHERE AccountId = :TestSubscriptionUtils.getTestAccountId()].Id;
        return [select Id from User where ContactId = :contactId];
    }

    private static final Id recordTypeBusiness = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();

     @IsTest
    private static void testEmployerOptInCRUD() {
        // same test as in My Details service but needs to cover using the AppFormsDriverController instead
        Account employer = new TestDataAccountBuilder('Business Account')
                .name('Deloitte').memberOf('NZICA').status('Active')
                .billingAddress('Unit 1, 50 Customhouse Quay, Wellington Central', 'WELLINGTON', '', '6011', 'NZ')
                .create(1, true).get(0);

        Id accountId = TestSubscriptionUtils.getTestAccountId();

        System.runAs(getMemberUser()) {
            ff_WriteData wd = new ff_WriteData();
            wd.records.put('INSERT-Employment_History__c2',
                    new Employment_History__c(
                            // member id not passed by client, populated by saver impl
                            Employer__c = employer.Id,
                            Job_Title__c = 'Mail Room Deputy Assistant',
                            Employee_Start_Date__c = System.today(),
                            Employee_End_Date__c = System.today(),
                            Status__c = 'Closed',
                            Work_Hours__c = 'Full Time'
                    ));

            // reset governor limits since the add, edit and delete are invoked in different requests
            // when invoked in real-world use
            
            AppFormsDriverController service = new AppFormsDriverController(accountId,'Special_Admissions');
            ff_WriteResult result = service.handler.save(wd);
            System.debug('opt-in insert result: ' + result);
            System.assertEquals(0, result.fieldErrors.size(), 'no errors during create');
            System.assertEquals(0, result.recordErrors.size(), 'no errors during create');
            System.debug('queries used in create: ' + Limits.getQueries());

            Test.startTest();
            
            service = new AppFormsDriverController(accountId,'Special_Admissions');
            ff_ReadData withHistoryRecord = service.handler.load(null);
            System.debug(withHistoryRecord);
            List<ff_Service.SObjectWrapper> histories = withHistoryRecord.records.get('Employment_History__c');
            System.debug(histories);
            // checking security access to history records
            System.assertEquals(1, histories.get(0).sObjectList.size(),
                    'created record can be read after create');

            Employment_History__c history = (Employment_History__c) histories.get(0).sObjectList.get(0);

            wd = new ff_WriteData();
            wd.records.put(history.Id,
                    new Employment_History__c(
                            Id = history.Id,
                            Status__c = 'Current'
                    ));
            System.debug('wd for update: ' + wd);

            service = new AppFormsDriverController(accountId,'Special_Admissions');
            result = service.handler.save(wd);
            System.debug('edit result: ' + result);
            System.assertEquals(0, result.fieldErrors.size(), 'no errors during edit');
            System.assertEquals(0, result.recordErrors.size(), 'no errors during edit');
            System.debug('queries used in edit: ' + Limits.getQueries());

            service = new AppFormsDriverController(accountId,'Special_Admissions');
            result = service.handler.deleteRecord(history.Id);
            System.debug('delete result: ' + result);
            System.assertEquals(0, result.fieldErrors.size(), 'no errors during delete');
            System.assertEquals(0, result.recordErrors.size(), 'no errors during delete');
            System.debug('queries used in delete: ' + Limits.getQueries());

            Test.stopTest();

        }
    }
    
    
     @IsTest
    private static void testPrimaryEmployerSwitch() {

        Account employer = new TestDataAccountBuilder('Business Account')
                .name('Deloitte').memberOf('NZICA').status('Active')
                .billingAddress('Unit 1, 50 Customhouse Quay, Wellington Central', 'WELLINGTON', '', '6011', 'NZ')
                .create(1, true).get(0);

        Account optedOutEmployer = new Account(
                Name = 'CAANZ',
                BillingStreet = 'Unit 1, 50 Customhouse Quay, Wellington Central',
                BillingCity = 'WELLINGTON',
                BillingState = '',
                BillingPostalCode = '6011',
                BillingCountryCode = 'NZ',
                Billing_DPID__c = 'DP123',
                PersonBillingAddressValidatedByQAS__c = true
        );

        Id accountId = TestSubscriptionUtils.getTestAccountId();

        System.runAs(getMemberUser()) {

            // first create an opted-in employer and save
            ff_WriteData wd = new ff_WriteData();
            wd.records.put('INSERT-Employment_History__c1',
                    new Employment_History__c(
                            // member id not passed by client, populated by saver impl
                            Employer__c = employer.Id,
                            Job_Title__c = 'Mail Room Deputy Assistant',
                            Employee_Start_Date__c = System.today(),
                            Employee_End_Date__c = System.today(),
                            Status__c = 'Current',
                            Primary_Employer__c = true,
                            Work_Hours__c = 'Full Time'
                    ));
            AppFormsDriverController service = new AppFormsDriverController(accountId,'Special_Admissions');
            ff_WriteResult result1 = service.handler.save(wd);
            SObject history1 = result1.results.values().get(0);
            String history1Id = history1.Id;

            Test.startTest(); // now start the test

            // opted out employer should remove the other opted out primary
            wd = new ff_WriteData();
            wd.records.put('INSERT-Employment_History__c2',
                    new Employment_History__c(
                            // member id not passed by client, populated by saver impl
                            Employer__r = optedOutEmployer,
                            Job_Title__c = 'Mail Room Deputy Assistant',
                            Employee_Start_Date__c = System.today(),
                            Employee_End_Date__c = System.today(),
                            Status__c = 'Current',
                            Primary_Employer__c = true,
                            Work_Hours__c = 'Full Time'
                    ));
            service = new AppFormsDriverController(accountId,'Special_Admissions');
            ff_WriteResult result2 = service.handler.save(wd);
            SObject history2 = result2.results.values().get(0);
            String history2Id = (String) history2.get('Employer_Name_at_Commencement__c');

            wd = new ff_WriteData();
            wd.records.put('INSERT-Employment_History__c3',
                    new Employment_History__c(
                            // member id not passed by client, populated by saver impl
                            Employer__r = optedOutEmployer,
                            Job_Title__c = 'Mail Room Deputy Assistant',
                            Employee_Start_Date__c = System.today(),
                            Employee_End_Date__c = System.today(),
                            Status__c = 'Current',
                            Primary_Employer__c = true,
                            Work_Hours__c = 'Full Time'
                    ));
            service = new AppFormsDriverController(accountId,'Special_Admissions');
            ff_WriteResult result3 = service.handler.save(wd);
            SObject history3 = result3.results.values().get(0);
            String history3Id = (String) history3.get('Employer_Name_at_Commencement__c');

            Test.stopTest();
        
        }
    }
    
    @IsTest
    static void testEducationDeleteUndergraduate_Mode1() {

        // Testing correct updates to legacy fields when employment child records are deleted
        // 1. Add AU undergrad mode1
        // 2. Add AU undergrad mode1
        // 3. Add AU undergrad mode1
        // 4. Add NZ undergrad mode1
        // 5. Delete last AU record
        // legacy fields should show NZ data and remaining (most recent i.e. #2 above) AU data

        Id accountId = TestSubscriptionUtils.getTestAccountId();
        ff_WriteData wd = new ff_WriteData();

        edu_University_Degree_Join__c joinSelectedAU1 = TestDataDegreeJoinBuilder.joinedApproved(
                'Melbourne University', 'Australia', 'Bachelor of Commerce', 'Undergraduate');
        edu_University_Degree_Join__c joinSelectedAU2 = TestDataDegreeJoinBuilder.joinedApproved(
                'Brisbane University', 'Australia', 'Bachelor of Auditing', 'Undergraduate');
        edu_University_Degree_Join__c joinSelectedAU3 = TestDataDegreeJoinBuilder.joinedApproved(
                'Perth University', 'Australia', 'Bachelor of Finance', 'Undergraduate');

        // note first NZ record is opted out
        edu_University_Degree_Join__c joinSelectedNZ1 = TestDataDegreeJoinBuilder.joinedOptedOut(
                'Coromandel University', 'Bachelor of Surfing');
        edu_University_Degree_Join__c joinSelectedNZ2 = TestDataDegreeJoinBuilder.joinedApproved(
                'Victoria University', 'New Zealand', 'Bachelor of Bean Counting', 'Undergraduate');

        AppFormsDriverController service = new AppFormsDriverController(accountId,'Special_Admissions');
        List<Id> historyIds = new List<Id>();
        for (edu_University_Degree_Join__c degreeJoin : new List<edu_University_Degree_Join__c>{
                joinSelectedAU1, joinSelectedAU2, joinSelectedAU3, joinSelectedNZ1, joinSelectedNZ2
        }) {
            edu_Education_History__c historyRecordFromClient = new edu_Education_History__c(
                    University_Degree__c = degreeJoin.Id);
            wd.records.put('INSERT-edu_Education_History__c0', historyRecordFromClient);
            ff_WriteResult result = service.handler.save(wd);
            System.debug(result);
            System.assertEquals(0, result.fieldErrors.size(), 'no errors during save');
            System.assertEquals(0, result.recordErrors.size(), 'no errors during save');
            historyIds.add(result.results.values().get(0).Id);
        }
        System.debug(historyIds);

        // state of fields after adds
        Application__c application = getApplicationWithLegacyFields(accountId);
        System.debug(application);
        System.assertEquals('Victoria University', application.University_New_Zealand_Undergraduate__c,
                'Undergrad NZ Uni is Vic because it is last NZ saved');
        System.assertEquals('Bachelor of Bean Counting', application.Undergraduate_nz__c,
                'Undergrad NZ degree is beans because it is last NZ saved');
        System.assertEquals('Perth University', application.University_Australia_Undergraduate__c,
                'Undergrad AU Uni is Perth because last AU saved');
        System.assertEquals('Bachelor of Finance', application.Undergraduate_AU__c,
                'Undergrad AU degree is finance because last AU saved');

        System.assertEquals('New Zealand', application.Location_of_Provider__c,
                'Undergrad location is Aotearoa because Vic was last save');

        System.debug('starting delete phase');
        Test.startTest();

        // now delete first au record and check
        service = new AppFormsDriverController(accountId,'Special_Admissions');
        ff_WriteResult result = service.handler.deleteRecord(historyIds.get(2)); // 3rd id is last AU
        System.debug('1st delete result: ' + result);
        System.assertEquals(0, result.fieldErrors.size(), 'no errors during delete');
        System.assertEquals(0, result.recordErrors.size(), 'no errors during delete');

        application = getApplicationWithLegacyFields(accountId);
        System.debug(application);

        System.assertEquals('New Zealand', application.Location_of_Provider__c,
                'Undergrad location is Aotearoa because Vic was still the last save');

        System.assertEquals('Victoria University', application.University_New_Zealand_Undergraduate__c,
                'Undergrad NZ Uni is still Vic since it was not deleted');
        System.assertEquals('Bachelor of Bean Counting', application.Undergraduate_nz__c,
                'Undergrad NZ degree is beans since it was not deleted');
        System.assertEquals('Brisbane University', application.University_Australia_Undergraduate__c,
                'Undergrad AU Uni is Brisvegas because saved just before Perth');
        System.assertEquals('Bachelor of Auditing', application.Undergraduate_AU__c,
                'Undergrad AU degree is Auditing because saved just before Perth');

        // now delete NZ and verify cleanout of NZ fields
        service = new AppFormsDriverController(accountId,'Special_Admissions');
        result = service.handler.deleteRecord(historyIds.get(3)); // 4th id is NZ opt-out
        System.debug('2nd delete result: ' + result);
        System.assertEquals(0, result.fieldErrors.size(), 'no errors during delete');
        System.assertEquals(0, result.recordErrors.size(), 'no errors during delete');
        result = service.handler.deleteRecord(historyIds.get(4)); // 5th id is NZ opt-in
        System.debug('3rd delete result: ' + result);
        System.assertEquals(0, result.fieldErrors.size(), 'no errors during delete');
        System.assertEquals(0, result.recordErrors.size(), 'no errors during delete');

        application = getApplicationWithLegacyFields(accountId);
        System.debug(application);

        System.assertEquals('Australia', application.Location_of_Provider__c,
                'Undergrad location is Aussie because no more NZ records');

        System.assertEquals(NULL, application.University_New_Zealand_Undergraduate__c,
                'Undergrad NZ Uni is cleared because it was the last one');
        System.assertEquals(NULL, application.Undergraduate_nz__c,
                'Undergrad NZ degree is beans it was the last one');
        System.assertEquals('Brisbane University', application.University_Australia_Undergraduate__c,
                'Undergrad AU Uni is still Brisvegas because it is most recent AU');
        System.assertEquals('Bachelor of Auditing', application.Undergraduate_AU__c,
                'Undergrad AU degree is Auditing because it is most recent AU');

        service = new AppFormsDriverController(accountId,'Special_Admissions');
        result = service.handler.deleteRecord(historyIds.get(0)); // first is AU
        System.debug('4th delete result: ' + result);
        System.assertEquals(0, result.fieldErrors.size(), 'no errors during delete');
        System.assertEquals(0, result.recordErrors.size(), 'no errors during delete');
        result = service.handler.deleteRecord(historyIds.get(1)); // second is AU
        System.debug('5th delete result: ' + result);
        System.assertEquals(0, result.fieldErrors.size(), 'no errors during delete');
        System.assertEquals(0, result.recordErrors.size(), 'no errors during delete');

        application = getApplicationWithLegacyFields(accountId);

        System.assertEquals(NULL, application.Location_of_Provider__c,
                'Undergrad location is empty because no more records');
        System.assertEquals(NULL, application.University_New_Zealand_Undergraduate__c,
                'Undergrad NZ Uni is cleared because no more records');
        System.assertEquals(NULL, application.Undergraduate_nz__c,
                'Undergrad NZ degree is empty because no more records');
        System.assertEquals(NULL, application.University_Australia_Undergraduate__c,
                'Undergrad AU Uni is empty because not more records');
        System.assertEquals(NULL, application.Undergraduate_AU__c,
                'Undergrad AU degree is Auditing because no more records');

        Test.stopTest();
    }
    
    @IsTest
    static void testEducationDelete_Mode4() {
        // testing a bug found in UAT where opted out records are not cleaning up legacy field when deleted
        Id accountId = TestSubscriptionUtils.getTestAccountId();
        ff_WriteData wd = new ff_WriteData();
        edu_University_Degree_Join__c joinOptedOut = TestDataDegreeJoinBuilder.joinedOptedOut(
                'Coromandel University', 'Bachelor of Surfing');

        edu_Education_History__c historyRecordFromClient = new edu_Education_History__c(
                University_Degree__c = joinOptedOut.Id);
        wd.records.put('INSERT-edu_Education_History__c0', historyRecordFromClient);

        AppFormsDriverController service = new AppFormsDriverController(accountId,'Special_Admissions');
        ff_WriteResult result = service.handler.save(wd);
        System.debug(result);
        System.assertEquals(0, result.fieldErrors.size(), 'no errors during save');
        System.assertEquals(0, result.recordErrors.size(), 'no errors during save');
        Id insertedId = result.results.values().get(0).Id;

        Application__c application = getApplicationWithLegacyFields(accountId);
        System.debug(application);

        System.assertEquals('Coromandel University', application.Other_University_Undergraduate__c,
                'Opted out Uni was populated');
        System.assertEquals('Bachelor of Surfing', application.Undergraduate_AU_other__c,
                'Opted out Degree was populated');

        // now delete and check
        service = new AppFormsDriverController(accountId,'Special_Admissions');
        result = service.handler.deleteRecord(insertedId);
        System.assertEquals(0, result.fieldErrors.size(), 'no errors during delete');
        System.assertEquals(0, result.recordErrors.size(), 'no errors during delete');

        application = getApplicationWithLegacyFields(accountId);
        System.debug(application);

        System.assertEquals(NULL, application.Other_University_Undergraduate__c,
                'Opted out Uni was cleaned');
        System.assertEquals(NULL, application.Undergraduate_AU_other__c,
                'Opted out Degree was cleaned');

    }
    
    //SOQL ISSUES, CRITICAL FIX
   /* @IsTest
    private static void testEmployerOptOutCRUD() {
        // tests for opt-out which causes the service to save records to Unknown_Employer__c instead of Employer_History__c
        // this includes the ability to be able to load data from both objects but return only Employer_History__c to the client
        // there are some tricky edge cases when switching from opt-in to opt-out in an update, so testing needs to be more robust there

        Id accountId = TestSubscriptionUtils.getTestAccountId();

        System.runAs(getMemberUser()) {
            ff_WriteData wd = new ff_WriteData();
            Account optedOutEmployer = new Account(
                    Name = 'CAANZ',
                    BillingStreet = 'Unit 1, 50 Customhouse Quay, Wellington Central',
                    BillingCity = 'WELLINGTON',
                    BillingState = '',
                    BillingPostalCode = '6011',
                    BillingCountryCode = 'NZ',
                    Billing_DPID__c = 'DP123',
                    PersonBillingAddressValidatedByQAS__c = true
            );
            wd.records.put('INSERT-Employment_History__c2',
                    new Employment_History__c(
                            // member id not passed by client, populated by saver impl
                            Employer__r = optedOutEmployer,
                            Job_Title__c = 'Mail Room Deputy Assistant',
                            Employee_Start_Date__c = System.today(),
                            Employee_End_Date__c = System.today(),
                            Status__c = 'Current',
                            Primary_Employer__c = true,
                            Work_Hours__c = 'Full Time'
                    ));

            // reset governor limits since the add, edit and delete are invoked in different requests
            // when invoked in real-world use
            Test.startTest(); 
            
            // CREATE
            AppFormsDriverController service = new AppFormsDriverController(accountId,'Special_Admissions');
            ff_WriteResult result = service.handler.save(wd);
            System.debug('opt-out insert result: ' + result);
            System.assertEquals(0, result.fieldErrors.size(), 'no errors during create');
            System.assertEquals(0, result.recordErrors.size(), 'no errors during create');
            System.debug('queries used in create: ' + Limits.getQueries());

            // READ
            service = new AppFormsDriverController(accountId,'Special_Admissions');
            ff_ReadData withHistoryRecord = service.handler.load(null);
            System.debug(withHistoryRecord);
            List<ff_Service.SObjectWrapper> histories = withHistoryRecord.records.get('Employment_History__c');
            System.debug('employer histories post-insert: ' + histories);
            System.assertEquals(1, histories.get(0).sObjectList.size(),
                    'opted out created record can be read after create');
            // sending id in Employer_Name_at_Commencement__c is a workaround to the mismatching SObject and Id types.
            // It's a hack but it's the least evil quick solution to the problem of using showing different sobject data
            // in a single list control
            System.assertNotEquals(NULL, histories.get(0).sObjectList.get(0).get('Employer_Name_at_Commencement__c'),
                    'Id has been sent to client in Employer_Name_at_Commencement__c field');

            // UPDATE
            Employment_History__c history = (Employment_History__c) histories.get(0).sObjectList.get(0);

            Test.StartTest();
            wd = new ff_WriteData();
            wd.records.put(history.Employer_Name_at_Commencement__c, // client list control moves id out of this field into Id before value change events start
                    new Employment_History__c(
                            // Employer_Name_at_Commencement__c field used for id, simulating the client list control/driver
                            Employer_Name_at_Commencement__c = history.Employer_Name_at_Commencement__c,
                            Status__c = 'Closed',
                            Primary_Employer__c = NULL // simulate what the client sends when this field hides/clears itself
                    ));
            System.debug('wd for update: ' + wd);

            service = new AppFormsDriverController(accountId,'Special_Admissions');
            result = service.handler.save(wd);
            System.debug('edit result: ' + result);
            System.assertEquals(0, result.fieldErrors.size(), 'no errors during edit');
            System.assertEquals(0, result.recordErrors.size(), 'no errors during edit');
            System.assertEquals(false, result.results.values().get(0).get('Primary_Employer__c'),
                    'The NULL value from the client has been translated into false');
            System.debug('queries used in edit: ' + Limits.getQueries());


            Test.stopTest();

            // DELETE
            service = new AppFormsDriverController(accountId,'Special_Admissions');
            result = service.handler.deleteRecord(history.Employer_Name_at_Commencement__c); // work-around/client sends back this id for delete
            System.debug('delete result: ' + result);
            System.assertEquals(0, result.fieldErrors.size(), 'no errors during delete');
            System.assertEquals(0, result.recordErrors.size(), 'no errors during delete');
            System.debug('queries used in delete: ' + Limits.getQueries());

        }
    }   */ 
    
    // modes of record save in the education history list editor are commented in EducationHistoryControlController.js
    @IsTest
    static void testEducationSaveUndergraduate_Mode1() {

        Id accountId = TestSubscriptionUtils.getTestAccountId();
        ff_WriteData wd = new ff_WriteData();

        edu_University_Degree_Join__c joinSelected = TestDataDegreeJoinBuilder.joinedApproved(
                'Victoria University', 'New Zealand', 'Bachelor of Finance', 'Undergraduate');

        edu_Education_History__c historyRecordFromClient = new edu_Education_History__c(
                University_Degree__c = joinSelected.Id);
        wd.records.put('INSERT-edu_Education_History__c0', historyRecordFromClient);

        AppFormsDriverController service = new AppFormsDriverController(accountId,'Special_Admissions');
        ff_WriteResult result = service.handler.save(wd);
        System.debug(result);
        System.assertEquals(0, result.fieldErrors.size(), 'no errors during save');
        System.assertEquals(0, result.recordErrors.size(), 'no errors during save');

        // checking legacy education field updates
        Application__c application = getApplicationWithLegacyFields(accountId);
        System.assertNotEquals(NULL, application.Location_of_Provider__c, 'Undergrad location was populated');
        System.assertNotEquals(NULL, application.University_New_Zealand_Undergraduate__c, 'NZ Uni was populated');
        System.assertNotEquals(NULL, application.Undergraduate_nz__c, 'NZ Degree was populated');
    }
    
    @IsTest
    static void testEducationSavePostgraduate_Mode1() {
        Id accountId = TestSubscriptionUtils.getTestAccountId();
        ff_WriteData wd = new ff_WriteData();

        edu_University_Degree_Join__c joinSelected = TestDataDegreeJoinBuilder.joinedApproved(
                'Macquarie University', 'Australia', 'MBA', 'Postgraduate');

        edu_Education_History__c historyRecordFromClient = new edu_Education_History__c(
                University_Degree__c = joinSelected.Id);
        wd.records.put('INSERT-edu_Education_History__c0', historyRecordFromClient);

        AppFormsDriverController service = new AppFormsDriverController(accountId,'Special_Admissions');
        ff_WriteResult result = service.handler.save(wd);
        System.debug(result);
        System.assertEquals(0, result.fieldErrors.size(), 'no errors during save');
        System.assertEquals(0, result.recordErrors.size(), 'no errors during save');

        // checking legacy education field updates
        Application__c application = getApplicationWithLegacyFields(accountId);
        System.assertNotEquals(NULL, application.Location_of_Provider_Graduate__c, 'Postgrad location was populated');
        System.assertNotEquals(NULL, application.University_Australia_Graduate__c, 'AU Uni was populated');
        System.assertNotEquals(NULL, application.Graduate_AU__c, 'AU Degree was populated');
    }
    
    @IsTest
    static void testEducationSaveMode2() {

        Id accountId = TestSubscriptionUtils.getTestAccountId();
        ff_WriteData wd = new ff_WriteData();

        edu_Degree__c degreeSelected = new TestDataDegreeBuilder('MBA').type('Postgraduate').create(1, true).get(0);

        edu_Education_History__c historyRecordFromClient = new edu_Education_History__c(
                University_Degree__r = new edu_University_Degree_Join__c(
                        University__r = new edu_University__c(
                                University_Name__c = 'Hogwarts'
                        ),
                        Degree__c = degreeSelected.Id));
        wd.records.put('INSERT-edu_Education_History__c0', historyRecordFromClient);

        AppFormsDriverController service = new AppFormsDriverController(accountId,'Special_Admissions');
        ff_WriteResult result = service.handler.save(wd);
        System.debug(result);
        System.assertEquals(0, result.fieldErrors.size(), 'no errors during save');
        System.assertEquals(0, result.recordErrors.size(), 'no errors during save');

        // checking legacy education field updates
        Application__c application = getApplicationWithLegacyFields(accountId);
        // no location can be populated for a uni opt-out since the data is not collected
        System.assertNotEquals(NULL, application.Other_University_Graduate__c, 'Other Uni was populated');
        System.assertNotEquals(NULL, application.Graduate_au_other__c, 'AU other Degree was populated for opt-out uni');
    }

    
    @IsTest
    static void testEducationSaveMode3() {

        Id accountId = TestSubscriptionUtils.getTestAccountId();
        ff_WriteData wd = new ff_WriteData();

        edu_Education_History__c historyRecordFromClient = new edu_Education_History__c(
                University_Degree__r = new edu_University_Degree_Join__c(
                        University__r = new edu_University__c(
                                University_Name__c = 'Hogwarts'
                        ),
                        Degree__r = new edu_Degree__c(
                                Degree_Name__c = 'Wizards Walk'
                        )));
        wd.records.put('INSERT-edu_Education_History__c0', historyRecordFromClient);

        AppFormsDriverController service = new AppFormsDriverController(accountId,'Special_Admissions');
        ff_WriteResult result = service.handler.save(wd);
        System.debug(result);
        System.assertEquals(0, result.fieldErrors.size(), 'no errors during save');
        System.assertEquals(0, result.recordErrors.size(), 'no errors during save');

        // checking legacy education field updates
        Application__c application = getApplicationWithLegacyFields(accountId);
        // no location can be populated for a uni opt-out since the data is not collected
        System.assertNotEquals(NULL, application.Other_University_Undergraduate__c, 'Other Uni undergrad was populated because undergrad is assumed as default');
        System.assertNotEquals(NULL, application.Undergraduate_AU_other__c, 'AU other Degree was populated for opt-out degree');
    }
    
    
    @IsTest
    static void testEducationSaveMode4() {

        Id accountId = TestSubscriptionUtils.getTestAccountId();
        ff_WriteData wd = new ff_WriteData();

        edu_University__c uniSelected = new TestDataUniversityBuilder('University of Technology').status('Approved').country('Australia').create(1, true).get(0);

        edu_Education_History__c historyRecordFromClient = new edu_Education_History__c(
                University_Degree__r = new edu_University_Degree_Join__c(
                        University__c = uniSelected.Id,
                        Degree__r = new edu_Degree__c(
                                Degree_Name__c = 'Wizards Walk'
                        )));
        wd.records.put('INSERT-edu_Education_History__c0', historyRecordFromClient);

        AppFormsDriverController service = new AppFormsDriverController(accountId,'Special_Admissions');
        ff_WriteResult result = service.handler.save(wd);
        System.debug(result);
        System.assertEquals(0, result.fieldErrors.size(), 'no errors during save');
        System.assertEquals(0, result.recordErrors.size(), 'no errors during save');

        // checking legacy education field updates
        Application__c application = getApplicationWithLegacyFields(accountId);
        
        //Test FIle Upload
        Blob bodyBlob=Blob.valueOf('Unit Test ContentVersion Body'); 
        ContentVersion newfile = new ContentVersion(
            Title='Header_Picture1', 
            PathOnClient ='/Header_Picture1.jpg',
            VersionData = bodyBlob, 
            origin = 'H'
        );
        insert newfile;
        
        ContentVersion cVersion = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :newfile.Id LIMIT 1];
        
        
        ContentDocumentLink contentlink = new ContentDocumentLink();
        contentlink.LinkedEntityId = application.id;
        contentlink.contentdocumentid = cVersion.contentdocumentid;
        contentlink.ShareType = 'V';
        insert contentlink; 
        
        AppFormsDriverController.UpdateFiles(new List<String>{cVersion.contentdocumentid}, new List<String>{'CV'}, application.id);
        
        System.debug('Application with Legacy Fields ' + application);
        // no location can be populated for a uni opt-out since the data is not collected
        System.assertNotEquals(NULL, application.University_Australia_Undergraduate__c, 'AU Uni was populated');
        System.assertNotEquals(NULL, application.Undergraduate_AU__c, 'AU other Degree was populated for opt-out uni');
    }
    
      private static Application__c getApplicationWithLegacyFields(Id accountId) {
        return [
                SELECT Id, // other means opt out for uni and degree
                        Location_of_Provider__c, Location_of_Provider_Graduate__c,
                        // 8 undergrad fields from page layout
                        University_Australia_Undergraduate__c, University_New_Zealand_Undergraduate__c, Other_University_Undergraduate__c,
                        Undergraduate_AU__c, Undergraduate_AU_other__c, Undergraduate_nz__c, Undergraduate_nz_other__c,
                        // 8 postgrad fields from page layout
                        University_Australia_Graduate__c, University_New_Zealand_Graduate__c, Other_University_Graduate__c,
                        Graduate_au__c, Graduate_au_other__c, Graduate_nz__c, Graduate_nz_other__c
                FROM Application__c
                WHERE Account__c = :accountId
        ];
    }     
     private static final List<String> RICH_TEXT_NAMES = new List<String>{
            'AppForms.Stage0.GoodStanding',
            'AppForms.Stage1.ChecklistEmpty',
            'AppForms.Stage1.ACCAChecklist',
            'AppForms.Stage1.ATChecklist',
            'AppForms.Stage1.ATChecklistAATUK',
            'AppForms.Stage1.CPAAustraliaChecklist',
            'AppForms.Stage1.CPACanadaChecklist',
            'AppForms.Stage1.GAAChecklist',
            'AppForms.Stage1.GAAChecklistAICPA',
            'AppForms.Stage1.GAAChecklistSAICA',
            'AppForms.Stage1.GAAChecklistICAS',
            'AppForms.Stage1.GAAChecklistICAI',
            'AppForms.Stage1.GAAChecklistICAEW',
            'AppForms.Stage1.ICAZChecklist',
            'AppForms.Stage1.MICPAChecklist',
            'AppFroms.Stage6.CVandEmploymentText',
            'AppFroms.Stage6.ReferenceText',
            'Registrations.AusCreditLicence',
            'Registrations.AFSSituation',
            'Registrations.AFSMultiple',
            'CPP.RequiredMessage',
            'CPP.NotRequiredMessage',
            'CPP.CountryAus',
            'CPP.CountryAusFurtherInfo',
            'CPP.CountryNZ',
            'CPP.CountryNZDescription',
            'CPP.CountryOther',
            'CPP.Questionnaire',
            'ProvApp.Stage5.LegalDisciplinaryByStatutory',
            'ProvApp.Stage5.LegalDisciplinaryByTertiary',
            'ProvApp.Stage5.Legal',
            'ProvApp.Stage5.LegalBankruptcy',
            'ProvApp.Stage5.LegalConviction', 
            'ProvApp.Stage5.NotToManageCorporation', 
            'ProvApp.Stage6.StatementPrivacyLink',
            'ProvApp.Stage6.StatementPrivacyStatement',
            'ProvApp.Stage6.StatementDeclaration', 
            'ProvApp.Stage6.StatementEUGDPRStatement', 
            'ProvApp.Stage7.Submitted',     
            'Application.ProvisionalMembershipObligations',
            'EducationHistory.YearOfCommence',
            'EducationHistory.YearOfFinish',
            'Application.SubjectToDisciplinaryByCompany',
            'Status.opts',
            'Type.opts'
          
    };
}