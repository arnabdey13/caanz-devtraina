/***********************************************************************************************************************************************************************
Name: Account_SegmentAccChangeHandler_Test
============================================================================================================================== 
Purpose: This class contains code related to Unit Testing and test coverage of class Account_SegmentationAccountChangeHandler
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR          DATE          DETAIL     Description 
1.0        Rama Krishna    19/02/2019    Created    This class contains code related to Unit Testing and test coverage of class Account_SegmentationAccountChangeHandler
                                                             
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/
@isTest
public class Account_SegmentAccChangeHandler_Test{
    static RecordType personAccountRecordType = [SELECT Id, IsPersonType FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account' and IsPersonType = true];   
    static Id personAccountRecordTypeId = personAccountRecordType.Id;
   
    //create test data for Account using @testsetup annotation
    @testSetup static void createTestData() {
        test_TestData();
    }
    static void test_TestData(){        
        List<Account> accList = new List<Account>();
        accList = TestObjectCreator.createPersonAccountBulk(25,personAccountRecordTypeId);
        for(Integer i=0;i<accList.size();i++){
            if(i==0){
                accList[i].Designation__c = 'ACA';
                accList[i].Membership_Type__c = 'Member';     
                accList[i].FP_Specialisation__c = true;
                accList[i].Registered_Company_Auditor__c = true;
                accList[i].Registered_Company_Liquidator__c = true;
                accList[i].Registered_Tax_Agent__c = true;
                accList[i].RollUpFromProfessionalConductAndFineCost__c=true;
                accList[i].Membership_Approval_Date__c = Date.today().addDays(1); 
                accList[i].Create_Portal_User__c = true;
                accList[i].Membership_Class__c='Provisional';                          
            }else{
                accList[i].Designation__c = 'Affiliate CAANZ';
                accList[i].Membership_Type__c = 'Member';
                accList[i].RollUpFromProfessionalConductAndFineCost__c=true;
            }
        }                    
        insert accList; 
    }   

//******************************************************************************************
//                             testMethods
//****************************************************************************************** 
  
    static testMethod void testSegmentAccChangeHandler(){
        Test.startTest();
        Profile profile1 = [Select Id from Profile where name = 'CAANZ Business Management User'];
        System.debug('What is the profile id ' + profile1);
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        User u = new User( UserRoleId = portalRole.Id,
                            ProfileId = profile1.Id,
                            Username = 'testtermsconditions1234423@charteredaccountantsanz.com',
                            Alias = 'batman',
                            Email='testtermsconditions1234423@charteredaccountantsanz.com',
                            EmailEncodingKey='UTF-8',
                            Firstname='Bruce',
                            Lastname='Wayne',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_NZ',
                            TimeZoneSidKey='Pacific/Auckland');
        insert u; 
        SegmentationAddressTriggerHandlerTest.forceTriggerDuringTest = TRUE;
        //run as user to test code to perform user level testing                         
        System.runas(u){             
            list<Account> acc = [select Id,Registered_Company_Auditor__c,Registered_Company_Liquidator__c,Registered_Trustee_in_Bankruptcy__c  from Account limit 2];      
            acc[0].Membership_Type__c = 'Non Member';
            acc[0].Membership_Class__c = 'Affiliate';            
            acc[0].PersonEmail = 'testterms1234423@charteredaccountantsanz.com';
            
            acc[1].PersonOtherStreet= '33 Saggers Road';
            update acc;                          
        }
        Test.stopTest();
    }    

    //test method helps in creating records in Map AddressSegmentsToUpdateIds in class Account_SegmentationAccountChangeHandler
    static testMethod void test_updateUserRecords() {        
        TestObjectCreator.testInsertPersonAccountPortalUserCAANZ();  
    }

}