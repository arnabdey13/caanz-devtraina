/*
    Developer: WDCi (TO)
    Date: 08 Apr 2016
    Task #: Member Data Intgeration
    
    Change History:
    LCA-502 WDCi Lean: add logic to remove member permission set when member resigned
    LCA-904 WDCi KH: check is user is active or not
    LCA-1044 WDCi Lean: handle affiliate permission set assignment
    PAN:5342 12/10/2018 WDCi Lean: CAF/CAP validation
    PAN:5340 17/10/2018 WDCi LKoh: fix for the issue with the Education History Year of Finish field value being non integer
    PAN:5340 18/10/2018 WDCi LKoh: added new validation with the Education History Year of Commence to check if it has value
    DIS:0001 29/10/2018 WDCi LKoh: Disable the account trigger that currently have a CAF/CAP validation to ensure they have "approved" university & degree
*/
public with sharing class luana_MemberDataIntegrationHandler{
    
    public static Map<Id, Id> processedRecordIds = new Map<Id, Id>();
    
    public static void accountAction(List<Account> newList, Map<Id, Account> newMap, List<Account> oldList, Map<Id, Account> oldMap, boolean isInsert, boolean isUpdate, boolean isDelete, boolean isUndelete, boolean isBefore, boolean isAfter){
        
        if(isBefore){
            if(isInsert){
                validateCAPCAFAssesability(newList, oldMap); //PAN:5342
            } else if(isUpdate){
                validateCAPCAFAssesability(newList, oldMap); //PAN:5342
            }
        } else if(isAfter){    
            if(isInsert){
                checkCommUserPermSet(isInsert, isUpdate, newList, oldMap);
            } else if(isUpdate){
                checkCommUserPermSet(isInsert, isUpdate, newList, oldMap);
            }
        }
    }
    
    public static void checkCommUserPermSet(boolean isInsert, boolean isUpdate, List<Account> newList, Map<Id, Account> oldMap){
        
        Map<String, String> accRecordTypeMap = new Map<String, String>();
        for(RecordType rt: [Select Id, name, developerName, SobjectType from RecordType Where sObjectType = 'Account']){
            accRecordTypeMap.put(rt.Id, rt.developerName);
        }
        
        Set<String> contId = new Set<String>();
        Set<String> passmemberContId = new Set<String>();
        
        for(Account acc: newList){
            if(acc.isPersonAccount){
                //Change for issue LCA-404
                if(
                    (accRecordTypeMap.get(acc.RecordTypeId)== 'Full_Member' && ((isInsert || (oldMap.containsKey(acc.Id) && oldMap.get(acc.Id).Membership_Class__c != acc.Membership_Class__c)) && (acc.Membership_Class__c == 'Provisional' || acc.Membership_Class__c == 'Full' || acc.Membership_Class__c == 'Affiliate') ))
                ){   
                    if(!processedRecordIds.containsKey(acc.Id)){
                        processedRecordIds.put(acc.Id, acc.Id);
                        contId.add(acc.PersonContactId);
                    }
                } else if(acc.Membership_Type__c == 'Past Member' && isUpdate && (oldMap.containsKey(acc.Id) && oldMap.get(acc.Id).Membership_Type__c != acc.Membership_Type__c)){
                //LCA-502
                if(!processedRecordIds.containsKey(acc.Id)){
                    processedRecordIds.put(acc.Id, acc.Id);
                    passmemberContId.add(acc.PersonContactId);
                }
            }
        }
        }
            
        System.debug('**run here 3: ' + contId);
        
        //LCA-518, confirm with lean
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defNonMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_NonMember_PermSet_Id');
        
        if(!contId.isEmpty()){
            //we will skip this if no settings defined. this can help to avoid error for other non luana_* test classes.
            if(defMemberPermSetId != null && defNonMemberPermSetId != null){
                updateCommUserPermSet(contId, defMemberPermSetId.Value__c, defNonMemberPermSetId.Value__c);
            }
        }
            
        if(!passmemberContId.isEmpty()){
            //we will skip this if no settings defined. this can help to avoid error for other non luana_* test classes.
            if(defMemberPermSetId != null && defNonMemberPermSetId != null){
                removeMemberPermissionSet(passmemberContId, defMemberPermSetId.Value__c, defNonMemberPermSetId.Value__c);
            }
        }
    }

    @future
    public static void updateCommUserPermSet(Set<String> contId, String memberPermSetId, String nonMemberPermSetId){
        List<User> users = new List<User>();
        
        List<User> nzicaUsers = [select Id, Name, Profile.Name, Contact.Id from User Where contactId in: contId and isActive = true]; //LCA-904 add isActive filter
        Map<String, PermissionSetAssignment> userPermMap = new Map<String, PermissionSetAssignment>();
        for(PermissionSetAssignment psa : [SELECT AssigneeId, PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId IN: nzicaUsers]) {
            String longId = '' + psa.PermissionSetId;
            userPermMap.put(psa.AssigneeId + '|' + longId.left(15), psa);
        }

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();
        List<PermissionSetAssignment> psaRemovalList = new List<PermissionSetAssignment>();
        
        for(User u: nzicaUsers){
            if(userPermMap.get(u.Id + '|' + memberPermSetId) == null) {
                psaList.add(new PermissionSetAssignment(AssigneeId=u.Id, PermissionSetId=memberPermSetId));
            
            }
            if(userPermMap.get(u.Id + '|' + nonMemberPermSetId) != null) {
                psaRemovalList.add(userPermMap.get(u.Id + '|' + nonMemberPermSetId));
            
            }
        }
        insert psaList;
        if(!psaRemovalList.isEmpty())
            delete psaRemovalList;
    }
    
    //LCA-502
    @future
    public static void removeMemberPermissionSet(Set<String> passmemberContId, String memberPermSetId, String nonMemberPermSetId){
        List<PermissionSetAssignment> psaToDelete = [select id from PermissionSetAssignment where Assignee.ContactId in: passmemberContId and PermissionSetId =: memberPermSetId];
        
        if(psaToDelete.size() > 0)
            delete psaToDelete;
    }
    
    //PAN:5342
    public static void validateCAPCAFAssesability(List<Account> newList, Map<Id, Account> oldMap){
        
        List<Account> qualifiedAccounts = new List<Account>();
        Set<Id> contactIds = new Set<Id>();
        
        //only consider person account and one of the checkboxes is updated to "checked"
        for(Account acc : newList){
            if(acc.IsPersonAccount){
                if(oldMap != null){
                    if((acc.Assessible_for_CAF_Program__c && acc.Assessible_for_CAF_Program__c != oldMap.get(acc.Id).Assessible_for_CAF_Program__c) 
                        || (acc.Assessible_for_CA_Program__c && acc.Assessible_for_CA_Program__c != oldMap.get(acc.Id).Assessible_for_CA_Program__c)){
                        
                        qualifiedAccounts.add(acc);
                        contactIds.add(acc.PersonContactId);
                    }
                } else {
                    qualifiedAccounts.add(acc);
                    contactIds.add(acc.PersonContactId);
                }
            }
        }
        
        //get education history with approved degree and university
        Map<Id, List<edu_Education_History__c>> contactsApprovedDegreeHistories = new Map<Id, List<edu_Education_History__c>>();
        for(edu_Education_History__c eduHistory : [select Id, FP_Year_of_Commence__c, FP_Year_of_Finish__c, FP_Contact__c from edu_Education_History__c where FP_Contact__c in: contactIds]){
            
            if(contactsApprovedDegreeHistories.containsKey(eduHistory.FP_Contact__c)){
                contactsApprovedDegreeHistories.get(eduHistory.FP_Contact__c).add(eduHistory);
            } else {
                contactsApprovedDegreeHistories.put(eduHistory.FP_Contact__c, new List<edu_Education_History__c>{eduHistory});
            }
        }
        
        //init current year
        Integer currentYear = system.today().year();
        
        for(Account studentAcc : qualifiedAccounts){
            
            //default to unqualified
            boolean cafQualified = false;
            boolean capQualified = false;
            
            if(contactsApprovedDegreeHistories.containsKey(studentAcc.PersonContactId)){
                if(studentAcc.Assessible_for_CAF_Program__c){
                    /**
                    //for CAF, qualified the student if as long as it has approved education history
                    cafQualified = true;
                    **/
                    
                    // PAN:5340 - Student must have Year of Commence in the approved education history
                    for(edu_Education_History__c eduHistory : contactsApprovedDegreeHistories.get(studentAcc.PersonContactId)) {                        
                        
                        if(!String.isBlank(eduHistory.FP_Year_of_Commence__c)) {
                            cafQualified = true;                            
                        }
                    }
                }
                
                if(studentAcc.Assessible_for_CA_Program__c && !String.isBlank(studentAcc.Membership_Class__c) && (studentAcc.Membership_Class__c == 'Provisional' || studentAcc.Membership_Class__c == 'Full')){
                    //for CAP and provisional member, we need to validate the year of finish to ensure it is greater or equal current year
                    for(edu_Education_History__c eduHistory : contactsApprovedDegreeHistories.get(studentAcc.PersonContactId)){
                        //init finish year, always set to next year if the field is blank to ensure we never qualify them during comparison
                        
                        // WDCi (LKoh) 17/10/2018 PAN:5340 - Improvement to cater for the Year of Finish not being Integer
                        // Integer finishYear = String.isBlank(eduHistory.FP_Year_of_Finish__c) ? currentYear + 1 : Integer.valueOf(eduHistory.FP_Year_of_Finish__c);
                        Integer finishYear = (String.isBlank(eduHistory.FP_Year_of_Finish__c) || eduHistory.FP_Year_of_Finish__c.equalsIgnoreCase('In Progress')) ? currentYear + 1 : Integer.valueOf(eduHistory.FP_Year_of_Finish__c);
                        
                        if(currentYear >= finishYear){
                            capQualified = true;
                            break;
                        }
                    }
                }
            }
            
            //throw error if student is unqualified
            if(!cafQualified){
                if(studentAcc.Assessible_for_CAF_Program__c){
                    studentAcc.Assessible_for_CAF_Program__c.addError('The student must have a valid commenced degree education history.');
                }
            }
            
            if(!capQualified){
                if(studentAcc.Assessible_for_CA_Program__c){
                    studentAcc.Assessible_for_CA_Program__c.addError('The student must have a valid completed degree education history and is currently a Provisional/Full Member.');
                }
            }
        }
        
    }
}