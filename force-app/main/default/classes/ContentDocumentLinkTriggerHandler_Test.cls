/***********************************************************************************************************************************************************************
Name: ContentDocumentLinkTriggerHandler_Test
============================================================================================================================== 
Purpose: This class contains code related to Unit Testing and test coverage of class ContentDocumentLinkTriggerHandler
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR           DATE          DETAIL     Description 
1.0        Vinay        27/03/2019    Created     This class contains code related to Unit Testing and test coverage of class ContentDocumentLinkTriggerHandler
                                                             
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/
@isTest
public class ContentDocumentLinkTriggerHandler_Test {
    //test method to check coverage of all the events in the AccountTriggerHandler class
    static testMethod void testContentDocLinkHandlerNegative(){        
        Profile profile1 = [Select Id from Profile where name ='CAANZ Service Desk'];
        System.debug('What is the profile id ' + profile1);
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        User u = new User( UserRoleId = portalRole.Id,
                            ProfileId = profile1.Id,
                            Username = 'testtermsconditions1234423@charteredaccountantsanz.com',
                            Alias = 'batman',
                            Email='testtermsconditions1234423@charteredaccountantsanz.com',
                            EmailEncodingKey='UTF-8',
                            Firstname='Bruce',
                            Lastname='Wayne',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_NZ',
                            TimeZoneSidKey='Pacific/Auckland');
        insert u;
                
        list<ContentNote> cntNoteList = new list<ContentNote>();
        ContentNote cntNote;
        
        for(integer i=0;i<25;i++){
            cntNote = new ContentNote();
            cntNote.Title = 'Test Content Document';
            cntNote.Content = Blob.valueOf('Test Note Content');
            cntNoteList.add(cntNote);
        }        
        
        //run as user to test code to perform user level testing
        System.runas(u){
            Exception unexpectedException;
            try{    
                IncaProfilesForNotesCreation__c incaProf = new IncaProfilesForNotesCreation__c(Name='CAANZ Service Desk');
                insert incaProf;
                
                insert cntNoteList;
                system.debug('***cntNoteList-->'+cntNoteList);
                system.debug('***cntNoteList record-->'+cntNoteList[0]);
                update cntNoteList;
                delete cntNoteList;
                undelete cntNoteList;
                
                PermissionSet muteProcessesPermission = [Select Id from PermissionSet where name = 'Mute_Triggers_Processes'];
                PermissionSetAssignment mutePermissonSetAssign = new PermissionSetAssignment(PermissionSetId = muteProcessesPermission.Id, AssigneeId = u.Id);
                insert mutePermissonSetAssign;
    
                update cntNoteList;
            }catch(Exception e){            
                unexpectedException = e;
                system.debug('**error Message-->'+e.getMessage());
                system.assertEquals(null, unexpectedException);
            }            
        }       
                
    } 
    
    static testMethod void testContentDocLinkHandleroPsitive(){        
        Profile profile1 = [Select Id from Profile where name ='CAANZ Business Management User'];
        System.debug('What is the profile id ' + profile1);
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        User u = new User( UserRoleId = portalRole.Id,
                            ProfileId = profile1.Id,
                            Username = 'testtermsconditions1234423@charteredaccountantsanz.com',
                            Alias = 'batman',
                            Email='testtermsconditions1234423@charteredaccountantsanz.com',
                            EmailEncodingKey='UTF-8',
                            Firstname='Bruce',
                            Lastname='Wayne',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_NZ',
                            TimeZoneSidKey='Pacific/Auckland');
        insert u;
                
        list<ContentNote> cntNoteList = new list<ContentNote>();
        ContentNote cntNote;
        
        for(integer i=0;i<25;i++){
            cntNote = new ContentNote();
            cntNote.Title = 'Test Content Document';
            cntNote.Content = Blob.valueOf('Test Note Content');
            cntNoteList.add(cntNote);
        }        
        
        system.debug('@@Profile Name:'+ u.ProfileId);
        //run as user to test code to perform user level testing
        System.runas(u){
            Exception unexpectedException;
            
            IncaProfilesForNotesCreation__c incaProf = new IncaProfilesForNotesCreation__c(Name='CAANZ Service Desk');
            insert incaProf;
            
            try{
                if(cntNoteList != null && !cntNoteList.isEmpty())
                    insert cntNoteList;                
            }catch(Exception e){            
                unexpectedException = e;
                system.debug('**error Message-->'+e.getMessage());
                system.assertNotEquals(null, unexpectedException);
            }            
        }
                
    }   
}