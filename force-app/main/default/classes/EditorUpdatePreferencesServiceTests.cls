@IsTest(seeAllData=false)
public class EditorUpdatePreferencesServiceTests {

    @testSetup static void createTestData() {
        TestSubscriptionUtils.createTestData();
    }

    @IsTest
    public static void testReadPrefs() {

        Map<String, Object> toClient = EditorUpdatePreferencesService.load(TestSubscriptionUtils.getTestAccountId());
        Map<String, Boolean> a = (Map<String, Boolean>) toClient.get('RECORD');
        System.debug(a);
        
        /*        
System.assertEquals(false, q.Lock__c,'New questionnaire is unlocked');
System.assertNotEquals(null, q.Residential_Country__c,'Country is populated');
System.assertNotEquals(null, q.Status__c,'Status is populated');
*/
    }

    @IsTest
    public static void testWritePrefs() {
        Map<String, Object> fromClient = EditorUpdatePreferencesService.load(TestSubscriptionUtils.getTestAccountId());
        Map<String, Boolean> a = (Map<String, Boolean>) fromClient.get('RECORD');
        fromClient.put('DIRTY',new Map<Object, Object>());
        fromClient.put('LABEL-MAPPINGS',new Map<Object,Object>());

        setAnswer(fromClient,'PersonDoNotCall',true,'Do not call me');
        setAnswer(fromClient,'NL_Asia_Insight__pc',false,'Asia Magazine');

        // 4 new fields
        setAnswer(fromClient,'Business_Valuation_Community_Newsletter__pc',true,'foo bar');
        setAnswer(fromClient,'Forensic_Accounting_Community_Newsletter__pc',true,'foo bar');
        setAnswer(fromClient,'IT_Community_Newsletter__pc',true,'foo bar');
        setAnswer(fromClient,'NL_Acuity_eNewsletter__pc',true,'foo bar');

        // client sends back json which loses type info so mimic that loss
        Map<Object, Object> clientRecord = new Map<Object, Object>();
        for (String f : a.keySet()) {
            clientRecord.put(f, a.get(f));
        }
        fromClient.put('RECORD', clientRecord);

        EditorUpdatePreferencesService.save(fromClient,TestSubscriptionUtils.getTestAccountId());

        System.debug(EditorUpdatePreferencesService.load(TestSubscriptionUtils.getTestAccountId()));
    }

    // this emulates what a client editor does in its onchange handler
    private static void setAnswer(Map<String, Object> fromClient, String answerName, Boolean answerValue, String label) {
        Map<String, Boolean> rec = (Map<String, Boolean>)fromClient.get('RECORD');
        system.debug(rec);
        // when the client sees a change, it updates the sobject map
        rec.put(answerName, answerValue);
        // and writes the label was used for the field changed
        Map<Object,Object> mappings = (Map<Object,Object>) fromClient.get('LABEL-MAPPINGS');
        mappings.put(answerName, label);
        // and writes a dirty flag for the field changed 
        Map<Object,Object> dirty = (Map<Object,Object>) fromClient.get('DIRTY');
        dirty.put(answerName, true);
    }

}