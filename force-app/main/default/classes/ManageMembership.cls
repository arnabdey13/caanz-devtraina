public with sharing class ManageMembership {
    public String strUserStatus {get;set;}
    public Account personRecord {get; set;}
    public Boolean bRenderOverrideHasSalutation {get;set;}
    public Boolean bRenderOverrideHasFirstName {get;set;}   
    public Boolean bRenderOverrideHasMiddleName {get;set;}
    public Boolean bRenderOverrideHasPreferredName {get;set;}
    public Boolean bRenderOverrideHasPersonBirthDate {get;set;}
    public Boolean bRenderOverrideHasGender {get;set;}
    public String strBirthdate {get;set;}
    public List<System.selectOption> listCountryOptions {get;set;}  
    public List<Employment> employments {get;set;}
    public Employment employmentRecord {get;set;}
    public String strSearchEmployer {get;set;}
    public List<Account> listEmployerAccounts {get;set;}
    Integer iEmploymentHistoryCounter = 0;
    Account a = new Account();
    public ManageMembership()  {
        strSearchEmployer = '';
        listEmployerAccounts = new List<Account>();
        populateAvailableCountries();
        a = getPersonRecord();
        personRecord = a;
        if(a != null) {
            if(a.PersonBirthdate != null) {
                strBirthdate = a.PersonBirthdate.format();
                bRenderOverrideHasPersonBirthDate = true;
            }
            if(a.Salutation != null) bRenderOverrideHasSalutation = true;
            if(a.FirstName != null) bRenderOverrideHasFirstName = true; 
            if(a.Middle_Name__c != null) bRenderOverrideHasMiddleName = true; 
            if(a.Preferred_Name__c != null) bRenderOverrideHasPreferredName = true; 
            if(a.Gender__c != null) bRenderOverrideHasGender = true;        
        }
        populateEmployerNames();
    }
    public class Employment {
        public Integer iPosition {get;set;} 
        public Boolean bSelected {get;set;}
        public Employment_History__c employmentHistory {get;set;}
        public String strMessage {get;set;}
        public String strEmployerName {get;set;}        
        public Boolean bRenderOverrideHasStartDate {get;set;}
        public Boolean bRenderOverrideHasEndDate {get;set;}
        public Employment (String strMemberId, Integer i){
            bSelected = false;
            employmentHistory = new Employment_History__c();
            employmentHistory.Member__c = strMemberId;
            employmentHistory.Status__c = 'Current';
            strMessage = '';        
            strEmployerName = '';
            iPosition = i;
            bRenderOverrideHasStartDate = false;
            bRenderOverrideHasEndDate = false;
        }
    }
    
    public Account getPersonRecord() {
        User u = [select AccountId, ContactId, Username, Email from User where Id=:UserInfo.getUserId()];
        
        String QueryString = 'Select Year_of_first_accounting_paper__c, Year_of_Study__c, Website, Type, SystemModstamp, Subjects__c, ' +
        'Student_Type__c, Status__c, ShippingStreet, ShippingState, ShippingPostalCode, ShippingLongitude, ShippingLatitude, ' +
        'ShippingCountry, ShippingCity, Segment__c, Sector__c, Secondary_Email__c, Second_Major__c, Salutation, Resignation_Reason__c, ' +
        'Relationship_Manager__c, Region__c, RecordTypeId, Preferred_Name__c, Phone, PersonTitle, PersonOtherStreet, PersonOtherState, PersonOtherStateCode, ' +
        'PersonOtherPostalCode, PersonOtherLongitude, PersonOtherLatitude, PersonOtherCountry, PersonOtherCountryCode, PersonOtherCity, PersonHasOptedOutOfEmail, ' +
        'PersonMobilePhone, PersonHomePhone, PersonOtherPhone, Preferred_Contact_Number__c, Mailing_Company_Name__c, PersonMailingStreet, PersonMailingState,' + 
        'PersonMailingStateCode, PersonMailingPostalCode, PersonMailingLongitude, ' +
        'PersonMailingLatitude, PersonMailingCountry, PersonMailingCountryCode, PersonMailingCity, PersonLeadSource, PersonLastCUUpdateDate, ' +
        'PersonLastCURequestDate, PersonEmailBouncedReason, PersonEmailBouncedDate, PersonEmail, ' +
        'PersonDepartment, PersonContactId, PersonBirthdate, PersonAssistantPhone, PersonAssistantName, ParentId, OwnerId, ' +
        'Office_Location__c, Number_of_Provisional_Member_Employees__c, Number_of_Partners__c, Number_of_Partners_Holding_CPP__c, ' +
        'Number_of_Full_Member_Employees__c, NumberOfEmployees, Newsletter_Recipient__c, Replacement_Certificate_request__c, Name, Member_ID__c, Mentor__c, Mentor1__c, ' +
        'Membership_student_history_status__c, Membership_Type__c, Membership_Tenure__c, Mailing_and_Residential_is_the_same__c, ' +
        'Membership_Class__c, Membership_Bodies__c, Membership_Approval_Date__c, Member_Of__c, Member_Basis__c, Mature_Age_Student__c, ' +
        'MasterRecordId, Legal_Name__c, LastViewedDate, LastReferencedDate, LastName, LastModifiedDate, LastModifiedById, LastActivityDate, ' +
        'LMG__c, JigsawCompanyId, IsPersonAccount, IsPartner, IsDeleted, IsCustomerPortal, International_Student__c, Industry, ' +
        'Indigenous_Student_Maori_Descent__c, Highest_Qualification__c, Gender__c, GNA__c, First_Major__c, ' +
        'FirstName, Find_a_CA_Service__c, Financial_Category__c, Fellow__c, Fax, Employee_Coordinator__c, Designation__c, Description, ' +
        'Date_qualification_awarded__c, CreatedDate, CreatedById, Create_New_Member__c, Company_Registration_Number__c, ' +
        'Communication_Preference__c, Colleges_Applied__c, Campus_of_study__c, CPP__c, CPP_Approval_Date__c, Middle_Name__c,  ' +
        'BillingStreet, BillingState, BillingPostalCode, BillingLongitude, BillingLatitude, BillingCountry, BillingCity, ' +
        'Awarding_tertiary_institution__c, Approved_Training_Employer__c, Approved_Training_Employer_ATE__c, AnnualRevenue, Age__c, ' +
        'Affiliated_Branch__c, Active__c, Acronym__c, ATE_Co_ordinator__c, ATE_Approved_Accreditation_Date__c, AFSL_Number__c, ' +
                    '(select Employer__c, Employer__r.Name, Employee_Start_Date__c, Employee_End_Date__c, Status__c, Primary_Employer__c, ' +
                    'Approved_Training_Employer__c, Member__c, Partner__c, Job_Title__c, Work_Hours__c, Director__c, ' +
                    'Full_Membership__c ' +
                    'from Employment_History__r order by Employee_Start_Date__c desc) ' +
        'from Account where Id=\'';
        
        if (UserInfo.getUserName().contains('tom.cusack') )
            QueryString += '001O000000LIkEi';
        else
            QueryString += u.AccountId;

        QueryString += '\' limit 1';
        
        List<Account> accounts = WithoutSharing.queryAccountObject(QueryString);
        if(accounts.size()>0) {
            Account account = accounts[0];
            employments = new List<Employment>();
            if(account.Employment_History__r != null && account.Employment_History__r.size()>0) {
                for(Employment_History__c emp:account.Employment_History__r) {
                    Employment e = new Employment(account.Id, iEmploymentHistoryCounter);
                    iEmploymentHistoryCounter++;
                    e.employmentHistory = emp;
                    if(e.employmentHistory.Employee_Start_Date__c != null) e.bRenderOverrideHasStartDate = true;
                    if(e.employmentHistory.Employee_End_Date__c != null) e.bRenderOverrideHasEndDate = true;
                    employments.add(e);
                }
            }
            return account;
        }
        else{
            return new Account();
        }
    }
    
       public PageReference save() {
        // Update Employment History
        List<Employment_History__c> employmentHistories = new List<Employment_History__c>();
        for(Employment emp:employments) {
            emp.strMessage = '';
            if(
            emp.employmentHistory.Employer__c!=null ||
            emp.employmentHistory.Job_Title__c!=null ||
            emp.employmentHistory.Employee_Start_Date__c!=null
            ){
                employmentHistories.add(emp.employmentHistory); // ignore the row if mandatory fields are not filled in
            }
        }
        
        List<Database.Upsertresult> saveResults;
        Boolean bHasErrors = false;
        for(Employment_History__c e:employmentHistories) {
            if(e.Id == null || (e.Id != null && string.valueof(e.Id) == '')) 
                e.Id = null;
        }
        saveResults = WithoutSharing.upsertEmploymentHistoryList( employmentHistories );
        // Employment History Save Results -- UPDATE
        Map<Employment_History__c, String> mapRecordErrorMessages = new Map<Employment_History__c, String>();
        Integer i = 0;
        for(Database.Upsertresult sr:saveResults) {
            if(!sr.isSuccess()) {
                system.debug('### ERROR UPDATE: ' + employmentHistories[i].Id + ': ' + sr.getErrors());
                mapRecordErrorMessages.put(employmentHistories[i], sr.getErrors()[0].getMessage());
                bHasErrors = true;
            }
            i++;
        }
        for(Employment emp:employments) {
            if(mapRecordErrorMessages.containsKey(emp.employmentHistory)) {
                system.debug('[error]: ' + mapRecordErrorMessages.get(emp.employmentHistory));
                //emp.strMessage = 'Field: ' + mapRecordErrorMessages.get(emp.employmentHistory.Id)[0].getFields() + ' ' +
                // 'Error: ' + mapRecordErrorMessages.get(emp.employmentHistory.Id)[0].getMessage();
                emp.strMessage = mapRecordErrorMessages.get(emp.employmentHistory); // Display friendly error message
            }
        }
        if(bHasErrors){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                'Please check the Employment History for errors.' ));
            return null;
        }
        
        // Update Person Account
        try {
            update a;
        }
        catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                '' + e.getMessage() ));
            return null;
        }
        return new PageReference('/ManageMembership');
    }   
    public PageReference edit() {
        return new PageReference('/EditMembership');
    }
    public PageReference cancel() {
        return new PageReference('/ManageMembership');
    }
    /*
    @future
    public static void updatePersonAccountDetails(Id idPersonAccount, String strMemberID,
        String strSalutation, String strFirstName, String strLastName, String strMiddleName, String strPreferredFirstName, 
        Date dtBirthdate, String strGender, String strEmail, String strMobilePhone, String strHomePhone, String strOtherPhone, String strPreferredContactNumber, 
        String strResidentialStreet, String strResidentialSuburb, String strResidentialCity, String strResidentialCountry, String strResidentialPostalCode,
        String strMailingStreet, String strMailingSuburb, String strMailingCity, String strMailingCountry, String strMailingPostalCode,
        String strCommunicationPreference, Boolean bNewsLetterRecipient) {
            Account accountMember = new Account(Id = idPersonAccount);
            accountMember.Member_ID__c = strMemberID;
            accountMember.Salutation = strSalutation; 
            accountMember.FirstName = strFirstName; 
            accountMember.LastName = strLastName; 
            accountMember.Middle_Name__c = strMiddleName;
            accountMember.Preferred_Name__c = strPreferredFirstName; 
            accountMember.PersonBirthDate = dtBirthdate; 
            accountMember.Gender__c = strGender; 
            accountMember.PersonEmail = strEmail; 
            accountMember.PersonMobilePhone = strMobilePhone;
            accountMember.PersonHomePhone = strHomePhone;
            accountMember.PersonOtherPhone = strOtherPhone;
            accountMember.Preferred_Contact_Number__c = strPreferredContactNumber;
            accountMember.PersonOtherStreet = strResidentialStreet; 
            accountMember.PersonOtherState = strResidentialSuburb; 
            accountMember.PersonOtherCity = strResidentialCity;
            accountMember.PersonOtherCountry = strResidentialCountry; 
            accountMember.PersonOtherPostalCode = strResidentialPostalCode;
            accountMember.PersonMailingStreet = strMailingStreet;
            accountMember.PersonMailingState = strMailingSuburb;  
            accountMember.PersonMailingCity = strMailingCity; 
            accountMember.PersonMailingCountry = strMailingCountry; 
            accountMember.PersonMailingPostalCode = strMailingPostalCode;
            accountMember.Communication_Preference__c = strCommunicationPreference; 
            accountMember.NewsLetter_Recipient__c = bNewsLetterRecipient;   
            Database.Saveresult sr = Database.update(accountMember, false);             
            if(!sr.isSuccess()) system.debug('### ERROR: ' + sr.getErrors());           
    }
    */
    public void populateEmployerNames() {
        if(employments != null && employments.size()>0) {   
            Set<String> setAccountIds = new Set<String>();
            for(Employment emp:employments) {
                if(emp.employmentHistory.Employer__c != null)
                    setAccountIds.add(emp.employmentHistory.Employer__c);
            }
            Map<Id, Account> mapAccountNames = new Map<Id, Account>(
                WithoutSharing.queryAccountObject('Select Name from Account where Id IN ', setAccountIds)
            );
            for(Employment emp:employments) {
                if(emp.employmentHistory.Employer__c != null && mapAccountNames.containsKey(emp.employmentHistory.Employer__c))
                    emp.strEmployerName = mapAccountNames.get(emp.employmentHistory.Employer__c).Name;
            }           
        }
    }   
    public void populateAvailableCountries() {
        listCountryOptions = new List<System.selectOption>();
        for(Country__c c:[select Name from Country__c order by Order__c limit 500]) {           
            listCountryOptions.add(new System.selectOption(String.escapeSingleQuotes(c.Name), String.escapeSingleQuotes(c.Name)));
        }
    }   
    public void addEmploymentRow() {
        System.debug('##1 ' + employments);
        if(employments == null || employments.size() == 0)
            employments = new List<Employment>();
        Employment emp = new Employment(a.Id, iEmploymentHistoryCounter);
        iEmploymentHistoryCounter++;
        employments.add(emp);
        System.debug('##2 ' + emp);
    }
    public void removeEmploymentRow() {
        if(employments != null && employments.size()>0) {
            List<Employment> temp = new List<Employment>();
            for(Employment emp:employments) {
                if(emp.bSelected != true) {
                    temp.add(emp);
                }
            }
            employments.clear();
            employments = temp;
        }
    }
    // Add Page Messages
    public void addPageMessage(ApexPages.severity severity, Object objMessage) {
        ApexPages.Message pMessage = new ApexPages.Message(severity, String.valueOf(objMessage));
        ApexPages.addMessage(pMessage);
    }
    public void addPageMessage(Object objMessage) {
        addPageMessage(ApexPages.severity.INFO, objMessage);
    }   
    public Boolean displayPopup {get {return (displayPopup == null)? false:displayPopup;} set;}
    public void closePopup() {
        displayPopup = false;
    }
    public void showPopup() {
        displayPopup = true;
    }
    public String accName{get;set;}
    public Id accId {get;set;}
    @RemoteAction
    public static List<Account> getAccounts(string searchTerm){
        List<Account> accList = new List<Account>();
        if(searchTerm != ''){
            String query = 'select Name, Member_ID__c, BillingCity from Account where Name like \'%' +  String.escapeSingleQuotes(searchTerm) + '%\'';
            accList = WithoutSharing.queryAccountObject(query);
        }
        return accList;
    }
    @RemoteAction
    public static Account getAccount(string strId){
        List<Account> accList = new List<Account>();
        if(strId != '') {
            String query = 'select Name, Member_ID__c, BillingStreet, BillingState, BillingCity, BillingCountry, BillingPostalCode from Account where Id = \'' +  strId + '\'';
            accList = WithoutSharing.queryAccountObject(query);
        }
        return accList[0];
    }
    public String strSelectedEmployerId {get;set;}
    public String strSelectedRowNumber {get;set;}
    public void SelectEmployer() {
        system.debug('strSelectedEmployerId: ' + strSelectedEmployerId);
        system.debug('strSelectedRowNumber: ' + strSelectedRowNumber);
        system.debug('employments: ' + employments);
        if(employments != null && employments.size()>0 && strSelectedEmployerId != null && strSelectedRowNumber != null
            && strSelectedEmployerId != '' && strSelectedRowNumber != '') {
            List<Employment> temp = new List<Employment>();
            for(Employment emp:employments) {
                if(emp.iPosition == Integer.valueOf(strSelectedRowNumber)) {
                    emp.employmentHistory.Employer__c = strSelectedEmployerId;
                }
                temp.add(emp);
            }
            employments.clear();
            employments = temp;
            populateEmployerNames();
        }
    }
}