/********************************************************************************************************************
Name: ApexUtil 
============================================================================================================
Purpose: Utility Class which will contain Utility methods that can be used across the Apex Coding. 
============================================================================================================ 
History 
-----------------------------------------------------------------------------------------------
VERSION    AUTHOR     DATE      DETAIL     Description 
1.0        Vinay     31/01/2019 Created    Utility Class which will contain Utility methods 
                                                  that can be used across the Apex Coding.
-----------------------------------------------------------------------------------------------
********************************************************************************************************************/
public Class ApexUtil{
    static Trigger_Settings_Configuration__mdt muteTrigCon;
    static string lastAPIName;
    static List<CustomPermission> customPermissions;
    static List<SetupEntityAccess> setupEntities;
    
    //This Method will give the Trigger Settings Configuration Custom Meta Data Record for the Object API given.
    public static Trigger_Settings_Configuration__mdt getTriggSettConf(String sObjectAPIName){        
        List<Trigger_Settings_Configuration__mdt > muteTrigConfList;
        if(lastAPIName != sObjectAPIName || muteTrigCon == null){
            lastAPIName = sObjectAPIName;        
            muteTrigConfList = [Select Trigger_Mute__c,Trigger_Loop_Count__c from Trigger_Settings_Configuration__mdt 
                                                                            where MasterLabel =: sObjectAPIName];
        }
        
        if(muteTrigConfList != null && !muteTrigConfList.isEmpty()){            
            muteTrigCon = muteTrigConfList[0];            
            return muteTrigCon;
        }else{
            return null;
        }        
    }
    
    //This Method will give the whether Current User has given Custom Permission assigned.
    public static Boolean getIfUserHasCustPerm(String custPerName){        
        if(customPermissions == null)
            customPermissions = [SELECT Id, DeveloperName FROM CustomPermission WHERE DeveloperName =: custPerName];
        
        if(setupEntities == null && customPermissions != null && !customPermissions.isEmpty())
            setupEntities = [SELECT SetupEntityId FROM SetupEntityAccess WHERE SetupEntityId in :customPermissions 
                                                 AND ParentId IN (SELECT PermissionSetId FROM PermissionSetAssignment WHERE 
                                                 AssigneeId = :UserInfo.getUserId())];
                                          
        If(setupEntities != null && !setupEntities.isEmpty())
            return TRUE;
            
        return FALSE;
    }
    
    //This Method will give the Record Type Id of the type of sObject and recordtype name given.
    public static Id getRecordTypeId(sObject sObjectType, String recordTypeDevName){
        return getRecordTpeInfoByObject(String.valueOf(sObjectType.getSObjectType())).get(recordTypeDevName).getRecordTypeId();        
    }
    
    //This Method will give the Record Type Id of sObject and recordtype name given.
    public static Id getRecordTypeId(String sObjectType, String recordTypeDevName){
        return getRecordTpeInfoByObject(sObjectType).get(recordTypeDevName).getRecordTypeId();        
    }
    
    //This will give map of Record Types for the given sobject api name
    public static Map<String,Schema.RecordTypeInfo> getRecordTpeInfoByObject(String objectApiName){
        Schema.DescribeSObjectResult[] descResult = Schema.describeSObjects(new List<String>{objectApiName});
        Map<String,Schema.RecordTypeInfo> rtMapByName;
        if(descResult != null && !descResult.isEmpty())
            rtMapByName = descResult[0].getRecordTypeInfosByDeveloperName();
        
        return rtMapByName;        
    }
}