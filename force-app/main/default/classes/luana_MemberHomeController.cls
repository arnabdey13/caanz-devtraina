/**
    Change Histroy:
    LCA-487 07/07/2016 - WDCi Lean: update authentication redirection to custom login page
**/

public without sharing class luana_MemberHomeController {
    
    public Id custCommConId {get; private set;}
    public Id custCommAccId {get; private set;}
    
    /* lean - obsolete
    public Education_Record__c educationRecordObj {get; set;}
    public List<Education_Record__c> returnedEduRecords {get; set;}*/
    
    /* lean - obsolete
    public boolean updateEnterUniInstFlag{get; set;}*/
    
    /* lean - obsolete
    private ApexPages.StandardSetController setEdu;
    
    public luana_MemberHomeController(ApexPages.StandardController controller) {
        educationRecordObj = new Education_Record__c();
        returnedEduRecords = new List<Education_Record__c>();
        
        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId());
        this.custCommConId = commUserUtil.custCommConId;
        //this.custCommConId = '003p0000003PlpE'; 
        
    }*/
    
    public luana_MemberHomeController(){
        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId()); 
      
        this.custCommConId = commUserUtil.custCommConId;       
        //this.custCommConId = '003p0000003PlpE';       
        this.custCommAccId = commUserUtil.custCommAccId;       
        
        /* lean - obsolete
        educationRecordObj = new Education_Record__c();*/   
        
        /* lean - obsolete
        updateEnterUniInstFlag = false;*/
        
    }
    
    public PageReference viewEnrolments() {
        return Page.luana_MemberMyEnrolment;
    }
        
    /* lean - obsolete
    public PageReference nonMemberEnrolment(){
        return new PageReference('/nonmember/luana_NonMemberMyEnrolment');
    }*/
    
    public PageReference forwardToAuthPage(){
        if(UserInfo.getUserType() == 'Guest'){
            
            //Fix for issue LCA-327 & LCA-328
            //return new PageReference('/CommunitiesLogin');
            return new PageReference(URL.getSalesforceBaseUrl().toExternalForm()+'/member/luana_Login?startURL=/member/luana_MemberHome'); //LCA-487
        }
        else{
            return null;
        }
    }
    
    /* lean - obsolete
    public List<Education_Record__c> getEduRecords() {
        return [SELECT Id, Name, Enter_university_instituition__c, Specialist_hours_code__c, Community_User_Entry__c, Verified__c, Date_Commenced__c, Event_Course_name__c, Qualifying_hours__c, Qualifying_hours_type__c, Contact_Student__c, CAHDP__c FROM Education_Record__c WHERE Contact_Student__c =: custCommConId ORDER BY Date_Commenced__c DESC LIMIT 10];
       
    }
    public List<LuanaSMS__Student_Program__c> getMyRecEnrolments() {
        return [SELECT Id, Name, LuanaSMS__Course__c, LuanaSMS__Course__r.Name, LuanaSMS__Status__c, LuanaSMS__Course__r.LuanaSMS__Start_Date__c, LuanaSMS__Course__r.Enrolment_Start_Date__c, LuanaSMS__Course__r.Enrolment_End_Date__c, Paid__c FROM LuanaSMS__Student_Program__c WHERE LuanaSMS__Contact_Student__c =: custCommConId AND RecordType.DeveloperName <>: 'Accredited_Program' ORDER BY CreatedDate DESC LIMIT 10];
    
    }*/
    
    /* lean - obsolete
    public PageReference viewAllEducationRecords(){
        
        PageReference pageRef = new PageReference('/luana_MemberMyEduRecords');
        return pageRef;
    }*/
    
    public PageReference viewEducations(){
        return Page.luana_MyEducationRecordsSearch;
    }
    
    public PageReference doNewEducationRecord(){
        return Page.luana_MyNewEducationRecord;
    }
    
    /* lean - not being used
    public List<Education_Record__c> getAllEduRecords() {
        return [SELECT Id, Name, Specialist_hours_code__c, Community_User_Entry__c, Date_Commenced__c, Event_Course_name__c, Qualifying_hours__c, 
                Qualifying_hours_type__c, Contact_Student__c, CAHDP__c, Date_completed__c, Country__c, State__c, University_professional_organisation__c,
                University_professional_organisation__r.Name FROM Education_Record__c 
                WHERE Contact_Student__c =: custCommConId ORDER BY Date_Commenced__c DESC];
        
    }*/
    
    /* lean - obsolete
    public void search(){
        String result = filter();
        returnedEduRecords = Database.query(result);
        this.setEdu= new ApexPages.StandardSetController(returnedEduRecords);
        this.setEdu.setpageNumber(1);
        this.setEdu.setPageSize(200);
    }
    
    public date dateFrom{get; set;}
    public date dateTo{get; set;}
    public String uniProfOrg {get; set;}
        
    public String filter(){
        
        String whereStr = '';
        
        Map<String, String> filterMap = new Map<String, String>();

        filterMap.put('Qualifying_hours__c', 'Qualifying_hours__c = ');
        filterMap.put('Enter_university_instituition__c', 'Enter_university_instituition__c like ');
        filterMap.put('Event_Course_name__c', 'Event_Course_name__c like ');
        
        Map<String, String> filterValue = new Map<String, String>();
                   
        if(educationRecordObj.Qualifying_hours_type__c!=null){
            filterValue.put('Qualifying_hours__c', '\''+educationRecordObj.Qualifying_hours__c+'\'');
        }else{
            filterValue.put('Qualifying_hours__c', null);
        }
        if(uniProfOrg != null && uniProfOrg != ''){
            filterValue.put('Enter_university_instituition__c', '\'%'+String.escapeSingleQuotes(uniProfOrg)+'%\'');
        }else{
            filterValue.put('Enter_university_instituition__c', null);
        }
        if(educationRecordObj.Event_Course_name__c !=null && educationRecordObj.Event_Course_name__c != ''){
            filterValue.put('Event_Course_name__c', '\'%'+String.escapeSingleQuotes(educationRecordObj.Event_Course_name__c)+'%\'');
        }else{
            filterValue.put('Event_Course_name__c', null);
        }        
        
        String dateSearch;
        if(dateFrom != null && dateTo == null){
            String fDate = datetime.newInstance(dateFrom.year(), dateFrom.month(),dateFrom.day()).format('yyyy-MM-dd');
            dateSearch = '(Date_Commenced__c >= ' + fDate + ')';
        } else if(dateTo != null && dateFrom == null){
            String tDate = datetime.newInstance(dateTo.year(), dateTo.month(),dateTo.day()).format('yyyy-MM-dd');
            dateSearch = '(Date_Commenced__c <= ' + tDate + ')';
        }else if(dateTo != null && dateFrom != null){
            String fDate = datetime.newInstance(dateFrom.year(), dateFrom.month(),dateFrom.day()).format('yyyy-MM-dd');
            String tDate = datetime.newInstance(dateTo.year(), dateTo.month(),dateTo.day()).format('yyyy-MM-dd');
            dateSearch = '(Date_Commenced__c >= ' + fDate + ' and Date_Commenced__c <= ' + tDate + ')';
        }else{
            dateSearch = null;
        }
        
        String filterStr = prepareFilterStr(filterMap, filterValue, dateSearch);
        
        String queryStr = 'SELECT Id, Name, Specialist_hours_code__c, Community_User_Entry__c, Date_Commenced__c, Event_Course_name__c, Qualifying_hours__c, '+
                'Qualifying_hours_type__c, Contact_Student__c, CAHDP__c, Date_completed__c, Country__c, State__c, University_professional_organisation__c,'+
                'University_professional_organisation__r.Name, Enter_university_instituition__c, Verified__c FROM Education_Record__c ' + filterStr + ' ORDER BY Date_Commenced__c DESC';
                
        System.debug('***queryStr: ' + queryStr);
        return queryStr;
    }
    
    public String prepareFilterStr(Map<String, String> filterMap, Map<String, String> filterValue, String dateSearch){
        
        String whereStr = '';
        
        List<String> filters = new List<String>();
        if(!filterMap.isEmpty()){
            for(String strField: filterMap.keySet()){
                if(filterValue.get(strField) != null){
                    filters.add(filterMap.get(strField) + filterValue.get(strField));
                }
            }
            filters.add('Contact_Student__c=\''+ String.escapeSingleQuotes(custCommConId) + '\'');
        }
        if(dateSearch != null){
            filters.add(dateSearch);
        }
        for(String filterStr: filters){
            whereStr += filterStr + ' and ';
        }
        
        if(whereStr.length() > 0){
            return ' Where ' + whereStr.subString(0, whereStr.lastIndexOf('and'));
        }else{
            return '';
        } 
        
        return null;
    }
    
    public Integer getTotalRecordFound(){
        return returnedEduRecords.size();
    }

    public PageReference addNewEducationRecord(){    
        
        educationRecordObj.Contact_Student__c = custCommConId;
        educationRecordObj.Community_User_Entry__c = true;
        
        
        if(selectedUniProOrg.trim() != null && selectedUniProOrg.trim().length() > 0){
            educationRecordObj.University_professional_organisation__c = selectedUniProOrg;
            
            if(educationRecordObj.Enter_university_instituition__c == selectedUniProOrgName){
                educationRecordObj.Enter_university_instituition__c = selectedUniProOrgName;
            }
        }
        
        insert educationRecordObj;
        
        PageReference pageRef = new PageReference('/luana_MemberHome');
        return pageRef;
    }*/
    
    /* lean - obsolete
    public PageReference cancel(){
        PageReference pageRef = new PageReference('/member');
        return pageRef;
    }*/
    
    /* lean - obsolete
    public PageReference goToAddEducationRecordPage(){
        educationRecordObj = new Education_Record__c(); 
        
        PageReference pageRef = new PageReference('/luana_MemberNewEducationRecord');
        return pageRef;
    }*/
    
    /* lean - obsolete
    public String searchUniProOrg {get; set;}
    public String selectedUniProOrg {get; set;}
    public String selectedUniProOrgName {get; set;}
    
    // JS Remoting action called when searching for a University professional organisation
    @RemoteAction
    public static List<Account> searchUniProfessionalOrg(String searchUniProOrg) {
        List<Account> accounts = new List<Account>();
        accounts = Database.query('Select Id, Name from Account where name like \'%' + String.escapeSingleQuotes(searchUniProOrg) + '%\' and RecordType.Name = \'Business Account\'');
        return accounts;
    }
    */
    
}