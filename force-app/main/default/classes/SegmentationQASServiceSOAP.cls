/**
* @author       Andrew Kopec
* @date         17/08/2017
* @group        QAS 
* @description  SOAP utils to execute SOAP QAS_SOAP_Address_Service call to Experian QAS
                class to run Experian QAS service to obtain the required address element(s)
  @required     WebServiceLog__c Object
                MetaData Configuration object with entry for QAS SOAP
				TODO: Make the debug flag configurable
  @rev
				akopec R01 fix - do not run second call if Callout Error to avoid  "You have uncommitted work pending"
                           fix - remove special characters from Address - breaks XML request
**/
public class SegmentationQASServiceSOAP {
    // QAS SOAP configurations
    public static final String CONFIG_QAS_DOSEARCH_SOAP_ACTION      = 'http://www.qas.com/OnDemand-2011-03/DoSearch';
    public static final String CONFIG_QAS_GETADDRESS_SOAP_ACTION    = 'http://www.qas.com/OnDemand-2011-03/DoGetAddress';
    public static final String CONFIG_QAS_DEFAULT_NAMESPACE         = 'http://www.qas.com/OnDemand-2011-03';
    //Path to State Tranisition in Response from the DoSearch method for Engine=Verification to determine status
    public static final String CONFIG_QAS_DOSEARCH_STATE_TRANS_PATH    = '/soap:Envelope/soap:Header/QAInformation';
    //Path to Verification Information in Response from the DoSearch method for Engine=Verification 
    public static final String CONFIG_QAS_DOSEARCH_VERIFY_LEVEL_PATH    = '/soap:Envelope/soap:Body/QASearchResult';
    //Path to Address Results Information in Response from the DoSearch method for Engine=Verification 
    //public static final String CONFIG_QAS_DOSEARCH_VERIFY_RESULTS_PATH  = '/soap:Envelope/soap:Body/QASearchResult/QAAddress';
    public static final String CONFIG_QAS_DOSEARCH_VERIFY_RESULTS_PATH  = '/soap:Envelope/soap:Body/QASearchResult/QAAddress';
    
    //Path to Address Results Information in Response when getAddress method is used  
    public static final String CONFIG_QAS_GETADDRESS_RESULTS_PATH       = '/soap:Envelope/soap:Body/Address/QAAddress';
    //Path to Pick List Results in Response when Verification Level is not Verified  
    public static final String CONFIG_QAS_DOSEARCH_QAPICKLIST_PATH      = '/soap:Envelope/soap:Body/QASearchResult/QAPicklist'; 
    //Path to Pick List Entries in Response when Verification Level is not Verified  
    public static final String CONFIG_QAS_DOSEARCH_PICKLIST_ENTRY_PATH  = '/soap:Envelope/soap:Body/QASearchResult/QAPicklist/PicklistEntry';
    public static Boolean debugFlag = false;

    /*********************************************************************************************
    * runQASSearch
    * @description  the main function to execute Experian QAS service and return address elements
    *               Steps:
    * 				1. Quick Search - Execute Search with Verification Engine 
    *               2. For Not succesfull calls try fuzzy method - with Singleline Engine
    *
    * @param        String sfId          - Salesforce ID - for logging purpose
    * @param        String searchAddress - the street address to search
    * @param        String countryCode   - the country the address belongs to
    *
    * @return       Map <String, String>  Map of address elements and processing flags
    *				Structure               
    *               <AddressLine1 ,      >
    *			    <AddressLine2 ,      >
    *			    <AddressLine3 ,      >
    *			    <AddressLine4 ,      >
    *				<MeshBlockID  ,      >
    *				<MeshBlockCode,      >
    *				<VerificationLevel,  >
    *				<Status       ,      >
    *               <EngineType   ,      >
    **********************************************************************************************/
	public static Map<String, String> runQASSearch(String sfId, String searchAddress, String countryCode) {
		Map<String, String> addrElements;
        String error;
        
        addrElements = runQuickQASSearch(sfId, searchAddress, countryCode);
		if (debugFlag) System.debug('runQuickQASSearch: address elements: ' + addrElements);
		
        if (!addrElements.isEmpty()) {
			error = addrElements.get('ERROR');
            // ak R01 fix - do not run again if CalloutSOAPServiceError to avoid  "You have uncommitted work pending"
			if (error!=null && error != 'Unsuccessful QuickQAS HTTP call') {
				addrElements = runFuzzyQASSearch(sfId, searchAddress, countryCode);  
                if (debugFlag) System.debug('runFUZZYQASSearch: address elements: ' + addrElements);}
  		}
        if (!addrElements.isEmpty()) {
            error = addrElements.get('ERROR');
        	if (error!=null) {
                if (error == 'No Matches' || error == 'Too Many Matches' || error == 'Unrecognised Status'){
                    if (debugFlag) System.debug('runQASSearch Error: address elements: ' + addrElements);
            		logWSMessage('QAS_SOAP_Address_Service', 'INFO', 'SegmentationQASServiceSOAP', 'runQASSearch', sfId, error + ': ' + searchAddress);}
            }
        }
		return addrElements;
	}
    
    /*********************************************************************************************
    * runQuickQASSearch
    * @description  function to run Quick call - using the Verification engine 
    * @param        String sfId          - Salesforce ID - for logging purpose
    * @param        String searchAddress - the street address to search
    * @param        String countryCode   - the country the address belongs to
    *
    **********************************************************************************************/
    public static Map<String, String> runQuickQASSearch(String sfId, String searchAddress, String countryCode) {
        Map<String, String> addrElements;
        Long startTime = System.now().getTime();
        
        String response = doSearchVerificationQAS(searchAddress, countryCode);
        if (debugFlag) System.debug('runQuickQASSearch:' + response);
        if(response.contains('CalloutSOAPServiceError')){
            logWSMessage('QAS_SOAP_Address_Service', 'ERROR', 'SegmentationQASServiceSOAP', 'runQuickQASSearch', sfId, response);
            return new Map<String, String> {'ERROR' => 'Unsuccessful QuickQAS HTTP call'};
        }
        addrElements = processDoSearchResults(response, countryCode, 'Verification');
        Long endTime = System.now().getTime();
        if (debugFlag) System.debug('doSearchQAS: RESPONSE : response time:' + (endTime-startTime) + ':' + addrElements);
        
        if (!addrElements.isEmpty()) {
            addrElements.put('EngineType', 'Verification' );  }
        return addrElements;
    }
    
    /*********************************************************************************************
    * runFuzzyQASSearch
    * @description  function to run Fuzzy call - using the QAS Singleline engine 
    *               typically the verification service did not return good results 
    *               so try fuzzy search using Singleline engine - slower
    *               Some custom logic expected in future
    * @param        String sfId          - Salesforce ID - for logging purpose
    * @param        String searchAddress - the street address to search
    * @param        String countryCode   - the country the address belongs to
    *
    **********************************************************************************************/
    public static Map<String, String> runFuzzyQASSearch(String sfId, String searchAddress, String countryCode) {
        Map<String, String> addrElements;
        Long startTime = System.now().getTime();
    	String response = SegmentationQASServiceSOAP.doSearchSinglelineQAS(searchAddress, countryCode);
        if (debugFlag) System.debug('runFuzzyQASSearch:' + response);
        if(response.contains('CalloutSOAPServiceError')){
            logWSMessage('QAS_SOAP_Address_Service', 'ERROR', 'SegmentationQASServiceSOAP', 'runFuzzyQASSearch', sfId, response);
            return new Map<String, String> {'ERROR' => 'Unsuccessful FuzzyQAS HTTP call'};
        }
        addrElements = processDoSearchResults(response, countryCode, 'Singleline');
        Long endTime = System.now().getTime();
        if (debugFlag) System.debug('doSearchQAS: RESPONSE : response time:' + (endTime-startTime) + ':' + addrElements);
        
        if (!addrElements.isEmpty()) {
            addrElements.put('EngineType', 'Singleline' );  }
        return addrElements;
    }
    
    /************************************************************************************************
    * processDoSearchResults
    * @description  function to process Experian doSearch results and return address elements. 
    *               Works for both engines:
    *               - Verification
    *               - Singleline
    *               Steps:
    * 				1.  check the status of the operation (StateTransition)
    * 					Possible values:
    *                           SearchResults  - Address Verified and Address Details returned - Success
    *                           PickList       - Address Partially verified, requires pick selection. No Address Details - Success with lower confidence
    *                           TooManyMatches - Address Partially matched, however too many results - may contain results if the list is short
    *                           NoMatches      - Address Not Matched - Not successful
    *                           
    *               2. For Picklist results- does not contain Address Details, a separate call is required
    *                   Number of returned results is determined through Threshold variable in DoSearch call. 
    *                         Step 1 - call getSearchResultMoniker to Parse results and return the Best Address Moniker (Address ID)
    *                         Step 2 - calls getAddressQAS with Address ID to obtain address details 
    * 
    * @param        String searchAddress - the street address to search
    * @param        String country       - the country the address belongs to
    *
    * @return       Map <String, String>  
    * @example      SegmentationQASServiceSOAP.runQASSearch( '119 Meadowland Dr, Auckland' , 'NZ' );
    * @example return:
    *               <AddressLine1 , 1563 Pacific Hwy     >
	*			    <AddressLine2 ,                      >
    *			    <AddressLine3 ,                      >
    *			    <AddressLine4 , WAHROONGA  NSW  2076 >
	*				<MeshBlockID  , MB1610418390000      >
	*				<MeshBlockCode, 10418390000          >
	*				<Verification , Verified             >
	*				<Status       , SearchResults        >
    **********************************************************************************************/
    public static Map<String, String> processDoSearchResults(String searchResponse, String countryCode, String engineType) {
        Map<String, String> addrElements;
        String VerificationLevel;
        String address;
        
        // check the status of the operation - StateTransition 
        String status = getDoSearchVerificationStatus(searchResponse);
		
        /** Status = SearchResults, QAS found, matched and verified address
        *   Result contains Address Details
        **/
        if (status == 'SearchResults'){
            
            if (engineType=='Verification') {
                VerificationLevel = getDoSearchVerificationLevel(searchResponse);}
            else {
                VerificationLevel = 'Fuzzy'; }
            
            addrElements = parseAddressElements(searchResponse, 'VERIFIED');

        }
        /** Status = PickList - QAS found matches to more than one address. Requires pick action from provided results. 
        *   Could be returned by both searches: Quick (Verification) and Fuzzy (Singleline). The Singleline engine does not provide Verification Level
        **/
        else if  (status == 'PickList'){
            VerificationLevel = getDoSearchVerificationLevel(searchResponse);
			address  = getPicklistBestAddress(searchResponse, countryCode);
            addrElements = parseAddressElements(address, 'GETADDRESS');
        }
        
		/** Status = TooManyMatches - most likely problem with address, engine found too many addresses that could match 
        *   It may or may not return results possibly based on how long the list is. If contain results Pick list behaviour
        **/
        else if (status == 'TooManyMatches'){
        	VerificationLevel = getDoSearchVerificationLevel(searchResponse);
            if ( getPickListTotal(searchResponse) == 0 ) {
                 return new Map<String, String> {'ERROR' => 'Too many Matches' };
            }
            address  = getPicklistBestAddress(searchResponse, countryCode);
            addrElements = parseAddressElements(address, 'GETADDRESS');
        }
        
        /** Status = NoMatches engine was unable to find a matched address 
        *   No results returned. 
        **/
   		else if  (status == 'NoMatches'){
              return new Map<String, String> {'ERROR' => 'No Matches'};
        } 
        else {
            return new Map<String, String> {'ERROR' => 'Unrecognised Status' };
        } 
        
        if (!addrElements.isEmpty()) {
            addrElements.put('VerificationLevel', VerificationLevel );
            addrElements.put('Status', status );
        }
        return addrElements;           
    }
     
    
	/**********************************************************************************************
    * @description  function to parse the Picklist results and take the first suggested address 
    * 				and corresponding moniker to obtain Address Details
    *               Use QAS getAddress method to obtain Address Details
    *               
    * @param        String searchAddress - the street address to search
    * @param        String country       - the country the address belongs to      
    * @return       String address        - String XML representing address         
	**/
	public static String getPicklistBestAddress(String response, String countryCode) {
            String moniker = SegmentationQASServiceSOAP.getSearchResultsBestMoniker(response) ;   
            if ( String.isBlank(moniker) ) {
                return 'ERROR: Unsuccessful PickList Parsing'; }   
            String layout   = getQASLayoutCode(countryCode);
            String address  = SegmentationQASServiceSOAP.getAddressQAS(moniker, layout);
        return address;
 	}              
                    
                    
    /**********************************************************************************************
    * @description  function to execute Experian QAS Search service
    *               - SOAP method DoSearch
    *               - Engine Type = Verification
    *               Method returns Extended Address Elements if Address is Verified.
    *               Steps:
    *               Step 1. Call to generate HTTP Body for the service
    *               Step 2. Calls generic callout function which 
    *                  a. generates HTTP Headers based on passed configuration
    *                  b. executes HTTP Request callout
    *                  c. returns HTTP call response body
    * @param        String searchAddress - the street address to search
    * @param        String country       - the country the address belongs to
    *
    * @return       String representing XML response
    * @example      SegmentationQASServiceSOAP.doSearchVerificationQAS( '119 Meadowland Dr, Auckland' , 'NZ' );
    ***********************************************************************************************/
    public static String doSearchVerificationQAS(String searchAddress, String countryCode) {
        String body       = getDoSearchVerificationSOAPBody( searchAddress, countryCode);
        String SOAPAction = CONFIG_QAS_DOSEARCH_SOAP_ACTION;
        String response   = CalloutSOAPService.executeCallout('QAS_SOAP_Address_Service', SOAPAction, body);
        return response;           
    }
    
    /************************************************************************************************
    * @description  function to generate QAS SOAP XML body for the method: DoSearch -> Engine Verification
    * 
    * Verification search requires complete address, as it would appear on an envelope. 
    * The entire address is submitted to the engine, and a verification level is returned. 
    * The verification level corresponds to the degree of confidence in the returned address. 
    * If an accurate match could not be found, a different engine could be used to select address.
    * A treshold = 5 established for picklist returns (first 5 results)
    * 
    * @param        String searchAddress - the street address to search for
    * @param        String country       - country the address belongs to
    *
    * @return       String body
    ***********************************************************************************************/
@TestVisible private static String getDoSearchVerificationSOAPBody(String searchAddress, String countryCode) {
        String country  = getQASCountryCode(countryCode);
        String layout   = getQASLayoutCode(countryCode);
    	String srchAddress = searchAddress.replaceAll('&|<|>|"|\'|\\\\', ' '); // R01
		String soapBody =
                 '<?xml version="1.0" encoding="UTF-8"?> '
                 + '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" > ' 
                 + '<soapenv:Body> '
                 +    '<ond:QASearch xmlns:ond="http://www.qas.com/OnDemand-2011-03"> '
                 +       '<ond:Engine Threshold="5" >Verification</ond:Engine> '
                 +       '<ond:Country>' + country + '</ond:Country> '
                 +       '<ond:Layout>'  + layout  + '</ond:Layout> '
                 +       '<ond:Search>'  + srchAddress + '</ond:Search> '
                 +    '</ond:QASearch> '
                 + '</soapenv:Body> '
                 + '</soapenv:Envelope> ';
        return soapBody;           
    }
    
    /**********************************************************************************************
    * @description  function to execute Experian QAS Search service
    *               - SOAP method DoSearch
    *               - Engine Type = Singleline
    *               Method used if the verification call did not succeed
    *               Performs more loose matching techniques and suggests result
    *               returns a picklist if address is matched in some way
    *
    * @param        String searchAddress - the street address to search
    * @param        String country       - the country the address belongs to
    *
    * @return       String
    * @example      SegmentationQASServiceSOAP.doSearchSinglelineQAS( '119 Meadowland Dr, Auckland' , 'NZ' );
    ***********************************************************************************************/
    public static String doSearchSinglelineQAS(String searchAddress, String countryCode) {
        String body = getDoSearchSinglelineSOAPBody( searchAddress, countryCode);
        String SOAPAction = CONFIG_QAS_DOSEARCH_SOAP_ACTION;    
        String response = CalloutSOAPService.executeCallout('QAS_SOAP_Address_Service', SOAPAction, body);
        return response;           
    }
    
    /**********************************************************************************************
    * @description  function to generate QAS SOAP XML body for method: DoSearch Engine Singleline
    * 
    * SingleLine searching requires an entry of two or three address elements, separated by a comma, in the order, they would appear on an envelope
    * SingleLine searches use a variety of techniques to return the correct address from incomplete or misspelled information.
    * Added Increased Timeout for complex matching that require more time
    * 
    * @param        String searchAddress - the street address to search for
    * @param        String country       - country the address belongs to
    *
    * @return       String body
    **********************************************************************************************/
	@TestVisible private static String getDoSearchSinglelineSOAPBody(String searchAddress, String countryCode) {
        String country  = getQASCountryCode(countryCode);
        String layout   = getQASLayoutCode(countryCode);
        String srchAddress = searchAddress.replaceAll('&|<|>|"|\'|\\\\', ' '); // R01
		String soapBody =
                 '<?xml version="1.0" encoding="UTF-8"?> '
                 + '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" > ' 
                 + '<soapenv:Body> '
                 +    '<ond:QASearch xmlns:ond="http://www.qas.com/OnDemand-2011-03"> '
                 +       '<ond:Engine Threshold="5" Timeout="50000" >Singleline</ond:Engine> '
                 +       '<ond:Country>' + country + '</ond:Country> '
                 +       '<ond:Layout>'  + layout  + '</ond:Layout> '
                 +       '<ond:Search>'  + srchAddress + '</ond:Search> '
                 +    '</ond:QASearch> '
                 + '</soapenv:Body> '
                 + '</soapenv:Envelope> ';
        return soapBody;           
    }
   
    /**********************************************************************************************
    * @description  function to return Address Elements
    *               SOAP method DoGetAddress
    * 				First call to search address and return moniker
    * @param        String moniker - the street address ID Experian
    * @param        String layout  - the name of the layout to return details
    *
    * @return       String
    * @example      SegmentationQASServiceSOAP.getAddressQAS( '' );
    **********************************************************************************************/
    public static String getAddressQAS(String moniker, String layout) {
        String body = getAddressSOAPBody( moniker, layout);
        String SOAPAction = CONFIG_QAS_GETADDRESS_SOAP_ACTION;
       
        String response = CalloutSOAPService.executeCallout('QAS_SOAP_Address_Service', SOAPAction, body);
        if (debugFlag) System.debug('getAddressQAS: RESPONSE ' + response);
        return response;           
    }
    
    /************************************************************************************************
    * @description  function to generate QAS SOAP XML body for method: getAddress
    * @param        String moniker - the id of the address returned from the DoSearch
    *
    * @return       String body
    ***********************************************************************************************/
    private static String getAddressSOAPBody(String moniker, String layout) {
		String soapBody =
                   '<?xml version="1.0" encoding="UTF-8"?> '
                 + '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ond="http://www.qas.com/OnDemand-2011-03"> ' 
                 + '<soapenv:Body> '
                 +    '<ond:QAGetAddress> '
                 +      '<ond:Layout>' + layout  + '</ond:Layout>  '
                 +      '<ond:Moniker>' + moniker + '</ond:Moniker> '
                 +    '</ond:QAGetAddress> '
                 + '</soapenv:Body> '
                 + '</soapenv:Envelope> \n';
        return soapBody;           
    }
     

     /**
    * @description  Function returns Status of the DoSearch search 
    *               takes XPath to find the QAInformation Node and determines StateTransition value
    *               Values: NoMatches, TooManyMatches, SearchResults, Picklist
    * @param        String XML Response from HTTP Request
    *
    * @return       String Status (State Transition)
    * @example      SegmentationQASServiceSOAP.getDoSearchVerificationStatus(String response); 
    */
    public static String getDoSearchVerificationStatus (String responseXML) {
        Dom.XmlNode QAInformation = findNode(responseXML, CONFIG_QAS_DOSEARCH_STATE_TRANS_PATH);
        String stateTransition;
        if ( QAInformation==null ) {
            System.debug('Error: Seg:getDoSearchVerificationStatus: Node not found');
            return null;}   
		
        try {
        	stateTransition  = QAInformation.getChildElement('StateTransition', CONFIG_QAS_DEFAULT_NAMESPACE ).getText();
        } catch(Exception ex) {
            System.debug('Error: Seg:getDoSearchVerificationStatus: exception ' + ex);
            return null;
        }
        if (debugFlag) System.debug('getDoSearchVerificationStatus: ' + stateTransition);
        return stateTransition;
	}
    

    /**
    * @description  calls findNode to get the QASearchResult Node and determine Verification Level
    *               VerifyLevel: Verified,VerifiedStreet,VerifiedPlace
    *               Lower Confidence: StreetPartial, PremisesPartial or Multiple, InteractionRequired
    *               None
    * @param        String XML Response from HTTP Request
    *
    * @return       DOM.XMLNode
    * @example      SegmentationQASServiceSOAP. getDoSearchVerificationLevel(String response); 
    */
    public static String getDoSearchVerificationLevel(String responseXML) {
        String verifyLevel;
        String attributeKey = 'VerifyLevel';
        String attributeNS  = null;
        
        Dom.XmlNode QASearchResult = findNode(responseXML, CONFIG_QAS_DOSEARCH_VERIFY_LEVEL_PATH);
        if (QASearchResult == null) {
            System.debug('Error: getDoSearchVerificationLevel: Node not found');
            return null;
        }
        try {
            verifyLevel = QASearchResult.getAttributeValue(attributeKey, attributeNS);
        } catch(Exception ex) {
            System.debug('Error: Seg:getDoSearchVerificationLevel: exception ' + ex);
            return null;
        }
        if (debugFlag) System.debug('getDoSearchVerificationLevel: ' + verifyLevel);
        if (verifyLevel==null)
            return null;
        
        return verifyLevel;
	}
    
    /**
    * @description  function checks total of matched results
    *               Only first results as per call Threshold are returned
    *               The variable could be used to estimate accuracy
    *               It also works as an indicator if any result is returned
    * @param        String XML Response from HTTP Request
    *
    * @return       Integer 
    * @example      SegmentationQASServiceSOAP.getPickListTotal(xml));
    */
    public static Integer getPickListTotal(String xml) {
        String  stringTotal;
        Integer integerTotal;
        Dom.XmlNode QAPicklistFirstEntry = findNode(xml, CONFIG_QAS_DOSEARCH_QAPICKLIST_PATH);   
        if (QAPicklistFirstEntry == null) {
            if (debugFlag) System.debug('Error: getPickListTotal: findNode empty');
            return null;
        }
        try {
            stringTotal  = QAPicklistFirstEntry.getChildElement('Total'  , CONFIG_QAS_DEFAULT_NAMESPACE ).getText();
            integerTotal = Integer.valueOf(stringTotal);
        } catch(Exception ex) {
          	System.debug('Error: Seg:getPickListTotal: exception ' + ex);
            return 0;
        }
		if (debugFlag) System.debug('getPickListTotal:' + integerTotal);
        return integerTotal;
	}
    
    
    /**
    * @description  function calls findNode and returns Moniker from the first pickListEntry
    *               First Pick List Entry should be closest to the searched address
    *               Could be first address on the street when Verification Level = "StreetPartial"
    * @param        String XML Response from HTTP Request
    *
    * @return       DOM.XMLNode
    * @example      SegmentationQASServiceSOAP.getDoSearchEntryMoniker(xml));
    *               /soap:Envelope/soap:Body/QASearchResult/QAPicklist/PicklistEntry';
    */
     @TestVisible private static String getSearchResultsBestMoniker(String xml) {
        String moniker;
        String postcode;
        // get the First PickListEntry XML ELEMENT from response
        Dom.XmlNode QAPicklistFirstEntry = findNode(xml, CONFIG_QAS_DOSEARCH_PICKLIST_ENTRY_PATH);   
        if (QAPicklistFirstEntry == null) {
            if (debugFlag) System.debug('Error: getSearchResultsBestMoniker: findNode empty');
            return null;
        }
        try {
            moniker  = QAPicklistFirstEntry.getChildElement('Moniker'  , CONFIG_QAS_DEFAULT_NAMESPACE ).getText();
            postcode = QAPicklistFirstEntry.getChildElement('Postcode' , CONFIG_QAS_DEFAULT_NAMESPACE ).getText();
        } catch(Exception ex) {
          	System.debug('Error: Seg:getSearchResultsBestMoniker: exception ' + ex);
        }
		if (debugFlag) System.debug('getSearchResultsBestMoniker:' + moniker);
        return moniker;
	}
    
    
    /**
    * @description  uses XPath to find the Node in the XML file             
    * @param        String XML Response from HTTP Request
    * @param        String Path to the Node
    *
    * @return       DOM.XMLNode
    * @example      Dom.XmlNode element = xp.findFirst(xp.root, '/soap:Envelope/soap:Body/QASearchResult/QAPicklist');
    */
    public static DOM.XMLNode findNode(String xml, String path) {
        XPath xp = new XPath(xml);
		DOM.XMLNode QAPicklist = xp.findFirst(xp.root, path);
        return QAPicklist;
	}
    
    
   /**
    * @description  getNodeChildValue - safely get value of the child
    *               removes dependencies on namespace, looks at direct children only          
    * @param        DOM.XMLNode Node
    * @param        String Child Name
    *
    * @return       String Child Value (Text)
    */ 
     @TestVisible private static String getNodeChildValue(DOM.XMLNode xmlnode, String childName)
    {
        for(Dom.XMLNode child : xmlnode.getChildElements())
        {
          if(child.getName()== childName){
          	return child.getText();}       
        }
        return null;
    }  
    
    
    
    /************************************************************************************************
    * @description  calls findNode and returns the Node with address elements
    *               traverse through the Node and generate Map
    *               Generates index for elements with the same name - Address Line
    *               AddressLine1, AddressLine2, ...
    *               AddressLines with LineContent = "None" represent a standard set of Addresses
    *               AddressLines with LineContent = "DataPlus" represent additional data 
    *               
    *               get Addresses Node structure depends on the source of information - different structures
    *               SearchResutls and GetAddress
	*
    *               Current Data Plus set covers 
    *               MeshblockID
    *               MeshblockCode
    * 
    * @param        String XML 
    *
    * @return       Map <String Address Element Name, String Address Elelemt value> 
    * @example      SegmentationQASServiceSOAP.parseAddressElements(xml);
    ***********************************************************************************************/
    //@TestVisible private static  Map<String, String> parseAddressElements(String xml, String source  ) {
    public static  Map<String, String> parseAddressElements(String xml, String source  ) {
        Dom.XmlNode QAAddress;
        Map<String, String> addressElements = new  Map<String, String> ();
        String name, value, label, lineContent;
        Integer attributeCount;
        
        if ( source == 'VERIFIED') {
            QAAddress = findNode(xml, CONFIG_QAS_DOSEARCH_VERIFY_RESULTS_PATH);
        }
        else if ( source == 'GETADDRESS') {
            QAAddress = findNode(xml, CONFIG_QAS_GETADDRESS_RESULTS_PATH);
        }
        
        if (QAAddress == null) {
            System.debug('Error: parseAddressElements: Node not found');
            return addressElements;
        }
		if (debugFlag) System.debug('ParseAddressElements:' + QAAddress);
        

        
        Integer counter = 0;
        for(Dom.XmlNode addrElem: QAAddress.getChildElements()) {
            name  = addrElem.getName();

            // generate keys (names) for the Map with address Elements
            // For standard fields add two Map keys: AddressLine_Index + Address Element
            // E.g. Address2 Dunedin Central, Suburb Dunedin Central 
            
            if ( name == 'AddressLine') {
                attributeCount = addrElem.getAttributeCount();
                lineContent    = addrElem.getAttributeValue('LineContent', null);
                
                // LineContent = "None" or Empty for standard Address Line Elements
                //  add index for elements with the same name
                
                if (lineContent == 'None' || lineContent==null) {
                    counter += 1;
                	name = name + counter;
					
                    // Apart from AddressLine add also Address element, could be usefull in future          
                    label = getNodeChildValue(addrElem, 'Label');
                    value = getNodeChildValue(addrElem, 'Line');
                    // First element is empty and represent Street Address
                    if (String.isEmpty(label)){
                        label = 'Street';}
                    // check for duplicate keys
					if(!addressElements.containsKey(label)){
						addressElements.put(label, value);
                        if (debugFlag) System.debug('Address Element: ' + label + ' : ' + value );
                    }

                } else if (lineContent == 'DataPlus') {
           			label = getNodeChildValue(addrElem, 'Label');
                	//label = addrElem.getChildElement('Label', CONFIG_QAS_DEFAULT_NAMESPACE ).getText().trim();
                    
                    if(label.contains('Mesh Block ID')){
                        name = 'MeshBlockID';}
                    if(label.contains('Mesh Block Code')){
                        name = 'MeshBlockCode';}
                    // AU only
                    if(label.contains('Latitude')){
                        name = 'Latitude';}
                     if(label.contains('Longitude')){
                        name = 'Longitude';}
                    
                    // NZ Only
                    if(label.contains('DPID')){
                        name = 'DPID';}
                    
                    if(label.contains('Regional Council Code')){
                        name = 'Regional Council Code';}
                    if(label.contains('Regional Council Name')){
                        name = 'Regional Council Name';}
                    
					if(label.contains('Territorial Authority Code')){
                        name = 'Territorial Authority Code';}                      
					if(label.contains('Territorial Authority Name')){
                        name = 'Territorial Authority Name';}                    
                    
                    if(label.contains('X Coordinate')){
                        name = 'X Coordinate';}                      
					if(label.contains('Y Coordinate')){
                        name = 'Y Coordinate';}     
                    
                    
                    // if name still 'AddressLine' - should not be anymore
                     if ( name == 'AddressLine') {
                         System.debug('Error: parseAddressElements: Unrecognised Label: ' + label);}
                } else {
                    System.debug('Error: parseAddressElements: Unrecognised LineContent: ' + lineContent);
                    return addressElements;
                }                 
                // get value from the Line Child for returned Map
                value = getNodeChildValue(addrElem, 'Line');
            }     
            if(!addressElements.containsKey(name)){
                addressElements.put(name, value);}
			if (debugFlag) System.debug('Address Element: ' + name + ' : ' + value );
        }
        return addressElements;
	}
    
    
    // Convert Salesforce Country codes into Experian QAS
    @TestVisible private static String getQASCountryCode(String countryCode) {
        if (countryCode.equals('AU')) {
            return 'AUG';
        } else if (countryCode.equals('NZ')) {
            //return 'NZL';
            return 'NZD';
        } else {
            return countryCode;
        }
    }
    
    @TestVisible private static String getQASLayoutCode(String countryCode) {
        if (countryCode.equals('AU')) {
            return 'CAANZAUG';
            //return 'AUGMeshBlock'
        } else if (countryCode.equals('NZ')) {
            return 'CAANZNZD';
        } else {
            return null;
        }
    }
   
    
    /**
     *          select 
                ServiceName__c, Level__c, Class__c, Method__c, SalesforceId__c, Message__c, CreatedBy
                from WebServiceLog__c
				SELECT CreatedDate, ServiceName__c, Level__c, Class__c, Method__c, SalesforceId__c, Message__c FROM WebServiceLog__c
     * 
     **/
    public static void logWSMessage(String serviceName, String level, String className, String methodName, String sfId, String message){              
        String safeMessage;

        if (message.length()>255){
			safeMessage = message.substring(0,254);}
        else{
            safeMessage = message;}
        
        WebServiceLog__c newLogMsg =
            new WebServiceLog__c(
                ServiceName__c = serviceName,
                Level__c = level,
                Class__c = className,
                Method__c = methodName,
                SalesforceId__c = sfId,
                Message__c = safeMessage);
        try{
            INSERT newLogMsg;
        }
        catch (Exception ex){
            System.debug('ERROR: SegmentationQASServiceSOAP: Failed to INSERT the web service log: Error: ' + ex.getMessage());
        }
    }
}