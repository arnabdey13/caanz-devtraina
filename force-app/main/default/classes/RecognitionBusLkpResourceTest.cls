@isTest
public class RecognitionBusLkpResourceTest {


	// set of lookups on matching noise
	public static Map<String, String> lkpBusWords;
	public static Map<String, String> lkpLocalityNoise;
	public static Map<String, String> lkpCommonNoise;
	public static Map<String, String> lkpTrustee;
	public static Map<String, String> lkpNumWords;
	public static Map<String, String> lkpLegalStd;
    
    public static Map<String, String> lkpCities;
	public static Map<String, String> lkpFirstnames;
    public static Map<String, String> lkpPracticeNoise;
    public static Map<String, String> lkpImportantPractice;
       
	/**
	*  load basic Lookups
	*/
	public static void loadBusNameLookups()
	{
		System.debug('Loading Bus Name Lookups');
        
		// Locality Noise Lookup
		if (lkpLocalityNoise==null)
	  		lkpLocalityNoise = RecognitionBusLkpResource.LocalityNoise;

		// Common Noise Lookup
		if (lkpCommonNoise==null)
	  		lkpCommonNoise = RecognitionBusLkpResource.CommonNoise;

		// Standard legal words Lookup
		if (lkpLegalStd==null)
	  		lkpLegalStd = RecognitionBusLkpResource.legalWords;
	  
		// Trustee Basic
		if (lkpTrustee==null)
	    	lkpTrustee = RecognitionBusLkpResource.Trustee;
        
        // Cities
		if (lkpCities==null)
	    	lkpCities = RecognitionBusLkpResource.Cities;
        
        // Frequent Firstnames in Company Names (First word only)
		if (lkpFirstnames==null)
	    	lkpFirstnames = RecognitionBusLkpResource.Firstnames;

        // Frequent Practice Noise in Company Names (First word only)
		if (lkpPracticeNoise==null)
	    	lkpPracticeNoise = RecognitionBusLkpResource.PracticeNoise;        

        // Important Practice
		if (lkpImportantPractice==null)
	    	lkpImportantPractice = RecognitionBusLkpResource.ImportantPractice;         
                
	}
    
	private static List<String> delimitedStringToList(String words) { 
		if (String.isEmpty(words)) 
			return new List<String>(); 

        List<String> ptrns = words.normalizeSpace().split(' ');
        
		List<String> cleanPatterns = new List<String>();
		for(String pattern : ptrns) {
			cleanPatterns.add(pattern.trim());
		}
		return cleanPatterns;
	}
    
    public static String getStdLegal(String str)
	{
		if (lkpLegalStd.containsKey(str))
			return lkpLegalStd.get(str);
        else
            return str;     
	}

    
	public static String getTrusteePattern(String str)
	{
		if (lkpTrustee.containsKey(str))
			return lkpTrustee.get(str);
        else
            return str;      
	}
	
	public static String rmLocalityNoise(String str)
	{
        String clean;
        List <String> strTokens = delimitedStringToList(str);
        for (String token : strTokens)
            if (!lkpLocalityNoise.containsKey(token))
				clean = (clean==null)? token : clean + ' ' + token;
        return clean;
	}

	public static String getStdLocality(String str)
	{
		if (lkpLocalityNoise.containsKey(str))
			return lkpLocalityNoise.get(str);
        else
            return str;
	}
    
	public static String replaceLocalityNoise(String str)
	{
        String clean;     
        List <String> strTokens = delimitedStringToList(str);
        for (String token : strTokens)
			if (!lkpLocalityNoise.containsKey(token))
				clean = (clean==null)? token : clean + ' ' + token;
        	else
				clean = (clean==null)? token : clean + ' ' + lkpLocalityNoise.get(token);               
        return clean;
	}


    public static String stdFirstPracticeNoise(String str){
        String clean;     
        List <String> strTokens = delimitedStringToList(str);
        Integer counter=0;
        for (String token : strTokens) {
			if (lkpPracticeNoise.containsKey(token) && counter == 0)
					clean = lkpPracticeNoise.get(token);
            else
				clean = (clean==null)? token : clean + ' ' + token;
            
            counter += 1;
		}    
        return clean;		
    }        
    
    
    public static String stdFirstFirstname(String str){
        String clean;
		if (lkpFirstnames==null)
           loadBusNameLookups();        
        List <String> strTokens = delimitedStringToList(str);
        Integer counter=0;
        for (String token : strTokens) {
			if (lkpFirstnames.containsKey(token) && counter == 0)
					clean = lkpFirstnames.get(token);
            else
				clean = (clean==null)? token : clean + ' ' + token;
            
            counter += 1;
		}    
        return clean;		
    }        
    
    public static String stdImportantPractice(String str){     
        List <String> strTokens = delimitedStringToList(str);
		if (!lkpImportantPractice.containsKey(strTokens[0]))
                return str;
        String ImportantPractice = lkpImportantPractice.get(strTokens[0]);
        if (str.startsWith(ImportantPractice))
            return ImportantPractice;
        else
            return str;
    }        

	public static String replaceLegalNoise(String str)
	{
        String clean;
        List <String> strTokens = delimitedStringToList(str);
        for (String token : strTokens)
            if (!lkpLegalStd.containsKey(token))
            	clean = (clean==null)? token : clean + ' ' + token;
        	else
				clean = (clean==null)? token : clean + ' ' + lkpLegalStd.get(token);         
        return clean;
	}
    
	public static String rmLegalNoise(String str)
	{
        String clean;  
        List <String> strTokens = delimitedStringToList(str);
        for (String token : strTokens)
            if (!lkpLegalStd.containsKey(token))
            	clean = (clean==null)? token : clean + ' ' + token;         
        return clean;
	}
    
	public static String rmTrusteeNoise(String str)
	{
        String clean = '';  
        clean = str.normalizeSpace();
		Set<String> tokens = lkpTrustee.keySet();
        for (String token: tokens){
            if (clean.contains(token))
                clean = clean.remove(token);
        }
        return clean;
	}      
        
	public static String rmCity(String str)
	{
        String clean = '';  
        clean = str.normalizeSpace();
		Set<String> cities = lkpCities.keySet();
        for (String city: cities){
            if (clean.contains(city)) 
                clean = clean.remove(city).normalizeSpace();
        }
        return clean;
	} 
    
	public static String stdCity(String city)
	{
        String cleanCity = city.toUpperCase();
        if (lkpCities.containsKey(cleanCity)) 
			return lkpCities.get(cleanCity);
        return cleanCity;
	} 
    

	public static String rmCommonNoise(String str)
	{
		String clean;
		List <String> strTokens = delimitedStringToList(str);
        for (String token : strTokens)
            if (!lkpCommonNoise.containsKey(token))
				clean = (clean==null)? token : clean + ' ' + token;
		return clean;
	}

	public static String rmBusWords(String str)
	{
		String clean;
		List <String> strTokens = delimitedStringToList(str);
        for (String token : strTokens)
            if (!lkpBusWords.containsKey(token))
				clean = (clean==null)? token : clean + ' ' + token;
		return clean;
	}

	public static String rmAllNoise(String str)
	{
		String clean = '';
		clean = str.normalizeSpace();
        clean = rmTrusteeNoise(clean);
        clean = rmCommonNoise(clean);
        clean = rmLegalNoise(clean);
        clean = rmLocalityNoise(clean);
		return clean;
	}
    
/////////////////////////////////////////////////////////////////  TEST  //////////////////////////////////////////////////////////
	private static testMethod void testBusLookup(){
        loadBusNameLookups();
        String locality = getStdLocality('AUSTRALIAN CAPITAL TERRITORY');        
        System.debug('Locality Standardisation');
        System.debug(locality);
        System.assertEquals('ACT', locality);
  
		String cleanNoise = rmLegalNoise('ABC COMPANY P./L AU');
        System.debug('Common Noise Removal');
        System.debug('@' + cleanNoise + '@');
        System.assertEquals('ABC COMPANY AU', cleanNoise);
 
        String legal = getStdLegal( 'LIMITED');
        System.debug('Legal Standardisation');
        System.assertEquals('LTD', legal);

        String cleanLocality = rmLocalityNoise('XYZ COMPANY P./L AU');
        System.debug('Locality Noise Removal');
        System.debug('@' + cleanLocality + '@');
        System.assertEquals('XYZ COMPANY P./L', cleanLocality);
        
		String cleanName = rmLegalNoise('ABC COMPANY P./L AU');
        cleanName = rmLocalityNoise(cleanName);
        System.debug('Noise Removal');
        System.debug(cleanName);
        System.assertEquals('ABC COMPANY', cleanName);   
        
		cleanName = rmLegalNoise('ABC  COMPANY   P./L   AU');
        cleanName = rmLocalityNoise(cleanName);
        System.debug('Noise Removal - ABC  COMPANY   P./L   AU');
        System.debug(cleanName);
        System.assertEquals('ABC COMPANY', cleanName);   
        
        String cleanTrustee = rmTrusteeNoise('ABC COMPANY AS TRUSTEE OF THE SMITH FAMILY AU');
        cleanTrustee = rmLocalityNoise(cleanTrustee);
        System.debug('Noise Trustee Removal - ABC COMPANY AS TRUSTEE OF THE SMITH FAMILY AU');
        System.debug(cleanTrustee);
        System.assertEquals('ABC COMPANY SMITH FAMILY', cleanTrustee);  
        
        //String cleanLegal = rmTrusteeNoise('ABC COMPANY AS TRUSTEE OF THE SMITH FAMILY P./L    INCORPORATED AU');
        String cleanLegal = rmTrusteeNoise('ABC COMPANY AS TRUSTEE OF THE SMITH FAMILY AU');
        System.debug('Noise Trustee & Legal Removal - ABC COMPANY AS TRUSTEE OF THE SMITH FAMILY INCORPORATED AU'); 
        cleanLegal = rmCommonNoise(cleanLegal);
        cleanLegal = rmLegalNoise(cleanLegal);
        cleanLegal = rmLocalityNoise(cleanLegal);
        System.debug(cleanLegal);
        System.assertEquals('ABC COMPANY SMITH FAMILY', cleanTrustee);  
        
        String cleanAll = rmAllNoise('ABC COMPANY AS TRUSTEE OF THE SMITH FAMILY P./L    INCORPORATED AU');
		System.debug(cleanAll);
		System.assertEquals('ABC COMPANY SMITH FAMILY', cleanAll);  
    }
}