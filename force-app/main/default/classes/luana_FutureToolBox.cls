global class luana_FutureToolBox {
    
    // Future Actions //
    //================//
    @future
    public static void setSPFuture(Set<Id> welcomeEmailToBeSetFalse) {
        
        List<LuanaSMS__Student_Program__c> spToSet = [SELECT Id, Welcome_Email_Sent__c FROM LuanaSMS__Student_Program__c WHERE Id IN :welcomeEmailToBeSetFalse];                
        
        for (LuanaSMS__Student_Program__c sp : spToSet) {
            if (welcomeEmailToBeSetFalse.contains(sp.Id)) sp.Welcome_Email_Sent__c = false;
        }
        update spToSet;
    }
}