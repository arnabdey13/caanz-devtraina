@isTest(seeAllData=false)
public class SegmentationRefreshTest {

    ////////////////////// Main Practice Lookup Prep ///////////////////////////////////////////////////////////////
    @testSetup 
    static void setup() {
        
        Reference_Main_Practices__c  deloitte = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Deloitte'
            , Search_Name__c     = 'Deloitte'
            , Full_Name__c       = 'Deloitte'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            = 'Deloitte'
        );
      
		Reference_Main_Practices__c  pwc = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'PricewaterhouseCoopers;PWC; PwC; PriceWaterHouseCoopers; PriceWaterhouseCoopers; PricewaterhousCoopers; Pricewaterhousecoopers'
            , Search_Name__c     = 'PricewaterhouseCoopers'
            , Full_Name__c       = 'PricewaterhouseCoopers'
            , Practice_Size__c   = 'Large Firm'
			, Big4__c            = 'PWC'
        );
        
		Reference_Main_Practices__c  kpmg = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'KPMG'
            , Search_Name__c     = 'KPMG'
            , Full_Name__c       = 'KPMG'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            = 'KPMG'
        );
        
		Reference_Main_Practices__c  ey = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Ernst and Young; EY; Ernst & Young'
            , Search_Name__c     = 'Ernst and Young'
            , Full_Name__c       = 'Ernst and Young'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            = 'EY'
        );
        
		Reference_Main_Practices__c  bdo = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'BDO'
            , Search_Name__c     = 'BDO'
            , Full_Name__c       = 'BDO'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            =  NULL
        );
        
		Reference_Main_Practices__c  gt = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Grant Thornton'
            , Search_Name__c     = 'Grant Thornton'
            , Full_Name__c       = 'Grant Thornton'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            =  NULL
        );        
        
        
		Reference_Main_Practices__c  vincents = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Vincents Chartered Accountants; Vincents'
            , Search_Name__c     = 'Vincents Chartered Accountants'
            , Full_Name__c       = 'Vincents Chartered Accountants'
            , Practice_Size__c   = 'Large Firm'
            , Big4__c            =  NULL
        );
        
        Reference_Main_Practices__c  boroughs = new Reference_Main_Practices__c (
             Matching_Tokens__c  = 'Boroughs'
            , Search_Name__c     = 'Boroughs'
            , Full_Name__c       = 'Boroughs'
            , Practice_Size__c   = 'Medium Firm'
            , Big4__c            =  NULL
        );
        
        insert new List<Reference_Main_Practices__c>{pwc, deloitte, kpmg, ey, bdo, gt, vincents, boroughs};  
            
        // Preload some members New Zealand    
        // Account JuniorMemberAccountNZ = SegmentationDataGenTest.genFullMemberNewZealand('Smith', 'Senior Accountant', 'smith1@test.co.nz', Date.valueOf('1987-05-12') );
        // insert JuniorMemberAccountNZ;      
		// Account big4BusinessAccountNZ = SegmentationDataGenTest.genPracticeNewZealand( 'Ernst & Young', 'Chartered Accounting', 'Auckland', 25);
		// insert big4BusinessAccountNZ;
		// Account smallBusinessAccount = SegmentationDataGenTest.genPracticeNewZealand( 'Bob Accounting', 'Chartered Accounting', 'Wellington', 1);
		// insert smallBusinessAccount;
 
 		Account nonMemberAccount = SegmentationDataGenTest.genNonMemberAustralia('Clown', 'Senior Accountant', 'clown1@test.co.nz', Date.valueOf('1987-05-12') );
		insert nonMemberAccount;
        
        // Preload some members AU          
       // Account provMemberAccountAU = SegmentationDataGenTest.genProvMemberAustralia('Lee', 'Accountant', 'lee@test.co.au', Date.valueOf('1991-05-12') );
        //insert provMemberAccountAU;
        Account SeniorMemberAccountAU = SegmentationDataGenTest.genFullMemberAustralia('Turner', 'Tax Manager', 'brown1@test.co.au', Date.valueOf('1977-05-12') );
        insert SeniorMemberAccountAU;   
        Account big4BusinessAccountAU = SegmentationDataGenTest.genPracticeAU( 'Deloitte', 'Chartered Accounting', 'Sydney', 25);
		insert big4BusinessAccountAU;
        Account smallBusinessAccountAU = SegmentationDataGenTest.genPracticeAU( 'Young and Co', 'Chartered Accounting', 'Brisbane', 2);
		insert smallBusinessAccountAU;
           
        Account overseasMemberAccount = SegmentationDataGenTest.genFullMemberOverseas('Kwong', 'Accountant', 'kwong@test.co.hk', Date.valueOf('1981-05-12') );
        insert overseasMemberAccount;
        
        Employment_History__c histEmpSeniorMemberAU = SegmentationDataGenTest.genHistoricalEmploymentHistory(SeniorMemberAccountAU.Id, big4BusinessAccountAU.Id, 'Tax Manager', 'Manager');  
		insert histEmpSeniorMemberAU;  

		//Employment_History__c currEmpProvMemberAU = SegmentationDataGenTest.genCurrentEmploymentHistory(provMemberAccountAU.Id, smallBusinessAccountAU.Id, 'Accountant', 'Accountant');  
		//insert currEmpProvMemberAU;

		Segmentation__c nonMemberAccountSeg = new Segmentation__c();
        nonMemberAccountSeg.Account__c = nonMemberAccount.Id;
        nonMemberAccountSeg.Organisation_Type__c = 'Not For Profit';
        nonMemberAccountSeg.Business_Segmentation_MS__c = 'Public Team Player';
		insert nonMemberAccountSeg;
        
    }

	// Test 
	private static testMethod void runBatchUpdate_1(){
		System.debug( 'Starting Batch');
        
		List<Segmentation__c> segBefore = [	SELECT Id, Account__c, Account__r.Name, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c 
											FROM Segmentation__c ];
		System.debug('Batch 1: Segments Before:' + segBefore);         
        Test.startTest();           
        SegmentationRefresh objClass = new SegmentationRefresh();
		Database.executeBatch (objClass, 10);
        
		Test.stopTest();
		List<Segmentation__c> segAfter = [	SELECT Id, Account__c, Account__r.Name, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c 
											FROM Segmentation__c ];
		System.debug('Batch 1: Segments After:' + segAfter);
        for (Segmentation__c seg : segAfter) {
			System.debug(seg);
			System.debug(seg.Account__r.Name);
            if (seg.Account__r.Name == 'Robert Turner')
                System.assertEquals('Member No Employment', seg.Business_Segmentation_MS__c) ;          
        }
        System.assert(segAfter.size()==3);
    }
    
    
    	// Test 
	private static testMethod void runBatchUpdate_2(){
		System.debug( 'Starting Batch');
        
		List<Segmentation__c> segBefore = [	SELECT Id, Account__c, Account__r.Name, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c 
											FROM Segmentation__c ];
		System.debug('Batch 2: Segments Before:' + segBefore);        

        Test.startTest();     
        String whereQry = ' Where Id in (Select Account__c from Segmentation__c) ';
        SegmentationRefresh objClass = new SegmentationRefresh(whereQry, TRUE);
		Database.executeBatch (objClass, 20);
        
		Test.stopTest();
		List<Segmentation__c> segAfter = [	SELECT Id, Account__c, Account__r.Name, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c 
											FROM Segmentation__c ];
		System.debug('Batch 2: Segments After:' + segAfter);
        for (Segmentation__c seg : segAfter) {
			System.debug(seg);
			System.debug(seg.Account__r.Name);
            
            if (seg.Account__r.Name == 'Robert Clown'){
				System.assertEquals(NULL, seg.Business_Segmentation_MS__c); 
            }
        }
        System.assert(segAfter.size()==1);
    }
    
    
	// Test 
	private static testMethod void runBatchUpdate_3(){
		System.debug( 'Starting Batch');
        
		List<Segmentation__c> segBefore = [	SELECT Id, Account__c, Account__r.Name, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c 
											FROM Segmentation__c ];
		System.debug('Batch 3: Segments Before:' + segBefore);        

        Test.startTest();     
        String whereQry = ' Where Id in (Select Account__c from Segmentation__c) ';
        SegmentationRefresh objClass = new SegmentationRefresh(whereQry);
		Database.executeBatch (objClass, 20);
        
		Test.stopTest();
		List<Segmentation__c> segAfter = [	SELECT Id, Account__c, Account__r.Name, Business_Segmentation_MS__c , Firm_Type__c, Organisation_Type__c, Firm_Office_Size__c 
											FROM Segmentation__c ];
		System.debug('Batch 3: Segments After:' + segAfter);
        for (Segmentation__c seg : segAfter) {
			System.debug(seg);
			System.debug(seg.Account__r.Name);
            
            if (seg.Account__r.Name == 'Robert Clown'){
				System.assertEquals(NULL, seg.Business_Segmentation_MS__c); 
            }
        }
        System.assert(segAfter.size()==1);
    }
    
}