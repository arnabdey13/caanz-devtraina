public without sharing class CustomPasswordSetController {

    public String password1 { get; set; }
    public String password2 { get; set; }
    public String message { get; set; }
    public Boolean validLoad { get; set; }

    private String userName { get; set; }
    private Id userId { get; set; }
    private Id accountId { get; set; }

    public String gackMessage { get {return 'We apologise but something is wrong with the link provided. Please contact our Support Team for assistance.';} }

    public CustomPasswordSetController() {
        String uuid = ApexPages.currentPage().getParameters().get('k');
  
        this.validLoad = true;
        if (uuid == NULL) {
            this.validLoad = false;
        } else {
            try {
                List<User> matches = [
                        SELECT Id,
                                Username,
                                IsActive,
                                Account.Id,
                                Account.IsCustomerPortal,
                                Account.Active__c,
                                Account.Registration_Temporary_Key__c,
                                Account.RecordType.Name
                        FROM User
                        WHERE Account.Registration_Temporary_Key__c = :uuid
                        LIMIT 1
                ];
                if (matches.size() == 0 || matches.size() > 1) {
                    this.validLoad = false;
                } else {
                    // TODO check active status
                    this.validLoad = true;
                    this.accountId = matches[0].Account.Id;
                    this.userId = matches[0].Id;
                    this.userName = matches[0].Username;
                }
            } catch (Exception e) {
                this.validLoad = false;
            }
        }
    }

    public PageReference setPassword() {

        this.message = null;

        try {
            if (password1 != password2) {
                this.message = 'Passwords do not match';
                return null;
            }

            try {
                System.setPassword(userId, password1);

                update new Account(
                        Id = this.accountId,
                        Registration_Temporary_Key__c = 'SELF_REGISTERED');

            } catch (Exception e) {
                this.message = e.getMessage();
                return null;
            }
          
            String startURL = ApexPages.currentPage().getParameters().get('startURL');
            if (startURL == '') {
                startURL = '/s/community-router';
            }
            return Site.login(userName, password1, startURL);

        } catch (Exception e) {
            this.message = gackMessage;
            return null;
        }
    }


}