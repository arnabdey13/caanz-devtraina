/**
* @author       Jannis Bott
* @date         14/12/2016
*
* @group        QAS
* @description  specific methods that are used to validate an address with QAS
* @change log   refactored this code from the original code written by DataColada
*/
public class EDQAddressService {

    /**
    * @description  should be called to search an address.
    *               It will then call a generic callout service to execute the callout
    *               before returning the response body
    * @param        String searchTerm - the street to search for
    * @param        String country - the country the address belongs to
    * @param        Integer take - the number of results that should be returned
    *
    * @return       Map<String, List<Map<String, String>>> - key contains SUCCESS/ERROR depending on status.
    *               The List contains either the error code or the results
    */
    public static Map<String, List<Map<String, Object>>> searchAddress(String searchTerm, String countryCode, Integer take) {
        Long startTime = System.now().getTime();
        String country = getQASCountryCode(countryCode);
        String addressSearchUrl = '/search?query=' + EncodingUtil.urlEncode(searchTerm, 'UTF-8');
        addressSearchUrl += '&country=' + country;
        addressSearchUrl += '&take=' + take;
        List<Map<String, Object>> suggestionIdMap = new List<Map<String, Object>>();

        EDQAddressService.QASWrapper response = executeSearch('QAS_Address_Service', addressSearchUrl, '');
        if (response.getError() != null) {
            response = executeSearch('QAS_Address_Service', addressSearchUrl, '');
        }
        System.debug(response);
        if (response.getError() == null) {
            Integer counter = 0;
            for (EDQAddressService.QASResultsWrapper wrap : response.getResults()) {
                String fullSuggestion = wrap.suggestion;
                String label1 = fullSuggestion.substring(0, fullSuggestion.indexOf(','));
                String label2 = fullSuggestion.substring(fullSuggestion.indexOf(', ') + 1, fullSuggestion.length());
                PageReference pr = new PageReference(wrap.format);
                suggestionIdMap.add(new Map<String, Object>{
                        'position' => counter,
                        'label1' => label1,
                        'label2' => label2,
                        'id' => pr.getParameters().get('id')
                });
                counter++;
            }
            return new Map<String, List<Map<String, Object>>>{
                    'SUCCESS' => suggestionIdMap
            };
        } else {
            suggestionIdMap.add(new Map<String, Object>{
                    'ErrorCode' => String.valueOf(response.getError())
            });
            return new Map<String, List<Map<String, Object>>>{
                    'ERROR' => suggestionIdMap
            };
        }
    }

    /**
    * @description  dispatches the callout
    * @param        String configurationName - the custom metadata that contains the configuration
    * @param        String url - the search url
    * @param        String body - the body of the request
    *
    * @return       EDQAddressService.QASWrapper - the deserialised body of the callout
    */
    private static EDQAddressService.QASWrapper executeSearch(String configurationName, String url, String body) {
        String response = CalloutService.executeCallout(configurationName, url, body);
        System.debug('EDQAddressService: RESPONSE ' + response);
        EDQAddressService.QASWrapper qasWrap = (EDQAddressService.QASWrapper) System.JSON.deserialize(response, EDQAddressService.QASWrapper.class);

        return qasWrap;
    }

    /**
    * @description  should be called to get the full QAS validated address.
    *               The formatUrl parameter is retrieved from the search results in the seachAddress method
    * @param        String formatUrl - the specific url that will get the full QAS address
    *
    * @return       String - the response body of the callout
    */
    public static Map<String, Object> formatAddress(String id, String countryCode) {
        String country = getQASCountryCode(countryCode);

        String fullAddressUrl = '/format?country=' + country;
        fullAddressUrl += '&id=' + id;
        String response = CalloutService.executeCallout('QAS_Address_Service', fullAddressUrl, '');
        if(response.contains('CalloutServiceError')){
            response = CalloutService.executeCallout('QAS_Address_Service', fullAddressUrl, '');
        }

        if(response.contains('CalloutServiceError')){
            EDQAddressService.QASWrapper qasWrap = (EDQAddressService.QASWrapper) System.JSON.deserialize(response, EDQAddressService.QASWrapper.class);
            return new Map<String, Object>{'ERROR' => String.valueOf(qasWrap.getError())};
        } else{
            return new Map<String, Object>{'SUCCESS' => extractAddress(response)};
        }
    }

    private static String getQASCountryCode(String countryCode) {
        if (countryCode.equals('AU')) {
            return 'aus';
        } else if (countryCode.equals('NZ')) {
            return 'nzl';
        } else {
            return null;
        }
    }

    private static Map<String, Object> extractAddress(String resultJSON) {
        Map<String, Object> response = new Map<String, Object>();
        System.debug(resultJSON);
        Map<String, Object> parsed = (Map<String, Object>) JSON.deserializeUntyped(resultJSON);
        System.debug(parsed);
        List<Object> addresses = (List<Object>) parsed.get('address');
        System.debug(addresses);
        String street = '';
        for (Object obj : addresses) {
            Map<String, Object> address = (Map<String, Object>) obj;
            if (address.get('addressLine1') != null && address.get('addressLine1') != '') {
                street = (String) address.get('addressLine1');
            }
            if (address.get('addressLine2') != null && address.get('addressLine2') != '') {
                street += ', ' + (String) address.get('addressLine2');
            }
            if (address.get('addressLine3') != null && address.get('addressLine3') != '') {
                street += ', ' + (String) address.get('addressLine3');
            }
            response.put('street', street);
        }

        List<Object> components = (List<Object>) parsed.get('components');
        for (Object obj : components) {
            Map<String, Object> component = (Map<String, Object>) obj;
            if (component.get('locality1') != null) {
                response.put('city', (String) component.get('locality1'));
            }
            if (component.get('province1') != null) {
                response.put('state', (String) component.get('province1'));
            }
            if (component.get('postalCode1') != null) {
                response.put('postcode', (String) component.get('postalCode1'));
            }
            if (component.get('country1') != null) {
                response.put('country', (String) component.get('country1'));
            }
            if (component.get('deliveryPointId1') != null) {
                response.put('dpid', (String) component.get('deliveryPointId1'));
            }
            if (component.get('countryISO1') != null) {
                if(component.get('countryISO1') == 'AUS'){
                    response.put('countryCode', 'AU');
                }
                if(component.get('countryISO1') == 'NZL'){
                    response.put('countryCode', 'NZ');
                }
            }
            response.put('qasValidated', true);
        }

        return response;
    }

    public class QASWrapper {

        private Integer count;

        private QASResultsWrapper[] results;

        private CalloutService.CalloutResult errorCode;

        public Integer getCount() {
            return count;
        }

        public List<QASResultsWrapper> getResults() {
            return results;
        }
        public CalloutService.CalloutResult getError() {
            return errorCode;
        }
    }

    public class QASResultsWrapper {

        private String suggestion;

        private Matched[] matched;

        private String format;

        public String getSuggestion() {
            return suggestion;
        }

        public Matched[] getMatched() {
            return matched;
        }

        public String getFormat() {
            return format;
        }
    }

    public class Matched {

    }

}