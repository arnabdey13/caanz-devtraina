/**
* @author Jannis Bott
* @date 23/11/2016
*
* @group FormsFramework
*
* @description  Account builder that is used to generate test accounts
*/
public with sharing class ff_TestDataCampaignBuilder {

    String name;

    public ff_TestDataCampaignBuilder name(String name) {
        this.name = name;
        return this;
    }

    public List<Campaign> create(Integer count, Boolean persist) {
        List<Campaign> results = new List<Campaign>();
        for (Integer i = 0; i < count; i++) {
            results.add(new Campaign(
                    Name = this.name
            ));
        }
        if (persist) insert results;
        return results;
    }

}