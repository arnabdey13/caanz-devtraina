public class CountryPicklistSetting implements Comparable {
    // this is a data class for sending data to Lightning. It is populated by parsing the Address.settings file.

    private String countryCode;
    private String label;
    // states are a sorted list of 2 element String lists. First element = stateCode, Second = label;
    private List<List<String>> states = new List<List<String>>();

    public CountryPicklistSetting() {}
    
    public CountryPicklistSetting(String code, String label) {
        this.countryCode = code;
        this.label = label;
    }
    
    public Integer compareTo(Object compareTo) {
        CountryPicklistSetting other = (CountryPicklistSetting) compareTo;
        String thisLabel = this.getCountryLabel();
        String otherLabel = other.getCountryLabel();
        return thisLabel.compareTo(otherLabel);
    }
    
    public void setLabel(String label) {
        this.label = label;
    }
    
    public void setCode(String code) {
        this.countryCode = code;
    }
    
    @AuraEnabled
    public String getCountryCode() {
        return this.countryCode;
    }

    @AuraEnabled
    public String getCountryLabel() {
        return this.label;
    }

    @AuraEnabled
    public List<List<String>> getStates() {
        return this.states;
    }

    public void addState(List<String> state) {
        states.add(state);
    }
    
}