public with sharing class PortalSelfRegistration {
	/* Internal Variables */	
	private List<Registration_Setting__c> settings = new List<Registration_Setting__c>();
	private Map<String, RecordType> mapRecordTypes {get {
			if(mapRecordTypes == null) {
				mapRecordTypes = new Map<String, RecordType>();
				for(RecordType r:[select Id, DeveloperName from RecordType where sObjectType = 'Account' and IsPersonType = true and IsActive = true]) {
					mapRecordTypes.put(r.DeveloperName, r);
				}	
			}
			return mapRecordTypes;		
		}
		set; }
	public Id userRecordId {get;set;}
	/* Page Variables */	
	public String strRegistrationType {get;set;}
	public String strRegistrationSubType {get;set;}
	public String DEFAULT_ACCOUNT_OWNER_ID; // = '00590000001HoNH'; // Defaulted. This is populated below
	public SignUserRecord userRecord {get;set;}
	public List<System.selectOption> listTitleOptions {get;set;}
	public List<System.selectOption> listCountryOptions {get;set;}
	public List<System.selectOption> listTertiarySubjectOptions {get;set;}
	public List<System.selectOption> listSecondarySubjectOptions {get;set;}
	public List<System.selectOption> listTertiarySourceOptions {get;set;}
	public List<System.selectOption> listSecondarySourceOptions {get;set;}	
	public List<System.selectOption> listTertiaryYearOptions {get;set;}
	public List<System.selectOption> listSecondaryYearOptions {get;set;}
	public List<System.selectOption> listTertiaryCampusOptions {get;set;}
	public List<System.selectOption> listTertiaryYearDegreeOptions {get;set;}
	public List<System.selectOption> listTertiaryFirstMajorOptions {get;set;}	
	public List<System.selectOption> listTertiarySecondMajorOptions {get;set;}	
	public List<System.selectOption> listTertiaryCampusOfStudy {
		get{
			if(listTertiaryCampusOfStudy==null){
				// Only displayed on the Tertiary page at the moment
				listTertiaryCampusOfStudy = new List<System.selectOption>();
				Schema.DescribeFieldResult DFR = Schema.SObjectType.Account.fields.Campus_of_study__c;
				for( Schema.PicklistEntry PLE : DFR.getPicklistValues() ){
					listTertiaryCampusOfStudy.add(new SelectOption( PLE.getLabel(), PLE.getLabel() ));
				}
			}
			return listTertiaryCampusOfStudy;
		}
		set;
	}
	public class SignUserRecord {
		// Generic Sign up 
		public String strRecordTypeId {get; set;}
		public String strUsername {get; set;}
		public String strPassword {get; set {strPassword = value == null ? value : value.trim();}}
		public String strConfirmPassword {get; set {strConfirmPassword = value == null ? value : value.trim();}}
		public String strTitle {get; set;}
		public String strFirstName {get; set;}
		public String strMiddleName {get; set;}	
		public String strLastName {get; set;}
		public String strPreferredFirstName {get; set;}	
		public String strBirthDate {get; set;}	
	    public String strGender {get; set;}	
		public String strEmail {get; set;}	
		public String strMobilePhone {get; set;}	
		public String strOtherAddress {get; set;}	
		public String strOtherSuburb {get; set;}
		public String strOtherCity {get; set;}
		public String strOtherCountry {get; set;}
		public String strOtherPostalCode {get; set;}	
		// Student Affiliate - Secondary School
		public String strSecondarySchool {get; set;}	
		public String strCurrentYear {get; set;}	
		public String strSubjects {get; set;}
		public Boolean bTakenAccounting {get; set;}
		public String strOtherInfo {get; set;}
		public String strWhereDidYouHearAboutTheStudentAffiliateProgramme {get; set;}	
		public String strWhereDidYouHearAboutTheStudentAffiliateProgrammeOther {get; set;}	
		// Student Affiliate - Tertiary School	
		public String strMailingAddress {get; set;}	
		public String strMailingSuburb {get; set;}
		public String strMailingCity {get; set;}
		public String strMailingCountry {get; set;}
		public String strMailingPostalCode {get; set;}	
		public String strPreferredMailingAddress {get;set;}
		public String strCampusOfStudy {get; set;}	
		public String strYearDegree {get; set;}
		public Boolean bInternationalStudent {get; set;}
		public Boolean bIndegineousStudent {get; set;}
		public Boolean bMatureStudent {get; set;}
		public Boolean bSubscribeTertiaryFirstAndSecondYear {get; set;}
		public Boolean bSubscribeTertiaryThirdAndFourthYear {get; set;}
		public String strNZICAId {get; set;}		
		public Boolean bHasNZICAId {get; set;}
		// Non Member
	}
	// Used in test method
	public void populateTestData() {
		// Generic Sign up 
		userRecord.strTitle = 'Mr';
		userRecord.strFirstName = 'Test';	
		userRecord.strLastName = 'User';
		userRecord.strMiddleName = 'Middle';
		userRecord.strPreferredFirstName = 'Test Nickname';	
		userRecord.strBirthDate = '06/01/1978';	
	    userRecord.strGender = 'Male';	
		userRecord.strEmail = 'gilbert.deleon@davanti.co.nz';	
		userRecord.strMobilePhone = '1234567890';	
		userRecord.strOtherAddress = '135 Vincent Street';	
		userRecord.strOtherSuburb = 'Auckland CBD';
		userRecord.strOtherCity = 'Auckland';
		userRecord.strOtherCountry = 'New Zealand';
		userRecord.strOtherPostalCode = '1010';	
		// Student Affiliate - Secondary School
		userRecord.strSecondarySchool = 'Secondary School';	
		userRecord.strCurrentYear = 'Current Year';	
		userRecord.strSubjects = 'Subjects 1; Subjects 2';
		userRecord.bTakenAccounting = true;
		userRecord.strOtherInfo = 'Other info';
		userRecord.strWhereDidYouHearAboutTheStudentAffiliateProgramme = 'Other';	
		userRecord.strWhereDidYouHearAboutTheStudentAffiliateProgrammeOther = 'Others 2';	
		// Student Affiliate - Tertiary School	
		userRecord.strMailingAddress = 'Mailing Address';	
		userRecord.strMailingSuburb = 'Mailing Suburb';
		userRecord.strMailingCity = 'Mailing City';
		userRecord.strMailingCountry = 'Mailing Country';
		userRecord.strMailingPostalCode = '1011';
		userRecord.strPreferredMailingAddress = 'Mailing';
		userRecord.strCampusOfStudy = 'Campus of study';	
		userRecord.strYearDegree = '1997';
		userRecord.bInternationalStudent = true;
		userRecord.bSubscribeTertiaryFirstAndSecondYear = true;
		userRecord.bSubscribeTertiaryThirdAndFourthYear = true;
		userRecord.strNZICAId = '102499';		
		userRecord.bHasNZICAId = false;		
	}			
	// Constructor
	public PortalSelfRegistration() {
		// Default Values		
		//TOM: DEFAULT_ACCOUNT_OWNER_ID = '00590000001HoNH'; // Defaulted. This is populated below
		userRecord = new SignUserRecord();
		userRecord.strGender = 'Male';
		if(ApexPages.currentPage().getParameters().get('type') != null) 
			strRegistrationType = ApexPages.currentPage().getParameters().get('type');
		
		populateAvailableTitles();	
		populateAvailableCountries();
		populateTertiaryYearDegreeOptions();
		populateRegistrationSettings();
		//populateTestData();
	}
	public Boolean Validate() {
		Boolean bReturnValue = true;
		// Required fields
		if(userRecord.strUsername != null) {
			List<User> usersList = new List<User>([select Id, username from User where username = :userRecord.strUsername]);	
			if(usersList.size()>0) {
				addPageMessage(ApexPages.severity.ERROR, 'Username already taken. Please try another one');
				bReturnValue = false;
			}		
		}
		return bReturnValue;
	}	
	public PageReference Register() {
		PageReference pref = null;
		if(Validate()) { // perform validations

			// create userRecord
			
			system.debug('### SignUserRecord: ' + userRecord);
			User u = new User();
	        u.Username = userRecord.strUsername;
	        u.FirstName = userRecord.strFirstName;
	        u.LastName = userRecord.strLastName;
	        u.Email = userRecord.strEmail;
	        u.CommunityNickname = getNickName(userRecord.strFirstName, userRecord.strLastName);
	        
	        if (u.firstname != null && u.lastname != null) u.Alias = u.FirstName.substring(0,1) + u.LastName.substring(0,1);
	        u.TimeZoneSidKey = 'Pacific/Auckland';
	        u.LocaleSidKey = 'en_NZ';
	        u.LanguageLocaleKey = 'en_US';
	        u.EmailEncodingKey = 'ISO-8859-1';
	        
	        if (userRecord.strMobilePhone != null) u.MobilePhone = userRecord.strMobilePhone;
	        if (userRecord.strOtherAddress != null) u.Street = userRecord.strOtherAddress;
	        if (userRecord.strOtherSuburb != null) u.City = userRecord.strOtherSuburb;
	        if (userRecord.strOtherCity != null) u.State = userRecord.strOtherCity;
	        if (userRecord.strOtherPostalCode != null) u.PostalCode = userRecord.strOtherPostalCode;
	        if (userRecord.strOtherCountry != null) u.Country = userRecord.strOtherCountry;
			// Determine record type
			if(strRegistrationType == 'StudentAffiliate' && mapRecordTypes.containsKey('Student_Affiliate')) {
				userRecord.strRecordTypeId = String.valueOf(mapRecordTypes.get('Student_Affiliate').Id).substring(0,15);
			}
			if(strRegistrationType == 'NonMember' && mapRecordTypes.containsKey('Non_member')) {
				userRecord.strRecordTypeId = String.valueOf(mapRecordTypes.get('Non_member').Id).substring(0,15);
			}	
			system.debug('### RECORD TYPE: ' + userRecord.strRecordTypeId);		
			Savepoint sp = Database.setSavepoint();
			try {
				system.debug('u:' + u);	
				system.debug('DEFAULT_ACCOUNT_OWNER_ID:' + DEFAULT_ACCOUNT_OWNER_ID);  
				system.debug('userRecord.strRecordTypeId:' + userRecord.strRecordTypeId);   
				system.debug('userRecord.strPassword:' + userRecord.strPassword);   
	        	userRecordId = Site.createPersonAccountPortalUser(u, DEFAULT_ACCOUNT_OWNER_ID, userRecord.strRecordTypeId, userRecord.strPassword);
	        	system.debug('### userRecordId: ' + userRecordId);	
	        	populateAdditionalMembershipDetails(userRecordId);
	        	if(userRecordId != null) {
		        	//String startUrl = '/PortalHome';
		        	String startUrl = '/home/home.jsp';
		        	return Site.login(userRecord.strUsername, userRecord.strPassword, startUrl);
	        	}
			}
	        catch (Exception e) {
	        	system.debug('### Registration Error: ' + e);
	        	Database.rollback(sp);
	        	addPageMessage(e);
	        	return null;
	        }
		}
		return pref;
	}
	public void populateAdditionalMembershipDetails(Id userId) {
		List<Account> accounts = new List<Account>();
        for (User u:[select Id, AccountId, ContactId, Username, Email from User where Id = :userId]) {
        	Account a = new Account(Id = u.AccountId);
        	if(userRecord.strMiddleName != null) a.Middle_Name__c = userRecord.strMiddleName;
        	if(userRecord.strPreferredFirstName != null) a.Preferred_Name__c = userRecord.strPreferredFirstName;
        	if(userRecord.strGender != null) a.Gender__c = userRecord.strGender;
        	if(userRecord.strBirthDate != null) a.PersonBirthdate = Date.parse(userRecord.strBirthDate);
        	if(userRecord.strYearDegree != null)  a.Year_of_first_accounting_paper__c = userRecord.strYearDegree;
        	
        	if(userRecord.strMailingAddress != null) a.PersonMailingStreet = userRecord.strMailingAddress;
        	if(userRecord.strMailingSuburb != null)  a.PersonMailingState = userRecord.strMailingSuburb;
        	if(userRecord.strMailingCity != null)  a.PersonMailingCity = userRecord.strMailingCity;
        	if(userRecord.strMailingCountry != null)  a.PersonMailingCountry = userRecord.strMailingCountry;
        	if(userRecord.strMailingPostalCode != null)  a.PersonMailingPostalCode = userRecord.strMailingPostalCode;
        	
 
         	if(userRecord.strOtherAddress != null) a.PersonOtherStreet = userRecord.strOtherAddress;
        	if(userRecord.strOtherSuburb != null)  a.PersonOtherState = userRecord.strOtherSuburb;
        	if(userRecord.strOtherCity != null)  a.PersonOtherCity = userRecord.strOtherCity;
        	if(userRecord.strOtherCountry != null)  a.PersonOtherCountry = userRecord.strOtherCountry;
        	if(userRecord.strOtherPostalCode != null)  a.PersonOtherPostalCode = userRecord.strOtherPostalCode;       	
        	
        	if(strRegistrationType == 'StudentAffiliate' && strRegistrationSubType != null) a.Student_Type__c = strRegistrationSubType;
        	if(userRecord.strCurrentYear != null) a.Year_of_Study__c = userRecord.strCurrentYear;
        	if(userRecord.strSubjects != null && userRecord.strSubjects != '[]') a.Subjects__c = userRecord.strSubjects;
        	if(userRecord.strCampusOfStudy != null){
        		if( a.Student_Type__c=='Tertiary' ){
        			a.Campus_of_study__c = userRecord.strCampusOfStudy;
        		}
        		else{
        			a.Campus_of_study__c = 'Other';
        		}
        	}
        	
        	//throw new newException('## test error');
        	
        	if(userRecord.strSecondarySchool != null) a.Other_Campus_of_Study__c = userRecord.strSecondarySchool;
        	if(userRecord.bInternationalStudent != null) a.International_Student__c = userRecord.bInternationalStudent;
        	if(userRecord.bIndegineousStudent != null) a.Indigenous_Student_Maori_Descent__c = userRecord.bIndegineousStudent;
        	if(userRecord.bMatureStudent != null) a.Mature_Age_Student__c = userRecord.bInternationalStudent;
        	if(userRecord.strMobilePhone != null) a.PersonMobilePhone = userRecord.strMobilePhone;
        	accounts.add(a);
        }
        if(accounts.size()>0){ 
        	//Database.update(accounts);
        	WithoutSharing.updateAccountOnRegistration(accounts);
        }
	}
	public void populateAvailableTitles() {
		listTitleOptions = new List<System.selectOption>();
		List<String> strAvailableTitles = new List<String> {'Mr', 'Mrs', 'Miss', 'Ms'};
		for(String s:strAvailableTitles) {
			listTitleOptions.add(new System.selectOption(s, s));
		}
	}
	public void populateAvailableCountries() {
		listCountryOptions = new List<System.selectOption>();
		for(Country__c c:[select Name from Country__c order by Order__c limit 500]) {			
			listCountryOptions.add(new System.selectOption(String.escapeSingleQuotes(c.Name), String.escapeSingleQuotes(c.Name)));
		}
	}
	public void populateTertiaryYearDegreeOptions() {
		listTertiaryYearDegreeOptions = new List<System.selectOption>();
		listTertiaryYearDegreeOptions.add(new System.selectOption('', ''));
		for(Integer i = 1990; i<=system.today().year(); i++) {
			 listTertiaryYearDegreeOptions.add(new System.selectOption(String.valueOf(i), String.valueOf(i)));
		}		
	}	
	public void populateRegistrationSettings() {
		settings = [Select Id, Value__c, Order__c, Name from Registration_Setting__c order by Name, Order__c limit 500];
		listSecondarySubjectOptions = new List<System.Selectoption>();
		listTertiarySubjectOptions = new List<System.Selectoption>();	
		listSecondarySourceOptions = new List<System.Selectoption>();
		listTertiarySourceOptions = new List<System.Selectoption>();
		listSecondaryYearOptions = new List<System.Selectoption>();
		listTertiaryYearOptions = new List<System.Selectoption>();
		listTertiaryCampusOptions = new List<System.Selectoption>();
		listTertiaryFirstMajorOptions = new List<System.Selectoption>();
		listTertiarySecondMajorOptions = new List<System.Selectoption>();			
		// Add Default Values
		listSecondarySourceOptions.add(new System.selectOption('', ''));
		listTertiarySourceOptions.add(new System.selectOption('', ''));	
		listTertiaryFirstMajorOptions.add(new System.selectOption('', ''));	
		listTertiarySecondMajorOptions.add(new System.selectOption('', ''));
		if(settings.size()>0) { 
			for(Registration_Setting__c rs:settings) {
				if(rs.Name == 'Secondary-Subjects') listSecondarySubjectOptions.add(new System.selectOption(rs.Value__c, rs.Value__c));
				if(rs.Name == 'Tertiary-Subjects') listTertiarySubjectOptions.add(new System.selectOption(rs.Value__c, rs.Value__c));
				if(rs.Name == 'Secondary-Source') listSecondarySourceOptions.add(new System.selectOption(rs.Value__c, rs.Value__c));
				if(rs.Name == 'Tertiary-Source') listTertiarySourceOptions.add(new System.selectOption(rs.Value__c, rs.Value__c));		
				if(rs.Name == 'Secondary-Year') listSecondaryYearOptions.add(new System.selectOption(rs.Value__c, rs.Value__c));
				if(rs.Name == 'Tertiary-Year') listTertiaryYearOptions.add(new System.selectOption(rs.Value__c, rs.Value__c));	
				if(rs.Name == 'Tertiary-Campus') listTertiaryCampusOptions.add(new System.selectOption(rs.Value__c, rs.Value__c));	
				if(rs.Name == 'Tertiary-FirstMajor') listTertiaryFirstMajorOptions.add(new System.selectOption(rs.Value__c, rs.Value__c));	
				if(rs.Name == 'Tertiary-SecondMajor') listTertiarySecondMajorOptions.add(new System.selectOption(rs.Value__c, rs.Value__c));
				if(rs.Name == 'Default Owner') DEFAULT_ACCOUNT_OWNER_ID = rs.Value__c;													
			}
		}		
	}
    public String getNickName(String firstname, String lastName){
    	String communityNickname = ''; 
    	if (firstname != '' && lastname != ''){
    		communityNickname = firstname + '.' + lastname;	
    	}
    	List<User> userRecordsList = new List<User>([select Id, CommunityNickname from User where CommunityNickname =:communityNickname]);
    	Integer sameNickName = 0;
    	if (userRecordsList.size()>0){
    		communityNickname = firstname + '.' + lastname + String.valueOf(math.round(math.random()*1000));
    	}
    	System.Debug('### Community nickname: ' + communityNickname);
    	return communityNickname;
    }	
	// Add Page Messages
	public void addPageMessage(ApexPages.severity severity, Object objMessage) {
		ApexPages.Message pMessage = new ApexPages.Message(severity, String.valueOf(objMessage));
		ApexPages.addMessage(pMessage);
	}
	public void addPageMessage(Object objMessage) {
		addPageMessage(ApexPages.severity.INFO, objMessage);
	}	
}