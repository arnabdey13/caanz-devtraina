public class luana_PrintTranscriptCtl {
    
    public List<LuanaSMS__Student_Program__c> spList {get; set;}
    
    public Id currentSPId {get; private set;}
    public Id currentContId {get; private set;}
    public Boolean viewAsPDF {get; set;}
    
    public Boolean isCommunityUser {get; set;}
    public Boolean isAwarded {get; set;}
    public Boolean isCompleted {get; set;}
    public Boolean isWithoutHeader { get; set; }
    
    public Boolean savePDFFlag{get; set;}
    
    public luana_PrintTranscriptCtl(){
        
        Id userId = UserInfo.getUserId();
        System.debug('****userId:: ' + userId);
        
        savePDFFlag = false;
        viewAsPDF = false;
        
        List<User> users = [select Id, IsActive, Profile.Name, UserRole.Name, UserType from User Where Id =: userId];
        if(users[0].UserType == 'CSPLitePortal'){
            isCommunityUser = true;
            isWithoutHeader = true;
        }else{
            isCommunityUser = false;
        }   
        
        currentSPId = ApexPages.currentPage().getParameters().get('id');
        currentContId = ApexPages.currentPage().getParameters().get('cid');
        isWithoutHeader = Boolean.valueOf(ApexPages.currentPage().getParameters().get('h'));
        System.debug('*****currentSPId:: ' + currentSPId);
        
        Map<Id, Boolean> hasAllSPAMPublished = new Map<Id, Boolean>();
        for(LuanaSMS__Student_Program__c spAM: [Select Id, Name, Result_Publish__c, LuanaSMS__Contact_Student__c from LuanaSMS__Student_Program__c Where LuanaSMS__Contact_Student__c =: currentContId and recordType.Name = 'Accredited Module']){
            if(hasAllSPAMPublished.containsKey(spAM.LuanaSMS__Contact_Student__c)){
                if(hasAllSPAMPublished.get(spAM.LuanaSMS__Contact_Student__c)){  //THis line will check if the Student contains any SP AM is published
                    hasAllSPAMPublished.put(spAM.LuanaSMS__Contact_Student__c, spAM.Result_Publish__c);
                }
            }else{
                hasAllSPAMPublished.put(spAM.LuanaSMS__Contact_Student__c, spAM.Result_Publish__c);
            }
        }
        
        System.debug('****hasAllSPAMPublished::: ' + hasAllSPAMPublished);
        
        Set<String> awardSet = new Set<String>();
        awardSet.add('CA Program');
        Set<String> completeSet = new Set<String>();
        completeSet.add('Draft - Completed');
        completeSet.add('Completed');
        for(LuanaSMS__Student_Program__c sp: [Select Id, Name, LuanaSMS__Contact_Student__c, LuanaSMS__Status__c, LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.Product_Type__c, LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.Award_Transcript_Format__c 
                                                from LuanaSMS__Student_Program__c Where Id =: currentSPId]){
            
            currentContId = sp.LuanaSMS__Contact_Student__c;
            
            if(sp.LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.Award_Transcript_Format__c){
                isAwarded = true;
            }else{
                isAwarded = false;
            }
            
            if(completeSet.contains(sp.LuanaSMS__Status__c)){
                if(hasAllSPAMPublished.containsKey(sp.LuanaSMS__Contact_Student__c)){
                    if(hasAllSPAMPublished.get(sp.LuanaSMS__Contact_Student__c)){
                        isCompleted = true;
                    }else{
                        isCompleted = false;
                    }
                }
            }else{
                isCompleted = false;
            }
            
        }
        
        
        System.debug('*****:: ' + isAwarded + ' - '+ isCompleted + ' - '+ isWithoutHeader);

    }
 /*   
    public pageReference setWithOrWithoutHeader() {
        String tempVal = Apexpages.currentPage().getParameters().get('hasHeader');
        System.debug('*****Set Header 1:: ' + tempVal);
        if(tempVal != null){
            isWithoutHeader = Boolean.valueOf(tempVal);
        }else{
            isWithoutHeader = false;
        }
        System.debug('*****Set Header 2:: ' + isWithoutHeader);
        
        PageReference pagePdf;
        if(isCommunityUser){
            pagePdf = new PageReference('/member/luana_PrintTranscript?id='+currentSPId+'&cid='+currentContId+'&h='+isWithoutHeader); 
        }else{
            pagePdf = new PageReference('/apex/luana_PrintTranscript?id='+currentSPId+'&cid='+currentContId+'&h='+isWithoutHeader); 
        }  
        pagePdf.setRedirect(true);
        return pagePdf;
    }
  */  
    public void savePDF(){
        System.debug('***save pdf::: ');
        if (!savePDFFlag){
            viewAsPDF = true;
            Document_Repository__c dr = new Document_Repository__c();
            dr.Contact__c = currentContId;
            dr.Is_Public__c = true;
            dr.Student_Program__c = currentSPId;
            dr.Name = 'Transcript ' + Datetime.now().format('yyyy-MM-dd HH:mm');
            insert dr;
            
            System.debug('***run here dr 1::: ' + dr);
            
            PageReference pagePdf;
            if(isCommunityUser){
                pagePdf = new PageReference('/member/luana_PrintTranscript?id='+currentSPId+'&cid='+currentContId+'&h='+isWithoutHeader); 
            }else{
                pagePdf = new PageReference('/apex/luana_PrintTranscript?id='+currentSPId+'&cid='+currentContId+'&h='+isWithoutHeader); 
            }       
            //System.debug('***run here dr 2::: ' + pagePdf.getContentAsPDF());
            Blob pdfPageBlob; 
            
            if(Test.isRunningTest()) { 
              pdfPageBlob = blob.valueOf('Unit.Test');
            } else {
              pdfPageBlob = pagePdf.getContentAsPDF();
            }
            
            //pdfPageBlob = pagePdf.getContentAsPDF(); 
            System.debug('***run here dr 3::: ');
            Attachment a = new Attachment(); 
            a.Body = pdfPageBlob; 
            a.ParentID = dr.Id;
            a.Name = 'Transcript ' + Datetime.now().format('yyyy-MM-dd HH:mm')+'.pdf'; 
            insert a; 
      
            System.debug('***run here a::: ' + a);
           
            savePDFFlag=true;
        }
    }
    
}