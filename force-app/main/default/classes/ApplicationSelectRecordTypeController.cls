public class ApplicationSelectRecordTypeController {
    
    @AuraEnabled
    public static List<String> getApplicationRecordTypeNames(Boolean excludeProvAppIfDraft,Boolean excludeIPPAppIfDraft) {
        Id accountId = EditorPageUtils.getUsersAccountId();
        List<Application__c> provisionalApps = [Select Id,RecordType.Name,Application_Status__c
                                                FROM Application__c
                                                WHERE Account__c = :accountId AND
                                                RecordType.Name IN ('Provisional Member','IPP Provisional Member') AND
                                                Application_Status__c IN ('Draft')];
        integer provApps=0; integer ippApps=0;
        if(provisionalApps.size()>0){
            for(Application__c app:provisionalApps){
                if(app.RecordType.Name == 'Provisional Member' ) 
                    provApps++;
                if(app.RecordType.Name == 'IPP Provisional Member')
                    ippApps++;               
            }            
        }
        
        Schema.SObjectType applicationSObjectType = Schema.getGlobalDescribe().get('Application__c');
        List<String> rtnames = new List<String>(); 
        for (String rc : Utility.GetAvailableRecordTypeNamesForSObject(applicationSObjectType)) {
            
            if(excludeProvAppIfDraft && rc == 'Provisional Member' && provApps==0) {
                rtnames.add(rc);                
            } 
            else if(excludeIPPAppIfDraft && rc == 'IPP Provisional Member' && ippApps==0 ){                
                rtnames.add(rc);                
            }
            else if(rc != 'Provisional Member' && rc != 'IPP Provisional Member'){
                rtnames.add(rc);
            }
        }
        return rtnames;
    }
}