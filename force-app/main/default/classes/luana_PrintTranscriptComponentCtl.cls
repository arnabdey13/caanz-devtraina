public class luana_PrintTranscriptComponentCtl {
    
    public Id currentSPId {get; private set;}
    public Id currentContId {get; private set;}
    
    public luana_PrintTranscriptComponentCtl(){
        
        currentSPId = ApexPages.currentPage().getParameters().get('id');
        currentContId = ApexPages.currentPage().getParameters().get('cid');
    }
    
    /*
        Assign the params in this way is because the controller in vf:component page will run first, therefore the params is not set. 
        If we have other action in component to call the method we won't need to do it in this way  
    */
    public Boolean isAwardedVal;
    public Boolean getIsAwardedVal() {return isAwardedVal;}
    public void setIsAwardedVal(Boolean isAward){
        isAwardedVal = isAward;
    }
    
    public Boolean isCommunityUserVal;
    public Boolean getIsCommunityUserVal() {return isCommunityUserVal;}
    public void setIsCommunityUserVal(Boolean isCommUser){
        isCommunityUserVal = isCommUser;
    }
    
    public Boolean isCompletedVal;
    public Boolean getIsCompletedVal() {return isCompletedVal;}
    public void setIsCompletedVal(Boolean isCompleted){
        isCompletedVal = isCompleted;
    }
    
    public Boolean hasHeaderVal;
    public Boolean getHasHeaderVal() {return hasHeaderVal;}
    public void setHasHeaderVal(Boolean hasHeader){
        hasHeaderVal = hasHeader;
    }
    
    public Id currentSPIdVal;
    public Id getCurrentSPIdVal() {return currentSPIdVal;}
    public void setCurrentSPIdVal(Id spId){
        currentSPIdVal = spId;
        luana_ViewTranscriptComponentCtl();
    }
    
    
    
    public static String awardHeaderText = 'Statement of Academic Record';
    public static String fullAwardHeader ='Chartered Accountants Program / Graduate Diploma (CA)';
    public static String nonAwardHeader = 'Chartered Accountants Program';
    public static String awardHeader {get; private set;}
    
    
    public static String completedAwardedFooter ='All requirements for the Chartered Accountants Program / Graduate Diploma (CA) were fulfilled on ';
    public static String nonCompletedAwardedFooter = 'Academic transcript shown above is valid as at ';
    public static String completedNonAwardedFooter ='All requirements for the Chartered Accountants Program were fulfilled on ';
    public static String nonCompletedNonAwardedFooter = 'Academic transcript shown above is valid as at ';
    public static String footerVal {get; set;}
    
    public static String footerBodyText {get; set;}
    public static String completedAwardedFooterBody ='This candidate has passed the Chartered Accountants Program / Graduate Diploma (CA) and has now fulfilled the educational requirements for admission to membership of the Chartered Accountants Australia and New Zealand. Subject to satisfying all other requirements, the above/named candidate is eligible to apply for membership.';
    public static String nonCompletedAwardedFooterBody = 'This candidate is required to complete additional modules to complete the Chartered Accountants Program / Graduate Diploma (CA) and fulfil the educational requirements for admission to membership of the Chartered Accountants Australia and New Zealand.';
    public static String completedNonAwardedFooterBody ='This candidate has passed the Chartered Accountants Program and has now fulfilled the educational requirements for admission to membership of the Chartered Accountants Australia and New Zealand. Subject to satisfying all other requirements, the above/named candidate is eligible to apply for membership.';
    public static String nonCompletedNonAwardedFooterBody = 'This candidate is required to complete additional modules to complete the Chartered Accountants Program and fulfil the educational requirements for admission to membership of the Chartered Accountants Australia and New Zealand.';
    
    public LuanaSMS__Student_Program__c currSP {get; set;}
    public LuanaSMS__Student_Program__c spAP {get; set;}
    //public List<LuanaSMS__Student_Program__c> allSP {get; set;}
    public List<tempStudProg> allSPList {get; set;}
    
    public Boolean splitPage {get; set;}
    
    public void luana_ViewTranscriptComponentCtl(){
        
        System.debug('*******luana_ViewTranscriptComponentCtl::: ' + getIsAwardedVal() + ' - '  + isCommunityUserVal + ' - '+ isCompletedVal + ' - ' + currentSPIdVal + ' - '+ hasHeaderVal);
        
        Map<String, Boolean> excludedSPMap = new Map<String, Boolean>();
        excludedSPMap.put('Pending Supp', true);
        excludedSPMap.put('Discontinued without Fail', true);
        excludedSPMap.put('Cancelled', true);
        excludedSPMap.put('Pending', true);
        excludedSPMap.put('Expired', true);
        
        if(isCommunityUserVal != null){
            if(isCommunityUserVal){
                setHasHeaderVal(true);
            }
        }
        
        allSPList = new List<tempStudProg>();
        for(LuanaSMS__Student_Program__c sp: [Select Id, LuanaSMS__Program_Commencement_Date__c, LuanaSMS__Contact_Student__r.Name, LuanaSMS__Contact_Student__r.Member_ID__c, RecordType.Name, Course_Code__c, LuanaSMS__Course__r.Description__c, Transcript_Status__c, LuanaSMS__Status__c, LuanaSMS__Completion_Date__c, Result_Publish__c,
                                                (Select Id, Completed__c, LuanaSMS__Subject_Id__c, LuanaSMS__Subject__r.Name, LuanaSMS__Outcome_National__c, Result__c, LuanaSMS__Student_Program__r.LuanaSMS__Completion_Date__c from LuanaSMS__Student_Program_Subjects__r Where LuanaSMS__Outcome_National__c = 'Recognition of prior learning granted') 
                                                from LuanaSMS__Student_Program__c Where LuanaSMS__Contact_Student__c =: currentContId order by LuanaSMS__Program_Commencement_Date__c ASC]){            
            
            currSP = sp;
            
            if(sp.RecordType.Name == 'Accredited Module'){
                if(!excludedSPMap.containsKey(sp.Transcript_Status__c)){
                    
                    String statusVal;
                    if(sp.Result_Publish__c){
                        statusVal = sp.LuanaSMS__Status__c;
                    }else{
                        if(sp.LuanaSMS__Status__c == 'Discontinued'){
                            statusVal = sp.LuanaSMS__Status__c;
                        }else{
                            statusVal = 'In Progress';
                        }
                    }
                    
                    allSPList.add(new tempStudProg(sp.Course_Code__c, sp.LuanaSMS__Course__r.Description__c, sp.Transcript_Status__c, statusVal, sp.LuanaSMS__Program_Commencement_Date__c));
                }
            }
            
            if(sp.RecordType.Name == 'Accredited Program'){
                for(LuanaSMS__Student_Program_Subject__c sps: sp.LuanaSMS__Student_Program_Subjects__r){
                    allSPList.add(new tempStudProg(sps.LuanaSMS__Subject_Id__c, sps.LuanaSMS__Subject__r.Name, 'Exempt', 'Completed', null));
                }
                
                spAP = sp;
            }
        }
        
        System.debug('***spAP:: ' + spAP);
        
        if(allSPList.size() > 10){
            splitPage = true;
        }else{
            splitPage = false;
        }
        
        if(isAwardedVal != null){
            if(isAwardedVal){
                awardHeader = awardHeaderText + '\n' + fullAwardHeader;
            }else{
                awardHeader = awardHeaderText + '\n' + nonAwardHeader;
            }
            
            //for footer text
            if(isAwardedVal && isCompletedVal) {
                
                String dateTimeStr = '';
                if(spAP.LuanaSMS__Completion_Date__c != null){
                    Integer d = spAP.LuanaSMS__Completion_Date__c.day();
                    Integer mo = spAP.LuanaSMS__Completion_Date__c.month();
                    Integer yr = spAP.LuanaSMS__Completion_Date__c.year();
                    DateTime dt = DateTime.newInstance(yr, mo, d);
                    
                    dateTimeStr = dt.format('dd MMMM yyyy');
                }
                
                footerVal = completedAwardedFooter + dateTimeStr;
                footerBodyText = completedAwardedFooterBody;
            }else if(isAwardedVal && !isCompletedVal){
                footerVal = nonCompletedAwardedFooter + Datetime.now().format('dd MMMM yyyy');
                footerBodyText = nonCompletedAwardedFooterBody;
            }else if(!isAwardedVal && isCompletedVal){
                
                String dateTimeStr = '';
                if(spAP.LuanaSMS__Completion_Date__c != null){
                    Integer d = spAP.LuanaSMS__Completion_Date__c.day();
                    Integer mo = spAP.LuanaSMS__Completion_Date__c.month();
                    Integer yr = spAP.LuanaSMS__Completion_Date__c.year();
                    DateTime dt = DateTime.newInstance(yr, mo, d);
                    
                    dateTimeStr = dt.format('dd MMMM yyyy');
                }
                footerVal = completedNonAwardedFooter + dateTimeStr;
                footerBodyText = completedNonAwardedFooterBody;
            }else if(!isAwardedVal && !isCompletedVal){
                footerVal = nonCompletedNonAwardedFooter + Datetime.now().format('dd MMMM yyyy');
                footerBodyText = nonCompletedNonAwardedFooterBody;
            }
            
            
        }
        
        
    }
    
    
    public class tempStudProg {
        public String codeStr {get; set;}
        public String moduleDescStr {get; set;}
        public String resultStr {get; set;}
        public String statusStr {get; set;}
        public Date dateStr {get; set;}
        
        //This is the contructor method. When we create a new cContact object we pass a Contact that is set to the con property. We also set the selected value to false
        public tempStudProg(String code, String mDesc, String result, String status, Date dStr) {
            codeStr = code;
            moduleDescStr = mDesc;
            resultStr = result;
            statusStr = status;
            dateStr = dStr;
        }
    }
}