/**
*	Project:			CAANZ Internal
*	Project Name:		Segmentation
*	Project Task Name:	Unit Test of the Account Address trigger Handlers
*	Developer:			akopec
*   Description			Set of tests for Address Trigger Handlers
*                       - SegmentationAddressTriggerUpdate
*                       - SegmentationAddressTriggerHandler
*                      Different scenario for each address: Residential, Mailing, Billing, Shipping and differenct country NZ, AU
*	Initial Date:		December 2018
*	Revision			R01 city centre
*   Required classes:
*     - SegmentationDataGenAddressTest (provides data for tests)
*     - CalloutSOAPHttpGenMock ( Experian Callout Mock Class)
**/

@isTest
public class SegmentationAddressTriggerHandlerTest {

    public static Boolean forceTriggerDuringTest = false;
    
    private static Map<Id, Account> oldMap;
	private static Map<Id, Account> newMap;
    private static List <Account> newList;
    private static List <Account> oldList;
    private static Boolean initiated = false;    
    private static Boolean debugFlag = false;
    
     @testSetup 
     static void setup() {
                    
		Segmentation_Settings__c setting1 = new Segmentation_Settings__c();   
         
        setting1.Name = 'SegmentationAddressUpdate';
		setting1.Active__c = TRUE;
		insert setting1;

		Segmentation_Settings__c setting2 = new Segmentation_Settings__c();          
        setting2.Name = 'SegmentationMembersUpdate';
		setting2.Active__c = TRUE;
		insert setting2;
     
        // create ref geo NZ
        Reference_Geography_New_Zealand__c ref_NZ = new Reference_Geography_New_Zealand__c();
        ref_NZ.Meshblock__c = '2416402';        
        ref_NZ.Name = '2416402';
        ref_NZ.Regional_Council_area_description__c = 'West Coast Region';
        ref_NZ.Territorial_authority_description__c = 'Westland District';
        insert ref_NZ;         
        
        // create ref geo AU
        Reference_Geography_Australia__c ref_AU = new Reference_Geography_Australia__c();
        ref_AU.SA1__c = '2115515';        
        ref_AU.SA2_Name__c = 'Surrey Hills (West) - Canterbury';
        ref_AU.SA3_Name__c = 'Boroondara';
        ref_AU.SA4_Name__c = 'Melbourne - Inner East';
        insert ref_AU;
         
        // create ref meshblock for above ref AU
		Reference_Meshblock_Australia__c meshSA1 = new Reference_Meshblock_Australia__c();
        meshSA1.Name = '20698620000';     
        meshSA1.MB_CODE_2016__c = '20698620000';            
        meshSA1.SA1_MAINCODE__c = '20701115515';
        meshSA1.SA1_ID__c = ref_AU.Id;
		insert meshSA1;
         
        Reference_Geography_to_City_Centre__c cityCentre = new Reference_Geography_to_City_Centre__c();
        cityCentre.City_Centre__c = 'Melbourne';
        cityCentre.SA2__c = 'Surrey Hills (West) - Canterbury';
        cityCentre.SA3__c = 'Boroondara';
        cityCentre.SA4__c = 'Melbourne - Inner East';
        insert cityCentre;
         
        Reference_Geography_to_City_Centre__c cityCentreNZ = new Reference_Geography_to_City_Centre__c();
        cityCentreNZ.City_Centre__c = 'Westland';
        cityCentreNZ.NZ_Territorial_Authority__c = 'Westland District';
        insert cityCentreNZ;
         

    }
    
    
    private static void addSegment(ID acctId) {
    	Segmentation__c seg = new Segmentation__c();        
        seg.Geo_AU_SA1__c = NULL;
        seg.Geo_NZ_Meshblock__c = NULL;
		seg.Account__c = acctId;
        seg.Geo_Confidence_Flag__c = NULL;
		insert seg;
   
    }
    
	private static void debugMaps () 
    {
			for ( ID oldIdKey : oldMap.keyset()) {
              System.debug('oldMap Person Account :' + oldMap.get(oldIdKey).isPersonAccount); 
                
              if (oldMap.get(oldIdKey).isPersonAccount==TRUE)
  	          	System.debug('oldMap Values ' + oldMap.get(oldIdKey).PersonOtherStreet);
               else
                System.debug('oldMap Values ' + oldMap.get(oldIdKey).BillingStreet);  
 
          	}
        
            for ( ID newIdKey : newMap.keyset()) {
              System.debug('New List is Person Account :' + newMap.get(newIdKey).isPersonAccount);
                
              if (newMap.get(newIdKey).isPersonAccount==TRUE)
  	          	System.debug('newMap Values ' + newMap.get(newIdKey).PersonOtherStreet);
              else
                System.debug('newMap Values ' + newMap.get(newIdKey).BillingStreet);

          	}  
        
        	System.debug('Old Map: ' + oldMap);
            System.debug('New Map: ' + newMap);
            System.debug('Old List:' + oldList);
            System.debug('New List:' + newList);
    }    

    /**
     * 
	 *	Initiate Individual Account for Australia
	 *  Both Residential and Mailing addresses populated
	 *
	**/

	private static void initAUAccountMap () {
        // prepare Person Accounts for testing       
        Account accnt = SegmentationDataGenAddressTest.genFullMemberWithAddressAU('Turner', 'Tax Manager', 'brown1@test.co.au', Date.valueOf('1977-05-12') );
       	insert accnt;
        List <Account> accounts = [SELECT Id, Name, isPersonAccount, Membership_Type__c,
                                   PersonOtherCountry, PersonOtherStreet, PersonOtherCity, PersonOtherPostalCode, PersonOtherStateCode,
                                   PersonMailingCountry, PersonMailingStreet, PersonMailingCity, PersonMailingPostalCode, PersonMailingStateCode
                                   FROM Account 
                                   WHERE Id = :accnt.Id LIMIT 1]; 
        
        // prepare old and new lists and maps
        oldMap = new Map<ID, Account>{} ;
        for (Account acct : accounts)
            oldMap.put(acct.ID, acct);      	
        newMap = oldMap.deepClone();
	}

    /**
     * 
	 *	Initiate Individual Account for New Zealand
	 *  Both Residential and Mailing addresses populated
	 *
	**/
    private static void initNZAccountMap () {
        // prepare Person Accounts for testing    
        Account accnt = SegmentationDataGenAddressTest.genFullMemberNZAuckland('Smith', 'Senior Accountant', 'smith1@test.co.nz', Date.valueOf('1987-05-12') );
       	insert accnt;
        List <Account> accounts = [SELECT Id, Name, isPersonAccount, Membership_Type__c,
                                   PersonOtherCountry, PersonOtherStreet, PersonOtherCity, PersonOtherPostalCode, PersonOtherStateCode,
                                   PersonMailingCountry, PersonMailingStreet, PersonMailingCity, PersonMailingPostalCode, PersonMailingStateCode
                                   FROM Account 
                                   WHERE Id = :accnt.Id LIMIT 1];  
        
        // prepare old and new lists and maps
        oldMap = new Map<ID, Account>{} ;
        for (Account acct : accounts)
            oldMap.put(acct.ID, acct);      	
        newMap = oldMap.deepClone();
	}
    
    /**
     * 
	 *	Initiate Business Account for Australia
	 *  Both Billing and Shipping addresses populated
	 *  Lattitude Longitude not populated
	 *
	**/    
    private static void initAUBusAccountMap () {
        // prepare Business Accounts for testing 
        Account accnt = SegmentationDataGenAddressTest.genPracticeAU( 'Ernst & Young', 'Chartered Accounting', 'Sydney', 25);
       	insert accnt;
        List <Account> accounts = [SELECT Id, Name, isPersonAccount, Membership_Type__c,
                                   BillingLatitude, BillingLongitude, BillingGeocodeAccuracy, ShippingLatitude, ShippingLongitude, ShippingGeocodeAccuracy,
                                   BillingCountry, BillingStreet, BillingCity, BillingPostalCode, BillingState,
                                   ShippingCountry, ShippingStreet, ShippingCity, ShippingPostalCode, ShippingState
                                   FROM Account
                                   WHERE isPersonAccount = false AND Id = :accnt.Id LIMIT 1]; 
        
        // prepare old and new lists and maps
        oldMap = new Map<ID, Account>{} ;
        for (Account acct : accounts)
            oldMap.put(acct.ID, acct);      	
        newMap = oldMap.deepClone();
    }
    
    /**
     * 
	 *	Initiate Business Account for New Zealand
	 *  Both Billing and Shipping addresses populated
	 * 	Lattitude Longitude not populated
	 *
	**/     
    private static void initNZBusAccountMap () {
        // prepare Business Accounts for testing 
        Account accnt = SegmentationDataGenAddressTest.genPracticeNewZealand( 'Ernst & Young', 'Chartered Accounting', 'Auckland', 25);
       	insert accnt;
        List <Account> accounts = [SELECT Id, Name, isPersonAccount, Membership_Type__c,
                                   BillingLatitude, BillingLongitude, BillingGeocodeAccuracy, ShippingLatitude, ShippingLongitude, ShippingGeocodeAccuracy,
                                   BillingCountry, BillingStreet, BillingCity, BillingPostalCode, BillingState,
                                   ShippingCountry, ShippingStreet, ShippingCity, ShippingPostalCode, ShippingState
                                   FROM Account
                                   WHERE isPersonAccount = false AND Id = :accnt.Id LIMIT 1]; 
        
        // prepare old and new lists and maps
        oldMap = new Map<ID, Account>{} ;
        for (Account acct : accounts)
            oldMap.put(acct.ID, acct);      	
        newMap = oldMap.deepClone();
    }

    /**
     * 
	 *	Initiate Business Account for Australia
	 *  Both Billing and Shipping addresses populated
	 * 	Lattitude Longitude populated
	 *
	**/    
     private static void initAUBusAccountMapLat () {
        // prepare Business Accounts for testing 
        Account accnt = SegmentationDataGenAddressTest.genPracticeAUwithLat( 'Ernst & Young', 'Chartered Accounting', 'Sydney', 25);
       	insert accnt;
        List <Account> accounts = [SELECT Id, Name, isPersonAccount, Membership_Type__c,
                                   BillingLatitude, BillingLongitude, BillingGeocodeAccuracy, ShippingLatitude, ShippingLongitude, ShippingGeocodeAccuracy,
                                   BillingCountry, BillingStreet, BillingCity, BillingPostalCode, BillingState,
                                   ShippingCountry, ShippingStreet, ShippingCity, ShippingPostalCode, ShippingState
                                   FROM Account
                                   WHERE isPersonAccount = false AND Id = :accnt.Id LIMIT 1]; 
        
        // prepare old and new lists and maps
        oldMap = new Map<ID, Account>{} ;
        for (Account acct : accounts)
            oldMap.put(acct.ID, acct);      	
        newMap = oldMap.deepClone();
    }
    
    /**
     * 
	 *	Change Address Scenarios for Individuals
	 *
	**/    
     private static void testIndScenario(String oldResidentialStreet, String oldMailingStreet, String newResidentialStreet, String newMailingStreet) 
    {
    		for ( ID newIdKey : newMap.keyset()) {
  	          newMap.get(newIdKey).PersonOtherStreet = newResidentialStreet;
          	}
        	for ( ID oldIdKey : oldMap.keyset()) {
  	          oldMap.get(oldIdKey).PersonOtherStreet = oldResidentialStreet;
          	}

        	for ( ID newIdKey : newMap.keyset()) {
  	          newMap.get(newIdKey).PersonMailingStreet = newMailingStreet;
          	}       
        	for ( ID oldIdKey : oldMap.keyset()) {
  	          oldMap.get(oldIdKey).PersonMailingStreet = oldMailingStreet;
          	}

        	newList = new List<Account>(newMap.values());
          	oldList = new List<Account>(oldMap.values());
        
        	if (debugFlag)
				debugMaps () ;
    }
    
    /**
     * 
	 *	Change Address Scenarios for Businesses
	 *
	**/      
     private static void testBusScenario(String oldBillingStreet, String oldShippingStreet, String newBillingStreet, String newShippingStreet) 
    {
    		for ( ID newIdKey : newMap.keyset()) {
  	          newMap.get(newIdKey).BillingStreet = newBillingStreet;
          	}
        	for ( ID oldIdKey : oldMap.keyset()) {
  	          oldMap.get(oldIdKey).BillingStreet = oldBillingStreet;
          	}

        	for ( ID newIdKey : newMap.keyset()) {
  	          newMap.get(newIdKey).ShippingStreet = newShippingStreet;
          	}       
        	for ( ID oldIdKey : oldMap.keyset()) {
  	          oldMap.get(oldIdKey).ShippingStreet = oldShippingStreet;
          	}

        	newList = new List<Account>(newMap.values());
          	oldList = new List<Account>(oldMap.values());
        
        	if (debugFlag)
				debugMaps () ;
    }
  
    
    ////////////////////////////////////// Tests /////////////////////////////////////
    

	/////////////////////////////////////////////////////////////////////////
    // 
	//	Utils
	//  
    /////////////////////////////////////////////////////////////////////////      

	public static testMethod void testStaticGeoAccuracy() {
        System.assert(SegmentationAddressTriggerHandler.getGeoAccuracy('Verified')  == 'Address') ;
        System.assert(SegmentationAddressTriggerHandler.getGeoAccuracy('PremisesPartial') == 'Block') ;
        System.assert(SegmentationAddressTriggerHandler.getGeoAccuracy('InteractionRequired') == 'Street') ;
 	    System.assert(SegmentationAddressTriggerHandler.getGeoAccuracy('Multiple') == 'Street') ;
        System.assert(SegmentationAddressTriggerHandler.getGeoAccuracy('StreetPartial') == 'Street') ;
        
        System.assert(String.isBlank(SegmentationAddressTriggerHandler.getGeoAccuracy('Partial'))) ;
        System.assert(String.isBlank(SegmentationAddressTriggerHandler.getGeoAccuracy(''))) ;
    }

   	public static testMethod void testStaticGeoConf() {
        System.assert(SegmentationAddressTriggerHandler.getConfLevel('Verified')  == 'High') ;
        System.assert(SegmentationAddressTriggerHandler.getConfLevel('PremisesPartial') == 'Med-High') ;
        System.assert(SegmentationAddressTriggerHandler.getConfLevel('InteractionRequired') == 'Med') ;
 	    System.assert(SegmentationAddressTriggerHandler.getConfLevel('Multiple') == 'Med') ;
        System.assert(SegmentationAddressTriggerHandler.getConfLevel('StreetPartial') == 'Med') ;
        
        System.assert(String.isBlank(SegmentationAddressTriggerHandler.getConfLevel('Partial'))) ;
        System.assert(String.isBlank(SegmentationAddressTriggerHandler.getConfLevel(''))) ;
    }

 
	/////////////////////////////////////////////////////////////////////////
    // 
	//	Change AU Residential Address Test for Individuals
	//  
    /////////////////////////////////////////////////////////////////////////      
 
    public static testMethod void testStaticAfterUpdateResidentialCase1() {
		
        Map<Id, String> results = new Map<Id, String>{};
        initAUAccountMap ();
        
        if (debugFlag)
            System.debug('Debugging Residential AU Address Change Case1');

        testIndScenario('100 George St', '' , '200 George St', '');
        results = SegmentationAddressTriggerHandler.handleSegmentsAfterUpdate(newList, newMap, oldList, oldMap) ;

        System.debug('TestOther1: ' + results);
        System.assert(results.size() > 0); 	
    }
    

	/////////////////////////////////////////////////////////////////////////
    // 
	//	Change AU Mailing Address Test for Individuals
	//  
    /////////////////////////////////////////////////////////////////////////      

    public static testMethod void testStaticAfterUpdateMailing1() {
		
        Map<Id, String> results = new Map<Id, String>{};
        initAUAccountMap ();
        
        if (debugFlag)
            System.debug('Debugging Mailing AU Address Change Case1');

        testIndScenario('', '100 George St' , '', '200 George St');
        results = SegmentationAddressTriggerHandler.handleSegmentsAfterUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('TestOther1: ' + results);
        System.assert(results.size() > 0); 	
    }

    
	/////////////////////////////////////////////////////////////////////////
    // 
	//	Change AU Residential Address Test for Individuals
	// 	Was not empty now all empty
	//  
    /////////////////////////////////////////////////////////////////////////      

    public static testMethod void testStaticAfterUpdateAllEmpty() {
		
        Map<Id, String> results = new Map<Id, String>{};
        initAUAccountMap ();
        
        if (debugFlag)
            System.debug('Debugging All Empty Addresses');

        testIndScenario('Was Not Empty', '' , '', '');
        results = SegmentationAddressTriggerHandler.handleSegmentsAfterUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('TestAllEmpty: ' + results);
        System.assert(results.size() > 0); 	
    }
    
    

	/////////////////////////////////////////////////////////////////////////
    // 
	//	Change AU Mailing Address Test for Individuals
	// 	Clear Existing Residential
	//  
    /////////////////////////////////////////////////////////////////////////      

    public static testMethod void testStaticAfterUpdateMailingCase2() {
		
        Map<Id, String> results = new Map<Id, String>{};
        initAUAccountMap ();
        
        if (debugFlag)
            System.debug('Debugging Mailing AU Address Change and Clear Residential Case2');

        testIndScenario('Was not empty', '100 George St' , '', '200 George St');
        results = SegmentationAddressTriggerHandler.handleSegmentsAfterUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('TestOther1: ' + results);
        System.assert(results.size() > 0); 	
    }
    
    
    /////////////////////////////////////////////////////////////////////////
    // 
	//	Change AU Billing Business Account
	// 	Lat Long was empty now populated
	//  
    /////////////////////////////////////////////////////////////////////////      

    public static testMethod void testStaticBillingAfterUpdateFuture() {

        Map<Id, String> results = new Map<Id, String>{};
        initAUBusAccountMap ();
        
        if (debugFlag) {
            System.debug('Debugging Future Billing Method');
        }
        ////////////////////////////////////// Testing Future Postal Changes /////////////////////////////////////
     
        Test.startTest();
        // SegmentationAddressTriggerHandlerTest.forceTriggerDuringTest = true;
		Test.setMock(HttpCalloutMock.class, new  CalloutSOAPHttpGenMock.SOAPHttpQASAUDataPlusRespSuccess()); 
        testBusScenario('100 George St', '' , '500 Pacific Hwy', '');
        results = SegmentationAddressTriggerHandler.handleSegmentsAfterUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('TestBillingFuture1: ' + results);	
 

        SegmentationAddressTriggerUpdate segUpdateJob = new SegmentationAddressTriggerUpdate(results);
        ID jobID = System.enqueueJob(segUpdateJob);
    	System.debug('LongLatUpdateJob Id:' + jobID); 
        
        Test.stopTest();	
            
        List <Account> accounts = [SELECT Id, Name, isPersonAccount, BillingLatitude, BillingLongitude, BillingGeocodeAccuracy, 
                                   		  ShippingLatitude, ShippingLongitude, ShippingGeocodeAccuracy
                                   FROM Account
                                   WHERE Id IN :oldMap.keySet() ];
        
        if (debugFlag) {
            System.debug('Latitude After ' + accounts[0].BillingLatitude );
        }
        for (Account acct : accounts) {
            System.assert(acct.BillingLatitude != null); 
        }
    }
    
    /////////////////////////////////////////////////////////////////////////
    // 
	//	Change AU Shipping Business Account
	// 	Lat Long was empty now populated
	//  
    /////////////////////////////////////////////////////////////////////////      

    public static testMethod void testStaticShippingAfterUpdateFuture() {

        Map<Id, String> results = new Map<Id, String>{};
        initAUBusAccountMap ();
        
        if (debugFlag) {
            System.debug('Debugging Future Billing Method');
        }
        ////////////////////////////////////// Testing Future Postal Changes /////////////////////////////////////
     
        Test.startTest();
        // SegmentationAddressTriggerHandlerTest.forceTriggerDuringTest = true;
		Test.setMock(HttpCalloutMock.class, new  CalloutSOAPHttpGenMock.SOAPHttpQASAUDataPlusRespSuccess()); 
        testBusScenario('', '100 George St' , '', '500 Pacific Hwy');
        results = SegmentationAddressTriggerHandler.handleSegmentsAfterUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('TestBillingFuture1: ' + results);	
        
        SegmentationAddressTriggerUpdate segUpdateJob = new SegmentationAddressTriggerUpdate(results);
        ID jobID = System.enqueueJob(segUpdateJob);
    	System.debug('LongLatUpdateJob Id:' + jobID); 
        
        Test.stopTest();	
            
        List <Account> accounts = [SELECT Id, Name, isPersonAccount, BillingLatitude, BillingLongitude, BillingGeocodeAccuracy, 
                                   		  ShippingLatitude, ShippingLongitude, ShippingGeocodeAccuracy
                                   FROM Account
                                   WHERE Id IN :oldMap.keySet() ];
        
        if (debugFlag) {
            System.debug('Latitude After ' + accounts);
        }
        for (Account acct : accounts) {
            System.assert(acct.ShippingLatitude != null); 
        }
    }
    
    /////////////////////////////////////////////////////////////////////////
    // 
	//	Change NZ Billing Business Account
	// 	Lat Long was empty now populated
	//  
    /////////////////////////////////////////////////////////////////////////      

     public static testMethod void testStaticNZBillingAfterUpdateFuture() {

        Map<Id, String> results = new Map<Id, String>{};
        initNZBusAccountMap ();
        
        if (debugFlag) {
            System.debug('Debugging Future Billing Method');
        }
        ////////////////////////////////////// Testing Future Postal Changes /////////////////////////////////////
     
        Test.startTest();
        // SegmentationAddressTriggerHandlerTest.forceTriggerDuringTest = true;
		Test.setMock(HttpCalloutMock.class, new  CalloutSOAPHttpGenMock.SOAPHttpQASDataPlusRespSuccess()); 
        testBusScenario('100 George St', '' , '150 Auckland Rd', '');
        results = SegmentationAddressTriggerHandler.handleSegmentsAfterUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('TestShippingNZFuture1: ' + results);	
        
        SegmentationAddressTriggerUpdate segUpdateJob = new SegmentationAddressTriggerUpdate(results);
        ID jobID = System.enqueueJob(segUpdateJob);
    	System.debug('LongLatUpdateJob Id:' + jobID); 
        
        Test.stopTest();	
            
        List <Account> accounts = [SELECT Id, Name, isPersonAccount, BillingLatitude, BillingLongitude, BillingGeocodeAccuracy, 
                                   		  ShippingLatitude, ShippingLongitude, ShippingGeocodeAccuracy
                                   FROM Account
                                   WHERE Id IN :oldMap.keySet() ];
        
        if (debugFlag) {
            System.debug('Latitude After ' + accounts);
        }
        for (Account acct : accounts) {
            System.assert(acct.BillingLatitude != null); 
        }
    }

    /////////////////////////////////////////////////////////////////////////
    // 
	//	Change NZ Shipping Business Account
	// 	Lat Long was empty now populated
	//  
    /////////////////////////////////////////////////////////////////////////      

    public static testMethod void testStaticNZShippingAfterUpdateFuture() {

        Map<Id, String> results = new Map<Id, String>{};
        initNZBusAccountMap ();
        
        if (debugFlag) {
            System.debug('Debugging Future Billing Method');
        }
        ////////////////////////////////////// Testing Future Postal Changes /////////////////////////////////////
     
        Test.startTest();
        // SegmentationAddressTriggerHandlerTest.forceTriggerDuringTest = true;
		Test.setMock(HttpCalloutMock.class, new  CalloutSOAPHttpGenMock.SOAPHttpQASDataPlusRespSuccess()); 
        testBusScenario('Not Empty', '100 George St' , '', '150 Auckland Rd');
        results = SegmentationAddressTriggerHandler.handleSegmentsAfterUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('TestShippingNZFuture1: ' + results);	
        
        SegmentationAddressTriggerUpdate segUpdateJob = new SegmentationAddressTriggerUpdate(results);
        ID jobID = System.enqueueJob(segUpdateJob);
    	System.debug('LongLatUpdateJob Id:' + jobID); 
        
        Test.stopTest();	
            
        List <Account> accounts = [SELECT Id, Name, isPersonAccount, BillingLatitude, BillingLongitude, BillingGeocodeAccuracy, 
                                   		  ShippingLatitude, ShippingLongitude, ShippingGeocodeAccuracy
                                   FROM Account
                                   WHERE Id IN :oldMap.keySet() ];
        
        if (debugFlag) {
            System.debug('Latitude After ' + accounts);
        }
        for (Account acct : accounts) {
            System.assert(acct.ShippingLatitude != null); 
        }
        for (Account acct : accounts) {
            System.assert(acct.BillingLatitude == null); 
        }
    }
   

    /////////////////////////////////////////////////////////////////////////
    // 
	//	Change Business Address to Address Not Matched
	// 	Should clear existing Lat Long
	//  
    /////////////////////////////////////////////////////////////////////////      

	public static testMethod void testUpdateBilNoMatch() {
		Map<Id, String> results = new Map<Id, String>{};
        initAUBusAccountMapLat  ();
        List <Account> acctns = [SELECT Id, Name, isPersonAccount, BillingLatitude, BillingLongitude, BillingGeocodeAccuracy, 
                                   		  ShippingLatitude, ShippingLongitude, ShippingGeocodeAccuracy
                                FROM Account
                                WHERE Id IN :oldMap.keySet() ];
        for (Account acctn : acctns) {
            System.assert(acctn.BillingLatitude != null); 
        }
         
        if (debugFlag) {
            System.debug('Debugging Future Billing No Match');
        }
        Test.startTest();
		Test.setMock(HttpCalloutMock.class, new  CalloutSOAPHttpGenMock.SOAPHttpQASResponseNoMatches()); 
        testBusScenario('100 George St', '' , '150 Auckland Rd', '');
        results = SegmentationAddressTriggerHandler.handleSegmentsAfterUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('TestBilling: ' + results);	
        
        SegmentationAddressTriggerUpdate segUpdateJob = new SegmentationAddressTriggerUpdate(results);
        ID jobID = System.enqueueJob(segUpdateJob);
    	System.debug('LongLatUpdateJob Id:' + jobID); 
        
        Test.stopTest();	
            
        List <Account> accounts = [SELECT Id, Name, isPersonAccount, BillingLatitude, BillingLongitude, BillingGeocodeAccuracy, 
                                   		  ShippingLatitude, ShippingLongitude, ShippingGeocodeAccuracy
                                   FROM Account
                                   WHERE Id IN :oldMap.keySet() ];
        
        if (debugFlag) {
            System.debug('Latitude After ' + accounts);
        }
        for (Account acct : accounts) {
            System.assert(acct.BillingLatitude == null); 
        }
    }
    
  
    /////////////////////////////////////////////////////////////////////////
    // 
	//	Change Business Shipping Address and generate Error
	// 	Should clear existing Lat Long
	//  
    /////////////////////////////////////////////////////////////////////////      

	public static testMethod void testUpdateShippingError() {
		Map<Id, String> results = new Map<Id, String>{};
        initAUBusAccountMapLat  ();
        List <Account> acctns = [SELECT Id, Name, isPersonAccount, BillingLatitude, BillingLongitude, BillingGeocodeAccuracy, 
                                   		  ShippingLatitude, ShippingLongitude, ShippingGeocodeAccuracy
                                FROM Account
                                WHERE Id IN :oldMap.keySet() ];
        for (Account acctn : acctns) {
            System.assert(acctn.ShippingLatitude != null); 
        }
         
        if (debugFlag) {
            System.debug('Debugging Future Shipping Address with Error');
        }
        Test.startTest();
		Test.setMock(HttpCalloutMock.class, new  CalloutSOAPHttpGenMock.SOAPHttpQASResponseFailure()); 
        testBusScenario('','100 George St', '', '150 Auckland Rd');
        results = SegmentationAddressTriggerHandler.handleSegmentsAfterUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('TestBilling: ' + results);	
        
        SegmentationAddressTriggerUpdate segUpdateJob = new SegmentationAddressTriggerUpdate(results);
        ID jobID = System.enqueueJob(segUpdateJob);
    	System.debug('LongLatUpdateJob Id:' + jobID); 
        
        Test.stopTest();	
            
        List <Account> accounts = [SELECT Id, Name, isPersonAccount, BillingLatitude, BillingLongitude, BillingGeocodeAccuracy, 
                                   		  ShippingLatitude, ShippingLongitude, ShippingGeocodeAccuracy
                                   FROM Account
                                   WHERE Id IN :oldMap.keySet() ];
        
        if (debugFlag) {
            System.debug('Latitude After ' + accounts);
        }
        for (Account acct : accounts) {
            System.assert(acct.ShippingLatitude == null); 
        }
    }
    
    
        
    
    /////////////////////////////////////////////////////////////////////////
    // 
	//	Change Residential Address AU
	// 	R01 Educational City Centre test
	//  Do not Use Queueable enqueueJob causes uncommited transactions error
	//  
    /////////////////////////////////////////////////////////////////////////      

     public static testMethod void testStaticAUCityCentreTriggerUpdate() {

        Map<Id, String> results = new Map<Id, String>{};
        initAUAccountMap ();
            
       if (debugFlag) {
            System.debug('Debugging Educational City Centre');
        }
        ////////////////////////////////////// Testing Future Postal Changes /////////////////////////////////////
        testIndScenario('100 George St', '' , '500 Pacific Hwy', '');
        results = SegmentationAddressTriggerHandler.handleSegmentsAfterUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('Educational City Centre: ' + results);
         
        Test.startTest();
		Test.setMock(HttpCalloutMock.class, new  CalloutSOAPHttpGenMock.SOAPHttpQASAUDataPlusRespSuccess());
        SegmentationAddressTriggerUpdate segUpdateJob = new SegmentationAddressTriggerUpdate(results);
        segUpdateJob.perform_call();
        
        Test.stopTest();	
            
         List <Account> accounts = [SELECT Id, Name, isPersonAccount, Membership_Type__c, Education_City_Centre__c,
                                   PersonOtherCountry, PersonOtherStreet, PersonOtherCity, PersonOtherPostalCode, PersonOtherStateCode,
                                   PersonMailingCountry, PersonMailingStreet, PersonMailingCity, PersonMailingPostalCode, PersonMailingStateCode
                                   FROM Account 
                                   WHERE Id IN :oldMap.keySet() ];
        
        if (debugFlag) {
            System.debug('Educational City Centre After ' + accounts);
        }
        for (Account acct : accounts) {
            System.assert(acct.Education_City_Centre__c == 'Melbourne'); 
            System.debug('City Centre:' + acct.Education_City_Centre__c); 
        }
    }
    


	/////////////////////////////////////////////////////////////////////////
    // 
	//	NZ Residential Address
	//  
    /////////////////////////////////////////////////////////////////////////      
    
     public static testMethod void testStaticNZResTriggerUpdate() {

        Map<Id, String> results = new Map<Id, String>{};
        initNZAccountMap ();
            
       if (debugFlag) {
            System.debug('Debugging Educational City Centre');
        }
        ////////////////////////////////////// Testing Future Postal Changes /////////////////////////////////////
        testIndScenario('100 George St', '' , '500 Pacific Hwy', '');
        results = SegmentationAddressTriggerHandler.handleSegmentsAfterUpdate(newList, newMap, oldList, oldMap) ; 
        System.debug('NZ Residential Address Change: ' + results);
         
        Test.startTest();
		Test.setMock(HttpCalloutMock.class, new  CalloutSOAPHttpGenMock.SOAPHttpQASDataPlusRespSuccess());
        SegmentationAddressTriggerUpdate segUpdateJob = new SegmentationAddressTriggerUpdate(results);
        segUpdateJob.perform_call();
        
        Test.stopTest();	
            
         List <Account> accounts = [SELECT Id, Name, isPersonAccount, Membership_Type__c, Education_City_Centre__c, Affiliated_Branch__c,
                                   NZ_Local_Govt_Territorial_Authority__c, NZ_Local_Govt_Regional_Council__c,
                                   PersonOtherCountry, PersonOtherStreet, PersonOtherCity, PersonOtherPostalCode, PersonOtherStateCode,
                                   PersonMailingCountry, PersonMailingStreet, PersonMailingCity, PersonMailingPostalCode, PersonMailingStateCode
                                   FROM Account 
                                   WHERE Id IN :oldMap.keySet() ];
        
        if (debugFlag) {
            System.debug('NZ Residential Address Change: ' + accounts);
        }
        for (Account acct : accounts) {
            System.assert(acct.Education_City_Centre__c == 'Westland');
            //System.assert(acct.Affiliated_Branch__c == 'Canterbury-Westland');
            System.assert(acct.NZ_Local_Govt_Regional_Council__c == 'West Coast Region');
            System.debug('City Centre:' + acct.Education_City_Centre__c); 
            System.debug('Affiliated Branch:' + acct.Affiliated_Branch__c); 
            System.debug('NZ Local Gov Regional Council :' + acct.NZ_Local_Govt_Regional_Council__c); 
        }
    }   
    
	/////////////////////////////////////////////////////////////////////////
    // 
	//	Membership Change
	//  
    /////////////////////////////////////////////////////////////////////////      
     
    
    // Case X Change Membership Type                    
	private static testMethod void newMemberCase(){ 
        Map<Id, Account> oldMap = new Map<Id, Account>{} ;
		Map<Id, Account> newMap = new Map<Id, Account>{} ;
     
        Account acct = new Account( 
			RecordTypeId = RecordTypeCache.getId('Account', 'Non_member')
            , Salutation = 'Mr'
            , FirstName = 'Robert'
            , LastName = 'Smith'
            , Preferred_Name__c = 'Bob'
            , Gender__c = 'Male'
            , PersonBirthdate = Date.valueOf('1987-05-12')
            , Membership_Type__c = 'Non Member'
            , Member_Of__c = 'ICAA'
            , Status__c = 'Active'
            , Job_Title__c = 'Senior Accountant'
            , PersonOtherPhone = '1234567'
			, PersonEmail = 'smith1@test.co.nz'     
            , Assessible_for_CAF_Program__c = false
            , PersonOtherCountry = 'Australia'
            , PersonOtherStreet = '1610 Pacific Hwy'
            , PersonOtherCity = 'Wahroonga'
            , PersonOtherState = 'New South Wales'
            //, PersonOtherStateCode = 'NSW'
            , PersonOtherPostalCode = '2076'
            , PersonMailingCountry = 'Australia'
            , Affiliated_Branch_Country__c = 'Australia'
            //, Affiliated_Branch__c = 'New South Wales'

        );
        insert acct;
        Account nonMemberAccountAU = [SELECT Id, Name, isPersonAccount, Membership_Type__c, Membership_Class__c, Status__c, PersonOtherCountry,
                                      PersonOtherStreet, PersonOtherCity, PersonOtherPostalCode, PersonOtherState
                                      FROM Account WHERE Id = :acct.Id]; 
          
        
		System.debug('Old:' + nonMemberAccountAU);
        Account newMemberAccountAU  = nonMemberAccountAU.clone(true, true, false, false);
        newMemberAccountAU.Membership_Type__c = 'Member';
        // prepare old and new lists and maps
        oldMap.put(nonMemberAccountAU.Id, nonMemberAccountAU);
		newMap.put(newMemberAccountAU.Id, newMemberAccountAU);
            
		List <Account> newList = new List<Account>(newMap.values());
		List <Account> oldList = new List<Account>(oldMap.values());

        Map <Id, String> results = SegmentationAddressTriggerHandler.handleSegmentsAfterMembershipChange(newList, newMap, oldList, oldMap) ; 
        System.debug('Test Address Trigger for Change Member Type: ' + results);
        System.assert(results.size() > 0);
       	Set<Id> keys = results.keySet();
        for (Id key : keys)
			//System.assertEquals('Case 1', results.get(key) );
        	System.debug(results.get(key) );
	}   

}