/***********************************************************************************************************************************************************************
Name: CaseTriggerHandler 
============================================================================================================================== 
Purpose: 
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR                 DATE          DETAIL     Description 
1.0        Mayukhman Pathak      10/07/2019    Created    
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/
public class CaseTriggerHandler extends TriggerHandler{
    //Context Variable Collections after Type Casting
    List<Case> newList = (List<Case>)Trigger.new;
    List<Case> oldList = (List<Case>)Trigger.old;
    Map<Id,Case> oldMap = (Map<Id,Case>)Trigger.oldMap;
    Map<Id,Case> newMap = (Map<Id,Case>)Trigger.newMap;      
    private static Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case').getRecordTypeId();  
    
    public CaseTriggerHandler(){     
    }
    
    public override void beforeInsert(){ 
        Set<Id> accoundIds = new Set<Id>();
        Map<Id,Id> personAccountContactId = new Map<Id,Id>();
        List<Entitlement> lstEntitlement = new List<Entitlement>([SELECT Id, Name, AccountId, Type, Status FROM Entitlement Where Name = 'CAANZ Member Support Entitlement' Limit 1]);
        
        for(Case cse : newList) {
            if(cse.ContactId == null && cse.AccountId != null)
                accoundIds.add(cse.AccountId);            
        }
        for(Account acc : [Select Id,PersonContactId From Account where Id IN: accoundIds]) {
            personAccountContactId.put(acc.Id,acc.PersonContactId);
        }
        for(Case cse : newList) {
            //Change the Portal Case record type to Case for MyCA case
            if(cse.Origin == 'MyCA' ||  cse.Origin == 'Old Community')
                cse.RecordTypeId = caseRecordTypeId;
            //Update First Classification time in Case based on Type/Subtype changes
            if(cse.Type != null){
                cse.First_Classification_Time__c = System.Now();
            }
            //Assign entitlement to new case
            if(cse.EntitlementId == null){
                if(lstEntitlement.size() > 0){
                    cse.EntitlementId = lstEntitlement[0].Id;
                }
            }
            //Assign ContactId if AccountId is filled only
            if(personAccountContactId.containsKey(cse.AccountId))
                cse.ContactId = personAccountContactId.get(cse.AccountId);
        }
    }
    
    public override void afterInsert(){  
        set<Id> updateCasesIds = new set<Id>();
        for(Case cse : newList) {              
            //Code for Privacy case creation
            if(cse.RecordTypeId == caseRecordTypeId){
                if(cse.Type == 'Complaints' && cse.Sub_Type__c != 'About CA ANZ' ){
                    updateCasesIds.add(cse.Id);
                }
                else if(cse.Privacy__c == True && (cse.Type != 'Complaints' || ( cse.Type == 'Complaints' && cse.Sub_Type__c == 'About CA ANZ' ) )){
                    updateCasesIds.add(cse.Id);
                }
            } 
        }
        if(updateCasesIds.size() > 0 )
            createChildPrivateCase(updateCasesIds);
    }
    public override void beforeUpdate(){ 
        Set<Id> accoundIds = new Set<Id>();
        Map<Id,Id> personAccountContactId = new Map<Id,Id>();
        for(Case cse : newList) {
            if(cse.AccountId != null)
                accoundIds.add(cse.AccountId);            
        }
        for(Account acc : [Select Id,PersonContactId From Account where Id IN: accoundIds]) {
            personAccountContactId.put(acc.Id,acc.PersonContactId);
        }
        List<Id> updateCases = new List<Id>();
        for(case cse: newMap.values()) {
            if(oldMap.containsKey(cse.Id) && cse.RecordTypeId == caseRecordTypeId){
                //Update First Classification time in Case based on Type/Subtype changes
                if(cse.Type != null && oldMap.get(cse.Id).Type == null && cse.First_Classification_Time__c == null){
                    cse.First_Classification_Time__c = System.Now();
                }
            }
            //Assign ContactId if AccountId is filled only
            if(personAccountContactId.containsKey(cse.AccountId))
                cse.ContactId = personAccountContactId.get(cse.AccountId);
            if(cse.AccountId == null && oldMap.get(cse.Id).AccountId != null)
                cse.ContactId = null;
        }     
    }
    public override void afterUpdate(){
        set<Id> updateCasesIds = new set<Id>();
        set<Id> deleteCasesIds = new set<Id>();
        for(case cse: newMap.values()) {
            //Update secure object related to case for Non-Complaint and About CA ANZ complaints
            if(oldMap.containsKey(cse.Id) && cse.RecordTypeId == caseRecordTypeId &&
               (cse.Type != oldMap.get(cse.Id).Type ||
                cse.Sub_Type__c != oldMap.get(cse.Id).Sub_Type__c ||
                cse.Privacy__c != oldMap.get(cse.Id).Privacy__c ||
                cse.AccountId != oldMap.get(cse.Id).AccountId)
              ){
                  if(cse.Privacy__c == False && (cse.Type != 'Complaints' ||  ( cse.Type == 'Complaints' && cse.Sub_Type__c == 'About CA ANZ' ) )){
                      deleteCasesIds.add(cse.Id);
                  }
                  else{
                      updateCasesIds.add(cse.Id);
                  }
              }            
        }
        if(updateCasesIds.size() > 0 )
            createChildPrivateCase(updateCasesIds);
        if(deleteCasesIds.size() > 0 )
            deleteChildPrivateCase(deleteCasesIds);
    }        
    /* Method to create private case based on Case Type and Sub-type */
    private void createChildPrivateCase(set<Id> newListCaseIds){   
        List<Secure_Case__c> lstPrivateCase = new List<Secure_Case__c>();     
        for(Case cse : [Select Id,AccountId,CaseNumber, (Select Id,Case__c,Account__c From Secure_Cases__r) From Case where Id IN: newListCaseIds]) {
            if(cse.Secure_Cases__r.size() == 0){
                Secure_Case__c secCase = new Secure_Case__c();
                secCase.Case__c = cse.id;
                secCase.Account__c = cse.AccountId;
                lstPrivateCase.add(secCase); 
            }
            else if(cse.Secure_Cases__r.size() == 1){
                Secure_Case__c secCase = cse.Secure_Cases__r[0];
                secCase.Account__c = cse.AccountId;
                lstPrivateCase.add(secCase);                 
            }
        }    
        if(lstPrivateCase.size() > 0 )
            upsert lstPrivateCase;
    }
    /* Method to delete private case based on Case Type and Sub-type */
    private void deleteChildPrivateCase(set<Id> deleteListCaseIds){   
        delete([SELECT Id from Secure_Case__c WHERE Case__c IN: deleteListCaseIds]) ;
    }
    /*
public override void afterDelete(){
}        
public override void afterUndelete(){
*/
}