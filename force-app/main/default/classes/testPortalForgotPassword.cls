@isTest
private class testPortalForgotPassword {
    static testMethod void unitTestPortalForgotPassword() {
        PortalForgotPassword pfp = new PortalForgotPassword();
        pfp.username = 'test@salesforce.com';    
        pfp.addPageMessage('test');
        pfp.getUserId(UserInfo.getUserName());
        EmailTemplate e = new EmailTemplate();
        e.Name = 'Communities Forgot Password Email HTML test';
        e.HtmlValue = 'username: ' + 'password: ' + '{!Receiving_User.FirstName}' + '{!Organization.Name}'; 
        e.DeveloperName = 'Communities_Forgot_Password_Email_HTML_2';
        //e.FolderId = '00l90000000b1xs'; // TODO: replace this
        e.FolderId = [select Id from Folder where Name = 'NZICA Templates' limit 1].Id;
        e.IsActive = True;
        e.Subject = 'Subject';
        e.TemplateType = 'Custom';
        Database.insert(e);
        pfp.replaceTemplateVariables('strUsername', 'strPassword');
        pfp.forgotPassword();
        pfp.register();
    }
}