/*
    Developer: WDCi (Lean)
    Date: 29/Jul/2016
    Task #: LCA-867 Luana implementation - Masterclass broadcast
    
    Change History:
    LCA-802 18/Aug/2016 - WDCi Lean: Combine opening and allocation broadcast to the same wizard
*/

public with sharing class luana_MasterclassBroadcastController {
        
    public boolean enabled {private set; public get;}
    public String buttonLocation {private set; public get;}
    
    public LuanaSMS__Course__c course {private set; public get;}
    
    private String targetCourseId {set; get;}
    private String targetActionType {set; get;} //LCA-802
    
    public boolean isOpening {private set; get;} //LCA-802
    public boolean isAllocation {private set; get;} //LCA-802
    
    public boolean hasError {private set; get;}
    
    public List<String> warningMessage {get; set;}
    
    public luana_MasterclassBroadcastController (ApexPages.StandardController controller) {
        
        enabled = false;
        isOpening = false;
        isAllocation = false;
        hasError = false;
        
        targetCourseId = getCourseIdFromUrl();
        
        buttonLocation = 'bottom';
        targetActionType = getActionTypeFromUrl(); //LCA-802
        
        if(getCourseIdFromUrl() != null && targetActionType != null && (targetActionType == luana_MasterclassBroadcastAsync.TYPE_OPENING || targetActionType == luana_MasterclassBroadcastAsync.TYPE_ALLOCATION)){
            try{
                course = [select Id, Name from LuanaSMS__Course__c where Id =: targetCourseId];
                
                if(targetActionType == luana_MasterclassBroadcastAsync.TYPE_OPENING){ //LCA-802
                    isOpening = true;
                    isAllocation = false;
                    
                    enabled = true;
                } else {
                    isOpening = false;
                    isAllocation = true;
                    
                    enabled = validateData();
                }
                
                
            } catch(Exception exp){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Error retrieving course. Please try again later or contact your system administrator. Error: ' + exp.getMessage()));
            }
        } else {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Invalid course or action type. Please try again later or contact your system administrator.'));
        }
    }
    
    public boolean validateData(){
        warningMessage = new List<String>();
        
        Map<String, List<LuanaSMS__Student_Program__c>> notAttendanceMap = new Map<String, List<LuanaSMS__Student_Program__c>>();  
        List<LuanaSMS__Attendance2__c> validSPs = new List<LuanaSMS__Attendance2__c>();
        
        for(LuanaSMS__Student_Program__c sp: [select Id, Name, (select Name, Record_Type_Name__c, LuanaSMS__Student_Program__c, LuanaSMS__Session__r.LuanaSMS__Venue__c, LuanaSMS__Session__r.LuanaSMS__Room__c,LuanaSMS__Session__r.LuanaSMS__Venue__r.Name, LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress, LuanaSMS__Session__r.Name, LuanaSMS__Session__r.LuanaSMS__Start_Time__c, LuanaSMS__Session__r.LuanaSMS__End_Time__c from LuanaSMS__Attendances__r Where (Record_Type_Name__c =: luana_SessionConstants.RECORDTYPE_ATTENDANCE_CASM or Record_Type_Name__c =: luana_SessionConstants.RECORDTYPE_ATTENDANCE_VIRTUAL)) from LuanaSMS__Student_Program__c where LuanaSMS__Course__c =: targetCourseId and Masterclass_Broadcasted__c = false and LuanaSMS__Status__c = 'In Progress' and Paid__c = true]){
            if(sp.LuanaSMS__Attendances__r.isEmpty()){
                if(notAttendanceMap.containsKey('emptyAttendance')){
                    notAttendanceMap.get('emptyAttendance').add(sp); 
                }else{
                    List<LuanaSMS__Student_Program__c> newSPs = new List<LuanaSMS__Student_Program__c>();
                    newSPs.add(sp);
                    notAttendanceMap.put('emptyAttendance', newSPs);
                  
                }
            } else {
                for(LuanaSMS__Attendance2__c att: sp.LuanaSMS__Attendances__r){
                    validSPs.add(att);
                }
            }
        }
        
        Map<String, List<LuanaSMS__Attendance2__c>> invalidAttMap = new Map<String, List<LuanaSMS__Attendance2__c>>(); 
        String urlCode = String.valueof(URL.getSalesforceBaseUrl().toExternalForm());
        
        Map<Id, String> venueMap = new Map<Id, String>();
        Map<Id, String> sessionMap = new Map<Id, String>();
        for(LuanaSMS__Attendance2__c att: validSPs){   
            if(att.Record_Type_Name__c != luana_SessionConstants.RECORDTYPE_ATTENDANCE_VIRTUAL){  
                String billingAddress;
                if(att.LuanaSMS__Session__r.LuanaSMS__Venue__c != null && !String.isBlank(att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getStreet()) && 
                    !String.isBlank(att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getCity()) && 
                    !String.isBlank(att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getPostalCode()) &&
                    !String.isBlank(att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getCountry())){
                  
                    if(att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getCountry() != 'Australia'){
                        billingAddress = att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getStreet()+ ' ' +
                            att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getCity()+ ' ' +
                            att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getPostalCode()+ ' ' +
                            att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getCountry();
                    } else {
                        if(!String.isBlank(att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getState())){
                            billingAddress = att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getStreet()+ ' ' +
                                att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getCity()+ ' ' +
                                att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getPostalCode()+ ' ' +
                                att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getState()+ ' ' +
                                att.LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress.getCountry();
                        }
                    }
                }
              
                if(!venueMap.containsKey(att.LuanaSMS__Session__r.LuanaSMS__Venue__c) && att.LuanaSMS__Session__r.LuanaSMS__Venue__c != null){
                    venueMap.put(att.LuanaSMS__Session__r.LuanaSMS__Venue__c, att.LuanaSMS__Session__r.LuanaSMS__Venue__r.Name);
                    validateAttendanceRecord(att, 'emptyBillingAdd', billingAddress, invalidAttMap);
                }
              
                if(!sessionMap.containsKey(att.LuanaSMS__Session__c) && att.LuanaSMS__Session__c != null){
                    sessionMap.put(att.LuanaSMS__Session__c, att.LuanaSMS__Session__r.Name);
                    validateAttendanceRecord(att, 'emptyVenue', att.LuanaSMS__Session__r.LuanaSMS__Venue__c, invalidAttMap);
                    validateAttendanceRecord(att, 'emptyStartTime', String.ValueOf(att.LuanaSMS__Session__r.LuanaSMS__Start_Time__c), invalidAttMap);
                    validateAttendanceRecord(att, 'emptyEndTime', String.ValueOf(att.LuanaSMS__Session__r.LuanaSMS__End_Time__c), invalidAttMap);
                    
                    //PAN:6222: Room is optional for CASM courses
                    //validateAttendanceRecord(att, 'emptyRoom', att.LuanaSMS__Session__r.LuanaSMS__Room__c, invalidAttMap);
                }
            }
        }
        
        if(notAttendanceMap.containsKey('emptyAttendance')){
            for(LuanaSMS__Student_Program__c failSP: notAttendanceMap.get('emptyAttendance')){
                warningMessage.add('Please create exam attendance for this Student Program record: <a href="'+urlCode+'/'+failSP.id+'"target="_blank">'+failSP.Name+'</a>');
            }
        }
      
        if(invalidAttMap.containsKey('emptyBillingAdd')){
            for(LuanaSMS__Attendance2__c failAtt: invalidAttMap.get('emptyBillingAdd')){
                warningMessage.add('Please create full address for this Venue record: <a href="'+urlCode+'/'+failAtt.LuanaSMS__Session__r.LuanaSMS__Venue__c+'" target="_blank">'+failAtt.LuanaSMS__Session__r.LuanaSMS__Venue__r.Name+'</a>');
            }
        }
  
        if(invalidAttMap.containsKey('emptyVenue')){
            for(LuanaSMS__Attendance2__c failAtt: invalidAttMap.get('emptyVenue')){
                warningMessage.add('Please provide Venue for this Session: <a href="'+urlCode+'/'+failAtt.LuanaSMS__Session__c+'" target="_blank">'+failAtt.LuanaSMS__Session__r.Name+'</a>');
            }
        }
        
        /*PAN:6222: Room is optional for CASM courses
        if(invalidAttMap.containsKey('emptyRoom')){
            for(LuanaSMS__Attendance2__c failAtt: invalidAttMap.get('emptyRoom')){
                warningMessage.add('Please provide Room for this Session: <a href="'+urlCode+'/'+failAtt.LuanaSMS__Session__c+'" target="_blank">'+failAtt.LuanaSMS__Session__r.Name+'</a>');
            }
        }
        */
        
        if(invalidAttMap.containsKey('emptyStartTime')){
            for(LuanaSMS__Attendance2__c failAtt: invalidAttMap.get('emptyStartTime')){
                warningMessage.add('Please provide Start Time for this Session: <a href="'+urlCode+'/'+failAtt.LuanaSMS__Session__c+'" target="_blank">'+failAtt.LuanaSMS__Session__r.Name+'</a>');
            }
        }
  
        if(invalidAttMap.containsKey('emptyEndTime')){
            for(LuanaSMS__Attendance2__c failAtt: invalidAttMap.get('emptyEndTime')){
                warningMessage.add('Please provide End Time for this Session: <a href="'+urlCode+'/'+failAtt.LuanaSMS__Session__c+'" target="_blank">'+failAtt.LuanaSMS__Session__r.Name+'</a>');
            }
        }
        
        if(invalidAttMap.isEmpty() && notAttendanceMap.isEmpty()){
            hasError = false;
        } else {
            hasError = true;
        }
        
        if(hasError){
            return false;
        } else {
            return true;
        }
    }
    
    public Map<String, List<LuanaSMS__Attendance2__c>> validateAttendanceRecord(LuanaSMS__Attendance2__c att, String typeOfError, String frieldToCheck, Map<String, List<LuanaSMS__Attendance2__c>> invalidAttMap){
        if(frieldToCheck == null){
            if(invalidAttMap.containsKey(typeOfError)){
                invalidAttMap.get(typeOfError).add(att);
            }else{
                List<LuanaSMS__Attendance2__c> newAtts = new List<LuanaSMS__Attendance2__c>();
                newAtts.add(att);
                invalidAttMap.put(typeOfError, newAtts);
            }
        }
        return invalidAttMap;
    }
    
    private String getCourseIdFromUrl(){
        if(ApexPages.currentPage().getParameters().containsKey('id')){
            return ApexPages.currentPage().getParameters().get('id');
            
        }
        
        return null;
    }
    
    //LCA-802
    private String getActionTypeFromUrl(){
        if(ApexPages.currentPage().getParameters().containsKey('actiontype')){
            return ApexPages.currentPage().getParameters().get('actiontype');
            
        }
        
        return null;
    }
    
    public PageReference doBroadcast(){
        
        try{
            Database.executeBatch(new luana_MasterclassBroadcastAsync(targetCourseId, targetActionType), 200);
            
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'The job has been submitted.'));
        
        } catch(Exception exp){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Error submitting job: ' + exp.getMessage()));
        }
        
        enabled = false;
        return null;
    }
}