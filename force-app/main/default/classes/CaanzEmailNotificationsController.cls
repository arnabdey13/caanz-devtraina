/*------------------------------------------------------------------------------------
Author:        Sumit Gupta
Company:       Salesforce
Description:   Email notifications component controller

History
Date            Author             Comments
--------------------------------------------------------------------------------------
16-05-2018     Sumit Gupta       	Initial Release
------------------------------------------------------------------------------------*/
public with sharing class CaanzEmailNotificationsController {

	private static List<String> memberFields = new List<String>{
		'PreferencesDisableFollowersEmail', 'PreferencesDisableLikeEmail', 
		'PreferencesDisableChangeCommentEmail', 'PreferencesDisProfPostCommentEmail', 
		'PreferencesDisableLaterCommentEmail', 'PreferencesDisableBookmarkEmail', 
		'PreferencesDisCommentAfterLikeEmail', 'PreferencesDisableMentionsPostEmail', 
		'PreferencesDisMentionsCommentEmail', 'PreferencesDisableItemFlaggedEmail', 
		'PreferencesDisableDirectMessageEmail'
	};

	@AuraEnabled
    public static String getNotificationData(Id contactId){
        return JSON.serialize(new NotificationData(getNetworkMember()));
    }

    @AuraEnabled
    public static String saveNotificationData(String notificationDataJSONString){
    	NotificationData notificationData = (NotificationData)JSON.deserialize(notificationDataJSONString, NotificationData.class);
        if(notificationData.member != null) {
    		notificationData.setFieldValues();
    		update notificationData.member; 
    	}
    	return JSON.serialize(notificationData);
    }

    private static NetworkMember getNetworkMember(){
    	List<NetworkMember> networkMembers = [
    		SELECT PreferencesDisableAllFeedsEmail, PreferencesDisableFollowersEmail,
    			PreferencesDisableLikeEmail, PreferencesDisableChangeCommentEmail,
    			PreferencesDisProfPostCommentEmail, PreferencesDisableLaterCommentEmail,
    			PreferencesDisableBookmarkEmail, PreferencesDisCommentAfterLikeEmail,
    			PreferencesDisableMentionsPostEmail, PreferencesDisMentionsCommentEmail,
    			PreferencesDisableItemFlaggedEmail, PreferencesDisableDirectMessageEmail
    		FROM NetworkMember WHERE MemberId =: UserInfo.getUserId() AND NetworkId =: Network.getNetworkId()
    	];
        return !networkMembers.isEmpty() ? networkMembers.get(0) : new NetworkMember();
    }

    public class NotificationData{
    	NetworkMember member;
    	List<String> memberValues;
        Boolean isDisabled;

    	public NotificationData(NetworkMember member){
    		this.member = member; memberValues = new List<String>();
    		for(String field : memberFields){
                Object value = member.get(field); if(value != null && !(Boolean)value) memberValues.add(field); 
    		}
            isDisabled = member.PreferencesDisableAllFeedsEmail;
    	}

    	public void setFieldValues(){
    		for(String field : memberFields){
    			member.put(field, (Object)(!memberValues.contains(field)));
    		}
    	}
    }
}