/**
* @author Andrew Kopec 
* @date 05/09/2017
*
* @description  Tests for CalloutSOAPServie
*
* Uses CalloutSOAPHttpGenMock
*/
@isTest
public without sharing class CalloutSOAPServiceTest {

    @isTest
    static void testCalloutSuccess() {
  	Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CalloutSOAPHttpGenMock.SOAPHttpQASResponseSuccess());
        HttpResponse res = CalloutSOAPService.executeHttpCallout('QAS_SOAP_Address_Service', 'http://www.qas.com/OnDemand-2011-03/DoSearch','{Sample Body}');
        System.debug(res);
        String contentType = res.getHeader('Content-Type');
        System.assert(contentType == 'application/xml');
        String actualValue = res.getBody();
		String expectedValue = '<?xml version="1.0" encoding="UTF-8"?> <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" > <soapenv:Body> <ond:QASearch xmlns:ond="http://www.qas.com/OnDemand-2011-03"> <ond:Engine Threshold="5" >Verification</ond:Engine> <ond:Country>AUG</ond:Country> <ond:Layout>AUGMeshBlock</ond:Layout> <ond:Search>33 Erskine St, Sydney NSW 2000</ond:Search> </ond:QASearch> </soapenv:Body> </soapenv:Envelope> ';
        Test.stopTest();
        System.assertEquals(actualValue, expectedValue);
        System.assertEquals(200, res.getStatusCode());    
    
    }

    @isTest
    static void testWrapCalloutSuccess() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new CalloutSOAPHttpGenMock.SOAPHttpQASResponseSuccess());
 		String actualValue = CalloutSOAPService.executeCallout('QAS_SOAP_Address_Service', 'http://www.qas.com/OnDemand-2011-03/DoSearch','{Sample Body}');
        System.debug(actualValue);
        String expectedValue = '<?xml version="1.0" encoding="UTF-8"?> <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" > <soapenv:Body> <ond:QASearch xmlns:ond="http://www.qas.com/OnDemand-2011-03"> <ond:Engine Threshold="5" >Verification</ond:Engine> <ond:Country>AUG</ond:Country> <ond:Layout>AUGMeshBlock</ond:Layout> <ond:Search>33 Erskine St, Sydney NSW 2000</ond:Search> </ond:QASearch> </soapenv:Body> </soapenv:Envelope> ';
        Test.stopTest();
        
        System.assertEquals(expectedValue, actualValue);
    }
    

    @isTest
    static void testCalloutError() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new  CalloutSOAPHttpGenMock.SOAPHttpQASResponseFailure());
        HttpResponse res = CalloutSOAPService.executeHttpCallout('QAS_SOAP_Address_Service', 'http://www.qas.com/OnDemand-2011-03/DoSearch','{Sample Body}');
        System.debug(res);
        String contentType = res.getHeader('Content-Type');
        System.assert(contentType == 'application/xml');
        String actualValue = res.getBody();
        System.assertNotEquals(200, res.getStatusCode());
        Test.stopTest();
    }

    @isTest
    static void testWrapCalloutError() {
		Test.startTest();
        Test.setMock(HttpCalloutMock.class, new  CalloutSOAPHttpGenMock.SOAPHttpQASResponseFailure());
        String actualValue = CalloutSOAPService.executeCallout('QAS_SOAP_Address_Service', 'http://www.qas.com/OnDemand-2011-03/DoSearch','{Sample Body}');
        System.debug(actualValue);
        String expectedValue = 'CalloutSOAPServiceError: NotLicensed : The user is not entitled to use New Zealand.';
        System.assertEquals(expectedValue, actualValue);
        Test.stopTest();
    }
       
     @isTest
    static void testCalloutException() {
		Test.startTest();
        Test.setMock(HttpCalloutMock.class, new  CalloutSOAPHttpGenMock.SOAPHttpQASCalloutFailure());
        HttpResponse res = CalloutSOAPService.executeHttpCallout('QAS_SOAP_Address_Service', 'http://www.qas.com/OnDemand-2011-03/DoSearch','{Sample Body}');
        System.debug(res);
        Test.stopTest();
        System.assert(res == null);
    }

     @isTest
    static void testWrapCalloutException() {
		Test.startTest();
        Test.setMock(HttpCalloutMock.class, new  CalloutSOAPHttpGenMock.SOAPHttpQASCalloutFailure());
 		String actualValue = CalloutSOAPService.executeCallout('QAS_SOAP_Address_Service', 'http://www.qas.com/OnDemand-2011-03/DoSearch','{Sample Body}');
        System.debug(actualValue);
        String expectedValue = 'CalloutSOAPServiceError: UNEXPECTED_EXCEPTION';
        Test.stopTest();
        System.assertEquals(expectedValue, actualValue);
    }
    
    // test incorrect config
    @isTest
    static void testNoConfig() {
		Test.startTest();        
        Test.setMock(HttpCalloutMock.class, new CalloutSOAPHttpGenMock.SOAPHttpQASResponseSuccess());
 		HttpResponse res = CalloutSOAPService.executeHttpCallout('XXX', 'http://www.qas.com/OnDemand-2011-03/DoSearch','{Sample Body}');
        System.debug(res);
        Test.stopTest();
        System.assert(res == null);
    }
    
    @isTest
    static void testNoAdmin() {
 		List<String> admin = CalloutSOAPService.getAdminEmails(new List<User>() );
        System.assert(admin.isEmpty());
    }
    
}