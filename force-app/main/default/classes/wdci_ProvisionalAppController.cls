/*
 * @author          WDCi (LKoh)
 * @date            30-May-2019
 * @description     Apex support class for the wdciProvisionalApp Lightning Web Components
 * @changehistory   Update 20190819 (Edy - #PAN6549) - add with sharing
 * @changehistory   #FIX002 - WDCi-LKoh - 04/09/2019 - Fixed the issue with Subject Paper selection
 * @changehistory   #UAT001 - WDCi-LKoh - 09/09/2019 - Added new configurable Text for subject paper on top and bottom (ff_RichTextStores)
 */
public with sharing class wdci_ProvisionalAppController {

    // == wdciProvisionalAppEQual methods == 

    @AuraEnabled (cacheable = true)
    public static string grabEQual(Id educationHistoryId) {

        List<edu_Education_History__c> getFullEducationHistoryRecord = [SELECT Id, FT_My_eQuals__c FROM edu_Education_History__c WHERE Id = :educationHistoryId];

        String eQualString = '';
        if (getFullEducationHistoryRecord.size() > 0 && !String.isBlank(getFullEducationHistoryRecord[0].FT_My_eQuals__c)) {
            eQualString = getFullEducationHistoryRecord[0].FT_My_eQuals__c;
        }
        return eQualString;
    }

    @AuraEnabled
    public static string saveEQual(Id educationHistoryId, String urlString) {

        String responseString = '';

        system.debug('saveEQual called for: ' +educationHistoryId+ ': ' +urlString);
        try {
            edu_Education_History__c eduHistoryToUpdate = new edu_Education_History__c();
            eduHistoryToUpdate.Id = educationHistoryId;
            eduHistoryToUpdate.FT_My_eQuals__c = urlString;
            update eduHistoryToUpdate;
            responseString = 'My eQuals link saved';
        } catch (Exception ex) {
            String exceptionMsg = ex.getMessage();
            exceptionMsg += ', StackTrace: ' +ex.getStackTraceString();
            responseString = 'ERROR: ' +exceptionMsg;
        }
        return responseString;
    }

    // == end of wdciProvisionalAppEQual methods ==

    // == wdciProvisionalAppStudentPaper methods == 

    @AuraEnabled (cacheable = true)
    public static string grabAdditionalRemark(Id educationHistoryId) {
        String educationHistoryRemarkNote = '';
        List<edu_Education_History__c> getFullEducationHistoryRecord = [SELECT Id, FT_Additional_Remark__c FROM edu_Education_History__c WHERE Id = :educationHistoryId];
        if (getFullEducationHistoryRecord.size() > 0) {
            educationHistoryRemarkNote = getFullEducationHistoryRecord[0].FT_Additional_Remark__c;
        }
        return educationHistoryRemarkNote;
    }

    /** OBSOLETE - #UAT001
    @AuraEnabled (cacheable = true)
    public static string grabRemarkLabel() {
        String remarkLabel = '';
        
        Schema.SObjectType applicationObjectType = Schema.getGlobalDescribe().get('edu_Education_History__c');
        Schema.DescribeSObjectResult applicationObjectDescribe = applicationObjectType.getDescribe();        
        Map<string, Schema.SObjectField> applicationFieldMap = applicationObjectDescribe.fields.getMap();
        remarkLabel = applicationFieldMap.get('FT_Additional_Remark__c'.toLowerCase()).getDescribe().getInlineHelpText();

        return remarkLabel;
    }
    */

    // #UAT001
    // Description text on top (below Subject History)
    @AuraEnabled (cacheable = true)
    public static string grabTopDescription() {
        String topDescription = '';
        List<ff_RichTextStore__c> richTextStoreList = [SELECT Display_Content__c FROM ff_RichTextStore__c WHERE Name__c = 'StudentPaper.TopDescription'];
        if (richTextStoreList.size() > 0) topDescription = richTextStoreList[0].Display_Content__c;
        return topDescription;
    }

    // #UAT001
    // Description text on bottom (below Additional transcript information)
    @AuraEnabled (cacheable = true)
    public static string grabBottomDescription() {
        String bottomDescription = '';
        List<ff_RichTextStore__c> richTextStoreList = [SELECT Display_Content__c FROM ff_RichTextStore__c WHERE Name__c = 'StudentPaper.BottomDescription'];
        if (richTextStoreList.size() > 0) bottomDescription = richTextStoreList[0].Display_Content__c;
        return bottomDescription;        
    }

    @AuraEnabled
    public static list<wdci_WrapperClass> grabUniSubjectListMk3(Id educationHistoryId) {

        List<FT_University_Subject__c> uniSubjectList = new List<FT_University_Subject__c>();

        List<edu_Education_History__c> getFullEducationHistoryRecord = [SELECT Id, FP_Year_of_Commence__c, FP_Year_of_Finish__c, University_Degree__r.Degree__c, University_Degree__r.University__c FROM edu_Education_History__c WHERE Id = :educationHistoryId];
        if (getFullEducationHistoryRecord.size() > 0) {
            edu_Education_History__c currentEducationHistory = getFullEducationHistoryRecord[0];
                        
            List<FT_University_Degree_Subject__c> uniDegreeSubjectList = [SELECT Id, FT_University_Subject__c FROM FT_University_Degree_Subject__c
                                                                          WHERE FT_University_Degree_Join__r.University__c = :currentEducationHistory.University_Degree__r.University__c 
                                                                          AND FT_University_Degree_Join__r.Degree__c = :currentEducationHistory.University_Degree__r.Degree__c
                                                                          AND FT_Year__c >= :currentEducationHistory.FP_Year_of_Commence__c
                                                                          AND FT_Year__c <= :currentEducationHistory.FP_Year_of_Finish__c];
            

            Set<Id> uniSubjectIdSet = new Set<Id>();
            for (FT_University_Degree_Subject__c uds : uniDegreeSubjectList) {
                uniSubjectIdSet.add(uds.FT_University_Subject__c);
            }
            uniSubjectList = [SELECT Id, Name, FT_University_Subject_Name__c, FT_Subject_Code__c FROM FT_University_Subject__c WHERE Id IN :uniSubjectIdSet ORDER BY NAME ASC];
        }

        Map<Id, FT_Student_Paper__c> existingStudentPaperMap = new Map<Id, FT_Student_Paper__c>();
        List<FT_Student_Paper__c> existingStudentPaper = [SELECT Id, FT_University_Subject__c FROM FT_Student_Paper__c WHERE FT_Education_History__c = :educationHistoryId];
        for (FT_Student_Paper__c sp : existingStudentPaper) {
            existingStudentPaperMap.put(sp.FT_University_Subject__c, sp);
        }
        
        List<wdci_WrapperClass> studentPaperMapList = new List<wdci_WrapperClass>();
        
        Luana_Extension_Settings__c ftEnablementSetting = Luana_Extension_Settings__c.getValues('FastTrack_Enablement');
        if(ftEnablementSetting != null && String.isNotBlank(ftEnablementSetting.Value__c) && ftEnablementSetting.Value__c == 'false'){
            return studentPaperMapList;
        }

        for (FT_University_Subject__c us : uniSubjectList) {            
            if (existingStudentPaperMap.containsKey(us.Id)) {
                // Student Paper for this Uni Subject already exist for the student
                wdci_WrapperClass newWrapper = new wdci_WrapperClass(true, us);
                studentPaperMapList.add(newWrapper);
            } else {
                // No Student Paper for this Uni Subject exist yet for the student
                wdci_WrapperClass newWrapper = new wdci_WrapperClass(false, us);                
                studentPaperMapList.add(newWrapper);
            }            
        }
        system.debug('studentPaperMapList: ' +studentPaperMapList);
        return studentPaperMapList;
    }

    @AuraEnabled
    public static string saveUniSubjectList(Id educationHistoryId, List<wdci_WrapperClass> subjectMapList, String internalNoteValue) {        

        String responseString = '';

        try {
            // Map out the existing Subject Paper for the Education History
            Map<Id, FT_Student_Paper__c> existingStudentPaperMap = new Map<Id, FT_Student_Paper__c>();
            List<FT_Student_Paper__c> existingStudentPaper = [SELECT Id, FT_Uni_Subject_Code__c, FT_University_Subject__c FROM FT_Student_Paper__c WHERE FT_Education_History__c = :educationHistoryId];
            for (FT_Student_Paper__c sp : existingStudentPaper) {
                existingStudentPaperMap.put(sp.FT_University_Subject__c, sp);   // #FIX002 - changed from the Uni Subject Code as the key to Uni Subject Id
            }

            List<FT_Student_Paper__c> studentPaperToCreate = new List<FT_Student_Paper__c>();
            List<FT_Student_Paper__c> studentPaperToDelete = new List<FT_Student_Paper__c>();

            for (wdci_WrapperClass wrapper : subjectMapList) {
                
                system.debug('wrapper: ' +wrapper);            
                if (wrapper.selected) {
                    if (existingStudentPaperMap.containsKey(wrapper.uniSubjectId)) {    // #FIX002
                        // do nothing
                    } else {
                        // add new Subject Paper
                        FT_Student_Paper__c newStudentPaper = new FT_Student_Paper__c();
                        newStudentPaper.FT_Education_History__c = educationHistoryId;
                        newStudentPaper.FT_University_Subject__c = wrapper.uniSubjectId;
                        newStudentPaper.FT_Passed_Credited__c = true;
                        studentPaperToCreate.add(newStudentPaper);
                    }
                } else {                
                    if (existingStudentPaperMap.containsKey(wrapper.uniSubjectId)) {    // #FIX002
                        // delete this Subject Paper                    
                        studentPaperToDelete.add(existingStudentPaperMap.get(wrapper.uniSubjectId));
                    } else {
                        // do nothing
                    }
                }
            }            

            // Update or Delete the Student Paper
            if (studentpaperToCreate.size() > 0) {
                system.debug('studentpaperToCreate: ' +studentpaperToCreate);
                insert studentpaperToCreate;
            }

            if (studentPaperToDelete.size() > 0) {
                system.debug('studentPaperToDelete: ' +studentPaperToDelete);
                delete studentPaperToDelete;
            }

            // Update the Note value for Education History
            edu_Education_History__c eduHistoryToUpdate = new edu_Education_History__c();
            eduHistoryToUpdate.FT_Additional_Remark__c = internalNoteValue;
            eduHistoryToUpdate.Id = educationHistoryId;
            update eduHistoryToUpdate;

            responseString = 'Subject History Saved';
        } catch (Exception ex) {
            String exceptionMsg = ex.getMessage();
            exceptionMsg += ', StackTrace: ' +ex.getStackTraceString();
            responseString = 'ERROR: ' +exceptionMsg;
        }        
        return responseString;
    }    

    // Valid Uni Subject to display are Uni Subject related to the University and Degree indicated by the Education History
    // Note: only subjects offerred during the Education History period should be shown
    // ## OBSOLETE ## - replaced by grabUniSubjectListMk3
    /*
    @AuraEnabled (cacheable = true)
    public static List<FT_University_Subject__c> grabUniSubjectList(Id educationHistoryId) { 

        system.debug('grabUniSubjectList called with educationHistoryId: ' +educationHistoryId);

        List<FT_University_Subject__c> uniSubjectList = new List<FT_University_Subject__c>();

        List<edu_Education_History__c> getFullEducationHistoryRecord = [SELECT Id, FP_Year_of_Commence__c, FP_Year_of_Finish__c, University_Degree__r.Degree__c, University_Degree__r.University__c FROM edu_Education_History__c WHERE Id = :educationHistoryId];
        if (getFullEducationHistoryRecord.size() > 0) {
            edu_Education_History__c currentEducationHistory = getFullEducationHistoryRecord[0];
            
            List<FT_University_Degree_Subject__c> uniDegreeSubjectList = [SELECT Id, FT_University_Subject__c FROM FT_University_Degree_Subject__c 
                                                                            WHERE FT_University_Degree_Year__r.FT_University_Degree_Join__r.University__c = :currentEducationHistory.University_Degree__r.University__c 
                                                                            AND FT_University_Degree_Year__r.FT_University_Degree_Join__r.Degree__c = :currentEducationHistory.University_Degree__r.Degree__c
                                                                            AND FT_University_Degree_Year__r.FT_Degree_Year__r.Name >= :currentEducationHistory.FP_Year_of_Commence__c
                                                                            AND FT_University_Degree_Year__r.FT_Degree_Year__r.Name <= :currentEducationHistory.FP_Year_of_Finish__c];

            Set<Id> uniSubjectIdSet = new Set<Id>();
            for (FT_University_Degree_Subject__c uds : uniDegreeSubjectList) {
                uniSubjectIdSet.add(uds.FT_University_Subject__c);
            }
            uniSubjectList = [SELECT Id, Name, FT_Subject_Code__c FROM FT_University_Subject__c WHERE Id IN :uniSubjectIdSet];
        }
        return uniSubjectList;        
    }

    // Map version of grabUniSubjectList
    @AuraEnabled (cacheable = true)
    public static List<Map<String, Object>> grabUniSubjectListMk2(Id educationHistoryId) { 

        system.debug('grabUniSubjectListMk2 called with educationHistoryId: ' +educationHistoryId);

        List<FT_University_Subject__c> uniSubjectList = new List<FT_University_Subject__c>();

        List<edu_Education_History__c> getFullEducationHistoryRecord = [SELECT Id, FP_Year_of_Commence__c, FP_Year_of_Finish__c, University_Degree__r.Degree__c, University_Degree__r.University__c FROM edu_Education_History__c WHERE Id = :educationHistoryId];
        if (getFullEducationHistoryRecord.size() > 0) {
            edu_Education_History__c currentEducationHistory = getFullEducationHistoryRecord[0];
            
            List<FT_University_Degree_Subject__c> uniDegreeSubjectList = [SELECT Id, FT_University_Subject__c FROM FT_University_Degree_Subject__c 
                                                                            WHERE FT_University_Degree_Year__r.FT_University_Degree_Join__r.University__c = :currentEducationHistory.University_Degree__r.University__c 
                                                                            AND FT_University_Degree_Year__r.FT_University_Degree_Join__r.Degree__c = :currentEducationHistory.University_Degree__r.Degree__c
                                                                            AND FT_University_Degree_Year__r.FT_Degree_Year__r.Name >= :currentEducationHistory.FP_Year_of_Commence__c
                                                                            AND FT_University_Degree_Year__r.FT_Degree_Year__r.Name <= :currentEducationHistory.FP_Year_of_Finish__c];

            Set<Id> uniSubjectIdSet = new Set<Id>();
            for (FT_University_Degree_Subject__c uds : uniDegreeSubjectList) {
                uniSubjectIdSet.add(uds.FT_University_Subject__c);
            }
            uniSubjectList = [SELECT Id, Name, FT_Subject_Code__c FROM FT_University_Subject__c WHERE Id IN :uniSubjectIdSet];
        }

        Map<Id, FT_Student_Paper__c> existingStudentPaperMap = new Map<Id, FT_Student_Paper__c>();
        List<FT_Student_Paper__c> existingStudentPaper = [SELECT Id, FT_University_Subject__c FROM FT_Student_Paper__c WHERE FT_Education_History__c = :educationHistoryId];
        for (FT_Student_Paper__c sp : existingStudentPaper) {
            existingStudentPaperMap.put(sp.FT_University_Subject__c, sp);
        }
        
        List<Map<String, Object>> studentPaperMapList = new List<Map<String, Object>>();
        for (FT_University_Subject__c us : uniSubjectList) {            
            if (existingStudentPaperMap.containsKey(us.Id)) {
                // Student Paper for this Uni Subject already exist for the student
                Map<String, Object> wrapp = new Map<String, Object> {
                    'selected' => true,
                    'uniSubject' => us
                };
                studentPaperMapList.add(wrapp);
            } else {
                // No Student Paper for this Uni Subject exist yet for the student
                Map<String, Object> wrapp = new Map<String, Object> {
                    'selected' => false,
                    'uniSubject' => us
                };
                studentPaperMapList.add(wrapp);
            }            
        }
        system.debug('studentPaperMapList: ' +studentPaperMapList);
        return studentPaperMapList;
    }

    // Returns related Student Paper for a given Education History
    @AuraEnabled
    public static List<FT_Student_Paper__c> grabStudentPaper(Id educationHistoryId) {

        List<FT_Student_Paper__c> studentPaperList = new List<FT_Student_Paper__c>();
        studentPaperList = [SELECT Id, FT_Education_History__c, FT_Passed_Credited__c FROM FT_Student_Paper__c WHERE FT_Education_History__c = :educationHistoryId];
        return studentPaperList;
    }        
    */
    // == end of wdciProvisionalAppStudentPaper methods == 
}