public with sharing class QPRQuestionnaireOverride {
    // Global variables
    public String strDebug {get;set;}
    public Boolean bShowHeader {get;set;}       
    public static String endpoint = 'https://charteredaccountantsanz.tfaforms.net/rest';    
    public Boolean bIsPortalUser {get;set;}
    public Boolean bIsView {get;set;}
    public Boolean bIsEditable {get;set;}
    public String strURL {get;set;}
    public QPR_Questionnaire__c qpr_questionnaire {get;set;}
    public User currentUser {get;set;}
    public Contact currentUserContact {get;set;}
    public String strRecordTypeParam {get; set;} // Record Type parameter
    public String strRecordTypeParamId {get; set;} // Record Type ID parameter
    public String strApplicationIDParam {get;set;} // Application ID parameter
    public String strStatus {get;set;} // Status. Duh.
    public String strReviewNumber {get;set;} //
    
    public String resBody {get;set;} // form body
    public String strJsonApplicationAttachments {get;set;} //A JSON string of application attachments
    
    // Internal variables
    private String strFirmId = '';
    private String strApplicantId = ''; 
    private String strSurveyId = '';  
    private String strMemberOf = '';
    private String strFormId = '';
    private String strSecurityToken = '';
    private String strParameters = '';
    private String strParametersForSig = '';
    private String strContext = '';
    private String strReviewId = '';
    private String strFormVersion='';
    private String readonlyForReviewer='';
    private RecordType rt;
    
    private Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
    // Constructor
    public QPRQuestionnaireOverride(ApexPages.standardController con) {
        Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=11');  
        Map<String, String> mapPageParameters = ApexPages.currentPage().getParameters();        
        strRecordTypeParam = (mapPageParameters.get('RecordType') != null)?
            (String) mapPageParameters.get('RecordType'):null;
        strRecordTypeParamId = (mapPageParameters.get('RecordTypeId') != null)?
            (String) mapPageParameters.get('RecordTypeId'):null;   
        strFirmId = (mapPageParameters.get('FirmAccount') != null)?
            (String) mapPageParameters.get('FirmAccount'):null; 
        strSurveyId = (mapPageParameters.get('QPR_Questionnaire__c') != null)?
            (String) mapPageParameters.get('QPR_Questionnaire__c'):null; 
        strReviewId = (mapPageParameters.get('Review__c') != null)?
            (String) mapPageParameters.get('Review__c'):null;  
         strFormVersion = (mapPageParameters.get('FormVersion') != null)?
            (String) mapPageParameters.get('FormVersion'):null;
        readonlyForReviewer = (mapPageParameters.get('ReviewerVersion') != null)?
            (String) mapPageParameters.get('ReviewerVersion'):null;
         system.debug('@@@ Value of Reviewer version: ' + readonlyForReviewer);
        qpr_questionnaire = (QPR_Questionnaire__c) con.getRecord();
        system.debug('@@@ con: ' + con);
        system.debug('@@@ con.getId(): ' + con.getId());
        if(con.getId() != null) {
            //@todo remove Lock -- not needed.
            qpr_questionnaire = [select Id, RecordTypeId, Lock__c, Status__c, Review_Number__c from QPR_Questionnaire__c where Id = :con.getId() limit 1];
            system.debug('@@@ qpr_questionnaire: ' + qpr_questionnaire);
            strRecordTypeParam = qpr_questionnaire.RecordTypeId;
            strStatus = qpr_questionnaire.Status__c;
            strReviewNumber = qpr_questionnaire.Review_Number__c;
        }
        
        rt = [select Id, Name, DeveloperName from RecordType 
              where (Name = :strRecordTypeParam or Id = :strRecordTypeParamId or
                     Id = :strRecordTypeParam or Name = :strRecordTypeParamId)
              and SObjectType = 'QPR_Questionnaire__c'
              limit 1];      
        
        //@TODO -- changing from so that only editable on.  Also to do -- make this better
        //Draft, Questionnaire Sent, In-Progress, Further Info Requested
        if ((qpr_questionnaire.Status__c == 'Draft' ||
            qpr_questionnaire.Status__c == 'Questionnaire Sent' ||
            qpr_questionnaire.Status__c == 'In-Progress' ||
            qpr_questionnaire.Status__c == 'Further Info Requested') && readonlyForReviewer == null) {
                
                bIsEditable = true;
            } else {
                bIsEditable = false;
            }
        
        // Determine context: NEW / EDIT / VIEW / LOCKED
        String currentRequestURL = URL.getCurrentRequestUrl().toExternalForm();
        bIsView = false; // Default to EDIT
        strContext = 'VIEW';    
        if(qpr_questionnaire.Id == null) { // NEW
            bIsView = false;    
            strContext = 'NEW';
        }
        else if(qpr_questionnaire.Id != null && qpr_questionnaire.Lock__c) { // LOCKED
            bIsView = true;
            strContext = 'LOCKED';
        }
        else if((qpr_questionnaire.Id != null && currentRequestURL.indexOf('/e') != -1 && !qpr_questionnaire.Lock__c) ||
                (currentRequestURL.indexOf('mode=edit') != -1) ) { // EDIT
                    bIsView = false;
                    strContext = 'EDIT';
                }
        // Determine if page is viewed from COMMUNITIES PORTAL OR ANONYMOUSLY
        bIsPortalUser = IsCommunityProfileUser();
        bShowHeader = (bIsPortalUser)? true:false;
        
        // addPageMessage('bIsView: ' + bIsView);  
        system.debug('bIsView: ' + bIsView);                                                
    }
    // Application EDIT override
    public PageReference modeEdit() {
        PageReference pref = Page.QPRQuestionnaireOverride;
        pref.getParameters().put('id', qpr_questionnaire.Id);
        pref.getParameters().put('mode', 'edit');
        if(strFormVersion!='')
        {
            pref.getParameters().put('FormVersion', strFormVersion);
        }
        if(readonlyForReviewer!='')
        {
            pref.getParameters().put('ReviewerVersion', readonlyForReviewer);
        }
        //if it is not a portal user -- go to the old page
        if(!bIsPortalUser) pref.getParameters().put('nooverride', '1');
        // if QPR reviewer profile and the qpr record type is AUS then go the old way
        //Get the profile name first -- this assumes modeedit only called once per page.
        Profile p = [Select Name from Profile where Id =: userinfo.getProfileid()];
        String pname = p.name;       
        if (pname=='QPR Reviewer User' && rt.DeveloperName=='PQR_AUS'){
            pref.getParameters().put('nooverride', '1');
        }
        if(ApexPages.currentPage().getParameters().get('retURL') != null) 
            pref.getParameters().put('retURL', ApexPages.currentPage().getParameters().get('retURL'));
        pref.setRedirect(true);
        return pref;
    }
    // Generate Form Assembly URL
    public PageReference generateFormURL() {
        PageReference pref = null;
        system.debug('bIsPortalUser: ' + bIsPortalUser);        
        currentUser = [select Id, ContactId, AccountId from User where Id = :UserInfo.getUserId() limit 1];
        if(currentUser.ContactId != null) 
            currentUserContact = [select Id, AccountId, Account.Member_Of__c from Contact where Id = :currentUser.ContactId limit 1];   
        // INTERNAL USER
        if(!bIsPortalUser && UserInfo.getUserType() != 'Guest') {       
            if(strContext == 'NEW') {   // NEW          
                String strPrefix = globalDescribe.get('QPR_Questionnaire__c').getDescribe().getkeyprefix();
                pref = new PageReference('/' + strPrefix + '/e');   
            }                   
            if(strContext == 'EDIT' || (strContext == 'LOCKED' && ApexPages.currentPage().getParameters().get('mode') == 'edit'))  // EDIT
                pref = new PageReference('/' + qpr_questionnaire.Id + '/e');
            if(strContext == 'VIEW'|| (strContext == 'LOCKED' && ApexPages.currentPage().getParameters().get('mode') != 'edit')) // VIEW
                pref = new PageReference('/' + qpr_questionnaire.Id);
            Set<String> setExcludedParams = new Set<String> {'sfdc.override', 'save_new', 'nooverride'};                
                for(String s:ApexPages.currentPage().getParameters().keySet()) {
                    String val = ApexPages.currentPage().getParameters().get(s);
                    if(s != null && val != null && !setExcludedParams.contains(s)) {    
                        if(pref != null && pref.getParameters() != null) pref.getParameters().put(s, val);
                    }
                }           
            if(pref != null && pref.getParameters() != null)
                pref.getParameters().put('nooverride', '1');
            return pref;
        }   
        // Read configuration settings
        Map<String, Application_Form__c> mapApplicationFormSettings = Application_Form__c.getAll();
        if(mapApplicationFormSettings.size() == 0) {
            addErrorMessage('Custom settings not defined');
        }       
        if(currentUserContact != null && currentUserContact.AccountId != null && UserInfo.getUserType() != 'Guest') 
            strApplicantId = currentUserContact.AccountId; // Applicant Id
        if(UserInfo.getUserType() == 'Guest') {  
            Map<String, String> mapPageParameters = ApexPages.currentPage().getParameters();        
            strApplicantId = (mapPageParameters.get('AccountId') != null)?
                (String) mapPageParameters.get('AccountId'):null; // Applicant Id
        }
        if(currentUserContact != null && currentUserContact.AccountId != null && currentUserContact.Account.Member_Of__c != null) 
            strMemberOf = currentUserContact.Account.Member_Of__c;  // Member Of
        String employmentHistory = '';      
        if(currentUserContact != null && currentUserContact.AccountId != null) { // Employment History
            Integer i = 0;
            for(Employment_History__c emp:[select Id, Member__c from Employment_History__c 
                                           where Member__c = :currentUser.AccountId 
                                           order by Employee_Start_Date__c desc]) {
                                               employmentHistory += '&Employment_History__c' + i + '=' + emp.Id;
                                               i++;
                                           }
        }   
        system.debug('### strRecordTypeParam: ' + strRecordTypeParam);     
        system.debug('### strRecordTypeParamId: ' + strRecordTypeParamId);      
        
        
        for(String s:mapApplicationFormSettings.keySet()) {
            Application_Form__c setting = mapApplicationFormSettings.get(s);
            if(strFormVersion != null)
            {
                if(setting.ReadOnly__c == bIsView && setting.Record_Type__c == strFormVersion ) {
                strFormId = setting.Form_ID__c; // FORM ID
                strSecurityToken = setting.Security_Token__c;
                // addPageMessage('strFormId: ' + strFormId);   
                system.debug('### strFormId: ' + strFormId);    
                break;
            	}
            }
            else
            {
               if(setting.ReadOnly__c == bIsView && setting.Record_Type__c == rt.DeveloperName) {
                strFormId = setting.Form_ID__c; // FORM ID
                strSecurityToken = setting.Security_Token__c;
                // addPageMessage('strFormId: ' + strFormId);   
                system.debug('### strFormId: ' + strFormId);    
                break;
            }
          }
         
        }
        /** start of different version form **/
        
        /*** end of different version form **/
        // Determine Related List IDs 
        List<String> listRelationshipNames = new List<String>(); 
        listRelationshipNames = DetermineRelatedListClass.getRelatedList(strRecordTypeParam, 'QPR_Questionnaire__c');
        // Build Application Query
        if(qpr_questionnaire.Id != null) {
            String strQuery = 'select Id, RecordTypeId, Lock__c'; 
            if(listRelationshipNames.size()>0) { // if additional related list exists
                for(String s:listRelationshipNames) {
                    
                    if (s != 'OpenActivities' && s !='ActivityHistories') { 
                        strQuery += ', (select Id from ' + String.escapeSingleQuotes(s) + ' )';
                    }
                }
            }
            strQuery += ' from QPR_Questionnaire__c where Id = \'' + String.escapeSingleQuotes(qpr_questionnaire.Id) + '\' limit 1';
            system.debug('### strQuery: ' + strQuery);
            //addPageMessage(strQuery);
            qpr_questionnaire = Database.query(strQuery);
        }
        // Build parameters 
        strParameters = 'SessionId=' + UserInfo.getSessionId(); // Session ID
        
        if(qpr_questionnaire.Id != null) {
            strParameters += '&QPR_Questionnaire__c=' + qpr_questionnaire.Id; // Application ID   
            /*        
// Add related list parameters
if(listRelationshipNames.size()>0) { // if additional related list exists           
for(String s:listRelationshipNames) {
if(qpr_questionnaire.getSObjects(s) != null && qpr_questionnaire.getSObjects(s).size()>0) {
Integer i = 0;
for(sObject item:qpr_questionnaire.getSObjects(s)) {
strParameters += '&' + s + String.valueOf(i) + '=' + item.get('Id'); // ID of related list item
i++;
}
}                   
}
}
*/
        }
        strParameters += '&Review__c=' + strReviewId;        
        strParameters += '&r=' + String.valueOf(Math.random()); // randomizer
        strParameters += '&IsPortal=' + String.valueOf(bIsPortalUser); // portal user
        if(strFirmId != '') strParameters += '&FirmAccount=' + strFirmId; // Firm       
        if(strApplicantId != '') strParameters += '&Account=' + strApplicantId; // Applicant    
        if(strMemberOf != '') strParameters += '&MemberOf=' + strMemberOf;  // Member Of    
        if(employmentHistory != null && employmentHistory != '') strParameters += employmentHistory; // employment history  
        strParametersForSig = strParameters;
        if(rt != null && rt.Name != null) {
            //the signature needs to be generated based on a non-urlencoded string.
            strParameters += '&RecordType=' + EncodingUtil.urlEncode(rt.Name, 'UTF-8'); // Record Type Name
            strParametersForSig += '&RecordType=' + rt.Name;
        }
        if(strRecordTypeParam != null && strRecordTypeParam != '') {
            strParameters += '&RecordTypeId=' + EncodingUtil.urlEncode(strRecordTypeParam, 'UTF-8'); // Record Type Id
            strParametersForSig += '&RecordTypeId=' + strRecordTypeParam;
        }
        
        //Adding the security token.  See url : https://help.formassembly.com/help/508885-prefill-lookups-using-secure-parameters 
        //note that the security code will always have to be done last to ensure the data string is completed first
        System.debug('Security token: ' + strSecurityToken);
        if(strSecurityToken != '') { 
            String data = strParametersForSig.replace('&','').replace('=',''); // strip '&' and '=' from string.
            System.debug('Data : ' + data);
            
            Blob mac = Crypto.generateMac('HMacSHA256', Blob.valueOf(data), Blob.valueOf(strSecurityToken));
            System.debug('Mac : ' + mac);
            
            String sig = EncodingUtil.urlEncode(EncodingUtil.base64Encode(mac), 'UTF-8');
            System.debug('Sig : ' + sig);
            
            strParameters += '&signature=' +   sig;
            
        }
        system.debug('strFormId: ' + strFormId);    
        system.debug('strParameters: ' + strParameters);  
        //addPageMessage(strParameters);   
        // Build PageReference
        // Get a list of attachments.. this is then put into javascript and can be used to show in page.
        List<Attachment> lstApplicationAttachments = [SELECT id, name FROM Attachment where ParentId = :qpr_questionnaire.Id];
        strJsonApplicationAttachments = JSON.serializePretty(lstApplicationAttachments);
        
        
        if(strFormId != '') {
            callFormAssembly(strFormId, strParameters);     
        }
        else {
            addErrorMessage('QPR Questionnaire form not found. Please contact your administrator.');
            system.debug('###Error: Formassembly form not found. Check custom settings.');
        }
        return null;
    }
    // Determine if page is viewed from portal
    public static Boolean IsCommunityProfileUser() {
        Boolean bReturnValue = false;
        system.debug('User Type: ' + UserInfo.getUserType());
        // addPageMessage('User Type: ' + UserInfo.getUserType());
        if(UserInfo.getUserType() == 'CspLitePortal' || UserInfo.getUserType() == 'PowerCustomerSuccess')
            bReturnValue = true;
        /*
String strCurrentProfileId = UserInfo.getProfileId();
for(Profile p:[select Id, Name from Profile where UserLicense.LicenseDefinitionKey 
like '%Customer_Community%' limit :(Limits.getLimitQueryRows() - Limits.getQueryRows())]) {
if(p.Id == strCurrentProfileId) {
bReturnValue = true; 
break;
}
}
*/  
        return bReturnValue;    
    }
    // Execute web service call 
    public void callFormAssembly(String strFormID, String strRequestParameters) {
        
        HTTPResponse response;      
        String strURL;
        PageReference pref = ApexPages.currentPage();
        system.debug('pref: ' + pref); 
        HttpRequest request = new HttpRequest();
        request.setMethod('GET');
        request.setTimeout(60000); // timeout
        // Build URL
        system.debug('pref.getParameters ' + pref.getParameters().get('tfa_next')); 
        if(pref.getParameters().get('tfa_next') == null) {
            strURL = endpoint + '/forms/view/' + strFormID;
        }
        else {
            strURL = endpoint + pref.getParameters().get('tfa_next');
        } 
        
        // Add parameters
        if(strRequestParameters != null && strRequestParameters.length()>0) {
            strURL = strURL + '?' + strRequestParameters;
        } 
        system.debug('strURL: ' + strURL);  
        //addPageMessage('strURL: ' + strURL); 
        request.setEndpoint(strURL);
        Http http = new Http();
        try {         
            if(!Test.isRunningTest()) {      
                response = http.send(request); // Execute web service call 
                system.debug('== response body =='+response.getBody());
                resBody = response.getBody();
            }
        } 
        catch(System.CalloutException e) {
            //addPageMessage(e);
            system.debug('CalloutException: ' + e);
            addErrorMessage('ERROR:  ' + e);
        }               
    }   
    // Page Messages
    public void addPageMessage(ApexPages.severity severity, Object objMessage) {
        ApexPages.Message pMessage = new ApexPages.Message(severity, String.valueOf(objMessage));
        ApexPages.addMessage(pMessage);
    }
    public void addPageMessage(Object objMessage) {
        addPageMessage(ApexPages.severity.INFO, objMessage);
    }       
    public void addErrorMessage(Object objMessage) {
        addPageMessage(ApexPages.severity.ERROR, objMessage);
    }
    public pagereference doRedirect(){
      // String currUrl = Apexpages.currentPage().getParameters().get('currUrl');
       String currUrl =Apexpages.currentPage().getUrl();
        system.debug('currURL'+currUrl);
     //   return new Pagereference(currUrl+'&tfa_next=%2Fresponses%2Flast_success%26sid%3Debf0d82ce53bcf386bc63d3cc9a4b40d');
     return new Pagereference(currUrl+'&tabc');
    }
    //FormAssembly Redirection
    @AuraEnabled
    public static String getBaseUrl () {
        if (Network.getNetworkId() != null) {
            return [SELECT Id, UrlPathPrefix FROM Network WHERE Id = :Network.getNetworkId()].UrlPathPrefix;
        }
        return '';
    }
}