/*
    Created by: WDCi (KH)
    Created on: 22 Mar 2017
    Task: Trigger for Program offering tasks
            - 1. Update AU/NZ/INT Product's standard price from PricebookEntry to PO's AU/NZ/INT Tuition Fees field
*/
public with sharing class luana_ProgramOfferingExtension{
    
    public static void ProgramOfferingAfterAction(List<LuanaSMS__Program_Offering__c> poList, Map<Id, LuanaSMS__Program_Offering__c> poMap, List<LuanaSMS__Program_Offering__c> oldPOList, Map<Id, LuanaSMS__Program_Offering__c> oldPOMap, boolean isInsert, boolean isUpdate, boolean isDelete, boolean isUndelete, boolean isBefore, boolean isAfter){

        List<LuanaSMS__Program_Offering__c> finalPOUpdateList = new List<LuanaSMS__Program_Offering__c>();
        if(isAfter){
            if(isInsert){
                //Do it in after insert is because we need the PO id to be the key and avoid overlap for bulk PO insert
                finalPOUpdateList.addAll(insertTuitioFeeAfterAction(poList)); // check for new PO to update the AU/NZ/INT Tuition Fees
            }
        }
        
        //Perform DML Action in here
        if(!finalPOUpdateList.isEmpty()){
            update finalPOUpdateList;
        }
    }
    
    public static void ProgramOfferingBeforeAction(List<LuanaSMS__Program_Offering__c> poList, Map<Id, LuanaSMS__Program_Offering__c> poMap, List<LuanaSMS__Program_Offering__c> oldPOList, Map<Id, LuanaSMS__Program_Offering__c> oldPOMap, boolean isInsert, boolean isUpdate, boolean isDelete, boolean isUndelete, boolean isBefore, boolean isAfter){
        if(isBefore){
            if(isUpdate){
                updateTuitioFeeBeforeAction(poList, oldPOMap);
            }
        }
    }
    
    //Gather all the after inserted PO to update the AU/NZ/INT Tuition Fee fields
    public static List<LuanaSMS__Program_Offering__c> insertTuitioFeeAfterAction(List<LuanaSMS__Program_Offering__c> poList){
        System.debug('***run here1');
        //This list is the final list for this method for ready PO records to cont for DML Action
        List<LuanaSMS__Program_Offering__c> readyPOList = new List<LuanaSMS__Program_Offering__c>();
        
        //Get all Product Ids from AU/NZ/INT Product fields
        Map<String, Id> prodIds = new Map<String, Id>();
        for(LuanaSMS__Program_Offering__c po: poList){
            if(po.NZ_Product__c != null){
                prodIds.put(po.Id+'_NZ', po.NZ_Product__c);
            }
            if(po.AU_Product__c != null){
                prodIds.put(po.Id+'_AU', po.AU_Product__c);
            }
            if(po.INT_Product__c != null){
                prodIds.put(po.Id+'_INT',po.INT_Product__c);
            }
        }
        
        //Generate the standard price for each product into a map
        Map<Id, Decimal> stdProdUnitPriceMap = new Map<Id, Decimal>();
        for(PricebookEntry pbe: [select Id, ProductCode, Product2Id, Pricebook2.Id, unitPrice, Pricebook2.IsStandard from PricebookEntry Where Product2Id =: prodIds.values() AND Pricebook2.IsStandard = true]){
            stdProdUnitPriceMap.put(pbe.Product2Id, pbe.unitPrice);
        }
        System.debug('******stdProdUnitPriceMap::: ' + stdProdUnitPriceMap);
        
        //Set the AU/NZ/INT Tuition Fee field
        for(LuanaSMS__Program_Offering__c po: poList){
            LuanaSMS__Program_Offering__c tempPO = new LuanaSMS__Program_Offering__c();
            tempPO.Id = po.Id;
            if(!prodIds.isEmpty()){
                for(String mapKey: prodIds.KeySet()){
                    if(mapKey == po.Id+'_NZ'){
                        tempPO.NZ_Tuition_Fee__c = stdProdUnitPriceMap.get(po.NZ_Product__c);
                    }else if(mapKey == po.Id+'_AU'){
                        tempPO.AU_Tuition_Fee__c = stdProdUnitPriceMap.get(po.AU_Product__c);
                    }else if(mapKey == po.Id+'_INT'){
                        tempPO.INT_Tuition_Fee__c = stdProdUnitPriceMap.get(po.INT_Product__c);
                    }
                }  
                readyPOList.add(tempPO);  
            }
        }
        System.debug('****readyPOList:: ' + readyPOList);
        return readyPOList;
    }
    
    public static void updateTuitioFeeBeforeAction(List<LuanaSMS__Program_Offering__c> poList, Map<Id, LuanaSMS__Program_Offering__c> oldPOMap){
        
        Map<String, Id> listToProcessMap = new Map<String, Id>();
        List<LuanaSMS__Program_Offering__c> contProcessPOList = new List<LuanaSMS__Program_Offering__c>();
        for(LuanaSMS__Program_Offering__c po: poList){
            System.debug('***********po: ' + po);
            if(oldPOMap.get(po.Id).AU_Product__c != po.AU_Product__c){
                listToProcessMap.put(po.Id+'_AU', po.AU_Product__c);
                contProcessPOList.add(po);
            }
            if(oldPOMap.get(po.Id).INT_Product__c != po.INT_Product__c){
                listToProcessMap.put(po.Id+'_INT', po.INT_Product__c);
                contProcessPOList.add(po);
            }
            if(oldPOMap.get(po.Id).NZ_Product__c != po.NZ_Product__c){
                listToProcessMap.put(po.Id+'_NZ', po.NZ_Product__c);
                contProcessPOList.add(po);
            }
        }
        //Generate the standard price for each product into a map
        Map<Id, Decimal> stdProdUnitPriceMap = new Map<Id, Decimal>();
        for(PricebookEntry pbe: [select Id, ProductCode, Product2Id, Pricebook2.Id, unitPrice, Pricebook2.IsStandard from PricebookEntry Where Product2Id =: listToProcessMap.values() AND Pricebook2.IsStandard = true]){
            stdProdUnitPriceMap.put(pbe.Product2Id, pbe.unitPrice);
        }
        System.debug('***stdProdUnitPriceMap: ' + stdProdUnitPriceMap);
        
        //Set the AU/NZ/INT Tuition Fee field
        for(LuanaSMS__Program_Offering__c po: contProcessPOList){
            if(!listToProcessMap.isEmpty()){
                for(String mapKey: listToProcessMap.KeySet()){
                    if(mapKey == po.Id+'_NZ'){
                        po.NZ_Tuition_Fee__c = stdProdUnitPriceMap.get(po.NZ_Product__c);
                    }else if(mapKey == po.Id+'_AU'){
                        po.AU_Tuition_Fee__c = stdProdUnitPriceMap.get(po.AU_Product__c);
                    }else if(mapKey == po.Id+'_INT'){
                        po.INT_Tuition_Fee__c = stdProdUnitPriceMap.get(po.INT_Product__c);
                    }
                }   
            }
            System.debug('***po: ' + po);
        }
    }
    
}