global class ff_WriteResult {

    global ff_WriteResult() {
        results = new Map<String, SObject>();
        recordErrors = new Map<String, List<String>>();
        fieldErrors = new Map<String, Map<String, String>>();
    }

    @AuraEnabled
    global Map<String, SObject> results {get;set;}

    @AuraEnabled
    global Map<String, List<String>> recordErrors {get;set;}

    @AuraEnabled
    global Map<String, Map<String, String>> fieldErrors {get;set;}

}