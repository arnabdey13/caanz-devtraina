/***********************************************************************************************************************************************************************
Name: QPRReferralTriggerHandler 
============================================================================================================================== 
Purpose: Trigger Handler for QPRReferral Object
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR                 DATE          DETAIL     Description 
1.0        Anitha T  			  2/08/2019    Created    Trigger Handler for QPRReferral Object


--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/

public class QPRReferralTriggerHandler extends TriggerHandler{ 
    //Context Variable Collections after Type Casting
    List<QPR_Referral__c> newList = (List<QPR_Referral__c>)Trigger.new;
    List<QPR_Referral__c> oldList = (List<QPR_Referral__c>)Trigger.old;
    Map<Id,QPR_Referral__c> oldMap = (Map<Id,QPR_Referral__c>)Trigger.oldMap;
    Map<Id,QPR_Referral__c> newMap = (Map<Id,QPR_Referral__c>)Trigger.newMap;        
    List<Account> accList = new List<Account>();
    public static Boolean isTrigExec =true;
    //After Insert Trigger 
    public override void afterInsert() {
        updateQPRReferralCountry(newList);
    }
    public override void afterUpdate(){
        updateQPRReferralCountry(newList);
    }
    
    public void updateQPRReferralCountry(List<QPR_Referral__c> newQPRReferralList)
    {
        
        if (isTrigExec ==true)
        {
            Map<Id, Id> qprrefAccountIdMap = new Map<Id, Id>();
            Map<Id, Account> qprrefAccountRecordMap = new Map<Id, Account>();
            List<QPR_Referral__c> qprrefList  = new List<QPR_Referral__c>();
            for(QPR_Referral__c qprref : newQPRReferralList){
               if(!String.isEmpty(qprref.Referred_Account__c)){
                    qprrefAccountIdMap.put(qprref.Id, qprref.Referred_Account__c) ; 
                }
                else if(!String.isEmpty(qprref.Referral_Contact__c)){
                    qprrefAccountIdMap.put(qprref.Id, qprref.Referral_Contact__c) ; 
                }
                else
                {	
                 QPR_Referral__c targetqprRef =   new QPR_Referral__c();
                 targetqprRef.Id = qprref.Id;
                 targetqprRef.Hidden_Country__c =  null;
                 qprrefList.add(targetqprRef);	
                }
            }
            if(qprrefAccountIdMap!=null && !qprrefAccountIdMap.isEmpty())  
            {
                accList = [SELECT Name,isPersonAccount,BillingCountry, PersonMailingCountry, Affiliated_Branch_Country__c
                           FROM Account WHERE Id IN :  qprrefAccountIdMap.values()];
                for(Account acc : accList)
                {
                    qprrefAccountRecordMap.put(acc.Id, acc);
                    
                }
                if(accList != null && !accList.isEmpty())
                {         
                    for(QPR_Referral__c qprreferral : newQPRReferralList){
                        if(String.isNotEmpty(qprreferral.Referred_Account__c) && qprrefAccountRecordMap!=null && !qprrefAccountRecordMap.isEmpty())
                        {
                            if(qprrefAccountRecordMap.containsKey(qprreferral.Referred_Account__c))
                            {
                                QPR_Referral__c targetqprRef =   new QPR_Referral__c(); 
                                targetqprRef.Id = qprreferral.Id;	
                                Account acc =  qprrefAccountRecordMap.get(qprreferral.Referred_Account__c);
                                if(acc.BillingCountry!=null && !string.isEmpty(acc.BillingCountry))
                                { if(acc.BillingCountry=='Australia' || acc.BillingCountry=='New Zealand')
                                {targetqprRef.Hidden_Country__c =  acc.BillingCountry;}
                                 else 
                                 {targetqprRef.Hidden_Country__c ='Overseas';}
                                }
                                
                                qprrefList.add(targetqprRef);
                            }
                        }
                        else if(String.isNotEmpty(qprreferral.Referral_Contact__c) && qprrefAccountRecordMap!=null && !qprrefAccountRecordMap.isEmpty()){
                            if(qprrefAccountRecordMap.containsKey(qprreferral.Referral_Contact__c))
                            {
                                QPR_Referral__c targetqprRef =   new QPR_Referral__c();
                                targetqprRef.Id = qprreferral.Id;
                                Account acc = qprrefAccountRecordMap.get(qprreferral.Referral_Contact__c);
                                if(acc.Affiliated_Branch_Country__c!=null && !string.isEmpty(acc.Affiliated_Branch_Country__c))
                                {
                                    targetqprRef.Hidden_Country__c =  acc.Affiliated_Branch_Country__c;
                                    qprrefList.add(targetqprRef);	  
                                }
                            }
                        }
                    }
                }
            } 
            if(qprrefList!=null && !qprrefList.isEmpty())  
            { Database.update(qprrefList);}
            isTrigExec =false;	
        }
        
    }
    
    
    public override void beforeInsert(){}
    public override void beforeUpdate(){}
    public override void afterDelete(){}
    public override void afterUndelete(){}
}