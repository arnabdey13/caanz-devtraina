@IsTest(seeAllData=false)
private class AnalyticsTrackerServiceTests {

    @testSetup static void createTestData() {
        TestSubscriptionUtils.createTestData();
    }

    @IsTest
    private static void testLoading() {
        Map<String, String> clientData = AnalyticsTrackerService.load(TestSubscriptionUtils.getTestAccountId());
        System.debug(clientData.keySet());

        System.assert(clientData.keySet().contains('userId'), 'UserId is always present');
        System.assert(clientData.keySet().contains('userName'), 'UserName is always present');
        System.assert(clientData.keySet().contains('membershipType'), 'membershipType is always present');

    }


}