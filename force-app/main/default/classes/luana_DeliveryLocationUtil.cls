/**
    Developer: WDCi (Lean)
    Development Date: 21/04/2016
    Task #: Util for delivery location LCA-302
    
    Change History:
    LCA-698 27/06/2016: WDCi Lean - added new method to return workshop location
    LCA-710 05/07/2016: WDCi Lean - added constants for course delivery location type
**/

public without sharing class luana_DeliveryLocationUtil {
    
    public static final String WORKSHOP_LOCATION = 'Workshop Location';
    public static final String EXAM_LOCATION = 'Exam Location';
    
    public static List<Course_Delivery_Location__c> getCourseExamDeliveryLocation(String courseId){
        
        //LCA-5, include workshop location type in the soql filter & Availability_Day__c
        return [Select Id, Name, Delivery_Location__c, Course__c, Type__c, Availability_Day__c,
                Delivery_Location__r.LuanaSMS__Country__c,
                Delivery_Location__r.LuanaSMS__Delivery_Location_Name__c
                from Course_Delivery_Location__c
                Where Course__c =: courseId AND (Type__c = 'Exam Location')
                ORDER BY Delivery_Location__r.LuanaSMS__Delivery_Location_Name__c];
    }
    
    public static List<Course_Delivery_Location__c> getCourseWorkshopLocation(String courseId){
        
        //LCA-5, include workshop location type in the soql filter & Availability_Day__c
        return [Select Id, Name, Delivery_Location__c, Course__c, Type__c, Availability_Day__c,
                Delivery_Location__r.LuanaSMS__Country__c,
                Delivery_Location__r.LuanaSMS__Delivery_Location_Name__c
                from Course_Delivery_Location__c
                Where Course__c =: courseId AND (Type__c = 'Workshop Location')
                ORDER BY Delivery_Location__r.LuanaSMS__Delivery_Location_Name__c];
    }
    
    public static List<Course_Delivery_Location__c> getCourseSuppExamDeliveryLocation(String courseId){
        return [Select Id, Name, Delivery_Location__c, Course__c, Type__c, Availability_Day__c,
                Delivery_Location__r.LuanaSMS__Country__c,
                Delivery_Location__r.LuanaSMS__Delivery_Location_Name__c
                from Course_Delivery_Location__c
                Where Course__c =: courseId AND (Type__c = 'Supp Exam Location')
                ORDER BY Delivery_Location__r.LuanaSMS__Delivery_Location_Name__c];
    }
    
}