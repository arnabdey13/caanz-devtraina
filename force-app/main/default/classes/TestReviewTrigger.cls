@isTest
Public Class TestReviewTrigger{
/************************************************
*
*Description: Test class for ReviewTeamTrigger
*History:
*25/07/2019 - Initial version - 
*
************************************************/

public static Id questionnaireAU = Schema.SObjectType.QPR_Questionnaire__c.getRecordTypeInfosByName().get('QPR AUS').getRecordTypeId();
@testSetup

    Static void setupTestData(){
        //Method to set up test data to be used in this test class.
        List<User> listUsers = new List<User>();
        Profile profileQPR = [SELECT Id, Name FROM Profile WHERE Name = 'CAANZ Business Management User' LIMIT 1];
        PermissionSet permQPRAU = [SELECT Id, Name FROM PermissionSet WHERE Name = 'QPR_AU_Permission_set' LIMIT 1];

        Profile profileReviewer = [SELECT Id, Name FROM Profile WHERE Name = 'QPR Reviewer User' LIMIT 1];
        PermissionSet permReviewer = [SELECT Id, Name FROM PermissionSet WHERE Name = 'QPR_Reviewer_AU' LIMIT 1];
        //Create Account and Review to associate Reviewer_Note__c to.
        RecordType typeBusiness = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Business_Account' LIMIT 1];
        RecordType typeMember = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Full_Member' LIMIT 1];

        List<Account> listAccounts = new List<Account>();

        
        
        Account personAccount = new Account();
        personAccount.RecordType = typeMember;
        personAccount.FirstName = 'Test';
        personAccount.LastName = 'Practice Contact';
        personAccount.Member_Of__c = 'ICAA';
        personAccount.Gender__c = 'Male';
        personAccount.Salutation = 'Mr';
        personAccount.PersonEmail = 'testpracticecontact@davanti.co.nz';
        personAccount.Create_Portal_User__c = false;
        personAccount.Communication_Preference__c= 'Home Phone';
        personAccount.PersonHomePhone= '1234';
        personAccount.PersonOtherStreet= '83 Saggers Road';
        personAccount.PersonOtherCity='JITARNING';
        personAccount.PersonOtherState='Western Australia';
        personAccount.PersonOtherCountry='Australia';
        personAccount.PersonOtherPostalCode='6365'; 
 		
        listAccounts.add(personAccount);

        Account acct = new Account();
        acct.RecordType = typeBusiness;
        acct.Name = 'Test Account A7';
        acct.Type = 'Unallocated';
        acct.Phone = '6421771557';
        acct.Member_Of__c = 'ICAA';
        acct.BillingStreet = '33 Customhouse Quay';
        acct.BillingCity = 'Wellington';
        acct.BillingCountry = 'New Zealand';
        acct.Review_Contact__c = personAccount.Id;
        

        listAccounts.add(acct);    

        Account reviewer = new Account();
        reviewer.RecordType = typeMember;
        reviewer.FirstName = 'Reviewer';
        reviewer.LastName = 'LastName';
        reviewer.Member_Of__c = 'ICAA';
        reviewer.Gender__c = 'Female';
        reviewer.Salutation = 'Ms';
        reviewer.PersonEmail = 'testreviewer@example.com';
        reviewer.Create_Portal_User__c = False;
        
        reviewer.Communication_Preference__c= 'Home Phone';
        reviewer.PersonHomePhone= '1234';
        reviewer.PersonOtherStreet= '83 Saggers Road';
        reviewer.PersonOtherCity='JITARNING';
        reviewer.PersonOtherState='Western Australia';
        reviewer.PersonOtherCountry='Australia';
        reviewer.PersonOtherPostalCode='6365';  
        reviewer.Reviewer__c=True;
        listAccounts.add(reviewer);

        insert listAccounts;
        //from here
        Account a = TestObjectCreator.createBusinessAccount();
		Account acct0 = [SELECT Id, FirstName, LastName FROM Account WHERE PersonEmail = 'testreviewer@example.com' LIMIT 1];
        Account acct1 = [SELECT Id, FirstName, LastName FROM Account WHERE PersonEmail = 'testpracticecontact@davanti.co.nz' LIMIT 1];

        QPR_Questionnaire__c oneQuestionnaire = new QPR_Questionnaire__c();
        oneQuestionnaire.RecordTypeId = questionnaireAU;
        oneQuestionnaire.Practice__c = a.Id;
        oneQuestionnaire.Practice_ID__c = '12345alpha';
        insert oneQuestionnaire;
        
        Review__c review1 = new Review__c();
        review1.Practice_Name__c = a.Id;
        review1.Review_Country__c = 'AU';
        review1.Process_Stage__c = 'AU';
        review1.Person_Account__c = acct1.Id; 
	    review1.Legacy_Quality_Review_ID__c = 'R-123456';       
		review1.Status__c ='In-Progress';
        review1.Review_Type__c ='Conflicts Review';
        review1.QPR_Questionnaire__c = oneQuestionnaire.Id;
        insert review1;
        oneQuestionnaire.Review_Code__c = review1.Id;
        oneQuestionnaire.Status__c = 'Submitted';
        update oneQuestionnaire;
        ////
        QPR_Questionnaire__c oneQuestionnaire1 = new QPR_Questionnaire__c();
        oneQuestionnaire1.RecordTypeId = questionnaireAU;
        oneQuestionnaire1.Practice__c = a.Id;
        oneQuestionnaire1.Practice_ID__c = '12345alpha';
        insert oneQuestionnaire1;
        
        Review__c review2 = new Review__c();
        review2.Practice_Name__c = a.Id;
        review2.Review_Country__c = 'AU';
        review2.Process_Stage__c = 'AU';
        review2.Person_Account__c = acct1.Id; 
	    review2.Legacy_Quality_Review_ID__c = 'R-1234567';       
		review2.Status__c ='In-Progress';
        review2.Review_Type__c ='Conflicts Review';
         review2.QPR_Questionnaire__c = oneQuestionnaire1.Id;
        insert review2;
        oneQuestionnaire1.Review_Code__c = review2.Id;
        oneQuestionnaire1.Status__c = 'Submitted';
        update oneQuestionnaire1;
         review1.Original_Review__c=review2.Id;
        update review1;
/*  
        QPR_Questionnaire__Share newShare = new QPR_Questionnaire__Share();
            newShare.parentId = oneQuestionnaire.Id;
            newShare.UserOrGroupId = reviewer.Id;
            newShare.AccessLevel = 'Read';
            newShare.RowCause = Schema.QPR_Questionnaire__Share.RowCause.Autoshare_Original_Review_QPR__c;
            insert newShare;
        
          QPR_Questionnaire__Share newShare1 = new QPR_Questionnaire__Share();
newShare1.parentId = oneQuestionnaire1.Id;
newShare1.UserOrGroupId = reviewer.Id;
        newShare1.AccessLevel =  'Read';
newShare1.RowCause = Schema.QPR_Questionnaire__Share.RowCause.Autoshare_Original_Review_QPR__c;
insert newShare1;
*/        
        ///
        
        Review_Team__c reviewTeam = new Review_Team__c();
        reviewTeam.Review__c = review1.Id;
        reviewTeam.Reviewer__c = reviewer.Id;
        insert reviewTeam;
        
        Map<ID, Review__c> b = new Map<ID, Review__c> ();
        ReviewTriggerHandler.getOriginalQuestionnaireId(reviewTeam,b);
        ReviewTriggerHandler.getQuestionnaireId(reviewTeam,b);
       
    }
    
    
    @isTest
    Static void testCreateReviewTeam(){
        //Review__c review  = [SELECT Id, Name FROM Review__c WHERE Legacy_Quality_Review_ID__c = 'R-12345' LIMIT 1];
        Account acct = [SELECT Id, FirstName, LastName FROM Account WHERE PersonEmail = 'testreviewer@example.com' LIMIT 1];
         Account acct1 = [SELECT Id, FirstName, LastName FROM Account WHERE PersonEmail = 'testpracticecontact@davanti.co.nz' LIMIT 1];
        Account reviewer = [SELECT Id, FirstName, LastName FROM Account WHERE PersonEmail = 'testreviewer@example.com' LIMIT 1];
        QPR_Questionnaire__c oneQuestionnaire = [SELECT Id  FROM QPR_Questionnaire__c WHERE Practice_ID__c = '12345alpha' LIMIT 1];
        Review__c review = new Review__c();
        review.Practice_Name__c = acct.Id;
        review.Review_Country__c = 'AU';
        review.Process_Stage__c = 'AU';
        review.Person_Account__c = acct1.Id; 
	    review.Legacy_Quality_Review_ID__c = 'R-12345';       
        
	//	review.Status__c ='In-Progress';
        

       Test.startTest();
  
        insert review;
  /*      String reviewId= review.Id;
       	
*/
        
        // update QPR Questionnaire
        /*
        */
     
	Review__c review1  = [SELECT Id, Name FROM Review__c WHERE Legacy_Quality_Review_ID__c = 'R-123456' LIMIT 1];
        review1.Status__c ='Practice Accepted';
        review1.Legacy_Quality_Review_ID__c =NULL;
        //review1.Original_Review__c=review.Id;
        update review1;
	
     //  Review__c review2  = [SELECT Id, Name FROM Review__c WHERE Id = :reviewId LIMIT 1];
 		review1.Status__c ='Cancelled';
       review1.Reason_for_cancellation__c	='Resigned';
       update review1;
   
       Test.stopTest(); 
    }

}