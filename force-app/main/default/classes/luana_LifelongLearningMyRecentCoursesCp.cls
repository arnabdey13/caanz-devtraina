/**
    Developer: WDCi (KH)
    Development Date: 21/03/2017
    Task #: Controller for luana_LifelongLearningMyRecentCourses vf component
    
**/
public without sharing class luana_LifelongLearningMyRecentCoursesCp {
    
    public string contactId;
    
    public void setContactId(String contactId){
        this.contactId = contactId;
    }
    
    public String getContactId(){
        return this.contactId;
    }
    
    public List<LuanaSMS__Student_Program__c> getLLLCourses() {
        return [SELECT Id, Name, LuanaSMS__Course__c, LuanaSMS__Course__r.Name, LuanaSMS__Status__c, RecordType.DeveloperName, LuanaSMS__Program_Commencement_Date__c 
            FROM LuanaSMS__Student_Program__c 
            WHERE LuanaSMS__Contact_Student__c =: getContactId() AND RecordType.DeveloperName =: luana_EnrolmentConstants.RECORDTYPE_STUDENTPROGRAM_LLL ORDER BY CreatedDate DESC LIMIT 200]; 
    
    }
}