@isTest
public class FormAssemblyWSTest {
	static {
		SchoolDetail__c sd1 = new SchoolDetail__c();
		sd1.Name = 'uniqueid1';
		sd1.Country__c = 'New Zealand';
		sd1.Name__c = 'Name of School';
		sd1.Region__c = 'Wellington';
		sd1.StudentType__c ='Tertiary';
		insert sd1;
		SchoolDetail__c sd2 = new SchoolDetail__c();
		sd2.Name = 'uniqueid2';
		sd2.Country__c = 'New Zealand';
		sd2.Name__c = 'Another Name here';
		sd2.Region__c = 'Wellington';
		sd2.StudentType__c ='Secondary';
		insert sd2;
		

	}
	
  static void setupRequest() {
 		RestRequest req = new RestRequest();
	    RestResponse res = new RestResponse();
	    req.requestURI = '/services/apexrest/FormAssembly';  //Request URL
	    req.httpMethod = 'GET';  //HTTP Request Type
	    RestContext.request = req;
	    RestContext.response = res;
  }
	
  static testMethod void testGetJson() {
	setupRequest();
	RestRequest req = RestContext.request;
	RestResponse res = RestContext.response;
	req.addParameter('schoolAutoCompleteTerm', 'oo');
	FormAssemblyWS.getDetails();
	String responseString = RestContext.response.responseBody.toString();
	List<SchoolDetail__c> results = (List<SchoolDetail__c>)JSON.deserialize(responseString, List<SchoolDetail__c>.class);
	System.assert(responseString.contains('Name of School'));
	System.assert(results.size() == 1);
  }
  
  static testMethod void testGetJsonWithCountryAndStudentType() {
	setupRequest();
	RestRequest req = RestContext.request;
	RestResponse res = RestContext.response;
	req.addParameter('schoolAutoCompleteTerm', 'Name');
	req.addParameter('country', 'New Zealand');
	req.addParameter('studenttype', 'Tertiary');
	FormAssemblyWS.getDetails();
	String responseString = RestContext.response.responseBody.toString();
	List<SchoolDetail__c> results = (List<SchoolDetail__c>)JSON.deserialize(responseString, List<SchoolDetail__c>.class);
	System.assert(responseString.contains('Name of School'));
	System.assert(results.size() == 1);
  }
  
  static testMethod void testGetJsonP() {	
	setupRequest();
	RestRequest req = RestContext.request;
	RestResponse res = RestContext.response;
	req.addParameter('schoolAutoCompleteTerm', 'oo');
	req.addParameter('callback', 'mycallbackname');
	FormAssemblyWS.getDetails();
	String responseString = RestContext.response.responseBody.toString();
	System.assert(responseString.contains('Name of School'));
	System.assert(responseString.startsWith('mycallbackname'));
  }
}