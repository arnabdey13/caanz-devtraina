public class NotificationsRouter {
    
    @AuraEnabled
    public static Map<String,Object> load() {
        return getMemberData(EditorPageUtils.getUsersAccountId());
    }
    
    @TestVisible
    private static Map<String,Object> getMemberData(Id accountId) {
        Map<String, Object> result = new Map<String,Object>();

        Account a = [select Membership_Class__c, PersonOtherCountryCode
                     from Account where Id = :accountId];
        // being verbose here to extra generate coverage since load method is hard to test
        System.assertNotEquals(null, a, 'an account is always available');

        result.put('prop.skipNotifications', a.Membership_Class__c == 'Provisional');

        // load ensures that a questionnaire is present
        Map<String,Object> questionnaire = (Map<String,Object>) EditorNotificationsService.load(accountId);

        if (questionnaire != null) {
            System.debug(questionnaire);
            String code = (String) questionnaire.get('prop.countryCode');
            if (code != null) {
                code = code.toLowerCase();
                result.put('prop.countryCode', ANZ.contains(code) ? code : 'other');
            }
        } else {
            System.debug('No Questionnaire loaded');
        }

        return result;
    }
    
    private static Set<String> ANZ = new Set<String>{'au','nz'};
    
}