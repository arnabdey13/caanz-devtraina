/*
    Change History:
    LCA-867 WDCi Lean: Add masterclass broadcast wizard
    LCA-827 WDCi KH: Add Supplementary Exam Broadcast wizard
    LCA-802 18/Aug/2016 - WDCi Lean: add masterclass allocation broadcast wizard
*/
public class luana_MainBroadcastController {
    
    private LuanaSMS__Course__c course;
    
    public luana_MainBroadcastController(ApexPages.StandardController controller) {
        
        this.course = (LuanaSMS__Course__c) controller.getRecord();
        
    }
    
    String type = null;
    
    public PageReference back() {
        return null;
    }
    
    public PageReference redirectBack() {
        PageReference backRef = new PageReference('/' + course.Id);
        backRef.setRedirect(true);
        return backRef;
    }
    
    public PageReference doRedirect() {
        
        PageReference pageRef;
        if(type == 'Virtual_Classes'){
            pageRef = Page.luana_VirtualClassBroadcastWizard;
            pageRef.getParameters().put('id', course.Id);
        }else if(type == 'Exam_Details'){
            pageRef = Page.luana_ExamBroadcastWizard;
            pageRef.getParameters().put('id', course.Id);
        }else if(type == 'Workshop_Details'){
            pageRef = Page.luana_WorkshopBroadcastWizard;
            pageRef.getParameters().put('id', course.Id);
        }else if(type == 'Masterclass_Opening'){
            pageRef = Page.luana_MasterclassBroadcastWizard;
            pageRef.getParameters().put('id', course.Id);
            pageRef.getParameters().put('actiontype', luana_MasterclassBroadcastAsync.TYPE_OPENING); //LCA-802
            
        }else if(type == 'Masterclass_Allocation_Details'){ //LCA-802
            pageRef = Page.luana_MasterclassBroadcastWizard;
            pageRef.getParameters().put('id', course.Id);
            pageRef.getParameters().put('actiontype', luana_MasterclassBroadcastAsync.TYPE_ALLOCATION);
        }
        
        //LCA-827
        else if(type == 'Supplementary_Exam_Opening'){
            pageRef = page.luana_SuppExamEnrolBroadcastWizard;
            pageRef.getParameters().put('id', course.Id);
            pageRef.getParameters().put('actiontype', 'opening');
        }else if(type == 'Supplementary_Exam_Allocation_Details'){
            pageRef = page.luana_SuppExamAllocationBroadcastWizard;
            pageRef.getParameters().put('id', course.Id);
            pageRef.getParameters().put('actiontype', 'allocation');
        }
        
        return pageRef;
    }
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Virtual_Classes','Virtual Classes')); 
        options.add(new SelectOption('Exam_Details','Exam Details')); 
        options.add(new SelectOption('Workshop_Details','Workshop Details'));
        // options.add(new SelectOption('Masterclass_Opening','Masterclass Opening'));
        options.add(new SelectOption('Masterclass_Allocation_Details','Masterclass Allocation Details'));
        //LCA-827
        options.add(new SelectOption('Supplementary_Exam_Opening', 'Supplementary Exam Opening'));
        options.add(new SelectOption('Supplementary_Exam_Allocation_Details', 'Supplementary Exam Allocation Details'));
                
        return options; 
    }
 
    public String getType() {
        return type;
    }
 
    public void setType(String type) {
        this.type = type;
    }
    
}