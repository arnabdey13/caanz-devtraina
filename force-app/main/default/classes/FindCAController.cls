/*
    Controller class for FindCA.page
    ======================================================
    Changes:
    	Sep 2016 	Davanti - YK	Created
        July 2017   CAANZ - akopec  Rev 01 - Added Accounting Specialties
*/

public without sharing class FindCAController {

    private final String STR_MEMBERTYPE_QUALIFIED_AUDITORS = 'Qualified Auditor';
    private final String STR_MEMBERTYPE_INSOLVENCY_PRACTICE = 'Insolvency Practitioner';
    private final String STR_MEMBERTYPE_SMSF_SPECIALIST = 'SMSF Specialist';
    private final String STR_MEMBERTYPE_BUSINESS_VALUATION_SPECIALIST = 'Business Valuation Specialist';
    private final String STR_MEMBERTYPE_FINANCIAL_PLANNING_SPECIALIST = 'Financial Planning Specialist';
    private final String STR_MEMBERTYPE_CPP_HELD = 'Public Practitioner';
    private final String STR_NAME = 'Name';
    
    private final String STR_COUNTRY_DEFAULT_OPTION = 'Select a country';
    private final String STR_BRANCH_DEFAULT_OPTION = 'Select a location';
    private final String STR_MEMBERTYPE_DEFAULT_OPTION = 'Select a type';
    
    private Map<String, List<String>> mapCountry2Branch = new Map<String, List<String>>();
    private Map<String, List<String>> mapCountry2MemberType = new Map<String, List<String>>(); 
    
    public List<SelectOption> listBranchCountryOptions {get; private set;} // Country drop down options
	public List<SelectOption> listBranchOptions {get; private set;} // Location drop down options
	public List<SelectOption> listMemberTypeOptions {get; private set;} // Type drop down options
	
	private List<CA> listCAs = new List<CA>(); // Account search results
    
    public Boolean bNewSearch {get; set;}
	public Boolean bDisplayResults {get; private set;}
	public Boolean bTooManyResultsFound {get; private set;}
	public Boolean bNoResultsFound {get; private set;}
    
    public Integer iSelectedPage {get; set;}
	public String strSortExp {get; set;}
	public String strSortDir {get; private set;}
	private String strPreviousSortExp;
	public String strQueryLimit {get; private set;}
	private Integer rowSize;
	private Integer numOfRecords;
	
	public String strSelectedBranchCountryOption {get; set;}
	public String strSelectedBranchOption {get; set;}
	public String strSelectedMemberTypeOption {get; set;}
	public String strInputName {get; set;}
	
	public Boolean bDisplaySelectBranchCountryMsg {get; private set;}
	public Boolean bDisplaySelectBranchMsg {get; private set;}
	public Boolean bDisplaySelectMemberTypeMsg {get; private set;}
	public Boolean bQualifiedAuditorSelected{
		get{
			return strSelectedMemberTypeOption == STR_MEMBERTYPE_QUALIFIED_AUDITORS;
		}
		set;
	}
    
    // Rev 01 start
    public Boolean bSpecialistSelected{
		get{
            if ( strSelectedMemberTypeOption == STR_MEMBERTYPE_BUSINESS_VALUATION_SPECIALIST || 
                 strSelectedMemberTypeOption == STR_MEMBERTYPE_FINANCIAL_PLANNING_SPECIALIST ||
                 strSelectedMemberTypeOption == STR_MEMBERTYPE_SMSF_SPECIALIST
               )
				return true;
            else
                return false;
		}
		set;
    }
            
    public Boolean bBVSpecialistSelected{
		get{
			return strSelectedMemberTypeOption == STR_MEMBERTYPE_BUSINESS_VALUATION_SPECIALIST;
		}
		set;
	}
          
    public Boolean bFPSpecialistSelected{
		get{
			return strSelectedMemberTypeOption == STR_MEMBERTYPE_FINANCIAL_PLANNING_SPECIALIST;
		}
		set;
	}
            
    public Boolean bSMSFSpecialistSelected{
		get{
			return strSelectedMemberTypeOption == STR_MEMBERTYPE_SMSF_SPECIALIST;
		}
		set;
	}
            
    // Rev 01 end
	
	public void displaySelectedPage() {
		System.debug(logginglevel.INFO, '##### iSelectedPage: ' + iSelectedPage);
    	setCon.setPageNumber(Integer.valueOf(iSelectedPage));
    }
	
	public List<Integer> listPageNumbers{
		get{
			List<Integer> listPageNums = new List<Integer>();
			for(Integer i = 1; i <= getNumOfPages(); i++){
				listPageNums.add(i);
			}
			return listPageNums;
		}
	}
	
    public Integer getNumOfPages() {
    	System.debug(logginglevel.INFO, '##### numOfRecords: ' + numOfRecords);
    	System.debug(logginglevel.INFO, '##### rowSize: ' + rowSize);
        if(numOfRecords == null || numOfRecords == 0)
            return 1;
        else if(math.mod(numOfRecords, rowSize) > 0)
            return numOfRecords/rowSize + 1;
        else
            return numOfRecords/rowSize;
    }
    
    // ====== CONSTRUCTOR ====== 
    public FindCAController() {
    	
        listBranchCountryOptions = new List<SelectOption>{new SelectOption('', STR_COUNTRY_DEFAULT_OPTION)};
        listBranchOptions = new List<SelectOption>{new SelectOption('', STR_BRANCH_DEFAULT_OPTION)};
		listMemberTypeOptions = new List<SelectOption>{new SelectOption('', STR_MEMBERTYPE_DEFAULT_OPTION)};
        
		for(Schema.PicklistEntry entry : Account.Affiliated_Branch_Country__c.getDescribe().getPicklistValues())
			listBranchCountryOptions.add(new SelectOption(entry.getLabel(), entry.getValue()));
        
        for(Region_State_Setting__mdt location : [Select Label, Country__c From Region_State_Setting__mdt]){
            if(mapCountry2Branch.keySet().contains(location.Country__c))
    			mapCountry2Branch.get(location.Country__c).add(location.Label);
    		else
    			mapCountry2Branch.put(location.Country__c, new List<String>{location.Label});
        }
    	
		mapCountry2MemberType.put('New Zealand', new List<String>{STR_MEMBERTYPE_BUSINESS_VALUATION_SPECIALIST, STR_MEMBERTYPE_QUALIFIED_AUDITORS, STR_MEMBERTYPE_INSOLVENCY_PRACTICE, STR_MEMBERTYPE_CPP_HELD});
		mapCountry2MemberType.put('Australia', new List<String>{STR_MEMBERTYPE_BUSINESS_VALUATION_SPECIALIST, STR_MEMBERTYPE_SMSF_SPECIALIST, STR_MEMBERTYPE_FINANCIAL_PLANNING_SPECIALIST, STR_MEMBERTYPE_CPP_HELD});
		mapCountry2MemberType.put('Overseas', new List<String>{STR_MEMBERTYPE_BUSINESS_VALUATION_SPECIALIST, STR_MEMBERTYPE_SMSF_SPECIALIST, STR_MEMBERTYPE_FINANCIAL_PLANNING_SPECIALIST, STR_MEMBERTYPE_CPP_HELD});
		
		bDisplayResults = false;
		strSortExp = STR_NAME;
		strSortDir = 'ASC';
		rowSize = (Label.FIND_CA_SEARCH_SIZE.isNumeric()) ? Integer.valueOf(Label.FIND_CA_SEARCH_SIZE) : 10;
		strQueryLimit = Label.FIND_CA_SOQL_Query_Limit;
		iSelectedPage = 1;
		
		/*
		strSelectedBranchCountryOption = '';
		strSelectedBranchOption = '';
		strSelectedBranchCountryOption = '';
		*/
	} 
	 
	// Clears search parameters to default options 
    public void refreshSearchParameters(){
    	listBranchOptions.clear();
    	listBranchOptions.add(new SelectOption('', STR_BRANCH_DEFAULT_OPTION));
    	if(mapCountry2Branch.keySet().contains(strSelectedBranchCountryOption)){
    		List<String> listBranches = mapCountry2Branch.get(strSelectedBranchCountryOption);
    		listBranches.sort();
    		for(String strBranch : listBranches)
            	listBranchOptions.add(new SelectOption(strBranch, strBranch));
    	}
    	
    	listMemberTypeOptions.clear();
    	listMemberTypeOptions.add(new SelectOption('', STR_MEMBERTYPE_DEFAULT_OPTION));
    	if(mapCountry2MemberType.keySet().contains(strSelectedBranchCountryOption)){
    		for(String strMemberType : mapCountry2MemberType.get(strSelectedBranchCountryOption))
        		listMemberTypeOptions.add(new SelectOption(strMemberType, strMemberType));
    	}
    }
    
    // Standard set controller to manage search results
    public ApexPages.StandardSetController setCon {
        get{
            if(setCon == null){
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(getQuery(false)));
                setCon.setPageSize(rowSize);
                setCon.setPageNumber(1);
                numOfRecords = setCon.getResultSize();
                
            }
            return setCon;
        }
        set;
    }  
	
	// List of search results, managed through standard set controller
    public List<CA> getCAs(){
		try {
			if(bDisplayResults){
				listCAs.clear();
				for(SObject sobj : setCon.getRecords())
					listCAs.add(new CA((Account)sobj, strSelectedMemberTypeOption));
                System.debug(logginglevel.INFO, '##### listCAs: ' + listCAs);
				return listCAs;
			}
			else
				return null;
		} 
		catch (exception e){
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error: ' + e));
			return null;
		}
	}
    
    // Page action Method.  Performs a search on accounts based on the page selection options
    public pageReference searchCA() {
        try {
        	
        	bDisplaySelectBranchCountryMsg = (String.isBlank(strSelectedBranchCountryOption)) ? true : false;
			bDisplaySelectBranchMsg = (String.isBlank(strSelectedBranchOption)) ? true : false;
			bDisplaySelectMemberTypeMsg = (String.isBlank(strSelectedMemberTypeOption)) ? true : false;
			
			System.debug(logginglevel.INFO, '##### strSelectedBranchCountryOption: ' + strSelectedBranchCountryOption);
			System.debug(logginglevel.INFO, '##### bDisplaySelectBranchCountryMsg: ' + bDisplaySelectBranchCountryMsg);
        	
        	if(validateSearchParameters()){
        		
        		AggregateResult results = Database.query(getQuery(true));
        		if(String.isNotBlank(strQueryLimit) && strQueryLimit.isNumeric())
        			bTooManyResultsFound = ((Integer)results.get('size') > Integer.valueOf(strQueryLimit)) ? true : false;
        		else
        			bTooManyResultsFound = false;
        		bNoResultsFound = ((Integer)results.get('size') == 0) ? true : false;
        		system.debug('### bTooManyResultsFound: ' + bTooManyResultsFound + '___ bNoResultsFound: ' + bNoResultsFound);
        		
	        	if(bNoResultsFound){
	        		bDisplayResults = false;
	        		return null;
	        	}
	        	else{
	        		
	        		if(bNewSearch){
		        		strSortExp = STR_NAME;
		        		strPreviousSortExp = null;
		        		bNewSearch = false;
		        	}
	        		
		        	bDisplayResults = true;
			        strSortDir = 'ASC';
			        
			        if(strPreviousSortExp  == strSortExp){
			            strSortDir = 'DESC';
			            strPreviousSortExp = null;
			        }
			        else{
			            strPreviousSortExp = strSortExp;
			        }
			        setCon = null;
	        	}
        	}
            return null;
        } 
        catch (exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error: ' + e));
            return null;
        }
    }
    
    // Constructs and returns a query string.
    // @param bCount - if true, will return a count(id) query.
    private String getQuery(Boolean bCount){
    	
    	String strQuery;
    	Integer index = 0;
    	List<String> listNames = new List<String>();
    	
		String strSelectedBranchCountryOptionSafe = String.escapeSingleQuotes(strSelectedBranchCountryOption);
		String strSelectedBranchOptionSafe = String.escapeSingleQuotes(strSelectedBranchOption);
		String strSelectedMemberTypeOptionSafe = String.escapeSingleQuotes(strSelectedMemberTypeOption);
		String strInputNameSafe = String.escapeSingleQuotes(strInputName);
    	
    	if(bCount){
    		strQuery = 'SELECT COUNT(Id) size';
    	}
    	else{	
    		strQuery = 'SELECT Id, LastName, Display_Name__c, Primary_Employer_Company_Name__c, Affiliated_Branch__c, Affiliated_Branch_Country__c ';
    		strQuery += ', PersonOtherPhone, PersonEmail, Website, Primary_Employer__r.Website';
            
            strQuery += ', IsPersonAccount, Primary_Employer__c';
            strQuery += ', BillingStreet, BillingCity, BillingStateCode, BillingPostalCode';
            strQuery += ', Primary_Employer__r.BillingAddress, Primary_Employer__r.BillingStreet, Primary_Employer__r.BillingCity, Primary_Employer__r.BillingStateCode, Primary_Employer__r.BillingPostalCode';
            strQuery += ', ShippingStreet, ShippingCity, ShippingStateCode, ShippingPostalCode';
            strQuery += ', Primary_Employer__r.ShippingAddress, Primary_Employer__r.ShippingStreet, Primary_Employer__r.ShippingCity, Primary_Employer__r.ShippingStateCode, Primary_Employer__r.ShippingPostalCode';
            strQuery += ', QA_Conditions_imposed__c';
            strQuery += ', BV_Areas_of_Practice__c, FP_Areas_of_Practice__c, SMSF_Areas_of_Practice__c ';  // Rev 01
    	}
    		
    	strQuery += ' FROM Account WHERE';
    	strQuery += ' Status__c = \'Active\'';
    	strQuery += ' AND Opt_out_of_Find_an_Accountant_register__c = false';
    	
    	if(strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_QUALIFIED_AUDITORS){
	        strQuery += ' AND ((IsPersonAccount = true AND Membership_Type__c = \'Member\') OR IsPersonAccount = false)';
	        strQuery += ' AND Qualified_Auditor__c = true';
	        strQuery += ' AND QA_Date_of_suspension__c = null';
	        strQuery += ' AND QA_Date_of_de_recognition__c = null';
    	}
    	else if(strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_INSOLVENCY_PRACTICE){
    		strQuery += ' AND Membership_Type__c IN (\'Member\', \'NZAIP\', \'NMP\')';
	        strQuery += ' AND IsPersonAccount = true';
	        strQuery += ' AND Insolvency_Practitioner__c = true';
	        strQuery += ' AND NZAIP_Date_of_suspension__c = null';
	        strQuery += ' AND NZAIP_Date_of_de_recognition__c = null';
    	}
    	else if(strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_SMSF_SPECIALIST
            || strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_BUSINESS_VALUATION_SPECIALIST
            || strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_FINANCIAL_PLANNING_SPECIALIST
        ){
        	if(Label.FIND_CA_OPT_IN.equalsIgnoreCase('true')) // if this feature is 'on' include opt in as a condition, otherwise ignore
        		strQuery += ' AND Find_CA_Opt_In__c = true';
        	strQuery += ' AND Membership_Type__c = \'Member\'';
	        strQuery += ' AND IsPersonAccount = true';
	        strQuery += ' AND (NOT Financial_Category__c LIKE \'%Retired%\')';
	        if(strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_SMSF_SPECIALIST)
	            strQuery += ' AND SMSF__c = true';
	        else if(strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_BUSINESS_VALUATION_SPECIALIST)
	            strQuery += ' AND BV__c = true';
	        else if(strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_FINANCIAL_PLANNING_SPECIALIST)
	            strQuery += ' AND FP_Specialisation__c = true';
        }
        else if(strSelectedMemberTypeOptionSafe == STR_MEMBERTYPE_CPP_HELD){
        	strQuery += ' AND Find_CA_Opt_In__c = true';
        	strQuery += ' AND Membership_Type__c = \'Member\'';
	        strQuery += ' AND IsPersonAccount = true';
	        strQuery += ' AND CPP__c = true';
        }
        
    	if(String.isNotBlank(strSelectedBranchCountryOptionSafe))
            strQuery += ' AND Affiliated_Branch_Country__c = \'' + strSelectedBranchCountryOptionSafe + '\'';
        if(String.isNotBlank(strSelectedBranchOptionSafe)){
        	if(strSelectedBranchCountryOptionSafe == 'Overseas' && strSelectedBranchOptionSafe == 'Other')
        		strQuery += ' AND Affiliated_Branch__c != \'UK\'';
        	else
        		strQuery += ' AND Affiliated_Branch__c = \'' + strSelectedBranchOptionSafe + '\'';
        }
        if(String.isNotBlank(strInputNameSafe)){
        	listNames = listCANames(strInputNameSafe);
        	if(listNames.size() > 0){
	        	strQuery += ' AND (';
	        	for(String strPart : listNames){
	        		index++;
	        		strQuery += ' FirstName LIKE \'%' + strPart + '%\'OR LastName LIKE \'%' + strPart + '%\' OR Display_Name__c LIKE \'%' + strPart + '%\'';
	        		if(index != listNames.size())
	        			strQuery += ' OR';
	        	}
	        	strQuery += ')';
        	}
        }
		
        if(!bCount){
			strQuery += ' ORDER BY ' + strSort();
			if(String.isNotBlank(strQueryLimit) && strQueryLimit.isNumeric())
				strQuery += ' LIMIT ' + strQueryLimit;
        }		
		System.debug(logginglevel.INFO, '##### strQuery: ' + strQuery);
        return strQuery;
    }
    
    // Constructs and returns the sort order for the query string.
    private String strSort(){
    	String strSortDirSafe = String.escapeSingleQuotes(strSortDir);
    	if(strSortExp == 'Name')
        	return 'Display_Name__c ' + strSortDirSafe; 
        else if(strSortExp == 'Company')
        	return 'Primary_Employer_Company_Name__c ' + strSortDirSafe;
        //else if(strSortExp == 'Location')
    	//	return 'Location__c ' + strSortDirSafe;
        else
        	return 'Display_Name__c ' + strSortDirSafe; 
    }
    
    // Helper method that splits a string by spaces
    private List<String> listCANames(String strParam) {
        return String.escapeSingleQuotes(strParam).split('\\W+');
    }
    
    // Determines if search criteria is valid
	private Boolean validateSearchParameters(){
		return (String.isNotBlank(strSelectedBranchCountryOption) 
				&& String.isNotBlank(strSelectedBranchOption) 
				&& String.isNotBlank(strSelectedMemberTypeOption));
    }

	// ====== INTERNAL WRAPPER CLASS ======     
    public class CA{
    	
    	public Account act {get; set;}
    	private String strSelectedMemberType;
    	private final String STR_MEMBERTYPE_QUALIFIED_AUDITORS = 'Qualified Auditor';
        private final String STR_MEMBERTYPE_SMSF_SPECIALIST = 'SMSF Specialist';								// Rev 01
    	private final String STR_MEMBERTYPE_BUSINESS_VALUATION_SPECIALIST = 'Business Valuation Specialist';	// Rev 01
    	private final String STR_MEMBERTYPE_FINANCIAL_PLANNING_SPECIALIST = 'Financial Planning Specialist';	// Rev 01
        
		public CA(Account account, String strSelectedMemberTypeOption) {
			act = account;
			strSelectedMemberType = strSelectedMemberTypeOption;
		}
        
        public String Name{
        	get{
        		return act.Display_Name__c;
        	}
        	set;
        }
        public String Company{
        	get{
                return act.Primary_Employer_Company_Name__c;
        	}
        	set;
        }
        /*
        public String Location{
        	get{
        		return act.Location__c;
        	}
        	set;
        }
        */
        
        public String BusinessAddress{
        	get{
                String strBusinessAddress = '';
                if(act.IsPersonAccount){
                    if(String.isNotBlank(act.Primary_Employer__c)){
                        if(String.isNotBlank(act.Primary_Employer__r.BillingCity)){
                            strBusinessAddress = capInitialLetters(act.Primary_Employer__r.BillingStreet);
                            strBusinessAddress += ', ' + capInitialLetters(act.Primary_Employer__r.BillingCity);
                            if(act.Affiliated_Branch_Country__c == 'Australia')
                                strBusinessAddress += ', ' + act.Primary_Employer__r.BillingStateCode.toUpperCase();
                            strBusinessAddress += ', ' + act.Primary_Employer__r.BillingPostalCode;
                        }
                        else if(String.isNotBlank(act.Primary_Employer__r.ShippingCity)){
                            strBusinessAddress = capInitialLetters(act.Primary_Employer__r.ShippingStreet);
                            strBusinessAddress += ', ' + capInitialLetters(act.Primary_Employer__r.ShippingCity);
                            if(act.Affiliated_Branch_Country__c == 'Australia')
                                strBusinessAddress += ', ' + act.Primary_Employer__r.ShippingStateCode.toUpperCase();
                            strBusinessAddress += ', ' + act.Primary_Employer__r.ShippingPostalCode;
                    	}
                    }
                }
                else{
                    if(String.isNotBlank(act.BillingCity)){
                        strBusinessAddress = capInitialLetters(act.BillingStreet);
                        strBusinessAddress += ', ' + capInitialLetters(act.BillingCity);
                        if(act.Affiliated_Branch_Country__c == 'Australia')
                            strBusinessAddress += ', ' + act.BillingStateCode.toUpperCase();
                        strBusinessAddress += ', ' + act.BillingPostalCode;
                    }
                    else if(String.isNotBlank(act.ShippingCity)){
                        strBusinessAddress = capInitialLetters(act.ShippingStreet);
                        strBusinessAddress += ', ' + capInitialLetters(act.ShippingCity);
                        if(act.Affiliated_Branch_Country__c == 'Australia')
                            strBusinessAddress += ', ' + act.ShippingStateCode.toUpperCase();
                        strBusinessAddress += ', ' + act.ShippingPostalCode;
                    }
                }
                return strBusinessAddress;
        	}
        	set;
        }
        public String Phone{
        	get{
        		if(strSelectedMemberType != STR_MEMBERTYPE_QUALIFIED_AUDITORS)
        			return act.PersonOtherPhone;
        		else
        			return null;
        	}
        	set;
        }
        public String Email{
        	get{
        		if(strSelectedMemberType != STR_MEMBERTYPE_QUALIFIED_AUDITORS)
        			return act.PersonEmail;
        		else
        			return null;
        	}
        	private set;
        }
        public String CompanyWebsite{
        	get{
        		if(strSelectedMemberType != STR_MEMBERTYPE_QUALIFIED_AUDITORS){
	                if(act.IsPersonAccount){
	                    if(string.isNotBlank(act.Primary_Employer__r.Website)){
	                        if(!act.Primary_Employer__r.Website.startsWithignoreCase('http://') && !act.Primary_Employer__r.Website.startsWithignoreCase('https://')){
	                    		return 'http://' + act.Primary_Employer__r.Website;        
	                        }
	                    }
	                    return act.Primary_Employer__r.Website;
	                }
	                else{
	                    if(string.isNotBlank(act.Website)){
	                        if(!act.Website.startsWithignoreCase('http://') && !act.Website.startsWithignoreCase('https://')){
	                    		return 'http://' + act.Website;        
	                        }
	                    }
	                    return act.Website;
	            	}
        		}
            	else{
            		return null;
            	}
        	}
        	set;
        }
        public String SpecialConditions{
            get{
                return (string.isNotBlank(act.QA_Conditions_imposed__c) ? act.QA_Conditions_imposed__c : null) ;
            }
            set;
        }
        
        // Rev 01 start

        public String Specialties{
            get{      
                // return (string.isNotBlank(act.Accounting_Specialties__c) ? act.Accounting_Specialties__c : null) ;
                String strOutput = '';
                Boolean first = true;
                
                // Only one of the below 3 options can happen at a time
                
                // BV Areas of Practice for BV Searches
                if (String.isNotBlank(act.BV_Areas_of_Practice__c) && strSelectedMemberType == STR_MEMBERTYPE_BUSINESS_VALUATION_SPECIALIST ) {
                    for (String entry : act.BV_Areas_of_Practice__c.split(';') ) {
                        if (first) {
                            strOutput += entry;
                            first = false;
                        }
                    	else
                        	strOutput += ', ' + entry;
                	}
                }
                
                // FP Areas of Practice for FP Searches
                if (String.isNotBlank(act.FP_Areas_of_Practice__c) && strSelectedMemberType == STR_MEMBERTYPE_FINANCIAL_PLANNING_SPECIALIST ) {
                    for (String entry : act.FP_Areas_of_Practice__c.split(';') ) {
                        if (first) {
                            strOutput += entry;
                            first = false;
                        }
                    	else
                        	strOutput += ', ' + entry;
                	}
                }
                
                // SMSF Areas of Practice for SMSF Searches 
                if (String.isNotBlank(act.SMSF_Areas_of_Practice__c) && strSelectedMemberType == STR_MEMBERTYPE_SMSF_SPECIALIST ) {
                    for (String entry : act.SMSF_Areas_of_Practice__c.split(';') ) {
                        if (first) {
                            strOutput += entry;
                            first = false;
                        }
                    	else
                        	strOutput += ', ' + entry;
                	}
                }
                
                
                return strOutput;
            }
            set;
        }
        
        // Rev 01 end
        
        private String capInitialLetters(String strInput){
            String strOutput = '';
            //for(String str : strInput.split('\\W+')){
            for(String str : strInput.split('\\s')){
                strOutput += str.toLowerCase().capitalize() + ' ';
            }
            return strOutput.substring(0, strOutput.length() - 1);
        }
    }
}