/*
    Developer: WDCi (KH)
    Development Date: 01/08/2016
    Task #: Supplementary wizard - Main controller
    
    Change History:
    LCA-1093 08/03/2017 - WDCi LKoh: Supplementary Exam will not show if the current user is not eligible
*/
public without sharing class luana_SupplementaryWizardController {

    // LCA-1093
    public static Boolean error {get;set;}

    public luana_CommUserUtil commUserUtil;
    public Id custCommConId {get; private set;}
    public Id custCommAccId {get; private set;}
    
    public String custCommCountry;
    public String currentyType {get; set;}
    public Boolean isFOC {get; set;}
    
    public String selectedCourseId {set; get;}
    public String selectedModuleName {get; set;}
    public String selectedExamLocationId {set; get;}
    public String selectedExamLocation {set; get;}
    
    public String selectedProductType {set; get;}
    
    public Map<Id, Course_Delivery_Location__c> examLocationMap {get; private set;}
    public Map<String, Luana_Extension_Settings__c> luanaConfigs;
    
    public LuanaSMS__Student_Program__c selectedSP {public get; private set;}
    public LuanaSMS__Student_Program__c workingStudentProgram {get; set;}
    
    public Boolean poRequirePayment {get; set;}
    
    public List<LuanaSMS__Student_Program_Subject__c> currentSPSList = new List<LuanaSMS__Student_Program_Subject__c>();
    
    public PricebookEntry selectedPricebookEntry {set; get;}
    public decimal totalAmount {set; get;}
    public decimal courseFee {set; get;}
    public String currencyKey;
    public String pricebookKey;
    public String currencyName {set; get;}
    
    public String orderNumber {set; get;}
    public Order newOrder {set; get;}
    
    public boolean paymentProcessed {public set; public get;}
    
    public List<LuanaSMS__Student_Program__c> tempSPList {get; set;}
    
    public boolean getIsInternal(){
        return luana_NetworkUtil.isInternal();
    }
    
    public String getPaymentUrl(){
        if(luanaConfigs.containsKey(luana_EnrolmentConstants.PAYMENT_GATEWAY_URL_KEY)){
            return luanaConfigs.get(luana_EnrolmentConstants.PAYMENT_GATEWAY_URL_KEY).Value__c;
        }
        return null;
    }
    
    public luana_SupplementaryWizardController(){
        
        ERROR = false;
        
        commUserUtil = new luana_CommUserUtil(UserInfo.getUserId());
        
        this.custCommConId = commUserUtil.custCommConId;       
        this.custCommAccId = commUserUtil.custCommAccId;
        custCommCountry = commUserUtil.custCommCountry;
        
        pricebookKey = 'Pricebook_' + this.custCommCountry;
        currencyKey = 'Currency_' + this.custCommCountry;
        
        luanaConfigs = Luana_Extension_Settings__c.getAll();
        isFOC = false;
        
        if(custCommCountry == 'Australia'){
            currentyType = 'AUD';
        }else if(custCommCountry == 'New Zealand'){
            currentyType = 'NZD';
        }
        
        poRequirePayment = false;
        
        if(getStudentProgramIdFromURL() != null){
            try{
                
                workingStudentProgram = new LuanaSMS__Student_Program__c();
                examLocationMap = new Map<Id, Course_Delivery_Location__c>();
                selectedProductType = luana_EnrolmentConstants.PRODUCTTYPE_CAMODULE;
                
                tempSPList = [Select Id, Name, Supp_Exam_Charge__c, Supp_Exam_Eligible__c, Course_Code__c, Accept_terms_and_conditions__c, Opt_In__c, Exam_Location__c,
                                LuanaSMS__Course__c, LuanaSMS__Course__r.Name, LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.Default_Program_Offering_Product__c,
                                LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.Order_Item_Quantity__c, LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.AU_Product_Supp_Exam__c, 
                                LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.NZ_Product_Supp_Exam__c, LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.INT_Product_Supp_Exam__c,
                                LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.Require_Payment__c, 
                                LuanaSMS__Course__r.Supp_Exam_Enrolment_End_Date__c, LuanaSMS__Course__r.Supp_Exam_Enrolment_Start_Date__c, LuanaSMS__Course__r.Supp_Exam_Netsuite_Event_Code_NZ__c, LuanaSMS__Course__r.Supp_Exam_Netsuite_Event_Code_AU__c,
                                LuanaSMS__Contact_Student__c, // LCA-1093
                                (select id, LuanaSMS__Subject__c, LuanaSMS__Subject__r.Name, Completed__c, Score__c, 
                                    Result__c, Merit__c, Decile_Band__c, LuanaSMS__Outcome_National__c, Module_Exemption_Without_Case__c, Module_Exemption_Case__c
                                     from LuanaSMS__Student_Program_Subjects__r) 
                                from LuanaSMS__Student_Program__c Where Id =: getStudentProgramIdFromURL() 
                                and Supp_Exam_Special_Consideration_Approved__c = 'Approved' and Sup_Exam_Enrolled__c = false];


                // LCA-1093 Check if the current Account is the owner of the SP, a student/employee should not be able to enrol Supp Exam for someone else
                if(!tempSPList.isEmpty() && custCommConId == tempSPList[0].LuanaSMS__Contact_Student__c && (tempSPList[0].LuanaSMS__Course__r.Supp_Exam_Enrolment_End_Date__c == NULL || tempSPList[0].LuanaSMS__Course__r.Supp_Exam_Enrolment_End_Date__c >= system.today())) {

                    selectedSP = tempSPList[0];
                    poRequirePayment = tempSPList[0].LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.Require_Payment__c;
                    
                    selectedCourseId = selectedSP.LuanaSMS__Course__c;
                    
                    setProgramOfferingPricing();
                    if(selectedSP.Supp_Exam_Charge__c == 'FOC'){
                        courseFee = 0;
                        isFOC = true;
                    }else if(selectedSP.Supp_Exam_Charge__c == 'Payable'){
                        isFOC = false;
                    }
                        
                    workingStudentProgram.Id = selectedSP.Id;
                                   
                    for(LuanaSMS__Student_Program_Subject__c sps: selectedSP.LuanaSMS__Student_Program_Subjects__r){
                        currentSPSList.add(sps);
                        selectedModuleName = sps.LuanaSMS__Subject__r.Name;
                    }                   
                } else {
                    error = true;
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'No record found! Please try again later or contact our support if the problem persists.'));                        
                }
            }Catch(Exception exp){
                error = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'You do not have the permission to view the record. Please try again later or contact our support if the problem persists. ' + exp.getStackTraceString() +' - ' + exp.getMessage() + ' - ' + tempSPList));    
            }
        }
    }
    
    
    public String getStudentProgramIdFromURL(){
        if(ApexPages.currentPage().getParameters().containsKey('spid')){
            return ApexPages.currentPage().getParameters().get('spid');
            
        }
        return null;
    }
    
    public String getDomainUrl(){
        return luana_NetworkUtil.getCommunityPath();
    }
    

    
    public void setProgramOfferingPricing(){
        String productId = '';
        String soqlFilter = '';
        selectedPricebookEntry = null;
        totalAmount = 0;
        courseFee = 0;
        
        if(selectedSP.LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.Default_Program_Offering_Product__c != null){
            pricebookKey = 'Pricebook_' + selectedSP.LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.Default_Program_Offering_Product__c;
            currencyKey = 'Currency_' + selectedSP.LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.Default_Program_Offering_Product__c;
        }
        
        if(luanaConfigs.containsKey(pricebookKey) && luanaConfigs.containsKey(currencyKey)){
            
            if(luanaConfigs.get(currencyKey).Value__c == 'AUD'){
                productId = selectedSP.LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.AU_Product_Supp_Exam__c;
            } else if(luanaConfigs.get(currencyKey).Value__c == 'NZD'){
                productId = selectedSP.LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.NZ_Product_Supp_Exam__c;
                
            } else if(luanaConfigs.get(currencyKey).Value__c == 'INT'){
                productId = selectedSP.LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.INT_Product_Supp_Exam__c;
            }
            
            if(productId != null){
                currencyName = (luanaConfigs.containsKey(luanaConfigs.get(currencyKey).Value__c) ? luanaConfigs.get(luanaConfigs.get(currencyKey).Value__c).Value__c : 'Australia Dollar (AUD)');
                
                soqlFilter = 'Product2Id = \'' + productId + '\' AND Pricebook2Id = \'' + luanaConfigs.get(pricebookKey).Value__c + '\'';     
                for(PricebookEntry pbe : Database.query('select id, unitprice, pricebook2id, product2.NetSuite_Internal_Id__c, product2.NetSuite_Pricing_Level__c from pricebookentry where ' + soqlFilter)){
                    selectedPricebookEntry = pbe;
                    
                    integer quantity = 1;
                    totalAmount = selectedPricebookEntry.UnitPrice * quantity;
                    courseFee = selectedPricebookEntry.UnitPrice * quantity;
                    break;
                }
                
            }
        }
    }
    
    public List<SelectOption> getExamLocationOptions(){
                
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--None--'));
        List<Course_Delivery_Location__c> courseDeliveryLocations = luana_DeliveryLocationUtil.getCourseSuppExamDeliveryLocation(selectedCourseId);
                
        for(Course_Delivery_Location__c dl: courseDeliveryLocations){
            if(dl.Type__c == 'Supp Exam Location'){
                SelectOption option = new SelectOption(dl.Id, dl.Delivery_Location__r.LuanaSMS__Delivery_Location_Name__c);
                options.add(option); 
                examLocationMap.put(dl.Id, dl);
            }
        }
        return options;
    }
    
    public Order createNewOrder(){
        try{
            newOrder = new Order();
            
            newOrder.EffectiveDate = system.today();
            newOrder.AccountId = custCommAccId;
            newOrder.Status = 'Draft';
            newOrder.Pricebook2Id = selectedPricebookEntry.Pricebook2Id;
            newOrder.Student_Program__c = selectedSP.Id;
            newOrder.Type = 'Supplementary Exam';
            
            insert newOrder;
            
            
            List<OrderItem> newOrderItems = new List<OrderItem>();
                    
            OrderItem newOrderItem = new OrderItem();
            newOrderItem.OrderId = newOrder.Id;
            newOrderItem.PricebookEntryId = selectedPricebookEntry.Id;
            newOrderItem.UnitPrice = selectedPricebookEntry.UnitPrice;
            
            newOrderItem.Quantity = 1;
            newOrderItem.NetSuite_Product_ID__c = selectedPricebookEntry.Product2.NetSuite_Internal_Id__c;
            newOrderItem.NetSuite_Pricing_Level__c = selectedPricebookEntry.Product2.NetSuite_Pricing_Level__c;
            newOrderItem.Start_Date__c = selectedSP.LuanaSMS__Course__r.Supp_Exam_Enrolment_Start_Date__c;
            newOrderItem.End_Date__c = selectedSP.LuanaSMS__Course__r.Supp_Exam_Enrolment_End_Date__c;
            newOrderItem.Netsuite_Event_Code__c = (custCommCountry != null && custCommCountry == 'New Zealand' ? selectedSP.LuanaSMS__Course__r.Supp_Exam_Netsuite_Event_Code_NZ__c : selectedSP.LuanaSMS__Course__r.Supp_Exam_Netsuite_Event_Code_AU__c);
            newOrderItems.add(newOrderItem);
            
            insert newOrderItems;
            return newOrder;
            
        }Catch(Exception exp){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Fail to update the Order and Order Item. Please try again later or contact our support if the problem persists. ' + exp.getStackTraceString() +' - ' + exp.getMessage()));    
            return null;
        }
    }
    
    public void updateCurrentSP(){
        try{
            
            workingStudentProgram.Supp_Exam_Location__c = selectedExamLocationId;
            workingStudentProgram.Sup_Exam_Enrolled__c = true;
            if(selectedSP.Supp_Exam_Charge__c == 'FOC'){
                workingStudentProgram.Supp_Exam_Paid__c = true;
            }
            update workingStudentProgram;
            
        }Catch(Exception exp){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Fail to update the Student Program. Please try again later or contact our support if the problem persists. ' + exp.getStackTraceString() +' - ' + exp.getMessage()));    
        }
    }
    
    
       
}