public without sharing class changePasswordApex {
    @AuraEnabled
    public static Boolean changePasswordApexMethod() {
        Id loggedinuserid = [Select id from User where id =: UserInfo.getUserId()].id;
        system.debug('loggedinuser***** '+loggedinuserid);
        System.ResetPasswordResult result = System.resetPassword(loggedinuserid, true);
        system.debug('mail sent $$$$$$$$$$$$$$$$');
        return true;
    }
}