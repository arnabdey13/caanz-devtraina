/**
    Developer: WDCi (Lean)
    Development Date: 27/04/2016
    Task #: Attachment cache holder for enrolment wizard LCA-282
**/

public without sharing class luana_AttachmentCache {
    
    Map<String, Attachment_Cache__c> attachmentCaches;
    String relatedAccountId;
    
    public luana_AttachmentCache(String relatedAccountId){
    	attachmentCaches = new Map<String, Attachment_Cache__c>();
    	this.relatedAccountId = relatedAccountId;
    }
    
    public Map<String, Attachment_Cache__c> getAttachmentCaches(){
    	return attachmentCaches;
    }
    
    public void addAttachmentCache(List<Attachment> atts, String sObjType){
    	
    	List<Attachment_Cache__c> oldAttachmentCaches = new List<Attachment_Cache__c>();
    	List<Attachment_Cache__c> newAttachmentCaches = new List<Attachment_Cache__c>();
    	List<Attachment> newAttachments = new List<Attachment>();
    	
    	for(Attachment att : atts){
	    	
	    	String key = sObjType + '_' + att.Name;
	    	//remove the old cache from map
	    	if(attachmentCaches.containsKey(key)){
	    		Attachment_Cache__c oldAttCache = attachmentCaches.get(key);
	    		
	    		attachmentCaches.remove(key);
	    		oldAttachmentCaches.add(oldAttCache);
	    	}
	    	
	    	//initialise new cache then
	    	Attachment_Cache__c newAttCache = new Attachment_Cache__c(Name = att.Name, Account__c = relatedAccountId, Object__c = sObjType);
	    	newAttachmentCaches.add(newAttCache);
    	}
    	
    	//delete the old cache
    	if(oldAttachmentCaches.size() > 0){
    		delete oldAttachmentCaches;
    	}
    	
    	//insert new cache and related attachment
    	if(newAttachmentCaches.size() > 0){
    		insert newAttachmentCaches;
    		
    		integer counter = 0;
    		for(Attachment att : atts){
    			att.ParentId = newAttachmentCaches.get(counter).Id;
    		}
    		
    		insert atts;
    		
    		counter = 0;
    		for(Attachment_Cache__c attCache : newAttachmentCaches){
    			String key = sObjType + '_' + atts.get(counter).Name;
    			
    			attCache.Attachment_Id__c = atts.get(counter).Id;
    			attachmentCaches.put(key, attCache);
    		}
    	}
    }
    
    public boolean hasAttachment(){
    	return !attachmentCaches.isEmpty();
    }
    
    public List<Attachment> getAttachments(String sObjType){
    	
    	Set<String> attIds = new Set<String>();
    	for(Attachment_Cache__c attCache : attachmentCaches.values()){
    		if(attCache.Object__c == sObjType){
    			attIds.add(attCache.Attachment_Id__c);
    		}
    	}
    	
    	return [select Id, Body, Name, ParentId from Attachment where id in: attIds];
    	
    }
    
    public void close(){
    	delete attachmentCaches.values();
    }
}