/**
    Developer: WDCi (kh)
    Development Date:28/11/2016
    Task: Library for ASS Test class

**/
@isTest
public class AAS_DataPrep_Test {
    
    public AAS_Settings__c createAASCustomSetting(String key, String value){
        AAS_Settings__c assSetting = new AAS_Settings__c();
        assSetting.Name = key;
        assSetting.Value__c = value;
        return assSetting;
    }
    
    public List<AAS_Settings__c> defauleCustomSetting(){
        List<AAS_Settings__c> assSettingList = new List<AAS_Settings__c>();
        assSettingList.add(createAASCustomSetting('Assessment Data Upload Report Id', 'test_assessment_data_upload_report'));
        assSettingList.add(createAASCustomSetting('Borderline Remark Report Id', 'test_Borderline_Remark_Report '));
        assSettingList.add(createAASCustomSetting('Cohort Report Id', 'test_cohort_report'));
        assSettingList.add(createAASCustomSetting('Default Borderline Range', '-5'));
        assSettingList.add(createAASCustomSetting('Default Cohort Adjustment', '0'));
        assSettingList.add(createAASCustomSetting('Default Passing Mark', '50'));
        assSettingList.add(createAASCustomSetting('Default Supplementary Exam Range', '-3'));
        assSettingList.add(createAASCustomSetting('Exam Result Release Report Id', 'test_exam_result_release_report'));
        assSettingList.add(createAASCustomSetting('Exceed Full Mark Report Id', 'test_exceed_full_mark_report'));
        assSettingList.add(createAASCustomSetting('Final Adjustment Report Id', 'test_final_adjustment_report'));
        assSettingList.add(createAASCustomSetting('Supp Exam Eligibility Report Id', 'test_supp_exam_eligibility_report'));
        assSettingList.add(createAASCustomSetting('Supp Exam Result Release Report Id', 'test_supp_exam_result_release_report'));
        
        return assSettingList;
    }
    
    public Map<String, Id> getRecordTypeMap(String name){
        Map<String, Id> recordTypeMap = new Map<String, Id>();
        List<RecordType> rtList = Database.Query('Select Id, Name, DeveloperName, SObjectType from RecordType Where Name Like \'%' + name + '%\'');
        for(RecordType rt: rtList){
            recordTypeMap.put(rt.SObjectType+':'+rt.DeveloperName, rt.Id);
        }
        return recordTypeMap;
    }
    
    
    public AAS_Course_Assessment__c createCourseAssessment(Id cId, String caName){
        
        AAS_Course_Assessment__c ca = new AAS_Course_Assessment__c();
        ca.AAS_Course__c = cId;
        ca.Name = caName;
        return  ca;
    }
    
    public AAS_Assessment_Schema_Section__c createAsessmentSchemaSection(Id caId, String assName, Id rtId){
        
        AAS_Assessment_Schema_Section__c ass = new AAS_Assessment_Schema_Section__c();
        ass.AAS_Course_Assessment__c = caId;
        ass.Name = assName;
        ass.RecordTypeId = rtId;
        
        return ass;
    }
    
    public AAS_Assessment_Schema_Section_Item__c createAssessmentSchemaSectionItems(Id assId, Id rtId, Integer fullMark, String assiName){
        AAS_Assessment_Schema_Section_Item__c assi = new AAS_Assessment_Schema_Section_Item__c();
        assi.AAS_Assessment_Schema_Section__c = assId;
        assi.RecordTypeId = rtId;
        assi.AAS_Full_Mark__c = fullMark;
        assi.Name = assiName;
        return assi;
    }    
    
    public AAS_Student_Assessment__c createStudentAssessment(Id caId){
        
        AAS_Student_Assessment__c sa = new AAS_Student_Assessment__c();
        sa.AAS_Course_Assessment__c = caId;
        
        return sa;
    }
    
    public AAS_Student_Assessment_Section__c createStudentAssessmentSection(Id saId, Id rtId){
        
        AAS_Student_Assessment_Section__c sas = new AAS_Student_Assessment_Section__c();
        sas.AAS_Student_Assessment__c = saId;
        sas.RecordTypeId = rtId;
        return sas;
    }
    
    public AAS_Student_Assessment_Section_Item__c createStudentAssessmentSectionItem(Id sasId, Id rtId, Id assId){
        
        AAS_Student_Assessment_Section_Item__c sasi = new AAS_Student_Assessment_Section_Item__c();
        sasi.AAS_Student_Assessment_Section__c = sasId;
        sasi.AAS_Assessment_Schema_Section_Item__c = assId;
        sasi.RecordTypeId = rtId;
        return sasi;
    }
    
    public LuanaSMS__Course__c createLuanaCourse(Id poId, Luana_DataPrep_Test dataPrepUtil){
        
        LuanaSMS__Course__c course1 = dataPrepUtil.createNewCourse('AAA216', poId, dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        course1.LuanaSMS__Allow_Online_Enrolment__c = true;
        course1.Supp_Exam_Enrolment_End_Date__c = system.today();
        return course1;
        
    }
    
    public List<LuanaSMS__Course__c> createGenericLuanaAMCourse(Integer amount, Id poId, Luana_DataPrep_Test dataPrepUtil){
        
        Id recordTypeID = dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module');
        
        List<LuanaSMS__Course__c> courseList = new List<LuanaSMS__Course__c>();
        for (Integer i = 0; i < amount; i++) {
            LuanaSMS__Course__c course1 = dataPrepUtil.createNewCourse('TestCourse'+i, poId, recordTypeID, 'Running');
            course1.LuanaSMS__Allow_Online_Enrolment__c = true;
            course1.Supp_Exam_Enrolment_End_Date__c = system.today();
            courseList.add(course1);
        }
        return courseList;
    }
    
    public LuanaSMS__Student_Program__c createLuanaStudProgram(Id courseId, Id custCommConId, Luana_DataPrep_Test dataPrepUtil){
        
        LuanaSMS__Student_Program__c studProgram1 = dataPrepUtil.createNewStudentProgram(dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), custCommConId, courseId, '', 'Fail');

        return studProgram1;
        
    }
    
    
    public void initialMember(String runSeqNo, String contextLongName, String contextShortName, Luana_DataPrep_Test dataPrepUtil){
        
        
        //Create all the custom setting
         
        insert dataPrepUtil.createLuanaConfigurationCustomSetting();
        
        List<Luana_Extension_Settings__c> customSessingList = dataPrepUtil.prepLuanaExtensionSettingCustomSettings();
        customSessingList.add(new Luana_Extension_Settings__c(Name = 'AdobeConnect_URL', Value__c = '  https://connect-staging.charteredaccountantsanz.com'));
        insert customSessingList;
        
        //Create user with Member and Employer community access
        Account memberAccount = dataPrepUtil.generateNewApplicantAcc('Joe_' + contextShortName, contextLongName, 'Full_Member');
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.Assessible_for_CA_Program__c = true;
        memberAccount.PersonEmail = 'joe_'+runSeqNo+'_' + contextShortName +'@gmail.com';//LCA-921
        insert memberAccount;
        
        System.debug('******memberAccount:: ' + memberAccount);
        
        //commProfIdMap = new Map<String, Id>();
        //for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
        //    commProfIdMap.put(prof.Name, prof.Id);
        //}
    }
    
    public Map<String, SObject> createBasicAssets(String contextLongName, String contextShortName, Luana_DataPrep_Test dataPrepUtil, AAS_DataPrep_Test aasDataPrepUtil) {
        
        Map<String, SObject> assetMap = new Map<String, SObject>();
        
        //Luana_DataPrep_Test dataPrepUtil = new Luana_DataPrep_Test();
        //AAS_DataPrep_Test aasDataPrepUtil = new AAS_DataPrep_Test();
        
        system.debug('UserInfo: ' +UserInfo.getUserId());
        
        luana_CommUserUtil userUtil = new luana_CommUserUtil(UserInfo.getUserId());
        
        // Training Org
        LuanaSMS__Training_Organisation__c trainOrg = dataPrepUtil.createNewTraningOrg(contextLongName, contextLongName, contextShortName, 'address line 1', 'address loc 1', '5000');
        insert trainOrg;
        assetMap.put('TRAININGORG1', trainOrg);
        
        // Program
        LuanaSMS__Program__c prog = dataPrepUtil.createNewProgram(contextShortName, contextLongName, 'Confirmed', 'Graduate diploma', 'Nationally accredited qualification specified in a national training package');
        insert prog;
        assetMap.put('PROGRAM1', prog);

        // Program Offering
        LuanaSMS__Program_Offering__c po1 = dataPrepUtil.createNewProgOffering('PO_AM_' + contextShortName, dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), prog.Id, trainOrg.Id, null, null, null, 1, 1);
        po1.IsCapstone__c = true;
        insert po1;
        assetMap.put('PROGRAMOFFERRING1', po1);
        
        return assetMap;
    }
    
    public Map<String, SObject> createBasicAssetsPart2(String contextLongName, String contextShortName, Luana_DataPrep_Test dataPrepUtil, AAS_DataPrep_Test aasDataPrepUtil, Map<String, SObject> basicAssetsMap) {

        // Course
        LuanaSMS__Course__c course1 = dataPrepUtil.createNewCourse('AAA216', basicAssetsMap.get('PROGRAMOFFERRING1').Id, dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        course1.LuanaSMS__Allow_Online_Enrolment__c = true;
        course1.Supp_Exam_Enrolment_End_Date__c = system.today();
        insert course1;
        basicAssetsMap.put('COURSE1', course1);
  
        // Course Assessment
        AAS_Course_Assessment__c ca1 = aasDataPrepUtil.createCourseAssessment(course1.Id, 'CA_' + contextShortName);
        ca1.AAS_Module_Passing_Mark__c = 40;
        ca1.AAS_Non_Exam_Data_Loaded__c = true;
        ca1.AAS_Exam_Data_Loaded__c = true;
        ca1.AAS_Cohort_Adjustment_Exam__c = true;
        ca1.AAS_Borderline_Remark__c = true;
        insert ca1;
        basicAssetsMap.put('COURSEASSESSMENT1', ca1);
        
        // Student Program
        Account memberAccount = (Account)basicAssetsMap.get('MEMBERACCOUNT1');
        LuanaSMS__Student_Program__c studProgram1 = dataPrepUtil.createNewStudentProgram(dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), memberAccount.PersonContactId, course1.Id, '', 'Fail');
        studProgram1.Paid__c = true;
        studProgram1.Supp_Exam_Broadcasted__c = false;
        studProgram1.Sup_Exam_Enrolled__c = false;
        studProgram1.Supp_Exam_Special_Consideration_Approved__c = 'Approved';
        studProgram1.Supp_Exam_Eligible__c = false;
        insert studProgram1;
        basicAssetsMap.put('STUDENTPROGRAM1', studProgram1);
        
        // Create the Student Assessment
        AAS_Student_Assessment__c sa1 = aasDataPrepUtil.createStudentAssessment(ca1.Id);
        sa1.AAS_Student_Program__c = studProgram1.Id;
        sa1.AAS_Final_Result__c = 'Pass';
        insert sa1;
        basicAssetsMap.put('STUDENTASSESSMENT1', sa1);
        
        // Create the Student Assessment Section
        AAS_Student_Assessment_Section__c sas1 = aasDataPrepUtil.createStudentAssessmentSection(sa1.Id, dataPrepUtil.getRecordTypeIdMap('AAS_Student_Assessment_Section__c').get('Exam_Assessment_SAS'));
        insert sas1;
        basicAssetsMap.put('STUDENTASSESSMENTSECTION1', sas1);
        
        
        return basicAssetsMap;
    }
}