/* 
	Developer: WDCi (Lean)
    Development Date: 06/07/2016
    Task #: LCA-487 Custom communities login page
*/

global with sharing class luana_LoginController {
    
    global String username {get;set;}
    global String password {get;set;}
    
    public AuthProvider fbAuthProviders {get;set;}
    public AuthProvider linkedInAuthProviders {get;set;}
    public String siteURL  {get; set;}
    public String startURL {get; set;}
    public String orgId {get; set;}
    
    global luana_LoginController(){
    	
    	siteURL  = Site.getBaseUrl();
        startURL = System.currentPageReference().getParameters().get('startURL');
        orgId = UserInfo.getOrganizationId();
        
        if (startURL == null){
        	startURL = '/';
        }
        
    	for(AuthProvider authProv : [SELECT Id,DeveloperName,FriendlyName,ProviderType FROM AuthProvider]){
    		if(authProv.DeveloperName == 'facebook'){
    			fbAuthProviders = authProv;
    		} else if(authProv.DeveloperName == 'linkedin'){
    			linkedInAuthProviders = authProv;
    		}
    	}
    }
    
    global PageReference forwardToCustomAuthPage() {
        return new PageReference('/luana_Login');
    }
    
    global PageReference login() {
        return Site.login(username, password, startURL);
    }
    
    global String getEncodedSiteUrl() {
        return EncodingUtil.urlEncode(siteURL, 'UTF-8');
    }
    
    global String getEncodedStartUrl() {
        return EncodingUtil.urlEncode(startURL, 'UTF-8');
    }
    
}