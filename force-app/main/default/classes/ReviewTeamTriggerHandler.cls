public without sharing class ReviewTeamTriggerHandler {
    public void onAfterUpdate(List<Review_Team__c> oldReviewTeamList, List<Review_Team__c> newReviewTeamList,
            Map<ID, Review_Team__c> oldReviewTeamMap , Map<ID, Review_Team__c> newReviewTeamMap ){

        //If no updated reviewers -- just don't bother.
        if (newReviewTeamList.size() == 0) {
            return;
        }

        Map<ID, Review_Team__c> notifiedDateAddedMap = new Map<ID, Review_Team__c>();
        Map<ID, Review_Team__c> accessRevokedMap = new Map<ID, Review_Team__c>();
        Set<ID> accessRevokedReviews = new Set<ID>();

        for (Review_Team__c newReviewTeam : newReviewTeamList) {
            Review_Team__c oldReviewTeam = oldReviewTeamMap.get(newReviewTeam.Id);
            if(newReviewTeam.Reviewer_Notified_Date__c != null && oldReviewTeam.Reviewer_Notified_Date__c == null){
                notifiedDateAddedMap.put(newReviewTeam.ID, newReviewTeam);
                System.debug('@@@ Notified date not blank for ' + newReviewTeam.Name);
            }
            else if (newReviewTeam.Revoke_Access__c && !oldReviewTeam.Revoke_Access__c) {
                //Need to use the old one because in the BEFORE trigger, we've already reparented.
                accessRevokedMap.put(oldReviewTeam.ID, oldReviewTeam);
                accessRevokedReviews.add(oldReviewTeam.review__c);
                System.debug('@@@ Revoking access for ' + oldReviewTeam.Name);
            }
        }

        // Given a List of Reviewers (ie Review_teams) -- return a map of Account to User.
        Map<ID,ID> accountsToUsersMap = ReviewTriggerHandler.getAccountsToUsersMap(notifiedDateAddedMap.values());

        List<Review__Share> notifiedDateAddedShares = new List<Review__Share>();
        for (Review_Team__c reviewer: notifiedDateAddedMap.values()) {
            Review__Share share = createShareForReview(reviewer.review__c, accountsToUsersMap.get(reviewer.reviewer__c), 'Edit');
            notifiedDateAddedShares.add(share);
        }
        //Create!
        for (Review__Share debugItem: notifiedDateAddedShares) {
            System.debug('Sharing: User id:' + debugItem.UserOrGroupId + ' Review id: ' + debugItem.ParentId);
        }
        try {
            Database.insert(notifiedDateAddedShares);
        } catch (Exception e) {
            //do nothing... just continue
        }


        List<Review__Share> accessRevokedShares = ReviewTriggerHandler.findSharesForReviewers(accessRevokedMap.values());
        for (Review__Share debugItem: accessRevokedShares) {
            System.debug('Deleting review share for: User id:' + debugItem.UserOrGroupId + ' Review id: ' + debugItem.ParentId);
        }
        try {
            Database.delete(accessRevokedShares);
        } catch (Exception e) {
            //do nothing... just continue
        }

        // Get a map of Map<REVIEWID, QUESTIONNAIREID>
        Map<ID, Review__c> reviewToQuestionnaireMap = new Map<ID, Review__c>([select id, QPR_Questionnaire__c from Review__c where id in :accessRevokedReviews]);

        List<QPR_Questionnaire__Share> accessRevokedQuestionnaireShares = ReviewTriggerHandler.findQuestionnaireSharesForReviewers(accessRevokedMap.values(), reviewToQuestionnaireMap);
        for (QPR_Questionnaire__Share debugItem: accessRevokedQuestionnaireShares) {
            System.debug('Deleting questionnaire share for: User id:' + debugItem.UserOrGroupId + ' Review id: ' + debugItem.ParentId);
        }
        try {
            Database.delete(accessRevokedQuestionnaireShares);
        } catch (Exception e) {
            //do nothing... just continue
        }

    }

    public void onBeforeUpdate(List<Review_Team__c> oldReviewTeamList, List<Review_Team__c> newReviewTeamList,
            Map<ID, Review_Team__c> oldReviewTeamMap , Map<ID, Review_Team__c> newReviewTeamMap ){
        System.debug('@@@ Before update' );

        //If no updated reviewers -- just don't bother.
        if (oldReviewTeamList.size() == 0) {
            return;
        }

        Map<ID, Review_Team__c> accessRevokedMap = new Map<ID, Review_Team__c>();
        Set<ID> accessRevokedReviews = new Set<ID>();



        for (Review_Team__c newReviewTeam : newReviewTeamList) {
            Review_Team__c oldReviewTeam = oldReviewTeamMap.get(newReviewTeam.Id);
            if (newReviewTeam.Revoke_Access__c && !oldReviewTeam.Revoke_Access__c) {
                accessRevokedMap.put(newReviewTeam.ID, newReviewTeam);
                accessRevokedReviews.add(newReviewTeam.review__c);
                System.debug('@@@ Before update - Reparenting reviewer with revoked access ' + newReviewTeam.Name);
            }
        }

        reparentReviewTeam(accessRevokedMap.values(), accessRevokedReviews);

    }




//      Reparenting the reviewTeam item.  This is the equivalent of deleting.
//      A conflict review must be set up in advance with the correct record type and country
//      Note -- it is assumed that this method is called only once for the trigger since it sets up the initial
//      reviews to reparent.

    private void reparentReviewTeam(List<Review_Team__c> reviewTeams, Set<ID> reviews) {
        Schema.RecordTypeInfo RecordTypeConflictsReview = Schema.SObjectType.Review__c.getRecordTypeInfosByName().get('Conflicts Review');
        //If this doesn't exist -- then the record type hasn't been set up and nothing will work.  So just exit.
        if (RecordTypeConflictsReview == null) {
            System.debug('@@@ CANNOT REPARENT -- PLEASE CREATE CONFLICTS REVIEW RECORD TYPE ');
            return;
        }
        Id RecordTypeConflictsReviewId = RecordTypeConflictsReview.getRecordTypeId();

        Review__c conflictReviewAu = null;
        Review__c conflictReviewNz = null;

        List<Review__c> conflictReviewsAu = [select id from Review__c
                    where RecordTypeid = :RecordTypeConflictsReviewId
                    and Review_Country__c =:'AU'];
        if (conflictReviewsAu.size() > 0) {
            //Just get the first one.  There are only supposed to be one -- but if more, pick one.
            conflictReviewAu = conflictReviewsAu[0];
        } else {
            System.debug('@@@ NO CONFLICTS REVIEW RECORD FOR AU -- PLEASE CREATE REVIEW RECORD WITH COUNTRY AU AND RECORDTYPE CONFLICTS REVIEW ');
        }

        List<Review__c> conflictReviewsNz = [select id from Review__c
        where RecordTypeid = :RecordTypeConflictsReviewId
        and Review_Country__c =:'NZ'];
        if (conflictReviewsNz.size() > 0) {
            //Just get the first one.  There are only supposed to be one -- but if more, pick one.
            conflictReviewNz = conflictReviewsNz[0];
        } else {
            System.debug('@@@ NO CONFLICTS REVIEW RECORD FOR AU -- PLEASE CREATE REVIEW RECORD WITH COUNTRY AU AND RECORDTYPE CONFLICTS REVIEW ');
        }


        for (Review_Team__c reviewTeam: reviewTeams) {
            String reviewerCountry = reviewTeam.Review_Country__c;
            if (reviewerCountry == 'NZ') {
                if (conflictReviewNz != null) {
                    reviewTeam.Review__c = conflictReviewNz.Id;
                    System.debug('@@@ Reparenting review team to NZ: ' + reviewTeam);
                }
            }
            else if (reviewerCountry == 'AU') {
                if (conflictReviewNz != null) {
                    reviewTeam.Review__c = conflictReviewAu.Id;
                    System.debug('@@@ Reparenting review team to AU: ' + reviewTeam);
                }
            } else {
                System.debug('@@@ Review team does not have a country of NZ or AU so cannot be reparented: ' + reviewTeam);
            }
        }
    }



    private Review__Share createShareForReview(ID reviewToShare, ID userToShareTo,
            String accessLevel ) {

        Review__Share newShare = new Review__Share();
        newShare.parentId = reviewToShare;
        newShare.UserOrGroupId = userToShareTo;
        newShare.AccessLevel = accessLevel;
        newShare.RowCause = Schema.Review__Share.RowCause.Auto_share_review_record__c;

        return newShare;
    }

}