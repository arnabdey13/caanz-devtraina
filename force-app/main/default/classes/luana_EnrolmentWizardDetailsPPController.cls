/**
    Developer: WDCi (Lean)
    Development Date: 05/04/2016
    Task #: Enrollment wizard - Controller for Details PPP
    
    Change history
    pppr06 14/09/2016 WDCi - KH: Remove workshop preference selection from this page 
**/

public without sharing class luana_EnrolmentWizardDetailsPPController extends luana_EnrolmentWizardObject{
    
    luana_EnrolmentWizardController stdController;
    
    public luana_EnrolmentWizardDetailsPPController(luana_EnrolmentWizardController stdController){
        this.stdController = stdController;
        
    }
    
    public luana_EnrolmentWizardDetailsPPController(){
        
        
    }
    
    public Boolean isRegional3(){
        if(apexpages.currentpage().getparameters().get('regional') == '1'){
            return true;
        }
        return false;
    }
    
    public PageReference detailsPPNext(){
        boolean hasError = false;
        
        try{
            /* pppr06 14/09/2016 WDCi - KH: Remove workshop preference checking 
            if(stdController.selectedWorkshopLocation1Id == null){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure the "Please select your workshop location preference" is selected.'));
                hasError = true;
            }else{
                stdController.workingStudentProgram.Workshop_Location_Preference_1__c = stdController.selectedWorkshopLocation1Id;
            }
            */
        
            if(!stdController.workingStudentProgram.Accept_terms_and_conditions__c){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please agree to the Terms and Conditions before proceeding'));
                hasError = true;
            }
                     
            if(!hasError){
                 PageReference pageRef = stdController.getDetailsNextPage();
                stdController.skipValidation = true;
                
                return pageRef;
            }
        
        }Catch(Exception exp){
        
        }
        return null;
    }
    
    public PageReference detailsPPBack(){
    
        PageReference pageRef;
        stdController.skipValidation = true;
        
        if(stdController.selectedProgramOffering.Require_Subject_Selection__c){
            pageRef = Page.luana_EnrolmentWizardSubject;
        } else {
            stdController.selectedCourseId = null;
            stdController.selectedCourse = null;
            
            pageRef = Page.luana_EnrolmentWizardCourse;
        }
        
        return pageRef;
    }
}