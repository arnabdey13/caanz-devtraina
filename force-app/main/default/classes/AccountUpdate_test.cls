@isTest
private class AccountUpdate_test {
    
static testMethod void validateAccountUpdate()
{   
  
    Account newAccount = new Account(FirstName ='Test', LastName = 'Test', Status__c = 'Active',  PersonBirthdate = Date.today(), Member_ID__c = '0', Member_Of__c = 'ICAA', PersonEmail = 'test@test.com', RecordTypeId = '01290000000OdAH'
                                     //DN 20160415 added Salutation which is now required by validation rules
                                    , Salutation='Mr');
    insert newAccount;
    
    System.debug('New account customer number: ' + newAccount.Id);
    
    Account RetrieveAccount = [SELECT Member_ID__c, Customer_ID__c from account WHERE Id = :newAccount.Id Limit 1];
    
    string sReturn ='';
       sReturn = AccountUpdate.updateCustomerID(RetrieveAccount.Customer_ID__c, 9999991);
     if (sReturn == 'true')
     {
         System.assert(true);
     }
    else
    {
        System.assert(false);
    }
        //Account RetrieveAccount2 = [SELECT Member_ID__c, Customer_ID__c from account WHERE Id = :RetrieveAccount.Id Limit 1];
     //   System.assertEquals('9999991', RetrieveAccount2.Member_ID__c);
    
    sReturn = AccountUpdate.updateCustomerID('test013', 9999991);
    if (sReturn != 'true')
    {
         System.assert(true);
    }
        else
    {
        System.assert(false);
    }
   
}
}