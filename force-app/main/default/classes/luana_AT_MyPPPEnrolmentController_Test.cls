/*
    Developer: WDCi (KH)
    Date: 13/09/2016
    Task #: Test class for luana_MyPPPEnrolmentController
    
*/
@isTest(seeAllData=false)
private class luana_AT_MyPPPEnrolmentController_Test {
    
    private static String classNamePrefixLong = 'luana_AT_MyPPPEnrolmentController_Test';
    private static String classNamePrefixShort = 'lapppe';
    public static Luana_DataPrep_Test testDataGenerator;
    private static map<String, Id> commProfIdMap {get; set;}
    
    public static Account memberAccount {get; set;}
    public static User memberUser {get; set;}
    
    public static Account memberAccountOversea {get; set;}
    public static User memberUserOversea {get; set;}
    
    public static Account nonMemberAccount {get; set;}
    public static User nonMemberUser {get; set;}
    
    static Account businessAccount;
    static LuanaSMS__Training_Organisation__c trainingOrg;
    static LuanaSMS__Program__c program;
    
    static LuanaSMS__Program_Offering__c poMember;
    static LuanaSMS__Program_Offering__c poMemberRegional;
    static LuanaSMS__Program_Offering__c poNonMember;
    static LuanaSMS__Program_Offering__c poMaster;
    
    static LuanaSMS__Course__c pppMemberCourse;
    static LuanaSMS__Course__c pppMemberRegionalCourse;
    static LuanaSMS__Course__c pppNonMemberCourse;
    static LuanaSMS__Course__c pppCourseMaster;
    static LuanaSMS__Student_Program__c newStudProg;
    
    static LuanaSMS__Delivery_Location__c devLocation;
    static Course_Delivery_Location__c courseDevLoc;
    static Account venueAccount;
    static LuanaSMS__Room__c room1;
    static LuanaSMS__Session__c session1;
    static LuanaSMS__Session__c session2;
    static LuanaSMS__Session__c session3;
    
    static void initial(){
        //initialize
        testDataGenerator = new Luana_DataPrep_Test();
        
        //Create all the custom setting
        insert testDataGenerator.prepLuanaExtensionSettingCustomSettings();
        insert testDataGenerator.createLuanaConfigurationCustomSetting();
        
        //Create user with Member and Employer community access
        memberAccount = testDataGenerator.generateNewApplicantAcc('Joe_1_' + classNamePrefixShort, classNamePrefixLong, 'Full_Member');
        memberAccount.Member_Id__c = '123451';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.Assessible_for_CA_Program__c = false;
        memberAccount.PersonEmail = 'joe_1_' +classNamePrefixShort+'@gmail.com';//LCA-921
        memberAccount.PersonHomePhone= '1234';
        memberAccount.PersonOtherStreet= '83 Saggers Road';
        memberAccount.PersonOtherCity='JITARNING';
        memberAccount.PersonOtherState='Western Australia';
        memberAccount.PersonOtherCountry='Australia';
        memberAccount.PersonOtherPostalCode='6365';
        insert memberAccount;
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
    }
    static void initialData(){
        //Create Business Account        
        Luana_Extension_Settings__c defaultUniName = Luana_Extension_Settings__c.getValues('Education Record Default University');
        businessAccount = testDataGenerator.generateNewBusinessAcc(defaultUniName.value__c, 'Business_Account', 'Chartered Accounting','30033003', '123456789', 'NZICA', '107 Bathurst Street', 'Active');
        businessAccount.Ext_Id__c = classNamePrefixShort+'_1';
        insert businessAccount;
    
        trainingOrg = testDataGenerator.createNewTraningOrg(classNamePrefixShort, classNamePrefixShort+ 'org', classNamePrefixShort+ 'org', 'test street', 'test location', '5400');
        insert trainingOrg;
        
        program = testDataGenerator.createNewProgram(classNamePrefixShort, classNamePrefixLong, 'Confirmed', 'Public Practice Program', 'Public Practice Program');
        insert program;
        
        List<LuanaSMS__Program_Offering__c> poList = new List<LuanaSMS__Program_Offering__c>();
        poMember = testDataGenerator.createNewProgOffering('PPP - Member_' + classNamePrefixShort, testDataGenerator.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('PPP'), program.Id, trainingOrg.Id, null, null, null, 1, 1);
        poMember.eLearn_Module_Completion_Total__c = 100;
        poMember.eLearn_Module_CPD_Hours__c = 2;
        poMember.Workshop_Eligibility_for_CPD_Hours__c = 2;
        poList.add(poMember);
        poMemberRegional = testDataGenerator.createNewProgOffering('PPP - Member (Regional)_' + classNamePrefixShort, testDataGenerator.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('PPP'), program.Id, trainingOrg.Id, null, null, null, 1, 1);
        poMemberRegional.eLearn_Module_Completion_Total__c = 100;
        poMemberRegional.eLearn_Module_CPD_Hours__c = 2;
        poMemberRegional.Workshop_Eligibility_for_CPD_Hours__c = 2;
        poList.add(poMemberRegional);
        poNonMember = testDataGenerator.createNewProgOffering('PPP - Non Member_' + classNamePrefixShort, testDataGenerator.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('PPP'), program.Id, trainingOrg.Id, null, null, null, 1, 1);
        poNonMember.eLearn_Module_Completion_Total__c = 100;
        poNonMember.eLearn_Module_CPD_Hours__c = 2;
        poNonMember.Workshop_Eligibility_for_CPD_Hours__c = 2;
        poList.add(poNonMember);
        
        poMaster = testDataGenerator.createNewProgOffering('PPP - Master_' + classNamePrefixShort, testDataGenerator.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('PPP'), program.Id, trainingOrg.Id, null, null, null, 1, 1);
        poMaster.eLearn_Module_Completion_Total__c = 100;
        poMaster.eLearn_Module_CPD_Hours__c = 2;
        poMaster.Workshop_Eligibility_for_CPD_Hours__c = 2;
        poList.add(poMaster);
        
        insert poList;
    }
    
    static void prepareData(){
        
        //Create course
        pppMemberCourse = testDataGenerator.createNewCourse('PPP - Member_' + classNamePrefixShort, poMember.Id, testDataGenerator.getRecordTypeIdMap('LuanaSMS__Course__c').get('PPP'), 'Running');
        pppMemberCourse.LuanaSMS__Allow_Online_Enrolment__c = true;
        insert pppMemberCourse;
        
        pppNonMemberCourse = testDataGenerator.createNewCourse('PPP - Non Member_' + classNamePrefixShort, poNonMember.Id, testDataGenerator.getRecordTypeIdMap('LuanaSMS__Course__c').get('PPP'), 'Running');
        pppNonMemberCourse.LuanaSMS__Allow_Online_Enrolment__c = true;
        insert pppNonMemberCourse;
        
        pppMemberRegionalCourse = testDataGenerator.createNewCourse('PPP - Member Regional_' + classNamePrefixShort, poMemberRegional.Id, testDataGenerator.getRecordTypeIdMap('LuanaSMS__Course__c').get('PPP'), 'Running');
        pppMemberRegionalCourse.LuanaSMS__Allow_Online_Enrolment__c = true;
        insert pppMemberRegionalCourse;
        System.debug('**regionalCourse: ' + pppMemberRegionalCourse);
        
        pppCourseMaster = testDataGenerator.createNewCourse('PPP - Course Master_' + classNamePrefixShort, poMemberRegional.Id, testDataGenerator.getRecordTypeIdMap('LuanaSMS__Course__c').get('PPP'), 'Running');
        insert pppCourseMaster;
        
        newStudProg = testDataGenerator.createNewStudentProgram(testDataGenerator.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('PPP'), memberUser.ContactId, pppMemberCourse.Id, 'Australia', 'In Progress');
        newStudProg.Blackboard_Integration_Ready__c = true;
        newStudProg.Blackboard_eLearn_Status__c = 'To be Started';
        newStudProg.Accept_terms_and_conditions__c = true;
        newStudProg.Opt_In__c = true;
        newStudProg.Terms_and_conditions_accept_date__c = System.today();
        
        insert newStudProg;
        
        System.debug('***newStudProg:: ' + newStudProg);
    }
    
    static void initial_Overseas(){
        //initialize
        testDataGenerator = new Luana_DataPrep_Test();
        
        //Create all the custom setting
        insert testDataGenerator.prepLuanaExtensionSettingCustomSettings();
        insert testDataGenerator.createLuanaConfigurationCustomSetting();
        
        //Create user with Member and Employer community access
        memberAccountOversea = testDataGenerator.generateNewApplicantAcc('Joe_2_' + classNamePrefixShort, classNamePrefixLong, 'Full_Member');
        memberAccountOversea.Member_Id__c = '123452';
        memberAccountOversea.Affiliated_Branch_Country__c = 'Overseas';
        memberAccountOversea.Membership_Class__c = 'Full';
        memberAccountOversea.PersonEmail = 'joe_2_' +classNamePrefixShort+'@gmail.com';//LCA-921
        memberAccountOversea.Communication_Preference__c= 'Home Phone';
        memberAccountOversea.PersonHomePhone= '1234';
        memberAccountOversea.PersonOtherStreet= '83 Saggers Road';
        memberAccountOversea.PersonOtherCity='JITARNING';
        memberAccountOversea.PersonOtherState='Western Australia';
        memberAccountOversea.PersonOtherCountry='Australia';
        memberAccountOversea.PersonOtherPostalCode='6365';  
        insert memberAccountOversea;
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
    }
    
    static void initial_nonMember(){
        //initialize
        testDataGenerator = new Luana_DataPrep_Test();
        
        //Create all the custom setting
        insert testDataGenerator.prepLuanaExtensionSettingCustomSettings();
        insert testDataGenerator.createLuanaConfigurationCustomSetting();
        
        //Create user with Member and Employer community access
        nonMemberAccount = testDataGenerator.generateNewApplicantAcc('Joe_3_' + classNamePrefixShort, classNamePrefixLong, 'Non_member');
        nonMemberAccount.Member_Id__c = '123453';
        nonMemberAccount.Affiliated_Branch_Country__c = 'Australia';
        nonMemberAccount.Assessible_for_CA_Program__c = false;
        nonMemberAccount.PersonEmail = 'joe_3_' +classNamePrefixShort+'@gmail.com';//LCA-921
        insert nonMemberAccount;
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
    }
    
    private static testMethod void testPPPNonMemberEnrolmentWizard() {
        
        test.startTest();
            initial_nonMember();
        test.stopTest();
         
        nonMemberUser = [Select Id, Email, FirstName, LastName, UserName, Name from User Where Email =: 'joe_3_' +classNamePrefixShort+'@gmail.com' limit 1];
        Luana_Extension_Settings__c defNonMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_NonMember_PermSet_Id');
        PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId=nonMemberUser.Id, PermissionSetId=defNonMemberPermSetId.value__c);
        
        system.runAs(nonMemberUser){
            insert psa;
        }
        initialData();
        
        List<Luana_Extension_Settings__c> csList = new List<Luana_Extension_Settings__c>();
        csList.add(new Luana_Extension_Settings__c(Name = 'PPP_Non_Member', Value__c = poNonMember.Id));
        insert csList;
        
        System.runAs(nonMemberUser){
            luana_MyPPPEnrolmentController pppEnrol = new luana_MyPPPEnrolmentController();
            pppEnrol.enrolNewPPP();
        }
    }

    private static testMethod void testPPPMemberOverseaEnrolmentWizard() {
        
        test.startTest();
            initial_Overseas();
        test.stopTest();
         
        memberUserOversea = [Select Id, Email, FirstName, LastName, UserName, Name, Contact.Account.Affiliated_Branch_Country__c from User Where Email =: 'joe_2_' +classNamePrefixShort+'@gmail.com' limit 1];
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId=memberUserOversea.Id, PermissionSetId=defMemberPermSetId.value__c);
        list<PermissionSetAssignment> permissAssign = [Select Id from PermissionSetAssignment where AssigneeId =: memberUserOversea.Id AND PermissionSetId =: defMemberPermSetId.value__c];
        system.runAs(memberUserOversea){
            if(permissAssign == null)
                insert psa;
        }
        
        initialData();
        
        List<Luana_Extension_Settings__c> csList = new List<Luana_Extension_Settings__c>();
        csList.add(new Luana_Extension_Settings__c(Name = 'PPP_Member_Standard', Value__c = poMember.Id));
        csList.add(new Luana_Extension_Settings__c(Name = 'PPP_Member_Regional', Value__c = poMemberRegional.Id));
        insert csList;
        
        memberAccountOversea.Affiliated_Branch_Country__c = 'Overseas';
        update memberAccountOversea;
        
        System.runAs(memberUserOversea){
            luana_MyPPPEnrolmentController pppEnrol = new luana_MyPPPEnrolmentController();
            pppEnrol.enrolNewPPP();
        }
    } 
    
    private static testMethod void testPPPMemberEnrolmentWizard() {
        
        test.startTest();
            initial();
            
            initialData();
        test.stopTest();
         
        memberUser = [Select Id, Email, FirstName, LastName, ContactId, UserName, Name from User Where Email =: 'joe_1_' +classNamePrefixShort+'@gmail.com' limit 1];
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c);
        list<PermissionSetAssignment> permissAssign = [Select Id from PermissionSetAssignment where AssigneeId =: memberUser.Id AND PermissionSetId =: defMemberPermSetId.value__c];
        system.runAs(memberUser){
            if(permissAssign == null)
                insert psa;
        }
        
//      initialData();
        
        List<Luana_Extension_Settings__c> csList = new List<Luana_Extension_Settings__c>();
        //csList.add(new Luana_Extension_Settings__c(Name = 'PPP_Member_Standard', Value__c = poMember.Id));
        csList.add(new Luana_Extension_Settings__c(Name = 'PPP_Member_Regional', Value__c = poMemberRegional.Id));
        csList.add(new Luana_Extension_Settings__c(Name = 'PPP_Master', Value__c = poMaster.Id));
        insert csList;
        
        System.debug('***csList::: ' + csList);
        prepareData();
        
        System.runAs(memberUser){
            luana_MyPPPEnrolmentController pppEnrol = new luana_MyPPPEnrolmentController();
            pppEnrol.enrolNewPPP();
            
            //LCA-998
            luana_MemberPPPLandingController pppLanding = new luana_MemberPPPLandingController();
            /*luana_MemberPPPLandingController pppLanding = new luana_MemberPPPLandingController();
            pppLanding.getTypes();
            pppLanding.isRegional = 'Yes';
            pppLanding.enrolNewPPPMember();
            
            pppLanding.isRegional = 'No';
            pppLanding.enrolNewPPPMember();
            
            pppLanding.isRegional = 'none';
            pppLanding.enrolNewPPPMember();
            
            pppLanding.doBack();*/
            
        }
        
        
        
        System.runAs(memberUser){
            luana_MyPPPEnrolmentController pppEnrol = new luana_MyPPPEnrolmentController();
            pppEnrol.workshopRegister();
            
            System.assertEquals(pppEnrol.isReadyForWorkshopRegister, false, 'PPP workshop Registration is not available yet at this point');
        }
        
        newStudProg.Blackboard_eLearn_Status__c = 'Completed';
        newStudProg.Paid__c = true;
        update newStudProg;
        /*System.runAs(memberUser){
            luana_MyPPPEnrolmentController pppEnrol = new luana_MyPPPEnrolmentController();
            pppEnrol.workshopRegister();
            
            System.assertEquals(pppEnrol.isReadyForWorkshopRegister, true, 'PPP workshop Registration is available at this point');
        }*/
        
        devLocation = testDataGenerator.createNewDeliveryLocationRecord('Australia - South Australia - Adelaide_' + classNamePrefixShort, 'Australia - South Australia - Adelaide_' + classNamePrefixShort, trainingOrg.Id, '1000', 'Australia');
        insert devLocation;
        
        courseDevLoc = testDataGenerator.createCourseDeliveryLocation(pppMemberCourse.Id, devLocation.Id, 'Workshop Location');
        insert courseDevLoc;
        
        venueAccount = testDataGenerator.generateNewBusinessAcc(classNamePrefixShort+ '_Test Venue_1', 'Venue', null, null, null, null, 'Sunshine Coast Office', 'Active');
        venueAccount.Ext_Id__c = classNamePrefixShort+'_venue_1';
        insert venueAccount;
        
        room1 = testDataGenerator.newRoom(classNamePrefixShort, classNamePrefixLong, venueAccount.Id, 20, 'Classroom', '1');
        insert room1;
        
        List<LuanaSMS__Session__c> sessionList = new List<LuanaSMS__Session__c>();
        session1 = testDataGenerator.newSession(classNamePrefixShort, classNamePrefixLong, 'Workshop', 20, venueAccount.Id, room1.Id, courseDevLoc.Id, system.now().addHours(1), system.now().addHours(2)); //e.g, 1pm - 2pm
        session1.Topic_Key__c = classNamePrefixShort+ '_TopicKey1';
        session1.LuanaSMS__Status__c = 'Running';
        session1.Adobe_Connect_Url__c = 'https://connect-staging.charteredaccountantsanz.com/r6wu6hclpou/';
        sessionList.add(session1);
        
        session2 = testDataGenerator.newSession(classNamePrefixShort, classNamePrefixLong, 'Workshop', 20, venueAccount.Id, room1.Id, courseDevLoc.Id, system.now().addHours(5), system.now().addHours(6)); //e.g, 1pm - 2pm
        session2.Topic_Key__c = classNamePrefixShort+ '_TopicKey1';
        session2.LuanaSMS__Status__c = 'Running';
        session2.Adobe_Connect_Url__c = 'https://connect-staging.charteredaccountantsanz.com/r6wu6hclpoi/';
        sessionList.add(session2);
        
        session3 = testDataGenerator.newSession(classNamePrefixShort, classNamePrefixLong, 'Workshop', 20, venueAccount.Id, room1.Id, courseDevLoc.Id, system.now().addHours(7), system.now().addHours(8)); //e.g, 1pm - 2pm
        session3.Topic_Key__c = classNamePrefixShort+ '_TopicKey2';
        session3.LuanaSMS__Status__c = 'Running';
        session3.Adobe_Connect_Url__c = 'https://connect-staging.charteredaccountantsanz.com/r6wu6hclpoi/';
        sessionList.add(session3);
        
        insert sessionList;
        
        ApexPages.currentPage().getParameters().put('cdlid',courseDevLoc.id);
        ApexPages.currentPage().getParameters().put('courseid',pppMemberCourse.Id);
        ApexPages.currentPage().getParameters().put('spid',newStudProg.Id);
        luana_PPPWorkshopRegisterController pppReg = new luana_PPPWorkshopRegisterController();
        
        pppReg.getTemplateName();
        pppReg.getWorkshop1LocationOptions();
        pppReg.selectedWorkshopLocation1Id = courseDevLoc.Id;
        pppReg.optionSelected();
        pppReg.selectedTopicKey = classNamePrefixShort+ '_TopicKey1';
        pppReg.doBooking();
        pppReg.getTemplateName();
        pppReg.doBack();
        
        newStudProg.Grant_Workshop_CPD_Hours__c = true;
        update newStudProg;
    }
    
    
}