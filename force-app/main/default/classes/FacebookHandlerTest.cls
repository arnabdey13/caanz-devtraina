@isTest
private class FacebookHandlerTest {
static testMethod void testCreateAndUpdateUser() {
    
    UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
    Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
    User portalAccountOwner1 = new User(
    UserRoleId = portalRole.Id,
    ProfileId = profile1.Id,
    Username = System.now().millisecond() + 'test2@test.com',
    Alias = 'batman',
    Email='standarduser@testorg.com',
    EmailEncodingKey='UTF-8',
    Firstname='Bruce',
    Lastname='Wayne',
    LanguageLocaleKey='en_US',
    LocaleSidKey='en_US',
    TimeZoneSidKey='America/Chicago'
    );
    Database.insert(portalAccountOwner1);

    //Create account
    Account portalAccount1 = new Account(
    Name = 'TestAccount',
    BillingStreet = 'testbillingstreet',
    OwnerId = portalAccountOwner1.Id
    );
    System.runAs(new User(Id = portalAccountOwner1.id)) {
       Database.insert(portalAccount1);
    }
    
        
    //Create contact
    Contact contact1 = new Contact(
    FirstName = 'Test',
        Lastname = 'McTesty',
    AccountId = portalAccount1.Id,
        Email = System.now().millisecond() + 'test@test.com'
    );
    System.runAs(new User(Id = UserInfo.getUserId())) {
       Database.insert(contact1);
    }
    
    //Create user
    Profile portalProfile = [Select Id from Profile where name = 'NZICA Community Login User' limit 1];
    User user1 = new User(
    Username = 'standarduser@testorg.com',
    ContactId = contact1.Id,
    ProfileId = portalProfile.Id,
    Alias = 'test123',
    Email = 'test12345@test.com',
    EmailEncodingKey = 'UTF-8',
    LastName = 'McTesty',
    CommunityNickname = 'test12345',
    TimeZoneSidKey = 'America/Los_Angeles',
    LocaleSidKey = 'en_US',
    LanguageLocaleKey = 'en_US'
    );
    System.runAs(new User(Id = portalAccountOwner1.id)) {
       insert user1;
    }
    
    
    
    
    FacebookHandler handler = new FacebookHandler();
    Auth.UserData sampleData = new Auth.UserData('testId', 'testFirst', 'testLast',
        'testFirst testLast', 'standarduser@testorg.com', null, 'testuserlong', 'en_US', 'facebook',
        null, new Map<String, String>{'language' => 'en_US'});
    User u1 = handler.createUser(null, sampleData);
    /*System.assertEquals('standarduser@testorg.com', u1.userName);
    System.assertEquals('standarduser@testorg.com', u1.email);
    System.assertEquals('testLast', u1.lastName);
    System.assertEquals('testFirst', u1.firstName);
    System.assertEquals('testuser', u1.alias);*/
    update(u1);
    String uid = u1.id;
    
    sampleData = new Auth.UserData('testNewId', 'testNewFirst', 'testNewLast',
        'testNewFirst testNewLast', 'testnewuser@example.org', null, 'testnewuserlong', 'en_US', 'facebook',
        null, new Map<String, String>{});
    handler.updateUser(uid, null, sampleData);
    
    User updatedUser = [SELECT userName, email, firstName, lastName, alias FROM user WHERE id=:uid];
    /*System.assertEquals('testnewuserlong@salesforce.com', updatedUser.userName);
    System.assertEquals('testnewuser@example.org', updatedUser.email);
    System.assertEquals('testNewLast', updatedUser.lastName);
    System.assertEquals('testNewFirst', updatedUser.firstName);
    System.assertEquals('testnewu', updatedUser.alias);*/
}
}