public with sharing class luana_RequestConstants {
      
    
    public static final String REQ_DIS_RT_NAME = 'Module Cancellation/Discontinue';
    public static final String REQ_EXEMPTION_RT_NAME = 'Module Exemption';
    public static final String REQ_OOAE_RT_NAME = 'Out of Approved Employment';
    public static final String REQ_SC_RT_NAME = 'Request Special Consideration';
    public static final String REQ_ASSISTANCE_RT_NAME = 'Request for Assistance';
    public static final String REQ_COMPETENCY_RT_NAME = 'Request Competency Area';

    
    
}