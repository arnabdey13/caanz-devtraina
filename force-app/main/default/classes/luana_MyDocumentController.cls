/*
    Developer: WDCi (KH)
    Date: 07/07/2016
    Task #: View transcript link (LCA-538)
   
*/
public without sharing class luana_MyDocumentController {
    
    Id custCommConId;
    Id custCommAccId;
    
    public List<Attachment> relatedTranscriptDocs;
    
    public luana_MyDocumentController(){
    
        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId()); 
       
        this.custCommConId = commUserUtil.custCommConId;       
        this.custCommAccId = commUserUtil.custCommAccId;  
        
        relatedTranscriptDocs = new List<Attachment>();
        
    }
    
    public List<Attachment> getDocRepoAttachments(){
        
        List<Document_Repository__c> docRepos = [select Id, Name, Is_Public__c, Contact__c, (Select Id, Name, createdDate from Attachments) from Document_Repository__c Where Contact__c =: custCommConId and Is_Public__c = true];
        
        for(Document_Repository__c dr: docRepos){
            for(Attachment att: dr.Attachments){
                relatedTranscriptDocs.add(att);
            }
        }
        
        return relatedTranscriptDocs;
    }

    public PageReference forwardToAuthPage() {
        if(UserInfo.getUserType() == 'Guest'){
            return new PageReference(URL.getSalesforceBaseUrl().toExternalForm()+'/member/luana_Login?startURL=/member/luana_MemberHome');
        }
        else{
            return null;
        }
    }

    public String getDomainUrl(){
        return luana_NetworkUtil.getCommunityPath();
    }
}