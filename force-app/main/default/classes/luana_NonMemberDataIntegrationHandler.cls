/*
    Developer: WDCi (KH)
    Date: 08 Apr 2016
    Task #: Non Member Data Intgeration 
    
    Change History:
    23/08/2016 WDCi - KH: LCA-904 to include is active user checking
*/
public with sharing class luana_NonMemberDataIntegrationHandler{
    
    /*LCA-403 disabled. Code added to AccountTriggerClass
    public static void userAction(List<User> newList, Map<Id, User> newMap, List<User> oldList, Map<Id, User> oldMap, boolean isInsert, boolean isUpdate, boolean isDelete, boolean isUndelete, boolean isBefore, boolean isAfter){
         
        if(isAfter){
            
            Set<Id> userToCreateNMPromissionSet = new Set<Id>();    
            
            Set<Id> relatedProfileId = new Set<Id>();
            for(User u: newList){
                relatedProfileId.add(u.ProfileId);
            }
            
            Map<Id, String> profileNameMap = new Map<Id, String>();
            for(Profile p: [Select Id, Name from Profile Where Id in: relatedProfileId]){
                profileNameMap.put(p.Id, p.Name);
            }
            
            for(User u: newList){
                if(profileNameMap.get(u.ProfileId) == 'NZICA Community Login User'){
                    userToCreateNMPromissionSet.add(u.Id);
                }
            }
            
            Luana_Extension_Settings__c defNonMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_NonMember_PermSet_Id');
            
            if(!userToCreateNMPromissionSet.isEmpty() && defNonMemberPermSetId != null){
            
                System.debug('****userToCreateNMPromissionSet: ' + userToCreateNMPromissionSet); 
                System.debug('****defNonMemberPermSetId: ' + defNonMemberPermSetId.Value__c);
                
                updateNonMemberPermSet(userToCreateNMPromissionSet, defNonMemberPermSetId.Value__c);
            }
        }

     }
     
    @future
    public static void updateNonMemberPermSet(Set<Id> nzicaUsers, String nonMemberPermSetId){

        Map<String, PermissionSetAssignment> userPermMap = new Map<String, PermissionSetAssignment>();
        for(PermissionSetAssignment psa : [SELECT AssigneeId, PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId IN: nzicaUsers]) {
            String longId = '' + psa.PermissionSetId;
            userPermMap.put(psa.AssigneeId + '|' + longId.left(15), psa);
        }

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();
        for(Id uId: nzicaUsers){
            
            if(userPermMap.get(uId + '|' + nonMemberPermSetId) == null) {
                psaList.add(new PermissionSetAssignment(AssigneeId=uId, PermissionSetId=nonMemberPermSetId));
            
            }
        }
        insert psaList;
    }*/
    
    //LCA-403 added new method for AccountTriggerClass
    public static void updateNonMemberPermSet(List<User> newUsers){
        
        Set<Id> nzicaUsers = new Set<Id>();
        for(User newUser : newUsers){
            //LCA-1140 KH to remove the user isActive checking, this will cause the non-member user permission set not working
            //if(newUser.isActive){ //LCA-904
                nzicaUsers.add(newUser.Id);
            //}
        }       
        
        String nonMemberPermSetId = '';
        
        if(Test.isRunningTest()){
            if(UserInfo.getOrganizationId().startsWith('00Dp00000000k6x')){
                //prepod
                nonMemberPermSetId = '0PSp0000000CebN';
            } else if(UserInfo.getOrganizationId().startsWith('00D90000000nGH8')){
                //production
                nonMemberPermSetId = '0PS900000018u6k';
            } else {
                //sandbox
                nonMemberPermSetId = '0PSp0000000CePH';
            }
            
        } else {
            Luana_Extension_Settings__c defNonMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_NonMember_PermSet_Id');
            nonMemberPermSetId = defNonMemberPermSetId.Value__c;
        }
        
        Map<String, PermissionSetAssignment> userPermMap = new Map<String, PermissionSetAssignment>();
        for(PermissionSetAssignment psa : [SELECT AssigneeId, PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId IN: nzicaUsers]) {
            String longId = '' + psa.PermissionSetId;
            userPermMap.put(psa.AssigneeId + '|' + longId.left(15), psa);
        }

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();
        for(Id uId: nzicaUsers){
            
            if(userPermMap.get(uId + '|' + nonMemberPermSetId) == null) {
                psaList.add(new PermissionSetAssignment(AssigneeId=uId, PermissionSetId=nonMemberPermSetId));
            
            }
        }
        
        if(!Test.isRunningTest()){
            insert psaList;
        }
    }
}