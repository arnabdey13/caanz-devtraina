/**
* @author Steve Buikhuizen, Jannis Bott
* @date 14/11/2016
*
* @group FormsFramework
*
* @description  Common code used during PicklistOption generation
*/
global abstract with sharing class ff_WrappedPicklistSource extends ff_WrapperSource {

}