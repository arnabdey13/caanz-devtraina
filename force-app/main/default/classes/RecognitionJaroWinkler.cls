/**
* A similarity algorithm indicating the percentage of matched characters between two character sequences.
*
* The Jaro measure is the weighted sum of percentage of matched characters
* from each file and transposed characters. Winkler increased this measure
* for matching initial characters.
* 
* @author andrew kopec 
* cloned from java http://www.apache.org/licenses/LICENSE-2.0 algorithm (Apache Commons Lang 3.3.)
*/

public class RecognitionJaroWinkler {

    private static final Double DEFAULT_THRESHOLD = 0.7;
    private static final Double DEFAULT_SCALE = 0.1;
    private final Double threshold;

    /**
     * Instantiate with default threshold (0.7).
     */
    public RecognitionJaroWinkler() {
        this.threshold = DEFAULT_THRESHOLD;
    }

    /**
     * Instantiate with provided threshold 
     * @param threshold
     */
    public RecognitionJaroWinkler( Double threshold) {
        this.threshold = threshold;
    }

    /**
     * Calculate Jaro-Winkler similarity.
     * @param left the first String, must not be null
     * @param right the second String, must not be null
     * @return Decimal result distance
     */
    public Decimal similarity(String left, String right) {
        if (left == null | right == null )
            return 0.0;

        if (left.equals(right))
            return 1; 

        Integer[] mtp = matches(left, right);
        Double m = mtp[0];
        if (m == 0)
            return 0.0;
        
        Decimal j = ((m / left.length() + m / right.length() + (m - mtp[1]) / m)) / 3;
        Decimal jw;
        if (j > threshold)
            jw = j + Math.min(DEFAULT_SCALE, 1.0 / mtp[3]) * mtp[2] * (1 - j);
        else
            jw = j;
        
        return jw.setScale(2);  
    }

    /**
    * This method returns the Jaro-Winkler string matches, transpositions, prefix, max array.
    *
    * @param first the first string to be matched
    * @param second the second string to be matched
    * @return mtp array containing: matches, transpositions, prefix, and max length
    */

    private Integer[] matches( String s1,  String s2) {
        String max, min;
        if (s1.length() > s2.length()) {
            max = s1;
            min = s2;
        } else {
            max = s2;
            min = s1;
        }
        Integer range = Math.max(max.length() / 2 - 1, 0);
        Integer[] match_indexes = new Integer[min.length()];
        for (Integer idx = 0; idx < min.length(); idx++) {
            match_indexes[idx] = -1;}
        
        Boolean[] match_flags = new Boolean[max.length()];
        for (Integer idx = 0; idx < max.length(); idx++) {
            match_flags[idx] = false; }
        
        Integer matches = 0;
        for (Integer mi = 0; mi < min.length(); mi++) {
            Integer c1 = min.charAt(mi);
            // System.debug('int:' + c1 + ' char:' + String.valueOf(c1));
            
            for (Integer xi = Math.max(mi - range, 0); xi < Math.min(mi + range + 1, max.length()) ; xi++) {	   
                if (!match_flags[xi] && c1 == max.charAt(xi)) {
                    match_indexes[mi] = xi;
                    match_flags[xi] = true;
                    matches++;
                    break;
                }
            }
        }
        Integer[] ms1 = new Integer[matches];
        Integer[] ms2 = new Integer[matches];
        for (Integer i = 0, si = 0; i < min.length(); i++) {
            if (match_indexes[i] != -1) {
                ms1[si] = min.charAt(i);
                si++;
            }
        }
        for (Integer i = 0, si = 0; i < max.length(); i++) {
            if (match_flags[i]) {
                ms2[si] = max.charAt(i);
                si++;
            }
        }
        Integer transpositions = 0;
        for (Integer mi = 0; mi < ms1.size(); mi++) {
            if (ms1[mi] != ms2[mi]) {
                transpositions++;
            }
        }
        Integer prefix = 0;
        for (Integer mi = 0; mi < min.length(); mi++) {
            if (s1.charAt(mi) == s2.charAt(mi)) {
                prefix++;
            } else {
                break;
            }
        }
        return new Integer[]{matches, transpositions / 2, prefix, max.length()};
    }
}