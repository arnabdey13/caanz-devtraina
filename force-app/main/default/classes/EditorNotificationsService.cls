public class EditorNotificationsService {

    private static Integer MAX_QUESTION_NUMBER = 54; //increased from 49 to 54 

    @AuraEnabled
    public static Map<String, Object> load() {
        return load(EditorPageUtils.getUsersAccountId());
    }

    public static Map<String, Object> load(Id accountId) {
        return load(accountId, getCurrentYear());
    }

    // translate the sobject to a map with field name and value type conversion
    private static Map<Object, Object> translateQuestionnaireToMap(Questionnaire_Response__c q) {
        Map<Object, Object> result = new Map<Object, Object>();

        // NOTE: this max number is affected by the field mapping scheme e.g. 5-5 => 6
        // and we are not using the last (#31) field
        for (Integer i = 1; i < MAX_QUESTION_NUMBER; i++) {
            String dbFieldName = 'Answer_' + i + '__c';

            if (q.get(dbFieldName) != null) {
                if (CHECKBOX_FIELDS.contains(dbFieldName)) {
                    result.put(mapDBToField('Answer_', dbFieldName), q.get(dbFieldName) == 'Yes');
                } else {
                    result.put(mapDBToField('Answer_', dbFieldName), q.get(dbFieldName));
                }
            }
        }

        for (String f : FIELDS) {
            result.put(f, q.get(f));
        }

        return result;
    }

    // translate the map from the client for question mappings
    private static Map<Object, Object> translateQuestionsToDB(Map<Object, Object> fromClient) {
        Map<Object, Object> result = new Map<Object, Object>();
        if (fromClient != null) {
            for (Object key : fromClient.keySet()) {
                String keyString = (String) key;
                if (keyString.startsWith('Question_') || keyString.startsWith('Answer_')) {
                    String mappedKeyString = mapFieldToDB(keyString.substring(0, keyString.indexOf('_') + 1), keyString);
                    if (keyString != mappedKeyString) {
                        System.debug('translating back to db: ' + keyString + ' to ' + mappedKeyString + ' -> ' + fromClient.get(keyString));
                        result.put(mappedKeyString, fromClient.get(keyString));
                    } else {
                        System.debug('not translating back to db: ' + keyString + ' -> ' + fromClient.get(keyString));
                        result.put(keyString, fromClient.get(keyString));
                    }
                }
            }
        }
        return result;
    }

    // public because used by the prev year class as well
    public static Map<String, Object> load(Id accountId, Integer year) {

        Questionnaire_Response__c questionnaire = getQuestionnaire(year, accountId);
        Account a = [
                select PersonOtherCountry, Qualified_Auditor__c, CPP__c, Insolvency_Practitioner__c, Designation__c, Licensed_Auditor__c, Fellow_MN__c, CPPEXEMPT_MN__c, ProvMN__c
                from Account
                where Id = :accountId
        ];
        if (a.PersonOtherCountry == null) {
            return null;
        }
        Map<String, Object> result = new Map<String, Object>();
        if (questionnaire == null) {
            Questionnaire_Response__c newQ = new Questionnaire_Response__c(
                    Name = 'Mandatory Notification',
                    Account__c = accountId,
                    Year__c = '' + year,
                    Status__c = 'Draft',
                    Lock__c = false,
                    Date__c = System.today(),
                    Residential_Country__c = a.PersonOtherCountry);

            if (a.PersonOtherCountry == 'Australia') {
                preloadAnswers(AU_ACCOUNT_FIELDS, newQ);
            } else if (a.PersonOtherCountry == 'New Zealand') {
                preloadAnswers(NZ_ACCOUNT_FIELDS, newQ);
            }

            insert newQ;
            result.put('RECORD', translateQuestionnaireToMap(newQ));
            result.put('prop.readOnly', false);
            questionnaire = newQ;
        } else {
            result.put('prop.readOnly', isReadOnly(questionnaire));
            result.put('RECORD', translateQuestionnaireToMap(questionnaire));
        }

        // add properties to control conditional visibility in the form
        result.put('prop.ACC_NOTCPP', !a.CPP__c);
        result.put('prop.ACC_QA', a.Qualified_Auditor__c);
        result.put('prop.ACC_NOTQAorNOTLA', !a.Qualified_Auditor__c || !a.Licensed_Auditor__c);
        result.put('prop.ACC_AIP', a.Insolvency_Practitioner__c);
        result.put('prop.ACC_LA', a.Licensed_Auditor__c);
        result.put('prop.ACC_NOTAFF', a.Designation__c != 'Affiliate CAANZ');
        result.put('prop.ACC_CPPandNOTNZAIP', a.CPP__c && !a.Insolvency_Practitioner__c); //Written by APorter 20/04/2017
        result.put('prop.ACC_NOTCPPandNOTAFF', !a.CPP__c && a.Designation__c != 'Affiliate CAANZ'); //Written by APorter 20/04/2017
        result.put('prop.ACC_NOTQAandNOTLA', !a.Qualified_Auditor__c && !a.Licensed_Auditor__c); //Written by APorter 20/04/2017
        result.put('prop.ACC_NOTNZAIP', !a.Insolvency_Practitioner__c); //Written by APorter 24/04/2017
        result.put('prop.ACC_FELLOWNOMS1', !a.Fellow_MN__c); //Written by AELLIOTT 22/03/2018
        result.put('prop.ACC_CPPEXEMPT', a.CPPEXEMPT_MN__c); //Written by AELLIOTT 2/05/2018
        result.put('prop.ACC_PROVMN', !a.ProvMN__c); //Written by AELLIOTT 8/05/2018

        // end properties to control conditional visibility in the form

        List<String> clientQuestionsNeedingLabels = new List<String>();
        for (String dbField : getAnswersWithoutQuestions(questionnaire)) {
            clientQuestionsNeedingLabels.add(mapDBToField('Answer_', dbField));
        }
        System.debug('client labels required: ' + clientQuestionsNeedingLabels);
        result.put('prop.labelsRequired', clientQuestionsNeedingLabels);

        List<CountryPicklistSetting> countries = EditorPageUtils.getCountriesAndStates();
        List<Map<String, String>> countriesForPicklist = new List<Map<String, String>>();

        for (CountryPicklistSetting c : countries) {
            if (questionnaire.Residential_Country__c == c.getCountryLabel()) {
                result.put('prop.countryCode', c.getCountryCode());
            }
            countriesForPicklist.add(new Map<String, String>{
                    'label' => c.getCountryLabel(),
                    'value' => c.getCountryCode()
            });
        }

        result.put('prop.countries', countriesForPicklist);

        Questionnaire_Response__c lastYearsQuestionnaire = getQuestionnaire(year - 1, accountId);
        result.put('prop.lastYearPresent', lastYearsQuestionnaire != null);

        return result;
    }

    // copies values from Account into questionnaire
    private static void preloadAnswers(Map<Integer, String> mappings, Questionnaire_Response__c q) {
        if (mappings != NULL && mappings.size() > 0) {
            String accountQuery = 'select ' +
                    String.join(mappings.values(), ',') +
                    ' from Account where Id = \'' + q.Account__c + '\'';
            system.debug(accountquery);
            Account preloadSource = Database.query(accountQuery);
            for (Integer qNum : mappings.keySet()) {
                Object answer = preloadSource.get(mappings.get(qNum));
                Boolean isAnswerBoolean = EditorPageUtils.getType(answer) == 'Boolean';
                if (answer != NULL & answer != false) {
                    System.debug('Preloading: ' + mappings.get(qNum) + ' with : ' + answer);
                    if (isAnswerBoolean) {
                        q.put('Answer_' + qNum + '__c', 'Yes');
                    } else {
                        q.put('Answer_' + qNum + '__c', String.valueOf(answer));
                    }
                }
            }
        }      
    }

    // for all questions that have answers without questions recorded (typically as a result of preloadAUAnswers above) return
    // the Question field name so that it can be sent to client
    private static List<String> getAnswersWithoutQuestions(Questionnaire_Response__c q) {
        List<String> result = new List<String>();

        for (Integer i = 1; i <= MAX_QUESTION_NUMBER; i++) {
            String questionField = 'Question_' + i + '__c';
            String answerField = 'Answer_' + i + '__c';
            String question = (String) q.get(questionField);
            String answer = (String) q.get(answerField);
            if (answer != NULL && question == NULL) {
                System.debug('Missing Question : ' + i + ' ' + question + ' -> ' + answer);
                result.add(answerField);
            }
        }

        return result;
    }

    private static Boolean isReadOnly(Questionnaire_Response__c q) {
        return q.Lock__c || q.Status__c == 'Submitted';
    }

    @AuraEnabled
    public static String save(Map<String, Object> properties) {
        return save(properties, EditorPageUtils.getUsersAccountId());
    }

    private static String getNotificationsSuffix(String countryCode) {
        Set<String> ANZ_CODES = new Set<String>{
                'au', 'nz'
        };
        String returnCode = countryCode.toLowerCase();
        return ANZ_CODES.contains(returnCode) ? returnCode : 'other';
    }

    @TestVisible
    private static String save(Map<String, Object> properties, Id accountId) {
        Questionnaire_Response__c q = getQuestionnaire(getCurrentYear(), accountId);

        Map<Object, Object> record = (Map<Object, Object>) properties.get('RECORD');
        Map<Object, Object> dirty = (Map<Object, Object>) properties.get('DIRTY');
        Map<Object, Object> mappings = (Map<Object, Object>) properties.get('LABEL-MAPPINGS');

        if (isReadOnly(q)) {
            throw new SObjectException('Cannot save a non-draft questionnaire');
        }

        if (dirty != null && dirty.containsKey('prop.countryCode')) {

            // this is a country change
            String newQuestionnaireCountryCode = (String) properties.get('prop.countryCode');

            String newCountryName;
            List<CountryPicklistSetting> countries = EditorPageUtils.getCountriesAndStates();
            for (CountryPicklistSetting c : countries) {
                if (newQuestionnaireCountryCode == c.getCountryCode()) {
                    newCountryName = c.getCountryLabel();
                    break;
                }
            }
            q.Residential_Country__c = newCountryName;

            // clean the country specific responses
            for (Integer i = 7; i <= MAX_QUESTION_NUMBER; i++) {
                q.put('Question_' + i + '__c', null);
                q.put('Answer_' + i + '__c', null);
            }

            Account a = [select PersonOtherCountryCode from Account where Id = :accountId];

            if (newQuestionnaireCountryCode == 'AU' && a.PersonOtherCountryCode == 'AU') {
                preloadAnswers(AU_ACCOUNT_FIELDS, q);
            } else if (newQuestionnaireCountryCode == 'NZ' && a.PersonOtherCountryCode == 'NZ') {
                preloadAnswers(NZ_ACCOUNT_FIELDS, q);
            }

            update q;

            return getNotificationsSuffix(newQuestionnaireCountryCode);
        } else {

            for (Object k : record.keySet()) {
                System.debug('before mapping scheme applied: ' + k + ' -> ' + record.get(k));
            }

            // translate back the question mappings to database keys
            System.debug('translating record');
            record = translateQuestionsToDB(record);
            properties.put('RECORD', record);

            for (Object k : record.keySet()) {
                System.debug('after mapping scheme applied: ' + k + ' -> ' + record.get(k));
            }

            System.debug('translating dirty');
            dirty = translateQuestionsToDB(dirty);
            properties.put('DIRTY', dirty);

            System.debug('translating labels');
            mappings = translateQuestionsToDB(mappings);
            properties.put('LABEL-MAPPINGS', mappings);
            System.debug('translated labels: ' + mappings);

            // convert the checkbox fields into Yes,No i.e. String values
            for (String f : CHECKBOX_FIELDS) {
                System.debug('reading possible boolean: ' + f + ' => ' + record.get(f));
                Boolean v = (Boolean) record.get(f);
                if (v != null) {
                    System.debug('translating ' + f + ' to Yes/No');
                    record.put(f, v ? 'Yes' : 'No');
                }
            }

            System.debug('properties: ' + properties);
            Questionnaire_Response__c toSave = (Questionnaire_Response__c) EditorPageUtils.getRecordFromSaveRequest(new Questionnaire_Response__c(Id = q.Id), properties);

            // if labels are sent back by client (see load code), the answer is present but the question is null then it should be included in the write to Questionnaire
            for (Integer i = 1; i <= MAX_QUESTION_NUMBER; i++) {
                String questionField = 'Question_' + i + '__c';
                String answerField = 'Answer_' + i + '__c';
                String question = (String) q.get(questionField);
                String answer = (String) q.get(answerField);
                if (mappings != null && mappings.containsKey(answerField) && question == NULL && answer != NULL) {
                    System.debug('Missing Question updating during save : ' + i + ' ' + mappings.get(answerField) + mappings.get(answerField));
                    toSave.put(questionField, mappings.get(answerField));
                }
            }

            if (properties.get('prop.confirmed') == true) {
                toSave.Status__c = 'Submitted';
            }

            System.debug('toSave after conversion: ' + toSave);

            Boolean isAUMember = [select PersonOtherCountryCode from Account where Id = :accountId].PersonOtherCountryCode == 'AU';
            Boolean isAUQuestionnaire = q.Residential_Country__c == 'Australia';
            Boolean shouldWriteAccountFields = isAUMember && isAUQuestionnaire;
            System.debug('questionnaire loaded: ' + q);

            Account a = new Account(Id = accountId);

            // if the user clicks save without making any changes, then dirty is absent
            if (dirty != null) {

                if (mappings == null) {
                    throw new SObjectException('mappings missing');
                }

                for (Object f : dirty.keySet()) {
                    String fieldName = (String) f;
                    String labelUsed = (String) mappings.get(fieldName);
                    if (labelUsed == null) {
                        throw new SObjectException('label missing: ' + fieldName);
                    }
                    if (!EditorPageUtils.isPropertyField(fieldName)) {
                        Integer questionNumber = Integer.valueOf(fieldName.split('_')[1]);
                        toSave.put('Question_' + questionNumber + '__c', labelUsed);

                        if (shouldWriteAccountFields) {
                            String accountField = AU_ACCOUNT_FIELDS.get(questionNumber);
                            if (accountField != null) {
                                Object value = translateValueForField(Account.SObjectType, accountField, record.get(fieldName));
                                System.debug('Loading AU Account field: ' + accountField + ' -> ' + value);
                                a.put(accountField, value);
                            }
                        }
                    }
                }
                if (shouldWriteAccountFields) {
                    Database.SaveResult sr = Database.update(a);
                    if (!sr.isSuccess()) {
                        for (Database.Error err : sr.getErrors()) {
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Account fields causing error: ' + err.getFields());
                        }
                    } else {
                        System.debug('AU Account Updated: ' + a);
                    }
                }
            }

            System.debug('toSave after label: ' + toSave);
            update toSave;

            return null;
        }

    }

    @TestVisible
    private static Object translateValueForField(Schema.sObjectType objToken, String fieldName, Object value) {
        Schema.DisplayType fieldType = objToken.getDescribe().fields.getMap().get(fieldName.toLowerCase()).getDescribe().getType();
        if (fieldType == Schema.DisplayType.BOOLEAN) {
            return value == true || value == 'Yes';
        } else if (fieldType == Schema.DisplayType.String) {
            return String.valueOf(value);
        } else {
            throw new SObjectException('Cannot convert value: ' + fieldName + ' ' + value);
        }
    }

    @TestVisible
    private static Questionnaire_Response__c getQuestionnaire(Integer year, Id accountId) {
        List<Questionnaire_Response__c> results = [
                SELECT Id, Name, Account__c, Date__c, Lock__c, Residential_Country__c, Status__c, Year__c,
                        Answer_1__c, Answer_2__c, Answer_3__c, Answer_4__c, Answer_5__c, Answer_6__c, Answer_7__c, Answer_8__c, Answer_9__c, Answer_10__c,
                        Answer_11__c, Answer_12__c, Answer_13__c, Answer_14__c, Answer_15__c, Answer_16__c, Answer_17__c, Answer_18__c, Answer_19__c, Answer_20__c,
                        Answer_21__c, Answer_22__c, Answer_23__c, Answer_24__c, Answer_25__c, Answer_26__c, Answer_27__c, Answer_28__c, Answer_29__c, Answer_30__c,
                        Answer_31__c, Answer_32__c, Answer_33__c, Answer_34__c, Answer_35__c, Answer_36__c, Answer_37__c, Answer_38__c, Answer_39__c, Answer_40__c,
                        Answer_41__c, Answer_42__c, Answer_43__c, Answer_44__c, Answer_45__c, Answer_46__c, Answer_47__c, Answer_48__c, Answer_49__c, Answer_50__c,
                        Answer_51__c, Answer_52__c, Answer_53__c, Answer_54__c,
                        Question_1__c, Question_2__c, Question_3__c, Question_4__c, Question_5__c, Question_6__c, Question_7__c, Question_8__c, Question_9__c, Question_10__c,
                        Question_11__c, Question_12__c, Question_13__c, Question_14__c, Question_15__c, Question_16__c, Question_17__c, Question_18__c, Question_19__c, Question_20__c,
                        Question_21__c, Question_22__c, Question_23__c, Question_24__c, Question_25__c, Question_26__c, Question_27__c, Question_28__c, Question_29__c, Question_30__c,
                        Question_31__c, Question_32__c, Question_33__c, Question_34__c, Question_35__c, Question_36__c, Question_37__c, Question_38__c, Question_39__c, Question_40__c,
                        Question_41__c, Question_42__c, Question_43__c, Question_44__c, Question_45__c, Question_46__c, Question_47__c, Question_48__c, Question_49__c,Question_50__c,
                        Question_51__c, Question_52__c, Question_53__c, Question_54__c
                FROM Questionnaire_Response__c
                WHERE Account__c = :accountId
                and Year__c = :String.valueOf(year)
        ];
        if (results.size() == 0) {
            return null;
        } else if (results.size() == 1) {
            return results[0];
        } else {
            throw new SObjectException('More than 1 Questionnaire found for account: ' + accountId + ' and year: ' + year);
        }
    }

    public static Integer getCurrentYear() {
        Decimal yearD = [
                select Number_Value__c
                from Notifications_Configuration__mdt
                where DeveloperName = 'Current_Year'
        ].Number_Value__c;
        return Integer.valueOf(yearD);
    }

    @TestVisible
    // translate a client field to the field saved in the database
    private static String mapFieldToDB(String prefix, String fieldName) {
        String qFromPage = fieldName.substring(prefix.length()).substringBefore('__c');
        Double qNumFromPage;
        if (qFromPage.contains('-')) {
            qNumFromPage = Double.valueOf(qFromPage.replace('-', '.'));
        } else {
            qNumFromPage = Double.valueOf(qFromPage);
        }

        Integer dbNum;
        if (qNumFromPage <= 5) {
            dbNum = qNumFromPage.intValue();
        } else if (qNumFromPage == 5.5) {
            dbNum = 6;
        } else if (qNumFromPage <= MAX_QUESTION_NUMBER) {
            dbNum = qNumFromPage.intValue() + 1;
        } else {
            throw new SObjectException('Invalid page field: ' + fieldName);
        }
        return prefix + dbNum + '__c';
    }

    @TestVisible
    // translate a database field to the field used by the client
    private static String mapDBToField(String prefix, String fieldName) {
        Integer qNum = Integer.valueOf(fieldName.substring(prefix.length()).substringBefore('__c'));
        if (qNum < 6) {
            return prefix + qNum + '__c';
        } else if (qNum == 6) {
            return prefix + '5-5__c';
        } else if (qNum <= MAX_QUESTION_NUMBER) {
            return prefix + (qNum - 1) + '__c';
        } else {
            throw new SObjectException('Invalid db field: ' + fieldName);
        }
    }

    public static Map<Integer, String> AU_ACCOUNT_FIELDS = new Map<Integer, String>();
    public static Map<Integer, String> NZ_ACCOUNT_FIELDS = new Map<Integer, String>();
    static {
        Map<String, Map<Integer, String>> accountMaps = new Map<String, Map<Integer, String>>{
                'AU' => AU_ACCOUNT_FIELDS, 'NZ' => NZ_ACCOUNT_FIELDS
        };
        for (Notifications_Account_Field_Mapping__mdt mapping : [
                select Label, Account_Field_Name__c, Country_Code__c
                from Notifications_Account_Field_Mapping__mdt
        ]) {
            String qNum = mapping.Label;
            Map<Integer, String> mappings = accountMaps.get(mapping.Country_Code__c);
            mappings.put(Integer.valueOf(qNum.substring(1)), mapping.Account_Field_Name__c);
        }
    }

    private static Set<String> FIELDS = new Set<String>{
            'Name', 'Account__c', 'Date__c', 'Lock__c', 'Residential_Country__c', 'Status__c', 'Year__c'
    };

    // note these are the fields after scheme mapping translation
    private static Set<String> CHECKBOX_FIELDS = new Set<String>{
            'Answer_34__c', 'Answer_35__c', 'Answer_36__c', 'Answer_49__c', 'Answer_50__c', 'Answer_51__c' // Was 'Answer_28__c', 'Answer_29__c', 'Answer_30__c'

    };


}