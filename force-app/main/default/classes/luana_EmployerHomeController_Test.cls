/**
    Developer: WDCi (kh)
    Development Date:17/03/2016
    Task: Luana Test class for luana_EmployerHomeController revoke function
    
    LCA-921 23/08/2016 WDCi - KH: add person account email
**/
@isTest(seeAllData=false)
private class luana_EmployerHomeController_Test {
    
    public static User memberUser {get; set;}
    public static Luana_DataPrep_Test dataPrep;
    public static luana_CommUserUtil userUtil {get; set;} 
    
    private static String classNamePrefixLong = 'luana_EmployerHomeController_Test';
    private static String classNamePrefixShort = 'lehc';
    private static map<String, Id> commProfIdMap {get; set;}
    public static Account memberAccount {get; set;}
    
    public static Payment_Token__c currentPToken {get; set;}
    
    public static void initial(){
        //initialize
        dataPrep = new Luana_DataPrep_Test();
        
        //Create all the custom setting
        insert dataPrep.prepLuanaExtensionSettingCustomSettings();
        insert dataPrep.createLuanaConfigurationCustomSetting();
        
        //Create user with Member and Employer community access
        memberAccount = dataPrep.generateNewApplicantAcc('Joe_' + classNamePrefixShort, classNamePrefixLong, 'Full_Member');
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.PersonEmail = 'joe_1_' + classNamePrefixShort+'@gmail.com';//LCA-921
        memberAccount.Communication_Preference__c= 'Home Phone';
        memberAccount.PersonHomePhone= '1234';
        memberAccount.PersonOtherStreet= '83 Saggers Road';
        memberAccount.PersonOtherCity='JITARNING';
        memberAccount.PersonOtherState='Western Australia';
        memberAccount.PersonOtherCountry='Australia';
        memberAccount.PersonOtherPostalCode='6365'; 
        insert memberAccount;
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
    }
    
    public static void prepareSampleEnrolmentData(){
        
        //LCA-921
        memberUser = [Select Id, Email, FirstName, LastName, UserName, Name from User Where Email =: 'joe_1_' + classNamePrefixShort+'@gmail.com' limit 1];
        
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
       
        

        userUtil = new luana_CommUserUtil(memberUser.Id);
        insert dataPrep.generateNewBusinessAcc('Price Waterhouse_' + classNamePrefixShort, 'Business_Account', 'Chartered Accounting', '11111111', '012112111', 'NZICA', '123 Main Street', 'Active');
        String bAccount = 'Price Waterhouse_' + classNamePrefixShort;
        Account acc = [Select Id, Name from Account Where Name =: bAccount limit 1];
        
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defEmployerPermSetId.value__c));
        list<PermissionSetAssignment> permissAssign = [Select Id from PermissionSetAssignment where AssigneeId =: memberUser.Id AND (PermissionSetId =: defMemberPermSetId.value__c OR PermissionSetId =: defEmployerPermSetId.value__c)];
        System.runAs(memberUser){
            if(permissAssign == null)
                insert psas;
        }
        
        Employment_History__c emplomentHistory = dataPrep.createNewEmploymentHistory(userUtil.custCommAccId, acc.Id, 'Current', 'Engineer');
        insert emplomentHistory;
        currentPToken = dataPrep.createNewPaymentToken(userUtil.custCommAccId, null, System.today(), false);
        insert currentPToken;
    }
    
    public static testMethod void testController1() { 
        Test.startTest();
            initial();
        Test.stopTest();
        prepareSampleEnrolmentData();

        System.runAs(memberUser){
            luana_EmployerHomeController employerHomeCtl = new  luana_EmployerHomeController();
            System.assertEquals(employerHomeCtl.getEmployerOptions().size(), 2, 'Expect 2 Employment History records in the seleceted list,  but get ' + employerHomeCtl.getEmployerOptions().size());
            
            //Select an Employer from the list
            employerHomeCtl.selectedEmployer = employerHomeCtl.getEmployerOptions()[1].getValue();
            //employerHomeCtl.getRelatedBulkCode();
            
            //Test Search fucntion
            employerHomeCtl.doSearch();
            employerHomeCtl.getRelatedBulkCodes();
            
            employerHomeCtl.next();
            employerHomeCtl.previous();
            employerHomeCtl.last();
            employerHomeCtl.first();
            
            employerHomeCtl.doNothing();
            
            
        }
    }
    
    public static testMethod void testController2() { 
        Test.startTest();
            initial();
        Test.stopTest();
        prepareSampleEnrolmentData();

        System.runAs(memberUser){

            PageReference pageRef = Page.luana_EmployerHome;
            Test.setCurrentPage(pageRef);
            System.debug('************3: ' + currentPToken);
            ApexPages.currentPage().getParameters().put('ptid',currentPToken.Id);
            
            luana_EmployerHomeController employerHomeCtl = new  luana_EmployerHomeController();
            
            //Revoke Payment token
            employerHomeCtl.reVokePaymentToken();
            employerHomeCtl.validPaymentTokenSelectedId = currentPToken.Id;
            
            employerHomeCtl.forwardToAuthPage();
            
            employerHomeCtl.getRelatedBulkCodes();
        }
    }
    
}