@isTest
public class ApplicationSelectRecTypConTest {

    static testMethod void getApplicationsForUser(){
        Account testAccount=TestObjectCreator.createFullMemberAccount();
        testAccount.Membership_Type__c='Member';

        Test.startTest();
        insert testAccount; // Future method
        Test.stopTest();
        Id testContactId = [Select id FROM Contact WHERE AccountId =: testAccount.Id limit 1].Id;
        User testCommunityUser = [Select Email, UserName, AccountId, FederationIdentifier FROM User WHERE contactId=:testContactId limit 1];
        system.debug('testCommunityUser='+testCommunityUser);
        
        Application__c testAppDraft=new Application__c(Account__c=testAccount.Id
                                                       ,Application_Status__c='Draft');
        
        Application__c testAppApproved=new Application__c(Account__c=testAccount.Id
                                                          ,Application_Status__c='Approved'
                                                          ,Application_Assessor__c=testCommunityUser.Id
                                                          ,Application_Fee_Paid1__c='Paid',Out_of_Approved_Employment__c = 'Yes');
        List<Application__c> testApplications=new List<Application__c>();
        testApplications.add(testAppDraft);
        
        insert testApplications;
        
        System.runAs(testCommunityUser){
            
            List<String> lstAppRecordTypes= ApplicationSelectRecordTypeController.getApplicationRecordTypeNames(false,true);
            System.assertNotEquals(lstAppRecordTypes,null,'No record types returned unexpectedly for Application');
        }
    }
}