/*
    Developer: WDCi (Lean)
    Date: 08/Jul/2016
    Task #: LCA-724 CPD Hours allocation
    
    Change History:
    LCA-727 13/Jul/2016 - WDCi Lean: update Attendance/Resource Booking start time & end time from session
*/

public with sharing class luana_SessionHandler {
    
    public static void initiateFlow(List<LuanaSMS__Session__c> newList, Map<Id, LuanaSMS__Session__c> newMap, List<LuanaSMS__Session__c> oldList, Map<Id, LuanaSMS__Session__c> oldMap, boolean isInsert, boolean isUpdate, boolean isDelete, boolean isUndelete, boolean isBefore, boolean isAfter){
        
        if(isAfter){
            if(isUpdate){
                setResourceBookingCPDFlag(newList, oldMap);
                setAttendanceTime(newList, oldMap);
                setResourceBookingTime(newList, oldMap);
            }
        }
    }
    
    public static void setResourceBookingCPDFlag(List<LuanaSMS__Session__c> newList, Map<Id, LuanaSMS__Session__c> oldMap){
        
        Set<Id> sessionIds = new Set<Id>();
        List<LuanaSMS__Resource_Booking__c> resourceBookingsToUpdate = new List<LuanaSMS__Resource_Booking__c>();
        
        for(LuanaSMS__Session__c session : newList){
            
            LuanaSMS__Session__c oldSession;
            
            if(oldMap != null && oldMap.containsKey(session.Id)){
                oldSession = oldMap.get(session.Id);
            }
            
            if(session.Allocate_CPD_Hours__c && (oldSession == null || !oldSession.Allocate_CPD_Hours__c)){
                sessionIds.add(session.Id);
            }
        }
        
        for(LuanaSMS__Resource_Booking__c rb : [select id, Allocate_CPD_Hours__c from LuanaSMS__Resource_Booking__c where LuanaSMS__Session__c in: sessionIds and Allocate_CPD_Hours__c = false]){
            LuanaSMS__Resource_Booking__c rbToUpdate = new LuanaSMS__Resource_Booking__c(Id = rb.Id, Allocate_CPD_Hours__c = true);
            
            resourceBookingsToUpdate.add(rbToUpdate);
        }
        
        if(resourceBookingsToUpdate.size() > 0){
            update resourceBookingsToUpdate;
        }
        
    }
    
    public static void setAttendanceTime(List<LuanaSMS__Session__c> newList, Map<Id, LuanaSMS__Session__c> oldMap){
        
        Map<Id, LuanaSMS__Session__c> updatedSessions = new Map<Id, LuanaSMS__Session__c>();
        
        for(LuanaSMS__Session__c session : newList){
            if(oldMap != null && oldMap.containsKey(session.Id) && (oldMap.get(session.Id).LuanaSMS__Start_Time__c != session.LuanaSMS__Start_Time__c || oldMap.get(session.Id).LuanaSMS__End_Time__c != session.LuanaSMS__End_Time__c) ){
                updatedSessions.put(session.Id, session);
            }
        }
        
        List<LuanaSMS__Attendance2__c> attendancesToUpdate = new List<LuanaSMS__Attendance2__c>();
        for(LuanaSMS__Attendance2__c attendance : [select id, LuanaSMS__Start_Time__c, LuanaSMS__End_Time__c, LuanaSMS__Session__c from LuanaSMS__Attendance2__c where LuanaSMS__Session__c in: updatedSessions.keySet()]){
            
            if(updatedSessions.containsKey(attendance.LuanaSMS__Session__c)){
                attendance.LuanaSMS__Start_Time__c = updatedSessions.get(attendance.LuanaSMS__Session__c).LuanaSMS__Start_Time__c;
                attendance.LuanaSMS__End_Time__c = updatedSessions.get(attendance.LuanaSMS__Session__c).LuanaSMS__End_Time__c;
                
                attendancesToUpdate.add(attendance);
            }
            
        }
        
        if(attendancesToUpdate.size() > 0){
            integer counter = 0;
            for(Database.SaveResult sr : Database.update(attendancesToUpdate, false)){
                
                if(!sr.isSuccess()){
                    LuanaSMS__Session__c sessionToError = updatedSessions.get(attendancesToUpdate.get(counter).LuanaSMS__Session__c);
                    sessionToError.addError('Failed to update the related Attendance record (' + attendancesToUpdate.get(counter).Id + '). Error: ' + sr.getErrors()[0].getMessage());
                }
                
                counter++;
            }
            
        }
        
    }
    
    public static void setResourceBookingTime(List<LuanaSMS__Session__c> newList, Map<Id, LuanaSMS__Session__c> oldMap){
        
        Map<Id, LuanaSMS__Session__c> updatedSessions = new Map<Id, LuanaSMS__Session__c>();
        
        for(LuanaSMS__Session__c session : newList){
            if(oldMap != null && oldMap.containsKey(session.Id) && (oldMap.get(session.Id).LuanaSMS__Start_Time__c != session.LuanaSMS__Start_Time__c || oldMap.get(session.Id).LuanaSMS__End_Time__c != session.LuanaSMS__End_Time__c) ){
                updatedSessions.put(session.Id, session);
            }
        }
        
        List<LuanaSMS__Resource_Booking__c> rBookingsToUpdate = new List<LuanaSMS__Resource_Booking__c>();
        for(LuanaSMS__Resource_Booking__c rBooking : [select id, LuanaSMS__Start_Time__c, LuanaSMS__End_Time__c, LuanaSMS__Session__c from LuanaSMS__Resource_Booking__c where LuanaSMS__Session__c in: updatedSessions.keySet()]){
            
            if(updatedSessions.containsKey(rBooking.LuanaSMS__Session__c)){
                rBooking.LuanaSMS__Start_Time__c = updatedSessions.get(rBooking.LuanaSMS__Session__c).LuanaSMS__Start_Time__c;
                rBooking.LuanaSMS__End_Time__c = updatedSessions.get(rBooking.LuanaSMS__Session__c).LuanaSMS__End_Time__c;
                
                rBookingsToUpdate.add(rBooking);
            }
            
        }
        
        if(rBookingsToUpdate.size() > 0){
            integer counter = 0;
            for(Database.SaveResult sr : Database.update(rBookingsToUpdate, false)){
                
                if(!sr.isSuccess()){
                    LuanaSMS__Session__c sessionToError = updatedSessions.get(rBookingsToUpdate.get(counter).LuanaSMS__Session__c);
                    sessionToError.addError('Failed to update the related Resource Booking record (' + rBookingsToUpdate.get(counter).Id + '). Error: ' + sr.getErrors()[0].getMessage());
                }
                
                counter++;
            }
            
        }
        
    }
}