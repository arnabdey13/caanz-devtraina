@IsTest @TestVisible
private with sharing class TestDataDegreeJoinBuilder {

    private final Id uni;
    private final Id degree;

    @TestVisible
    private TestDataDegreeJoinBuilder(Id uni, Id degree) {
        this.uni = uni;
        this.degree = degree;
    }

    @TestVisible
    private List<edu_University_Degree_Join__c> create(Integer count, Boolean persisted) {
        List<edu_University_Degree_Join__c> results = new List<edu_University_Degree_Join__c>();
        for (Integer i = 0; i < count; i++) {
            results.add(new edu_University_Degree_Join__c(
                    University__c = this.uni,
                    Degree__c = this.degree));
        }
        if (persisted) insert results;
        return results;
    }

    @TestVisible
    private static edu_University_Degree_Join__c joinedApproved(String universityName, String country, String degreeName, String degreeType) {
        List<edu_University__c> universities = new TestDataUniversityBuilder(universityName)
                .status('Approved')
                .country(country)
                .create(1, true);
        List<edu_Degree__c> degrees = new TestDataDegreeBuilder(degreeName)
                .type(degreeType)
                .status('Approved')
                .create(1, true);
        List<edu_University_Degree_Join__c> joins = new TestDataDegreeJoinBuilder(universities.get(0).Id, degrees.get(0).Id).create(1, true);
        return joins.get(0);
    }

    @TestVisible
    private static edu_University_Degree_Join__c joinedOptedOut(String universityName, String degreeName) {
        List<edu_University__c> universities = new TestDataUniversityBuilder(universityName)
                .status('Submitted')
                // no country when opting out
                .create(1, true);
        List<edu_Degree__c> degrees = new TestDataDegreeBuilder(degreeName)
                .type('Undergraduate')
                .status('Submitted')
                .create(1, true);
        List<edu_University_Degree_Join__c> joins = new TestDataDegreeJoinBuilder(universities.get(0).Id, degrees.get(0).Id).create(1, true);
        return joins.get(0);
    }

}