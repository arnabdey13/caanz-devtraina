/*
    Developer: WDCi (Lean)
    Development Date: 13/04/2016
    Task #: luana_MyEducationRecordsSearch for enrolment wizard
    
    Change History
    LCA-555 29-06-2016 KH: Include edit and delete methods
    LCA-553/LCA-901 10/08/2016 - WDCi Lean: remove noncommunity check
    LCA-1070 15/Dec/2016 WDCi - Lean: change date search from Date Commenced to Date Completed
*/

public without sharing class luana_MyEducationRecordsSearchController {
    
    public Education_Record__c educationRecordObj {get; set;}
    public List<Education_Record__c> educationRecords {get; set;}
    
    public date dateFrom{get; set;}
    public date dateTo{get; set;}
    
    public String uniProfOrg {get; set;}
    
    public Id custCommConId {public get; private set;}
    public Id custCommAccId {public get; private set;}
    
    public String selectedEduId {get; set;}
    
    //LCA-555
    public Map<Id, boolean> isValidToEditDelete {get; set;}

    
    public luana_MyEducationRecordsSearchController(){
        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId());
        // /*
        this.custCommConId = commUserUtil.custCommConId;
        this.custCommAccId = commUserUtil.custCommAccId;
        // */
        
        isValidToEditDelete = new Map<Id, boolean>();
        
         /*
        this.custCommConId = '003p000000431MN';
        this.custCommAccId = '001p0000004OMw7';
         */
        
        educationRecordObj = new Education_Record__c();

    }
    
    public PageReference doNew(){
        return Page.luana_MyNewEducationRecord;
    }
    
    public PageReference doSearch(){
        String result = getFilterQuery();
        educationRecords = Database.query(result);
        //LCA-555
        for(Education_Record__c er: educationRecords){
            if(er.Contact_Student__c == custCommConId && er.Community_User_Entry__c){
                isValidToEditDelete.put(er.Id, true);
            }else{
                isValidToEditDelete.put(er.Id, false);  
            }
        }
        
        return null;
    }
    
    public String getFilterQuery(){
        
        String whereStr = '';
        
        Map<String, String> filterMap = new Map<String, String>();

        filterMap.put('Qualifying_hours__c', 'Qualifying_hours__c = ');
        filterMap.put('Enter_university_instituition__c', 'Enter_university_instituition__c like ');
        filterMap.put('Event_Course_name__c', 'Event_Course_name__c like ');
        
        Map<String, String> filterValue = new Map<String, String>();
                   
        if(educationRecordObj.Qualifying_hours_type__c!=null){
            filterValue.put('Qualifying_hours__c', '\''+educationRecordObj.Qualifying_hours__c+'\'');
        }else{
            filterValue.put('Qualifying_hours__c', null);
        }
        if(uniProfOrg != null && uniProfOrg != ''){
            filterValue.put('Enter_university_instituition__c', '\'%'+String.escapeSingleQuotes(uniProfOrg)+'%\'');
        }else{
            filterValue.put('Enter_university_instituition__c', null);
        }
        if(educationRecordObj.Event_Course_name__c !=null && educationRecordObj.Event_Course_name__c != ''){
            filterValue.put('Event_Course_name__c', '\'%'+String.escapeSingleQuotes(educationRecordObj.Event_Course_name__c)+'%\'');
        }else{
            filterValue.put('Event_Course_name__c', null);
        }
        /**/
        
        
        String dateSearch;
        if(dateFrom != null && dateTo == null){
            String fDate = datetime.newInstance(dateFrom.year(), dateFrom.month(),dateFrom.day()).format('yyyy-MM-dd');
            dateSearch = '(Date_completed__c >= ' + fDate + ')'; //LCA-1070 change from Date_Commenced__c to Date_completed__c
        } else if(dateTo != null && dateFrom == null){
            String tDate = datetime.newInstance(dateTo.year(), dateTo.month(),dateTo.day()).format('yyyy-MM-dd');
            dateSearch = '(Date_completed__c <= ' + tDate + ')'; //LCA-1070 change from Date_Commenced__c to Date_completed__c
        }else if(dateTo != null && dateFrom != null){
            String fDate = datetime.newInstance(dateFrom.year(), dateFrom.month(),dateFrom.day()).format('yyyy-MM-dd');
            String tDate = datetime.newInstance(dateTo.year(), dateTo.month(),dateTo.day()).format('yyyy-MM-dd');
            dateSearch = '(Date_completed__c >= ' + fDate + ' and Date_completed__c <= ' + tDate + ')'; //LCA-1070 change from Date_Commenced__c to Date_completed__c
        }else{
            dateSearch = null;
        }
        
        String filterStr = prepareFilterStr(filterMap, filterValue, dateSearch);
        
        String queryStr = 'SELECT Id, Name, Specialist_hours_code__c, Community_User_Entry__c, Date_Commenced__c, Event_Course_name__c, Qualifying_hours__c, '+
                'Qualifying_hours_type__c, Contact_Student__c, CAHDP__c, Date_completed__c, Country__c, State__c, University_professional_organisation__c,'+
                'University_professional_organisation__r.Name, Enter_university_instituition__c, Verified__c FROM Education_Record__c ' + filterStr + ' ORDER BY Date_completed__c DESC'; //LCA-1070 change order by from Date_Commenced__c to Date_completed__c
                
        System.debug('***queryStr: ' + queryStr);
        return queryStr;
    }
    
    //LCA-311, get template name
    public String getTemplateName(){
		return luana_NetworkUtil.getTemplateName();
		
        /*LCA-901
    	if(luana_NetworkUtil.isMemberCommunity()){
        	return 'luana_MemberTemplate';
    	} else if(luana_NetworkUtil.isNonMemberCommunity()){
        	return 'luana_NonMemberTemplate';
    	} else if(luana_NetworkUtil.isInternal()){
        	return 'luana_InternalTemplate';
     	}
		
    	return '';*/
    }
    
    public String prepareFilterStr(Map<String, String> filterMap, Map<String, String> filterValue, String dateSearch){
        
        String whereStr = '';
        
        List<String> filters = new List<String>();
        if(!filterMap.isEmpty()){
            for(String strField: filterMap.keySet()){
                if(filterValue.get(strField) != null){
                    filters.add(filterMap.get(strField) + filterValue.get(strField));
                }
            }
            filters.add('Contact_Student__c=\''+ String.escapeSingleQuotes(custCommConId) + '\'');
        }
        if(dateSearch != null){
            filters.add(dateSearch);
        }
        for(String filterStr: filters){
            whereStr += filterStr + ' and ';
        }
        
        if(whereStr.length() > 0){
            return ' Where ' + whereStr.subString(0, whereStr.lastIndexOf('and'));
        }else{
            return '';
        } 
        
        return null;
    }
    
    //LCA-555
    public pageReference doEdit(){
        PageReference pageRef;
        pageRef = Page.luana_MyNewEducationRecord;
        pageRef.getParameters().put('id', selectedEduId);
        return pageRef; 
    }
    
    //LCA-555
    public pageReference doDelete(){
        try{
            List<Education_Record__c> deleteEdus = new List<Education_Record__c>();
            for(Education_Record__c er: [Select Id from Education_Record__c Where Id =: selectedEduId and Contact_Student__c =: custCommConId]){
                deleteEdus.add(er);
            }
            delete deleteEdus;

            PageReference pageRef;
            pageRef = ApexPages.currentPage();
            return pageRef;
        }Catch(Exception exp){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Fail to delete the Education Record. '+ exp.getMessage() +'. Please contact our support if you would like to proceed with the deletion.'));
            return null;
        }
    }
}