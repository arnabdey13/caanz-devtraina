/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestEmploymentHistoryTrigger {
	private static Account BusinessAccountObjectInDB;
	private static Employment_History__c EmploymentHistoryObjectInDB;
	static{
		insertBusinessAccountObject();
	}
	private static void insertBusinessAccountObject(){
		BusinessAccountObjectInDB = TestObjectCreator.createBusinessAccount();
		//## Required Relationships
		//## Additional fields and relationships / Updated fields
		insert BusinessAccountObjectInDB;
	}
	private static Employment_History__c getEmploymentHistoryObject(){
		Employment_History__c EmploymentHistoryObject = TestObjectCreator.createEmploymentHistory();
		//## Required Relationships
		EmploymentHistoryObject.Employer__c = BusinessAccountObjectInDB.Id;
		//## Additional fields and relationships / Updated fields
		EmploymentHistoryObject.Is_CPP_Provided__c = false;
		return EmploymentHistoryObject;
	}
	
	//******************************************************************************************
	//                             TestMethods
	//******************************************************************************************
	static testMethod void TestEmploymentHistoryTrigger_ProvisionalMember() {
		Account FullMemberAccountObject = TestObjectCreator.createFullMemberAccount();
		FullMemberAccountObject.Membership_Class__c = 'Provisional';
		insert FullMemberAccountObject;
		
		Employment_History__c EmploymentHistoryObject = getEmploymentHistoryObject();
		EmploymentHistoryObject.Member__c = FullMemberAccountObject.Id;
		insert EmploymentHistoryObject;
		
		// Check Results
		System.debug('##r ' + 
			[Select e.Rollup_Provisional_Member__c From Employment_History__c e where e.Id=:EmploymentHistoryObject.Id]
		);
	}
}