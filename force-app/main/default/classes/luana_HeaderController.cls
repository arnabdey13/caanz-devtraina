/* 
    Developer: WDCi
    
    Change History
    LCA-701: 28/06/2016 - include Contributor permissionSet
*/

public class luana_HeaderController {
    Id custCommConId;
    Id custCommAccId;
    public Boolean hasAccessToMember {set; get;}
    public Boolean hasAccessToEmployer {set; get;}
    public Boolean hasAccessToNonMember {set; get;}
    
    //LCA-701
    public Boolean hasAccessToContributor {set; get;}
    
    public String homePageUrl {private set; public get;}
    
    public String userPhotoUrl {get; set;}//LCA-346
    
    public luana_HeaderController() {
    
        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId()); 
       
        this.custCommConId = commUserUtil.custCommConId;       
        this.custCommAccId = commUserUtil.custCommAccId;
        this.hasAccessToMember = commUserUtil.hasAccessToMember;
        this.hasAccessToEmployer = commUserUtil.hasAccessToEmployer;
        this.hasAccessToNonMember = commUserUtil.hasAccessToNonMember;
        
        //LCA-701
        this.hasAccessToContributor = commUserUtil.hasAccessToContributor;
        
        //LCA-247
        homePageUrl = luana_NetworkUtil.getCommunityPath();
        
        //LCA-346
        userPhotoUrl = commUserUtil.getUserPhoto(Network.getNetworkId(), UserInfo.getUserId());
    }
}