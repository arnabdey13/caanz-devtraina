@IsTest @TestVisible
private with sharing class TestDataFFRichTextBuilder {

    private String name;
    private String content;

    @TestVisible
    private TestDataFFRichTextBuilder(String name) {
        this.name = name;
    }

    @TestVisible
    private TestDataFFRichTextBuilder content(String content) {
        this.content = content;
        return this;
    }

    @TestVisible
    private List<ff_RichTextStore__c> create(Integer count, Boolean persisted) {
        List<ff_RichTextStore__c> results = new List<ff_RichTextStore__c>();
        for (Integer i = 0; i < count; i++) {
            results.add(new ff_RichTextStore__c(
                    Name__c = this.name,
                    Display_Content__c = this.content));
        }
        if (persisted) insert results;
        return results;
    }

    @TestVisible
    private static List<ff_RichTextStore__c> multiple(List<String> names, String content, Boolean persisted) {
        List<ff_RichTextStore__c> result = new List<ff_RichTextStore__c>();
        for (String name: names) {
            result.addAll(new TestDataFFRichTextBuilder(name).content(content).create(1, persisted));
        }
        return result;
    }

}