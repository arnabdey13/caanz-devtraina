public with sharing class DeclarationOverrideController {
	private Declaration__c Declaration;
	private Account AccountObj;
	
	public DeclarationOverrideController(ApexPages.StandardController controller) {
		Declaration = (Declaration__c) controller.getRecord();
		
		if( UserInfo.getProfileId()==ProfileCache.getId('NZICA Community Login User') ){
			User CurrentUser = [Select u.Contact.AccountId, u.ContactId 
				From User u where u.Id=:UserInfo.getUserId()];
			AccountObj = [Select Name, Membership_Class__c, RecordType.Name
				From Account where Id=:CurrentUser.Contact.AccountId];
		}
	}
	
	public PageReference gotoNewPage(){
		if( UserInfo.getProfileId()==ProfileCache.getId('NZICA Community Login User') ){
			if(AccountObj.Membership_Class__c!='Full'){
				return Page.DeclarationWizardAccessDenied;
			}
			else{
				return Page.DeclarationWizardPage1;
			}
		}
		else{
			String currentPageUrl = ApexPages.currentPage().getUrl();
			String DeclarationNewOverrideString = (Test.isRunningTest())?
				'/apex/declarationeditoverride':'/apex/DeclarationNewOverride';
			currentPageUrl = currentPageUrl.replace( DeclarationNewOverrideString,
				'/'+ Schema.sObjectType.Declaration__c.getKeyPrefix() +'/e');
			currentPageUrl = currentPageUrl.replace('&save_new=1','');
			
			PageReference PR = new PageReference( currentPageUrl );
			PR.getParameters().put('nooverride','1');
			return PR;
		}
	}
	
	public PageReference gotoEditPage(){
		if( UserInfo.getProfileId()==ProfileCache.getId('NZICA Community Login User') ){
			if(AccountObj.Membership_Class__c!='Full'){
				return Page.DeclarationWizardAccessDenied;
			}
			else if( Declaration.Declaration_Status__c=='In Progress' ){
				return Page.DeclarationWizardPage1;
			}
			else{
				return Page.DeclarationWizardNoAccess;
			}
		}
		else{
			PageReference PR = new PageReference('/' + Declaration.Id +
				'/e?retURL=%2F' + Declaration.Id );
			PR.getParameters().put('nooverride','1');
			return PR;
		}
	}
}