/*------------------------------------------------------------------------------------
Author:        Paras Prajapati
Company:       Salesforce
Description:   Test Class for CASUB_EmploymentHistory_CA

History
Date            Author             Comments
--------------------------------------------------------------------------------------
04-02-2019     Paras Prajapati       	Initial Release
------------------------------------------------------------------------------------*/
@isTest
public class CASUB_EmploymentHistory_CA_Test {
    
    private static User currentUser;
    private static Account currentAccount;
    private static Contact currentContact;
    private static My_Preference__c myPreference;

    static {
        currentAccount = TestObjectCreator.createFullMemberAccount();
        currentAccount.Affiliated_Branch_Country__c = 'Australia';
        currentAccount.Member_Of__c = 'NZICA' ;

        insert currentAccount;
        currentContact = [SELECT Id FROM Contact WHERE AccountId =: currentAccount.Id];
    }

    private static User getCurrentUser(){
        Test.startTest(); Test.stopTest(); // Future method needs to run to create the user.
        return currentUser = [SELECT Name,Id,AccountId FROM User WHERE AccountId =: currentAccount.Id];
    }
    @isTest static void test_CASUB_EmploymentHistory_CA_Controller() {
        Account BusinessAccountObject = TestObjectCreator.createBusinessAccount();
        BusinessAccountObject.BillingStreet = '8 Nelson Street';
        BusinessAccountObject.Member_Of__c = 'NZICA' ;
        BusinessAccountObject.Status__c = 'Active' ;
        insert BusinessAccountObject;
        
        System.runAs(getCurrentUser()) {
            //System.debug('getCurrentUser().Id'+getCurrentUser().Id);
            //System.debug('userRec.AccountId' + getCurrentUser().AccountId);
            CASUB_EmploymentHistory_CA empClass = new CASUB_EmploymentHistory_CA();
            CASUB_EmploymentHistory_CA.returnCurrntUserMember() ;
            List<Employment_History__c> empLoadList = CASUB_EmploymentHistory_CA.EmpList();
            System.assertEquals(0, empLoadList.size(), 'empLoadList.size' );
            //Creating new Employment History record.
            Employment_History__c EmploymentHistoryObject = TestObjectCreator.createEmploymentHistory();         
            EmploymentHistoryObject.Job_Title__c = 'SFDC Dev Opps Team' ;
            EmploymentHistoryObject.Is_CPP_Provided__c = false;
            EmploymentHistoryObject.Employee_Start_Date__c = System.today() - 20 ;
            //EmploymentHistoryObject.Employee_End_Date__c = System.today() - 2 ;
            EmploymentHistoryObject.Status__c = 'Current';
            CASUB_EmploymentHistory_CA.getAccountupdatedlist(EmploymentHistoryObject, BusinessAccountObject.Id) ; 
            System.debug('EmploymentHistoryObject Id' + EmploymentHistoryObject);
            List<Employment_History__c> empInsertList = CASUB_EmploymentHistory_CA.EmpList();
            System.assertEquals(1, empInsertList.size(), 'empLoadList.size after insert' );
            
            Employment_History__c  empRecord = CASUB_EmploymentHistory_CA.getEmpRecordDetail(empInsertList[0].Id) ;
            System.assertEquals('SFDC Dev Opps Team', empRecord.Job_Title__c, 'empLoadList Job Title' );
            
            CASUB_EmploymentHistory_CA.updateEmpRecord(empInsertList[0]) ;
            List<Account> accList = CASUB_EmploymentHistory_CA.findByName('Test', 'NZICA') ;
            CASUB_EmploymentHistory_CA.getStatusPickListClass() ;
            CASUB_EmploymentHistory_CA.getCPPPickListClass();
            CASUB_EmploymentHistory_CA.checkPrimaryEmpDuplicate(currentAccount.Id);
        }
    }
    /*
    private static Account getFullMemberAccountObject(){
        Account FullMemberAccountObject = TestObjectCreator.createFullMemberAccount();
        FullMemberAccountObject.Member_Of__c = 'NZICA' ;
        insert FullMemberAccountObject;
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        return FullMemberAccountObject;
    }
    
    static testMethod void empListTestMethod(){
        Account BusinessAccountObject = TestObjectCreator.createBusinessAccount();
        BusinessAccountObject.BillingStreet = '8 Nelson Street';
        BusinessAccountObject.Member_Of__c = 'NZICA' ;
        BusinessAccountObject.Status__c = 'Active' ;
        insert BusinessAccountObject;
        
        Account FullMemberAccountObject = getFullMemberAccountObject();
        Test.startTest() ;
        // Future method
        Test.stopTest() ;
        // Check Results
        Id FullMemberContactId = [Select id FROM Contact WHERE AccountId =: FullMemberAccountObject.Id].Id;
        List<User> User_List = [Select FederationIdentifier FROM User WHERE contactId=:FullMemberContactId];
        System.assertEquals(1, User_List.size(), 'User_List.size' );
        System.assertNotEquals(null, User_List[0].FederationIdentifier, 'FederationIdentifier' );
        system.runAs(User_List[0]) {
            CASUB_EmploymentHistory_CA empClass = new CASUB_EmploymentHistory_CA();
            CASUB_EmploymentHistory_CA.returnCurrntUserMember() ;
            List<Employment_History__c> empLoadList = CASUB_EmploymentHistory_CA.EmpList();
            System.assertEquals(0, empLoadList.size(), 'empLoadList.size' );
            //Creating new Employment History record.
            Employment_History__c EmploymentHistoryObject = TestObjectCreator.createEmploymentHistory();         
            EmploymentHistoryObject.Job_Title__c = 'SFDC Dev Opps Team' ;
            EmploymentHistoryObject.Is_CPP_Provided__c = false;
            EmploymentHistoryObject.Employee_Start_Date__c = System.today() - 20 ;
            EmploymentHistoryObject.Employee_End_Date__c = System.today() - 2 ;
            CASUB_EmploymentHistory_CA.getAccountupdatedlist(EmploymentHistoryObject, BusinessAccountObject.Id) ;
            
            List<Employment_History__c> empInsertList = CASUB_EmploymentHistory_CA.EmpList();
            System.assertEquals(1, empInsertList.size(), 'empLoadList.size after insert' );
            
            Employment_History__c  empRecord = CASUB_EmploymentHistory_CA.getEmpRecordDetail(empInsertList[0].Id) ;
            System.assertEquals('SFDC Dev Opps Team', empRecord.Job_Title__c, 'empLoadList Job Title' );
            
            CASUB_EmploymentHistory_CA.updateEmpRecord(empInsertList[0]) ;
            List<Account> accList = CASUB_EmploymentHistory_CA.findByName('Test', 'NZICA') ;
            CASUB_EmploymentHistory_CA.getStatusPickListClass() ;
        }
        
    } */
}