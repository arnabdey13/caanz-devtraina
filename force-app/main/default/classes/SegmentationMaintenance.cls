public with sharing class SegmentationMaintenance {
    
    private static Boolean debug = true;
    
	// Remove Duplicate Segments - parameter indicating test run - TRUE test run only
    // Typically created by Merging records
    // SegmentationMaintenance.removeDuplicates( TRUE);
	public static void removeDuplicates(Boolean testRun){
		if (debug) System.debug( 'TEST PROCESSING - Segmentation Remove Duplicates');

        AggregateResult[] duplicates  = [select Account__c, count(Id) total
                                          from Segmentation__c 
                                          where Account__r.Membership_Type__c = 'Member'
                                          group by Account__c 
                                          having count(Id) = 2 ];
  
        Map<Id, Decimal> dups = new Map<Id, Decimal>();
        for (Integer idx=0; idx<duplicates.size(); idx++){
            Id acct = (Id)duplicates[idx].get('Account__c');
            Decimal total = (Decimal)duplicates[idx].get('total');
            // System.debug('Duplicate:' + acct + ' Total:' + total);
            dups.put(acct, total);
        }       
        
        List<Segmentation__c> dupSegments  = [ select Id, Account__c, Business_Segmentation_MS__c,  Geo_AU_SA1__c, Geo_NZ_Meshblock__c, Organisation_Type__c 
                                           		 from Segmentation__c 
                                                 where Account__c IN : dups.keySet()
                                                 order by Account__c, LastModifiedDate desc NULLS LAST ];        

        List<Id> toDelete = new List<Id>();
        for (Integer i=0; i<dupSegments.size(); i=i+2){
            if (dupSegments[i].Geo_AU_SA1__c != NULL){
                if ( dupSegments[i].Geo_AU_SA1__c == dupSegments[i+1].Geo_AU_SA1__c )
                	toDelete.add(dupSegments[i+1].Id);
                else
					System.debug('Duplicate: SA1 Not Resolved:' + dupSegments[i]); 
            }
            else if (dupSegments[i].Geo_NZ_Meshblock__c != NULL){
                if ( dupSegments[i].Geo_NZ_Meshblock__c == dupSegments[i+1].Geo_NZ_Meshblock__c )
                	toDelete.add(dupSegments[i+1].Id);    
                else
					System.debug('Duplicate: Meshblock Not Resolved:' + dupSegments[i]);     
            }
            else if (dupSegments[i].Geo_AU_SA1__c == NULL && dupSegments[i+1].Geo_AU_SA1__c == NULL &&
                     dupSegments[i].Geo_NZ_Meshblock__c == NULL && dupSegments[i+1].Geo_NZ_Meshblock__c == NULL )
            {
            	System.debug('None has geography:' + dupSegments[i] + ' & ' + dupSegments[i+1]);
				toDelete.add(dupSegments[i+1].Id); 
            }
            else 
            	System.debug('Duplicate Not Resolved:' + dupSegments[i]);           
        }
        
        if (testRun)
			System.debug('Duplicates Resolved to Delete:' + toDelete); 
        else {
			System.debug('Removing:' + toDelete);
            List<Segmentation__c> deleteSegments  = [ select Id, Account__c from Segmentation__c where Id in : toDelete];    
            delete deleteSegments;
        }  
    }  
    
    // Refresh Some of existing Segments
    //         String lastModDate = DateTime.now().addDays(1).format('yyyy-MM-dd\'T\'hh:mm:ss\'z\'');  
	public static void refreshOldExistingSegments(String lastModDate, Integer lim){
		if (debug) System.debug( 'TEST PROCESSING - Segmentation Refresh');
		String whereQry = ' Where Id in (Select Account__c from Segmentation__c where Business_Segmentation_MS__c != NULL  ';
		       whereQry += ' And LastModifiedDate < ' + lastModDate + ')';
		if (lim > 0)
		 		whereQry += ' LIMIT ' + lim; 		
		
        if (debug) System.debug(whereQry);
		SegmentationRefresh objClass = new SegmentationRefresh(whereQry);
		Database.executeBatch (objClass, 20);
	}

    // Refresh All existing Segments
	public static void refreshAllExistingSegments(){
		if (debug) System.debug( 'TEST PROCESSING - Segmentation Refresh');
		String whereQry = ' Where Id in (Select Account__c from Segmentation__c where Business_Segmentation_MS__c != NULL ) ';		
		
		SegmentationRefresh objClass = new SegmentationRefresh(whereQry);
		Database.executeBatch (objClass, 20);
	}
	
	// Refresh All existing Members
	public static void refreshAllMembers(){
		if (debug) System.debug( 'TEST PROCESSING - Segmentation Refresh');
		String whereQry = ' Where Membership_Type__c = \'Member\' And Status__c = \'Active\'  ';		
		
		SegmentationRefresh objClass = new SegmentationRefresh(whereQry);
		Database.executeBatch (objClass, 20);
	}
	
	// Create Segments for New Members
	public static void refreshNewMembers(){
		if (debug) System.debug( 'TEST PROCESSING - Segmentation Refresh');
		String whereQry =   ' Where Membership_Type__c = \'Member\' And Status__c = \'Active\'  ';		
				whereQry += ' And ( Id not in (Select Account__c from Segmentation__c where Business_Segmentation_MS__c != NULL))';
		SegmentationRefresh objClass = new SegmentationRefresh(whereQry);
		Database.executeBatch (objClass, 20);
	} 
}