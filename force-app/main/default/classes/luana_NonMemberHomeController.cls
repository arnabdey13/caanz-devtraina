/**
    Developer: WDCi (Lean)
    Development Date: 11/04/2016
    Task #: luana_NonMemberHome Wizard
    
    Change Histroy:
    LCA-487 07/07/2016 - WDCi Lean: update authentication redirection to custom login page
**/

public without sharing class luana_NonMemberHomeController {
    
    public Id custCommConId {get; private set;}
    public Id custCommAccId {get; private set;}
    
    public luana_NonMemberHomeController(){
        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId());
        // /*
        this.custCommConId = commUserUtil.custCommConId;
        this.custCommAccId = commUserUtil.custCommAccId;
        // */
        
         /*
        this.custCommConId = '003p000000431MN';
        this.custCommAccId = '001p0000004OMw7';
         */
    }
    
    public PageReference forwardToAuthPage(){
        if(UserInfo.getUserType() == 'Guest'){
            //Fix for issue LCA-327 & LCA-328
            //return new PageReference('/CommunitiesLogin');
            return new PageReference(URL.getSalesforceBaseUrl().toExternalForm()+'/nonmember/luana_Login?startURL=/nonmember/luana_NonMemberHome'); //LCA-487
        }
        else{
            return null;
        }
    }
    
    public PageReference viewEnrolments(){
        return Page.luana_NonMemberMyEnrolment;
    }
    
    public PageReference viewEducations(){
        return Page.luana_MyEducationRecordsSearch;
    }
    
    public PageReference doNewEducationRecord(){
        return Page.luana_MyNewEducationRecord;
    }
    
}