/*------------------------------------------------------------------------------------
Author:        Rinaldo Clucher
Company:       Salesforce
Description:   test class for cpd Batch Handler class

History
Date            Author             Comments
--------------------------------------------------------------------------------------
16-08-2018     Rinaldo Clucher       	Initial Release
------------------------------------------------------------------------------------*/
@isTest
private with sharing class CPDBatchHandlerTest {

	private static List<String> specialistHoursCodes;
	
	static{
		specialistHoursCodes = new List<String>{
			'Type F - Financial Planning Specialist',
			'Type A - Registered Company Auditor',
			'Type L - Registered Company Liquidator',
			'Type T - Registered Tax Agent',
			'Type AF - Australian Financial Services Licensee (AFSL)', 
			'Type B - Registered Trustee in Bankruptcy'
		};
	}

	@testSetup static void createTestData() {
		createCPDDefaultValuesConfig();
    }

    private static void createCPDDefaultValuesConfig(){
    	CPD_Default_Values__c config = new CPD_Default_Values__c(
    		Total_Required_Annual_Hours_ACA__c = 15,
    		Total_Required_Annual_Hours_AT__c = 60,
    		Total_Required_Annual_Hours_CA__c = 20,
    		Total_Required_Triennium_Hours_ACA__c = 90,
    		Total_Required_Triennium_Hours_AT__c = 10,
    		Total_Required_Triennium_Hours_CA__c = 120,
    		Maximum_Triennium_Informal_Hours_AU_CA__c = 30
    	);
    	insert config;
    }

    private static List<CPD_Summary__c> getSummaries(){
    	return [
			SELECT Id, Contact_Student__c, Triennium_Start_Date__c, Triennium_End_Date__c,
			Specialisation_1__c, Specialisation_2__c, Specialisation_3__c, Specialisation_4__c,
			Specialisation_Hours_1__c, Specialisation_Hours_2__c, Specialisation_Hours_3__c, 
			Specialisation_Hours_4__c
			FROM CPD_Summary__c
		];
    }

    private static List<Education_Record__c> getEducationRecords(){
    	return [
			SELECT Id, Contact_Student__c, CPD_Processed__c, CPD_Summary__r.Contact_Student__c 
			FROM Education_Record__c
		];
    }

    private static Account getMemberAccount(){
    	Account acc = TestObjectCreator.createPersonAccount();
    	acc.Designation__c = 'ACA';
    	acc.Membership_Type__c = 'Member';
    	acc.Membership_Approval_Date__c = Date.today().addDays(-400);
    	return acc;
    }


    private static Education_Record__c newEducationRecord(Id contactId, Id CPDSummaryId, String specialHourCode, Decimal hours){
        Education_Record__c education = new Education_Record__c(
        	Contact_Student__c = contactId,
//            CPD_Summary__c = CPDSummaryId,
        	Specialist_hours_code__c = specialHourCode,
        	Qualifying_hours__c = hours,
        	Date_Commenced__c = System.today(),
        	Date_Completed__c = System.today(),
        	Event_Course_name__c = 'Course_1',
        	Enter_university_instituition__c = 'University ABC'
        );
        return education;
    }

    private static CPD_Exemption__c getExemptionRecord(Id accountId, Id CPDSummaryId){
        CPD_Exemption__c exemption = new CPD_Exemption__c(
        	Account_Name__c = accountId,
            CPD_Summary__c = CPDSummaryId,
            Start_Date__c = System.today(),
            End_Date__c = System.today(),
            Exemption_Hours__c = 1,
            Formal_verified_exemption_hours__c = 1
        );
        return exemption;
    }

    static testMethod void test_CPDBatchHandler() {
        List<Account> accounts = new List<Account>{ getMemberAccount() };
		insert accounts;

        list<Contact> Contacts = [Select Id from Contact where AccountId = :accounts[0].Id];

		List<CPD_Summary__c> CPDSUmmaries = [Select Id from CPD_Summary__c 
                                            	Where Current_Triennium__c = True];
        System.debug('This is the CPD SUmmary Record for Test- ' + CPDSUmmaries.size());
        System.debug('Specialist Code Zero is - ' + specialistHoursCodes[0]);
        
        if (!Contacts.isempty() && !CPDSUmmaries.isempty()){
	        List<Education_Record__c> educations = new List<Education_Record__c>();
            educations.add(newEducationRecord(Contacts[0].Id, CPDSUmmaries[0].Id, specialistHoursCodes[0], 10));
            insert educations;

    
    		List<CPD_Exemption__c> exemption = new List<CPD_Exemption__c>();
            exemption.add(getExemptionRecord(accounts[0].Id,CPDSUmmaries[0].Id));
            insert exemption;
        }

        Test.startTest();
        DateTime	REFERENCE_DATE = DateTime.newInstance(2018, 8, 17, 0, 0, 0);
        CPDBatchHandler.initiate(1, false, REFERENCE_DATE);
        Test.stopTest();
        
        List<CPD_Summary__c> summaries = getSummaries();
		system.assertEquals(4, summaries.Size());
        
    }
    
    static testMethod void test_CPDBatchEduHandler() {
        List<Account> accounts = new List<Account>{ getMemberAccount() };
		insert accounts;

        list<Contact> Contacts = [Select Id from Contact where AccountId = :accounts[0].Id];

		List<CPD_Summary__c> CPDSUmmaries = [Select Id from CPD_Summary__c 
                                            	Where Current_Triennium__c = True];
        System.debug('This is the CPD SUmmary Record for Test- ' + CPDSUmmaries.size());
        System.debug('Specialist Code Zero is - ' + specialistHoursCodes[0]);
        
        if (!Contacts.isempty() && !CPDSUmmaries.isempty()){
	        List<Education_Record__c> educations = new List<Education_Record__c>();
            educations.add(newEducationRecord(Contacts[0].Id, CPDSUmmaries[0].Id, specialistHoursCodes[0], 10));
            insert educations;

    
    		List<CPD_Exemption__c> exemption = new List<CPD_Exemption__c>();
            exemption.add(getExemptionRecord(accounts[0].Id,CPDSUmmaries[0].Id));
            insert exemption;
        }

        Test.startTest();
        DateTime	REFERENCE_DATE = DateTime.newInstance(2018, 8, 17, 0, 0, 0);
        CPDBatchEduHandler.initiate(1, false);
        Test.stopTest();
        
        List<Education_Record__c> educations = getEducationRecords();
        System.debug('Processed flag is - ' + educations[0].CPD_Processed__c);
		system.assertEquals(educations[0].CPD_Summary__r.Contact_Student__c, educations[0].Contact_Student__c);
		system.assertEquals(educations[0].CPD_Processed__c, false);
    }
}