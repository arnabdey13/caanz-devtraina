/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class LeadTrigger_Test {
    private static User IntegrationUserObjectInDB;
    static{
        insertIntegrationUserObject();
    }
    private static void insertIntegrationUserObject(){
        IntegrationUserObjectInDB = TestObjectCreator.createUser();
        //## Required Relationships
        IntegrationUserObjectInDB.Profileid = ProfileCache.getId('Integration Profile');
        //## Additional fields and relationships / Updated fields
        insert IntegrationUserObjectInDB;
    }
    
    private static Lead getLeadObject(){
        Lead LeadObject = TestObjectCreator.createLead();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        return LeadObject;
    }
    //******************************************************************************************
    //                             TestMethods
    //******************************************************************************************
    static testMethod void LeadTrigger_testInsertLeadAsIntegrationUser() {
        System.runAs(IntegrationUserObjectInDB){
            Lead LeadObject = getLeadObject();
            try{
                insert LeadObject;
                system.debug('@@@ Successfully inserted');
            }
            catch(Exception ex){
                //System.debug( '## ' + 
                //System.assert(false, '## ' + 
                //  ex.getMessage() + ' at ' + 
                //      ex.getStackTraceString()
                //);
            }
            if(LeadObject != null) {
         	   //Lead LeadResult = [Select l.ConvertedAccountId From Lead l where l.Id = :LeadObject.Id];
               List<Lead> LeadResultList = [Select l.ConvertedAccountId From Lead l where l.Id = :LeadObject.Id limit 1] ;
                
                if(LeadResultList != null && LeadResultList.size() > 0) {
                    Lead LeadResult = LeadResultList[0];
                }
        	}
            //System.assertNotEquals( null, LeadResult.ConvertedAccountId, 'ConvertedAccountId not null' );
        }
    }
}