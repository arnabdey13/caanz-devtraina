/**
    Developer: WDCi (Lean)
    Development Date: 18/05/2016
    Task #: Controller for luana_MyVirtualClassNew vf page LCA-499
    
    Change History:
    LCA-727 WDCi - KH 13/07/2016 to include update attendance start time & end time base on session start time & end time
    LCA-831 WDCi - KH 09/08/2016 Flush info from SPS's Subject outcome section -> SPS's Previous outsome when Supp Exam Attendance record's Status changed to 'Attended/Partial Attendance'
    07/09/2016 LCA-819 WDCi Lean: Add logic to grant CPD hours for workshop completion
    LCA-1174 WDCi - KH: Workshop reminder for PPP
**/

public without sharing class luana_AttendanceHandler {
    
    public static void initiateFlow(List<LuanaSMS__Attendance2__c> newList, Map<Id, LuanaSMS__Attendance2__c> newMap, List<LuanaSMS__Attendance2__c> oldList, Map<Id, LuanaSMS__Attendance2__c> oldMap, boolean isInsert, boolean isUpdate, boolean isDelete, boolean isUndelete, boolean isBefore, boolean isAfter){
        
        if(isBefore){
            if(isInsert){
                setDefaultValue(newList);
                pppReminderAttendance(newList);
            }
        } else if(isAfter){
            
            if(isInsert){
                /* LCA-602 - disable temporarily
                sendCalendarInvitation(newList);
                */
            } else if(isUpdate){
                System.debug('***after update::: ' + newList);
                flushSPSPreviousOutcome(newList, oldMap); //LCA-831
                grantStudentWorkshopCPDHours(newList, oldList); //LCA-819
            }
        }
    }
    
    public static void setDefaultValue(List<LuanaSMS__Attendance2__c> newList) {
    
        Set<Id> sessionIds = new Set<Id>();
        for(LuanaSMS__Attendance2__c attendance : newList) {
            sessionIds.add(attendance.LuanaSMS__Session__c);
        }
        
        Map<Id, LuanaSMS__Session__c> sessionsMap = new Map<Id, LuanaSMS__Session__c>();//LCA-727 to change the map type from Map<Id, String> to Map<Id, LuanaSMS_Session__c>
        for(LuanaSMS__Session__c session : [SELECT Id, RecordType.Name, LuanaSMS__End_Time__c, LuanaSMS__Start_Time__c FROM LuanaSMS__Session__c WHERE Id IN: sessionIds]) {
            sessionsMap.put(session.Id, session);
        }
        
        Map<String, Id> attendanceRTMap = new Map<String, Id>();
        
        for(RecordType rt : [SELECT Id, Name FROM RecordType WHERE SObjectType = 'LuanaSMS__Attendance2__c']) {
            attendanceRTMap.put(rt.Name, rt.Id);
        }
        
        for(LuanaSMS__Attendance2__c attendance : newList) {
            
            if(attendance.LuanaSMS__Session__c != null) {
                if(sessionsMap.containsKey(attendance.LuanaSMS__Session__c) && attendanceRTMap.containsKey(sessionsMap.get(attendance.LuanaSMS__Session__c).RecordType.Name)) {
                    attendance.RecordTypeId = attendanceRTMap.get(sessionsMap.get(attendance.LuanaSMS__Session__c).RecordType.Name);
                }
                
                /* WDCi Lean LCA-727 - disable this as this is done using existing workflow
                if(sessionsMap.containsKey(attendance.LuanaSMS__Session__c) && attendance.LuanaSMS__Start_time__c == null && attendance.LuanaSMS__End_time__c == null){
                    attendance.LuanaSMS__End_time__c = sessionsMap.get(attendance.LuanaSMS__Session__c).LuanaSMS__End_Time__c;
                    attendance.LuanaSMS__Start_time__c = sessionsMap.get(attendance.LuanaSMS__Session__c).LuanaSMS__Start_Time__c;
                }*/                
            }
            
            
        }
    
    }
    
    //LCA-1174
    public static void pppReminderAttendance(List<LuanaSMS__Attendance2__c> newList){
        List<LuanaSMS__Attendance2__c> pppReminderAttList = new List<LuanaSMS__Attendance2__c>();
        List<Id> spIdList = new List<Id>();
        Map<String, List<LuanaSMS__Attendance2__c>> currAttsMap = new Map<String, List<LuanaSMS__Attendance2__c>>();
        for(LuanaSMS__Attendance2__c att: newList){
            spIdList.add(att.LuanaSMS__Student_Program__c);
            
            if(currAttsMap.containsKey(att.LuanaSMS__Student_Program__c+'_'+att.Session_s_Topic_Key__c)){
                currAttsMap.get(att.LuanaSMS__Student_Program__c+'_'+att.Session_s_Topic_Key__c).add(att);
            }else{
                List<LuanaSMS__Attendance2__c> attList = new List<LuanaSMS__Attendance2__c>();
                attList.add(att);
                currAttsMap.put(att.LuanaSMS__Student_Program__c+'_'+att.Session_s_Topic_Key__c, attList);
            }
        }
        System.debug('*****spIdList::: ' + spIdList + ' - '+ currAttsMap);
        
        for(LuanaSMS__Attendance2__c att: [Select Id, Session_s_Topic_Key__c, LuanaSMS__Session__c, LuanaSMS__Student_Program__c, Skip_Reminder__c, LuanaSMS__Start_time__c, LuanaSMS__Session_Start_Time__c from LuanaSMS__Attendance2__c 
                                            Where LuanaSMS__Student_Program__c in: spIdList and LuanaSMS__Student_Program__r.RecordType.Name = 'PPP' and LuanaSMS__Session__r.RecordType.Name = 'Workshop']){
            pppReminderAttList.add(att);
        }
        for(LuanaSMS__Attendance2__c att: newList){
            pppReminderAttList.addAll(currAttsMap.get(att.LuanaSMS__Student_Program__c+'_'+att.Session_s_Topic_Key__c));
        }
        System.debug('*****pppReminderAttList::: ' + pppReminderAttList);
        
        Map<String, List<LuanaSMS__Attendance2__c>> sessAttendaceMap = new Map<String, List<LuanaSMS__Attendance2__c>>(); //This map is using the combination of SP_Session Token as the key to keep the attendance
        for(LuanaSMS__Attendance2__c att: pppReminderAttList){
            if(sessAttendaceMap.containsKey(att.LuanaSMS__Student_Program__c+'_'+att.Session_s_Topic_Key__c)){
                sessAttendaceMap.get(att.LuanaSMS__Student_Program__c+'_'+att.Session_s_Topic_Key__c).add(att);
            }else{
                List<LuanaSMS__Attendance2__c> newAtt = new List<LuanaSMS__Attendance2__c>();
                newAtt.add(att);
                sessAttendaceMap.put(att.LuanaSMS__Student_Program__c+'_'+att.Session_s_Topic_Key__c, newAtt);
            }
        }
        //After this the Attendance will be grouped together
        System.debug('*****sessAttendaceMap::: ' + sessAttendaceMap);
        
        Map<String, List<LuanaSMS__Attendance2__c>> sessAttendaceLatestMap = new Map<String, List<LuanaSMS__Attendance2__c>>();
        Map<String, LuanaSMS__Attendance2__c> firstAttMap = new Map<String, LuanaSMS__Attendance2__c>();
        for(LuanaSMS__Attendance2__c att: pppReminderAttList){
            if(sessAttendaceMap.containsKey(att.LuanaSMS__Student_Program__c+'_'+att.Session_s_Topic_Key__c)){
                List<LuanaSMS__Attendance2__c> attList = new List<LuanaSMS__Attendance2__c>();
                for(LuanaSMS__Attendance2__c groupedAtt: sessAttendaceMap.get(att.LuanaSMS__Student_Program__c+'_'+att.Session_s_Topic_Key__c)){
                    if(firstAttMap.containsKey(att.LuanaSMS__Student_Program__c+'_'+att.Session_s_Topic_Key__c)){
                        if(firstAttMap.get(att.LuanaSMS__Student_Program__c+'_'+att.Session_s_Topic_Key__c).LuanaSMS__Session_Start_Time__c > groupedAtt.LuanaSMS__Session_Start_Time__c){
                            firstAttMap.put(att.LuanaSMS__Student_Program__c+'_'+att.Session_s_Topic_Key__c, groupedAtt);
                        }
                    }else{
                        firstAttMap.put(att.LuanaSMS__Student_Program__c+'_'+att.Session_s_Topic_Key__c, groupedAtt);
                    }
                }
            }
        }
        //After this will get the first start date
        System.debug('*****firstAttMap::: ' + firstAttMap);
        
        List<LuanaSMS__Attendance2__c> skipReminderAttList = new List<LuanaSMS__Attendance2__c>();
        for(LuanaSMS__Attendance2__c att: pppReminderAttList){
            if(firstAttMap.containsKey(att.LuanaSMS__Student_Program__c+'_'+att.Session_s_Topic_Key__c)){
                if(att.LuanaSMS__Session_Start_Time__c != firstAttMap.get(att.LuanaSMS__Student_Program__c+'_'+att.Session_s_Topic_Key__c).LuanaSMS__Session_Start_Time__c && att.Skip_Reminder__c == false){
                    skipReminderAttList.add(att);
                }
            }
        }
        //After this the Attendances will be group for update the skip reminder field
        System.debug('*****skipReminderAttList::: ' + skipReminderAttList);
           
        List<LuanaSMS__Attendance2__c> sildingAtts = new List<LuanaSMS__Attendance2__c>();
        if(!skipReminderAttList.isEmpty()){
            for(LuanaSMS__Attendance2__c att: skipReminderAttList){
                if(att.Id != null){
                    att.Skip_Reminder__c = true;
                    sildingAtts.add(att);
                }else{
                    att.Skip_Reminder__c = true;
                }
            }        
        } 
        if(!sildingAtts.isEmpty()){
            update sildingAtts;  
        }
    }
    
    //LCA-831
    public static void flushSPSPreviousOutcome(List<LuanaSMS__Attendance2__c> newList, Map<Id, LuanaSMS__Attendance2__c> oldMap){
        
        Set<Id> relatedSPIds = new Set<Id>();
        //System.debug('****:newList:' +newList);
        for(LuanaSMS__Attendance2__c att: newList){
            if(oldMap.get(att.Id).LuanaSMS__Attendance_Status__c == null && (att.LuanaSMS__Attendance_Status__c == 'Attended' || att.LuanaSMS__Attendance_Status__c == 'Partial attendance') && att.Record_Type_Name__c == 'Supplementary_Exam'){
                relatedSPIds.add(att.LuanaSMS__Student_Program__c);
            }
        }
        
        List<LuanaSMS__Student_Program_Subject__c> updateSPSs = new List<LuanaSMS__Student_Program_Subject__c>();
        for(LuanaSMS__Student_Program_Subject__c sps: [Select Id, Name, Decile_Band__c, Merit__c, Result__c, Score__c, LuanaSMS__Outcome_National__c,
                                                        Past_Decile_Band__c, Past_Merit__c,Past_Outcome_National__c, Past_Result__c, Past_Score__c from LuanaSMS__Student_Program_Subject__c 
                                                        Where LuanaSMS__Student_Program__c in: relatedSPIds and Results_Flush__c = false]){
                                                        
            LuanaSMS__Student_Program_Subject__c tempSPS = new LuanaSMS__Student_Program_Subject__c();
            tempSPS.Id = sps.Id;
            tempSPS.Decile_Band__c = null;
            tempSPS.Merit__c = null;
            tempSPS.Result__c = null;
            tempSPS.Score__c = null;
            tempSPS.LuanaSMS__Outcome_National__c = null;
            
            tempSPS.Past_Decile_Band__c = sps.Decile_Band__c;
            tempSPS.Past_Merit__c = sps.Merit__c;
            tempSPS.Past_Outcome_National__c = sps.LuanaSMS__Outcome_National__c;
            tempSPS.Past_Result__c = sps.Result__c;
            tempSPS.Past_Score__c = sps.Score__c;
            
            tempSPS.Results_Flush__c = true;
            
            updateSPSs.add(tempSPS);
        }
        
        update updateSPSs;
    }
    
    /* LCA-602 - disable temporarily
    public static void sendCalendarInvitation(List<LuanaSMS__Attendance2__c> newList){
        
        List<Messaging.SingleEmailMessage> invitationEmails = new List<Messaging.SingleEmailMessage>();
        
        Set<Id> sessionIds = new Set<Id>();
        for(LuanaSMS__Attendance2__c newAttendance : newList){
            if(newAttendance.Record_Type_Name__c == luana_SessionConstants.RECORDTYPE_ATTENDANCE_VIRTUAL && !newAttendance.Contact_Is_Email_Bounced__c){
                sessionIds.add(newAttendance.LuanaSMS__Session__c);
            }
            
        }
        
        Map<Id, LuanaSMS__Session__c> sessionsDetails = new Map<Id, LuanaSMS__Session__c>([select Id, Session_Details__c from LuanaSMS__Session__c where id in: sessionIds] );
        
        for(LuanaSMS__Attendance2__c newAttendance : newList){
            if(newAttendance.Record_Type_Name__c == luana_SessionConstants.RECORDTYPE_ATTENDANCE_VIRTUAL && !newAttendance.Contact_Is_Email_Bounced__c){
                
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                efa.setFileName('invite.ics');
                efa.setContentType('text/calendar');
                efa.setBody(Blob.valueOf(
                    createIcalContent(
                        newAttendance.LuanaSMS__Session_Start_Time__c, 
                        newAttendance.LuanaSMS__Session_End_Time__c, 
                        newAttendance.Contact_Name__c, 
                        newAttendance.Contact_Email__c, 
                        newAttendance.Session_Name__c, 
                        (sessionsDetails.containsKey(newAttendance.LuanaSMS__Session__c) ? sessionsDetails.get(newAttendance.LuanaSMS__Session__c).Session_Details__c : ''), 
                        newAttendance.Adobe_Connect_Url__c, 
                        'Adobe Connect Meeting'
                    )));
                efa.setInline(true);
                
                if(Luana_Extension_Settings__c.getAll().containsKey('CalendarInvitation_OrgWideAddressId')){
                    email.setOrgWideEmailAddressId(Luana_Extension_Settings__c.getValues('CalendarInvitation_OrgWideAddressId').Value__c);
                }
                
                email.setSubject('Adobe Connect  - Meeting Invitation to ' + newAttendance.Session_Name__c);
                email.setToAddresses( new List<String>{newAttendance.Contact_Email__c} );
                email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
                email.setPlainTextBody( createEmailContent(newAttendance.LuanaSMS__Session_Start_Time__c, newAttendance.Session_Name__c, sessionsDetails.get(newAttendance.LuanaSMS__Session__c).Session_Details__c, newAttendance.Adobe_Connect_Url__c) );
                invitationEmails.add(email);
            }
        }
        
        Messaging.SendEmailResult [] results = Messaging.sendEmail(invitationEmails);
        
    }
    
    public static String createEmailContent(Datetime sessionStart, String sessionName, String sessionDetails, String sessionUrl){
        
        Map<String, Luana_Extension_Settings__c> luanaSettings = Luana_Extension_Settings__c.getAll();
        
        String sessionStartStr = sessionStart.format('yyyyMMdd\'T\'HHmmss\'Z\'', 'GMT+0');
        
        String companyUser = luanaSettings.containsKey('CalendarInvitation_Name') && luanaSettings.get('CalendarInvitation_Name').Value__c != null ? luanaSettings.get('CalendarInvitation_Name').Value__c : '';
        String companyMailTo = luanaSettings.containsKey('CalendarInvitation_MailTo') && luanaSettings.get('CalendarInvitation_MailTo').Value__c != null ? luanaSettings.get('CalendarInvitation_MailTo').Value__c : '';
        
        
        String emailBody = 'Please join me in an Adobe Connect Meeting.\n\n' + 
                            'Meeting Name: ' + sessionName + '\n' +
                            'Summary: ' + (sessionDetails == null ? '' : sessionDetails) + '\n' +
                            'Invited By: ' + companyUser + ' (' + companyMailTo + ')\n' +
                            'To join the meeting:\n' +
                            (sessionUrl == null ? '' : sessionUrl) + '\n\n\n' +
                            
                            '----------------\n' +
                            'If you have never attended an Adobe Connect meeting before:\n\n' +
                            'Test your connection: \n' +
                            (sessionUrl == null ? '' : sessionUrl) + '\n\n' +
                            'Get a quick overview: http://www.adobe.com/products/adobeconnect.html\n\n' +
                            'Adobe, the Adobe logo, Acrobat and Adobe Connect are either registered trademarks or trademarks of Adobe Systems Incorporated in the United States and/or other countries.';
                                    
        return emailBody;
    }
    
    public static String createIcalContent(Datetime sessionStart, Datetime sessionEnd, String receiverName, String receiverEmail, String sessionName, String sessionDetails, String sessionUrl, String location){
        
        String currentTimeStr = system.now().format('yyyyMMdd\'T\'HHmmss\'Z\'', 'GMT+0');
        String sessionStartStr = sessionStart == null ? '' : sessionStart.format('yyyyMMdd\'T\'HHmmss\'Z\'', 'GMT+0');
        String sessionEndStr = sessionEnd == null ? '' : sessionEnd.format('yyyyMMdd\'T\'HHmmss\'Z\'', 'GMT+0');
        
        Map<String, Luana_Extension_Settings__c> luanaSettings = Luana_Extension_Settings__c.getAll();
        
        String companyUser = luanaSettings.containsKey('CalendarInvitation_Name') && luanaSettings.get('CalendarInvitation_Name').Value__c != null ? luanaSettings.get('CalendarInvitation_Name').Value__c : '';
        String companyMailTo = luanaSettings.containsKey('CalendarInvitation_MailTo') && luanaSettings.get('CalendarInvitation_MailTo').Value__c != null ? luanaSettings.get('CalendarInvitation_MailTo').Value__c : '';
        
        String content = 'BEGIN:VCALENDAR\n' +
                        'PRODID:-//Luana SMS\n' +
                        'VERSION:1.0\n' +
                        'CALSCALE:GREGORIAN\n' +
                        'METHOD:REQUEST\n' +
                        'BEGIN:VEVENT\n' +
                        'DTSTART:' + sessionStartStr + '\n' +
                        'DTEND:' + sessionEndStr + '\n' +
                        'DTSTAMP:' + currentTimeStr + '\n' +
                        'ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;RSVP=TRUE;CN=' + companyUser + ';X-NUM-GUESTS=0:mailto:' + companyMailTo + '\n' +
                        'ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;RSVP=TRUE;CN=' + receiverName + ';X-NUM-GUESTS=0:mailto:' + receiverEmail + '\n' +
                        'CREATED:' + currentTimeStr + '\n' +
                        'DESCRIPTION:' + (sessionDetails == null ? '' : sessionDetails.replace('\r\n', '\\n')) + '\\n' + (sessionUrl == null ? '' : sessionUrl) + '\n' +
                        'LAST-MODIFIED:' + currentTimeStr + '\n' +
                        'LOCATION:' + location + '\n' +
                        'STATUS:CONFIRMED\n' +
                        'SUMMARY:' + sessionName + '\n' +
                        'TRANSP:OPAQUE\n' +
'UID:040000008200E00074C5B7101A82E00800000000609B98C7CFB5D101000000000000000\n' +
'   01000000049C97E573E83714EB7B47CC5175BD035\n' +
'X-ALT-DESC;FMTTYPE=text/html:<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//E\n' +
'   N">\\n<HTML>\\n<HEAD>\\n<META NAME="Generator" CONTENT="MS Exchange Server ve\n' +
'   rsion rmj.rmm.rup.rpr">\\n<TITLE></TITLE>\\n</HEAD>\\n<BODY>\\n<!-- Converted \n' +
'   from text/rtf format -->\\n\\n<P DIR=LTR><SPAN LANG="en-gb"></SPAN></P>\\n\\n<\n' +
'   /BODY>\\n</HTML>\n' +
'X-MICROSOFT-CDO-BUSYSTATUS:TENTATIVE\n' +
'X-MICROSOFT-CDO-IMPORTANCE:1\n' +
'X-MICROSOFT-CDO-INTENDEDSTATUS:BUSY\n' +
'X-MICROSOFT-DISALLOW-COUNTER:FALSE\n' +
'X-MS-OLK-AUTOSTARTCHECK:FALSE\n' +
'X-MS-OLK-CONFTYPE:0\n' +
                        'END:VEVENT\n' +
                        'END:VCALENDAR\n';
                        
        return content;
    }
    */
    
    //LCA-819 this is for PPP workshop cpd hours allocation
    public static void grantStudentWorkshopCPDHours(List<LuanaSMS__Attendance2__c> newList, List<LuanaSMS__Attendance2__c> oldList){
        
        Set<Id> studentProgramIds = new Set<Id>();
        Map<Id, LuanaSMS__Attendance2__c> oldMap = new Map<Id, LuanaSMS__Attendance2__c>(oldList);
        
        List<LuanaSMS__Student_Program__c> eligibleStudentPrograms = new List<LuanaSMS__Student_Program__c>();
        
        for(LuanaSMS__Attendance2__c attendance : newList){
            if(attendance.Record_Type_Name__c == luana_SessionConstants.RECORDTYPE_ATTENDANCE_WORKSHOP && (
                (!oldMap.containsKey(attendance.Id) && attendance.LuanaSMS__Attendance_Status__c == 'Attended' ) ||
                (oldMap.containsKey(attendance.Id) && attendance.LuanaSMS__Attendance_Status__c == 'Attended' && oldMap.get(attendance.Id).LuanaSMS__Attendance_Status__c != attendance.LuanaSMS__Attendance_Status__c) 
                )
            ){
                studentProgramIds.add(attendance.LuanaSMS__Student_Program__c);
            }
        }
        
        for(LuanaSMS__Student_Program__c studProg : [select Id, LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.Workshop_Eligibility_for_CPD_Hours__c, (select id from LuanaSMS__Attendances__r where RecordType.DeveloperName =: luana_SessionConstants.RECORDTYPE_ATTENDANCE_WORKSHOP and LuanaSMS__Attendance_Status__c = 'Attended') from LuanaSMS__Student_Program__c where id in: studentProgramIds and RecordType.DeveloperName = 'PPP' and Grant_Workshop_CPD_Hours__c = false]){
            if(studProg.LuanaSMS__Attendances__r.size() >= studProg.LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.Workshop_Eligibility_for_CPD_Hours__c){
                
                LuanaSMS__Student_Program__c studProgToUpdate = new LuanaSMS__Student_Program__c(Id = studProg.Id, Grant_Workshop_CPD_Hours__c = true);
                eligibleStudentPrograms.add(studProgToUpdate);
            }
        }
        
        if(!eligibleStudentPrograms.isEmpty()){
            update eligibleStudentPrograms;
        }
    }
}