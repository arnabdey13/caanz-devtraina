/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 */
public with sharing class CommunitiesLandingController {
    
    // Code we will invoke on page load.
    public PageReference forwardToStartPage() {

    return new PageReference(Site.getPrefix()+'/s/community-router');  //Route social sign-on login to community-router
    //return Network.communitiesLanding();                               Previous routing was to Home page                                
    }
    
    public CommunitiesLandingController() {}
 
}