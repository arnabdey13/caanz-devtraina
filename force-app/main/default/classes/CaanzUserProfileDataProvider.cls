/*------------------------------------------------------------------------------------
Author:        Sumit Gupta
Company:       Salesforce
Description:   User profile data provider

History
Date            Author             Comments
--------------------------------------------------------------------------------------
11-07-2018     Sumit Gupta          Initial Release
------------------------------------------------------------------------------------*/
public with sharing class CaanzUserProfileDataProvider {
	
	@AuraEnabled
    public static String getUserProfile(){
        return JSON.serialize(new UserProfile());
    }

    @AuraEnabled
    public static String getUserProfileNickname(){
        return getUserNickname();
    }

    @AuraEnabled
    public static void updateUserProfileNickname(String nickname){
        update new User(CommunityNickname = nickname, Id = UserInfo.getUserId());
    }

    public class UserProfile{
    	public String userId, username, photoUrl;

    	public UserProfile(){
    		userId = UserInfo.getUserId(); username = getUserNickname();
            if(!Test.isRunningTest()) photoUrl = ConnectApi.UserProfiles.getPhoto(Network.getNetworkId(), UserInfo.getUserId()).smallPhotoUrl;
    	}
    }

    private static String getUserNickname(){
        return [SELECT CommunityNickname FROM User Where Id =: UserInfo.getUserId() LIMIT 1].CommunityNickname;
    } 
}