/*
    Developer: WDCi (Vin)
    Date: 8/Oct/2018
    Task #: CAANZ - Flexipath   
*/

public with sharing class FP_GainedCompetencyAreaHandler{
    
    public static void calcCADependency(List<FP_Gained_Competency_Area__c> gcaList){
        Set<Id> parentCASet = new Set<Id>();
        Set<Id> studentSet = new Set<Id>();
        
        // Build unique CA and Student set
        for(FP_Gained_Competency_Area__c gca : gcaList) {
        parentCASet.add(gca.FP_Competency_Area__r.FP_Parent_Competency_Area__c);
        studentSet.add(gca.FP_Contact__c);
        }
        
        // Build a Map with ParentCAId, Set of ChildCAIDs
        List<FP_Competency_Area__c> childCAList = [SELECT Id, FP_Parent_Competency_Area__c FROM FP_Competency_Area__c WHERE FP_Parent_Competency_Area__c IN: parentCASet];
        // Parent CA Id, Child CA Id Set
        Map<Id, Set<Id>> parentChildCASet = new Map<Id, Set<Id>>();
        
        for(FP_Competency_Area__c ca : childCAList) {
            if(parentChildCASet.get(ca.FP_Parent_Competency_Area__c) != null) {
                parentChildCASet.get(ca.FP_Parent_Competency_Area__c).add(ca.Id);
            } else {
                Set<Id> newChildCASet = new Set<Id>();
                newChildCASet.add(ca.Id);
                parentChildCASet.put(ca.FP_Parent_Competency_Area__c, newChildCASet);
            }
        }
        
        // Build a Map with StudentId, Set of GCAs
        List<FP_Gained_Competency_Area__c> stuGcaList = [SELECT Id, FP_Competency_Area__c, FP_Contact__c FROM FP_Gained_Competency_Area__c WHERE FP_Contact__c IN: studentSet];
        //Student Contact Id, Competency Id Set
        Map<Id, Set<Id>> stuGcaSet = new Map<Id, Set<Id>>();
        
        for(FP_Gained_Competency_Area__c gca : stuGcaList) {
            if(stuGcaSet.get(gca.FP_Contact__c) != null) {
                stuGcaSet.get(gca.FP_Contact__c).add(gca.FP_Competency_Area__c);
            } else {
                Set<Id> newChildCASet = new Set<Id>();
                newChildCASet.add(gca.FP_Competency_Area__c);
                stuGcaSet.put(gca.FP_Contact__c, newChildCASet);
            }
        }
        
        // Compare both sets, if found, upsert GCA records
        List<FP_Gained_Competency_Area__c> newGCAUpsert = new List<FP_Gained_Competency_Area__c>();
        
        for(Id parentCAId : parentChildCASet.keySet()) {
            for(Id stuId : stuGcaSet.keySet()) {
                if(stuGcaSet.get(stuId).containsAll(parentChildCASet.get(parentCAId))) {
                    FP_Gained_Competency_Area__c gca = new FP_Gained_Competency_Area__c(FP_External_Id__c=stuId+'|'+parentCAId, FP_Contact__c=stuId, FP_Competency_Area__c=parentCAId);
                    newGCAUpsert.add(gca);
                }
            }    
        }
        
        upsert newGCAUpsert FP_External_Id__c;
    }
}