/*
    Developer: WDCi (KH)
    Date: 05/Aug/2016
    Task #: LCA-827 Luana implementation - Supplementary Broadcast for Eligible to Enrol & Supplementary Broadcast for Allocation Detail Async
*/


global with sharing class luana_SuppExamBroadcastAsync implements Database.Batchable<sObject>, Database.Stateful {
    
    public final static String TYPE_OPENING = 'Opening';
    public final static String TYPE_ALLOCATION = 'Allocation';
    
    String courseId;
    String errorRecords;
    String broadcastType;
    boolean hasError;
    
    global luana_SuppExamBroadcastAsync(String courseId, String broadcastType){
        this.courseId = courseId;
        this.errorRecords = '';
        this.broadcastType = broadcastType;
        this.hasError = false;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        
        if(broadcastType == TYPE_OPENING){

            return Database.getQueryLocator('select Id, LuanaSMS__Student_Program__c from LuanaSMS__Attendance2__c ' +
                                'Where LuanaSMS__Student_Program__c in '+ 
                                '(Select Id from LuanaSMS__Student_Program__c ' + 
                                    'where Supp_Exam_Eligible__c = false and LuanaSMS__Course__c = \'' + courseId + '\' ' + 
                                'and Supp_Exam_Special_Consideration_Approved__c != null)');
                                     
        }else if(broadcastType == TYPE_ALLOCATION){          
            return Database.getQueryLocator('select Id, LuanaSMS__Student_Program__c from LuanaSMS__Attendance2__c ' +
                                'Where LuanaSMS__Student_Program__c in '+ 
                                '(Select Id from LuanaSMS__Student_Program__c ' + 
                                    'where Supp_Exam_Eligible__c = true and LuanaSMS__Course__c = \'' + courseId + '\' ' +
                                'and Sup_Exam_Enrolled__c = true and Supp_Exam_Location__c != null) and recordType.DeveloperName = \'Supplementary_Exam\'');
                                        
        }    
        return null;
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> scope){
      List<LuanaSMS__Student_Program__c> studProgsToBroadcast = new List<LuanaSMS__Student_Program__c>();
      List<LuanaSMS__Attendance2__c> attendancesToBroadcast = new List<LuanaSMS__Attendance2__c>();
      Set<Id> stuProgSet = new Set<Id>();
      for(SObject sobj: scope){
          LuanaSMS__Attendance2__c att = (LuanaSMS__Attendance2__c)sobj;
          
          att.Supp_Exam_Broadcasted__c = true;
          stuProgSet.add(att.LuanaSMS__Student_Program__c);
          attendancesToBroadcast.add(att);
      }
      
      for(LuanaSMS__Student_Program__c sp: [Select Id, Supp_Exam_Eligible__c, Supp_Exam_Broadcasted__c from LuanaSMS__Student_Program__c Where Id in: stuProgSet]){

        if(broadcastType == TYPE_OPENING){
            sp.Supp_Exam_Eligible__c = true;
        }else if(broadcastType == TYPE_ALLOCATION){
            sp.Supp_Exam_Broadcasted__c = true;
        }
        studProgsToBroadcast.add(sp);
      }
      
      integer attCounter = 0;
      for(Database.SaveResult sr : Database.update(attendancesToBroadcast, false)){
          if(!sr.isSuccess()){
              errorRecords += attendancesToBroadcast.get(attCounter) + ' - ' + sr.getErrors()[0].getMessage() + '\n';
              hasError = true;
          }
          
          attCounter ++;
      }
            
      integer counter = 0;
      for(Database.SaveResult sr : Database.update(studProgsToBroadcast, false)){
        if(!sr.isSuccess()){
          errorRecords += studProgsToBroadcast.get(counter) + ' - ' + sr.getErrors()[0].getMessage() + '\n';
          hasError = true;
        }
        
        counter ++;
      }
      
    }
    
    global void finish(Database.BatchableContext bc){
      
      LuanaSMS__Luana_Log__c errorLog = new LuanaSMS__Luana_Log__c();
      errorLog.LuanaSMS__Notify_User__c = false;
      errorLog.LuanaSMS__Related_Course__c = courseId;
      errorLog.LuanaSMS__Parent_Id__c = courseId;
      
      if(hasError){
        errorLog.LuanaSMS__Status__c = 'Failed';
        errorLog.LuanaSMS__Level__c = 'Fatal';
        errorLog.LuanaSMS__Log_Message__c = 'Error broadcasting to the following record/s:\n\n' + errorRecords;
        
      } else {
        errorLog.LuanaSMS__Status__c = 'Completed';
        errorLog.LuanaSMS__Level__c = 'Info';
      }
      
      insert errorLog;
      
    }
}