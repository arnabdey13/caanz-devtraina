/*------------------------------------------------------------------------------------
Author:        Mayukhman
Company:       Tech Mahindra
Description:   Test class for most of the Controllers - CASUB_MainComponent_Controller

History
Date            Author             Comments
--------------------------------------------------------------------------------------
09-05-2019     Mayukhman         Initial Release
------------------------------------------------------------------------------------*/
@isTest 
public class CASUB_MainComponent_Controller_CA_Test {
    private static User currentUser;
    private static Account currentAccount;
    private static Subscription__c currentSubscription;
    static {
        currentAccount = CASUB_MainComponent_Controller_CA_Test.createFullMemberAccount();
        currentAccount.Affiliated_Branch_Country__c = 'Australia';
        insert currentAccount;
        currentSubscription = CASUB_MainComponent_Controller_CA_Test.createSubscriptionRecord();
    }
    
    private static User getCurrentUser(){
        Test.startTest(); Test.stopTest(); // Future method needs to run to create the user.
        return currentUser = [SELECT Name FROM User WHERE AccountId =: currentAccount.Id];
    }
    
    // <Create FullMemberAccount>
    public static Account createFullMemberAccount(){
        Account FullMemberAccountObject = new Account();
        FullMemberAccountObject.Salutation = 'Mr.';
        FullMemberAccountObject.FirstName = 'TestFName';
        FullMemberAccountObject.PersonEmail= 'Full_Member'+ CASUB_MainComponent_Controller_CA_Test.getRandomNumber(910) +'@gmail.com';
        FullMemberAccountObject.LastName = 'Test'+ CASUB_MainComponent_Controller_CA_Test.getRandomNumber(910);
        FullMemberAccountObject.RecordTypeId = RecordTypeCache.getId(FullMemberAccountObject, 'Full_Member');
        FullMemberAccountObject.Communication_Preference__c= 'Home Phone';
        FullMemberAccountObject.PersonHomePhone= '1234';
        FullMemberAccountObject.PersonOtherStreet= '83 Saggers Road';
        FullMemberAccountObject.PersonOtherCity='JITARNING';
        FullMemberAccountObject.PersonOtherState='Western Australia';
        FullMemberAccountObject.PersonOtherCountry='Australia';
        FullMemberAccountObject.PersonOtherPostalCode='6365';  
        FullMemberAccountObject.Financial_Category__c = 'Low income concession';
        return FullMemberAccountObject;
    }
    
    public static Subscription__c createSubscriptionRecord(){
        Subscription__c subscription = new Subscription__c();
        subscription.Account__c = currentAccount.Id;
        subscription.Contact_Details_Status__c = 'Pending';
        subscription.Obligation_Status__c = 'Pending';
        subscription.Sales_Order_Status__c = 'Pending';
        subscription.Year__c = '2019';
        insert subscription;
        return subscription;
    }
    public static Integer getRandomNumber(Integer size){
          Double d = math.random() * size;
          return d.intValue();
     }
    
    @isTest static void test_CASUB_MainComponent_Controller() {
        System.runAs(getCurrentUser()) {
        currentSubscription = CASUB_MainComponent_Controller.loadSubscriptionInformation();
        //CASUB_MainComponent_Controller.updateSubscriptionInformation(0,currentSubscription );
        }
    }
    @isTest static void test_CASUB_MainComponent_Controller_Step1() {
        System.runAs(getCurrentUser()) {
        currentSubscription = CASUB_MainComponent_Controller.loadSubscriptionInformation();
        CASUB_MainComponent_Controller.updateSubscriptionInformation(1,currentSubscription );
        }
    }
}