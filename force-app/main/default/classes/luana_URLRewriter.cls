global with sharing class luana_URLRewriter implements Site.UrlRewriter {
    
    String nooverrideParam = 'nooverride';
    
    global PageReference mapRequestUrl(PageReference myUrl){
        
        String url = myUrl.getUrl();
        
        if(url.contains(nooverrideParam)){
            
            return new PageReference('/');
        }
        
        return null;
    }
    
    global List<PageReference> generateUrlFor(List<PageReference> myUrls){
        
        return null;
    }
    
}