/*------------------------------------------------------------
Author:         Salman Zafar
Company:        Davanti Consulting
Description:    Inbound email handler class to create activities for email alerts
History
12/10/2015      Salman Zafar    Created
------------------------------------------------------------*/

global class LogEmailAlertActivity implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, 
                                                             Messaging.Inboundenvelope envelope) {           
        
        string strEmailPlainText = email.plainTextBody;
        string strhtmlBody = email.htmlBody ;
        string strSubject = email.subject;
        
        system.debug('strhtmlBody>>'+strhtmlBody);
        
        string strGetDataFromBody = strhtmlBody != null ? strhtmlBody : strEmailPlainText;
        
        string strRecordId = strSubject.substringBetween('R_SF_ID(',')');
        string strOwnerId = strSubject.substringBetween('R_OWNER_ID(',')');  
        string strContactId = strSubject.substringBetween('PC_ID(',')');  
                
        system.debug('strRecordId>>'+strRecordId);
        system.debug('strOwnerId>>'+strOwnerId);
        system.debug('strContactId>>'+strContactId);
        
        
        strSubject = strSubject.remove('('+strRecordId+')').remove('R_SF_ID').remove('('+strOwnerId+')').remove('R_OWNER_ID').remove('('+strContactId +')').remove('PC_ID');
                
        Task createTask = new Task(whatId = strRecordId,
                          Subject = strSubject,
                          OwnerId = strOwnerId,
                          WhoId = strContactId,
                          Status = 'Completed',
                          Priority = 'Normal',  
                          ActivityDate = Date.today().addDays(28), 
                          Description = strEmailPlainText);
        
        system.debug('createTask>>'+createTask);       
        insert createTask;
        return null;
    }
}