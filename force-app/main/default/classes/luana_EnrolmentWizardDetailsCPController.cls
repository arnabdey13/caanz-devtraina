/**
    Developer: WDCi (Lean)
    Development Date: 05/04/2016
    Task #: Enrollment wizard - Controller for Details CA Program
    
    LCA-875 25/08/2016 WDCi - KH: Change wording when t&c checkbox is uncheck
**/

public without sharing class luana_EnrolmentWizardDetailsCPController extends luana_EnrolmentWizardObject{
    
    luana_EnrolmentWizardController stdController;
    
    public luana_EnrolmentWizardDetailsCPController(luana_EnrolmentWizardController stdController){
        this.stdController = stdController;
        
    }
    
    public luana_EnrolmentWizardDetailsCPController(){
        
    }
    
    public String errMsg;
    public PageReference detailsCPNext(){
        
        try{
        
            if(stdController.workingStudentProgram.Accept_terms_and_conditions__c){
                /* standardize all the insert function to the main ctl class
                //PageReference pageRef = stdController.getDetailsNextPage();   
                
                //LCA-468  --Start
                LuanaSMS__Student_Program__c newSP = stdController.setStudProgramValue();
                insert newSP;
                
                List<LuanaSMS__Student_Program_Subject__c> newSPs = stdController.setStudProgramSubjectValue(newSP);
                insert newSPs;
                
                //Bug fix for LCA-471
                Attachment caProgAtt = stdController.getTermAndConAttachmentFile(true);
                Attachment newAtt = caProgAtt.clone(false, true);
                newAtt.ParentId = newSp.Id;
                insert newAtt;
                */
            } else {                
                errMsg = 'Please ensure that you are agreed with the Terms and Conditions by checking on the checkbox';//LCA-875 
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, errMsg));
                return null;
            }
            
            PageReference pageRef = stdController.getDetailsNextPage();             
            stdController.skipValidation = true;
            return pageRef;
            
            //stdController.skipValidation = true;
            //return page.luana_EnrolmentWizardComplete;
            
            //return null;
        
        }Catch(Exception exp){
            errMsg = 'Problem enroling course: ' + exp.getMessage() + '. Please try again later or contact our support if the problem persists.';
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, errMsg));
            return null;
        }
    }
    
    /*public PageReference doCheckError(){
        if(errMsg != null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, errMsg));
            return null;
        }else{
            stdController.skipValidation = true;
            return page.luana_EnrolmentWizardComplete;
        }
    }*/
    
    public PageReference detailsCPBack(){
        
        PageReference pageRef = Page.luana_EnrolmentWizardSubject;
        stdController.skipValidation = true;
        
        return pageRef;
    }
    
}