/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private with sharing class NewException_test {
	private static Integer int1 = 1;
	private static Integer int2 = 2;
	private static Integer int3 = 1;
	
	static testMethod void NewException_testAssertIsFalse() {
		String ErrorMessage = int1+' is not equal to '+int2;
		try{
			NewException.assert( int1==int2, ErrorMessage );
		}
		catch(Exception ex){
			System.assertEquals( ErrorMessage, ex.getMessage() );
			return;
		}
		System.assert(false, 'An error should have been throw!');
	}
	
	static testMethod void NewException_testAssertIsTrue() {
		String ErrorMessage = int1+' is not equal to '+int3;
		try{
			NewException.assert( int1==int3, ErrorMessage );
		}
		catch(Exception ex){
			System.assert(false, 'No error should have been throw!');
		}
	}
	
	static testMethod void NewException_testFailureWithBuildStackTraceAndMessage() {
		String ErrorMessage = int1+' is not equal to '+int2;
		try{
			NewException.assert( int1==int2, ErrorMessage );
		}
		catch(Exception ex){
			System.debug('## ' + NewException.buildStackTrace('ERROR: ', ex) );
			return;
			//Class.NewException_test.NewException_testBuildStackTrace
		}
		System.assert(false, 'An error should have been throw!');
	}
	
	static testMethod void NewException_testFailureWithBuildStackTrace() {
		String ErrorMessage = int1+' is not equal to '+int2;
		try{
			NewException.assert( int1==int2, ErrorMessage );
		}
		catch(Exception ex){
			System.debug('## ' + NewException.buildStackTrace(ex) );
			return;
			//Class.NewException_test.NewException_testBuildStackTrace
		}
		System.assert(false, 'An error should have been throw!');
	}
}