/**
    Developer: WDCi (Lean)
    Development Date: 05/04/2016
    Task #: Enrollment wizard - Controller for Program Offering selection
**/

public without sharing class luana_EnrolmentWizardLandingController extends luana_EnrolmentWizardObject {
    
    luana_EnrolmentWizardController stdController;
    
    public luana_EnrolmentWizardLandingController(luana_EnrolmentWizardController stdController){
        this.stdController = stdController;
        
    }
    
    public luana_EnrolmentWizardLandingController(){
    	
    }
    
    public PageReference landingNext(){
        return null;
    }
    
    public PageReference landingBack(){
        return null;
    }
    
}