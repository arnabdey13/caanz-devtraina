/*
 * @author          WDCi (LKoh)
 * @date            19-June-2019
 * @description     Apex support class for the wdciUniSubjectPathway Lightning Web Components
 * @changehistory
 */
public with sharing class wdci_UniSubjectPathwayController {
    
    @AuraEnabled(cacheable=true)
    public static List<FT_Pathway__c> grabRelatedPathway(Id uniSubjectId) {

        system.debug('Entering grabRelatedPathway: ' +uniSubjectId);
        
        List<FT_Pathway__c> relatedPathway = new List<FT_Pathway__c>();
        Set<Id> uniSubjectIdSet = new Set<Id>{uniSubjectId};
        Map<Id, List<FT_Pathway__c>> pathwayMap = wdci_UniversitySubjectHandler.mapPathway(uniSubjectIdSet);
        if (pathwayMap.containsKey(uniSubjectId)) {
            relatedPathway = pathwayMap.get(uniSubjectId);
        }
                
        system.debug('Exiting grabRelatedPathway: ' +relatedPathway);
        return relatedPathway;
    }
}