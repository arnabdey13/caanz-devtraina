/**
* @author Jannis Bott
* @date 25/01/2017
*
* @group FormsCommon
*
* @description  Used to generate richtext wrappers based on custom object data
*/
global with sharing class ff_WrappedRichtextSource extends ff_WrapperSource {
    private String richtextRecordKey;

    /**
    * @description Constructor that sets the record key of the record that contains the rich text data
    * @param String richtextRecordKey the key of the record that relates to the rich text record
    * @example
    * new ff_WrappedRichtextSource(ProvApp.stage1.reciprocalExplanation )
    */
    global ff_WrappedRichtextSource(String richtextRecordKey) {
        this.richtextRecordKey = richtextRecordKey;
    }

    /**
    * @description Queries the correct record and returns the correct rich text data
    */
    global override List<ff_Service.CustomWrapper> getWrappers() {
        List<ff_Service.CustomWrapper> result = new List<ff_Service.CustomWrapper>();

        List<ff_RichTextStore__c> content = [
                SELECT Display_Content__c
                FROM ff_RichTextStore__c
                WHERE Name__c = :richtextRecordKey
        ];

        if(content.isEmpty()){
            throw new SObjectException('No entry found with the provided rich text key: ' + richtextRecordKey);
        } else if(content.size() > 1){
            throw new SObjectException('More than one entry found with the provided rich text key: ' + richtextRecordKey);
        } else {
            ff_Service.RichTextWrapper wrap = new ff_Service.RichTextWrapper();
            wrap.richtext = content[0].Display_Content__c;
            result.add(wrap);
        }
        return result;
    }
}