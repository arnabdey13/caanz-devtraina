/* 
    Developer: WDCi (Lean)
    Date: 02/Aug/2016
    Task #: LCA-822 Luana implementation - Wizard for Non AU program enrolment
*/

public without sharing class luana_ProgramEnrolmentWizardPOController {
    
    luana_ProgramEnrolmentWizardController stdController;
        
    public luana_ProgramEnrolmentWizardPOController(luana_ProgramEnrolmentWizardController stdController){
        this.stdController = stdController;
        
        init();
    }
    
    public void init(){
    	
    	if(stdController.selectedProductType != null){
    		
    		stdController.programOfferings = getProgramOfferings(stdController.selectedProductType);
    		
    	}
    }
    
    public List<LuanaSMS__Program_Offering__c> getProgramOfferings(String productType){
        
        Set<Id> progOfferIds = new Set<Id>();

        List<LuanaSMS__Program_Offering__c> rawProgramOfferings;
		
        rawProgramOfferings = [select id, Name, Maximum_Number_of_Required_Electives__c, LuanaSMS__Number_of_Required_Electives__c, Product_Type__c, Subject_Selection_Option__c,
            (select id, name, LuanaSMS__Allow_Online_Enrolment__c, LuanaSMS__Start_Date__c, End_Date__c, LuanaSMS__Program_Offering__c, Enrolment_Start_Date__c, Enrolment_End_Date__c, Course_Detail__c from LuanaSMS__Courses__r where LuanaSMS__Allow_Online_Enrolment__c =: true and LuanaSMS__Status__c = 'Running' order by LuanaSMS__Start_Date__c nulls last, name), 
            (select id, name, LuanaSMS__Completion_Requirement__c, LuanaSMS__Essential__c, LuanaSMS__Subject__c, LuanaSMS__Subject__r.Name, LuanaSMS__Program_Offering__c from LuanaSMS__Program_Offering_Subjects__r order by name) 
            from LuanaSMS__Program_Offering__c where LuanaSMS__Allow_Online_Enrolment__c =: true and Product_Type__c =: productType order by Name];
		
        List<LuanaSMS__Program_Offering__c> permittedProgramOfferings = new List<LuanaSMS__Program_Offering__c>();
        
        for(LuanaSMS__Program_Offering__c progOffering : rawProgramOfferings){
                progOfferIds.add(progOffering.Id);
        }
        
        Map<Id, boolean> progOfferingPermission = stdController.commUserUtil.getProgramOfferingPermission(progOfferIds);

        for(LuanaSMS__Program_Offering__c progOffering : rawProgramOfferings){
			
            if(progOfferingPermission.containsKey(progOffering.Id) && progOfferingPermission.get(progOffering.Id)){
                permittedProgramOfferings.add(progOffering);
            }
        }
        
        System.debug('***permittedProgramOfferings: ' + permittedProgramOfferings);
        return permittedProgramOfferings;
    }
    
    public List<SelectOption> getProgramOfferingOptions(){
        
        List<SelectOption> options = new List<SelectOption>();
        if(stdController.programOfferings != null){
            for(LuanaSMS__Program_Offering__c programOffering : stdController.programOfferings){
                
                
                system.debug('programOffering.LuanaSMS__Courses__r: ' + programOffering.Id + ' - ' + programOffering.LuanaSMS__Courses__r);
                SelectOption option;
                if(programOffering.LuanaSMS__Courses__r.size() == 0){
                    option = new SelectOption(programOffering.Id, programOffering.Name + ' - No Available Courses', true);
                }else{
                    option = new SelectOption(programOffering.Id, programOffering.Name, false);
                    options.add(option); //LCA-148 - do not show program offering without course
                }
                
                //options.add(option); //LCA-148 - do not show program offering without course
            }
        }
        
        return options;
    }
    
    public PageReference programNext(){
    	
    	if(stdController.selectedProgramOfferingId != null && stdController.programOfferings != null){
            
            for(LuanaSMS__Program_Offering__c programOffering : stdController.programOfferings){
                if(programOffering.Id == stdController.selectedProgramOfferingId){
                    stdController.selectedProgramOffering = programOffering;
                    
                    stdController.maxElective = Integer.valueOf(stdController.selectedProgramOffering.Maximum_Number_of_Required_Electives__c);
                    
                    //reset the value
                    stdController.selectedCourseId = null;
                    stdController.selectedCourse = null;
                    
                    break;
                }
            }
                        
            PageReference pageRef = Page.luana_ProgramEnrolmentWizardCourse;
            
            return pageRef;
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please select a program offering below.'));
        }
        
        return null;
    }
    
    public PageReference programBack(){
    	
    	return null;
    }
}