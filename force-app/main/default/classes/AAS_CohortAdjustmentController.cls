/*
    Developer: WDCi (KH)
    Development Date: 07/11/2016
    Task #: AAS mark adjustment for exam and sub-exam by updating the cohort and passing exam scrore  
    
    Change:
        - 06 Feb 2017: AAS-24 add in check for Eligible for Deduction logic
        - 11 Apr 2017: AAS-24 KH Bug found in adding AAS Cohort Adjustment value at line 146
        - 11 Apr 2017: AAS-24 KH Bug found in Commiting with 3700 records in cohort adjustment get (batchable instance is too big) error message, implement 200 per batch and reduce the fields for processing
*/
global with sharing class AAS_CohortAdjustmentController {
    
    
    public Static String DEF_COHORT_KEYWORD = 'cohort';
    public Static String DEF_PASSING_MARK_KEYWORD = 'passing';
    
    public AAS_Course_Assessment__c courseAssessment {get; set;}
    public Static Decimal passingRate {get; set;}
    public static String selectedRecordType{get;set;}
    
    public boolean validToProcess {get; set;}
    
    public String markValue {get; set;}
    public String markTypeValue {get; set;}
    public String changeBy {get; set;}
    public Map<String, Double> adjustmentMap {get; set;}
    
    private Double examPassingMark {get; set;}
    private Double subexamPassingMark {get; set;}
    
    private Double examCohortMark {get; set;}
    private Double subexamCohortMark {get; set;}
    
    public List<ChartWedgeData> ChartWedgeDataList {get; set;}
    
    //public List<AAS_Student_Assessment_Section__c> searchSASResults {get; set;}
    
    List<AAS_Student_Assessment_Section__c> searchSASResults;
    
    ID batchprocessid;
    public boolean isUpdateDone {get;set;}
    public boolean isUpdateDoneWithError {get;set;}
    public boolean checkCommitStatus {get; set;}
    
    public String selectedType {get; set;}
    public Double currentCohortMark {get; set;}
    public Double currentPassingMark {get; set;}
    public List<AAS_Assessment_Schema_Section__c> relatedASSList {get; set;}
    
    public String defaultReportURL;
    public String reportURL {get; set;}
    public Map<String, String> sasRecordTypeIdMap = new Map<String, String>();
    
    public String exceedFullMarkReportId {get; set;}
    public boolean hasExceedFullMark {get; set;}
    public String sasRecordTypeId;
    
    public AAS_CohortAdjustmentController(ApexPages.StandardController controller) {
        
        if(!test.isRunningTest()){
            List<String> addFieldName = new List<String>{'Name','AAS_Course__c', 'AAS_Non_Exam_Data_Loaded__c', 'AAS_Exam_Data_Loaded__c', 'AAS_Course__r.Name', 'AAS_Module_Passing_Mark__c', 
                                                        'AAS_Cohort_Adjustment_Exam__c', 'AAS_Cohort_Adjustment_Supp_Exam__c', 'AAS_Cohort_Batch_Processing__c'};
            controller.addFields(addFieldName);
            courseAssessment = (AAS_Course_Assessment__c) controller.getRecord();
        }else{
            courseAssessment = (AAS_Course_Assessment__c) controller.getRecord();
        }
        
        //AAS-24
        if(!courseAssessment.AAS_Cohort_Batch_Processing__c){
        
            selectedType = apexpages.currentpage().getparameters().get('type');
            //Show the warning message if this cohort process is done before
            
            System.debug('***:: ' + selectedType);
            
            if(selectedType == 'Exam_Assessment'){
                if(courseAssessment.AAS_Cohort_Adjustment_Exam__c){
                    addPageMessage(ApexPages.severity.WARNING, 'Cohort adjustment was commited before, if you wish to continue old data will be lost.');
                }
            }else if(selectedType == 'Supp_Exam_Assessment'){
                if(courseAssessment.AAS_Cohort_Adjustment_Supp_Exam__c){
                    addPageMessage(ApexPages.severity.WARNING, 'Cohort adjustment was commited before, if you wish to continue old data will be lost.');
                }
            }
            validToProcess = true;
            hasExceedFullMark = false;
            adjustmentMap = new Map<String, Double>();
         
            passingRate = 0;
            
            //Initial set the passing rate donut chart value
            ChartWedgeDataList = new List<ChartWedgeData>();
            ChartWedgeDataList.add(new ChartWedgeData('Fail (0%)', 0));
            ChartWedgeDataList.add(new ChartWedgeData('Pass (0%)', 100));
            
            
            searchSASResults = new List<AAS_Student_Assessment_Section__c>();
            
            isUpdateDone= false;
            isUpdateDoneWithError = false;
            checkCommitStatus= false;
            
            selectedRecordType = selectedType + '_SAS';
            doInitQueryCheck();
            if(validToProcess){
                doQuery();
            }
            
            //Set the default value to display in the UI for cohort and passing value
            List<AAS_Settings__c> aasSets = AAS_Settings__c.getall().values();
            for(AAS_Settings__c assSetting: aasSets){
                if(assSetting.Name == 'Exceed Full Mark Report Id'){
                    exceedFullMarkReportId = assSetting.Value__c +'?pv0='+String.ValueOf(courseAssessment.Id).subString(0,15)+'&pv1=';
                }
            }
        
        }else{
            addPageMessage(ApexPages.severity.WARNING, 'There is already a Cohort batch processing at the moment. When the processing is completed, the user who has requested for the Cohort Adjustment will receive an email notification.');
        }
        
    }
    
    //This method is used when changed the type in UI all the used back the default value
    public void doInitQueryCheck(){
        
        adjustmentMap = new Map<String, Double>();
        
        if(courseAssessment.AAS_Non_Exam_Data_Loaded__c && courseAssessment.AAS_Exam_Data_Loaded__c){
        
            //Here is to set the default value from cohort and passing mark from custom setting, the value will be replace if the cohort is done before
            List<AAS_Settings__c> aasSets = AAS_Settings__c.getall().values();
            for(AAS_Settings__c assSetting: aasSets){
                if(assSetting.Name == 'Default Cohort Adjustment'){
                    adjustmentMap.put(DEF_COHORT_KEYWORD+'_Exam_Assessment_SAS', double.ValueOf(assSetting.Value__c));
                    adjustmentMap.put(DEF_COHORT_KEYWORD+'_Supp_Exam_Assessment_SAS', double.ValueOf(assSetting.Value__c));
                    currentCohortMark = double.ValueOf(assSetting.Value__c);
                }
               
                if(assSetting.Name == 'Default Passing Mark'){
                    adjustmentMap.put(DEF_PASSING_MARK_KEYWORD+'_Exam_Assessment_SAS', double.ValueOf(assSetting.Value__c));
                    adjustmentMap.put(DEF_PASSING_MARK_KEYWORD+'_Supp_Exam_Assessment_SAS', double.ValueOf(assSetting.Value__c));
                    currentPassingMark = double.ValueOf(assSetting.Value__c);
                }    
            }
            
            //Here is to re-set the default cohort and passing mark value from ASS if it has value
            relatedASSList = [Select Id, Name, RecordType.DeveloperName, AAS_Cohort_Adjustment__c, AAS_Passing_Mark__c 
                                                                    from AAS_Assessment_Schema_Section__c Where AAS_Course_Assessment__c =: courseAssessment.Id];
            
            for(AAS_Assessment_Schema_Section__c ass: relatedASSList){
    
                if(selectedRecordType == 'exam_assessment_sas'){
                    if(ass.RecordType.DeveloperName == 'Exam_Assessment'){
                        if(ass.AAS_Cohort_Adjustment__c != null){
                            adjustmentMap.put(DEF_COHORT_KEYWORD+'_'+ass.RecordType.DeveloperName+'_SAS', ass.AAS_Cohort_Adjustment__c);
                            currentCohortMark = ass.AAS_Cohort_Adjustment__c;
                        }
                    }
                    
                    
                    if(courseAssessment.AAS_Module_Passing_Mark__c != null){
                        adjustmentMap.put(DEF_PASSING_MARK_KEYWORD+'_'+ass.RecordType.DeveloperName+'_SAS', courseAssessment.AAS_Module_Passing_Mark__c);
                        currentPassingMark = courseAssessment.AAS_Module_Passing_Mark__c;
                    }

                    
                }else if(selectedRecordType == 'supp_exam_assessment_sas'){
                    
                    if(ass.RecordType.DeveloperName == 'Supp_Exam_Assessment'){
                        if(ass.AAS_Cohort_Adjustment__c != null){
                            adjustmentMap.put(DEF_COHORT_KEYWORD+'_'+ass.RecordType.DeveloperName+'_SAS', ass.AAS_Cohort_Adjustment__c);
                            currentCohortMark = ass.AAS_Cohort_Adjustment__c;
                        }
                    }
                    if(courseAssessment.AAS_Module_Passing_Mark__c != null){
                        adjustmentMap.put(DEF_PASSING_MARK_KEYWORD+'_'+ass.RecordType.DeveloperName+'_SAS', courseAssessment.AAS_Module_Passing_Mark__c);
                        currentPassingMark = courseAssessment.AAS_Module_Passing_Mark__c;
                    }
                }
            }
        
        }else{
            validToProcess = false;
            String errorMessage = '';
            if(!courseAssessment.AAS_Non_Exam_Data_Loaded__c && !courseAssessment.AAS_Exam_Data_Loaded__c){
                errorMessage = 'Non Exam and Exam data load';
            }else if(courseAssessment.AAS_Non_Exam_Data_Loaded__c && !courseAssessment.AAS_Exam_Data_Loaded__c){
                errorMessage = 'Exam data load';
            }else if(!courseAssessment.AAS_Non_Exam_Data_Loaded__c && courseAssessment.AAS_Exam_Data_Loaded__c){
                errorMessage = 'Non Exam data load';
            }
            addPageMessage(ApexPages.severity.ERROR, 'You are not allow to continue in this step. Please complete the ' + errorMessage + ' first.');
        }
        
    }
    
    public void doQuery(){
        try{
            
            ChartWedgeDataList = new List<ChartWedgeData>();
            
            System.debug('*******adjustmentMap 1: ' + currentCohortMark + ' : ' + adjustmentMap.get(DEF_PASSING_MARK_KEYWORD+'_'+selectedRecordType));
            
            //Set the default value for adjustment map for cohort/passing mark and Exm/Supp Exam record type
            if(changeBy != 'typeChange'){
                for(String strVal: adjustmentMap.keySet()){
                    if(markTypeValue == 'passing'){
                        adjustmentMap.put(DEF_PASSING_MARK_KEYWORD+'_'+selectedRecordType, courseAssessment.AAS_Module_Passing_Mark__c);
                        currentPassingMark = courseAssessment.AAS_Module_Passing_Mark__c;
                        
                    }else if(markTypeValue == 'cohort'){
                        adjustmentMap.put(DEF_COHORT_KEYWORD+'_'+selectedRecordType, double.valueOf(markValue));
                    }
                }
            }
            
            //Do calculation for add the cohort adjusted value to total mark and calculate the passing rate
            searchSASResults = new List<AAS_Student_Assessment_Section__c>();
            //AAS_Total_Raw_Mark__c,
            searchSASResults = [Select Id, recordTypeId, AAS_Assessment_Schema_Section__r.AAS_Passing_Mark__c, recordType.developerName, AAS_Exceed_Full_Mark_Count__c, AAS_Student_Assessment__r.Total_Non_Exam_Mark__c, AAS_Student_Assessment__r.AAS_Total_Exam_Mark__c from AAS_Student_Assessment_Section__c 
                                                    Where AAS_Student_Assessment__r.AAS_Course_Assessment__c =: courseAssessment.Id and RecordType.DeveloperName =: selectedRecordType];
            
            
            Map<Id, Decimal> totalModuleMarkMap = new Map<id, Decimal>();
            Map<Id, Boolean> examPassedMap = new Map<id, Boolean>();
            if(!searchSASResults.isEmpty()){
                for(AAS_Student_Assessment_Section__c sas: searchSASResults){
                    
                    //System.debug('*******adjustmentMap: ' + currentCohortMark + ' : ' + adjustmentMap.get(DEF_COHORT_KEYWORD+'_'+selectedRecordType));
                    
                    
                    
                    Decimal totalAdjValue = 0;
                    if(currentCohortMark > 0){
                        totalAdjValue = (currentCohortMark - adjustmentMap.get(DEF_COHORT_KEYWORD+'_'+selectedRecordType)) * -1;
                    }else if(currentCohortMark < 0){
                        if(adjustmentMap.get(DEF_COHORT_KEYWORD+'_'+selectedRecordType) < 0){
                            System.debug('*******adjustmentMap2: ' + currentCohortMark + ' : ' + (adjustmentMap.get(DEF_COHORT_KEYWORD+'_'+selectedRecordType) * -1));
                            totalAdjValue = (currentCohortMark + (adjustmentMap.get(DEF_COHORT_KEYWORD+'_'+selectedRecordType) * -1)) * -1;
                        }else{
                            System.debug('*******adjustmentMap3: ' + currentCohortMark + ' : ' + (adjustmentMap.get(DEF_COHORT_KEYWORD+'_'+selectedRecordType) * -1));
                            totalAdjValue = ((currentCohortMark * -1) + adjustmentMap.get(DEF_COHORT_KEYWORD+'_'+selectedRecordType));
                        }
                    }else{
                        if(adjustmentMap.get(DEF_COHORT_KEYWORD+'_'+selectedRecordType) > 0){
                            totalAdjValue = adjustmentMap.get(DEF_COHORT_KEYWORD+'_'+selectedRecordType);
                        }else if(adjustmentMap.get(DEF_COHORT_KEYWORD+'_'+selectedRecordType) < 0){
                            totalAdjValue = adjustmentMap.get(DEF_COHORT_KEYWORD+'_'+selectedRecordType);
                        }else{
                            totalAdjValue = currentCohortMark;
                        }
                    }
                    
                    
                    //System.debug('*******totalAdjValue: ' + totalAdjValue);
                    
                    Decimal totalAdjustedMark = sas.AAS_Student_Assessment__r.AAS_Total_Exam_Mark__c + (totalAdjValue);
                    // + sas.AAS_Student_Assessment__r.AAS_Total_Exam_Mark__c
                    //System.debug('***run here: ' + totalAdjustedMark);
                    //+ adjustmentMap.get(DEF_COHORT_KEYWORD+'_'+selectedRecordType);
                    
                    System.debug('*******All: ' + sas.AAS_Assessment_Schema_Section__r.AAS_Passing_Mark__c + ' : ' + sas.AAS_Student_Assessment__r.AAS_Total_Exam_Mark__c + ' - ' + sas.AAS_Student_Assessment__r.Total_Non_Exam_Mark__c + ' - '+ totalAdjValue + ' : ' + totalAdjustedMark);
                    
                    if(sas.AAS_Assessment_Schema_Section__r.AAS_Passing_Mark__c != null){   //for technical check
                        if(totalAdjustedMark >= sas.AAS_Assessment_Schema_Section__r.AAS_Passing_Mark__c){
                            examPassedMap.put(sas.Id, true);
                        }else{
                            examPassedMap.put(sas.Id, false);
                        }
                    }else{
                        examPassedMap.put(sas.Id, true);  // for capstone check
                    }
                    System.debug('***examPassedMap: ' + examPassedMap);
                    
                    Decimal totalModuleAdjustmentMark = (sas.AAS_Student_Assessment__r.Total_Non_Exam_Mark__c + sas.AAS_Student_Assessment__r.AAS_Total_Exam_Mark__c) + (totalAdjValue);
                    totalModuleMarkMap.put(sas.Id, totalModuleAdjustmentMark);
                    
                    sasRecordTypeIdMap.put(sas.RecordType.developerName, String.valueOf(sas.RecordTypeId).subString(0,15));
                }
                System.debug('***totalModuleMarkMap: ' + totalModuleMarkMap + ' - ' + adjustmentMap.get(DEF_PASSING_MARK_KEYWORD+'_'+selectedRecordType));
                
                Decimal numOfPassed = 0;
                if(adjustmentMap.containsKey(DEF_PASSING_MARK_KEYWORD+'_'+selectedRecordType)){
                    for(Id sasId: totalModuleMarkMap.keySet()){
                        if(examPassedMap.get(sasId)){ // Technical check for exam passing
                            if(totalModuleMarkMap.get(sasId) >= adjustmentMap.get(DEF_PASSING_MARK_KEYWORD+'_'+selectedRecordType)){
                                numOfPassed++;
                            }
                        }
                    }
                    
                    //Set the passing rate to the chart
                    if(!totalModuleMarkMap.values().isEmpty()){
                        passingRate = (numOfPassed/totalModuleMarkMap.values().size()) * 100;
                        Decimal failRate = 100 - passingRate;
                        
                        ChartWedgeDataList.add(new ChartWedgeData('Fail (' +failRate.setScale(2)+'%)', Integer.valueOf(failRate)));
                        ChartWedgeDataList.add(new ChartWedgeData('Pass (' +passingRate.setScale(2)+'%)', Integer.valueOf(passingRate)));
                        
                    }
                }
                
                System.debug('***ChartWedgeDataList: ' + ChartWedgeDataList); 
                
            }else{
                
                ChartWedgeDataList.add(new ChartWedgeData('Fail (0%)', 0));
                ChartWedgeDataList.add(new ChartWedgeData('Pass (0%)', 100));
                addPageMessage(ApexPages.severity.WARNING, 'No record found!');
            }
            
        }catch(Exception exp){
            addPageMessage(ApexPages.severity.ERROR, 'Error calculating adjustment passing rate. Please try again later or contact your system administrator. Error: ' + exp.getMessage() + ' - ' + exp.getStackTraceString());
        }      
    }
    
    public List<SelectOption> getRecordTypeOptions() {
        
        Schema.DescribeFieldResult statusFieldDescription = RecordType.DeveloperName.getDescribe();
        List<SelectOption> statusOptions = new list<SelectOption>();
        
        //May need to change the query filter to handle supp exam in future
        for(AAS_Assessment_Schema_Section__c ass: [Select Id, RecordType.Name, RecordType.developerName from AAS_Assessment_Schema_Section__c 
                                                    Where AAS_Course_Assessment__c =: courseAssessment.Id and RecordType.DeveloperName = 'Exam_Assessment']){
            statusOptions.add(new SelectOption(ass.RecordType.developerName+'_SAS', ass.RecordType.Name));
        }
            
        return statusOptions;
    }
    
    
    public class StudentAssessmentSectionItemWrapper {
        public AAS_Student_Assessment_Section_Item__c stuAssSecItem {get; set;}
        public Integer adjustMark {get; set;}
        
        public StudentAssessmentSectionItemWrapper(AAS_Student_Assessment_Section_Item__c soItem, Integer am){
            stuAssSecItem = soItem;
            adjustMark = am;
        }
    }
    
    public void addPageMessage(ApexPages.severity sev, String msg){
        ApexPages.Message warningmsg = new ApexPages.Message(sev, msg);
        ApexPages.addmessage(warningmsg);
    }
    
    //Will update the SASI adjustment field, update the CA checkbox and ASS cohort adjustment value
    public PageReference doCommit(){
        
        Savepoint sp = Database.setSavepoint();
        try{
            
            //initial set the CA's Cohort Adjustment & Cohort Batch Processing Flag
            AAS_Course_Assessment__c initialUpdateCA = new AAS_Course_Assessment__c();
            initialUpdateCA.Id = courseAssessment.Id;
            initialUpdateCA = updateCohortBatchProcessingFlag(initialUpdateCA, true);
            initialUpdateCA = updateCohortAdjustmentFlag(initialUpdateCA, false);
            update initialUpdateCA;
            
            //Perform CA proc
            doProcessing();
            
            return toLandingPage();
            
        }Catch(Exception exp){
            Database.rollback(sp);
            addPageMessage(ApexPages.severity.ERROR, 'Error processing. Please try again later or contact your system administrator. Error: ' + exp.getMessage() + ' - ' + exp.getStackTraceString());

        }
        
        return null;
        
    }
    
    
    //Check batch update respone result

    public void checkBatchCompletion(){
        
        List<AsyncApexJob> aajs = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =: batchprocessid and Status = 'Completed'];

        if(!aajs.isEmpty()){
            
            for(AsyncApexJob aaj: aajs){
                
                if(aaj.NumberOfErrors > 0){
                    isUpdateDone = false;
                    isUpdateDoneWithError = true;
                    
                    checkCommitStatus = false;
                    
                }else{
                    isUpdateDone = true;
                    isUpdateDoneWithError = false;
                }
            }
            
            checkCommitStatus = false;
        }
    }
        
    public PageReference doCancel(){
        PageReference returnPage = new PageReference('/'+courseAssessment.Id);
        return returnPage;
    }
    
    public PageReference doRedirect(){
    
        if(isUpdateDone){
            PageReference returnPage = new PageReference('/'+courseAssessment.Id);
            return returnPage;
        }else{
            if(!checkCommitStatus){
                addPageMessage(ApexPages.severity.ERROR, 'Error found during commit process. Please try again later or contact your system administrator.');
            }
            return null;
        }
        
    }
    
    public List<ChartWedgeData> getChartData() {
        return ChartWedgeDataList;
    }
    
    // Wrapper class
    public class ChartWedgeData {

        public String name { get; set; }
        public Integer data { get; set; }

        public ChartWedgeData(String name, Integer data) {
            this.name = name;
            this.data = data;
        }
    }
    
    //-----------------------new change Apr 2017 for batch function-----------------------------//
    public AAS_Course_Assessment__c updateCohortBatchProcessingFlag(AAS_Course_Assessment__c ca, Boolean flagStatus){
        ca.AAS_Cohort_Batch_Processing__c = flagStatus;
        return ca;
    }
    
    public AAS_Course_Assessment__c updateCohortAdjustmentFlag(AAS_Course_Assessment__c ca, Boolean flagStatus){
        ca.AAS_Cohort_Adjustment_Exam__c = flagStatus;
        return ca;
    }
    
    public AAS_Assessment_Schema_Section__c updateASSCohortAdjustment(AAS_Assessment_Schema_Section__c ass, Decimal cohortAdjValue){
        ass.AAS_Cohort_Adjustment__c = cohortAdjValue;
        return ass;
    }
    
    public PageReference toLandingPage(){
        PageReference landingPage = Page.AAS_CohortAdjustmentLanding;
        landingPage.getParameters().put('id', courseAssessment.Id);
        landingPage.getParameters().put('type', selectedType);
        landingPage.setredirect(true);
        return landingPage;
    }
    
    public void doProcessing(){
            
        reportURL = defaultReportURL;
        
        for(AAS_Student_Assessment_Section__c sasi: searchSASResults){
            if(sasi.AAS_Exceed_Full_Mark_Count__c > 0){
                hasExceedFullMark = true;
            }
            sasRecordTypeId = sasi.RecordTypeId;
        }
        
        if(!hasExceedFullMark){
            if(selectedType + '_SAS' == selectedRecordType){
                List<Id> sasIdList = new List<Id>();
                for(AAS_Student_Assessment_Section__c sas: searchSASResults){
                    sasIdList.add(sas.Id);
                }
                
                //AAS_Student_Assessment_Section__c, 
                //List<AAS_Student_Assessment_Section_Item__c> searchSASIResults = [Select Id, AAS_Adjustment__c, AAS_Eligible_for_Addition__c, AAS_Eligible_for_Deduction__c, AAS_Student_Assessment_Section__r.AAS_Eligible_for_Deduction_Count__c, AAS_Student_Assessment_Section__r.AAS_Eligible_for_Addition_Count__c from AAS_Student_Assessment_Section_Item__c 
                //                                                                    Where AAS_Student_Assessment_Section__c in: sasIdList];
                
                system.debug(': Heap size is 1: ' + limits.getHeapSize() + ' enforced is ' + limits.getLimitHeapSize());
                
                AAS_Assessment_Schema_Section__c examAss = new AAS_Assessment_Schema_Section__c();
                for(AAS_Assessment_Schema_Section__c ass: relatedASSList){
                    if(ass.RecordType.DeveloperName == 'Exam_Assessment'){
                        examAss.Id = ass.Id;
                        examAss.AAS_Cohort_Adjustment__c = adjustmentMap.get(DEF_COHORT_KEYWORD+'_'+selectedRecordType);
                        
                    }
                }
                    
                update examAss;
                
                List<AAS_Student_Assessment_Section_Item__c> searchSASIResultsFlush = new List<AAS_Student_Assessment_Section_Item__c>();
                
                system.debug(': Heap size is 1: ' + limits.getHeapSize() + ' enforced is ' + limits.getLimitHeapSize());
                
                List<AAS_Student_Assessment_Section_Item__c> sasiList = new List<AAS_Student_Assessment_Section_Item__c>();
                
                for(AAS_Student_Assessment_Section_Item__c sasi: [Select Id, AAS_Adjustment__c, AAS_Eligible_for_Addition__c, AAS_Eligible_for_Deduction__c, AAS_Student_Assessment_Section__r.AAS_Eligible_for_Deduction_Count__c, AAS_Student_Assessment_Section__r.AAS_Eligible_for_Addition_Count__c from AAS_Student_Assessment_Section_Item__c 
                                                                                    Where AAS_Student_Assessment_Section__c in: sasIdList]){
                    
                    if(sasi.AAS_Adjustment__c != null){
                        searchSASIResultsFlush.add(sasi);
                    }
                    
                    if(adjustmentMap.get(DEF_COHORT_KEYWORD+'_'+selectedRecordType) >= 0){ // AAS-24
                        if(sasi.AAS_Eligible_for_Addition__c){
                            if(Integer.ValueOf(sasi.AAS_Student_Assessment_Section__r.AAS_Eligible_for_Addition_Count__c) > 0){
                                sasi.AAS_Adjustment__c = adjustmentMap.get(DEF_COHORT_KEYWORD+'_'+selectedRecordType) / Integer.ValueOf(sasi.AAS_Student_Assessment_Section__r.AAS_Eligible_for_Addition_Count__c); 
                                sasiList.add(sasi);
                            }else{
                                sasi.AAS_Adjustment__c = 0;
                                sasiList.add(sasi);
                            }
                        }else{      
                            sasi.AAS_Adjustment__c = null;
                            sasiList.add(sasi);
                        }
                    }else{ //AAS-24
                        if(sasi.AAS_Eligible_for_Deduction__c){
                            if(Integer.ValueOf(sasi.AAS_Student_Assessment_Section__r.AAS_Eligible_for_Deduction_Count__c) > 0){
                                sasi.AAS_Adjustment__c = adjustmentMap.get(DEF_COHORT_KEYWORD+'_'+selectedRecordType) / Integer.ValueOf(sasi.AAS_Student_Assessment_Section__r.AAS_Eligible_for_Deduction_Count__c); 
        
                                sasiList.add(sasi);
                            }else{
                                sasi.AAS_Adjustment__c = 0;
                                sasiList.add(sasi);
                            }
                        }else{            
                            sasi.AAS_Adjustment__c = null;
                            sasiList.add(sasi);
                        }
                    }
                }
                
                system.debug(': Heap size is 2: ' + limits.getHeapSize() + ' enforced is ' + limits.getLimitHeapSize());
                
           
                //List<AAS_Student_Assessment_Section_Item__c> sasiList = doCohortAdjustmentCalculationForQuestion(searchSASIResults);

                checkCommitStatus = true;
                reportURL = reportURL + sasRecordTypeIdMap.get(selectedRecordType);
                
                //Call the batchable class to update the record in batch
                AAS_CohortFlushRecordBatch updateSASIBatch = New AAS_CohortFlushRecordBatch(searchSASIResultsFlush, sasiList, true, courseAssessment, examAss);
                batchprocessid= database.executeBatch(updateSASIBatch, 200);
                
                
            }
        }
    }
    
    
    /*public List<AAS_Student_Assessment_Section_Item__c> doCohortAdjustmentCalculationForQuestion(List<AAS_Student_Assessment_Section_Item__c> searchSASIResults){
        List<AAS_Student_Assessment_Section_Item__c> sasiList = new List<AAS_Student_Assessment_Section_Item__c>();
        for(AAS_Student_Assessment_Section_Item__c sasi: searchSASIResults){
            if(adjustmentMap.get(DEF_COHORT_KEYWORD+'_'+selectedRecordType) >= 0){ // AAS-24
                if(sasi.AAS_Eligible_for_Addition__c){
                    if(Integer.ValueOf(sasi.AAS_Student_Assessment_Section__r.AAS_Eligible_for_Addition_Count__c) > 0){
                        sasi.AAS_Adjustment__c = adjustmentMap.get(DEF_COHORT_KEYWORD+'_'+selectedRecordType) / Integer.ValueOf(sasi.AAS_Student_Assessment_Section__r.AAS_Eligible_for_Addition_Count__c); 
                        sasiList.add(sasi);
                    }else{
                        sasi.AAS_Adjustment__c = 0;
                        sasiList.add(sasi);
                    }
                }else{      
                    sasi.AAS_Adjustment__c = null;
                    sasiList.add(sasi);
                }
            }else{ //AAS-24
                if(sasi.AAS_Eligible_for_Deduction__c){
                    if(Integer.ValueOf(sasi.AAS_Student_Assessment_Section__r.AAS_Eligible_for_Deduction_Count__c) > 0){
                        sasi.AAS_Adjustment__c = adjustmentMap.get(DEF_COHORT_KEYWORD+'_'+selectedRecordType) / Integer.ValueOf(sasi.AAS_Student_Assessment_Section__r.AAS_Eligible_for_Deduction_Count__c); 

                        sasiList.add(sasi);
                    }else{
                        sasi.AAS_Adjustment__c = 0;
                        sasiList.add(sasi);
                    }
                }else{            
                    sasi.AAS_Adjustment__c = null;
                    sasiList.add(sasi);
                }
            }

        }
        return sasiList;
    }*/
    
}