public with sharing class ff_WrappedAttachmentSource extends ff_WrapperSource {

    private Id parentId;

    public ff_WrappedAttachmentSource(Id parentId) {
        this.parentId = parentId;
    }

    public override List<ff_Service.CustomWrapper> getWrappers() {
        List<ff_Service.AttachmentWrapper> wrappers = new List<ff_Service.AttachmentWrapper>();

        // use this map to de-dupe the files by prefix, returning the most recently uploaded file for each prefix
        Map<String, Map<String, Attachment>> mostRecent = new Map<String, Map<String, Attachment>>();

        // use bulk query for best performance
        for (List<Attachment> attachments : [
                SELECT Name, LastModifiedDate
                FROM Attachment
                WHERE ParentId = :this.parentId
                ORDER BY LastModifiedDate DESC
        ]) {
            for (Attachment attachment : attachments) {
                List<String> parsedName = attachment.Name.split(':');
                String filePrefix = parsedName[0];
                String fileName = parsedName[1];
                if (!mostRecent.containsKey(filePrefix)) {
                    mostRecent.put(filePrefix, new Map<String, Attachment>());
                }
                Map<String, Attachment> uniqueFileNames = mostRecent.get(filePrefix);
                if (!uniqueFileNames.containsKey(fileName)) {
                    uniqueFileNames.put(fileName, attachment);
                }
            }
        }

        System.debug('de-duped attachments for parent: ' + this.parentId + ' = ' + mostRecent);

        for (String filePrefix : mostRecent.keySet()) {
            wrappers.add(new ff_Service.AttachmentWrapper(
                    new List<Attachment>(mostRecent.get(filePrefix).values()),
                    parentId));
        }

        // always include a wrapper with no files so that empty upload controls know which parent to use
        wrappers.add(new ff_Service.AttachmentWrapper(new List<Attachment>(), parentId));

        return wrappers;
    }

}