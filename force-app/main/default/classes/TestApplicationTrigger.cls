/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestApplicationTrigger {
	private static Account FullMemberAccountObjectInDB;
	private static Application__c ApplicationObjectInDB;
	private static Fellow_Nomination__c FellowNominationObjectInDB;
	
	private static TriggerConfig__c TriggerConfigObject_1InDB;
	private static TriggerConfig__c TriggerConfigObject_2InDB;
	private static TriggerConfig__c TriggerConfigObject_3InDB;
	
	static{
		insertFullMemberAccountObject();
		insertApplicationObject();
		insertFellowNominationObject();
		
		insertTriggerConfigObject_1();
		insertTriggerConfigObject_2();
		insertTriggerConfigObject_3();
	}
	private static void insertFullMemberAccountObject(){
		FullMemberAccountObjectInDB = TestObjectCreator.createFullMemberAccount();
		//## Required Relationships
		//## Additional fields and relationships / Updated fields
		FullMemberAccountObjectInDB.Status__c = 'Active';
		FullMemberAccountObjectInDB.Membership_Class__c = 'Full';
		FullMemberAccountObjectInDB.Designation__c = 'CA';
		insert FullMemberAccountObjectInDB;
	}
	private static void insertApplicationObject(){
		ApplicationObjectInDB = TestObjectCreator.createApplication();
		//## Required Relationships
		ApplicationObjectInDB.Account__c = FullMemberAccountObjectInDB.Id;
		//## Additional fields and relationships / Updated fields
		ApplicationObjectInDB.Application_Status__c = 'For Approval';
		insert ApplicationObjectInDB;
	}
	private static void insertFellowNominationObject(){
		FellowNominationObjectInDB = TestObjectCreator.createFellowNomination();
		//## Required Relationships
		FellowNominationObjectInDB.Member__c = FullMemberAccountObjectInDB.Id;
		//## Additional fields and relationships / Updated fields
		insert FellowNominationObjectInDB;
	}
	private static void insertTriggerConfigObject_1(){
		TriggerConfigObject_1InDB = new TriggerConfig__c();
		TriggerConfigObject_1InDB.Name = 'Name_8avE5OdUl0rp';
		TriggerConfigObject_1InDB.SourceObject__c = 'Application__c';
		TriggerConfigObject_1InDB.SourceField__c = 'Application_Status__c';
		TriggerConfigObject_1InDB.SourceFieldNewValue__c = 'Approved';
		TriggerConfigObject_1InDB.SourceFieldOldValue__c = 'For Approval';
		TriggerConfigObject_1InDB.SourceRelationshipToTarget__c = 'Account__c';
		TriggerConfigObject_1InDB.TargetObject__c = 'Account';
		TriggerConfigObject_1InDB.TargetField__c = 'RecordTypeId';
		TriggerConfigObject_1InDB.TargetFieldNewValue__c = 'Full_Member';
		TriggerConfigObject_1InDB.TargetFieldOldValue__c = 'Student_Affiliate';
		insert TriggerConfigObject_1InDB;
	}
	private static void insertTriggerConfigObject_2(){
		TriggerConfigObject_2InDB = new TriggerConfig__c();
		TriggerConfigObject_2InDB.Name = 'Name_6FLaMz4QyKMK';
		TriggerConfigObject_2InDB.SourceObject__c = 'Application__c';
		TriggerConfigObject_2InDB.SourceField__c = 'Application_Status__c';
		TriggerConfigObject_2InDB.SourceFieldNewValue__c = 'Declined';
		//TriggerConfigObject_2InDB.SourceFieldOldValue__c = 'For Approval';
		TriggerConfigObject_2InDB.SourceRelationshipToTarget__c = 'Account__c';
		TriggerConfigObject_2InDB.TargetObject__c = 'Account';
		TriggerConfigObject_2InDB.TargetField__c = 'Status__c';
		TriggerConfigObject_2InDB.TargetFieldNewValue__c = 'Declined';
		//TriggerConfigObject_2InDB.TargetFieldOldValue__c = 'Student_Affiliate';
		insert TriggerConfigObject_2InDB;
	}
	private static void insertTriggerConfigObject_3(){
		TriggerConfigObject_3InDB = new TriggerConfig__c();
		TriggerConfigObject_3InDB.Name = 'Name_gv4RsYxzNnSf';
		TriggerConfigObject_3InDB.SourceObject__c = 'Fellow_Nomination__c';
		TriggerConfigObject_3InDB.SourceField__c = 'Status__c';
		TriggerConfigObject_3InDB.SourceFieldNewValue__c = 'Approved';
		//TriggerConfigObject_3InDB.SourceFieldOldValue__c = 'For Approval';
		TriggerConfigObject_3InDB.SourceRelationshipToTarget__c = 'Member__c';
		TriggerConfigObject_3InDB.TargetObject__c = 'Account';
		TriggerConfigObject_3InDB.TargetField__c = 'Designation__c';
		TriggerConfigObject_3InDB.TargetFieldNewValue__c = 'FCA';
		TriggerConfigObject_3InDB.TargetFieldOldValue__c = 'CA';
		/*
			CA -> FCA
			Affiliate CAANZ (ignore!)
			AT -> AT (Fellow)
			ACA -> FACA
		*/
		insert TriggerConfigObject_3InDB;
	}
	
	//******************************************************************************************
	//                             TestMethods
	//******************************************************************************************
	/*
	static testMethod void testTriggerConfig_1() {
		ApplicationObjectInDB.Application_Status__c = TriggerConfigObject_1InDB.SourceFieldNewValue__c;
		update ApplicationObjectInDB;
		
		// Check results
		List<Account> Account_List = [Select RecordType.DeveloperName, RecordTypeId from Account 
			where id=:ApplicationObjectInDB.Account__c];
			
		System.assertEquals(1, Account_List.size(), 'Account_List.size');
		System.assertEquals(TriggerConfigObject_1InDB.TargetFieldNewValue__c, Account_List[0].RecordType.DeveloperName, 'RecordType.DeveloperName');
	}
	
	static testMethod void testTriggerConfig_2() {
		Test.startTest();
		ApplicationObjectInDB.Application_Status__c = TriggerConfigObject_2InDB.SourceFieldNewValue__c;
		update ApplicationObjectInDB;
		Test.stopTest();
		
		// Check results
		List<Account> Account_List = [Select Status__c from Account 
			where id=:ApplicationObjectInDB.Account__c];
			
		System.assertEquals(1, Account_List.size(), 'Account_List.size');
		//System.assertEquals(TriggerConfigObject_2InDB.TargetFieldNewValue__c, Account_List[0].Status__c, 'Account_List[0].Status__c');
	}
	*/
	static testMethod void testTriggerConfig_3() {
		Test.startTest();
		FellowNominationObjectInDB.Status__c = TriggerConfigObject_3InDB.SourceFieldNewValue__c;
		update FellowNominationObjectInDB;
		Test.stopTest();
		
		// Check results
		List<Account> Account_List = [Select Designation__c from Account 
			where id=:FellowNominationObjectInDB.Member__c];
			
		System.assertEquals(1, Account_List.size(), 'Account_List.size');
		//System.assertEquals(TriggerConfigObject_3InDB.TargetFieldNewValue__c, Account_List[0].Designation__c, 'Account_List[0].Designation__c');
	}
	
	/* Working for multiple records updated
	static testMethod void TestApplicationTrigger2() {
		ApplicationObjectInDB.Application_Status__c = 'Approved';
		ApplicationObjectInDB2.Application_Status__c = 'Approved';
		List<Application__c> Application_List = new List<Application__c>();
		Application_List.add(ApplicationObjectInDB);
		Application_List.add(ApplicationObjectInDB2);
		update Application_List;
		
		// Check results
		List<Account> Account_List = [Select RecordType.DeveloperName, RecordTypeId from Account];
		System.debug('XX ' + Account_List);
			//where id=:ApplicationObjectInDB.Account__c];
		
		//System.assertEquals(1, Account_List.size(), 'Account_List.size');
		//System.assertEquals('Full_Member', Account_List[0].RecordType.DeveloperName, 'RecordType.DeveloperName');
		//System.assertEquals(FullMemberAccountObjectInDB.Id, Account_List[0].Id, 'Account_List[0].Id');
	}
	*/
}