/*------------------------------------------------------------------------------------
Author:        Sumit Gupta
Company:       Salesforce
Description:   Test class for Content KB Trigger Handler class

History
Date            Author             Comments
--------------------------------------------------------------------------------------
04-06-2018     Sumit Gupta          Initial Release
------------------------------------------------------------------------------------*/
@isTest private class ContentKBTriggerHandlerTest {
	
	@isTest static void test_CreateArticles() {
		Content_KB__c content = new Content_KB__c(
			Name = 'Test', Summary__c = 'Test', Active__c = true, Topic__c = 'Test',
			Sub_Topic__c = new List<String>(ContentKBTriggerHandler.DATA_CATEGORY_LIST_SUBTOPIC)[0],
			Country__c = new List<String>(ContentKBTriggerHandler.DATA_CATEGORY_LIST_LOCATION)[0],
			Published_Date__c = String.valueOf(Date.today())
		);
		insert content;
		update content;
	}
}