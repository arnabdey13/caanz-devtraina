/*
    Test class for LogEmailAlertActivity.cls
    ======================================================
    Changes:
    	Jul 2016 	Davanti - YK	Created
*/

@isTest
private class LogEmailAlertActivityTest {
	
    @testSetup
    static void setup() {
    	
		Account businessAccount = TestObjectCreator.createBusinessAccount();
		insert businessAccount;
		
		Contact businessContact = TestObjectCreator.createContact();
        businessContact.AccountId = businessAccount.Id;
        insert businessContact;
		
		Review__c review = new Review__c(Practice_Name__c = businessAccount.Id);
        insert review;
		
		QPR_Questionnaire__c QPR1 = new QPR_Questionnaire__c(
			Type_of_Practice__c = 'Specialist FSA',
			Hidden_Country__c = 'AU',
			Person_Email__c = 'test@email.co.nz',
			Status__c = 'Questionnaire Complete',
			Questionnaire_Sent_Date__c = Date.today(),
			Questionnaire_Due_Date__c = Date.today().addDays(7),
			Questionnaire_Submitted_Date__c = Date.today(),
			RecordTypeId = Schema.SObjectType.QPR_Questionnaire__c.getRecordTypeInfosByName().get('QPR AUS').getRecordTypeId(),
			Practice__c = businessAccount.Id,
			Practice_ID__c = '12345',
			Review_Code__c = review.Id,
			PC_ID__c = businessContact.Id
		);
		insert QPR1;
	}
    
	static testMethod void Test_handleInboundEmail() {
		
		QPR_Questionnaire__c QPR1 = [SELECT Id, Name, CreatedById, PC_ID__c FROM QPR_Questionnaire__c LIMIT 1];
		
		String strSubject = 'Subject - ' + QPR1.Name + ' ';
		strSubject += 'R_SF_ID(' + QPR1.Id + ') ';
		strSubject += 'R_OWNER_ID(' + QPR1.CreatedById + ') ';
		strSubject += 'PC_ID(' + QPR1.PC_ID__c + ')';
		
		Messaging.InboundEmail email = new Messaging.InboundEmail();
		email.Subject = strSubject; 
		email.fromName = 'TESTER';
		email.htmlBody = 'TEST';
		email.plainTextBody = 'TEST';
		
		Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
		envelope.fromAddress = 'test@email.co.nz';
		
		Test.startTest();
		LogEmailAlertActivity emailActivity = new LogEmailAlertActivity();
		Messaging.InboundEmailResult result = emailActivity.handleInboundEmail(email, envelope);
		//System.assertEquals(result.success, true);
		Test.stopTest(); 
		
		Task emailTask = [SELECT WhatId, OwnerId, WhoId, Description FROM Task LIMIT 1];
		System.assertEquals(emailTask.whatId, QPR1.Id);
		System.assertEquals(emailTask.OwnerId, QPR1.CreatedById);
		System.assertEquals(emailTask.WhoId, QPR1.PC_ID__c);
		System.assertEquals(emailTask.Description, 'TEST');
		
	}
}