/***********************************************************************************************************************************************************************
Name: ContentDocumentLinkTriggerHandler 
============================================================================================================================== 
Purpose: Trigger Handler for ContentDocumentLinkTrigger.
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR                 DATE          DETAIL     Description 
1.0        Vinay                27/03/2019      Created    Trigger Handler for ContentDocumentLinkTrigger.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/
public class ContentDocumentLinkTriggerHandler extends TriggerHandler{
    //Context Variable Collections after Type Casting
    List<ContentDocumentLink> newList = (List<ContentDocumentLink>)Trigger.new;
    List<ContentDocumentLink> oldList = (List<ContentDocumentLink>)Trigger.old;
    Map<Id,ContentDocumentLink> oldMap = (Map<Id,ContentDocumentLink>)Trigger.oldMap;
    Map<Id,ContentDocumentLink> newMap = (Map<Id,ContentDocumentLink>)Trigger.newMap;
    
     public override void beforeInsert(){
         //Prevent Note Creation by the users with profile assigned from the list defined - Start.
        list<Profile> profileList = [Select Id, Name from Profile where Id =: UserInfo.getProfileId()];
        
        if(profileList != null && !profileList.isEmpty()){
         
            //Retrieving the custom setting inorder to determentine the profile for creating Notes
            IncaProfilesForNotesCreation__c IProfiles = IncaProfilesForNotesCreation__c.getInstance(profileList[0].Name);
            
            //Get the Key Prefix of INCAIncident sObject.
            String INCAObjectKeyPrefix = inCa_Incident__c.sObjectType.getDescribe().getKeyPrefix();
            system.debug('==>INCAObjectKeyPrefix ==>'+INCAObjectKeyPrefix);
            
            //Check if Current User Profile is exception for creation Notes.
            if(IProfiles == Null){
                if(newList != null && !newList.isEmpty()){             
                     for(ContentDocumentLink cntDocLinkRec : newList){
                         system.debug('***cntDocLinkRec-->'+cntDocLinkRec);                         
                         String linkEntiId = cntDocLinkRec.LinkedEntityId;
                         //Check if linkedEntityId of ContentDocumentLink is of INCA Incident sObject.
                         if(linkEntiId.startsWith(INCAObjectKeyPrefix))
                            cntDocLinkRec.addError(Label.Insufficient_Privileges_Creating_Notes);
                     }
                 }
             }
             
         }
         //Prevent Note Creation by the users with profile assigned from the list defined - End.
     } 
     
     public override void beforeUpdate(){} public override void beforeDelete(){} public override void afterInsert(){} public override void afterUpdate(){} public override void afterDelete(){} public override void afterUndelete(){}
}