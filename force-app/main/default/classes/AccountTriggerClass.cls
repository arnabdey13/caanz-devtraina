/*
* Trigger Handler Class for Account
  ======================================================
    History
    Apr 2014    TC Davanti  Created
    Jun 2016    RN Davanti  Updated for PASA, added Is_Migration_Agent__c check for profile allocation
                            and intercept update of 'Member Of' value.
    Aug 2016    Davanti     Updated for EHCH - created createPIRelationship() to create relationship records if "Apply PI Contact to child businesses?" is ticked.
    Jun 2018    Rinaldo     Added code to utilise MYCA profiles when after migration to MyCA Community (CCH - Customer Community Hub)  
    Oct 2018    Jun Rey     Updates for MyCA improvements - nickname and alias  
*/
public without sharing class AccountTriggerClass {

    public static Boolean isEmailUpdateRunning = true;
    public static Boolean isContactEmailUpdateBeforeCheckRunning = true;
    public static Boolean isProfileUpdateRunning = true;
  public static String CUSTOMER_COMMUNITY_MIGRATION_AGENT_PROFILE = 'Migration Agent User';
  public static String CUSTOMER_COMMUNITY_USER_PROFILE = 'NZICA Community Login User';
  public static String MYCA_COMMUNITY_NON_MEMBER_PROFILE = 'CAANZ CCH Community Non-Member';
  public static String MYCA_COMMUNITY_MEMBER_PROFILE = 'CAANZ CCH Community Member';
  public static String MYCA_COMMUNITY_PROV_MEMBER_PROFILE = 'CAANZ CCH Community Provisional Member';
  public static String MYCA_COMMUNITY_REVIEWER_PROFILE = 'CAANZ CCH Community Reviewer';
  public static String MYCA_COMMUNITY_AGENT_PROFILE = 'CAANZ CCH Community Migration Agent';
    
    @future
    public static void createCommunityMemberUser(Set<Id> accountIdSet) {
        List<Account> createdAccountList = [select Member_Of__c, PersonContactId, FirstName, LastName,Is_Migration_Agent__c,
                PersonEmail, Customer_ID__c, PersonContact.Email from Account where Id in :accountIdSet];

        MyCA_Config__mdt transitionedToMyCA = [SELECT My_CA_Config_Item__c,QualifiedApiName FROM MyCA_Config__mdt limit 1];

        List<User> userToCreateList = new List<User> ();
        for (Account acct : createdAccountList) {
            User u = new User();
            u.EmailEncodingKey = 'ISO-8859-1';
            u.LanguageLocaleKey = 'en_US';
            u.FirstName = acct.FirstName; // Needs to exist!!!
            u.LastName = acct.LastName;
            u.Email = acct.PersonEmail; // Needs to exist!!!
            u.ContactId = acct.PersonContactId;

            String UserHexCode = encodingUtil.convertToHex(Crypto.generateDigest('SHA1',
                    blob.valueOf(String.valueOf(Crypto.getRandomInteger())))).subString(0, 3);
             u.CommunityNickname = acct.FirstName+ '.' +acct.LastName+ '.' +UserHexCode;
             u.Alias = acct.FirstName.subString(0,1)+UserHexCode;

            u.FederationIdentifier = acct.Customer_ID__c; //.Customer_ID__c;
            System.debug('##x1 ' + u.FederationIdentifier);
            // PASA Change
      System.debug('*** Transitioned to MyCA is  ***'+transitionedToMyCA.My_CA_Config_Item__c);      
            if(!transitionedToMyCA.My_CA_Config_Item__c){
                //  Community users still using Customer Community registration process
        System.debug('*** Creating user as Customer Community Profile ***');
                if(acct.Is_Migration_Agent__c){
                    u.ProfileId = ProfileCache.getId(CUSTOMER_COMMUNITY_MIGRATION_AGENT_PROFILE);
                }
                else{
                    u.ProfileId = ProfileCache.getId(CUSTOMER_COMMUNITY_USER_PROFILE);
                }                
            }else
                //  Community users have transitioned to still using Customer Community registration process
            {
        System.debug('*** Creating user as MyCA Community Profile ***');
                if(acct.Is_Migration_Agent__c){
                    u.ProfileId = ProfileCache.getId(MYCA_COMMUNITY_AGENT_PROFILE);
                }
                else{
                    u.ProfileId = ProfileCache.getId(MYCA_COMMUNITY_NON_MEMBER_PROFILE);
                }                
            }
            //End PASA Change

            boolean isMemberOfNzica = (acct.Member_Of__c != 'NZICA') ? false : true;
            u.TimeZoneSidKey = (isMemberOfNzica) ? 'Pacific/Auckland' : 'Australia/Sydney';
            u.LocaleSidKey = (isMemberOfNzica) ? 'en_NZ' : 'en_AU';
            u.UserName = acct.PersonContact.Email; //+ '.caanz'; //acct.Customer_ID__c + ((isMemberOfNzica)?'@nzica.co.nz':'@caanz.com');
            userToCreateList.add(u);
        }
        insert userToCreateList;

        //WDCi LCA-403 to add non-member permission set after user is created.
        luana_NonMemberDataIntegrationHandler.updateNonMemberPermSet(userToCreateList);

    }



    // PASA formAssembly forms might update the member Of value, which would fail validation.
    // This method stops the update from occuring
    public static void stopMemberOfUpdate(List<Account> personAccList, Map<Id, Account> mapOldAccount){

        for(Account acct: personAccList){
            Account oldAcct = mapOldAccount.get(acct.Id);
            if(acct.Member_Of__c != oldAcct.Member_Of__c){
                acct.Member_Of__c = oldAcct.Member_Of__c;
            }
        }
    }
    public static void checkDuplicateUsername(List<Account> personAccList, Map<Id, Account> mapContactOld, Boolean isInsert){
        if(AccountTriggerClass.isContactEmailUpdateBeforeCheckRunning){
            AccountTriggerClass.isContactEmailUpdateBeforeCheckRunning = false;
            Map<String, Account> contactEmailAccountMap = new Map<String, Account> ();
            Map<String, String> contactIdEmailMap = new Map<String, String> ();
            Map<String, User> existingUserMap = new Map<String, User> ();

            for (Account conRec : personAccList) {
                if(isInsert){
                    contactEmailAccountMap.put(conRec.PersonEmail, conRec);
                    contactIdEmailMap.put(conRec.PersonEmail, conRec.PersonEmail);
                    conRec.Person_Account_Changed_Email__c = conRec.PersonEmail;
                }else{
                    if (String.isNotEmpty(conRec.PersonEmail) && conRec.PersonEmail != mapContactOld.get(conRec.Id).PersonEmail) {
                        conRec.Person_Account_Changed_Email__c = conRec.PersonEmail;
                        contactEmailAccountMap.put(conRec.Id, conRec);
                        contactIdEmailMap.put(conRec.Id, conRec.PersonEmail);
                    }
                }
                system.debug('***IN contactEmailAccountMap***' + contactEmailAccountMap);
                system.debug('***IN contactIdEmailMap***' + contactIdEmailMap);
                if (!contactIdEmailMap.isEmpty()) {
                    for (User u : [SELECT UserName FROM User WHERE Username IN :contactIdEmailMap.values()]) {
                        existingUserMap.put(u.Username, u);
                    }
                    system.debug('***IN existingUserMap***' + existingUserMap);
                    if (!existingUserMap.isEmpty()) {
                        for (String contactEmail : contactEmailAccountMap.keySet())
                        {
                            //Following line was updated by A.Porter, to add 'And Profile is not Equal to Marketo Integration User.  Prevents Marketo user from encountering Duplicate email validation error. 
                            if ((existingUserMap.keySet().contains(contactEmailAccountMap.get(contactEmail).PersonEmail)) && (userinfo.getProfileId() != '00e90000001aJh5AAE'))
                            {    
                                // Commenting the below code as, User is not able to update email address from MYCA Subscription and My Profile Privacy.
                                //contactEmailAccountMap.get(contactEmail).PersonEmail.addError('Duplicate Email Found');
                            }
                        }
                    }
                }

            }
        }
    }


  // Update member profile based on Member Type & Member Class 
    public static void updateMemberProfile(List<Account> personAccList, Map<Id, Account> mapOldAccount){
    
        system.debug('***IN updateMemberProfile METHOD***'+AccountTriggerClass.isProfileUpdateRunning);
        
        if(AccountTriggerClass.isProfileUpdateRunning){
            AccountTriggerClass.isProfileUpdateRunning = false ;

            Map<Id, User> accountIdUserMap = new Map<Id, User>();
            List<User> userUpdateList = new List<User>();
            Set<Id> AccountId_Set = new Set<Id>();
            
            for(Account personAccitem : personAccList){
              if( //Check if member Type or Class has changed and if so create a list of modified Accounts
                  personAccitem.Membership_Type__c!=mapOldAccount.get(personAccitem.Id).Membership_Type__c ||
                  personAccitem.Membership_Class__c!=mapOldAccount.get(personAccitem.Id).Membership_Class__c
                ){                    
                    AccountId_Set.add(personAccitem.Id);
                } 
            }
            // Create Map of user records that may need to have their profile changed.
            if(!AccountId_Set.isEmpty()){           
              system.debug('***IN Reading User Records***'+ AccountId_Set);
        updateUserRecords(AccountId_Set);
            }
            
        }
    }

    @future
    public static void updateUserRecords(Set<Id> accountIdSet){    
        Map<Id, User> accountIdUserMap = new Map<Id, User>();
        List<User> userUpdateList = new List<User>();
        List<Account> personAccList = new List<Account>();
        
        // Create Map of user records that may need to have their profile changed.
        if(!accountIdSet.isEmpty()){  
            personAccList = [SELECT Id, Membership_Type__c, Firstname, Lastname, Membership_Class__c FROM Account WHERE Id IN : accountIdSet];
            system.debug('***IN Reading User & Account Records***'+ accountIdSet);
            for(User userRec : [SELECT Id, Community_MyCA_User__c, Profile.Name, AccountId FROM User WHERE AccountId IN : accountIdSet]){
                accountIdUserMap.put(userRec.AccountId,userRec);
            }
            
            for(Account personAccitem : personAccList){
                system.debug('***Looping Account records to check for Profile Updates***'+ personAccitem.Firstname + ' ' +personAccitem.Lastname);
                // If there is no realted user record then continue to next record
                if (accountIdUserMap.get(personAccitem.Id) == Null)
                    continue;
                // If the user is no a My CA user then continue to next record
                if(!accountIdUserMap.get(personAccitem.Id).Community_MyCA_User__c && !Test.isRunningTest())
                    continue;
                // If the user is an Agent or a reviewer, don't update the profile
                if (accountIdUserMap.get(personAccitem.Id).Profile.Name == MYCA_COMMUNITY_REVIEWER_PROFILE  ||
                    accountIdUserMap.get(personAccitem.Id).Profile.Name == MYCA_COMMUNITY_AGENT_PROFILE)
                    continue;
                system.debug('*** Profile Updates***' );
                system.debug('*** Membership Type is '+ personAccitem.Membership_Type__c);
                system.debug('*** Membership Class is '+ personAccitem.Membership_Class__c);
                if(personAccitem.Membership_Type__c == 'Member'){
                    if(personAccitem.Membership_Class__c == 'Provisional' && 
                       accountIdUserMap.get(personAccitem.Id).Profile.Name != MYCA_COMMUNITY_PROV_MEMBER_PROFILE ){
                           system.debug('***Set Profile to Provisional***');
                           accountIdUserMap.get(personAccitem.Id).ProfileId = ProfileCache.getId(MYCA_COMMUNITY_PROV_MEMBER_PROFILE);
                           userUpdateList.add(accountIdUserMap.get(personAccitem.Id));
                       }
                    else  if(personAccitem.Membership_Class__c != 'Provisional' && 
                             accountIdUserMap.get(personAccitem.Id).Profile.Name != MYCA_COMMUNITY_MEMBER_PROFILE ){
                                 system.debug('***Set Profile to Member***');
                                 accountIdUserMap.get(personAccitem.Id).ProfileId = ProfileCache.getId(MYCA_COMMUNITY_MEMBER_PROFILE);
                                 userUpdateList.add(accountIdUserMap.get(personAccitem.Id));
                             }
                } else if (( personAccitem.Membership_Type__c == 'Past Member' || personAccitem.Membership_Type__c == 'Non Member') && 
                           accountIdUserMap.get(personAccitem.Id).Profile.Name != MYCA_COMMUNITY_NON_MEMBER_PROFILE ){
                               system.debug('***Set Profile to Non Member***');
                               accountIdUserMap.get(personAccitem.Id).ProfileId = ProfileCache.getId(MYCA_COMMUNITY_NON_MEMBER_PROFILE);
                               userUpdateList.add(accountIdUserMap.get(personAccitem.Id));                                          
                           }
            }
            if(!userUpdateList.isEmpty()){
                system.debug('***Updating User Profile***');             
                Database.update(userUpdateList);            
            }
        }
    }        
 }