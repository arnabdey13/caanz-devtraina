/*------------------------------------------------------------------------------------
Author:        Paras Prajapati
Company:       Salesforce
Description:   Test class for most of the Controllers - CASUB_AddressDetailsController_CA

History
Date            Author             Comments
--------------------------------------------------------------------------------------
01-04-2019     Paras Prajapati          Initial Release
------------------------------------------------------------------------------------*/
@isTest public class CASUB_AddressDetailsController_CA_Test {
    private static User currentUser;
    private static Account currentAccount;
    private static Contact currentContact;
    private static My_Preference__c myPreference;

    static {
        currentAccount = TestObjectCreator.createFullMemberAccount();
        currentAccount.Affiliated_Branch_Country__c = 'Australia';
        insert currentAccount;
        currentContact = [SELECT Id FROM Contact WHERE AccountId =: currentAccount.Id];
    }

    private static User getCurrentUser(){
        Test.startTest(); Test.stopTest(); // Future method needs to run to create the user.
        return currentUser = [SELECT Name FROM User WHERE AccountId =: currentAccount.Id];
    }
    
    @isTest static void test_CaanzAddressDetailsController() {
        System.runAs(getCurrentUser()) {
            String data = CASUB_CaanzAddressDetailsController_CA.getAddressData(null);
            System.assertNotEquals(data, null);
            String data2 = CASUB_CaanzAddressDetailsController_CA.getCountryAndStateOptions();
            System.assertNotEquals(data2, null);
            String data3 = CASUB_CaanzAddressDetailsController_CA.searchAddress('Test', 'AU', 10);
            System.assertNotEquals(data3, null);
            String data4 = CASUB_CaanzAddressDetailsController_CA.formatAddress('test', 'AU');
            System.assertNotEquals(data4, null);
            String data5 = CASUB_CaanzAddressDetailsController_CA.saveDeclarationStatement(currentAccount.Id, true);
            System.assertNotEquals(data5, null);
            try{
                CASUB_CaanzAddressDetailsController_CA.saveAddressData(data);
            } catch(Exception ex){
                System.debug(ex.getMessage());
            }
        }
    }
}