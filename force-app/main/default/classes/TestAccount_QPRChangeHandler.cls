@isTest
public class TestAccount_QPRChangeHandler {
  public static Id questionnaireAU = Schema.SObjectType.QPR_Questionnaire__c.getRecordTypeInfosByName().get('QPR AUS').getRecordTypeId();

@testSetup
     Static void setupTestData(){
        RecordType typeBusiness = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Business_Account' LIMIT 1];
        RecordType typeMember = [SELECT Id, Name FROM RecordType WHERE DeveloperName = 'Full_Member' LIMIT 1];
        List<Account> listAccounts = new List<Account>();
        Account personAccount = new Account();
        personAccount.RecordType = typeMember;
        personAccount.FirstName = 'Test';
        personAccount.LastName = 'Practice Contact';
        personAccount.Member_Of__c = 'ICAA';
        personAccount.Gender__c = 'Male';
        personAccount.Salutation = 'Mr';
        personAccount.PersonEmail = 'testpracticecontact@example.com';
        personAccount.Create_Portal_User__c = false;
        personAccount.Communication_Preference__c= 'Home Phone';
        personAccount.PersonHomePhone= '1234';
        personAccount.PersonOtherStreet= '83 Saggers Road';
        personAccount.PersonOtherCity='JITARNING';
        personAccount.PersonOtherState='Western Australia';
        personAccount.PersonOtherCountry='Australia';
        personAccount.PersonOtherPostalCode='6365'; 
        personAccount.PersonMailingCountry='Australia';
           personAccount.Affiliated_Branch_Country__c='Australia';
          personAccount.Is_QPR_Referral_Contact__c=true;
        listAccounts.add(personAccount);
          Account acct = new Account();
        acct.RecordType = typeBusiness;
        acct.Name = 'Test Account.ABC';
        acct.Type = 'Unallocated';
        acct.Phone = '6421771557';
        acct.Member_Of__c = 'ICAA';
        acct.BillingStreet = '33 Customhouse Quay';
        acct.BillingCity = 'Wellington';
        acct.BillingCountry = 'New Zealand';
        acct.Review_Contact__c = personAccount.Id;
        

        listAccounts.add(acct);   
        insert listAccounts;
         List<QPR_Risk_Assessment__c> qprRiskAssessmentList  = new List<QPR_Risk_Assessment__c>();
         QPR_Risk_Assessment__c qprRiskAssessmentRecord1  = new QPR_Risk_Assessment__c();
         qprRiskAssessmentRecord1.Risk_Driver__c = 'Monitoring';
         qprRiskAssessmentRecord1.Risk_Weighting_1_5__c =2;
         qprRiskAssessmentRecord1.Risk_Assessment_Date__c = Date.Today();
         qprRiskAssessmentRecord1.Risk_Summary__c ='test qprref trigger1';
         qprRiskAssessmentRecord1.Account__c = acct.Id;
         qprRiskAssessmentList.add(qprRiskAssessmentRecord1);
         QPR_Risk_Assessment__c qprRiskAssessmentRecord2  = new QPR_Risk_Assessment__c();
         qprRiskAssessmentRecord2.Risk_Driver__c = 'Monitoring';
         qprRiskAssessmentRecord2.Risk_Weighting_1_5__c =3;
         qprRiskAssessmentRecord2.Risk_Assessment_Date__c = Date.Today();
         qprRiskAssessmentRecord2.Risk_Summary__c ='test qprref trigger2';
         qprRiskAssessmentRecord2.Member_Name__c = personAccount.Id;
         qprRiskAssessmentList.add(qprRiskAssessmentRecord2);
        
        insert qprRiskAssessmentList;
        List<QPR_Referral__c> qprReferralList  = new List<QPR_Referral__c>();
        QPR_Referral__c qprReferralRecord1  = new QPR_Referral__c();
        qprReferralRecord1.Referral_Body__c = 'CA Board';
       	qprReferralRecord1.Review_referred_date__c =Date.Today();
        qprReferralRecord1.Referral_due_date__c = Date.Today();
       qprReferralRecord1.Other_Referral_Body__c ='test qprref trigger1';
       qprReferralRecord1.Referred_Account__c =acct.Id;
        qprReferralList.add(qprReferralRecord1);
        QPR_Referral__c qprReferralRecord2  = new QPR_Referral__c();
        qprReferralRecord2.Referral_Body__c = 'CA Board';
       	qprReferralRecord2.Review_referred_date__c =Date.Today();
        qprReferralRecord2.Referral_due_date__c = Date.Today();
       qprReferralRecord2.Other_Referral_Body__c ='test qprref trigger2';
         qprReferralRecord2.Referral_Contact__c=personAccount.Id;
        qprReferralList.add(qprReferralRecord2);
        
        insert qprReferralList;
     }
     @isTest
 Static void test_Account_QPRChangeHandler(){
    
        List<Account> listAccount = new List<Account>();
       	Account businessAccount = [SELECT Id, FirstName, LastName FROM Account WHERE Name = 'Test Account.ABC' LIMIT 1];
        Account personAccount = [SELECT Id, FirstName, LastName FROM Account WHERE PersonEmail = 'testpracticecontact@example.com' LIMIT 1];
      	personAccount.Affiliated_Branch_Country__c = 'New Zealand';
        listAccount.add(personAccount);
        businessAccount.BillingCountry='Australia';
        listAccount.add(businessAccount);
        update listAccount;
    } 	
}