/***********************************************************************************************************************************************************************
Name: Account_LuanaHandler_Test 
============================================================================================================================== 
Purpose: This class contains code related to Unit Testing and test coverage of class Account_LuanaHandler 
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR           DATE          DETAIL     Description 
1.0        Rama Krishna    08/02/2019    Created     This class contains code related to Unit Testing and test coverage of class Account_LuanaHandler
                                                             
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/
@isTest
public class Account_LuanaHandler_Test { 

    public static Luana_DataPrep_Test testDataGenerator;
    private static String classNamePrefixLong = 'Account_LuanaHandler_Test';
    private static String classNamePrefixShort = 'lanmu2';
    public static Account memberAccount;
    public static User memberUser {get; set;}
    public static Map<String,Id> commProfIdMap;
    public static luana_CommUserUtil userUtil {get; set;} 

    @testSetup static void createTestData() {
        test_TestData();
    }
    static void test_TestData(){ 
        //initialize
        testDataGenerator = new Luana_DataPrep_Test();
        List<Account> memAccList = new List<Account>();  
        List<edu_Education_History__c> edu_List = new List<edu_Education_History__c>();

        //Create all the custom setting
        insert testDataGenerator.prepLuanaExtensionSettingCustomSettings();
        insert testDataGenerator.createLuanaConfigurationCustomSetting();   
        Map<String, SObject> eduAssets = testDataGenerator.createEducationAssets();                              
        
        //Create user with Member and Employer community access 
        Id personAccountRecordTypeId = testDataGenerator.getRecordTypeIdMap('Account').get('Full_Member'); 
        memAccList = testDataGenerator.generateNewApplicantAccBulk(25,'JoeTest_' + classNamePrefixShort, classNamePrefixLong,personAccountRecordTypeId);
        Integer i=1;
        for(Account memacc:memAccList){
            memacc.Member_Id__c = '23456'+i;
            memacc.Affiliated_Branch_Country__c = 'Australia';
            memacc.Membership_Class__c = 'Full';        
            memacc.Assessible_for_CA_Program__c = false;
            memacc.PersonEmail = 'joe_1_' + classNamePrefixShort+ i +'@gmail.com'; 
            memacc.Communication_Preference__c= 'Home Phone';
            memacc.PersonHomePhone= '1234';
            memacc.PersonOtherStreet= '83 Saggers Road';
            memacc.PersonOtherCity='JITARNING';
            memacc.PersonOtherState='Western Australia';
            memacc.PersonOtherCountry='Australia';
            memacc.PersonOtherPostalCode='6365'; 
            i++;  
        }
        insert memAccList;
        
        List<Account> accList = [SELECT Id, PersonContactId FROM Account WHERE Id IN:memAccList];
        for(Account checkAccount :accList){
             edu_Education_History__c edu_asset = new edu_Education_History__c();             
             edu_asset = testDataGenerator.createEducationHistory(null, checkAccount.PersonContactId, true, eduAssets.get('universityDegreeJoin1').Id);
             edu_List.add(edu_asset);
        }
        insert edu_List;  

        // once the valid Education History is in place, only then can we set the Accessible for CA Program to true 
        for(Account acc:memAccList){
            acc.Assessible_for_CA_Program__c = true; 
        }       
        update memAccList;
    }    
 
//******************************************************************************************
//                             testMethods
//****************************************************************************************** 
    static testmethod void setupUser(){
        //LCA-921
        memberUser = [Select Id, Email, FirstName, LastName, UserName, Name from User Where Email =: 'joe_1_' + classNamePrefixShort+'1'+'@gmail.com' limit 1];
        //memberUser = testDataGenerator.generateNewApplicantUser(classNamePrefixLong, classNamePrefixshort, memberAccount, commProfIdMap.get('Customer Community Login User'));
        //insert memberUser;
        
        userUtil = new luana_CommUserUtil(memberUser.Id);
        
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
       
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defEmployerPermSetId.value__c));
        
        System.runAs(memberUser){
            insert psas;
        }

    }
    static testMethod void testAccountLuanaHandler(){      
        List<Account> accList = [select Id,PersonEmail,Assessible_for_CAF_Program__c from Account];            
        // once the valid Education History is in place, only then can we set the Accessible for CA Program to true
        for(Account acc:accList){
            acc.Assessible_for_CAF_Program__c = true;  
        }
        update accList;
        List<User> userList = [Select Id, Email, FirstName, LastName, UserName, Name from User Where Email =: 'joe_1_' + classNamePrefixShort+'1'+'@gmail.com' ];
        system.assertequals(true,userList.size()>0);
    }   

}