/*
    Developer: WDCi (Lean)
    Development Date: 05/04/2016
    Task #: Enrollment wizard - Controller for Summary
    
    Change History:
    LCA-998 27/09/2016 WDCI - KH remove all Regional Discount references
*/
public class luana_MyPPPEnrolmentController {
    

    public String custCommCountry {get; private set;} 
    
    private Boolean isMember {get; set;}
    private Boolean isNonMember {get; set;}
    public Boolean isReadyForWorkshopRegister {get; set;}
    public Boolean hasEnrolled {get; set;}
    public Boolean hasWorkshop {get; set;}
    
    public List<LuanaSMS__Student_Program__c> hasSP;
    public LuanaSMS__Student_Program__c selectedSP {get; set;}
    public List<LuanaSMS__Attendance2__c> workshopAtts {get; set;}
    
    
    public luana_MyPPPEnrolmentController(){

        isMember = false;
        isNonMember = false;
        hasEnrolled = false;
        hasWorkshop = false;
        isReadyForWorkshopRegister = false;
    
        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId());     
        custCommCountry = commUserUtil.custCommCountry;
        isMember = commUserUtil.hasAccessToMember; 
        isNonMember = commUserUtil.hasAccessToNonMember;
        
        workshopAtts = new List<LuanaSMS__Attendance2__c>();
        
        hasSP = [Select Id, Name, Specify_Assistance_Details__c, LuanaSMS__Contact_Student__r.Account.FirstName, LuanaSMS__Contact_Student__r.Account.Preferred_Name__c, LuanaSMS__Contact_Student__r.Account.LastName, 
                        LuanaSMS__Course__r.Name, Exam_Location__r.Name, Paid__c, Blackboard_eLearn_Status__c, LuanaSMS__Course__c, Workshop_Location_Preference_1__c, Workshop_Location_Preference_1__r.Name,
                        (select Name, LuanaSMS__Attendance_Status__c, LuanaSMS__Student_Program__c, LuanaSMS__Session__r.LuanaSMS__Venue__c, LuanaSMS__Session__r.LuanaSMS__Room__c,LuanaSMS__Session__r.LuanaSMS__Venue__r.Name, 
                        LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingAddress, 
                        LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingCity,
                        LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingStreet,
                        LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingState,
                        LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingPostalCode,
                        LuanaSMS__Session__r.LuanaSMS__Venue__r.BillingCountry,
                        LuanaSMS__Session__r.LuanaSMS__Room__r.Special_Instructions__c,
                        LuanaSMS__Session__r.LuanaSMS__Room__r.Name,
                        LuanaSMS__Session__r.Session_Start_Time_Venue__c,
                        LuanaSMS__Session__r.Session_End_Time_Venue__c,
                        LuanaSMS__Session__r.Name, LuanaSMS__Session__r.LuanaSMS__Start_Time__c, LuanaSMS__Session__r.LuanaSMS__End_Time__c 
                        from LuanaSMS__Attendances__r Where Record_Type_Name__c = 'Workshop') 
                     from LuanaSMS__Student_Program__c Where LuanaSMS__Contact_Student__c =: commUserUtil.custCommConId and RecordType.developerName = 'PPP'];
        
        System.debug('***hasSP:: ' + hasSP);
        
        if(!hasSP.isEmpty()){
            hasEnrolled = true;
            
            selectedSP = hasSP[0];
            
            System.debug('***selectedSP:: ' + selectedSP.Workshop_Location_Preference_1__c);
            
            if(selectedSP.Paid__c && selectedSP.Blackboard_eLearn_Status__c == 'Completed'){
                isReadyForWorkshopRegister = true;
            }
            
            if(!selectedSP.LuanaSMS__Attendances__r.isEmpty()){
                hasWorkshop = true;
                
                for(LuanaSMS__Attendance2__c att: selectedSP.LuanaSMS__Attendances__r){
                    workshopAtts.add(att);
                }
            }
        }
        
        
        
    }
    
    public PageReference enrolNewPPP(){
        
        //Luana_Extension_Settings__c pppStandardMember = Luana_Extension_Settings__c.getValues('PPP_Member_Standard');
        //Luana_Extension_Settings__c pppRegionalMember = Luana_Extension_Settings__c.getValues('PPP_Member_Regional');
        
        try{
            if(isNonMember){
                Luana_Extension_Settings__c pppNonMember = Luana_Extension_Settings__c.getValues('PPP_Non_Member');
                
                LuanaSMS__Course__c runningCourse = [select Id from LuanaSMS__Course__c where LuanaSMS__Status__c = 'Running' and LuanaSMS__Program_Offering__c =: pppNonMember.value__c limit 1];
                
                PageReference pageRef = Page.luana_enrolmentwizardcourse;
                pageRef.getParameters().put('producttype', 'ppp');
                pageRef.getParameters().put('poid', pppNonMember.value__c);
                pageRef.getParameters().put('courseid', runningCourse.Id);
                
                return pageRef;
                 
            }else if(isMember){
                
                //if(custCommCountry == 'Overseas'){LCA-998
                    Luana_Extension_Settings__c pppStandardMember = Luana_Extension_Settings__c.getValues('PPP_Member_Standard');
                    
                    LuanaSMS__Course__c runningCourse = [select Id from LuanaSMS__Course__c where LuanaSMS__Status__c = 'Running' and LuanaSMS__Program_Offering__c =: pppStandardMember.value__c limit 1];
                    
                    PageReference pageRef = Page.luana_enrolmentwizardcourse;
                    pageRef.getParameters().put('producttype', 'ppp');
                    pageRef.getParameters().put('poid', pppStandardMember.value__c);
                    pageRef.getParameters().put('courseid', runningCourse.Id);
                    
                    return pageRef;
                
                /*}else{
                    PageReference pageRef = Page.luana_MemberPPPLanding;
                    return pageRef;
                }*/
            }
        
        } catch (Exception exp){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Error loading PPP enrolment page: ' + exp.getMessage() + '. Please try again later or contact our support if the problem persists.'));
        }
        
        return null;
    }
    
    public PageReference workshopRegister(){
         PageReference pageRef = Page.luana_PPPWorkshopRegister;
         pageRef.getParameters().put('spid', selectedSP.Id);
         pageRef.getParameters().put('cdlid', selectedSP.Workshop_Location_Preference_1__c);
         pageRef.getParameters().put('courseid', selectedSP.LuanaSMS__Course__c);
         return pageRef;
    }
    
    /*public List<SelectOption> getWorkshopLocationOptions(){
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--None--'));
        List<Course_Delivery_Location__c> courseDeliveryLocations = luana_DeliveryLocationUtil.getCourseWorkshopLocation(selectedCourseId); //LCA-698

        for(Course_Delivery_Location__c dl: courseDeliveryLocations){
            if(dl.Type__c == 'Workshop Location'){
                SelectOption option = new SelectOption(dl.Id, dl.Delivery_Location__r.LuanaSMS__Delivery_Location_Name__c);
                options.add(option); 
                workshopLocationMap.put(dl.Id, dl);
            }
        }
        return options;
    }*/
}