/**
    Developer: WDCi (kh)
    Development Date:17/03/2016
    Task: Luana Test class for luana_MemberProgramEnrolmentController
    
    Change History
    //LCA-921 23/08/2019 WDCi - KH: Add person account email
**/
@isTest(seeAllData=false)
private class luana_MemberProgramEnrolmentCtl_Test {
    
    public static User memberUser {get; set;}
    public static Luana_DataPrep_Test dataPrep;
    public static luana_CommUserUtil userUtil {get; set;} 
    public static Account bussinessAcc {get; set;}
    
    private static String classNamePrefixLong = 'luana_MemberProgramEnrolmentCtl_Test';
    private static String classNamePrefixShort = 'lmpc';
    private static map<String, Id> commProfIdMap {get; set;}
    public static Account memberAccount {get; set;}
    
    public static void initial(){
        //initialize
        dataPrep = new Luana_DataPrep_Test();
        
        //Create all the custom setting
        insert dataPrep.prepLuanaExtensionSettingCustomSettings();
        insert dataPrep.createLuanaConfigurationCustomSetting();
        
        //Create user with Member and Employer community access
        memberAccount = dataPrep.generateNewApplicantAcc('Joe_' + classNamePrefixShort, classNamePrefixLong, 'Full_Member');
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.PersonEmail = 'joe_1_' +classNamePrefixShort+'@gmail.com';//LCA-921
        memberAccount.Communication_Preference__c= 'Home Phone';
        memberAccount.PersonHomePhone= '1234';
        memberAccount.PersonOtherStreet= '83 Saggers Road';
        memberAccount.PersonOtherCity='JITARNING';
        memberAccount.PersonOtherState='Western Australia';
        memberAccount.PersonOtherCountry='Australia';
        memberAccount.PersonOtherPostalCode='6365';  
        insert memberAccount;
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
    }
    
    public static void prepareSampleEduData(){
        
        //LCA-921
        memberUser = [Select Id, Email, FirstName, LastName, UserName, Name from User Where Email =: 'joe_1_' + classNamePrefixShort+'@gmail.com' limit 1];
        //memberUser = testDataGenerator.generateNewApplicantUser(classNamePrefixLong, classNamePrefixshort, memberAccount, commProfIdMap.get('Customer Community Login User'));
        //insert memberUser;
        
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
       
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defEmployerPermSetId.value__c));
        list<PermissionSetAssignment> permissAssign = [Select Id from PermissionSetAssignment where AssigneeId =: memberUser.Id AND (PermissionSetId =: defMemberPermSetId.value__c OR PermissionSetId =: defEmployerPermSetId.value__c)];
        System.runAs(memberUser){
            if(permissAssign == null)
                insert psas;
        }
        
        userUtil = new luana_CommUserUtil(memberUser.Id);  
        
        List<Terms_and_Conditions__c> tncList = dataPrep.createDefaultTNCs();
        insert tncList;
        
        List<Attachment> attchs = new List<Attachment>();
        for(Terms_and_Conditions__c tnc: tncList){
            attchs.add(dataPrep.addAttachmentToParent(tnc.Id, classNamePrefixLong + '-attchment')); 
        }
        insert attchs;

    }
    
    public static testMethod void testNavigateFunction() { 
        
        Test.startTest();
            initial();
        Test.stopTest();
        
        prepareSampleEduData();

        System.RunAs(memberUser){
          luana_MemberProgramEnrolmentController mpec = new luana_MemberProgramEnrolmentController();
          
          mpec.doNavigateMain();
        
        }
        
    }
    
    public static testMethod void testEnrolNewCourse() { 
        
        Test.startTest();
            initial();
        Test.stopTest();
        
        prepareSampleEduData();
        
        //dataPrep.createNewCourse('Graduate Diploma of Chartered Accounting', Id poId, Id recordTypeId);
        
        //Create traning org
        LuanaSMS__Training_Organisation__c trainingOrg = dataPrep.createNewTraningOrg(classNamePrefixLong, classNamePrefixLong, classNamePrefixShort, 'St 123', 'Wollonggong', '2244');
        insert trainingOrg;
        
        //Create program
        LuanaSMS__Program__c progrm = dataPrep.createNewProgram(classNamePrefixShort, classNamePrefixLong, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        insert progrm;
        
        //Create Product
        List<Product2> prodList = new List<Product2>();
        prodList.add(dataPrep.createNewProduct('FIN_AU', 'AU0001'));
        prodList.add(dataPrep.createNewProduct('FIN_NZ', 'NZ0001'));
        prodList.add(dataPrep.createNewProduct('FIN_INT', 'INT0001'));
        insert prodList;
        
        //Create Program Offering
        LuanaSMS__Program_Offering__c po = dataPrep.createNewProgOffering('PO_AM_' + classNamePrefixLong, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), progrm.Id, trainingOrg.Id, prodList[0].Id,prodList[1].Id, prodList[2].Id, 1, 1);
        insert po;
        
        //Create Course
        LuanaSMS__Course__c course = dataPrep.createNewCourse('Graduate Diploma of Chartered Accounting', po.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Program'), 'Running');
        insert course;
        
        //Set default enrolloment course id
        Luana_Extension_Settings__c defCourseEnrolmentId = new Luana_Extension_Settings__c(Name = 'Default_CA_Program_Course_Id', Value__c = course.Id);
        insert defCourseEnrolmentId;
        
        //Create multiple subjects
        List<LuanaSMS__Subject__c> subjList = new List<LuanaSMS__Subject__c>();
        subjList.add(dataPrep.createNewSubject('TAX AU_' + classNamePrefixShort, 'TAX AU_' + classNamePrefixLong, 'TAX AU_' + classNamePrefixShort, 1, 'Module'));
        subjList.add(dataPrep.createNewSubject('TAX NZ_' + classNamePrefixShort, 'TAX NZ_' + classNamePrefixLong, 'TAX NZ_' + classNamePrefixShort, 1, 'Module'));
        subjList.add(dataPrep.createNewSubject('AAA_' + classNamePrefixShort, 'AAA_' + classNamePrefixLong, 'AAA_' + classNamePrefixShort, 1, 'Module'));
        subjList.add(dataPrep.createNewSubject('Cap_' + classNamePrefixShort, 'Capstone_' + classNamePrefixLong, 'Cap_' + classNamePrefixShort, 1, 'Module'));
        subjList.add(dataPrep.createNewSubject('FIN_' + classNamePrefixShort, 'FIN_' + classNamePrefixLong, 'FIN_' + classNamePrefixShort, 55, 'Module'));
        subjList.add(dataPrep.createNewSubject('MAAF_' + classNamePrefixShort, 'MAAF_' + classNamePrefixLong, 'MAAF_' + classNamePrefixShort, 60, 'Module'));
        insert subjList;
        
        //Create multiple Program Offering Subject
        List<LuanaSMS__Program_Offering_Subject__c> posList = new List<LuanaSMS__Program_Offering_Subject__c>();
        for(LuanaSMS__Subject__c sub: subjList){
            if(sub.name =='TAX AU_' + classNamePrefixShort || sub.name =='TAX NZ_' + classNamePrefixShort){
                posList.add(dataPrep.createNewProgOffSubject(po.Id, sub.Id, 'Elective'));
            }else{
                posList.add(dataPrep.createNewProgOffSubject(po.Id, sub.Id, 'Core'));
            }
        }
        insert posList;
        
        
        //Test.startTest();
              System.RunAs(memberUser){
                  luana_MemberProgramEnrolmentController mpec = new luana_MemberProgramEnrolmentController();
                  
                  List<LuanaSMS__Course__c> courseFound = mpec.getProgramCourses();
                  System.assertEquals(courseFound.size(), 1, 'Expect 1 course found.');  
                  
                  mpec.doNavigateSubSelection();
                  
                  mpec.courseName = course.Name;
                  mpec.courseId = course.Id;
                  mpec.selectedCourseId = course.Id;
                  mpec.selectedPOId = course.LuanaSMS__Program_Offering__c;
                  mpec.minElective = 1;
                  mpec.maxElective = 0;
                  
                  mpec.subWrapList = mpec.getSWList();
                  System.assertEquals(mpec.subWrapList.size(), 6, 'Expect 6 Subject Wrapper found.');
                  
                  mpec.subWrapList[0].isSelected = true;
                  mpec.subWrapList[0].isCore = false;
                  
                  mpec.subWrapList[1].isSelected = true;
                  mpec.subWrapList[1].isCore = false;
                  
                  mpec.doCompleteSubSelect();
                  mpec.newSp.Accept_terms_and_conditions__c = true;
                  mpec.hasCAProgAtt = true;
                  mpec.createSpAndSps();
                  mpec.doNavigateProgramCourseSelect();
                  
                  mpec.getDomainUrl();
              }
        //Test.stopTest();
    }
}