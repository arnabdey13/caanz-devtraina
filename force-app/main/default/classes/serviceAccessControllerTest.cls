@isTest
public class serviceAccessControllerTest {

    //test member service access
    static testMethod void getServiceAccess_Member(){
        Account testAccount=TestObjectCreator.createFullMemberAccount();
        testAccount.Membership_Type__c='Member';

        Test.startTest();
        insert testAccount; // Future method
        Test.stopTest();
        Id testContactId = [Select id FROM Contact WHERE AccountId =: testAccount.Id limit 1].Id;
        User testCommunityUser = [Select Email, UserName, AccountId, FederationIdentifier FROM User WHERE contactId=:testContactId limit 1];
        
         boolean hasAccess=testGetServiceAccess(testCommunityUser,'My_Applications');
        system.debug('hasAccess='+hasAccess);
         System.assertEquals(true,hasAccess,'Member unexpectedly doesn\'t have access to My_Applications!');
    }

    //test student affiliate service access
    static testMethod void getServiceAccess_StudentAffiliate(){
        Account testAccount=TestObjectCreator.createStudentAffiliateAccount();
        testAccount.Membership_Type__c='Student Affiliate';

        Test.startTest();
        insert testAccount; // Future method
        Test.stopTest();
        Id testContactId = [Select id FROM Contact WHERE AccountId =: testAccount.Id limit 1].Id;
        User testCommunityUser = [Select Email, UserName, AccountId, FederationIdentifier FROM User WHERE contactId=:testContactId limit 1];
        
         boolean hasAccess=testGetServiceAccess(testCommunityUser,'My_Applications');
        system.debug('hasAccess='+hasAccess);
         System.assertEquals(true,hasAccess,'Student affiliate unexpectedly doesn\'t have access to My_Applications!');
    }

    //test non member service access
    static testMethod void getServiceAccess_NonMember(){
        Account testAccount=TestObjectCreator.createNonMemberAccount();
        testAccount.Membership_Type__c='Non Member';

        Test.startTest();
        insert testAccount; // Future method
        Test.stopTest();
        Id testContactId = [Select id FROM Contact WHERE AccountId =: testAccount.Id limit 1].Id;
        User testCommunityUser = [Select Email, UserName, AccountId, FederationIdentifier FROM User WHERE contactId=:testContactId limit 1];
        
         boolean hasAccess=testGetServiceAccess(testCommunityUser,'My_Applications');
        system.debug('hasAccess='+hasAccess);
         System.assertEquals(true,hasAccess,'Non member unexpectedly doesn\'t have access to My_Applications!');
    }

    //test past member service access
    static testMethod void getServiceAccess_PastMember(){
        Account testAccount=TestObjectCreator.createNonMemberAccount();
        testAccount.Membership_Type__c='Past Member';

        Test.startTest();
        insert testAccount; // Future method
        Test.stopTest();
        Id testContactId = [Select id FROM Contact WHERE AccountId =: testAccount.Id limit 1].Id;
        User testCommunityUser = [Select Email, UserName, AccountId, FederationIdentifier FROM User WHERE contactId=:testContactId limit 1];
        
         boolean hasAccess=testGetServiceAccess(testCommunityUser,'My_Applications');
        system.debug('hasAccess='+hasAccess);
         System.assertEquals(true,hasAccess,'Past member unexpectedly doesn\'t have access to My_Applications!');
    }

    //test past member service access
    static testMethod void getServiceAccess_Guest(){

		User testCommunityUser=TestObjectCreator.createMemberPortalSiteGuestUser();
		insert testCommunityUser;        
         boolean hasAccess=testGetServiceAccess(testCommunityUser,'My_Applications');
        system.debug('hasAccess='+hasAccess);
         System.assertEquals(false,hasAccess,'Guest unexpectedly has access to My_Applications!');
    }

    static boolean testGetServiceAccess(User testUser,String serviceName){
		boolean hasAccess;        
        System.runAs(testUser){
            hasAccess=serviceAccessController.getServiceAccess(serviceName);
        }
        return hasAccess;
    }
 
}