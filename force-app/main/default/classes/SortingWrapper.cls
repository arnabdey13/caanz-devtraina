/*********************************************
Description: Used to display list of values sorted by specific field. 
Proeject: Forms digitization
Author: RXP
*************************************************/

global class SortingWrapper implements Comparable {

    public Application_Categories__mdt appSubCateg;
    
    // Constructor
    public SortingWrapper(Application_Categories__mdt apSub) {
        appSubCateg = apSub;
    }
    
    // Compare record based on the object field.
    global Integer compareTo(Object compareTo) {
        // Cast argument to Wrapper
        SortingWrapper compareTosubcateg = (SortingWrapper)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (appSubCateg.Sub_Category__c > compareTosubcateg.appSubCateg.Sub_Category__c) {
            // Set return value to a positive value.
            returnValue = 1;
        } else if (appSubCateg.Sub_Category__c > compareTosubcateg.appSubCateg.Sub_Category__c) {
            // Set return value to a negative value.
            returnValue = -1;
        }
        
        return returnValue;       
    }
}