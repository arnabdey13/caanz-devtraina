/**
    Developer: WDCi (Lean)
    Development Date: 05/04/2016
    Task #: New Member Enrollment Wizard
**/

public abstract class luana_EnrolmentWizardObject {
    
    public static luana_EnrolmentWizardObject getController(luana_EnrolmentWizardController stdController, System.PageReference currentPage){
        
        system.debug('params :: ' + currentPage.getParameters());
        system.debug('currentPage :: ' + currentPage.getUrl());
        system.debug('luana_EnrolmentWizardLanding :: ' + Page.luana_EnrolmentWizardLanding.getUrl());
        
        if(currentPage.getUrl().toLowerCase().startsWith(Page.luana_EnrolmentWizardLanding.getUrl().toLowerCase())){
            return new luana_EnrolmentWizardLandingController();
            
        } else if(currentPage.getUrl().toLowerCase().startsWith(Page.luana_EnrolmentWizardProgram.getUrl().toLowerCase())){
            return new luana_EnrolmentWizardProgramController();
            
        } else if(currentPage.getUrl().toLowerCase().startsWith(Page.luana_EnrolmentWizardCourse.getUrl().toLowerCase())){
            return new luana_EnrolmentWizardCourseController();
            
        } else if(currentPage.getUrl().toLowerCase().startsWith(Page.luana_EnrolmentWizardSubject.getUrl().toLowerCase())){
            return new luana_EnrolmentWizardSubjectController();
            
        } else if(currentPage.getUrl().toLowerCase().startsWith(Page.luana_EnrolmentWizardDetailsDefault.getUrl().toLowerCase())){
            return new luana_EnrolmentWizardDetailsDEController();
            
        } else if(currentPage.getUrl().toLowerCase().startsWith(Page.luana_EnrolmentWizardDetailsCAProgram.getUrl().toLowerCase())){
            return new luana_EnrolmentWizardDetailsCPController();
            
        } else if(currentPage.getUrl().toLowerCase().startsWith(Page.luana_EnrolmentWizardDetailsCAModule.getUrl().toLowerCase())){
            return new luana_EnrolmentWizardDetailsCMController();
            
        } else if(currentPage.getUrl().toLowerCase().startsWith(Page.luana_EnrolmentWizardDetailsMasterclass.getUrl().toLowerCase())){
            return new luana_EnrolmentWizardDetailsMCController();
            
        } else if(currentPage.getUrl().toLowerCase().startsWith(Page.luana_EnrolmentWizardDetailsPPP.getUrl().toLowerCase())){
            return new luana_EnrolmentWizardDetailsPPController();
            
        } else if(currentPage.getUrl().toLowerCase().startsWith(Page.luana_EnrolmentWizardDetailsLLL.getUrl().toLowerCase())){
            return new luana_EnrolmentWizardDetailsLLController();
            
        } else if(currentPage.getUrl().toLowerCase().startsWith(Page.luana_EnrolmentWizardDetailsFoundation.getUrl().toLowerCase())){
            return new luana_EnrolmentWizardDetailsFDController();
            
        } else if(currentPage.getUrl().toLowerCase().startsWith(Page.luana_EnrolmentWizardDetailsNonAcc.getUrl().toLowerCase())){
            return new luana_EnrolmentWizardDetailsNAController();
            
        }
                
        return null;
    }
    
    
}