/**
    Developer: WDCi (Lean)
    Development Date: 29/04/2016
    Task #: LCA-247
    
    Change History:
    LCA-553/LCA-901 10/08/2016 - WDCi Lean: remove noncommunity check
**/
@isTest
private class luana_NetworkUtilTest {
    
    static testMethod void testNetworkUtil() {
        
        system.assert(luana_NetworkUtil.getCommunityPath() == '', 'This suppose to be empty in test class');
        system.assert(luana_NetworkUtil.isMemberCommunity() == true, 'This suppose to be true in test class');
        //LCA-901 system.assert(luana_NetworkUtil.isNonMemberCommunity() == true, 'This suppose to be true in test class');
        
        system.assert(luana_NetworkUtil.getTemplateName() == 'luana_MemberTemplate', 'This suppose to be luana_MemberTemplate in test class');
        
        System.assert(luana_NetworkUtil.isInternal(), 'This suppose to be true');
    }
}