/**
    Developer: WDCi (CM)
    Development Date:29/08/2016
    Task: Luana Test class for luana_WebinarAllocationWizardController
**/
@isTest(seeAllData=false)
private class luana_AT_WebinarAllocationWizard_Test {
    
    private static String classNamePrefixLong = 'luana_AT_WebinarAllocationWizard_Test';
    private static String classNamePrefixShort = 'lawawt';

    private static Luana_DataPrep_Test dataPrep;
    private static List<Account> memberAccounts;
    private static List<Contact> contactList;
    private static LuanaSMS__Course__c course;
    private static List<LuanaSMS__Course_Session__c> courseSessions;
    private static List<LuanaSMS__Student_Program__c> spList;

    private static void setup(String runSeqKey){
        dataPrep = new Luana_DataPrep_Test();
        
        List<Luana_Extension_Settings__c> settings = dataPrep.prepLuanaExtensionSettingCustomSettings();
        //Include Webinar settings
        settings.add(new Luana_Extension_Settings__c(Name = 'Webinar Allocation Report Params', Value__c = 'dummyText')); // Is normally used to return url of a report
        insert settings;
        insert dataPrep.createLuanaConfigurationCustomSetting();
        
        //Create traning org
        LuanaSMS__Training_Organisation__c trainingOrg = dataPrep.createNewTraningOrg(classNamePrefixLong, classNamePrefixLong, classNamePrefixShort, 'St 123', 'Wollonggong', '2244');
        insert trainingOrg;
        
        //Create program
        LuanaSMS__Program__c progrm = dataPrep.createNewProgram(classNamePrefixShort, classNamePrefixLong, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        insert progrm;
        
        //Create Product
        List<Product2> prodList = new List<Product2>();
        prodList.add(dataPrep.createNewProduct('FIN_AU_' + classNamePrefixLong, 'AU0001'));
        prodList.add(dataPrep.createNewProduct('FIN_NZ_' + classNamePrefixLong, 'NZ0001'));
        prodList.add(dataPrep.createNewProduct('FIN_INT_' + classNamePrefixLong, 'INT0001'));
        insert prodList;
        
        //Create Program Offering
        LuanaSMS__Program_Offering__c poAM = dataPrep.createNewProgOffering('PO_AM_' + classNamePrefixLong, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('CASM'), progrm.Id, trainingOrg.Id, prodList[0].Id,prodList[1].Id, prodList[2].Id, 1, 1);
        insert poAM;
        
        //Create course
        course = dataPrep.createNewCourse('Graduate Diploma of Chartered Accountring', poAM.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('CASM'), 'Running');
        insert course;
        
        //Create course delivery location
        LuanaSMS__Delivery_Location__c devLocation = dataPrep.createNewDeliveryLocationRecord('Australia - South Australia - Adelaide_' + classNamePrefixShort, 'Australia - South Australia - Adelaide_' + classNamePrefixShort, trainingOrg.Id, '1000', 'Australia');
        insert devLocation;
        Course_Delivery_Location__c courseDevLoc = dataPrep.createCourseDeliveryLocation(course.Id, devLocation.Id, 'Exam Location');
        insert courseDevLoc;
        Account venueAccount = dataPrep.generateNewBusinessAcc(classNamePrefixShort + 'Test Venue_'+runSeqKey, 'Venue', null, null, null, null, 'Sunshine Coast Office', 'Active');
        venueAccount.Ext_Id__c = classNamePrefixShort+'_'+runSeqKey;
        insert venueAccount;
        LuanaSMS__Room__c room1 = dataPrep.newRoom(classNamePrefixShort, classNamePrefixLong, venueAccount.Id, 20, 'Classroom', runSeqKey);
        insert room1;
        
        //Create sessions
        List<LuanaSMS__Session__c> sessions = new List<LuanaSMS__Session__c>();
        sessions.add(dataPrep.newSession(classNamePrefixShort, classNamePrefixLong, 'Virtual', 10, venueAccount.Id, room1.Id, courseDevLoc.Id, system.now().addHours(1), system.now().addHours(2))); //e.g, 1pm - 2pm
        //sessions.add(dataPrep.newSession(classNamePrefixShort, classNamePrefixLong, 'Virtual', 10, venueAccount.Id, room1.Id, courseDevLoc.Id, system.now().addHours(3), system.now().addHours(4))); //e.g, 3pm - 4pm        
        insert sessions;
        courseSessions = new List<LuanaSMS__Course_Session__c>();
        courseSessions.add(dataPrep.createCourseSession(course.id, sessions[0].id));
        //courseSessions.add(dataPrep.createCourseSession(course.id, sessions[1].id));
        insert courseSessions;        
        
        //Create students
        memberAccounts = new List<Account>();
        for(integer i = 0; i < 10; i ++){ 
            Account mAccount = dataPrep.generateNewApplicantAcc('Joe_' + classNamePrefixShort + '_' + i, classNamePrefixLong, 'Full_Member');
            mAccount.PersonEmail = classNamePrefixLong + i + '@example.com';
            mAccount.Member_Id__c = '1234' + i;
            mAccount.Affiliated_Branch_Country__c = 'Australia';
            mAccount.Membership_Class__c = 'Full';
            mAccount.Communication_Preference__c= 'Home Phone';
            mAccount.PersonHomePhone= '1234';
            mAccount.PersonOtherStreet= '83 Saggers Road';
            mAccount.PersonOtherCity='JITARNING';
            mAccount.PersonOtherState='Western Australia';
            mAccount.PersonOtherCountry='Australia';
            mAccount.PersonOtherPostalCode='6365';  
            memberAccounts.add(mAccount);
        }
        insert memberAccounts;
        Set<Id> accountIds = new Set<Id>();
        for (Account a : memberAccounts){
            accountIds.add(a.id);
        }
        contactList = [SELECT Id, AccountId FROM Contact WHERE AccountId IN : accountIds];
        
        //Insert Student Programs
        spList = new List<LuanaSMS__Student_Program__c>();
        for(Contact c : contactList){
            LuanaSMS__Student_Program__c sp = dataPrep.createNewStudentProgram(dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('CASM'), c.id, course.Id, 'Australia', 'In Progress');
            sp.Paid__c = true;
            spList.add(sp); 
        }
        insert spList;
    }
    
    private static testmethod void runWebinarWizardSuccess(){
        
        setup('1');
        
        test.startTest();
        
        //VF page
        PageReference pageRef = Page.luana_WebinarAllocationWizard;
        pageRef.getParameters().put('id', course.id);
        
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(course);
        luana_WebinarAllocationWizardController lwawc = new luana_WebinarAllocationWizardController(sc);
        
        system.assert(lwawc.workshopCourses.size() > 0, 'Could not find workshop courses.');
        
        for (integer i = 0; i < lwawc.workshopCourses.size(); i++){
            system.assert(lwawc.workshopCourses[i].sessionGroups.size() > 0, 'Could not find workshop course session groups.');
            for (integer j = 0; j < lwawc.workshopCourses[i].sessionGroups.size(); j++){
                //Something's off here.
                system.assert(lwawc.workshopCourses[i].sessionGroups[j].sessions.size() > 0, 'Could not find course sessions.');
                lwawc.workshopCourses[i].sessionGroups[j].fillUp = true;
                Boolean hasExisting = lwawc.workshopCourses[i].sessionGroups[j].getHasExistingSession(); //Called by page
            }
        }
        
        List<LuanaSMS__Student_Program__c> studentPrograms = lwawc.courseStudentPrograms.get(course.id);
        for (LuanaSMS__Student_Program__c sp : studentPrograms){
            system.Debug(':: Student Program :: ' + sp.id);
        }
        
        
        lwawc.doSave();
        
        test.stopTest();
        
        //Assertions
        String checkString = spList.size() + ' student program/s are allocated successfully. Please see the reports below for details.';
        boolean found = false;
        List<Apexpages.Message> messages = ApexPages.getMessages();
        for(Apexpages.Message msg : messages){
            if (msg.getDetail().contains(checkString)){
                found = true;
            }
        }
        System.assert(found, 'Not all student programs were successfully allocated.');
        
        Set<Id> contactIds = new Set<Id>();
        for (Contact c : contactList){
            contactIds.add(c.id);
        }
               
        //Check Attendances are made for each enrolment
        List<LuanaSMS__Attendance2__c> checkAttList = [SELECT Id, LuanaSMS__Contact_Student__c, RecordTypeId FROM LuanaSMS__Attendance2__c
                                                    WHERE LuanaSMS__Contact_Student__c IN : contactIds];
        system.assertEquals(spList.size(), checkAttList.size(), 'Incorrect number of attendances made.');
        
    }
    
    private static testmethod void runWebinarWizardAttendanceError(){
        
        setup('2');
        
        test.startTest();
        
        //VF page
        PageReference pageRef = Page.luana_WebinarAllocationWizard;
        pageRef.getParameters().put('id', course.id);
        
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(course);
        luana_WebinarAllocationWizardController lwawc = new luana_WebinarAllocationWizardController(sc);
        
        system.assert(lwawc.workshopCourses.size() > 0, 'Could not find workshop courses.');
        
        for (integer i = 0; i < lwawc.workshopCourses.size(); i++){
            system.assert(lwawc.workshopCourses[i].sessionGroups.size() > 0, 'Could not find workshop course session groups.');
            for (integer j = 0; j < lwawc.workshopCourses[i].sessionGroups.size(); j++){
                system.assert(lwawc.workshopCourses[i].sessionGroups[j].sessions.size() > 0, 'Could not find course sessions.');
                lwawc.workshopCourses[i].sessionGroups[j].fillUp = true;
                Boolean hasExisting = lwawc.workshopCourses[i].sessionGroups[j].getHasExistingSession(); //Called by page
            }
        }
        
        //Someone else removes a student program
        delete spList[0];
        
        lwawc.doSave();
        
        test.stopTest();
        
        //Assertions
        String checkString = 'There are 1 student program/s failed to be allocated.';
        boolean found = false;
        List<Apexpages.Message> messages = ApexPages.getMessages();
        for(Apexpages.Message msg : messages){
            if (msg.getDetail().contains(checkString)){
                found = true;
            }
        }
        //Check Attendances
        Set<Id> contactIds = new Set<Id>();
        for (Contact c : contactList){
            contactIds.add(c.id);
        }
        List<LuanaSMS__Attendance2__c> checkAttList = [SELECT Id, LuanaSMS__Contact_Student__c, RecordTypeId FROM LuanaSMS__Attendance2__c
                                                    WHERE LuanaSMS__Contact_Student__c IN : contactIds AND RecordTypeId = : dataPrep.getRecordTypeIdMap('LuanaSMS__Attendance2__c').get('CASM')];
        System.assert(found, 'Incorrect number of student programs were successfully allocated. Expecting: ' + (spList.size() - 1) + ' Actual: ' + checkAttList.size());
    }
    
    
}