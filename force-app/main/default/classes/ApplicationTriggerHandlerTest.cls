/*
* Handler for Application trigger
  ======================================================
    History 
    April 2016      Davanti     Created for PASA. Test class for ApplicationTriggerHandler  
    Jun 2019 - RXP-Modified for IPP related changes
*/
@isTest
private class ApplicationTriggerHandlerTest {
    static string APP_PASA_RT = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('PASA Application (Migration Assessment)').getRecordTypeId();
             
    static testMethod void onAfterInsertTest(){
        
        Account testAccount = TestObjectCreator.createFullMemberAccount();
        testAccount.Membership_Type__c='Member';
        
        Account testAccountAgent = TestObjectCreator.createFullMemberAccount();
        testAccountAgent.Membership_Type__c='Member';
        testAccountAgent.Is_Migration_Agent__c = true;
        
        Test.startTest();           
        
        insert testAccount; // Future method
        insert testAccountAgent;
        
        test.stopTest();
        
        Id testContactId = [Select id FROM Contact WHERE AccountId =: testAccount.Id limit 1].Id;
        User testCommunityUser = [Select Email, UserName, AccountId FROM User WHERE contactId=:testContactId limit 1];
        
        Id testMAContactId = [Select id FROM Contact WHERE AccountId =: testAccountAgent.Id limit 1].Id;
        //User testMACommunityUser = [Select Email, UserName, AccountId  FROM User WHERE contactId=:testMAContactId limit 1];

        
        List<User> listUsers = [select ID, AccountId from User where AccountId = :testAccountAgent.Id];
        
        List<AccountShare> shareList = [select Id, AccountId, UserOrGroupId from AccountShare
                                        where UserOrGroupId IN :listUsers
                                        and AccountId = :testAccount.Id];
        system.assert(shareList.size()==0);
        
        Application__c testAppDraft=new Application__c(Account__c=testAccount.Id ,
                                            Application_Status__c='Draft', 
                                            Migration_Agent__c = testAccountAgent.Id,
                                            recordtypeId = APP_PASA_RT);
        
        insert testAppDraft;
        
        shareList = [select Id, AccountId, UserOrGroupId from AccountShare
                        where UserOrGroupId IN :listUsers
                        and AccountId = :testAccount.Id];
        system.debug('### shareList: ' + shareList);
                            
        //system.assert(shareList.size()>0);
    }
    
    static testMethod void onAfterUpdateTest(){
    
        
        List<Application__c> ippList = new List<Application__c>();
        List<Application__c> updateList = new List<Application__c>();
        
        List<Account> nonMemAccs = new List<Account>();
        
        Account nonMemberAcc1 = TestObjectCreator.createNonMemberAccount();
        nonMemAccs.add(nonMemberAcc1);        
        
        Account nonMemberAcc2 = TestObjectCreator.createNonMemberAccount();
        nonMemAccs.add(nonMemberAcc2);        
        
        Account nonMemberAcc3 = TestObjectCreator.createNonMemberAccount();
        nonMemAccs.add(nonMemberAcc3);
        
        Account nonMemberAcc4 = TestObjectCreator.createNonMemberAccount();
        nonMemAccs.add(nonMemberAcc4);
        
        insert nonMemAccs;
        
        Application__c ippRecord1 = createIPPApplication();
        ippRecord1.Accounting_Bodies__c = 'ICAI (India)';
        ippRecord1.Account__c = nonMemAccs[3].Id; 
        
        Application__c ippRecord2 = createIPPApplication();
        ippRecord2.Accounting_Bodies__c = 'CA Sri Lanka';
        ippRecord2.Account__c = nonMemAccs[0].Id; 
        
        Application__c ippRecord3 = createIPPApplication();
        ippRecord3.Accounting_Bodies__c = 'ICAN (Nepal)';
        ippRecord3.Account__c = nonMemAccs[1].Id; 
        
        
        Application__c ippRecord4 = createIPPApplication();
        ippRecord4.Accounting_Bodies__c = 'ICAP (Pakistan)';
        ippRecord4.Account__c = nonMemAccs[2].Id;
        
        ippList.add(ippRecord1);
        ippList.add(ippRecord2);
        ippList.add(ippRecord3);
        ippList.add(ippRecord4);
            
        insert ippList;
        
        
        List<Application__c> insertedList = [select Id,Application_Status__c,Application_Fee_Paid1__c,application_assessor__c from Application__c where id IN :ippList];
       
            Application__c ap1 = insertedList[0];
            ap1.Application_Status__c = 'Approved'; 
            ap1.Application_Fee_Paid1__c = 'Yes';
            ap1.application_assessor__c = userinfo.getUserId();
            update ap1;
        
            
            Application__c ap2 = insertedList[1];
            ap2.Application_Status__c = 'Approved'; 
            ap2.Application_Fee_Paid1__c = 'Yes';
            ap2.application_assessor__c = userinfo.getUserId();
            update ap2;
            
        
            Application__c ap3 = insertedList[2];
            ap3.Application_Status__c = 'Approved'; 
            ap3.Application_Fee_Paid1__c = 'Yes';
            ap3.application_assessor__c = userinfo.getUserId();
            update ap3;
            
        
            Application__c ap4 = insertedList[3];
            ap4.Application_Status__c = 'Approved'; 
            ap4.Application_Fee_Paid1__c = 'Yes';
            ap4.application_assessor__c = userinfo.getUserId();
            //update ap4;
            //updateList.add(ap4);   
            
            Set<ID> updatedApps = new Set<ID>();
        	updatedApps.add(ap1.Id);
       		updatedApps.add(ap2.Id);
        	updatedApps.add(ap3.Id);
        
            List<Application__c> updatedList = [select Id,Application_Status__c,Application_Fee_Paid1__c,application_assessor__c from Application__c where id IN :updatedApps];
        	List<Application__c> approveList = new List<Application__c>();	
        	for(Application__c newapp: updatedList){
                newapp.Application_Status__c = 'Approved';
                approveList.add(newapp);               
            }
        
          
        
         	update approveList;
             
        
    }
    
    static testMethod void onAfterUpdateTest_mode1(){
        
        List<Application__c> ippList = new List<Application__c>();
        List<Application__c> updateList = new List<Application__c>();
        
        List<Account> nonMemAccs = new List<Account>();
        
        Account nonMemberAcc1 = TestObjectCreator.createNonMemberAccount();
        nonMemAccs.add(nonMemberAcc1); 
        
        insert nonMemberAcc1;
        
        Application__c ippRecord2 = createIPPApplication();
        ippRecord2.Accounting_Bodies__c = 'CA Sri Lanka';
        ippRecord2.Account__c = nonMemberAcc1.Id; 
        
        Application__c ippRecord3 = createIPPApplication();
        ippRecord3.Accounting_Bodies__c = 'ICAN (Nepal)';
        ippRecord3.Account__c = nonMemberAcc1.Id; 
        
        ippList.add(ippRecord2);
        ippList.add(ippRecord3);
            
        insert ippList;
        
         List<Application__c> insertedList = [select Id,Application_Status__c,Application_Fee_Paid1__c,application_assessor__c from Application__c where id IN :ippList];
       
            Application__c ap1 = insertedList[0];
            ap1.Application_Status__c = 'Approved'; 
            ap1.Application_Fee_Paid1__c = 'Yes';
            ap1.application_assessor__c = userinfo.getUserId();
            update ap1;
        
            Application__c ap2 = insertedList[1];
            ap2.Application_Status__c = 'Approved'; 
            ap2.Application_Fee_Paid1__c = 'Yes';
            ap2.application_assessor__c = userinfo.getUserId();
            update ap2;
        
          Set<ID> updatedApps = new Set<ID>();
        	updatedApps.add(ap1.Id);
       		updatedApps.add(ap2.Id);
        
            List<Application__c> updatedList = [select Id,Application_Status__c,Application_Fee_Paid1__c,application_assessor__c from Application__c where id IN :updatedApps];
        	List<Application__c> approveList = new List<Application__c>();	
        	for(Application__c newapp: updatedList){
                newapp.Application_Status__c = 'Approved';
                approveList.add(newapp);               
            }
        
          
        
         	update approveList;
        
        
        
        
        
    }
    
    
    
    static testMethod Application__c createIPPApplication(){
    
        ApexUtil.getRecordTypeId('Application__c','IPP_Provisional_Member');
        ApexUtil.getRecordTpeInfoByObject('Application__c');

        
        Account nonMemberAcc = TestObjectCreator.createNonMemberAccount();
        insert nonMemberAcc;
         
        Id IPPRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('IPP Provisional Member').getRecordTypeId();
    
        Application__c IPPApplication = TestObjectCreator.createApplication();        
        IPPApplication.recordtypeid=IPPRecordTypeId;
        IPPApplication.Adjudged_Bankrupt__c='No';
        IPPApplication.Application_Status__c='Submitted for Assessment';        
        IPPApplication.Lock__c=true;
        IPPApplication.Convicted_of_any_crime__c='No'; 
        IPPApplication.Subject_to_disciplinary_proceedings__c='No';
        IPPApplication.Prohibited_from_company_management__c='No';
        IPPApplication.Reciprocal_Application__c='No';
        IPPApplication.College__c='Chartered Accountants';
        IPPApplication.Reference_s_email__c = 'kat@gmail.com';
        IPPApplication.Reference_s_First_name__c='testAB';
        IPPApplication.Reference_s_Last__c='kavy';
        IPPApplication.Reference_s_phone_number__c='9000000009';
        IPPApplication.Reference_2_Company__c='ilfs';
        IPPApplication.Reference_2_Email__c='test@gmail.com';
        IPPApplication.Reference_2_first_name__c='warner';
        IPPApplication.reference_2_last_name__c='DY';
        IPPApplication.Reference_2_member_number__c='123445';
        IPPApplication.reference_2_phone_number__c='9000990909';
        IPPApplication.Reference_2_Role__c='manage';
        IPPApplication.Reference_s_member_number__c='123456';
        IPPApplication.Subject_to_disciplinary_by_education__c='No';
        IPPApplication.Accounting_Bodies__c = 'ICAI (India)';        
        return IPPApplication;
        
        
        
        
    }
}