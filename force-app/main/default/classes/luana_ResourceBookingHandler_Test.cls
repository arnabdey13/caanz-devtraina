/**
    Developer: WDCi (kh)
    Development Date:24/06/2016
    Task: Luana Test class for luana_ResourceBookingAction trigger & luana_ResourceBookingHandler class
    
    Change History:
    11/07/2016 WDCi - KH: include custom setting(Education Record Default University)
    LCA-921 23/08/2019 WDCi - KH: Add person account email
**/
@isTest(seeAllData=false)
private class luana_ResourceBookingHandler_Test{
    
    public static User memberUser {get; set;}
    public static Luana_DataPrep_Test dataPrep;
    public static luana_CommUserUtil userUtil {get; set;} 
    
    private static String classNamePrefixLong = 'luana_ResourceBookingHandler_Test';
    private static String classNamePrefixShort = 'lrbh';
    private static map<String, Id> commProfIdMap {get; set;}
    public static Account memberAccount {get; set;}
    
    public static Payment_Token__c currentPToken {get; set;}
    
    public static Account venueAccount;
    public static LuanaSMS__Room__c room1;
    public static LuanaSMS__Session__c session1;
    public static LuanaSMS__Session__c session2;
    
    public static LuanaSMS__Resource__c resource1;
    
    //LCA-921
    public static void initial(){
        //initialize
        dataPrep = new Luana_DataPrep_Test();
        
        //Create all the custom setting
        insert dataPrep.prepLuanaExtensionSettingCustomSettings();
        insert dataPrep.createLuanaConfigurationCustomSetting();
        
        Luana_Extension_Settings__c eduRecDefaultU = new Luana_Extension_Settings__c();
        eduRecDefaultU.Name = 'Education Record Default University';
        eduRecDefaultU.Value__c = 'Chartered Accountants Australia and New Zealand';
        insert eduRecDefaultU;
        
        Luana_Extension_Settings__c defaultUniName = Luana_Extension_Settings__c.getValues('Education Record Default University');

        //Create user with Member and Employer community access
        memberAccount = dataPrep.generateNewApplicantAcc('Joe_' + classNamePrefixShort, classNamePrefixLong, 'Full_Member');
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.PersonEmail = 'joe_1_' +classNamePrefixShort+'@gmail.com';//LCA-921
        memberAccount.Communication_Preference__c= 'Home Phone';
        memberAccount.PersonHomePhone= '1234';
        memberAccount.PersonOtherStreet= '83 Saggers Road';
        memberAccount.PersonOtherCity='JITARNING';
        memberAccount.PersonOtherState='Western Australia';
        memberAccount.PersonOtherCountry='Australia';
        memberAccount.PersonOtherPostalCode='6365';
        insert memberAccount;
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
    }
    
    public static void prepareSampleEnrolmentData(String runSeqKey){
        
        //LCA-921
        memberUser = [Select Id, Email, FirstName, LastName, UserName, Name, ContactId from User Where Email =: 'joe_1_' + classNamePrefixShort+'@gmail.com' limit 1];
        //memberUser = testDataGenerator.generateNewApplicantUser(classNamePrefixLong, classNamePrefixshort, memberAccount, commProfIdMap.get('Customer Community Login User'));
        //insert memberUser;
        
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
        
        
        Account busAcc = dataPrep.generateNewBusinessAcc('Chartered Accountants Australia and New Zealand', 'Business_Account', 'Chartered Accounting', '11111112', '012112111', 'NZICA', '1 Main Street', 'Active');
        insert busAcc;
        
        userUtil = new luana_CommUserUtil(memberUser.Id);
        insert dataPrep.generateNewBusinessAcc('Price Waterhouse_' + classNamePrefixShort, 'Business_Account', 'Chartered Accounting', '11111111', '012112111', 'NZICA', '123 Main Street', 'Active');
        String bAccount = 'Price Waterhouse_' + classNamePrefixShort;
        Account acc = [Select Id, Name from Account Where Name =: bAccount limit 1];
        
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defEmployerPermSetId.value__c));
        list<PermissionSetAssignment> permissAssign = [Select Id from PermissionSetAssignment where AssigneeId =: memberUser.Id AND (PermissionSetId =: defMemberPermSetId.value__c OR PermissionSetId =: defEmployerPermSetId.value__c)];
        System.runAs(memberUser){
            if(permissAssign == null)
                insert psas;
        }
        
        Employment_History__c emplomentHistory = dataPrep.createNewEmploymentHistory(userUtil.custCommAccId, acc.Id, 'Current', 'Engineer');
        insert emplomentHistory;
        currentPToken = dataPrep.createNewPaymentToken(userUtil.custCommAccId, null, System.today(), false);
        insert currentPToken;
        
        resource1 = dataPrep.createResource('resource1_' + classNamePrefixShort, 'resource1_' + classNamePrefixShort);
        insert resource1;
        
        venueAccount = dataPrep.generateNewBusinessAcc(classNamePrefixShort + 'Test Venue', 'Venue', null, null, null, null, 'Sunshine Coast Office', 'Active');
        venueAccount.Ext_Id__c = classNamePrefixShort +'_'+runSeqKey;
        insert venueAccount;

        room1 = dataPrep.newRoom(classNamePrefixShort, classNamePrefixLong, venueAccount.Id, 20, 'Classroom', runSeqKey);
        insert room1;
        
        session1 = dataPrep.newSession(classNamePrefixShort, classNamePrefixLong, 'Virtual', 20, venueAccount.Id, room1.Id, null, system.now().addHours(1), system.now().addHours(2)); //e.g, 1pm - 2pm
        session1.Topic_Key__c = classNamePrefixShort + '_T1';
        session1.LuanaSMS__Start_Time__c = System.now().addDays(-2);
        session1.LuanaSMS__End_Time__c = System.now().addDays(-1);
        
        session2 = dataPrep.newSession(classNamePrefixShort, classNamePrefixLong, 'Virtual', 20, venueAccount.Id, room1.Id, null, system.now().addHours(3), system.now().addHours(4)); //e.g, 3pm - 4pm        
        session2.Topic_Key__c = classNamePrefixShort + '_T1';
        insert new List<LuanaSMS__Session__c>{session1, session2};
        
    }
    
    static testMethod void testCreateNewResrouceBooking() {
        
        Test.startTest();
            initial();
        Test.stopTest();
        
        prepareSampleEnrolmentData('1');
        
        List<PermissionSetAssignment> initialPermSetAssignments = [SELECT Id, AssigneeId, PermissionSet.Name FROM PermissionSetAssignment Where PermissionSet.Name = 'Luana_ContributorCommunityPermission'];
        //System.assertEquals(initialPermSetAssignments.isEmpty(), true, 'Expect no user in \'Luana_ContributorCommunityPermission\'');
        
        LuanaSMS__Resource_Booking__c resourceBooking = dataPrep.createResourceBooking(userUtil.custCommConId, resource1.Id, session1.Id);
        resourceBooking.Allocate_CPD_Hours__c = true;
        resourceBooking.CPD_Hours__c = 2;
        resourceBooking.LuanaSMS__Start_Time__c = System.now().addDays(-2);
        resourceBooking.LuanaSMS__End_Time__c = System.now().addDays(-1);
        
        System.debug('****resourceBooking: ' + resourceBooking);
        
        insert resourceBooking;
            
        List<PermissionSetAssignment> finalPermSetAssignments = [SELECT Id, AssigneeId, PermissionSet.Name FROM PermissionSetAssignment Where PermissionSet.Name = 'Luana_ContributorCommunityPermission'];
        System.assertEquals(finalPermSetAssignments.isEmpty(), false, 'Expect one user is assigned to \'Luana_ContributorCommunityPermission\'');
    }
    
    static testMethod void testUpdateWithDuplicateResrouceBooking() {
        
        Test.startTest();
            initial();
        Test.stopTest();
        
        prepareSampleEnrolmentData('2');
        
        List<PermissionSetAssignment> initialPermSetAssignments = [SELECT Id, AssigneeId, PermissionSet.Name FROM PermissionSetAssignment Where PermissionSet.Name = 'Luana_ContributorCommunityPermission'];
        //System.assertEquals(initialPermSetAssignments.isEmpty(), true, 'Expect no user in \'Luana_ContributorCommunityPermission\'');
        
        LuanaSMS__Resource_Booking__c resourceBooking = dataPrep.createResourceBooking(userUtil.custCommConId, resource1.Id, session1.Id);
        insert resourceBooking;
        
        resourceBooking.LuanaSMS__End_Time__c = System.now().addDays(1);
        update resourceBooking;
            
        
        
        List<PermissionSetAssignment> finalPermSetAssignments = [SELECT Id, AssigneeId, PermissionSet.Name FROM PermissionSetAssignment Where PermissionSet.Name = 'Luana_ContributorCommunityPermission'];
        System.assertEquals(finalPermSetAssignments.isEmpty(), false, 'Expect one user is assigned to \'Luana_ContributorCommunityPermission\'');
    }
    
    
    static testMethod void testDeleteExistingResrouceBooking() {
        
        Test.startTest();
            initial();
        Test.stopTest();    
        
        prepareSampleEnrolmentData('3');
        
        List<PermissionSetAssignment> initialPermSetAssignments = [SELECT Id, AssigneeId, PermissionSet.Name FROM PermissionSetAssignment Where PermissionSet.Name = 'Luana_ContributorCommunityPermission'];
        //System.assertEquals(initialPermSetAssignments.isEmpty(), true, 'Expect no user in \'Luana_ContributorCommunityPermission\'');
        
        
        LuanaSMS__Resource_Booking__c resourceBooking = new LuanaSMS__Resource_Booking__c();
        
        resourceBooking = dataPrep.createResourceBooking(userUtil.custCommConId, resource1.Id, session1.Id);
        insert resourceBooking;
            
        
        List<PermissionSetAssignment> finalPermSetAssignments = [SELECT Id, AssigneeId, PermissionSet.Name FROM PermissionSetAssignment Where PermissionSet.Name = 'Luana_ContributorCommunityPermission'];
        System.assertEquals(finalPermSetAssignments.isEmpty(), false, 'Expect one user is assigned to \'Luana_ContributorCommunityPermission\'');
        
        delete resourceBooking;

    }
}