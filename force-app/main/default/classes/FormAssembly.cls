global without sharing class FormAssembly { // without sharing because portal user will not have access to account data
        
    public FormAssembly(ApexPages.StandardController controller){

    }
    public String GetMySessionId() { return UserInfo.getSessionId(); }
        
    webService static String getMentor(String mentId, String mentEmail) {
    //DN COAA-15 add mentor id to result - altered query, added str4

        List<Account> account = [SELECT Id, Name, Mentor__c, Member_ID__c, PersonTitle, PersonEmail, Secondary_Email__c
                                 FROM Account WHERE Member_ID__c = :String.escapeSingleQuotes(mentId)
                                 and ((PersonEmail = :String.escapeSingleQuotes(mentEmail) and PersonEmail != null)
                                    or (Secondary_Email__c = :String.escapeSingleQuotes(mentEmail) and Secondary_Email__c != null))
                                 limit 1];

        if(!account.ISEMPTY()){
            String str1=account[0].Name;
            String str2=';';
            String str3=';';
            
            String str4=';'+account[0].Id;
            
            List<Employment_History__c> empHis=[Select Name, Employer__r.Name from Employment_History__c 
                                                where Primary_Employer__c=true 
                                                AND Member__c=:account[0].Id];

            if(!empHis.ISEMPTY() && empHis[0].Employer__c!=null){
                str2=';'+empHis[0].Employer__r.Name;
            }
            if(account[0].PersonEmail != null) {
                str3=';'+account[0].PersonEmail;
            }
            return str1+str2+str3+str4;
        } else{
            return null;
        }   
    }
    webService static List<Account> getApprovedAccounts(string searchTerm, string memberOf) {   	
        List<Account> accounts = new List<Account>();
        String strQuery = 'select Id, Name, Member_ID__c, BillingStreet, BillingCity, BillingCountry, BillingState, BillingPostalCode from Account where RecordType.Name = \'Business Account\'' +
         					' and Member_Of__c = \'' +  String.escapeSingleQuotes(memberOf) + '\'' +
                            ' and Name like \'' +  String.escapeSingleQuotes(searchTerm) + '%\' limit 25';
        accounts = Database.query(strQuery);  
        return accounts;
    }
      
    webService static List<Account> getAccountDetails(string strID) {
        List<Account> accounts = new List<Account>();
        String strQuery = 'select Id, Name from Account where recordtype.developername = \'Business_Account\'' +       					
                            ' and Id = \'' +  String.escapeSingleQuotes(strID) + '\'';
        accounts = Database.query(strQuery);  
        return accounts;
    }
                
  
    webService static List<Employment_History__c> getAccounts(string searchTerm){
        List<Employment_History__c> accList = new List<Employment_History__c>();
        if(searchTerm != ''){
            String query = 'select Name, id, Job_Title__c,Employer_Name__c, Employer__c from Employment_History__c where Employer_Name__c like \'' +  String.escapeSingleQuotes(searchTerm) + '%\'';
            accList= Database.query( query);
        }
        System.debug('accList------------'+accList);
        return accList;
    }
  
    webService static Employment_History__c getAccount(string strId){
        List<Employment_History__c> accList = new List<Employment_History__c>();
        if(strId != '') {
            String query = 'select Name, id, Job_Title__c, Employer__c, Employer_Name__c from Employment_History__c where Id = \'' +  strId + '\'';
            accList=Database.query( query);
        }
        Employment_History__c emp = new Employment_History__c();
        if(accList.size()>0) emp = accList[0];
        return emp;
    }
   
}