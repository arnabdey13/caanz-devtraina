/*
	Change History:
	LCA-1169    06/02/2018 - WDCi Lean:     replace CAPSTONE subject name with checkbox
	LCA-1161    07/03/2018 - WDCi LKoh:     include Status & Final Module Mark column to the page    
*/

public class luana_JoinedEnrolment {
        public Id spsId {set; get;}
        public String subName {set; get;}
        public String subId {set; get;}
        public String cName {set; get;}
        public String status {set; get;}
        public String result {set; get;}
        public String merit {set; get;}
        public Decimal score {set; get;}
        public Boolean isPaid {set; get;}
        public Id spId {set; get;}
        public Boolean isEnrolEnabled {set; get;}
        public Integer enrolMode {set; get;}
        public String actionUrl {set; get;}
        public boolean isCapstone {set; get;}

    	
    	//LCA-1161
        public String resolution {get; set;}
    	
        public luana_JoinedEnrolment(Id spsId,
                                     String subName,
                                     String subId,
                                     String cName,
                                     String status,
                                     String result,
                                     String merit,
                                     Decimal score,                                   
                                     Boolean isPaid,
                                     Id spId,
                                     Boolean isEnrolEnabled,
                                     Integer enrolMode,
                                     String actionUrl,
                                     String resolution,
                                     boolean isCapstone) {
            
            this.spsId = spsId;
            this.subName = subName;
            this.subId = subId;
            this.cName = cName;
            this.status = status;
            this.result = result;
            this.score = score;
            this.merit = merit;
            this.isPaid = isPaid;
            this.spId = spId;
            this.isEnrolEnabled = isEnrolEnabled;
            this.enrolMode = enrolMode;
            this.actionUrl = actionUrl;
            this.resolution = resolution;
            this.isCapstone = isCapstone;
        }
        
    }