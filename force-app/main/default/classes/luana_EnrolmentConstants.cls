/*
    Change History:
    LCA-1169 06/02/2018 - WDCi Lean: add new custom setting keys for india and nepal program
    PAN:5340 11/10/2018 - WDCi Lean: add new custom setting key for flexipath
*/

public with sharing class luana_EnrolmentConstants {
    
    public static final Integer ACTION_MODE_ENROL_ON = 0;
    public static final Integer ACTION_MODE_ENROL_OFF = 1;
    public static final Integer ACTION_MODE_VIEW = 2;  
    
    public static final String RECORDTYPE_STUDENTPROGRAM_ACCREDITEDMODULE = 'Accredited_Module';
    public static final String RECORDTYPE_STUDENTPROGRAM_ACCREDITEDPROGRAM = 'Accredited_Program';
    public static final String RECORDTYPE_STUDENTPROGRAM_FOUNDATION = 'Foundation';
    public static final String RECORDTYPE_STUDENTPROGRAM_MASTERCLASS = 'Masterclass';
    public static final String RECORDTYPE_STUDENTPROGRAM_CASM = 'CASM';
    public static final String RECORDTYPE_STUDENTPROGRAM_LLL = 'Lifelong_Learning';
    
    public static final String RECORDTYPE_PROGRAMOFFERING_ACCREDITEDMODULE = 'Accredited_Module';
    public static final String RECORDTYPE_PROGRAMOFFERING_ACCREDITEDPROGRAM = 'Accredited_Program';
    public static final String RECORDTYPE_PROGRAMOFFERING_FOUNDATION = 'Foundation';
    public static final String RECORDTYPE_PROGRAMOFFERING_MASTERCLASS = 'Masterclass';
    public static final String RECORDTYPE_PROGRAMOFFERING_CASM = 'CASM';
    public static final String RECORDTYPE_PROGRAMOFFERING_LLL = 'Lifelong_Learning';
    //public static final String RECORDTYPE_PROGRAMOFFERING_NONACCREDITEDPROGRAM = 'Non-Accredited Program';
    
    public static final String STUDENTPROGRAM_STATUS_INPROGRESS = 'In Progress';
    public static final String STUDENTPROGRAM_STATUS_FAIL = 'Fail';
    // LCA-516 Phase 3
    public static final String STUDENTPROGRAM_STATUS_PENDING = 'Pending';
    public static final String STUDENTPROGRAM_STATUS_Unpaid = 'Unpaid';
    
    public static final String STUDENTPROGRAM_DUPLICATE_CHECK_ERROR = 'The student has already enrolled to the same course. Please uncheck "Enforce Duplicate Enrolment Check" if you still would like to proceed.';
    
    //public static final String PRICEBOOK_OTHER = 'Pricebook_Other';
    //public static final String CURRENCY_OTHER = 'Currency_Other';
    
    public static final String SUBJECT_CAP = 'Capstone';
    public static final Decimal SUBJECT_PASSING_SCORE = 50;
    
    public static final String PAYMENT_GATEWAY_URL_KEY = 'Payment Gateway URL';
    
    public static final String PRODUCTTYPE_CAPROGRAM = 'CA Program';
    public static final String PRODUCTTYPE_CAMODULE = 'CA Module';
    public static final String PRODUCTTYPE_MASTERCLASS = 'Masterclass';
    public static final String PRODUCTTYPE_PPP = 'PPP';
    public static final String PRODUCTTYPE_LLL = 'LLL';
    public static final String PRODUCTTYPE_FOUNDATION = 'Foundation';
    public static final String PRODUCTTYPE_NONACCREDITEDPROGRAM = 'Non Accredited Program';
    
    public static final Map<String, String> productTypeToSPType = new Map<String, String>();
    static{
        productTypeToSPType.put(PRODUCTTYPE_CAPROGRAM.toLowerCase(), RECORDTYPE_STUDENTPROGRAM_ACCREDITEDPROGRAM);
        productTypeToSPType.put(PRODUCTTYPE_CAMODULE.toLowerCase(), RECORDTYPE_STUDENTPROGRAM_ACCREDITEDMODULE);
        productTypeToSPType.put(PRODUCTTYPE_MASTERCLASS.toLowerCase(), RECORDTYPE_STUDENTPROGRAM_MASTERCLASS);
        //productTypeToSPType.put(PRODUCTTYPE_PPP.toLowerCase(), RECORDTYPE_STUDENTPROGRAM_ACCREDITEDPROGRAM);
        productTypeToSPType.put(PRODUCTTYPE_FOUNDATION.toLowerCase(), RECORDTYPE_STUDENTPROGRAM_FOUNDATION);
        //productTypeToSPType.put(PRODUCTTYPE_NONACCREDITEDPROGRAM.toLowerCase(), RECORDTYPE_STUDENTPROGRAM_ACCREDITEDPROGRAM);
    }
    
    public static final String EMPLOYMENT_HISTORY_STATUS_CURRENT = 'Current';
    
    //LCA-1169
    public static final String CS_DEFAULT_CA_PROGRAM_COURSE_ID = 'Default_CA_Program_Course_Id';
    public static final String CS_DEFAULT_ICA_INDIA_PROGRAM_COURSE_ID = 'Default_ICA_INDIA_Program_Course_Id';
    public static final String CS_DEFAULT_ICA_NEPAL_PROGRAM_COURSE_ID = 'Default_ICA_NEPAL_Program_Course_Id';
    public static final String CS_DEFAULT_ICA_PAKISTAN_PROGRAM_COURSE_ID = 'Default_ICA_PAKISTAN_Program_Course_Id';
    public static final String CS_DEFAULT_ICA_SRILANKA_PROGRAM_COURSE_ID = 'Default_ICA_SRILANKA_Program_Course_Id';
    
    //PAN:5340
    public static final String CS_FLEXIPATH_CUTOFF_DATE = 'Flexipath_Cutoff_Date';
    
    public static final String COMPETENCY_AREA_TYPE_REQUIRES = 'Requires';
    public static final String COMPETENCY_AREA_TYPE_GAINS = 'Gains';
}