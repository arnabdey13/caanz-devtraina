/*
    Developer: WDCi (Carter)
    Date: 07/June/2016
    Task #: Test class for luana_VCImportWizardController
*/
@isTest(seeAllData=false)
private class luana_AT_VCImportWizardController_Test {
    
    final static String contextLongName = 'luana_VCImportWizardController_Test';
    final static String contextShortName = 'lvciwct';
    
    public static LuanaSMS__Course__c course;
    
    static void setup(){
        Luana_DataPrep_Test dataPrep = new Luana_DataPrep_Test();
        
        List<Luana_Extension_Settings__c> luanaExtensionConfig = dataPrep.prepLuanaExtensionSettingCustomSettings();
        luanaExtensionConfig.add(new Luana_Extension_Settings__c(Name='AdobeConnect_URL', Value__c = 'https://connect-staging.charteredaccountantsanz.com')); 
        insert luanaExtensionConfig;
        
        LuanaSMS__Training_Organisation__c trainingOrg = dataPrep.createNewTraningOrg(contextLongName, contextLongName, contextShortName, 'Virtual Street 123', 'Queensland', '4551');
        insert trainingOrg;
        LuanaSMS__Program__c program = dataPrep.createNewProgram(contextShortName, contextLongName, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        insert program;
        LuanaSMS__Program_Offering__c po = dataPrep.createNewProgOffering('po' + contextShortName, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), program.Id, trainingOrg.Id, null, null, null, 1, 1);
        insert po;
        course = dataPrep.createNewCourse('Graduate Diploma of Virtual Accounting_' + contextShortName, po.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Program'), 'Running');
        course.LuanaSMS__Allow_Online_Enrolment__c = true;
        insert course;

    }
    
    /*
        Successfully logging in
    */
    static testMethod void testSuccessLogin() {
        
        setup();
        
        

        //Create and link mock http class
        Test.setMock(HttpCalloutMock.class, new luana_AT_VCImportWizardMockHttp_Test());

        
        //Main Test Area
        Test.startTest();
        
        
       
        PageReference pageRef = Page.luana_VCImportWizard;
        pageRef.getParameters().put('id', course.id);
        
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController lvci_sc = new ApexPages.StandardController(course);
        luana_VCImportWizardController lvci_e = new luana_VCImportWizardController(lvci_sc);
        
        lvci_e.username = 'test@correct.com';
        lvci_e.password = 'abcde';
        lvci_e.sessionCookie= 'breezhupgyv7r4ibs4tbs';
        lvci_e.filter = 'test';
        lvci_e.getHasRequiredParams();        
        lvci_e.goToVCImportSummary();
        lvci_e.back();
        for(luana_VCImportWizardController.SessWrapper sessWrapper : lvci_e.sessWrapperList) {
            sessWrapper.sess.Registration_Start_Date__c = System.today() - 2;
            sessWrapper.sess.Registration_End_Date__c = System.today() + 2;
        }
        lvci_e.goToVCUpsert();
        
        Test.stopTest();
        
        
    }
}