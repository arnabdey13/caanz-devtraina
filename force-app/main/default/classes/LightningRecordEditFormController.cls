public with sharing class LightningRecordEditFormController {
    
    
    @AuraEnabled
    public static List<LayoutSection> fieldsRelatedtoPageLayout(string objectName,string pageLayoutName) {
        Set<string> accessibleFields = new Set<string>();        
        SObjectType sObjectType = Schema.getGlobalDescribe().get(objectName);
        for (SObjectField field : sObjectType.getDescribe().fields.getMap().values()){
            if (field.getDescribe().isAccessible())
                accessibleFields.add(string.valueof(field));
        } 
        List<LayoutSection> lstSections = new List<LayoutSection>();
        
        try {
            List<String> componentNameList = new List<String>{pageLayoutName};
                List<Metadata.Metadata> components = Metadata.Operations.retrieve(Metadata.MetadataType.Layout, componentNameList);
            Metadata.Layout contLayout = (Metadata.Layout) components.get(0);
            for( Metadata.LayoutSection ls : contLayout.layoutSections ) {  
                LayoutSection section = new LayoutSection( ls.label, ls.layoutColumns.size() );
                List<LayoutColumn> lstColumns = new List<LayoutColumn>();
                Integer maxFieldsInColumn = 0;
                for( Metadata.LayoutColumn lc : ls.layoutColumns ) {                    
                    LayoutColumn column = new LayoutColumn();
                    if( lc.layoutItems != null ) { 
                        if( maxFieldsInColumn < lc.layoutItems.size() ) {
                            maxFieldsInColumn = lc.layoutItems.size();
                        }
                        for( Metadata.LayoutItem li : lc.layoutItems ) {   
                            if(accessibleFields.contains(string.valueof(li.field)) )
                                column.lstFields.add( new LayoutField( li ) );
                        }
                    }
                    if( column.lstFields.size() > 0 ) {
                        lstColumns.add( column );
                    }
                }
                if( maxFieldsInColumn > 0 ) {
                    for( Integer i = 0; i < maxFieldsInColumn; i++ ) {
                        for( Integer j = 0; j < lstColumns.size(); j++ ){
                            if( lstColumns[j].lstFields.size() > i ) {
                                if(accessibleFields.contains(string.valueof(lstColumns[j].lstFields[i].fieldName)) )
                                    section.lstFields.add( lstColumns[j].lstFields[i] );    
                            }    
                            else {
                             //   section.lstFields.add( new LayoutField() );
                            }
                        }    
                    }    
                }                
                lstSections.add( section );
            }
        }
        catch( Exception e ){
            System.debug( e.getLineNumber() + ' ===== ' + e.getMessage() );
        }
        return lstSections;
    }
    
    public class LayoutSection {   
        @AuraEnabled public String label;
        @AuraEnabled public List<LayoutField> lstFields;
        @AuraEnabled public Integer totalColumns;
        public LayoutSection( String label, Integer totalColumns ) {
            this.label = label;
            this.totalColumns = totalColumns;
            this.lstFields = new List<LayoutField>();
        }
    }
    
    private class LayoutColumn {
        private List<LayoutField> lstFields;    
        public LayoutColumn() {
            this.lstFields = new List<LayoutField>();
        }
    }
    
    public class LayoutField {
        @AuraEnabled public String fieldName;
        @AuraEnabled public Boolean isRequired;
        @AuraEnabled public Boolean isReadOnly;        
        public LayoutField() {}        
        public LayoutField( Metadata.LayoutItem li ) {            
            this.fieldName = li.field;
            if(li.field == 'RecordTypeId' || li.field == 'OwnerId' ){this.isReadOnly = true;}
            else if( li.behavior == Metadata.UiBehavior.Required ) {this.isRequired = true;}
            else if( li.behavior == Metadata.UiBehavior.ReadOnly ) {this.isReadOnly = true;}    
        }
    }
}