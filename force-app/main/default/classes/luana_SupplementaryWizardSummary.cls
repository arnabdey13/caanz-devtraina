/*
    Developer: WDCi (KH)
    Development Date: 01/08/2016
    Task #: Supplementary wizard - summary controller
     
*/
public with sharing class luana_SupplementaryWizardSummary {
    
    luana_SupplementaryWizardController stdController;
    
    public luana_SupplementaryWizardSummary(luana_SupplementaryWizardController stdController) {
        this.stdController = stdController;
    }
    
    public PageReference doBack(){
        
        PageReference backSPPage = Page.luana_SupplementaryWizardDetail;
        backSPPage.getParameters().put('spid',stdController.selectedSP.Id);
        
        return backSPPage;
    }
    
    public PageReference doCheckout(){
        Savepoint sp = Database.setSavepoint();
        try{

            stdController.updateCurrentSP();
        
            stdController.createNewOrder();
            
        }Catch(Exception exp){
            Database.rollback(sp);
        }
        return null;
        
    }
    
    public PageReference doSyncNetsuiteInvoice(){
    
        Order insertedOrder = [select id,OrderNumber, OwnerId, 
            Account.Salutation, Account.FirstName, Account.LastName, Account.PersonEmail, Account.PersonMailingStreet, Account.PersonMailingCity, Account.PersonMailingState, Account.PersonMailingPostalCode, Account.PersonMailingCountry, 
            Account.Customer_ID__c, Account.Member_ID__c, Account.Member_Of__c, Account.Membership_Type__c, Account.Mailing_Company_Name__c, Account.Affiliated_Branch_Country__c,
            (select id, Quantity, Netsuite_Product_ID__c, Netsuite_Pricing_Level__c, Start_Date__c, End_Date__c, Netsuite_Event_Code__c from OrderItems)
        from Order where Id =: stdController.newOrder.Id];
        
        stdController.orderNumber = insertedOrder.OrderNumber;
        
        String body = prepareOrderRequestBody(insertedOrder);
        
        Dom.Document doc = new Dom.Document();
        doc.load(body);
        HttpResponse response = new HttpResponse();
        if(Test.isRunningTest()){
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"foo":"bar"}');
            response.setStatusCode(200);
        }else{
            response = BoomiMockCallOut.boomiCallout(doc);
        }     
       
        if(response.getStatusCode() == 200){
            
            if(null != [select NetSuite_Order_ID__c from Order where id =: stdController.newOrder.Id].NetSuite_Order_ID__c){
                stdController.newOrder.Status = 'Activated';
                update stdController.newOrder;
                
                stdController.paymentProcessed = true;
                
                PageReference viewSPPage = Page.luana_SupplementaryWizardComplete;
                return viewSPPage;
            } else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Fail to process order (Order Number: ' + insertedOrder.OrderNumber + '). Please contact our support.'));  
            }
        } else {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Fail to process order (Order Number: ' + insertedOrder.OrderNumber + '). Please contact our support.')); 
        }
    
        PageReference viewSPPage = Page.luana_SupplementaryWizardComplete;
        return viewSPPage;
    }
        
    private String prepareOrderRequestBody(Order ord){
        
        String body = '';
        
        body = '<Order>';
        
            body += createElement('OrderNumber', ord.OrderNumber);
            body += createElement('Owner', ord.OwnerId);
            body += '<Account>';
                body += createElement('Salutation', ord.Account.Salutation);
                body += createElement('FirstName', ord.Account.FirstName);
                body += createElement('LastName', ord.Account.LastName);
                body += createElement('PersonEmail', ord.Account.PersonEmail);
                body += createElement('PersonMailingStreet', ord.Account.PersonMailingStreet);
                body += createElement('PersonMailingCity', ord.Account.PersonMailingCity);
                body += createElement('PersonMailingState', ord.Account.PersonMailingState);
                body += createElement('PersonMailingPostalCode', ord.Account.PersonMailingPostalCode);
                body += createElement('PersonMailingCountry', ord.Account.PersonMailingCountry);
                body += createElement('Customer_ID__c', ord.Account.Customer_ID__c);
                body += createElement('Member_ID__c', ord.Account.Member_ID__c);
                body += createElement('Member_Of__c', ord.Account.Member_Of__c);
                body += createElement('Membership_Type__c', ord.Account.Membership_Type__c);
                body += createElement('Mailing_Company_Name__c', ord.Account.Mailing_Company_Name__c);
                body += createElement('Affiliated_Branch_Country__c', ord.Account.Affiliated_Branch_Country__c);
            body += '</Account>';
            
            for(OrderItem oi : ord.OrderItems){
                body += '<OrderProduct>';
                    body += createElement('Quantity', String.valueOf(oi.Quantity));
                    body += createElement('NS_Internal_ID__c', oi.Netsuite_Product_ID__c);
                    body += createElement('NetSuite_Pricing_Level__c', oi.Netsuite_Pricing_Level__c);
                    body += createElement('Start_Date__c', String.valueOf(oi.Start_Date__c));
                    body += createElement('End_Date__c', String.valueOf(oi.End_Date__c));
                    body += createElement('Netsuite_Event_Code__c', oi.Netsuite_Event_Code__c);
                body += '</OrderProduct>';
            }
            
        body += '</Order>';
        
        return body;
    }
    
    private String createElement(String fieldName, String value){
        if(value != null){
            value = value.replace('&', '&amp;');
            value = value.replace('"', '&quot;');
            value = value.replace('\'', '&apos;');
            value = value.replace('<', '&lt;');
            value = value.replace('>', '&gt;');
            
            return '<' + fieldName + '>' + value + '</' + fieldName + '>';
        } else {
            return '<' + fieldName + '/>';
        }
    }
}