/**
    Developer: WDCi (CM)
    Development Date: 24/08/2016
    Task: Luana Test class for luana_BulkPaymentWizardController
**/
@isTest(seeAllData=false)
private class luana_AT_BulkPaymentWizard_Test {
    
    private static Luana_DataPrep_Test dataPrep;
    private static Account businessAccount;
    private static List<Account> memberAccounts;
    private static List<Contact> contactList;
    private static LuanaSMS__Course__c course;
    private static Employment_Token__c eToken1;
    private static Employment_Token__c eToken2;
    private static List<Product2> prodList;
    
    private static String classNamePrefixLong = 'luana_BulkPaymentWizard_Test';
    private static String classNamePrefixShort = 'lbpwt'; //Also used as the employer Token

    //Used to initialise data
    //Country is either Australia, New Zealand, Overseas
    private static void setup(String country){
        
        dataPrep = new Luana_DataPrep_Test();
        
        insert dataPrep.prepLuanaExtensionSettingCustomSettings();
        insert dataPrep.createLuanaConfigurationCustomSetting();
        
        //Create country's business account
        businessAccount = dataPrep.generateNewBusinessAcc(classNamePrefixLong + 'Test Account', 'Business_Account', 'Chartered Accounting', classNamePrefixShort, null, null, 'Test billing street', 'Active');
        businessAccount.billingCity = 'city';
        businessAccount.billingPostalCode = '1234';
        if (country != 'Overseas'){
            businessAccount.billingCountry = country;
        } else {
            businessAccount.billingCountry = 'Malaysia';
        }
        
        businessAccount.Affiliated_Branch_Country__c = country;
        insert businessAccount;
        
        //Might need to create employment token manually
        eToken1 = dataPrep.createEmplToken(classNamePrefixLong + '1', businessAccount.Id, classNamePrefixShort + '1', classNamePrefixShort, system.today().addDays(2), true);
        eToken2 = dataPrep.createEmplToken(classNamePrefixLong + '2', businessAccount.Id, classNamePrefixShort + '2', classNamePrefixShort, system.today().addDays(2), true);
        eToken2.Discounted_Price__c = 1000;
        eToken2.Cancellation_Fee__c = 500;
        List<Employment_Token__c> eTokenList = new List<Employment_Token__c>();
        eTokenList.add(eToken1);
        eTokenList.add(eToken2);
        insert eTokenList;
        
        //Create traning org
        LuanaSMS__Training_Organisation__c trainingOrg = dataPrep.createNewTraningOrg(classNamePrefixLong, classNamePrefixLong, classNamePrefixShort, 'St 123', 'Wollonggong', '2244');
        insert trainingOrg;
        
        //Create program
        LuanaSMS__Program__c progrm = dataPrep.createNewProgram(classNamePrefixShort, classNamePrefixLong, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        insert progrm;
        
        //Create Product
        prodList = new List<Product2>();
        prodList.add(dataPrep.createNewProduct('FIN_AU_' + classNamePrefixLong, 'AU0001'));
        prodList.add(dataPrep.createNewProduct('FIN_NZ_' + classNamePrefixLong, 'NZ0001'));
        prodList.add(dataPrep.createNewProduct('FIN_INT_' + classNamePrefixLong, 'INT0001'));
        insert prodList;
        
        //Create Program Offering
        LuanaSMS__Program_Offering__c poAM = dataPrep.createNewProgOffering('PO_AM_' + classNamePrefixLong, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), progrm.Id, trainingOrg.Id, prodList[0].Id,prodList[1].Id, prodList[2].Id, 1, 1);
        insert poAM;
        
        //Create course
        course = dataPrep.createNewCourse('Graduate Diploma of Chartered Wizardry', poAM.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        insert course;
        
        //Create students
        memberAccounts = new List<Account>();
        for(integer i = 0; i < 4; i ++){
            Account mAccount = dataPrep.generateNewApplicantAcc('Joe_' + classNamePrefixShort + '_' + i, classNamePrefixLong, 'Full_Member');
            mAccount.PersonEmail = classNamePrefixLong + i + '@example.com';
            mAccount.Member_Id__c = '1234' + i;
            mAccount.Affiliated_Branch_Country__c = 'Australia';
            mAccount.Membership_Class__c = 'Full';
            mAccount.Communication_Preference__c= 'Home Phone';
            mAccount.PersonHomePhone= '1234';
            mAccount.PersonOtherStreet= '83 Saggers Road';
            mAccount.PersonOtherCity='JITARNING';
            mAccount.PersonOtherState='Western Australia';
            mAccount.PersonOtherCountry='Australia';
            mAccount.PersonOtherPostalCode='6365'; 
            memberAccounts.add(mAccount);
        }
        insert memberAccounts;
        Set<Id> accountIds = new Set<Id>();
        for (Account a : memberAccounts){
            accountIds.add(a.id);
        }
        contactList = [SELECT Id, AccountId FROM Contact WHERE AccountId IN : accountIds];
        
        //Create payment tokens
        List<Payment_Token__c> pTokenList = new List<Payment_Token__c>();
        Integer counter = 0;
        for (Contact c : contactList){
            Payment_Token__c pToken = dataPrep.createNewPaymentToken(businessAccount.id, course.id, system.today().addDays(2), false);
            pToken.Date_Used__c = system.today();
            //2 with basic token
            if (counter < 2){
                pToken.Employment_Token__c = eToken1.id;
            //2 with discounted and cancellation fee token
            } else {
                pToken.Employment_Token__c = eToken2.id;
            }
            pTokenList.add(pToken);
            counter++;
        }
        insert pTokenList;
        
        //Insert Student Programs
        counter = 0;
        List<LuanaSMS__Student_Program__c> spList = new List<LuanaSMS__Student_Program__c>();
        for(Contact c : contactList){
            if (counter < 2 ){
                LuanaSMS__Student_Program__c sp = dataPrep.createNewStudentProgram(dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), c.id, course.Id, country, 'In Progress');
                spList.add(sp); 
            } else {
                LuanaSMS__Student_Program__c sp = dataPrep.createNewStudentProgram(dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), c.id, course.Id, country, 'Completed');
                spList.add(sp);
            }
            
            counter++;
        }
        insert spList;
    }

/* Commented out by CM: Main tests moved to luana_AT_MasterEnrolmentWizard_Test because of pricebook entries.
    
    private static void runTest(){
        
        //VF page
        PageReference pageRef = Page.luana_BulkPaymentWizard;
        pageRef.getParameters().put('id', businessAccount.id);
        
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController lbpw_sc = new ApexPages.StandardController(businessAccount);
        luana_BulkPaymentWizardController lbpwc = new luana_BulkPaymentWizardController(lbpw_sc);
        //should do init() automatically
        //Search for all student programs
        lbpwc.filterObj.Date_Used__c = system.today();
        lbpwc.filterObj.Expiry_Date__c = system.today().addDays(5);
        
        lbpwc.doSearch();
        //Select a student program
        List<luana_BulkPaymentWizardController.PaymentTokenWrapper> pTokenWrapperList = lbpwc.getCurrentPagePaymentTokens();
        for (luana_BulkPaymentWizardController.PaymentTokenWrapper pwToken : pTokenWrapperList){
            pwToken.isSelected = true;  
        }
        
        
        //Create order
        lbpwc.doSave(); 
        
        //Test Cancelled programs
        LuanaSMS__Student_Program__c sp = dataPrep.createNewStudentProgram(dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), c.id, course.Id, country, 'Cancelled');
        sp.Withdrawal_Cancellation_Date__c = system.today();
        insert sp;
        Test.setCurrentPage(pageRef);
        lbpwc.filterObj.Date_Used__c = system.today();
        lbpwc.filterObj.Expiry_Date__c = system.today().addDays(5);
        
        lbpwc.Apply_Cancellation_Fee__c = true;
        lbpwc.doSearch();
        
    }
    */
    
    //Test assorted functions (code coverage)
    private static testmethod void testButtons(){
        
        setup('Australia');
        
        
        //Setup VF Page
        PageReference pageRef = Page.luana_BulkPaymentWizard;
        pageRef.getParameters().put('id', businessAccount.id);
        
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController lbpw_sc = new ApexPages.StandardController(businessAccount);
        luana_BulkPaymentWizardController lbpwc = new luana_BulkPaymentWizardController(lbpw_sc);
        lbpwc.filterObj.Date_Used__c = system.today();
        lbpwc.filterObj.Expiry_Date__c = system.today().addDays(5);
        lbpwc.doSearch();
        
        
        Boolean checkBool;
        Integer checkInt;
        
        test.startTest();
        
        checkBool = lbpwc.hasNext;
        checkBool = lbpwc.hasPrevious;
        checkInt = lbpwc.pageNumber;
        lbpwc.first();
        lbpwc.last();
        lbpwc.previous();
        lbpwc.next();
        
        lbpwc.doSave();
        
        test.stopTest();
        
    }
    
}