/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AccountTrigger_test {
    @testSetup static void createTestData() {
        test_insertTestData();
    }
    static void test_insertTestData(){  
        List<Account> accs = new List<Account>();       
        Account acc = TestObjectCreator.createPersonAccount();
        acc.Membership_Type__c = 'Member';
        acc.Membership_Class__c = 'affiliate'; 
        acc.Communication_Preference__c= 'Home Phone';
acc.PersonHomePhone= '1234';
acc.PersonOtherStreet= '83 Saggers Road';
acc.PersonOtherCity='JITARNING';
acc.PersonOtherState='Western Australia';
acc.PersonOtherCountry='Australia';
acc.PersonOtherPostalCode='6365';         
        accs.add(acc);
       // Account acc1 = TestObjectCreator.createFullMemberAccount();
       // accs.add(acc1);
        insert accs;            
    }



    private static Account getFullMemberAccountObject() {
        Account FullMemberAccountObject = TestObjectCreator.createFullMemberAccount();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        return FullMemberAccountObject;
    }
    
   
    //******************************************************************************************
    //                             TestMethods
    //******************************************************************************************
   
    static testmethod User UserGuestAccountObject() {
        User UserAccountObject = TestObjectCreator.createMemberPortalSiteGuestUser();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        return UserAccountObject;      
    }
        
    static testmethod Account PersonAccountObject() {
        Account PersonAccountObject = TestObjectCreator.createPersonAccount();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        return PersonAccountObject;      
    }
    
    static testmethod Lead LeadAccountObject() {
        Lead LeadAccountObject = TestObjectCreator.createLead();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        return LeadAccountObject;      
    }
          
    static testmethod Account getNonMemberAccountObject() {
        Account NonMemberAccountObject = TestObjectCreator.createNonMemberAccount();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        return NonMemberAccountObject;      
    }
  
    
    static testmethod Account getStudMemberAccountObject() {
        Account StudMemberAccountObject = TestObjectCreator.createStudentAffiliateAccount();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        return StudMemberAccountObject;      
    }
        
    static testMethod void AccountTrigger_testInsertAccount() {
        Test.startTest();
        Profile profile1 = [Select Id from Profile where name = 'CAANZ Business Management User'];
        System.debug('What is the profile id ' + profile1);
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        User u = new User( UserRoleId = portalRole.Id,
                            ProfileId = profile1.Id,
                            Username = 'testtermsconditions1234423@charteredaccountantsanz.com',
                            Alias = 'batman',
                            Email='testtermsconditions1234423@charteredaccountantsanz.com',
                            EmailEncodingKey='UTF-8',
                            Firstname='Bruce',
                            Lastname='Wayne',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_NZ',
                            TimeZoneSidKey='Pacific/Auckland');
        insert u; 
        system.runas(u){
            List<Account> accs=new List<Account>();  
            accs = [select id,Membership_Type__c,Membership_Class__c from Account ];
            Integer i=0;
            for(Account a:accs){
                a.PersonEmail = 'testemailupdate'+i+'@example.com'; 
                i++;           
            }
            update accs;
            
    
            // Check Results
          /*  Id FullMemberContactId = [Select id from Contact where AccountId = :FullMemberAccountObject.Id].Id;
            List<User> User_List = [Select FederationIdentifier from User where contactId = :FullMemberContactId];
            System.assertEquals(1, User_List.size(), 'User_List.size');
            System.assertNotEquals(null, User_List[0].FederationIdentifier, 'FederationIdentifier');*/
        }
        Test.stopTest();
    }

    static testMethod void AccountTrigger_stopMemberUpdate() {
        List<Account> accs = new List<Account>();
        Map<Id, Account> oldAccountsByIds = new Map<Id, Account>();
        AccountTriggerClass.stopMemberOfUpdate(accs, oldAccountsByIds);
    }
   
    static testMethod void AccountTrigger_updateMemberProfile() {
        
        Test.startTest();
        Profile profile1 = [Select Id from Profile where name = 'CAANZ Business Management User'];
        System.debug('What is the profile id ' + profile1);
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        User u = new User( UserRoleId = portalRole.Id,
                            ProfileId = profile1.Id,
                            Username = 'testtermsconditions1234423@charteredaccountantsanz.com',
                            Alias = 'batman',
                            Email='testtermsconditions1234423@charteredaccountantsanz.com',
                            EmailEncodingKey='UTF-8',
                            Firstname='Bruce',
                            Lastname='Wayne',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_NZ',
                            TimeZoneSidKey='Pacific/Auckland');
        insert u; 
        //run as user to test code to perform user level testing                         
        System.runas(u){  
         List<Account> accs=new List<Account>();  
         Account a = [select id,Membership_Type__c,Membership_Class__c from Account limit 1];          
         a.Membership_Class__c = 'Provisional';        
         update a;  
         accs.add(a);
         Map<Id, Account> oldAccountsByIds = new Map<Id, Account>();
        oldAccountsByIds.put(a.id,a); 
        AccountTriggerClass.updateMemberProfile(accs, oldAccountsByIds);
        }
        Test.stopTest();
    }
    
     /*static testMethod void AccountTrigger_updateUserRecords() {
         
         Set<Id> acctId = new Set<Id>();        
         //TestObjectCreator.testInsertPersonAccountPortalUserCAANZ();
         Account acc = TestObjectCreator.createPersonAccount();
         acc.Membership_Type__c = 'Member';
         acc.Membership_Class__c = 'affiliate'; 
         
         insert acc;     
         
         acctId.add(acc.Id);                      
         AccountTriggerClass.updateUserRecords(acctId);
        
    }*/
}