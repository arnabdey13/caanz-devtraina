/*------------------------------------------------------------------------------------
Author:        Mayukhman
Company:       Tech Mahindra
Description:   Test class for most of the Controllers - TaskTriggerHandler & EmailMessageTriggerHandler 

History
Date            Author             Comments
--------------------------------------------------------------------------------------
24-07-2019     Mayukhman         Initial Release
------------------------------------------------------------------------------------*/
@isTest 
public class TaskTriggerHandler_Test {
    private static Account currentAccount;
    private static Case currentCase;
    private static Case currentCase2;
    private static Task currentTask;
    private static EmailMessage currentEmailMessage;
    static {
        currentAccount = TaskTriggerHandler_Test.createFullMemberAccount();
        insert currentAccount;
        currentCase = TaskTriggerHandler_Test.createCase();
        insert currentCase;
        currentCase2 = TaskTriggerHandler_Test.createCase();
        insert currentCase2;
        currentTask = TaskTriggerHandler_Test.createTask();
        insert currentTask;
        currentEmailMessage = TaskTriggerHandler_Test.createEmailMessage();
        insert currentEmailMessage;
    }
    
    
    // <Create FullMemberAccount>
    public static Account createFullMemberAccount(){
        Account FullMemberAccountObject = new Account();
        FullMemberAccountObject.Salutation = 'Mr.';
        FullMemberAccountObject.FirstName = 'TestFName';
        FullMemberAccountObject.PersonEmail= 'Full_Member'+ CaseTriggerHandler_Test.getRandomNumber(910) +'@gmail.com';
        FullMemberAccountObject.LastName = 'Test'+ CaseTriggerHandler_Test.getRandomNumber(910);
        FullMemberAccountObject.RecordTypeId = RecordTypeCache.getId(FullMemberAccountObject, 'Full_Member');
        FullMemberAccountObject.Communication_Preference__c= 'Home Phone';
        FullMemberAccountObject.PersonHomePhone= '1234';
        FullMemberAccountObject.PersonOtherStreet= '83 Saggers Road';
        FullMemberAccountObject.PersonOtherCity='JITARNING';
        FullMemberAccountObject.PersonOtherState='Western Australia';
        FullMemberAccountObject.PersonOtherCountry='Australia';
        FullMemberAccountObject.PersonOtherPostalCode='6365';  
        return FullMemberAccountObject;
    }
    public static Case createCase(){
        Id recordTypeID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case').getRecordTypeId();
        Case CAANZMSTCase = new Case();
        CAANZMSTCase.AccountId = currentAccount.Id;
        CAANZMSTCase.Subject = 'Test';
        CAANZMSTCase.Type = 'Complaints';
        CAANZMSTCase.Sub_Type__c = 'About CA ANZ';
        CAANZMSTCase.RecordTypeId = recordTypeID;
        return CAANZMSTCase;
    }
    public static Task createTask(){
        Task t = new Task();
        t.Subject='Test';
        t.Priority='Normal';
        t.WhatId = currentCase.Id;
        t.Status = 'In Progress';
        t.TaskSubtype = 'Call';
        return t; 
    }
    public static EmailMessage createEmailMessage(){
        EmailMessage email = new EmailMessage();
        email.FromAddress = 'test@abc.org';
        email.Incoming = True;
        email.ToAddress= 'test@xyz.org';
        email.Subject = 'Test email';
        email.HtmlBody = 'Test email body';
        email.ParentId = currentCase2.Id; 
        return email;
    }
    public static Integer getRandomNumber(Integer size){
        Double d = math.random() * size;
        return d.intValue();
    }
    
    @isTest static void test_taskTriggerHandler() {
    }
}