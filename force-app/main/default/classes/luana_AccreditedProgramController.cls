/*
    Developer: WDCi (Lean)
    Date: 22/Mar/2016
    Task #: Luana implementation
    
    Change History:
    LCA-615 08/06/2016: WDCi Lean - this is obsolete
*/

public class luana_AccreditedProgramController {
	
	/*LCA-615
    LuanaSMS__Program__c program;
    public List<LuanaSMS__Program_Offering__c> accreditedProgList {set; get;}
    public List<LuanaSMS__Program_Offering__c> accreditedModList {set; get;}

    public luana_AccreditedProgramController (ApexPages.StandardController controller) {
        
        if(Test.isRunningTest()){
            program = (LuanaSMS__Program__c) controller.getRecord();
            program = [select id, name from LuanaSMS__Program__c where Id =: program.Id];
        } else {
            controller.addFields(new List<String>{'Name'});
            program = (LuanaSMS__Program__c) controller.getRecord();
        }
        
        
        accreditedProgList = [SELECT Id, Name, LuanaSMS__Number_of_Required_Electives__c FROM LuanaSMS__Program_Offering__c WHERE LuanaSMS__Program__c=: program.Id AND RecordType.Name='Accredited Program'];
        accreditedModList = [SELECT Id, Name FROM LuanaSMS__Program_Offering__c WHERE LuanaSMS__Program__c=: program.Id AND RecordType.Name='Accredited Module'];
        
    }

    public PageReference doNewPO(){
        
        String targetURL = '';
        String encoded = '';
        PageReference pageRef;
        
        if(UserInfo.getOrganizationId().startsWith('00Dp00000000k6x')){
            //prepod
            targetURL = '/a1o/e?retURL=/' + program.Id + '&CF00Np0000000LPmh=' + program.Name + '&CF00Np0000000LPmh_lkid=' + program.Id;
            encoded = EncodingUtil.urlEncode(targetURL, 'UTF-8');
            pageRef = new PageReference('/setup/ui/recordtypeselect.jsp?&ent=01Ip00000004Ofo&retURL=' + program.Id + '&save_new_url=' + encoded);
            
        } else if(UserInfo.getOrganizationId().startsWith('00Dp00000000jgK')){
            //luanadev
            targetURL = '/a1o/e?retURL=/' + program.Id + '&CF00Np0000000LAam=' + program.Name + '&CF00Np0000000LAam_lkid=' + program.Id;
            encoded = EncodingUtil.urlEncode(targetURL, 'UTF-8');
            pageRef = new PageReference('/setup/ui/recordtypeselect.jsp?&ent=01Ip00000004NoB&retURL=' + program.Id + '&save_new_url=' + encoded);
            
        } else if(UserInfo.getOrganizationId().startsWith('00D90000000nGH8')){
            //production
            targetURL = '/a1o/e?retURL=/' + program.Id + '&CF00N9000000EIkOq=' + program.Name + '&CF00N9000000EIkOq_lkid=' + program.Id;
            encoded = EncodingUtil.urlEncode(targetURL, 'UTF-8');
            pageRef = new PageReference('/setup/ui/recordtypeselect.jsp?&ent=01I90000001hY2y&retURL=' + program.Id + '&save_new_url=' + encoded);
        }
        
        return pageRef;
    }
    LCA-615*/
}