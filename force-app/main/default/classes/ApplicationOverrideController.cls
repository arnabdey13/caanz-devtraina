public with sharing class ApplicationOverrideController {
	Application__c Application;
	
	public ApplicationOverrideController(){}
	public ApplicationOverrideController(ApexPages.StandardController controller) {
		Application = (Application__c) controller.getRecord();
	}
	
	public PageReference gotoTabPage(){
		if(UserInfo.getProfileId()==ProfileCache.getId('NZICA Community Login User') ){
			User CommunityUser = [Select u.AccountId From User u where u.Id=:UserInfo.getUserId()];
			List<Account> Account_List = [Select a.Member_Of__c From Account a where a.Id=:CommunityUser.AccountId];
			if(Account_List.size()>0){
				if(Account_List[0].Member_Of__c=='ICAA'){
					return null;
				}
			}
		}
		PageReference PR = new PageReference('/'+ Schema.sObjectType.Application__c.getKeyPrefix() +'/o');
		PR.getParameters().put('nooverride','1');
		return PR;
	}
	
	public PageReference gotoNewPage(){
		if( UserInfo.getProfileId()==ProfileCache.getId('NZICA Community Login User') ){
			PageReference PR =  Page.ApplicationWizardPage0;
			PR.getParameters().put('RecordType', Application.RecordTypeId);
			return PR;
		}
		else{
			String currentPageUrl = ApexPages.currentPage().getUrl();
			String ApplicationNewOverrideString = (Test.isRunningTest())?
				'/apex/applicationnewoverride':'/apex/ApplicationNewOverride';
			currentPageUrl = currentPageUrl.replace( ApplicationNewOverrideString,
				'/'+ Schema.sObjectType.Application__c.getKeyPrefix() +'/e');
			currentPageUrl = currentPageUrl.replace('&save_new=1','');
			
			PageReference PR = new PageReference( currentPageUrl );
			PR.getParameters().put('nooverride','1');
			return PR;
		}
	}
	
	public PageReference gotoEditPage(){
		if( UserInfo.getProfileId()==ProfileCache.getId('NZICA Community Login User') ){
			if( Application.Application_Status__c=='Draft' ){
				PageReference PR = Page.ApplicationWizardPage0;
				PR.getParameters().put('RecordType', Application.RecordTypeId);
				return PR;
			}
			else{
				return Page.ApplicationWizardPageNoAccess;
			}
		}
		else{
			PageReference PR = new PageReference('/' + Application.Id +
				'/e?retURL=%2F' + Application.Id );
			PR.getParameters().put('nooverride','1');
			return PR;
		}
	}
}