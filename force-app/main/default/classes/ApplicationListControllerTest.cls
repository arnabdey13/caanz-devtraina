@isTest
public class ApplicationListControllerTest {

    static testMethod void getApplicationsForUser(){
        Account testAccount=TestObjectCreator.createFullMemberAccount();
        testAccount.Membership_Type__c='Member';

        Test.startTest();
        insert testAccount; // Future method
        Test.stopTest();
        Id testContactId = [Select id FROM Contact WHERE AccountId =: testAccount.Id limit 1].Id;
        User testCommunityUser = [Select Email, UserName, AccountId, FederationIdentifier FROM User WHERE contactId=:testContactId limit 1];
		system.debug('testCommunityUser='+testCommunityUser);
        
		Application__c testAppDraft=new Application__c(Account__c=testAccount.Id
                                                       ,Application_Status__c='Draft');
        
		Application__c testAppApproved=new Application__c(Account__c=testAccount.Id
                                                          ,Application_Status__c='Approved'
                                                          ,Application_Assessor__c=testCommunityUser.Id
                                                          ,Application_Fee_Paid1__c='Paid');
        List<Application__c> testApplications=new List<Application__c>();
        testApplications.add(testAppDraft);
        testApplications.add(testAppApproved);
        insert testApplications;
        //Approved application expected to change automatically to Submitted for Assessment status
        system.debug('testApplications='+testApplications);
        
        System.runAs(testCommunityUser){
            //test 1 include Draft, exclude Approved
            List<Application__c> myApps=ApplicationListController.getApplicationsForUser('Draft,-Submitted for Assessment');
            system.debug('Test 1 - myapps='+myapps);
            Map<Id,Application__c> myAppsMap= new Map<Id,Application__c>();
            myAppsMap.putAll(myApps);
            system.debug('Test 1 - myAppsMap.get(testAppDraft.Id='+myAppsMap.get(testAppDraft.Id));
            system.debug('Test 1 - myAppsMap.get(testAppApproved.Id='+myAppsMap.get(testAppApproved.Id));
            System.assertNotEquals(myAppsMap.get(testAppDraft.Id),null,'Test 1 - User unexpectedly can\'t see Draft application in list!');
            System.assertEquals(myAppsMap.get(testAppApproved.Id),null,'Test 1 - User unexpectedly can see Approved application in list!');
            
            //test 2 should return all
            myApps.clear();
            myApps=ApplicationListController.getApplicationsForUser('');
            system.debug('Test 2 - myapps='+myapps);
            myAppsMap.clear();
            myAppsMap.putAll(myApps);
            system.debug('Test 2 - myAppsMap.get(testAppDraft.Id='+myAppsMap.get(testAppDraft.Id));
            system.debug('Test 2 - myAppsMap.get(testAppApproved.Id='+myAppsMap.get(testAppApproved.Id));
            System.assertNotEquals(myAppsMap.get(testAppDraft.Id),null,'Test 2 - User unexpectedly can\'t see Draft application in list!');
            System.assertNotEquals(myAppsMap.get(testAppApproved.Id),null,'Test 2 - User unexpectedly can\'t see Approved application in list!');
            
            //test 3 should return just Draft
            myApps.clear();
            myApps=ApplicationListController.getApplicationsForUser('Draft');
            system.debug('Test 3 - myapps='+myapps);
            myAppsMap.clear();
            myAppsMap.putAll(myApps);
            system.debug('Test 3 - myAppsMap.get(testAppDraft.Id='+myAppsMap.get(testAppDraft.Id));
            system.debug('Test 3 - myAppsMap.get(testAppApproved.Id='+myAppsMap.get(testAppApproved.Id));
            System.assertNotEquals(myAppsMap.get(testAppDraft.Id),null,'Test 3 - User unexpectedly can\'t see Draft application in list!');
            System.assertEquals(myAppsMap.get(testAppApproved.Id),null,'Test 3 - User unexpectedly can see Approved application in list!');
            
            //test 4 should return all except Approved
            myApps.clear();
            myApps=ApplicationListController.getApplicationsForUser('-Submitted for Assessment');
            system.debug('Test 4 - myapps='+myapps);
            myAppsMap.clear();
            myAppsMap.putAll(myApps);
            system.debug('Test 4 - myAppsMap.get(testAppDraft.Id='+myAppsMap.get(testAppDraft.Id));
            system.debug('Test 4 - myAppsMap.get(testAppApproved.Id='+myAppsMap.get(testAppApproved.Id));
            System.assertNotEquals(myAppsMap.get(testAppDraft.Id),null,'Test 4 - User unexpectedly can\'t see Draft application in list!');
            System.assertEquals(myAppsMap.get(testAppApproved.Id),null,'Test 4 - User unexpectedly can see Approved application in list!');
            
        }
    }
}