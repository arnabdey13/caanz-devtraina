@isTest
public class QPRSearchReportQueueableTest {
    
    private static Account getFullMemberAccountObject(){       
        Account FullMemberAccountObject = TestObjectCreator.createFullMemberAccount();
        FullMemberAccountObject.Member_Of__c = 'NZICA' ;
        FullMemberAccountObject.Financial_Category__c = 'Life' ;
        FullMemberAccountObject.FP_Specialisation__c = true ;
        FullMemberAccountObject.Registered_BAS_Agent__c = true ;
        FullMemberAccountObject.Reviewer_of_Second_Tier_Companies__c = true ;
        FullMemberAccountObject.FP_Subscriber__c = true ;
        FullMemberAccountObject.SMSF__c = true ;
        FullMemberAccountObject.Registered_Company_Auditor__c = true ;
        FullMemberAccountObject.SMSF_Auditor__c = true ;
        FullMemberAccountObject.Registered_Company_Liquidator__c = true ;
        FullMemberAccountObject.BV__c = true ;
        FullMemberAccountObject.Financial_Planner__c = true ;
        FullMemberAccountObject.Registered_Trustee_in_Bankruptcy__c = true ;
        FullMemberAccountObject.Registered_Tax_Agent__c = true ;
        FullMemberAccountObject.Licensee_of_AFSL__c = true ;
        FullMemberAccountObject.Authorised_Representative_of_an_AFSL__c = true ;
        FullMemberAccountObject.CPP_Picklist_AU__c = 'Full';
        FullMemberAccountObject.Status__c = 'Active';
        FullMemberAccountObject.PersonOtherCity = 'Sydney';
        FullMemberAccountObject.PersonOtherPostalCode = '1010';
        FullMemberAccountObject.PersonEmail = 'jai.chaturvedi6327t32@davanti.co.nz';
        // FullMemberAccountObject.Last_Review_Date_member__c = System.today() - 400 ;
        return FullMemberAccountObject;
    }
    
    static testmethod void test1() {
        Account businessAccountObject = TestObjectCreator.createBusinessAccount();
        businessAccountObject.BillingStreet = '88 Nelson Street';
        businessAccountObject.Member_Of__c = 'NZICA' ;
        businessAccountObject.Status__c = 'Active' ;
        businessAccountObject.Affiliated_Branch_Country__c = 'Australia' ; 
        businessAccountObject.Affiliated_Branch__c = 'New South Wales' ; 
        businessAccountObject.BillingCity = 'Australia' ;
        businessAccountObject.BillingPostalCode = '1010';
        businessAccountObject.CPP_Picklist_AU__c = 'Full' ; 
        businessAccountObject.Affiliated_Branch_Country__c = 'Australia' ;
        businessAccountObject.Last_Review_Date_1__c = System.today() - 400 ;
        businessAccountObject.Practice_Information_Questionnaire_Date__c = System.today() - 400 ;
        businessAccountObject.Status__c = 'Active';
        businessAccountObject.Has_open_review__c = false ;
        businessAccountObject.Has_open_survey__c = false;
        
        insert businessAccountObject;
        List<QPR_Questionnaire__c> ret = new List<QPR_Questionnaire__c>() ;
        Account FullMemberAccountObject = getFullMemberAccountObject();
        Test.startTest() ;
        insert FullMemberAccountObject; // Future method
        
        
        for(Integer i=0; i<30 ; i++){
            ret.add(new QPR_Questionnaire__c(Practice__c=businessAccountObject.Id, Account__c =FullMemberAccountObject.Id ));
        }
        QPRSearchReportQueueable.splitList(ret);
        
        Test.stopTest() ;
        
        
        
    }
}