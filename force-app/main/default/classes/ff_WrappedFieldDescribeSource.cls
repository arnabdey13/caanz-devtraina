/**
* @author Steve Buikhuizen, Jannis Bott
* @date 14/11/2016
*
* @group FormsFramework
*
* @description  Used to generate PicklistOptions based on describing a Salesforce Picklist field.
*/
global with sharing class ff_WrappedFieldDescribeSource extends ff_WrappedPicklistSource {

    private Schema.SObjectField field;
    private String label1;
    private String label2;
    private Map<String, Set<String>> valueToParentMap;

    /**
    * @description Constructor that sets the Schema.SObjectField
    * @param Schema.SObjectField the picklist field that will be described
    * @example
    * new ff_WrappedFieldDescribeSource(Account.Type)
    */
    global ff_WrappedFieldDescribeSource(Schema.SObjectField field) {
        this.field = field;
    }

    /**
    * @description  Constructor that sets the Schema.SObjectField and the labels
    *               for true/false if the field is a boolean
    * @param Schema.SObjectField the picklist field that will be described
    * @param String booleanTrueLabel the label for the true option
    * @param String booleanFalseLabel the label for the false option
    * @example
    * new ff_WrappedFieldDescribeSource(Account.Type, 'Active', 'Inactive')
    */
    global ff_WrappedFieldDescribeSource(Schema.SObjectField field, String label1, String label2) {
        this.field = field;
        this.label1 = label1;
        this.label2 = label2;
    }

    /**
    * @description  Constructor that sets the Schema.SObjectField and the parentValue Map
    *               that is used to set the dependency for the picklist values
    * @param Schema.SObjectField the picklist field that will be described
    * @param Map<String, List<String>>  parentValueMap a map of parent keys mapped to corresponding child values
    * @example
    * new ff_WrappedFieldDescribeSource(Account.Type, 'Active', 'Inactive')
    */
    global ff_WrappedFieldDescribeSource(Schema.SObjectField field, Map<String, Set<String>> valueToParentMap) {
        this.field = field;
        this.label1 = label1;
        this.label2 = label2;
        this.valueToParentMap = valueToParentMap;
    }

    global override List<ff_Service.CustomWrapper> getWrappers() {
        List<ff_Service.CustomWrapper> result = new List<ff_Service.CustomWrapper>();

        //if picklist extract its values
        if (field.getDescribe().getType() == Schema.DisplayType.PICKLIST) {
            result.addAll(getPicklistValues());
            //if checkbox set the true and false values
        } else if (field.getDescribe().getType() == Schema.DisplayType.BOOLEAN) {
            result.addAll(getCheckboxValues());
        } else {
            throw new SObjectException('Not a picklist or checkbox field: ' + field.getDescribe().getName());
        }
        return result;
    }

    private List<ff_Service.CustomWrapper> getPicklistValues() {
        List<ff_Service.CustomWrapper> result = new List<ff_Service.CustomWrapper>();

        // used for translating values from e.g. true/false (checkbox field) to Yes/No wrappers
        if (label1 != null && label2 != null && field.getDescribe().getPicklistValues().size() == 2) {
            Integer count = 1;
            for (Schema.PicklistEntry ple : field.getDescribe().getPicklistValues()) {
                if (ple.isActive()) {
                    ff_Service.PicklistOption wrap = new ff_Service.PicklistOption();
                    if (count == 1) {
                        wrap.label = label1;
                    } else {
                        wrap.label = label2;
                    }
                    wrap.value = ple.getValue();
                    result.add(wrap);
                    count++;
                }
            }
        } else if (valueToParentMap != null) { // dependent picklist
            for (Schema.PicklistEntry ple : field.getDescribe().getPicklistValues()) {
                if (ple.isActive()) {
                    ff_Service.PicklistOption wrap = new ff_Service.PicklistOption();
                    wrap.label = ple.getLabel();
                    wrap.value = ple.getValue();
                    if (valueToParentMap.containsKey(ple.getValue())) {
                        //wrap.parentValues = valueToParentMap.get(ple.getLabel());
                        wrap.parentValues = valueToParentMap.get(ple.getValue());
                    }
                    result.add(wrap);
                }
            }
        } else {
            for (Schema.PicklistEntry ple : field.getDescribe().getPicklistValues()) {
                if (ple.isActive()) {
                    ff_Service.PicklistOption wrap = new ff_Service.PicklistOption();
                    wrap.label = ple.getLabel();
                    wrap.value = ple.getValue();
                    result.add(wrap);
                }
            }
        }
        return result;
    }

    private List<ff_Service.CustomWrapper> getCheckboxValues() {
        List<ff_Service.CustomWrapper> result = new List<ff_Service.CustomWrapper>();

        ff_Service.PicklistOption wrapTrue = new ff_Service.PicklistOption();
        wrapTrue.label = label1;
        wrapTrue.value = String.valueOf(true);
        result.add(wrapTrue);

        ff_Service.PicklistOption wrapFalse = new ff_Service.PicklistOption();
        wrapFalse.label = label2;
        wrapFalse.value = String.valueOf(false);
        result.add(wrapFalse);

        return result;
    }
}