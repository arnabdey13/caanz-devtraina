public class luana_AdobeConnectUtil {
    
    // Custom settings to store Adobe Connect app URL
    Luana_Extension_Settings__c adobeConn = Luana_Extension_Settings__c.getValues('AdobeConnect_URL');  

    public luana_AdobeConnectUtil() {
    
    
    }
    
    
    // Check Custom Setting - AdobeConnect URL
    public String getAdobeConnectURL() {
        if(adobeConn != null && adobeConn.Value__c != null)
            return adobeConn.Value__c; 
        else {
            return '';
        }
    }
    
    
    public Boolean doExtLogin(String conId, String cookie) {
        Http loginH = new Http();
        HttpRequest loginReq = new HttpRequest();
        loginReq.setEndpoint(getAdobeConnectURL() + '/api/xml?action=login&external-auth=use&session=' + cookie);
        loginReq.setMethod('GET');
        loginReq.setHeader('cchauth', conId);
        
        HttpResponse loginRes;
        if(Test.isRunningTest()){
            loginRes = new HttpResponse();
            loginRes.setHeader('Content-Type', 'text/xml');
            loginRes.setBody(luana_AT_VCImportWizardMockHttp_Test.loggingInResp);
        }else{
            loginRes = loginH.send(loginReq);
        }
        
        Dom.Document loginDoc = loginRes.getBodyDocument();
        Dom.XMLNode loginResults = loginDoc.getRootElement();
                
               
        if(loginResults.getChildElement('status', null) != null && loginResults.getChildElement('status', null).getAttribute('code', null) == 'ok') 
            return true;        
        else 
            return false;
    }
    
    public String getSessionCookie() {
        Http commonInfoH = new Http();
        HttpRequest commonInfoReq = new HttpRequest();
        
        // Get Session Cookie
        commonInfoReq.setEndpoint(getAdobeConnectURL() + '/api/xml?action=common-info');
        commonInfoReq.setMethod('GET');
        
        HttpResponse commonInfoRes;
        if(Test.isRunningTest()){
            commonInfoRes = new HttpResponse();
            commonInfoRes.setHeader('Content-Type', 'text/xml');
            commonInfoRes.setBody(luana_AT_VCImportWizardMockHttp_Test.commoInfoResp);
        }else{
            commonInfoRes = commonInfoH.send(commonInfoReq);
        }
        
        Dom.Document commonInfoDoc = commonInfoRes.getBodyDocument();
        Dom.XMLNode commonInfoResults = commonInfoDoc.getRootElement();
            
        // Check if Common element and cookie element is in XML body
        if(commonInfoResults.getChildElement('common', null) != null && commonInfoResults.getChildElement('common', null).getChildElement('cookie', null) != null)
            return commonInfoResults.getChildElement('common', null).getChildElement('cookie', null).getText();
        else
            return '';
    
    }    
}