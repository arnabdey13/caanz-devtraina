/* 
    Developer: WDCi (KH)
    Development Date: 01/12/2016
    Task #: AAS Reset method  
*/

public with sharing class AAS_CourseAssessmentResetController {
    
    public AAS_Course_Assessment__c courseAssessment {get; set;}
    //public Boolean hasFinalResultReleased {get; set;}
    public Boolean hasResultReleasedLock {get; set;}
    
    List<AAS_Student_Assessment__c> studAssList;
    
    ID batchprocessid;
    public boolean isUpdateDone {get;set;}
    public boolean checkCommitStatus {get; set;}
    
    Integer numOfSASRecordsDelete;
    Integer numOfSASIRecordsDelete;
    
    public AAS_CourseAssessmentResetController(ApexPages.StandardController controller){
        
        if(!test.isRunningTest()){
            List<String> addFieldName = new List<String>{'AAS_Cohort_Adjustment_Exam__c', 'AAS_Course__r.LuanaSMS__Program_Offering__r.IsCapstone__c',
                                                        'AAS_Cohort_Adjustment_Supp_Exam__c', 'AAS_Borderline_Remark__c', 'AAS_Borderline_Remark_Supp_Exam__c', 'AAS_Final_Result_Release_Exam__c', 
                                                        'AAS_Exam_Adjustment__c', 'AAS_Pre_Final_Adjustment__c' , 'AAS_Result_Release_Lock__c'};
            controller.addFields(addFieldName);
        }
        courseAssessment = (AAS_Course_Assessment__c) controller.getRecord();
        
        isUpdateDone= false;
        checkCommitStatus= false;
        
        numOfSASRecordsDelete = 0;
        numOfSASIRecordsDelete = 0;
        
        hasResultReleasedLock = courseAssessment.AAS_Result_Release_Lock__c;
        
        if(!courseAssessment.AAS_Result_Release_Lock__c){
            
            studAssList = new List<AAS_Student_Assessment__c>();
            studAssList = [Select Id from AAS_Student_Assessment__c Where AAS_Course_Assessment__c =: courseAssessment.Id];
            
            List<AggregateResult> sasResults = [Select Count(Id) from AAS_Student_Assessment_Section__c where AAS_Student_Assessment__c in: studAssList];
            numOfSASRecordsDelete = (Integer) sasResults[0].get('expr0');
            
            List<AggregateResult> sasiResults = [Select Count(Id) from AAS_Student_Assessment_Section_Item__c where AAS_Student_Assessment_Section__r.AAS_Student_Assessment__c in: studAssList];
            numOfSASIRecordsDelete = (Integer) sasiResults[0].get('expr0');
            
            addPageMessage(ApexPages.severity.INFO, 'Total ' + studAssList.size() + ' Student Assessment record/s found.' );
        }else{
            addPageMessage(ApexPages.severity.WARNING, 'You are not allow to reset as Result Release Lock is completed before.');
        }
    }
    
    public void doReset(){
        Savepoint sp = Database.setSavepoint();
        try{
            if(!courseAssessment.AAS_Result_Release_Lock__c){
                
                courseAssessment.AAS_Non_Exam_Data_Loaded__c = false;
                courseAssessment.AAS_Exam_Data_Loaded__c = false;
                courseAssessment.AAS_Cohort_Adjustment_Exam__c = false;
                courseAssessment.AAS_Borderline_Remark__c = false;
                courseAssessment.AAS_Final_Adjustment_Exam__c = false;
                courseAssessment.AAS_Final_Result_Calculation_Exam__c = false;
                
                //new checkbox to reset:
                courseAssessment.AAS_Exam_Adjustment__c = false;
                courseAssessment.AAS_Pre_Final_Adjustment__c = false;
                courseAssessment.AAS_Result_Release_Lock__c = false;

                update courseAssessment;
                
               if(!studAssList.isEmpty()){
                    AAS_DeleteBatch updateSASIBatch = New AAS_DeleteBatch(studAssList);
                    batchprocessid= database.executeBatch(updateSASIBatch);
                    checkCommitStatus = true; 
                }else{
                    isUpdateDone = true;
                    checkCommitStatus = false;
                    addPageMessage(ApexPages.severity.INFO, 'No record deleted.');
                    
                } 
            }
        }Catch(Exception exp){
            Database.rollback(sp);
            addPageMessage(ApexPages.severity.ERROR, 'Error processing. Please try again later or contact your system administrator. Error: ' + exp.getMessage() + ' - ' + exp.getStackTraceString());
        }
        
    }
    
    public void checkBatchCompletion(){
        if(!isUpdateDone){
            List<AsyncApexJob> aajs = [SELECT Id, Status, ExtendedStatus, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =: batchprocessid and Status = 'Completed'];
            System.debug('********aajs: ' + aajs);
            if(!aajs.isEmpty()){
                if(aajs[0].NumberOfErrors == 0){
                    isUpdateDone = true;
                    checkCommitStatus = false;
                    
                    addPageMessage(ApexPages.severity.INFO, 'Course Assessment \'Reset\' successful: <ul><li> Total ' + studAssList.size() + ' Student Assessment record/s are deleted. </li>'+
                                                '<li> Total ' + numOfSASRecordsDelete+ ' Student Assessment Section record/s are deleted. </li>'+
                                                '<li> Total ' + numOfSASIRecordsDelete+ ' Student Assessment Section Items record/s are deleted. </li></ul>');
                }else{
                    isUpdateDone = true;
                    checkCommitStatus = false;
                    
                    addPageMessage(ApexPages.severity.WARNING, 'Course Assessment \'Reset\' failed: <br />' + 
                                                                'Error Message: ' + aajs[0].ExtendedStatus);
                }
            }
        }
    }
    
    public PageReference doCancel(){
        PageReference returnPage = new PageReference('/'+courseAssessment.Id);
        return returnPage;
    }
    
    public void addPageMessage(ApexPages.severity sev, String msg){
        ApexPages.Message warningmsg = new ApexPages.Message(sev, msg);
        ApexPages.addmessage(warningmsg);
    }
}