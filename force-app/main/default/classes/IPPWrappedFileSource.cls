public with sharing class IPPWrappedFileSource extends ff_WrapperSource {
    
    private Id parentId;

    public IPPWrappedFileSource(Id parentId) {
        this.parentId = parentId;
    }

    public override List<ff_Service.CustomWrapper> getWrappers() {
        //wrappers - ContentDocument type
        List<ff_Service.FilesWrapper> wrappers = new List<ff_Service.FilesWrapper>();
        Set<Id> docIds = new Set<Id>();
        
        List<ContentDocumentLink> cdl = [SELECT id,LinkedEntityId,ContentDocumentId 
                                            FROM ContentDocumentLink 
                                            WHERE LinkedEntityId=:this.parentId];
        
        for(ContentDocumentLink cdLink: cdl) {
            docIds.add(cdLink.ContentDocumentId);
        }

        //insert files into wrapper
        wrappers.add(new ff_Service.FilesWrapper(
            new List<ContentDocument>([SELECT Title FROM ContentDocument WHERE id IN: docIds]), this.parentId));        
        
        return wrappers;
    }
}