public with sharing class form_MyDetailsService {

    ////////// LIGHTNING APIS //////////

    @AuraEnabled
    public static ff_ReadData load() {
        return new form_MyDetailsService(EditorPageUtils.getUsersAccountId()).handler.load(null);
    }

    @AuraEnabled
    public static ff_WriteResult save(String writeData) {
        System.debug('in save ' + writeData);
        return new form_MyDetailsService(EditorPageUtils.getUsersAccountId()).handler.save(writeData);
    }

    @AuraEnabled
    public static ff_WriteResult deleteRecord(Id id) {
        return new form_MyDetailsService(EditorPageUtils.getUsersAccountId()).handler.deleteRecord(id);
    }

    @AuraEnabled
    public static List<Map<String, Object>> search(String objectName, String fieldName, String searchTerm, Map<String, Object> searchContext) {
        RecordIdProviderImpl idProvider = new RecordIdProviderImpl(EditorPageUtils.getUsersAccountId());
        return new RelaxedSharingEmployerHistory().searchBusinessAccounts(idProvider, searchTerm);
    }

    ////////// HANDLERS //////////

    @TestVisible
    private final ff_ServiceHandler handler;

    @TestVisible
    private form_MyDetailsService(Id accountId) {
        RecordIdProviderImpl idProvider = new RecordIdProviderImpl(accountId);
        handler = new ff_ServiceHandler(idProvider, new LoaderImpl(idProvider), new SaverImpl());
    }

    public class RecordIdProviderImpl implements ff_ServiceHandler.RecordIdProvider {

        Id accountId;
        Set<Id> historyIds;

        public RecordIdProviderImpl(Id accountId) {
            this.accountId = accountId;
            this.historyIds = new RelaxedSharingEmployerHistory().getHistoryIds(accountId);
        }
        public Map<Schema.sObjectType, Set<Id>> getContext() {
            Map<Schema.sObjectType, Set<Id>> result = new Map<Schema.sObjectType, Set<Id>>();
            result.put(Account.getSObjectType(), new Set<Id>{
                    accountId
            });
            result.put(Employment_History__c.getSObjectType(), historyIds);
            return result;
        }
    }

    private class LoaderImpl extends ff_ServiceHandler.LoadHandler {

        private final ff_ServiceHandler.RecordIdProvider idProvider;

        private form_MyDetailsService.LoaderImpl(ff_ServiceHandler.RecordIdProvider idProvider) {
            this.idProvider = idProvider;
        }

        public override Map<String, List<ff_Service.SObjectWrapper>> loadRecordsForRead(Map<Schema.sObjectType, Set<Id>> recordIds) {

            List<ff_Service.SObjectWrapper> accountWrappers = new List<ff_Service.SObjectWrapper>();
            List<ff_Service.SObjectWrapper> employerWrappers = new List<ff_Service.SObjectWrapper>();

            //add the account to the loader
            if (recordIds.containsKey(Account.getSObjectType())) {

                List<Account> accounts = getAccount(ff_ServiceHandler.getProvidedId(idProvider, Account.getSObjectType(), 0));
                if (accounts.size() == 1) {
                    accountWrappers.add(new ff_Service.SObjectWrapper(
                            accounts,
                            true));
                    employerWrappers.add(new ff_Service.SObjectWrapper(
                            new RelaxedSharingEmployerHistory().getEmploymentHistories(idProvider.getContext().get(Employment_History__c.getSObjectType())),
                            false));
                } else {
                    throw new SObjectException('More than one Account record found.');
                }
            } else {
                throw new SObjectException('No Account Id was retrieved.');
            }

            return new Map<String, List<ff_Service.SObjectWrapper>>{
                    'Account' => accountWrappers,
                    'Employment_History__c' => employerWrappers
            };
        }

        public override Map<String, List<ff_Service.CustomWrapper>> loadWrappersForRead(Map<Schema.sObjectType, Set<Id>> recordIds) {

            Map<String, ff_WrapperSource> sources = new Map<String, ff_WrapperSource>();

            sources.put('Account.Salutation', new ff_WrappedFieldDescribeSource(Account.Salutation));
            sources.put('Account.Gender', new ff_WrappedFieldDescribeSource(Account.Gender__c));
            sources.put('Status.opts', new ff_WrappedFieldDescribeSource(Employment_History__c.Status__c));
            sources.put('Type.opts', new ff_WrappedFieldDescribeSource(Employment_History__c.Work_Hours__c));

            sources.put('MyDetails.PersonalInformation', new ff_WrappedRichtextSource('MyDetails.PersonalInformation'));
            sources.put('MyDetails.ResidentialAddress', new ff_WrappedRichtextSource('MyDetails.ResidentialAddress'));
            sources.put('MyDetails.MailingAddress', new ff_WrappedRichtextSource('MyDetails.MailingAddress'));
            return ff_ServiceHandler.loadWrappers(sources);
        }

        private List<Account> getAccount(Id accountId) {
            return [
                    SELECT Id, FirstName, LastName, PersonEmail, Name, Preferred_Contact_Number__c,
                            Customer_ID__c, Member_ID__c, Salutation, Middle_Name__c, PersonMobilePhone, PersonHomePhone, Preferred_Name__c,
                            PersonOtherStreet, PersonOtherCity, PersonOtherState, PersonOtherStateCode, PersonOtherPostalCode, PersonOtherCountryCode,
                            PersonMailingStreet, PersonMailingCity, PersonMailingState, PersonMailingStateCode, PersonMailingPostalCode, PersonMailingCountryCode,
                            PersonMailingAddressValidatedByQAS__c, Mailing_DPID__c, PersonOtherAddressValidatedByQAS__c, Residential_DPID__c,
                            Mailing_and_Residential_is_the_same__c,
                            Newsletter_Recipient__c, Replacement_Certificate_request__c, PersonOtherPhone, PersonBirthdate, Gender__c, PersonHasOptedOutOfEmail,
                            PersonHasOptedOutOfMail__pc, PersonHasOptedOutOfSMS__pc, PersonDoNotCall, Designation__c, Financial_Category__c, Membership_Class__c
                    FROM Account
                    WHERE Id = :accountId
            ];
        }
    }

    private static Set<SObjectType> SUPPORTED = new Set<SObjectType>{
            Account.getSObjectType(),
            Employment_History__c.getSObjectType()
    };

    public class SaverImpl extends ff_ServiceHandler.SaveHandler {

        public override List<Database.SaveResult> doInsert(List<SObject> toSave) {
            if (toSave.get(0).getSObjectType() == Employment_History__c.getSObjectType()) {
                return new RelaxedSharingEmployerHistory().insertHistory(toSave);
            } else {
                return super.doInsert(toSave);
            }
        }

        public override SObject enhanceResult(ff_ServiceHandler.RecordIdProvider idProvider, SObject record) {
            if (record.getSobjectType() == Employment_History__c.getSObjectType()) {
                // when employers are opted-out, the nested employer record is required in response to client so that
                // a FK is present and the autocomplete has a value i.e. doesn't block as empty
                return new form_MyDetailsService.RelaxedSharingEmployerHistory()
                        .getEmploymentHistories(new Set<Id>{
                                record.Id
                        })
                        .get(0);
            } else {
                return super.enhanceResult(idProvider, record);
            }
        }

        public override List<Database.SaveResult> doUpdate(List<SObject> toSave) {
            if (toSave.get(0).getSObjectType() == Employment_History__c.getSObjectType()) {
                return new RelaxedSharingEmployerHistory().updateHistory(toSave);
            } else {
                return super.doUpdate(toSave);
            }
        }

        public override Database.DeleteResult doDelete(Id id) {
            if (id.getSObjectType() == Employment_History__c.getSObjectType()) {
                return new RelaxedSharingEmployerHistory().deleteHistory(id);
            } else {
                return super.doDelete(id);
            }
        }

        public override List<SObject> processUpserts(ff_ServiceHandler.RecordIdProvider idProvider, List<SObject> upsertRecords, List<SObject> otherRecords) {

            if (otherRecords.size() == 1) {
                if (!SUPPORTED.contains(otherRecords.get(0).getSObjectType())) {
                    throw new SObjectException('Unsupported sObject type');
                }

                // happy path
                if (otherRecords.get(0).getSObjectType() == Account.getSObjectType()) {
                    // main form save, no mods required
                } else {
                    Employment_History__c history = (Employment_History__c) otherRecords.get(0);
                    new RelaxedSharingEmployerHistory().processEmployeeHistoryUpsert(idProvider, history);
                }

            } else {
                throw new SObjectException('Only single record operations supported');
            }

            return otherRecords;
        }
    }

    private static String getMembership(ff_ServiceHandler.RecordIdProvider idProvider) {
        Account a = [
                SELECT Id, Member_Of__c
                FROM Account
                WHERE Id in :idProvider.getContext().get(Account.getSObjectType())
        ];
        return a.Member_Of__c;
    }

    // shared history methods TODO design/impl composable saver components
    public without sharing class RelaxedSharingEmployerHistory {

        private final Id recordTypeBusiness = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();

        public Set<Id> getHistoryIds(Id accountId) {
            System.debug('loading history ids for: ' + accountId);
            Set<Id> historyIds = new Set<Id>();
            for (Employment_History__c h : [SELECT Id FROM Employment_History__c WHERE Member__c = :accountId]) {
                historyIds.add(h.Id);
            }
            System.debug('loaded history ids: ' + historyIds);
            return historyIds;
        }

        public List<Employment_History__c> getEmploymentHistories(Set<Id> historyIds) {
            System.debug('history ids: ' + historyIds);
            return [
                    SELECT Id, Job_Title__c, Employee_Start_Date__c, Employee_End_Date__c, Primary_Employer__c, Status__c,
                            No_of_Hours_Per_week_For_Part_Time__c, Work_Hours__c,
                            Employer__c, Employer__r.Id, Employer__r.Name, Employer__r.Website,
                            Employer__r.BillingCountryCode, Employer__r.BillingStreet, Employer__r.BillingCity, Employer__r.BillingState, Employer__r.BillingPostalCode
                    FROM Employment_History__c
                    WHERE Id in :historyIds
            ];
        }

        // required to support members creating records with references to Accounts that
        // sharing is hiding from them
        public List<Database.SaveResult> insertHistory(List<SObject> toInsert) {
            return Database.insert(toInsert, false);
        }

        public List<Database.SaveResult> updateHistory(List<SObject> toUpdate) {
            return Database.update(toUpdate, false);
        }

        public Database.DeleteResult deleteHistory(Id id) {
            return Database.delete(id, false);
        }

        /*
        Removes the "Primary Employer" from all other Current Employment_History__c records
         */
        public void removeOtherPrimaries(Id memberAccountId, Id newPrimaryRecordId) {
            Set<Id> excluded = new Set<Id>();
            if (newPrimaryRecordId != null) {
                excluded.add(newPrimaryRecordId);
            }

            List<SObject> toUpdate = new List<SObject>();
            for (Employment_History__c history : [
                    SELECT Id
                    FROM Employment_History__c
                    WHERE Member__c = :memberAccountId
                    AND Status__c = 'Current'
                    AND Primary_Employer__c = true
                    AND Id NOT IN :excluded
            ]) {
                history.Primary_Employer__c = false;
                toUpdate.add(history);
            }

            if (toUpdate.size() > 0) {
                System.debug('Other opted-in primaries being unset: ' + toUpdate);
                update toUpdate;
            }
        }

        // required to support loading of employer data which sharing hides from member
        public void processEmployeeHistoryUpsert(
                ff_ServiceHandler.RecordIdProvider idProvider,
                Employment_History__c history) {

            Id membersAccountId = ff_ServiceHandler.getProvidedId(idProvider, Account.getSObjectType(), 0);
            Set<Id> applicationIds = idProvider.getContext().get(Application__c.getSObjectType());

            System.debug('history pre-upsert: ' + history);
            // important: must use .get when checking for null in a boolean field
            if (history.get('Primary_Employer__c') == NULL) {
                history.Primary_Employer__c = false;
            } else if (history.Primary_Employer__c) {
                // need to uncheck all other primaries since only 1 primary is allowed by business rules
                // and a validation rule in production breaks for duplicate current/primary Employment_History__c records
                removeOtherPrimaries(membersAccountId, history.Id);
            }

            // connect history to account
            Account member = [SELECT Id, Member_Of__c FROM Account WHERE Id = :membersAccountId];
            history.Member__c = membersAccountId;
            history.Member__r = member;

            Account employerAccount = history.Employer__r;
            if (employerAccount == null) {
                if (history.Employer__c != null) { // opted-in employer search
                    // ensure member_of is present in nested employer so that validation rules can use it
                    Account employer = [select Id, Member_Of__c FROM Account where Id = :history.Employer__c];
                    System.debug('employer: ' + employer);
                    history.Employer__r = employer;
                } else {
                    // this is path for updating an existing history
                }
            } else {
                // this should never happen in MyDetails (disabled) or ProvApp (now uses Unknown_Employer__c)
                throw new SObjectException('Opt-out not supported');
            }
        }

        // required to allow members to search accounts that sharing hides from them
        // TODO make a composable search interface/concrete class following wrapper pattern above
        public List<Map<String, Object>> searchBusinessAccounts(ff_ServiceHandler.RecordIdProvider idProvider, String searchTerm) {

            String memberOf = getMembership(idProvider);
            List<Map<String, Object>> result = new List<Map<String, Object>>();
            System.debug('in search ' + searchTerm);
            List<List<SObject>> searchList = [
                    FIND :searchTerm
                    IN NAME FIELDS
                            RETURNING Account (Id, Name, BillingStreet, BillingCity
                            WHERE RecordTypeId = :recordTypeBusiness AND Member_Of__c = :memberOf AND Status__c = 'Active')
            ];

            if (!searchList.isEmpty()) {
                Integer counter = 0;
                for (Account account : (List<Account>) searchList[0]) {
                    String addressString = '';
                    if (account.BillingStreet != NULL) {
                        addressString = account.BillingStreet;
                    }
                    if (account.BillingCity != NULL) {
                        addressString = addressString + ', ' + account.BillingCity;
                    }
                    Map<String, Object> match = new Map<String, Object>{
                            'id' => account.Id,
                            'label1' => account.Name,
                            'label2' => addressString,
                            'position' => counter
                    };
                    result.add(match);
                    counter++;
                }
            }
            return result;
        }
    }
}