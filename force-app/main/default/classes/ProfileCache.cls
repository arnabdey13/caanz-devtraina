public with sharing class ProfileCache {
	private static Map<String, Profile> currentCache = null;
	
	static {
		refreshCache();
	}
	
	public static Profile getProfile(String Name){
		if(currentCache == null){
			refreshCache();
		}
		Profile result = currentCache.get(Name);
		if (result == null) {
			refreshCache();
			result = currentCache.get(Name);
		}
		if (result == null) {
			throw new NewException('Unable to find Profile for ' + Name);
		}
		return result;
	}
	
	public static Profile getProfile(Id ProfileId) {
		if (currentCache == null) {
			refreshCache();
		}
		
		Profile result = currentCache.get(ProfileId);
		if (result == null) {
			refreshCache();
			result = currentCache.get(ProfileId);
		}
		if (result == null) {
			throw new NewException('Unable to find Profile for ' + ProfileId + currentCache);
		}
		return result;
	}
	
	/**
	 * Get the ProfileId for the given sObjectType and developerName (Case Sensitive)
	 */
	public static Id getId(String Name) {
		Profile rt = getProfile(Name);
		if (rt != null) {
			return rt.id;
		} 
		return null;
	}
	
	private static void refreshCache() {
		Map<String, Profile> newCache = new Map<String, Profile>();
		
		for (Profile rt : [ Select p.UserType, p.Name, p.Id From Profile p ]) {
			newCache.put(rt.Name, rt);
			newCache.put(rt.Id, rt);
		}
		currentCache = newCache;
	}
}