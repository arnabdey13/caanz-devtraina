/*
    Developer: WDCi (KH)
    Date: 23/06/2016
    Task #: Resource Booking update/create permission set
    
    Change History:
    LCA-724 8/Jul/2016 - WDCi Lean: set cpd hours and create education record
    LCA-727 13/Jul/2016 - WDCi KH: update Resource Booking start time & end time from session start time & end time
    LCA-791 21/Jul/2016 - WDCi KH: Include resource booking check to avoid multiple resource booking in the list for update
    LCA-819 08/Sept/2016 - WDCi KH: update education qualifying hour for PPP CPD allocation task
    LCA-1057 15/Nov/2016 - WDCi Lean: Set default value for education record creation
*/
public with sharing class luana_ResourceBookingHandler{
    
    public static Map<Id, Id> processedRecordIds = new Map<Id, Id>();

    public static void initiateFlow(List<LuanaSMS__Resource_Booking__c> newList, Map<Id, LuanaSMS__Resource_Booking__c> newMap, List<LuanaSMS__Resource_Booking__c> oldList, Map<Id, LuanaSMS__Resource_Booking__c> oldMap, boolean isInsert, boolean isUpdate, boolean isDelete, boolean isUndelete, boolean isBefore, boolean isAfter){
        
        if(isBefore){
            if(isInsert){
                setCPDHours(newList, oldMap); //LCA-724
                setStartEndTime(newList, oldMap); //LCA-727
            }
        } else if(isAfter){
            
            Set<String> resourceBookingId = new Set<String>();
            
        
            if(isInsert){
                //LCA-791
                for(LuanaSMS__Resource_Booking__c rb: newList){
                    if(!processedRecordIds.containsKey(rb.Id)){
                        processedRecordIds.put(rb.Id, rb.Id);
                        resourceBookingId.add(rb.LuanaSMS__Contact_Trainer_Assessor__c);
                    }
                }
            
                if(!resourceBookingId.isEmpty()){//LCA-791
                    assignContributorPermission(newList);
                    createEducationRecord(newList, oldMap); //LCA-724
                }
            } else if(isUpdate){
                //LCA-791
                for(LuanaSMS__Resource_Booking__c rb: newList){
                    if(!processedRecordIds.containsKey(rb.Id)){
                        processedRecordIds.put(rb.Id, rb.Id);
                        resourceBookingId.add(rb.LuanaSMS__Contact_Trainer_Assessor__c);
                    }
                }
            
                if(!resourceBookingId.isEmpty()){//LCA-791
                    assignContributorPermission(newList);
                    createEducationRecord(newList, oldMap); //LCA-724
                }
            }
            
        } 
    }
    
    public static void assignContributorPermission(List<LuanaSMS__Resource_Booking__c> newList){
        Set<String> contId = new Set<String>();
        
        for(LuanaSMS__Resource_Booking__c newRb: newList){
            contId.add(newRb.LuanaSMS__Contact_Trainer_Assessor__c);
        }
        
        Luana_Extension_Settings__c defContributorCommPermSetId = Luana_Extension_Settings__c.getValues('Default_ContCom_PermSet_Id');
        
        if(!contId.isEmpty()){
            if(defContributorCommPermSetId != null){
                updateCommUserPermSet(contId, defContributorCommPermSetId.Value__c);
            }
        }
    }
    
    @future
    public static void updateCommUserPermSet(Set<String> contId, String contCommPermSetId){
        List<User> users = new List<User>();
        
        List<User> nzicaUsers = [select Id, Name, Profile.Name, Contact.Id from User Where contactId in: contId];
        Map<String, PermissionSetAssignment> userPermMap = new Map<String, PermissionSetAssignment>();
        for(PermissionSetAssignment psa : [SELECT AssigneeId, PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId IN: nzicaUsers]) {
            String longId = '' + psa.PermissionSetId;
            userPermMap.put(psa.AssigneeId + '|' + longId.left(15), psa);
        }
        
        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(User u: nzicaUsers){
            
            if(!userPermMap.containsKey(u.Id + '|' + contCommPermSetId)){
                psaList.add(new PermissionSetAssignment(AssigneeId=u.Id, PermissionSetId=contCommPermSetId));
            }
        }
        
        insert psaList;
    }
    
    //LCA-727
    public static void setStartEndTime(List<LuanaSMS__Resource_Booking__c> newList, Map<Id, LuanaSMS__Resource_Booking__c> oldMap){
        
        Set<Id> sessionIds = new Set<Id>();
        for(LuanaSMS__Resource_Booking__c rb : newList) {
            sessionIds.add(rb.LuanaSMS__Session__c);
        }
        
        Map<Id, LuanaSMS__Session__c> sessionsMap = new Map<Id, LuanaSMS__Session__c>();
        for(LuanaSMS__Session__c session : [SELECT Id, RecordType.Name, LuanaSMS__End_Time__c, LuanaSMS__Start_Time__c, Course_Delivery_Location__c, Course_Delivery_Location__r.Course__c FROM LuanaSMS__Session__c WHERE Id IN: sessionIds]) {
            sessionsMap.put(session.Id, session);
        }
        
        for(LuanaSMS__Resource_Booking__c rb : newList){
            if(sessionsMap.containsKey(rb.LuanaSMS__Session__c)){
                rb.LuanaSMS__End_time__c = sessionsMap.get(rb.LuanaSMS__Session__c).LuanaSMS__End_Time__c;
                rb.LuanaSMS__Start_time__c = sessionsMap.get(rb.LuanaSMS__Session__c).LuanaSMS__Start_Time__c;
                
                //LCA-1057 added course mapping from CDL. We can't do from Course Session as there will be multiple records
                if(sessionsMap.get(rb.LuanaSMS__Session__c).Course_Delivery_Location__c != null){
                	rb.LuanaSMS__Course__c = sessionsMap.get(rb.LuanaSMS__Session__c).Course_Delivery_Location__r.Course__c;
                }
            }        
        }
    }
    
    //LCA-724
    public static void setCPDHours(List<LuanaSMS__Resource_Booking__c> newList, Map<Id, LuanaSMS__Resource_Booking__c> oldMap){
        
        Set<Id> resourceIds = new Set<Id>();
        Set<Id> sessId = new Set<Id>();
        
        for(LuanaSMS__Resource_Booking__c rb : newList){
            resourceIds.add(rb.LuanaSMS__Resource__c);
            sessId.add(rb.LuanaSMS__Session__c);
        }
        
        //LCA-819 to get all sessions
        List<LuanaSMS__Session__c> sessions = [Select Id, Course_Delivery_Location__c, Course_Delivery_Location__r.Course__c, Course_Delivery_Location__r.Course__r.RecordType.DeveloperName, 
                                                Course_Delivery_Location__r.Course__r.LuanaSMS__Program_Offering__r.Workshop_Eligibility_for_CPD_Hours__c from LuanaSMS__Session__c where Id in: sessId and Course_Delivery_Location__r.Course__r.LuanaSMS__Program_Offering__r.Workshop_Eligibility_for_CPD_Hours__c != null];

        Map<Id, LuanaSMS__Resource__c> relatedResources = new Map<Id, LuanaSMS__Resource__c>([select id, CPD_Hours__c from LuanaSMS__Resource__c where CPD_Hours__c > 0 and id in: resourceIds]);

        
        Map<Id, LuanaSMS__Session__c> sessionMap = new Map<Id, LuanaSMS__Session__c>();
        
        for(LuanaSMS__Session__c sess: sessions){
            sessionMap.put(sess.Id, sess);
        }
        
        //LCA-819 update allocation CPD Hours from workshop
        for(LuanaSMS__Resource_Booking__c rb : newList){
            if(sessionMap.containsKey(rb.LuanaSMS__Session__c) && sessionMap.get(rb.LuanaSMS__Session__c).Course_Delivery_Location__r.Course__r.RecordType.DeveloperName == 'PPP'){
                if(rb.LuanaSMS__Resource__c != null && relatedResources.containsKey(rb.LuanaSMS__Resource__c)){
                    if(sessionMap.containsKey(rb.LuanaSMS__Session__c)){
                        rb.CPD_Hours__c = relatedResources.get(rb.LuanaSMS__Resource__c).CPD_Hours__c/sessionMap.get(rb.LuanaSMS__Session__c).Course_Delivery_Location__r.Course__r.LuanaSMS__Program_Offering__r.Workshop_Eligibility_for_CPD_Hours__c;
                    }
                }
            }else{
                if(rb.LuanaSMS__Resource__c != null && relatedResources.containsKey(rb.LuanaSMS__Resource__c)){
                    rb.CPD_Hours__c = relatedResources.get(rb.LuanaSMS__Resource__c).CPD_Hours__c;
                }
            }
        }
    }
    
    //LCA-724
    public static void createEducationRecord(List<LuanaSMS__Resource_Booking__c> newList, Map<Id, LuanaSMS__Resource_Booking__c> oldMap){
        
        List<Education_Record__c> newEducationRecords = new List<Education_Record__c>();
        
        Luana_Extension_Settings__c defaultUniName = Luana_Extension_Settings__c.getValues('Education Record Default University');
        
        Account caanzAccount;
        
        for(Account acc : [select id, Name from Account where Name =: defaultUniName.Value__c and RecordType.DeveloperName = 'Business_Account' limit 1]){
            caanzAccount = acc;
        }
        
        Set<Id> sessionIds = new Set<Id>();
        Map<Id, LuanaSMS__Session__c> rbSessions = new Map<Id, LuanaSMS__Session__c>();
        
        for(LuanaSMS__Resource_Booking__c rb : newList){
        	if(rb.LuanaSMS__Session__c != null){
        		sessionIds.add(rb.LuanaSMS__Session__c);
        	}
        }
        
        if(!sessionIds.isEmpty()){
        	rbSessions = new Map<Id, LuanaSMS__Session__c>([select Id, LuanaSMS__Venue__c, LuanaSMS__Venue__r.BillingState, LuanaSMS__Venue__r.BillingCountry from LuanaSMS__Session__c where id in: sessionIds]);
        }
        
        for(LuanaSMS__Resource_Booking__c rb : newList){
            
            LuanaSMS__Resource_Booking__c oldRB;
            
            if(oldMap != null && oldMap.containsKey(rb.Id)){
                oldRB = oldMap.get(rb.Id);
            }
            
            if(rb.Allocate_CPD_Hours__c && (oldRB == null || !oldRB.Allocate_CPD_Hours__c) && rb.CPD_Hours__c != null && rb.CPD_Hours__c > 0){
                
                if(caanzAccount != null){
                    Education_Record__c eduRecord = new Education_Record__c();
                    
                    eduRecord.University_professional_organisation__c = caanzAccount.Id;
                    eduRecord.Enter_university_instituition__c = caanzAccount.Name;
                    eduRecord.Event_Course_name__c = rb.Session_Name__c;
                    eduRecord.Qualifying_hours__c = rb.CPD_Hours__c;
                    eduRecord.Date_Commenced__c = rb.LuanaSMS__Start_Time__c.date();
                    eduRecord.Date_completed__c = rb.LuanaSMS__End_Time__c.date();
                    eduRecord.Resource_Booking__c = rb.Id;
                    eduRecord.Contact_Student__c = rb.LuanaSMS__Contact_Trainer_Assessor__c;
                    eduRecord.Community_User_Entry__c = false;
                    
                    //LCA-1057
                    eduRecord.Qualifying_hours_type__c = 'Formal/Verifiable';
                    eduRecord.Verified__c = true;
                    eduRecord.Training_and_development__c = true;
                    
                    if(rbSessions.containsKey(rb.LuanaSMS__Session__c)){
                    	eduRecord.State__c = rbSessions.get(rb.LuanaSMS__Session__c).LuanaSMS__Venue__r.BillingState;
                    	eduRecord.Country__c = rbSessions.get(rb.LuanaSMS__Session__c).LuanaSMS__Venue__r.BillingCountry;
                    }
                    //LCA-1057
                    
                    newEducationRecords.add(eduRecord);
                    
                } else {
                    rb.addError('Account named \'' + defaultUniName.Value__c + '\' is not found.');
                }
            }
        }
        
        if(newEducationRecords.size() > 0){
            insert newEducationRecords;
        }
    }
    
}