/**
    Developer: WDCi (Lean)
    Date: 29/April/2016
    Task #: luana_CommUserUtil
    
    Change history
    LCA-921 25/08/2016 WDCi - KH, include person email
 */
@isTest
private class luana_CommUserUtilTest {
    
    public static Account businessAccount;
    
    public static Account member;
    public static User memberUser;
    public static Luana_DataPrep_Test dataPrep;
    public static luana_CommUserUtil userUtil {get; set;} 
    
    public static LuanaSMS__Student_Program__c stuProgm {get; set;}
    public static LuanaSMS__Course__c course {get; set;}
    public static LuanaSMS__Delivery_Location__c devLocation {get; set;}
    public static List<LuanaSMS__Subject__c> subjList {get; set;}
    
    private static String classNamePrefixLong = 'luana_CommUserUtilTest';
    private static String classNamePrefixShort = 'lcuu';
    
    public static LuanaSMS__Program_Offering__c po;
    
    public static void initial(){
        //Create all the custom setting
        dataPrep = new Luana_DataPrep_Test();
        List<Luana_Extension_Settings__c> extensionSettings = dataPrep.prepLuanaExtensionSettingCustomSettings();
        insert extensionSettings;

        member = dataPrep.generateNewApplicantAcc('Jeo','Black_' + classNamePrefixShort, 'Full_Member');
        member.personEmail = 'jeo_1_'+ classNamePrefixShort+'@gmail.com';
        member.Affiliated_Branch_Country__c = 'Australia';
        member.Membership_Class__c = 'Full';
        member.Assessible_for_CA_Program__c = true;
        insert member;
    }
    
    public static void setup(){
        businessAccount = dataPrep.generateNewBusinessAcc('Com_' + classNamePrefixShort, 'Business_Account', 'Chartered Accounting', '1111', '123456887', 'NZICA', 'sampel street', 'Active');
        insert businessAccount;
        
        //Create 'NZICA Community Login User' user
        //memberUser = dataPrep.generateNewApplicantUser('Black_' + classNamePrefixLong, classNamePrefixShort, member, dataPrep.getCustCommProfId());
        //insert memberUser;
        //LCA-921
        memberUser = [Select Id, Email, FirstName, LastName, UserName, Name from User Where Email =: 'jeo_1_'+ classNamePrefixShort+'@gmail.com' limit 1];
        
        userUtil = new luana_CommUserUtil(memberUser.Id);
        System.debug('****userUtil.custCommAccId: ' + userUtil.custCommAccId);

        List<Employment_History__c> ehList = new List<Employment_History__c>();
        for(integer i = 0; i < 5; i++){
            ehList.add(dataPrep.createNewEmploymentHistory(userUtil.custCommAccId, businessAccount.Id, 'Closed', 'Engineer'));
        }

        insert ehList;
        System.debug('****ehList: ' + ehList);
        
        List<Payment_Token__c> ptList = new List<Payment_Token__c>();
        for(Employment_History__c eh: ehList){
            ptList.add(dataPrep.createNewPaymentToken(userUtil.custCommAccId, null, System.today(), false));
        }
        insert ptList;
        System.debug('*******ptList: ' + ptList);

        //Create traning org
        LuanaSMS__Training_Organisation__c trainingOrg = dataPrep.createNewTraningOrg('CAANZ_TEST', 'CAANZ_TEST', 'CAANZ_TEST', 'St 123', 'Wollonggong', '2244');
        insert trainingOrg;
        
        //Create Delivery Location
        devLocation = dataPrep.createNewDeliveryLocationRecord('Australia - Western Australia_' + classNamePrefixShort, 'Australia - Western Australia_' + classNamePrefixLong, trainingOrg.Id, '5500', 'Australia');
        
        //Create program
        LuanaSMS__Program__c progrm = dataPrep.createNewProgram('DipCA_Test', 'Graduate Diploma of Chartered Accounting_' + classNamePrefixLong, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        insert progrm;
        
        //Create Product
        List<Product2> prodList = new List<Product2>();
        prodList.add(dataPrep.createNewProduct('FIN_AU_Test', 'AU0001'));
        prodList.add(dataPrep.createNewProduct('FIN_NZ_Test', 'NZ0001'));
        prodList.add(dataPrep.createNewProduct('FIN_INT_Test', 'INT0001'));
        insert prodList;

        
        //create price book entry
        /* Lean - 20/05/2016 - disabling to avoid lock contention
        List<PricebookEntry> pbes = new List<PricebookEntry>();
        pbes.add(dataPrep.createPricebookEntry(Test.getStandardPricebookId(), prodList[0].Id, 1300));
        pbes.add(dataPrep.createPricebookEntry(Test.getStandardPricebookId(), prodList[1].Id, 1300));
        pbes.add(dataPrep.createPricebookEntry(Test.getStandardPricebookId(), prodList[2].Id, 1300));

        insert pbes;
        */
        
        //Create Program Offering
        po = dataPrep.createNewProgOffering('DipCA_' + classNamePrefixLong, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Program'), progrm.Id, trainingOrg.Id, prodList[0].Id,prodList[1].Id, prodList[2].Id, 1, 5);
        insert po;
        
        Product_Permission__c pp = new Product_Permission__c();
        pp.Grant_Permission__c = 'Member Community';
        pp.Program_Offering__c = po.Id;
        pp.Member_Criteria__c = 'Assessible for CA Program';
        
        Product_Permission__c emppp = new Product_Permission__c();
        emppp.Grant_Permission__c = 'Employer Community';
        emppp.Program_Offering__c = po.Id;
        emppp.Member_Criteria__c = 'Assessible for CA Program';
        
        
        insert new List<Product_Permission__c>{pp, emppp};
        
        /*LCA-1169 this is not allowed, MIXED_DML_OPERATION error
        Luana_Extension_Settings__c memberPerm = Luana_Extension_Settings__c.getInstance('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c nonmemberPerm = Luana_Extension_Settings__c.getInstance('Default_NonMember_PermSet_Id');
        
        PermissionSetAssignment memberpsa = new PermissionSetAssignment(AssigneeId = memberUser.Id, PermissionSetId = memberPerm.Value__c);
        PermissionSetAssignment nonmemberpsa = new PermissionSetAssignment(AssigneeId = memberUser.Id, PermissionSetId = nonmemberPerm.Value__c);
        
        insert new List<PermissionSetAssignment>{memberpsa, nonmemberpsa};*/
    }
    
    static testMethod void testCommUserUtil() {
        
        Test.startTest();
            initial();
        Test.stopTest();
        setup();
        
        Map<Id, String> permissionSetMap;
        //List<User> nzicaUsers = [select Id, Name, Profile.Name, Contact.Id from User Where Id =: memberUser.Id];
        //Test.startTest();
        
            System.runAs(memberUser){
                
                luana_EmploymentUtil.getUserEmploymentHistory(String.valueOf(userUtil.custCommAccId));
                luana_EmploymentUtil.getEmployees(userUtil.custCommAccId, 'Joe', userUtil.custCommAccId);
                
                List<Account> memberAcc = [Select Id, Affiliated_Branch_Country__c from Account Where Id =: userUtil.custCommAccId limit 1];
                System.assertEquals(memberAcc[0].Affiliated_Branch_Country__c, 'Australia', 'Luana_AT_MemberEnrolment_Test_0001_1: Expect Affiliated Branch Country is Australia');
                
                permissionSetMap = new Map<Id, String>();
                for(PermissionSet ps: [select Id, Name from PermissionSet Where Name in ('Luana_MemberCommunityPermission', 'Luana_NonMemberCommunityPermission', 'Luana_EmployerCommunityPermission')]){
                    permissionSetMap.put(ps.Id, ps.Name);
                }
                
                memberAcc[0].RecordTypeId = dataPrep.getRecordTypeIdMap('Account').get('Full_Member');
                memberAcc[0].Membership_Class__c = 'Provisional';
                update memberAcc[0];

            }
            
        //Test.stopTest();
        //LCA-1169 hack here to pass validation as we are not allowed to insert perm set
        userUtil.hasAccessToMember = true;
        userUtil.hasAccessToNonMember = true;
        
        userUtil.getProgramOfferingPermission(new Set<Id>{po.id});
        userUtil.getUserPhoto(null, memberUser.Id);
    }
}