/**
* @author Andrew Kopec
* @author clonned from Jannis Bott REST CalloutService
* @date 17/08/2017
*
* @description  Handles SOAP callouts. Retained the error handling for consistency   
*/
public with sharing class CalloutSOAPService {
    
 	public static final String CONFIGURATION_LOAD_ERROR_HEADER = 'Callout exception!';
    public static final String CONFIGURATION_LOAD_ERROR_BODY = 'An exception occured while loading callout configuration data. The error is: {0}';
    public static final String CALLOUT_ERROR_MESSAGE = 'Callout response did not return 200 when calling ther enpoint address URL: {0}. The status code is: {1}';
    public static final String CALLOUT_EXCEPTION_MESSAGE = 'An exception occured while calling out Experian Web Service. The error is: {0}';
    public static final String CALLOUT_CONFIG_DOES_NOT_EXIST = 'No callout configuration data found.';
    public static final List<Profile> SYSTEM_ADMIN_ID = [SELECT Id FROM Profile WHERE Name = 'System Administrator' Limit 1];
    public static final List<User> SYSTEM_ADMINS = [SELECT Email FROM User WHERE ProfileId = :SYSTEM_ADMIN_ID AND LastName = 'Kopec'];
    
    public static final String CALLOUT_SOAP_FAULT_PATH = '/soap:Envelope/soap:Body/soap:Fault';
    
    public static final String CONTENT_TYPE = 'Content-Type';
    public static final String AUTH_TOKEN   = 'Auth-Token';
    public static final String SOAP_ACTION  = 'SOAPAction';

    
    /**
    * @description  executes callout - executeHTTPCallout wrapper
    * @param        String configurationName - the name of the custom metadata configuration that contains the callout config
    * @param        String SoapAction - SOAP method to be executed  
    * @param        String body - the content that should be send as body
    * @return       String - the response of the callout
    */
    public static String executeCallout(String configurationName, String soapAction, String body) {
    	HttpResponse response = executeHTTPCallout(configurationName, soapAction, body);
        if (response==null){
        	return 'CalloutSOAPServiceError: '+ CalloutSOAPService.CalloutResult.UNEXPECTED_EXCEPTION; }
        try {
            Integer statusCode = response.getStatusCode();
            if (statusCode != 200) {
            	return 'CalloutSOAPServiceError: ' + getSOAPFaultMessage (response.getBody()); }
        } catch(Exception ex) {
            return 'CalloutSOAPServiceError: Failed to retrieve status from HTTP Response:' + ex;
        }
        return response.getBody();
    }
        
 
    /**
    * @description  gets the callout configuration and executes HTTP callout 
    * @param        String configurationName - the name of the custom metadata configuration that contains the callout config
    * @param        String SoapAction - SOAP method to be executed  
    * @param        String body - the content that should be send as body
    * @return       HttpResponse - the response of the HTTP callout
    */
    public static HttpResponse executeHTTPCallout(String configurationName, String soapAction, String body) {

        Callout_Configuration__mdt calloutConfig;
        HttpRequest req;
        Http h;
        HttpResponse response;

        // prepare request based on configuration metadata
        // test for config and request
        try {
            calloutConfig = getConfigurationData(configurationName);
            // System.debug('CalloutSOAPService: Callout Configuration: ' + calloutConfig);
            req = createSOAPRequest(calloutConfig, soapAction, body);
            System.debug('CalloutSOAPService: REQUEST: ' + req);
            
        } catch(Exception ex) {
            	 System.debug('CalloutSOAPService REQUEST Exception ' + ex);
                 if (calloutConfig == null) {
                        sendEmail( 	getAdminEmails(SYSTEM_ADMINS), 
									CALLOUT_CONFIG_DOES_NOT_EXIST,
                       				String.format(CALLOUT_EXCEPTION_MESSAGE,
                                    	new List<String>{ ex.getMessage() }) );
                 }
        	return null;
        }
            
        h = new Http();             
		try {  

            response = h.send(req);
            System.debug('CalloutService: RESPONSE BODY ' + response.getBody());
            // If response is not successful send and email to admin with the error details.
            if (response.getStatusCode() != 200) {
                if (calloutConfig != null) {
                	sendEmail(
                        new List<String>{
                                calloutConfig.Failure_Notification_Email__c
                        },
                        CONFIGURATION_LOAD_ERROR_HEADER,
                        String.format(CALLOUT_ERROR_MESSAGE,
                                new List<String>{
                                        calloutConfig.Endpoint_URL__c,
                                        soapAction,
                                        String.valueOf(response.getStatusCode())
                                	}
                                )
                		);
                }
            }
            return response;
                
        } catch(Exception ex) {
            System.debug('CalloutSOAPService exception ' + ex);
            if (calloutConfig != null) {
                sendEmail(
                        new List<String>{
                                calloutConfig.Failure_Notification_Email__c
                        },
                    
                        CONFIGURATION_LOAD_ERROR_HEADER,
                        String.format(CALLOUT_EXCEPTION_MESSAGE,
                                new List<String>{
                                        ex.getMessage()
                                })
                );
            }
            return null;
        }
    }

    
    
    /**
    * @description  generates a http request for SOAP callout
    * @param        Callout_Configuration__mdt calloutConfig - the custom metadata configuration
    * @param        String SoapAction - method to be executed
    * @param        String body - the content that should be send as the body
    * @return       HttpRequest - the request ready for sending
    * @example      createSOAPRequest(calloutConfig, body);
    */
    private static HttpRequest createSOAPRequest(Callout_Configuration__mdt calloutConfig, String SoapAction, String body) {
        HttpRequest req = new HttpRequest();

        req.setEndpoint(calloutConfig.Endpoint_URL__c);
        req.setMethod(calloutConfig.Request_Method__c);

        req.setHeader(CONTENT_TYPE, calloutConfig.Content_Type__c);
        req.setHeader(SOAP_ACTION, SoapAction);
        if (!String.isEmpty(calloutConfig.Authentication_Token__c)) {
            req.setHeader(AUTH_TOKEN, calloutConfig.Authentication_Token__c);
        }      

        if (!String.isEmpty(body)) req.setBody(body);

        return req;
    }

    
    
    /**
    * @description  executes soql to retrieve the custom metadata configuration
    * @param        String configurationName - the name of the custom metadata configuration that needs to be queried
    * @return       Callout_Configuration__mdt - the custom metadata configuration
    * @example      getConfigurationData(configurationName);
    */
    private static Callout_Configuration__mdt getConfigurationData(String configurationName) {

        Callout_Configuration__mdt configuration = [
                SELECT Authentication_Token__c,
                        Endpoint_URL__c,
                        Timeout_In_Seconds__c,
                        Failure_Notification_Email__c,
                        Request_Method__c,
                        Content_Type__c
                FROM Callout_Configuration__mdt
                WHERE DeveloperName = :configurationName
        ];

        return configuration;
    }

        
    /**
    * @description  Function returns  Error Message from the Faulty SOAP call 
    *               takes XPath to find the Fault Node and determines Fault Code/String value
    * @param        String XML Response from HTTP Request
    *
    * @return       String Fault Message
    */
    public static String getSOAPFaultMessage (String responseXML) {
        XPath xp = new XPath(responseXML);
		DOM.XMLNode FaultInformation = xp.findFirst(xp.root, CALLOUT_SOAP_FAULT_PATH);
        String FaultMessage;
        if ( FaultInformation==null ) {
            System.debug('Error: Callout:getSOAPFaultMessage: Node not found');
            return null;}   
		
        try {
            String faultcode = FaultInformation.getChildElement('faultcode', null ).getText();
            String faultstring = FaultInformation.getChildElement('faultstring', null ).getText();
            FaultMessage  = faultcode + ' : ' + faultstring;
        } catch(Exception ex) {
            System.debug('Error: Seg:getSOAPFaultMessage: exception ' + ex);
            return null;
        }
        System.debug('getSOAPFaultMessage: ' + FaultMessage);
        return FaultMessage;
	}
    
    

    /**
    * @description  get list of admins emails
    * @param        List<User> list of admin users
    * @return       List<String> list of admin emails
    */
@TestVisible private static List<String> getAdminEmails(List<User> admins) {
		List<String> sysAdminsEmails = new List<String>();
        if (admins==null | admins.isEmpty())
            return sysAdminsEmails;
        
		for(User admin : admins ) {
			sysAdminsEmails.add(admin.Email);
		}
        return sysAdminsEmails;
    }

    
    
    /**
    * @description  sends emails
    * @param        List<String> notifyerEmail - list of receiver emails
    * @param        String subject - email subject
    * @param        String body - email body
    * @example      sendEmail(getAdminEmails(SYSTEM_ADMINS), CONFIGURATION_LOAD_ERROR_HEADER, String.format(CONFIGURATION_LOAD_ERROR_BODY, new List<String>{ex.getMessage()}));
    */
    public static void sendEmail(List<String> notifyerEmail, String subject, String body) {

        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setSubject(subject);
        email.setToAddresses(notifyerEmail);
        email.setPlainTextBody(body);

        if (!Test.isRunningTest()) {
            Messaging.sendEmail(new Messaging.Email[]{
                    email
            });
        }
    }

    public Enum CalloutResult {
        SUCCESS, READ_TIMEOUT, UNEXPECTED_EXCEPTION, INCORRECT_HTTP_REQUEST
    }
}