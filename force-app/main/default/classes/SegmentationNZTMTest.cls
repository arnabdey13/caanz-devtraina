@isTest
public class SegmentationNZTMTest {
	static testMethod void tryAKnownNZTM_One() {
        Map<String,decimal> output = SegmentationNZTM.getLatLong(1749308.71783085, 5427028.23177705);
        
        // Address: 1 Courtenay Place, Wellington, New Zealand
        Decimal expected_latitude = -41.2943229;
        Decimal expected_longitude = 174.7811771;
        
        // Ensure that the values are equal within 3 decimal places
        System.assertEquals(expected_latitude.setScale(3, System.RoundingMode.DOWN), output.get('Latitude').setScale(3, System.RoundingMode.DOWN));
    }

    
    static testMethod void tryAKnownNZTM_Two() {
        Map<String,decimal> output = SegmentationNZTM.getLatLong(1433474.03630687, 5268367.65300363);
        
        // Address: 80 Weld Street, Hokitika, 7810, New Zealand
        Decimal expected_latitude = -42.7192247;
        Decimal expected_longitude = 170.9639727;
        
        // Ensure that the values are equal within 3 decimal places
        System.assertEquals(expected_latitude.setScale(3, System.RoundingMode.DOWN), output.get('Latitude').setScale(3, System.RoundingMode.DOWN));
    }
}