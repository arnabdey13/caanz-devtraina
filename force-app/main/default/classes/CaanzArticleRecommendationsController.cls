/*------------------------------------------------------------------------------------
Author:        Sumit Gupta
Company:       Salesforce
Description:   Article recommendations component controller

History
Date            Author             Comments
--------------------------------------------------------------------------------------
29-05-2018     Sumit Gupta       	Initial Release
------------------------------------------------------------------------------------*/
public with sharing class CaanzArticleRecommendationsController {

	private static String QUERY_BASE = 'SELECT Id, Name, Summary__c, Article_Link__c, Published_Date__c, Source__c FROM Content_KB__c WHERE Active__c = true';
	private static Decimal LIMIT_DEFAULT = 100;

    private static Map<String, String> ICON_MAPPING = new Map<String, String>{
        'kohagreenIcon' => 'standard:article', 'blueIcon' => 'standard:article', 'greenIcon' => 'standard:article'
    };

    private static Map<String, String> ICON_COLOR_MAPPING = new Map<String, String>{
        'koha' => 'kohagreen', 'caanz' => 'blue', 'acuity' => 'green'
    };

	@AuraEnabled
    public static String getRecommendations(Decimal limitQuery){
    	List<Recommendation> recommendations = new List<Recommendation>();
    	try{
    		if(limitQuery == null) limitQuery = LIMIT_DEFAULT;
    		recommendations = getRecommendations(getArticles(limitQuery));
    	} catch(Exception ex){
 			System.debug(ex.getMessage());
    	} 
    	return JSON.serialize(recommendations);
    }

    private static List<Content_KB__c> getArticles(Decimal limitQuery){
        List<Content_KB__c> articles = new List<Content_KB__c>();
        List<String> topics = getApplicableTopics();
        if(!topics.isEmpty()){
            String query = QUERY_BASE + ' AND Sub_Topic__c INCLUDES (\'' + String.join(topics, '\',\'') + '\')';
            query += getAccountFilterCriteria() + ' ORDER BY Published_Date__c LIMIT ' + limitQuery;
            articles = Database.query(query);
        }
        return articles;
    } 

    private static List<Recommendation> getRecommendations(List<Content_KB__c> articles){
    	List<Recommendation> recommendations = new List<Recommendation>();
    	for(Content_KB__c article : articles){
    		recommendations.add(new Recommendation(article));
    	}
    	return recommendations;
    }

    private static List<String> getApplicableTopics(){
    	List<String> topics = new List<String>();
    	for(My_Preference__c myPreference : getMyPreferences()){
    		topics.add(myPreference.Name);
    	}
    	return topics;
    }

    private static String getAccountFilterCriteria(){
    	String criteria = '';
    	Account acc = getCurrentUserAccount();
    	if(acc != null){
    		if(String.isNotBlank(acc.Affiliated_Branch_Country__c)){
	    		criteria += ' AND Country__c INCLUDES (\'' + acc.Affiliated_Branch_Country__c + '\')';
	    	}
	    	if(!acc.Segmentations__r.isEmpty()){
                Segmentation__c seg = acc.Segmentations__r[0];
                if(String.isNotBlank(seg.Career_Stage__c)){
                    criteria += ' AND (Career_Stage__c = null OR Career_Stage__c = \'' + seg.Career_Stage__c + '\')';
                }
                if(String.isNotBlank(seg.Firm_Type__c)){
                    criteria += ' AND (Firm_Type__c = null OR Firm_Type__c = \'' + seg.Firm_Type__c + '\')';
                }
                if(String.isNotBlank(seg.Organisation_Type__c)){
                    criteria += ' AND (Org_Type__c = null OR Org_Type__c = \'' + seg.Organisation_Type__c + '\')';
                }
            }
    	}
    	return criteria;
    }

    private static Account getCurrentUserAccount(){
    	List<Account> accounts = [
    		SELECT Affiliated_Branch_Country__c, 
    		(SELECT Id, Career_Stage__c, Firm_Type__c, Organisation_Type__c FROM Segmentations__r LIMIT 1) 
    		FROM Account WHERE Id IN (SELECT AccountId FROM User WHERE Id =: UserInfo.getUserId()) LIMIT 1
    	];
    	return !accounts.isEmpty() ? accounts.get(0) : null;
    }

    private static List<My_Preference__c> getMyPreferences(){
    	return [
    		SELECT Name FROM My_Preference__c 
    		WHERE User__c =: UserInfo.getUserId()
    		AND (Type__c != 'Boolean' OR Value__c = 'true')
    	];
    }

    public class Recommendation{
    	Content_KB__c article;
        String icon, iconClass;

    	public Recommendation(Content_KB__c article){
    		this.article = article;
            getIcon(getIconClass(getSource()));
    	}

        private void getIcon(String iconClass){
            icon = ICON_MAPPING.containsKey(iconClass) ? ICON_MAPPING.get(iconClass) : 'standard:product';
        }

        private String getIconClass(String source){
            return iconClass = (ICON_COLOR_MAPPING.containsKey(source) ? ICON_COLOR_MAPPING.get(source) : 'caanz') + 'Icon';
        }

        private String getSource(){
            return (article.Source__c != null ? article.Source__c : '').toLowerCase().replaceAll(' ', '');
        }
    }
}