public class serviceAccessController {

	@AuraEnabled    
    public static boolean getServiceAccess(String serviceName){
        //check user type - internal users have full access, chatter users have no access
        String currentUserType=UserInfo.getUserType();
        system.debug('###serviceAccessController:getServiceAccess:currentUserType='+currentUserType);
        
        if(currentUserType=='Standard')return true;
        if(currentUserType=='CsnOnly')return false;

        //if portal or communities user then lookup against Community User Access metadata
        User currentUser=[select Contact.Account.Membership_Type__c from User where Id=:UserInfo.getUserId()];
        system.debug('###serviceAccessController:getServiceAccess:currentUser='+currentUser);
        if(currentUser.Contact.Account.Membership_Type__c!=null)system.debug('###serviceAccessController:getServiceAccess:currentUser.Contact.Account.Membership_Type__c='+currentUser.Contact.Account.Membership_Type__c);
        
		boolean hasAccess=false;
        List<Community_User_Access__mdt> serviceAccess = [select DeveloperName
                                                          , Member_access__c
                                                          , Non_member_access__c
                                                          , Past_member_access__c
                                                          , Student_affiliate_access__c
                                                          from Community_User_Access__mdt
                                                          where DeveloperName=:serviceName limit 1];
        system.debug('###serviceAccessController:getServiceAccess:serviceAccess='+serviceAccess);
        if(serviceAccess!=null&&!serviceAccess.isEmpty()){
            if(currentUser.Contact.Account.Membership_Type__c=='Member'
              	&& serviceAccess[0].Member_access__c=='Yes'){
                     return true;
            }if(((currentUser.Contact.Account.Membership_Type__c=='Non member')||(currentUser.Contact.Account.Membership_Type__c=='Applicant'))
              	&& serviceAccess[0].Non_member_access__c=='Yes'){
                     return true;
            }if(currentUser.Contact.Account.Membership_Type__c=='Student Affiliate'
              	&& serviceAccess[0].Student_affiliate_access__c=='Yes'){
                     return true;
            }if(currentUser.Contact.Account.Membership_Type__c=='Past Member'
              	&& serviceAccess[0].Past_member_access__c=='Yes'){
                     return true;
            }
        }
        return hasAccess;
    }
    
}