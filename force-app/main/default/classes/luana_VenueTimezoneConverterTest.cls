// LCA-916 Session Display Venue Timezone (Temporary Workaround)
// Test class for Apex Trigger - luana_VenueTimezoneConverter

@isTest(SeeAllData=false)
private class  luana_VenueTimezoneConverterTest {
    static testMethod void testSess() {
            
        Account acc = new Account(Name='Test Venue', BillingStreet='test', Venue_Timezone__c='Australia/Sydney');
        insert acc;
        
        LuanaSMS__Session__c sess = new LuanaSMS__Session__c(Name='Test Sess', LuanaSMS__Start_Time__c=System.now(), LuanaSMS__End_Time__c=System.now(), LuanaSMS__Venue__c=acc.Id);
        insert sess;
    }
    
}