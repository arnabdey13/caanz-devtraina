@isTest
private class testWebSiteTickerController {
    static testMethod void unitTestWebSiteTickerController() {
        WebSiteTickerController w = new WebSiteTickerController();
        List<Website_Ticker__c> tickers = new List<Website_Ticker__c>();
        for(Integer i = 0; i<5; i++) {
        	Website_Ticker__c wt = new Website_Ticker__c();
        	wt.Name = 'test - ' + String.valueOf(i);
        	wt.Active__c = true;
        	wt.Background_Image__c = 'URL for Image';
        	wt.Link__c = 'Link for Image';  
        	wt.Order__c = i;      	
        	tickers.add(wt);
        }
        Database.insert(tickers);
        w.getWebsiteTickers();
    }
}