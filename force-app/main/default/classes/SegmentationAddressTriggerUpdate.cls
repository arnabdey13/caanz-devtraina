/**
*	Project:			CAANZ Internal
*	Project Name: 		Segmentation
*	Project Task Name: 	Account trigger Handler After Update
*	Developer: 			akopec
*	Initial Date: 		October 2017
*	Revisions			R01 akopec May 2018 added Education City Centre	
*   Description: 		Executes updates to Account and Geo Segmentation for the correspondent Account ID using queueable interface
*   ToDo 				Add the testing for actual segments
**/

public without sharing class SegmentationAddressTriggerUpdate implements Queueable, Database.AllowsCallouts {
	
	private Map<Id, String> SegmentAcctToUpdate;
    public static Boolean debugFlag = false;
    
    /** 
     *   Copy Constructor
     **/
	public SegmentationAddressTriggerUpdate(Map<Id, String> segmentAccountForUpdate){                    
        if ( segmentAccountForUpdate.isEmpty())
           return; 

        SegmentAcctToUpdate = new Map<Id, String>(segmentAccountForUpdate);
        if (debugFlag) System.debug('Segments to Update:' + SegmentAcctToUpdate);
    }

	/**
	 * Get Affiliated Branch for Territorial Authority from Custom Metadata
	 * We do not want hard coded rules
	 * The new Affiliated Branch definition is based on Territorial Authorities
	 * 
	 **/
	public static String getAffiliatedBranch( String Region, String TerritorialAuthority) {
       if (String.isBlank(TerritorialAuthority)) 
           return NULL;

       List<NZ_Affiliated_Branch__mdt> affBranch = [select Id, Territorial_Authority__c, Affiliated_Branch__c
        										 	from NZ_Affiliated_Branch__mdt 
        										 	where Territorial_Authority__c = :TerritorialAuthority ];  
        if (affBranch.isEmpty()) {
            if (debugFlag) System.debug('No Affiliated Branch for"' + TerritorialAuthority);
            return null;
        }
        
		return affBranch[0].Affiliated_Branch__c;
    }
    
    /**
	 * R01 Get Education City Centre from Reference table
	 **/
	public static String getEducationCityCentreNZ( String Region, String TerritorialAuthority) {
       if (String.isBlank(TerritorialAuthority)) 
           return NULL;

       List<Reference_Geography_to_City_Centre__c> cityCentre = [select Id, City_Centre__c, NZ_Territorial_Authority__c, NZ_Regional_Council__c
        										 	from Reference_Geography_to_City_Centre__c 
        										 	where NZ_Territorial_Authority__c = :TerritorialAuthority ];  
        if (cityCentre.isEmpty()) {
            if (debugFlag) System.debug('No City Centre for:' + TerritorialAuthority);
            return null;
        }
		return cityCentre[0].City_Centre__c;
    }
    
    public static String getEducationCityCentreAU( ID SA1_ID) {
       if (SA1_ID==NULL) 
           return NULL;

        List <Reference_Geography_Australia__c> refAU = [SELECT Id, SA1__c, SA2_Name__c, SA3_Name__c, SA4_Name__c from Reference_Geography_Australia__c WHERE Id = : SA1_ID];
        if (refAU.isEmpty()){
            if (debugFlag) System.debug('No Ref Au for :' + SA1_ID);
            return null;
        }
        	 

        List<Reference_Geography_to_City_Centre__c> cityCentre = [select Id, City_Centre__c, SA2__c, SA3__c, SA4__c
        										 	from Reference_Geography_to_City_Centre__c 
        										 	where SA2__c = :refAU[0].SA2_Name__c OR SA3__c=:refAU[0].SA3_Name__c OR SA4__c = :refAU[0].SA4_Name__c ];  
        if (cityCentre.isEmpty()) {
            if (debugFlag) System.debug('No City Centre for:' + refAU[0].SA2_Name__c + ':' + refAU[0].SA3_Name__c + ';' + refAU[0].SA4_Name__c);
            return null;
        }
        
		if (debugFlag) System.debug('City Centre ' + cityCentre[0].City_Centre__c + ' for:' + refAU[0].SA2_Name__c + ':' + refAU[0].SA3_Name__c + ';' + refAU[0].SA4_Name__c);
		return cityCentre[0].City_Centre__c;
    }
    
     /**
	 * R01 Get Education City Centre End
	 **/

    
    /*********************************************************************************************
    * execute Queueable
    * @description  Parse address elements passed as primitive types and execute
    *               Web Service call to obtain Extended Data Set Details:
    *               Latitude, Longitude, Meshblock, Regional Council and Territorial Authority
    *               For Australia - convert Meshblock to SA1
    *               For New Zealand - convert X,Y to Latitude Longitude
    * 		        Update Segmentation for Person Account
    * 				Update Account Regional Council, TA and Affiliated Branch for NZ
    * 				Update Account Lat Long for Business Accounts
    * 
    *		parse the string with address  to determine country and address type
    *		example 'AU:RES:100 Pacific Hwy, Chatswood 2067'
	*		Country=AU, AddressType= Residential, Address 100 Pacific Hwy, Chatswood 2067
    *
    **********************************************************************************************/ 
	public void execute(QueueableContext context) {
        perform_call();
    }
        
    public void perform_call() {
        // Check Custom Setting if Status is Active
        if(!SegmentationAddressTriggerHandler.getConfigAddressUpdateStatus('SegmentationAddressUpdate' )) return; //On/off
        if (SegmentAcctToUpdate==null) return;
        if (debugFlag) System.debug('SegmentAcctToUpdate : ' + SegmentAcctToUpdate);    
        
        if(SegmentationAddressTriggerHandler.getConfigAddressUpdateDebug('SegmentationAddressTriggerDebug' )) 
            debugFlag=true; //Turn on debugging
                
        String Country='';
        String Address='';
        String AddrType='';
        
		List<Segmentation__c> segmentsToUpdateList = new List <Segmentation__c>();
        List<Segmentation__c> segmentsToInsertList = new List <Segmentation__c>(); 
        List<Segmentation__c> segmentsUpdate  = [select Id, Geo_AU_SA1__c, Geo_NZ_Meshblock__c, Geo_Confidence_Flag__c, Account__c
        										 from Segmentation__c 
        										 where Account__c IN :SegmentAcctToUpdate.keySet() ];
            
        List<Account> accountsToUpdateList = new List <Account>();  
        Map<Id, Account> acctU  = new Map<Id, Account> ([select Id, BillingLatitude, BillingLongitude, BillingGeocodeAccuracy,
                                                         			ShippingLatitude, ShippingLongitude, ShippingGeocodeAccuracy
        										         from Account 
                                                         where Id IN :SegmentAcctToUpdate.keySet() ]);
 
        Map<Id, Account> acctNZ = new Map<Id, Account> ([select Id, NZ_Local_Govt_Regional_Council__c, NZ_Local_Govt_Territorial_Authority__c, 
                                                                Affiliated_Branch_Country__c, Affiliated_Branch__c, Education_City_Centre__c		// R01 	Education_City_Centre__c
        						 			             from Account 
                                                         where Id IN :SegmentAcctToUpdate.keySet() ]);
  		// R01 
		Map<Id, Account> acctAU = new Map<Id, Account> ([select Id, Education_City_Centre__c		
        						 			             from Account 
                                                         where Id IN :SegmentAcctToUpdate.keySet() ]);
        if (debugFlag) System.debug('City Centre Account: ' + acctAU);
        
        //######################## Prepare QAS web service call #####################################
            
        for (Id key : SegmentAcctToUpdate.keySet()) {
			String addrToUpdate = SegmentAcctToUpdate.get(key);
            // parse the string with address  to determine country and address type
            if (String.isBlank(addrToUpdate) || addrToUpdate.length() < 8)
                continue;     
            Country = addrToUpdate.substring(0,2); 
            if (Country !='AU' && Country != 'NZ')
                continue;  
            AddrType = addrToUpdate.substring(3,6);
            Address  = addrToUpdate.substring(7);
            
			if (debugFlag) System.debug('handleSegmentsAfterUpdateAsync Before QAS call -> Country:' + Country + ', Address:' + Address + ', Address Type:' + AddrType);
            
           	// ################# Execute QAS Call  ##########################
           	
			Map<String, String> addrElements = SegmentationQASServiceSOAP.runQASSearch(key, Address, Country);
            
            // ################# start processing QAS results ###############
            
            if (debugFlag) System.debug('After QAS call:' + addrElements);
            if (debugFlag) System.debug('After QAS Meshblock =:' + addrElements.get('MeshBlockCode'));
            if (!addrElements.isEmpty()) {

                // Case 1 ######### Res or Mail ######### update of the segmentation only
                if (AddrType == 'RES' || AddrType == 'MAL'){

                    Segmentation__c newSegment = new Segmentation__c();         
                    // check if Segmentation exists (is it Update or Insert)
                    if ( segmentsUpdate.isEmpty()) {
                        newSegment.Account__c = key;  
                    } else {
                        for ( Segmentation__c seg : segmentsUpdate){
                            if (seg.Account__c == key)
                                newSegment = seg;
                        }
            		}
                    if (debugFlag) System.debug('New Segment Initiated:' + newSegment);
                    
                    
                    // Clear existing segmentation if Address Processing Error and Segmentation exists
                    if (!String.isBlank(addrElements.get('ERROR')) ) {
                        if (segmentsUpdate.isEmpty() )
                            continue;
                        else {
                            newSegment.Geo_NZ_Meshblock__c = null;
                            newSegment.Geo_AU_SA1__c = null;
                            newSegment.Geo_Confidence_Flag__c = null;
                            if (debugFlag) System.debug('Geo Segmentation clear - Address Processing Error ID= ' + key);
                            segmentsToUpdateList.add(newSegment);
                            continue;
                        }
                    } 
                    
                    if (Country=='NZ'){
                        //  experian forces  to use code becauces MeshBlockId is not returned by some engines
                        String MeshblockCode = addrElements.get('MeshBlockCode');
                        String Meshblock = 'MB ' + MeshblockCode.leftPad(7, '0');
                        if (debugFlag) System.debug('MeshblockNZ: ' + Meshblock);
                        if (!String.isBlank(Meshblock)){
                            newSegment.Geo_NZ_Meshblock__c = SegmentationAddressTriggerHandler.getNZMeshblockID(Meshblock);
                            newSegment.Geo_AU_SA1__c = null;
                        }

                        
                        // prepare for Update of NZ Regional Council & TA, make sure the Country Branch is correct
                        Account newNZAcct  =  acctNZ.get(key);
                        if (newNZAcct.Affiliated_Branch_Country__c == 'New Zealand') {
                            newNZAcct.NZ_Local_Govt_Regional_Council__c = addrElements.get('Regional Council Name');
                            newNZAcct.NZ_Local_Govt_Territorial_Authority__c = addrElements.get('Territorial Authority Name');
                            newNZAcct.Affiliated_Branch__c = getAffiliatedBranch( newNZAcct.NZ_Local_Govt_Regional_Council__c ,  
                                                                                 newNZAcct.NZ_Local_Govt_Territorial_Authority__c) ;
                            newNZAcct.Education_City_Centre__c = getEducationCityCentreNZ( newNZAcct.NZ_Local_Govt_Regional_Council__c ,  			// R01
                                                                                 		   newNZAcct.NZ_Local_Govt_Territorial_Authority__c) ;		// R01
							if (debugFlag) System.debug('NZ Account to update: ' + newNZAcct); //R02
                            accountsToUpdateList.add(newNZAcct);                               
                        }  
                    }
                    else if (Country=='AU') {
                        String MeshblockAU = addrElements.get('MeshBlockCode');
                        if (debugFlag) System.debug('MeshblockAU: ' + MeshblockAU);
                        if (!String.isBlank(MeshblockAU)){
                       		newSegment.Geo_AU_SA1__c = SegmentationAddressTriggerHandler.getAUSA1ID( MeshblockAU );
                            if (debugFlag) System.debug('Meshblock Geo_AU_SA1__c: ' + newSegment.Geo_AU_SA1__c);
                        	newSegment.Geo_NZ_Meshblock__c = null;
                        }
                        
                        // R01 start re-use the Geo_AU_SA1 from above
                        Account newAUAcct  =  acctAU.get(key);
                        if (debugFlag) System.debug('City Centre Account: ' + newAUAcct);
                        if (newSegment.Geo_AU_SA1__c != NULL ) {
							newAUAcct.Education_City_Centre__c = getEducationCityCentreAU( newSegment.Geo_AU_SA1__c);
							if (debugFlag) System.debug('New City Centre: ' + newAUAcct.Education_City_Centre__c);
                        	accountsToUpdateList.add(newAUAcct);                            
                        }
                        else {
                        	// clear it if required
                            if (newAUAcct.Education_City_Centre__c != NULL) {
                            	newAUAcct.Education_City_Centre__c = NULL;
								accountsToUpdateList.add(newAUAcct);
                            }                                
                        }
                        // R01 end
                    }
                    
                    newSegment.Geo_Confidence_Flag__c = SegmentationAddressTriggerHandler.getConfLevel(addrElements.get('VerificationLevel'));
                    if (debugFlag) System.debug('Verification Level:' + addrElements.get('VerificationLevel') + '; Geo Confidence:' + newSegment.Geo_Confidence_Flag__c);

                    if ( segmentsUpdate.isEmpty()) 
                        segmentsToInsertList.add(newSegment);
                    else
                        segmentsToUpdateList.add(newSegment);
                        
                }
                
                // Case 2 ######### Billing or Shipping ######### update of the Account Lat Long Only       
                if (AddrType == 'BIL' || AddrType == 'SHP'){
                    Account newAcct  = acctU.get(key);
                   if (debugFlag) System.debug ('Account newAcct:' + newAcct);
                   if (debugFlag) System.debug('All Address Elements:' + addrElements);
                   if (debugFlag) System.debug('Latitude:' + addrElements.get('Latitude')); 
                    
                     // Clear Lat Long if Address Processing Error and Lat Long exist (Update Mode)
                    if (!String.isBlank(addrElements.get('ERROR'))) {
                         if ( AddrType=='BIL' && newAcct.BillingLatitude != null) {
                            newAcct.BillingLatitude   = null;
                            newAcct.BillingLongitude  = null;
                            newAcct.BillingGeocodeAccuracy = null;
                            accountsToUpdateList.add(newAcct);
                         } else if (AddrType=='SHP' && newAcct.ShippingLatitude != null) {
                            newAcct.ShippingLatitude   = null;
                            newAcct.ShippingLongitude  = null;
                            newAcct.ShippingGeocodeAccuracy = null; 
							accountsToUpdateList.add(newAcct);
                         }
                        if (debugFlag) System.debug('Lat Long clear - Address Processing Error ID= ' + key);
                        continue;    
                    } 
                    
                    // wee need to convert NZ X/Y to Lat Long
                    try {
                        if (        Country=='AU' && AddrType=='BIL') {
                            newAcct.BillingLatitude   = Decimal.valueOf(addrElements.get('Latitude'));
                            newAcct.BillingLongitude  = Decimal.valueOf(addrElements.get('Longitude'));
                            newAcct.BillingGeocodeAccuracy = SegmentationAddressTriggerHandler.getGeoAccuracy(addrElements.get('VerificationLevel'));
                        } else if ( Country=='AU' && AddrType=='SHP') {
                            newAcct.ShippingLatitude  = Decimal.valueOf(addrElements.get('Latitude'));
                            newAcct.ShippingLongitude = Decimal.valueOf(addrElements.get('Longitude'));
                            newAcct.ShippingGeocodeAccuracy = SegmentationAddressTriggerHandler.getGeoAccuracy(addrElements.get('VerificationLevel'));
                        } else if ( Country=='NZ' && AddrType=='BIL') {
                            Map<String,decimal> longLat = SegmentationNZTM.getLatLong(Decimal.valueOf(addrElements.get('X Coordinate')), Decimal.valueOf(addrElements.get('Y Coordinate')));
                            if (longLat.size() > 0) {                                                                          	
                                newAcct.BillingLatitude   = longLat.get('Latitude' );
                                newAcct.BillingLongitude  = longLat.get('Longitude');
                                newAcct.BillingGeocodeAccuracy = SegmentationAddressTriggerHandler.getGeoAccuracy(addrElements.get('VerificationLevel'));
                            }
                        } else if ( Country=='NZ' && AddrType=='SHP') {
                            Map<String,decimal> longLatit = SegmentationNZTM.getLatLong(Decimal.valueOf(addrElements.get('X Coordinate')), Decimal.valueOf(addrElements.get('Y Coordinate')));
                            if (longLatit.size() > 0) {                                                                          	
                                newAcct.ShippingLatitude   = longLatit.get('Latitude' );
                                newAcct.ShippingLongitude  = longLatit.get('Longitude');
                                newAcct.ShippingGeocodeAccuracy = SegmentationAddressTriggerHandler.getGeoAccuracy(addrElements.get('VerificationLevel'));
                            }
                        }
                        accountsToUpdateList.add(newAcct);
                    } catch(Exception e) {
                        System.debug('Conversion Error: ' + e.getMessage());
                    }
                } // End of Case 2
            } 
		} // End of Main Loop
        
        if (debugFlag) System.debug('Before Update');
        if (debugFlag) System.debug('Account to Update Size ' + accountsToUpdateList.size() );
        
        if (accountsToUpdateList.size() > 0 ){
           update accountsToUpdateList; 
        }

        if (segmentsToInsertList.size() > 0 )
        	insert segmentsToInsertList;
        
        if (segmentsToUpdateList.size() > 0 )
        	update segmentsToUpdateList; 
        
        if (debugFlag) System.debug('Completed');
    }
}