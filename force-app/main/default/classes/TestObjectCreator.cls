/** 
 * <Project>Davanti Internal</Project>
 * <Title>TestObjectCreator</Title>
 * 
 * <Description>
 *  This class creates sObjects to be used in YOUR testMethods.
 *  It should be updated to include the custom sObjects needed for YOUR testMethods.
 *  Existing testMethods in this class may fail due to custom validation rules or required custom fields.
 *  These, if required, should be updated so that it can be successfully inserted to the database.
 * 
 *  The sObjects created here have the bare minimum of fields needed to be inserted.
 *  Additional fields required for your testMethods should be added to in YOUR testMethods, not here.
 *  Any relationships including RecordTypes should be added to in YOUR testMethods, not here.
 *  There should be no DML or SOQL in the code here, only in the testMethods.
 *  Object with RecordType should be specified, otherwise they are default which is bad practice.
 *  All class should always have 100% code coverage.
 * </Description>
 * 
 * <Organization>Davanti<Organization>
 * <ClassName>TestObjectCreator<ClassName>
 * <ApiVersion>25.0<ApiVersion>
 * <CreatedBy>Tom Cusack<CreatedBy>
 * <CreatedDate>Thu, 20/9/2012<CreatedDate>
 * 
 * <Changes>
 *  <Author>JR</Author>
 *  <Date>20/02/2015</Date>
 *  <Description>
 *  Added salutaion for account obejcts - removed changes
 *  </Description>
 * </Changes>
 
    Aug 2016    Davanti - RN    EHCH - Update to testInsertEmploymentHistory(), accommodate for default 'Provides CPP servicves' = true 
 */
@isTest
public with sharing class TestObjectCreator {    
    //******************************************************************************************
    //                  Standard Object Creation
    //******************************************************************************************
    // <Create Lead>
    public static Lead createLead(){
        Lead LeadObject = new Lead();
        LeadObject.FirstName = 'FName';
        LeadObject.LastName = 'LName'+ TestObjectCreator.getRandomNumber(910);
        LeadObject.Preferred_Name__c = 'PreferredName';
        LeadObject.Birth_Date__c = System.today();
        LeadObject.Email = 'QuxkNhQw2ms5@gmail.com';
        LeadObject.Street='200 Broadway Av' ;
        LeadObject.City= 'WEST BEACH SA';
        LeadObject.State= 'Western Australia';
        LeadObject.Country= 'Australia';
        LeadObject.PostalCode='6365' ;
        
        
        
        return LeadObject;
    }
    public static testMethod void testInsertLead(){
        Lead LeadObject = TestObjectCreator.createLead();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        Test.startTest();
        insert LeadObject;
        Test.stopTest();
        System.assertNotEquals(null, LeadObject.Id, 'inserted');
    }
    // </Create Lead>
    
    // <Create BusinessAccount>
    public static Account createBusinessAccount(){
        Account BusinessAccountObject = new Account();
        BusinessAccountObject.Name = 'Test Business Account';
        BusinessAccountObject.RecordTypeId = RecordTypeCache.getId(BusinessAccountObject, 'Business_Account');
        BusinessAccountObject.BillingStreet = '1 Test Street'; //DN20160415 added to comply with validation rules
        return BusinessAccountObject;
    }
    public static testMethod void testInsertBusinessAccount(){
        Account BusinessAccountObject = TestObjectCreator.createBusinessAccount();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        Test.startTest();
        insert BusinessAccountObject;
        Test.StopTest();
        System.assertNotEquals(null, BusinessAccountObject.Id, 'inserted');
    }
    // </Create BusinessAccount>
    
    // <Create FullMemberAccount>
    public static Account createFullMemberAccount(){
        Account FullMemberAccountObject = new Account();
        FullMemberAccountObject.Salutation = 'Mr.';
        FullMemberAccountObject.FirstName = 'TestFName';
        FullMemberAccountObject.PersonEmail= 'Full_Member'+ TestObjectCreator.getRandomNumber(910) +'@gmail.com';
        FullMemberAccountObject.LastName = 'Test'+ TestObjectCreator.getRandomNumber(910);
        FullMemberAccountObject.RecordTypeId = RecordTypeCache.getId(FullMemberAccountObject, 'Full_Member');
        FullMemberAccountObject.Communication_Preference__c= 'Home Phone';
        FullMemberAccountObject.PersonHomePhone= '1234';
        FullMemberAccountObject.PersonOtherStreet= '83 Saggers Road';
        FullMemberAccountObject.PersonOtherCity='JITARNING';
        FullMemberAccountObject.PersonOtherState='Western Australia';
        FullMemberAccountObject.PersonOtherCountry='Australia';
        FullMemberAccountObject.PersonOtherPostalCode='6365';  
        return FullMemberAccountObject;
    }
    public static Account createFullMemberAccountBulk(integer i){
        Account FullMemberAccountObject = new Account();
        FullMemberAccountObject.Salutation = 'Mr.';
        FullMemberAccountObject.FirstName = 'TestFName';
        FullMemberAccountObject.PersonEmail= 'Full_Member'+ TestObjectCreator.getRandomNumber(910) + i + '@gmail.com';
        FullMemberAccountObject.LastName = 'Test'+ TestObjectCreator.getRandomNumber(910);
        FullMemberAccountObject.RecordTypeId = RecordTypeCache.getId(FullMemberAccountObject, 'Full_Member');
        FullMemberAccountObject.Communication_Preference__c= 'Home Phone';
        FullMemberAccountObject.PersonHomePhone= '1234';
        FullMemberAccountObject.PersonOtherStreet= '83 Saggers Road';
        FullMemberAccountObject.PersonOtherCity='JITARNING';
        FullMemberAccountObject.PersonOtherState='Western Australia';
        FullMemberAccountObject.PersonOtherCountry='Australia';
        FullMemberAccountObject.PersonOtherPostalCode='6365';  
        return FullMemberAccountObject;
    }
    public static testMethod void testInsertFullMemberAccount(){
        Account FullMemberAccountObject = TestObjectCreator.createFullMemberAccount();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        Test.startTest();
        insert FullMemberAccountObject;
        Test.StopTest();
        System.assertNotEquals(null, FullMemberAccountObject.Id, 'inserted');
    }
    // </Create FullMemberAccount>
    
    // <Create StudentAffiliateAccount>
    public static Account createStudentAffiliateAccount(){
        Account StudentAffiliateAccountObject = new Account();
        StudentAffiliateAccountObject.Salutation = 'Mr.';
        StudentAffiliateAccountObject.FirstName = 'TestF';
        StudentAffiliateAccountObject.PersonEmail = 'Student_Affiliate' + TestObjectCreator.getRandomNumber(960) +'@gmail.com';
        StudentAffiliateAccountObject.LastName = 'TestL'+ TestObjectCreator.getRandomNumber(910);
        StudentAffiliateAccountObject.RecordTypeId = RecordTypeCache.getId(StudentAffiliateAccountObject, 'Student_Affiliate');
        StudentAffiliateAccountObject.Communication_Preference__c= 'Home Phone';
        StudentAffiliateAccountObject.PersonHomePhone= '1234';
        StudentAffiliateAccountObject.PersonOtherStreet= '83 Saggers Road';
        StudentAffiliateAccountObject.PersonOtherCity='JITARNING';
        StudentAffiliateAccountObject.PersonOtherState='Western Australia';
        StudentAffiliateAccountObject.PersonOtherCountry='Australia';
        StudentAffiliateAccountObject.PersonOtherPostalCode='6365';   
        
        
        return StudentAffiliateAccountObject;
    }
    public static testMethod void testInsertStudentAffiliateAccount(){
        Account StudentAffiliateAccountObject = TestObjectCreator.createStudentAffiliateAccount();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        Test.startTest();
        insert StudentAffiliateAccountObject;
        Test.stopTest();
        System.assertNotEquals(null, StudentAffiliateAccountObject.Id, 'inserted');
    }
    // </Create StudentAffiliateAccount>
    
    // <Create NonMemberAccount>
    public static Account createNonMemberAccount(){
        Account NonMemberAccountObject = new Account();
        NonMemberAccountObject.Salutation = 'Mr.';
        NonMemberAccountObject.FirstName = 'TestF';
        NonMemberAccountObject.PersonEmail = 'NonMember' + TestObjectCreator.getRandomNumber(960) +'@gmail.com';
        NonMemberAccountObject.LastName = 'TestL'+ TestObjectCreator.getRandomNumber(910);
        NonMemberAccountObject.RecordTypeId = RecordTypeCache.getId(NonMemberAccountObject, 'Non_member');
        NonMemberAccountObject.Communication_Preference__c= 'Home Phone';
        NonMemberAccountObject.PersonHomePhone= '1234';
        NonMemberAccountObject.PersonOtherStreet= '83 Saggers Road';
        NonMemberAccountObject.PersonOtherCity='JITARNING';
        NonMemberAccountObject.PersonOtherState='Western Australia';
        NonMemberAccountObject.PersonOtherCountry='Australia';
        NonMemberAccountObject.PersonOtherPostalCode='6365';  
        return NonMemberAccountObject;
    }
    public static testMethod void testInsertNonMemberAccount(){
        Account NonMemberAccountObject = TestObjectCreator.createNonMemberAccount();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        Test.StartTest();
        insert NonMemberAccountObject;
        Test.StopTest();
        System.assertNotEquals(null, NonMemberAccountObject.Id, 'inserted');
    }
    // </Create NonMemberAccount>
    
    // <Create PersonAccount>
    public static Account createPersonAccount(){
        Account PersonAccountObject = new Account();
        PersonAccountObject.Salutation = 'Mr.';
        PersonAccountObject.FirstName = 'FName';
        PersonAccountObject.LastName = 'LName'+ TestObjectCreator.getRandomNumber(910);
        PersonAccountObject.PersonEmail= 'PersonAccount' + TestObjectCreator.getRandomNumber(919) +'@gmail.com';
        PersonAccountObject.RecordTypeId = ApexUtil.getRecordTypeId(PersonAccountObject, 'PersonAccount');      
        PersonAccountObject.Communication_Preference__c= 'Home Phone';
        PersonAccountObject.Communication_Preference__c= 'Home Phone';
        PersonAccountObject.PersonHomePhone= '1234';
        PersonAccountObject.PersonOtherStreet= '83 Saggers Road';
        PersonAccountObject.PersonOtherCity='JITARNING';
        PersonAccountObject.PersonOtherState='Western Australia';
        PersonAccountObject.PersonOtherCountry='Australia';
        PersonAccountObject.PersonOtherPostalCode='6365'; 
        
        return PersonAccountObject;
    }
    public static List<Account> createPersonAccountBulk(Integer j,Id personAccRecTypeId){
        List<Account> personAccList = new List<Account>();
        for(Integer i=1;i<=j;i++){
            Account PersonAccountObject = new Account();
            PersonAccountObject.Salutation = 'Mr.';
            PersonAccountObject.FirstName = 'FNameBulk';
            PersonAccountObject.LastName = 'LNameBulk'+ TestObjectCreator.getRandomNumber(910);
            PersonAccountObject.PersonEmail= 'PersonAccount' + i +'@gmail.com';
            PersonAccountObject.RecordTypeId= personAccRecTypeId;
            PersonAccountObject.Communication_Preference__c= 'Home Phone';
            PersonAccountObject.PersonHomePhone= '1234456';
            PersonAccountObject.Membership_Approval_Date__c = Date.today().addDays(i);
            PersonAccountObject.PersonHomePhone= '1234';
         PersonAccountObject.PersonOtherStreet= '83 Saggers Road';
         PersonAccountObject.PersonOtherCity='JITARNING';
         PersonAccountObject.PersonOtherState='Western Australia';
         PersonAccountObject.PersonOtherCountry='Australia';
         PersonAccountObject.PersonOtherPostalCode='6365';
            personAccList.add(PersonAccountObject);
        }
        return personAccList;
    }
    public static testMethod void testInsertPersonAccount(){
        Account PersonAccountObject = TestObjectCreator.createPersonAccount();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        Test.StartTest();
        insert PersonAccountObject;
        Test.stopTest();
        System.assertNotEquals(null, PersonAccountObject.Id, 'inserted');
    }
    // </Create PersonAccount>
    
    // <Create User>
    public static User createUser(){
        User UserObject = new User();
        UserObject.alias = 'standt'; 
        UserObject.email='standarduser' + TestObjectCreator.getRandomNumber(920) +'@testorg.com';
        UserObject.emailencodingkey='UTF-8'; 
        UserObject.lastname='Testing';
        UserObject.languagelocalekey='en_US';
        UserObject.localesidkey='en_US';
        UserObject.timezonesidkey='America/Los_Angeles';
        UserObject.username='standarduser_dsStt8B8' + TestObjectCreator.getRandomNumber(930) +'@winaggs.co.nz';
        UserObject.UserRoleId = [Select u.Name, u.Id From UserRole u limit 1].Id;
        return UserObject;
    }
    public static testMethod void testInsertUser(){
        User UserObject = TestObjectCreator.createUser();
        //## Required Relationships
        UserObject.Profileid = ProfileCache.getId('System Administrator');
        UserObject.UserRoleId = [Select Id from UserRole Where PortalType = 'None' LIMIT 1].Id;// RN240616 Updated with different Role
        //## Additional fields and relationships / Updated fields
        Test.startTest();
        insert UserObject;
        Test.StopTest();
        System.assertNotEquals(null, UserObject.Id, 'inserted');
        //update UserObject; // coverage!
    }
    //*/// </Create User>
    
    // <Create MemberPortalSiteGuestUser>
    public static User createMemberPortalSiteGuestUser(){
        User MemberPortalSiteGuestUserObject = TestObjectCreator.createUser();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        MemberPortalSiteGuestUserObject.UserRoleId=null; // Guest Users cannot have a user role
        MemberPortalSiteGuestUserObject.Profileid = ProfileCache.getId('Member Portal Profile');
        return MemberPortalSiteGuestUserObject;
    }
    public static testMethod void testInsertMemberPortalSiteGuestUser(){
        User MemberPortalSiteGuestUserObject = TestObjectCreator.createMemberPortalSiteGuestUser();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        Test.startTest();
        insert MemberPortalSiteGuestUserObject;
        Test.StopTest();
        System.assertNotEquals(null, MemberPortalSiteGuestUserObject.Id, 'inserted');
    }
    //*/// </Create MemberPortalSiteGuestUser>
    
    // <Create PersonAccountPortalUser>
    public static User createPersonAccountPortalUser(){
        User PersonAccountPortalUserObject = TestObjectCreator.createUser();
        PersonAccountPortalUserObject.FirstName = 'PersonAccount';
        PersonAccountPortalUserObject.LastName = 'PortalUser'+ TestObjectCreator.getRandomNumber(910);
        PersonAccountPortalUserObject.UserRoleId=null; // High Volume Portal Users cannot have a user role
        return PersonAccountPortalUserObject;
    }
    public static testMethod void testInsertPersonAccountPortalUser(){
        Account BusinessAccountObject = TestObjectCreator.createBusinessAccount();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        Test.startTest();
        insert BusinessAccountObject;
        
        Contact ContactObject = TestObjectCreator.createContact();
        //## Required Relationships
        ContactObject.AccountId = BusinessAccountObject.Id; // portal account owner must have a role
        //## Additional fields and relationships / Updated fields
        insert ContactObject;
        
        User PersonAccountPortalUserObject = TestObjectCreator.createPersonAccountPortalUser();
        //## Required Relationships
        PersonAccountPortalUserObject.ContactId = ContactObject.Id;
        PersonAccountPortalUserObject.Profileid = ProfileCache.getId('NZICA Community Login User');
        //## Additional fields and relationships / Updated fields
        insert PersonAccountPortalUserObject;
        Test.StopTest();
        System.assertNotEquals(null, PersonAccountPortalUserObject.Id, 'inserted');
    }
    
    public static testMethod void testInsertPersonAccountPortalUserCAANZ(){
        Account BusinessAccountObject = TestObjectCreator.createBusinessAccount();
        BusinessAccountObject.Membership_Class__c = 'Provisional';
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        Test.startTest();
        insert BusinessAccountObject;
        
        Contact ContactObject = TestObjectCreator.createContact();
        //## Required Relationships
        ContactObject.AccountId = BusinessAccountObject.Id; // portal account owner must have a role
        //## Additional fields and relationships / Updated fields
        insert ContactObject;
        
        User PersonAccountPortalUserObject = TestObjectCreator.createPersonAccountPortalUser();
        //## Required Relationships
        PersonAccountPortalUserObject.ContactId = ContactObject.Id;
        PersonAccountPortalUserObject.Profileid = ProfileCache.getId('CAANZ CCH Community Non-Member');
        //## Additional fields and relationships / Updated fields
        insert PersonAccountPortalUserObject;
        Test.StopTest();
        System.assertNotEquals(null, PersonAccountPortalUserObject.Id, 'inserted');
    }
    //*/// </Create PersonAccountPortalUser>
    
    // <Create Contact>
    public static Contact createContact(){
        Contact ContactObject = new Contact();
        ContactObject.Salutation = 'Mr.';
        ContactObject.LastName = 'Test LastName'+ TestObjectCreator.getRandomNumber(910);
        ContactObject.MailingCountry = 'Australia';
        ContactObject.MailingState = 'New South Wales'; // City
        ContactObject.MailingStreet = 'Test MailingStreet';
        ContactObject.MailingPostalCode = 'Test PostalCode';
        ContactObject.Phone = '1234';
        
        return ContactObject;
    }
    public static testMethod void testInsertContact(){
        Contact ContactObject = TestObjectCreator.createContact();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        Test.startTest();
        insert ContactObject;
        Test.StopTest();
        System.assertNotEquals(null, ContactObject.Id, 'inserted');
    }
    // </Create Contact>
    
    // <Create Task>
    public static Task createTask(){
        Task TaskObject = new Task();
        TaskObject.Subject = 'Test Subject';
        return TaskObject;
    }
    public static testMethod void testInsertTask(){
        Task TaskObject = TestObjectCreator.createTask();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        insert TaskObject;
        System.assertNotEquals(null, TaskObject.Id, 'inserted');
    }
    // </Create Task>
    
    // <Create Case>
    public static Case createCase(){
        Case CaseObject = new Case();
        CaseObject.Subject = 'Test Subject';
        return CaseObject;
    }
    public static testMethod void testInsertCase(){
        Case CaseObject = TestObjectCreator.createCase();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        insert CaseObject;
        System.assertNotEquals(null, CaseObject.Id, 'inserted');
    }
    // </Create Case>
    
    //******************************************************************************************
    //                  Custom Object Creation
    //******************************************************************************************
    
    // <Create Application>
    public static Application__c createApplication(){
        Application__c ApplicationObject = new Application__c();
        return ApplicationObject;
    }
    public static testMethod void testInsertApplication(){
        Account FullMemberAccountObject = TestObjectCreator.createFullMemberAccount();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        insert FullMemberAccountObject;
        
        Application__c ApplicationObject = TestObjectCreator.createApplication();
        //## Required Relationships
        ApplicationObject.Account__c = FullMemberAccountObject.Id;
        //## Additional fields and relationships / Updated fields
        insert ApplicationObject;
        System.assertNotEquals(null, ApplicationObject.Id, 'inserted');
    }
    
    public static Declaration__c createDeclaration(){
        Declaration__c DeclarationObject = new Declaration__c();
        return DeclarationObject;
    }
    public static testMethod void testInsertDeclaration(){
        Account FullMemberAccountObject = TestObjectCreator.createFullMemberAccount();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        insert FullMemberAccountObject;
        
        Declaration__c DeclarationObject = TestObjectCreator.createDeclaration();
        //## Required Relationships
        DeclarationObject.Member_Name__c = FullMemberAccountObject.Id;
        //## Additional fields and relationships / Updated fields
        insert DeclarationObject;
        System.assertNotEquals(null, DeclarationObject.Id, 'inserted');
    }
    // </Create Application>
    
    // <Create FellowNomination>
    public static Fellow_Nomination__c createFellowNomination(){
        Fellow_Nomination__c FellowNominationObject = new Fellow_Nomination__c();
        return FellowNominationObject;
    }
    public static testMethod void testInsertFellowNomination(){
        Account FullMemberAccountObjectInDB = TestObjectCreator.createFullMemberAccount();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        FullMemberAccountObjectInDB.Status__c = 'Active';
        FullMemberAccountObjectInDB.Membership_Class__c = 'Full';
        insert FullMemberAccountObjectInDB;
        
        Fellow_Nomination__c FellowNominationObject = TestObjectCreator.createFellowNomination();
        //## Required Relationships
        FellowNominationObject.Member__c = FullMemberAccountObjectInDB.Id;
        //## Additional fields and relationships / Updated fields
        insert FellowNominationObject;
        System.assertNotEquals(null, FellowNominationObject.Id, 'inserted');
    }
    // </Create FellowNomination>
    
    // <Create Relationship>
    public static Relationship__c createRelationship(){
        Relationship__c RelationshipObject = new Relationship__c();
        return RelationshipObject;
    }
    public static testMethod void testInsertRelationship(){
        Relationship__c RelationshipObject = TestObjectCreator.createRelationship();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        insert RelationshipObject;
        System.assertNotEquals(null, RelationshipObject.Id, 'inserted');
    }
    // </Create Relationship>
    
    // <Create ProfessionalSupport>
    public static Professional_Support__c createProfessionalSupport(){
        Professional_Support__c ProfessionalSupportObject = new Professional_Support__c();
        ProfessionalSupportObject.Subject__c = 'test Subject__c';
        return ProfessionalSupportObject;
    }
    public static testMethod void testInsertProfessionalSupport(){
        Case CaseObject = TestObjectCreator.createCase();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        insert CaseObject;
        
        Professional_Support__c ProfessionalSupportObject = TestObjectCreator.createProfessionalSupport();
        //## Required Relationships
        ProfessionalSupportObject.Case__c = CaseObject.Id;
        //## Additional fields and relationships / Updated fields
        insert ProfessionalSupportObject;
        System.assertNotEquals(null, ProfessionalSupportObject.Id, 'inserted');
    }
    // </Create ProfessionalSupport>
    
    // <Create EmploymentHistory>
    public static Employment_History__c createEmploymentHistory(){
        Employment_History__c EmploymentHistoryObject = new Employment_History__c();
        return EmploymentHistoryObject;
    }
    public static testMethod void testInsertEmploymentHistory(){
        /*
        Account BusinessAccountObject = TestObjectCreator.createBusinessAccount();
        //## Required Relationships
        //## Additional fields and relationships / Updated fields
        insert BusinessAccountObject;
        
        Employment_History__c EmploymentHistoryObject = TestObjectCreator.createEmploymentHistory();
        //## Required Relationships
        EmploymentHistoryObject.Employer__c = BusinessAccountObject.Id;
        //## Additional fields and relationships / Updated fields
        insert EmploymentHistoryObject;
        System.assertNotEquals(null, EmploymentHistoryObject.Id, 'inserted');
        */
        
        Account accountEmployer = TestObjectCreator.createBusinessAccount();
        Account accountMember = TestObjectCreator.createBusinessAccount();
        insert new List<Account>{accountEmployer, accountMember};
        
        Employment_History__c EmploymentHistoryObject = TestObjectCreator.createEmploymentHistory();
        //## Required Relationships
        EmploymentHistoryObject.Employer__c = accountEmployer.Id;
        EmploymentHistoryObject.Member__c = accountMember.Id;
        EmploymentHistoryObject.Is_CPP_Provided__c = false;
        //## Additional fields and relationships / Updated fields
        insert EmploymentHistoryObject;
        System.assertNotEquals(null, EmploymentHistoryObject.Id, 'inserted');
    }
    // </Create EmploymentHistory>
    
    public static Integer getRandomNumber(Integer size){
          Double d = math.random() * size;
          return d.intValue();
     }
    
    public static ContentDocument createContentNote(){
        ContentDocument cD = new ContentDocument();
        cD.Title = 'Test Title';
        return cD;
    }
}