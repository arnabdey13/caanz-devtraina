/**
    Developer: WDCi (Lean)
    Development Date: 26/04/2016
    Task #: Employer wizard
**/

public without sharing class luana_EmploymentUtil {
    
    public static List<Employment_History__c> getUserActiveEmploymentHistory(String personAccountId){
        return [Select Id, Name, Employer__c, Employer__r.Name, Member__c, Member__r.Name, Member__r.PersonContactId, Status__c from Employment_History__c Where Member__c =: personAccountId and Nominated_Employee__c = true and Status__c = 'Current'];
    }
    
    public static List<Employment_History__c> getUserEmploymentHistory(String personAccountId){
        return [Select Id, Name, Employer__c, Employer__r.Name, Member__c, Member__r.Name, Member__r.PersonContactId, Status__c from Employment_History__c Where Member__c =: personAccountId and Status__c = 'Current'];
        
    }
    
    public static List<Employment_History__c> getEmployees(String employerId, String name, String memberId){
        
        String nameFilter = '%' + (name == null ? '' : String.escapeSingleQuotes(name)) + '%';
        String memberIdFilter = '%' + (memberId == null ? '' : String.escapeSingleQuotes(memberId)) + '%';

        return [Select Id, Name, Employer__c, Employer__r.Name, Member__c, Member__r.Name, Member__r.PersonContactId, Status__c from Employment_History__c Where Employer__c =: employerId and Member__r.Name like: nameFilter and Status__c = 'Current' and Member__r.Member_ID__c like: memberIdFilter];
        
    }
    
}