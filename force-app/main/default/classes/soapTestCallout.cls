public class soapTestCallout {
    

    public string startCall(){
    //Beginning of XML Generation for Partner API SOAP Login Call
        string bodyToSendLogin;
        string outCallResultLogin;
        string sessionID;
        string loginURL;
        //string userName = 'mohan.jerome@charteredaccountantsanz.com.mohanDev';
        //string password = '******!';
        String userName = '{!$Credential.UserName}';
        String password = '{!$Credential.Password}';        

        loginURL='callout:FormAssembly_User_NamedCred/services/Soap/u/45.0';
        //bodyToSendLogin = SOAP_Call + '' + userName + '' + password + '';        
        bodyToSendLogin = String.format(Label.loginTemplate, new String[]{ username , password });


         system.debug('--NJ bodyToSendLogin ' + bodyToSendLogin);
        outCallResultLogin = makeHTTPCall(loginURL, bodyToSendLogin);
        system.debug('--NJ outCallResultLogin Output: ' + outCallResultLogin);

        sessionID = getValueFromXMLString(outCallResultLogin, 'sessionId');
        system.debug('--MJ sessionID ---  ' + sessionID.substring(15) );
        //System.debug(('-- NJ S-'+UserInfo.getOrganizationId()+'-SID-'+UserInfo.getSessionId()+'-E-').length());
        //System.debug(UserInfo.getOrganizationId()+''+UserInfo.getSessionId().SubString(15)); 
 
        
        //return performRestCallUsingSID(sessionID);
        return performSoapCallToWS(sessionID);


        //End of XML Generation for Partner API SOAP Login Call
    }


    private string makeHTTPCall(string endPoint, string soapBody) {
        Http hLLogin = new Http();
        HttpRequest reqLLogin = new HttpRequest();
        reqLLogin.setTimeout(60000);
        reqLLogin.setEndpoint(endPoint);
        reqLLogin.setMethod('POST');
        reqLLogin.setHeader('SFDC_STACK_DEPTH', '1');
        reqLLogin.setHeader('SOAPAction', 'DoesNotMatter');
        //reqLLogin.setHeader('User-Agent', 'SFDC-Callout/22.0');
        reqLLogin.setHeader('Accept', 'text/xml');
        reqLLogin.setHeader('Content-type', 'text/xml');
        reqLLogin.setHeader('charset', 'UTF-8');
        system.debug('-- NJ Request: ' + reqLLogin);

        reqLLogin.setBody(soapBody);
        HttpResponse resLLogin = hLLogin.send(reqLLogin);
        string outCallResultLogin = '';
        outCallResultLogin = resLLogin.getBody();
        return outCallResultLogin;
    }

    private static string getValueFromXMLString(string xmlString, string keyField) {
        String valueFound = '';
        if (xmlString.contains('<' + keyField + '>') && xmlString.contains('</' + keyField + '>')) {
            try {
                valueFound = xmlString.substringBetween('<' + keyField + '>', '</' + keyField + '>');
            } catch (exception e) {
                system.debug('Error in getValueFromXMLString.  Details: ' + e.getMessage() + ' keyfield: ' + keyfield);
            }
        }
        return valueFound;
    }

    private static string performRestCallUsingSID(string sessionID){

        Http http = new Http();
        HttpRequest httpReq = new HttpRequest();
        HttpResponse httpRes = new HttpResponse();
        httpReq.setMethod('GET');
        httpReq.setHeader('Authorization', 'Bearer ' +sessionID);
        httpReq.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm()+ '/services/data/v45.0/sobjects/Account/');
        httpRes = http.send(httpReq);
        return httpRes.getBody();
          
    }

    private static string performSoapCallToWS(string sessionID){
        Http hLLogin = new Http();
        HttpRequest reqLLogin = new HttpRequest();
        reqLLogin.setTimeout(60000);
        //reqLLogin.setEndpoint('https://charteredaccountantsanz--devTrainA.cs6.my.salesforce.com/services/Soap/package/FormAssembly');
        reqLLogin.setEndpoint('https://devtraina-charteredaccountantsanz.cs6.force.com/services/Soap/package/FormAssembly');
        reqLLogin.setMethod('POST');
        reqLLogin.setHeader('SFDC_STACK_DEPTH', '1');
        reqLLogin.setHeader('SOAPAction', 'DoesNotMatter');
        //reqLLogin.setHeader('User-Agent', 'SFDC-Callout/22.0');
        reqLLogin.setHeader('Accept', 'text/xml');
        reqLLogin.setHeader('Content-type', 'text/xml');
        reqLLogin.setHeader('charset', 'UTF-8');  

        //String sBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="http://soap.sforce.com/schemas/class/FormAssembly"><soapenv:Header><urn:SessionHeader><urn:sessionId>'+sessionID+'</urn:sessionId></urn:SessionHeader></soapenv:Header><soapenv:Body><urn:getApprovedAccounts><urn:searchTerm>KPMG</urn:searchTerm><urn:memberOf>ICAA</urn:memberOf></urn:getApprovedAccounts></soapenv:Body></soapenv:Envelope>';
        //String sBody = '<se:Envelope xmlns:se="http://schemas.xmlsoap.org/soap/envelope/"><se:Header xmlns:sfns="http://soap.sforce.com/schemas/package/FormAssembly"><sfns:SessionHeader><sessionId>00DN0000000RCwr!ARIAQChun1aIM6_aKUYjDESHI9VzeeLLeZ55Lts0mo3ijmWoTN5ipHXe24cmeFmfcaF8nADDVuqMAkC9AHzNuwfcqKOYG4P8</sessionId></sfns:SessionHeader></se:Header><se:Body><getApprovedAccounts xmlns="http://soap.sforce.com/schemas/package/FormAssembly"><searchTerm>kpmg </searchTerm><memberOf>ICAA</memberOf></getApprovedAccounts></se:Body></se:Envelope>';
        String sBody = '<se:Envelope xmlns:se="http://schemas.xmlsoap.org/soap/envelope/"><se:Header xmlns:sfns="http://soap.sforce.com/schemas/package/FormAssembly"><sfns:SessionHeader><sessionId>'+sessionID+'</sessionId></sfns:SessionHeader></se:Header><se:Body><getApprovedAccounts xmlns="http://soap.sforce.com/schemas/package/FormAssembly"><searchTerm>kpmg </searchTerm><memberOf>ICAA</memberOf></getApprovedAccounts></se:Body></se:Envelope>';


        system.debug('-- NJ Request: ' + sBody);
        reqLLogin.setBody(sBody);
        HttpResponse resLLogin = hLLogin.send(reqLLogin);
        return resLLogin.getBody();

    }    

}