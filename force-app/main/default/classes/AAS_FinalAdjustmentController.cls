/* 
    Developer: WDCi (KH)
    Development Date: 18/11/2016
    Task #: AAS Final mark adjustment for exam and sub-exam  
*/

public with sharing class AAS_FinalAdjustmentController {
/* 
    public AAS_Course_Assessment__c courseAssessment {get; set;}
    public static String selectedRecordType{get;set;}
    
    //public Transient List<FinalAdjustmentWrapper> finalAdjustmentWrapperList {get; set;}
    //private List<FinalAdjustmentWrapper> commitRecords {get; set;}
    
    public String urlSelectedType {get; set;}
    public Boolean validToProcess {get; set;}
    public Boolean readyToCommit {get; set;}
    public Boolean hasRecords {get; set;}
    
    //public String finalAdjReportId {get; set;}
    public String exceedFullMarkReportId {get; set;}
    public Boolean hasExceedFullMark {get; set;}
    public String sasRecordTypeId;
    
    public String totalMarkLabel {get; set;}
    public Map<Id, Double> totalMarkValueMap {get; set;}
*/    
    public AAS_FinalAdjustmentController(ApexPages.StandardController controller) {
        /*
        if(!test.isRunningTest()){
            List<String> addFieldName = new List<String>{'AAS_Course__c', 'AAS_Module_Passing_Mark__c', 'AAS_Course__r.Name', 'AAS_Cohort_Adjustment_Exam__c', 'AAS_Final_Adjustment_Exam__c', 'AAS_Final_Adjustment_Supp_Exam__c', 
                                                         'AAS_Borderline_Remark__c', 'AAS_Borderline_Remark_Supp_Exam__c', 'AAS_Cohort_Adjustment_Supp_Exam__c'};
            controller.addFields(addFieldName);
        }
        courseAssessment = (AAS_Course_Assessment__c) controller.getRecord();

        urlSelectedType = apexpages.currentpage().getparameters().get('type');
        selectedRecordType = urlSelectedType + '_SAS';
        doValidation();
        
        readyToCommit= false;
        commitRecords = new List<FinalAdjustmentWrapper>();
        hasExceedFullMark = false;
        hasRecords = true;
        
        //Set the default value to display in the UI for cohort and passing value
        List<AAS_Settings__c> aasSets = AAS_Settings__c.getall().values();
        for(AAS_Settings__c assSetting: aasSets){
            
            //Change to show report in CA custom link
            //if(assSetting.Name == 'Final Adjustment Report Id'){    
            //    finalAdjReportId = '/' + assSetting.Value__c+ '?pv0='+String.ValueOf(courseAssessment.Id).subString(0,15)+'&pv1=';
            //}
            
            if(assSetting.Name == 'Exceed Full Mark Report Id'){
                exceedFullMarkReportId = assSetting.Value__c +'?pv0='+String.ValueOf(courseAssessment.Id).subString(0,15)+'&pv1=';
            }
        }
        
        if(selectedRecordType == 'Exam_Assessment_SAS'){
            totalMarkLabel = 'Total Module Mark (Non Exam + Exam)';
        }else if(selectedRecordType == 'Supp_Exam_Assessment_SAS'){
            totalMarkLabel = 'Total Module Mark (Non Exam + Supp Exam)';
        }
        totalMarkValueMap = new Map<Id, Double>();
        */
    }

/*    
    public void doQuery(){
        
        readyToCommit = false;
        commitRecords = new List<FinalAdjustmentWrapper>();
        
        System.debug('**********::::1: ' + selectedRecordType + ' - '+ courseAssessment.Id);
        
        Transient List<AAS_Student_Assessment_Section__c> sasList = [Select Id, Name, AAS_Exceed_Full_Mark_Count__c, AAS_Student_Assessment__c, 
                AAS_Student_Assessment__r.AAS_Total_Module_Mark_Non_Exam_Exam__c, AAS_Student_Assessment__r.AAS_Total_Module_Mark_Non_Exam_Supp_Exam__c,
                AAS_Student_Assessment__r.AAS_Student_Program__c, AAS_Student_Assessment__r.AAS_Student_Program__r.LuanaSMS__Contact_Student__r.Member_ID__c,
                AAS_Student_Assessment__r.AAS_Student_Program__r.LuanaSMS__Contact_Student__r.Name, AAS_Assessment_Schema_Section__r.RecordType.developerName,
                AAS_Student_Assessment__r.AAS_Student_Program__r.Name, AAS_Student_Assessment__r.AAS_Course_Assessment__r.AAS_Module_Passing_Mark__c, RecordType.DeveloperName
                from AAS_Student_Assessment_Section__c 
                Where RecordType.DeveloperName =: selectedRecordType and AAS_Student_Assessment__r.AAS_Course_Assessment__c =: courseAssessment.Id and AAS_Student_Assessment__r.AAS_Final_Adjustment__c = null];    
        
        //The checking of 0.5,1.5,-0.5 is done in the wrapper class, in here we just need to set the value to the wrapper class
        Transient List<FinalAdjustmentWrapper> tempFinalAdjustmentWrapperList = new List<FinalAdjustmentWrapper>();
        for(AAS_Student_Assessment_Section__c sas: sasList){
            tempFinalAdjustmentWrapperList.add(new FinalAdjustmentWrapper(sas));
            
            if(sas.AAS_Exceed_Full_Mark_Count__c > 0){
                hasExceedFullMark = true;
            }
            sasRecordTypeId = sas.RecordTypeId;
            
            if(sas.RecordType.DeveloperName  == 'Exam_Assessment_SAS'){
                totalMarkValueMap.put(sas.AAS_Student_Assessment__r.Id, sas.AAS_Student_Assessment__r.AAS_Total_Module_Mark_Non_Exam_Exam__c);
            }else if(sas.RecordType.DeveloperName  == 'Supp_Exam_Assessment_SAS'){
                totalMarkValueMap.put(sas.AAS_Student_Assessment__r.Id, sas.AAS_Student_Assessment__r.AAS_Total_Module_Mark_Non_Exam_Supp_Exam__c);
            }
        }

        
        Integer limitNum = 200;
        Integer counterLimit = 1;
        for(FinalAdjustmentWrapper faw: tempFinalAdjustmentWrapperList){
            if(faw.finalAdjustMark != 0){
                if(counterLimit <= limitNum){
                    finalAdjustmentWrapperList.add(faw);//Display the record in visualforce page, only show 200 records
                }
                counterLimit++;
                
                readyToCommit = true;
                
                commitRecords.add(faw);//Set into this list again because the previous list the most will only contains 200 records
            }
        }
        
        System.debug('******:: ' + finalAdjustmentWrapperList.size());
        
        if(!finalAdjustmentWrapperList.isEmpty()){
            hasRecords = true;
            if(commitRecords.size() <= limitNum){
                addPageMessage(ApexPages.severity.INFO, 'Total ' + finalAdjustmentWrapperList.size() +' record/s found.');
            }else{
                addPageMessage(ApexPages.severity.INFO, 'Total ' + commitRecords.size() +' records found, table below will only show maximum ' + limitNum + ' records.');
            }
        }else{
            hasRecords = false;
            addPageMessage(ApexPages.severity.INFO, 'No record found. Please press \'Done\' to indicate that Final Adjustment is completed and exit.');
        }
         
    }
    
    public PageReference doCommit(){
        Savepoint sp = Database.setSavepoint();
        try{
            if(!hasExceedFullMark){
//Final Adjustment Module change
//                List<AAS_Student_Assessment_Section__c> updateSASList = new List<AAS_Student_Assessment_Section__c>();
//                for(FinalAdjustmentWrapper faw: commitRecords){
//                    faw.stuAssSec.AAS_Final_Adjustment__c = faw.finalAdjustMark;
//                    updateSASList.add(faw.stuAssSec);
//                }
//                update updateSASList;

                List<AAS_Student_Assessment__c> updateSAList = new List<AAS_Student_Assessment__c>();
                for(FinalAdjustmentWrapper faw: commitRecords){
                    AAS_Student_Assessment__c sa = new AAS_Student_Assessment__c();
                    sa.Id = faw.stuAssSec.AAS_Student_Assessment__c;
                    sa.AAS_Final_Adjustment__c = faw.finalAdjustMark;
                    updateSAList.add(sa);
                }
                System.debug('******updateSAList: ' + updateSAList);
                update updateSAList;
                
                addPageMessage(ApexPages.severity.Info, 'Committed success.');
                readyToCommit= false;
                
                AAS_Course_Assessment__c ca = new AAS_Course_Assessment__c();
                ca.Id = courseAssessment.Id;
                if(selectedRecordType.equals('Exam_Assessment_SAS')){
                    ca.AAS_Final_Adjustment_Exam__c = true;
                }else if(selectedRecordType.equals('Supp_Exam_Assessment_SAS')){
                    ca.AAS_Final_Adjustment_Supp_Exam__c = true;
                }
                update ca;
                
                PageReference returnPage = new PageReference('/'+courseAssessment.Id);
                return returnPage;
            }else{
                addPageMessage(ApexPages.severity.ERROR, 'Error found! One of the record has exceeded full mark, please refer to <a href="/'+exceedFullMarkReportId+sasRecordTypeId.subString(0, 15)+'" target="_blank">here</a> to fix the record manually.');
            }
        }Catch(Exception exp){
            Database.rollback(sp);
            addPageMessage(ApexPages.severity.ERROR, 'Committed fail. Please try again later or contact your system administrator. Error: ' + exp.getMessage() + ' - ' + exp.getStackTraceString());
        }
        return null;
    }
    
    public void doValidation(){
        
        if(selectedRecordType == 'Exam_Assessment_SAS'){
            if(!courseAssessment.AAS_Final_Adjustment_Exam__c){
                if(courseAssessment.AAS_Cohort_Adjustment_Exam__c && courseAssessment.AAS_Borderline_Remark__c){
                    validToProcess = true;
                }else{
                    validToProcess = false;
                    readyToCommit = false;
                    addPageMessage(ApexPages.severity.ERROR, 'You are not allow to continue in this step. Please complete the Cohort Adjustment & Borderline remark processes first.');
                }
  
            }else{
                validToProcess = true;
                addPageMessage(ApexPages.severity.WARNING, 'Final Adjustment was commited before. If you re-commit again, previous Final Adjustment data will not be undo.');        
            }
        }else if(selectedRecordType == 'Supp_Exam_Assessment_SAS'){
            if(!courseAssessment.AAS_Final_Adjustment_Supp_Exam__c){
                if(courseAssessment.AAS_Cohort_Adjustment_Supp_Exam__c && courseAssessment.AAS_Borderline_Remark_Supp_Exam__c){
                    validToProcess = true;
                } else{
                    validToProcess = false;
                    readyToCommit = false;
                    addPageMessage(ApexPages.severity.ERROR, 'You are not allow to continue in this step. Please complete the Cohort Adjustment & Borderline remark processes first.');
                }
            }else{
                addPageMessage(ApexPages.severity.WARNING, 'Final Adjustment was commited before. If you re-commit again, previous Final Adjustment data will not be undo.');
            }
        }
    }
    
    // returns a list of wrapper objects for the sObjects in the current page set
    public void getResults() {
        finalAdjustmentWrapperList = new List<FinalAdjustmentWrapper>();
        doQuery();
    }
    
    public PageReference doCancel(){
        PageReference returnPage = new PageReference('/'+courseAssessment.Id);
        return returnPage;
    }
    
    public PageReference doComplete(){
        AAS_Course_Assessment__c ca = new AAS_Course_Assessment__c();
        ca.Id = courseAssessment.Id;
        if(selectedRecordType.equals('Exam_Assessment_SAS')){
            ca.AAS_Final_Adjustment_Exam__c = true;
        }else if(selectedRecordType.equals('Supp_Exam_Assessment_SAS')){
            ca.AAS_Final_Adjustment_Supp_Exam__c = true;
        }
        update ca;
    
        PageReference returnPage = new PageReference('/'+courseAssessment.Id);
        return returnPage;
    }
    
    public List<SelectOption> getRecordTypeOptions() {
        
        Schema.DescribeFieldResult statusFieldDescription = RecordType.DeveloperName.getDescribe();
        List<SelectOption> statusOptions = new list<SelectOption>();
        
        for(AAS_Assessment_Schema_Section__c ass: [Select RecordType.Name, RecordType.developerName
                                                    from AAS_Assessment_Schema_Section__c 
                                                    Where AAS_Course_Assessment__c =: courseAssessment.Id and RecordType.DeveloperName = 'Exam_Assessment']){
            statusOptions.add(new SelectOption(ass.RecordType.developerName+'_SAS', ass.RecordType.Name));
        }

        return statusOptions;
    }
    
    public class FinalAdjustmentWrapper{
        
        public AAS_Student_Assessment_Section__c stuAssSec {get; set;}
        public Double finalAdjustMark {get; set;}
        
        public FinalAdjustmentWrapper(AAS_Student_Assessment_Section__c sas){
            stuAssSec = sas;
          
          //Do checking logic in here  
          if(selectedRecordType == 'Exam_Assessment_SAS'){
                if(sas.AAS_Student_Assessment__r.AAS_Course_Assessment__r.AAS_Module_Passing_Mark__c != null && sas.AAS_Assessment_Schema_Section__r.RecordType.developerName +'_SAS' == 'Exam_Assessment_SAS'){
                    if(sas.AAS_Student_Assessment__r.AAS_Course_Assessment__r.AAS_Module_Passing_Mark__c - sas.AAS_Student_Assessment__r.AAS_Total_Module_Mark_Non_Exam_Exam__c == 0.5){ 
                        finalAdjustMark = 0.5;
                    }else if(sas.AAS_Student_Assessment__r.AAS_Course_Assessment__r.AAS_Module_Passing_Mark__c - sas.AAS_Student_Assessment__r.AAS_Total_Module_Mark_Non_Exam_Exam__c == 1.5){
                        finalAdjustMark = -0.5;
                    }else if(sas.AAS_Student_Assessment__r.AAS_Course_Assessment__r.AAS_Module_Passing_Mark__c - sas.AAS_Student_Assessment__r.AAS_Total_Module_Mark_Non_Exam_Exam__c == 1){
                        finalAdjustMark = -1;
                    }else{
                        finalAdjustMark = 0;
                    }
                     
                }
            }
            
        }
    } 
    
    public void addPageMessage(ApexPages.severity sev, String msg){
        ApexPages.Message warningmsg = new ApexPages.Message(sev, msg);
        ApexPages.addmessage(warningmsg);
    }  
*/     
}