/*------------------------------------------------------------------------------------
Author:        Sumit Gupta
Company:       Salesforce
Description:   cpd triggers handler

History
Date            Author             Comments
--------------------------------------------------------------------------------------
02-05-2018     Sumit Gupta          Initial Release
------------------------------------------------------------------------------------*/
public with sharing class CPDSummaryHandler {

    //CPD specialisation Mappings
    private static List<CPD_Specialisation_Mapping__mdt> specialisationMappings;

    //Create missing summary records
    public static List<CPD_Summary__c> createMissingCPDSummaryRecords(List<Account> accounts) {
        List<CPD_Summary__c> summaries = new List<CPD_Summary__c>();
        Map<Id, Contact> contactsMap = getContactsMap(accounts);
        for(Account acc : accounts){
            if(!contactsMap.containsKey(acc.Id)) continue;
            Contact cont = contactsMap.get(acc.Id);

            Date today = Date.today();
            Date expectedStartDate = acc.Triennium_Firstl_Start_Date__c.addYears(-6);
            Date firstStartDate = cont.First_Triennium_Start_Date__c != null ? cont.First_Triennium_Start_Date__c : acc.Triennium_Firstl_Start_Date__c;
            Date lastStartDate = cont.Last_Triennium_Start_Date__c != null ? cont.Last_Triennium_Start_Date__c : acc.Triennium_Firstl_Start_Date__c.addYears(-3);

            while(firstStartDate > expectedStartDate){
                summaries.add(getSummaryRecord(cont.Id, firstStartDate = firstStartDate.addYears(-3)));
            }
            while(lastStartDate < today){
                summaries.add(getSummaryRecord(cont.Id, lastStartDate = lastStartDate.addYears(3)));
            }
        }
        if(!summaries.isEmpty()){
            insert summaries;
        }
        return summaries;
    }

    //Filter accounts where type is member and has membership approval date
    public static List<Account> filterSummaryAccounts(List<Account> accounts){
        List<Account> summaryAccounts = new List<Account>();
        for(Account acc : accounts){
            if(isSummaryAccount(acc)){
                summaryAccounts.add(acc);
            }
        }
        return summaryAccounts;
    }

    //Check if account type is member and has membership approval date
    public static Boolean isSummaryAccount(Account acc){
        return (acc.Membership_Type__c == 'Member' && acc.Membership_Approval_Date__c != null);
    }

    //Set account specialisations
    public static void setAccontSpecialisations(List<Account> accounts){
        for(Account acc : accounts){
            updateAccountSpecialisations(acc, null);
        }
    }

    //Create account summary records
    public static void createAccountSummaryRecords(List<Account> accounts){
        createMissingCPDSummaryRecords(accounts);
    }

    //Update account specialisations
    public static void updateAccountSpecialisations(Account newAcc, Account oldAcc){
        Account splAcc = getSpecialistAccount(newAcc);
        newAcc.Specialisation_1__c = splAcc.Specialisation_1__c;
        newAcc.Specialisation_2__c = splAcc.Specialisation_2__c;
        newAcc.Specialisation_3__c = splAcc.Specialisation_3__c;
        newAcc.Specialisation_4__c = splAcc.Specialisation_4__c;
        newAcc.Specialisation_5__c = splAcc.Specialisation_5__c;
        newAcc.Specialisation_6__c = splAcc.Specialisation_6__c;
        newAcc.Specialisation_7__c = splAcc.Specialisation_7__c;
        newAcc.Specialisation_8__c = splAcc.Specialisation_8__c;
    }

    //Check if account specialisations changed
    public static Boolean isAccountSpecialisationsChanged(Account newAcc, Account oldAcc){
        return (oldAcc.Specialisation_1__c != newAcc.Specialisation_1__c
            || oldAcc.Specialisation_2__c != newAcc.Specialisation_2__c
            || oldAcc.Specialisation_3__c != newAcc.Specialisation_3__c
            || oldAcc.Specialisation_4__c != newAcc.Specialisation_4__c
            || oldAcc.Specialisation_5__c != newAcc.Specialisation_5__c
            || oldAcc.Specialisation_6__c != newAcc.Specialisation_6__c
            || oldAcc.Specialisation_7__c != newAcc.Specialisation_7__c
            || oldAcc.Specialisation_8__c != newAcc.Specialisation_8__c);
    }

    //Update summary specialisations
    public static void updateSummarySpecialisations(List<Account> accounts){
        Map<String, Account> accountsMap = getAccountsMap(accounts);
        List<CPD_Summary__c> summaries = getCurrentSummaries(accountsMap.keySet());
        if(!summaries.isEmpty()){
            updateSummarySpecialisations(summaries, accountsMap);
            update summaries;
        }
    }

    //Update summary specialisations
    public static void setSummarySpecialisations(List<CPD_Summary__c> summaries){
        Set<Id> accountIds = new Set<Id>();
        for(CPD_Summary__c summary : summaries){
            accountIds.add(summary.Account__c);
        }
        updateSummarySpecialisations(summaries, getAccountsMap(getSummaryAccounts(accountIds)));
    }

    //Check if summary specialisations changed
    public static Boolean isSummarySpecialisationsChanged(CPD_Summary__c newSummary, CPD_Summary__c oldSummary){
        return (oldSummary.Specialisation_1__c != newSummary.Specialisation_1__c
            || oldSummary.Specialisation_2__c != newSummary.Specialisation_2__c
            || oldSummary.Specialisation_3__c != newSummary.Specialisation_3__c
            || oldSummary.Specialisation_4__c != newSummary.Specialisation_4__c
            || oldSummary.Specialisation_5__c != newSummary.Specialisation_5__c
            || oldSummary.Specialisation_6__c != newSummary.Specialisation_6__c
            || oldSummary.Specialisation_7__c != newSummary.Specialisation_7__c
            || oldSummary.Specialisation_8__c != newSummary.Specialisation_8__c);
    }

    //Update summary specialisation hours
    public static void updateSummarySpecialisationHours(List<CPD_Summary__c> summaries){
        Map<Id, CPD_Summary__c> summariesMap = new Map<Id, CPD_Summary__c>();
        for(CPD_Summary__c summary : summaries){
            summary.Specialisation_Hours_1__c = 0;
            summary.Specialisation_Hours_2__c = 0;
            summary.Specialisation_Hours_3__c = 0;
            summary.Specialisation_Hours_4__c = 0;
            summary.Specialisation_Hours_5__c = 0;
            summary.Specialisation_Hours_6__c = 0;
            summary.Specialisation_Hours_7__c = 0;
            summary.Specialisation_Hours_8__c = 0;
            summariesMap.put(summary.Id, summary);
        }
        List<Education_Record__c> educations = [
            SELECT CPD_Summary__c, Specialist_hours_code__c, Qualifying_hours__c 
            FROM Education_Record__c WHERE CPD_Summary__c IN :summariesMap.keySet()
            AND Specialist_hours_code__c != null AND Qualifying_hours__c != null 
        ];
        for(Education_Record__c education : educations){
            CPD_Summary__c summary = summariesMap.get(education.CPD_Summary__c);
            if(education.Specialist_hours_code__c == summary.Specialisation_1__c){
                summary.Specialisation_Hours_1__c += education.Qualifying_hours__c;
            } else if(education.Specialist_hours_code__c == summary.Specialisation_2__c){
                summary.Specialisation_Hours_2__c += education.Qualifying_hours__c;
            } else if(education.Specialist_hours_code__c == summary.Specialisation_3__c){
                summary.Specialisation_Hours_3__c += education.Qualifying_hours__c;
            } else if (education.Specialist_hours_code__c == summary.Specialisation_4__c){
                summary.Specialisation_Hours_4__c += education.Qualifying_hours__c;
            } else if (education.Specialist_hours_code__c == summary.Specialisation_5__c){
                summary.Specialisation_Hours_5__c += education.Qualifying_hours__c;
            } else if (education.Specialist_hours_code__c == summary.Specialisation_6__c){
                summary.Specialisation_Hours_6__c += education.Qualifying_hours__c;
            } else if (education.Specialist_hours_code__c == summary.Specialisation_7__c){
                summary.Specialisation_Hours_7__c += education.Qualifying_hours__c;
            } else if (education.Specialist_hours_code__c == summary.Specialisation_8__c){
                summary.Specialisation_Hours_8__c += education.Qualifying_hours__c;
            }
        }
    }

    //Update missing contact or summary details
    public static void updateEducationContactAndSummary(List<Education_Record__c> educations){
        Id communityUserContactId;

        Map<Id, List<Education_Record__c>> contactEducations = new Map<Id, List<Education_Record__c>>();
        Map<Id, List<Education_Record__c>> summaryEducations = new Map<Id, List<Education_Record__c>>();
        Boolean isACommunityUser = [SELECT Community_User__c FROM User WHERE Id =: userInfo.getUserId()].Community_User__c;

        for(Education_Record__c education : educations){
            if(education.Contact_Student__c == null && education.CPD_Summary__c == null && isACommunityUser){
                if(communityUserContactId == null){
                    communityUserContactId = [SELECT ContactId FROM User WHERE Id =: userInfo.getUserId()].ContactId;
                }
                education.Contact_Student__c = communityUserContactId;
            }
            if(education.Contact_Student__c != null && education.CPD_Summary__c == null){
                if(!contactEducations.containsKey(education.Contact_Student__c)){
                    contactEducations.put(education.Contact_Student__c, new List<Education_Record__c>{ education });
                } else {
                    contactEducations.get(education.Contact_Student__c).add(education);
                }
            } else if(education.Contact_Student__c == null && education.CPD_Summary__c != null){
                if(!summaryEducations.containsKey(education.CPD_Summary__c)){
                    summaryEducations.put(education.CPD_Summary__c, new List<Education_Record__c>{ education });
                } else {
                    summaryEducations.get(education.CPD_Summary__c).add(education);
                }
            }
        }        
        if(!contactEducations.isEmpty() || !summaryEducations.isEmpty()){
            Map<Id, List<CPD_Summary__c>> summariesMap = getEducationSummariesMap(contactEducations.keySet(), summaryEducations.keySet());
            if(summariesMap.isEmpty()) return;
            for(Id contactId : summariesMap.keySet()){
                List<CPD_Summary__c> summaries = summariesMap.get(contactId);
                if(contactEducations.containsKey(contactId)){
                    for(Education_Record__c education : contactEducations.get(contactId)){
                        for(CPD_Summary__c summary : summaries){
                            if(education.Date_Completed__c >= summary.Triennium_Start_Date__c && education.Date_Completed__c <= summary.Triennium_End_Date__c){
                                education.CPD_Summary__c = summary.Id;
                                break;
                            }
                        }   
                    }
                } else {
                    for(CPD_Summary__c summary : summaries){
                        if(summaryEducations.containsKey(summary.Id)){
                            for(Education_Record__c education : summaryEducations.get(summary.Id)){
                                if(education.Contact_Student__c==null)
                                    education.Contact_Student__c = contactId;
                            }
                            break;
                        }
                    }
                }
            }
        }
    }

    //Update summary specialisation hours
    public static void updateSummarySpecialisationHours(List<Education_Record__c> educations, Map<Id, Education_Record__c> oldEducations){
        Set<Id> summaryIds = new Set<Id>();
        for(Education_Record__c education : educations){
            if(oldEducations != null){
                Education_Record__c oldEducation = oldEducations.get(education.Id);
                if(education.Specialist_hours_code__c == oldEducation.Specialist_hours_code__c && 
                    education.Qualifying_hours__c == oldEducation.Qualifying_hours__c){
                    continue;
                }
            }
            if(education.CPD_Summary__c != null){
                summaryIds.add(education.CPD_Summary__c);
            }
        }
        if(summaryIds.isEmpty()) return;

        List<CPD_Summary__c> summaries = getEducationSummaries(summaryIds);
        updateSummarySpecialisationHours(summaries);
        update summaries;
    }

    //set ManuallyAdded flag for Education records when created by a community user.
    public static void setEducationManuallyAdded(List<Education_Record__c> educations){

        Boolean isACommunityUser = [SELECT Community_User__c FROM User WHERE Id =: userInfo.getUserId()].Community_User__c;

        for(Education_Record__c education : educations){
            if(isACommunityUser){
                education.Community_User_Entry__c = true;
            }
        }    
    }

    
    //Set cpd exemption account lookup
    public static void updateExemptionAccountAndSummary(List<CPD_Exemption__c> exemptions){
        Id communityUserAccountId;

        Map<Id, List<CPD_Exemption__c>> accountExemptions = new Map<Id, List<CPD_Exemption__c>>();
        Map<Id, List<CPD_Exemption__c>> summaryExemptions = new Map<Id, List<CPD_Exemption__c>>();
        Boolean isACommunityUser = [SELECT Community_User__c FROM User WHERE Id =: userInfo.getUserId()].Community_User__c;

        for(CPD_Exemption__c exemption : exemptions){
            if(exemption.Account_Name__c == null && exemption.CPD_Summary__c == null && isACommunityUser){
                if(communityUserAccountId == null){
                    communityUserAccountId = [SELECT AccountId FROM User WHERE Id =: userInfo.getUserId()].AccountId;
                }
                exemption.Account_Name__c = communityUserAccountId;
            }
            if(exemption.Account_Name__c != null && exemption.CPD_Summary__c == null){
                if(!accountExemptions.containsKey(exemption.Account_Name__c)){
                    accountExemptions.put(exemption.Account_Name__c, new List<CPD_Exemption__c>{ exemption });
                } else {
                    accountExemptions.get(exemption.Account_Name__c).add(exemption);
                }
            } else if(exemption.Account_Name__c == null && exemption.CPD_Summary__c != null){
                if(!summaryExemptions.containsKey(exemption.CPD_Summary__c)){
                    summaryExemptions.put(exemption.CPD_Summary__c, new List<CPD_Exemption__c>{ exemption });
                } else {
                    summaryExemptions.get(exemption.CPD_Summary__c).add(exemption);
                }
            }
        }
        if(!accountExemptions.isEmpty() || !summaryExemptions.isEmpty()){
            Map<Id, List<CPD_Summary__c>> summariesMap = getExemptionSummariesMap(accountExemptions.keySet(), summaryExemptions.keySet());
            if(summariesMap.isEmpty()) return;
            for(Id accountId : summariesMap.keySet()){
                List<CPD_Summary__c> summaries = summariesMap.get(accountId);
                if(accountExemptions.containsKey(accountId)){
                    for(CPD_Exemption__c exemption : accountExemptions.get(accountId)){
                        for(CPD_Summary__c summary : summaries){
                            if(exemption.End_Date__c >= summary.Triennium_Start_Date__c && exemption.End_Date__c <= summary.Triennium_End_Date__c){
                                exemption.CPD_Summary__c = summary.Id;
                                break;
                            }
                        }   
                    }
                } else {
                    for(CPD_Summary__c summary : summaries){
                        if(summaryExemptions.containsKey(summary.Id)){
                            for(CPD_Exemption__c exemption : summaryExemptions.get(summary.Id)){
                                if(exemption.Account_Name__c==null)
                                    exemption.Account_Name__c = summary.Contact_Student__r.AccountId;
                            }
                            break;
                        }
                    }
                }
            }
        }
    }

    public static void updateSummaryRollupFields(List<Education_Record__c> educations, Map<Id, Education_Record__c> oldEducations){
        Set<Id> summaryIds = new Set<Id>();
        for(Education_Record__c education : educations){
            if(oldEducations != null){
                Education_Record__c oldEducation = oldEducations.get(education.Id);
                if(oldEducation.Qualifying_hours_type__c == education.Qualifying_hours_type__c && 
                    oldEducation.Triennium_Year__c == education.Triennium_Year__c && 
                        oldEducation.Qualifying_hours__c == education.Qualifying_hours__c){
                    continue;
                }
            }
            if(education.CPD_Summary__c != null){
                summaryIds.add(education.CPD_Summary__c);
            }
        } 
        if(summaryIds.isEmpty()) return;

        List<CPD_Summary__c> summaries = [
            SELECT Id, Formal_Verifiable_Hours_Y1__c, Formal_Verifiable_Hours_Y2__c, Formal_Verifiable_Hours_Y3__c, 
                Informal_Nonverifiable_Hours_Y1__c, Informal_Nonverifiable_Hours_Y2__c, Informal_Nonverifiable_Hours_Y3__c,
                (SELECT Id, Name, CPD_Summary__c, Qualifying_hours_type__c, Triennium_Year__c, Qualifying_hours__c FROM Education_Record__r ORDER BY Name) 
            FROM CPD_Summary__c WHERE Id IN : summaryIds
        ];
        for(CPD_Summary__c summary : summaries){
            summary.Formal_Verifiable_Hours_Y1__c = 0;
            summary.Formal_Verifiable_Hours_Y2__c = 0;
            summary.Formal_Verifiable_Hours_Y3__c = 0;
            summary.Informal_Nonverifiable_Hours_Y1__c = 0;
            summary.Informal_Nonverifiable_Hours_Y2__c = 0;
            summary.Informal_Nonverifiable_Hours_Y3__c = 0;

            for(Education_Record__c education : summary.Education_Record__r){
                if(education.Qualifying_hours_type__c == 'Formal/Verifiable'){
                    if(education.Triennium_Year__c == 'Year 1'){
                        summary.Formal_Verifiable_Hours_Y1__c += education.Qualifying_hours__c;
                    } else if(education.Triennium_Year__c == 'Year 2'){
                        summary.Formal_Verifiable_Hours_Y2__c += education.Qualifying_hours__c;
                    } else if(education.Triennium_Year__c == 'Year 3'){
                        summary.Formal_Verifiable_Hours_Y3__c += education.Qualifying_hours__c;
                    } 
                } else if(education.Qualifying_hours_type__c == 'Informal/Non-verifiable'){
                    if(education.Triennium_Year__c == 'Year 1'){
                        summary.Informal_Nonverifiable_Hours_Y1__c += education.Qualifying_hours__c;
                    } else if(education.Triennium_Year__c == 'Year 2'){
                        summary.Informal_Nonverifiable_Hours_Y2__c += education.Qualifying_hours__c;
                    } else if(education.Triennium_Year__c == 'Year 3'){
                        summary.Informal_Nonverifiable_Hours_Y3__c += education.Qualifying_hours__c;
                    } 
                }
            }
        }
        if(!summaries.isEmpty()){
            update summaries;
        }
    }

    public static void updateSummaryRollupFields(List<CPD_Exemption__c> exemptions, Map<Id, CPD_Exemption__c> oldExemptions){
        Set<Id> summaryIds = new Set<Id>();
        for(CPD_Exemption__c exemption : exemptions){
            if(oldExemptions != null){
                CPD_Exemption__c oldExemption = oldExemptions.get(exemption.Id);
                if(oldExemption.Exemption_Hours__c == exemption.Exemption_Hours__c && 
                    oldExemption.Formal_verified_exemption_hours__c == oldExemption.Formal_verified_exemption_hours__c){
                    continue;
                }
            }
            if(exemption.CPD_Summary__c != null){
                summaryIds.add(exemption.CPD_Summary__c);
            }
        } 
        if(summaryIds.isEmpty()) return;

        List<CPD_Summary__c> summaries = [
            SELECT Id, Total_Exempt_Hours__c, Total_Formal_Exempt_Hours__c, 
                (SELECT Id, CPD_Summary__c, Exemption_Hours__c, Formal_verified_exemption_hours__c FROM CPD_Exemptions__r) 
            FROM CPD_Summary__c WHERE Id IN : summaryIds
        ];
        for(CPD_Summary__c summary : summaries){
            summary.Total_Exempt_Hours__c = 0;
            summary.Total_Formal_Exempt_Hours__c = 0;

            for(CPD_Exemption__c exemption : summary.CPD_Exemptions__r){
                summary.Total_Exempt_Hours__c += exemption.Exemption_Hours__c;
                summary.Total_Formal_Exempt_Hours__c += exemption.Formal_verified_exemption_hours__c;
            }
        }
        if(!summaries.isEmpty()){
            update summaries;
        }
    }

    //Get summary accounts
    public static List<Account> getMemberAccounts(){
        return [
            SELECT Id, Membership_Approval_Date__c, Triennium_Firstl_Start_Date__c FROM Account 
            WHERE Membership_Type__c = 'Member' AND Membership_Class__c = 'Provisional'
            AND ( (Last_Triennium_Start_Date__pc = null AND Membership_Approval_Date__c != null 
            AND Triennium_Firstl_Start_Date__c != null) OR (Last_Triennium_Start_Date__pc != null 
            AND Last_Triennium_Start_Date__pc <=: Date.today()) )
        ];
    }

    private static Map<String, Account> getAccountsMap(List<Account> accounts){
        Map<String, Account> accountsMap = new Map<String, Account>();
        for(Account acc : accounts){
            accountsMap.put(String.valueOf(acc.Id).substring(0, 15), acc);
        }
        return accountsMap;
    }

    //Update summary specialisations
    private static void updateSummarySpecialisations(List<CPD_Summary__c> summaries, Map<String, Account> accountsMap){
        for(CPD_Summary__c summary : summaries){
            Account acc = accountsMap.get(summary.Account__c);
            summary.Specialisation_1__c = acc.Specialisation_1__c;
            summary.Specialisation_2__c = acc.Specialisation_2__c;
            summary.Specialisation_3__c = acc.Specialisation_3__c;
            summary.Specialisation_4__c = acc.Specialisation_4__c;
            summary.Specialisation_5__c = acc.Specialisation_5__c;
            summary.Specialisation_6__c = acc.Specialisation_6__c;
            summary.Specialisation_7__c = acc.Specialisation_7__c;
            summary.Specialisation_8__c = acc.Specialisation_8__c;
        }
    }

    //Get current triennium year summaries
    private static List<CPD_Summary__c> getCurrentSummaries(Set<String> accountIds){
        return [
            SELECT Id, Account__c, Specialisation_1__c, Specialisation_2__c, Specialisation_3__c, Specialisation_4__c, 
                Specialisation_5__c, Specialisation_6__c, Specialisation_7__c, Specialisation_8__c, Specialisation_Hours_1__c, 
                Specialisation_Hours_2__c, Specialisation_Hours_3__c, Specialisation_Hours_4__c, Specialisation_Hours_5__c,
                Specialisation_Hours_6__c, Specialisation_Hours_7__c, Specialisation_Hours_8__c
            FROM CPD_Summary__c WHERE Account__c IN :accountIds 
            AND Triennium_End_Date__c >=: Date.today()
        ];
    }

    //Get contact summaries
    private static Map<Id, List<CPD_Summary__c>> getEducationSummariesMap(Set<Id> contactIds, Set<Id> summaryIds){
        List<CPD_Summary__c> summaries = [
            SELECT Id, Contact_Student__r.AccountId, Triennium_Start_Date__c, Triennium_End_Date__c FROM CPD_Summary__c 
            WHERE Id IN :summaryIds OR Contact_Student__c IN :contactIds
        ];
        return getAccountSummariesMap(summaries, true);
    }

    //Get account summaries
    private static Map<Id, List<CPD_Summary__c>> getExemptionSummariesMap(Set<Id> accountIds, Set<Id> summaryIds){
        List<CPD_Summary__c> summaries = [
            SELECT Id, Contact_Student__r.AccountId, Triennium_Start_Date__c, Triennium_End_Date__c  FROM CPD_Summary__c 
            WHERE Id IN :summaryIds OR Contact_Student__r.AccountId IN :accountIds
        ];
        return getAccountSummariesMap(summaries, false);
    }

    //Get account summaries map
    private static Map<Id, List<CPD_Summary__c>> getAccountSummariesMap(List<CPD_Summary__c> summaries, Boolean contactAsKey){
        Map<Id, List<CPD_Summary__c>> summariesMap = new Map<Id, List<CPD_Summary__c>>();
        for(CPD_Summary__c summary : summaries){
            Id key = contactAsKey ? summary.Contact_Student__c : summary.Contact_Student__r.AccountId;
            if(!summariesMap.containsKey(key)) summariesMap.put(key, new List<CPD_Summary__c>{ summary });
            else summariesMap.get(key).add(summary); 
        }
        return summariesMap;
    }

    //Get education summaries
    private static List<CPD_Summary__c> getEducationSummaries(Set<Id> summaryIds){
        return [
            SELECT Id, Account__c, Specialisation_1__c, Specialisation_2__c, Specialisation_3__c, Specialisation_4__c, 
                Specialisation_5__c, Specialisation_6__c, Specialisation_7__c, Specialisation_8__c, Specialisation_Hours_1__c, 
                Specialisation_Hours_2__c, Specialisation_Hours_3__c, Specialisation_Hours_4__c, Specialisation_Hours_5__c,
                Specialisation_Hours_6__c, Specialisation_Hours_7__c, Specialisation_Hours_8__c
            FROM CPD_Summary__c WHERE Id IN :summaryIds 
        ];
    }

    //Get current triennium year summaries
    private static List<Account> getSummaryAccounts(Set<Id> accountIds){
        return [
            SELECT Id, Specialisation_1__c, Specialisation_2__c, Specialisation_3__c, Specialisation_4__c,
                Specialisation_5__c, Specialisation_6__c, Specialisation_7__c, Specialisation_8__c
            FROM Account WHERE Id IN :accountIds 
        ];
    }

    //Get contacts
    private static Map<Id, Contact> getContactsMap(List<Account> accounts){
        Map<Id, Contact> contactsMap = new Map<Id, Contact>();
        List<Contact> contacts = [
            SELECT Id, AccountId, First_Triennium_Start_Date__c, Last_Triennium_Start_Date__c 
            FROM Contact WHERE AccountId IN :accounts
        ];
        for(Contact cont : contacts){
            contactsMap.put(cont.AccountId, cont);
        }
        return contactsMap;
    }

    //Get new summary record
    private static CPD_Summary__c getSummaryRecord(Id contactId, Date startDate){
        return new CPD_Summary__c(
            Contact_Student__c = contactId, Triennium_Start_Date__c = startDate
            //Triennium_End_Date__c = startDate.addYears(3).addDays(-1)
        );
    }

	
    //Get new account with new specialisations
    private static Account getSpecialistAccount(SObject acc){
        initSpecialisationMappings();
        Account splAcc = new Account();
        for(CPD_Specialisation_Mapping__mdt mapping : specialisationMappings){
            if((Boolean)acc.get(mapping.Field__c)){
                if(splAcc.Specialisation_1__c == null){
                    splAcc.Specialisation_1__c = mapping.Specialisation__c;
                } else if(splAcc.Specialisation_2__c == null){
                    splAcc.Specialisation_2__c = mapping.Specialisation__c;
                } else if(splAcc.Specialisation_3__c == null){
                    splAcc.Specialisation_3__c = mapping.Specialisation__c;
                } else if(splAcc.Specialisation_4__c == null){
                    splAcc.Specialisation_4__c = mapping.Specialisation__c;
                } else if(splAcc.Specialisation_5__c == null){
                    splAcc.Specialisation_5__c = mapping.Specialisation__c;
                } else if(splAcc.Specialisation_6__c == null){
                    splAcc.Specialisation_6__c = mapping.Specialisation__c;
                } else if(splAcc.Specialisation_7__c == null){
                    splAcc.Specialisation_7__c = mapping.Specialisation__c;
                } else if(splAcc.Specialisation_8__c == null){
                    splAcc.Specialisation_8__c = mapping.Specialisation__c;
                } else {
                    break;
                }
            }
        }
        return splAcc;
    } 

    //Get cpd specialisation mappings
    private static void initSpecialisationMappings(){
        if(specialisationMappings == null){
            specialisationMappings = [
                SELECT Id, Sequence__c, Field__c, Specialisation__c 
                FROM CPD_Specialisation_Mapping__mdt 
                WHERE Specialisation__c != null ORDER BY Sequence__c ASC
            ];
        }
    }

    //Update the Triennium Year on Education records if changed. 
    public static void updateEducationTrienniumYear(List<Education_Record__c> educations, Map<Id, Education_Record__c> oldEducations){
        Set<Id> summaryIds = new Set<Id>(), educationIds = new Set<Id>();
        for(Education_Record__c education : educations){
            if(oldEducations != null){
                Education_Record__c oldEducation = oldEducations.get(education.Id);
                if(oldEducation.Date_Completed__c == education.Date_Completed__c && 
                    oldEducation.Triennium_Year__c == education.Triennium_Year__c &&
                  	oldEducation.Triennium_Year__c != null){
                    continue;
                }
            }
            if(education.CPD_Summary__c != null){
                summaryIds.add(education.CPD_Summary__c);
                educationIds.add(education.Id);
            }
        } 
        if(summaryIds.isEmpty()) return;

        List<CPD_Summary__c> summaries = [
            SELECT Id, Triennium_Start_Date__c, 
                (SELECT Id, Date_Completed__c, Triennium_Year__c FROM Education_Record__r WHERE Id IN : educationIds) 
            FROM CPD_Summary__c WHERE Id IN : summaryIds
        ];
        List<Education_Record__c> updatableEducations = new List<Education_Record__c>();
        for(CPD_Summary__c summary : summaries){ 
            Date firstYearStartDate = summary.Triennium_Start_Date__c;
            Date secondYearStartDate = getNextYearTrienniumDate(firstYearStartDate);
            Date thirdYearStartDate = getNextYearTrienniumDate(secondYearStartDate);
            Date fourthYearStartDate = getNextYearTrienniumDate(thirdYearStartDate);

            for(Education_Record__c education : summary.Education_Record__r){
                String trienniumYear =
                    isDateInBetween(firstYearStartDate, secondYearStartDate, education.Date_Completed__c) ? 'Year 1' :
                    isDateInBetween(secondYearStartDate, thirdYearStartDate, education.Date_Completed__c) ? 'Year 2' :
                    isDateInBetween(thirdYearStartDate, fourthYearStartDate, education.Date_Completed__c) ? 'Year 3' : 
                    education.Triennium_Year__c != 'Year X' ? 'Year X' : education.Triennium_Year__c;

                if(education.Triennium_Year__c != trienniumYear){
                    education.Triennium_Year__c = trienniumYear;
                    updatableEducations.add(education);
                }
            }
        }
        if(!updatableEducations.isEmpty()) update updatableEducations;
    }

    private static Boolean isDateInBetween(Date startDate, Date endDate, Date currentDate){
        return currentDate >= startDate && currentDate < endDate;
    }

    private static Date getNextYearTrienniumDate(Date trienniumDate){
        return Date.newInstance(trienniumDate.year() + 1, 7, 1);
    }
    
    public static void createBackupOfDeletedEducationRecord(List<Education_Record__c> cpdLogEducationList){
        List<Education_Record__c> educationRecordList = new List<Education_Record__c>();
        List<CPD_Log_Backup__c> backupEducationRecordList = new List<CPD_Log_Backup__c>();
        List<ID> educationRecordID = new List<ID>();
            if(cpdLogEducationList!=null && !cpdLogEducationList.isEmpty()){
                for(Education_Record__c educationCpdEntry : cpdLogEducationList){
                    educationRecordID.add(educationCpdEntry.Id);
                    
                    
                    
                }
                if(!educationRecordID.isEmpty()){
                    educationRecordList = [Select IsDeleted, Name, CurrencyIsoCode,Contact_Student__c, Award_as_Presenter__c, Bridging__c, 
                                           CAHDP__c,Community_User_Entry__c, Country__c, Date_Commenced__c, Date_completed__c, Degree_Country__c, 
                                           Degree_Other__c, Degree__c, Enter_university_instituition__c, Event_Course_name__c, Ext_Id__c, 
                                           Other_State__c, Primary_Qualification__c, Provider_State__c, Qualifying_hours__c, Qualifying_hours_type__c, 
                                           Specialist_hours_code__c, State__c, Training_and_development__c, University_professional_organisation__c, 
                                           Verified__c, Other_Type__c, Type__c, Resource_Booking__c, Professional_Competency__c, Type_of_Activity__c, 
                                           Migrated__c, CPD_Contact_and_Summary_Relate__c, CPD_End_Date_is_before_Triennium_Start__c, CPD_Summary__c,
                                           Description_Of_Activity__c, Linked_To_Current_Triennium__c, Triennium_Start_Date__c, Triennium_Year__c, 
                                           University_or_Institution__c, CPD_Processed__c, Dummy_field_to_trigger_update__c FROM Education_Record__c where Id in :educationRecordID];
                    
                }
                if(!educationRecordList.isEmpty()){
                    backupEducationRecordList = createBackupEducationRecords(educationRecordList);  
                }
                insert backupEducationRecordList;
            }
            
        
    }
    
    private static List<CPD_Log_Backup__c> createBackupEducationRecords (List<Education_Record__c> educationRecordList){
        List<CPD_Log_Backup__c> educationBackupRecordList = new List<CPD_Log_Backup__c>();
        for(Education_Record__c educationRecord : educationRecordList){
            CPD_Log_Backup__c educationBackupRecord = new CPD_Log_Backup__c();
            //educationBackupRecord.IsDeleted = educationRecord.IsDeleted;
            //educationBackupRecord.Name = educationRecord.Name;
            educationBackupRecord.CurrencyIsoCode = educationRecord.CurrencyIsoCode;
            educationBackupRecord.Contact_Student__c = educationRecord.Contact_Student__c;
            educationBackupRecord.Award_as_Presenter__c = educationRecord.Award_as_Presenter__c;
            educationBackupRecord.Bridging__c = educationRecord.Bridging__c;
            educationBackupRecord.CAHDP__c = educationRecord.CAHDP__c;
            educationBackupRecord.Community_User_Entry__c = educationRecord.Community_User_Entry__c;
            educationBackupRecord.Country__c = educationRecord.Country__c;
            educationBackupRecord.Date_Commenced__c = educationRecord.Date_Commenced__c;
            educationBackupRecord.Date_completed__c = educationRecord.Date_completed__c;
            educationBackupRecord.Degree_Country__c = educationRecord.Degree_Country__c;
            educationBackupRecord.Degree_Other__c = educationRecord.Degree_Other__c;
            educationBackupRecord.Degree__c = educationRecord.Degree__c;
            educationBackupRecord.Enter_university_instituition__c = educationRecord.Enter_university_instituition__c;
            educationBackupRecord.Event_Course_name__c = educationRecord.Event_Course_name__c;
            educationBackupRecord.Ext_Id__c = educationRecord.Ext_Id__c;
            educationBackupRecord.Other_State__c = educationRecord.Other_State__c;
            educationBackupRecord.Primary_Qualification__c = educationRecord.Primary_Qualification__c;
            educationBackupRecord.Provider_State__c = educationRecord.Provider_State__c;
            educationBackupRecord.Provider_State__c = educationRecord.Provider_State__c;
            educationBackupRecord.Qualifying_hours__c = educationRecord.Qualifying_hours__c;
            educationBackupRecord.Qualifying_hours_type__c = educationRecord.Qualifying_hours_type__c;
            educationBackupRecord.Specialist_hours_code__c = educationRecord.Specialist_hours_code__c;
            educationBackupRecord.State__c = educationRecord.Other_State__c;
            educationBackupRecord.Training_and_development__c = educationRecord.Training_and_development__c;
            educationBackupRecord.University_professional_organisation__c = educationRecord.University_professional_organisation__c;
            educationBackupRecord.Verified__c = educationRecord.Verified__c;
            educationBackupRecord.Other_Type__c = educationRecord.Other_Type__c;
            educationBackupRecord.Type__c = educationRecord.Type__c;
            educationBackupRecord.Resource_Booking__c = educationRecord.Resource_Booking__c;
            educationBackupRecord.Professional_Competency__c = educationRecord.Professional_Competency__c;
            educationBackupRecord.Type_of_Activity__c = educationRecord.Type_of_Activity__c;
            educationBackupRecord.Migrated__c = educationRecord.Migrated__c;
            //educationBackupRecord.CPD_Contact_and_Summary_Relate__c = educationRecord.CPD_Contact_and_Summary_Relate__c;
            //educationBackupRecord.CPD_End_Date_is_before_Triennium_Start__c = educationRecord.CPD_End_Date_is_before_Triennium_Start__c;
            educationBackupRecord.CPD_Summary__c = educationRecord.CPD_Summary__c;
            educationBackupRecord.Description_Of_Activity__c = educationRecord.Description_Of_Activity__c;
            //educationBackupRecord.Linked_To_Current_Triennium__c = educationRecord.Linked_To_Current_Triennium__c;
            //educationBackupRecord.Triennium_Start_Date__c = educationRecord.Triennium_Start_Date__c;
            //educationBackupRecord.Triennium_Year__c = educationRecord.Triennium_Year__c;
            //educationBackupRecord.University_or_Institution__c = educationRecord.University_or_Institution__c;
            educationBackupRecord.CPD_Processed__c = educationRecord.CPD_Processed__c;
            educationBackupRecord.Dummy_field_to_trigger_update__c = educationRecord.Dummy_field_to_trigger_update__c;
            educationBackupRecordList.add(educationBackupRecord);
        }
        return educationBackupRecordList;
    }

}