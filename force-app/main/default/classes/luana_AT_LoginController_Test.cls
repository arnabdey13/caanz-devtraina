/**
    Developer: WDCi (kh)
    Development Date:11/07/2016
    Task: Luana Test class for luana_ContributorAttDownloadCtl class
    
    Change History:
    LCA-614 08/06/2016: WDCi Lean - part of this is obsolete
**/
@isTest(seeAllData=false)
public class luana_AT_LoginController_Test {

    
    
     public static testMethod void testContributor1() { 
        
        Test.startTest();
            
            luana_LoginController login = new luana_LoginController();
            login.forwardToCustomAuthPage();
            login.login();
            login.getEncodedSiteUrl();
            login.getEncodedStartUrl();
            
        
        Test.stopTest();
    }

}