/*------------------------------------------------------------------------------------
Author:        Sumit Gupta
Company:       Salesforce
Description:   test class for cpd summary handler class

History
Date            Author             Comments
--------------------------------------------------------------------------------------
02-05-2018     Sumit Gupta       	Initial Release
------------------------------------------------------------------------------------*/
@isTest
private with sharing class CPDSummaryHandlerTest {

	private static List<String> specialistHoursCodes;
	
	static{
		specialistHoursCodes = new List<String>{
			'Type F - Financial Planning Specialist',
			'Type A - Registered Company Auditor',
			'Type L - Registered Company Liquidator',
			'Type T - Registered Tax Agent',
			'Type AF - Australian Financial Services Licensee (AFSL)', 
			'Type B - Registered Trustee in Bankruptcy'
		};
	}

	@testSetup static void createTestData() {
		createCPDDefaultValuesConfig();
    }

    private static void createCPDDefaultValuesConfig(){
    	CPD_Default_Values__c config = new CPD_Default_Values__c(
    		Total_Required_Annual_Hours_ACA__c = 15,
    		Total_Required_Annual_Hours_AT__c = 60,
    		Total_Required_Annual_Hours_CA__c = 20,
    		Total_Required_Triennium_Hours_ACA__c = 90,
    		Total_Required_Triennium_Hours_AT__c = 10,
    		Total_Required_Triennium_Hours_CA__c = 120,
    		Maximum_Triennium_Informal_Hours_AU_CA__c = 30
    	);
    	insert config;
    }

    private static Account getMemberAccount(){
    	Account acc = TestObjectCreator.createPersonAccount();
    	acc.Designation__c = 'ACA';
    	acc.Membership_Type__c = 'Member';
    	acc.Membership_Approval_Date__c = Date.today().addDays(1);
    	return acc;
    }

    private static Account getMemberAccountWithSpecialisations(){
    	Account acc = getMemberAccount();

    	acc.FP_Specialisation__c = true;
    	acc.Registered_Company_Auditor__c = true;
    	acc.Registered_Company_Liquidator__c = true;
    	acc.Registered_Tax_Agent__c = true;

    	return acc;
    }

    private static Education_Record__c getEducationRecord(Id contactId, Id summaryId, String specialHourCode, Decimal hours){
        Education_Record__c education = new Education_Record__c(
        	Contact_Student__c = contactId,
        	CPD_Summary__c = summaryId,
        	Specialist_hours_code__c = specialHourCode,
        	Qualifying_hours__c = hours,
        	Date_Commenced__c = System.today(),
        	Event_Course_name__c = 'Course_1',
        	Enter_university_instituition__c = 'University ABC'
        );
        return education;
    }

    private static List<CPD_Summary__c> getSummaries(){
    	return [
			SELECT Id, Contact_Student__c, Triennium_Start_Date__c, Triennium_End_Date__c,
			Specialisation_1__c, Specialisation_2__c, Specialisation_3__c, Specialisation_4__c,
			Specialisation_Hours_1__c, Specialisation_Hours_2__c, Specialisation_Hours_3__c, 
			Specialisation_Hours_4__c
			FROM CPD_Summary__c
		];
    }

    static testMethod void test_createSummaryRecords() {
    	Account acc = getMemberAccount();
    	insert acc;

    	List<CPD_Summary__c> summaries = getSummaries();
		system.assertEquals(3, summaries.size());
    }

    static testMethod void test_createSummmaryRecordsWithSpecialisations() {
    	Account acc = getMemberAccountWithSpecialisations();
    	insert acc;

    	List<CPD_Summary__c> summaries = getSummaries(); system.debug(summaries);
		system.assertEquals(3, summaries.size());

		for(CPD_Summary__c summary : summaries){
			system.assertEquals(specialistHoursCodes[0], summary.Specialisation_1__c);
			system.assertEquals(specialistHoursCodes[1], summary.Specialisation_2__c);
			system.assertEquals(specialistHoursCodes[2], summary.Specialisation_3__c);
			system.assertEquals(specialistHoursCodes[3], summary.Specialisation_4__c);
		}
    }

    static testMethod void test_updateAccountSpecialisations() {
    	Account acc = getMemberAccountWithSpecialisations();
    	insert acc;

    	acc.Registered_Company_Auditor__c = false;
    	acc.Registered_Company_Liquidator__c = false;
    	acc.Registered_Trustee_in_Bankruptcy__c = true;

    	update acc;

    	List<CPD_Summary__c> summaries = getSummaries();
		system.assertEquals(3, summaries.size());

		for(CPD_Summary__c summary : summaries){
			if(summary.Triennium_End_Date__c >= Date.today()){
				system.assertEquals(specialistHoursCodes[0], summary.Specialisation_1__c);
				system.assertEquals(specialistHoursCodes[3], summary.Specialisation_2__c);
				system.assertEquals(specialistHoursCodes[5], summary.Specialisation_3__c);
			}
		}
    }

    static testMethod void test_createEducationRecordsWithSpecialisationHours() {
    	Account acc = getMemberAccountWithSpecialisations();
    	insert acc;

    	CPD_Summary__c summary = getSummaries().get(1);
    	List<Education_Record__c> educations = new List<Education_Record__c>();

    	educations.add(getEducationRecord(summary.Contact_Student__c, summary.Id, specialistHoursCodes[0], 10));
		educations.add(getEducationRecord(summary.Contact_Student__c, summary.Id, specialistHoursCodes[1], 20));
		educations.add(getEducationRecord(summary.Contact_Student__c, summary.Id, specialistHoursCodes[2], 30));
		educations.add(getEducationRecord(summary.Contact_Student__c, summary.Id, specialistHoursCodes[3], 40));

		insert educations;

		List<CPD_Summary__c> summaries = getSummaries();
		system.assertEquals(3, summaries.size());

		for(CPD_Summary__c record : summaries){
			if(record.Id == summary.Id){
				system.assertEquals(10, record.Specialisation_Hours_1__c);
				system.assertEquals(20, record.Specialisation_Hours_2__c);
				system.assertEquals(30, record.Specialisation_Hours_3__c);
				system.assertEquals(40, record.Specialisation_Hours_4__c);
			}
		}
    }

    static testMethod void test_createExemptionRecord() {
    	Account acc = getMemberAccountWithSpecialisations();
    	insert acc;

    	List<CPD_Summary__c> summaries = getSummaries();
    	CPD_Summary__c summary = summaries.get(1);

    	CPD_Exemption__c exemption = new CPD_Exemption__c(
    		CPD_Summary__c = summary.Id, Exemption_Hours__c = 10, Formal_verified_exemption_hours__c = 10
    	);
    	insert exemption;

    	exemption = [SELECT Account_Name__c FROM CPD_Exemption__c];

    	system.assertEquals(acc.Id, exemption.Account_Name__c);
    }

	static testMethod void test_allTogether() {
		List<Account> accounts = new List<Account>{ getMemberAccount(), getMemberAccount() };
		insert accounts;

		List<CPD_Summary__c> summaries = getSummaries();
		system.assertEquals(6, summaries.size());

		List<Education_Record__c> educations = new List<Education_Record__c>();
		Education_Record__c education, education1, education2;
		CPD_Summary__c summary = summaries.get(0);
		Account acc = accounts.get(0);

		summary.Specialisation_1__c = acc.Specialisation_1__c = specialistHoursCodes[0];
		summary.Specialisation_2__c = acc.Specialisation_2__c = specialistHoursCodes[1];

		educations.add(getEducationRecord(summary.Contact_Student__c, summary.Id, acc.Specialisation_1__c, 10));
		educations.add(getEducationRecord(summary.Contact_Student__c, summary.Id, acc.Specialisation_2__c, 20));

		update acc;
		insert educations;

		educations.clear();

		summary.Specialisation_3__c = specialistHoursCodes[2];
		summary.Specialisation_4__c = specialistHoursCodes[3];

		educations.add(getEducationRecord(summary.Contact_Student__c, summary.Id, summary.Specialisation_2__c, 30));
		educations.add(education1 = getEducationRecord(summary.Contact_Student__c, summary.Id, summary.Specialisation_3__c, 40));
		educations.add(education2 = getEducationRecord(summary.Contact_Student__c, summary.Id, summary.Specialisation_4__c, 50));

		update summary;
		insert educations;

		summaries = getSummaries();

		for(CPD_Summary__c record : summaries){
			if(record.Id == summary.Id){
				system.assertEquals(10, record.Specialisation_Hours_1__c);
				system.assertEquals(50, record.Specialisation_Hours_2__c);
				system.assertEquals(40, record.Specialisation_Hours_3__c);
				system.assertEquals(50, record.Specialisation_Hours_4__c);
			}
		}

		educations.clear();

		education = getEducationRecord(summary.Contact_Student__c, summary.Id, summary.Specialisation_1__c, 60);
		education.Id = education1.Id;
		educations.add(education);

		education = getEducationRecord(summary.Contact_Student__c, summary.Id, summary.Specialisation_2__c, 70);
		education.Id = education2.Id;
		educations.add(education);

		educations.add(getEducationRecord(summary.Contact_Student__c, summary.Id, summary.Specialisation_3__c, 80));

		update summary;
		upsert educations;

		summaries = getSummaries();

		for(CPD_Summary__c record : summaries){
			if(record.Id == summary.Id){
				system.assertEquals(70, record.Specialisation_Hours_1__c);
				system.assertEquals(120, record.Specialisation_Hours_2__c);
				system.assertEquals(80, record.Specialisation_Hours_3__c);
				system.assertEquals(0, record.Specialisation_Hours_4__c);
			}
		}
	}

	static testMethod void test_CPDSummaryListController() {
		CPDSummaryListController.getCPDSummariesForUser();
		CPDSummaryListController.getEducationRecordsForUser();
	}
    
    static testMethod void test_CPDSummaryScheduler() {
        (new CPDSummaryScheduler()).execute(null);
    }
    
    @isTest
    public static void testCreateBackupOfDeletedEducationRecord(){
        //CPDSummaryHandler.createBackupOfDeletedEducationRecord
    }
}