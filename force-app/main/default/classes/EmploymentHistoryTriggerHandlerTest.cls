/*
    Test class for EmploymentHistoryTriggerHandler.cls
    ======================================================
    Changes:
    	Jul 2016 	Davanti - YK	Created
    	Aug 2016	Davanti - RN	EHCH - Updated for after delete trigger handler
*/

@isTest
private class EmploymentHistoryTriggerHandlerTest {

    @testSetup
    static void setup() {
    	
    	Account accountEmployer = TestObjectCreator.createBusinessAccount();
    	Account accountMember = TestObjectCreator.createPersonAccount();
    	AccountMember.CPP_Picklist_NZ__c = 'Full';
    	Accountmember.CPP_Picklist_AU__c = 'Full';
    	Accountmember.Designation__c = 'Affiliate CAANZ';
    	
    	List<Account> listAccount = new List<Account>();
    	listAccount.add(accountEmployer);
    	listAccount.add(accountMember);
    	
    	insert listAccount;
    	
    	Employment_History__c employmentHistory = new Employment_History__c(
	    	Employer__c = listAccount[0].Id,//accountEmployer.Id,
	    	Member__c = listAccount[1].Id,//accountMember.Id,
	    	Job_Title__c = 'Tester',
	    	Employee_Start_Date__c = Date.today(), 
	    	Status__c = 'Current',
	    	Is_CPP_Provided__c = false,
	    	Primary_Employer__c = true
    	);
    	
    	Employment_History__c employmentHistory2 = new Employment_History__c(
	    	Employer__c = listAccount[0].Id,//accountEmployer.Id,
	    	Member__c = listAccount[1].Id,//accountMember.Id,
	    	Job_Title__c = 'Tester',
	    	Employee_Start_Date__c = Date.today(),
	    	Status__c = 'Current',
	    	Is_CPP_Provided__c = true,
	    	Primary_Employer__c = false
    	);
    	
        insert new List<Employment_History__c>{employmentHistory, employmentHistory2};
	}
		
	static testMethod void Test_OnBeforeUpdate() {
		
		Employment_History__c EH1;
		Employment_History__c EH2;
		
		for(Employment_History__c EH : [SELECT Id, Is_CPP_Provided__c, Primary_Employer__c, Status__c FROM Employment_History__c]){
			if(EH.Is_CPP_Provided__c == false && EH.Primary_Employer__c == true)
				EH1 = EH;
			else if(EH.Is_CPP_Provided__c == true && EH.Primary_Employer__c == false)
				EH2 = EH;
		}
		
		if(EH1 != null){
			EH1.Is_CPP_Provided__c = true;
			EH1.Primary_Employer__c = false;
			update EH1;
		}
		
		if(EH2 != null){
			EH2.Is_CPP_Provided__c = false;
			EH2.Primary_Employer__c = true;
			update EH2;
		}
		
		if(EH1 != null){
			EH1.Status__c = 'Default';
			update EH1;
		}
	}
	
	static testMethod void Test_onAfterDelete(){
		// Set some Bus and member Accts with Employee coordinators and primary employers
		List<Id> listAccountId = new List<Id>();
		List<Employment_History__c> listEH = [SELECT Id, Member__c, Employer__c, Is_Employee_Coordinator__c, Primary_Employer__c, Status__c FROM Employment_History__c];
		system.debug('### listEH: ' + listEH);
		for(Employment_History__c eh : listEH){
			eh.Is_Employee_Coordinator__c = true;
			//eh.Primary_Employer__c = true;
			listAccountId.add(eh.Member__c);
			listAccountId.add(eh.Employer__c);
		}
		update listEH;
		 
		Map<Id,Account> mapAccount = new map<Id, Account>([SELECT Id, Employee_Coordinator_Lookup__c, Primary_Employer__c FROM Account WHERE Id IN :listAccountId]);
		
		for(Employment_History__c eh : listEH){
			Account acctBus = mapAccount.get(eh.Employer__c);
			Account acctMem = mapAccount.get(eh.Member__c);
			//System.assert(acctBus.Employee_Coordinator_Lookup__c == eh.Member__c); removed for EHCH Prod deployment 3 Dec 2016
			//System.assert(acctMem.Primary_Employer__c == eh.Employer__c); removed for EHCH Prod deployment 3 Dec 2016
		}
		
		// Now delete the records
		delete listEH;
		
		for(Account acct : [SELECT Id, Employee_Coordinator_Lookup__c, Primary_Employer__c FROM Account WHERE Id IN :listAccountId]){
			//system.assert(acct.Employee_Coordinator_Lookup__c == null); removed for EHCH Prod deployment 3 Dec 2016
			//system.assert(acct.Primary_Employer__c == null); removed for EHCH Prod deployment 3 Dec 2016
		}
		
	}
}