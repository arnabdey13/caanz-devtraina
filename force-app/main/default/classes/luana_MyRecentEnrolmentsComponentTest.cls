/*
    Developer: WDCi (Lean)
    Development Date: 13/04/2016
    Task #: Test luana_MyRecentEducationRecordsComponent for enrolment wizard
    
    Change history
    LCA-921 23/08/2019 WDCi - KH: Add person account email
*/

@isTest(seeAllData=false)
private class luana_MyRecentEnrolmentsComponentTest {
    
    public static Account member;
    public static User memberUser;
    public static Luana_DataPrep_Test dataPrep;
    public static luana_CommUserUtil userUtil {get; set;} 
    
    public static LuanaSMS__Student_Program__c stuProgm {get; set;}
    public static LuanaSMS__Delivery_Location__c devLocation {get; set;}
    public static Course_Delivery_Location__c courseDevLoc {get; set;}
    public static List<LuanaSMS__Subject__c> subjList {get; set;}
    
    private static String classNamePrefixLong = 'luana_MyRecentEnrolmentsComponentTest';
    private static String classNamePrefixShort = 'lmrec';
    private static map<String, Id> commProfIdMap {get; set;}
    public static Account memberAccount {get; set;}
    public static List<Product2> prodList {get; set;}
    public static LuanaSMS__Course__c courseAM {get; set;}
    public static LuanaSMS__Course__c courseAP {get; set;}
    
    public static void initial(){
        //initialize
        dataPrep = new Luana_DataPrep_Test();
        
        //Create all the custom setting
        insert dataPrep.prepLuanaExtensionSettingCustomSettings();
        insert dataPrep.createLuanaConfigurationCustomSetting();
        
        //Create user with Member and Employer community access
        memberAccount = dataPrep.generateNewApplicantAcc('Joe_' + classNamePrefixShort, classNamePrefixLong, 'Full_Member');
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.PersonEmail = 'joe_1_' +classNamePrefixShort+'@gmail.com';//LCA-921
        memberAccount.Communication_Preference__c= 'Home Phone';
        memberAccount.PersonHomePhone= '1234';
        memberAccount.PersonOtherStreet= '83 Saggers Road';
        memberAccount.PersonOtherCity='JITARNING';
        memberAccount.PersonOtherState='Western Australia';
        memberAccount.PersonOtherCountry='Australia';
        memberAccount.PersonOtherPostalCode='6365';
        insert memberAccount;
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
    }
    
    public static void setup(){
        //LCA-921
        memberUser = [Select Id, Email, FirstName, LastName, UserName, Name, ContactId from User Where Email =: 'joe_1_' + classNamePrefixShort+'@gmail.com' limit 1];
        //memberUser = testDataGenerator.generateNewApplicantUser(classNamePrefixLong, classNamePrefixshort, memberAccount, commProfIdMap.get('Customer Community Login User'));
        //insert memberUser;
        
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
       
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defEmployerPermSetId.value__c));
        list<PermissionSetAssignment> permissAssign = [Select Id from PermissionSetAssignment where AssigneeId =: memberUser.Id AND (PermissionSetId =: defMemberPermSetId.value__c OR PermissionSetId =: defEmployerPermSetId.value__c)];
        System.runAs(memberUser){
            if(permissAssign == null)
                insert psas;
        }
        
        userUtil = new luana_CommUserUtil(memberUser.Id);
               
        //Create traning org
        LuanaSMS__Training_Organisation__c trainingOrg = dataPrep.createNewTraningOrg(classNamePrefixLong, classNamePrefixLong, classNamePrefixShort, 'St 123', 'Wollonggong', '2244');
        insert trainingOrg;
        
        //Create Delivery Location
        //devLocation = dataPrep.createNewDeliveryLocationRecord('Australia - Western Australia', trainingOrg.Id);
        
        //Create program
        LuanaSMS__Program__c progrm = dataPrep.createNewProgram(classNamePrefixShort, classNamePrefixLong, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        insert progrm;
        
        //Create Product
        prodList = new List<Product2>();
        prodList.add(dataPrep.createNewProduct('FIN_AU_' + classNamePrefixLong, 'AU0001'));
        prodList.add(dataPrep.createNewProduct('FIN_NZ_' + classNamePrefixLong, 'NZ0001'));
        prodList.add(dataPrep.createNewProduct('FIN_INT_' + classNamePrefixLong, 'INT0001'));
        insert prodList;
        
        //Create Product
        List<Product2> examLocationFeeProds = new List<Product2>();
        Product2 auProd = dataPrep.createNewProduct('Oversea Exam Location (AU)_' + + classNamePrefixLong, 'AU0001');
        auProd.IsActive = true;
        auProd.Member_Of__c = 'ICAA';
        examLocationFeeProds.add(auProd);
        Product2 nzProd = dataPrep.createNewProduct('Oversea Exam Location (NZ)_' + classNamePrefixLong, 'NZ0001');
        nzProd.IsActive = true;
        nzProd.Member_Of__c = 'ICAA';
        examLocationFeeProds.add(nzProd);
        insert examLocationFeeProds;
        
        List<Luana_Extension_Settings__c> csList = new List<Luana_Extension_Settings__c>();
        
        csList.add(new Luana_Extension_Settings__c(Name = 'ExamLocationFeeProduct_Australia', Value__c = examLocationFeeProds[0].Id));
        csList.add(new Luana_Extension_Settings__c(Name = 'ExamLocationFeeProduct_New Zealand', Value__c = examLocationFeeProds[1].Id));
        csList.add(new Luana_Extension_Settings__c(Name = 'ExamLocationFeeProduct_Overseas', Value__c = examLocationFeeProds[0].Id));
        insert csList;
        
        //create price book entry
        /* Lean - 20/05/2016 - disabling to avoid lock contention
        List<PricebookEntry> pbes = new List<PricebookEntry>();
        pbes.add(dataPrep.createPricebookEntry(Test.getStandardPricebookId(), prodList[0].Id, 1300));
        pbes.add(dataPrep.createPricebookEntry(Test.getStandardPricebookId(), prodList[1].Id, 1300));
        pbes.add(dataPrep.createPricebookEntry(Test.getStandardPricebookId(), prodList[2].Id, 1300));
        
        pbes.add(dataPrep.createPricebookEntry(Test.getStandardPricebookId(), examLocationFeeProds[0].Id, 370));
        pbes.add(dataPrep.createPricebookEntry(Test.getStandardPricebookId(), examLocationFeeProds[1].Id, 470));
        insert pbes;
        */
        
        //Create Program Offering
        LuanaSMS__Program_Offering__c poAM = dataPrep.createNewProgOffering('PO_AM_' + classNamePrefixLong, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), progrm.Id, trainingOrg.Id, prodList[0].Id,prodList[1].Id, prodList[2].Id, 1, 1);
        insert poAM;
        
        LuanaSMS__Program_Offering__c poAP = dataPrep.createNewProgOffering('PO_AP_' + classNamePrefixLong, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Program'), progrm.Id, trainingOrg.Id, prodList[0].Id,prodList[1].Id, prodList[2].Id, 1, 1);
        insert poAP;
        
        //Create multiple subjects
        subjList = new List<LuanaSMS__Subject__c>();
        subjList.add(dataPrep.createNewSubject('TAX AU_' + classNamePrefixShort, 'TAX AU_' + classNamePrefixLong, 'TAX AU_' + classNamePrefixShort, 1, 'Module'));
        subjList.add(dataPrep.createNewSubject('TAX NZ_' + classNamePrefixShort, 'TAX NZ_' + classNamePrefixLong, 'TAX NZ_' + classNamePrefixShort, 1, 'Module'));
        subjList.add(dataPrep.createNewSubject('AAA_' + classNamePrefixShort, 'AAA_' + classNamePrefixLong, 'AAA_' + classNamePrefixShort, 1, 'Module'));
        subjList.add(dataPrep.createNewSubject('Cap_' + classNamePrefixShort, 'Capstone_' + classNamePrefixLong, 'Cap_' + classNamePrefixShort, 1, 'Module'));
        subjList.add(dataPrep.createNewSubject('FIN_' + classNamePrefixShort, 'FIN_' + classNamePrefixLong, 'FIN_' + classNamePrefixShort, 55, 'Module'));
        subjList.add(dataPrep.createNewSubject('MAAF_' + classNamePrefixShort, 'MAAF_' + classNamePrefixLong, 'MAAF_' + classNamePrefixShort, 60, 'Module'));
        insert subjList;
        
        //Create multiple Program Offering Subject
        List<LuanaSMS__Program_Offering_Subject__c> posList = new List<LuanaSMS__Program_Offering_Subject__c>();
        for(LuanaSMS__Subject__c sub: subjList){
            if(sub.name =='TAX AU_' + classNamePrefixShort || sub.name =='TAX NZ_' + classNamePrefixShort){
                posList.add(dataPrep.createNewProgOffSubject(poAP.Id, sub.Id, 'Elective'));
            }else{
                posList.add(dataPrep.createNewProgOffSubject(poAP.Id, sub.Id, 'Core'));
            }
        }
        insert posList;
        
        List<LuanaSMS__Program_Offering_Subject__c> posAMList = new List<LuanaSMS__Program_Offering_Subject__c>();
        for(LuanaSMS__Subject__c sub: subjList){
            if(sub.name =='TAX AU_' + classNamePrefixShort || sub.name =='TAX NZ_' + classNamePrefixShort){
                posAMList.add(dataPrep.createNewProgOffSubject(poAM.Id, sub.Id, 'Elective'));
            }else{
                posAMList.add(dataPrep.createNewProgOffSubject(poAM.Id, sub.Id, 'Core'));
            }
        }
        insert posAMList;
        
        //Create course
        courseAP = dataPrep.createNewCourse('Graduate Diploma of Chartered Accounting_' + classNamePrefixShort, poAP.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Program'), 'Running');
        courseAP.LuanaSMS__Allow_Online_Enrolment__c = true;
        insert courseAP;
        
        courseAM= dataPrep.createNewCourse('AAA216', poAM.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        courseAM.LuanaSMS__Allow_Online_Enrolment__c = true;
        insert courseAM;
        
        List<LuanaSMS__Program_Offering_Subject__c> posResult = [Select Id, Name from LuanaSMS__Program_Offering_Subject__c Where LuanaSMS__Program_Offering__c =: poAP.Id];
        System.assertEquals(posResult.isEmpty(), false, 'Expect Program Offering Subject is created in this PO AP ' + poAP.Name);
        
        
        //Set default enrolloment course id
        Luana_Extension_Settings__c defCourseEnrolmentId = new Luana_Extension_Settings__c(Name = 'Default_CA_Program_Course_Id', Value__c = courseAP.Id);
        insert defCourseEnrolmentId;
        
        List<Relationship__c> relsShip = new List<Relationship__c>();
        relsShip.add(dataPrep.createRelationship(userUtil.custCommAccId, userUtil.custCommAccId, 'Current', 'Mentor', 'Mentee', System.today(), System.today().addDays(10)));
        insert relsShip;
        
        devLocation = dataPrep.createNewDeliveryLocationRecord('Australia - South Australia - Adelaide_' + classNamePrefixShort, 'Australia - South Australia - Adelaide_' +classNamePrefixLong, trainingOrg.Id, '1000', 'Australia');
        insert devLocation;
        
        courseDevLoc = dataPrep.createCourseDeliveryLocation(courseAM.Id, devLocation.Id, 'Exam Location');
        insert courseDevLoc;
        
        List<Terms_and_Conditions__c> tncs = dataPrep.createDefaultTNCs();
        insert tncs;
        
        List<Attachment> attchs = new List<Attachment>();
        for(Terms_and_Conditions__c tnc: tncs){
            attchs.add(dataPrep.addAttachmentToParent(tnc.Id, classNamePrefixLong + '-attchment')); 
        }
        insert attchs;
        
        insert dataPrep.generateNewBusinessAcc('Price Waterhouse_' + classNamePrefixShort, 'Business_Account', 'Chartered Accounting', '11111111', '012112111', 'NZICA', '123 Main Street', 'Active');
        //dataPrep.generateNewBusinessAcc();
        String bAccount = 'Price Waterhouse_' + classNamePrefixShort;
        Account acc = [Select Id, Name from Account Where Name =: bAccount limit 1];
        
        Employment_History__c emplomentHistory = dataPrep.createNewEmploymentHistory(userUtil.custCommAccId, acc.Id, 'Current', 'Engineer');
        insert emplomentHistory;
        
        
        //createEmplToken(String longContextName, Id accId, String tokenCodeValue, String shortContextName, date expDate)
        List<Employment_Token__c> employerTokens = new List<Employment_Token__c>();
        employerTokens.add(dataPrep.createEmplToken(classNamePrefixLong + '_Token_valid', acc.Id, '11111111', classNamePrefixShort, System.today().addDays(10), true));
        employerTokens.add(dataPrep.createEmplToken(classNamePrefixLong + '_Token_expired', acc.Id, '22222222', classNamePrefixShort, System.today().addDays(-2), true));
        insert employerTokens;
        

        //Create Student Program
        stuProgm =  dataPrep.createNewStudentProgram(dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), userUtil.custCommConId, courseAM.Id, 'Australia', 'In Progress');
        stuProgm.LuanaSMS__Enrolment_Date__c = System.today().addMonths(1);
        stuProgm.Opt_In__c = true;
        
        insert stuProgm;
        System.debug('****:stuProgm: ' + stuProgm);
    }
    
    static testMethod void testViewRecentEnrolments() { 
        
        Test.startTest();
            initial();
        Test.stopTest();
        
        setup();
        System.runAs(memberUser){
            
            luana_MyRecentEnrolmentsComponent myRecentEnrolmentViewCtl = new luana_MyRecentEnrolmentsComponent();
            myRecentEnrolmentViewCtl.setContactId(userUtil.custCommConId);
            myRecentEnrolmentViewCtl.getContactId();
            myRecentEnrolmentViewCtl.setEnabledView(true);
            myRecentEnrolmentViewCtl.getEnabledView();
            
            myRecentEnrolmentViewCtl.getEnrolments();
            myRecentEnrolmentViewCtl.getViewURL();
            
        }
    }
    
}