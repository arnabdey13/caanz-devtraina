@isTest
public class RecognitionBusinessUtilTest {

     @testSetup 
    static void setup() {

        Account smallBusinessAccountAU_1 = SegmentationDataGenTest.genPracticeAU( 'James Smith', 'Chartered Accounting', 'Sydney', 2);
		insert smallBusinessAccountAU_1;
        Account smallBusinessAccountAU_2 = SegmentationDataGenTest.genPracticeAU( 'John Brown', 'Chartered Accounting', 'Brisbane', 2);
		insert smallBusinessAccountAU_2;
        Account smallBusinessAccountAU_3 = SegmentationDataGenTest.genPracticeAU( 'John and Co', 'Chartered Accounting', 'Melbourne', 2);
		insert smallBusinessAccountAU_3;
        
        Reference_Recognition_CAANZ_Repository__c  jamesS = new Reference_Recognition_CAANZ_Repository__c (
             Company_Name__c     = 'James Smith'
            , Account__c     	 = smallBusinessAccountAU_1.Id
            , BillingCountry__c  = 'Australia'
            , BillingCity__c     = 'Sydney'
        );
        insert jamesS;
        
		Reference_Recognition_CAANZ_Repository__c  johnB = new Reference_Recognition_CAANZ_Repository__c (
             Company_Name__c     = 'John Brown'
            , Account__c     	 = smallBusinessAccountAU_2.Id
            , BillingCountry__c  = 'Australia'
            , BillingCity__c     = 'Brisbane'
        );
        insert johnB;
        
		Reference_Recognition_CAANZ_Repository__c  johnC = new Reference_Recognition_CAANZ_Repository__c (
             Company_Name__c     = 'John and Co'
            , Account__c     	 = smallBusinessAccountAU_3.Id
            , BillingCountry__c  = 'Australia'
            , BillingCity__c     = 'Melbourne'
        );
        insert johnC;
    }
    
     /**
     * Test Business Utils for Recognition
     */
	private static testMethod void testStd(){
        RecognitionBusinessUtil rbu = new RecognitionBusinessUtil();
        RecognitionBusinessUtil.loadBusNameLookups();
        
        String locality = RecognitionBusinessUtil.getStdLocality('AUSTRALIAN CAPITAL TERRITORY');        
        System.debug('Locality Standardisation');
        System.debug(locality);
        System.assertEquals('ACT', locality);
        
        String legal = RecognitionBusinessUtil.getStdLegal( 'LIMITED');
        System.debug('Legal Standardisation');
        System.debug(legal);
        System.assertEquals('LTD', legal);
        
		String cleanNoise = RecognitionBusinessUtil.rmLegalNoise('ABC COMPANY P./L AU');
        System.debug('Common Noise Removal');
        System.debug('@' + cleanNoise + '@');
        System.assertEquals('ABC COMPANY AU', cleanNoise);

     	String cleanLocality = RecognitionBusinessUtil.rmLocalityNoise('XYZ COMPANY P./L AU');
        System.debug('Locality Noise Removal');
        System.debug('@' + cleanLocality + '@');
        System.assertEquals('XYZ COMPANY P./L', cleanLocality);   
        
        String cleanName = RecognitionBusinessUtil.rmLegalNoise('ABC COMPANY P./L AU');
        cleanName = RecognitionBusinessUtil.rmLocalityNoise(cleanName);
        System.debug('Noise Removal');
        System.debug(cleanName);
        System.assertEquals('ABC COMPANY', cleanName);   
        
		cleanName = RecognitionBusinessUtil.rmLegalNoise('ABC  COMPANY   P./L   AU');
        cleanName = RecognitionBusinessUtil.rmLocalityNoise(cleanName);
        System.debug('Noise Removal - ABC  COMPANY   P./L   AU');
        System.debug(cleanName);
        System.assertEquals('ABC COMPANY', cleanName);   
        
        String cleanTrustee = RecognitionBusinessUtil.rmTrusteeNoise('ABC COMPANY AS TRUSTEE OF THE SMITH FAMILY AU');
        cleanTrustee = RecognitionBusinessUtil.rmLocalityNoise(cleanTrustee);
        System.debug('Noise Trustee Removal - ABC COMPANY AS TRUSTEE OF THE SMITH FAMILY AU');
        System.debug(cleanTrustee);
        System.assertEquals('ABC COMPANY SMITH FAMILY', cleanTrustee);  
        
        //String cleanLegal = RecognitionBusLkpUtil.rmTrusteeNoise('ABC COMPANY AS TRUSTEE OF THE SMITH FAMILY P./L    INCORPORATED AU');
        String cleanLegal = RecognitionBusinessUtil.rmTrusteeNoise('ABC COMPANY AS TRUSTEE OF THE SMITH FAMILY AU');
        System.debug('Noise Trustee & Legal Removal - ABC COMPANY AS TRUSTEE OF THE SMITH FAMILY INCORPORATED AU'); 
        cleanLegal = RecognitionBusinessUtil.rmCommonNoise(cleanLegal);
        cleanLegal = RecognitionBusinessUtil.rmLegalNoise(cleanLegal);
        cleanLegal = RecognitionBusinessUtil.rmLocalityNoise(cleanLegal);

        System.debug(cleanLegal);
        System.assertEquals('ABC COMPANY SMITH FAMILY', cleanTrustee);  
        
        String cleanAll = RecognitionBusinessUtil.rmAllNoise('ABC COMPANY AS TRUSTEE OF THE SMITH FAMILY P./L    INCORPORATED AU');
		System.debug(cleanAll);
		System.assertEquals('ABC COMPANY SMITH FAMILY', cleanAll);  
        
        String cleanCity = RecognitionBusinessUtil.rmCity('ABC COMPANY ADELAIDE');
		System.debug(cleanCity);
		System.assertEquals('ABC COMPANY', cleanCity);  
        
        String Company1;
		String Company2;
        
//        System.debug(RecognitionMatching.isEqualString('2609','2604', 2));
        System.debug(RecognitionBusinessUtil.stdBusName('Michael J Salvato Pty Ltd'));
            
        System.debug(RecognitionBusinessUtil.stdImportantPractice('ERNST YOUNG SERVICES ADELAIDE'));
        System.debug(RecognitionBusinessUtil.stdImportantPractice('PRICEWATERHOUSECOOPERS CONSULTING TAX LEGAL'));
        
        System.debug(RecognitionBusinessUtil.stdBusName('PricewaterhouseCoopers Morrinsville'));
        System.debug(RecognitionBusinessUtil.stdBusName('PricewaterhouseCoopers Tax and Legal Pty Ltd'));
        System.debug(RecognitionBusinessUtil.stdBusName('Ernst and Young Services Pty Limited ADELAIDE'));
        System.debug(RecognitionBusinessUtil.stdBusName('Ernst and Young Real Estate Pty Ltd'));
        
        System.debug(RecognitionBusinessUtil.stdBusName('KPMG Audit Auckland'));
        System.debug(RecognitionBusinessUtil.stdBusName('KPMG Christchurch'));
        System.debug(RecognitionBusinessUtil.stdBusName('Deloitte and Touche Tohmatsu'));
        System.debug(RecognitionBusinessUtil.stdBusName('Deloitte Growth Solutions'));
        
        System.debug(RecognitionBusinessUtil.stdBusName('Pitcher Partners BA&A Pty Ltd'));
        System.debug(RecognitionBusinessUtil.stdBusName('Grant Thornton Christchurch'));
        System.debug(RecognitionBusinessUtil.stdBusName('Grant Rae Chartered Accountant Ltd'));
        System.debug(RecognitionBusinessUtil.stdBusName('Grant T. Russell Pty Ltd'));
        System.debug(RecognitionBusinessUtil.stdBusName('Grant Thornton Forensics Pty Ltd'));
       
        
        System.debug(RecognitionBusinessUtil.getStreet('Level 2 - 81 Carlton Gore Road  Newmarket') );
        
        System.debug(RecognitionBusinessUtil.getStreet(' Unit 1 123 George St'));
        System.debug(RecognitionBusinessUtil.getStreet('1 George St Sydney'));
        System.debug(RecognitionBusinessUtil.getStreet('University of Sydney 123 George St'));
        

		        
        //Company1 = RecognitionBusinessUtil.genBusNameSearchKey('Deloitte ADELAIDE');
        //Company2 = RecognitionBusinessUtil.genBusNameSearchKey('Deloitte (Adelaide)');
		System.debug(Company1);
		System.debug(Company2);                                                      
		System.assertEquals(Company1, Company2);  
        
		String CompKey1 = RecognitionBusinessUtil.genBusSearchKey('Deloitte ADELAIDE');
		String CompKey2 = RecognitionBusinessUtil.genBusSearchKey('Deloitte (Adelaide)');    
		System.debug(CompKey1);
		System.debug(CompKey2);                                                      
		System.assertEquals(CompKey1, CompKey2); 
		System.assertEquals(CompKey1, CompKey2);         
		System.assertEquals('D430', CompKey2);         
     
        System.assertEquals('SYDNEY', RecognitionBusinessUtil.stdCity('AUSTRALIA SQUARE'));
        System.assertEquals('bobaccount', RecognitionBusinessUtil.genBusNameStem('Bob Accounting'));
        System.assertEquals('', RecognitionBusinessUtil.getTrusteePattern('TRUSTEES FOR THE'));
        System.assertEquals('ABC LTD', RecognitionBusinessUtil.replaceLegalNoise('ABC LIMITED.'));
        System.assertEquals('ABC AUST', RecognitionBusinessUtil.replaceLocalityNoise('ABC (AUSTRALIA)'));

        System.assertEquals('610299887766', RecognitionBusinessUtil.stdPhone('61 02 99887766'));
        
		System.assertEquals(RecognitionBusinessUtil.genBusSearchKey('Deloitte ADELAIDE'), RecognitionBusinessUtil.genBusSearchKey('Deloitte (Adelaide)')); 
		System.debug(RecognitionBusinessUtil.genBusSearchKey('Deloitte Growth Solutions'));
		System.debug(RecognitionBusinessUtil.genBusSearchKey('Deloitte and Touche Tohmatsu'));
		System.debug(RecognitionBusinessUtil.genBusSearchKey('Deloitte Private Pty Ltd'));
        
		System.assertEquals(RecognitionBusinessUtil.genBusSearchKey('Deloitte ADELAIDE'), RecognitionBusinessUtil.genBusSearchKey('Deloitte Growth Solutions')); 
		System.assertEquals(RecognitionBusinessUtil.genBusSearchKey('Deloitte Growth Solutions'), RecognitionBusinessUtil.genBusSearchKey('Deloitte and Touche Tohmatsu'));         
		System.assertEquals(RecognitionBusinessUtil.genBusSearchKey('Deloitte and Touche Tohmatsu'), RecognitionBusinessUtil.genBusSearchKey('Deloitte Private Pty Ltd'));         
    }
    
    /**
     * Test of most frequent Words in Company Name
     */
	private static testMethod void testFreqNames(){ 
 		List<String> names = RecognitionBusinessUtil.getCompanyNameFreq();
		System.assertEquals('John:2', names[0]);           
    }
}