/**
*	Project:			CAANZ Internal
*	Project Name: 		Segmentation - Members
*	Project Task Name: 	Account trigger Handler After Update
*	Developer: 			akopec
*	Initial Date: 		March 2018
*	
*   Description: 		Change to Account Memebrs information 
**/
public without sharing class SegmentationMembersTriggerHandler {
    // Protection from Workflows and other Triggers
    @TestVisible private static Boolean alreadyProcessedSegments = false;
    public static Boolean debugFlag = false;

    /*********************************************************************************************
    * getConfigMembersUpdateDebug
    * @description  turn on debugging
    * @param        Config Name
    **********************************************************************************************/     
  public static Boolean getConfigMembersUpdate(String ConfigName ) {
        List <Segmentation_Settings__c> Config = [SELECT Id, Name, Active__c FROM Segmentation_Settings__c WHERE Name = : ConfigName LIMIT 1];
        System.debug(' Segmentation Setting for : ' + ConfigName + ' = ' + Config );
        if (!Config.isEmpty())
            return Config[0].Active__c;
        else
        	return FALSE; 
    }
   
     // static call to determine a list of Accounts that will require update of the segmentation object	
     public static Map<Id, String> handleSegmentsAfterAccountUpdate(List<Account> newList, Map<Id, Account> newMap, List<Account> oldList, Map<Id, Account> oldMap){ 
		// Check Custom Setting if Status is Active
       
        if(SegmentationMembersTriggerHandler.getConfigMembersUpdate('SegmentationMemberTriggerDebug' )) 
            debugFlag=true; //Turn on debugging        
        if (debugFlag) System.debug('Starting debugging Members in Trigger Handler');
		if (debugFlag) System.debug('SegmentationMembersTriggerHandler: handleSegmentsAfterAccountUpdate Starting: OldList:'  + oldList + 'NewList:' + newList );
       
        Map<Id, String> SegmentAcctIdsToUpdate = new Map<Id, String>{};
              
        // don't run it again (workflows could trigger it again)
        if (alreadyProcessedSegments) {
            if (debugFlag) System.debug('Already Processed');
            return SegmentAcctIdsToUpdate;
         }        

         for(Account acc: newList){
			if (debugFlag) System.debug('SegmentationMembersTriggerHandler: handleSegmentsAfterAccountUpdate isPersonAccount:'  + acc.isPersonAccount);  
        	if(acc.isPersonAccount){
				if (debugFlag) System.debug('SegmentationMembersTriggerHandler: handleSegmentsAfterAccountUpdate I am Person : OldList:'  + oldList + 'NewList:' + newList );
                if ( acc.Membership_Type__c == 'Student Affiliate' || acc.Membership_Type__c == 'Non Member' || String.isBlank(acc.Membership_Type__c) ) {
                    // Case 0. for new records do not bother
                    if ( oldMap == null )
                        return new  Map<Id, String>();
                    
                    // Case 0. Test for change From Member to Non-Member -> Clearence                      
                    if ( oldMap != null && oldMap.containsKey(acc.Id) && oldMap.get(acc.Id).Membership_Type__c == 'Member' )
                    { 
						if (debugFlag) System.debug('SegmentationMembersTriggerHandler: handleSegmentsAfterAccountUpdate I am in Case 0');
                        SegmentAcctIdsToUpdate.put(acc.Id, 'Case 0');
                        continue;
                    }
                }
                               
                // Case 1. New record or change Membership Type
                if ( ( oldMap == null && !String.isBlank(acc.Membership_Type__c) ) ||
                     ( oldMap != null && oldMap.containsKey(acc.Id)  
									  && !String.isBlank(acc.Membership_Type__c) 
                    				  && oldMap.get(acc.Id).Membership_Type__c != acc.Membership_Type__c ))
                { 
					if (debugFlag) System.debug('SegmentationMembersTriggerHandler: handleSegmentsAfterAccountUpdate I am in Case 1 - Membership Type Change');
					SegmentAcctIdsToUpdate.put(acc.Id, 'Case 1');
                }
                
                // Case 2. New record do not have Membership but had one before 
                else if (oldMap != null && oldMap.containsKey(acc.Id) 
                                        &&  String.isBlank(acc.Membership_Type__c)
                                        && !String.isBlank(oldMap.get(acc.Id).Membership_Type__c) ) {   
					if (debugFlag) System.debug('SegmentationMembersTriggerHandler: handleSegmentsAfterAccountUpdate I am in Case 2 - Membership Type Removed');
					SegmentAcctIdsToUpdate.put(acc.Id, 'Case 2');
        		}
                // Case 3. Change in Membership Class
                else if ( ( oldMap == null && !String.isBlank(acc.Membership_Class__c) ) ||
                     ( oldMap != null && oldMap.containsKey(acc.Id)  
									  && !String.isBlank(acc.Membership_Class__c) 
                    				  && oldMap.get(acc.Id).Membership_Class__c != acc.Membership_Class__c ))
                { 
					if (debugFlag) System.debug('SegmentationMembersTriggerHandler: handleSegmentsAfterAccountUpdate I am in Case 3 - Membership Class Change');
					SegmentAcctIdsToUpdate.put(acc.Id, 'Case 3');
                }
                // Case 4. Removed Membership Class
                else if (oldMap != null && oldMap.containsKey(acc.Id) 
                                        &&  String.isBlank(acc.Membership_Class__c)
                                        && !String.isBlank(oldMap.get(acc.Id).Membership_Class__c) ) {   
					if (debugFlag) System.debug('SegmentationMembersTriggerHandler: handleSegmentsAfterAccountUpdate I am in Case 4 - Membership Class Removed');
					SegmentAcctIdsToUpdate.put(acc.Id, 'Case 4');
        		}
                // Case 5. Change in Status
                else if ( ( oldMap != null && oldMap.containsKey(acc.Id)  
									  && !String.isBlank(acc.Status__c) 
                    				  && oldMap.get(acc.Id).Status__c != acc.Status__c ))
                { 
					if (debugFlag) System.debug('SegmentationMembersTriggerHandler: handleSegmentsAfterAccountUpdate I am in Case 5 - Status Changed');
					SegmentAcctIdsToUpdate.put(acc.Id, 'Case 5');
                }
                // Case 6. Financial Category Financial_Category__c
                else if (oldMap != null && oldMap.containsKey(acc.Id) 
                                        && !String.isBlank(acc.Financial_Category__c)
                                        // && acc.Financial_Category__c.contains('Retired') )
                    					&& oldMap.get(acc.Id).Financial_Category__c != acc.Financial_Category__c )
                {   
					if (debugFlag) System.debug('SegmentationMembersTriggerHandler: handleSegmentsAfterAccountUpdate I am in Case 6 - Financial Category Retired');
					SegmentAcctIdsToUpdate.put(acc.Id, 'Case 6');
        		}
                // Case 7. Country changed to or from Overseas
                else if (oldMap != null && oldMap.containsKey(acc.Id) 
                                        &&  !String.isBlank(acc.PersonOtherCountry)
                                        && oldMap.get(acc.Id).PersonOtherCountry != acc.PersonOtherCountry )  {   
					if (debugFlag) System.debug('SegmentationMembersTriggerHandler: handleSegmentsAfterAccountUpdate I am in Case 7- Country changed to Overseas');
					SegmentAcctIdsToUpdate.put(acc.Id, 'Case 7');
        		}

            } else // ####################  Nothing for Businesses #######################	            
       			continue; 
        }
         
		if (debugFlag) System.debug('SegmentationMembersTriggerHandler: handleSegmentsAfterAccountUpdate: After Process - SegmentsAcctYoUpdate:'+ SegmentAcctIdsToUpdate);

       	if ( SegmentAcctIdsToUpdate.size() > 0) {
          	alreadyProcessedSegments = true;
        }
       	return SegmentAcctIdsToUpdate;   	
    }
    
// static call to determine a list of Accounts that will require update of the segmentation object	
public static Map<Id, String> handleSegmentsAfterEHUpdate(List<Employment_History__c> newList, Map<Id, Employment_History__c> newMap, 
                                                          List<Employment_History__c> oldList, Map<Id, Employment_History__c> oldMap){ 
        
        if(SegmentationMembersTriggerHandler.getConfigMembersUpdate('SegmentationMemberTriggerDebug' )) 
            debugFlag=true; //Turn on debugging        
        if (debugFlag) System.debug('SegmentationMembersTriggerHandler: Starting debugging Members in Employment History Trigger Handler');   
        Map<Id, String> SegmentAcctIdsToUpdate = new Map<Id, String>{};

        // don't run it again (workflows could trigger it again)
        if (alreadyProcessedSegments) {
            if (debugFlag) System.debug('Already Processed');
            return SegmentAcctIdsToUpdate;
        }
 
		if (debugFlag) System.debug('DEBUG: SegmentationMembersTriggerHandler: handleSegmentsAfterEHUpdate : Old ' + oldList);   
		if (debugFlag) System.debug('DEBUG: SegmentationMembersTriggerHandler: handleSegmentsAfterEHUpdate : New ' + newList);
		if (debugFlag) {
            if (oldList!= NULL)
				System.debug('SegmentationMembersTriggerHandler: New Size ' + oldList.size()); 
        }
		if (debugFlag) System.debug('SegmentationMembersTriggerHandler: New Size ' + newList.size());
                                                              
		// add logic for change of Primary Employer
		
		for(Employment_History__c eh: newList){   

            // Case 1. First or New Employer
            if ( oldMap == null && eh.Employer__c != NULL )
            { 
        		if (debugFlag) System.debug('SegmentationMembersTriggerHandler: handleSegmentsAfterEHUpdate: I am in Case 1 - New Employer');
                SegmentAcctIdsToUpdate.put(eh.Member__c, 'Case 1'); 
            }
             // Case 1. Change of Employer - Employer updated (if possible)
            else if ( oldMap != null && oldMap.containsKey(eh.Id)  
                 && eh.Employer__c != NULL
                 && oldMap.get(eh.Id).Employer__c != eh.Employer__c )
            { 
        		if (debugFlag) System.debug('SegmentationMembersTriggerHandler: handleSegmentsAfterEHUpdate: I am in Case 1 - updated Employer ');
                SegmentAcctIdsToUpdate.put(eh.Member__c, 'Case 1'); 
            }
            // job title affects Career Phase
            else if ( ( oldMap == null && eh.Job_Title__c != NULL ) ||
                ( oldMap != null && oldMap.containsKey(eh.Id)  
                 && eh.Job_Title__c != NULL
                 && oldMap.get(eh.Id).Job_Title__c != eh.Job_Title__c ))
            { 
        		if (debugFlag) System.debug('SegmentationMembersTriggerHandler: handleSegmentsAfterEHUpdate: I am in Case 2 - Job Title Change'); 
                SegmentAcctIdsToUpdate.put(eh.Member__c, 'Case 2'); 
            }
            // Primary Employer, Current Status or End Date changed - no new employment
            else if (
                ( oldMap != null && oldMap.containsKey(eh.Id) && oldMap.size() == newMap.size() 
                				 && eh.Primary_Employer__c != NULL
                 				 && oldMap.get(eh.Id).Primary_Employer__c != eh.Primary_Employer__c ) ||
                ( oldMap != null && oldMap.containsKey(eh.Id)  && oldMap.size() == newMap.size()
                 				 && eh.Status__c != NULL
                 				 && oldMap.get(eh.Id).Status__c != eh.Status__c ) ||
                ( oldMap != null && oldMap.containsKey(eh.Id) && oldMap.size() == newMap.size() 
                 				 && eh.Employee_End_Date__c != NULL
                				 && oldMap.get(eh.Id).Employee_End_Date__c == NULL) ||
                ( oldMap != null && oldMap.containsKey(eh.Id) && oldMap.size() == newMap.size() 
                 				 && eh.Employee_End_Date__c == NULL
                				 && oldMap.get(eh.Id).Employee_End_Date__c != NULL) ||
                ( oldMap != null && oldMap.containsKey(eh.Id) && oldMap.size() == newMap.size() 
                 				 && eh.Employee_End_Date__c == NULL
                				 && oldMap.get(eh.Id).Employee_Start_Date__c != eh.Employee_Start_Date__c)
                )
                 
            { 
        		if (debugFlag) System.debug('SegmentationMembersTriggerHandler: handleSegmentsAfterEHUpdate: I am in Case 3 - End of Employment Change');
                SegmentAcctIdsToUpdate.put(eh.Member__c, 'Case 3'); 
            }
            else
				if (debugFlag) System.debug('SegmentationMembersTriggerHandler: handleSegmentsAfterEHUpdate: None of the cases'); 
            
        }
		if (debugFlag) System.debug('SegmentationMembersTriggerHandler: handleSegmentsAfterEHUpdate: After Process - SegmentsAcctYoUpdate:'+ SegmentAcctIdsToUpdate);

       	if ( SegmentAcctIdsToUpdate.size() > 0) {
          	alreadyProcessedSegments = true;
        }                                                    
		return SegmentAcctIdsToUpdate; 
	} 
    
    
/*********************************************************************************************
* The actual update of Segmentation Object 
*
* Fields to update:
* Business_Segmentation_MS__c ( set of 18 segments)
* Firm Type
* Organisation Type
* Firm Local Office Size
* Career Stage
* Member Journey
*
*
* @description  turn on debugging
* @param        Config Name
**********************************************************************************************/  
@future 
public static void handleSegmentsAfterUpdateAsync(Map <Id, String> SegmentsToUpdate){         
	
    if ( SegmentsToUpdate==NULL)
        return;
    if ( SegmentsToUpdate.isEmpty())
        return;
                                                                        
    System.debug('Get config Members Update ' + SegmentationMembersTriggerHandler.getConfigMembersUpdate('SegmentationMembersUpdate'));
	if(!SegmentationMembersTriggerHandler.getConfigMembersUpdate('SegmentationMembersUpdate' )) return; //On/off
    System.debug(' After Get config Members Update ' );   

	List<Segmentation__c> segmentsToUpdateList = new List <Segmentation__c>();
	List<Segmentation__c> segmentsToInsertList = new List <Segmentation__c>(); 
	List<Segmentation__c> segmentsUpdate  = [select Id, Account__c, Business_Segmentation_MS__c, Organisation_Type__c, Firm_Type__c, Firm_Office_Size__c, Career_Stage__c
        										 from Segmentation__c 
        										 where Account__c IN :SegmentsToUpdate.keySet() ];
    
	if (debugFlag) System.debug('SegmentationMembersTriggerHandler: handleSegmentsAfterUpdateAsync: Update handle: Segments to Update' + SegmentsToUpdate);
	for (Id key : SegmentsToUpdate.keySet()) {
		if (debugFlag) System.debug( 'Case to Update: ' + SegmentsToUpdate.get(key));  			
		
        Segmentation__c newSegment;         
		// check if Segmentation exists (is it Update or Insert)
        if ( !segmentsUpdate.isEmpty()){
            for ( Segmentation__c seg : segmentsUpdate){
                if (seg.Account__c == key)
                    newSegment = seg;
            }
		if (debugFlag) System.debug('SegmentationMembersTriggerHandler: handleSegmentsAfterUpdateAsync: Existing Segments to Update' + newSegment);
        }
          
        Map<Id, Map<String, String>> segments = SegmentationCAANZCategories.getSegment(new List<Id>{key});
		if (debugFlag) System.debug('SegmentationMembersTriggerHandler: handleSegmentsAfterUpdateAsync: Calculated New Segments Map:' + segments);
        if (segments.isEmpty())
            return;
        Map<String, String> cat = segments.get(key);
		if (debugFlag) System.debug('SegmentationMembersTriggerHandler: handleSegmentsAfterUpdateAsync: New Segments List: ' + cat);
        
        if (cat.isEmpty())
            return;
        
        ///////////////////////////////////////////////// NOT A MEMBER ////////////////////////////////////
        if (cat.get('Segment') == 'Not a Member'){
			if ( newSegment==NULL) // not existing segment - we do not need to insert an empty Segments
                continue;
            // existing Segment - clear all fields
			newSegment.Organisation_Type__c = NULL; 
			newSegment.Firm_Type__c = NULL; 
			newSegment.Firm_Office_Size__c = NULL; 
			newSegment.Career_Stage__c = NULL;       
			newSegment.Business_Segmentation_MS__c = NULL; 
			segmentsToUpdateList.add(newSegment);
            continue;
        }

		///////////////////////////////////////////////// THIS IS MEMBER ////////////////////////////////////
        if ( newSegment==NULL) {
            newSegment = new Segmentation__c();
            newSegment.Account__c = key;              
        }
        
		newSegment.Organisation_Type__c = cat.get('Organisation Type'); 
		newSegment.Firm_Type__c = cat.get('Firm Type'); 
		newSegment.Firm_Office_Size__c = cat.get('Firm Office Size'); 
		newSegment.Career_Stage__c = cat.get('Career Stage');      
		newSegment.Business_Segmentation_MS__c = cat.get('Segment');       
		segmentsToUpdateList.add(newSegment);
	}
    
    if ( !segmentsToUpdateList.isEmpty()) {
        upsert segmentsToUpdateList;   
    }
  }
}