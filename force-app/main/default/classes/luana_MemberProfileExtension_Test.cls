/**
    Developer: WDCi (kh)
    Development Date:17/03/2016
    Task: Luana Test class for luana_MemberProfileExtension
    
    Change History
    LCA-921 26/08/2016 WDCi KH: include person email
**/
@isTest(seeAllData=false)
private class luana_MemberProfileExtension_Test {
    
    public static User memberUser {get; set;}
    public static Luana_DataPrep_Test dataPrep;
    public static luana_CommUserUtil userUtil {get; set;} 
    public static Account bussinessAcc {get; set;}
    
    private static String classNamePrefixLong = 'luana_MemberProfileExtension_Test';
    private static String classNamePrefixShort = 'lmpe';
    private static map<String, Id> commProfIdMap {get; set;}
    public static Account memberAccount {get; set;}
    
    //LCA-921
    public static void initial(){
        //initialize
        dataPrep = new Luana_DataPrep_Test();
        
        //Create all the custom setting
        insert dataPrep.prepLuanaExtensionSettingCustomSettings();
        insert dataPrep.createLuanaConfigurationCustomSetting();
        
        //Create user with Member and Employer community access
        memberAccount = dataPrep.generateNewApplicantAcc('Joe_' + classNamePrefixShort, classNamePrefixLong, 'Full_Member');
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.PersonEmail = 'joe_1_' +classNamePrefixShort+'@gmail.com';//LCA-921
        memberAccount.Communication_Preference__c= 'Home Phone';
        memberAccount.PersonHomePhone= '1234';
        memberAccount.PersonOtherStreet= '83 Saggers Road';
        memberAccount.PersonOtherCity='JITARNING';
        memberAccount.PersonOtherState='Western Australia';
        memberAccount.PersonOtherCountry='Australia';
        memberAccount.PersonOtherPostalCode='6365'; 
        insert memberAccount;
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
    }
    
    public static void setup(){
        //LCA-921
        memberUser = [Select Id, Email, FirstName, LastName, UserName, Name from User Where Email =: 'joe_1_' + classNamePrefixShort+'@gmail.com' limit 1];
        //memberUser = dataPrep.generateNewApplicantUser(classNamePrefixLong, classNamePrefixshort, memberAccount, commProfIdMap.get('Customer Community Login User'));
        //insert memberUser;
        System.assertEquals(memberUser.Id != null, true, 'Expected user is created');
        
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
       
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defEmployerPermSetId.value__c));
        list<PermissionSetAssignment> permissAssign = [Select Id from PermissionSetAssignment where AssigneeId =: memberUser.Id AND (PermissionSetId =: defMemberPermSetId.value__c OR PermissionSetId =: defEmployerPermSetId.value__c)];
        System.runAs(memberUser){
            if(permissAssign == null)
                insert psas;
        }
    }
    
    public static testMethod void testNavigateFunction() { 
        
        Test.startTest();
            initial();
        Test.stopTest();
        
        setup();
        System.RunAs(memberUser){
        
            ApexPages.StandardController std = new ApexPages.StandardController(memberUser);
            luana_MemberProfileExtension mpe = new luana_MemberProfileExtension(std);
            
            mpe.save();
            mpe.cancel();

        }
    }
    
}