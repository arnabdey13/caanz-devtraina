@IsTest
public class LightningRecordEditFormController_Test {
    @isTest
    static void fieldsRelatedtoLayoutSection_nonExistentLayoutSection_expectEmptyLayoutSectionTest() {
        String nonExistentLayoutSection = 'fewfewfewsf';
        
        List<LightningRecordEditFormController.LayoutSection> layoutSection =
            LightningRecordEditFormController.fieldsRelatedtoPageLayout('Account',nonExistentLayoutSection);
        if(layoutSection.size() > 0 )
        assertEmptyLayoutSection(layoutSection[0]);
    }
    
    @isTest
    static void fieldsRelatedtoLayoutSection_existingLayoutSection_expectNonEmptyLayoutSectionTest() {
        String existentLayoutSection = 'PersonAccount-CAANZ Member Support Person Account';
        
        List<LightningRecordEditFormController.LayoutSection> layoutSection =
            LightningRecordEditFormController.fieldsRelatedtoPageLayout('Account',existentLayoutSection);
        
        if(layoutSection.size() > 0 )
        assertEmptyLayoutSection(layoutSection[0]);
    }
    
    static void assertEmptyLayoutSection(LightningRecordEditFormController.LayoutSection layoutSection) {
        system.assert(layoutSection != null, 'The page layout should not be null.');
    }
    
    static void assertNonEmptyLayoutSection(LightningRecordEditFormController.LayoutSection layoutSection) {
        system.assert(layoutSection != null, 'The page layout should not be null.');
    }
}