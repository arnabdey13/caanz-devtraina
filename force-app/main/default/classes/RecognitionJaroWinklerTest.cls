@isTest
public class RecognitionJaroWinklerTest {
    /**
     * Test of similarity for class JaroWinkler.
     */
	private static testMethod void testSimilarity(){
        System.debug('similarity');
        RecognitionJaroWinkler instance = new RecognitionJaroWinkler();

        System.debug('similarity');
             
        System.debug('ABC Corporation = ABC Corp = 0.93');
        System.assertEquals(0.93, instance.similarity('ABC Corporation', 'ABC Corp'));
        
        System.debug('D N H Enterprises Inc = D & H Enterprises, Inc. = 0.95');             
        System.assertEquals(0.95, instance.similarity('D N H Enterprises Inc', 'D & H Enterprises, Inc.'));
        
        System.debug('My Gym Children\'s Fitness Center = My Gym. Childrens Fitness = 0.92');             
        System.assertEquals(0.92, instance.similarity('My Gym Children\'s Fitness Center', 'My Gym. Childrens Fitness'));
                     
        System.debug('PENNSYLVANIA = PENNCISYLVNIA = 0.88' );   
        System.assertEquals(0.88, instance.similarity('PENNSYLVANIA', 'PENNCISYLVNIA')); 
        
    }
        
    private static testMethod void testBig4Similarity(){
        String Company1;
		String Company2;
        
        System.debug('similarity');
        RecognitionJaroWinkler instance = new RecognitionJaroWinkler();
        
        Company1 = 'Deloitte ADELAIDE';
        Company2 = 'Deloitte (Adelaide)';
        
        System.debug(Company1 + '=' + Company2 );   
        System.debug(instance.similarity(Company1.toLowerCase(), Company2.toLowerCase()));
        
        Company1 = 'Deloitte ADELAIDE';
        Company2 = 'Deloitte Private Pty Ltd';
        
        System.debug(Company1 + '=' + Company2 );   
        System.debug(instance.similarity(Company1.toLowerCase(), Company2.toLowerCase()));
        
        Company1 = 'Deloitte ADELAIDE';
        Company2 = 'Deloitte and Touche Tohmatsu';
        
        System.debug(Company1 + '=' + Company2 );   
        System.debug(instance.similarity(Company1.toLowerCase(), Company2.toLowerCase()));
        
        Company1 = 'Deloitte ADELAIDE';
        Company2 = 'Deloitte Growth Solutions';
        
        System.debug(Company1 + '=' + Company2 );   
        System.debug(instance.similarity(Company1.toLowerCase(), Company2.toLowerCase()));
        
        Company1 = 'Deloitte Private Pty Ltd';
        Company2 = 'Deloitte Growth Solutions';
        
        System.debug(Company1 + '=' + Company2 );   
        System.debug(instance.similarity(Company1.toLowerCase(), Company2.toLowerCase()));
        
        Company1 = 'Deloitte Private Pty Ltd';
        Company2 = 'Deloitte and Touche Tohmatsu';
        
        System.debug(Company1 + '=' + Company2 );   
        System.debug(instance.similarity(Company1.toLowerCase(), Company2.toLowerCase()));

        
    }
}