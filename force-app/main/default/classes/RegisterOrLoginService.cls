/**
*   @author Jannis Bott
*   @date   28/11/2016
*   @group  Register and Login
*   @description Supports aura calls from LoginOrRegister page.
*   Handles load, save, email check, password resend link and login
 */
public with sharing class RegisterOrLoginService {

    @TestVisible
    public enum RegistrationState {
        LOGIN, INACTIVE, EXPIRED, REGISTER, NON_REPEATABLE_ERROR, REPEATABLE_ERROR, SUCCESS, PASSWORD_NOT_SET
    }

    @AuraEnabled
    public static Map<String, List<String>> load() {
        Map<String, List<String>> picklists = new Map<String, List<String>>();

        List<String> countries = new List<String>();
        countries.add('Australia');
        countries.add('New Zealand');
        for (String s : getPicklistValues(Account.PersonMailingCountryCode)) {
            if (s.equals('Australia') || s.equals('New Zealand')) continue; else countries.add(s);
        }

        picklists.put('countries', countries);
        picklists.put('salutations', getPicklistValues(Account.Salutation));

        return picklists;
    }

    @AuraEnabled
    public static String save(String email, String salutation, String firstName, String lastName,
            String country, String destination, Boolean subscribe, Boolean termsConditions) {
        Id nonMemberRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non Member').getRecordTypeId();
        String guid = generateGUID();

        if (isDuplicateGuid(guid)) return String.valueOf(RegistrationState.REPEATABLE_ERROR);

        // TODO monitoring in code
        Account newMember = new Account(RecordTypeId = nonMemberRecordTypeId,
                Salutation = salutation,
                FirstName = firstName,
                LastName = lastName,
                PersonEmail = email,
                PersonOtherCountry = country,
                OwnerId = Environment_Ids__c.getInstance('Non_Member_Registration').Id__c,
                Registration_Temporary_Key__c = guid,
                Registration_Destination_URL__c = destination,
                Opt_In_To_Receive_Comms__c = subscribe,
                Agree_to_terms_and_conditions__c = termsConditions);
                
                system.debug('Account newMember : '+newMember);
                
        system.debug('#### newMember detail is '+newMember );
        
        if (country.equals('New Zealand')) {
            newMember.Member_Of__c = 'NZICA';
        } else {
            newMember.Member_Of__c = 'ICAA';
        }

        try {
            insert newMember;
            return String.valueOf(RegistrationState.SUCCESS);
        } catch (Exception e) {
            return String.valueOf(RegistrationState.NON_REPEATABLE_ERROR);
        }
    }

    @AuraEnabled
    public static String resendEmail(String email) {
        RelaxedSharing rs = new RelaxedSharing();
        List<User> user = rs.getUser(email);

        String guid = generateGUID();

        if (isDuplicateGuid(guid)) return String.valueOf(RegistrationState.REPEATABLE_ERROR);


        if (user.isEmpty() || user.size() > 1) {
            return String.valueOf(RegistrationState.NON_REPEATABLE_ERROR);
        } else {
            Account accountForUpdate = new Account(Id = user[0].Account.Id,
                    Registration_Temporary_Key__c = guid);

            try {
                rs.updateAccount(accountForUpdate);
                RegisterOrLoginEmailProcess.sendWelcomeEmail(new List<Id>{
                        user[0].Account.Id
                });
                return String.valueOf(RegistrationState.SUCCESS);
            } catch (Exception e) {
                return String.valueOf(RegistrationState.NON_REPEATABLE_ERROR);
            }
        }
    }

    @AuraEnabled
    public static String resetPW(String email) {
        RelaxedSharing rs = new RelaxedSharing();
        List<User> user = rs.getUser(email);

        if (user.isEmpty() || user.size() > 1) {
            return String.valueOf(RegistrationState.NON_REPEATABLE_ERROR);
        } else {
            try {
                System.resetPassword(user[0].Id, true);
                return String.valueOf(RegistrationState.SUCCESS);
            } catch (Exception e) {
                return String.valueOf(RegistrationState.NON_REPEATABLE_ERROR);
            }
        }
    }

    @AuraEnabled
    public static String checkUser(String email) {
        RelaxedSharing rs = new RelaxedSharing();
        List<User> user = rs.getUser(email);

        //Error Case - more than one user returned
        if (!user.isEmpty() && user.size() > 1) {
            return String.valueOf(RegistrationState.NON_REPEATABLE_ERROR);
        }

        //User does not exist
        if (user.isEmpty()) {
            return String.valueOf(RegistrationState.REGISTER);
        }

        //User found cases
        if (!user.isEmpty() && user.size() == 1) {
            return String.valueOf(existingMemberStatus(user[0], user[0].Account));
        } else {
            return String.valueOf(RegistrationState.NON_REPEATABLE_ERROR);
        }
    }

    @TestVisible
    public static RegistrationState existingMemberStatus(User user, Account personAccount) {
        //Member cases
        if (personAccount.RecordType.Name.equals('Member') || personAccount.RecordType.Name.equals('Applicant')) {
            if (user.IsActive && personAccount.Status__c.equals('Active')) {
                return RegistrationState.LOGIN;
            } else {
                return RegistrationState.INACTIVE;
            }
        } else {
            //Non Member cases
            //if UUID is null return error
            if (personAccount.Registration_Temporary_Key__c == null) {
                return RegistrationState.NON_REPEATABLE_ERROR;
            }
            //User has set password previously
            else if (personAccount.Registration_Temporary_Key__c.equals('SELF_REGISTERED')) {
                if (user.IsActive && personAccount.Status__c.equals('Active')) {
                    return RegistrationState.LOGIN;
                } else {
                    return RegistrationState.INACTIVE;
                }
            }
            //Users UUID has expired
            else if (personAccount.Registration_Temporary_Key__c.equals('EXPIRED')) {
                return RegistrationState.EXPIRED;
            } else {
                return RegistrationState.PASSWORD_NOT_SET;
            }
        }
    }

    @AuraEnabled
    public static Map<String, String> login(String username, String password, String url) {
        String startURL = null;
        try {
            RelaxedSharing rs = new RelaxedSharing();
            List<User> user = rs.getUser(username);
            if (user.size() == 1) {
                Account acc = new Account(Id = user[0].AccountId, Registration_Destination_URL__c = url);
                rs.updateAccount(acc);
            }
            return new Map<String, String>{
                    'SUCCESS' => Site.login(username, password, startURL).getUrl()
            };
        } catch (Exception e) {
            return new Map<String, String>{
                    'ERROR' => e.getMessage()
            };
        }
    }

    private static List<String> getPicklistValues(Schema.SObjectField field) {
        List<String> options = new List<String>();
        for (Schema.PicklistEntry ple: field.getDescribe().getPicklistValues()) {
            if (ple.isActive()) {
                options.add(ple.getLabel());
            }
        }
        return options;
    }

    private static Boolean isDuplicateGuid(String guid) {
        List<Account> duplicateGUIDs = [SELECT Id FROM Account WHERE Registration_Temporary_Key__c = :guid];
        if (duplicateGUIDs.size() > 0) return true; else return false;
    }

    private static String generateGUID() {
        Blob key = Crypto.GenerateAESKey(128);
        String encodedKey = EncodingUtil.ConvertTohex(key);
        String guid = encodedKey.SubString(0, 8) + '-' +
                encodedKey.SubString(8, 12) + '-' +
                encodedKey.SubString(12, 16) + '-' +
                encodedKey.SubString(16, 20) + '-' +
                encodedKey.substring(20);
        return guid;
    }

    public without sharing class RelaxedSharing {
        public List<User> getUser(String email) {
            return [
                    SELECT Id,
                            IsActive,
                            Account.Id,
                            Account.PersonEmail,
                            Account.IsCustomerPortal,
                            Account.Status__c,
                            Account.Registration_Temporary_Key__c,
                            Account.Registration_Destination_URL__c,
                            Account.RecordType.Name
                    FROM User
                    WHERE Email = :email
                    AND Account.IsCustomerPortal = true
            ];
        }

        public void updateAccount(Account accountForUpdate) {
            update accountForUpdate;
        }
    }
}