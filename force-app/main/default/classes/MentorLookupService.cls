/**
 @description: This class is without sharing simply because this service is used by
 community users that execute a search against an object with a private sharing model
 */
public without sharing class MentorLookupService {

    @AuraEnabled
    public static Map<String, Map<String, String>> findMentor(String mentorId, String mentorEmail, String designation) {
        Map<String, Map<String, String>> results = findMentorImpl(mentorId, mentorEmail, designation);

        if (results.get('SUCCESS') != NULL) {
            Relationship__c relationship = ensureRelationship(EditorPageUtils.getUsersAccountId(), results.get('SUCCESS').get('id'));
            if (relationship != NULL) {
                Database.SaveResult sr = Database.insert(relationship, false);
                if (!sr.success) {
                    System.debug('Relationship insert failed! Failing silently'); // TODO notify operations/PSA team
                    System.debug(sr.getErrors());
                }
            }
        }

        return results;
    }

    @TestVisible
    private static Map<String, Map<String, String>> findMentorImpl(String mentorId, String mentorEmail, String designation) {
        System.debug(mentorId + ' ' + mentorEmail + ' ' + designation);
        Set<String> filter = new Set<String>();

        if (designation == 'Chartered Accountants') {
            filter.add('CA');
            filter.add('FCA');
        } else if (designation == 'Associate Chartered Accountants') {
            filter.add('CA');
            filter.add('FCA');
            filter.add('ACA');
            filter.add('FACA');
        }

        System.debug(filter);
        List<Account> mentors = [
                SELECT FirstName,
                        LastName,
                        Primary_Employer__r.Name
                FROM Account
                WHERE Member_ID__c = :mentorId AND
                Designation__c IN :filter AND
                (PersonEmail = :mentorEmail OR Secondary_Email__c = :mentorEmail) AND
                ((Membership_Class__c = 'Full' AND Membership_Type__c = 'Member') OR
                (GAA_member__c = true AND Membership_Type__c = 'Non Member'))
        ];
        System.debug(mentors);
        if (mentors.size() == 1) {
            Map<String, String> response = new Map<String, String>{
                    'id' => mentors[0].Id,
                    'name' => mentors[0].FirstName + ' ' + mentors[0].LastName,
                    'accountName' => mentors[0].Primary_Employer__r.Name
            };

            return new Map<String, Map<String, String>>{
                    'SUCCESS' => response
            };
        } else {
            return new Map<String, Map<String, String>>{
                    'ERROR' => new Map<String, String>{
                            'response' => 'No matching Mentor was found.'
                    }
            };
        }
    }

    // see SFS-601 for requirements
    @TestVisible
    private static Relationship__c ensureRelationship(Id menteeAccountId, Id mentorAccountId) {
        System.debug('Ensuring relationship for ' + menteeAccountId + ' -> ' + mentorAccountId);
        List<Relationship__c> matchingRelationships = [
                SELECT Id
                FROM Relationship__c
                WHERE Primary_Relationship__c = 'Mentor'
                AND Reciprocal_Relationship__c = 'Mentee'
                AND Account__c = :mentorAccountId
                AND Member__c = :menteeAccountId
        ];
        if (matchingRelationships.size() == 0) {
            return new Relationship__c(
                    Primary_Relationship__c = 'Mentor',
                    Reciprocal_Relationship__c = 'Mentee',
                    Account__c = mentorAccountId,
                    Member__c = menteeAccountId);
        } else {
            return null;
        }
    }
}