global class AAS_CohortUpdateBatch implements Database.Batchable<sObject>{
    
    List<SObject> batchSObjList = new List<SObject>();
    AAS_Course_Assessment__c courseAssessment;
    AAS_Assessment_Schema_Section__c assObj;

    global AAS_CohortUpdateBatch(List<SObject> sobjList, AAS_Course_Assessment__c ca, AAS_Assessment_Schema_Section__c ass){
        batchSObjList = sobjList;
        courseAssessment = ca;
        assObj = ass;
    }
    
    global List<SObject> start(Database.BatchableContext BC) {
        return batchSObjList;
    }
    
    global void executeBatch(Database.BatchableContext BC, integer scope){
        
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        try{
            System.debug('*********size:: ' + batchSObjList.size() + ' - ' + scope.size());
            database.update(scope,true);
            
        }Catch(Exception exp){
            
            throw exp;
        }
    }

    global void finish(Database.BatchableContext BC) {
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        AsyncApexJob aaj = [Select TotalJobItems, Status, NumberOfErrors, JobType, JobItemsProcessed, ExtendedStatus, CreatedById, CreatedBy.Email, CompletedDate From AsyncApexJob WHERE id = :BC.getJobId()];//get the job Id
        
        List<String> eTemplateName = new List<String>();
        eTemplateName.add('AAS_Fail_Cohort_Adjustment_Notification');
        eTemplateName.add('AAS_Success_Cohort_Adjustment_Notification');
        List<EmailTemplate> eTemplates = [Select id, name, developerName, Subject, HtmlValue, Body from EmailTemplate Where DeveloperName in: eTemplateName];
        
        
        String [] email = new String[] {aaj.CreatedBy.Email};
        
        //below code will send an email to User about the status
        mail.setToAddresses(email);
        mail.setSenderDisplayName('AAS Cohort Processing Status');
 
        //If process failed will flush all the record again
        System.debug('****aaj.ExtendedStatus:: ' + aaj.ExtendedStatus);
        
        if(aaj.NumberOfErrors > 0){
            for(SObject sobj: batchSObjList){
                AAS_Student_Assessment_Section_Item__c newObjScope = (AAS_Student_Assessment_Section_Item__c)sobj;
                newObjScope.AAS_Adjustment__c = null;
            }
            
            //Call the batchable class to update the record in batch
            AAS_CohortFlushRecordBatch updateSASIBatch = New AAS_CohortFlushRecordBatch(batchSObjList, batchSObjList, false, courseAssessment, assObj);
            database.executeBatch(updateSASIBatch, 200);
            
        }else{
        
            for(EmailTemplate et: eTemplates){
                if(et.developerName == 'AAS_Success_Cohort_Adjustment_Notification'){
                    mail.setSubject(processUpdateEmailTemplateSubject(et.Subject, courseAssessment.Name));
                    mail.setPlainTextBody(processUpdateEmailTemplateHTMLValue(et.HtmlValue, courseAssessment.Name, courseAssessment.Id, null));
                }
            }
           
            AAS_Course_Assessment__c ca = new AAS_Course_Assessment__c();
            ca.Id = courseAssessment.Id;
            ca.AAS_Cohort_Batch_Processing__c = false;
            ca.AAS_Cohort_Adjustment_Exam__c = true;
            
            update ca;
            
            Messaging.sendEmail(new Messaging.Singleemailmessage [] {mail});
        }
        
        
        
    }
    
    public String processUpdateEmailTemplateSubject(String emailTemplateSubject, String caName){
        String subject = emailTemplateSubject;
        subject = subject.replace('{!AAS_Course_Assessment__c.Name}', caName);
        return subject;
    }
    
    public String processUpdateEmailTemplateHTMLValue(String emailTemplateHTMLValue, String caName, String caId, String errorMessage){
        String htmlValue = emailTemplateHTMLValue;
        htmlValue = htmlValue.replace('{!AAS_Course_Assessment__c.Name}', caName);
        
        String caURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + caId;
        htmlValue = htmlValue.replace('{!AAS_Course_Assessment__c.Link}', caURL);
        if(errorMessage != null){
            htmlValue = htmlValue.replace('Error Message:', 'Error Message: <br/>' + errorMessage);
        }
        return htmlValue;
    }

}