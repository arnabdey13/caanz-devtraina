@isTest
private class GetEmploymentDetails_test {
static testMethod void validateGetEmploymentDetails()
{
    GetEmploymentDetails.EmploymentID eID = new GetEmploymentDetails.EmploymentID();
    
    Account testMemberAccount = new Account(name='Test_Member EmployemntDetials', Member_ID__c = 'TestGEM123'
                                           ,BillingStreet='1 Test Street'); //DN20160415 comply with validation rule
    insert testMemberAccount;
    
    eID.memberID = testMemberAccount.Id;
        
    Account testBusinessAccount = new Account(name='Test_Bussiness EmployemntDetials', Member_ID__c = 'TestGEB123'
                                             ,BillingStreet='1 Test Street'); //DN20160415 comply with validation rule
    insert testBusinessAccount;
    eID.employerID = testBusinessAccount.ID;    
        
    GetEmploymentDetails.EmploymentDetails ed = new GetEmploymentDetails.EmploymentDetails();
    
    ed = GetEmploymentDetails.getDetails(eID);
}
}