/*
    Change History:
    LCA-624 09/06/2016: WDCi Lean - fix timezone issue
    LCA-610 14/06/2016: WDCi Lean - fix url encoding issue for filter
    LCA-641 28/06/2016: WDCi Lean - improve wizard to take start/end date from child session
*/
public without sharing class luana_VCImportWizardController {

    public LuanaSMS__Course__c c {set; get;}
    
    public String username {set; get;}
    public String password {set; get;}
    public String filter {set; get;}
    public String sessionCookie {set; get;}
    public Integer meetingCount {set; get;}
    RecordType rtVSession;
    public List<SessWrapper> sessWrapperList {set; get;}
    
    // Custom settings to store Adobe Connect app URL
    Luana_Extension_Settings__c adobeConn = Luana_Extension_Settings__c.getValues('AdobeConnect_URL');  
    

    public luana_VCImportWizardController(ApexPages.StandardController controller) {
        c = (LuanaSMS__Course__c) controller.getRecord();
        rtVSession = [SELECT Id FROM RecordType WHERE SObjectType='LuanaSMS__Session__c' AND DeveloperName='Virtual' LIMIT 1];
        meetingCount = 0;
    }
    
    
    // Initial page rendering option. Check Course, Custom Setting - AdobeConnect URL
    public boolean getHasRequiredParams() {
        if(isCourseIdProvided() && isACURLSetup())    
            return true;
            
        else
            return false;
    }
    
    
    // Check Custom Setting - AdobeConnect URL
    public boolean isACURLSetup() {
        if(adobeConn != null && adobeConn.Value__c != null)
            return true;
        else {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'This Wizard is relying on a Custom Setting - AdobeConnect_URL, please contact Administrator and ensure this is setup before running this wizard.'));
            return false;
        }
    }

    // Check Course
    public boolean isCourseIdProvided() {
        List<LuanaSMS__Course__c> cList = [SELECT Name FROM LuanaSMS__Course__c WHERE Id=: c.Id];
        for(LuanaSMS__Course__c course : cList)
            c = course;
            
        if(cList.size() == 0) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Course Id is not provided. Please run this Wizard from a Course.'));
            return false;
        } else {
            return true;        
        }
    }
    
    public List<SessWrapper> getACSessions() {
    
        sessWrapperList = new List<SessWrapper>();
        
        Http queryH = new Http();
        HttpRequest queryReq = new HttpRequest();
        queryReq.setEndpoint(getACConnWithAction('report-my-meetings') + '&filter-like-name=' + EncodingUtil.urlEncode(filter, 'UTF-8') + '&filter-rows=50&sort-name=asc'); //LCA-610 added EncodingUtil
        queryReq.setMethod('GET');
        HttpResponse queryRes = queryH.send(queryReq);
        Dom.Document queryDoc = queryRes.getBodyDocument();
        Dom.XMLNode queryResults = queryDoc.getRootElement();
        Dom.XMLNode[] myMeetings = new Dom.XMLNode[0];
        
        if(queryResults.getChildElement('my-meetings', null) != null)
           myMeetings = queryResults.getChildElement('my-meetings', null).getChildElements();

        for(Dom.XMLNode meeting : myMeetings) {
            meetingCount++;
            String sessName = '', domainName = '', urlPath = '', topicKey = '', scoId = '';
            DateTime dtST = null, dtET = null;
            if(meeting.getChildElement('name', null) != null) {
                sessName = meeting.getChildElement('name', null).getText();
                String[] sessNameArray = sessName.split(' ');
                topicKey = sessNameArray[0].replace(sessNameArray[0].right(3), '');
            }

            if(meeting.getAttribute('sco-id', null) != null)
                scoId = meeting.getAttribute('sco-id', null);

            if(meeting.getChildElement('domain-name', null) != null)
                domainName = meeting.getChildElement('domain-name', null).getText();
            
            if(meeting.getChildElement('url-path', null) != null)
                urlPath = meeting.getChildElement('url-path', null).getText();
            
            /*LCA-641 will be using child date/time instead
            if(meeting.getChildElement('date-begin', null) != null){
                dtST = parseDate(meeting.getChildElement('date-begin', null).getText().replace('T', ' '));
            }
            
            if(meeting.getChildElement('date-end', null) != null){
                dtET = parseDate(meeting.getChildElement('date-end', null).getText().replace('T', ' '));
            }*/
            
            LuanaSMS__Session__c sess = new LuanaSMS__Session__c(
                                                    Name=sessName,
                                                    //LuanaSMS__Start_Time__c=dtST, //LCA-641
                                                    //LuanaSMS__End_Time__c=dtET, //LCA-641
                                                    RecordTypeId=rtVSession.Id,
                                                    Topic_Key__c=topicKey,
                                                    Maximum_Capacity__c=200,
                                                    Adobe_Connect_SCO_Id__c=scoId,
                                                    AdobeConnect_Session_External_Id__c=sessName,
                                                    Adobe_Connect_Url__c='https://' + domainName + urlPath
                                                );
            
            //LCA-641 query the child session and set the session date/time here 
            setSessionDateTime(scoId, sess);
            
            SessWrapper sessWrapper = new SessWrapper(true, sess, '');
            sessWrapperList.add(sessWrapper);
            
        }
    
        return sessWrapperList;
    }
    
    //LCA-624
    private DateTime parseDate(String dtString){
        String timezone = dtString.substring(23, dtString.length());
        String [] timezoneinfo = timezone.split(':');
        
        integer hourz = 0 - Integer.valueOf(timezoneinfo[0]);
        integer minz;
        if(hourz > 0){
            minz = Integer.valueOf(timezoneinfo[1]);
        } else {
            minz = 0 - Integer.valueOf(timezoneinfo[1]);
        }
        
        Datetime dt = DateTime.valueOfGmt(dtString);
        dt = dt.addHours(hourz);
        dt = dt.addMinutes(minz);
        
        return dt;
    }
    
    //LCA-641
    private void setSessionDateTime(String parentSCOId, LuanaSMS__Session__c session){
        
        String rawEndPoint = getACConnWithAction('sco-expanded-contents') + '&sco-id={0}&filter-rows=1&filter-type=seminarsession';
        
        Http queryH = new Http();
        HttpRequest queryReq = new HttpRequest();
        queryReq.setEndpoint(String.format(rawEndPoint, new List<String>{EncodingUtil.urlEncode(parentSCOId, 'UTF-8')}));
        queryReq.setMethod('GET');
        HttpResponse queryRes = queryH.send(queryReq);
        Dom.Document queryDoc = queryRes.getBodyDocument();
        Dom.XMLNode queryResults = queryDoc.getRootElement();
        Dom.XMLNode[] scos;
        
        if(queryResults.getChildElement('expanded-scos', null) != null){
           scos = queryResults.getChildElement('expanded-scos', null).getChildElements();
        }
        
        if(scos != null){
            for(Dom.XMLNode sco : scos) {
                
                if(sco.getChildElement('date-begin', null) != null){
                    session.LuanaSMS__Start_Time__c = parseDate(sco.getChildElement('date-begin', null).getText().replace('T', ' '));
                }
                
                if(sco.getChildElement('date-end', null) != null){
                    session.LuanaSMS__End_Time__c = parseDate(sco.getChildElement('date-end', null).getText().replace('T', ' '));
                }
                
            }
        }
    }
    
    public String getACConnWithAction(String action) {
        // This custom setting should not be null, page 1 should check
        // We will need a remote site as deployment component
        Luana_Extension_Settings__c adobeConn = Luana_Extension_Settings__c.getValues('AdobeConnect_URL');      
        return adobeConn.Value__c + '/api/xml?action=' + action + '&session=' + sessionCookie;
    
    }
    
    // Check to ensure username, password and filter are populated in page 1
    public boolean hasReqInput() {
        if(username == '' || password == '' || filter == '')    
            return false;
        else
            return true;
    
    }
    
    public PageReference goToVCUpsert() {
        boolean hasError = false;
        List<LuanaSMS__Session__c> sessList = new List<LuanaSMS__Session__c>();
        
        for(SessWrapper sessWrap : sessWrapperList) {
            if(sessWrap.isSelected) {
                if(sessWrap.sess.Name == null) {
                    sessWrap.errorMessage = 'Name must not be empty';  
                    hasError = true;
                } else if(sessWrap.sess.Topic_Key__c == null) {
                    sessWrap.errorMessage = 'Topic Key must not be empty';  
                    hasError = true;
                } else if(sessWrap.sess.Registration_Start_Date__c == null) {
                    sessWrap.errorMessage = 'Registration Start Date must not be empty';  
                    hasError = true;
                } else if(sessWrap.sess.Registration_End_Date__c == null) {
                    sessWrap.errorMessage = 'Registration End Date must not be empty';  
                    hasError = true;
                } else if(sessWrap.sess.Maximum_Capacity__c == null) {
                    sessWrap.errorMessage = 'Maximum Capacity must not be empty';  
                    hasError = true;
                } else {
                    sessWrap.errorMessage = '';  
                }
                
                if(!hasError)
                    sessList.add(sessWrap.sess);
            }
        }
        
        if(hasError) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'All fields must be populated for all selected rows. Please check the Data Validation message'));
            return null;    
        } else {
            try {  
                upsert sessList AdobeConnect_Session_External_Id__c;
                
                List<LuanaSMS__Course_Session__c> csList = [SELECT Id, LuanaSMS__Session__c FROM LuanaSMS__Course_Session__c WHERE LuanaSMS__Course__c =: c.Id AND LuanaSMS__Session__c in: sessList];
                Map<Id, Id> existingCSMap = new Map<Id, Id>();
                for(LuanaSMS__Course_Session__c cs : csList) {
                    existingCSMap.put(cs.LuanaSMS__Session__c, cs.Id);
                }
                List<LuanaSMS__Course_Session__c> newCSList = new List<LuanaSMS__Course_Session__c>();
                for(LuanaSMS__Session__c sess : sessList) {
                    if(!existingCSMap.containsKey(sess.Id)) {
                        newCSList.add(new LuanaSMS__Course_Session__c(LuanaSMS__Session__c=sess.Id, LuanaSMS__Course__c= c.Id));
                    }
                }             
                insert newCSList;                
                return new PageReference('/' + c.Id);
            } catch (DmlException ex) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Error while upserting the Session data. Error: ' + ex.getMessage()));
                return null;
            }                
        }    
    }

    public PageReference back() {        
        return Page.luana_VCImportWizard;
    
    }

    public PageReference goToVCImportSummary() {  
         
        meetingCount = 0;
        if(hasReqInput()) {     
            Http commonInfoH = new Http();
            HttpRequest commonInfoReq = new HttpRequest();
            // Get Session Cookie
            commonInfoReq.setEndpoint(getACConnWithAction('common-info'));
            commonInfoReq.setMethod('GET');
            HttpResponse commonInfoRes = commonInfoH.send(commonInfoReq);
            Dom.Document commonInfoDoc = commonInfoRes.getBodyDocument();
            Dom.XMLNode commonInfoResults = commonInfoDoc.getRootElement();
            
            // Check if Common element and cookie element is in XML body
            if(commonInfoResults.getChildElement('common', null) != null && commonInfoResults.getChildElement('common', null).getChildElement('cookie', null) != null)
                sessionCookie = commonInfoResults.getChildElement('common', null).getChildElement('cookie', null).getText();
            else {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Unable to obtain Adobe Connect Session Cookie to login'));
                return null;
            }
            
            // Check if Cookie is returned            
            if(sessionCookie != null && sessionCookie != '') {
                Http loginH = new Http();
                HttpRequest loginReq = new HttpRequest();
                loginReq.setEndpoint(getACConnWithAction('login') + '&login=' + EncodingUtil.urlEncode(username, 'UTF-8') + '&password=' + EncodingUtil.urlEncode(password, 'UTF-8'));
                loginReq.setMethod('GET');
                HttpResponse loginRes = loginH.send(loginReq);
                Dom.Document loginDoc = loginRes.getBodyDocument();
                Dom.XMLNode loginResults = loginDoc.getRootElement();
                
                System.debug('=== loginResults ===' + loginResults);
                if(loginResults.getChildElement('status', null) != null)
                    loginResults.getChildElement('status', null).getAttribute('code', null);                    
                else {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Unable to login to Adobe Connect. Status Element is not found'));
                    return null;
                }            
                
                if(loginResults.getChildElement('status', null) != null && loginResults.getChildElement('status', null).getAttribute('code', null) == 'ok') {
                    getACSessions();
                    return Page.luana_VCImportWizardSummary;        
                } else {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Unable to login to Adobe Connect. Status code : ' + loginResults.getChildElement('status', null).getAttribute('code', null)));
                    return null;    
                }
            } else {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'Unable to obtain Adobe Connect Session Cookie to login'));
                return null;
            }
        } else {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL, 'All fields must be populated before you proceed.'));
            return null;
        }
        
        
    }
    
    public class SessWrapper {
        public boolean isSelected {set; get;}
        public LuanaSMS__Session__c sess {set; get;}
        public String errorMessage {set; get;}
        
        public SessWrapper(Boolean isSelected, LuanaSMS__Session__c sess, String errorMessage) {
            this.isSelected = isSelected;
            this.sess = sess;
            this.errorMessage = errorMessage;
        }
    }
    
}