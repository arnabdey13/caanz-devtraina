/**
    Developer: WDCi (kh)
    Development Date:13/12/2016
    Task: Test class for AAS_FinalResultCalculationController
**/
@isTest(seeAllData=false)
private class AAS_Test_FinalResultCalculationCtl {

    final static String contextLongName = 'AAS_Test_FinalResultCalculationCtl';
    final static String contextShortName = 'frc';
    
    public static AAS_Course_Assessment__c courseAss;
    public static AAS_Student_Assessment__c sa;
    public static AAS_Student_Assessment_Section__c sas;
    
    public static List<AAS_Student_Assessment_Section_Item__c> sasiList;
    
    public static void initial(Boolean isCapstone){
        
        Luana_DataPrep_Test dataPrepUtil = new Luana_DataPrep_Test();
        AAS_DataPrep_Test aasdataPrepUtil = new AAS_DataPrep_Test();
        
        insert aasdataPrepUtil.defauleCustomSetting();
        
        Map<String, Id> recordTypeMap = aasdataPrepUtil.getRecordTypeMap('Exam');
        
        //Training Org
        LuanaSMS__Training_Organisation__c trainOrg = dataPrepUtil.createNewTraningOrg(contextLongName, contextLongName, contextShortName, 'address line 1', 'address loc 1', '5000');
        insert trainOrg;
        
        //Program
        LuanaSMS__Program__c prog = dataPrepUtil.createNewProgram(contextShortName, contextLongName, 'Confirmed', 'Graduate diploma', 'Nationally accredited qualification specified in a national training package');
        insert prog;
        
        //Product
        List<Product2> prodList = new List<Product2>();
        prodList.add(dataPrepUtil.createNewProduct('FIN_AU_' + contextShortName, 'AU0001'));
        prodList.add(dataPrepUtil.createNewProduct('FIN_NZ_' + contextShortName, 'NZ0001'));
        prodList.add(dataPrepUtil.createNewProduct('FIN_INT_' + contextShortName, 'INT0001'));
        insert prodList;
        
        //Program Offerings
        LuanaSMS__Program_Offering__c poAM = dataPrepUtil.createNewProgOffering('PO_AM_' + contextShortName, dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), prog.Id, trainOrg.Id, null, null, null, 1, 1);
        if(isCapstone){
            poAM.IsCapstone__c = true;
        }
        insert poAM;
        
        //Course
        LuanaSMS__Course__c courseAM = dataPrepUtil.createNewCourse('AAA216', poAM.Id, dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        courseAM.LuanaSMS__Allow_Online_Enrolment__c = true;
        insert courseAM;
        
        List<LuanaSMS__Course__c> course = [Select Id, Name, LuanaSMS__Program_Offering__r.IsCapstone__c from LuanaSMS__Course__c Where Id=:courseAM.Id];
        System.debug('***course:: ' + course);
        
        List<LuanaSMS__Program_Offering__c> po = [Select Id, Name, IsCapstone__c from LuanaSMS__Program_Offering__c Where Id=:poAM.Id];
        System.debug('***po:: ' + po);
        
        //CA
        courseAss = aasdataPrepUtil.createCourseAssessment(courseAM.Id, 'CA_' + contextShortName);
        courseAss.AAS_Module_Passing_Mark__c = 40;
        courseAss.AAS_Non_Exam_Data_Loaded__c = true;
        courseAss.AAS_Exam_Data_Loaded__c = true;
        courseAss.AAS_Cohort_Adjustment_Exam__c = true;
        courseAss.AAS_Borderline_Remark__c = true;
        insert courseAss;
        

        //ASS
        AAS_Assessment_Schema_Section__c ass = aasdataPrepUtil.createAsessmentSchemaSection(courseAss.Id, 'ASS_' + contextShortName, recordTypeMap.get('AAS_Assessment_Schema_Section__c:Exam_Assessment'));
        ass.AAS_Passing_Mark__c = 40;
        ass.AAS_Cohort_Adjustment__c = 2;
        insert ass;
        
        List<AAS_Assessment_Schema_Section_Item__c> assiList = new List<AAS_Assessment_Schema_Section_Item__c>();
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA111 Q #1'));
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA111 Q #2'));
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA111 Q #3'));
        assiList.add(aasdataPrepUtil.createAssessmentSchemaSectionItems(ass.Id, recordTypeMap.get('AAS_Assessment_Schema_Section_Item__c:Exam_Assessment'), 20, 'AAA111 Q #4'));
        insert assiList;
        
        //SA - 
        sa = aasdataPrepUtil.createStudentAssessment(courseAss.Id);
        insert sa;
        
        AAS_Student_Assessment__c sa2 = aasdataPrepUtil.createStudentAssessment(courseAss.Id);
        insert sa2;

        //SAS
        sas = aasdataPrepUtil.createStudentAssessmentSection(sa.Id, recordTypeMap.get('AAS_Student_Assessment_Section__c:Exam_Assessment_SAS'));
        sas.AAS_Assessment_Schema_Section__c = ass.Id;
        sas.AAS_Adjustment__c = 1;
        insert sas;
        
        AAS_Student_Assessment_Section__c sas2 = aasdataPrepUtil.createStudentAssessmentSection(sa.Id, recordTypeMap.get('AAS_Student_Assessment_Section__c:Exam_Assessment_SAS'));
        sas2.AAS_Assessment_Schema_Section__c = ass.Id;
        sas2.AAS_Adjustment__c = 1;
        insert sas2;
        
        //SASI
        sasiList = new List<AAS_Student_Assessment_Section_Item__c>();
        
        for(AAS_Assessment_Schema_Section_Item__c assi: assiList){
            AAS_Student_Assessment_Section_Item__c sasi = aasdataPrepUtil.createStudentAssessmentSectionItem(sas.Id, recordTypeMap.get('AAS_Student_Assessment_Section_Item__c:Exam_Assessment_SASI'), assi.Id);
            sasi.AAS_Raw_Mark__c = 9;
            sasi.AAS_Adjustment__c = 0.75;
            //sasi.AAS_Special_Consideration_Adjustment__c = 0; //hk 9april17
            sasiList.add(sasi);
        }
        insert sasiList;
        
        List<AAS_Student_Assessment_Section_Item__c> sasiList2 = new List<AAS_Student_Assessment_Section_Item__c>();
        
        for(AAS_Assessment_Schema_Section_Item__c assi: assiList){
            AAS_Student_Assessment_Section_Item__c sasi2 = aasdataPrepUtil.createStudentAssessmentSectionItem(sas2.Id, recordTypeMap.get('AAS_Student_Assessment_Section_Item__c:Exam_Assessment_SASI'), assi.Id);
            sasi2.AAS_Raw_Mark__c = 12;
            sasi2.AAS_Adjustment__c = 0.75;
            //sasi2.AAS_Special_Consideration_Adjustment__c = 0; //hk 9april17
            sasiList2.add(sasi2);
        }
        insert sasiList2;
        
    }
    
    public static testMethod void test_FinalAdjNotDone() { 
    
        initial(true);
        
        Test.startTest();
            try{
                PageReference pageRef1 = Page.AAS_FinalResultCalculation;
                pageRef1.getParameters().put('id', String.valueOf(courseAss.Id));
                pageRef1.getParameters().put('type', String.valueOf('Exam_Assessment'));
                Test.setCurrentPage(pageRef1);
                
                ApexPages.StandardController sc1 = new ApexPages.StandardController(courseAss);
                AAS_FinalResultCalculationController finalResultCal = new AAS_FinalResultCalculationController(sc1);
            }Catch(Exception exp){
                Boolean expectedExceptionThrown =  exp.getMessage().contains('You are not allow to continue in this step. Please complete the Cohort Adjustment, Borderline Remark and Final Adjusment processes first.') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true, exp.getMessage());
            }
        Test.stopTest();
    }  
    
    public static testMethod void test_FinalAdjCompletedBefore() { 
    
        initial(true);
        
        courseAss.AAS_Final_Result_Calculation_Exam__c = true;
        update courseAss;
        
        Test.startTest();
            try{
                PageReference pageRef1 = Page.AAS_FinalResultCalculation;
                pageRef1.getParameters().put('id', String.valueOf(courseAss.Id));
                pageRef1.getParameters().put('type', String.valueOf('Exam_Assessment'));
                Test.setCurrentPage(pageRef1);
                
                ApexPages.StandardController sc1 = new ApexPages.StandardController(courseAss);
                AAS_FinalResultCalculationController finalResultCal = new AAS_FinalResultCalculationController(sc1);
            }Catch(Exception exp){
                Boolean expectedExceptionThrown =  exp.getMessage().contains('Final Result Calculation was completed before, if you wish to continue old data will be lost.') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true, exp.getMessage());
            }
        Test.stopTest();
    }  
    
    public static testMethod void test_MainIsCapstone() { 
    
        initial(true);
        
        courseAss.AAS_Final_Adjustment_Exam__c = true;
        update courseAss;

        Test.startTest();
            try{
                PageReference pageRef1 = Page.AAS_FinalResultCalculation;
                pageRef1.getParameters().put('id', String.valueOf(courseAss.Id));
                pageRef1.getParameters().put('type', String.valueOf('Exam_Assessment'));
                Test.setCurrentPage(pageRef1);
                
                ApexPages.StandardController sc1 = new ApexPages.StandardController(courseAss);
                AAS_FinalResultCalculationController finalResultCal = new AAS_FinalResultCalculationController(sc1);
                
                finalResultCal.isCapstone =  true;
                
                finalResultCal.doCalculate();
                finalResultCal.checkBatchCompletion();
                finalResultCal.doCancel();
                
                finalResultCal.isUpdateDone = true;
                finalResultCal.doRedirect();
                
                List<AAS_Student_Assessment__c> saResult = [Select Id,AAS_Final_Result__c from AAS_Student_Assessment__c Where Id =: sa.Id];
                
            }Catch(Exception exp){
                
            }
        Test.stopTest();
    }  
    
    public static testMethod void test_MainIsNotCapstone() { 
    
        initial(false);
        
        courseAss.AAS_Final_Adjustment_Exam__c = true;
        update courseAss;
        
        Test.startTest();
            try{
                PageReference pageRef1 = Page.AAS_FinalResultCalculation;
                pageRef1.getParameters().put('id', String.valueOf(courseAss.Id));
                pageRef1.getParameters().put('type', String.valueOf('Exam_Assessment'));
                Test.setCurrentPage(pageRef1);
                
                ApexPages.StandardController sc1 = new ApexPages.StandardController(courseAss);
                AAS_FinalResultCalculationController finalResultCal = new AAS_FinalResultCalculationController(sc1);
                
                finalResultCal.doCalculate();
                finalResultCal.checkBatchCompletion();
                finalResultCal.doCancel();
                
                finalResultCal.isUpdateDone = true;
                finalResultCal.doRedirect();
                
            }Catch(Exception exp){
                
            }
        Test.stopTest();
    }    
    
    public static testMethod void test_FinalAdjResultCalculated() { 
    
        initial(true);
        
        courseAss.AAS_Final_Adjustment_Exam__c = true;
        courseAss.AAS_Final_Result_Calculation_Exam__c= true;
        update courseAss;
        
        Test.startTest();
            try{
                PageReference pageRef1 = Page.AAS_FinalResultCalculation;
                pageRef1.getParameters().put('id', String.valueOf(courseAss.Id));
                pageRef1.getParameters().put('type', String.valueOf('Exam_Assessment'));
                Test.setCurrentPage(pageRef1);
                
                ApexPages.StandardController sc1 = new ApexPages.StandardController(courseAss);
                AAS_FinalResultCalculationController finalResultCal = new AAS_FinalResultCalculationController(sc1);
            }Catch(Exception exp){
                Boolean expectedExceptionThrown =  exp.getMessage().contains('Final Result Calculation was completed before, if you wish to continue old data will be lost.') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true, exp.getMessage());
            }
        Test.stopTest();
    }      
    
}