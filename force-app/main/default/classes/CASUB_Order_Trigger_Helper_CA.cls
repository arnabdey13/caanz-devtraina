/*------------------------------------------------------------------------------------
Author:        Paras Prajapati
Company:       Salesforce
Description:   This is an helper class, which will be trigger by OrderTrigger, for updating Questionnaire_Response__c and Subscription__c record post Order Payment done.

History
Date            Author             Comments
--------------------------------------------------------------------------------------
05-24-2019     Paras Prajapati         Initial Release
------------------------------------------------------------------------------------*/
public class CASUB_Order_Trigger_Helper_CA {
    
    
    public static void OnAfterOrderPaymentUpdate( List<Order> newOrders, List<Order> oldOrders, Map<ID, Order> newOrdersMap , Map<ID, Order> oldOrdersMap ){
        List<String> updatedOrdersAccountIDsList = new List<String>();
        List<String> updatedOrdersIDsList = new List<String>();
        Id recordTypeSubscription = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        for(Order order:newOrders) {
            if(order.Netsuite_Revenue_Status__c=='Completed' && order.RecordTypeId == recordTypeSubscription){
                updatedOrdersAccountIDsList.add(order.AccountId);
                updatedOrdersIDsList.add(order.Id);
            }
        }
        if(updatedOrdersAccountIDsList!=null && updatedOrdersAccountIDsList.size()>0){
            /**
            *  Fetch Subsription from Account
            */ 
            CASUB_Mandatory_Notification_Config__mdt casub_Mandatory_Notification_Config = getCurrentYear();
            String currentYear = casub_Mandatory_Notification_Config.Current_Year__c;
            List<Subscription__c> subsList = getSubscriptionRecord(newOrdersMap.keySet(),currentYear,updatedOrdersIDsList);
            System.debug('subsList to Process' + subsList);
            if(subsList!=null && subsList.size()>0){
               Database.SaveResult[] subsUpdateResultList = Database.update(subsList, false); 
            }
            /*List<Questionnaire_Response__c> questionnaireRespList =  getQuestionnaireResponseRecord(updatedOrdersAccountIDsList,currentYear);
            System.debug('questionnaireRespList to Process' + questionnaireRespList);
            if(updatedOrdersAccountIDsList!=null && updatedOrdersAccountIDsList.size()>0){
              Database.SaveResult[] questionnaireRespResultList = Database.update(questionnaireRespList, false);
            }
            */
        }
    }
    
    public static void OnAfterOrderInsertUpdate( List<Order> newOrders, List<Order> oldOrders, Map<ID, Order> newOrdersMap , Map<ID, Order> oldOrdersMap ){
        Id recordTypeSubscription = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Subscription').getRecordTypeId();
        CASUB_Mandatory_Notification_Config__mdt casub_Mandatory_Notification_Config = getCurrentYear();
        String currentYear = casub_Mandatory_Notification_Config.Current_Year__c;
        List<String> updatedOrdersAccountIDsList = new List<String>();
        List<String> updatedOrdersIDsList = new List<String>();
        Map<String,String> accountIdOrderIdsMap = new Map<String,String>();
        Map<String,Order> accountIdOrderMap = new Map<String,Order>();
        
        for(Order order:newOrders) {
            if((order.Status=='Pending' || order.Status=='Activated') && order.NetSuite_Order_ID__c!=null &&  order.RecordTypeId == recordTypeSubscription){
                updatedOrdersAccountIDsList.add(order.AccountId);
                updatedOrdersIDsList.add(order.Id); 
                Order tempOrder = new Order();
                if(accountIdOrderMap.containsKey(order.AccountId)){
                    tempOrder = accountIdOrderMap.get(order.AccountId);
                    if(tempOrder.CreatedDate<=order.CreatedDate){
                        accountIdOrderMap.put(order.AccountId,order);
                    }
                }else{
                    accountIdOrderMap.put(order.AccountId,order);
                }
                //accountIdOrderIdsMap.put(order.AccountId,order);
            }
        }
        List<Subscription__c> subscriptionList = getSubscriptionRecordRelatedToAccount(updatedOrdersAccountIDsList,currentYear);
        List<Subscription__c> updatedSubsList = new List<Subscription__c>();
        if(subscriptionList!=null && subscriptionList.size()>0){
            for(Subscription__c subscription : subscriptionList){
                if(accountIdOrderMap.containsKey(subscription.Account__c)){
                    subscription.Sales_Order__c = accountIdOrderMap.get(subscription.Account__c).Id;
                    updatedSubsList.add(subscription);
                }
            }
        }
        if(updatedSubsList!=null && updatedSubsList.size()>0){
            update updatedSubsList;
        }
    }
    
    public static CASUB_Mandatory_Notification_Config__mdt getCurrentYear() { 
        String currentYear='';
        List<CASUB_Mandatory_Notification_Config__mdt> casub_Mandatory_Notification_ConfigList = [select Current_Year__c, Membership_Approval_Date__c, 
                                                                                                  CPP_Approval_Date__c from CASUB_Mandatory_Notification_Config__mdt where DeveloperName = 'Current_Year'];
        if(casub_Mandatory_Notification_ConfigList!=null && casub_Mandatory_Notification_ConfigList.size()>0){
            return casub_Mandatory_Notification_ConfigList.get(0);       
        }
        return casub_Mandatory_Notification_ConfigList.get(0);
    }
    
    private static List<Subscription__c> getSubscriptionRecord(Set<Id> orderIdList,String currentYear,List<String> updatedOrdersIDsList){ 
        List<Subscription__c> subscriptionList = new List<Subscription__c>();
        for(List<Subscription__c> subsList : [Select Id,Account__c,Sales_Order_Status__c,Sales_Order__c from Subscription__c where 
                                              Sales_Order__c IN: updatedOrdersIDsList AND Year__c = : currentYear]){
            for(Subscription__c subscription : subsList){
                subscription.Sales_Order_Status__c = 'Completed';
                subscriptionList.add(subscription);
            }
        }
        return subscriptionList;
    }
    
    private static List<Subscription__c> getSubscriptionRecordRelatedToAccount(List<String> accountIdList,String currentYear){ 
        List<Subscription__c> subscriptionList = new List<Subscription__c>();
        for(List<Subscription__c> subsList : [Select Id,Account__c,Sales_Order_Status__c,Sales_Order__c from Subscription__c where 
                                              Account__c IN: accountIdList AND Year__c = : currentYear]){
            for(Subscription__c subscription : subsList){
                //subscription.Sales_Order_Status__c = 'Completed';
                subscriptionList.add(subscription);
            }
        }
        return subscriptionList;
    }
    
    /*
    private static List<Questionnaire_Response__c> getQuestionnaireResponseRecord(List<String> accountIdList,String currentYear){ 
        List<Questionnaire_Response__c> questionnaireResponseList = new List<Questionnaire_Response__c>();
        for(List<Questionnaire_Response__c> questionnaireRespList : [Select Id,Account__c,Lock__c,Status__c from Questionnaire_Response__c 
                                                                     where Account__c IN: accountIdList AND Year__c = : currentYear]){
            for(Questionnaire_Response__c questionnaireResp : questionnaireRespList){
                questionnaireResp.Status__c = 'Submitted';
                questionnaireResp.Lock__c=true;
                questionnaireResp.Date__c = System.today(); 
                questionnaireResponseList.add(questionnaireResp);
            }
        }
        return questionnaireResponseList;
    }
    */
}