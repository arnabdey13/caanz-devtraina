/*
    Test class for FindCAConfigMemberType.cls
    ======================================================
    Changes:
        October 2017    Andrew Kopec  Created
*/
@isTest
public class FindCAConfigMemberTypeTest {


static testMethod void testDoGet() {
        
    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();
  
    req.requestURI = '/services/apexrest/FindCA/v1.0/Config/MemberType';  
    req.httpMethod = 'GET';
    req.addParameter('BranchCountry','Australia');
        
    RestContext.request = req;
    RestContext.response = res;    

    List<FindCA_API_Member_Type__mdt> results = FindCAConfigMemberType.doGet();
    //System.assertEquals(1, results.size());

    System.debug(results);
        
  }

}