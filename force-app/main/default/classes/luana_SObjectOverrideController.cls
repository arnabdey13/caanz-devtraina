/*
  Change History
    LCA-789: 21/07/2016 - fixed list view from record
  
*/
public with sharing class luana_SObjectOverrideController {

    SObject sObj;
    String objKeyPrefix;
    String filterId;
    Boolean isCommUser;
    
    public luana_SObjectOverrideController(ApexPages.StandardController controller) {
        sObj = controller.getRecord();
        getObjectKeyPrefix(sObj);       
        setIsCommUser();         
    }
    public luana_SObjectOverrideController(ApexPages.StandardSetController controller) {
        sObj = controller.getRecord();
        filterId = controller.getFilterId();
        getObjectKeyPrefix(sObj);
        setIsCommUser();
        
        system.debug('getRecords() :: ' + controller.getRecords());
    }
    
    private void setIsCommUser() {
        if(UserInfo.getProfileId() == ProfileCache.getId('NZICA Community Login User'))
            isCommUser = true;
        else
            isCommUser = false;
    
    }
    private void getObjectKeyPrefix(SObject sObj) {
        Schema.DescribeSObjectResult r = sObj.getSObjectType().getDescribe();
        objKeyPrefix = r.getKeyPrefix();
    }
    
    public PageReference gotoViewPage() {
        if(isCommUser){
            return new PageReference('/');
        } else {
            PageReference PR = new PageReference('/' + sObj.Id);
            PR.getParameters().put('nooverride','1');
            return PR;                        
        }
    }
       
    public PageReference gotoEditPage() {
        if(isCommUser){
            return new PageReference('/');
        } else {
            PageReference PR = new PageReference('/' + sObj.Id + '/e');
            PR.getParameters().put('retURL', ApexPages.currentPage().getParameters().get('retURL'));
            PR.getParameters().put('nooverride','1');
            return PR;                        
        }            
    }

    public PageReference gotoNewPage() {
        if(isCommUser){
            return new PageReference('/');
        } else {
            PageReference PR = new PageReference('/' + objKeyPrefix + '/e'); 
            
            /*
            String recordTypeId = ApexPages.currentPage().getParameters().get('RecordType');
            if(recordTypeId != null)
                PR.getParameters().put('RecordType', recordTypeId);
            PR.getParameters().put('retURL', ApexPages.currentPage().getParameters().get('retURL'));
            PR.getParameters().put('nooverride','1');
            
            */
            // Fix for LCA-109
            Map<String, String> paramMap = ApexPages.currentPage().getParameters();
            for(String param : paramMap.keySet()) {
                if(param != 'save_new')            
                    PR.getParameters().put(param, paramMap.get(param));
            }
            
            PR.getParameters().put('nooverride','1');
            
            return PR;                        
        }            
    }
    
    public String parentId {set;get;} //LCA-789
    
    public PageReference gotoListPage() {
        /*LCA-789 - disabled 
        if(isCommUser){
            return new PageReference('/');
        } else {
            PageReference PR = new PageReference('/' + objKeyPrefix);
            
            for(String key : ApexPages.currentPage().getParameters().keySet()){ //LCA-789
              PR.getParameters().put(key, ApexPages.currentPage().getParameters().get(key));
            }
            
            PR.getParameters().put('fcf',filterId.left(15));
            PR.getParameters().put('nooverride','1');
            return PR;                         
        }*/
        
        //LCA-789
        if(isCommUser){
            return new PageReference('/');
        } else {
            PageReference PR = new PageReference('/' + objKeyPrefix);
            
            for(String key : ApexPages.currentPage().getParameters().keySet()){ //LCA-789
              PR.getParameters().put(key, ApexPages.currentPage().getParameters().get(key));
            }
            
            PR.getParameters().put('fcf',filterId.left(15));
            PR.getParameters().put('nooverride','1');
            
            if(parentId != null && parentId != ''){
              PR.getParameters().put('id',parentId);
            }
            
            return PR;                         
        }          
    }
    
    
    
}