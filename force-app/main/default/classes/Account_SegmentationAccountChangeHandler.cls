/***********************************************************************************************************************************************************************
Name: Account_SegmentationAccountChangeHandler
============================================================================================================================== 
Purpose: This Class contains the code related to Segmentation Account Change Functionality invoked from the AccountTriggerHandler 
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR                 DATE          DETAIL     Description 
1.0        Rama Krishna    07/02/2019    Created          This Class contains the code related to Segmentation Account Change Functionality invoked from the AccountTriggerHandler 
Rev1.1     Andrew Kopec    24/04/2019    Change           Trigger a call to Address Change Handler even if address did not change but Membership changed from Non Memeber to Memeber
Rev1.2     Andrew Kopec    24/04/2019    Bug              Incorrect condition - triggers change during tests data preparation
                                                             
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/
public class Account_SegmentationAccountChangeHandler{
         
        public static void segmentationAccountChanges(List<Account> newAccountList,Map<Id,Account> newAccountMap,List<Account> oldAccountList,Map<Id,Account> oldAccountMap){
           // skip the trigger during tests, unless this handler Test
           // R1.2 if (!Test.isRunningTest() || ( !SegmentationAddressTriggerHandlerTest.forceTriggerDuringTest && !SegmentationMembersTriggerHandlerTest.forceTriggerDuringTest)) {            
           if (!Test.isRunningTest() || SegmentationAddressTriggerHandlerTest.forceTriggerDuringTest || SegmentationMembersTriggerHandlerTest.forceTriggerDuringTest) {                
                system.debug('Hi am here');
                //Map to determine a list of Accounts that will require update of the segmentation object for Address Change
                Map <Id, String> AddressSegmentsToUpdateIds = SegmentationAddressTriggerHandler.handleSegmentsAfterUpdate(newAccountList, newAccountMap, oldAccountList, oldAccountMap); 
                //Map to determine a list of Accounts that will require update of the segmentation object for Member data Change
                Map <Id, String> MembersSegmentsToUpdateIds = SegmentationMembersTriggerHandler.handleSegmentsAfterAccountUpdate(newAccountList, newAccountMap, oldAccountList, oldAccountMap); 
                
                if (AddressSegmentsToUpdateIds.size() >0 ){
                // It invokes a Queable Interface which handles a Web service Call to get the Address details to polulate on Segmentation records
                    SegmentationAddressTriggerUpdate segUpdateJob = new SegmentationAddressTriggerUpdate(AddressSegmentsToUpdateIds); ID jobID = System.enqueueJob(segUpdateJob);                 
                } 
                if (MembersSegmentsToUpdateIds.size() >0 ){
                //Upsert the Segmentation object records on Change to Account Memebrs information 
                  SegmentationMembersTriggerHandler.handleSegmentsAfterUpdateAsync(MembersSegmentsToUpdateIds); 
                    
                    // Rev1.1 change start - 
                    // if no address change but Membership changed from Non Member to Member force a call to Address trigger if address exists
                    if (AddressSegmentsToUpdateIds.size() == 0 ){
                        Map <Id, String> AddrMembSegToUpdateIds = SegmentationAddressTriggerHandler.handleSegmentsAfterMembershipChange(newAccountList, newAccountMap, oldAccountList, oldAccountMap);
                        if (AddrMembSegToUpdateIds.size() >0 ){
                            SegmentationAddressTriggerUpdate segUpdateJob2 = new SegmentationAddressTriggerUpdate(AddrMembSegToUpdateIds); ID jobID2 = System.enqueueJob(segUpdateJob2);
                            System.debug('SegUpdateJob Id:' + jobID2); 
                        } 
                    }
                    // Rev1.1 change end
                }
            }
        }
}