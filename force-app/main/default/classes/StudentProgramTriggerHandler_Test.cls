/***********************************************************************************************************************************************************************
Name: StudentProgramTriggerHandler_Test 
============================================================================================================================== 
Purpose: This class contains code related to Unit Testing and test coverage of StudentProgramTriggerHandler
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR           DATE          DETAIL     Description 
1.0        Vinay           21/02/2019    Created     This class contains code related to Unit Testing and test coverage of class StudentProgramTriggerHandler 
                                                             
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/
@isTest
public with sharing class StudentProgramTriggerHandler_Test {
    public static Luana_DataPrep_Test dataPrep;
    private static String classNamePrefixLong = 'StudentProgramTriggerHandler_Test';
    private static String classNamePrefixShort = 'spth';
    public static testmethod void testStudentProgramHandler(){
        Profile profile1 = [Select Id from Profile where name = 'CAANZ Service Desk'];
        PermissionSet luanaAdminPermission = [Select Id from PermissionSet where name = 'Luana_AdminPermission'];
        System.debug('What is the profile id ' + profile1);
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        User u = new User( UserRoleId = portalRole.Id,
                            ProfileId = profile1.Id,
                            Username = 'testtermsconditions1234423@charteredaccountantsanz.com',
                            Alias = 'batman',
                            Email='testtermsconditions1234423@charteredaccountantsanz.com',
                            EmailEncodingKey='UTF-8',
                            Firstname='Bruce',
                            Lastname='Wayne',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_NZ',
                            TimeZoneSidKey='Pacific/Auckland');
        insert u;
        
        PermissionSetAssignment permissonSetAssign = new PermissionSetAssignment(PermissionSetId = luanaAdminPermission.Id, AssigneeId = u.Id);
        insert permissonSetAssign;
        System.runas(u){
            //initialize
            dataPrep = new Luana_DataPrep_Test();
            
            //Create all the custom setting
            insert dataPrep.prepLuanaExtensionSettingCustomSettings();
            insert dataPrep.createLuanaConfigurationCustomSetting();
            
            //Create traning org
            LuanaSMS__Training_Organisation__c trainingOrg = dataPrep.createNewTraningOrg(classNamePrefixLong, classNamePrefixLong, classNamePrefixShort, 'St 123', 'Wollonggong', '2244');
            insert trainingOrg;
            
            //Create Product
            List<Product2> prodList = new List<Product2>();
            prodList.add(dataPrep.createNewProduct('FIN_AU_' + classNamePrefixLong, 'AU0001'));
            prodList.add(dataPrep.createNewProduct('FIN_NZ_' + classNamePrefixLong, 'NZ0001'));
            prodList.add(dataPrep.createNewProduct('FIN_INT_' + classNamePrefixLong, 'INT0001'));
            insert prodList;
            
            //Create program
            LuanaSMS__Program__c newProgrm = dataPrep.createNewProgram(classNamePrefixShort, classNamePrefixLong, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
            insert newProgrm;

            //Create Program Offerings
            List<LuanaSMS__Program_Offering__c> progList = new List<LuanaSMS__Program_Offering__c>();
            LuanaSMS__Program_Offering__c poAM1 = dataPrep.createNewProgOffering('PO_AM_1' + classNamePrefixLong, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), newProgrm.Id, trainingOrg.Id, prodList[0].Id,prodList[1].Id, prodList[2].Id, 1, 1);
            poAM1.IsCapstone__c = true;
            progList.add(poAM1);            
            insert progList;
            
            //Create courses
            List<LuanaSMS__Course__c> courseList = new List<LuanaSMS__Course__c>();
            LuanaSMS__Course__c c = dataPrep.createNewCourse('Graduate Diploma of Chartered Accounting', progList[0].Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
            courseList.add(c);
            LuanaSMS__Course__c c1 = dataPrep.createNewCourse('Graduate Diploma of Chartered Accounting 1', progList[0].Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('PPP'), 'Running');
            courseList.add(c1);
            LuanaSMS__Course__c c2 = dataPrep.createNewCourse('Graduate Diploma of Chartered Accounting 1', progList[0].Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Program'), 'Running');
            courseList.add(c2);
            LuanaSMS__Course__c c3 = dataPrep.createNewCourse('Graduate Diploma of Chartered Accounting 1', progList[0].Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Foundation'), 'Running');
            courseList.add(c3);
            insert courseList;
            
            List<Account> memberAccounts = new List<Account>();
            for(integer i = 0; i < 3; i ++){
                Account mAccount = dataPrep.generateNewApplicantAcc('Zac_' + classNamePrefixShort + '_' + i, classNamePrefixLong, 'Full_Member');
                //May need to add email to generateNewApplicantAcc
                mAccount.PersonEmail = classNamePrefixLong + i + '@example.com';
                mAccount.Member_Id__c = '1234' + i;
                mAccount.Affiliated_Branch_Country__c = 'Australia';
                mAccount.Membership_Class__c = 'Full';
                mAccount.Communication_Preference__c= 'Home Phone';
                mAccount.PersonHomePhone= '1234';
                mAccount.PersonOtherStreet= '83 Saggers Road';
                mAccount.PersonOtherCity='JITARNING';
                mAccount.PersonOtherState='Western Australia';
                mAccount.PersonOtherCountry='Australia';
                mAccount.PersonOtherPostalCode='6365';
                
                memberAccounts.add(mAccount);
            }
            
            Test.startTest();
            insert memberAccounts;               
            
            List<Contact> contactList = [SELECT Id, AccountId FROM Contact WHERE AccountID IN : memberAccounts];
            
            List<LuanaSMS__Student_Program__c> studentProgramsList = new List<LuanaSMS__Student_Program__c>();
            for(integer i = 0; i < 3; i ++){
                for(LuanaSMS__Course__c courseRec : courseList){
                    LuanaSMS__Student_Program__c sp = dataPrep.createNewStudentProgram(dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), 
                                                        contactList[i].id, courseRec.Id, 'Australia', 'Completed');      
                    
                    studentProgramsList.add(sp);
                }
            }            
            system.debug('***size of Student program list-->'+studentProgramsList.size());        
            insert studentProgramsList;
            system.debug('***Student program list-->'+studentProgramsList);

            list<LuanaSMS__Student_Program__c> studenrProgList = [Select Id from LuanaSMS__Student_Program__c where Random_Number__c != null AND Duplicate_Check_Key__c != null];
            system.assertEquals(true, studenrProgList.size()>0);
            system.assertEquals(true, studenrProgList.size()==studentProgramsList.size());

            update studentProgramsList;
            
            delete studentProgramsList;
            Test.stopTest();

            list<LuanaSMS__Luana_Log__c> luanaLogList = [Select Id from LuanaSMS__Luana_Log__c];
            system.assertEquals(true, luanaLogList.size()>0);

            undelete studentProgramsList;

            PermissionSet muteProcessesPermission = [Select Id from PermissionSet where name = 'Mute_Triggers_Processes'];
            PermissionSetAssignment mutePermissonSetAssign = new PermissionSetAssignment(PermissionSetId = muteProcessesPermission.Id, AssigneeId = u.Id);
            insert mutePermissonSetAssign;

            update studentProgramsList;
        }
    }
}