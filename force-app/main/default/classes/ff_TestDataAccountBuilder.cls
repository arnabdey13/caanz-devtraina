/**
* @author Jannis Bott
* @date 23/11/2016
*
* @group FormsFramework
*
* @description  Account builder that is used to generate test accounts
*/
public with sharing class ff_TestDataAccountBuilder {

    private String name;
    private List<String> billingAddress;

    public ff_TestDataAccountBuilder name(String name) {
        this.name = name;
        return this;
    }

    @TestVisible
    private ff_TestDataAccountBuilder billingAddress(String street, String city, String state, String postCode, String country) {
        this.billingAddress = new List<String>{
                street, city, state, postCode, country
        };
        return this;
    }

    public List<Account> create(Integer count, Boolean persist) {
        List<Account> accounts = new List<Account>();
        for (Integer i = 0; i < count; i++) {
            accounts.add(new Account(
                    Name = this.name,
                    BillingStreet = this.billingAddress[0],
                    BillingCity = this.billingAddress[1],
                    BillingState = this.billingAddress[2],
                    BillingPostalCode = this.billingAddress[3],
                    BillingCountry = this.billingAddress[4]
            ));
        }
        if (persist) insert accounts;
        return accounts;
    }

}