/**
    Developer: WDCi (kh)
    Development Date:22/03/2017
    Task: Luana Test class for luana_LifelongLearningCoursesController
    
**/
@isTest(seeAllData=false)
private class luana_LifelongLearningCoursesCtl_Test {
     
    public static User memberUser {get; set;}
    public static Luana_DataPrep_Test dataPrep;
    public static luana_CommUserUtil userUtil {get; set;} 
    public static Account memberAccount {get; set;}
    private static map<String, Id> commProfIdMap {get; set;}
    public static Payment_Token__c currentPToken {get; set;}
     
    private static String classNamePrefixLong = 'luana_LifelongLearningCoursesCtl_Test';
    private static String classNamePrefixShort = 'lllc';
    
    
    public static void initial(){
        //initialize
        dataPrep = new Luana_DataPrep_Test();
        
        //Create all the custom setting
        insert dataPrep.prepLuanaExtensionSettingCustomSettings();
        insert dataPrep.createLuanaConfigurationCustomSetting();
        
        //Create user with Member and Employer community access
        memberAccount = dataPrep.generateNewApplicantAcc('Joe_' + classNamePrefixShort, classNamePrefixLong, 'Full_Member');
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.PersonEmail = 'joe_1_' + classNamePrefixShort+'@gmail.com';//LCA-921
        memberAccount.Communication_Preference__c= 'Home Phone';
        memberAccount.PersonHomePhone= '1234';
        memberAccount.PersonOtherStreet= '83 Saggers Road';
        memberAccount.PersonOtherCity='JITARNING';
        memberAccount.PersonOtherState='Western Australia';
        memberAccount.PersonOtherCountry='Australia';
        memberAccount.PersonOtherPostalCode='6365';  

        insert memberAccount;
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
    }
    
    public static void prepareSampleEnrolmentData(){
        
        //LCA-921
        memberUser = [Select Id, Email, FirstName, LastName, UserName, Name from User Where Email =: 'joe_1_' + classNamePrefixShort+'@gmail.com' limit 1];
        
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
       
        

        userUtil = new luana_CommUserUtil(memberUser.Id);
        insert dataPrep.generateNewBusinessAcc('Price Waterhouse_' + classNamePrefixShort, 'Business_Account', 'Chartered Accounting', '11111111', '012112111', 'NZICA', '123 Main Street', 'Active');
        String bAccount = 'Price Waterhouse_' + classNamePrefixShort;
        Account acc = [Select Id, Name from Account Where Name =: bAccount limit 1];
        
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defEmployerPermSetId.value__c));
        list<PermissionSetAssignment> permissAssign = [Select Id from PermissionSetAssignment where AssigneeId =: memberUser.Id AND (PermissionSetId =: defMemberPermSetId.value__c OR PermissionSetId =: defEmployerPermSetId.value__c)];
        System.runAs(memberUser){
            if(permissAssign == null)
                insert psas;
        }
        
        Map<String, Id> courseRecordTypeMap = dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c');
        
        LuanaSMS__Training_Organisation__c trainingOrg = dataPrep.createNewTraningOrg(classNamePrefixShort, classNamePrefixLong, classNamePrefixShort, 'Virtual Street 123', 'Queensland', '4551');
        insert trainingOrg;
        LuanaSMS__Program__c program = dataPrep.createNewProgram(classNamePrefixShort, classNamePrefixLong, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        insert program;
        LuanaSMS__Program_Offering__c po = dataPrep.createNewProgOffering('po' + classNamePrefixShort, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), program.Id, trainingOrg.Id, null, null, null, 1, 1);
        insert po;
        LuanaSMS__Course__c courseV = dataPrep.createNewCourse('Graduate Diploma of Virtual Accounting_' + classNamePrefixShort, po.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Program'), 'Running');
        courseV.LuanaSMS__Allow_Online_Enrolment__c = true;
        courseV.recordTypeId = courseRecordTypeMap.get('Lifelong_Learning');
        courseV.LuanaSMS__Status__c = 'Running';
        insert courseV;
        
        
        Id studProgAccModuleId = dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get(luana_EnrolmentConstants.RECORDTYPE_STUDENTPROGRAM_LLL);
        
        LuanaSMS__Student_Program__c newStudProg = dataPrep.createNewStudentProgram(studProgAccModuleId, userUtil.custCommConId, courseV.Id, 'Australia', 'In Progress');
        newStudProg.Paid__c = TRUE;

            
        insert newStudProg;
    }
    
    
    public static testMethod void testController1() { 
        Test.startTest();
            initial();
        Test.stopTest();
        prepareSampleEnrolmentData();
        
        System.runAs(memberUser){
            luana_LifelongLearningCoursesController llcc = new luana_LifelongLearningCoursesController();
            
            luana_LifelongLearningAvailableCoursesCp llAvailable = new luana_LifelongLearningAvailableCoursesCp();
            List<LuanaSMS__Course__c> courses = llAvailable.getAvailableCourses();
            System.assertEquals(courses.size(), 1, 'Expect one available course found');
            
            luana_LifelongLearningMyRecentCoursesCp llMyRecent = new luana_LifelongLearningMyRecentCoursesCp();
            llMyRecent.setContactId(userUtil.custCommConId);
            List<LuanaSMS__Student_Program__c> spList = llMyRecent.getLLLCourses();
            System.assertEquals(spList.size(), 1, 'Expect one SP found');
        }
    }
}