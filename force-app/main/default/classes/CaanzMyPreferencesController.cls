/*------------------------------------------------------------------------------------
Author:        Sumit Gupta
Company:       Salesforce
Description:   My preferences component controller

History
Date            Author             Comments
--------------------------------------------------------------------------------------
07-05-2018     Sumit Gupta       	Initial Release
------------------------------------------------------------------------------------*/
public with sharing class CaanzMyPreferencesController {
	
	@AuraEnabled
    public static String getPreferencesMetadata(){
        return JSON.serialize(getAllPreferencesMetadata());
    }

    @AuraEnabled
    public static String savePreferences(String preferencesJSONString){
    	List<MyPreference> myPreferences = (List<MyPreference>)JSON.deserialize(preferencesJSONString, List<MyPreference>.class);
        if(!myPreferences.isEmpty()){
        	Map<String, My_Preference__c> myPreferencesMap = new Map<String, My_Preference__c>();
        	for(MyPreference myPreference : myPreferences){
	    		myPreferencesMap.put(myPreference.name, myPreference.getRecord());
	    	}
	    	upsert myPreferencesMap.values();
	    	for(MyPreference myPreference : myPreferences){
	    		myPreference.id = myPreferencesMap.get(myPreference.name).Id;
	    	}
        }
        return JSON.serialize(myPreferences);
    }

    private static List<PreferenceGroup> getAllPreferencesMetadata(){
    	Map<String, PreferenceGroup> prefGroupsMap = new Map<String, PreferenceGroup>();
    	List<Preference_Group__mdt> prefGroups = getPreferenceGroups();
    	for(Preference_Group__mdt prefGroup : prefGroups){
    		prefGroupsMap.put(prefGroup.DeveloperName, new PreferenceGroup(prefGroup));
    	}
    	Set<String> prefGroupItemNames = new Set<String>();
    	List<Preference_Group_Item__mdt> prefGroupItems = getPreferenceGroupItems(prefGroupsMap.keySet());
    	for(Preference_Group_Item__mdt prefGroupItem : prefGroupItems){
    		PreferenceGroup prefGroup = prefGroupsMap.get(prefGroupItem.Preference_Group__r.DeveloperName);
    		prefGroup.addGroupItem(new PreferenceGroupItem(prefGroupItem));
    		prefGroupItemNames.add(prefGroupItem.DeveloperName);
    	}
    	List<My_Preference__c> myPreferences = getMyPreferences(prefGroupItemNames);
    	for(My_Preference__c myPreference : myPreferences){
    		PreferenceGroup prefGroup = prefGroupsMap.get(myPreference.Preference_Group__c);
    		prefGroup.addMyPreference(myPreference);
    	}
    	return sortPreferencesMetadata(prefGroupsMap.values());
    }

    private static List<Preference_Group__mdt> getPreferenceGroups(){
    	return [
    		SELECT Id, DeveloperName, MasterLabel, Description__c, Sequence__c 
    		FROM Preference_Group__mdt WHERE Active__c = true
    	]; 
    }

    private static List<Preference_Group_Item__mdt> getPreferenceGroupItems(Set<String> prefGroupNames){
    	return [
    		SELECT Id, DeveloperName, MasterLabel, Preference_Group__c, Preference_Group__r.DeveloperName,
    			Type__c, Picklist_Values__c, Sequence__c 
    		FROM Preference_Group_Item__mdt 
    		WHERE Preference_Group__r.DeveloperName IN: prefGroupNames AND Active__c = true
    	]; 
    }

    private static List<My_Preference__c> getMyPreferences(Set<String> prefGroupItemNames){
    	return [
    		SELECT Id, Name, Preference_Group__c, Preference_Group_Item__c, Type__c, Value__c
    		FROM My_Preference__c WHERE Preference_Group_Item__c IN: prefGroupItemNames 
    		AND User__c =: UserInfo.getUserId() AND Active__c = true 
    	]; 
    }

    private static List<PreferenceGroup> sortPreferencesMetadata(List<PreferenceGroup> prefGroups){
    	List<PreferenceGroup> sortedPrefGroups = new List<PreferenceGroup>();
    	Map<Decimal, PreferenceGroup> prefGroupsMap = new Map<Decimal, PreferenceGroup>();
    	for(PreferenceGroup prefGroup : prefGroups){
    		prefGroupsMap.put(prefGroup.sequence, prefGroup);
    		prefGroup.sortGroupItems();
    	}
    	List<Decimal> sequences = new List<Decimal>(prefGroupsMap.keySet());
    	sequences.sort();
    	for(Decimal seq : sequences){
    		sortedPrefGroups.add(prefGroupsMap.get(seq));
    	}
    	return sortedPrefGroups;
    }

    public class PreferenceGroup{
    	public Id id;
    	public String name, label, description;
    	public Decimal sequence;
    	public List<PreferenceGroupItem> groupItems;
        public Boolean isSelectedAll;

    	public PreferenceGroup(Preference_Group__mdt prefGroup){
    		id = prefGroup.Id;
    		name = prefGroup.DeveloperName; 
    		label = prefGroup.MasterLabel; 
    		description = prefGroup.Description__c;
    		sequence = prefGroup.Sequence__c;
    		groupItems = new List<PreferenceGroupItem>();
            isSelectedAll = false;
    	}

    	public void addGroupItem(PreferenceGroupItem groupItem){
    		groupItems.add(groupItem); if(!isSelectedAll) isSelectedAll = true;
    	} 

    	public void sortGroupItems(){
            if(groupItems.isEmpty()) return;
    		List<PreferenceGroupItem> sortedGroupItems = new List<PreferenceGroupItem>();
	    	Map<Decimal, PreferenceGroupItem> groupItemsMap = new Map<Decimal, PreferenceGroupItem>();
	    	for(PreferenceGroupItem groupItem : groupItems){
	    		groupItemsMap.put(groupItem.sequence, groupItem);
                if(isSelectedAll && groupItem.isBooleanType() && !groupItem.isSelected()){
                    isSelectedAll = false;
                }
	    	}
	    	List<Decimal> sequences = new List<Decimal>(groupItemsMap.keySet());
	    	sequences.sort();
	    	for(Decimal seq : sequences){
	    		sortedGroupItems.add(groupItemsMap.get(seq));
	    	}
	    	groupItems = sortedGroupItems;
    	}

    	public void addMyPreference(My_Preference__c myPreference){
    		for(PreferenceGroupItem groupItem : groupItems){
    			if(groupItem.name == myPreference.Preference_Group_Item__c){
    				groupItem.setMyPreference(myPreference);
    				break;
    			}
    		}
    	}
    }

    public class PreferenceGroupItem{
    	public Id id, groupId;
    	public String name, label, type;
    	public Decimal sequence;
        public Set<String> picklistValues;
    	public MyPreference myPreference;

    	public PreferenceGroupItem(Preference_Group_Item__mdt prefGroupItem){
    		id = prefGroupItem.Id;
    		groupId = prefGroupItem.Preference_Group__c; 
    		name = prefGroupItem.DeveloperName; 
    		label = prefGroupItem.MasterLabel;
    		type = prefGroupItem.Type__c;
    		sequence = prefGroupItem.Sequence__c;
    		if(prefGroupItem.Picklist_Values__c != null){
    			setPicklistValues(prefGroupItem.Picklist_Values__c);
    		}
    		myPreference = new MyPreference(label, type, prefGroupItem.Preference_Group__r.DeveloperName, name);
    	}

    	private void setPicklistValues(String values){
    		picklistValues = new Set<String>();
    		for(String value : values.split(',')){
    			picklistValues.add(value.trim());
    		}
    	}

    	public void setMyPreference(My_Preference__c myPreference){
    		this.myPreference.setMyPreference(myPreference);
    	}

        public Boolean isBooleanType(){
            return myPreference.isBooleanType;
        }

        public Boolean isSelected(){
            return myPreference.isSelected;
        }
    }

    public class MyPreference{
    	public Id id;
    	public String name, type, value, groupName, groupItemName;
    	public Boolean isBooleanType, isSelected;

    	public MyPreference(String name, String type, String groupName, String groupItemName){
    		this.name = name; this.type = type; this.groupName = groupName; this.groupItemName = groupItemName;
            isBooleanType = (type.toLowerCase() == 'boolean'); value = ''; isSelected = false;
    	}

    	public void setMyPreference(My_Preference__c myPreference){
    		id = myPreference.Id; value = myPreference.Value__c;
            if(isBooleanType) isSelected = (value == 'true'); 
    	}

    	public My_Preference__c getRecord(){
    		return new My_Preference__c(
    			Id = id, Name = name, Preference_Group__c = groupName, Preference_Group_Item__c = groupItemName,
                User__c = UserInfo.getUserId(), Type__c = type, Value__c = getValue()
    		);
    	} 

        private String getValue(){
            return (isBooleanType ? (isSelected ? 'true' : 'false') : value);
        }
    }
}