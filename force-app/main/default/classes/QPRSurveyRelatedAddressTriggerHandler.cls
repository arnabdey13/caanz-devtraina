public class QPRSurveyRelatedAddressTriggerHandler extends TriggerHandler {
     //Context Variable Collections after Type Casting
    List<QPR_Survey_Related_Address__c> newList = (List<QPR_Survey_Related_Address__c>)Trigger.new;
    List<QPR_Survey_Related_Address__c> oldList = (List<QPR_Survey_Related_Address__c>)Trigger.old;
    Map<Id,QPR_Survey_Related_Address__c> oldMap = (Map<Id,QPR_Survey_Related_Address__c>)Trigger.oldMap;
    Map<Id,QPR_Survey_Related_Address__c> newMap = (Map<Id,QPR_Survey_Related_Address__c>)Trigger.newMap;        
   
    public static Boolean isTrigExec =true;
    //After Insert Trigger 
    public override void afterInsert() {
        
    }
    public override void afterUpdate(){
                           updateQPRSurveyRelatedAddress(newList);
                              }
    public void updateQPRSurveyRelatedAddress(List<QPR_Survey_Related_Address__c> newQPRSurveyRelatedAddressList)
    {
        List<QPR_Survey_Related_Address__c> delRelatedAddress = new List<QPR_Survey_Related_Address__c>();
         List<ID> delRelatedAddressID = new List<ID>();
        if (isTrigExec ==true)
        {
            if(newQPRSurveyRelatedAddressList!=null && !newQPRSurveyRelatedAddressList.isEmpty())
            {
                  for(QPR_Survey_Related_Address__c qprRelatedAddress : newQPRSurveyRelatedAddressList){
                      if(qprRelatedAddress.RemoveFlag__c!=null)
                      {
                          if(qprRelatedAddress.RemoveFlag__c==true)
                		 {delRelatedAddressID.add(qprRelatedAddress.Id);}
                      }
                  }
                if(!delRelatedAddressID.isEmpty())
                {
                    delRelatedAddress= [SELECT Id, Name FROM QPR_Survey_Related_Address__c where Id in :delRelatedAddressID];
                     delete delRelatedAddress;
                }
            isTrigExec =false;
            }
           
        }
    }
     public override void beforeInsert(){}
     public override void beforeUpdate(){}
     public override void afterDelete(){}
     public override void afterUndelete(){}
    
}