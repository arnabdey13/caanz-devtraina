/***********************************************************************************************************************************************************************
Name: Account_LuanaHandler 
============================================================================================================================== 
Purpose: This Class contains the code related to Luana Functionality invoked from the AccountTriggerHandler 
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR                 DATE          DETAIL     Description 
1.0        Rama Krishna    04/02/2019    Created          This Class contains the code related to Luana Functionality invoked from the AccountTriggerHandler 
                                                             
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/
public class Account_LuanaHandler{
    static List<Account> accList;
    static PermissionSet ps;
    static List<User> userList;
    static List<PermissionSetAssignment> permSetAssList;
        // The following method Assigns LUANA_NonMemberCommunityPermissionset to users
        public static void luanaActivateNMUser(List<Account> newAccountList){
        if(accList == null)
            accList = [SELECT Id, IsPersonAccount, Assessible_for_CAF_Program__c, PersonContactId FROM Account WHERE Id IN: newAccountList and Assessible_for_CAF_Program__c=true];
        List<Id> conIdList = new List<Id>();
        //get the Permission set Luana_NonMemberCommunityPermission
        if(ps == null)
            ps = [SELECT Id FROM PermissionSet WHERE Name=:System.Label.Luana_NonMemberCommunityPermission];
        // AccId, ConId
        Map<Id, Id> accConMap = new Map<Id, Id>();
        Map<Id, Id> userPermSetMap = new Map<Id, Id>();
        for(Account acc : accList) {             
            conIdList.add(acc.PersonContactId);
            accConMap.put(acc.PersonContactId, acc.Id);                
        }
        if(userList == null) 
            userList = [SELECT Id, isActive, Username, Email, ContactId FROM User WHERE ContactID IN: conIdList];
        // AccId, User
        Map<Id, User> accUserMap = new Map<Id, User>();
        List<Id> userIdList = new List<Id>();
            
        for(User user : userList) {
            if(accConMap.containsKey(user.ContactId)) {        
                accUserMap.put(accConMap.get(user.ContactId), user);
            }
            userIdList.add(user.Id);   
        }
        if(permSetAssList == null)
            permSetAssList = [SELECT Id, AssigneeId, PermissionSetId FROM PermissionSetAssignment WHERE PermissionSetId =: ps.Id AND AssigneeId IN: userIdList];
        // UserId, PermSetId
        Map<Id, Id> userPermMap = new Map<Id, Id>();
            
        for(PermissionSetAssignment psa : permSetAssList) {
            userPermMap.put(psa.AssigneeId, psa.PermissionSetId);
        }
            
        List<User> userUpdateList = new List<User>();
        List<PermissionSetAssignment> permSetAssInsertList = new List<PermissionSetAssignment>();
            
            
        for(Account acc : newAccountList) {
            if(acc.IsPersonAccount && acc.Assessible_for_CAF_Program__c) {
                // Check if Account has Community User
                if(accUserMap.containsKey(acc.Id)) {
                    userPermSetMap.put(accUserMap.get(acc.Id).Id, ps.Id);
                } else
                    acc.addError(System.Label.Luana_ActivateNMUserError);
            }
        }
        //future call which adds the non member permission set for the Community users
        luana_NMUserAndPermSetUtil.runFutureUpdate(userPermSetMap, userPermMap);                       
      }
    
}