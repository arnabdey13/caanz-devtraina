/*------------------------------------------------------------------------------------
Author:        Mayukhman
Company:       Tech Mahindra
Description:   This Controller will be used for trigger case Assignment rule from quick action
History
Date            Author             Comments
--------------------------------------------------------------------------------------
0707-2019      Mayukhman     Initial Release
------------------------------------------------------------------------------------*/
public with sharing class case_CallAssignmentRuleController {
    
    /*Method to trigger Assignment Rule on Quick Action for MST user */
    @AuraEnabled
    public static boolean callCaseAssignmentRule(string caseId){
        List<Case> cseList = new List<Case>([Select Id,isClosed From Case where Id =: caseId]);
        if(cseList.size() > 0){  
            if(!cseList[0].isClosed){
                Database.DMLOptions dmo = new Database.DMLOptions();
                dmo.assignmentRuleHeader.useDefaultRule= true;
                cseList[0].setOptions(dmo);
                try{
                    update cseList;
                }catch(DmlException e) {
                    throw new AuraHandledException(e.getMessage());
                }catch(Exception e){
                    throw new AuraHandledException(e.getMessage());
                }
                finally {
                }
                return true;
            }
            else{
                return false;
            }
        }
        else
            return false;
    }
    
    @InvocableMethod
    public static void callCaseAssignmentRulePB(List<Case> cseList){
        if(cseList.size() > 0){              
            caseAssignFuture(cseList[0].Id);
        }
    }
    
    @future
    public static void caseAssignFuture(string caseId){
        List<Case> cseList = new List<Case>([Select Id From Case where Id =: caseId]);
        database.DMLOptions dmo = new Database.DMLOptions();
        dmo.assignmentRuleHeader.useDefaultRule = true;
        cseList[0].setOptions(dmo);
        update cseList;
    }
    
}