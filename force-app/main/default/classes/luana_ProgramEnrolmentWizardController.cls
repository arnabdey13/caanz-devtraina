/* 
    Developer: WDCi (Lean)
    Date: 02/Aug/2016
    Task #: LCA-822 Luana implementation - Wizard for Non AU program enrolment
*/

public without sharing class luana_ProgramEnrolmentWizardController {
    
    public Id custCommConId {get; private set;}
    public Id custCommAccId {get; private set;}
    public User custUser {get; private set;}
    
    public String custCommCountry;
    
    public luana_CommUserUtil commUserUtil;
    
    public Map<String, Luana_Extension_Settings__c> luanaConfigs;
    
    public boolean formEnabled {set; get;}
    
    public Integer maxElective {set; get;}
    
    public String selectedProductType {set; get;}
    public String selectedProgramOfferingId {set; get;}
    public String selectedCourseId {set; get;}
    
    public LuanaSMS__Program_Offering__c selectedProgramOffering {set; get;}
    public LuanaSMS__Course__c selectedCourse {set; get;}
    public List<luana_ProgramOfferingSubjectWrapper> selectedProgramOfferingSubjects {set; get;}
    
    public List<LuanaSMS__Program_Offering__c> programOfferings {set; get;}
    public LuanaSMS__Student_Program__c workingStudentProgram {set; get;}
    public List<LuanaSMS__Student_Program_Subject__c> studentAccreditedProgramSubjects {get; set;}
    
    public luana_ProgramEnrolmentWizardController(){
    	
    	init();
    	
    }
    
    public void init(){
    	formEnabled = false;
    	
    	for(User targetMemberUser : [select id from User where ContactId =: getContactIdFromURL() limit 1]){
            commUserUtil = new luana_CommUserUtil(targetMemberUser.Id);
        }
        
        if(commUserUtil != null){
            custUser = commUserUtil.user;
            
            custCommConId = commUserUtil.custCommConId;
            custCommAccId = commUserUtil.custCommAccId;
            custCommCountry = commUserUtil.custCommCountry;
            
            //object init
            programOfferings = new List<LuanaSMS__Program_Offering__c>();
            workingStudentProgram = new LuanaSMS__Student_Program__c();
            studentAccreditedProgramSubjects = new List<LuanaSMS__Student_Program_Subject__c>();
            
            //string init
            selectedProductType = getProductTypeFromURL();            
            
            formEnabled = true;
            
        }
    }
    
    private String getContactIdFromURL(){
        if(ApexPages.currentPage().getParameters().containsKey('contactid')){
            return ApexPages.currentPage().getParameters().get('contactid');
            
        }
        
        return null;
    }
    
    private String getProductTypeFromURL(){
        if(ApexPages.currentPage().getParameters().containsKey('producttype') && getProductTypes().contains(ApexPages.currentPage().getParameters().get('producttype').toLowerCase()) ){
            return ApexPages.currentPage().getParameters().get('producttype').toLowerCase();
            
        }
        
        return null;
    }
    
    private String getRetURLFromURL(){
        if(ApexPages.currentPage().getParameters().containsKey('retURL')){
            return ApexPages.currentPage().getParameters().get('retURL');
            
        }
        
        return null;
    }
    
    public Set<String> getProductTypes(){
        Set<String> options = new Set<String>();
        
        Schema.DescribeFieldResult fieldResult = LuanaSMS__Program_Offering__c.Product_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple) {
            options.add(f.getLabel().toLowerCase());
        }
        
        return options;
    }
    
    public String getTemplateName(){
        //only applicable to internal user for now
        return 'luana_InternalTemplate';
        
    }
    
}