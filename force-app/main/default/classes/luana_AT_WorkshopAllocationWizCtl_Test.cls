/*
    Developer: WDCi (KH)
    Date: 14/July/2016
    Task #: Test class for luana_WorkshopAllocationWizardController
    
    Change History
    LCA-921 23/08/2016 WDCi - KH add person account email
*/
@isTest(seeAllData=false)
private class luana_AT_WorkshopAllocationWizCtl_Test {
    
    final static String contextLongName = 'luana_WorkshopAllocationWizardController_Test';
    final static String contextShortName = 'lwatw';
    private static map<String, Id> commProfIdMap {get; set;}
    
    public static User memberUser {get; set;}
    public static Account memberAccount {get; set;}
    public static luana_CommUserUtil userUtil {get; set;} 
    
    public static LuanaSMS__Course__c course1;
    public static LuanaSMS__Course__c course2;
    public static LuanaSMS__Course__c course3;
    
    public static LuanaSMS__Session__c newSession1;
    public static LuanaSMS__Session__c newSession2;
    public static LuanaSMS__Session__c newSession3;
    
    public static LuanaSMS__Attendance2__c att1;
    public static LuanaSMS__Attendance2__c att2;
    
    public static List<LuanaSMS__Attendance2__c> attendances;
    
    public static LuanaSMS__Student_Program__c newStudProg1;
    public static LuanaSMS__Student_Program__c newStudProg2;
    public static LuanaSMS__Student_Program__c newStudProg3;
    public static LuanaSMS__Student_Program__c newStudProg4;
    
    public static Course_Delivery_Location__c cdl1;    
    public static Course_Delivery_Location__c cdl2;
    public static Course_Delivery_Location__c cdl3;
    
    public static Luana_DataPrep_Test dataPrep {get; set;}
    
    static void initial(){
        dataPrep = new Luana_DataPrep_Test();
        
        //Create all the custom setting
        insert dataPrep.createLuanaConfigurationCustomSetting();
        
        List<Luana_Extension_Settings__c> customSessingList = dataPrep.prepLuanaExtensionSettingCustomSettings();
        customSessingList.add(new Luana_Extension_Settings__c(Name = 'AdobeConnect_URL', Value__c = '  https://connect-staging.charteredaccountantsanz.com'));
        customSessingList.add(new Luana_Extension_Settings__c(Name = 'Workshop Allocation Report Params', Value__c = '/00ON0000000LOze?pv0='));
        customSessingList.add(new Luana_Extension_Settings__c(Name = 'Workshop Allocation Default Series', Value__c = '3'));
        customSessingList.add(new Luana_Extension_Settings__c(Name = 'Workshop Allocation Default Max Set', Value__c = '25'));
        insert customSessingList;
        
        //Create user with Member and Employer community access
        memberAccount = dataPrep.generateNewApplicantAcc('Joe_' + contextShortName, contextLongName, 'Full_Member');
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.Assessible_for_CA_Program__c = true;
        memberAccount.PersonEmail = 'joe_1_' + contextShortName+'@gmail.com';//LCA-921 
        insert memberAccount;
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
    }
    
    static void setup(String runSeqKey){
        //LCA-921
        memberUser = [Select Id, Email, FirstName, LastName, UserName, Name from User Where Email =: 'joe_1_' + contextShortName+'@gmail.com' limit 1];
        //memberUser = dataPrep.generateNewApplicantUser(contextLongName, contextShortName, memberAccount, commProfIdMap.get('Customer Community Login User'));
        //insert memberUser;
        
        userUtil = new luana_CommUserUtil(memberUser.Id);
        
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
        Luana_Extension_Settings__c defContriPermSetId = Luana_Extension_Settings__c.getValues('Default_ContCom_PermSet_Id');
       
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defEmployerPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defContriPermSetId.value__c));
        
        System.runAs(memberUser){
            insert psas;
        }
        
        LuanaSMS__Training_Organisation__c trainingOrg = dataPrep.createNewTraningOrg(contextLongName, contextLongName, contextShortName, 'Virtual Street 123', 'Queensland', '4551');
        insert trainingOrg;
        LuanaSMS__Program__c program = dataPrep.createNewProgram(contextShortName, contextLongName, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        insert program;
        LuanaSMS__Program_Offering__c po = dataPrep.createNewProgOffering('po' + contextShortName, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), program.Id, trainingOrg.Id, null, null, null, 1, 1);
        insert po;
        
        LuanaSMS__Delivery_Location__c dl1 = dataPrep.createNewDeliveryLocationRecord(contextShortName, contextLongName, trainingOrg.Id, '1111', 'Australia');
        insert dl1;
        
        List<LuanaSMS__Course__c> courses = new List<LuanaSMS__Course__c>();
        course1 = dataPrep.createNewCourse('Graduate Diploma of Workshop1 Accounting_' + contextShortName, po.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        course1.LuanaSMS__Allow_Online_Enrolment__c = true;
        course1.LuanaSMS__Delivery_Location__c = dl1.Id;
        courses.add(course1);
        
        course2 = dataPrep.createNewCourse('Graduate Diploma of Workshop2 Accounting_' + contextShortName, po.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        course2.LuanaSMS__Allow_Online_Enrolment__c = true;
        course2.LuanaSMS__Delivery_Location__c = dl1.Id;
        courses.add(course2);
        
        course3 = dataPrep.createNewCourse('Graduate Diploma of Workshop2 Accounting_' + contextShortName, po.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        course3.LuanaSMS__Allow_Online_Enrolment__c = true;
        course3.LuanaSMS__Delivery_Location__c = dl1.Id;
        courses.add(course3);
        
        insert courses;
        
        List<Course_Delivery_Location__c> courseDevLocs = new List<Course_Delivery_Location__c>();
        cdl1 = dataPrep.createCourseDeliveryLocation(course1.Id, dl1.Id, 'Workshop Location');
        cdl1.Availability_Day__c = 'Saturday;Sunday';
        courseDevLocs.add(cdl1);
        
        cdl2 = dataPrep.createCourseDeliveryLocation(course2.Id, dl1.Id, 'Workshop Location');
        cdl2.Availability_Day__c = 'Saturday;Sunday';
        courseDevLocs.add(cdl2);
        
        cdl3 = dataPrep.createCourseDeliveryLocation(course3.Id, dl1.Id, 'Workshop Location');
        cdl3.Availability_Day__c = 'Saturday;Sunday';
        courseDevLocs.add(cdl3);
        insert courseDevLocs;
        
        List<LuanaSMS__Student_Program__c> stuPrograms = new List<LuanaSMS__Student_Program__c>();
        newStudProg1 = dataPrep.createNewStudentProgram(dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), userUtil.custCommConId, course1.Id, '', 'In Progress');
        newStudProg1.Paid__c = true;
        newStudProg1.Workshop_Broadcasted__c = false;
        newStudProg1.Workshop_Location_Preference_1__c = cdl1.Id;
        newStudProg1.Day_of_the_Week_Preference_1__c = 'Saturday';
        newStudProg1.Workshop_Location_Preference_2__c = cdl1.Id;
        newStudProg1.Day_of_the_Week_Preference_2__c = 'Sunday';
        stuPrograms.add(newStudProg1);
        
        newStudProg2 = dataPrep.createNewStudentProgram(dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), userUtil.custCommConId, course2.Id, '', 'In Progress');
        newStudProg2.Paid__c = true;
        newStudProg2.Workshop_Broadcasted__c = false;
        newStudProg2.Workshop_Location_Preference_1__c = cdl2.Id;
        newStudProg2.Day_of_the_Week_Preference_1__c = 'Saturday';
        newStudProg2.Workshop_Location_Preference_2__c = cdl2.Id;
        newStudProg2.Day_of_the_Week_Preference_2__c = 'Sunday';
        stuPrograms.add(newStudProg2);
        
        newStudProg3 = dataPrep.createNewStudentProgram(dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), userUtil.custCommConId, course1.Id, '', 'In Progress');
        newStudProg3.Paid__c = true;
        newStudProg3.Workshop_Broadcasted__c = false;
        newStudProg3.Workshop_Location_Preference_1__c = cdl1.Id;
        newStudProg3.Day_of_the_Week_Preference_1__c = 'Saturday';
        newStudProg3.Workshop_Location_Preference_2__c = cdl1.Id;
        newStudProg3.Day_of_the_Week_Preference_2__c = 'Sunday';
        stuPrograms.add(newStudProg3);
        
        newStudProg4 = dataPrep.createNewStudentProgram(dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), userUtil.custCommConId, course3.Id, '', 'In Progress');
        newStudProg4.Paid__c = true;
        newStudProg4.Workshop_Broadcasted__c = false;
        newStudProg4.Workshop_Location_Preference_1__c = cdl3.Id;
        newStudProg4.Day_of_the_Week_Preference_1__c = 'Saturday';
        newStudProg4.Workshop_Location_Preference_2__c = cdl3.Id;
        newStudProg4.Day_of_the_Week_Preference_2__c = 'Sunday';
        stuPrograms.add(newStudProg4);
        
        insert stuPrograms;
        
        List<Account> venues = new List<Account>();
        Account venueAccount1 = dataPrep.generateNewBusinessAcc(contextShortName+ 'Test Venue_'+runSeqKey+'_1', 'Venue', null, null, null, null, 'Sunshine Coast Office', 'Active');
        venueAccount1.billingCity = 'city';
        venueAccount1.billingPostalCode = '1234';
        venueAccount1.billingCountry = 'Malaysia';
        venueAccount1.Ext_Id__c = contextShortName+'_'+runSeqKey+'_1';
        venues.add(venueAccount1);
        
        Account venueAccount2 = dataPrep.generateNewBusinessAcc(contextShortName+ 'Test Venue_'+runSeqKey+'_2', 'Venue', null, null, null, null, 'Sunshine Coast Office', 'Active');
        venueAccount2.billingCity = 'city';
        venueAccount2.billingPostalCode = '1234';
        venueAccount2.billingCountry = 'Australia';
        venueAccount2.billingState = 'New South Wales';
        venueAccount2.Ext_Id__c = contextShortName+'_'+runSeqKey+'_2';
        venues.add(venueAccount2);
        
        Account venueAccount3 = dataPrep.generateNewBusinessAcc(contextShortName+ 'Test Venue_'+runSeqKey+'_3', 'Venue', null, null, null, null, 'Sunshine Coast Office', 'Active');
        venueAccount3.billingCity = 'city';
        venueAccount3.billingPostalCode = '1234';
        venueAccount3.billingCountry = 'Australia';
        venueAccount3.billingState = 'New South Wales';
        venueAccount3.Ext_Id__c = contextShortName+'_'+runSeqKey+'_3';
        venues.add(venueAccount3);
        insert venues;
        
        List<LuanaSMS__Room__c> rooms = new List<LuanaSMS__Room__c>();
        LuanaSMS__Room__c room1 = dataPrep.newRoom(contextShortName, contextLongName, venueAccount1.Id, 20, 'Classroom', runSeqKey+'_1');
        LuanaSMS__Room__c room2 = dataPrep.newRoom(contextShortName, contextLongName, venueAccount2.Id, 20, 'Classroom', runSeqKey+'_2');
        LuanaSMS__Room__c room3 = dataPrep.newRoom(contextShortName, contextLongName, venueAccount3.Id, 20, 'Classroom', runSeqKey+'_3');
        rooms.add(room1);
        rooms.add(room2);
        rooms.add(room3);
        insert rooms;
        
        List<LuanaSMS__Session__c> sessions = new List<LuanaSMS__Session__c>();
        newSession1 = dataPrep.newSession(contextShortName, contextLongName, 'Workshop', 20, venueAccount1.Id, room1.Id, null, system.now().addHours(1), system.now().addHours(2));
        newSession1.Topic_Key__c = contextShortName+ '_T1';
        newSession1.Course_Delivery_Location__c = cdl2.Id;
        newSession1.Day_of_Week__c = 'Saturday';
        sessions.add(newSession1);
        
        newSession2 = dataPrep.newSession(contextShortName, contextLongName, 'Workshop', 20, venueAccount2.Id, room2.Id, null, system.now().addHours(1), system.now().addHours(2));
        newSession2.Topic_Key__c = contextShortName+ '_T1';
        newSession2.Course_Delivery_Location__c = cdl2.Id;
        newSession2.Day_of_Week__c = 'Saturday';
        sessions.add(newSession2);
        
        newSession3 = dataPrep.newSession(contextShortName, contextLongName, 'Workshop', 20, venueAccount3.Id, room3.Id, null, system.now().addHours(1), system.now().addHours(2));
        newSession3.Topic_Key__c = contextShortName+ '_T1';
        newSession3.Course_Delivery_Location__c = cdl3.Id;
        newSession3.Day_of_Week__c = 'Saturday';
        sessions.add(newSession3);
        
        insert sessions;
        
        attendances = new List<LuanaSMS__Attendance2__c>();
        att1 = dataPrep.createAttendance(userUtil.custCommConId, 'Workshop', newStudProg2.Id, newSession1.Id);
        attendances.add(att1);
        att2 = dataPrep.createAttendance(userUtil.custCommConId, 'Workshop', newStudProg2.Id, newSession2.Id);
        attendances.add(att2);
        insert attendances;
       

        
    }
    
    //WorkshopAllocationWizard - Course- without attendance
    static testMethod void testWSAllocationWizard_CourseWithoutAttedance() {
        
        Test.startTest();
            initial();
            
        Test.stopTest();
        
        setup('1');
        
        //Test.startTest();
            
        PageReference pageRef = Page.luana_WorkshopAllocationWizard;
        pageRef.getParameters().put('id', course1.id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController stdCtl = new ApexPages.StandardController(course1);
        luana_WorkshopAllocationWizardController wsAllocate = new luana_WorkshopAllocationWizardController(stdCtl);
        
        wsAllocate.doSave();
        
        List<LuanaSMS__Attendance2__c> newAtt1 = [Select Id, Name from LuanaSMS__Attendance2__c Where LuanaSMS__Student_Program__c =: newStudProg1.Id];
        System.assertEquals(newAtt1.size(), 3, 'LCA-710: Expect 3 new Attendance created');
        
        List<LuanaSMS__Session__c> newSess1 = [Select Id, Name from LuanaSMS__Session__c Where Course_Delivery_Location__c =: cdl1.Id];
        System.assertEquals(newSess1.size(), 3, 'LCA-710: Expect 3 new Sessions created');
        
        //Test.stopTest();
    }
    
    //WorkshopAllocationWizard Course - without attendance
    static testMethod void testWSAllocationWizard_CourseWithAttedance() {
        
        Test.startTest();
            initial();
            
        Test.stopTest();
        
        
        setup('2');
        
        //Test.startTest();
            
        PageReference pageRef = Page.luana_WorkshopAllocationWizard;
        pageRef.getParameters().put('id', course2.id);
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController stdCtl = new ApexPages.StandardController(course2);
        luana_WorkshopAllocationWizardController wsAllocate = new luana_WorkshopAllocationWizardController(stdCtl);
        
        wsAllocate.doSave();
        
        System.assertEquals(wsAllocate.workshopDeliveryLocations.size(), 0, 'LCA-710: Expect no workshop for allocation');
        
        ApexPages.Message[] pageMessages = ApexPages.getMessages();
        System.assertNotEquals(0, pageMessages.size());
        
        // Check that the error message you are expecting is in pageMessages
        Boolean messageFound = false;
        for(ApexPages.Message message : pageMessages) {
            if(message.getSummary() == 'No outstanding enrolment to be allocated for this course.') {
                messageFound = true;        
            }
        }
        
        System.assertEquals(messageFound, true, 'LCA-710: \'No outstanding enrolment to be allocated for this course.\'');
            
            
        //Test.stopTest();
    }
    
    //WorkshopAllocationWizard Course - without attendance
    static testMethod void testWSAllocationWizard_CDLWithoutAttedance() {
        
        Test.startTest();
            initial();
            
        Test.stopTest();

        setup('3');
        
        //Test.startTest();
            
            PageReference pageRef = Page.luana_WorkshopAllocationWizard;
            pageRef.getParameters().put('cdlid', cdl3.id);
            pageRef.getParameters().put('id', course3.id);
            Test.setCurrentPage(pageRef);
            
            ApexPages.StandardController stdCtl = new ApexPages.StandardController(cdl3);
            luana_WorkshopAllocationWizardController wsAllocate = new luana_WorkshopAllocationWizardController(stdCtl);
            
            wsAllocate.doSave();
            
            List<LuanaSMS__Attendance2__c> newAtt1 = [Select Id, Name from LuanaSMS__Attendance2__c Where LuanaSMS__Student_Program__c =: newStudProg4.Id];
            System.assertEquals(newAtt1.size(), 1, 'LCA-710: Expect 1 new Attendance created');
            
            List<LuanaSMS__Session__c> newSess1 = [Select Id, Name from LuanaSMS__Session__c Where Course_Delivery_Location__c =: cdl3.Id];
            System.assertEquals(newSess1.size(), 1, 'LCA-710: Expect 1 new Sessions created');
            
        //Test.stopTest();
    }    

    //WorkshopAllocationWizard Course - Average Seat > Max
    static testMethod void testWSAllocationWizard_AvgMoreThenMax() {
        
        Test.startTest();
            initial();
            
        Test.stopTest();
        
        setup('4');
        
        //Test.startTest();
            
            PageReference pageRef = Page.luana_WorkshopAllocationWizard;
            //pageRef.getParameters().put('cdlid', cdl1.id);
            pageRef.getParameters().put('id', course1.id);
            Test.setCurrentPage(pageRef);
            
            ApexPages.StandardController stdCtl = new ApexPages.StandardController(course1);
            luana_WorkshopAllocationWizardController wsAllocate = new luana_WorkshopAllocationWizardController(stdCtl);
            
            wsAllocate.workshopDeliveryLocations[0].sessionGroups[0].maxSeat = 25;
            wsAllocate.workshopDeliveryLocations[0].sessionGroups[0].avgSeat = 26;
            
            wsAllocate.doSave();
            
            ApexPages.Message[] pageMessages = ApexPages.getMessages();
            System.assertNotEquals(0, pageMessages.size());
            
            // Check that the error message you are expecting is in pageMessages
            Boolean messageFound = false;
            for(ApexPages.Message message : pageMessages) {
                if(message.getSummary() == 'The \'Avg. Seat\' for new session cannot be more than \'Max. Set\'. Please verify the details again.') {
                    messageFound = true;        
                }
            }
            
            System.assertEquals(messageFound, true, 'LCA-710: Expect warning message \'The \'Avg. Seat\' for new session cannot be more than \'Max. Set\'. Please verify the details again.\'');

        //Test.stopTest();
    }
    
    //WorkshopAllocationWizard Course - Max seat is 0
    static testMethod void testWSAllocationWizard_MaxSeatIsZero() {
        
        Test.startTest();
            initial();
            
        Test.stopTest();

        setup('5');
        
        //Test.startTest();
            
            PageReference pageRef = Page.luana_WorkshopAllocationWizard;
            //pageRef.getParameters().put('cdlid', cdl1.id);
            pageRef.getParameters().put('id', course1.id);
            Test.setCurrentPage(pageRef);
            
            ApexPages.StandardController stdCtl = new ApexPages.StandardController(course1);
            luana_WorkshopAllocationWizardController wsAllocate = new luana_WorkshopAllocationWizardController(stdCtl);
            
            wsAllocate.workshopDeliveryLocations[0].sessionGroups[0].maxSeat = 0;
            
            wsAllocate.doSave();
            
            ApexPages.Message[] pageMessages = ApexPages.getMessages();
            System.assertNotEquals(0, pageMessages.size());
            
            // Check that the error message you are expecting is in pageMessages
            Boolean messageFound = false;
            for(ApexPages.Message message : pageMessages) {
                if(message.getSummary() == 'The \'Max. Seat\' for new session cannot be zero or empty. Please verify the details again.') {
                    messageFound = true;        
                }
            }
            
            System.assertEquals(messageFound, true, 'LCA-710: Expect warning message \'The \'Max. Seat\' for new session cannot be zero or empty. Please verify the details again.\'');

        //Test.stopTest();
    }
}