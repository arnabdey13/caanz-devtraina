/*
    Developer: WDCi (KH)
    Date: 14/July/2016
    Task #: Test class for luana_WorkshopAllocationTrxWizController
    
    Change History
    LCA-921 WDCi - KH 23/08/2016 Add person account email
    
*/
@isTest(seeAllData=false)
private class luana_AT_WorkshopAllocateTrxWizCtl_Test {
    
    final static String contextLongName = 'luana_WorkshopAllocationTrxWizController_Test';
    final static String contextShortName = 'lwat';
    private static map<String, Id> commProfIdMap {get; set;}
    
    public static User memberUser {get; set;}
    public static Account memberAccount {get; set;}
    public static luana_CommUserUtil userUtil {get; set;} 
    
    public static LuanaSMS__Course__c course1;
    public static LuanaSMS__Course__c course2;
    public static LuanaSMS__Course__c course3;
    
    public static LuanaSMS__Session__c newSession1;
    public static LuanaSMS__Session__c newSession2;
    public static LuanaSMS__Session__c newSession3;
    public static LuanaSMS__Session__c newSession4;
    
    public static LuanaSMS__Attendance2__c att1;
    public static LuanaSMS__Attendance2__c att2;
    
    public static List<LuanaSMS__Attendance2__c> attendances;
    
    public static Luana_DataPrep_Test dataPrep;
    
    static void initial(){
    
        //initialize
        dataPrep = new Luana_DataPrep_Test();
        
        //Create all the custom setting
        insert dataPrep.createLuanaConfigurationCustomSetting();
        
        List<Luana_Extension_Settings__c> customSessingList = dataPrep.prepLuanaExtensionSettingCustomSettings();
        
        customSessingList.add(new Luana_Extension_Settings__c(Name = 'AdobeConnect_URL', Value__c = '  https://connect-staging.charteredaccountantsanz.com'));
        insert customSessingList;
        
        //Create user with Member and Employer community access
        memberAccount = dataPrep.generateNewApplicantAcc('Joe_' + contextShortName, contextLongName, 'Full_Member');
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.Assessible_for_CA_Program__c = true;
        memberAccount.PersonEmail = 'joe_1_'+contextShortName+'@gmail.com';//LCA-921
        insert memberAccount;
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
    }
    
    static void setup(String runSeqKey){
       
        memberUser = [Select Id, Email, FirstName, LastName, UserName, Name from User Where Email =: 'joe_1_' + contextShortName+'@gmail.com' limit 1];
        //memberUser = dataPrep.generateNewApplicantUser(contextLongName, contextShortName, memberAccount, commProfIdMap.get('Customer Community Login User'));
        //insert memberUser;
        
        userUtil = new luana_CommUserUtil(memberUser.Id);
        
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
        Luana_Extension_Settings__c defContriPermSetId = Luana_Extension_Settings__c.getValues('Default_ContCom_PermSet_Id');
       
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defEmployerPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defContriPermSetId.value__c));
        
        System.runAs(memberUser){
            insert psas;
        }
        
        //List<Luana_Extension_Settings__c> luanaExtensionConfig = new List<Luana_Extension_Settings__c>();
        //luanaExtensionConfig.add(new Luana_Extension_Settings__c(Name='AdobeConnect_URL', Value__c = 'https://connect-staging.charteredaccountantsanz.com')); 
        //insert luanaExtensionConfig;
        
        LuanaSMS__Training_Organisation__c trainingOrg = dataPrep.createNewTraningOrg(contextLongName, contextLongName, contextShortName, 'Virtual Street 123', 'Queensland', '4551');
        insert trainingOrg;
        LuanaSMS__Program__c program = dataPrep.createNewProgram(contextShortName, contextLongName, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        insert program;
        LuanaSMS__Program_Offering__c po = dataPrep.createNewProgOffering('po' + contextShortName, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), program.Id, trainingOrg.Id, null, null, null, 1, 1);
        insert po;
        
        List<LuanaSMS__Course__c> courses = new List<LuanaSMS__Course__c>();
        course1 = dataPrep.createNewCourse('Graduate Diploma of Workshop1 Accounting_' + contextShortName, po.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Program'), 'Running');
        course1.LuanaSMS__Allow_Online_Enrolment__c = true;
        courses.add(course1);
        
        course2 = dataPrep.createNewCourse('Graduate Diploma of Workshop2 Accounting_' + contextShortName, po.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Program'), 'Running');
        course2.LuanaSMS__Allow_Online_Enrolment__c = true;
        courses.add(course2);
        
        insert courses;
        
        List<LuanaSMS__Student_Program__c> stuPrograms = new List<LuanaSMS__Student_Program__c>();
        LuanaSMS__Student_Program__c newStudProg1 = dataPrep.createNewStudentProgram(dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), userUtil.custCommConId, course1.Id, '', 'In Progress');
        newStudProg1.Paid__c = true;
        newStudProg1.Workshop_Broadcasted__c = false;
        stuPrograms.add(newStudProg1);
        
        LuanaSMS__Student_Program__c newStudProg2 = dataPrep.createNewStudentProgram(dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), userUtil.custCommConId, course2.Id, '', 'In Progress');
        newStudProg2.Paid__c = true;
        newStudProg2.Workshop_Broadcasted__c = false;
        stuPrograms.add(newStudProg2);
        
        insert stuPrograms;
        
        List<Account> venues = new List<Account>();
        Account venueAccount1 = dataPrep.generateNewBusinessAcc(contextShortName+ 'Test Venue_'+runSeqKey+'_1', 'Venue', null, null, null, null, 'Sunshine Coast Office', 'Active');
        venueAccount1.billingCity = 'city';
        venueAccount1.billingPostalCode = '1234';
        venueAccount1.billingCountry = 'Malaysia';
        venueAccount1.Ext_Id__c = contextShortName+'_'+runSeqKey+'_1';
        venues.add(venueAccount1);
        
        Account venueAccount2 = dataPrep.generateNewBusinessAcc(contextShortName+ 'Test Venue_'+runSeqKey+'_2', 'Venue', null, null, null, null, 'Sunshine Coast Office', 'Active');
        venueAccount2.billingCity = 'city';
        venueAccount2.billingPostalCode = '1234';
        venueAccount2.billingCountry = 'Australia';
        venueAccount2.billingState = 'New South Wales';
        venueAccount2.Ext_Id__c = contextShortName+'_'+runSeqKey+'_2';
        venues.add(venueAccount2);
        
        Account venueAccount3 = dataPrep.generateNewBusinessAcc(contextShortName+ 'Test Venue_'+runSeqKey+'_3', 'Venue', null, null, null, null, 'Sunshine Coast Office', 'Active');
        venueAccount3.billingCity = 'city';
        venueAccount3.billingPostalCode = '1234';
        venueAccount3.billingCountry = 'Australia';
        venueAccount3.billingState = 'New South Wales';
        venueAccount3.Ext_Id__c = contextShortName+'_'+runSeqKey+'_3';
        venues.add(venueAccount3);
        
        Account venueAccount4 = dataPrep.generateNewBusinessAcc(contextShortName+ 'Test Venue_'+runSeqKey+'_4', 'Venue', null, null, null, null, 'Sunshine Coast Office', 'Active');
        venueAccount4.billingCity = 'city';
        venueAccount4.billingPostalCode = '1234';
        venueAccount4.billingCountry = 'Australia';
        venueAccount4.billingState = 'New South Wales';
        venueAccount4.Ext_Id__c = contextShortName+'_'+runSeqKey+'_4';
        venues.add(venueAccount4);
        insert venues;
        
        List<LuanaSMS__Room__c> rooms = new List<LuanaSMS__Room__c>();
        LuanaSMS__Room__c room1 = dataPrep.newRoom(contextShortName, contextLongName+'_1', venueAccount1.Id, 20, 'Classroom', runSeqKey+'_1');
        LuanaSMS__Room__c room2 = dataPrep.newRoom(contextShortName, contextLongName+'_2', venueAccount2.Id, 20, 'Classroom', runSeqKey+'_2');
        LuanaSMS__Room__c room3 = dataPrep.newRoom(contextShortName, contextLongName+'_3', venueAccount3.Id, 20, 'Classroom', runSeqKey+'_3');
        LuanaSMS__Room__c room4 = dataPrep.newRoom(contextShortName, contextLongName+'_4', venueAccount4.Id, 20, 'Classroom', runSeqKey+'_4');
        rooms.add(room1);
        rooms.add(room2);
        rooms.add(room3);
        rooms.add(room4);
        insert rooms;
        
        List<LuanaSMS__Session__c> sessions = new List<LuanaSMS__Session__c>();
        newSession1 = dataPrep.newSession(contextShortName, contextLongName, 'Workshop', 20, venueAccount1.Id, room1.Id, null, system.now().addHours(1), system.now().addHours(2));
        newSession1.Topic_Key__c = contextShortName+ '_T1';
        sessions.add(newSession1);
        
        newSession2 = dataPrep.newSession(contextShortName, contextLongName, 'Workshop', 20, venueAccount2.Id, room2.Id, null, system.now().addHours(1), system.now().addHours(2));
        newSession2.Topic_Key__c = contextShortName+ '_T1';
        sessions.add(newSession2);
        
        newSession3 = dataPrep.newSession(contextShortName, contextLongName, 'Workshop', 20, venueAccount3.Id, room3.Id, null, system.now().addHours(3), system.now().addHours(4));
        newSession3.Topic_Key__c = contextShortName+ '_T1';
        newSession3.Maximum_Capacity__c = 10;
        sessions.add(newSession3);
        
        newSession4 = dataPrep.newSession(contextShortName, contextLongName, 'Workshop', 20, venueAccount3.Id, room3.Id, null, system.now().addHours(5), system.now().addHours(6));
        newSession4.Topic_Key__c = contextShortName+ '_T1';
        newSession4.Maximum_Capacity__c = 1;
        sessions.add(newSession4);
        
        insert sessions;
        
        attendances = new List<LuanaSMS__Attendance2__c>();
        att1 = dataPrep.createAttendance(userUtil.custCommConId, 'Workshop', newStudProg2.Id, newSession1.Id);
        attendances.add(att1);
        att2 = dataPrep.createAttendance(userUtil.custCommConId, 'Workshop', newStudProg2.Id, newSession2.Id);
        attendances.add(att2);
        insert attendances;
        
    }
    
    /*
        Broadcast workshop fail - without attendance
    */
    static testMethod void testWorkshopAllocationWizard() {
        
        Test.startTest();
            initial();
        Test.stopTest();
        
        setup('1');
        
        //Main Test Area
        //Test.startTest();
            
        ApexPages.StandardSetController stdCtl = new ApexPages.StandardSetController(attendances);
        stdCtl.setSelected(attendances);
        luana_WorkshopAllocationTrxWizController wsAllocate = new luana_WorkshopAllocationTrxWizController(stdCtl);
        
        List<LuanaSMS__Attendance2__c> selectedAtts = wsAllocate.selectedAttendances;
        
        for(LuanaSMS__Attendance2__c updateAtt: selectedAtts){
            updateAtt.LuanaSMS__Session__c = newSession3.Id;
        }
        
        wsAllocate.doSave();
        
        for(LuanaSMS__Attendance2__c newAtt: wsAllocate.selectedAttendances){
            System.assertEquals(newAtt.LuanaSMS__Session__c, newSession3.Id, 'Expect attendance session is updated to session3');
        }
        //Test.stopTest();
    }
    
    /*
        Broadcast workshop fail - exit num of student
    */
    static testMethod void testTransferWithExitNumOfStud() {
        Test.startTest();
            initial();
        Test.stopTest();
        
        setup('2');
        
        //Main Test Area
        //Test.startTest();
            
            ApexPages.StandardSetController stdCtl = new ApexPages.StandardSetController(attendances);
            stdCtl.setSelected(attendances);
            luana_WorkshopAllocationTrxWizController wsAllocate = new luana_WorkshopAllocationTrxWizController(stdCtl);
            
            List<LuanaSMS__Attendance2__c> selectedAtts = wsAllocate.selectedAttendances;
            
            for(LuanaSMS__Attendance2__c updateAtt: selectedAtts){
                updateAtt.LuanaSMS__Session__c = newSession4.Id;
            }
            
            wsAllocate.doSave();
            List<Apexpages.Message> msgs = ApexPages.getMessages();
            boolean msg1 = false;
            for(Apexpages.Message msg:msgs){
                if (msg.getDetail().contains('Please click \'Save & ignore\' if you still would like to proceed.')){
                    msg1 = true;
                }
            }
            System.assertEquals(msg1, true, 'Expect error message  \'Please click \'Save & ignore\' if you still would like to proceed.\' show');
            
            wsAllocate.doSaveAndIgnore();
            for(LuanaSMS__Attendance2__c newAtt: wsAllocate.selectedAttendances){
                System.assertEquals(newAtt.LuanaSMS__Session__c, newSession4.Id, 'Expect attendance session is updated to session4');
            }
        //Test.stopTest();
    }
    
}