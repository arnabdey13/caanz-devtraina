public with sharing class QuestionnaireOverride { 
    // Global variables
    public String strDebug {get;set;}
    public Boolean bShowHeader {get;set;}       
    public static String endpoint = 'https://charteredaccountantsanz.tfaforms.net/rest';    
    public Boolean bIsPortalUser {get;set;}
    public Boolean bIsView {get;set;}
    public String strURL {get;set;}
    public Questionnaire_Response__c questionnaire {get;set;}
    public User currentUser {get;set;}
    public Contact currentUserContact {get;set;}
    public String strQuestionnaireIDParam {get;set;} // Application ID parameter
    public String resBody {get;set;} // form body
    // Internal variables
    private String strApplicantId = '';
    private String strMemberOf = '';
    private String strFormId = '';
    private String strParameters = '';
    private String strContext = '';
    private Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
    // Constructor
    public QuestionnaireOverride(ApexPages.standardController con) {          
        Map<String, String> mapPageParameters = ApexPages.currentPage().getParameters();                  
        if(con != null) questionnaire = (Questionnaire_Response__c) con.getRecord(); 
        if(con.getId() != null) {
            questionnaire = [select Id, Lock__c, Year__c, Account__c from Questionnaire_Response__c where Id = :con.getId() limit 1];
        }   
        // Determine context: NEW / EDIT / VIEW / LOCKED
        String currentRequestURL = URL.getCurrentRequestUrl().toExternalForm();
        bIsView = true; // Default to VIEW
        strContext = 'VIEW';    
        if(questionnaire.Id == null) { // NEW
            bIsView = false;    
            strContext = 'NEW';       
        }
        else if(questionnaire.Id != null && questionnaire.Lock__c) { // LOCKED
            bIsView = true;
            strContext = 'LOCKED';
        }
        else if((questionnaire.Id != null && currentRequestURL.indexOf('/e') != -1 && !questionnaire.Lock__c) ||
                (currentRequestURL.indexOf('mode=edit') != -1) ) { // EDIT
            bIsView = false; 
            strContext = 'EDIT';
        }
        // Determine if page is viewed from COMMUNITIES PORTAL OR ANONYMOUSLY
        bIsPortalUser = IsCommunityProfileUser();
        bShowHeader = (bIsPortalUser)? true:false;
                
        // addPageMessage('bIsView: ' + bIsView);   
        system.debug('bIsView: ' + bIsView);                                                
    }
    // Application EDIT override
    public PageReference modeEdit() {
        PageReference pref = Page.QuestionnaireOverride;
        pref.getParameters().put('id', questionnaire.Id);
        pref.getParameters().put('mode', 'edit');
        if(!bIsPortalUser) pref.getParameters().put('nooverride', '1');
        if(ApexPages.currentPage().getParameters().get('retURL') != null) 
            pref.getParameters().put('retURL', ApexPages.currentPage().getParameters().get('retURL'));
        pref.setRedirect(true);
        return pref;
    }
    // Generate Form Assembly URL
    public PageReference generateFormURL() {
        PageReference pref = null;
        Map<String, String> mapPageParameters = ApexPages.currentPage().getParameters();  
        system.debug('bIsPortalUser: ' + bIsPortalUser);        
        currentUser = [select Id, ContactId, AccountId from User where Id = :UserInfo.getUserId() limit 1];
        if(currentUser.ContactId != null) 
            currentUserContact = [select Id, AccountId, Account.Member_Of__c from Contact where Id = :currentUser.ContactId limit 1];  
        if(currentUser.AccountId != null && bIsPortalUser == true)  {
            String strYear = String.valueOf(system.today().year());
            String strQuestionnaireName = '';
            if(mapPageParameters != null && mapPageParameters.containsKey('Year')) strYear = mapPageParameters.get('Year'); 
            if(mapPageParameters != null && mapPageParameters.containsKey('Name')) strQuestionnaireName = mapPageParameters.get('Name'); 
            for(Questionnaire_Response__c q:[select Id, Account__c, Year__c, Lock__c from Questionnaire_Response__c 
                where Account__c = :currentUser.AccountId and Name like :strQuestionnaireName and Year__c = :strYear]) {
                questionnaire = q;
                if(q.Lock__c) { // LOCKED
                    bIsView = true;
                    strContext = 'LOCKED';       
                }        
                break;
            }  
            
        }
        // INTERNAL USER
        if(!bIsPortalUser && UserInfo.getUserType() != 'Guest') {       
            if(strContext == 'NEW') {   // NEW          
                String strPrefix = globalDescribe.get('Questionnaire_Response__c').getDescribe().getkeyprefix();
                pref = new PageReference('/' + strPrefix + '/e');                 
            }                   
            if(strContext == 'EDIT' || (strContext == 'LOCKED' && ApexPages.currentPage().getParameters().get('mode') == 'edit'))  // EDIT
                pref = new PageReference('/' + questionnaire.Id + '/e');
            if(strContext == 'VIEW'|| (strContext == 'LOCKED' && ApexPages.currentPage().getParameters().get('mode') != 'edit')) // VIEW
                pref = new PageReference('/' + questionnaire.Id); 
            Set<String> setExcludedParams = new Set<String> {'sfdc.override', 'save_new', 'nooverride'};                
            for(String s:ApexPages.currentPage().getParameters().keySet()) {
                String val = ApexPages.currentPage().getParameters().get(s);
                if(s != null && val != null && !setExcludedParams.contains(s)) {    
                    if(pref != null && pref.getParameters() != null) pref.getParameters().put(s, val);
                }
            }           
            if(pref != null && pref.getParameters() != null) 
                pref.getParameters().put('nooverride', '1');
            return pref;
        }   
        // Read configuration settings
        Map<String, Application_Form__c> mapApplicationFormSettings = Application_Form__c.getAll();
        if(mapApplicationFormSettings.size() == 0) {
            addErrorMessage('Custom settings not defined');
        }       
        if(currentUserContact != null && currentUserContact.AccountId != null && UserInfo.getUserType() != 'Guest') 
             strApplicantId = currentUserContact.AccountId; // Applicant Id
        if(UserInfo.getUserType() == 'Guest') {  
            mapPageParameters = ApexPages.currentPage().getParameters();        
            strApplicantId = (mapPageParameters.get('Account') != null)?
                (String) mapPageParameters.get('Account'):null; // Applicant Id
        }
        if(currentUserContact != null && currentUserContact.AccountId != null && currentUserContact.Account.Member_Of__c != null) 
            strMemberOf = currentUserContact.Account.Member_Of__c;  // Member Of
         

        for(String s:mapApplicationFormSettings.keySet()) {
            Application_Form__c setting = mapApplicationFormSettings.get(s);
            if(setting.ReadOnly__c == bIsView && setting.Name.startsWith('Questionnaire')) { // TODO:
                strFormId = setting.Form_ID__c; // FORM ID
                // addPageMessage('strFormId: ' + strFormId);   
                system.debug('### strFormId: ' + strFormId);    
                break;
            }
        }
        // Build Questionnaire Query
        if(questionnaire.Id != null) {
            String strQuery = 'select Id, Lock__c'; 
            strQuery += ' from Questionnaire_Response__c where Id = \'' + String.escapeSingleQuotes(questionnaire.Id) + '\' limit 1';
            system.debug('### strQuery: ' + strQuery);
            // addPageMessage(strQuery);
            questionnaire = Database.query(strQuery);
        }
        // Build parameters 
        strParameters = 'SessionId=' + UserInfo.getSessionId(); // Session ID
        strParameters += '&r=' + String.valueOf(Math.random()); // randomizer
        strParameters += '&IsPortal=' + String.valueOf(bIsPortalUser); // portal user
        if(strApplicantId != '') strParameters += '&Account=' + strApplicantId; // Applicant    
        if(strMemberOf != '') strParameters += '&MemberOf=' + strMemberOf;  // Member Of                   
        if(questionnaire.Id != null) {
            strParameters += '&Questionnaire_Response__c=' + questionnaire.Id; // Application ID             
        }
        system.debug('strFormId: ' + strFormId);    
        system.debug('strParameters: ' + strParameters);  
        // addPageMessage(strParameters);   
        // Build PageReference
        if(strFormId != '') {
            callFormAssembly(strFormId, strParameters);     
        }
        else {
            addErrorMessage('Questionnaire form not found. Please contact your administrator.');
            system.debug('###Error: Formassembly form not found. Check custom settings.');
        }
        return null;
    }
    // Determine if page is viewed from portal
    public static Boolean IsCommunityProfileUser() {
        Boolean bReturnValue = false;
        // addPageMessage('User Type: ' + UserInfo.getUserType());
        if(UserInfo.getUserType() == 'CspLitePortal') 
            bReturnValue = true;
        /*
        String strCurrentProfileId = UserInfo.getProfileId();
        for(Profile p:[select Id, Name from Profile where UserLicense.LicenseDefinitionKey 
                        like '%Customer_Community%' limit :(Limits.getLimitQueryRows() - Limits.getQueryRows())]) {
            if(p.Id == strCurrentProfileId) {
                bReturnValue = true; 
                break;
            }
        }
        */  
        return bReturnValue;    
    }
    // Execute web service call 
    public void callFormAssembly(String strFormID, String strRequestParameters) {
        HTTPResponse response;      
        String strURL;
        PageReference pref = ApexPages.currentPage();
        HttpRequest request = new HttpRequest();
        request.setMethod('GET');
        request.setTimeout(60000); // timeout
        // Build URL
        if(pref.getParameters().get('tfa_next') == null) {
            strURL = endpoint + '/forms/view/' + strFormID;
        }
        else {
            strURL = endpoint + pref.getParameters().get('tfa_next');
        }
        // Add parameters
        if(strRequestParameters != null && strRequestParameters.length()>0) {
            strURL = strURL + '?' + strRequestParameters;
        } 
        system.debug('strURL: ' + strURL);  
        // addPageMessage('strURL: ' + strURL); 
        request.setEndpoint(strURL);
        Http http = new Http();
        try {   
            if(!Test.isRunningTest()) {        
                response = http.send(request); // Execute web service call 
                resBody = response.getBody();
            }
        } 
        catch(System.CalloutException e) {
            //addPageMessage(e);
            system.debug('CalloutException: ' + e);
            addErrorMessage('ERROR:  ' + e);
        }               
    }   
    // Page Messages
    public void addPageMessage(ApexPages.severity severity, Object objMessage) {
        ApexPages.Message pMessage = new ApexPages.Message(severity, String.valueOf(objMessage));
        ApexPages.addMessage(pMessage);
    }
    public void addPageMessage(Object objMessage) {
        addPageMessage(ApexPages.severity.INFO, objMessage);
    }       
    public void addErrorMessage(Object objMessage) {
        addPageMessage(ApexPages.severity.ERROR, objMessage);
    }
}