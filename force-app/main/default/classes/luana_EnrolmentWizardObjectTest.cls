/**
    Developer: WDCi (Lean)
    Development Date: 11/04/2016
    Task #: Test class for New Member Enrollment Wizard
**/

@isTest
private class luana_EnrolmentWizardObjectTest {

    static testMethod void testWizardObject() {
        
        Luana_DataPrep_Test testDataGenerator = new Luana_DataPrep_Test();
        List<Luana_Extension_Settings__c> extensionSettings = testDataGenerator.prepLuanaExtensionSettingCustomSettings();
        
        insert extensionSettings;
        
        PageReference landingPage = Page.luana_EnrolmentWizardLanding;
		landingPage.getParameters().put('producttype', 'foundation');
		Test.setCurrentPage(landingPage);
			
        luana_EnrolmentWizardObject.getController(new luana_EnrolmentWizardController(), Page.luana_EnrolmentWizardLanding);
        luana_EnrolmentWizardObject.getController(new luana_EnrolmentWizardController(), Page.luana_EnrolmentWizardProgram);
        luana_EnrolmentWizardObject.getController(new luana_EnrolmentWizardController(), Page.luana_EnrolmentWizardCourse);
        luana_EnrolmentWizardObject.getController(new luana_EnrolmentWizardController(), Page.luana_EnrolmentWizardSubject);
        luana_EnrolmentWizardObject.getController(new luana_EnrolmentWizardController(), Page.luana_EnrolmentWizardDetailsDefault);
        luana_EnrolmentWizardObject.getController(new luana_EnrolmentWizardController(), Page.luana_EnrolmentWizardDetailsCAProgram);
        luana_EnrolmentWizardObject.getController(new luana_EnrolmentWizardController(), Page.luana_EnrolmentWizardDetailsCAModule);
        luana_EnrolmentWizardObject.getController(new luana_EnrolmentWizardController(), Page.luana_EnrolmentWizardDetailsMasterclass);
        luana_EnrolmentWizardObject.getController(new luana_EnrolmentWizardController(), Page.luana_EnrolmentWizardDetailsPPP);
        luana_EnrolmentWizardObject.getController(new luana_EnrolmentWizardController(), Page.luana_EnrolmentWizardDetailsFoundation);
        luana_EnrolmentWizardObject.getController(new luana_EnrolmentWizardController(), Page.luana_EnrolmentWizardDetailsNonAcc);
        
    }
}