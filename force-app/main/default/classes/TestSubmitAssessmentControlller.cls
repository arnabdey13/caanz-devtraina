@isTest(seeAllData=true)
public class TestSubmitAssessmentControlller {
    
    
    static Account acc;
    static Account acc2;
    static Application__c app;
    static Application__c app2;
    static Product2 prod;
    static Product2 prod2;
    static Pricebook2 priceb2;
    static PricebookEntry priceEntry;
    static PricebookEntry priceEntry1;
    static PricebookEntry priceEntry2;
    static PricebookEntry priceEntry3;
    static Application_Form__c ap;
    static Application_Form__c ap2;
    static Order ord;
    static Order ord2;
    static OrderItem ordPd;
    static OrderItem ordPd1;
    static OrderItem ordPd2;
    static PageReference pref;
    
        
    private static void init() {  
         Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true limit 1];
         
         acc=new Account();
         acc.FirstName='Test';
         acc.LastName='Test';
         acc.Preferred_Name__c='Test';
         acc.Member_Of__c='NZICA';
         acc.Current_citizen_residency_status__c='Other';
         acc.Salutation = 'Mr.';
         acc.PersonEmail = 'testAbc@gmail.com';
        
        acc.Communication_Preference__c= 'Home Phone';
        acc.PersonHomePhone= '416091495';
        acc.PersonOtherStreet= '83 Saggers Road';
        acc.PersonOtherCity='JITARNING';
        acc.PersonOtherState='Western Australia';
        acc.PersonOtherCountry='Australia';
        acc.PersonOtherPostalCode='2150';  
        acc.PersonMailingStreet= '83 Saggers Road';
        acc.PersonMailingCity='JITARNING';
        acc.PersonMailingCountry='Australia';
        acc.PersonMailingPostalCode='2150';  
         
        acc.Member_ID__c = '111';
         acc.Membership_Type__c = 'Non Member';
         
         insert acc;
         
         app=new Application__c();
         app.Account__c=acc.Id;
         app.Application_Status__c = 'Test Assessment';
         app.College__c = 'Accounting Technicians';
         app.Pathway__c = 'Academic Pathway';
         app.RecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Provisional Member').getRecordTypeId();
         insert app;
         
         prod=new Product2();
         prod.Name='Test Product';
         prod.Record_Type__c='Provisional_Member';
         prod.Criteria_1__c='Accounting Technicians';
         prod.Criteria_2__c='Academic Pathway';
         prod.Member_Of__c ='NZICA';
         insert prod;
         
         priceb2 =new Pricebook2();
         priceb2.Name='Test Procebook2 Name';
         insert priceb2;
         
         priceEntry1=new PricebookEntry();
         priceEntry1.Pricebook2Id=standardPB.Id;
         priceEntry1.Product2Id=prod.Id;
         priceEntry1.UnitPrice=0;
         insert priceEntry1;
         
         priceEntry=new PricebookEntry();
         priceEntry.Pricebook2Id=priceb2.Id;
         priceEntry.Product2Id=prod.Id;
         priceEntry.UnitPrice=100;
         insert priceEntry;
                  
         ap2=new Application_Form__c();
         ap2.Name='Test Name';
         ap2.Record_Type__c=prod.Record_Type__c;
         ap2.Criterias__c='College__c,Pathway__c';
         ap2.Form_ID__c='6';
         insert ap2;
         
         ord=new Order();
         ord.AccountId=acc.Id;
         ord.Pricebook2Id=standardPB.Id;
         ord.EffectiveDate=Date.Today();
         ord.Status='Draft';
         ord.Application__c=app.Id;
         insert ord;
         
         ordPd = new OrderItem();
         ordPd.PriceBookEntryId=priceEntry1.Id; 
         ordPd.OrderId=ord.Id;
         ordPd.Quantity=1;
         ordPd.UnitPrice=99;
         insert ordPd;
	
         ordPd1 = new OrderItem();
         ordPd1.PriceBookEntryId=priceEntry1.Id; 
         ordPd1.OrderId=ord.Id;
         ordPd1.Quantity=1;
         ordPd1.UnitPrice=0;
         insert ordPd1;
        
         ord.Status='Activated';
         update ord;
    }

    private static void initPASA() {  
        
         Pricebook2 standardPB1 = [select id from Pricebook2 where isStandard=true limit 1];
              
         acc2=new Account();
         acc2.FirstName='Test';
         acc2.LastName='Test';
         acc2.Preferred_Name__c='Test';
         //acc2.Member_Of__c='NZICA';
         acc2.Current_citizen_residency_status__c='Other';
         acc2.Salutation = 'Mr.';
         acc2.PersonEmail = 'test@test2.com';
        acc2.Communication_Preference__c= 'Home Phone';
        
        acc2.PersonHomePhone= '416091495';
        acc2.PersonOtherStreet= '83 Saggers Road';
        acc2.PersonOtherCity='JITARNING';
        acc2.PersonOtherState='Western Australia';
        acc2.PersonOtherCountry='Australia';
        acc2.PersonOtherPostalCode='2150';  
        acc2.PersonMailingStreet= '83 Saggers Road';
        acc2.PersonMailingCity='JITARNING';
        acc2.PersonMailingCountry='Australia';
        acc2.PersonMailingPostalCode='2150';  
        
     //    acc2.Member_ID__c = '111';
        acc2.Membership_Type__c = 'Non Member';
         acc2.Is_Migration_Agent__c = true;
         
       
         insert acc2;
         
         app2=new Application__c();
         app2.Account__c=acc2.Id;
         app2.Application_Status__c = 'Draft';
         app2.Submitted_by_Migration_Agent__c = true;
         //app2.College__c = 'Accounting Technicians';
         //app2.Pathway__c = 'Academic Pathway';
         app2.RecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('PASA Application (Migration Assessment)').getRecordTypeId();
         insert app2;
         
         prod2=new Product2();
         prod2.Name='Test Product';
         prod2.Record_Type__c='PASA_Application';
         prod2.Criteria_1__c='  PASA Assessment Only';
         prod2.Criteria_2__c='Standard';
         prod2.Member_Of__c ='ICAA';
         insert prod2;
         
         priceb2 =new Pricebook2();
         priceb2.Name='Test Procebook2 Name';
         insert priceb2;
         
         priceEntry2=new PricebookEntry();
         priceEntry2.Pricebook2Id=standardPB1.Id;
         priceEntry2.Product2Id=prod2.Id;
         priceEntry2.UnitPrice=100;
         insert priceEntry2;
         
         priceEntry3=new PricebookEntry();
         priceEntry3.Pricebook2Id=priceb2.Id;
         priceEntry3.Product2Id=prod2.Id;
         priceEntry3.UnitPrice=100;
         insert priceEntry3;
                  
         ap2=new Application_Form__c();
         ap2.Name='Test Name';
         ap2.Record_Type__c=prod2.Record_Type__c;
         ap2.Criterias__c='College__c,Pathway__c';
         ap2.Form_ID__c='6';
         insert ap2;
         
         ord2=new Order();
         ord2.AccountId=acc2.Id;
         ord2.Pricebook2Id=standardPB1.Id;
         ord2.EffectiveDate=Date.Today();
         ord2.Status='Draft';
         ord2.Application__c=app2.Id;
         insert ord2;
         
         ordPd2 = new OrderItem();
         ordPd2.PriceBookEntryId=priceEntry2.Id; 
         ordPd2.OrderId=ord2.Id;
         ordPd2.Quantity=1;
         ordPd2.UnitPrice=99;
         insert ordPd2;

         ord2.Status='Activated';
         update ord2;
    }
    
    
    static testMethod void testParse() {    
    
        String json='{'+
        '   "relatedLists": ['+
        '    {'+
        '      "accessLevelRequiredForCreate": null,'+
        '      "buttons": ['+
        '        {'+
        '          "behavior": null,'+
        '          "colors": ['+
        '            {'+
        '              "color": "4BC076",'+
        '              "context": "primary",'+
        '              "theme": "theme4"'+
        '            }'+
        '          ],'+
        '          "content": null,'+
        '          "contentSource": null,'+
        '          "custom": false,'+
        '          "encoding": null,'+
        '          "height": null,'+
        '          "icons": ['+
        '            {'+
        '              "contentType": "image/svg+xml",'+
        '              "height": 0,'+
        '              "theme": "theme4",'+
        '              "url": "https://cs5.salesforce.com/img/icon/t4v32/action/new_task.svg",'+
        '              "width": 0'+
        '            },'+
        '            {'+
        '              "contentType": "image/png",'+
        '              "height": 60,'+
        '              "theme": "theme4",'+
        '              "url": "https://cs5.salesforce.com/img/icon/t4v32/action/new_task_60.png",'+
        '              "width": 60'+
        '            },'+
        '            {'+
        '              "contentType": "image/png",'+
        '              "height": 120,'+
        '              "theme": "theme4",'+
        '              "url": "https://cs5.salesforce.com/img/icon/t4v32/action/new_task_120.png",'+
        '              "width": 120'+
        '            }'+
        '          ],'+
        '          "label": "New Task",'+
        '          "menubar": false,'+
        '          "name": "NewTask",'+
        '          "overridden": false,'+
        '          "resizeable": false,'+
        '          "scrollbars": false,'+
        '          "showsLocation": false,'+
        '          "showsStatus": false,'+
        '          "toolbar": false,'+
        '          "url": null,'+
        '          "width": null,'+
        '          "windowPosition": null'+
        '        },'+
        '        {'+
        '          "behavior": null,'+
        '          "colors": ['+
        '            {'+
        '              "color": "EB7092",'+
        '              "context": "primary",'+
        '              "theme": "theme4"'+
        '            }'+
        '          ],'+
        '          "content": null,'+
        '          "contentSource": null,'+
        '          "custom": false,'+
        '          "encoding": null,'+
        '          "height": null,'+
        '          "icons": ['+
        '            {'+
        '              "contentType": "image/svg+xml",'+
        '              "height": 0,'+
        '              "theme": "theme4",'+
        '              "url": "https://cs5.salesforce.com/img/icon/t4v32/action/new_event.svg",'+
        '              "width": 0'+
        '            },'+
        '            {'+
        '              "contentType": "image/png",'+
        '              "height": 60,'+
        '              "theme": "theme4",'+
        '              "url": "https://cs5.salesforce.com/img/icon/t4v32/action/new_event_60.png",'+
        '              "width": 60'+
        '            },'+
        '            {'+
        '              "contentType": "image/png",'+
        '              "height": 120,'+
        '              "theme": "theme4",'+
        '              "url": "https://cs5.salesforce.com/img/icon/t4v32/action/new_event_120.png",'+
        '              "width": 120'+
        '            }'+
        '          ],'+
        '          "label": "New Event",'+
        '          "menubar": false,'+
        '          "name": "NewEvent",'+
        '          "overridden": false,'+
        '          "resizeable": false,'+
        '          "scrollbars": false,'+
        '          "showsLocation": false,'+
        '          "showsStatus": false,'+
        '          "toolbar": false,'+
        '          "url": null,'+
        '          "width": null,'+
        '          "windowPosition": null'+
        '        },'+
        '        {'+
        '          "behavior": null,'+
        '          "colors": null,'+
        '          "content": null,'+
        '          "contentSource": null,'+
        '          "custom": false,'+
        '          "encoding": null,'+
        '          "height": null,'+
        '          "icons": null,'+
        '          "label": "New Meeting Request",'+
        '          "menubar": false,'+
        '          "name": "NewProposeMeeting",'+
        '          "overridden": false,'+
        '          "resizeable": false,'+
        '          "scrollbars": false,'+
        '          "showsLocation": false,'+
        '          "showsStatus": false,'+
        '          "toolbar": false,'+
        '          "url": null,'+
        '          "width": null,'+
        '          "windowPosition": null'+
        '        }'+
        '      ],'+
        '      "columns": ['+
        '        {'+
        '          "field": "OpenActivity.Subject",'+
        '          "format": null,'+
        '          "label": "Subject",'+
        '          "lookupId": "Id",'+
        '          "name": "Subject"'+
        '        },'+
        '        {'+
        '          "field": "Name.Name",'+
        '          "format": null,'+
        '          "label": "Name",'+
        '          "lookupId": "WhoId",'+
        '          "name": "Who.Name"'+
        '        },'+
        '        {'+
        '          "field": "OpenActivity.IsTask",'+
        '          "format": null,'+
        '          "label": "Task",'+
        '          "lookupId": null,'+
        '          "name": "IsTask"'+
        '        },'+
        '        {'+
        '          "field": "OpenActivity.ActivityDate",'+
        '          "format": "date",'+
        '          "label": "Due Date",'+
        '          "lookupId": null,'+
        '          "name": "ActivityDate"'+
        '        },'+
        '        {'+
        '          "field": "OpenActivity.Status",'+
        '          "format": null,'+
        '          "label": "Status",'+
        '          "lookupId": null,'+
        '          "name": "toLabel(Status)"'+
        '        },'+
        '        {'+
        '          "field": "OpenActivity.Priority",'+
        '          "format": null,'+
        '          "label": "Priority",'+
        '          "lookupId": null,'+
        '          "name": "toLabel(Priority)"'+
        '        },'+
        '        {'+
        '          "field": "User.Name",'+
        '          "format": null,'+
        '          "label": "Assigned To",'+
        '          "lookupId": "Owner.Id",'+
        '          "name": "Owner.Name"'+
        '        }'+
        '      ],'+
        '      "custom": false,'+
        '      "field": "WhatId",'+
        '      "label": "Open Activities",'+
        '      "limitRows": 5,'+
        '      "name": "OpenActivities",'+
        '      "sobject": "OpenActivity",'+
        '      "sort": ['+
        '        {'+
        '          "ascending": true,'+
        '          "column": "ActivityDate"'+
        '        },'+
        '        {'+
        '          "ascending": false,'+
        '          "column": "LastModifiedDate"'+
        '        }'+
        '      ]'+
        '    },'+
        '    {'+
        '      "accessLevelRequiredForCreate": null,'+
        '      "buttons": ['+
        '        {'+
        '          "behavior": null,'+
        '          "colors": ['+
        '            {'+
        '              "color": "48C3CC",'+
        '              "context": "primary",'+
        '              "theme": "theme4"'+
        '            }'+
        '          ],'+
        '          "content": null,'+
        '          "contentSource": null,'+
        '          "custom": false,'+
        '          "encoding": null,'+
        '          "height": null,'+
        '          "icons": ['+
        '            {'+
        '              "contentType": "image/svg+xml",'+
        '              "height": 0,'+
        '              "theme": "theme4",'+
        '              "url": "https://cs5.salesforce.com/img/icon/t4v32/action/log_a_call.svg",'+
        '              "width": 0'+
        '            },'+
        '            {'+
        '              "contentType": "image/png",'+
        '              "height": 60,'+
        '              "theme": "theme4",'+
        '              "url": "https://cs5.salesforce.com/img/icon/t4v32/action/log_a_call_60.png",'+
        '              "width": 60'+
        '            },'+
        '            {'+
        '              "contentType": "image/png",'+
        '              "height": 120,'+
        '              "theme": "theme4",'+
        '              "url": "https://cs5.salesforce.com/img/icon/t4v32/action/log_a_call_120.png",'+
        '              "width": 120'+
        '            }'+
        '          ],'+
        '          "label": "Log a Call",'+
        '          "menubar": false,'+
        '          "name": "LogCall",'+
        '          "overridden": false,'+
        '          "resizeable": false,'+
        '          "scrollbars": false,'+
        '          "showsLocation": false,'+
        '          "showsStatus": false,'+
        '          "toolbar": false,'+
        '          "url": null,'+
        '          "width": null,'+
        '          "windowPosition": null'+
        '        },'+
        '        {'+
        '          "behavior": null,'+
        '          "colors": null,'+
        '          "content": null,'+
        '          "contentSource": null,'+
        '          "custom": false,'+
        '          "encoding": null,'+
        '          "height": null,'+
        '          "icons": null,'+
        '          "label": "Mail Merge",'+
        '          "menubar": false,'+
        '          "name": "MailMerge",'+
        '          "overridden": false,'+
        '          "resizeable": false,'+
        '          "scrollbars": false,'+
        '          "showsLocation": false,'+
        '          "showsStatus": false,'+
        '          "toolbar": false,'+
        '          "url": null,'+
        '          "width": null,'+
        '          "windowPosition": null'+
        '        },'+
        '        {'+
        '          "behavior": null,'+
        '          "colors": ['+
        '            {'+
        '              "color": "95AEC5",'+
        '              "context": "primary",'+
        '              "theme": "theme4"'+
        '            }'+
        '          ],'+
        '          "content": null,'+
        '          "contentSource": null,'+
        '          "custom": false,'+
        '          "encoding": null,'+
        '          "height": null,'+
        '          "icons": ['+
        '            {'+
        '              "contentType": "image/svg+xml",'+
        '              "height": 0,'+
        '              "theme": "theme4",'+
        '              "url": "https://cs5.salesforce.com/img/icon/t4v32/action/email.svg",'+
        '              "width": 0'+
        '            },'+
        '            {'+
        '              "contentType": "image/png",'+
        '              "height": 60,'+
        '              "theme": "theme4",'+
        '              "url": "https://cs5.salesforce.com/img/icon/t4v32/action/email_60.png",'+
        '              "width": 60'+
        '            },'+
        '            {'+
        '              "contentType": "image/png",'+
        '              "height": 120,'+
        '              "theme": "theme4",'+
        '              "url": "https://cs5.salesforce.com/img/icon/t4v32/action/email_120.png",'+
        '              "width": 120'+
        '            }'+
        '          ],'+
        '          "label": "Send an Email",'+
        '          "menubar": false,'+
        '          "name": "SendEmail",'+
        '          "overridden": false,'+
        '          "resizeable": false,'+
        '          "scrollbars": false,'+
        '          "showsLocation": false,'+
        '          "showsStatus": false,'+
        '          "toolbar": false,'+
        '          "url": null,'+
        '          "width": null,'+
        '          "windowPosition": null'+
        '        },'+
        '        {'+
        '          "behavior": null,'+
        '          "colors": null,'+
        '          "content": null,'+
        '          "contentSource": null,'+
        '          "custom": false,'+
        '          "encoding": null,'+
        '          "height": null,'+
        '          "icons": null,'+
        '          "label": "Compose Gmail",'+
        '          "menubar": false,'+
        '          "name": "ComposeGmail",'+
        '          "overridden": false,'+
        '          "resizeable": false,'+
        '          "scrollbars": false,'+
        '          "showsLocation": false,'+
        '          "showsStatus": false,'+
        '          "toolbar": false,'+
        '          "url": null,'+
        '          "width": null,'+
        '          "windowPosition": null'+
        '        },'+
        '        {'+
        '          "behavior": null,'+
        '          "colors": null,'+
        '          "content": null,'+
        '          "contentSource": null,'+
        '          "custom": false,'+
        '          "encoding": null,'+
        '          "height": null,'+
        '          "icons": null,'+
        '          "label": "Request Update",'+
        '          "menubar": false,'+
        '          "name": "RequestUpdate",'+
        '          "overridden": false,'+
        '          "resizeable": false,'+
        '          "scrollbars": false,'+
        '          "showsLocation": false,'+
        '          "showsStatus": false,'+
        '          "toolbar": false,'+
        '          "url": null,'+
        '          "width": null,'+
        '          "windowPosition": null'+
        '        },'+
        '        {'+
        '          "behavior": null,'+
        '          "colors": null,'+
        '          "content": null,'+
        '          "contentSource": null,'+
        '          "custom": false,'+
        '          "encoding": null,'+
        '          "height": null,'+
        '          "icons": null,'+
        '          "label": "View All",'+
        '          "menubar": false,'+
        '          "name": "ViewAll",'+
        '          "overridden": false,'+
        '          "resizeable": false,'+
        '          "scrollbars": false,'+
        '          "showsLocation": false,'+
        '          "showsStatus": false,'+
        '          "toolbar": false,'+
        '          "url": null,'+
        '          "width": null,'+
        '          "windowPosition": null'+
        '        }'+
        '      ],'+
        '      "columns": ['+
        '        {'+
        '          "field": "ActivityHistory.Subject",'+
        '          "format": null,'+
        '          "label": "Subject",'+
        '          "lookupId": "Id",'+
        '          "name": "Subject"'+
        '        },'+
        '        {'+
        '          "field": "Name.Name",'+
        '          "format": null,'+
        '          "label": "Name",'+
        '          "lookupId": "WhoId",'+
        '          "name": "Who.Name"'+
        '        },'+
        '        {'+
        '          "field": "ActivityHistory.IsTask",'+
        '          "format": null,'+
        '          "label": "Task",'+
        '          "lookupId": null,'+
        '          "name": "IsTask"'+
        '        },'+
        '        {'+
        '          "field": "ActivityHistory.ActivityDate",'+
        '          "format": "date",'+
        '          "label": "Due Date",'+
        '          "lookupId": null,'+
        '          "name": "ActivityDate"'+
        '        },'+
        '        {'+
        '          "field": "User.Name",'+
        '          "format": null,'+
        '          "label": "Assigned To",'+
        '          "lookupId": "Owner.Id",'+
        '          "name": "Owner.Name"'+
        '        },'+
        '        {'+
        '          "field": "ActivityHistory.LastModifiedDate",'+
        '          "format": "datetime",'+
        '          "label": "Last Modified Date/Time",'+
        '          "lookupId": null,'+
        '          "name": "LastModifiedDate"'+
        '        }'+
        '      ],'+
        '      "custom": false,'+
        '      "field": "WhatId",'+
        '      "label": "Activity History",'+
        '      "limitRows": 5,'+
        '      "name": "ActivityHistories",'+
        '      "sobject": "ActivityHistory",'+
        '      "sort": ['+
        '        {'+
        '          "ascending": false,'+
        '          "column": "ActivityDate"'+
        '        },'+
        '        {'+
        '          "ascending": true,'+
        '          "column": "LastModifiedDate"'+
        '        }'+
        '      ]'+
        '    },'+
        '    {'+
        '      "accessLevelRequiredForCreate": null,'+
        '      "buttons": null,'+
        '      "columns": ['+
        '        {'+
        '          "field": "CombinedAttachment.Title",'+
        '          "format": null,'+
        '          "label": "Title",'+
        '          "lookupId": "Id",'+
        '          "name": "Title"'+
        '        },'+
        '        {'+
        '          "field": "CombinedAttachment.RecordType",'+
        '          "format": null,'+
        '          "label": "Type",'+
        '          "lookupId": null,'+
        '          "name": "RecordType"'+
        '        },'+
        '        {'+
        '          "field": "CombinedAttachment.LastModifiedDate",'+
        '          "format": null,'+
        '          "label": "Last Modified",'+
        '          "lookupId": null,'+
        '          "name": "LastModifiedDate"'+
        '        },'+
        '        {'+
        '          "field": "CombinedAttachment.CreatedById",'+
        '          "format": null,'+
        '          "label": "Created By",'+
        '          "lookupId": null,'+
        '          "name": "CreatedBy.Name"'+
        '        },'+
        '        {'+
        '          "field": "CombinedAttachment.FileType",'+
        '          "format": null,'+
        '          "label": "File Type",'+
        '          "lookupId": null,'+
        '          "name": "FileType"'+
        '        },'+
        '        {'+
        '          "field": "CombinedAttachment.ContentSize",'+
        '          "format": null,'+
        '          "label": "Content Size",'+
        '          "lookupId": null,'+
        '          "name": "ContentSize"'+
        '        },'+
        '        {'+
        '          "field": "CombinedAttachment.FileExtension",'+
        '          "format": null,'+
        '          "label": "File Extension",'+
        '          "lookupId": null,'+
        '          "name": "FileExtension"'+
        '        },'+
        '        {'+
        '          "field": "CombinedAttachment.ContentUrl",'+
        '          "format": null,'+
        '          "label": "Content URL",'+
        '          "lookupId": null,'+
        '          "name": "ContentUrl"'+
        '        },'+
        '        {'+
        '          "field": "CombinedAttachment.ExternalDataSourceName",'+
        '          "format": null,'+
        '          "label": "External Data Source",'+
        '          "lookupId": null,'+
        '          "name": "ExternalDataSourceName"'+
        '        }'+
        '      ],'+
        '      "custom": false,'+
        '      "field": "ParentId",'+
        '      "label": "Notes & Attachments",'+
        '      "limitRows": 5,'+
        '      "name": "CombinedAttachments",'+
        '      "sobject": "CombinedAttachment",'+
        '      "sort": ['+
        '        {'+
        '          "ascending": false,'+
        '          "column": "LastModifiedDate"'+
        '        }'+
        '      ]'+
        '    }'+
        '  ]'+
        '}';
        
 
        pageLayoutJSON.cls_relatedLists objRL=new pageLayoutJSON.cls_relatedLists(); 
        pageLayoutJSON.cls_detailButtons objDB=new pageLayoutJSON.cls_detailButtons();
        pageLayoutJSON.cls_behavior objB=new pageLayoutJSON.cls_behavior();
        pageLayoutJSON.cls_encoding objE=new pageLayoutJSON.cls_encoding();
        pageLayoutJSON.cls_icons objI=new pageLayoutJSON.cls_icons();
        pageLayoutJSON.cls_detailLayoutSections objDLS=new pageLayoutJSON.cls_detailLayoutSections();
        pageLayoutJSON.cls_layoutRows objLR=new pageLayoutJSON.cls_layoutRows();
        pageLayoutJSON.cls_layoutItems objLI=new pageLayoutJSON.cls_layoutItems();
        pageLayoutJSON.cls_layoutComponents objLC=new pageLayoutJSON.cls_layoutComponents();
        pageLayoutJSON.cls_details objD=new pageLayoutJSON.cls_details(); 
        pageLayoutJSON.cls_picklistValues objPV=new pageLayoutJSON.cls_picklistValues(); 
        pageLayoutJSON.cls_editLayoutSections objELS=new pageLayoutJSON.cls_editLayoutSections();
        pageLayoutJSON.cls_buttons objEB=new pageLayoutJSON.cls_buttons();
        pageLayoutJSON.cls_columns objES=new pageLayoutJSON.cls_columns(); 
        pageLayoutJSON.cls_buttonLayoutSection objEBLS=new pageLayoutJSON.cls_buttonLayoutSection();
        pageLayoutJSON.cls_colors objCLC=new pageLayoutJSON.cls_colors();
        pageLayoutJSON.cls_sort objCLSS=new pageLayoutJSON.cls_sort();
        pageLayoutJSON.cls_quickActionListItems objQALI=new pageLayoutJSON.cls_quickActionListItems();
        pageLayoutJSON.cls_quickActionList objQAL=new pageLayoutJSON.cls_quickActionList();
    }
    
    static testmethod void testDetermineRL(){
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());       
            DetermineRelatedListClass.getRelatedList('string');
            Test.stopTest();
    }
    
    static testmethod void testSubmittAssessment(){
        init();
        
        pref = Page.SubmitAssessment;
        Test.setCurrentPage(pref);
        System.currentPageReference().getParameters().put('Application__c', app.Id);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        SubmitAssessmentController testClass=new SubmitAssessmentController();
        testClass.performChecks();
        testclass.checkOut();
        testClass.submitForAssessment();    
        SubmitAssessmentController.retrieveProductCode(app.Id);
 

        //Check other if condtions for boomicallout
        acc.PersonMailingStreet = '121';
        acc.PersonMailingCity = 'Auckland';
        acc.PersonMailingPostalCode = '1010';
        acc.PersonMailingCountry = 'New Zealand';
        acc.Mailing_Company_Name__c = 'test';    
        update acc;
        testclass.checkOut();     
        //testclass.getInfoFromExternalService();

        Test.stopTest();
    }

        static testmethod void testSubmitPASAAssessment(){
        initPASA();
        
        pref = Page.SubmitAssessment;
        Test.setCurrentPage(pref);
        System.currentPageReference().getParameters().put('Application__c', app2.Id);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        SubmitAssessmentController testClass=new SubmitAssessmentController();
        testClass.performChecks();
        testclass.checkOut();
        testClass.submitForAssessment();    
        SubmitAssessmentController.retrieveProductCode(app2.Id);
 

        //Check other if condtions for boomicallout
        acc2.PersonMailingStreet = '121';
        acc2.PersonMailingCity = 'Auckland';
        acc2.PersonMailingPostalCode = '1010';
        acc2.PersonMailingCountry = 'New Zealand';
        acc2.Mailing_Company_Name__c = 'test';    
        update acc2;
        testclass.checkOut();     
        //testclass.getInfoFromExternalService();

        Test.stopTest();
    }

    static testMethod void testSubmittAssessmentNoMatch() {
        init();
        list<Account> accList = [Select Id from Account limit 1];
        Account acc1=new Account();
        acc1 = accList[0];       

        Application__c app1=new Application__c();
        app1.Account__c=acc1.Id;
        app1.Application_Status__c = 'Test Assessment';
        app1.College__c = 'Accounting Technicians';
        app1.Pathway__c = 'Academic Pathway';
        app1.RecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('Provisional Member').getRecordTypeId();
        insert app1;

        pref = Page.SubmitAssessment;
        Test.setCurrentPage(pref);
        System.currentPageReference().getParameters().put('Application__c', app.Id);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        SubmitAssessmentController testClass=new SubmitAssessmentController();
        testClass.performChecks();
        testClass.submitForAssessment();    
        SubmitAssessmentController.retrieveProductCode(app1.Id);
        testclass.addPageMessage('test');
        //testclass.getInfoFromExternalService();

        Test.stopTest();
    }

    static testMethod void testOrderTrigger() {
        init();
        Test.startTest();
        ord.NetSuite_Order_Id__c = '12121';
        ord.NetSuite_Payment_Date__c = date.today();
        update ord;
        system.debug('### ord =>' + ord);
        Test.stopTest();
        //assert application status 
        List<Application__c> appList = [Select Id, Application_Fee_Paid1__c, Application_Fee_Paid__c, Application_Status__c From Application__c where id=:app.Id];
        system.debug('### appList =>' + appList);
        if(appList.size() > 0) {
            system.assertEquals(appList.get(0).Application_Fee_Paid1__c, 'Paid');
            system.assertEquals(appList.get(0).Application_Status__c, 'Submitted for Assessment');
        }

    }
    
}