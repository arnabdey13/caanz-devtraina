/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class testRelationshipTrigger {
	private static Account BusinessAccountObjectInDB;
	private static Account PersonAccount_Tom_ObjectInDB;
	private static Account PersonAccount_Barry_ObjectInDB;
	private static Relationship__c RelationshipObjectInDB;
	static {
		insertBusinessAccountObjectInDB();
		insertPersonAccount_Tom_ObjectInDB();
		insertPersonAccount_Barry_ObjectInDB();
		insertRelationshipObjectInDB();
		
		TriggerHelper.startNewTransaction();
	}
	private static void insertBusinessAccountObjectInDB(){
		BusinessAccountObjectInDB = TestObjectCreator.createBusinessAccount();
		//## Required Relationships
		//## Additional fields and relationships / Updated fields
		insert BusinessAccountObjectInDB;
	}
	private static void insertPersonAccount_Tom_ObjectInDB(){
		PersonAccount_Tom_ObjectInDB = TestObjectCreator.createPersonAccount();
		//## Required Relationships
		//## Additional fields and relationships / Updated fields
		insert PersonAccount_Tom_ObjectInDB;
	}
	private static void insertPersonAccount_Barry_ObjectInDB(){
		PersonAccount_Barry_ObjectInDB = TestObjectCreator.createPersonAccount();
		//## Required Relationships
		//## Additional fields and relationships / Updated fields
		insert PersonAccount_Barry_ObjectInDB;
	}
	private static void insertRelationshipObjectInDB(){
		RelationshipObjectInDB = TestObjectCreator.createRelationship();
		//## Required Relationships
		//## Additional fields and relationships / Updated fields
		RelationshipObjectInDB.Account__c = BusinessAccountObjectInDB.Id;
		RelationshipObjectInDB.Member__c = PersonAccount_Tom_ObjectInDB.Id;
		RelationshipObjectInDB.Primary_Relationship__c = 'A';
		RelationshipObjectInDB.Reciprocal_Relationship__c = 'B';
		insert RelationshipObjectInDB;
	}
	
	//******************************************************************************************
	//						TestMethods
	//******************************************************************************************
	static testMethod void testRelationshipTrigger_Insert() {
		Relationship__c RelationshipObject = TestObjectCreator.createRelationship();
		//## Required Relationships
		RelationshipObject.Account__c = BusinessAccountObjectInDB.Id;
		RelationshipObject.Member__c = PersonAccount_Tom_ObjectInDB.Id;
		//## Additional fields and relationships / Updated fields
		RelationshipObject.Primary_Relationship__c = 'A';
		RelationshipObject.Reciprocal_Relationship__c = 'B';
		insert RelationshipObject;
		
		// Check Results
		List<Relationship__c> Relationship_List = [Select 
			Name
			, Account__c
			, Member__c
			, Primary_Relationship__c
			, Reciprocal_Relationship__c
			, Status__c
			, Relationship_Start_Date__c
			, Relationship_End_Date__c
			from Relationship__c where CorrespondingRelationshipId__c=:RelationshipObject.Id];
		
		System.assertEquals(1, Relationship_List.size(), 'Relationship_List.size');
		Relationship__c ReciprocalRelationship = Relationship_List[0];
		
		System.assertEquals(RelationshipObject.Member__c, ReciprocalRelationship.Account__c, 'ReciprocalRelationship.Account__c');
		System.assertEquals(RelationshipObject.Account__c, ReciprocalRelationship.Member__c, 'ReciprocalRelationship.Member__c');
		System.assertEquals(RelationshipObject.Primary_Relationship__c, ReciprocalRelationship.Reciprocal_Relationship__c, 'ReciprocalRelationship.Reciprocal_Relationship__c');
		System.assertEquals(RelationshipObject.Reciprocal_Relationship__c, ReciprocalRelationship.Primary_Relationship__c, 'ReciprocalRelationship.Primary_Relationship__c');
		
		System.assertEquals(RelationshipObject.Status__c, ReciprocalRelationship.Status__c, 'ReciprocalRelationship.Status__c');
		System.assertEquals(RelationshipObject.Relationship_Start_Date__c, ReciprocalRelationship.Relationship_Start_Date__c, 'ReciprocalRelationship.Relationship_Start_Date__c');
		System.assertEquals(RelationshipObject.Relationship_End_Date__c, ReciprocalRelationship.Relationship_End_Date__c, 'ReciprocalRelationship.Relationship_End_Date__c');
	}
	
	static testMethod void testRelationshipTrigger_UpdatePrimaryInformation() {
		System.assertEquals(PersonAccount_Tom_ObjectInDB.Id, RelationshipObjectInDB.Member__c, 'RelationshipObjectInDB.Member__c');
		RelationshipObjectInDB.Member__c = PersonAccount_Barry_ObjectInDB.Id;
		RelationshipObjectInDB.Primary_Relationship__c = 'C';
		update RelationshipObjectInDB;
		
		// Check Results
		List<Relationship__c> Relationship_List = [Select 
			Name
			, Account__c
			, Reciprocal_Relationship__c
			from Relationship__c where CorrespondingRelationshipId__c=:RelationshipObjectInDB.Id];
		
		System.assertEquals(1, Relationship_List.size(), 'Relationship_List.size');
		Relationship__c ReciprocalRelationship = Relationship_List[0];
		
		System.assertEquals(PersonAccount_Barry_ObjectInDB.Id, ReciprocalRelationship.Account__c, 'ReciprocalRelationship.Account__c');
		System.assertEquals('C', ReciprocalRelationship.Reciprocal_Relationship__c, 'ReciprocalRelationship.Reciprocal_Relationship__c');
	}
	
	static testMethod void testRelationshipTrigger_UpdateSecondaryInformation() {
		List<Relationship__c> Relationship_List = [Select 
			Name
			, Account__c
			, Member__c
			, Reciprocal_Relationship__c
			from Relationship__c where CorrespondingRelationshipId__c=:RelationshipObjectInDB.Id];
		System.assertEquals(1, Relationship_List.size(), 'Relationship_List.size');
		Relationship__c ReciprocalRelationship = Relationship_List[0];
		
		System.assertEquals(PersonAccount_Tom_ObjectInDB.Id, ReciprocalRelationship.Account__c, 'ReciprocalRelationship.Account__c');
		ReciprocalRelationship.Member__c = PersonAccount_Barry_ObjectInDB.Id;
		ReciprocalRelationship.Primary_Relationship__c = 'C';
		update ReciprocalRelationship;
		
		// Check Results
		Relationship_List = [Select 
			Name
			, Account__c
			, Member__c
			, Account__r.Name
			, Member__r.Name
			, Reciprocal_Relationship__c
			from Relationship__c where Id=:RelationshipObjectInDB.Id];
		System.assertEquals(1, Relationship_List.size(), 'Relationship_List.size');
		Relationship__c OriginalRelationship = Relationship_List[0];
		
		System.assertEquals(PersonAccount_Barry_ObjectInDB.Id, OriginalRelationship.Account__c, 'OriginalRelationship.Account__c');
		System.assertEquals('C', OriginalRelationship.Reciprocal_Relationship__c, 'OriginalRelationship.Reciprocal_Relationship__c');
	}
	
	static testMethod void testRelationshipTrigger_DeletePrimaryRelationship() {
		List<Relationship__c> Relationship_List = [Select Name from Relationship__c 
			where CorrespondingRelationshipId__c=:RelationshipObjectInDB.Id];
		System.assertEquals(1, Relationship_List.size(), 'Relationship_List.size');
		
		delete RelationshipObjectInDB;
		
		// Check Results
		Relationship_List = [Select Name from Relationship__c 
			where CorrespondingRelationshipId__c=:RelationshipObjectInDB.Id];
		System.assertEquals(0, Relationship_List.size(), 'Relationship_List.size');
	}
	
	static testMethod void testRelationshipTrigger_DeleteSecondaryRelationship() {
		List<Relationship__c> Relationship_List = [Select Name from Relationship__c 
			where CorrespondingRelationshipId__c=:RelationshipObjectInDB.Id];
		System.assertEquals(1, Relationship_List.size(), 'Relationship_List.size');
		Relationship__c CorrespondingRelationship = Relationship_List[0];
		
		delete CorrespondingRelationship;
		
		// Check Results
		Relationship_List = [Select Name from Relationship__c 
			where Id=:RelationshipObjectInDB.Id];
		System.assertEquals(0, Relationship_List.size(), 'Relationship_List.size');
	}
}