global class RecognitionMatchKeyRefresh Implements Database.Batchable <Sobject> {
          
// 
// RecognitionMatchKeyRefresh objClass = new RecognitionMatchKeyRefresh();
// Database.executeBatch (objClass, 10);
// 
// Test status of the Async job
// SELECT ApexClassId,CompletedDate,CreatedById,CreatedDate,ExtendedStatus,Id,JobItemsProcessed,JobType,LastProcessed,LastProcessedOffset,MethodName,NumberOfErrors,ParentJobId,Status 
// FROM AsyncApexJob

	global  String query;
    global List<ID> accountsToUpdate;
    global final Map<String, ID> main_prcts_lkp;
    String email;
    
    RecognitionBusinessUtil rbu = new RecognitionBusinessUtil();
    
	// constructor to populate query
    public RecognitionMatchKeyRefresh(List<ID> accountsIdToUpdate ){
        accountsToUpdate = accountsIdToUpdate;
        query =  ' Select ID, Name, BillingCountry, BillingStreet, BillingCity, BillingPostalCode, Billing_DPID__c, Type, ShippingCountry, ShippingStreet, ShippingCity,  ShippingPostalCode, Shipping_DPID__c, Phone ';
        query += ' From Account Where Id IN :accountsToUpdate ';
    }
    
    // constructor to populate query
    public RecognitionMatchKeyRefresh(Boolean practiceOnly ){
        query =  ' Select ID, Name, BillingCountry, BillingStreet, BillingCity, BillingPostalCode, Billing_DPID__c, Type, ShippingCountry, ShippingStreet, ShippingCity,  ShippingPostalCode, Shipping_DPID__c, Phone ';
        query += ' From Account Where isPersonAccount = false ';
		if (practiceOnly)
        	query +=  ' And Type like \'%Account%\' ';
        query += ' And Status__c = \'Active\' ';
        query += ' And BillingCountry in ( \'Australia\', \'New Zealand\') ';
    }

	// start the batch
    global Database.QueryLocator start(Database.BatchableContext BC){ 
        System.debug('Update Started');
        return Database.getQueryLocator(query); 
    } 
    

    global void execute(Database.BatchableContext BC, List<sObject> scope){      
        List <Reference_Recognition_CAANZ_Repository__c> busToUpdate = new List <Reference_Recognition_CAANZ_Repository__c>();
        for (sObject objScope: scope) {     
        	Account newObjScope = (Account) objScope ;
            Reference_Recognition_CAANZ_Repository__c result = RecognitionMatching.getMatchKeys(newObjScope);
            System.debug(' Batch Result' + result);
            busToUpdate.add(result);  
        }
        if (busToUpdate != null && busToUpdate.size()>0) {
            Database.upsert(busToUpdate); 
            System.debug('List Size '+ busToUpdate.size());
        }
    } 
    
    
    global void sendMail(){ 
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
        mail.setToAddresses(new String[] {email}); 
        mail.setReplyTo('andrew.kopec@charteredaccountantsanz.com'); 
        mail.setSenderDisplayName('Practice Recognition Batch Processing'); 
        mail.setSubject('Batch Process Completed'); 
        mail.setPlainTextBody('Batch Process has completed'); 
        // if (!Test.isRunningTest())
        //	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
    } 

 
	global void finish(Database.BatchableContext BC){ 
        sendMail();
        System.debug('Update Completed');
    }    
    
}