/**
* @author Steve Buikhuizen, Jannis Bott
* @date 14/11/2016
*
* @group FormsFramework
*
* @description  Contains all data declarations used throughout
*               the forms framework
*/
global class ff_Service {

    /**
    * @author Steve Buikhuizen, Jannis Bott
    * @date 14/11/2016
    *
    * @group FormsFramework
    *
    * @description  Contains List<SObject> plus extra control data
    */
    global class SObjectWrapper {

        @AuraEnabled
        global List<SObject> sObjectList {get;set;}

        /**
        * @description specifies that the list contains one or multiple rows
        */
        @AuraEnabled
        global Boolean single {get;set;}

        global SObjectWrapper(List<SObject> sObjectList, Boolean single){
            this.sObjectList = sObjectList;
            this.single = single;
        }
    }

    /**
    * @author Steve Buikhuizen
    * @date 27/1/2017
    *
    * @group FormsFramework
    *
    * @description Contains Attachments and parent ids
    */
    global class AttachmentWrapper implements CustomWrapper {

        @AuraEnabled
        global List<Attachment> attachments {get;set;}

        /**
        * @description specifies that the list contains one or multiple rows
        */
        @AuraEnabled
        global Id parent {get;set;}

        @AuraEnabled
        global List<ContentDocument> fileList { get; set; }

        global ff_Service.AttachmentWrapper(List<Attachment> attachments, Id parent){
            this.attachments = attachments;
            this.parent = parent;
        }
    }

	/**
    * @author RXP
    * @date 06/2019
    *
    * @purpose Additional Code for Files processing. New Forms are using Files, instead of attachments 
    *
    * @description Contains Files and parent ids
    */		

    global class FilesWrapper implements CustomWrapper {
        @AuraEnabled
        global List<ContentDocument> fileList { get; set; }

        @AuraEnabled
        global Id parent {get;set;}

        global ff_Service.FilesWrapper(List<ContentDocument> fileList, Id parent){
            this.fileList = fileList;
            this.parent = parent;
        }
    }

    /**
    * @author Steve Buikhuizen, Jannis Bott
    * @date 14/11/2016
    *
    * @group FormsFramework
    *
    * @description  Class holds key and value like normal picklist but also a Set<String>
    *               that can declare dependencies to parent
    */
    global class PicklistOption implements CustomWrapper {
        @AuraEnabled
        global String label {get;set;}
        @AuraEnabled
        global String value {get;set;}

        /**
        * @description used for dependent picklists. Allows filtering based on a parent value
        */
        @AuraEnabled
        global Set<String> parentValues {get;set;}
    }

    /**
    * @author Jannis Bott
    * @date 25/01/2017
    *
    * @group FormsFramework
    *
    * @description  Class holds rich text content
    */
    global class RichTextWrapper implements CustomWrapper {
        @AuraEnabled
        global String richtext {get;set;}
    }

    /**
    * @description Marker interface for all non-SObjects returned in Read/WriteData
    */
    global interface CustomWrapper {}
}