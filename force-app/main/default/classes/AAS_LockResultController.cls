/* 
    Developer: WDCi (Terry)
    Development Date: 21/04/2017
    Task #: AAS Final Result Release for exam 
*/
public with sharing class AAS_LockResultController {

    public AAS_Course_Assessment__c courseAssessment {get; set;}
    public Boolean validToProcess {get; set;}
    
    PageReference caReturnPage;


    public AAS_LockResultController(ApexPages.StandardController controller) {
        courseAssessment = (AAS_Course_Assessment__c) controller.getRecord();
        courseAssessment = [SELECT Id, Name, AAS_Pre_Final_Adjustment__c FROM AAS_Course_Assessment__c WHERE id=: courseAssessment.Id];
        caReturnPage = new PageReference('/'+courseAssessment.Id);
        doValidation();
    }
    
    public void doValidation(){
        if(!courseAssessment.AAS_Pre_Final_Adjustment__c){
            validToProcess = false;
            ApexPages.addmessage(new ApexPages.Message(ApexPages.severity.ERROR, 'You are not allowed to proceed until you complete Step 7: Pre-Final Adjustment.'));            
        } else 
             validToProcess = true;                       
    }
 
    public PageReference doCommit(){    
        courseAssessment.AAS_Result_Release_Lock__c = true;
        update courseAssessment;
        return caReturnPage;
    }
    
    public PageReference doCancel(){
        return caReturnPage;
    }
}