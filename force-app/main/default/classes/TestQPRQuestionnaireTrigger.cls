@isTest
private class TestQPRQuestionnaireTrigger {

    public static Id questionnaireAU = Schema.SObjectType.QPR_Questionnaire__c.getRecordTypeInfosByName().get('QPR AUS').getRecordTypeId();

    static testMethod void createQPRQuestionnaire() {
        // Create member
        Account a = TestObjectCreator.createBusinessAccount();
        insert a;
        
        // Create Review
        Review__c r = new Review__c();
        r.Practice_Name__c = a.Id;
        insert r;
        
        // Create QPR Questionnaire
        QPR_Questionnaire__c oneQuestionnaire = new QPR_Questionnaire__c();
        oneQuestionnaire.RecordTypeId = questionnaireAU;
        oneQuestionnaire.Practice__c = a.Id;
        oneQuestionnaire.Practice_ID__c = '12345';
        oneQuestionnaire.Review_Code__c = r.Id;
        insert oneQuestionnaire;
        
        Test.startTest();
        
        oneQuestionnaire.Status__c = 'Submitted';
        update oneQuestionnaire;
        
        Test.stopTest();
    }

}