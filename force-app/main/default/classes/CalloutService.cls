/**
* @author Jannis Bott
* @date 25/11/2016
*
* @description  Handles callouts based on configuration data stored in custom metadata types and
*               received params from calling class
*/
public with sharing class CalloutService {

    public static final String CONFIGURATION_LOAD_ERROR_HEADER = 'Callout exception!';
    public static final String CONFIGURATION_LOAD_ERROR_BODY = 'An exception occured while loading callout configuration data. The error is: {0}';
    public static final String CALLOUT_ERROR_MESSAGE = 'Callout response did not return 200 when calling ther enpoint address URL: {0}. The status code is: {1}';
    public static final String CALLOUT_EXCEPTION_MESSAGE = 'An exception occured while calling out. The error is: {0}';
    public static final String CALLOUT_CONFIG_DOES_NOT_EXIST = 'No callout configuration data found.';
    public static final Id SYSTEM_ADMIN_ID = [SELECT Id FROM Profile WHERE Name = 'System Administrator' Limit 1].Id;
    public static final List<User> SYSTEM_ADMINS = [SELECT Email FROM User WHERE ProfileId = :SYSTEM_ADMIN_ID AND LastName = 'Bott'];
    public static final String CONTENT_TYPE = 'Content-Type';
    public static final String AUTH_TOKEN = 'Auth-Token';

    /**
    * @description  gets the callout configuration and executes callout before returning the response body
    * @param        String configurationName - the name of the custom metadata configuration that contains the callout config
    * @param        String body - the content that should be send as the body of the callout
    * @return       String - the response body of the callout
    * @example      CalloutService.executeCallout('HttpMock', '{Sample Body}');
    */
    public static String executeCallout(String configurationName, String urlParams, String body) {

        Callout_Configuration__mdt calloutConfig;
        HttpRequest req;
        Http h;
        HttpResponse response;


        try {
            calloutConfig = getConfigurationData(configurationName);
            System.debug('CalloutService: Callout Configuration: ' + calloutConfig);

            req = createRequest(calloutConfig, urlParams, body);
            System.debug('CalloutService: REQUEST: ' + req);
            h = new Http();
            response = h.send(req);
            System.debug('CalloutService: RESPONSE BODY ' + response.getBody());
            // If response is not successful send and email to admin with the error details.
            if (response.getStatusCode() != 200) {
                sendEmail(
                        new List<String>{
                                calloutConfig.Failure_Notification_Email__c
                        },
                        CONFIGURATION_LOAD_ERROR_HEADER,
                        String.format(CALLOUT_ERROR_MESSAGE,
                                new List<String>{
                                        calloutConfig.Endpoint_URL__c + urlParams,
                                        String.valueOf(response.getStatusCode())
                                })
                );
            }
            return response.getBody();

        } catch(CalloutException ce){

            System.debug('callout execption ' + ce);
            return JSON.serialize(new Map<String, CalloutService.CalloutResult>{
                    'CalloutServiceError' => CalloutService.CalloutResult.READ_TIMEOUT
            });
        } catch(Exception ex) {
            System.debug(' execption ' + ex);
            //if no callout config is found send email to all users that are system admins
            if (calloutConfig != null) {
                sendEmail(
                        new List<String>{
                                calloutConfig.Failure_Notification_Email__c
                        },
                        CONFIGURATION_LOAD_ERROR_HEADER,
                        String.format(CALLOUT_EXCEPTION_MESSAGE,
                                new List<String>{
                                        ex.getMessage()
                                })
                );
            }
            return JSON.serialize(new Map<String, CalloutService.CalloutResult>{
                    'CalloutServiceError' => CalloutService.CalloutResult.UNEXPECTED_EXCEPTION
            });
        }
    }

    /**
    * @description  generates the http request for the callout
    * @param        Callout_Configuration__mdt calloutConfig - the custom metadata configuration that contains the callout config
    * @param        String body - the content that should be send as the body of the callout
    * @return       HttpRequest - the request ready for sending
    * @example      createRequest(calloutConfig, body);
    */
    private static HttpRequest createRequest(Callout_Configuration__mdt calloutConfig, String urlParams, String body) {
        HttpRequest req = new HttpRequest();
        req.setTimeout(Integer.valueOf(calloutConfig.Timeout_In_Seconds__c) * 1000);
        req.setEndpoint(calloutConfig.Endpoint_URL__c + urlParams);
        req.setHeader(CONTENT_TYPE, calloutConfig.Content_Type__c);

        if (!String.isEmpty(calloutConfig.Authentication_Token__c)) req.setHeader(AUTH_TOKEN, calloutConfig.Authentication_Token__c);

        req.setMethod(calloutConfig.Request_Method__c);

        if (!String.isEmpty(body)) req.setBody(body);

        return req;
    }

    /**
    * @description  executes soql to retrieve the custom metadata configuration
    * @param        String configurationName - the name of the custom metadata configuration that needs to be queried
    * @return       Callout_Configuration__mdt - the custom metadata configuration
    * @example      getConfigurationData(configurationName);
    */
    private static Callout_Configuration__mdt getConfigurationData(String configurationName) {

        Callout_Configuration__mdt configuration = [
                SELECT Authentication_Token__c,
                        Endpoint_URL__c,
                        Timeout_In_Seconds__c,
                        Failure_Notification_Email__c,
                        Request_Method__c,
                        Content_Type__c
                FROM Callout_Configuration__mdt
                WHERE DeveloperName = :configurationName
        ];

        return configuration;
    }

    /**
    * @description  sends emails
    * @param        List<String> notifyerEmail - list of receiver emails
    * @param        String subject - email subject
    * @param        String body - email body
    * @example      sendEmail(getAdminEmails(SYSTEM_ADMINS), CONFIGURATION_LOAD_ERROR_HEADER, String.format(CONFIGURATION_LOAD_ERROR_BODY, new List<String>{ex.getMessage()}));
    */
    public static void sendEmail(List<String> notifyerEmail, String subject, String body) {
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setSubject(subject);
        email.setToAddresses(notifyerEmail);
        email.setPlainTextBody(body);

        if (!Test.isRunningTest()) {
            Messaging.sendEmail(new Messaging.Email[]{
                    email
            });
        }
    }

    public Enum CalloutResult {
        SUCCESS, READ_TIMEOUT, UNEXPECTED_EXCEPTION
    }
}