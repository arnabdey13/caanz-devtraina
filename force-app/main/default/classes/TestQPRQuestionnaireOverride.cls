@isTest
public class TestQPRQuestionnaireOverride {
    public static Id questionnaireAU = Schema.SObjectType.QPR_Questionnaire__c.getRecordTypeInfosByName().get('QPR AUS').getRecordTypeId();
    @testSetup
    Static void setupTestData(){
        // Create member
        Account a = TestObjectCreator.createBusinessAccount();
        insert a;
        
        // Create Review
        Review__c r = new Review__c();
        r.Practice_Name__c = a.Id;
        insert r;
        
        // Create QPR Questionnaire
        QPR_Questionnaire__c oneQuestionnaire = new QPR_Questionnaire__c();
        oneQuestionnaire.RecordTypeId = questionnaireAU;
        oneQuestionnaire.Practice__c = a.Id;
        oneQuestionnaire.Practice_ID__c = '12345';
        oneQuestionnaire.Lock__c = false;
        oneQuestionnaire.Status__c = 'Questionnaire Submitted';
        insert oneQuestionnaire;
        
        Application_Form__c appNew = new Application_Form__c();
        appNew.Name = 'CLONE - AU QPR QUESTIONNAIRE';
        appNew.Security_Token__c = '8b6f7b90cf0da4c51324d546a95073b644343';
        appNew.Form_ID__c = '224';
        appNew.Record_Type__c = 'PQR_AUS';
        insert appNew;
        /*
        Profile profile1 = [Select Id from Profile where name = 'CAANZ Business Management User'];
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        User u = new User( UserRoleId = portalRole.Id,
                          ProfileId = profile1.Id,
                          Username = 'testtermsconditions1234423@charteredaccountantsanz.com',
                          Alias = 'batman',
                          Email='testtermsconditions1234423@charteredaccountantsanz.com',
                          EmailEncodingKey='UTF-8',
                          Firstname='Bruce',
                          Lastname='Wayne',
                          LanguageLocaleKey='en_US',
                          LocaleSidKey='en_NZ',
                          TimeZoneSidKey='Pacific/Auckland');
        insert u; 
        */
        
    }
    
    @isTest
    public static void testgenearteFormURL(){
        PageReference pageRef = Page.QPRQuestionnaireOverride;
        Test.setCurrentPage(pageRef);
        QPR_Questionnaire__c oneQuestionnaire = [ select RecordTypeId, Practice__c, Practice_ID__c, Lock__c, Status__c from QPR_Questionnaire__c where  Practice_ID__c = '12345'];
        ApexPages.currentPage().getParameters().put('id',oneQuestionnaire.Id);
        String relURL =  ApexPages.currentPage().getUrl()+'/e' ;
        PageReference pageRef2 = new PageReference(relURL+'/e');
        Test.setCurrentPage(pageRef2);
        system.debug('relURL'+relURL);
        test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(oneQuestionnaire);
        QPRQuestionnaireOverride qprOverride = new QPRQuestionnaireOverride(sc);
        qprOverride.modeEdit();
        String nextPage = qprOverride.generateFormURL().getURL();
        test.stopTest();
    }
    @isTest
    public static void testgenearteFormURL_2(){
        PageReference pageRef = Page.QPRQuestionnaireOverride;
        Test.setCurrentPage(pageRef);
        QPR_Questionnaire__c oneQuestionnaire = [ select RecordTypeId, Practice__c, Practice_ID__c, Lock__c, Status__c from QPR_Questionnaire__c where  Practice_ID__c = '12345'];
        ApexPages.currentPage().getParameters().put('id',oneQuestionnaire.Id);
        String relURL =  ApexPages.currentPage().getUrl()+'/e' ;
        PageReference pageRef2 = new PageReference(relURL+'/e');
        Test.setCurrentPage(pageRef2);
        system.debug('relURL'+relURL);
        test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(oneQuestionnaire);
        QPRQuestionnaireOverride qprOverride = new QPRQuestionnaireOverride(sc);
        qprOverride.bIsPortalUser = true;
        qprOverride.generateFormURL();
        Account objMessage = new Account();
        //qprOverride.addPageMessage() ;
        qprOverride.addPageMessage(objMessage);
        qprOverride.addErrorMessage(objMessage);
        qprOverride.doRedirect();
        QPRQuestionnaireOverride.getBaseUrl();
        test.stopTest();
    }
    
    
}