/***********************************************************************************************************************************************************************
Name: Account_CommunityHandler_Test 
============================================================================================================================== 
Purpose: This class contains code related to Unit Testing and test coverage of class Account_CommunityHandler
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR           DATE          DETAIL     Description 
1.0        Rama Krishna    19/02/2019    Created     This class contains code related to Unit Testing and test coverage of class Account_CommunityHandler 
                                                             
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/
@isTest
public class Account_CommunityHandler_Test{
    
    static RecordType personAccountRecordType = [SELECT Id, IsPersonType FROM RecordType WHERE Name = 'Member' and SObjectType = 'Account' and IsPersonType = true];   
    static Id personAccountRecordTypeId = personAccountRecordType.Id;   
    
    //create test data using @testsetup annotation
    @testSetup static void createTestData() {
        test_insertTestData();
    }
    static void test_insertTestData(){ 
        List<Account> accList = new List<Account>();   
        Employment_History__c eh = new Employment_History__c();
        accList = TestObjectCreator.createPersonAccountBulk(25,personAccountRecordTypeId);
        Integer i=1;
        for(Account acc:accList){
            
            if(i==1){
                acc.Create_Portal_User__c=false;            
            }
            else{
                acc.Create_Portal_User__c=true;
            }
            acc.Membership_Class__c='Provisional';            
            i++;     
        } 
        
        insert accList;             

        //Insert test data for Employement History     
        eh.Member__c=accList[0].Id;
        eh.Employer__c=accList[1].Id;
        eh.Employee_Start_Date__c = system.today();
        insert eh;
   } 

//******************************************************************************************
//                             testMethods
//******************************************************************************************  
    
    //test method which checks whether Community user is created after the updation of Account 
    static testMethod void checkCommunityHandler() { 
        Test.startTest();
        Profile profile1 = [Select Id from Profile where name = 'CAANZ Business Management User'];
        System.debug('What is the profile id ' + profile1);
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        User u = new User( UserRoleId = portalRole.Id,
                            ProfileId = profile1.Id,
                            Username = 'testtermsconditions1234423@charteredaccountantsanz.com',
                            Alias = 'batman',
                            Email='testtermsconditions1234423@charteredaccountantsanz.com',
                            EmailEncodingKey='UTF-8',
                            Firstname='Bruce',
                            Lastname='Wayne',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_NZ',
                            TimeZoneSidKey='Pacific/Auckland');
        insert u;                             
        System.runas(u){
            Account psacc = [Select Customer_ID__c,PersonEmail,Membership_Class__c,Create_Portal_User__c from Account limit 1];                                                     
            psacc.Create_Portal_User__c=true; 
            psacc.PersonEmail = 'testemailupdate@example.com';
            psacc.Membership_Class__c='Affiliate';                                                                         
            update psacc;                       

            Profile p = [Select Id from Profile where Name='NZICA Community Login User'];
            List<User> userList = [Select id from User where ProfileID=:p.Id limit 25];
            system.assertEquals(true, userList.size()>0);              
        }
       Test.stopTest(); 
    }
}