@isTest
public class luana_MassCDLControllerTest {

    private static Boolean DEBUG = true;
    
    private static Map<String,Id> accountRecordTypeMap;
    private static Map<String,Id> courseRecordTypeMap;
    private static Map<String,Id> programOfferingRecordTypeMap;
    private static Map<String, Account> personAccountMap;
    private static Map<String, Account> businessAccountMap;
    private static Map<String, LuanaSMS__Program__c> programMap;
    private static Map<String, LuanaSMS__Program_Offering__c> programOfferingMap;
    private static Map<String, Program_Offering_Related__c> programOfferingRelatedMap;
    private static Map<String, LuanaSMS__Course__c> courseMap;
    private static Map<String, LuanaSMS__Training_Organisation__c> trainingOrgMap;
    private static Map<String, LuanaSMS__Delivery_Location__c> deliveryLocationMap;
    private static Map<String, Course_Delivery_Location__c> courseDeliveryLocationMap;

    static testMethod void testPageInitialization() {

        testDataGenerator();

        Test.startTest();

        PageReference pg1 = Page.luana_MassCDLPage1;
        
        if (debug) system.debug('## TEST ## courseMap: ' +courseMap);
        
        pg1.getParameters().put('id', courseMap.get('AM Course1').Id);
        Test.setCurrentPage(pg1);
                    
        luana_MassCDLController newController = new luana_MassCDLController();
        
        //KH Added to improve converage
        newController.searchTermName = 'name';
        newController.searchTermState = 'NSW';
        newController.searchTermCountry = 'Australia';
        
        newController.searchRecord();
        newController.getWrappedDL();
        newController.getDays();
        newController.getSelectedItemExist();
        
        newController.cancel();
        newController.nextToPage2();
        newController.nextToPage3();
        newController.nextToPage4();
        newController.pageDone();
        
        newController.Previous();
        newController.Next();
        newController.Beginning();
        newController.End();        
        
        Test.stopTest();        
    }
    
    static testMethod void testPageSuccessfulRun() {
        
        testDataGenerator();
        
        Test.startTest();
        
        PageReference pg1 = Page.luana_MassCDLPage1;
        pg1.getParameters().put('id', courseMap.get('AM Course1').Id);
        Test.setCurrentPage(pg1);
        
        luana_MassCDLController newController = new luana_MassCDLController();
        newController.selectedType = 'Workshop Location';
        newController.dummyPaymentToken.Course__c = courseMap.get('AM Course2').Id;
        //KH to improve test class converage
        newController.dummyPaymentToken.Course_Lookup__c = courseMap.get('AM Course2').Id;
        
        PageReference pg2 = newController.nextToPage2();
        Test.setCurrentPage(pg2);
        
        List<luana_MassCDLController.objectWrapper> checkWrappedObject = new List<luana_MassCDLController.objectWrapper>();
        for (String idString : newController.queryResultHolderMap.keySet()) {
            checkWrappedObject.add(newController.queryResultHolderMap.get(idString));
        }

        if (debug) {
            system.debug('## TEST ## checkWrappedObject: ' +checkWrappedObject);
        }

        system.assert(!checkWrappedObject.isEmpty(), 'Wrapper map did not contain any search result during MassCDLController test');
        
        checkWrappedObject[0].selected = true;
        
        newController.nextToPage3();
        
        if (debug) {
            // system.debug('## TEST ## wrappedObject' +)
        }
        
        Test.stopTest();
    }
    
    private static void testDataGenerator() {
        
        // Record Type map
        accountRecordTypeMap = recordTypeMapper('Account');
        courseRecordTypeMap = recordTypeMapper('LuanaSMS__Course__c');
        programOfferingRecordTypeMap = recordTypeMapper('LuanaSMS__Program_Offering__c');
                        
        // Business Account (Employer)
        businessAccountMap = businessAccountGenerator(accountRecordTypeMap);
        
        // Personal Account (Employee that will be enrolled to the main course)
        personAccountMap = personAccountGenerator(accountRecordTypeMap, businessAccountMap.get('BusinessAccount1').Id);
        
        // Program
        programMap = programGenerator();
        
        // Program Offering
        programOfferingMap = programOfferingGenerator(programMap, programOfferingRecordTypeMap);
        
        // Course
        courseMap = courseGenerator(programOfferingMap, courseRecordTypeMap);
        
        // Training Organisaiton
        trainingOrgMap = trainingOrgGenerator();
        
        // Delivery Location
        deliveryLocationMap = deliveryLocationGenerator(trainingOrgMap);
        
        // Course Delivery Location
        courseDeliveryLocationMap = courseDeliveryLocationGenerator(courseMap, deliveryLocationMap);

    }
    
    private static Map<String, Account> personAccountGenerator(Map<String, Id> recordTypeMap, Id employerID) {

        Map<String, Account> paMap = new Map<String, Account>();
        
        Account pa1 = new Account();
        pa1.RecordTypeId = recordTypeMap.get('Full_Member');
        pa1.LastName = 'C';
        pa1.FirstName = 'BRON';
        pa1.PersonEmail = 'TestEmail1@gmail.com';
        pa1.Salutation = 'TestSalutation';
        pa1.Primary_Employer__c = employerID;
        pa1.Communication_Preference__c= 'Home Phone';
        pa1.PersonHomePhone= '1234';
        pa1.PersonOtherStreet= '83 Saggers Road';
        pa1.PersonOtherCity='JITARNING';
        pa1.PersonOtherState='Western Australia';
        pa1.PersonOtherCountry='Australia';
        pa1.PersonOtherPostalCode='6365';  
                
        Account pa2 = new Account();
        pa2.RecordTypeId = recordTypeMap.get('Full_Member');
        pa2.LastName = 'D';
        pa2.FirstName = 'BRON';
        pa2.PersonEmail = 'TestEmail2@gmail.com';
        pa2.Salutation = 'TestSalutation';
        pa2.Primary_Employer__c = employerID;
        pa2.Communication_Preference__c= 'Home Phone';
        pa2.PersonHomePhone= '1234';
        pa2.PersonOtherStreet= '83 Saggers Road';
        pa2.PersonOtherCity='JITARNING';
        pa2.PersonOtherState='Western Australia';
        pa2.PersonOtherCountry='Australia';
        pa2.PersonOtherPostalCode='6365'; 
        List<Account> accountList = new List<Account>();
        accountList.add(pa1);
        accountList.add(pa2);
                
        if(DEBUG) system.debug('##TEST## - pa accountList: ' +accountList);
        insert accountList;

        List<Account> paListCheck = [SELECT Id, LastName, FirstName, PersonEmail, Salutation, RecordTypeId, PersonContactId FROM Account WHERE Id in :accountList];
        for (Account pa : paListCheck) {
            
            system.debug('paListCheck: ' +paListCheck);
            
            if (pa.Id == pa1.Id) paMap.put('massCDLPersonAccount1', pa);
            if (pa.Id == pa2.Id) paMap.put('massCDLPersonAccount2', pa);
        }

        system.debug('paListCheck: ' +paListCheck); 
            
        return paMap;
    }
    
    private static Map<String, Account> businessAccountGenerator(Map<String, Id> recordTypeMap) {
        
        Map<String, Account> baMap = new Map<String, Account>();
        
        Account ba1 = new Account();
        ba1.RecordTypeId = recordTypeMap.get('Business_Account');
        ba1.Name = 'STAR1';
        ba1.BillingStreet = 'massCDL Test Address 1';
        
        List<Account> accountList = new List<Account>();
        accountList.add(ba1);
        
        if(DEBUG) system.debug('##TEST## - ba accountList: ' +accountList);
        insert accountList;
        
        baMap.put('BusinessAccount1', ba1);
        
        return baMap;       
    }
    
    private static Map<String, LuanaSMS__Program__c> programGenerator() {
        
        Map<String, LuanaSMS__Program__c> newProgramMap = new Map<String, LuanaSMS__Program__c>();
        
        LuanaSMS__Program__c program1 = new LuanaSMS__Program__c();
        program1.name = 'massCDLPROGRAM1';
        
        List<LuanaSMS__Program__c> programList = new List<LuanaSMS__Program__c>();
        programList.add(program1);
        
        if (DEBUG) system.debug('##TEST## - programList: ' +programList);
        insert programList;
        
        newProgramMap.put('Program1', program1);

        return newProgramMap;
    }
    
    private static Map<String, LuanaSMS__Program_Offering__c> programOfferingGenerator(Map<String, LuanaSMS__Program__c> pMap, Map<String, Id> pRTMap) {
        
        Map<String, LuanaSMS__Program_Offering__c> newProgramOfferingMap = new Map<String, LuanaSMS__Program_Offering__c>();
        
        LuanaSMS__Program_Offering__c newPO1 = new LuanaSMS__Program_Offering__c();
        newPO1.name = 'massCDLPROGRAMOFFERING1';
        newPO1.LuanaSMS__Program__c = pMap.get('Program1').Id;
        newPO1.RecordTypeID = pRTMap.get('Accredited_Module');
        
        
        LuanaSMS__Program_Offering__c newPO2 = new LuanaSMS__Program_Offering__c();
        newPO2.name = 'massCDLPROGRAMOFFERING2';
        newPO2.LuanaSMS__Program__c = pMap.get('Program1').Id;
        newPO2.RecordTypeID = pRTMap.get('Accredited_Module');
        
        List<LuanaSMS__Program_Offering__c> programOfferingList = new List<LuanaSMS__Program_Offering__c>();
        programOfferingList.add(newPO1);
        programOfferingList.add(newPO2);
        
        if (DEBUG) system.debug('##TEST## - programOfferingList: ' +programOfferingList);
        insert programOfferingList;
        
        newProgramOfferingMap.put('AM ProgramOffering1', newPO1);
        newProgramOfferingMap.put('AM ProgramOffering2', newPO2);
        
        return newProgramOfferingMap;
    }
    
    private static Map<String, LuanaSMS__Course__c> courseGenerator(Map<String, LuanaSMS__Program_Offering__c> poMap, Map<String, Id> courseRTMap) {
        
        Map<String, LuanaSMS__Course__c> newCourseMap = new Map<String, LuanaSMS__Course__c>();
        
        LuanaSMS__Course__c newCourse1 = new LuanaSMS__Course__c();
        newCourse1.name = 'massCDLCOURSE1';
        newCourse1.LuanaSMS__Program_Offering__c = poMap.get('AM ProgramOffering1').Id;
        newCourse1.RecordTypeID = courseRTMap.get('Accredited_Module');
        newCourse1.LuanaSMS__Status__c = 'Running';
        
        LuanaSMS__Course__c newCourse2 = new LuanaSMS__Course__c();
        newCourse2.name = 'massCDLCOURSE2';
        newCourse2.LuanaSMS__Program_Offering__c = poMap.get('AM ProgramOffering2').Id;
        newCourse2.RecordTypeID = courseRTMap.get('Accredited_Module');
        newCourse2.LuanaSMS__Status__c = 'Running';
        
        List<LuanaSMS__Course__c> courseList = new List<LuanaSMS__Course__c>();
        courseList.add(newCourse1);
        courseList.add(newCourse2);
        
        if (DEBUG) system.debug('##TEST## - courseList: ' +courseList);
        insert courseList;
        
        newCourseMap.put('AM Course1', newCourse1);
        newCourseMap.put('AM Course2', newCourse2);
        
        return newCourseMap;
    }    
    
    private static Map<String, LuanaSMS__Student_Program__c> studentProgramGenerator(Id mainCourseId, List<Account> studentAccountList) {
        
        Map<String, LuanaSMS__Student_Program__c> spMap = new Map<String, LuanaSMS__Student_Program__c>();
        
        List<LuanaSMS__Student_Program__c> spList = new List<LuanaSMS__Student_Program__c>();
        Integer i = 1;
        for (Account pa : studentAccountList) {
            LuanaSMS__Student_Program__c studentProgram1 = new LuanaSMS__Student_Program__c();          
            studentProgram1.LuanaSMS__Course__c = mainCourseId;
            studentProgram1.LuanaSMS__Contact_Student__c = pa.PersonContactId;
            studentProgram1.Paid__c = true;
            studentProgram1.LuanaSMS__Status__c = 'In Progress';
            spList.add(studentProgram1);
            spMap.put('StudentProgram' + i, studentProgram1);
            i++;
        }
        insert spList;
        
        return spMap;
    }
    
    private static Map<String, LuanaSMS__Training_Organisation__c> trainingOrgGenerator() {
        
        Map<String, LuanaSMS__Training_Organisation__c> toMap = new Map<String, LuanaSMS__Training_Organisation__c>();
        
        LuanaSMS__Training_Organisation__c to1 = new LuanaSMS__Training_Organisation__c();
                
        List<LuanaSMS__Training_Organisation__c> toList = new List<LuanaSMS__Training_Organisation__c>();
        toList.add(to1);
        
        if (debug) system.debug('##TEST## - toList: ' +toList);
        insert toList;
        
        toMap.put('Training Organisation 1', to1);
        return toMap;
    }
    
    private static Map<String, LuanaSMS__Delivery_Location__c> deliveryLocationGenerator(Map<String, LuanaSMS__Training_Organisation__c> toMap) {
        
        Map<String, LuanaSMS__Delivery_Location__c> dlMap = new Map<String, LuanaSMS__Delivery_Location__c>();
        
        LuanaSMS__Delivery_Location__c dl1 = new LuanaSMS__Delivery_Location__c();
        dl1.LuanaSMS__Training_Organisation__c = toMap.get('Training Organisation 1').Id;
        
        LuanaSMS__Delivery_Location__c dl2 = new LuanaSMS__Delivery_Location__c();
        dl2.LuanaSMS__Training_Organisation__c = toMap.get('Training Organisation 1').Id;
        
        List<LuanaSMS__Delivery_Location__c> dlList = new List<LuanaSMS__Delivery_Location__c>();
        dlList.add(dl1);
        dlList.add(dl2);
        
        if (debug) system.debug('##TEST## - dlList: ' +dlList);
        insert dlList;
        
        dlMap.put('Delivery Location 1', dl1);
        dlMap.put('Delivery Location 2', dl2);
        return dlMap;
    }
    
    private static Map<String, Course_Delivery_Location__c> courseDeliveryLocationGenerator(Map<String, LuanaSMS__Course__c> cMap, Map<String, LuanaSMS__Delivery_Location__c> dlMap) {
        
        Map<String, Course_Delivery_Location__c> cdlMap = new Map<String, Course_Delivery_Location__c>();
        
        Course_Delivery_Location__c cdl1 = new Course_Delivery_Location__c();
        cdl1.Course__c = cMap.get('AM Course2').Id;
        cdl1.Delivery_Location__c = dlMap.get('Delivery Location 2').Id;
        cdl1.Type__c = 'Workshop Location';
        cdl1.Availability_Day__c = 'Monday;Tuesday';
        
        List<Course_Delivery_Location__c> cdlList = new List<Course_Delivery_Location__c>();
        cdlList.add(cdl1);
        
        if (debug) system.debug('##TEST## - cdlList: ' +cdlList);
        insert cdlList;
        
        cdlMap.put('Course Deliveyr Location 1', cdl1);
        return cdlMap;
    }

    private static Map<String, Id> recordTypeMapper(String objName) {
        List<RecordType> recordTypeList = [SELECT Id, DeveloperName FROM RecordType WHERE SObjectType = :objName];
        if (!recordTypeList.isEmpty()) {
            Map<String, Id> recordTypeMap = new Map<String, Id>();
            for (RecordType rt : recordTypeList) {
                recordTypeMap.put(rt.DeveloperName, rt.Id);
            }
            return recordTypeMap;
        } else {
            return null;
        }
    }
}