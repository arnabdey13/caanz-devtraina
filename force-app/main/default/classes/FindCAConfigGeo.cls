/*
    API for FindCA - Geographical Configuration Country -> Branches
    October 2017   CAANZ - akopec  

    Example: /services/apexrest/FindCA/v1.0/Config/Geo?BranchCountry=
*/
@RestResource(urlMapping='/FindCA/v1.0/Config/Geo/*')
global with sharing class FindCAConfigGeo {

@HttpGet
  global static List<Region_State_Setting__mdt> doGet() {
      List<Region_State_Setting__mdt> locations;
      if (!String.isBlank(RestContext.request.params.get('BranchCountry'))){
      	String vBranchCountry = RestContext.request.params.get('BranchCountry');
        locations = [Select Label, Country__c From Region_State_Setting__mdt where Country__c = :vBranchCountry];
      }
      else
		locations = [Select Label, Country__c From Region_State_Setting__mdt];
    return locations;
  }
}