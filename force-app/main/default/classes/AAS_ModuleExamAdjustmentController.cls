/* 
    Developer: WDCi (KH)
    Development Date: 31/03/2017
    Task #: AAS split the adjustment for Module Adjustnment(Capston) and Exam Adjustment(Technical). 
            This change will include standardize the AAS_FinalAdjustmentController class for Module & Exam controller class
*/
public with sharing class AAS_ModuleExamAdjustmentController {
    
    //Moved from AAS_FinalAdjustmentController class
    public Boolean validationAction(AAS_Course_Assessment__c courseAssessment, String selectedRecordType){
        Boolean validToProcess = false;
        
        System.debug('************:::: ' + courseAssessment + ' - ' + selectedRecordType);
        
        if(selectedRecordType == 'Exam_Assessment_SAS'){
            
            //Schema.DescribeSObjectResult R = AAS_Course_Assessment__c.SObjectType.getDescribe();
            //List<Schema.RecordTypeInfo> RT = R.getRecordTypeInfosByName();
            
            Id techRTId = Schema.SObjectType.AAS_Course_Assessment__c.getRecordTypeInfosByName().get('Technical Module').getRecordTypeId();
            Id capstoneRTId = Schema.SObjectType.AAS_Course_Assessment__c.getRecordTypeInfosByName().get('Capstone Module').getRecordTypeId();
            
            if(courseAssessment.RecordTypeId == capstoneRTId){
                if(!courseAssessment.AAS_Final_Adjustment_Exam__c){
                    if(courseAssessment.AAS_Cohort_Adjustment_Exam__c && courseAssessment.AAS_Borderline_Remark__c){
                        validToProcess = true;
                    }else{
                        validToProcess = false;
                        //readyToCommit = false;
                        addPageMessage(ApexPages.severity.ERROR, 'You are not allow to continue in this step. Please complete the Cohort Adjustment & Borderline remark processes first.');
                    }
      
                }else{
                    validToProcess = false;
                    addPageMessage(ApexPages.severity.WARNING, 'Module Adjustment was commited before. You are not allow to re-commit again.');        
                }
            }else if(courseAssessment.RecordTypeId == techRTId){
                if(!courseAssessment.AAS_Exam_Adjustment__c){
                    if(courseAssessment.AAS_Cohort_Adjustment_Exam__c && courseAssessment.AAS_Borderline_Remark__c){
                        validToProcess = true;
                    }else{
                        validToProcess = false;
                        //readyToCommit = false;
                        addPageMessage(ApexPages.severity.ERROR, 'You are not allow to continue in this step. Please complete the Cohort Adjustment & Borderline remark processes first.');
                    }
      
                }else{
                    validToProcess = false;
                    addPageMessage(ApexPages.severity.WARNING, 'Exam Adjustment was commited before. You are not allow to re-commit again.');        
                }
            }
        
            
        }
        /* Currently we are not handling Supp Exam Assessment
        else if(selectedRecordType == 'Supp_Exam_Assessment_SAS'){
            if(!courseAssessment.AAS_Final_Adjustment_Supp_Exam__c){
                if(courseAssessment.AAS_Cohort_Adjustment_Supp_Exam__c && courseAssessment.AAS_Borderline_Remark_Supp_Exam__c){
                    validToProcess = true;
                } else{
                    validToProcess = false;
                    //readyToCommit = false;
                    addPageMessage(ApexPages.severity.ERROR, 'You are not allow to continue in this step. Please complete the Cohort Adjustment & Borderline remark processes first.');
                }
            }else{
                addPageMessage(ApexPages.severity.WARNING, 'Final Adjustment was commited before. If you re-commit again, previous Final Adjustment data will not be undo.');
            }
        }*/
        return validToProcess;
    }
    
    //Moved from AAS_FinalAdjustmentController class
    public void addPageMessage(ApexPages.severity sev, String msg){
        ApexPages.Message warningmsg = new ApexPages.Message(sev, msg);
        ApexPages.addmessage(warningmsg);
    }
    
    //Moved from AAS_FinalAdjustmentController class
    public PageReference cancelAction(AAS_Course_Assessment__c courseAssessment){
        PageReference returnPage = new PageReference('/'+courseAssessment.Id);
        return returnPage;
    }   
    
    public PageReference completeAction(AAS_Course_Assessment__c courseAssessment){
        AAS_Course_Assessment__c ca = new AAS_Course_Assessment__c();
        ca.Id = courseAssessment.Id;
        ca.AAS_Final_Adjustment_Exam__c = true;
        update ca;
    
        PageReference returnPage = new PageReference('/'+courseAssessment.Id);
        return returnPage;
    }
    
    //Moved from AAS_FinalAdjustmentController class ('Exam_Assessment')
    public List<SelectOption> getRecordTypeOptions(AAS_Course_Assessment__c courseAssessment, String recordTypeDevName) {
        
        Schema.DescribeFieldResult statusFieldDescription = RecordType.DeveloperName.getDescribe();
        List<SelectOption> statusOptions = new list<SelectOption>();
        
        for(AAS_Assessment_Schema_Section__c ass: [Select RecordType.Name, RecordType.developerName
                                                    from AAS_Assessment_Schema_Section__c 
                                                    Where AAS_Course_Assessment__c =: courseAssessment.Id and RecordType.DeveloperName =: recordTypeDevName]){
            statusOptions.add(new SelectOption(ass.RecordType.developerName+'_SAS', ass.RecordType.Name));
        }

        return statusOptions;
    }
    
}