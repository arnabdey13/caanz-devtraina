/**
    Developer: WDCi (KH)
    Development Date: 21/03/2017
    Task #: Controller for luana_LifelongLearningAvailableCourses vf component
    
**/
public with sharing class luana_LifelongLearningAvailableCoursesCp {
    
    public Map<Id, String> urlMap {get; set;}
    
    public luana_LifelongLearningAvailableCoursesCp(){
        urlMap = new Map<Id, String>();
    }
    
    public List<LuanaSMS__Course__c> getAvailableCourses() {
        List<LuanaSMS__Course__c> courses = [select Id, Name, Description__c, Course_Detail__c, LuanaSMS__Allow_Online_Enrolment__c, LuanaSMS__Program_Offering__c from LuanaSMS__Course__c 
            WHERE LuanaSMS__Allow_Online_Enrolment__c = true AND RecordType.DeveloperName = 'Lifelong_Learning' AND LuanaSMS__Status__c = 'Running' LIMIT 200]; 
            
        for(LuanaSMS__Course__c c: courses){
        
            urlMap.put(c.Id, getViewURL() + '?producttype='+luana_EnrolmentConstants.PRODUCTTYPE_LLL+'&poid=' + c.LuanaSMS__Program_Offering__c + '&courseid='+c.id);
        }
            
        return courses;        
    }
    
    public String getViewURL(){
        
        return luana_NetworkUtil.getCommunityPath() + '/luana_EnrolmentWizardLanding';
    }
 
}