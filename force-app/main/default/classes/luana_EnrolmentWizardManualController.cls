/* 
    Developer: WDCi (KH)
    Date: 11/May/2016
    Task #: Luana implementation - Wizard for Late Enrolment
*/

public with sharing class luana_EnrolmentWizardManualController {
    
    public LuanaSMS__Course__c course {get; set;}
    public LuanaSMS__Student_Program__c newSPObj {get; set;}
    
    public luana_EnrolmentWizardManualController (ApexPages.StandardController controller){
        
        if(Test.isRunningTest()){
            course = (LuanaSMS__Course__c) controller.getRecord();
            course = [select id, name, LuanaSMS__Program_Offering__c, LuanaSMS__Program_Offering__r.Product_Type__c from LuanaSMS__Course__c where Id =: course.Id];
        } else {
            //this.acct = (LuanaSMS__Course__c)controller.getRecord();
            controller.addFields(new List<String>{'Id', 'Name', 'LuanaSMS__Program_Offering__c', 'LuanaSMS__Program_Offering__r.Product_Type__c'});
            this.course = (LuanaSMS__Course__c) controller.getRecord();
        }
        
        newSPObj = new LuanaSMS__Student_Program__c();
    }
        
    public pageReference nextEnrol(){
        
        PageReference pageRef = Page.luana_EnrolmentWizardLanding;
        pageRef.getParameters().put('poid', course.LuanaSMS__Program_Offering__c);
        pageRef.getParameters().put('producttype', course.LuanaSMS__Program_Offering__r.Product_Type__c);
        pageRef.getParameters().put('courseid', course.id);
        pageRef.getParameters().put('contactid', newSPObj.LuanaSMS__Contact_Student__c);
        pageRef.getParameters().put('applylateenrol', String.valueOf(newSPObj.Apply_Late_Enrolment_Fee__c));
        
        if(getRetURLFromURL() != null){
        	pageRef.getParameters().put('retURL', getRetURLFromURL());
        }
        
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    private String getRetURLFromURL(){
        if(ApexPages.currentPage().getParameters().containsKey('retURL')){
            return ApexPages.currentPage().getParameters().get('retURL');
            
        }
        
        return null;
    }
}