global class RetrieveObject 
{
    
    global class objectData
    {
        webservice string customerId;
        webservice string memberId;
        webservice string name;
        webservice string email;
        webservice boolean bExists;   
    }

   webservice static objectData GetObject(string memberID, string SFID, string objectType)
   {
       try
       {    objectData oObject = new objectData();
            oObject.bExists = false;
        
           if(objectType.toLowerCase() == 'user')
           {
               User queryResult = [SELECT Name, Email from user where ID =:SFID Limit 1];
              
               oObject.name = queryResult.Name;
               oObject.email = queryResult.Email;
               oObject.bExists = true;
                              
           }
           else if (objectType.toLowerCase() == 'account')
           {
               Account queryResult = [SELECT ID, Name,  PersonEmail, Customer_ID__c, Member_ID__c from account where Member_ID__C =:memberID Limit 1];
                           
               oObject.name = queryResult.Name;
               oObject.email = queryResult.PersonEmail;
               oObject.customerId = queryResult.Customer_ID__c;
               oObject.memberId = queryResult.Member_ID__c;
               oObject.bExists = true;
           }
            return oObject;
       }
       catch (Exception e)
       {
                System.debug('An exception occurred: ' + e.getMessage());
                
                objectData oObject= new objectData();
                
                oObject.customerId = '0';
                oObject.bExists = false;
                
                return oObject;
       }
   }
        
}