/**
 * Class to refresh Segmentation
 **/ 
global class SegmentationRefresh Implements Database.Batchable <Sobject> {
          
// 
// SegmentationRefresh objClass = new SegmentationRefresh();
// Database.executeBatch (objClass, 10);
// 
// Test status of the Async job
// SELECT ApexClassId,CompletedDate,CreatedById,CreatedDate,ExtendedStatus,Id,JobItemsProcessed,JobType,LastProcessed,LastProcessedOffset,MethodName,NumberOfErrors,ParentJobId,Status 
// FROM AsyncApexJob

	global  String query;
    global  Boolean debug=false;
    global List<ID> accountsToUpdate;
    // ????? global final Map<String, ID> main_prcts_lkp;
    String email;
    
	// constructor to populate query
    public SegmentationRefresh(List<ID> accountsIdToUpdate ){
        accountsToUpdate = accountsIdToUpdate;
        query =  ' Select ID, Name ';
        query += ' From Account Where Id IN :accountsToUpdate ';
    }
    
    // constructor to populate query (Member Only)
    public SegmentationRefresh(){
        query =  ' Select ID, Name From Account Where Membership_Type__c = \'Member\' And Status__c = \'Active\' ';
    }

    // constructor to populate query
    // Where Status__c = \'Active\' And ( Membership_Type__c = \'Member\' Or Id in (Select Account__c from Segmentation__c) )';
    public SegmentationRefresh(String whereQry){
        query =  ' Select ID, Name From Account ';
        query += whereQry; 
    }    

    // constructor to populate query with Where Clause and debug flag
    public SegmentationRefresh(String whereQry, Boolean debugFlag){
        debug = debugFlag;
        query =  ' Select ID, Name From Account ';
        query += whereQry; 
    }        
    
	// start the batch
    global Database.QueryLocator start(Database.BatchableContext BC){ 
        System.debug('Update Started');
        return Database.getQueryLocator(query); 
    } 

    
    global void execute(Database.BatchableContext BC, List<sObject> scope){        
        List<Segmentation__c> segmentsToUpdateList = new List <Segmentation__c>();
        
        for (sObject objScope: scope) {     
            //Account newObjScope = (Account) objScope ;
            Id key = (Id) objScope.get('Id');          
            
            Map<Id, Map<String, String>> segments = SegmentationCAANZCategories.getSegment(new List<Id>{key});
			if(debug) System.debug('Batch Refresh: New Segments:' + segments);
            if (segments.isEmpty())
                continue;
            Map<String, String> cat = segments.get(key);
            if (cat.isEmpty())
                continue;
            
            List<Segmentation__c> segmentsUpdate  = [select Id, Account__c, Business_Segmentation_MS__c, Organisation_Type__c, Firm_Type__c, Firm_Office_Size__c, Career_Stage__c
                                                     from Segmentation__c 
                                                     where Account__c = :key ]; 
			if(debug) System.debug('Batch Refresh: Old Segments:' + segmentsUpdate);
            
            Segmentation__c newSegment;         
            if ( !segmentsUpdate.isEmpty())
                newSegment = segmentsUpdate[0]; 
            ///////////////////////////////////////////////// NOT A MEMBER ////////////////////////////////////
            // We need to clear the segment if exists
            if (cat.get('Segment') == 'Not a Member'){              
                if ( newSegment==NULL) // not existing segment
                    continue;
                // existing Segment - clear all fields
                newSegment.Organisation_Type__c = NULL; 
                newSegment.Firm_Type__c = NULL; 
                newSegment.Firm_Office_Size__c = NULL; 
                newSegment.Career_Stage__c = NULL;       
                newSegment.Business_Segmentation_MS__c = NULL; 
                segmentsToUpdateList.add(newSegment);
                continue;
            }
            ///////////////////////////////////////////////// THIS IS MEMBER ////////////////////////////////////
            if ( newSegment==NULL) {
                newSegment = new Segmentation__c();
                newSegment.Account__c = key;              
            }  
            newSegment.Organisation_Type__c = cat.get('Organisation Type'); 
            newSegment.Firm_Type__c = cat.get('Firm Type'); 
            newSegment.Firm_Office_Size__c = cat.get('Firm Office Size'); 
            newSegment.Career_Stage__c = cat.get('Career Stage');      
            newSegment.Business_Segmentation_MS__c = cat.get('Segment');       
            segmentsToUpdateList.add(newSegment);
	}   
        
	if ( !segmentsToUpdateList.isEmpty()) {
		upsert segmentsToUpdateList;   
	}
   
} 
    
    
    global void sendMail(){ 
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
        mail.setToAddresses(new String[] {email}); 
        mail.setReplyTo('andrew.kopec@charteredaccountantsanz.com'); 
        mail.setSenderDisplayName('Practice Recognition Batch Processing'); 
        mail.setSubject('Batch Process Completed'); 
        mail.setPlainTextBody('Batch Process has completed'); 
        // if (!Test.isRunningTest())
        //	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
    } 

 
	global void finish(Database.BatchableContext BC){ 
        sendMail();
        System.debug('Update Completed');
    }    
    
}