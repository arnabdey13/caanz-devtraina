/**
* @author Jannis Bott
* @date 09/01/2017
*
* @description  implements static methods that populate the platform cache and return cached data
*/
public with sharing class PlatformCacheUtil {

    /**
    * @description  should be called to set the cache with a specific namespace
    * @param        String partition - the partition the data resides in
    * @param        String key - the namespace given to the data within the partition
    * @param       Object data - a map of the data to the object
    */
    private static void setOrgCache(String partition, String key, Object data) {

        //get the correct partition within the cache
        Cache.OrgPartition concretePartition = getPartition(partition);

        //set the cache with the data and namespace reference
        concretePartition.put(key, data);
    }

    public static Object readOrgCache(String partition, String key, ObjectFactory factory) {

        //get the correct partition within the cache
        Cache.OrgPartition concretePartition = getPartition(partition);

        //clear previously stored data
        if (concretePartition.contains(key)) {
            System.debug('CACHE HIT');
            return concretePartition.get(key);
        } else {
            System.debug('CACHE MISS');
            Object data = factory.get();
            setOrgCache(partition, key, data);
            return data;
        }
    }

    /**
    * @description  should be called to get a specific partition
    * @param        String partitionName - the name of the partition
    */
    private static Cache.OrgPartition getPartition(String partitionName) {
        return Cache.Org.getPartition(partitionName);
    }

    public interface ObjectFactory {
        Object get();
    }
}