/**
    Developer: WDCi (Lean)
    Development Date: 05/04/2016
    Task #: Enrollment wizard - Controller for Details Foundation
    
    Change History:
    LCA-534 15/06/2016: WDCi Lean - remove require assistance related fields and logic
**/

public without sharing class luana_EnrolmentWizardDetailsFDController extends luana_EnrolmentWizardObject{
    
    luana_EnrolmentWizardController stdController;
    
    public luana_EnrolmentWizardDetailsFDController(luana_EnrolmentWizardController stdController){
        this.stdController = stdController;
    }
    
    public luana_EnrolmentWizardDetailsFDController(){
    
    }
    
    public PageReference detailsFDNext(){
        /*LCA-534
        if(stdController.workingStudentProgram.Do_you_require_assistance__c == 'Yes' && (stdController.workingStudentProgram.Specify_Assistance_Details__c == null || stdController.assistanceAtt.body == null)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please provide the Special Assistance Details and supporting document.'));
            
            return null;
        }*/
        
        if(stdController.selectedExamLocationId != null && stdController.workingStudentProgram.Accept_terms_and_conditions__c){
            stdController.selectedExamLocation = stdController.examLocationMap.get(stdController.selectedExamLocationId).Delivery_Location__r.LuanaSMS__Delivery_Location_Name__c;
			            
            PageReference pageRef = stdController.getDetailsNextPage();
            stdController.skipValidation = true;
            return pageRef;
            
        } else{
            if(!stdController.workingStudentProgram.Accept_terms_and_conditions__c && stdController.selectedExamLocationId != null){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure that the "Accept Terms and Conditions" is selected.'));
            }else if(stdController.selectedExamLocationId == null && stdController.workingStudentProgram.Accept_terms_and_conditions__c){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure the "Please select your exam location preference" is selected.'));
            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure the "Please select your exam location preference" and "Accept Terms and Conditions" are selected.'));
            }
            
            return null;
        }
    }
    
    public PageReference detailsFDBack(){
        
        PageReference pageRef;
        stdController.skipValidation = true;
        if(stdController.selectedProgramOffering.Require_Subject_Selection__c){
            pageRef = Page.luana_EnrolmentWizardSubject;
        } else {
            
            stdController.selectedCourseId = null;
            stdController.selectedCourse = null;
            
            pageRef = Page.luana_EnrolmentWizardCourse;
        }
        
        return pageRef;
    }
        
    public Attachment getCAFoundationTermNCond(){
        return stdController.getTermAndConAttachmentFile(false);
    }
}