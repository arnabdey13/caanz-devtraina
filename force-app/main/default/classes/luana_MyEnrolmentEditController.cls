/**
    Developer: WDCi (KH)
    Development Date: 21/04/2016
    Task #: Controller for luana_MyEnrolmentEdit vf page LCA-223
    
    Change History
    LCA-711 WDCi - KH: 08/07/2016 change to include workshop location edit fields
    LCA-705&LCA-800 WDCi - KH: 22/07/2016 to enable non-member for edit
    LCA-553/LCA-901 10/08/2016 - WDCi Lean: remove noncommunity check
    LCA-1066 06/12/2016 - WDCi Lean: Fix workshop preference day options
    LCA-1161 07/03/2018 - WDCi LKoh: include Status & Final Module Mark column to the page
**/

public without sharing class luana_MyEnrolmentEditController{
    
    luana_MyEnrolmentViewController stdController;
    
    public Id custCommConId {get; private set;}
    public Id custCommAccId {get; private set;}
    
    public LuanaSMS__Student_Program__c selectedSP {public get; private set;}
    public boolean isValid {public get; private set;}
    
    public Set<String> mentorList {public get; private set;}
    public List<Employment_History__c> employmentHistories {get; set;}
    
    public luana_MyEnrolmentEditController(luana_MyEnrolmentViewController stdController) {
        this.stdController = stdController;
    }
    
    Id selectedCourseId;
    
    public String selectedExamLocationId {get; set;}
    
    public boolean validToChangeExamLoc {get; private set;}
    
    //LCA-711, check isCspstone or not
    public boolean isCapstone {get; set;}
    public boolean validToChangeWorkshopLoc {get; private set;}
    
    public String selectedWorkshopLocation1Id {get; set;}
    public String selectedWorkshopLocation2Id{get; set;}
    
    //LCA-1066
    Map<Id, List<String>> cdlWorkshopDayPrefs;
    
    //LCA-1161
    public boolean communityDisplayEnabled {get; set;}
    
    public luana_MyEnrolmentEditController(){
        
        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId()); 
        this.custCommConId = commUserUtil.custCommConId;       
        this.custCommAccId = commUserUtil.custCommAccId;
        
        isValid = false;
        validToChangeExamLoc = true;
        validToChangeWorkshopLoc = true;
        isCapstone = false;
        
        //LCA-1161
        Luana_Extension_Settings__c luanaExtCS = Luana_Extension_Settings__c.getValues('CommunityDisplayStatus');
        if(luanaExtCS.Value__c == 'True'){
            communityDisplayEnabled = true;
        }else{
            communityDisplayEnabled = false;
        }
        
        //LCA-705&LCA-800 added this into checking luana_NetworkUtil.isNonMemberCommunity()
        if(ApexPages.currentPage().getParameters().containsKey('spid') && (luana_NetworkUtil.isMemberCommunity() /*LCA-901|| luana_NetworkUtil.isNonMemberCommunity()*/ )){
            
             try{
                //LCA-466, Phase 3 Add exam location to the query
                //LCA-711, add in extra fields for Workshop Location 1 & 2 for edit
                selectedSP = [Select Id, Name, Final_Module_Status__c, Exam_Location__c, Exam_Location__r.Name, Select_your_exam_location_preference__c, Paid__c, LuanaSMS__Status__c, Opt_In__c, LuanaSMS__Course__c, LuanaSMS__Course__r.Name, LuanaSMS__Course__r.LuanaSMS__Program_Offering__c, LuanaSMS__Program_Id__c, 
                    LuanaSMS__Program_Name__c, LuanaSMS__Course__r.Exam_Location_Cut_off_Date__c, 
                    Workshop_Location_Preference_1__c, Workshop_Location_Preference_1__r.Name, Day_of_the_Week_Preference_1__c, 
                    Workshop_Location_Preference_2__c, Workshop_Location_Preference_2__r.Name, Day_of_the_Week_Preference_2__c,
                    LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.IsCapstone__c, Line_of_service__c, Organisation_type__c, LuanaSMS__Course__r.Workshop_Location_Cut_Off_Date__c,
                    LuanaSMS__Enrolment_Date__c, LuanaSMS__Contact_Student__c, LuanaSMS__Contact_Student__r.Account.FirstName, LuanaSMS__Contact_Student__r.Account.Preferred_Name__c,
                    LuanaSMS__Contact_Student__r.Account.LastName, Select_your_exam_location_preference__r.Name, LuanaSMS__Contact_Student__r.Account.IsPersonAccount, LuanaSMS__Contact_Student__r.Name,
                    (select id, LuanaSMS__Subject__c, LuanaSMS__Subject__r.Name, Completed__c, Score__c, Result__c, Merit__c from LuanaSMS__Student_Program_Subjects__r) 
                    from LuanaSMS__Student_Program__c Where Id =: ApexPages.currentPage().getParameters().get('spid') and LuanaSMS__Contact_Student__c =: custCommConId];
                System.debug('*****run here: ' + selectedSP);
                selectedCourseId = selectedSP.LuanaSMS__Course__c;
                //LCA-466, Phase 3
                selectedExamLocationId = selectedSP.Exam_Location__c;
                
                //LCA-711
                selectedWorkshopLocation1Id = selectedSP.Workshop_Location_Preference_1__c;
                selectedWorkshopLocation2Id = selectedSP.Workshop_Location_Preference_2__c;
                
                if(selectedSP.LuanaSMS__Course__r.Exam_Location_Cut_off_Date__c < System.today()){
                    validToChangeExamLoc = false;
                }
                
                //LCA-711, include workshop location cut off date check
                if(selectedSP.LuanaSMS__Course__r.Workshop_Location_Cut_Off_Date__c < System.today()){
                    validToChangeWorkshopLoc = false;
                }
                
                isValid = true;
                
                isCapstone = selectedSP.LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.IsCapstone__c;
                
                //LCA-1066 we need to prepare the workshop day pref here -- start
                cdlWorkshopDayPrefs = new Map<Id, List<String>>();
                for(Course_Delivery_Location__c cdl : luana_DeliveryLocationUtil.getCourseWorkshopLocation(selectedCourseId)){
                    List<String> dayPrefs = new List<String>();
                    
                    if(cdl.Availability_Day__c != ''){
                        for(String availDay: cdl.Availability_Day__c.split(';')){
                            dayPrefs.add(availDay);
                        }
                    }
                    
                    cdlWorkshopDayPrefs.put(cdl.Id, dayPrefs);
                }
                
                //we need to load the default options
                getDayofWeekPref1WSLoc();
                getDayofWeekPref2WSLoc();
                //LCA-1066 -- end
                
            }Catch(Exception exp){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'You do not have the permission to view the record. Please try again later or contact our support if the problem persists.'));
            }
        }else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Invalid request. Please try again later or contact our support if the problem persists.'));
        }
    }
    
    public List<SelectOption> getFoundationExamLocationOptions(){
    
        List<SelectOption> options = new List<SelectOption>();
        
        List<Course_Delivery_Location__c> courseDeliveryLocations = luana_DeliveryLocationUtil.getCourseExamDeliveryLocation(selectedCourseId);
        
        for(Course_Delivery_Location__c dl: courseDeliveryLocations){
            //LCA-466, Phase 3
            SelectOption option = new SelectOption(dl.Id, dl.Delivery_Location__r.LuanaSMS__Delivery_Location_Name__c);
            options.add(option); 
        }
        
        return options;
    }//*/
    
    //LCA-711, get workshop location
    public List<SelectOption> getWorkshopLocationOptions(){
    
        List<SelectOption> options = new List<SelectOption>();
        
        List<Course_Delivery_Location__c> courseDeliveryLocations = luana_DeliveryLocationUtil.getCourseWorkshopLocation(selectedCourseId);
        
        options.add(new SelectOption('', '--None--')); //LCA-1066 to leave the field empty if non selected, we will validate this in isValid()
        for(Course_Delivery_Location__c dl: courseDeliveryLocations){
            //LCA-466, Phase 3
            SelectOption option = new SelectOption(dl.Id, dl.Delivery_Location__r.LuanaSMS__Delivery_Location_Name__c);
            options.add(option); 
        }
        
        return options;
    }
    
    public PageReference doCancel(){

        PageReference pRef = new PageReference(luana_NetworkUtil.getCommunityPath() + '/luana_MyEnrolmentView?spid=' + selectedSP.Id);
        return pRef;
    }
    
    
    public PageReference save(){
        if(isValid()){ //LCA-1066
            LuanaSMS__Student_Program__c newSP = new LuanaSMS__Student_Program__c();
            
            newSP.Id = selectedSP.Id;
            newSP.Opt_In__c = selectedSP.Opt_In__c;
            //LCA-466, Phase 3
            newSP.Exam_Location__c = selectedExamLocationId;
            
            //LCA-711
            newSP.Workshop_Location_Preference_1__c = selectedWorkshopLocation1Id;
            newSP.Workshop_Location_Preference_2__c = selectedWorkshopLocation2Id;
            newSP.Day_of_the_Week_Preference_1__c = selectedSP.Day_of_the_Week_Preference_1__c;
            newSP.Day_of_the_Week_Preference_2__c = selectedSP.Day_of_the_Week_Preference_2__c;
            newSP.Line_of_service__c = selectedSP.Line_of_service__c;
            newSP.Organisation_type__c = selectedSP.Organisation_type__c;
             
            update newSP;
            
            PageReference pRef = new PageReference(luana_NetworkUtil.getCommunityPath() + '/luana_MyEnrolmentView?spid=' + selectedSP.Id);
            return pRef;
            
        } else {
            return null;
        }
    }
    
    //LCA-311, get template name
    public String getTemplateName(){
        return luana_NetworkUtil.getTemplateName();
        
        /*LCA-901
        if(luana_NetworkUtil.isMemberCommunity()){
            return 'luana_MemberTemplate';
        } else if(luana_NetworkUtil.isNonMemberCommunity()){
            return 'luana_NonMemberTemplate';
        } else if(luana_NetworkUtil.isInternal()){
            return 'luana_InternalTemplate';
        }
      
        return '';
        */
    }
    
    //LCA-1066 -- start
    public List<SelectOption> dayOfWeekPref1Options {get; private set;}
    public List<SelectOption> dayOfWeekPref2Options {get; private set;}
    
    public void getDayofWeekPref1WSLoc(){
        dayOfWeekPref1Options = new List<SelectOption>();
        dayOfWeekPref1Options.add(new SelectOption('', '--None--'));
        
        if(selectedWorkshopLocation1Id != null && selectedWorkshopLocation1Id != '' && cdlWorkshopDayPrefs.containsKey(selectedWorkshopLocation1Id)){
            for(String dayPref : cdlWorkshopDayPrefs.get(selectedWorkshopLocation1Id)){
                dayOfWeekPref1Options.add(new SelectOption(dayPref, dayPref));
            }
        }
        
        
    }
    
    public void getDayofWeekPref2WSLoc(){
        dayOfWeekPref2Options = new List<SelectOption>();
        dayOfWeekPref2Options.add(new SelectOption('', '--None--'));
        
        if(selectedWorkshopLocation2Id != null && selectedWorkshopLocation2Id != '' && cdlWorkshopDayPrefs.containsKey(selectedWorkshopLocation2Id)){
            for(String dayPref : cdlWorkshopDayPrefs.get(selectedWorkshopLocation2Id)){
                dayOfWeekPref2Options.add(new SelectOption(dayPref, dayPref));
            }
        }
    }
    
    private boolean isValid(){
        boolean valid = true;
        
        if(isCapstone){
            if(selectedWorkshopLocation1Id == null || selectedWorkshopLocation1Id == ''){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure the "Please select your 1st workshop location preference" is selected.'));
                valid = false;
            }
            
            if(selectedWorkshopLocation2Id == null || selectedWorkshopLocation2Id == ''){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure the "Please select your 2st workshop location preference" is selected.'));
                valid = false;
            }
            
            if(selectedSP.Day_of_the_Week_Preference_1__c == null || selectedSP.Day_of_the_Week_Preference_1__c == ''){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure the "Please select your 1st day of the week preference" is selected.'));
                valid = false;
            }
            
            if(selectedSP.Day_of_the_Week_Preference_2__c == null || selectedSP.Day_of_the_Week_Preference_2__c == ''){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please ensure the "Please select your 2st day of the week preference" is selected.'));
                valid = false;
            }
        }
        
        return valid;
    }
    //LCA-1066 -- end
}