/**
    Developer: WDCi (KH)
    Development Date: 12/12/2016
    Reason : Used to check if the AUS status is change to Ready to process, must have attachment
**/

public with sharing class AAS_AssessmentDataUploadHandler {
    
    public static void initiateFlow(List<AAS_Assessment_Data_Upload__c> newList, Map<Id, AAS_Assessment_Data_Upload__c> newMap, List<AAS_Assessment_Data_Upload__c> oldList, Map<Id, AAS_Assessment_Data_Upload__c> oldMap, boolean isInsert, boolean isUpdate, boolean isDelete, boolean isUndelete, boolean isBefore, boolean isAfter){
        
        
        //before update
        if(isBefore){
            if(isUpdate){
                doCheck(newList);
            }
        }
        
        //after insert
        if(isAfter){
            if(isInsert){
                doCheck(newList);
            }
        }
        
    }
    
    public static void doCheck(List<AAS_Assessment_Data_Upload__c> newList){
        
        //Only asign the 'Ready for Processing' record into list to process later
        Map<Id, Boolean> hasAttachmentMap = new Map<Id, Boolean>();
        List<AAS_Assessment_Data_Upload__c> aduList = new List<AAS_Assessment_Data_Upload__c>();
        for(AAS_Assessment_Data_Upload__c adu: newList){
            if(adu.AAS_Status__c == 'Ready for Processing'){
                aduList.add(adu);
                
                hasAttachmentMap.put(adu.Id, false);//At this stage the attachment havent query, temp set to false first
            }
        }
        
        //Update the hasAttachmentMap to true if attachment is found
        for(Attachment attch: [Select Id, ParentId from Attachment where ParentId in: aduList]){
            if(hasAttachmentMap.containsKey(attch.ParentId)){
                hasAttachmentMap.put(attch.ParentId, true);
            }
        }
        
        //Set error message if no attachment record
        for(AAS_Assessment_Data_Upload__c adu: newList){
            if(hasAttachmentMap.containsKey(adu.Id)){
                if(!hasAttachmentMap.get(adu.Id)){
                    adu.addError('There is no attachment record found for ('+adu.Name+'). Please attach the attachment for this record.');
                }
            }    
        }
        
    }
    
    
}