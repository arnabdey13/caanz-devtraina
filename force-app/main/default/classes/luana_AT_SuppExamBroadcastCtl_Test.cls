/*
    Developer: WDCi (KH)
    Date: 30/08/2016
    Task #: Test class for luana_SuppExamBroadcastController
    
*/
@isTest(seeAllData=false)
private class luana_AT_SuppExamBroadcastCtl_Test {
    
    final static String contextLongName = 'luana_AT_SuppExamBroadcastCtl_Test';
    final static String contextShortName = 'lsebc';
    private static map<String, Id> commProfIdMap {get; set;}
    
    public static User memberUser {get; set;}
    public static Account memberAccount {get; set;}
    public static luana_CommUserUtil userUtil {get; set;} 
    
    public static LuanaSMS__Course__c course1;
    public static LuanaSMS__Course__c course2;
    public static LuanaSMS__Course__c course3;
    public static LuanaSMS__Course__c course4;
    public static LuanaSMS__Course__c course5;
    
    public static LuanaSMS__Session__c newSession1;
    public static LuanaSMS__Session__c newSession2;
    public static LuanaSMS__Session__c newSession3;
    
    public static Luana_DataPrep_Test dataPrep {get; set;}
    
    public static List<LuanaSMS__Attendance2__c> attendances;
    
    public static LuanaSMS__Student_Program__c newStudProg1;
    public static LuanaSMS__Student_Program__c newStudProg2;
    public static LuanaSMS__Student_Program__c newStudProg3;
    public static LuanaSMS__Student_Program__c newStudProg4;
    public static LuanaSMS__Student_Program__c newStudProg5_1;
    public static LuanaSMS__Student_Program__c newStudProg5_2;
    
    static void initial(String runSeqNo){
        //initialize
        dataPrep = new Luana_DataPrep_Test();
        
        //Create all the custom setting
         
        insert dataPrep.createLuanaConfigurationCustomSetting();
        
        List<Luana_Extension_Settings__c> customSessingList = dataPrep.prepLuanaExtensionSettingCustomSettings();
        customSessingList.add(new Luana_Extension_Settings__c(Name = 'AdobeConnect_URL', Value__c = '  https://connect-staging.charteredaccountantsanz.com'));
        insert customSessingList;
        
        //Create user with Member and Employer community access
        memberAccount = dataPrep.generateNewApplicantAcc('Joe_' + contextShortName, contextLongName, 'Full_Member');
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.Assessible_for_CA_Program__c = true;
        memberAccount.PersonEmail = 'joe_'+runSeqNo+'_' + contextShortName +'@gmail.com';//LCA-921
        insert memberAccount;
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
    }
    
    static void setup(String runSeqKey){
        
        //LCA-921
        memberUser = [Select Id, Email, FirstName, LastName, UserName, Name from User Where Email =: 'joe_'+runSeqKey+'_' + contextShortName+'@gmail.com' limit 1];
        //memberUser = dataPrep.generateNewApplicantUser(contextLongName, contextShortName, memberAccount, commProfIdMap.get('Customer Community Login User'));
        //insert memberUser;
        
        userUtil = new luana_CommUserUtil(memberUser.Id);
        
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
        Luana_Extension_Settings__c defContriPermSetId = Luana_Extension_Settings__c.getValues('Default_ContCom_PermSet_Id');
       
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defEmployerPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defContriPermSetId.value__c));
        
        System.runAs(memberUser){
            insert psas;
        }
        
        LuanaSMS__Training_Organisation__c trainingOrg = dataPrep.createNewTraningOrg(contextLongName, contextLongName, contextShortName, 'Virtual Street 123', 'Queensland', '4551');
        insert trainingOrg;
        LuanaSMS__Program__c program = dataPrep.createNewProgram(contextShortName, contextLongName, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        insert program;
        LuanaSMS__Program_Offering__c po = dataPrep.createNewProgOffering('po' + contextShortName, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), program.Id, trainingOrg.Id, null, null, null, 1, 1);
        insert po;
        
        List<LuanaSMS__Course__c> courses = new List<LuanaSMS__Course__c>();
        course1 = dataPrep.createNewCourse('Graduate Diploma of Workshop1 Accounting_' + contextShortName, po.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Program'), 'Running');
        course1.LuanaSMS__Allow_Online_Enrolment__c = true;
        course1.Supp_Exam_Enrolment_End_Date__c = System.today().addMonths(3);
        courses.add(course1);
        
        course2 = dataPrep.createNewCourse('Graduate Diploma of Workshop2 Accounting_' + contextShortName, po.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Program'), 'Running');
        course2.LuanaSMS__Allow_Online_Enrolment__c = true;
        course2.Supp_Exam_Enrolment_End_Date__c = System.today().addMonths(3);
        courses.add(course2);
        
        course3 = dataPrep.createNewCourse('Graduate Diploma of Workshop2 Accounting_' + contextShortName, po.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Program'), 'Running');
        course3.LuanaSMS__Allow_Online_Enrolment__c = true;
        course3.Supp_Exam_Enrolment_End_Date__c = System.today().addMonths(3);
        courses.add(course3);
        
        course4 = dataPrep.createNewCourse('Graduate Diploma of Workshop2 Accounting_' + contextShortName, po.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Program'), 'Running');
        course4.LuanaSMS__Allow_Online_Enrolment__c = true;
        course4.Supp_Exam_Enrolment_End_Date__c = System.today().addMonths(3);
        courses.add(course4);
        
        course5 = dataPrep.createNewCourse('Graduate Diploma of Workshop2 Accounting_' + contextShortName, po.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Program'), 'Running');
        course5.LuanaSMS__Allow_Online_Enrolment__c = true;
        course5.Supp_Exam_Enrolment_End_Date__c = System.today().addMonths(3);
        courses.add(course5);
        
        insert courses;
        
        List<LuanaSMS__Student_Program__c> stuPrograms = new List<LuanaSMS__Student_Program__c>();
        newStudProg1 = dataPrep.createNewStudentProgram(dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), userUtil.custCommConId, course1.Id, '', 'Fail');
        newStudProg1.Paid__c = true;
        newStudProg1.Supp_Exam_Broadcasted__c = false;
        newStudProg1.Sup_Exam_Enrolled__c = false;
        newStudProg1.Supp_Exam_Special_Consideration_Approved__c = 'Approved';
        newStudProg1.Supp_Exam_Eligible__c = false;
        stuPrograms.add(newStudProg1);
        
        newStudProg2 = dataPrep.createNewStudentProgram(dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), userUtil.custCommConId, course2.Id, '', 'Fail');
        newStudProg2.Paid__c = true;
        newStudProg2.Supp_Exam_Paid__c = true;
        newStudProg2.Supp_Exam_Broadcasted__c = false;
        newStudProg2.Sup_Exam_Enrolled__c = true;
        newStudProg2.Supp_Exam_Special_Consideration_Approved__c = 'Approved';
        stuPrograms.add(newStudProg2);
        
        newStudProg3 = dataPrep.createNewStudentProgram(dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), userUtil.custCommConId, course3.Id, '', 'Fail');
        newStudProg3.Paid__c = true;
        newStudProg3.Supp_Exam_Broadcasted__c = false;
        newStudProg3.Sup_Exam_Enrolled__c = false;
        newStudProg3.Supp_Exam_Special_Consideration_Approved__c = 'Approved';
        newStudProg3.Supp_Exam_Eligible__c = false;
        stuPrograms.add(newStudProg3);
        
        newStudProg4 = dataPrep.createNewStudentProgram(dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), userUtil.custCommConId, course4.Id, '', 'Fail');
        newStudProg4.Paid__c = true;
        newStudProg4.Supp_Exam_Broadcasted__c = false;
        stuPrograms.add(newStudProg4);
        
        newStudProg5_1 = dataPrep.createNewStudentProgram(dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), userUtil.custCommConId, course5.Id, '', 'Fail');
        newStudProg5_1.Paid__c = true;
        newStudProg5_1.Supp_Exam_Paid__c = true;
        newStudProg5_1.Supp_Exam_Broadcasted__c = false;
        newStudProg5_1.Sup_Exam_Enrolled__c = true;
        newStudProg5_1.Supp_Exam_Special_Consideration_Approved__c = 'Approved';
        stuPrograms.add(newStudProg5_1);
        
        newStudProg5_2 = dataPrep.createNewStudentProgram(dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module'), userUtil.custCommConId, course5.Id, '', 'Fail');
        newStudProg5_2.Paid__c = true;
        newStudProg5_2.Supp_Exam_Paid__c = true;
        newStudProg5_2.Supp_Exam_Broadcasted__c = false;
        newStudProg5_2.Sup_Exam_Enrolled__c = true;
        stuPrograms.add(newStudProg5_2);
        
        insert stuPrograms;
        
        LuanaSMS__Subject__c sub = dataPrep.createNewSubject('FIN_' + contextShortName, 'FIN_' + contextLongName, 'FIN_' + contextShortName, 55, 'Module');
        insert sub;
        
        //Create student Program Subject
        LuanaSMS__Student_Program_Subject__c sps = dataPrep.createNewStudentProgramSubject(userUtil.custCommConId, newStudProg1.Id, sub.Id);
        insert sps;
        
        
        List<Account> venues = new List<Account>();
        Account venueAccount1 = dataPrep.generateNewBusinessAcc(contextShortName+ 'Test Venue_'+runSeqKey+'_1', 'Venue', null, null, null, null, 'Sunshine Coast Office', 'Active');
        venueAccount1.billingCity = 'city';
        venueAccount1.billingPostalCode = '1234';
        venueAccount1.billingCountry = 'Malaysia';
        venueAccount1.Ext_Id__c = contextShortName+'_'+runSeqKey+'_1';
        venues.add(venueAccount1);
        
        Account venueAccount2 = dataPrep.generateNewBusinessAcc(contextShortName+ 'Test Venue_'+runSeqKey+'_2', 'Venue', null, null, null, null, 'Sunshine Coast Office', 'Active');
        venueAccount2.billingCity = 'city';
        venueAccount2.billingPostalCode = '1234';
        venueAccount2.billingCountry = 'Australia';
        venueAccount2.billingState = 'New South Wales';
        venueAccount2.Ext_Id__c = contextShortName+'_'+runSeqKey+'_2';
        venues.add(venueAccount2);
        
        Account venueAccount3 = dataPrep.generateNewBusinessAcc(contextShortName+ 'Test Venue_'+runSeqKey+'_3', 'Venue', null, null, null, null, 'Sunshine Coast Office', 'Active');
        /*venueAccount3.billingCity = 'city';
        venueAccount3.billingPostalCode = '1234';
        venueAccount3.billingCountry = 'Australia';
        */
        venueAccount3.Ext_Id__c = contextShortName+'_'+runSeqKey+'_3';
        venues.add(venueAccount3);
        insert venues;
        
        List<LuanaSMS__Room__c> rooms = new List<LuanaSMS__Room__c>();
        LuanaSMS__Room__c room1 = dataPrep.newRoom(contextShortName, contextLongName, venueAccount1.Id, 20, 'Exam', runSeqKey+'_1');
        LuanaSMS__Room__c room2 = dataPrep.newRoom(contextShortName, contextLongName, venueAccount2.Id, 20, 'Exam', runSeqKey+'_2');
        LuanaSMS__Room__c room3 = dataPrep.newRoom(contextShortName, contextLongName, venueAccount3.Id, 20, 'Exam', runSeqKey+'_3');
        rooms.add(room1);
        rooms.add(room2);
        rooms.add(room3);
        insert rooms;
        
        List<LuanaSMS__Session__c> sessions = new List<LuanaSMS__Session__c>();
        newSession1 = dataPrep.newSession(contextShortName, contextLongName, 'Exam', 20, venueAccount1.Id, room1.Id, null, system.now().addHours(1), system.now().addHours(2));
        newSession1.Topic_Key__c = contextShortName+ '_T1';
        sessions.add(newSession1);
        
        newSession2 = dataPrep.newSession(contextShortName, contextLongName, 'Supplementary_Exam', 20, venueAccount2.Id, room2.Id, null, system.now().addHours(1), system.now().addHours(2));
        newSession2.Topic_Key__c = contextShortName+ '_T1';
        sessions.add(newSession2);
        
        newSession3 = dataPrep.newSession(contextShortName, contextLongName, 'Supplementary_Exam', 20, venueAccount3.Id, room3.Id, null, system.now().addHours(1), system.now().addHours(2));
        newSession3.Topic_Key__c = contextShortName+ '_T1';
        sessions.add(newSession3);
        
        insert sessions;
        
        attendances = new List<LuanaSMS__Attendance2__c>();
        LuanaSMS__Attendance2__c att1 = dataPrep.createAttendance(userUtil.custCommConId, 'Exam', newStudProg1.Id, newSession1.Id);
        attendances.add(att1);
        LuanaSMS__Attendance2__c att2 = dataPrep.createAttendance(userUtil.custCommConId, 'Supplementary_Exam', newStudProg1.Id, newSession2.Id);
        attendances.add(att2);
        
        LuanaSMS__Attendance2__c att3 = dataPrep.createAttendance(userUtil.custCommConId, 'Exam', newStudProg2.Id, newSession1.Id);
        attendances.add(att3);
        LuanaSMS__Attendance2__c att4 = dataPrep.createAttendance(userUtil.custCommConId, 'Supplementary_Exam', newStudProg2.Id, newSession2.Id);
        attendances.add(att4);
        
        LuanaSMS__Attendance2__c att5 = dataPrep.createAttendance(userUtil.custCommConId, 'Supplementary_Exam', newStudProg3.Id, newSession3.Id);
        attendances.add(att5);
        
        insert attendances;
        
    }
    
    /*
        Broadcast supplementary - success
    */
    static testMethod void testSuppExamBroadcastOpening(){
        
        Test.startTest();
            initial('1');
        Test.stopTest();
        
        setup('1');
        List<LuanaSMS__Attendance2__c> newAtts = new List<LuanaSMS__Attendance2__c>();
        
        System.runAs(memberUser){
            
            //Create and link mock http class
            Test.setMock(HttpCalloutMock.class, new luana_AT_VCImportWizardMockHttp_Test());
            //Main Test Area
           
            PageReference pageRef = Page.luana_SuppExamEnrolBroadcastWizard;
            pageRef.getParameters().put('id', course1.id);
            pageRef.getParameters().put('actiontype', 'opening');
            
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController wsBroadcast = new ApexPages.StandardController(course1);
            luana_SuppExamBroadcastWizardCtrl broadcast1 = new luana_SuppExamBroadcastWizardCtrl(wsBroadcast); 
            
            broadcast1.doOpeningBroadcast();
        
            List<LuanaSMS__Attendance2__c> getAttendances = [Select Id, Name, LuanaSMS__Attendance_Status__c from LuanaSMS__Attendance2__c Where Record_Type_Name__c = 'Supplementary_Exam'];

            for(LuanaSMS__Attendance2__c att: attendances){
                att.LuanaSMS__Attendance_Status__c = 'Attended';
                newAtts.add(att);
            }
            
        }
        update newAtts;
    }
    
    
    
    /*
        Broadcast supplementary success
    */
    static testMethod void testSuppExamBroadcastAllocation(){
        
        Test.startTest();
            initial('2');
        Test.stopTest();
        
        setup('2');
        
        //Create and link mock http class
        Test.setMock(HttpCalloutMock.class, new luana_AT_VCImportWizardMockHttp_Test());
        //Main Test Area
       
        PageReference pageRef = Page.luana_SuppExamEnrolBroadcastWizard;
        pageRef.getParameters().put('id', course2.id);
        pageRef.getParameters().put('actiontype', 'allocation');
        
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController wsBroadcast = new ApexPages.StandardController(course2);
        luana_SuppExamBroadcastWizardCtrl broadcast2 = new luana_SuppExamBroadcastWizardCtrl(wsBroadcast);
        
        broadcast2.doAllocationBroadcast();
    }

    
    /*
        Broadcast supplementary exam fail - No attendnace
    */
    static testMethod void testSuppExamBroadcastNoAtt(){
        
        Test.startTest();
            initial('3');
        Test.stopTest();
        
        setup('3');
        
        //Create and link mock http class
        Test.setMock(HttpCalloutMock.class, new luana_AT_VCImportWizardMockHttp_Test());
        //Main Test Area
       
        PageReference pageRef = Page.luana_SuppExamEnrolBroadcastWizard;
        pageRef.getParameters().put('id', course4.id);
        pageRef.getParameters().put('actiontype', 'allocation');
        
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController wsBroadcast = new ApexPages.StandardController(course4);
        luana_SuppExamBroadcastWizardCtrl broadcast2 = new luana_SuppExamBroadcastWizardCtrl(wsBroadcast);
        
        broadcast2.doAllocationBroadcast();
    }
    
    
    /*
        Broadcast supplementary exam fail - No attendnace
    */
    static testMethod void testSuppExamBroadcastNoAdd(){
        
        Test.startTest();
            initial('4');
        Test.stopTest();
        
        setup('4');
        
        //Create and link mock http class
        Test.setMock(HttpCalloutMock.class, new luana_AT_VCImportWizardMockHttp_Test());
        //Main Test Area
       
        PageReference pageRef = Page.luana_SuppExamEnrolBroadcastWizard;
        pageRef.getParameters().put('id', course3.id);
        pageRef.getParameters().put('actiontype', 'allocation');
        
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController wsBroadcast = new ApexPages.StandardController(course3);
        luana_SuppExamBroadcastWizardCtrl broadcast2 = new luana_SuppExamBroadcastWizardCtrl(wsBroadcast);
        
        broadcast2.doAllocationBroadcast();
    }
  
    /*
        Broadcast supplementary success
    */
    static testMethod void testSuppExamBroadcastAllocationNoAttendance(){
        
        Test.startTest();
            initial('5');
        Test.stopTest();
        
        setup('5');
        
        //Create and link mock http class
        Test.setMock(HttpCalloutMock.class, new luana_AT_VCImportWizardMockHttp_Test());
        //Main Test Area
       
        PageReference pageRef = Page.luana_SuppExamEnrolBroadcastWizard;
        pageRef.getParameters().put('id', course5.id);
        pageRef.getParameters().put('actiontype', 'allocation');
        
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController wsBroadcast = new ApexPages.StandardController(course5);
        luana_SuppExamBroadcastWizardCtrl broadcast5 = new luana_SuppExamBroadcastWizardCtrl(wsBroadcast);
        
        broadcast5.doAllocationBroadcast();
    }
}