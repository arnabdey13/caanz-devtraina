/*
    Developer: WDCi (Carter)
    Date: 7/June/2016
    Task #: Test class for luana_VirtualClassBroadcastController
    
    Change History
    LCA-921 23/08/2016 WDCi - KH: Add Person Account email
*/
@isTest(seeAllData=false)
private class luana_AT_VirtualClassBroadcast_Test {

    final static String contextLongName = 'luana_VirtualClassBroadcast_Test';
    final static String contextShortName = 'lvcbt';
    
    public static LuanaSMS__Course__c courseV;

    //Setup method
    static void setup(){
        Luana_DataPrep_Test dataPrep = new Luana_DataPrep_Test();
        
        LuanaSMS__Training_Organisation__c trainingOrg = dataPrep.createNewTraningOrg(contextLongName, contextLongName, contextShortName, 'Virtual Street 123', 'Queensland', '4551');
        insert trainingOrg;
        LuanaSMS__Program__c program = dataPrep.createNewProgram(contextShortName, contextLongName, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        insert program;
        LuanaSMS__Program_Offering__c po = dataPrep.createNewProgOffering('po' + contextShortName, dataPrep.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Accredited_Module'), program.Id, trainingOrg.Id, null, null, null, 1, 1);
        insert po;
        courseV = dataPrep.createNewCourse('Graduate Diploma of Virtual Accounting_' + contextShortName, po.Id, dataPrep.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Program'), 'Running');
        courseV.LuanaSMS__Allow_Online_Enrolment__c = true;
        insert courseV;
        
        List<Account> memberAccounts = new List<Account>();
        for(integer i = 0; i < 5; i ++){
            Account memberAccount = dataPrep.generateNewApplicantAcc('Bob_' + contextShortName + '_' + i, contextLongName, 'Full_Member');
            memberAccount.Member_Id__c = '12345' + i;
            memberAccount.Affiliated_Branch_Country__c = 'Australia';
            memberAccount.Membership_Class__c = 'Full';
            memberAccount.PersonEmail = 'bob_'+i+'_'+contextShortName+'@gmail.com';//LCA-921 add person account email
            memberAccount.Communication_Preference__c= 'Home Phone';
            memberAccount.PersonHomePhone= '1234';
            memberAccount.PersonOtherStreet= '83 Saggers Road';
            memberAccount.PersonOtherCity='JITARNING';
            memberAccount.PersonOtherState='Western Australia';
            memberAccount.PersonOtherCountry='Australia';
            memberAccount.PersonOtherPostalCode='6365';  

            memberAccounts.add(memberAccount);
        }
        insert memberAccounts;       
        
        List<LuanaSMS__Student_Program__c> newStudentPrograms = new List<LuanaSMS__Student_Program__c>();
        Id studProgAccModuleId = dataPrep.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module');
        
        for(Contact contact : [select Id from Contact where AccountId in: memberAccounts]){
            LuanaSMS__Student_Program__c newStudProg = dataPrep.createNewStudentProgram(studProgAccModuleId, contact.Id, courseV.Id, 'Australia', 'In Progress');
            newStudProg.Paid__c = TRUE;
            newStudentPrograms.add(newStudProg);
        }
        insert newStudentPrograms;
        
    }

    /*Scenario - A course has sent out a virtual broadcast to the student enrolled in it.
     Everything is set up correctly and the job should complete successfully.
    */
    static testMethod void testBroadcastSuccess() {
        
        setup();
        
        //Main Test Area
        Test.startTest();
        PageReference pageRef = Page.luana_VirtualClassBroadcastWizard;
        pageRef.getParameters().put('id', courseV.id);
        
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController lvcbt_sc = new ApexPages.StandardController(courseV);
        luana_VirtualClassBroadcastController lvcbt_e = new luana_VirtualClassBroadcastController(lvcbt_sc);
        lvcbt_e.doBroadcast();
        Test.stopTest();
        
        LuanaSMS__Luana_Log__c eLog = [SELECT LuanaSMS__Status__c FROM LuanaSMS__Luana_Log__c];
        System.AssertEquals(eLog.LuanaSMS__Status__c, 'Completed');
        
    }
    
    /*Scenario - Someone tries to broadcast from a non-existant course
    The page will give them an error and be unable to run the job.
    */
    static testmethod void testBroadcastFailNoCourse(){
        
        setup();
        
        //Main Test Area
        Test.startTest();
        PageReference pageRef = Page.luana_VirtualClassBroadcastWizard;
        
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController lvcbt_sc = new ApexPages.StandardController(courseV);
        luana_VirtualClassBroadcastController lvcbt_e = new luana_VirtualClassBroadcastController(lvcbt_sc);
        Test.stopTest();
        
        
        List<Apexpages.Message> messages = ApexPages.getMessages();
        boolean found = false;
        for(Apexpages.Message msg : messages){
            if (msg.getDetail().contains('Invalid course. Please try again later or contact your system administrator.')){
                found = true;
            }
        }
        System.assert(found);
        
    }
}