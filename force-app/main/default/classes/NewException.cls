/*

http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_classes_exception_methods.htm

public class BaseException extends Exception {}
public class OtherException extends BaseException {}
try {
	Integer i;
	// Your code here
	if (i < 5){
		throw new OtherException('This is bad');
	}
	catch (BaseException e){
		// ##This catches the OtherException
	}
}


TestObjectCreator

TestRelationshipTrigger

RelationshipTrigger

RecordTypeCache

*/
public class NewException extends Exception {
	public static void assert(Boolean good, String msg){
		if(!good){
			// NewException.assert( false, 'This is not allowed' );
			throw new NewException(msg);
		}
	}

	/**
	* Build a complete stack trace for logging
	*/
	public static String buildStackTrace(Exception ex){
		return buildStackTrace('', ex);
	}
	public static String buildStackTrace(String msg, Exception ex){
		/*
		try{
			integer i = 10/0;
		}
		catch(Exception ex){
			throw new NewException( NewException.buildStackTrace( System.now().format() + ' - ', ex ) );
		}
		*/
		// NewException: 21/09/2012 3:38 PM - System.MathException: Divide by 0 at AnonymousBlock: line 2, column 1.
		msg += 
			ex.getTypeName() + ': ' + 
			ex.getMessage() + ' at ' + 
			ex.getStackTraceString() + '. ';

		if(ex.getCause()!=null){
			msg += '\ncaused by:\n' + buildStackTrace(msg, ex.getCause());
		}
		return msg;
	}
}