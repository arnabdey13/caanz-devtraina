@isTest
public class UtilityTest {
    static testMethod void testGetAvailableRecordTypeNamesforSObject_True(){
        //has record types test - application
        Schema.SObjectType testSObjectType = Schema.getGlobalDescribe().get('Application__c');
        List<String> lstRecordTypes = Utility.GetAvailableRecordTypeNamesForSObject(testSObjectType);
        System.assertNotEquals(null,lstRecordTypes,'Record types unexpectedly not available for Application');
    }
    static testMethod void testGetAvailableRecordTypeNamesforSObject_False(){
        //no record type test - employment history
        Schema.SObjectType testSObjectType = Schema.getGlobalDescribe().get('Employment_History__c');
        List<String> lstRecordTypes = Utility.GetAvailableRecordTypeNamesForSObject(testSObjectType);
        System.assert(lstRecordTypes.isEmpty(),'Record types unexpectedly returned for Employment History');
    }
}