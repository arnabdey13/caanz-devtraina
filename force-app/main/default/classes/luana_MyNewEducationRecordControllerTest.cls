/*
    Developer: WDCi (Lean)
    Development Date: 13/04/2016
    Task #: Test luana_MyNewEducationRecordController for enrolment wizard
    
    Change History:
    LCA-555 WDCi - KH: Include edit test class
    LCA-921 23/08/2019 WDCi - KH: Add person account email
*/

@isTest(seeAllData=false)
private class luana_MyNewEducationRecordControllerTest {
    
    private static String classNamePrefixLong = 'luana_MyNewEducationRecordControllerTest';
    private static String classNamePrefixShort = 'lmnec';
    private static map<String, Id> commProfIdMap {get; set;}
    public static Account memberAccount {get; set;}
    public static User memberUser {get; set;}
    
    public static Luana_DataPrep_Test dataPrep;
    public static Account businessAccount;
    
    static void initial(){
        //initialize
        dataPrep = new Luana_DataPrep_Test();
        
        //Create all the custom setting
        insert dataPrep.prepLuanaExtensionSettingCustomSettings();
        insert dataPrep.createLuanaConfigurationCustomSetting();
        
        //Create user with Member and Employer community access
        memberAccount = dataPrep.generateNewApplicantAcc('Joe_' + classNamePrefixShort, classNamePrefixLong, 'Full_Member');
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.PersonEmail = 'joe_1_' +classNamePrefixShort+'@gmail.com';//LCA-921
        memberAccount.Communication_Preference__c= 'Home Phone';
        memberAccount.PersonHomePhone= '1234';
        memberAccount.PersonOtherStreet= '83 Saggers Road';
        memberAccount.PersonOtherCity='JITARNING';
        memberAccount.PersonOtherState='Western Australia';
        memberAccount.PersonOtherCountry='Australia';
        memberAccount.PersonOtherPostalCode='6365';
        insert memberAccount;
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
    }
    
    static void setup(){
        //LCA-921
        memberUser = [Select Id, Email, FirstName, LastName, UserName, Name, ContactId from User Where Email =: 'joe_1_' + classNamePrefixShort+'@gmail.com' limit 1];
        //memberUser = testDataGenerator.generateNewApplicantUser(classNamePrefixLong, classNamePrefixshort, memberAccount, commProfIdMap.get('Customer Community Login User'));
        //insert memberUser;
        
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
       
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defEmployerPermSetId.value__c));
        list<PermissionSetAssignment> permissAssign = [Select Id from PermissionSetAssignment where AssigneeId =: memberUser.Id AND (PermissionSetId =: defMemberPermSetId.value__c OR PermissionSetId =: defEmployerPermSetId.value__c)];
        System.runAs(memberUser){
            if(permissAssign == null)
                insert psas;
        }
        
        businessAccount = dataPrep.generateNewBusinessAcc('Price Waterhouse_' + classNamePrefixShort, 'Business_Account', 'Chartered Accounting', '11111111', '012112111', 'NZICA', '123 Main Street', 'Active');
        insert businessAccount;
        
        luana_CommUserUtil userUtil = new luana_CommUserUtil(memberUser.Id);
        
        //Create 5 Education Records
        List<Education_Record__c> erlist = new List<Education_Record__c>();
        for(integer i = 0; i < 5; i++){
            erlist.add(dataPrep.createNewEducationRecords(memberUser.ContactId, System.today().addDays(i), 'Course_' + i + '_' + classNamePrefixShort, 'University ABC'));
        }
        
        insert erlist;
    }
    
    static testMethod void testNewEducationRecord() { 
    
        Test.startTest();
            initial();
        Test.stopTest();
        
        setup();
        
        System.RunAs(memberUser){
            luana_MyNewEducationRecordController newCtrl = new luana_MyNewEducationRecordController();
            
            newCtrl.searchUniProOrg = 'Pri';
            newCtrl.selectedUniProOrg = businessAccount.Id;
            
            newCtrl.educationRecordObj.Country__c = 'Australia';
            newCtrl.educationRecordObj.State__c = 'Queensland';
            newCtrl.educationRecordObj.Enter_university_instituition__c = 'University ABC';
            newCtrl.educationRecordObj.Date_Commenced__c = System.today();
            newCtrl.educationRecordObj.Event_Course_name__c = 'Progamming ABC';
            newCtrl.educationRecordObj.Date_completed__c = System.today().addMonths(1);
            newCtrl.educationRecordObj.Bridging__c = true;
            newCtrl.educationRecordObj.CAHDP__c = true;
            newCtrl.educationRecordObj.Training_and_development__c = true;
            newCtrl.educationRecordObj.Qualifying_hours__c = 24;
            newCtrl.educationRecordObj.Qualifying_hours_type__c = 'Formal';
            newCtrl.educationRecordObj.Specialist_hours_code__c = 'Type - A Registered Company Auditor';
            
            //before insert check
            List<Education_Record__c> beforeERrlist = [Select Id, Name, Country__c, State__c, Enter_university_instituition__c, Contact_Student__c 
                                                from Education_Record__c 
                                                Where Contact_Student__c =: memberUser.ContactId and Enter_university_instituition__c = 'University ABC'];
            System.assertEquals(beforeERrlist.size(), 5, 'Expect a new Education Record is created but it is not.');  
            
            newCtrl.doSave();
            
            //After insert check
            List<Education_Record__c> afterERrlist = [Select Id, Name, Country__c, State__c, Enter_university_instituition__c, Contact_Student__c, Community_User_Entry__c
                                                from Education_Record__c 
                                                Where Contact_Student__c =: memberUser.ContactId and Enter_university_instituition__c = 'University ABC'];
            System.assertEquals(afterERrlist.size(), 6, 'Expect a new Education Record is created but it is not.');
            
            PageReference landingPage = Page.luana_MyNewEducationRecord;
            Test.setCurrentPageReference(landingPage); 
            System.currentPageReference().getParameters().put('id', newCtrl.educationRecordObj.Id);

            System.debug('****landingPage: ' + landingPage);
            luana_MyNewEducationRecordController newCtrl2 = new luana_MyNewEducationRecordController();
            
            newCtrl.getTemplateName();
             
            //navigation test
            newCtrl.doCancel();
            
            List<Account> searchUniProfOrgs = luana_MyNewEducationRecordController.searchUniProfessionalOrg('University ABC');
        }
        

    }
    
}