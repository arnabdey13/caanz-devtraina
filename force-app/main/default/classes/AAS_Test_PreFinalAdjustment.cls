/*
    Developer: WDCi (Leo)
    Date: 20/04/2017
    Task#: Pre-Final Adjustment Wizard Test Class
*/
@isTest
private class  AAS_Test_PreFinalAdjustment {
    
    final static String contextLongName = 'AAS_Test_PreFinalAdjustment';
    final static String contextShortName = 'frr';
    
    public static Luana_DataPrep_Test dataPrepUtil {get;set;}
    public static AAS_DataPrep_Test aasDataPrepUtil {get;set;}
    public static luana_CommUserUtil userUtil {get; set;}
    public static User memberUser {get; set;}
    public static Account memberAccount {get; set;}
    private static map<String, Id> commProfIdMap {get; set;}

    public static testMethod void preFinalAdjustmentStandardCase() {

        dataPrepUtil = new Luana_DataPrep_Test();
        aasDataPrepUtil = new AAS_DataPrep_Test();
        insert dataPrepUtil.prepLuanaExtensionSettingCustomSettings();
        insert aasDataPrepUtil.defauleCustomSetting();
        
        userUtil = new luana_CommUserUtil(UserInfo.getUserId());

        Map<String, Id> recordTypeMap = aasDataPrepUtil.getRecordTypeMap('Exam');
        Map<String, SObject> assetMap = aasDataPrepUtil.createBasicAssets(contextLongName, contextShortName, dataPrepUtil, aasDataPrepUtil);

        //Create new account for testing usage
        memberAccount = dataPrepUtil.generateNewApplicantAcc('Joe_' + contextShortName, contextLongName, 'Full_Member');
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.Assessible_for_CA_Program__c = true;
        memberAccount.PersonEmail = 'joe_1_' + contextShortName +'@gmail.com';//LCA-921
        insert memberAccount;
        
        memberAccount =  [SELECT Id, PersonContactID, Member_Id__c, Affiliated_Branch_Country__c, Membership_Class__c, Assessible_for_CA_Program__c, PersonEmail FROM Account WHERE Id =: memberAccount.Id LIMIT 1];
        system.debug('memberAccount: ' +memberAccount);
        assetMap.put('MEMBERACCOUNT1', memberAccount);
        
        assetMap = aasDataPrepUtil.createBasicAssetsPart2(contextLongName, contextShortName, dataPrepUtil, aasDataPrepUtil, assetMap);
        
        Test.startTest();
        
            PageReference initialPage = Page.AAS_PreFinalAdjustmentPage0A;
            initialPage.getParameters().put('id', String.valueOf(assetMap.get('COURSEASSESSMENT1').Id));
            Test.setCurrentPage(initialPage);
            
            AAS_PreFinalAdjustment pageController = new AAS_PreFinalAdjustment();
            
            // Simulating page 0A
            pageController.preliminarySetup();
            pageController.nextToPage0B();
            
            // Simulating page 0B
            pageController.autoRedirectB();
            pageController.nextToPage1New();
            
            // Simulating page 1
            pageController.searchRecord();
            Integer resultTotal = pageController.resultTotal;
            Integer totalAvailable = pageController.totalAvailableRecords;
            Boolean hasNext = pageController.hasNext;
            Boolean hasPrevious = pageController.hasPrevious;
            
            system.debug('resultTotal : ' +resultTotal);
            system.debug('totalAvailable : ' +totalAvailable);
            
            pageController.Next();
            pageController.Previous();
            pageController.End();
            pageController.Beginning();
            
            // Simulating the user selecting an employee from the SAS list
            for (AAS_Student_Assessment_Section__c sas : pageController.getSASList()) {
                sas.AAS_MarkedForAdjustment__c = true;
            }
            
            pageController.nextToPage2();
            
            // Simulating page 2
            pageController.EditNext();
            pageController.EditPrevious();
            pageController.EditEnd();
            pageController.EditBeginning();

            pageController.saveAndNext();
            pageController.saveAndDone();
            
            Integer pageNumber = pageController.pageNumber;
            Integer pageTotal = pageController.pageTotal;
            Integer searchLimit = pageController.searchLimit;
            
            pageController.nextToPage3();
            
            // Simulating page 3
            pageController.cancel();
            
            
        Test.stopTest();
    }
    
    public static testMethod void courseAssessmentSequenceChecker() {
        
        dataPrepUtil = new Luana_DataPrep_Test();
        aasDataPrepUtil = new AAS_DataPrep_Test();
        insert dataPrepUtil.prepLuanaExtensionSettingCustomSettings();
        insert aasDataPrepUtil.defauleCustomSetting();
        
        userUtil = new luana_CommUserUtil(UserInfo.getUserId());

        Map<String, Id> recordTypeMap = aasDataPrepUtil.getRecordTypeMap('Exam');
        Map<String, SObject> assetMap = aasDataPrepUtil.createBasicAssets(contextLongName, contextShortName, dataPrepUtil, aasDataPrepUtil);

        // Course
        List<LuanaSMS__Course__c> courseList = new List<LuanaSMS__Course__c>();
        LuanaSMS__Course__c course1 = dataPrepUtil.createNewCourse('AAA216', assetMap.get('PROGRAMOFFERRING1').Id, dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        course1.LuanaSMS__Allow_Online_Enrolment__c = true;
        course1.Supp_Exam_Enrolment_End_Date__c = system.today();
        courseList.add(course1);
        
        LuanaSMS__Course__c course2 = dataPrepUtil.createNewCourse('AAA217', assetMap.get('PROGRAMOFFERRING1').Id, dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        course2.LuanaSMS__Allow_Online_Enrolment__c = true;
        course2.Supp_Exam_Enrolment_End_Date__c = system.today();
        courseList.add(course2);
        
        LuanaSMS__Course__c course3 = dataPrepUtil.createNewCourse('AAA218', assetMap.get('PROGRAMOFFERRING1').Id, dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        course3.LuanaSMS__Allow_Online_Enrolment__c = true;
        course3.Supp_Exam_Enrolment_End_Date__c = system.today();
        courseList.add(course3);
        
        LuanaSMS__Course__c course4 = dataPrepUtil.createNewCourse('AAA219', assetMap.get('PROGRAMOFFERRING1').Id, dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        course4.LuanaSMS__Allow_Online_Enrolment__c = true;
        course4.Supp_Exam_Enrolment_End_Date__c = system.today();
        courseList.add(course4);
        
        LuanaSMS__Course__c course5 = dataPrepUtil.createNewCourse('AAA220', assetMap.get('PROGRAMOFFERRING1').Id, dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        course5.LuanaSMS__Allow_Online_Enrolment__c = true;
        course5.Supp_Exam_Enrolment_End_Date__c = system.today();
        courseList.add(course5);
        
        LuanaSMS__Course__c course6 = dataPrepUtil.createNewCourse('AAA221', assetMap.get('PROGRAMOFFERRING1').Id, dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        course6.LuanaSMS__Allow_Online_Enrolment__c = true;
        course6.Supp_Exam_Enrolment_End_Date__c = system.today();
        courseList.add(course6);
        
        LuanaSMS__Course__c course7 = dataPrepUtil.createNewCourse('AAA222', assetMap.get('PROGRAMOFFERRING1').Id, dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        course7.LuanaSMS__Allow_Online_Enrolment__c = true;
        course7.Supp_Exam_Enrolment_End_Date__c = system.today();
        courseList.add(course7);
        
        LuanaSMS__Course__c course8 = dataPrepUtil.createNewCourse('AAA223', assetMap.get('PROGRAMOFFERRING1').Id, dataPrepUtil.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Module'), 'Running');
        course8.LuanaSMS__Allow_Online_Enrolment__c = true;
        course8.Supp_Exam_Enrolment_End_Date__c = system.today();
        courseList.add(course8);
        
        insert courseList;
  
        // Course Assessment
        List<AAS_Course_Assessment__c> caList = new List<AAS_Course_Assessment__c>();
        AAS_Course_Assessment__c ca1 = aasDataPrepUtil.createCourseAssessment(course1.Id, 'CA1_' + contextShortName);
        ca1.AAS_Module_Passing_Mark__c = 40;
        ca1.AAS_Non_Exam_Data_Loaded__c = false;
        caList.add(ca1);
        
        // Additional CA to test the sequence checker
        AAS_Course_Assessment__c ca2 = aasDataPrepUtil.createCourseAssessment(course2.Id, 'CA2_' + contextShortName);
        ca2.AAS_Module_Passing_Mark__c = 40;
        ca2.AAS_Exam_Data_Loaded__c = false;
        caList.add(ca2);
        
        // Additional CA to test the sequence checker
        AAS_Course_Assessment__c ca3 = aasDataPrepUtil.createCourseAssessment(course3.Id, 'CA3_' + contextShortName);
        ca3.AAS_Module_Passing_Mark__c = 40;
        ca3.AAS_Cohort_Adjustment_Exam__c = false;
        caList.add(ca3);
        
        // Additional CA to test the sequence checker
        AAS_Course_Assessment__c ca4 = aasDataPrepUtil.createCourseAssessment(course4.Id, 'CA4_' + contextShortName);
        ca4.AAS_Module_Passing_Mark__c = 40;
        ca4.AAS_Borderline_Remark__c = false;
        caList.add(ca4);
        
        // Additional CA to test the sequence checker
        AAS_Course_Assessment__c ca5 = aasDataPrepUtil.createCourseAssessment(course5.Id, 'CA5_' + contextShortName);
        ca5.AAS_Module_Passing_Mark__c = 40;
        ca5.AAS_Exam_Adjustment__c = false;
        caList.add(ca5);
        
        // Additional CA to test the sequence checker
        AAS_Course_Assessment__c ca6 = aasDataPrepUtil.createCourseAssessment(course6.Id, 'CA6_' + contextShortName);
        ca6.AAS_Module_Passing_Mark__c = 40;
        ca6.AAS_Final_Result_Calculation_Exam__c = false;
        caList.add(ca6);
        
        // Additional CA to test the sequence checker
        AAS_Course_Assessment__c ca7 = aasDataPrepUtil.createCourseAssessment(course7.Id, 'CA7_' + contextShortName);
        ca7.AAS_Module_Passing_Mark__c = 40;
        ca7.AAS_Result_Release_Lock__c = false;
        caList.add(ca7);
        
        // Additional CA to test the sequence checker
        AAS_Course_Assessment__c ca8 = aasDataPrepUtil.createCourseAssessment(course8.Id, 'CA8_' + contextShortName);
        ca8.AAS_Module_Passing_Mark__c = 40;
        ca8.AAS_Final_Result_Release_Exam__c = false;
        caList.add(ca8);
        
        insert caList;
        
        AAS_ToolBox.courseAssessmentSequenceChecker(ca1, new List<String>{'Non Exam Data Loaded'});
        AAS_ToolBox.courseAssessmentSequenceChecker(ca1, new List<String>{'Exam Data Loaded'});
        AAS_ToolBox.courseAssessmentSequenceChecker(ca1, new List<String>{'Cohort Adjustment'});
        AAS_ToolBox.courseAssessmentSequenceChecker(ca1, new List<String>{'Borderline Remark'});
        AAS_ToolBox.courseAssessmentSequenceChecker(ca1, new List<String>{'Exam Adjustment'});
        AAS_ToolBox.courseAssessmentSequenceChecker(ca1, new List<String>{'Final Result Calculation'});
        AAS_ToolBox.courseAssessmentSequenceChecker(ca1, new List<String>{'Result Release Lock'});
        AAS_ToolBox.courseAssessmentSequenceChecker(ca1, new List<String>{'Final Result Release Display'});
    }
}