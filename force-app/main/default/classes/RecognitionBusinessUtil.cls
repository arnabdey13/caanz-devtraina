/**
 * RecognitionBusinessUtil
 * 
 * Recognition package - 
 * set of utilities for performing matching
 * - noise lookup functionality 
 * - pattern recognition utils 
 * - standardisation utils 
 * 
 * @author akopec March 2018
 * 
 */

public class RecognitionBusinessUtil {

	// set of lookups on matching noise
	public static Map<String, String> lkpBusWords;
	public static Map<String, String> lkpLocalityNoise;
	public static Map<String, String> lkpCommonNoise;
	public static Map<String, String> lkpTrustee;
	public static Map<String, String> lkpNumWords;
	public static Map<String, String> lkpLegalStd;
    
    public static Map<String, String> lkpCities;
	public static Map<String, String> lkpFirstnames;
    public static Map<String, String> lkpPracticeNoise;
    public static Map<String, String> lkpImportantPractice;
       
	/**
	*  load basic Lookups
	*/
	public static void loadBusNameLookups()
	{
		System.debug('Loading Bus Name Lookups');
        
		// Locality Noise Lookup
		if (lkpLocalityNoise==null)
	  		lkpLocalityNoise = RecognitionBusLkpResource.LocalityNoise;

		// Common Noise Lookup
		if (lkpCommonNoise==null)
	  		lkpCommonNoise = RecognitionBusLkpResource.CommonNoise;

		// Standard legal words Lookup
		if (lkpLegalStd==null)
	  		lkpLegalStd = RecognitionBusLkpResource.legalWords;
	  
		// Trustee Basic
		if (lkpTrustee==null)
	    	lkpTrustee = RecognitionBusLkpResource.Trustee;
        
        // Cities
		if (lkpCities==null)
	    	lkpCities = RecognitionBusLkpResource.Cities;
        
        // Frequent Firstnames in Company Names (First word only)
		if (lkpFirstnames==null)
	    	lkpFirstnames = RecognitionBusLkpResource.Firstnames;

        // Frequent Practice Noise in Company Names (First word only)
		if (lkpPracticeNoise==null)
	    	lkpPracticeNoise = RecognitionBusLkpResource.PracticeNoise;        

        // Important Practice
		if (lkpImportantPractice==null)
	    	lkpImportantPractice = RecognitionBusLkpResource.ImportantPractice;         
                
	}

     ////////////////////////  Recognition Utils ////////////////////////
 
    /**
    * Standardise Business Name
    * Perform typical cleaning of business names:
    *  - remove Legal Noise
    *  - remove Locality Noise
    *  - remove Common Noise
    *  - remove City Noise
	*
    **/
    public static String stdBusName( String busName){
        String busClean = busName.toUpperCase();     
        busClean = rmNonAlphaNum(busClean);
        busClean = rmCity(busClean);        
        busClean = rmAllNoise(busClean);
        // large practice handling
        busClean = stdImportantPractice(busClean);
		busClean = stdFirstPracticeNoise(busClean);
        busClean = stdFirstFirstname(busClean);
		busClean = joinInitials(busClean);

		return busClean;
    }
    
    /**
    * Generate Business Name Soundex on Full Name
    * Perform typical cleaning of business names:
    *  - remove Noise
    *  - apply soundex
    **/
    public static String getBusNameFullSoundex( String busName){
        String busClean = stdBusName(busName);               
		return soundex(busClean);
    }
    
    /**
    * Generate  Business Name Search Key to include all potential candidates in searching
    * Perform just basic noise clean and take the first token
    * If the Toke is shorter than 3 letters take first two (at this point we should not have anything starting with "The" and other noise)
    *  - apply soundex
    **/
    public static String genBusSearchKey( String busName){
        String busClean = busName.toUpperCase();           
        busClean = rmNonAlphaNum(busClean);
        busClean = rmCity(busClean);        
        busClean = rmAllNoise(busClean); 
        busClean = joinInitials(busClean);
        
        List<String> Tokens = busClean.split(' ');
        if (Tokens.size()>1)
			return (Tokens[0].length()>2)? soundex(Tokens[0]) : soundex(Tokens[0] + Tokens[1]);
        else
            return soundex(busClean);
    }

    /**
	* Function generate Search Keys based on Business Name
	* Takes only first two words, applies cleaning first
	* Replace soundex with Stemming
	**/
	public static String genBusNameStem(String busName)
	{
		RecognitionPorterStemmer porterStemmer = new RecognitionPorterStemmer();
        String stemName;
        String busClean = stdBusName(busName); 
        List<String> Tokens = busClean.split(' ');
        if (Tokens.size() > 1)
            return  porterStemmer.stemWord(Tokens[0]) + porterStemmer.stemWord(Tokens[1]);
        else
            return  porterStemmer.stemWord(busClean);
	}
     
 	///////////////////////  Std Utils ///////////////////////////   
   /**
    * Standardise Phone
    * Perform cleaning on phone
	*
    **/
    public static String stdPhone( String phone){
        String phoneClean = phone.deleteWhitespace();               
		return phoneClean;
    }
    
	/**
    * Remove Non Alpha Numeric characters
    **/
    public static String rmNonAlphaNum( String dirty){
		Pattern nonAlphanum = Pattern.compile('[^a-zA-Z0-9 ]');
        Matcher matcher = nonAlphanum.matcher(dirty);              
		return matcher.replaceAll(' ');
    }
    
    
    /**
    * Standardise Street Address
	* RecognitionBusinessUtil.get Street('123 George St);
    **/
    public static String getStreet(String streetAddress){
        if ( streetAddress.contains('BOX'))
            return '';
        List<String> tokens = streetAddress.normalizeSpace().split(' ');
        String street = '';

        for (Integer idx=tokens.size()-1; idx>0; idx--){
             if (!tokens[idx].substring(0,1).isNumeric())
                street = tokens[idx];
            else
                break;
    	}
        return street;
    }
    
   /**
    * Standardise Business Name - join Initials separated by space
	* RecognitionBusinessUtil.joinInitials
    **/
    public static String joinInitials( String busName){
        List<String> tokens = busName.trim().split(' ');
        String clean = '';
        for (Integer idx=0; idx<tokens.size(); idx++){
             if ((tokens[idx]).length()==1) {
                clean += tokens[idx];
            }
            else {
                if (clean.length()>1)  // there are some
                    for (Integer ldx=idx; ldx< tokens.size() ; ldx++) {
                    	clean += ' ' + tokens[ldx];
                    }
                else
                    clean = busName;
                break;
        	}
        }       
		return clean;
    }
    
	/////////////////////// Lookup Business Noise Utils ///////////////////////////
	/**
	* Function Standardise One Business Word

	public static String getStdBusWord(String str)
	{
        if (lkpBusWords==null)
           loadBusNameLookups();
		if (lkpBusWords.containsKey(str))
			return lkpBusWords.get(str);
        else
            return str;
	}
	**/
    
    /**
	* Function Standardise Locality
	**/
	public static String getStdLocality(String str)
	{
        if (lkpLocalityNoise==null)
           loadBusNameLookups();
		if (lkpLocalityNoise.containsKey(str))
			return lkpLocalityNoise.get(str);
        else
            return str;
	}

    /**
	* Function Standardise Legal Words 
	**/
	public static String getStdLegal(String str)
	{
        if (lkpLegalStd==null)
           loadBusNameLookups(); 
		if (lkpLegalStd.containsKey(str))
			return lkpLegalStd.get(str);
        else
            return str;     
	}

	/**
	* Function Finds Trustee Patterns 
	**/
	public static String getTrusteePattern(String str)
	{
        if (lkpTrustee==null)
           loadBusNameLookups();  
		if (lkpTrustee.containsKey(str))
			return lkpTrustee.get(str);
        else
            return str;      
	}
	
    /**
	* Function checks for the existence of locality noise in the string
	* and removes it from the string (VIC, AUS, TAS ...)
	**/
	public static String rmLocalityNoise(String str)
	{
        String clean;
		if (lkpLocalityNoise==null)
           loadBusNameLookups();       
        List <String> strTokens = delimitedStringToList(str);
        for (String token : strTokens)
            if (!lkpLocalityNoise.containsKey(token))
				clean = (clean==null)? token : clean + ' ' + token;
        return clean;
	}

    /**
	* Function checks for the existence of locality noise in the string
	* and replace it with standard value
	**/
	public static String replaceLocalityNoise(String str)
	{
        String clean;
		if (lkpLocalityNoise==null)
           loadBusNameLookups();        
        List <String> strTokens = delimitedStringToList(str);
        for (String token : strTokens)
			if (!lkpLocalityNoise.containsKey(token))
				clean = (clean==null)? token : clean + ' ' + token;
        	else
				clean = (clean==null)? token : clean + ' ' + lkpLocalityNoise.get(token);               
        return clean;
	}


    /**
    * Standardise First Word Firstnames
	* RecognitionBusinessUtil.stdFirstPracticeNoise('Accounting and Tax');
    **/
    public static String stdFirstPracticeNoise(String str){
        String clean;
		if (lkpPracticeNoise==null)
           loadBusNameLookups();        
        List <String> strTokens = delimitedStringToList(str);
        Integer counter=0;
        for (String token : strTokens) {
			if (lkpPracticeNoise.containsKey(token) && counter == 0)
					clean = lkpPracticeNoise.get(token);
            else
				clean = (clean==null)? token : clean + ' ' + token;
            
            counter += 1;
		}    
        return clean;		
    }        
    
    
    
	/**
    * Standardise First Word Firstnames
	* RecognitionBusinessUtil.stdFirstFirstname('Andrew Moore & CO');
    **/
    public static String stdFirstFirstname(String str){
        String clean;
		if (lkpFirstnames==null)
           loadBusNameLookups();        
        List <String> strTokens = delimitedStringToList(str);
        Integer counter=0;
        for (String token : strTokens) {
			if (lkpFirstnames.containsKey(token) && counter == 0)
					clean = lkpFirstnames.get(token);
            else
				clean = (clean==null)? token : clean + ' ' + token;
            
            counter += 1;
		}    
        return clean;		
    }        
    
    /**
    * Standardise Important Practice - we do not want any noise just the practice name
    * Need upper case and no noise e.g. AND
	* RecognitionBusinessUtil.stdImportantPractice('ERNST YOUNG SERVICES ADELAIDE');
	* RecognitionBusinessUtil.stdImportantPractice('PRICEWATERHOUSECOOPERS CONSULTING TAX LEGAL');
    **/
    public static String stdImportantPractice(String str){
		if (lkpImportantPractice==null)
           loadBusNameLookups();        
        List <String> strTokens = delimitedStringToList(str);
		if (!lkpImportantPractice.containsKey(strTokens[0]))
                return str;
        
        //  we probably have importante practice
        String ImportantPractice = lkpImportantPractice.get(strTokens[0]);
        if (str.startsWith(ImportantPractice))
            return ImportantPractice;
        else
            return str;
    }        
    
        
    
	/**
	* Function checks for the existence of legal noise in the string
	* and replace it with standard value
	**/
	public static String replaceLegalNoise(String str)
	{
        String clean;
		if (lkpLegalStd==null)
           loadBusNameLookups();    
        List <String> strTokens = delimitedStringToList(str);
        for (String token : strTokens)
            if (!lkpLegalStd.containsKey(token))
            	clean = (clean==null)? token : clean + ' ' + token;
        	else
				clean = (clean==null)? token : clean + ' ' + lkpLegalStd.get(token);         
        return clean;
	}
    
    /**
	* Function checks for the existence of legal noise in the string
	* and removes it
	**/
	public static String rmLegalNoise(String str)
	{
        String clean;
		if (lkpLegalStd==null)
           loadBusNameLookups();    
        List <String> strTokens = delimitedStringToList(str);
        for (String token : strTokens)
            if (!lkpLegalStd.containsKey(token))
            	clean = (clean==null)? token : clean + ' ' + token;         
        return clean;
	}
    
    /**
	* Function checks for the existence of legal noise in the string
	* and removes it
	* It is a multispace pattern so do not split
	**/
	public static String rmTrusteeNoise(String str)
	{
        String clean = '';
		if (lkpTrustee==null)
           loadBusNameLookups();    
        clean = str.normalizeSpace();
		Set<String> tokens = lkpTrustee.keySet();
        for (String token: tokens){
            if (clean.contains(token))
                clean = clean.remove(token);
        }
        return clean;
	}      
        
  
	/**
	* Function checks for the existence of City and removes it
	* It is a multispace pattern so does not split
	**/
	public static String rmCity(String str)
	{
        String clean = '';
		if (lkpCities==null)
           loadBusNameLookups();    
        clean = str.normalizeSpace();
		Set<String> cities = lkpCities.keySet();
        for (String city: cities){
            if (clean.contains(city)) 
                clean = clean.remove(city).normalizeSpace();
        }
        return clean;
	} 
    
   /**
	* Function checks for the existence of City and removes and returns clean version
	* It is a multispace pattern so does not split
	**/
	public static String stdCity(String city)
	{
        String cleanCity = city.toUpperCase();
		if (lkpCities==null)
           loadBusNameLookups();    
        if (lkpCities.containsKey(cleanCity)) 
			return lkpCities.get(cleanCity);
        return cleanCity;
	} 
    
    /**
	* Function checks for the existence of Common noise in the string
	* and removes it - N/A; T-A; P/L
	**/
	public static String rmCommonNoise(String str)
	{
		String clean;
		if (lkpCommonNoise==null)
           loadBusNameLookups();
		List <String> strTokens = delimitedStringToList(str);
        for (String token : strTokens)
            if (!lkpCommonNoise.containsKey(token))
				clean = (clean==null)? token : clean + ' ' + token;
		return clean;
	}

	/**
	* Function checks for the existence of business words
	* and removes it from the string

	public static String rmBusWords(String str)
	{
		String clean;
		if (lkpBusWords==null)
           loadBusNameLookups(); 
		List <String> strTokens = delimitedStringToList(str);
        for (String token : strTokens)
            if (!lkpBusWords.containsKey(token))
				clean = (clean==null)? token : clean + ' ' + token;
		return clean;
	}
	**/
    
	/**
	* Function checks for the existence of any noise: Location, Legal, Common, Trustee
	* and removes it from the string
	**/
	public static String rmAllNoise(String str)
	{
		String clean = '';
		if (lkpCommonNoise==null)
           loadBusNameLookups(); 
		clean = str.normalizeSpace();
        clean = rmTrusteeNoise(clean);
        clean = rmCommonNoise(clean);
        clean = rmLegalNoise(clean);
        clean = rmLocalityNoise(clean);
        
		return clean;
	}
   
    //////////////////////////// General Utils /////////////////////////////////    
	/***
	* splits multiwords into a Collection of word
	* 
	***/
	private static List<String> delimitedStringToList(String words) { 
		if (String.isEmpty(words)) 
			return new List<String>(); 

        List<String> ptrns = words.normalizeSpace().split(' ');
        
		List<String> cleanPatterns = new List<String>();
		for(String pattern : ptrns) {
			cleanPatterns.add(pattern.trim());
		}
		return cleanPatterns;
	}
      
    /*
     * Soundex Function for fuzzy matching following the standard algorithm
    */
    public static String soundex(String s) { 
        if (s==null | s.length()==0)
            return s;
        
		Map<String,String> mapForSoundex = new Map<String,String>{
    	'A'=>'0','B'=>'1','C'=>'2','D'=>'3','E'=>'0','F'=>'1','G'=>'2','H'=>'0','I'=>'0','J'=>'2','K'=>'2','L'=>'4','M'=>'5',
        'N'=>'5','O'=>'0','P'=>'1','Q'=>'2','R'=>'6','S'=>'2','T'=>'3','U'=>'0','V'=>'1','W'=>'0','X'=>'2','Y'=>'0','Z'=>'2'
    	};
        
        s = s.toUpperCase();
        String sndxStr;
        String prevKey;
         
        for (Integer i=0; i<s.length(); i++){
            String letter = s.substring(i,i+1);
            if (mapForSoundex.containsKey(letter)) {
                if (sndxStr == null || sndxStr.length()==0){
					sndxStr = letter; 
                    prevKey = mapForSoundex.get(letter);
                }
                else {
                    String nextKey = mapForSoundex.get(letter);
                    if ( !nextKey.equals('0') && !nextKey.equals(prevKey))
                        sndxStr += nextKey;
                    prevKey = nextKey;
                }
             }
        }
        sndxStr = sndxStr.rightPad(4, '0').substring(0,4);
        return sndxStr;
    } 
    
    // RecognitionBusinessUtil.getCompanyNameFreq() 
    public static List<String> getCompanyNameFreq() {
		Map<String, Integer> lkp = new Map<String, Integer>();
        String s;
        List<Reference_Recognition_CAANZ_Repository__c> refs = [Select ID, Company_Name__c From Reference_Recognition_CAANZ_Repository__c];
        for ( Reference_Recognition_CAANZ_Repository__c ref : refs) {
            String CompanyName = ref.Company_Name__c;
            if (CompanyName == null || CompanyName.length()==0)
                continue;
            if (CompanyName.indexOf(' ') < 2)
                continue;
            List<String> tokens = CompanyName.split(' ');
            if (tokens.size() < 2)
                continue;
            String token = tokens[0];
            if (token.length() <3 )
                continue;
            if (lkp.containsKey(token))
                 lkp.put(token, lkp.get(token)+1);
            else
                lkp.put(token, 1);
    	}
        
                
        Integer counter = 0;
        List<String> listNames = new List<String>(); 
		List<MapCountWrapper> NameList = new List<MapCountWrapper>();
            
        for (String name : lkp.keySet()) {
            MapCountWrapper myWrapper = new MapCountWrapper(name, lkp.get(Name));
            NameList.add(myWrapper);
        }
		NameList.sort();
        
        // put it back into list
        for (MapCountWrapper mapWrap : NameList ) {
            if (counter < 50) {				
				listNames.add((String)mapWrap.name + ':' + String.valueOf(mapWrap.counter));
				counter += 1;
			}
			else
				break;
        }
       	return listNames;
    }

    
public class MapCountWrapper implements Comparable {
    public String name;
    public Integer counter;
    
    public MapCountWrapper(String name, Integer counter) {
        this.name = name;
        this.counter = counter;
    }
    public Integer compareTo(Object other) {
		MapCountWrapper compareToMap = (MapCountWrapper)other;
        if (this.counter > compareToMap.counter)
            return -1;
		else if (this.counter < compareToMap.counter)
            return 1;
    	return 0;
    }
}

}