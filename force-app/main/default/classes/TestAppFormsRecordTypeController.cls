@IsTest(seeAllData = false)
private class TestAppFormsRecordTypeController  {

    
    @IsTest private static void testFormCategories(){    
        
    Application_Categories__mdt acateg = new Application_Categories__mdt(
            Parent_Category__c = 'Admission apps',
            Sub_Category__c = 'Special_Admissions',
            MyCA__c = true,
            Customer__c = false
    );
     Application_Categories__mdt acateg1 = new Application_Categories__mdt(
            Parent_Category__c = 'Other Applications',
            Sub_Category__c = 'Readmission',
            MyCA__c = true,
            Customer__c = false
    );
    Application_Categories__mdt acateg2 = new Application_Categories__mdt(
            Parent_Category__c = 'Other Applications',
            Sub_Category__c = 'Readmission',
            MyCA__c = false,
            Customer__c = true
    );
    
        SortingWrapper sr = new SortingWrapper(acateg);
        Map<String,List<Application_Categories__mdt>> lstMyCAApplicationtypes = AppFormsRecordTypeController.getApplicationTypes('MyCA');
        Map<String,List<Application_Categories__mdt>> lstCustomerApplicationtypes = AppFormsRecordTypeController.getApplicationTypes('customer');
        
        System.assertEquals(4, lstMyCAApplicationtypes.size(), '');
        System.assertEquals(4, lstCustomerApplicationtypes.size(), '');
             
    
    
    }
    
}