/* 
    Developer: WDCi (KH)
    Development Date: 02/12/2016
    Task #: AAS Final Result Release for exam 
    
    Change History:
    AAS-61 				06/06/2017 		WDCi - KH: 	Step 10 Final Result Release Display Allow Multiple Run    
    LCA-1176            12/03/2018      WDCi LKoh:  added Result Release Date setup into the wizard
*/
public with sharing class AAS_FinalResultReleaseController {

    public AAS_Course_Assessment__c courseAssessment {get; set;}
    public static String selectedRecordType{get;set;}
    public String urlSelectedType {get; set;}
    public Boolean validToProcess {get; set;}
    
    public Boolean isCapstone;

    ID batchprocessid;
    public boolean isUpdateDone {get;set;}
    public boolean isUpdateDoneWithError {get;set;}
    public boolean checkCommitStatus {get; set;}

	// LCA-1176
	public boolean askSuppExamReleaseDate {get;set;}
	public LuanaSMS__Course__c courseDummy {get;set;}
	public String thisIsForSupp {get;set;}
    public String userTimeZone {get;set;}

    public AAS_FinalResultReleaseController(ApexPages.StandardController controller) {
        
        if(!test.isRunningTest()){
            List<String> addFieldName = new List<String>{'Name', 'AAS_Course__c', 'AAS_Course__r.Name', 'AAS_Cohort_Adjustment_Exam__c', 'AAS_Final_Adjustment_Exam__c', 'AAS_Final_Adjustment_Supp_Exam__c', 
                                                        'AAS_Borderline_Remark__c', 'AAS_Borderline_Remark_Supp_Exam__c', 'AAS_Cohort_Adjustment_Supp_Exam__c', 'AAS_Final_Result_Release_Exam__c',
                                                        'AAS_Final_Result_Release_Supp_Exam__c', 'AAS_Final_Result_Calculation_Exam__c', 'AAS_Course__r.LuanaSMS__Program_Offering__r.IsCapstone__c', 
                                                        'AAS_Final_Result_Release_Exam__c', 'AAS_Exam_Adjustment__c', 'AAS_Pre_Final_Adjustment__c', 'RecordType.DeveloperName', 'AAS_Result_Release_Lock__c', 'AAS_Final_Result_Release_Processing__c',
                                                        'AAS_Course__r.Result_Release_Date__c', 'AAS_Course__r.Supp_Result_Release_Date_Time__c' 
                                                        };
            controller.addFields(addFieldName);
            courseAssessment = (AAS_Course_Assessment__c) controller.getRecord();
        } else {
            // LKoh :: 15-05-2017
            // Note :: Added to allow the class to be covered more thoroughly in test class environment
            courseAssessment = [SELECT Name, AAS_Course__c, AAS_Course__r.Name, AAS_Cohort_Adjustment_Exam__c, AAS_Final_Adjustment_Exam__c, AAS_Final_Adjustment_Supp_Exam__c, AAS_Borderline_Remark__c, AAS_Borderline_Remark_Supp_Exam__c, AAS_Cohort_Adjustment_Supp_Exam__c, AAS_Final_Result_Release_Supp_Exam__c, AAS_Final_Result_Calculation_Exam__c, AAS_Course__r.LuanaSMS__Program_Offering__r.IsCapstone__c, AAS_Final_Result_Release_Exam__c, AAS_Exam_Adjustment__c, AAS_Pre_Final_Adjustment__c, RecordType.DeveloperName, AAS_Result_Release_Lock__c, AAS_Final_Result_Release_Processing__c, 
            					AAS_Course__r.Result_Release_Date__c, AAS_Course__r.Supp_Result_Release_Date_Time__c // LCA-1176
            					FROM AAS_Course_Assessment__c WHERE Id = :controller.getRecord().Id];
        }
        
        isCapstone = courseAssessment.AAS_Course__r.LuanaSMS__Program_Offering__r.IsCapstone__c;
        validToProcess = true;
        
        urlSelectedType = apexpages.currentpage().getparameters().get('type');
        selectedRecordType = urlSelectedType + '_SAS';
        doValidation();
         
        isUpdateDone= false;
        isUpdateDoneWithError = false;
        checkCommitStatus= false; 
        
        // LCA-1176
        askSuppExamReleaseDate = false;
        if (courseAssessment.AAS_Course__r.Result_Release_Date__c != null || courseAssessment.AAS_Course__r.Supp_Result_Release_Date_Time__c != null) askSuppExamReleaseDate = true;        
        thisIsForSupp = '';
        courseDummy = new LuanaSMS__Course__c();
        TimeZone tz = UserInfo.getTimeZone();
        userTimeZone = tz.getDisplayName();
        
        System.debug('*****courseAssessment::::: ' + courseAssessment);
        
    }
    
    // LCA-1176
    public List<SelectOption> getReleaseDateOptions(){
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('',''));		
		options.add(new SelectOption('yes', 'YES'));
		options.add(new SelectOption('no', 'NO'));
		return options;
	}
    
    public void doValidation(){
        
        if(selectedRecordType == 'Exam_Assessment_SAS'){
            System.debug('*************::: ' + courseAssessment.RecordType.DeveloperName);
        
            if(!courseAssessment.AAS_Final_Result_Release_Exam__c){
                
                Boolean hasAdjustmentProcessDone = false;
                String adjustmentProcessName = '';
                
                if(courseAssessment.RecordType.DeveloperName == 'Technical_Module'){
                    hasAdjustmentProcessDone = courseAssessment.AAS_Exam_Adjustment__c;
                    adjustmentProcessName = 'Exam Adjustment';
                }else if(courseAssessment.RecordType.DeveloperName == 'Capstone_Module'){
                    hasAdjustmentProcessDone = courseAssessment.AAS_Final_Adjustment_Exam__c;
                    adjustmentProcessName = 'Module Adjustment';
                }
                
                if(!courseAssessment.AAS_Cohort_Adjustment_Exam__c || !courseAssessment.AAS_Borderline_Remark__c || !hasAdjustmentProcessDone || !courseAssessment.AAS_Pre_Final_Adjustment__c || !courseAssessment.AAS_Result_Release_Lock__c){
                    validToProcess = false;
                    addPageMessage(ApexPages.severity.ERROR, 'You are not allowed to continue in this step. Please complete the Cohort Adjustment, Borderline Remark, '+ adjustmentProcessName +', Pre-Final Adjustment and Result Release Lock processes first.');
                }
                
                if(courseAssessment.AAS_Final_Result_Release_Processing__c){
                    validToProcess = false;
                    addPageMessage(ApexPages.severity.ERROR, 'This process is running now, you are not allowed to run this process. Please try it again later.');
                }
            
            //AAS-61: remove this checking to enable to multiple final release process
            }else{
                validToProcess = false;
                addPageMessage(ApexPages.severity.ERROR, 'Final Result Release has completed, you are not allowed to re-do this step.');
            }   
        }
    }
    
 
    public PageReference doCommit(){    
        Savepoint sp = Database.setSavepoint(); 

        // LCA-1176
		system.debug('thisIsForSupp: ' +thisIsForSupp);
		if (askSuppExamReleaseDate && thisIsForSupp == null) {
			addPageMessage(ApexPages.severity.ERROR, 'Please state if the Result Release Date is intended for Supplementary Exam by choosing one of the option');            
            return null;
		}
        
        if (courseDummy.Result_Release_Date__c == null) {
			addPageMessage(ApexPages.severity.ERROR, 'Please provide a Result Release Date');            
            return null;
		}

        try{
            checkCommitStatus = true;
            
            //initial set the CA's Cohort Adjustment & Cohort Batch Processing Flag
            AAS_Course_Assessment__c initialUpdateCA = new AAS_Course_Assessment__c();
            initialUpdateCA.Id = courseAssessment.Id;
            initialUpdateCA = updateFinalResultReleaseBatchProcessingFlag(initialUpdateCA, true, true);
            initialUpdateCA.AAS_Last_Modified_User__c = UserInfo.getUserEmail();
            
            update initialUpdateCA;
            
            // LCA-1176 - update the Course Release Date Field
            LuanaSMS__Course__c courseToSave = new LuanaSMS__Course__c();
            courseToSave.Id = courseAssessment.AAS_Course__c;
            if (askSuppExamReleaseDate && thisIsForSupp == 'yes') {
                courseToSave.Supp_Result_Release_Date_Time__c = courseDummy.Result_Release_Date__c;
            } else {
                courseToSave.Result_Release_Date__c = courseDummy.Result_Release_Date__c;
            }
            update courseToSave;
            
            PageReference returnPage = new ApexPages.StandardController(initialUpdateCA).view();
            returnPage.setRedirect(true);
            return returnPage;
   
        }Catch(Exception exp){
            Database.rollback(sp);
            addPageMessage(ApexPages.severity.ERROR, 'Error processing. Please try again later or contact your system administrator. Error: ' + exp.getMessage() + ' - ' + exp.getStackTraceString());
            
            return null;
        }
        
    }
    
    
    public PageReference doRedirect(){
        if(isUpdateDone){
            PageReference returnPage = new PageReference('/'+courseAssessment.Id);
            return returnPage;
        }else{
            if(!checkCommitStatus){
                addPageMessage(ApexPages.severity.ERROR, 'Error found during Commit process. Please try again later or contact your system administrator.');
            }
            return null;
        }
    }
    
    public PageReference doCancel(){
        PageReference returnPage = new PageReference('/'+courseAssessment.Id);
        return returnPage;
    }
    
    public void addPageMessage(ApexPages.severity sev, String msg){
        ApexPages.Message warningmsg = new ApexPages.Message(sev, msg);
        ApexPages.addmessage(warningmsg);
    }   
    
    //-----------------------new change Apr 2017 for batch function-----------------------------//
    public AAS_Course_Assessment__c updateFinalResultReleaseBatchProcessingFlag(AAS_Course_Assessment__c ca, Boolean processingFlagStatus, Boolean finalResultReleaseFlag){
        ca.AAS_Final_Result_Release_Processing__c = processingFlagStatus;
        //ca.AAS_Final_Result_Release_Exam__c  = finalResultReleaseFlag;
        return ca;
    }
    
    public PageReference toLandingPage(){
        PageReference landingPage = Page.AAS_FinalResultReleaseLanding;
        landingPage.getParameters().put('id', courseAssessment.Id);
        landingPage.getParameters().put('type', urlSelectedType);
        landingPage.setredirect(true);
        return landingPage;
    }

    public PageReference doWebserviceCallout(){
        /*try{
            String requestBody = prepareCARequestBody(courseAssessment);
            System.debug('*********doWebserviceCallout:: ' + requestBody);
            
            Dom.Document doc = new Dom.Document();
            doc.load(requestBody);
            
            HttpResponse response = boomiCallout(doc);
            
            if(response.getStatusCode() != 200){
                addPageMessage(ApexPages.severity.ERROR, 'Fail to process ' + courseAssessment.Name + ' (' + response.getStatus() + '). Please contact our support.');
                checkCommitStatus = false;
                return null;
            }else{
                
            
                return toLandingPage();
            }
        }Catch(Exception exp){
            addPageMessage(ApexPages.severity.ERROR, 'Error processing. Please try again later or contact your system administrator. Error: ' + exp.getMessage() + ' - ' + exp.getStackTraceString());
            checkCommitStatus = false;
        }*/
        return null;
    }

   
    //-----------outbound message used
    public static HttpResponse boomiCallout(DOM.Document doc) {
        /*Http h = new Http();
        
        //This is for WDCI Main Test: https://connect.boomi.com/ws/simple/upsertStudentProgramSubject;boomi_auth=d2RjaS1NVzY3MVM6ZmMzZTEzOTEtOWZkOS00MmQ1LWFiMTEtOTk3ZmQ1MTI4NTM1
        //This is for Devluana3 Test: https://test.connect.boomi.com/ws/simple/upsertStudentProgramSubject;boomi_auth=Q0FAY2hhcnRlcmVkYWNjb3VudGFudHNvZmF1cy04TlY0N0MuTDdBT0wzOjE2M2I2YTU3LTdjNWUtNGY1NC04ZjM3LTUyMzdjNzg1OTBmYQ==
        
        HttpRequest req = new HttpRequest();
        //Custom Settings
        Service_Integrations__c boomiIntegration = Service_Integrations__c.getInstance('Boomi AAS FinalResultRelease REST');  
        String url = boomiIntegration.Endpoint_URL__c; //'https://test.connect.boomi.com/ws/simple/upsertOrder_IUC02;boomi_auth=Q0FAY2hhcnRlcmVkYWNjb3VudGFudHNvZmF1cy04TlY0N0MuTDdBT0wzOmViOWViMTdhLTJjNGYtNGRhNi04NDBkLTg4NWQ0YTBmMDNkYg==';
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'text/xml');            
        req.setBodyDocument(doc);
        req.setTimeout(120000);
    
        HttpResponse res = h.send(req);
        
        return res;        
        */
        return null;
    }
    
    private String prepareCARequestBody(AAS_Course_Assessment__c ca){
        /*
        String body = '';
        
        body = '<AAS_Course_Assessment__c>';
            body += createElement('Id', ca.Id);
            body += createElement('AAS_Course__c', ca.AAS_Course__c);
        body += '</AAS_Course_Assessment__c>';
        
        return body;
        */
        return null;
    }
    
    private String createElement(String fieldName, String value){
        /*
        if(value != null){
            value = value.replace('&', '&amp;');
            value = value.replace('"', '&quot;');
            value = value.replace('\'', '&apos;');
            value = value.replace('<', '&lt;');
            value = value.replace('>', '&gt;');
            
            return '<' + fieldName + '>' + value + '</' + fieldName + '>';
        } else {
            return '<' + fieldName + '/>';
        }
        */
        return null;
    }
  

}