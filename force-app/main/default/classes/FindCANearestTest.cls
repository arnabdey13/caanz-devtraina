/*
    Test class for FindCA API Nearest Search.cls
    ======================================================
    Changes:
        November 2017    Andrew Kopec  Created
*/

@isTest
private class FindCANearestTest {
    @testSetup 
    static void setup() {
        
        Account accountPersonNZ = new Account(
            //RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Member').getRecordTypeId()
            RecordTypeId = RecordTypeCache.getId('Account', 'Full_Member')
            , Salutation = 'Mr'
            , FirstName = 'John'
            , LastName = 'Smith'
            , Preferred_Name__c = 'Johnny'
            , Gender__c = 'Male'
            , Membership_Type__c = 'Member'
            , Member_Of__c = 'NZICA'
            , Status__c = 'Active'
            , Opt_out_of_Find_an_Accountant_register__c = false
            , Assessible_for_CAF_Program__c = false
            , PersonOtherPhone = '051234567'
            
            // NZ attributes
            , PersonOtherCountry = 'New Zealand'
            , Affiliated_Branch_Country__c = 'New Zealand'
            , Affiliated_Branch__c = 'Auckland'
            , PersonEmail = 'test.find.ca@tester.find.ca.co.nz'
            
            // Qualified Auditor attributes
            , Qualified_Auditor__c = true
            , QA_Date_of_suspension__c = null
            , QA_Date_of_de_recognition__c = null
        
            // Insolvency Practitioner attributes
            , Insolvency_Practitioner__c = true
            , NZAIP_Date_of_suspension__c = null
            , NZAIP_Date_of_de_recognition__c = null
            
            // Public Practitioner attributes
            , Find_CA_Opt_In__c = true
            , CPP_Picklist__c = 'Full'
            ,Communication_Preference__c= 'Home Phone'
            ,PersonHomePhone= '1234'
            ,PersonOtherStreet= '83 Saggers Road'
            ,PersonOtherCity='JITARNING'
          
            
            ,PersonOtherPostalCode='6365' 
        );
        
        Account accountPersonAU = new Account(
            //RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Member').getRecordTypeId()
            RecordTypeId = RecordTypeCache.getId('Account', 'Full_Member')
            , Salutation = 'Mr'
            , FirstName = 'Andrew'
            , LastName = 'Brown'
            , Preferred_Name__c = 'Andy'
            , Gender__c = 'Male'
            , Membership_Type__c = 'Member'
            , Member_Of__c = 'ICAA'
            , Status__c = 'Active'
            , Opt_out_of_Find_an_Accountant_register__c = false
            , Assessible_for_CAF_Program__c = false
            , PersonOtherPhone = '0299123456'
            
            // AU attributes
            , PersonOtherCountry = 'Australia'
            , Affiliated_Branch_Country__c = 'Australia'
            , Affiliated_Branch__c = 'New South Wales'
            , PersonEmail = 'test.find.ca@tester.find.ca.co.au'
            
            // Specialists (SMSF, BV, FP) attributes
            , Financial_Category__c = 'Standard fee applies'
            , SMSF__c = true
            , BV__c = true
            , FP_Specialisation__c = true
            
            // Rev 01 area of practice
            , BV_Areas_of_Practice__c =  'Tax;Family Law'
            , FP_Areas_of_Practice__c =  'TBA'
            , SMSF_Areas_of_Practice__c =  'TBA'
            
            // Public Practitioner attributes
            , Find_CA_Opt_In__c = true
            , CPP_Picklist__c = 'Full'
            ,Communication_Preference__c= 'Home Phone'
            ,PersonHomePhone= '1234'
            ,PersonOtherStreet= '83 Saggers Road'
            ,PersonOtherCity='JITARNING'
            ,PersonOtherState='New South Wales'
            
            ,PersonOtherPostalCode='6365' 
        );
        
        
        Account accountPersonAU2 = new Account(
            //RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Member').getRecordTypeId()
            RecordTypeId = RecordTypeCache.getId('Account', 'Full_Member')
            , Salutation = 'Ms'
            , FirstName = 'Susan'
            , LastName = 'Jackson'
            , Preferred_Name__c = 'Suzi'
            , Gender__c = 'Female'
            , Membership_Type__c = 'Member'
            , Member_Of__c = 'ICAA'
            , Status__c = 'Active'
            , Opt_out_of_Find_an_Accountant_register__c = false
            , Assessible_for_CAF_Program__c = false
            , PersonOtherPhone = '0299886655'
            
            // AU attributes
            , PersonOtherCountry = 'Australia'
            , Affiliated_Branch_Country__c = 'Australia'
            , Affiliated_Branch__c = 'New South Wales'
            , PersonEmail = 'suzi.find.ca@tester.find.ca.co.au'
            
            // Specialists (SMSF, BV, FP) attributes
            , Financial_Category__c = 'Standard fee applies'
            , SMSF__c = false
            , BV__c = true
            , FP_Specialisation__c = false
            
            // Rev 01 area of practice
            , BV_Areas_of_Practice__c =  'Tax;Family Law'
            , FP_Areas_of_Practice__c =  ''
            , SMSF_Areas_of_Practice__c =  ''
            
            // Public Practitioner attributes
            , Find_CA_Opt_In__c = true
            , CPP_Picklist__c = 'Full'
            ,Communication_Preference__c= 'Home Phone'
            ,PersonHomePhone= '1234'
            ,PersonOtherStreet= '83 Saggers Road'
            ,PersonOtherCity='JITARNING'
            ,PersonOtherState='New South Wales'
            
            ,PersonOtherPostalCode='6365' 
        );
        
        
        Account accountBusinessNZ = new Account(
            //RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId()
            RecordTypeId = RecordTypeCache.getId('Account', 'Business_Account')
            , Name = 'Business QA  New Zealand'
            , Status__c = 'Active'
            , Member_ID__c = '12345'
            , Type = 'Chartered Accounting'
            , BillingStreet = '123 Street'
            , BillingCity = 'Auckland'
            , BillingCountry = 'New Zealand'
            , BillingPostalCode = '1234'
            , BillingLatitude =  -36.8804549
            , BillingLongitude = 174.7407973
            , Member_Of__c = 'NZICA'
            , Website = 'www.caanz.com.nz'
            , Affiliated_Branch_Country__c = 'New Zealand'
            , Affiliated_Branch__c = 'Auckland'
            , Opt_out_of_Find_an_Accountant_register__c = false
            
            // Qualified Auditor attributes
            , Qualified_Auditor__c = true
            , QA_Date_of_suspension__c = null
            , QA_Date_of_de_recognition__c = null
        );
        
        Account accountBusinessAU = new Account(
            //RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId()
            RecordTypeId = RecordTypeCache.getId('Account', 'Business_Account')
            , Name = 'Chartered Accountants Australia'
            , Status__c = 'Active'
            , Member_ID__c = '98765'
            , Type = 'Chartered Accounting'
            , BillingCountry = 'Australia'
            , BillingStreet = '33 Erskine St'
            , BillingCity = 'Sydney'
            , BillingState = 'New South Wales'
            , BillingPostalCode = '2060'
            , BillingLatitude = -35.000
            , BillingLongitude = 154.000
            , Member_Of__c = 'ICAA'
            , Website = 'www.caanz.com.au'
            
        );
        insert new List<Account>{accountPersonNZ, accountPersonAU, accountPersonAU2, accountBusinessNZ, accountBusinessAU};
        
        Employment_History__c employHistNZ = new Employment_History__c(
            Member__c = accountPersonNZ.Id
            , Employer__c = accountBusinessNZ.Id
            , Job_Title__c = 'Principal'
            , Status__c = 'Current'
            , Primary_Employer__c = true
            , Employee_Start_Date__c = System.Today()
        );
        Employment_History__c employHistAU = new Employment_History__c(
            Member__c = accountPersonAU.Id
            , Employer__c = accountBusinessAU.Id
            , Job_Title__c = 'Principal'
            , Status__c = 'Current'
            , Primary_Employer__c = true
            , Employee_Start_Date__c = System.Today()
        );
        Employment_History__c employHistAU2 = new Employment_History__c(
            Member__c = accountPersonAU2.Id
            , Employer__c = accountBusinessAU.Id
            , Job_Title__c = 'Accountant'
            , Status__c = 'Current'
            , Primary_Employer__c = true
            , Employee_Start_Date__c = System.Today()
        );
        insert new List<Employment_History__c>{employHistNZ, employHistAU, employHistAU2};
    }

    static testMethod void testDoGetNoMemberType() {

    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();
  
    req.requestURI = 'https://charteredaccountantsanz--devkopec.cs31.my.salesforce.com/services/apexrest/FindCA/v1.0/Public';  
    req.httpMethod = 'GET';
    req.addParameter('Latitude','-35.500');
    req.addParameter('Longitude','154.000');
    req.addParameter('Limit','1');
        
    RestContext.request = req;
    RestContext.response = res;    
        
    List<FindCANearest.CA> results = FindCANearest.doGet();
    System.assertEquals(1, results.size());
        
    for ( FindCANearest.CA ca : results) 
        System.assertEquals('33 Erskine St, Sydney, NSW, 2060', ca.BusinessAddress);
        
    System.debug(results);
       
  }
    
    
     static testMethod void testDoGetWithMemberType() {
        
    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();
  
    req.requestURI = 'https://charteredaccountantsanz--devkopec.cs31.my.salesforce.com/services/apexrest/FindCA/v1.0/Public';  
    req.httpMethod = 'GET';
    req.addParameter('MemberType', 'Business Valuation Specialist');
    req.addParameter('Latitude','-34.000');
    req.addParameter('Longitude','154.000');

    RestContext.request = req;
    RestContext.response = res;    
        
    List<FindCANearest.CA> results = FindCANearest.doGet();
    System.assertEquals(2, results.size());
        
    for ( FindCANearest.CA ca : results) 
        System.assertEquals('33 Erskine St, Sydney, NSW, 2060', ca.BusinessAddress);
        
    System.debug(results);
        
  }

    
    
    static testMethod void testDoGetWithMemberTypeQANZ() {
        
    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();
  
    req.requestURI = 'https://charteredaccountantsanz--devkopec.cs31.my.salesforce.com/services/apexrest/FindCA/v1.0/Public';  
    req.httpMethod = 'GET';
    req.addParameter('Latitude','-34.000');
    req.addParameter('Longitude','174.500');
    req.addParameter('MemberType', 'Qualified Auditor');
        
    RestContext.request = req;
    RestContext.response = res;    
        
    List<FindCANearest.CA> results = FindCANearest.doGet();
    System.assertEquals(1, results.size());
        
    for ( FindCANearest.CA ca : results) 
        System.assertEquals('123 Street, Auckland, 1234', ca.BusinessAddress);
      
    System.debug(results);
        
  }
    
   static testMethod void testDoGetWithMemberTypeIPNZ() {
        
    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();
  
    req.requestURI = 'https://charteredaccountantsanz--devkopec.cs31.my.salesforce.com/services/apexrest/FindCA/v1.0/Public';  
    req.httpMethod = 'GET';
    req.addParameter('Latitude','-36.000');
    req.addParameter('Longitude','174.000');
    req.addParameter('MemberType', 'Insolvency Practitioner');
        
    RestContext.request = req;
    RestContext.response = res;    
        
    List<FindCANearest.CA> results = FindCANearest.doGet();
    System.assertEquals(1, results.size());
        
    for ( FindCANearest.CA ca : results) 
        System.assertEquals('123 Street, Auckland, 1234', ca.BusinessAddress);
      
    System.debug(results);
        
  }
    
    
   static testMethod void testDoGetWithMemberTypeCPPNZ() {
        
    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();
  
    req.requestURI = 'https://charteredaccountantsanz--devkopec.cs31.my.salesforce.com/services/apexrest/FindCA/v1.0/Public';  
    req.httpMethod = 'GET';
    req.addParameter('Latitude','-35.500');
    req.addParameter('Longitude','174.000');
    req.addParameter('MemberType', 'Public Practitioner');
    req.addParameter('Limit','1');
       
    RestContext.request = req;
    RestContext.response = res;    
        
    List<FindCANearest.CA> results = FindCANearest.doGet();
    System.assertEquals(1, results.size());
        
       for ( FindCANearest.CA ca : results) {
                System.assertEquals('123 Street, Auckland, 1234', ca.BusinessAddress);
       }
 
    System.debug(results);
        
  }
    
    
    static testMethod void testDoGetWithMemberTypeRadius() {
        
    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();
  
    req.requestURI = 'https://charteredaccountantsanz--devkopec.cs31.my.salesforce.com/services/apexrest/FindCA/v1.0/Public';  
    req.httpMethod = 'GET';
    req.addParameter('Latitude','-36.800');
    req.addParameter('Longitude','174.750');
    req.addParameter('MemberType', 'Public Practitioner');
    req.addParameter('Radius','50');

    RestContext.request = req;
    RestContext.response = res;    
        
    List<FindCANearest.CA> results = FindCANearest.doGet();
    System.assertEquals(1, results.size());
        
       for ( FindCANearest.CA ca : results) {
                System.assertEquals('123 Street, Auckland, 1234', ca.BusinessAddress);
       }
 
    System.debug(results);
        
  }
    
    
}