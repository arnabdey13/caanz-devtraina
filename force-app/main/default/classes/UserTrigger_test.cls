/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class UserTrigger_test {
	private static User MemberPortalSiteGuestUserObjectInDB; // RunAs Guest
	private static Account FullMemberAccountObjectInDB; // Person Account
	private static Id FullMemberContactIdInDB;
	private static User PersonAccountPortalUserObjectInDB;
	static{
		insertMemberPortalSiteGuestUserObject();
		insertFullMemberAccountObject();
	}
	private static void insertMemberPortalSiteGuestUserObject(){
		MemberPortalSiteGuestUserObjectInDB = TestObjectCreator.createMemberPortalSiteGuestUser();
		//## Required Relationships
		//## Additional fields and relationships / Updated fields
		insert MemberPortalSiteGuestUserObjectInDB;
	}
	
	private static void insertFullMemberAccountObject(){
		FullMemberAccountObjectInDB = TestObjectCreator.createFullMemberAccount();
		//## Required Relationships
		//## Additional fields and relationships / Updated fields
		FullMemberAccountObjectInDB.Create_Portal_User__c = false;
		insert FullMemberAccountObjectInDB;
		FullMemberContactIdInDB = [Select id from Contact where AccountId=:FullMemberAccountObjectInDB.Id].Id;
		
		//FullMemberAccountObjectInDB.OwnerId = '005900000024aY1AAI';
		//update FullMemberAccountObjectInDB;
	}
	
	private static User getPersonAccountPortalUserObject(){
		User PersonAccountPortalUserObject = TestObjectCreator.createPersonAccountPortalUser();
		//## Required Relationships
		PersonAccountPortalUserObject.ContactId = FullMemberContactIdInDB;
		PersonAccountPortalUserObject.Profileid = ProfileCache.getId('NZICA Community Login User');
		//## Additional fields and relationships / Updated fields
		PersonAccountPortalUserObject.Username = 'PersonAccountPortalUserX@gmail.com';
		return PersonAccountPortalUserObject;
	}
	//******************************************************************************************
	//                             TestMethods
	//******************************************************************************************
	/*
		Is this trigger still needed now that there is SSO?
	*/
	
	static testMethod void UserTrigger_test() {
		System.runAs(MemberPortalSiteGuestUserObjectInDB) {
		//	http://blog.jeffdouglas.com/2010/09/02/error-portal-account-owner-must-have-a-role/ 
			User PersonAccountPortalUserObject = getPersonAccountPortalUserObject();
			Test.startTest();
			//insert PersonAccountPortalUserObject;
			Test.stopTest();
			
			// Check Results
			//List<User> User_List = [Select FederationIdentifier From User where Id = :PersonAccountPortalUserObject.Id];
			//System.assertEquals(1, User_List.size(), 'User_List.size' );
			//System.assertNotEquals(null, User_List[0].FederationIdentifier, 'FederationIdentifier' );
		}
	}
}