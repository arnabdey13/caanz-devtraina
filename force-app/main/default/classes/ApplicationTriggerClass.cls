public with sharing class ApplicationTriggerClass {
    
    public static Id IPPRecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByName().get('IPP Provisional Member').getRecordTypeId();
    public static Id MemberRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Member').getRecordTypeId(); 
       
    public static void checkApplicationChanges(Map<Id,Application__c> oldMap,Map<Id,Application__c> newMap){
        
        set<Application__c> approvedIds = new set<Application__c>();
        for(Application__c app:newMap.values()){            
            if(newMap.get(app.Id).RecordTypeId == IPPRecordTypeId &&
               newMap.get(app.Id).Application_Status__c == 'Approved' && 
               (oldMap.get(app.Id).Application_Status__c<>newMap.get(app.Id).Application_Status__c)) {
                    approvedIds.add(app);
               }              
           }
               if(approvedIds.size()>0)            
               {
                        processAccountUpdates(approvedIds,newMap);
               }
    }       
    public static void processAccountUpdates(set<Application__c> approvedIds,Map<Id,Application__c> newMap){
              
              set<Id> accIds = new set<Id>();
              List<Account> UpdateAcc = new List<Account>();
              Map<Id,Account> applicants;
              for(Application__c appl:newMap.values()){
                    accIds.add(appl.Account__c);
                } 
              if(!accIds.isEmpty()){
                applicants = new Map<Id,Account>([select Id,Accessible_for_ICAI__c,Accessible_for_ICAN__c,Accessible_for_ICAP__c,Accessible_for_ICASL__c,Assessible_for_CAF_Program__c,
                                                  Assessible_for_CA_Program__c,Membership_Class__c,Membership_Type__c,Designation__c,Financial_Category__c,RecordTypeId
                                                  from Account where id=:accIds]);
              }
              
              if(!applicants.isEmpty()) {
              for(Application__c app:approvedIds){ // Run it for approved Ids Only
               
                if(applicants.containsKey(app.Account__c)){
                
                   Account targetAcc =   applicants.get(app.Account__c);                   
                   if(app.Accounting_Bodies__c == 'CA Sri Lanka'){                        
                       targetAcc.Accessible_for_ICASL__c = true;
                       targetAcc.Accessible_for_ICAI__c = false;
                       targetAcc.Accessible_for_ICAN__c = false;
                       targetAcc.Accessible_for_ICAP__c = false; 
                   }
                   else if(app.Accounting_Bodies__c == 'ICAI (India)'){
                       targetAcc.Accessible_for_ICAI__c = true;
                       targetAcc.Accessible_for_ICASL__c = false;                       
                       targetAcc.Accessible_for_ICAN__c = false;
                       targetAcc.Accessible_for_ICAP__c = false;   
                   }
                   else if(app.Accounting_Bodies__c == 'ICAN (Nepal)'){
                       targetAcc.Accessible_for_ICAN__c = true;
                       targetAcc.Accessible_for_ICASL__c = false;
                       targetAcc.Accessible_for_ICAI__c = false;                       
                       targetAcc.Accessible_for_ICAP__c = false; 
                   }
                   else if(app.Accounting_Bodies__c == 'ICAP (Pakistan)'){
                       targetAcc.Accessible_for_ICAP__c = true;
                       targetAcc.Accessible_for_ICAI__c = false;
                       targetAcc.Accessible_for_ICAN__c = false;
                       targetAcc.Accessible_for_ICASL__c = false;
                   }
                    
                   targetAcc.Assessible_for_CAF_Program__c = false;
                   targetAcc.Assessible_for_CA_Program__c = false;                 
                   targetAcc.Membership_Class__c ='Provisional';
                   targetAcc.Membership_Type__c ='Member';
                   targetAcc.Designation__c ='CA';
                   targetAcc.Financial_Category__c = 'Nil Rated';
                   targetAcc.RecordTypeId = MemberRecordType;
                   
                   UpdateAcc.add(targetAcc);                   
                   if(!UpdateAcc.isEmpty()){
                       Database.update(UpdateAcc);
                   }
                   
                }
            }
        }
    }    
}