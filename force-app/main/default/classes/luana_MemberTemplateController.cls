/*
	Change History:
	LCA-553/LCA-890 10/08/2016 - WDCi Lean: update member type in profile section
	LCA-553/LCA-900 10/08/2016 - WDCi Lean: update sidebar option
*/

public class luana_MemberTemplateController {
    
    public String userPhotoUrl {get; set;}
    private Id communityId;
    
    public String memberType {get; private set;}
    public Boolean isMember {get; private set;}
        
    public luana_MemberTemplateController(){
        
        luana_CommUserUtil conUser = new luana_CommUserUtil(UserInfo.getUserId());
        
        //LCA-890 - start combine the logic
        if(conUser.hasAccessToMember){ 
        	isMember = true;
        	
        	if(conUser.user.Contact.Account.Membership_Class__c != null){
	            memberType = conUser.user.Contact.Account.Membership_Class__c + ' Member';
	        }
        } else if(conUser.hasAccessToNonMember){
            isMember = false;
            memberType = 'Non Member';
            
        }
        //LCA-890 - end
        
        //LCA-346
        userPhotoUrl = conUser.getUserPhoto(Network.getNetworkId(), UserInfo.getUserId());
        
    }
    
}