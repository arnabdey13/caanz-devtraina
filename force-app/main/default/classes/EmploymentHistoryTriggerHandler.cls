/*
    Handler class for Employment History Trigger
    ======================================================
    Changes:
    	Jun 2016 	Davanti - RN	EHCH - Created to populate Member record with a count of EH records a member provides CPP services at
    	Jul 2016	Davanti - RN	EHCH - Update to support Primary Employer validation logic.  Member can only have one Primary employer at a time
    	Oct 2016	Davanti - RN	EHCH - Validation rule for 'Provides CPP services'.  Will only fire if update is made directly on the EH record, 
											i.e. Member updates should not trigger the validation rule
		Jul 2017	Davanti - RN	EHCH QPRR1-1038 - updated updateMemberAccountRecords() logic when firing from Form Assembly Practical Experience form.
*/
public without sharing class EmploymentHistoryTriggerHandler {

	private static set<string> setCustomFieldsToExclude = new set<String>{'Rollups_Recalculation__c'};

	public static void onBeforeInsert(List<Employment_History__c> listNewEH){
		checkEHProvidesCPPServices(listNewEH);
        
        // QPRR1-1038 if the EH record is from Form Assembly, trigger EH PE count if its the last one in the list
        list<Employment_History__c> listEHToConsiderPriEmp = new List<Employment_History__c>();
        for(Employment_History__c eh : listNewEH){
        	if(eh.Updated_from_FA__c){ 
                if(eh.Trigger_Primary_Employer_Count__c){
                    listEHToConsiderPriEmp.add(eh);
                    
                }
            }
            else{
                listEHToConsiderPriEmp.add(eh);
			}
            eh.Updated_from_FA__c = false;
            eh.Trigger_Primary_Employer_Count__c = false;
        }// end QPRR1-1038
        
		updateMemberAccountRecords(listNewEH, listEHToConsiderPriEmp); // QPRR1-1038 replaced listNewEH with listEHToConsiderPriEmp
	}
	public static void onBeforeUpdate(List<Employment_History__c> listNewEH, List<Employment_History__c> listEHOld, map<Id, Employment_History__c> mapEHOld){
		
		list<Employment_History__c> listEHToConsiderCPP = new List<Employment_History__c>();
		list<Employment_History__c> listEHToConsiderPriEmp = new List<Employment_History__c>();
		list<Employment_History__c> listEHToConsiderProvidesCPP = new List<Employment_History__c>();
		
		Set<String> setFieldsForUpdate = getFieldsForUpdate();
		system.debug('### setFieldsForUpdate: ' + setFieldsForUpdate);
		
		for(Employment_History__c eh : listNewEH){
			Employment_History__c oldEH = mapEHOld.get(eh.Id);
			
			// deterimine if a validation rule on 'Provides CPP services' should fire for this record
			SObject objEH = (SObject)eh;
			SObject objOldEH = (SObject)oldEH;
			
			integer intCounter = 0;
			for(String strFieldName : setFieldsForUpdate){
				if(objEH.get(strFieldName) != objOldEH.get(strFieldName)){
					intCounter ++;
				}
			}
			if(intCounter > 0 ){
				listEHToConsiderProvidesCPP.add(eh);
			}
			
			// only need to consider those EH records that have had their status changed to Closed, or CPP has been unticked
			if((oldEH.Status__c != eh.Status__c) || (oldEH.Is_CPP_Provided__c != eh.Is_CPP_Provided__c)){
				listEHToConsiderCPP.add(eh);
			}
			if(eh.Updated_from_FA__c){ // QPRR1-1038 if the EH record is from Form Assembly, trigger EH PE count if its the last one in the list
                if(eh.Trigger_Primary_Employer_Count__c){
                    listEHToConsiderPriEmp.add(eh);
                }
            }// end QPRR1-1038 
            else if ((oldEH.Status__c != eh.Status__c) || (oldEH.Primary_Employer__c != eh.Primary_Employer__c)){
                system.debug('### oldEH.Primary_Employer__c: ' + oldEH.Primary_Employer__c + ' __ eh.Primary_Employer__c: ' + eh.Primary_Employer__c);
                listEHToConsiderPriEmp.add(eh);
			}
            eh.Trigger_Primary_Employer_Count__c = false;  // QPRR1-1038
        	eh.Updated_from_FA__c = false;  // QPRR1-1038
		}
        
		if(listEHToConsiderProvidesCPP.size() > 0 ){
			checkEHProvidesCPPServices(listEHToConsiderProvidesCPP);
		}
		updateMemberAccountRecords(listEHToConsiderCPP, listEHToConsiderPriEmp);
	}
	
	public static void onAfterDelete(List<Employment_History__c> listOldEH, map<Id, Employment_History__c> mapEHOld){
		updateBusinessAccountRecords(listOldEH, mapEHOld);
	}
	
	// Determines which fields to exclude from the 'Is_CPP_Provided__c' validation rule
	private static Set<String> getFieldsForUpdate(){
		Set<String> setFields = new set<String>();
		
		Schema.DescribeSObjectResult objectDescribe  = Employment_History__c.SObjectType.getDescribe();
		Map<String, Schema.SObjectField> mapField = objectDescribe.fields.getMap();
		for( String strFieldName : mapField.keyset())  {
		    Schema.SObjectField field = mapField.get( strFieldName );
		    Schema.DescribeFieldResult fieldDescribe = field.getDescribe();
		    
		    // exclude all calculated type fields
		    if(!setCustomFieldsToExclude.contains(fieldDescribe.getName()) && !fieldDescribe.isCalculated() && !fieldDescribe.isAutoNumber() && !fieldDescribe.isNameField() && fieldDescribe.isUpdateable()){
		    	setFields.add(fieldDescribe.getName());
		    }
		}
		return setFields;
	}
	
	// Determines if the EH record should fail 'Provides CPP services' validation error.
	// Member requires CPP AU, CPP NZ or Designation = 'Affiliate CAANZ' before 'Provides CPP Services' can be true
	private static void checkEHProvidesCPPServices(list<Employment_History__c> listNewEH){
		system.debug('### listNewEH: ' + listNewEH);
		for(Employment_History__c eh : listNewEH){
			if(eh.Is_CPP_Provided__c && eh.CPP_AU__c == false && eh.CPP_NZ__c == false && eh.Designation__c != 'Affiliate CAANZ'){
				eh.Is_CPP_Provided__c.addError('This member is not CPP approved or Affiliate, or their CPP approval has been removed please review their membership details.');
			}
		}
	}
	
	
	// Updates the member record with a count of the number of EH records they provide CPP services at and the number of Primary Employer records
	private static void updateMemberAccountRecords(list<Employment_History__c> listNewEHCPP, list<Employment_History__c> listNewEHPriEmp){
		
		Set<Id> setEHToNotHaveInQueryCPP = new set<Id>(); // map<EH Id, Employment_History__c>
		Set<Id> setEHToNotHaveInQueryPriEmp = new set<Id>(); // map<EH Id, Employment_History__c>
		map<Id, set<Id>> mapAccountsAndEHCPPCount = new map<Id, set<Id>>(); // map<AccountId, set<EH Ids>>
		map<Id, set<Id>> mapAccountsAndEHPriEmpCount = new map<Id, set<Id>>(); // map<AccountId, set<EH Ids>>
		map<Id,Account> mapAccountsToUpdate = new map<Id,Account>(); //map<accountid, account> the Account records to update
		
		for(Employment_History__c eh : listNewEHCPP){
			set<Id> setEHIdsForMember = new set<Id>();
			setEHToNotHaveInQueryCPP.add(eh.Id);
			
			if(eh.Status__c == 'Current' && eh.Is_CPP_Provided__c && eh.Member__c != null){
				
				// create a map of EH records per member
				if(mapAccountsAndEHCPPCount.containsKey(eh.Member__c)){
					setEHIdsForMember = mapAccountsAndEHCPPCount.get(eh.Member__c);
				}
				setEHIdsForMember.add(eh.Id);
			}
			mapAccountsAndEHCPPCount.put(eh.Member__c, setEHIdsForMember);
		}
		
		for(Employment_History__c eh : listNewEHPriEmp){
			set<Id> setEHIdsForMember = new set<Id>();
			setEHToNotHaveInQueryPriEmp.add(eh.Id);
			
			if(eh.Status__c == 'Current' && eh.Primary_Employer__c && eh.Member__c != null){
				
				// create a map of EH records per member
				if(mapAccountsAndEHPriEmpCount.containsKey(eh.Member__c)){
					setEHIdsForMember = mapAccountsAndEHPriEmpCount.get(eh.Member__c);
				}
				setEHIdsForMember.add(eh.Id);
			}
			mapAccountsAndEHPriEmpCount.put(eh.Member__c, setEHIdsForMember);
		}
		set<Id> setCombinedMemberIds = new Set<Id>();
		setCombinedMemberIds.addAll(mapAccountsAndEHCPPCount.keyset());
		setCombinedMemberIds.addAll(mapAccountsAndEHPriEmpCount.keyset());
		
		
		// Find other EH records belonging to the member, but that aren't in the records this trigger
		for(Employment_History__c eh: [SELECT Id, Member__c, Status__c, Primary_Employer__c, Is_CPP_Provided__c
														FROM Employment_History__c
														WHERE Member__c IN :setCombinedMemberIds
														AND (Primary_Employer__c = TRUE OR Is_CPP_Provided__c = TRUE) ]){
														//AND Id NOT IN :setEHToNotHaveInQueryPriEmp]){
			
			if(eh.Is_CPP_Provided__c && setEHToNotHaveInQueryCPP.contains(eh.Id) == false){
				set<Id> setEHIdsForMember = mapAccountsAndEHCPPCount.containsKey(eh.Member__c)? mapAccountsAndEHCPPCount.get(eh.Member__c): new set<Id>();
				setEHIdsForMember.add(eh.Id);
				mapAccountsAndEHCPPCount.put(eh.Member__c, setEHIdsForMember);
			}
			system.debug('### mapAccountsAndEHCPPCount: ' + mapAccountsAndEHCPPCount);
			if(eh.Primary_Employer__c && setEHToNotHaveInQueryPriEmp.contains(eh.Id) == false){
				set<Id> setEHIdsForMember = mapAccountsAndEHPriEmpCount.containsKey(eh.Member__c)? mapAccountsAndEHPriEmpCount.get(eh.Member__c): new set<Id>();
				setEHIdsForMember.add(eh.Id);
				mapAccountsAndEHPriEmpCount.put(eh.Member__c, setEHIdsForMember);
			}																
		}
		
		// Now create an account update map
		for(Id acctId : mapAccountsAndEHCPPCount.keyset()){
			
			set<Id> setEHIdsForMember = mapAccountsAndEHCPPCount.get(acctId);
			Account acct = mapAccountsToUpdate.containsKey(acctId) ? mapAccountsToUpdate.get(acctId) : new Account(Id = acctId);
			acct.CPP_Employment_History_Count_Member__c = setEHIdsForMember.size();
			mapAccountsToUpdate.put(acctId, acct); 
		}
		system.debug('### mapAccountsToUpdate: ' + mapAccountsToUpdate);
		for(Id acctId : mapAccountsAndEHPriEmpCount.keyset()){
			set<Id> setEHIdsForMember = mapAccountsAndEHPriEmpCount.get(acctId);
			Account acct = mapAccountsToUpdate.containsKey(acctId) ? mapAccountsToUpdate.get(acctId) : new Account(Id = acctId);
			acct.Primary_Employer_EH_Count__c = setEHIdsForMember.size();
			mapAccountsToUpdate.put(acctId, acct); 
		}
		system.debug('### mapAccountsToUpdate: ' + mapAccountsToUpdate);
		if(mapAccountsToUpdate.size() > 0){
			try{
				update mapAccountsToUpdate.values();
			}catch(DMLException dmlEx){
				//DN20160812
				//employment history DML failed due to account update (probably validation rules)
				//the salesforce default error messages are difficult for end user to read
				//requirement is to prettify but must also ensure rollback by using adderror

				//NOTE this is a one-off fix for just one method
				//Implementing app wide prettified messages would require an exception message framework
				//and overhaul of all apex DML
				
				//build list of all potentially affected employment history records
				List<Employment_History__c>lstAllEH=new List<Employment_History__c>();
				lstAllEH.addAll(listNewEHCPP);				
				lstAllEH.addAll(listNewEHPriEmp);				
				system.debug('###EmploymentHistoryTriggerHandler:updateMemberAccountRecords:lstAllEH='+lstAllEH);

				Map<Id,String>mapExceptionAccounts=new Map<Id,String>();
				
				for(integer i=0;i<dmlEx.getNumDml();i++){
					String errorMessage=ExceptionHandler.prettifyDMLExceptionMessage(dmlEx.getDmlMessage(i));
					//populate list of affected accounts
					mapExceptionAccounts.put(dmlEx.getDmlId(i),errorMessage);
					system.debug('###EmploymentHistoryTriggerHandler:updateMemberAccountRecords:mapExceptionAccounts='+mapExceptionAccounts);
				}
				//cycle through all employment history and add errors to records affected by account exceptions
				for(Employment_History__c recEH:lstAllEH){
					system.debug('###EmploymentHistoryTriggerHandler:onBeforeUpdate:recEH='+recEH);
					if(mapExceptionAccounts.containsKey(recEH.Member__c)){
						String errorMessage=mapExceptionAccounts.get(recEH.Member__c);
						recEH.addError(errorMessage);
						system.debug('###EmploymentHistoryTriggerHandler:updateMemberAccountRecords:errorMessage='+errorMessage);
					}
				}
			}
		}
	}
	
	// EHCH - Updates business account record if the EH member was an employee coordinator or 
	private static void updateBusinessAccountRecords(List<Employment_History__c> listEH, map<Id, Employment_History__c> mapEH){
		//Update Primary Employer and Job Title
		//Update Employer Coordinator
		
		map<Id, set<Id>> mapMemberPrimaryEmps = new map<Id, set<Id>>(); // map<Member Id, set<Id>> Members that have an EH record with Primary Emp = true
		map<Id, set<Id>> mapAccountEmpCoord = new map<Id, set<Id>>(); // map<account Id, set<Member Id>> Accounts that have an EH record with Emp Coord = true
		
		List<Account> listAccountsToUpdate = new List<Account>();  
		
		set<Id> setAllAcctIds = new set<Id>();
		
		// Collate a list of Account records to update
		for(Employment_History__c eh : listEH){
			
			// If currently Primary employer, add to the set against the member
			if(eh.Primary_Employer__c && eh.Status__c == 'Current'){
				set<Id> setBusIds = mapMemberPrimaryEmps.containsKey(eh.Member__c) ? mapMemberPrimaryEmps.get(eh.Member__c) : new Set<Id>();
				setBusIds.add(eh.Employer__c);
				mapMemberPrimaryEmps.put(eh.Member__c, setBusIds);
			}
			
			// If currently a Employee Coordinator, add to the set against the Bus Acct. 
			if(eh.Is_Employee_Coordinator__c && eh.Status__c == 'Current'){
				set<Id> setMemberIds = mapAccountEmpCoord.containsKey(eh.Employer__c) ? mapAccountEmpCoord.get(eh.Employer__c) : new set<Id>();
				setMemberIds.add(eh.Member__c);
				mapAccountEmpCoord.put(eh.Employer__c, setMemberIds);
			}
		}
		
		if(mapMemberPrimaryEmps.size() > 0) { setAllAcctIds.addAll(mapMemberPrimaryEmps.keyset()); }
		if(mapAccountEmpCoord.size() > 0) { setAllAcctIds.addAll(mapAccountEmpCoord.keyset()); }
		
		
		// Find matching business account records
		for(Account acct : [SELECT Id, Employee_Coordinator_Lookup__c, Primary_Employer__c  FROM Account
									WHERE Id IN :setAllAcctIds]){
			
			
			integer intUpdatedCount = 0;
			
			// if the primary employer matches, set this to null on the member account since the EH record has been deleted
			if(mapMemberPrimaryEmps.containsKey(acct.id)){
				set<Id> setBusIds =  mapMemberPrimaryEmps.get(acct.Id);
				if(setBusIds.contains(acct.Primary_Employer__c)){
					intUpdatedCount++;
					acct.Primary_Employer__c = null;
				}				
			}

			// If the Employee coordinator matches, set this to null on the business account since the EH record has been deleted
			if(mapAccountEmpCoord.containsKey(acct.Id)){
				set<Id> setMemberIds = mapAccountEmpCoord.get(acct.Id);
				if(setMemberIds.contains(acct.Employee_Coordinator_Lookup__c)){
					intUpdatedCount++;
					acct.Employee_Coordinator_Lookup__c = null;
				}
			}
			
			// If the record was updated, add to the list
			if(intUpdatedCount > 0){
				listAccountsToUpdate.add(acct);
			}					
		}
		// Update.
		if(listAccountsToUpdate.size() > 0){
			update listAccountsToUpdate;
		}
		
	}
}