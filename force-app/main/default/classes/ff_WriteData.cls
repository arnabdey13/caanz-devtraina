/**
* @author Steve Buikhuizen, Jannis Bott
* @date 14/11/2016
*
* @group FormsFramework
*
* @description  The wrapper that is send data to apex on
 *              when the Lightning Driver writes data
*/
global class ff_WriteData {

    global ff_WriteData() {
        records = new Map<String, SObject>();
    }

    @AuraEnabled
    global Map<String, SObject> records {get;set;}
}