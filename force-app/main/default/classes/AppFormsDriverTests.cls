/***********************************************
Author:        RXP 
Description:   Test Class for AppFormsDriverController
History
Date            Author             Comments
***********************************************
13-08-2019     Prasanthi        Initial Release
************************************************/
@isTest
public class AppFormsDriverTests {

  @testSetup static void createTestData() {
        TestSubscriptionUtils.createTestData();
    }

    /*private static User getMemberUser() {
       Id contactId = [SELECT Id FROM Contact WHERE AccountId = :TestSubscriptionUtils.getTestAccountId() LIMIT 1].Id;
       return [select Id from User where ContactId = :contactId];
    }*/
    
    private static User currentUser;
    private static Account currentAccount;
    private static Contact currentContact;
    
    static {
        currentAccount = TestObjectCreator.createFullMemberAccount();
        currentAccount.Member_Of__c='NZICA';
        
        insert currentAccount;
        currentContact = [SELECT Id FROM Contact WHERE AccountId =: currentAccount.Id];
    }

    private static User getCurrentUser(){
        Test.startTest(); Test.stopTest(); // Future method needs to run to create the user.
        return currentUser = [SELECT Name,Id,AccountId FROM User WHERE AccountId =: currentAccount.Id];
    }
    
     private static Account getCurrentAccount(){
         return [SELECT Name,Id FROM Account WHERE Id =: currentAccount.Id];
    }
    
  
    
    
     @IsTest
    private static void testEmploymentHistory() {
    
        //TestSubscriptionUtils.createTestData();
    
        Account employer = AppFormsTestUtil.createBusinessAccount();
        insert employer; 
         
        Id useraccountId = getCurrentUser().Id;
        
        Map<String,Schema.RecordTypeInfo> appTypes = Schema.SObjectType.Application__c.getRecordTypeInfosByDeveloperName();
        Id specialsRectype = appTypes.get('Special_Admissions').getRecordTypeId();
         
        Application__c testApplication = AppFormsTestUtil.createApplication(useraccountId, 'Draft',specialsRectype);
        insert testApplication;
        
        TestDataFFRichTextBuilder.multiple(RICH_TEXT_NAMES, 'fake content', true);
            
        
        //System.runAs(getMemberUser()) {        
          
            ff_WriteData wd = new ff_WriteData();
            wd.records.put('INSERT-Employment_History__c1',
                    new Employment_History__c(
                            // member id not passed by client, populated by saver impl
                            Employer__c = employer.Id,
                            Job_Title__c = 'Executive',
                            Employee_Start_Date__c = System.today(),
                            Employee_End_Date__c = System.today(),
                            Status__c = 'Closed',
                            Work_Hours__c = 'Full Time'
                            
                    ));
        
        AppFormsDriverController service = new AppFormsDriverController(useraccountId,'Special_Admissions');
        ff_WriteResult result = service.handler.save(wd);
            
            System.debug('opt-in insert result: ' + result);
            System.assertEquals(0, result.fieldErrors.size(), 'no errors during create');
            System.assertEquals(0, result.recordErrors.size(), 'no errors during create');
            
            Test.startTest();
            service = new AppFormsDriverController(useraccountId,'Special_Admissions');
            ff_ServiceHandler handler;
       
            ff_ReadData withHistoryRecord = service.handler.load(null);
            System.debug(withHistoryRecord);
            List<ff_Service.SObjectWrapper> histories = withHistoryRecord.records.get('Employment_History__c');
            System.debug(histories);
            // checking security access to history records
            System.assertEquals(1, histories.get(0).sObjectList.size(),
                    'created record can be read after create');
                    
            Test.stopTest();
           
       //}
        
    }
    
     @IsTest
    private static void testReferences() {
    
        Id useraccountId = getCurrentUser().Id;
        
        Map<String,Schema.RecordTypeInfo> appTypes = Schema.SObjectType.Application__c.getRecordTypeInfosByDeveloperName();
        Id specialsRectype = appTypes.get('Special_Admissions').getRecordTypeId();
        
        //Account fullMember = TestObjectCreator.createFullMemberAccount();
        //insert fullMember;
         
        Application__c testApplication = AppFormsTestUtil.createApplication(useraccountId, 'Draft',specialsRectype);
        insert testApplication;
        
        TestDataFFRichTextBuilder.multiple(RICH_TEXT_NAMES, 'fake content', true);
        
        AppFormsDriverController service = new AppFormsDriverController(useraccountId,'Special_Admissions');
        ff_WriteData wd = new ff_WriteData();
        wd.records.put('INSERT-Application_References__c1', new Application_References__c(        
                        External_Reference_Email__c= 'abc@123.com'));
        wd.records.put('INSERT-Application_References__c2', new Application_References__c(        
                        Internal_Member_Id__c= '1234'));  
                        
        //System.runAs(getMemberUser()) {
            
            ff_WriteResult result = service.handler.save(wd); 
        
        //}
    
    }
    

    

     private static final List<String> RICH_TEXT_NAMES = new List<String>{
            'AppForms.Stage0.GoodStanding',
            'AppForms.Stage1.ChecklistEmpty',
            'AppForms.Stage1.ACCAChecklist',
            'AppForms.Stage1.ATChecklist',
            'AppForms.Stage1.ATChecklistAATUK',
            'AppForms.Stage1.CPAAustraliaChecklist',
            'AppForms.Stage1.CPACanadaChecklist',
            'AppForms.Stage1.GAAChecklist',
            'AppForms.Stage1.GAAChecklistAICPA',
            'AppForms.Stage1.GAAChecklistSAICA',
            'AppForms.Stage1.GAAChecklistICAS',
            'AppForms.Stage1.GAAChecklistICAI',
            'AppForms.Stage1.GAAChecklistICAEW',
            'AppForms.Stage1.ICAZChecklist',
            'AppForms.Stage1.MICPAChecklist',
            'AppFroms.Stage6.CVandEmploymentText',
            'AppFroms.Stage6.ReferenceText',
            'Registrations.AusCreditLicence',
            'Registrations.AFSSituation',
            'Registrations.AFSMultiple',
            'CPP.RequiredMessage',
            'CPP.NotRequiredMessage',
            'CPP.CountryAus',
            'CPP.CountryAusFurtherInfo',
            'CPP.CountryNZ',
            'CPP.CountryNZDescription',
            'CPP.CountryOther',
            'CPP.Questionnaire',
            'ProvApp.Stage5.LegalDisciplinaryByStatutory',
            'ProvApp.Stage5.LegalDisciplinaryByTertiary',
            'ProvApp.Stage5.Legal',
            'ProvApp.Stage5.LegalBankruptcy',
            'ProvApp.Stage5.LegalConviction', 
            'ProvApp.Stage5.NotToManageCorporation', 
            'ProvApp.Stage6.StatementPrivacyLink',
            'ProvApp.Stage6.StatementPrivacyStatement',
            'ProvApp.Stage6.StatementDeclaration', 
            'ProvApp.Stage6.StatementEUGDPRStatement', 
            'ProvApp.Stage7.Submitted',     
            'Application.ProvisionalMembershipObligations',
            'EducationHistory.YearOfCommence',
            'EducationHistory.YearOfFinish',
            'Application.SubjectToDisciplinaryByCompany',
            'Status.opts',
            'Type.opts'
          
    };
     
    
}