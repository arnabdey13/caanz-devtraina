/* 
    Developer: WDCi
    Development Date: 17/06/216
    
    Change History
    LCA-658: 17/06/2016 - include foundation enrolment checking
    LCA-701: 28/06/2016 - include Contributor permissionSet
    LCA-1169 06/02/2018 - WDCi Lean: add Nepal and India program enrolment logic
*/
public class luana_CommUserUtil {
    public Id custCommConId {set; get;}
    public Id custCommAccId {set; get;}
    public String custCommCountry {set; get;}
    public Boolean hasAccessToMember {set; get;}
    public Boolean hasAccessToEmployer {set; get;}
    public Boolean hasAccessToNonMember {set; get;}
    
    //LCA-701: create contributor access check
    public Boolean hasAccessToContributor {set; get;}
    
    public User user {set; get;}
    
    //LCA-447
    public Boolean validCAProgramToEnrol {get; private set;}
    
    public luana_CommUserUtil(Id userId) {
    
        validCAProgramToEnrol = false;
        
        user = getLoggedInUser(userId);
        //If this user is a Customer Community User, retrieve necessary information
        if(user != null && (user.UserType == 'CSPLitePortal' || user.UserType == 'PowerCustomerSuccess' || user.UserType == 'CustomerSuccess')) {
            // store Member Contact Id
            custCommConId = user.Contact.Id;
            
            // store Member Account Id
            if(user.Contact.IsPersonAccount){
                custCommAccId = user.Contact.AccountId;
                custCommCountry = user.Contact.Account.Affiliated_Branch_Country__c;
            }
        } 
        
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
        Luana_Extension_Settings__c defNonMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_NonMember_PermSet_Id');
        
        //LCA-701: Get custom setting
        Luana_Extension_Settings__c defContributorPermSetId = Luana_Extension_Settings__c.getValues('Default_ContCom_PermSet_Id');
        
        hasAccessToMember = false;
        hasAccessToEmployer = false;
        hasAccessToNonMember = false;
        
        for(PermissionSetAssignment psa : [SELECT AssigneeId, PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId =: userId]) {
            if(psa.PermissionSetId == defMemberPermSetId.Value__c)
                hasAccessToMember = true;
            if(psa.PermissionSetId == defEmployerPermSetId.Value__c)
                hasAccessToEmployer = true;
            if(psa.PermissionSetId == defNonMemberPermSetId.Value__c)
                hasAccessToNonMember = true;
                
            //LCA-701
            if(psa.PermissionSetId == defContributorPermSetId.Value__c){
                hasAccessToContributor = true;
            }
        }
        
      
    }
    
    public User getLoggedInUser(Id userId) {
        if(UserInfo.getUserId() != null) {
            User user = [SELECT Id, Contact.Id, Contact.IsPersonAccount, Contact.AccountId, Contact.Account.Affiliated_Branch_Country__c, Contact.Account.Membership_Class__c, UserType, Contact.Account.FirstName, Contact.Account.Preferred_Name__c, Contact.Account.LastName, Contact.Account.Assessible_for_CA_Program__c, Contact.Account.Assessible_for_CAF_Program__c, 
            Contact.Account.Accessible_for_ICAI__c, Contact.Account.Accessible_for_ICAN__c, Contact.Account.Accessible_for_ICAP__c, Contact.Account.Accessible_for_ICASL__c //LCA-1169
            FROM User WHERE Id=: userId];    
            return user;
        } else
            return null;
     
    }
    
    public Map<Id, boolean> getProgramOfferingPermission(Set<Id> poIds) {
        Map<Id, boolean> posPermission = new Map<Id, boolean>();
        
        //LCA-447, include Member_Criteria__c to query
        for(Product_Permission__c proPerm : [SELECT Id, Grant_Permission__c, Program_Offering__c, Program_Offering__r.RecordType.developerName, Member_Criteria__c FROM Product_Permission__c WHERE Program_Offering__c in: poIds]) {
            
            //LCA-447
            if(proPerm.Grant_Permission__c == 'Member Community' && hasAccessToMember){
                //Will not check for accredited Module & foundation
                if(proPerm.Program_Offering__r.RecordType.developerName == luana_EnrolmentConstants.RECORDTYPE_STUDENTPROGRAM_ACCREDITEDPROGRAM){
                    if((User.Contact.Account.Assessible_for_CA_Program__c 
                    	|| User.Contact.Account.Accessible_for_ICAI__c || User.Contact.Account.Accessible_for_ICAN__c || User.Contact.Account.Accessible_for_ICAP__c || User.Contact.Account.Accessible_for_ICASL__c) //LCA-1169
                    	&& proPerm.Member_Criteria__c == 'Assessible for CA Program'){
                    		
                        posPermission.put(proPerm.Program_Offering__c, true);
                        validCAProgramToEnrol = true;
                    }
                }else {
                    posPermission.put(proPerm.Program_Offering__c, true);
                }
            }
            
            if(proPerm.Grant_Permission__c == 'Non-Member Community' && hasAccessToNonMember)
                //posPermission.put(proPerm.Program_Offering__c, true);
                
                //LCA-658
                if(proPerm.Program_Offering__r.RecordType.developerName == luana_EnrolmentConstants.RECORDTYPE_STUDENTPROGRAM_FOUNDATION){
                    if(User.Contact.Account.Assessible_for_CAF_Program__c && proPerm.Member_Criteria__c == 'Assessible for CAF Program'){
                        posPermission.put(proPerm.Program_Offering__c, true);
                    }
                }else {
                    posPermission.put(proPerm.Program_Offering__c, true);
                }
                
            if(proPerm.Grant_Permission__c == 'Employer Community' && hasAccessToEmployer)
                posPermission.put(proPerm.Program_Offering__c, true);
        
        }
        return posPermission;
    }
    
    //LCA-346
    public String getUserPhoto(Id networkId, Id userId){
    
        if(Test.isRunningTest()){
            return '/test/imageurl';
        } else {
            ConnectApi.Photo userProfPhoto = ConnectApi.UserProfiles.getPhoto(networkId, userId);
            return userProfPhoto.largePhotoUrl;
        }
    }
   
}