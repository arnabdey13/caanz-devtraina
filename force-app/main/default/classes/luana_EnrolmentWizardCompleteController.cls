/**
    Developer: WDCi (Lean)
    Development Date: 19/04/2016
    Task #: Enrollment wizard - Controller for Complete page
**/

public with sharing class luana_EnrolmentWizardCompleteController {
    
    luana_EnrolmentWizardController stdController;
    
    public luana_EnrolmentWizardCompleteController(luana_EnrolmentWizardController stdController){
        this.stdController = stdController;
        
    }
    
    public luana_EnrolmentWizardCompleteController(){
    	
    }
    
    public PageReference completeNext(){
        return null;
    }
    
    public PageReference completeBack(){
        return null;
    }
    
}