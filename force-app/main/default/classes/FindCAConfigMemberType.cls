/*
    API for FindCA - Members Type Search Options Configuration per Country -> Branches
    October 2017   CAANZ - akopec  

    Example: /services/apexrest/FindCA/v1.0/Config/MemberType?BranchCountry=
*/
@RestResource(urlMapping='/FindCA/v1.0/Config/MemberType/*')
global with sharing class FindCAConfigMemberType {

@HttpGet
  global static List<FindCA_API_Member_Type__mdt> doGet() {
      List<FindCA_API_Member_Type__mdt> members;
      if (!String.isBlank(RestContext.request.params.get('BranchCountry'))){
      	String vBranchCountry = RestContext.request.params.get('BranchCountry');
        members = [Select Label, Country__c, Member_Type__c, Display_Name__c, Display_Order__c From FindCA_API_Member_Type__mdt where Country__c = :vBranchCountry];
      }
      else
		members = [Select Label, Country__c, Member_Type__c, Display_Name__c, Display_Order__c From FindCA_API_Member_Type__mdt];
    return members;
  }
}