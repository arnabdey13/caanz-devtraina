/*------------------------------------------------------------------------------------
Author:        Sumit Gupta
Company:       Salesforce
Description:   schedulable class to create cpd summaries for member accounts

History
Date            Author             Comments
--------------------------------------------------------------------------------------
02-05-2018     Sumit Gupta       	Initial Release
------------------------------------------------------------------------------------*/
global class CPDSummaryScheduler implements Schedulable {

	global void execute(SchedulableContext sc) {
		createAccountSummaries();
	}

	//Create missing summary records
    private void createAccountSummaries(){
    	List<Account> accounts = CPDSummaryHandler.getMemberAccounts(); 
    	if(!accounts.isEmpty()){
    		CPDSummaryHandler.createMissingCPDSummaryRecords(accounts);
    	}
    }
}