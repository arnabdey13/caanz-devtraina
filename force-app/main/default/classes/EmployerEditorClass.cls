public without sharing class EmployerEditorClass {

    public static Id recordTypeBusiness = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();


    @AuraEnabled
    public static List<Employment_History__c> EmpList() {
        User userRec = [SELECT AccountId FROM User WHERE ID =: UserInfo.getUserId()] ;
        String accIdVal = userRec.AccountId ;
        system.debug('***accIdVal***--'+ accIdVal);
        List<Employment_History__c> a = [SELECT Id, Name, CreatedDate, Employer__c,Employer__r.Name, Member__c, Job_Title__c, Employer_Name__c, Primary_Employer__c, Employee_Start_Date__c,
                                         Employee_End_Date__c, Status__c, Is_CPP_Provided__c, Is_Closed__c 
                                         FROM Employment_History__c 
                                         WHERE Member__c =: accIdVal 
                                         ORDER BY Status__c ASC, Employee_Start_Date__c DESC];
        return a;
    }

    @AuraEnabled
    public static Account returnCurrntUserMember() {
        User userRec = [SELECT AccountId FROM User WHERE ID =: UserInfo.getUserId()] ;
        String accIdVal = userRec.AccountId ;
        Account memberOfValue = new Account() ;
        if(String.isNotEmpty(accIdVal)){
            Account accRec = [SELECT Id, Member_Of__c, Has_CPP_in_AU_and_or_NZ__c,CPP_Employment_History_Count_Member__c, Primary_Employer_EH_Count__c
            							FROM Account WHERE Id =: accIdVal];
            if(String.isNotEmpty(accRec.Id)){
                memberOfValue = accRec ;
            }
        }
        return memberOfValue ;
    }

    @AuraEnabled
    public static void getAccountupdatedlist(Employment_History__c newAcc, String accId) {
        system.debug('*****in getAccountupdatedlist**'+ accId + '---'+ newAcc);
        User userRec = [SELECT AccountId FROM User WHERE ID =: UserInfo.getUserId()] ;
        String userId = userRec.AccountId ;
        newAcc.Member__c = userId ;
        newAcc.Employer__c = accId ;
        if(newAcc.Primary_Employer__c){
            newAcc.Primary_Employer__c = true ;
        }
        if(String.isEmpty(newAcc.Status__c)){
            newAcc.Status__c = 'Current' ;
        }
        insert newAcc;
    }

    @AuraEnabled
    public static Employment_History__c getEmpRecordDetail(String empId) {
        Employment_History__c empTemp = [SELECT Id, Name, Employer__c,Employer__r.Name, Member__c, Job_Title__c, Primary_Employer__c,Employee_Start_Date__c,
                                         Employee_End_Date__c, Status__c, Employer_Name__c, Is_CPP_Provided__c, Is_Closed__c FROM Employment_History__c WHERE Id =: empId];
        return empTemp;
    }

    @AuraEnabled
    public static void updateEmpRecord(Employment_History__c updateAcc) {
        update updateAcc;
    }

    @AuraEnabled
    public static List<Account> findByName(String searchKey, String memberOfValue) {
        system.debug('*****in memberOfValue**'+ memberOfValue );
        String name = '%' + searchKey + '%';
        return [SELECT Id, Name, BillingCity, BillingCountry, RecordTypeId, Member_Of__c, Status__c, Cannot_Have_Employment_History__c FROM Account WHERE Name LIKE :name AND 
        		RecordTypeId =: recordTypeBusiness AND Member_Of__c =: memberOfValue AND Status__c = 'Active' AND Cannot_Have_Employment_History__c = false LIMIT 50];
    }

    @AuraEnabled
    public static List<String> getStatusPickListClass() {
        List<String> pickValues = getPicklistValues('Employment_History__c', 'Status__c');
        return pickValues;
    }
    
    @AuraEnabled
    public static List<String> getCPPPickListClass() {
    	List<String> pickValues = new List<String>();
    	pickValues.add('Yes');
    	pickValues.add('No');
        return pickValues;
    }

    @AuraEnabled
    public static Boolean checkPrimaryEmpDuplicate(String personAccountId){
        if([SELECT count() FROM Employment_History__c WHERE Primary_Employer__c = true AND Member__c =: personAccountId AND Status__c = 'Current'] > 0){
            return true ;
        }else{
            return false ;
        }
    }

    public static List<String> getPicklistValues(String ObjectApi_name,String Field_name){
        List<String> lstPickvals=new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
            lstPickvals.add(a.getValue());//add the value  to our final list
        }
        return lstPickvals;
    }

}