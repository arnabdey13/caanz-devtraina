/*
    Developer: WDCi (kh)
    Development Date:6/09/2016
    Task: PPP Regional Landing page controller
    
    Change History:
     LCA-998 27/09/2016 WDCI - KH remove all Regional Discount references and the Landing page
*/
public class luana_MemberPPPLandingController {
    
    
    
    public luana_MemberPPPLandingController(){
        
    }
    
/*  
    public String isRegional {get; set;}  
    public PageReference enrolNewPPPMember(){
        
        if(isRegional == 'Yes'){
            Luana_Extension_Settings__c pppRegionalMember = Luana_Extension_Settings__c.getValues('PPP_Member_Regional');
            LuanaSMS__Course__c runningCourse = [select Id from LuanaSMS__Course__c where LuanaSMS__Status__c = 'Running' and LuanaSMS__Program_Offering__c =: pppRegionalMember.value__c limit 1];
            
            PageReference pageRef = Page.luana_enrolmentwizardcourse;
            pageRef.getParameters().put('producttype', 'ppp');
            pageRef.getParameters().put('poid', pppRegionalMember.value__c);
            pageRef.getParameters().put('courseid', runningCourse.Id);
            
            return pageRef;
        }
        if(isRegional == 'No'){
            Luana_Extension_Settings__c pppStandardMember = Luana_Extension_Settings__c.getValues('PPP_Member_Standard');
            LuanaSMS__Course__c runningCourse = [select Id from LuanaSMS__Course__c where LuanaSMS__Status__c = 'Running' and LuanaSMS__Program_Offering__c =: pppStandardMember.value__c limit 1];
            
            PageReference pageRef = Page.luana_enrolmentwizardcourse;
            pageRef.getParameters().put('producttype', 'ppp');
            pageRef.getParameters().put('poid', pppStandardMember.value__c);
            pageRef.getParameters().put('courseid', runningCourse.Id);
            
            return pageRef;
        }
        
        if(isRegional == 'none'){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'Please select one of your answer from the picklist'));
            return null;
        }
        
        return null;
    }
    
    public List<SelectOption> getTypes() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new selectOption('none', '--None--'));
        options.add(new SelectOption('Yes','Yes')); 
        options.add(new SelectOption('No','No')); 
        return options; 
    }
    
    
    public pageReference doBack(){
    
        PageReference pageRef = Page.luana_MyPPPEnrolment;
        return pageRef;
    }
    
    public Attachment getTermsAndCondition(){
      for(Terms_and_Conditions__c termAndCon : [Select Id, Name from Terms_and_Conditions__c Where Name = 'PPP' order by createddate desc limit 1]){
        for(Attachment att : [Select Id, Name from Attachment Where ParentId =: termAndCon.Id order by createddate desc limit 1]){
                return att;
            }
      }
      
      return null;
    }
    
    public String getDomainUrl(){
        return luana_NetworkUtil.getCommunityPath();
    }
    */
}