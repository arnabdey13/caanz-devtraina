@isTest
private class GetEmploymentDetailstest
{
    testMethod static void testgetDetails()
    {
        GetEmploymentDetails.EmploymentID IDs = new GetEmploymentDetails.EmploymentID();
        
        Account memberAccount = new Account(FirstName = 'Test', LastName = 'Test', PersonEmail = 'get@employment.com'
                                           ,Salutation='Mr',Communication_Preference__c= 'Home Phone'
            ,PersonHomePhone= '1234'
            ,PersonOtherStreet= '83 Saggers Road'
            ,PersonOtherCity='JITARNING'
            ,PersonOtherPostalCode='6365' ,PersonOtherCountry='Australia'   ); //DN20160415 comply with validation rule
        insert memberAccount;
        
        Account employerAccount = new Account(Name = 'Test Org'
                                                ,BillingStreet='1 Test Street'); //DN20160415 comply with validation rule
        insert employerAccount;
        //IDs.memberID = '001N000000DluTzIAJ';
        //IDs.EmployerID = '001N000000Dluc8IAB';
        IDs.memberId = memberAccount.Id;
        IDs.EmployerID = memberAccount.Id;
        
              
        GetEmploymentDetails.EmploymentDetails details = new GetEmploymentDetails.EmploymentDetails();
        
        details = GetEmploymentDetails.getDetails(IDs);
        
        System.assert(details != null);
        
    }
}