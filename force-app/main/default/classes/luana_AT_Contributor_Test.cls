/**
    Developer: WDCi (kh)
    Development Date:11/07/2016
    Task: Luana Test class for luana_ContributorAttDownloadCtl class
    
    Change History:
    LCA-614             08/06/2016      WDCi Lean:  part of this is obsolete
    LCA-921             23/08/2016      WDCi KH:    add person account email field
    PAN:6222            21/05/2019      WDCi LKoh:  Fixed issue with validation on the Account + test class coverage increase
    PAN:6226            06/06/2019      WDCi Lean: enhanced code coverage
**/
@isTest(seeAllData=false)
public class luana_AT_Contributor_Test {
    
    final static String classNamePrefixShort = 'cadt';
    final static String classNamePrefixLong = 'luana_ContributorAttDownloadCtlTest';
    private static map<String, Id> commProfIdMap {get; set;}
    
    
    public static List<Product2> prodList {get; set;}
    public static Luana_DataPrep_Test testDataGenerator;
    
    public static User memberUser {get; set;}
    public static Account memberAccount {get; set;}
    public static Account businessAccount {get; set;}
    public static List<LuanaSMS__Subject__c> subjList {get; set;}
    public static LuanaSMS__Course__c courseAP {get; set;}
    public static List<LuanaSMS__Course__c> courseAMList {get; set;}
    
    public static LuanaSMS__Program_Offering__c po{get; set;}
    public static List<LuanaSMS__Delivery_Location__c> devLocations {get; set;}
    public static luana_CommUserUtil userUtil {get; set;} 
    
    public static List<Course_Delivery_Location__c> cdls {get; set;}
    public static LuanaSMS__Program__c progrm {get; set;}
    public static LuanaSMS__Training_Organisation__c trainingOrg {get; set;}
    
    public static Relationship__c relationship {get; set;}
    public static Employment_History__c ehObj {get; set;}
    
    public static List<PricebookEntry> pbes {get; set;}
    
    public static Map<String, LuanaSMS__Delivery_Location__c> devLocMap = new Map<String, LuanaSMS__Delivery_Location__c>();
    
    public static LuanaSMS__Program_Offering__c poF{get; set;}
    public static LuanaSMS__Course__c courseF {get; set;}
    
    public static List<LuanaSMS__Student_Program_Subject__c> newSPSs {get; set;}
    public static List<LuanaSMS__Program_Offering__c> amPOs {get; set;}
    
    public static LuanaSMS__Room__c room1;
    public static LuanaSMS__Session__c session1;
    public static LuanaSMS__Course_Session__c coursesession1;
    
    public static void initial(){
        System.debug('##luana_AT_Contributor_Test - Prepare Data##: '+ System.today());
        //initialize
        testDataGenerator = new Luana_DataPrep_Test();
        
        //Create all the custom setting
         
        insert testDataGenerator.createLuanaConfigurationCustomSetting();
        
        List<Luana_Extension_Settings__c> customSessingList = testDataGenerator.prepLuanaExtensionSettingCustomSettings();
        
        
        customSessingList.add(new Luana_Extension_Settings__c(Name = 'AdobeConnect_URL', Value__c = '  https://connect-staging.charteredaccountantsanz.com'));
        
        insert customSessingList;

        // PAN:6222
        Map<String, SObject> eduAssets = testDataGenerator.createEducationAssets();
        
        //Create user with Member and Employer community access
        memberAccount = testDataGenerator.generateNewApplicantAcc('Joe_' + classNamePrefixShort, classNamePrefixLong, 'Full_Member'); 
        memberAccount.Member_Id__c = '12345';
        memberAccount.Affiliated_Branch_Country__c = 'Australia';
        memberAccount.Membership_Class__c = 'Full';
        memberAccount.Assessible_for_CA_Program__c = false;
        memberAccount.PersonEmail = 'joe_1_' + classNamePrefixShort+'@gmail.com';//LCA-921 add personEmail
        memberAccount.PersonMailingStreet='www';
        memberAccount.PersonMailingCity='xxx';
        memberAccount.PersonMailingCountry='Indonesia';
        memberAccount.PersonMailingPostalCode='5678909';

        insert memberAccount;

        // PAN:6222 - We can't have the account be set to be assessible for CA Program upon insert as we need the Education History for the Account to do so
        List<Account> checkAccount = [SELECT Id, PersonContactId FROM Account WHERE Id = :memberAccount.Id];
        edu_Education_History__c edu_asset = testDataGenerator.createEducationHistory(null, checkAccount[0].PersonContactId, true, eduAssets.get('universityDegreeJoin1').Id);
        insert edu_asset;
        
        // once the valid Education History is in place, only then can we set the Accessible for CA Program to true
        memberAccount.Assessible_for_CA_Program__c = true;        
        update memberAccount;
        
        commProfIdMap = new Map<String, Id>();
        for(Profile prof: [SELECT Id, Name FROM Profile WHERE Name='NZICA Community Login User' or Name='Customer Community Login User']){
            commProfIdMap.put(prof.Name, prof.Id);
        }
    }
    
    //Used to initialise all the variables
    static void setup(String runSeqKey){
         
        //Create Business Account        
        businessAccount = testDataGenerator.generateNewBusinessAcc('Pricewaterhouse' + classNamePrefixShort, 'Business_Account', 'Chartered Accounting','30033003', '123456789', 'NZICA', '107 Bathurst Street', 'Active');
        businessAccount.Ext_Id__c = classNamePrefixShort+'_'+runSeqKey+'_1';
        insert businessAccount;
        
        //Create traning org
        trainingOrg = testDataGenerator.createNewTraningOrg(classNamePrefixLong, classNamePrefixLong, classNamePrefixShort, 'St 123', 'Wollonggong', '2244');
        insert trainingOrg;
       
        
        //Create program
        progrm = testDataGenerator.createNewProgram(classNamePrefixShort, classNamePrefixLong, 'Confirmed', 'Graduate diploma', 'Higher-level qualifications, other than training package qualifications or nationally recognised accredited courses');
        insert progrm;
        
        //Create Product
        prodList = new List<Product2>();
        List<String> prodLocName = new List<String>{'AU', 'NZ', 'INT'};
        createProductMethod(prodLocName, 'FIN', classNamePrefixShort);
        createProductMethod(prodLocName, 'Late', classNamePrefixShort);
        createProductMethod(prodLocName, 'Capstone', classNamePrefixShort);
        
        prodList.add(testDataGenerator.createNewProduct('TAX AU_' + classNamePrefixShort, 'TAX AU0001_' +classNamePrefixShort));
        
        Product2 auProd = testDataGenerator.createNewProduct('Oversea Exam Location (AU)_' + classNamePrefixShort, 'AU0001_OEL_' +classNamePrefixShort);
        auProd.IsActive = true;
        auProd.Member_Of__c = 'NZICA';
        prodList.add(auProd);
        Product2 nzProd = testDataGenerator.createNewProduct('Oversea Exam Location (NZ)_' + classNamePrefixShort, 'NZ0001_OEL_' + classNamePrefixShort);
        nzProd.IsActive = true;
        nzProd.Member_Of__c = 'NZICA';
        prodList.add(nzProd);
        
        insert prodList;
        //create price book entry
        pbes = new List<PricebookEntry>();
        List<Luana_Extension_Settings__c> csList = new List<Luana_Extension_Settings__c>();
        /*for(Product2 prod: prodList){
            if(prod.Name == 'FIN_AU_' + classNamePrefixShort){
                pbes.add(testDataGenerator.createPricebookEntry(Test.getStandardPricebookId(), prod.Id, 1300));
            }else if(prod.Name == 'FIN_NZ_' + classNamePrefixShort){
                pbes.add(testDataGenerator.createPricebookEntry(Test.getStandardPricebookId(), prod.Id, 1400));
            }else if(prod.Name == 'FIN_INT_' + classNamePrefixShort){
                pbes.add(testDataGenerator.createPricebookEntry(Test.getStandardPricebookId(), prod.Id, 1500));
            }else if(prod.Name == 'Oversea Exam Location (AU)_' + classNamePrefixShort){
                pbes.add(testDataGenerator.createPricebookEntry(Test.getStandardPricebookId(), prod.Id, 370));
                csList.add(new Luana_Extension_Settings__c(Name = 'ExamLocationFeeProduct_Australia', Value__c = prod.Id));
                csList.add(new Luana_Extension_Settings__c(Name = 'ExamLocationFeeProduct_Overseas', Value__c = prod.Id));
            }else if(prod.Name == 'Oversea Exam Location (NZ)_' + classNamePrefixShort){
                pbes.add(testDataGenerator.createPricebookEntry(Test.getStandardPricebookId(), prod.Id, 450));
                csList.add(new Luana_Extension_Settings__c(Name = 'ExamLocationFeeProduct_New Zealand', Value__c = prod.Id));
            }else if(prod.Name == 'Late_AU_' + classNamePrefixShort){
                pbes.add(testDataGenerator.createPricebookEntry(Test.getStandardPricebookId(), prod.Id, 140));
                csList.add(new Luana_Extension_Settings__c(Name = 'LateEnrolmentFeeProduct_Australia', Value__c = prod.Id));
            }else if(prod.Name == 'Late_NZ_' + classNamePrefixShort){
                pbes.add(testDataGenerator.createPricebookEntry(Test.getStandardPricebookId(), prod.Id, 150));
                csList.add(new Luana_Extension_Settings__c(Name = 'LateEnrolmentFeeProduct_New Zealand', Value__c = prod.Id));
            }else if(prod.Name == 'Late_INT_' + classNamePrefixShort){
                pbes.add(testDataGenerator.createPricebookEntry(Test.getStandardPricebookId(), prod.Id, 160));
                csList.add(new Luana_Extension_Settings__c(Name = 'LateEnrolmentFeeProduct_Overseas', Value__c = prod.Id));
            }else if(prod.Name == 'Capstone_AU_' + classNamePrefixShort){
                pbes.add(testDataGenerator.createPricebookEntry(Test.getStandardPricebookId(), prod.Id, 1600));
            }else if(prod.Name == 'Capstone_NZ_' + classNamePrefixShort){
                pbes.add(testDataGenerator.createPricebookEntry(Test.getStandardPricebookId(), prod.Id, 1700));
            }else if(prod.Name == 'Capstone_INT_' + classNamePrefixShort){
                pbes.add(testDataGenerator.createPricebookEntry(Test.getStandardPricebookId(), prod.Id, 1800));
            }
        }

        insert pbes;
        */
        insert csList;
        
        //Create multiple subjects
        subjList = new List<LuanaSMS__Subject__c>();
        //subjList.add(testDataGenerator.createNewSubject('TAX AU_' + classNamePrefixShort, 'TAX AU_' + classNamePrefixLong, 'TAX AU_' + classNamePrefixShort, 1, 'Module'));
        subjList.add(testDataGenerator.createNewSubject('TAX NZ_' + classNamePrefixShort, 'TAX NZ_' + classNamePrefixLong, 'TAX NZ_' + classNamePrefixShort, 1, 'Module'));
        //subjList.add(testDataGenerator.createNewSubject('AAA_' + classNamePrefixShort, 'AAA_' + classNamePrefixLong, 'AAA_' + classNamePrefixShort, 1, 'Module'));
        subjList.add(testDataGenerator.createNewSubject('Capstone_' + classNamePrefixShort, 'Capstone_' + classNamePrefixLong, 'Capstone', 1, 'Module'));
        subjList.add(testDataGenerator.createNewSubject('FIN_' + classNamePrefixShort, 'FIN_' + classNamePrefixLong, 'FIN_' + classNamePrefixShort, 55, 'Module'));
        //subjList.add(testDataGenerator.createNewSubject('MAAF_' + classNamePrefixShort, 'MAAF_' + classNamePrefixLong, 'MAAF_' + classNamePrefixShort, 60, 'Module'));
        insert subjList;
        
        //Create Program Offering
        po = cretePOMethod('PO_', 'Accredited_Program', progrm.Id, trainingOrg.Id, 
                           null, null, null, 1, true, 'Australia', 'CA Program', 
                           false, true, 'Manual Selection', false, 'luana_EnrolmentWizardDetailsCAProgram');
        insert po;
        
        poF = testDataGenerator.createNewProgOffering('PO_' + classNamePrefixLong, testDataGenerator.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get('Foundation'), 
        progrm.Id, trainingOrg.Id, prodList[0].Id, prodList[1].Id, prodList[2].Id, 1, 1);
        poF.LuanaSMS__Number_of_Required_Electives__c = 1;
        poF.LuanaSMS__Allow_Online_Enrolment__c = true;
        poF.Default_Program_Offering_Product__c = 'Australia';
        poF.Order_Item_Quantity__c = 'Based on Selected Subjects';
        poF.Product_Type__c = 'Foundation';
        poF.Require_Payment__c = true;
        poF.Require_Subject_Selection__c = true;
        poF.Subject_Selection_Option__c = 'Manual Selection';
        poF.Support_Employment_Token__c = false;
        poF.Wizard_Details_Page_Name__c = 'luana_EnrolmentWizardDetailsFoundation';
        
        insert poF;
        
        //Create multiple Program Offering Subject
        List<LuanaSMS__Program_Offering_Subject__c> posList = new List<LuanaSMS__Program_Offering_Subject__c>();
        for(LuanaSMS__Subject__c sub: subjList){
            if(sub.name =='TAX AU_' + classNamePrefixShort || sub.name =='TAX NZ_' + classNamePrefixShort){
                posList.add(testDataGenerator.createNewProgOffSubject(po.Id, sub.Id, 'Elective'));
                posList.add(testDataGenerator.createNewProgOffSubject(poF.Id, sub.Id, 'Elective'));
            }else{
                posList.add(testDataGenerator.createNewProgOffSubject(po.Id, sub.Id, 'Core'));
                posList.add(testDataGenerator.createNewProgOffSubject(poF.Id, sub.Id, 'Core'));
            }
        }
        insert posList;
        
        //Create course
        courseF = testDataGenerator.createNewCourse('Graduate Diploma of Chartered Accounting_' + classNamePrefixShort, poF.Id, testDataGenerator.getRecordTypeIdMap('LuanaSMS__Course__c').get('Foundation'), 'Running');
        courseF.LuanaSMS__Allow_Online_Enrolment__c = true;
        insert courseF;
        
        //Create Delivery Location
        devLocations = new List<LuanaSMS__Delivery_Location__c>();
        devLocations.add(testDataGenerator.createNewDeliveryLocationRecord('Australia - South Australia - Adelaide_' + classNamePrefixShort, 'Australia - South Australia - Adelaide_' +classNamePrefixLong, trainingOrg.Id, '1000', 'Australia'));
        devLocations.add(testDataGenerator.createNewDeliveryLocationRecord('United Kingdom - London_' + classNamePrefixShort, 'United Kingdom - London_' +classNamePrefixLong, trainingOrg.Id, '1200', 'United Kingdom'));
        insert devLocations;
        
        for(LuanaSMS__Delivery_Location__c dl: devLocations){
            devLocMap.put(dl.Name, dl);
        }
        
        //Create course
        courseAP = testDataGenerator.createNewCourse('Graduate Diploma of Chartered Accounting_' + classNamePrefixShort, po.Id, testDataGenerator.getRecordTypeIdMap('LuanaSMS__Course__c').get('Accredited_Program'), 'Running');
        courseAP.LuanaSMS__Allow_Online_Enrolment__c = true;
        insert courseAP;
        
        //Create default course custom setting
        insert new Luana_Extension_Settings__c(Name = 'Default_CA_Program_Course_Id', Value__c = courseF.Id);
        
        cdls = new List<Course_Delivery_Location__c>();
        for(LuanaSMS__Delivery_Location__c dl: devLocations){
            cdls.add(testDataGenerator.createCourseDeliveryLocation(courseF.Id, dl.Id, 'Exam Location'));
        }
        insert cdls;
        
        
        Account venueAccount = testDataGenerator.generateNewBusinessAcc(classNamePrefixShort + 'Test Venue'+'_'+runSeqKey+'_2', 'Venue', null, null, null, null, 'Sunshine Coast Office', 'Active');
        venueAccount.Ext_Id__c = classNamePrefixShort +'_'+runSeqKey+'_2';
        insert venueAccount;
        
        room1 = testDataGenerator.newRoom(classNamePrefixShort, classNamePrefixLong, venueAccount.Id, 20, 'Classroom', runSeqKey);
        insert room1;
        
        session1 = testDataGenerator.newSession(classNamePrefixShort, classNamePrefixLong, 'Virtual', 20, venueAccount.Id, room1.Id, null, system.now().addHours(1), system.now().addHours(2)); //e.g, 1pm - 2pm
        session1.Topic_Key__c = classNamePrefixShort + '_T1';
        session1.Adobe_Connect_Url__c = 'https://connect-staging.charteredaccountantsanz.com/r6wu6hclpou/';
        insert session1;
        
        coursesession1 = testDataGenerator.createCourseSession(courseF.Id, session1.Id);
        
        insert new List<LuanaSMS__Course_Session__c>{coursesession1};
        
        
    }
    
    //method for prepare sample data(prepareSampleEnrolmentData1): create multiple product
    public static void createProductMethod(List<String> prodLocName, String prodName, String namePrefixShort){
        integer counter = 1;
        for(String prodLoc: prodLocName){
            prodList.add(testDataGenerator.createNewProduct(prodName+'_'+prodLoc+'_' + namePrefixShort, prodName+'_'+prodLoc+'000'+counter+'_' +namePrefixShort));
            counter++;
        }
    }
    
    public static LuanaSMS__Program_Offering__c cretePOMethod(String Name, String recordType, Id prgId, Id tOrgId, 
                                                                Id prod1Id, Id prod2Id, Id prod3Id, Integer numOfReqElective, Boolean allowOnlineEnrol, String defProgOffProd, String prodType, 
                                                                Boolean reqPayment, Boolean reqSubjSelection, String subjSelectionOpt, Boolean suppEmplToken, String wDetailPage){
                                                                
        LuanaSMS__Program_Offering__c poObj = testDataGenerator.createNewProgOffering(Name + classNamePrefixShort, 
                                            testDataGenerator.getRecordTypeIdMap('LuanaSMS__Program_Offering__c').get(recordType), 
                                            prgId, tOrgId, prod1Id, prod2Id, prod3Id, 1, 1);
        poObj.LuanaSMS__Number_of_Required_Electives__c = numOfReqElective;
        poObj.LuanaSMS__Allow_Online_Enrolment__c = allowOnlineEnrol;
        poObj.Default_Program_Offering_Product__c = defProgOffProd;
        poObj.Product_Type__c = prodType;
        poObj.Require_Payment__c = reqPayment;
        poObj.Require_Subject_Selection__c = reqSubjSelection;
        poObj.Subject_Selection_Option__c = subjSelectionOpt;
        poObj.Support_Employment_Token__c = suppEmplToken;
        poObj.Wizard_Details_Page_Name__c = wDetailPage;
        
        return poObj;
    }
       
    public static testMethod void testContributor1() { 
        
        Test.startTest();
        
            initial();
            setup('1');
        Test.stopTest();

        //LCA-921
        memberUser = [Select Id, Email, FirstName, LastName, UserName, ContactId, Name from User Where Email =: 'joe_1_' + classNamePrefixShort+'@gmail.com' limit 1];
        userUtil = new luana_CommUserUtil(memberUser.Id);
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
        Luana_Extension_Settings__c defContriPermSetId = Luana_Extension_Settings__c.getValues('Default_ContCom_PermSet_Id');
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        //psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c));
        //psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defEmployerPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defContriPermSetId.value__c));
        
        System.runAs(memberUser){
            insert psas;
        }

        Id studProgAccModuleId = testDataGenerator.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module');

        LuanaSMS__Student_Program__c newStudProg = testDataGenerator.createNewStudentProgram(studProgAccModuleId, memberUser.ContactId, courseF.Id, 'Australia', 'In Progress');
        newStudProg.LuanaSMS__Auto_Student_Program_Subject_Creation__c = true;
        newStudProg.Exam_Location__c = cdls[0].Id;
        
        List<LuanaSMS__Student_Program__c> newStudentPrograms = new List<LuanaSMS__Student_Program__c>();
        newStudentPrograms.add(newStudProg);
        
        insert newStudentPrograms;
        
        LuanaSMS__Attendance2__c existingAttendance = new LuanaSMS__Attendance2__c();
        existingAttendance.LuanaSMS__Student_Program__c = newStudProg.Id;
        existingAttendance.LuanaSMS__Session__c = session1.Id;
        existingAttendance.LuanaSMS__Contact_Student__c = userUtil.custCommConId;
        existingAttendance.LuanaSMS__Type__c = 'Student';
        
        insert existingAttendance;
        
        LuanaSMS__Resource__c resource = testDataGenerator.createResource('Resource1', 'R1');
        insert resource;
        
        LuanaSMS__Resource_Booking__c rbObj = testDataGenerator.createResourceBooking(userUtil.custCommConId, resource.Id, session1.Id);
        insert rbObj;
        
        system.runAs(memberUser){
           
            luana_ContributorResBookingSearchCtl contSearch = new luana_ContributorResBookingSearchCtl();

            //initial run 
            contSearch.forwardToAuthPage();
            contSearch.getResourceBookings();
            
            //filter the search result by 'Virtual'
            contSearch.getResourceOptions();
            contSearch.assignmentType = 'Virtual';
            contSearch.doSearch();
            
            //select the record and view the detail
            contSearch.selectedResourceBooking = rbObj.Id + ':Virtual';
            contSearch.viewDetail();
            
            //select the record and view the detail
            contSearch.selectedResourceBooking = rbObj.Id + ':Workshop';
            contSearch.viewDetail();
            
            //select the record and view the detail
            contSearch.selectedResourceBooking = rbObj.Id + ':Exam Marking';
            contSearch.viewDetail();
            
            //view the detail page
            Test.setMock(HttpCalloutMock.class, new luana_AT_VCImportWizardMockHttp_Test());
            
            Test.setCurrentPageReference(new PageReference('luana_ContributorResBookingDetail')); 
            System.currentPageReference().getParameters().put('id', rbObj.Id);
            System.currentPageReference().getParameters().put('type', 'Workshop');
            luana_ContributorResBookingDetailCtl contDetailWs = new luana_ContributorResBookingDetailCtl();

            //PAN:6226 save without setting status
            contDetailWs.saveAttendance();

            // PAN:6222 PAN:6226 - Save the attendances
            for(LuanaSMS__Attendance2__c att : contDetailWs.sessionAttendances){
                att.LuanaSMS__Attendance_Status__c = 'Attended';
            }

            contDetailWs.saveAttendance();

            
            Test.setCurrentPageReference(new PageReference('luana_ContributorResBookingDetail')); 
            System.currentPageReference().getParameters().put('id', rbObj.Id);
            System.currentPageReference().getParameters().put('type', 'Exam Marking');
            luana_ContributorResBookingDetailCtl contDetailEm = new luana_ContributorResBookingDetailCtl();
            
            Test.setCurrentPageReference(new PageReference('luana_ContributorResBookingDetail')); 
            System.currentPageReference().getParameters().put('id', rbObj.Id);
            System.currentPageReference().getParameters().put('type', 'Virtual');
            luana_ContributorResBookingDetailCtl contDetailVt = new luana_ContributorResBookingDetailCtl();

            contDetailVt.getAttendances();
            contDetailVt.adobeSessionAccess();
            
            // Download the attendances
            contDetailVt.getDomainUrl();
            contDetailVt.selectedSessionId = session1.Id;
            contDetailVt.downloadAttendances();
            
        }

     }
     
     public static testMethod void testContributorDownload() { 
        
        Test.startTest();
            initial();
            setup('2');
        Test.stopTest();

        //LCA-921
        memberUser = [Select Id, Email, FirstName, LastName, UserName, ContactId, Name from User Where Email =: 'joe_1_' + classNamePrefixShort+'@gmail.com' limit 1];
        userUtil = new luana_CommUserUtil(memberUser.Id);
        Luana_Extension_Settings__c defMemberPermSetId = Luana_Extension_Settings__c.getValues('Default_Member_PermSet_Id');
        Luana_Extension_Settings__c defEmployerPermSetId = Luana_Extension_Settings__c.getValues('Default_Employer_PermSet_Id');
        Luana_Extension_Settings__c defContriPermSetId = Luana_Extension_Settings__c.getValues('Default_ContCom_PermSet_Id');
        List<PermissionSetAssignment> psas = new List<PermissionSetAssignment>();
        //psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defMemberPermSetId.value__c));
        //psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defEmployerPermSetId.value__c));
        psas.add(new PermissionSetAssignment(AssigneeId=memberUser.Id, PermissionSetId=defContriPermSetId.value__c));
        
        System.runAs(memberUser){
            insert psas;
        }
        
        Id studProgAccModuleId = testDataGenerator.getRecordTypeIdMap('LuanaSMS__Student_Program__c').get('Accredited_Module');

        LuanaSMS__Student_Program__c newStudProg = testDataGenerator.createNewStudentProgram(studProgAccModuleId, memberUser.ContactId, courseF.Id, 'Australia', 'In Progress');
        newStudProg.LuanaSMS__Auto_Student_Program_Subject_Creation__c = true;
        newStudProg.Exam_Location__c = cdls[0].Id;
        
        List<LuanaSMS__Student_Program__c> newStudentPrograms = new List<LuanaSMS__Student_Program__c>();
        newStudentPrograms.add(newStudProg);
        
        insert newStudentPrograms;
        
        LuanaSMS__Resource__c resource = testDataGenerator.createResource('Resource1', 'R1');
        insert resource;
        
        LuanaSMS__Resource_Booking__c rbObj = testDataGenerator.createResourceBooking(userUtil.custCommConId, resource.Id, session1.Id);
        insert rbObj;
        
        LuanaSMS__Attendance2__c existingAttendance = new LuanaSMS__Attendance2__c();
        existingAttendance.LuanaSMS__Student_Program__c = newStudProg.Id;
        existingAttendance.LuanaSMS__Session__c = session1.Id;
        existingAttendance.LuanaSMS__Contact_Student__c = userUtil.custCommConId;
        existingAttendance.LuanaSMS__Type__c = 'Student';
        
        insert existingAttendance;

        
        system.runAs(memberUser){
            //query the attendance record for download
            PageReference pageRef = Page.luana_ContributorAttDownload;
            pageRef.getParameters().put('id', session1.id);
            Test.setCurrentPage(pageRef);
            luana_ContributorAttDownloadCtl contAttDown = new luana_ContributorAttDownloadCtl();
            
            
            
            System.assertEquals(contAttDown.wpAttendance.size(), 1, 'Expected 1 record is write into the attendnace csv file');
            
        }
            

     }
     
}