/**
* @author Jannis Bott
* @date 25/11/2016
*
* @description  Tests that callouts work and that exceptions are handled correctly.
* calloutSuccess test is using 'HttpMock' custom metadata configuration
*/
@isTest(SeeAllData=false)
public with sharing class CalloutServiceTest {

    @isTest
    static void calloutSuccess() {
        Test.setMock(HttpCalloutMock.class, new HttpResponseMock.HttpResponseSuccess());

        String response = CalloutService.executeCallout('HttpMock', '{Sample Params}','{Sample Body}');
        System.debug(response);

        System.assertEquals('{"count":1,"results":[{"suggestion":"147 Ross Street, FOREST LODGE  NSW 2037","matched":[],"format":"https://api.edq.com/capture/address/v2/format?country=AUS&id=700AUS-aOAUSHArgBwAAAAAIAwEAAAAAK.6m0AAgAAAAAAAAAAD..2QAAAAA.....wAAAAAAAAAAAAAAAAAxNDcgUm9zcyBTdHJlZXQA"}]}', response, 'Mock response should be returned and match.');
    }

    @isTest
    static void calloutNoConfigFoundError() {
        Test.setMock(HttpCalloutMock.class, new HttpResponseMock.HttpResponseFailure());

        String response = CalloutService.executeCallout('Does not exist', '{Sample Params}', '{Sample Body}');
        System.debug(response);

        System.assertEquals('{"CalloutServiceError":"UNEXPECTED_EXCEPTION"}', response, 'Mock response should return null');
    }

    @isTest
    static void calloutError() {
        Test.setMock(HttpCalloutMock.class, new HttpResponseMock.HttpResponseFailure());

        String response = CalloutService.executeCallout('HttpMock', '{Sample Params}', '{Sample Body}');
        System.debug(response);

        System.assertEquals('{"Data" : "Not found", "Message" : "Error"}', response, 'Mock response should return error response');
    }
}