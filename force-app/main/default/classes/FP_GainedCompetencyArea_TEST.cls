/**
 * Developer: WDCi (LKoh)
 * Date: 18-10-2018
 * Task #: PAN5340 - Flexipath Gained Competency Area UI    
**/
@isTest
public class FP_GainedCompetencyArea_TEST {
	
    static testmethod void validateGainedCompetencyArea() {
        
        Luana_DataPrep_Test testDataGenerator = new Luana_DataPrep_Test();        
        
        // Generate test assets
        Map<String, SObject> assetMap = assetGenerator();
        
        // Generate Education History assets
        Map<String, SObject> eduAssetMap = testDataGenerator.createEducationAssets();
        
        edu_Education_History__c edu_asset = testDataGenerator.createEducationHistory(null, assetMap.get('contact1').Id, true, eduAssetMap.get('universityDegreeJoin1').Id);
        insert edu_asset;

        FP_GainedCompetencyArea_CTRL gcaCtrl = new FP_GainedCompetencyArea_CTRL();
        gcaCtrl.passedContactID = assetMap.get('contact1').Id;
        gcaCtrl.getEducationHistoryWrappers();
        gcaCtrl.getCompetencyAreaWrappers();

        test.startTest();
		
        // Simulating the addition of new Gained Competency Area by checking on one of the listed Competency Area        
        for (FP_GainedCompetencyArea_CTRL.CompetencyAreaWrapper caw : gcaCtrl.competencyAreaWrapperList) {
            caw.caChecked = true;
            break;
        }
        
        // Simulating deletion of education history
        system.debug('Education History: ' +gcaCtrl.educationHistoryWrapperList);        
        for (FP_GainedCompetencyArea_CTRL.EducationHistoryWrapper ehw : gcaCtrl.educationHistoryWrapperList) {
            gcaCtrl.SelectedEduHistoryId = ehw.ehRecord.Id;
            break;
        }        
        gcaCtrl.deleteEduHistory();

        gcaCtrl.saveRecords();
        
        test.stopTest();
    }
    
    static Map<String, SObject> assetGenerator() {
        
        Map<String, SObject> assetMap = new Map<String, SObject>();        
        
        // Record Type
        List<RecordType> requiredRecordType = [SELECT Id FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName ='Business_Account'];
        assetMap.put('businessAccountID', requiredRecordType[0]);
        
        // Competency Area
        List<FP_Competency_Area__c> caList = new List<FP_Competency_Area__c>();
        
        FP_Competency_Area__c ca1 = new FP_Competency_Area__c();	
        ca1.FP_Active__c = true;
        ca1.FP_Description__c = 'Test Description 1';
        ca1.FP_Display_Only__c = false;
        caList.add(ca1);
        
        FP_Competency_Area__c ca2 = new FP_Competency_Area__c();
        ca2.FP_Active__c = true;
        ca2.FP_Description__c = 'Test Description 2';
        ca2.FP_Display_Only__c = true;
        caList.add(ca2);
        
        insert caList;
        assetMap.put('ca1', ca1);
        assetMap.put('ca2', ca2);
        
        // PersonAccount
        List<Account> accountList = new List<Account>();
        Account account1 = new Account();        
        account1.RecordTypeId = requiredRecordType[0].Id;
        account1.Name = 'Test Account1';
        account1.BillingStreet = 'Test Street';
        /**
        account1.FirstName = 'Tester1';
        account1.LastName = 'Person';
        account1.PersonEmail = 'test@gmail.com';
        account1.PersonHomePhone = '0000000';
        account1.Salutation = 'Mr';
		**/
        
        insert account1;
        accountList.add(account1);
        
        // Contact
        List<Contact> contactList = new List<Contact>();
        
        Contact contact1 = new Contact();
        contact1.FirstName = 'Tester1';
        contact1.LastName = 'Person';
        contact1.AccountId = account1.Id;
        contactList.add(contact1);
        
        insert contactList;
        assetMap.put('contact1', contact1);
        
        // Subject
        List<LuanaSMS__Subject__c> subjectList = new List<LuanaSMS__Subject__c>();
        
        LuanaSMS__Subject__c subject1 = new LuanaSMS__Subject__c();
        subjectList.add(subject1);
            
        insert subjectList;
        assetMap.put('subject1', subject1);
        
        // Relevant Competency Area
        List<FP_Relevant_Competency_Area__c> rcaList = new List<FP_Relevant_Competency_Area__c>();
        
        FP_Relevant_Competency_Area__c rca1 = new FP_Relevant_Competency_Area__c();
        rca1.FP_Competency_Area__c = ca1.Id;
        rca1.FP_Type__c = 'Requires';
        rca1.FP_Subject__c = subject1.Id;
        rcaList.add(rca1);
        
        insert rcaList;
        assetMap.put('rca1', rca1);
        
        // Gained Competency Area
        List<FP_Gained_Competency_Area__c> gcaList = new List<FP_Gained_Competency_Area__c>();
        
        FP_Gained_Competency_Area__c gca1 = new FP_Gained_Competency_Area__c();
        gca1.FP_Competency_Area__c = ca1.Id;
        gca1.FP_Contact__c = contact1.Id;
        
        gcaList.add(gca1);
        
        return assetMap;
	}
}