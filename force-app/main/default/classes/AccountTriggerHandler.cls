/***********************************************************************************************************************************************************************
Name: AccountTriggerHandler 
============================================================================================================================== 
Purpose: Trigger Handler for Merging of Multiple Triggerd of Account. Below are the list of Triggers Merged
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR                 DATE          DETAIL     Description 
1.0        Rama Krishna, Vinay    31/01/2019    Created    Trigger Handler for Merging of Multiple Triggers of Account. Below are the list of Triggers Merged
                                                            * AccountTrigger
                                                            * SegmentationAccountChange
                                                            * CPDAccountTrigger
                                                            * luana_ActivateNMUser
                                                            * MemberTrigger
                                                            * luana_AccountAfterAction 
                                                            * ExperianDataQuality_Account_BIBU
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/
public class AccountTriggerHandler extends TriggerHandler{
        //Context Variable Collections after Type Casting
        List<Account> newList = (List<Account>)Trigger.new;
        List<Account> oldList = (List<Account>)Trigger.old;
        Map<Id,Account> oldMap = (Map<Id,Account>)Trigger.oldMap;
        Map<Id,Account> newMap = (Map<Id,Account>)Trigger.newMap;        
        
        static Id memPortalProId; 
        //Get Filtered summary accounts where type is member and has membership approval date              
        List<Account> accounts;                          
        Map<Id, Id> userPermSetMap = new Map<Id, Id>();
        
        public AccountTriggerHandler(){            
            if(Trigger.new != null && !Trigger.new.isEmpty())
                accounts = CPDSummaryHandler.filterSummaryAccounts(Trigger.new);
                
            if(memPortalProId == null){
                list<Profile> profileList = [SELECT UserType, Name, Id FROM Profile where Name =: System.Label.Member_Portal_ProfileName];
                if(profileList != null && !profileList.isEmpty())
                    memPortalProId = profileList[0].Id;
            }            
        }
        
        public override void beforeInsert(){            
            //Provide Validation for Account Duplicate User name          
            AccountTriggerClass.checkDuplicateUsername(newList, oldMap, TRUE);
            
            if(!accounts.isEmpty()){
                //Set Account Specializations from CPD Summary records
                CPDSummaryHandler.setAccontSpecialisations(accounts);
            }
            
            //Perform Logic for EDQ.DataQualityService
            EDQ.DataQualityService.SetValidationStatus(newList, oldList,Trigger.IsInsert, 2);
            
            //Adds logic to remove member permission set when member resigned & handle affiliate permission set assignment 
            luana_MemberDataIntegrationHandler.validateCAPCAFAssesability(newList, oldMap);            
        }
                     
        public override void beforeUpdate(){              
            //Provide Validation for Account Duplicate User name       
            AccountTriggerClass.checkDuplicateUsername(newList, oldMap, FALSE);
            
            //Stops the update of member value from PASA formAssembly forms
            AccountTriggerClass.stopMemberOfUpdate(newList, oldMap);  
            
            //Perform Logic for creation of CPD Summary records and fucntionality related to Member Accounts.    
            Account_CPDMemberHandler.accounts=accounts;            
            Account_CPDMemberHandler.CPDMemberChanges(newList, oldMap);
            
            //Perform Logic for EDQ.DataQualityService
            EDQ.DataQualityService.SetValidationStatus(newList, oldList,Trigger.IsInsert, 2);
            
            //Adds logic to remove member permission set when member resigned & handle affiliate permission set assignment
            luana_MemberDataIntegrationHandler.validateCAPCAFAssesability(newList, oldMap);                     
        }       
        
        public override void beforeDelete(){
        }
               
        public override void afterInsert(){              
            //Creates Community Users for the Accounts.
            Account_CommunityHandler.afterInsertChanges(newList);
            
            if(!accounts.isEmpty()){
                //Create Account Summary Records
                CPDSummaryHandler.createAccountSummaryRecords(accounts);
            } 
            
            //Inserts or updates the Segmentation object records on Account Changes    
            Account_SegmentationAccountChangeHandler.segmentationAccountChanges(newList, newMap, oldList, oldMap);
            
            //Assigns LUANA_NonMemberCommunityPermissionset to users
            Account_LuanaHandler.luanaActivateNMUser(newList);
            
            //Adds logic to remove member permission set when member resigned & handle affiliate permission set assignment
            luana_MemberDataIntegrationHandler.checkCommUserPermSet(true,false,newList, newMap);           
        }

        public override void afterUpdate(){              
            //Creates Community Users for Accounts.
            Account_CommunityHandler.afterUpdateChanges(newList,oldMap);
            
            //Update Account Summary Specializations
            Account_CPDMemberHandler.accounts=accounts;            
            Account_CPDMemberHandler.updateSummarySpecialisations(newList,oldMap);
            
            //Insert or update the Segmentation object records on Account Changes
            Account_SegmentationAccountChangeHandler.segmentationAccountChanges(newList, newMap, oldList, oldMap);  
            AccountUserEmailChangeClass.personAccEmailUpdate(newList, oldMap);
            
            //Updates member profile based on Member Type & Membership Class of Account.
            AccountTriggerClass.updateMemberProfile(newList, oldMap);
            
            //Assigns LUANA_NonMemberCommunityPermissionset to users
            Account_LuanaHandler.luanaActivateNMUser(newList);
            
            //Logic to remove member permission set when member resigned, affiliate permission set assignments, validations related to degree and year.
            luana_MemberDataIntegrationHandler.checkCommUserPermSet(false,true,newList,oldMap);            
       
            //Update qpr Referral and qpr Risk Assessment Records
            Account_QPRChangeHandler.qprAccountChanges(newList, newMap, oldList, oldMap);
        
        }
        
        public override void afterDelete(){
          
        }
        
        public override void afterUndelete(){
           
        }
      
}