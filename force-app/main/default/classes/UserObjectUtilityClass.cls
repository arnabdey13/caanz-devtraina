public class UserObjectUtilityClass {
    
    /*
* Method for after insert trigger
* Return : Void
* Parameter : List<User>
*/
    /*public static void updateUserName(List<User> userList, String suffixString){
        List<User> tempUserList = new List<User>();
        Map<Id, Boolean> userIdisAllowedMap = UserObjectUtilityClass.checkProfileAccess(userList, 'CAANZCustomerCommunityProfile') ;
        for(User userRec : userList){
            if(userRec.IsActive && userRec.IsPortalEnabled && userIdisAllowedMap.keySet().contains(userRec.Id)){
                User u = new User(Id = userRec.Id, Username = userRec.Username);
                u.Username = userRec.Email + '.' + suffixString ;  
                tempUserList.add(u);
            }
        }
        update tempUserList ;
    }*/
    
    /*
* Method for after insert/update trigger
* Return : Void
* Parameter : List<User>
*/
    /*public static void updateContactEmail(List<User> userList, Map<Id, User> mapUserOld, Boolean flagCheck){
        Set<Id> userIdSet = new Set<Id>();
        Map<Id, String> contactIdEmailMap = new Map<Id, String>();
        Map<Id, Boolean> userIdisAllowedMap = UserObjectUtilityClass.checkProfileAccess(userList, 'CAANZCustomerCommunityProfile') ;
        
        //filtering user's which are on portal + active + profile considered for updating Username as [Email+Suffix]
        for(User userRec : userList){
            if( (userRec.IsActive && userRec.IsPortalEnabled && userIdisAllowedMap.keySet().contains(userRec.Id) && flagCheck) || 
               (userRec.IsActive && userRec.IsPortalEnabled && userIdisAllowedMap.keySet().contains(userRec.Id) && !flagCheck && userRec.Email != mapUserOld.get(userRec.Id).Email) ){
                   userIdSet.add(userRec.Id) ;           
               }
        }
        for(User userTemp : [SELECT ContactId, Email FROM User WHERE Id IN : userIdSet]){
            contactIdEmailMap.put(userTemp.ContactId, userTemp.Email) ;
        }
        //Calling method to update Person Account email field.
        if(!contactIdEmailMap.isEmpty()){
            UserObjectUtilityClass.updateContactEmailOnUserEmailUpdate(contactIdEmailMap);
        }
        
    }     */   
    
    
    
    /*
* Method for updating PersonAccount email on User's email update
* Return : Void
* Parameter : List<User>
*/
    
    /*public static void updateContactEmailOnUserEmailUpdate(Map<Id, String> contactIdEmailMap){
        List<Contact> contactUpdateList = new List<Contact>();
        for(Contact con : [SELECT Email FROM Contact WHERE Id IN : contactIdEmailMap.keySet()]){
            con.Email = contactIdEmailMap.get(con.Id) ;
            contactUpdateList.add(con);
        }
        if(!contactUpdateList.isEmpty()){
            Database.update(contactUpdateList) ;
        }        
    }*/
    
    /*
* Method for validating User's profile against 'CAANZCustomerCommunityProfile' custom permisison.
* Profiles assigned with 'CAANZCustomerCommunityProfile' custom permission are only considered for UserName formatting.
* Return : Map<Id, Boolean>
* Parameter : List<User> userList, String customPermissionName
*/
    
    public static Map<Id, Boolean> checkProfileAccess(List<User> userList, String customPermissionName){
        Boolean isProfileAllowed = false;
        Set<Id> userIdSet = new Set<Id>();
        for(User u : userList){
            userIdSet.add(u.Id);
        }
        //Fetching custom permision for checking weather current user's profile considered for UserName update.
        String customPermissionId = null ;
        for(CustomPermission c : [SELECT Id, DeveloperName FROM CustomPermission WHERE DeveloperName = 'CAANZCustomerCommunityProfile' LIMIT 1]){
            customPermissionId = c.Id ;
        }
        
        Map<Id, Id> persmisisonUserMap = new Map<Id, Id>();
        List<PermissionSetAssignment> permisson = [SELECT PermissionSetId, AssigneeId FROM PermissionSetAssignment WHERE AssigneeId IN : userIdSet];
        for(PermissionSetAssignment p: permisson){
            persmisisonUserMap.put(p.PermissionSetId, p.AssigneeId);
        }
        system.debug('****persmisisonUserMap***'+persmisisonUserMap);
        
        //Collecting UserId+Boolean value to check is User's profile is have cusotm permission assigned or not.
        Map<Id, Boolean> userIdisAllowedMap = new Map<Id, Boolean>();
        for(SetupEntityAccess s : [SELECT SetupEntityId, ParentId FROM SetupEntityAccess WHERE SetupEntityId =: customPermissionId AND
                                   ParentId IN : persmisisonUserMap.keySet()]){
                                       if(persmisisonUserMap.keySet().contains(s.ParentId)){
                                           userIdisAllowedMap.put(persmisisonUserMap.get(s.ParentId), true);
                                       }  
                                   }
        system.debug('****userIdisAllowedMap***'+userIdisAllowedMap);
        return userIdisAllowedMap ;
        
    }
    
}