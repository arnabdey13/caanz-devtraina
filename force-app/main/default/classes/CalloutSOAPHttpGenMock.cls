/**
* @author Andrew Kopec
* @date 05/09/2017
*
* @description  HttpMockGenerator for SOAP
*
*/
@isTest
global class CalloutSOAPHttpGenMock {
	
    // general call to confirm success
    global class SOAPHttpResponseSuccess implements HttpCalloutMock {
        
        global HTTPResponse respond(HTTPRequest request) {
            System.assertEquals('POST', request.getMethod());
			
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/xml');
			response.setBody('{"foo":"bar"}');
            response.setStatusCode(200);
            return response;
        }
    }

    // QAS SOAP call to confirm successful verification call
    global class SOAPHttpQASResponseSuccess implements HttpCalloutMock {
        global HTTPResponse respond(HTTPRequest request) {
			 // Test 33 Erskine St, Sydney NSW 2000'
    		String XMLResponse = '<?xml version="1.0" encoding="UTF-8"?> <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" > <soapenv:Body> <ond:QASearch xmlns:ond="http://www.qas.com/OnDemand-2011-03"> <ond:Engine Threshold="5" >Verification</ond:Engine> <ond:Country>AUG</ond:Country> <ond:Layout>AUGMeshBlock</ond:Layout> <ond:Search>33 Erskine St, Sydney NSW 2000</ond:Search> </ond:QASearch> </soapenv:Body> </soapenv:Envelope> ';
        	System.assertEquals('POST', request.getMethod()); 

            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/xml');
            response.setBody(XMLResponse);
            response.setStatusCode(200);
            return response;
        }
    }
    
	// QAS SOAP call to confirm successful verification call
    global class SOAPHttpQASDataPlusRespSuccess implements HttpCalloutMock {
        global HTTPResponse respond(HTTPRequest request) {
    		String XMLResponse = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Header><QAInformation xmlns="http://www.qas.com/OnDemand-2011-03"><StateTransition>SearchResults</StateTransition><CreditsUsed>1</CreditsUsed></QAInformation></soap:Header><soap:Body><QASearchResult VerifyLevel="Verified" xmlns="http://www.qas.com/OnDemand-2011-03"><QAAddress DPVStatus="DPVNotConfigured"><AddressLine LineContent="None"><Label /><Line>80 Weld Street</Line></AddressLine><AddressLine><Label>Suburb</Label><Line /></AddressLine><AddressLine><Label>City</Label><Line>Hokitika</Line></AddressLine><AddressLine><Label>Postcode</Label><Line>7810</Line></AddressLine><AddressLine><Label>Country</Label><Line>New Zealand</Line></AddressLine><AddressLine LineContent="DataPlus"><Label>DPID</Label><Line>804169</Line></AddressLine><AddressLine LineContent="DataPlus"><Label>Mesh Block ID</Label><Line>MB 2416402</Line></AddressLine><AddressLine LineContent="DataPlus"><Label>Mesh Block Code</Label><Line>2416402</Line></AddressLine><AddressLine LineContent="DataPlus"><Label>Regional Council Code</Label><Line>12</Line></AddressLine><AddressLine LineContent="DataPlus"><Label>Regional Council Name</Label><Line>West Coast Region</Line></AddressLine><AddressLine LineContent="DataPlus"><Label>Territorial Authority Code</Label><Line>57</Line></AddressLine><AddressLine LineContent="DataPlus"><Label>Territorial Authority Name</Label><Line>Westland District</Line></AddressLine><AddressLine LineContent="DataPlus"><Label>Centroid of Property NZTM X Coordinate</Label><Line>1433474.03630687</Line></AddressLine><AddressLine LineContent="DataPlus"><Label>Centroid of Property NZTM Y Coordinate</Label><Line>5268367.65300363</Line></AddressLine></QAAddress><VerificationFlags /></QASearchResult></soap:Body></soap:Envelope> ';
        	System.assertEquals('POST', request.getMethod()); 

            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/xml');
            response.setBody(XMLResponse);
            response.setStatusCode(200);
            return response;
        }
    }
    
    // QAS SOAP call to confirm successful verification call
    global class SOAPHttpQASAUDataPlusRespSuccess implements HttpCalloutMock {
        global HTTPResponse respond(HTTPRequest request) {
            // 20698620000 - 10416402
    		String XMLResponse = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Header><QAInformation xmlns="http://www.qas.com/OnDemand-2011-03"><StateTransition>SearchResults</StateTransition><CreditsUsed>1</CreditsUsed></QAInformation></soap:Header><soap:Body><QASearchResult VerifyLevel="Verified" xmlns="http://www.qas.com/OnDemand-2011-03"><QAAddress DPVStatus="DPVNotConfigured"><AddressLine LineContent="None"><Label /><Line>800 Pacific Hwy</Line></AddressLine><AddressLine><Label>Suburb</Label><Line /></AddressLine><AddressLine><Label>City</Label><Line>Chatswood</Line></AddressLine><AddressLine><Label>Postcode</Label><Line>72067</Line></AddressLine><AddressLine><Label>Country</Label><Line>Australia</Line></AddressLine><AddressLine LineContent="DataPlus"><Label>DPID</Label><Line>804169</Line></AddressLine><AddressLine LineContent="DataPlus"><Label>Mesh Block ID</Label><Line>20698620000</Line></AddressLine><AddressLine LineContent="DataPlus"><Label>Mesh Block Code</Label><Line>20698620000</Line></AddressLine><AddressLine LineContent="DataPlus"><Label>Latitude</Label><Line>14.03630687</Line></AddressLine><AddressLine LineContent="DataPlus"><Label>Longitude</Label><Line>152.65300363</Line></AddressLine></QAAddress><VerificationFlags /></QASearchResult></soap:Body></soap:Envelope> ';
        	System.assertEquals('POST', request.getMethod()); 

            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/xml');
            response.setBody(XMLResponse);
            response.setStatusCode(200);
            return response;
        }
    }
    
    
     // QAS SOAP call to confirm successful getAddress call
    global class SOAPHttpQASGetAddressResponse implements HttpCalloutMock {
        global HTTPResponse respond(HTTPRequest request) {
            String XMLResponse = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Header><QAInformation xmlns="http://www.qas.com/OnDemand-2011-03"><StateTransition>SearchResults</StateTransition><CreditsUsed>1</CreditsUsed></QAInformation></soap:Header><soap:Body><Address xmlns="http://www.qas.com/OnDemand-2011-03"><QAAddress DPVStatus="DPVNotConfigured"><AddressLine LineContent="None"><Label /><Line>Suite 1</Line></AddressLine><AddressLine LineContent="None"><Label /><Line>123 Meadowland Drive</Line></AddressLine><AddressLine LineContent="None"><Label /><Line>Somerville</Line></AddressLine><AddressLine LineContent="None"><Label /><Line>Auckland   2014</Line></AddressLine></QAAddress></Address></soap:Body></soap:Envelope>';
			System.assertEquals('POST', request.getMethod());
                
    	 	HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/xml');
            response.setBody(XMLResponse);
            response.setStatusCode(200);
            return response;
        }
    }
    
    
    // QAS SOAP call to confirm successful getAddress call
    global class SOAPHttpQASGetAddressResponseFromPickList implements HttpCalloutMock {
        global HTTPResponse respond(HTTPRequest request) {
            // String XMLResponse = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Header><QAInformation xmlns="http://www.qas.com/OnDemand-2011-03"><StateTransition>SearchResults</StateTransition><CreditsUsed>1</CreditsUsed></QAInformation></soap:Header><soap:Body><Address xmlns="http://www.qas.com/OnDemand-2011-03"><QAAddress DPVStatus="DPVNotConfigured"><AddressLine LineContent="None"><Label/><Line>2 Hill Street</Line></AddressLine><AddressLine><Label>Suburb</Label><Line>Thorndon</Line></AddressLine><AddressLine><Label>City</Label><Line>Wellington</Line></AddressLine><AddressLine><Label>Postcode</Label><Line>6011</Line></AddressLine></QAAddress></Address></soap:Body></soap:Envelope>';			
            String XMLResponse = '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Header><QAInformation xmlns="http://www.qas.com/OnDemand-2011-03"><StateTransition>SearchResults</StateTransition><CreditsUsed>1</CreditsUsed></QAInformation></soap:Header><soap:Body><Address xmlns="http://www.qas.com/OnDemand-2011-03"><QAAddress DPVStatus="DPVNotConfigured"><AddressLine LineContent="None"><Label/><Line>2 Hill Street</Line></AddressLine><AddressLine><Label>Suburb</Label><Line>Thorndon</Line></AddressLine><AddressLine><Label>City</Label><Line>Wellington</Line></AddressLine><AddressLine><Label>Postcode</Label><Line>6011</Line></AddressLine><AddressLine LineContent="DataPlus"><Label>Mesh Block Code</Label><Line>61211111</Line></AddressLine></QAAddress></Address></soap:Body></soap:Envelope>';			
            
            System.assertEquals('POST', request.getMethod());
                
    	 	HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/xml');
            response.setBody(XMLResponse);
            response.setStatusCode(200);
            return response;
        }
    }
    

    // QAS SOAP call to for Failure
    global class SOAPHttpQASResponseFailure implements HttpCalloutMock {
        global HTTPResponse respond(HTTPRequest request) {

            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/xml');
            //response.setBody('<?xml version="1.0" encoding="UTF-8"?>');
            String fault = '<?xml version="1.0" encoding="utf-8"?> ' +
                			'<soap:Envelope ' +
                            '   xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" ' +
                            '   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' +
                            '   xmlns:xsd="http://www.w3.org/2001/XMLSchema"> ' +
                            '       <soap:Body> ' +
                            '           <soap:Fault> ' +
                            '                <faultcode>NotLicensed</faultcode> ' +
                            '                <faultstring>The user is not entitled to use New Zealand.</faultstring> ' +
                            '                <faultactor>http://www.qas.com/OnDemandIntermediary/</faultactor> ' +
                            '                <detail> ' +
                            '                    <ExceptionCode>NotLicensed</ExceptionCode> ' +
                            '                    <Message>The user is not entitled to use New Zealand.</Message> ' +
                            '                    <ErrorId>-1000</ErrorId> ' +
                            '                    <RequestId>72931830-46f5-4978-813b-da3cec361d15</RequestId> ' +
                            '                </detail> ' +
                            '            </soap:Fault> ' +
                            '        </soap:Body> ' +
                            '    </soap:Envelope> ' ;

          
            response.setBody(fault);
            response.setStatusCode(404);

            return response;
        }
    }
    

     // QAS SOAP call to for No Match
    global class SOAPHttpQASResponseNoMatches implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest request) {

            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/xml');
            //response.setBody('<?xml version="1.0" encoding="UTF-8"?>');
            String body =   '<?xml version="1.0" encoding="utf-8"?> ' +
                			'<soap:Envelope ' +
                            '   xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" ' +
                            '   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' +
                            '   xmlns:xsd="http://www.w3.org/2001/XMLSchema"> ' +
                			'	<soap:Header> ' +
							'	<QAInformation ' +
							'	xmlns="http://www.qas.com/OnDemand-2011-03"> ' +
							'	<StateTransition>NoMatches</StateTransition> ' +
							'	<CreditsUsed>0</CreditsUsed> ' +
							'	</QAInformation> ' +
							'	</soap:Header> ' +
                            '	<soap:Body> ' +
							'	<QASearchResult ' +
							'		xmlns="http://www.qas.com/OnDemand-2011-03"> ' +
							'		<QAPicklist> ' +
							'			<FullPicklistMoniker>AUG|cc7756f1-2580….</FullPicklistMoniker> ' +
							'			<PicklistEntry Information="true" WarnInformation="true"> ' +
							'				<Moniker /> ' +
							'				<PartialAddress /> ' +
							'				<Picklist>No matches</Picklist> ' +
							'				<Postcode /> ' +
							'				<Score>0</Score> ' +
							'			</PicklistEntry> ' +
							'			<Prompt>Enter selection</Prompt> ' +
							'			<Total>0</Total> ' +
							'		</QAPicklist> ' +
							'		<VerificationFlags /> ' +
							'	</QASearchResult> ' +               
                            '	</soap:Body> ' +
                            '</soap:Envelope> ' ;

            response.setBody(body);
            response.setStatusCode(200);

            return response;
        }
    }
    
    // QAS SOAP Wrap call for Failure
    global class SOAPHttpQASWrapResponseFailure implements HttpCalloutMock {
        global HTTPResponse respond(HTTPRequest request) {

            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/xml');
            response.setBody('<?xml version="1.0" encoding="UTF-8"?>');
            response.setStatusCode(404);

            return response;
        }
    }
    
    // QAS SOAP call with Exception
    global class SOAPHttpQASCalloutFailure implements HttpCalloutMock {
        global HTTPResponse respond(HTTPRequest request) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/xml');
            response.setStatusCode(999);
			CalloutException e = (CalloutException)CalloutException.class.newInstance();
        	e.setMessage('QAS call unsuccessful - check configuration and access keys');
        	throw e;
            return response;
        }
    }
    
    
    
    
}