/**
    Developer: WDCi (Lean)
    Development Date: 05/04/2016
    Task #: Enrollment wizard - Controller for Subject selection
    
    Change History:
    LCA-822 03/08/2016: WDCi Lean - add sps object for ProgramEnrolmentWizard (ICAI)
**/

public without sharing class luana_ProgramOfferingSubjectWrapper {
    
    public boolean isSelected {set; get;}
    public boolean isCore {set; get;}
    public boolean isDisabled {set; get;}
    
    public LuanaSMS__Program_Offering_Subject__c pos {set; get;}
    public LuanaSMS__Student_Program_Subject__c sps {set; get;}
    
    public luana_ProgramOfferingSubjectWrapper(boolean isSelected, boolean isCore, boolean isDisabled, LuanaSMS__Program_Offering_Subject__c pos, LuanaSMS__Student_Program_Subject__c sps) {
        this.isSelected = isSelected;
        this.isCore = isCore;
        this.isDisabled = isDisabled;
        this.pos = pos;
        this.sps = sps;
    }
    
}