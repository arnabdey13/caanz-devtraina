/*
    Developer: WDCi (Lean)
    Date: 09/May/2016
    Task #: Luana implementation - Wizard for Student session allocation for a course
    
    Change History:
    08/08/2016 WDCi - KH: LCA-831, include Supp Exam Location for exam allocation
*/

public with sharing class luana_StudentSessionAllocationController {
    
    public LuanaSMS__Course__c course {private set; public get;}
    public List<Course_Delivery_Location__c> courseDeliveryLocations {private set; public get;}
    public boolean enabled {private set; public get;}
    public boolean hasMissingStudProgLocation {private set; public get;}
    
    public Map<Id, List<LuanaSMS__Student_Program__c>> courseDeliveryLocationStudentPrograms {private set; get;}
    public Map<Id, integer> courseDeliverySPCount {private set; get;}
    public Map<Id, decimal> courseDeliveryAvailabilityCount {private set; get;}
    
    public List<luana_ErrorRecordWrapper> errorRecords {private set; get;}
    
    public integer successCount {private set; get;}
    public integer errorCount {private set; get;}
    
    private String targetCourseDeliveryLocationId {set; get;}
    private String targetCourseId {set; get;}
    private Luana_Extension_Settings__c luanaReportConfig {set; get;}
    
    Course_Delivery_Location__c selectedCDL {set; get;}
    
    public luana_StudentSessionAllocationController (ApexPages.StandardController controller) {
        
        errorRecords = new List<luana_ErrorRecordWrapper>();
        successCount = 0;
        errorCount = 0;
        
        targetCourseDeliveryLocationId = getCourseDeliveryLocationIdFromUrl();
        targetCourseId = getCourseIdFromUrl();
                
        init();
    }
    
    private String getCourseIdFromUrl(){
        if(ApexPages.currentPage().getParameters().containsKey('id')){
            return ApexPages.currentPage().getParameters().get('id');
            
        }
        
        return null;
    }
    
    private String getCourseDeliveryLocationIdFromUrl(){
        if(ApexPages.currentPage().getParameters().containsKey('cdlid')){
            return ApexPages.currentPage().getParameters().get('cdlid');
            
        }
        
        return null;
    }
    
    public void init(){
        enabled = false;
        hasMissingStudProgLocation = false;
        
        courseDeliveryLocations = new List<Course_Delivery_Location__c>();
        courseDeliveryLocationStudentPrograms = new Map<Id, List<LuanaSMS__Student_Program__c>>();
        courseDeliverySPCount = new Map<Id, integer>();
        courseDeliveryAvailabilityCount = new Map<Id, decimal>();
        
        selectedCDL = new Course_Delivery_Location__c();
        
        luanaReportConfig = Luana_Extension_Settings__c.getValues('Exam Allocation Report Params');
        
        if(luanaReportConfig != null){
            if(targetCourseId != null){
                try{
                    //get course name, also work as validation to make sure course is valid
                    course = [select Id, Name from LuanaSMS__Course__c where Id =: targetCourseId];
                    
                    if(!String.isBlank(targetCourseDeliveryLocationId)){
                        selectedCDL = [select id, Type__c from Course_Delivery_Location__c where id =: targetCourseDeliveryLocationId];
                    }
                    
                    Map<Id, LuanaSMS__Student_Program__c> nonAllocatedStudPrograms = new Map<Id, LuanaSMS__Student_Program__c>();
                        
                    for(LuanaSMS__Student_Program__c studProg : Database.query(getStudentProgramQuery())){
                        if(studProg.LuanaSMS__Attendances__r.size() == 0 && selectedCDL.Type__c != 'Supp Exam Location'){
                            if(studProg.Exam_Location__c != null){
                                
                                //LCA-831, improve the code to method to cater for supp exam allocation
                                /*if(courseDeliveryLocationStudentPrograms.containsKey(studProg.Exam_Location__c)){
                                    List<LuanaSMS__Student_Program__c> existingStudProgs = courseDeliveryLocationStudentPrograms.get(studProg.Exam_Location__c);
                                    existingStudProgs.add(studProg);
                                    
                                    courseDeliveryLocationStudentPrograms.put(studProg.Exam_Location__c, existingStudProgs);
                                    
                                } else {
                                    List<LuanaSMS__Student_Program__c> newStudProgs = new List<LuanaSMS__Student_Program__c>();
                                    newStudProgs.add(studProg);
                                    
                                    courseDeliveryLocationStudentPrograms.put(studProg.Exam_Location__c, newStudProgs);
                                    
                                }*/
                                
                                setExamSessionLocation(studProg, studProg.Exam_Location__c);
                            } else {
                                hasMissingStudProgLocation = true;
                            }
                        
                        //LCA-831, Assume attendance must not empty because first attendance is for failed exam    
                        } else { //Start from here is for Supp Exam, because Supp Exam should has Attendance already & Supp Exam Location should has value as well
                            integer counter = 0;
                            for(LuanaSMS__Attendance2__c att: studProg.LuanaSMS__Attendances__r){
                                if(att.RecordType.DeveloperName == luana_SessionConstants.RECORDTYPE_ATTENDANCE_SUPP_EXAM){
                                    counter++;
                                }
                            }

                            if(counter == 0 && studProg.Supp_Exam_Location__c != null && studProg.Sup_Exam_Enrolled__c == true){   
                                setExamSessionLocation(studProg, studProg.Supp_Exam_Location__c);
                            }
                        }
                    }
                    
                    system.debug('courseDeliveryLocationStudentPrograms :: ' + courseDeliveryLocationStudentPrograms);
                    
                    if(!courseDeliveryLocationStudentPrograms.isEmpty()){
                        
                        for(Course_Delivery_Location__c cdl : Database.query(getCourseDeliveryLocationQuery())){
                            
                            if(cdl.Sessions__r.size() > 0 || courseDeliveryLocationStudentPrograms.containsKey(cdl.Id)){
                                courseDeliveryLocations.add(cdl);
                                
                                if(courseDeliveryLocationStudentPrograms.containsKey(cdl.Id)){
                                    courseDeliverySPCount.put(cdl.Id, courseDeliveryLocationStudentPrograms.get(cdl.Id).size());
                                } else {
                                    courseDeliverySPCount.put(cdl.Id, 0);
                                }
                                
                                Decimal availability = 0;
                                for(LuanaSMS__Session__c session : cdl.Sessions__r){
                                    availability += session.Maximum_Capacity__c - session.LuanaSMS__Number_of_enrolled_students__c;
                                }
                                
                                courseDeliveryAvailabilityCount.put(cdl.Id, availability);
                                
                            }
                        }
                        
                        if(courseDeliveryLocations.size() > 0){
                            enabled = true;
                        } else {
                            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'The course does not have delivery location.'));
                        }
                        
                        if(hasMissingStudProgLocation){
                            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'There is enrolment/s missing Exam Location. Please note that the enrolment/s will not be allocated.'));
                        }
                    } else {
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'No outstanding enrolment to be allocated for this course.'));
                    }
                } catch(Exception exp){
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error retrieving course. Please try again later or contact your system administrator. Error: ' + exp.getMessage()));
                }
            } else {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Invalid course. Please try again later or contact your system administrator.'));
            }
        } else {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Missing report configuration \'Exam Allocation Report Params\'. Please try again later or contact your system administrator.'));
        }
    }
    
    //LCA-831 method for allocate exam session this improvement is for exam and sub exam 
    public void setExamSessionLocation(LuanaSMS__Student_Program__c studProg, Id examLocId){
    
        if(courseDeliveryLocationStudentPrograms.containsKey(examLocId)){
            List<LuanaSMS__Student_Program__c> existingStudProgs = courseDeliveryLocationStudentPrograms.get(examLocId);
            existingStudProgs.add(studProg);
            
            courseDeliveryLocationStudentPrograms.put(examLocId, existingStudProgs);
            
        } else {
            List<LuanaSMS__Student_Program__c> newStudProgs = new List<LuanaSMS__Student_Program__c>();
            newStudProgs.add(studProg);
            
            courseDeliveryLocationStudentPrograms.put(examLocId, newStudProgs);
            
        }
        
    }
    
    public PageReference doSave(){
    
        List<LuanaSMS__Attendance2__c> newAttendances = new List<LuanaSMS__Attendance2__c>();
        Map<Id, Course_Delivery_Location__c> courseDeliveryLocationsMap = new Map<Id, Course_Delivery_Location__c>(courseDeliveryLocations);
        
        Map<Id, decimal> sessionSeatCount = new Map<Id, decimal>();
        Map<Id, LuanaSMS__Session__c> allocatedSessions = new Map<Id, LuanaSMS__Session__c>();
        
        errorRecords = new List<luana_ErrorRecordWrapper>();
        successCount = 0;
        errorCount = 0;
        
        for(Course_Delivery_Location__c cdl : courseDeliveryLocations){
            for(LuanaSMS__Session__c session : cdl.Sessions__r){
                sessionSeatCount.put(session.Id, session.LuanaSMS__Number_of_enrolled_students__c);
            }
        }
        
        for(String examLocationId : courseDeliveryLocationStudentPrograms.keySet()){
            for(LuanaSMS__Student_Program__c studProg : courseDeliveryLocationStudentPrograms.get(examLocationId)){
                if(courseDeliveryLocationsMap.containsKey(examLocationId)){
                    for(LuanaSMS__Session__c session : courseDeliveryLocationsMap.get(examLocationId).Sessions__r){
                        
                        if(sessionSeatCount.get(session.Id) < session.Maximum_Capacity__c){
                            LuanaSMS__Attendance2__c newAttendance = new LuanaSMS__Attendance2__c();
                            newAttendance.LuanaSMS__Contact_Student__c = studProg.LuanaSMS__Contact_Student__c;
                            newAttendance.LuanaSMS__Student_Program__c = studProg.Id;
                            newAttendance.LuanaSMS__Start_time__c = session.LuanaSMS__Start_Time__c;
                            newAttendance.LuanaSMS__End_time__c = session.LuanaSMS__End_time__c;
                            newAttendance.LuanaSMS__Session__c = session.Id;

                            
                            newAttendances.add(newAttendance);
                            
                            sessionSeatCount.put(session.Id, sessionSeatCount.get(session.Id) + 1);
                            
                            allocatedSessions.put(session.Id, session);
                            
                            break;
                        }
                        
                    }
                    
                } else {
                    
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Missing course delivery location (' + examLocationId + '). Please try again later or contact your system administrator.'));
                    return null;
                }
            }
        }
        
        Savepoint sp = Database.setSavepoint();
        
        try{
            
            Set<Id> succeededSessionIds = new Set<Id>();
            
            for(List<LuanaSMS__Attendance2__c> pagedNewAttendances : getPageIterable(newAttendances, 500)){
                
                
                Database.SaveResult[] saveResults = Database.insert(pagedNewAttendances, false);
                integer counter = 0;
                for(Database.SaveResult sr : saveResults){
                    if(!sr.isSuccess()){
                        
                        luana_ErrorRecordWrapper errorRecord = new luana_ErrorRecordWrapper(pagedNewAttendances.get(counter), sr.getErrors()[0].getMessage());
                        errorRecords.add(errorRecord);
                        
                        errorCount ++;
                    } else {
                        successCount ++;
                        succeededSessionIds.add(pagedNewAttendances.get(counter).LuanaSMS__Session__c);
                    }
                    
                    counter ++;
                }
            }
            
            if(successCount > 0){
                
                String detailsMsg = '';
                for(Id sessionId : succeededSessionIds){
                    detailsMsg += '<a href="' + luanaReportConfig.Value__c + String.valueOf(sessionId).substring(0, 15) + '" target="_blank">' + allocatedSessions.get(sessionId).Name + '</a><br/>';
                }
                
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, successCount + ' student program/s are allocated successfully. Please see the reports below for details.'));
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, detailsMsg));
            }
            
            if(errorCount > 0){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'There are ' + errorCount + ' student program/s failed to be allocated.'));
            }
            
            init();
            return null;
            
        } catch(Exception exp){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error allocating session for student/s. Please try again later or contact your system administrator. Error: ' + exp.getMessage()));
            Database.rollback(sp);
        }
        
        return null;
    }
    
    private List<List<LuanaSMS__Attendance2__c>> getPageIterable(List<LuanaSMS__Attendance2__c> sobjectList, integer pageSize){
         
        List<List<LuanaSMS__Attendance2__c>> iterableList = new List<List<LuanaSMS__Attendance2__c>>();
         
        if(sobjectList != null){
            integer size = pageSize;
             
            integer counter = 1;
            integer listCounter = 0;
            List<LuanaSMS__Attendance2__c> pageList = new List<LuanaSMS__Attendance2__c>();
             
            for(LuanaSMS__Attendance2__c sobj : sobjectList){
                 
                pageList.add(sobj);
                 
                if(counter == size || listCounter == (sobjectList.size() - 1)){
                    iterableList.add(pageList.clone());
                    pageList = new List<LuanaSMS__Attendance2__c>();
                    counter = 1;
                     
                } else {
                    counter ++;
                }
                 
                listCounter ++;
                 
            }
        }
        
        return iterableList;
    } 
    
    //LCA-831, include SUPP_EXAM filter for Attendnace Query and also include Supp_Exam_Location__c, Sup_Exam_Enrolled__c, Supp_Exam_Paid__c fields
    private String getStudentProgramQuery(){
        if(targetCourseDeliveryLocationId != null){
            if(selectedCDL.Type__c == 'Supp Exam Location'){
                return 'select Id, Name, Exam_Location__c, LuanaSMS__Contact_Student__c, Supp_Exam_Location__c, Sup_Exam_Enrolled__c, Supp_Exam_Paid__c, (select Id, Name, RecordType.DeveloperName from LuanaSMS__Attendances__r where (RecordType.DeveloperName = \'' + luana_SessionConstants.RECORDTYPE_ATTENDANCE_SUPP_EXAM + '\')) from LuanaSMS__Student_Program__c where LuanaSMS__Course__c =: targetCourseId and Supp_Exam_Location__c =: targetCourseDeliveryLocationId and Supp_Exam_Paid__c = true and (LuanaSMS__Status__c = \'In Progress\' OR LuanaSMS__Status__c = \'Fail\') order by Random_Number__c asc'; //LCA-715
            } else {
                return 'select Id, Name, Exam_Location__c, LuanaSMS__Contact_Student__c, Supp_Exam_Location__c, Sup_Exam_Enrolled__c, Supp_Exam_Paid__c, (select Id, Name, RecordType.DeveloperName from LuanaSMS__Attendances__r where (RecordType.DeveloperName = \'' + luana_SessionConstants.RECORDTYPE_ATTENDANCE_EXAM + '\')) from LuanaSMS__Student_Program__c where LuanaSMS__Course__c =: targetCourseId and Exam_Location__c =: targetCourseDeliveryLocationId and Paid__c = true and (LuanaSMS__Status__c = \'In Progress\') order by Random_Number__c asc'; //LCA-715
            }
        } else {
            return 'select Id, Name, Exam_Location__c, LuanaSMS__Contact_Student__c, Supp_Exam_Location__c, Sup_Exam_Enrolled__c, Supp_Exam_Paid__c, (select Id, Name, RecordType.DeveloperName from LuanaSMS__Attendances__r where (RecordType.DeveloperName = \'' + luana_SessionConstants.RECORDTYPE_ATTENDANCE_EXAM + '\')) from LuanaSMS__Student_Program__c where LuanaSMS__Course__c =: targetCourseId and Paid__c = true and (LuanaSMS__Status__c = \'In Progress\') order by Random_Number__c asc'; //LCA-715
        }
    }
    
    //LCA-831, include SUPP_EXAM filter for Session Query
    private String getCourseDeliveryLocationQuery(){
        if(targetCourseDeliveryLocationId != null){
            if(selectedCDL.Type__c == 'Supp Exam Location'){
                return 'select Id, Name, Delivery_Location__r.Name, ' +
                   '(select Id, Name, LuanaSMS__Number_of_enrolled_students__c, Maximum_Capacity__c, LuanaSMS__Start_Time__c, LuanaSMS__End_Time__c, LuanaSMS__Venue__r.Name, LuanaSMS__Room__r.Name, RecordType.DeveloperName from Sessions__r where (RecordType.DeveloperName = \'' + luana_SessionConstants.RECORDTYPE_SESSION_SUPP_EXAM + '\') order by LuanaSMS__Start_Time__c asc nulls last) ' +  
                   'from Course_Delivery_Location__c where Course__c =: targetCourseId and Id =: targetCourseDeliveryLocationId';
            } else {
                return 'select Id, Name, Delivery_Location__r.Name, ' +
                   '(select Id, Name, LuanaSMS__Number_of_enrolled_students__c, Maximum_Capacity__c, LuanaSMS__Start_Time__c, LuanaSMS__End_Time__c, LuanaSMS__Venue__r.Name, LuanaSMS__Room__r.Name, RecordType.DeveloperName from Sessions__r where (RecordType.DeveloperName = \'' + luana_SessionConstants.RECORDTYPE_SESSION_EXAM + '\') order by LuanaSMS__Start_Time__c asc nulls last) ' +  
                   'from Course_Delivery_Location__c where Course__c =: targetCourseId and Id =: targetCourseDeliveryLocationId';
            }
        } else {
            return 'select Id, Name, Delivery_Location__r.Name, ' +
               '(select Id, Name, LuanaSMS__Number_of_enrolled_students__c, Maximum_Capacity__c, LuanaSMS__Start_Time__c, LuanaSMS__End_Time__c, LuanaSMS__Venue__r.Name, LuanaSMS__Room__r.Name, RecordType.DeveloperName from Sessions__r where (RecordType.DeveloperName = \'' + luana_SessionConstants.RECORDTYPE_SESSION_EXAM + '\') order by LuanaSMS__Start_Time__c asc nulls last) ' +  
               'from Course_Delivery_Location__c where Course__c =: targetCourseId and Type__c = \'Exam Location\'';
        }
    }
    
    class luana_ErrorRecordWrapper{
        
        public String errorMessage {set;get;}
        public LuanaSMS__Attendance2__c attendance {set; get;}
        
        public luana_ErrorRecordWrapper(LuanaSMS__Attendance2__c attendance, String errorMessage){
            this.attendance = attendance;
            this.errorMessage = errorMessage;
        }
    }
}