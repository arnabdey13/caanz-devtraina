/*
    Developer: WDCi (Leo)
    Date: 03/04/2017
    Task#: Pre-Final Adjustment Wizard
*/

public with sharing class AAS_PreFinalAdjustment {

    private static Boolean debug = true;
    private static final Integer LIST_SIZE = 100;
    private static final Integer EDIT_SIZE = 100;
    private static final Integer SEARCH_TOTAL_SIZE = 2000;
    private static final Integer EDIT_TOTAL_SIZE = 2000;
    /**
    private static final String queryFieldSearch = 
    ' Id, AAS_MarkedForAdjustment__c, AAS_Member_Id_Formula__c, AAS_First_Name_Formula__c' +
	', AAS_Last_Name_Formula__c, AAS_Total_Non_Exam_Mark_Formula__c, AAS_Exam_Number__c	' +
	', AAS_Total_Module_Mark_Formula__c, AAS_Total_Final_Mark__c' +		
	', AAS_Student_Assessment__r.AAS_Module_Result__c' +
	', AAS_Temporary_Final_Result__c';
	
	private static final String queryFieldEdit = 
    ' Id, AAS_MarkedForAdjustment__c, AAS_Member_Id_Formula__c, AAS_First_Name_Formula__c' +
	', AAS_Last_Name_Formula__c, AAS_Total_Non_Exam_Mark_Formula__c, AAS_Exam_Number__c' +
	', AAS_Total_Module_Mark_Formula__c, AAS_Total_Final_Mark_Baseline__c, AAS_Result__c' +		
	', AAS_Student_Assessment__r.AAS_Module_Result__c, AAS_Student_Assessment__r.AAS_Final_Result__c' +
	', AAS_Module_Mark_Adjustment__c, AAS_Special_Consideration_Adjustment__c' +
	', AAS_Temporary_Exam_Result__c, AAS_Temporary_Module_Result__c, AAS_Temporary_Exam_Mark__c, AAS_Temporary_Module_Mark__c, AAS_Temporary_Final_Result__c ';
	**/
	
	private static final String queryFieldSearch = 
    ' AAS_MarkedForAdjustment__c' +
    ', AAS_Total_Non_Exam_Mark_Formula__c' +
    ', AAS_Result__c' +
    ', AAS_Total_Module_Mark_Formula__c' +
    ', AAS_Total_Final_Mark__c' +
    // ', AAS_Student_Assessment__r.AAS_Module_Result__c' +
    ', AAS_Module_Result_Formula__c' +
    ', AAS_Temporary_Final_Result__c' +
    // ', AAS_Name_Member_ID__c' +
    ', AAS_First_Name_Formula__c' +
    ', AAS_Last_Name_Formula__c' +
    ', AAS_Member_Id_Formula__c' +
	', AAS_Exam_Number__c';
	// ', AAS_Student_Assessment__r.AAS_Student_Program__r.LuanaSMS__Contact_Student__r.FirstName' +
	// ', AAS_Student_Assessment__r.AAS_Student_Program__r.LuanaSMS__Contact_Student__r.LastName' +
	// ', AAS_Student_Assessment__r.AAS_Student_Program__r.LuanaSMS__Contact_Student__r.Member_ID__c';
	
	private static final String queryFieldEdit = 
    ' Id, AAS_MarkedForAdjustment__c, AAS_Member_Id_Formula__c' +
    ', AAS_First_Name_Formula__c' +
    ', AAS_Last_Name_Formula__c' +
	', AAS_Total_Non_Exam_Mark_Formula__c, AAS_Exam_Number__c' +
	', AAS_Total_Module_Mark_Formula__c, AAS_Total_Final_Mark_Baseline__c, AAS_Result__c' +
	', AAS_Student_Assessment__r.AAS_Module_Result__c' +
	// ', AAS_Student_Assessment__r.AAS_Module_Result__c, AAS_Student_Assessment__r.AAS_Final_Result__c' +
	', AAS_Module_Mark_Adjustment__c, AAS_Special_Consideration_Adjustment__c' +
	', AAS_Temporary_Exam_Result__c, AAS_Temporary_Module_Result__c, AAS_Temporary_Exam_Mark__c, AAS_Temporary_Module_Mark__c, AAS_Temporary_Final_Result__c ';
	
	private static final String queryPassingMark = 
    ' Id, AAS_Student_Assessment__r.AAS_Course_Assessment__r.AAS_Module_Passing_Mark__c, AAS_Passing_Mark__c';
    
	// private AAS_Settings__c aasConfigs;
	public Boolean error {get;set;}

	// Page 1 filter criterias
	public String st_MemberID {get;set;}
    public String st_ExamNo {get;set;}
    
    public String st_FirstName {get;set;}
    public String st_LastName {get;set;}
    
    public String st_Name {get;set;}
    public Decimal st_TotalExamMarkLow {get;set;}
    public Decimal st_TotalExamMarkHigh {get;set;}
    public Decimal st_FinalModuleMarkLow {get;set;}
    public Decimal st_FinalModuleMarkHigh {get;set;}
    public Boolean sasSearchNoResult {get;set;}

    // Dynamic Title and Descriptions
    public String headerTitle {get;set;}
    public String headerDescription {get;set;}
    public String page0Instruction1 {get;set;}
    public String page0Instruction2 {get;set;}
    public String page0AInstruction1 {get;set;}
    public String headerDescriptionPage1 {get;set;}
    public String headerDescriptionPage2 {get;set;}
    
    // Page 0A variables
    public Boolean preliminaryProcessingComplete {get;set;}
    
    // Page 3 variables
    public Integer sasUpdatedCount {get;set;}
    
    // RecordType IDs
    private Id examAssessment_recordTypeID;
    private Id courseAssessmentTM_recordTypeID;
    
    // Stores the Course Assessment record that was used to initialize the wizard
    public AAS_Course_Assessment__c initialCA {get;set;}
    
    // Stores the passing marks that are going to be used to calculate the exam and module pass
    public String examPassingMark {get;set;}
    public Boolean examPassingMarkEmpty {get;set;}
    public Decimal modulePassingMark {get;set;}
    
    // Record Count variables
    public Integer totalAvailableRecords {get;set;}
        	
    public AAS_PreFinalAdjustment() {
        initialize();
    }

    private void initialize() {

		// Read the custom setting for AAS
		// aasConfigs = AAS_Settings__c.getValues('PreFinalAdjustmentUser');

        // URL param reader
        Map<String, String> urlParameterMap = ApexPages.currentPage().getParameters();      
        if (urlParameterMap.containsKey('id')) {
            initialCA = [SELECT Id, RecordTypeId, AAS_Pre_Final_Adjustment__c, AAS_Non_Exam_Data_Loaded__c, AAS_Exam_Data_Loaded__c, AAS_Cohort_Adjustment_Exam__c, AAS_Borderline_Remark__c, AAS_Exam_Adjustment__c, AAS_Final_Adjustment_Exam__c FROM AAS_Course_Assessment__c WHERE Id = :urlParameterMap.get('id') LIMIT 1];
        }
        
		// saving recordType IDs for runtime use
		examAssessment_recordTypeID = AAS_ToolBox.recordTypeRetriever(AAS_Student_Assessment_Section__c.SObjectType, 'Exam Assessment SAS');
		courseAssessmentTM_recordTypeID = AAS_ToolBox.recordTypeRetriever(AAS_Course_Assessment__c.SObjectType, 'Technical Module');		
		
		// Check for the Sequence checkbox
        // note: Step 5 checkbox does not exist
        String sequenceCheckResult = '';
        if (initialCA.RecordTypeId == courseAssessmentTM_recordTypeID) {
        	sequenceCheckResult = AAS_ToolBox.courseAssessmentSequenceChecker(initialCA, new List<String>{'Non Exam Data Loaded','Exam Data Loaded','Cohort Adjustment','Borderline Remark','Exam Adjustment'});
        } else {
        	sequenceCheckResult = AAS_ToolBox.courseAssessmentSequenceChecker(initialCA, new List<String>{'Non Exam Data Loaded','Exam Data Loaded','Cohort Adjustment','Borderline Remark','Module Adjustment'});
        }
        
        if(debug) system.debug('sequenceCheckResult: ' +sequenceCheckResult);        
		if (sequenceCheckResult != 'PASS') {
			// Sequence check failed, aborting wizard sequence
			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'Error: You are not allowed to continue in this step. Please complete the Exam Adjustment/Module Adjustment first.'));
			error = true;
		} else {
			error = false;
		}
		
		String preFinalResultRan = AAS_ToolBox.courseAssessmentSequenceChecker(initialCA, new List<String> {'Pre-Final Adjustment'});
		if (preFinalResultRan == 'PASS') {
		    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'Pre-Final Adjustment was commited before. You are not allowed to re-do Pre-Final Adjustment.'));
			error = true;
		} else {
		    error = false;
		}
		
		// Query and store the passing mark for both exam and module, the mark is shared between all SAS of the same Course Assessment so only 1 record is necessary
		// This is not included as part of the SAS query as it would cost too much view state given the record size
		String optionalQueryString = ' AND RecordTypeID = \'' +examAssessment_recordTypeID+ '\' LIMIT 1';
		List<AAS_Student_Assessment_Section__c> passingMarkQuery = AAS_ToolBox.getAllRelevantSAS(initialCA, queryPassingMark, optionalQueryString);
		system.debug('passingMarkQuery: ' +passingMarkQuery);
		for (AAS_Student_Assessment_Section__c sas : passingMarkQuery) {
		    if (sas.AAS_Passing_Mark__c == null) {
		        examPassingMark = 'EMPTY';
		        examPassingMarkEmpty = true;
		    } else {
		        examPassingMark = String.valueOf(sas.AAS_Passing_Mark__c);
		        examPassingMarkEmpty = false;
		    }
		    modulePassingMark = sas.AAS_Student_Assessment__r.AAS_Course_Assessment__r.AAS_Module_Passing_Mark__c;
		}

        preliminaryProcessingComplete = false;

        st_MemberID = '';
        st_ExamNo = '';
        /**
        st_FirstName = '';
        st_LastName = '';
        **/
        st_Name = '';
        sasSearchNoResult = false;        

        // Initializing the Dynamic Title values
        headerTitle = 'Pre-Final Adjustment';
        headerDescription = 'This wizard provides full visibility of mark and result at Exam and Module level so that user can decide whether to add the following mark to students: </br>1. Module Mark Adjustment </br>2. Special Consideration Adjustment </br>These adjustments will be added into exam mark after commit.';
        page0Instruction1 = 'New Session			: clear off the previous selection';
        page0Instruction2 = 'Continue Old Session	: continue with the previously selected records';
        page0AInstruction1 = 'Click on Prepare Data to begin.';
        headerDescriptionPage1 = ' </br> </br> Note: Search Result will return a maximum of 2000 records';                
        headerDescriptionPage2 = ' </br> </br> Note: Adjustment value will be saved as soon as the user moved from the current page unless the page is closed without completing the wizard. </br> Use \'Save and Continue Later\' to save all current adjustment into the records and close the wizard to continue later';
    }
    
    public ApexPages.StandardSetController con {
        get {
            if (con == null) {
                // String optionalQueryString = ' AND RecordTypeID = \'' +examAssessment_recordTypeID+ '\'';
                String optionalQueryString = ' AND RecordTypeID = \'' +examAssessment_recordTypeID+ '\' LIMIT ' +SEARCH_TOTAL_SIZE;
                List<AAS_Student_Assessment_Section__c> sasList = AAS_ToolBox.getAllRelevantSAS(initialCA, queryFieldSearch, optionalQueryString);
				con = new ApexPages.StandardSetController(sasList);
		        con.setPageSize(LIST_SIZE);
            }                
            return con;
        }
        set;
    }
    
    //######## Action method ########//
    
    // Save the existing selection if there is any
    public void saveCurrentRecord() {
    	try {
	    	if (debug) system.debug('currentSASList for saving: ' +con.getRecords());
	    	con.save();
    	} catch (Exception e) {
    		if (debug) system.debug('Exception in saveCurrentRecord: ' +e+ ' stack trace: ' +e.getStackTraceString());    		
    		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error encountered : ' +e.getMessage()));
    	}
    }
    
    // Save the currently viewed selectedRecords 
    public Boolean saveAllSelectedRecord() {
    	try {
    	    con.save();
    	    return true;
    	} catch (Exception e) {
    		if (debug) system.debug('Exception in saveCurrentRecord: ' +e+ ' stack trace: ' +e.getStackTraceString());    		
    		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error encountered : ' +e.getMessage()));
    		return false;
    	}
	}
	
	public List<AAS_Student_Assessment_Section__c> getSASList() {
    	return con.getRecords(); 
    }

    //-------- Action method --------//

    //######## Search method ########//
    public void searchRecord() {
    	
    	// Save the existing selection if there is any
    	saveCurrentRecord();    	
		
		String optionalQueryString = ' AND RecordTypeID = \'' +examAssessment_recordTypeID+ '\'';
		String searchString = '';
		
		// construct the search string based on the search fields
		if (!String.isBlank(st_MemberID)) {
			searchString += ' AND AAS_Student_Assessment__r.AAS_Member_Id__c = \'' +String.escapeSingleQuotes(st_MemberID)+ '\'';
		}
		if (!String.isBlank(st_ExamNo)) {
			searchString += ' AND AAS_Exam_Number__c = \'' +String.escapeSingleQuotes(st_ExamNo)+ '\'';
		}
		
		if (!String.isBlank(st_Name)) {
		    searchString += ' AND AAS_Name_Member_ID__c LIKE \'%' +String.escapeSingleQuotes(st_Name)+ '%\'';
		}
		
		if (!String.isBlank(st_FirstName)) {
			searchString += ' AND AAS_Student_Assessment__r.AAS_First_Name__c LIKE \'%' +String.escapeSingleQuotes(st_FirstName)+ '%\'';
		}
		if (!String.isBlank(st_LastName)) {
			searchString += ' AND AAS_Student_Assessment__r.AAS_Last_Name__c LIKE \'%' +String.escapeSingleQuotes(st_LastName)+ '%\'';
		}
		
		if (st_TotalExamMarkLow != null) {
			searchString += ' AND AAS_Total_Final_Mark__c >= ' +st_TotalExamMarkLow;
		}
		if (st_TotalExamMarkHigh != null && st_TotalExamMarkHigh != 0) {
			searchString += ' AND AAS_Total_Final_Mark__c <= ' +st_TotalExamMarkHigh;
		}
		if (st_FinalModuleMarkLow != null) {
			searchString += ' AND AAS_Student_Assessment__r.AAS_Total_Module_Mark_Non_Exam_Exam__c >= ' +st_FinalModuleMarkLow;
		}
		if (st_FinalModuleMarkHigh != null && st_FinalModuleMarkHigh != 0) {
			searchString += ' AND AAS_Student_Assessment__r.AAS_Total_Module_Mark_Non_Exam_Exam__c <= ' +st_FinalModuleMarkHigh;
		}

		optionalQueryString += searchString;
		optionalQueryString += ' LIMIT ' +SEARCH_TOTAL_SIZE;
		
		List<AAS_Student_Assessment_Section__c> sasList = AAS_ToolBox.getAllRelevantSAS(initialCA, queryFieldSearch, optionalQueryString);
		con = new ApexPages.StandardSetController(sasList);
        con.setPageSize(LIST_SIZE);
    }
    
    //-------- Search method --------//
    
    //######## Navigation Methods ########//
    public PageReference cancel() {
    	try {
    	    cleanAssets();
    	} catch (Exception e) {
    	    system.debug('Problem occurred during asset cleaning: ' +e);
    	}
    	PageReference backToCourse = new PageReference('/' + initialCA.Id);
        return backToCourse;
    }

    public PageReference exitWithoutReset() {

        saveCurrentRecord();
        PageReference backToCourse = new PageReference('/' + initialCA.Id);
        return backToCourse;
    }
    
    public PageReference exitWithoutReset2() {
        
        if (saveAllSelectedRecord()) {
            PageReference backToCourse = new PageReference('/' + initialCA.Id);
            return backToCourse;
        } else {
            // If Error occurred during save, do not redirect to Course Assessment so the error message can be shown
            return null;
        }
    }
    
    /**
    public PageReference autoRedirectA() {
    	
    	// Check for the presence of another user logged in to use the wizard based on the custom setting
    	// if any exist, stop and warn the user if they wish to overidde the existing user
    	if (AAS_ToolBox.preFinalAdjustmentCurrentlyUsed(aasConfigs) || error) {    		
    		return null;
    	} else {
    		return nextToPage0B();
    	}
    }
    **/
    
    public void preliminarySetup() {
    	// Update all the relevant SAS as a preemptive move to prepare the data
    	/**
        if (AAS_ToolBox.updateSAS(initialCA.Id, examAssessment_recordTypeID)) {
        **/
        try {
            String preliminarySetupField = ' Id, AAS_Total_Final_Mark_Baseline__c, AAS_Module_Mark_Adjustment__c, AAS_Special_Consideration_Adjustment__c, AAS_Temporary_Exam_Mark__c, AAS_Temporary_Module_Mark__c, AAS_Total_Non_Exam_Mark_Formula__c, AAS_Temporary_Exam_Result__c, AAS_Temporary_Module_Result__c, AAS_Temporary_Final_Result__c';
            List<AAS_Student_Assessment_Section__c> relevantSASList = AAS_ToolBox.getAllRelevantSAS(initialCA, preliminarySetupField, ' AND RecordTypeID = \'' +examAssessment_recordTypeID+ '\'');
            if (relevantSASList.size() > 0) {
                AAS_ToolBox.calculateMarkAndPass(relevantSASList, examPassingMark, modulePassingMark);
                update relevantSASList;
                
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Preliminary processing complete, the wizard is now ready for use '));
                preliminaryProcessingComplete = true;
            } else {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'Warning, no valid Student Assesment Section were found'));
                preliminaryProcessingComplete = false;
            }
            totalAvailableRecords = relevantSASList.size();
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Problem encountered during Preliminary process: ' +e));
            preliminaryProcessingComplete = false;
        }
    }
    
    public PageReference nextToPage0B() {
        PageReference page0B = Page.AAS_PreFinalAdjustmentPage0B;
        return page0B;
    }
    
    public PageReference autoRedirectB() {
    	    	
    	// Check for the presence of SAS records that have the flag marked, if any exist stop and warn the user if they want to start fresh or start anyway as is 
    	List<AAS_Student_Assessment_Section__c> markedSAS = AAS_ToolBox.getAllRelevantSAS(initialCA, ' Id',  ' AND AAS_MarkedForAdjustment__c = true AND RecordTypeID = \'' +examAssessment_recordTypeID+ '\'');    	
    	
    	if (markedSAS.size() > 0) {
    		// existing marked SAS present
    		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'Warning, previous mark adjustment record/s found, please select New Session to clear previous selection and start a new one or choose Continue Old Session to continue previous session.'));
    		return null;
    	} else {
    	    return nextToPage1();
    	    /**
    	    if (AAS_ToolBox.updateSAS(initialCA.Id, examAssessment_recordTypeID)) {
    	    	return nextToPage1();
            } else {
            	return null;
            }
            **/
    	}    	    	
    }
    
    public PageReference nextToPage1New() {
    	
    	List<AAS_Student_Assessment_Section__c> markedSAS = AAS_ToolBox.getAllRelevantSAS(initialCA, ' Id, AAS_MarkedForAdjustment__c', ' AND AAS_MarkedForAdjustment__c = true AND RecordTypeID = \'' +examAssessment_recordTypeID+ '\'');
    	AAS_ToolBox.resetMarkedSAS(markedSAS);
    	update markedSAS;
    	
    	// PageReference page1 = Page.AAS_PreFinalAdjustmentPage1;
    	return nextToPage1();
    }
    
    public PageReference nextToPage1() {
    	PageReference page1 = Page.AAS_PreFinalAdjustmentPage1;
    	return page1;
    }
    
    public PageReference nextToPage2() {
    	
		saveCurrentRecord();
		
		con = null;
		
		String optionalQueryString = ' AND AAS_MarkedForAdjustment__c = true AND RecordTypeID = \'' +examAssessment_recordTypeID+ '\'';
        List<AAS_Student_Assessment_Section__c> sasList = AAS_ToolBox.getAllRelevantSAS(initialCA, queryFieldEdit, optionalQueryString);
        if (debug) system.debug('sasList: ' +sasList);
        
        if (sasList.size() > EDIT_TOTAL_SIZE) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING, 'There are more than ' +EDIT_TOTAL_SIZE+ ' records selected for Adjustment, this Wizard can only perform adjustment to a maximum of ' +EDIT_TOTAL_SIZE+ ' records in one session'));
            return null;
        }
        
		con = new ApexPages.StandardSetController(sasList);
        con.setPageSize(EDIT_SIZE);
		sasUpdatedCount = sasList.size();
		
		AAS_ToolBox.calculateMarkAndPass(con.getRecords(), examPassingMark, modulePassingMark);
		con.save();

        PageReference page2 = Page.AAS_PreFinalAdjustmentPage2;
        return page2;
    }
    
    public PageReference nextToPage3() {
    	
    	if (saveAllSelectedRecord()) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, sasUpdatedCount + ' Record/s updated'));
            
            AAS_ToolBox.updateCASequence(initialCA);
            
    		PageReference page3 = Page.AAS_PreFinalAdjustmentPage3;
    		return page3;
    	} else {
    		return null;
    	} 	
    }
    
    public void cleanAssets() {
    	
    	String optionalQueryString = ' AND AAS_MarkedForAdjustment__c = true AND RecordTypeID = \'' +examAssessment_recordTypeID+ '\''; 
        List<AAS_Student_Assessment_Section__c> markedSAS = AAS_ToolBox.getAllRelevantSAS(initialCA, queryFieldEdit, optionalQueryString);
        
        if (debug) system.debug('markedSAS: ' +markedSAS);
        
    	AAS_ToolBox.resetMarkedSAS(markedSAS);
        update markedSAS;
    }

    //-------- Navigation Methods --------//
    
    //######## Pagination Methods ########//
    
    // Search Page
    public void Previous() {
    	saveCurrentRecord();
        con.previous();
    }
    
    public void Next() {
    	saveCurrentRecord();
        con.next();        
    }
    
    public void Beginning() {
    	saveCurrentRecord();
        con.first();
    }
    
    public void End() {
    	saveCurrentRecord();
        con.last();
    }
    
    public void saveAndNext() {
        saveCurrentRecord();
        con.next();
        AAS_ToolBox.calculateMarkAndPass(con.getRecords(), examPassingMark, modulePassingMark);
    }
    
    public PageReference saveAndDone() {
        saveCurrentRecord();
        try {
    	    cleanAssets();
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, sasUpdatedCount + ' Record/s updated'));
            AAS_ToolBox.updateCASequence(initialCA);
    		PageReference page3 = Page.AAS_PreFinalAdjustmentPage3;
    		return page3;
    	} catch (Exception e) {
    	    system.debug('Problem occurred during asset cleaning: ' +e);
    	    return null;
    	}
    }
    
    // Navigation methods used specifically for the Edit Page that has to perform recalculation on the Mark Value before populating the table
    public void EditPrevious() {
        saveCurrentRecord();
        con.previous();
        AAS_ToolBox.calculateMarkAndPass(con.getRecords(), examPassingMark, modulePassingMark);
        // saveCurrentRecord();
    }
    
    public void EditNext() {
        saveCurrentRecord();
        con.next();
        AAS_ToolBox.calculateMarkAndPass(con.getRecords(), examPassingMark, modulePassingMark);
        // saveCurrentRecord();
    }
    
    public void EditBeginning() {
        saveCurrentRecord();
        con.first();
        AAS_ToolBox.calculateMarkAndPass(con.getRecords(), examPassingMark, modulePassingMark);
        // saveCurrentRecord();
    }
    
    public void EditEnd() {
        saveCurrentRecord();
        con.last();
        AAS_ToolBox.calculateMarkAndPass(con.getRecords(), examPassingMark, modulePassingMark);
        // saveCurrentRecord();
    }
    
    public Boolean hasNext {        
        get {
            return con.getHasNext();
        }
        set;
    }

    public Boolean hasPrevious {
        get {
            return con.getHasPrevious();
        }
        set;
    }
    
    public Integer pageNumber {
        get {
            return con.getPageNumber();
        }
        set;
    }
    
    public Integer pageTotal {
        get {
            Decimal pageTotal = (con.getResultSize()/ (Decimal)EDIT_SIZE).round(System.RoundingMode.CEILING);
            return pageTotal.intValue();
        }
        set;
    }
    
    public Integer resultTotal {
        get {
            return con.getResultSize();
        }
        set;
    }
    
    public Integer searchLimit {
        get{
            return SEARCH_TOTAL_SIZE;
        }
        set;
    }
    
    
    
    /**
    // Edit Page
    public void PreviousSelected() {
    	conSelected.previous();
    }
    
    public void NextSelected() {
    	conSelected.next();
    }
    
    public void BeginningSelected() {
    	con.first();
    }
    
    public void EndSelected() {
    	con.last();
    }
    
    public Boolean hasNextSelected {
    	get {
    		return conSelected.getHasNext();
    	}
    	set;
    }
    
    public Boolean hasPreviousSelected {
    	get {
    		return conSelected.getHasPrevious();
    	}
    	set;
    }
    **/
    //-------- Pagination Methods --------//
}