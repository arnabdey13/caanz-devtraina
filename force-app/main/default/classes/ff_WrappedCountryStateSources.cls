public with sharing class ff_WrappedCountryStateSources {

    private List<CountryPicklistSetting> countries;

    public ff_WrappedCountryStateSources() {
        StaticResource src = [SELECT Body FROM StaticResource where Name = 'AddressPicklists'];
        countries = parseCountriesMetaData(src.Body.toString());
        countries.sort();
    }

    // this method the api for this class. returns two picklists, one for coutries and one for states
    public Map<String, ff_WrapperSource> getSources() {
        List<ff_Service.CustomWrapper> countryWrapperSource = new List<ff_Service.CustomWrapper>();
        List<ff_Service.CustomWrapper> stateWrapperSource = new List<ff_Service.CustomWrapper>();

        for (CountryPicklistSetting cps: countries) {
            ff_Service.PicklistOption countryWrapper = new ff_Service.PicklistOption();
            countryWrapper.label = cps.label;
            countryWrapper.value = cps.countryCode;
            countryWrapperSource.add(countryWrapper);

            for (List<String> state : cps.states) {
                ff_Service.PicklistOption stateWrapper = new ff_Service.PicklistOption();
                stateWrapper.label = state[1];
                stateWrapper.value = state[0];
                stateWrapper.parentValues = new Set<String>{
                        cps.countryCode
                };
                stateWrapperSource.add(stateWrapper);
            }
        }

        return new Map<String, ff_WrapperSource>{
                'countries' => new WrappedSource(countryWrapperSource),
                'states' => new WrappedSource(stateWrapperSource)
        };
    }

    private class WrappedSource extends ff_WrappedPicklistSource {

        private final List<ff_Service.CustomWrapper> wrappers;
        private WrappedSource(List<ff_Service.CustomWrapper> wrappers) {
            this.wrappers = wrappers;
        }

        public override List<ff_Service.CustomWrapper> getWrappers() {
            return wrappers;
        }
    }

    private List<CountryPicklistSetting> parseCountriesMetaData(String toParse) {
        DOM.Document doc = new DOM.Document();
        doc.load(toParse);
        DOM.XMLNode root = doc.getRootElement();
        List<CountryPicklistSetting> countries = new List<CountryPicklistSetting>();
        for (DOM.XmlNode countryNode : root.getChildElements()[0].getChildElements()) {
            CountryPicklistSetting country = translateXmlToCountry(countryNode);
            if (country != null) { // inactive countries are returned as nulls
                countries.add(translateXmlToCountry(countryNode));
            }
        }
        return countries;
    }

    // Parse through the XML, determine the author and the characters
    private CountryPicklistSetting translateXmlToCountry(DOM.XmlNode countryNode) {
        CountryPicklistSetting country = new CountryPicklistSetting();

        List<SortableList> unsortedStates = new List<SortableList>();

        for (DOM.XmlNode child : countryNode.getChildElements()) {
            if (child.getName() == 'active') {
                if (child.getText() == 'false') {
                    return null;
                }
            } else if (child.getName() == 'label') {
                country.label = child.getText();
            } else if (child.getName() == 'isoCode') {
                country.countryCode = child.getText();
            } else if (child.getName() == 'states') {

                List<String> state = new List<String>(2);
                for (DOM.XmlNode stateNode : child.getChildElements()) {
                    if (stateNode.getName() == 'active') {
                        if (stateNode.getText() == 'false') {
                            state = null;
                            break;
                        }
                    } else if (stateNode.getName() == 'integrationValue') {
                        state.set(0, stateNode.getText());
                    } else if (stateNode.getName() == 'label') {
                        state.set(1, stateNode.getText());
                    }
                }
                if (state != null) { // inactive states are null
                    unsortedStates.add(new SortableList(1, state));
                }
            }
        }

        unsortedStates.sort();
        for (SortableList s : unsortedStates) {
            country.addState(s.data);
        }

        return country;
    }

    // util class for sorting lists of Objects. you provide an index to a String element in the list for sorting
    private class SortableList implements Comparable {
        private Integer sortIndex;
        private List<String> data;
        private SortableList(Integer index, List<String> data) {
            this.sortIndex = index;
            this.data = data;
        }
        public Integer compareTo(Object compareTo) {
            SortableList other = (SortableList) compareTo;
            String thisVal = this.data.get(sortIndex);
            String otherVal = other.data.get(sortIndex);
            return thisVal.compareTo(otherVal);
        }
    }

    private class CountryPicklistSetting implements Comparable {

        private String countryCode;
        private String label;
        // states are a sorted list of 2 element String lists. First element = stateCode, Second = label;
        private List<List<String>> states = new List<List<String>>();

        public CountryPicklistSetting() {
        }

        public CountryPicklistSetting(String code, String label) {
            this.countryCode = code;
            this.label = label;
        }

        public Integer compareTo(Object compareTo) {
            CountryPicklistSetting other = (CountryPicklistSetting) compareTo;
            return this.label.compareTo(other.label);
        }

        private void addState(List<String> state) {
            states.add(state);
        }

    }

}