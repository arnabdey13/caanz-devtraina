/***********************************************************************************************************************************************************************
Name: Account_CPDMemberHandler_Test 
============================================================================================================================== 
Purpose: This class contains code related to Unit Testing and test coverage of class Account_CPDMemberHandler 
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR           DATE          DETAIL     Description 
1.0        Rama Krishna    19/02/2019    Created     This class contains code related to Unit Testing and test coverage of class Account_CPDMemberHandler 
                                                             
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/
@isTest
public class Account_CPDMemberHandler_Test {
    private static List<String> specialistHoursCodes;   
    static RecordType personAccountRecordType = [SELECT Id, IsPersonType FROM RecordType WHERE Name = 'Member' and SObjectType = 'Account' and IsPersonType = true];   
    static Id personAccountRecordTypeId = personAccountRecordType.Id;
       
   //create test data for checking the functionality to set Specialisations for CPD Summary records.    
    static{
        specialistHoursCodes = new List<String>{
            'Type F - Financial Planning Specialist',
            'Type A - Registered Company Auditor',
            'Type L - Registered Company Liquidator',
            'Type T - Registered Tax Agent',
            'Type AF - Australian Financial Services Licensee (AFSL)', 
            'Type B - Registered Trustee in Bankruptcy'
        };
    }
    
    /******create Test Data using testsetup annotation for Objects Account,Professional Conduct,
       Professional Conduct Tribunal,Appeal Tribunal,Fine Cost
    *******/   
    @testSetup static void createTestData() {
        //test_insertTestData();
    }
    static void test_insertTestData(){             
        List<Conduct_Support__c> lstConductSupport = new List<Conduct_Support__c>();     
        Conduct_Support__c cs = new Conduct_Support__c();
        Conduct_Support__c cs1 = new Conduct_Support__c();
        Professional_Conduct_Tribunal__c pct = new Professional_Conduct_Tribunal__c(); 
        Fine_Cost__c fc = new Fine_Cost__c();
        
        Account acc = TestObjectCreator.createPersonAccount();
        acc.Designation__c = 'ACA';
        acc.Membership_Type__c = 'Member';
        acc.FP_Specialisation__c = true;
        acc.Registered_Company_Auditor__c = true;
        acc.Registered_Company_Liquidator__c = true;
        acc.Registered_Tax_Agent__c = true;
        acc.RollUpFromProfessionalConductAndFineCost__c=true;
        acc.Membership_Approval_Date__c = Date.today().addDays(1); 
        acc.Create_Portal_User__c = true;
        acc.Membership_Class__c='Provisional'; 
        acc.Communication_Preference__c= 'Home Phone';
        acc.PersonHomePhone= '1234';
        acc.PersonOtherStreet= '83 Saggers Road';
        acc.PersonOtherCity='JITARNING';
        acc.PersonOtherState='Western Australia';
        acc.PersonOtherCountry='Australia';
        acc.PersonOtherPostalCode='6365';  
        
        
          
        //create test data for Person Account    
        insert acc;               
        
        cs.Member__c=acc.Id;
        //Set the RecordType to AU which updates the country to AU
        cs.RecordTypeId=ApexUtil.getRecordTypeId(cs, 'AU');
        cs.Overall_Status__c='Open';
        cs.Complaint__c=false;
        lstConductSupport.add(cs);      
        cs1.Member__c=acc.Id;
        //Set the RecordType to NZ which updates the country to NZ
        cs1.RecordTypeId=ApexUtil.getRecordTypeId(cs1, 'NZ');
        cs1.Overall_Status__c='Open';
        cs1.Complaint__c=true;
        lstConductSupport.add(cs1);  
        // create test data for Professional Conduct   
        insert lstConductSupport;     
        
        pct.Professional_Conduct__c=lstConductSupport[0].Id;
        //create test data for Professional Conduct Tribunal
        insert pct; 

        Appeal_Tribunal__c at = new Appeal_Tribunal__c();
        at.Professional_Conduct__c = lstConductSupport[0].Id;
        //create test data for Appeal Tribunal
        insert at;          
        
        fc.Professional_Conduct__c=lstConductSupport[0].Id;
        fc.Professional_Conduct_Tribunal__c=pct.Id;      
        fc.Cost_Recovery_Status__c='Unpaid';   
        //create test data for Fine Cost  
        insert fc;               
    }
//******************************************************************************************
//                             testMethods
//******************************************************************************************

    //This returns CPD Summary records which are created after Account creation in order to check the Test results
    private static List<CPD_Summary__c> getSummaries(){
        return [
            SELECT Id, Contact_Student__c, Triennium_Start_Date__c, Triennium_End_Date__c,
            Specialisation_1__c, Specialisation_2__c, Specialisation_3__c, Specialisation_4__c,
            Specialisation_Hours_1__c, Specialisation_Hours_2__c, Specialisation_Hours_3__c, 
            Specialisation_Hours_4__c
            FROM CPD_Summary__c
        ];
    }
   
    //Test method which creates a Member Account and then checks for summary records with specialisations  
    /*static testMethod void test_createSummmaryRecordsWithSpecialisations() {
        Test.startTest();
        Profile profile1 = [Select Id from Profile where name = 'CAANZ Business Management User'];
        System.debug('What is the profile id ' + profile1);
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        User u = new User( UserRoleId = portalRole.Id,
                            ProfileId = profile1.Id,
                            Username = 'testtermsconditions1234423@charteredaccountantsanz.com',
                            Alias = 'batman',
                            Email='testtermsconditions1234423@charteredaccountantsanz.com',
                            EmailEncodingKey='UTF-8',
                            Firstname='Bruce',
                            Lastname='Wayne',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_NZ',
                            TimeZoneSidKey='Pacific/Auckland');
        insert u; 
        //run as user to test code to perform user level testing                         
        System.runas(u){
            Account acct = [select Id,Registered_Company_Auditor__c,Registered_Company_Liquidator__c,Registered_Trustee_in_Bankruptcy__c from Account limit 1];              
            acct.Registered_Company_Auditor__c = false;
            acct.Registered_Company_Liquidator__c = false;
            acct.Registered_Trustee_in_Bankruptcy__c = true;
            update acct;                               
            //get summary records in order to use them in assertions
            List<CPD_Summary__c> summaries = getSummaries(); system.debug(summaries);
            for(CPD_Summary__c summary : summaries){
                system.assertEquals(specialistHoursCodes[0], summary.Specialisation_1__c);
                system.assertEquals(specialistHoursCodes[1], summary.Specialisation_2__c);
                system.assertEquals(specialistHoursCodes[2], summary.Specialisation_3__c);
                system.assertEquals(specialistHoursCodes[3], summary.Specialisation_4__c);
            }
            Test.stopTest();
        }  
    }*/
   
    //test method which inserts bulk test data and checks the functionality  
    static testMethod void test_createSummaryRecordsBulk() {
        Profile profile1 = [Select Id from Profile where name = 'CAANZ Business Management User'];
        System.debug('What is the profile id ' + profile1);
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        User u = new User( UserRoleId = portalRole.Id,
                            ProfileId = profile1.Id,
                            Username = 'testtermsconditions1234423@charteredaccountantsanz.com',
                            Alias = 'batman',
                            Email='testtermsconditions1234423@charteredaccountantsanz.com',
                            EmailEncodingKey='UTF-8',
                            Firstname='Bruce',
                            Lastname='Wayne',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_NZ',
                            TimeZoneSidKey='Pacific/Auckland');
        insert u; 
        //run as user to test code to perform user level testing                         
        System.runas(u){
        
        List<Conduct_Support__c> lstConductSupport = new List<Conduct_Support__c>();     
        Conduct_Support__c cs = new Conduct_Support__c();
        Conduct_Support__c cs1 = new Conduct_Support__c();
        Professional_Conduct_Tribunal__c pct = new Professional_Conduct_Tribunal__c(); 
        Fine_Cost__c fc = new Fine_Cost__c();
        
        Test.startTest();      
        List<Account> accList = new List<Account>();
        accList  = TestObjectCreator.createPersonAccountBulk(20,personAccountRecordTypeId);
        for(Account acc:accList){            
            acc.Designation__c = 'Affiliate CAANZ';
            acc.Membership_Type__c = 'Member';
            acc.RollUpFromProfessionalConductAndFineCost__c=true;  
            acc.Communication_Preference__c= 'Home Phone';
            acc.PersonHomePhone= '1234';
            acc.PersonOtherStreet= '83 Saggers Road';
            acc.PersonOtherCity='JITARNING';
            acc.PersonOtherState='Western Australia';
            acc.PersonOtherCountry='Australia';
            acc.PersonOtherPostalCode='6365';        
        }
        
        insert accList;  
        Test.stopTest();
        
        cs.Member__c=accList[0].Id;
        //Set the RecordType to AU which updates the country to AU
        cs.RecordTypeId=ApexUtil.getRecordTypeId(cs, 'AU');
        cs.Overall_Status__c='Open';
        cs.Complaint__c=false;
        lstConductSupport.add(cs);      
        cs1.Member__c=accList[0].Id;
        //Set the RecordType to NZ which updates the country to NZ
        cs1.RecordTypeId=ApexUtil.getRecordTypeId(cs1, 'NZ');
        cs1.Overall_Status__c='Open';
        cs1.Complaint__c=true;
        lstConductSupport.add(cs1);  
        // create test data for Professional Conduct   
        insert lstConductSupport;     
        
        pct.Professional_Conduct__c=lstConductSupport[0].Id;
        //create test data for Professional Conduct Tribunal
        insert pct; 

        Appeal_Tribunal__c at = new Appeal_Tribunal__c();
        at.Professional_Conduct__c = lstConductSupport[0].Id;
        //create test data for Appeal Tribunal
        insert at;          
        
        fc.Professional_Conduct__c=lstConductSupport[0].Id;
        fc.Professional_Conduct_Tribunal__c=pct.Id;      
        fc.Cost_Recovery_Status__c='Unpaid';   
        //create test data for Fine Cost  
        insert fc;
        
        
            Account acct = [select Id,Registered_Company_Auditor__c,Registered_Company_Liquidator__c,Registered_Trustee_in_Bankruptcy__c from Account limit 1];              
            acct.Registered_Company_Auditor__c = false;
            acct.Registered_Company_Liquidator__c = false;
            acct.Registered_Trustee_in_Bankruptcy__c = true;
            update acct;                               
            //get summary records in order to use them in assertions
            List<CPD_Summary__c> summaries = getSummaries(); system.debug(summaries);
            //system.assertEquals(true, summaries.size()>0);
            for(CPD_Summary__c summary : summaries){
                /*system.assertEquals(specialistHoursCodes[0], summary.Specialisation_1__c);
                system.assertEquals(specialistHoursCodes[1], summary.Specialisation_2__c);
                system.assertEquals(specialistHoursCodes[2], summary.Specialisation_3__c);
                system.assertEquals(specialistHoursCodes[3], summary.Specialisation_4__c);*/
            }            
        } 
        
        //get the test summary records to check the assertion
        //List<CPD_Summary__c> summaries = getSummaries();
        //system.assertEquals(true, summaries.size()>0);
    }   
    
}