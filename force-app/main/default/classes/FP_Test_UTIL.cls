@isTest
public class FP_Test_UTIL {

    public static FP_Competency_Area__c createCompetencyArea(String testKey, Boolean active, String description, Boolean displayOnly) {
        FP_Competency_Area__c ca = new FP_Competency_Area__c();	
        ca.FP_Active__c = true;
        ca.FP_Description__c = testKey + ' ' + description;
        ca.FP_Display_Only__c = false;
        return ca;
    }
    
    public static Account createAccount(String testKey, Id recordTypeID, String name) {
        Account acc = new Account();        
        acc.RecordTypeId = recordTypeID;
        acc.Name = testKey + ' ' + name;
        acc.BillingStreet = 'Test Street';
        return acc;
    }
    
    public static Contact createContact(String testKey, String firstName, String LastName, Id accountId) {
        Contact cont = new Contact();
        cont.FirstName = firstName;
        cont.LastName = testKey + ' ' + lastName;
        cont.AccountId = accountId;
        return cont;
    }
    
    public static FP_Relevant_Competency_Area__c createRCA(Id caId, String rcaType, Id subjectId) {
        FP_Relevant_Competency_Area__c rca = new FP_Relevant_Competency_Area__c();
        rca.FP_Competency_Area__c = caId;
        rca.FP_Type__c = rcaType;
        rca.FP_Subject__c = subjectId;
        return rca;
    }
    
    public static Case createCase(Id contactId, Id recordTypeID, String resolution, Id rcaId) {        
        Case cs = new Case();
        cs.ContactId = contactId;
        cs.RecordTypeId = recordTypeID;
        cs.Resolution__c = resolution;
        cs.FP_Requested_Competency_Area__c = rcaId;
        return cs;
    }
    
    public static LuanaSMS__Student_Program_Subject__c createSPS(Id subjectID, Id contactID, String outcomeNational) {        
        LuanaSMS__Student_Program_Subject__c sps = new LuanaSMS__Student_Program_Subject__c();
        sps.LuanaSMS__Subject__c = subjectID;
        sps.LuanaSMS__Contact_Student__c = contactID;
        sps.LuanaSMS__Outcome_National__c = outcomeNational;
        return sps;
    }
    
	public static Map<String, SObject> assetGenerator(String testKey) {
        
        Map<String, SObject> assetMap = new Map<String, SObject>();        
        
        // Record Type
        List<RecordType> requiredRecordTypes = [SELECT Id, SObjectType, DeveloperName FROM RecordType WHERE SObjectType = 'Account' OR SObjectType = 'Case'];
        for (RecordType rt : requiredRecordTypes) {
            if (rt.SObjectType == 'Account' && rt.DeveloperName == 'Business_Account') assetMap.put('businessAccountID', rt);
        	if (rt.SObjectType == 'Case' && rt.DeveloperName == 'Request_Competency_Area') assetMap.put('rcaCaseID', rt);
        }
        
        // Competency Area
        List<FP_Competency_Area__c> caList = new List<FP_Competency_Area__c>();
        
        FP_Competency_Area__c ca1 = createCompetencyArea(testKey, true, 'Description 1', false);
        insert ca1;
        
        FP_Competency_Area__c ca2 = createCompetencyArea(testKey, true, 'Description 2', true);        
        ca2.FP_Parent_Competency_Area__c = ca1.Id;
        caList.add(ca2);
        
        insert caList;
        assetMap.put('ca1', ca1);
        assetMap.put('ca2', ca2);
        
        // PersonAccount
        List<Account> accountList = new List<Account>();
            
        Account acc1 = createAccount(testKey, assetMap.get('businessAccountID').Id, 'Account 1');        
        accountList.add(acc1);
        
        insert accountList;
        assetMap.put('acc1', acc1);
        
        // Contact
        List<Contact> contactList = new List<Contact>();
        
        Contact cont1 = createContact(testKey, 'Tester1', 'Person', acc1.Id);
        contactList.add(cont1);
        
        insert contactList;
        assetMap.put('cont1', cont1);
        
        // Subject
        List<LuanaSMS__Subject__c> subjectList = new List<LuanaSMS__Subject__c>();
        
        LuanaSMS__Subject__c subject1 = new LuanaSMS__Subject__c();
        subjectList.add(subject1);
            
        insert subjectList;
        assetMap.put('subject1', subject1);
        
        // Relevant Competency Area
        List<FP_Relevant_Competency_Area__c> rcaList = new List<FP_Relevant_Competency_Area__c>();
        
        FP_Relevant_Competency_Area__c rca1 = createRCA(ca1.Id, 'Requires', subject1.Id);        
        rcaList.add(rca1);
        
        insert rcaList;
        assetMap.put('rca1', rca1);
        
        return assetMap;
	}
}