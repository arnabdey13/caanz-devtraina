/*
    Developer: WDCi (KH)
    Development Date: 26/04/2016
    Task #: employee enrolment view
*/

public without sharing class luana_EnmployeeEnrolmentViewContorller {
    
    public Id custCommConId {get; private set;}
    public Id custCommAccId {get; private set;}
    
    private Id selectedSPId;
    private String spURLParamValue = 'spid';
    
    public List<Payment_Token__c> paymentTokens {get; private set;}
    public String selectedPTId {set; get;}
    
    public LuanaSMS__Student_Program__c workingStudentProgram {set; get;}
    
    public luana_EnmployeeEnrolmentViewContorller(){
        
        luana_CommUserUtil commUserUtil = new luana_CommUserUtil(UserInfo.getUserId()); 
        this.custCommConId = commUserUtil.custCommConId;       
        this.custCommAccId = commUserUtil.custCommAccId;
        
        paymentTokens = new List<Payment_Token__c>();
        
        if(ApexPages.currentPage().getParameters().containsKey(spURLParamValue)){
            selectedSPId = ApexPages.currentPage().getParameters().get(spURLParamValue);
            
            workingStudentProgram = getRelatedSP();
        }
        
    }
    
    //Fix for issue LCA-433, move the validEmployer check to public
    public static boolean validEmployer {get; private set;}
    private LuanaSMS__Student_Program__c getRelatedSP(){
        
        try{
            
            List<LuanaSMS__Student_Program__c> selecetedSP = [Select Id, Name, Opt_In__c, LuanaSMS__Enrolment_Date__c, Paid__c, LuanaSMS__Contact_Student__c, LuanaSMS__Contact_Student__r.AccountId,
                                                        LuanaSMS__Course__c, LuanaSMS__Course__r.Name, LuanaSMS__Course__r.Description__c,
                                                        (select id, Name, Code__c, Date_Used__c, Employer__c, Employer__r.Name, Revoke__c, Employer__r.BillingCity from Payment_Tokens__r),
                                                        (select id, LuanaSMS__Subject__r.Name, Completed__c, Score__c, Result__c, Merit__c from LuanaSMS__Student_Program_Subjects__r)
                                                        from LuanaSMS__Student_Program__c Where Id =: selectedSPId and RecordType.DeveloperName = 'Accredited_Module' and Opt_In__c = true limit 1];
            
            LuanaSMS__Student_Program__c studentProgram = selecetedSP[0];
            
            List<Employment_History__c> studentEmpHistories = luana_EmploymentUtil.getUserEmploymentHistory(studentProgram.LuanaSMS__Contact_Student__r.AccountId);
            List<Employment_History__c> userEmpHistories = luana_EmploymentUtil.getUserActiveEmploymentHistory(custCommAccId);
            
            validEmployer = false;
            
            for(Employment_History__c userEmpHistory : userEmpHistories){
                for(Employment_History__c studentEmpHistory : studentEmpHistories){
                    if(userEmpHistory.Employer__c == studentEmpHistory.Employer__c){
                        validEmployer = true;
                    }
                }
            }
            
            if(validEmployer){
                for(LuanaSMS__Student_Program__c sp: selecetedSP){
                    for(Payment_Token__c pt: sp.Payment_Tokens__r){
                        paymentTokens.add(pt);
                    }
                }                                            
                
                return studentProgram;
            } else {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'You do not have the permission to view the record. Please try again later or contact our support if the problem persists.'));
            }
            
        }catch(Exception exp){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, 'You do not have the permission to view the record. Please try again later or contact our support if the problem persists.'));
        }
        return null;
    }
    
}