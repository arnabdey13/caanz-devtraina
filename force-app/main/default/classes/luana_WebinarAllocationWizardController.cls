/*
    Developer: WDCi (Lean)
    Date: 15/Aug/2016
    Task #: LCA-802 Luana implementation - Wizard for webinar session allocation
*/

public without sharing class luana_WebinarAllocationWizardController {
    
    public LuanaSMS__Course__c course {private set; public get;}
    public List<luana_WorkshopCourse> workshopCourses {private set; public get;}
    
    public boolean enabled {private set; public get;}
    
    public List<luana_ErrorRecordWrapper> attendanceErrorRecords {private set; get;}
    
    public Map<String, List<LuanaSMS__Student_Program__c>> courseStudentPrograms {private set; get;}
    
    public integer successCount {private set; get;}
    public integer errorCount {private set; get;}
    
    private String targetCourseId {set; get;}
    private Luana_Extension_Settings__c luanaReportConfig {set; get;}
    
    private Map<String, String> sessionNames;
    
    public luana_WebinarAllocationWizardController (ApexPages.StandardController controller) {
    	
    	attendanceErrorRecords = new List<luana_ErrorRecordWrapper>();
        successCount = 0;
        errorCount = 0;
        
        targetCourseId = getCourseIdFromUrl();
        
        init();
        
    }
    
    private String getCourseIdFromUrl(){
        if(ApexPages.currentPage().getParameters().containsKey('id')){
            return ApexPages.currentPage().getParameters().get('id');
            
        }
        
        return null;
    }
    
    public void init(){
        enabled = false;
        
        sessionNames = new Map<String, String>();
        courseStudentPrograms = new Map<String, List<LuanaSMS__Student_Program__c>>();
        luanaReportConfig = Luana_Extension_Settings__c.getValues('Webinar Allocation Report Params');
        
        //even though we know there is only 1 course, we still use list here for future enhancement and more dynamic
     	workshopCourses = new List<luana_WorkshopCourse>(); 
     	
        if(luanaReportConfig != null){
            if(targetCourseId != null){
                try{
                    //get course name, also work as validation to make sure course is valid
                    course = [select Id, Name from LuanaSMS__Course__c where Id =: targetCourseId];
                    
                    for(LuanaSMS__Student_Program__c studProg : Database.query(getStudentProgramQuery())){
                    	if(studProg.LuanaSMS__Attendances__r.isEmpty()){
                    		if(courseStudentPrograms.containsKey(studProg.LuanaSMS__Course__c)){
                                List<LuanaSMS__Student_Program__c> existingStudProgs = courseStudentPrograms.get(studProg.LuanaSMS__Course__c);
                                existingStudProgs.add(studProg);
                                
                                courseStudentPrograms.put(studProg.LuanaSMS__Course__c, existingStudProgs);
                                
                            } else {
                                List<LuanaSMS__Student_Program__c> newStudProgs = new List<LuanaSMS__Student_Program__c>();
                                newStudProgs.add(studProg);
                                
                                courseStudentPrograms.put(studProg.LuanaSMS__Course__c, newStudProgs);
                                
                            }
                    	}
                    }
                    
                    if(!courseStudentPrograms.isEmpty()){
                    	
                    	//prepare empty key for session without topic_key__c
                        String emptyGroupHash = system.now().format('yyyyMMddHHmmssSSS');
                        integer emptyGroupCounter = 1;
                        
                    	//we don't multiple courses here, so single level is good enough to handle the grouping
                        luana_WorkshopCourse workshopCourse = new luana_WorkshopCourse(course.Id, course.Name, course.Name, new List<luana_WorkshopSessionGroup>(), courseStudentPrograms.get(course.Id).size());
                        
                        Map<String, luana_WorkshopSessionGroup> sessionGroupsByKey = new Map<String, luana_WorkshopSessionGroup>();
                        
                        boolean hasWebinarSession = false;
                        
                    	for(LuanaSMS__Course_Session__c courseSession : Database.query(getCourseSessionQuery())){
                    		integer maxCap = courseSession.LuanaSMS__Session__r.Maximum_Capacity__c == null ? null : courseSession.LuanaSMS__Session__r.Maximum_Capacity__c.intValue();
                            integer xSeries = 1;
                            
                            List<LuanaSMS__Course_Session__c> sList = new List<LuanaSMS__Course_Session__c>();
                            sList.add(courseSession);
                            
                            //since virtual class session is individual, we force the session to be in their own group here
                            String groupKey = emptyGroupHash + '_' + emptyGroupCounter;
                            emptyGroupCounter ++;
                            
                            luana_WorkshopSessionGroup newWsg = new luana_WorkshopSessionGroup(workshopCourse.id, (courseSession.LuanaSMS__Session__r.Topic_Key__c != null ? courseSession.LuanaSMS__Session__r.Topic_Key__c : null), (courseSession.LuanaSMS__Session__r.Topic_Key__c != null ? courseSession.LuanaSMS__Session__r.Topic_Key__c : 'Empty Group'), sList, maxCap, maxCap, xSeries, (courseSession.LuanaSMS__Session__r.LuanaSMS__Number_of_enrolled_students__c >= courseSession.LuanaSMS__Session__r.Maximum_Capacity__c ? false : true), false);
                            
                            sessionGroupsByKey.put(groupKey, newWsg);
                            
                            System.debug('****sList: ' + sList);
                            
                            hasWebinarSession = true;
                    	}
                    	
                    	if(!hasWebinarSession){
                    		addPageMessage(ApexPages.severity.INFO, 'No webinar session available in this course. Please use the VC Import wizard to import the sessions from Adobe Connect.');
                    	} else {
                    		//add all existing session list
	                        workshopCourse.sessionGroups.addAll(sessionGroupsByKey.values());
	                        workshopCourses.add(workshopCourse);
	                        enabled = true;
                    	}
                    	
                    	
                        
                    } else {
                        addPageMessage(ApexPages.severity.INFO, 'No outstanding enrolment to be allocated for this course.');
                    }
                    
                } catch(Exception exp){
                	addPageMessage(ApexPages.severity.ERROR, 'Error retrieving course. Please try again later or contact your system administrator. Error: ' + exp.getMessage() + ' - ' + exp.getStackTraceString());
                }
            } else {
                addPageMessage(ApexPages.severity.ERROR, 'Invalid course. Please try again later or contact your system administrator.');
            }
        } else {
            addPageMessage(ApexPages.severity.ERROR, 'Missing configuration \'Webinar Allocation Report Params\'. Please try again later or contact your system administrator.');
        }
        
    }
    
    public PageReference doSave(){
    	
    	List<LuanaSMS__Attendance2__c> newWebinarAttendance = new List<LuanaSMS__Attendance2__c>();
    	
    	attendanceErrorRecords = new List<luana_ErrorRecordWrapper>();
    	
    	for(luana_WorkshopCourse workshopCourse : workshopCourses){
        	integer studentSequence = 0;
        	//step#1: we assign student to existing session first with fill up
        	
	        for(luana_WorkshopSessionGroup wsg : workshopCourse.sessionGroups){
	        	
	        	if(wsg.fillUp && !wsg.isNew && courseStudentPrograms.containsKey(workshopCourse.id) && wsg.sessions != null && !wsg.sessions.isEmpty()){
	        		List<LuanaSMS__Student_Program__c> studentPrograms = courseStudentPrograms.get(workshopCourse.id);
	        		
	        		system.debug('wsg studentSequence :: ' + studentSequence);
	        		
	        		for(;studentSequence < studentPrograms.size();){
                    	LuanaSMS__Student_Program__c studentProg = studentPrograms.get(studentSequence);
                    	List<LuanaSMS__Attendance2__c> tempAttendances = new List<LuanaSMS__Attendance2__c>();
                    	
                    	 for(LuanaSMS__Course_Session__c courseSession : wsg.sessions){
                    	 	
                    	 	system.debug('wsg :: ' + wsg.groupKey + ' - ' + studentProg.Id);
                    	 	
                    	 	system.debug('wsg courseSession :: ' + courseSession.LuanaSMS__Session__r.LuanaSMS__Number_of_enrolled_students__c + ' - ' + courseSession.Id + ' - ' + courseSession.LuanaSMS__Session__c);
                    	 	
                            //step#1.1: we need to make sure that all the session in the series/group has enough seat to fill in
                            if((courseSession.LuanaSMS__Session__r.LuanaSMS__Number_of_enrolled_students__c + studentSequence) < courseSession.LuanaSMS__Session__r.Maximum_Capacity__c){
                                LuanaSMS__Attendance2__c newAttendance = new LuanaSMS__Attendance2__c();
                                //attendance mapping here
                                newAttendance.LuanaSMS__Contact_Student__c = studentProg.LuanaSMS__Contact_Student__c;
                                newAttendance.LuanaSMS__Session__c = courseSession.LuanaSMS__Session__r.Id;
                                newAttendance.LuanaSMS__Student_Program__c = studentProg.Id;
                                newAttendance.LuanaSMS__Type__c = 'Student';
                                newAttendance.LuanaSMS__Start_time__c = courseSession.LuanaSMS__Session__r.LuanaSMS__Start_Time__c;
                                newAttendance.LuanaSMS__End_time__c = courseSession.LuanaSMS__Session__r.LuanaSMS__End_Time__c;
                                
                                tempAttendances.add(newAttendance);
                                
                                sessionNames.put(courseSession.LuanaSMS__Session__r.Id, courseSession.LuanaSMS__Session__r.Name);
                                
                            } else {
                                break;
                            }
                        }
                        
                        if(wsg.sessions.size() == tempAttendances.size()){
                            newWebinarAttendance.addAll(tempAttendances);
                            
                            studentSequence ++;
                        } else {
                            break;
                        }
	        		}
	        		
	        		//added logic here to ensure that we always remove the allocated student program from the list and reset the sequence.
	        		//otherwise, the comparison for (# of Enrolled Students + Student Sequence) < session.Max Seat will always fail.
	        		//this is different from workshop allocation as webinar session has no group of session.
	        		for(integer i = 0; i < studentSequence; i++){
                		studentPrograms.remove(0); //Changed from i to 0
                	}
                	
                	studentSequence = 0;
	        	}
	        }
    	}
    	
    		Savepoint sp = Database.setSavepoint();
            
            try{
            	
            	Set<Id> succeededSessionIds = new Set<Id>();
                Set<Id> succeededStudentIds = new Set<Id>();
                Set<Id> failedStudentIds = new Set<Id>();
                
            	for(List<SObject> pagedNewAttendances : luana_ListUtil.getPageIterable(newWebinarAttendance, 600)){
                    Database.SaveResult[] saveResults = Database.insert(pagedNewAttendances, false);
                    
                    system.debug('saveResults without room ::' + saveResults);
                    
                    integer attCounter = 0;
                    for(Database.SaveResult sr : saveResults){
                        LuanaSMS__Attendance2__c resultAtt = (LuanaSMS__Attendance2__c)pagedNewAttendances.get(attCounter);
                        
                        system.debug('saveResult without room ::' + sr.isSuccess() + ' - ' + sr.getErrors());
                        
                        if(sr.isSuccess()){
                            succeededStudentIds.add(resultAtt.LuanaSMS__Contact_Student__c);
                            succeededSessionIds.add(resultAtt.LuanaSMS__Session__c);
                        } else {
                            luana_ErrorRecordWrapper errorRecord = new luana_ErrorRecordWrapper(null, resultAtt, 'Attendance', sr.getErrors()[0].getMessage());
                            attendanceErrorRecords.add(errorRecord);
                            
                            failedStudentIds.add(resultAtt.LuanaSMS__Contact_Student__c);
                        }
                        
                        attCounter ++;
                    }
                }
            	
            	successCount = succeededStudentIds.size();
                errorCount = failedStudentIds.size();
                
                if(successCount > 0){
                
                    String detailsMsg = '';
                    for(Id sessionId : succeededSessionIds){
                        detailsMsg += '<a href="' + luanaReportConfig.Value__c + String.valueOf(sessionId).substring(0, 15) + '" target="_blank">' + sessionNames.get(sessionId) + '</a><br/>';
                    }
                    
                    addPageMessage(ApexPages.severity.INFO, successCount + ' student program/s are allocated successfully. Please see the reports below for details.');
                    addPageMessage(ApexPages.severity.INFO, detailsMsg);
                }
                
                if(errorCount > 0){
                    addPageMessage(ApexPages.severity.ERROR, 'There are ' + errorCount + ' student program/s failed to be allocated.');
                }
                
                init();
                
            } catch(Exception exp){
                
                addPageMessage(ApexPages.severity.ERROR, 'Error creating webinar attendances. Please try again later or contact your system administrator. Error: ' + exp.getMessage() + ' - ' + exp.getStackTraceString());
                Database.rollback(sp);
                
            }
    	
    	return null;
    }
        
    private String getStudentProgramQuery(){
    	return 'select Id, Name, LuanaSMS__Course__c, LuanaSMS__Contact_Student__c, (select Id, Name from LuanaSMS__Attendances__r where RecordType.DeveloperName = \'' + luana_SessionConstants.RECORDTYPE_ATTENDANCE_VIRTUAL + '\') from LuanaSMS__Student_Program__c where LuanaSMS__Course__c =: targetCourseId and Paid__c = true and LuanaSMS__Status__c = \'In Progress\' order by Random_Number__c asc';
    }
    
    private String getCourseSessionQuery(){
    	return 'select Id, Name, LuanaSMS__Course__c, LuanaSMS__Session__r.Id, LuanaSMS__Session__r.Name, LuanaSMS__Session__r.LuanaSMS__Number_of_enrolled_students__c, LuanaSMS__Session__r.Maximum_Capacity__c, LuanaSMS__Session__r.LuanaSMS__Start_Time__c, LuanaSMS__Session__r.LuanaSMS__End_Time__c, LuanaSMS__Session__r.X_of_Series__c, LuanaSMS__Session__r.Day_of_Week__c, LuanaSMS__Session__r.Topic_Key__c, LuanaSMS__Session__r.LuanaSMS__Venue__r.Name, LuanaSMS__Session__r.LuanaSMS__Room__c, LuanaSMS__Session__r.LuanaSMS__Room__r.Name from LuanaSMS__Course_Session__c where LuanaSMS__Course__c =: targetCourseId and LuanaSMS__Session__r.RecordType.DeveloperName = \'' + luana_SessionConstants.RECORDTYPE_SESSION_VIRTUAL + '\' order by LuanaSMS__Session__r.Topic_Key__c, LuanaSMS__Session__r.LuanaSMS__Start_Time__c asc nulls last';
    }
    
    public void addPageMessage(ApexPages.severity sev, String msg){
    	ApexPages.Message warningmsg = new ApexPages.Message(sev, msg);
        ApexPages.addmessage(warningmsg);
    }
    
    class luana_ErrorRecordWrapper{
        
        public String errorMessage {set;get;}
        public LuanaSMS__Attendance2__c attendance {set; get;}
        public LuanaSMS__Session__c session {set; get;}
        public String objectType {set;get;}
        
        public luana_ErrorRecordWrapper(LuanaSMS__Session__c session, LuanaSMS__Attendance2__c attendance, String objectType, String errorMessage){
            this.session = session;
            this.attendance = attendance;
            this.objectType = objectType;
            this.errorMessage = errorMessage;
        }
    }
    
    class luana_WorkshopCourse {
        
        public String id {set; get;}
        public String courseId {set; get;}
        public String displayName {set; get;}
        public String name {set; get;}
        public List<luana_WorkshopSessionGroup> sessionGroups {set; get;}
        public integer noOfStudents {set; get;}
        
        public luana_WorkshopCourse(String courseId, String name, String displayName, List<luana_WorkshopSessionGroup> sessionGroups, integer noOfStudents){
            this.id = courseId;
            
            this.courseId = courseId;
            this.name = name;
            this.displayName = displayName;
            this.sessionGroups = sessionGroups;
            this.noOfStudents = noOfStudents;
        }
    }
    
    class luana_WorkshopSessionGroup {
        
        public String parentId {set; get;}
        public String groupKey {set; get;}
        public String name {set; get;}
        public List<LuanaSMS__Course_Session__c> sessions {set; get;}
        public integer avgSeat {set; get;}
        public integer maxSeat {set; get;}
        public integer noOfSeries {set; get;}
        public boolean fillUp {set; get;}
        public boolean isNew {set; get;}
        
        public luana_WorkshopSessionGroup(String parentId, String groupKey, String name, List<LuanaSMS__Course_Session__c> sessions, integer avgSeat, integer maxSeat, integer noOfSeries, boolean fillUp, boolean isNew){
            this.parentId = parentId;
            this.groupKey = groupKey;
            this.name = name;
            this.sessions = sessions;
            this.avgSeat = avgSeat;
            this.maxSeat = maxSeat;
            this.noOfSeries = noOfSeries;
            this.fillUp = fillUp;
            this.isNew = isNew;
        }
        
        public boolean getHasExistingSession(){
            if(sessions != null && sessions.size() > 0){
                return true;
            } else {
                return false;
            }
        }
        
    }
}