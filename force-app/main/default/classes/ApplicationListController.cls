public class ApplicationListController {

    @AuraEnabled
    public static List<Application__c> getApplicationsForUser(String applicationStatusesCSV) {
        string TOKEN_EXCLUDE = '-';
        
        List<Application__c> appls;
        String[] applicationStatuses  = null;
        String[] applicationStatusesNot = new List<String>();

        system.debug('###ApplicationListController:getApplicationsForUser:applicationStatusesCSV=' + applicationStatusesCSV);

        if (applicationStatusesCSV != null && applicationStatusesCSV != '')applicationStatuses = applicationStatusesCSV.split(',');

        system.debug('###ApplicationListController:getApplicationsForUser:after-split applicationStatuses=' + applicationStatuses);

        if (applicationStatuses != null && !applicationStatuses.isEmpty()) {
            integer filterParameterSize = applicationStatuses.size();
            //iterate through list in reverse because removing items
            for (integer i = filterParameterSize - 1; i >= 0; i--) {

                system.debug('###ApplicationListController:getApplicationsForUser:applicationStatuses[i]=' + applicationStatuses[i]);

                if (applicationStatuses[i].startsWith(TOKEN_EXCLUDE)) {
                    applicationStatusesNot.add(applicationStatuses[i].replace(TOKEN_EXCLUDE, ''));
                    applicationStatuses.remove(i);
                }
            }
        }
            
        system.debug('###ApplicationListController:getApplicationsForUser:after-loop applicationStatuses=' + applicationStatuses);
        system.debug('###ApplicationListController:getApplicationsForUser:after-loop applicationStatusesNot=' + applicationStatusesNot);

        List<User> userAccountIds = [select AccountId, account.Is_Migration_Agent__c from User where Id = :userInfo.getUserId() limit 1]; //PASA change Is_Migration_Agent__c
        if (userAccountIds != null && !userAccountIds.isEmpty()) {
            //Start PASA change

            if (applicationStatuses != null && !applicationStatuses.isEmpty() && applicationStatusesNot != null && !applicationStatusesNot.isEmpty()) {
                system.debug('### condition 1');// PASA
                appls = [select Id, Name, Application_Status__c, Application_Type_Formula__c, Applicant_Formula__c //PASA change Applicant_Formula__c
                       from Application__c
                       where Application_Status__c in :applicationStatuses
                       and Application_Status__c not in :applicationStatusesNot
                       //and Account__c = :userAccountIds[0].AccountId limit 5000];
                       and (Account__c = :userAccountIds[0].AccountId OR Migration_Agent__c=:userAccountIds[0].AccountId) // PASA Update
                       limit 5000];
            } else if (applicationStatuses != null && !applicationStatuses.isEmpty()) {
                system.debug('### condition 2');// PASA
                appls = [select Id, Name, Application_Status__c, Application_Type_Formula__c, Applicant_Formula__c //PASA change Applicant_Formula__c
                       from Application__c
                       where Application_Status__c in :applicationStatuses
                       //and Account__c = :userAccountIds[0].AccountId limit 5000];
                       and (Account__c = :userAccountIds[0].AccountId OR Migration_Agent__c=:userAccountIds[0].AccountId) // PASA Update
                       limit 5000];
            } else if (applicationStatusesNot != null && !applicationStatusesNot.isEmpty()){
                system.debug('### condition 3');// PASA
                appls = [select Id, Name, Application_Status__c, Application_Type_Formula__c, Applicant_Formula__c //PASA change Applicant_Formula__c
                       from Application__c
                       where Application_Status__c not in :applicationStatusesNot
                       //and Account__c = :userAccountIds[0].AccountId limit 5000];
                       and (Account__c = :userAccountIds[0].AccountId OR Migration_Agent__c=:userAccountIds[0].AccountId) // PASA Update
                       limit 5000];
            } else {
            	system.debug('### condition 4');// PASA
            	appls = [select Id, Name, Application_Status__c, Application_Type_Formula__c, Applicant_Formula__c //PASA change Applicant_Formula__c
                       from Application__c
                       //where Account__c = :userAccountIds[0].AccountId limit 5000];
                       where (Account__c = :userAccountIds[0].AccountId OR Migration_Agent__c = :userAccountIds[0].AccountId) // PASA Update
                       limit 5000];
            }
        }
        return appls;
    }
    
    @AuraEnabled
    public static boolean getIsAgent(){
    	List<User> userAccountIds=[select AccountId, account.Is_Migration_Agent__c from User where Id=:userInfo.getUserId() limit 1];
    	system.debug('### getIsAgentCalled');
    	if(userAccountIds!=null&&!userAccountIds.isEmpty()){
    		system.debug('### userAccountsRetuned, userAccountIds[0].account.Is_Migration_Agent__c: ' + userAccountIds[0].account.Is_Migration_Agent__c);	
    		return userAccountIds[0].account.Is_Migration_Agent__c;
    	}
    	else{
    		return false;
    	}
    	
    }

}