/**
* @author Jannis Bott
* @date 19/12/2016
*
* @description  Tests that QAS AddressService methods are callable and return data
*/
@isTest(seeAllData=false)
public class EDQAddressServiceTests {

    @isTest
    static void testSearchAddress() {
        Test.setMock(HttpCalloutMock.class, new HttpResponseMock.HttpResponseSuccess());

        Map<String, List<Map<String, Object>>> response = EDQAddressService.searchAddress('Test Address', 'AU', 10);

        System.debug(response);

        Map<String, Object> values = new Map<String, Object>{
                'id' => '700AUS-aOAUSHArgBwAAAAAIAwEAAAAAK.6m0AAgAAAAAAAAAAD..2QAAAAA.....wAAAAAAAAAAAAAAAAAxNDcgUm9zcyBTdHJlZXQA',
                'label1' => '147 Ross Street',
                'label2' => ' FOREST LODGE  NSW 2037',
                'position' => 0
        };

        List<Map<String, Object>> listMap = new List<Map<String, Object>>();
        listMap.add(values);

        Map<String, List<Map<String, Object>>> expectedResponse = new Map<String, List<Map<String, Object>>>{
                'SUCCESS' => listMap
        };

        System.assertEquals(expectedResponse, response, 'Mock response should be returned and match.');
    }

    @isTest
    public static void testFormatAddress() {
        Test.setMock(HttpCalloutMock.class, new HttpResponseMock.HttpResponseSuccessQASFormat());

        Map<String, Object> response = EDQAddressService.formatAddress('700AUS-SOAUSHArgBwAAAAAIAwEAAAABMqWiUAAgAAAAAAAAAAD..2QAAAAA.....wAAAAAAAAAAAAAAAAAxNDcgUm9zcyBTdHJlZXQA', 'AU');
        System.debug(response);
        Map<String, Object> expectedResponseContent = new Map<String, Object>{
                'city' => 'PORT MELBOURNE',
                'country' => 'AUSTRALIA',
                'countryCode' => 'AU',
                'dpid' => '30717717',
                'postcode' => '3207',
                'qasValidated' => true,
                'state' => 'Victoria',
                'street' => '147 Ross St'
        };

        Map<String, Object> expectedResponse = new Map<String, Object>{
                'SUCCESS' => expectedResponseContent
        };
        System.assertEquals(expectedResponse, response, 'Mock response should be returned and match.');
    }
}