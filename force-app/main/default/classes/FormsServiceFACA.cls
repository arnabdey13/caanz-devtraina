/**
 * @Author: Jannis Bott
 * @Date: 11/11/2016
 * @Description: This is the controller class that serves the Find a CA component for the community
 */
public with sharing class FormsServiceFACA {

    @AuraEnabled
    public static List<FacaOptIn> load() {
        return load(EditorPageUtils.getUsersAccountId());
    }

    @testVisible
    private static List<FacaOptIn> load(Id accountId) {
        List<FacaOptIn> facaObjects = new List<FacaOptIn>();
        List<Account> accounts = [
                SELECT Find_CA_Opt_In__c,
                        Membership_Type__c,
                        Status__c,
                        IsPersonAccount,
                        Insolvency_Practitioner__c,
                        Qualified_Auditor__c,
                        QA_Date_of_suspension__c,
                        QA_Date_of_de_recognition__c,
                        Opt_out_of_Find_an_Accountant_register__c,
                        Financial_Category__c,
                        BV__c,
                        SMSF__c,
                        FP_Specialisation__c,
                        NZAIP_Date_of_suspension__c,
                        NZAIP_Date_of_de_recognition__c,
                        CPP__c
                FROM Account
                WHERE Id = :accountId
                AND Status__c = 'Active'
                AND Opt_out_of_Find_an_Accountant_register__c = false
        ];

        if (!accounts.isEmpty()) {
            Account acc = accounts[0];

            if (isCPP(acc)) {
                if (acc.Find_CA_Opt_In__c) {
                    facaObjects.add(new FacaOptIn('Public Practitioner', false, true, true));
                } else {
                    facaObjects.add(new FacaOptIn('Public Practitioner', false, false, true));
                }
            }
            if (isQualifiedAuditor(acc)) facaObjects.add(new FacaOptIn('Qualified Auditor', true, true, false));
            if (isInsolvencyPractitioner(acc)) facaObjects.add(new FacaOptIn('Insolvency Practitioner', true, true, false));
            if (isBusinessValuationSpecialist(acc)) facaObjects.add(new FacaOptIn('Business Valuation Specialists', true, true, false));
            if (isFinancialPlanningSpecialist(acc)) facaObjects.add(new FacaOptIn('Financial Planning Specialists', true, true, false));
            if (isSMSFSpecialist(acc)) facaObjects.add(new FacaOptIn('SMSF Specialist', true, true, false));
        }
        return facaObjects;
    }

    @AuraEnabled
    public static Boolean save(Boolean caOptIn) {
        return save(caOptIn, EditorPageUtils.getUsersAccountId());
    }

    @TestVisible
    private static Boolean save(Boolean caOptIn, Id accountId) {
        System.debug(caOptIn);
        Account acc = new Account(Id = accountId, Find_CA_Opt_In__c = caOptIn);
        update acc;
        return true;
    }

    @testVisible
    private static Boolean isQualifiedAuditor(Account acc) {
        if (acc.Membership_Type__c.equals('Member') &&
                acc.Qualified_Auditor__c == true &&
                acc.QA_Date_of_suspension__c == null &&
                acc.QA_Date_of_de_recognition__c == null) {
            return true;
        } else {
            return false;
        }
    }

    @testVisible
    private static Boolean isInsolvencyPractitioner(Account acc) {
        if ((acc.Membership_Type__c.equals('Member')
                || acc.Membership_Type__c.equals('NZAIP')
                || acc.Membership_Type__c.equals('NMP')) &&
                acc.IsPersonAccount == true &&
                acc.Insolvency_Practitioner__c == true &&
                acc.NZAIP_Date_of_suspension__c == null &&
                acc.NZAIP_Date_of_de_recognition__c == null) {
            return true;
        } else {
            return false;
        }
    }

    @testVisible
    private static Boolean isBusinessValuationSpecialist(Account acc) {
        if (acc.Membership_Type__c.equals('Member') &&
                acc.IsPersonAccount == true &&
                (String.isBlank(acc.Financial_Category__c) ||
                        !acc.Financial_Category__c.contains('Retired')) &&
                acc.BV__c == true) {
            return true;
        } else {
            return false;
        }
    }

    @testVisible
    private static Boolean isFinancialPlanningSpecialist(Account acc) {
        if (acc.Membership_Type__c.equals('Member') &&
                acc.IsPersonAccount == true &&
                (String.isBlank(acc.Financial_Category__c) ||
                        !acc.Financial_Category__c.contains('Retired')) &&
                acc.FP_Specialisation__c == true) {
            return true;
        } else {
            return false;
        }
    }

    @testVisible
    private static Boolean isSMSFSpecialist(Account acc) {
        if (acc.Membership_Type__c.equals('Member') &&
                acc.IsPersonAccount == true &&
                (String.isBlank(acc.Financial_Category__c) ||
                        !acc.Financial_Category__c.contains('Retired')) &&
                acc.SMSF__c == true) {
            return true;
        } else {
            return false;
        }
    }

    @testVisible
    private static Boolean isCPP(Account acc) {
        if (acc.Membership_Type__c.equals('Member') &&
                acc.IsPersonAccount == true &&
                acc.CPP__c == true) {
            return true;
        } else {
            return false;
        }
    }

    public class FacaOptIn {
        @AuraEnabled
        public String label;

        @AuraEnabled
        public Boolean disabled;

        @AuraEnabled
        public Boolean active;

        @AuraEnabled
        public Boolean underlineBold;

        public FacaOptIn(String label, Boolean disabled, Boolean active, Boolean underlineBold) {
            this.label = label;
            this.disabled = disabled;
            this.active = active;
            this.underlineBold = underlineBold;
        }
    }
    
    @AuraEnabled
    public static String getUserPermissions(List<String> customPermissions){
        List<CustomPermission> customPermissionRecords = [
            SELECT Id, DeveloperName, (
                SELECT Id FROM SetupEntityAccessItems 
                WHERE ParentId IN: getPermissionSetIds() OR ParentId =: UserInfo.getProfileId()
            ) 
            FROM CustomPermission WHERE DeveloperName IN : customPermissions
        ];
        return JSON.serialize(getUserPermissions(customPermissionRecords));
    }

    private static Set<String> getUserPermissions(List<CustomPermission> permissions){
    	Set<String> userPermissions = new Set<String>();
    	for(CustomPermission permission : permissions){
    		if(!permission.SetupEntityAccessItems.isEmpty()){
    			userPermissions.add(permission.DeveloperName);
    		}
    	}
    	return userPermissions;
    }

    private static Set<Id> getPermissionSetIds(){
        Set<Id> psIds = new Set<Id>();
        for(PermissionSetAssignment ps : [SELECT PermissionSetId FROM PermissionSetAssignment 
            WHERE AssigneeId =: UserInfo.getUserId()]){
            psIds.add(ps.PermissionSetId);
        }
        return psIds;
    }

}