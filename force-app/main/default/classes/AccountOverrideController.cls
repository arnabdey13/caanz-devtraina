public with sharing class AccountOverrideController {
	Account AccountObj;
	public AccountOverrideController(ApexPages.StandardController controller) {
		AccountObj = (Account) controller.getRecord();
	}
	public PageReference gotoEditPage(){
		if( UserInfo.getProfileId()==ProfileCache.getId('NZICA Community Login User') ){
			String retURL = ApexPages.currentPage().getParameters().get('retURL');
			return new PageReference( retURL );
		}
		else{
			PageReference PR = new PageReference('/' + AccountObj.Id + '/e?retURL=%2F' + AccountObj.Id);
			PR.getParameters().put('nooverride','1');
			return PR;
		}
	}
	public PageReference gotoViewPage(){
		if( UserInfo.getProfileId()==ProfileCache.getId('NZICA Community Login User') ){
			return new PageReference('/'+ Schema.sObjectType.Application__c.getKeyPrefix() +'/o');
		}
		else{
			PageReference PR = new PageReference('/' + AccountObj.Id );
			PR.getParameters().put('nooverride','1');
			return PR;
		}
	}
}