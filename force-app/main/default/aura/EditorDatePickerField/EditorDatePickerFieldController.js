({
    onChange : function(component, event, helper) { 
        var dateStr = component.get("v.value").split('-').join('/');
        var dateFormat = $A.get("$Locale.dateFormat");
        var dateString = $A.localizationService.formatDateTime(new Date(dateStr), dateFormat);
        helper.applyChange(component, dateString);
    }
})