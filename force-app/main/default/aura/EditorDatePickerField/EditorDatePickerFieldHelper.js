({
    load : function(component, event) {
        var readOnlyIfNotNull = component.get("v.value") ;
        if(readOnlyIfNotNull && component.get("v.disableIfNull")){
            component.set("v.disabled", true);
        }        
    },
    validate : function(component) {
        var isRequired = component.get("v.required");
        var hidden = component.get("v.hidden");
        var value = component.get("v.value");
        var isValueMissing = $A.util.isUndefined(value);
        var invalid = !hidden && isRequired && isValueMissing;        
        return !invalid;
    },
    setUIValid : function(component, isValid) {
        var editor = component.find("editor");
        if (isValid) {
            component.set("v.invalid", false);
            component.set("v.validationMessage", null);
        } else {
            component.set("v.invalid", true);
            component.set("v.validationMessage", "Please choose a Date");
        }
    }
})