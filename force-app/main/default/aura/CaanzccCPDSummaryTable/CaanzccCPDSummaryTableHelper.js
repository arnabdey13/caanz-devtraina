({
	getData : function(component) {

        var action = component.get("c.getCPDSummariesForUser");

        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            var rtnValue = response.getReturnValue();

            if (state === "SUCCESS") {
                // Alert the user with the value returned 
                // from the server
                //alert("From server: " + response.getReturnValue());
            	console.log('CPD Summry rtnValue='+rtnValue);
            	$A.log('CPD Summry rtnValue='+rtnValue);
            	if (rtnValue !== null) {
            		console.log('Not Null - List Length =  '+rtnValue.length);
                    for (var i = 0; i < rtnValue.length; i++){
                        if (rtnValue[i].Current_Triennium__c == true){                          
                            
                            component.set('v.DateRangeYear1',rtnValue[i].Year_1_Date_Range__c);                            
                            component.set('v.DateRangeYear2',rtnValue[i].Year_2_Date_Range__c);                            
                            component.set('v.DateRangeYear3',rtnValue[i].Year_3_Date_Range__c); 
                            component.set('v.TotalHoursYear1',Number(rtnValue[i].Qualifying_Informal_Hours_Y1__c) + Number(rtnValue[i].Formal_Verifiable_Hours_Y1__c));                            
                            component.set('v.TotalHoursYear2',rtnValue[i].Qualifying_Informal_Hours_Y2__c + rtnValue[i].Formal_Verifiable_Hours_Y2__c);                            
                            component.set('v.TotalHoursYear3',rtnValue[i].Qualifying_Informal_Hours_Y3__c + rtnValue[i].Formal_Verifiable_Hours_Y3__c);                            
                            component.set('v.VerifiedHoursYear1',rtnValue[i].Formal_Verifiable_Hours_Y1__c);                            
                            component.set('v.VerifiedHoursYear2',rtnValue[i].Formal_Verifiable_Hours_Y2__c);                            
                            component.set('v.VerifiedHoursYear3',rtnValue[i].Formal_Verifiable_Hours_Y3__c);                            
                        }
                        else {
                            console.log('Not Current Triennium - List Index = ' + i + ' Triennium Start Date = '+ rtnValue[i].Triennium_Start_Date__c);
                        }

                    }
            	}

                // You would typically fire a event here to trigger 
                // client-side notification that the server-side 
                // action is complete
            }
            else if (state === "INCOMPLETE") {
                alert("Something went wrong. Refresh the page.");
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

       $A.enqueueAction(action);
		
	}
})