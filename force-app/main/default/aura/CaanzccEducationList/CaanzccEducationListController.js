({
    doInit : function(component, event, helper) {

        // helper.setEducationTableHeaders();

        var action = component.get("c.getEducationRecordsForUser");

        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            var rtnValue = response.getReturnValue();

            if (state === "SUCCESS") {
                // Alert the user with the value returned 
                // from the server
                //alert("From server: " + response.getReturnValue());
            	console.log('Education List rtnValue='+rtnValue);
            	$A.log('Education List rtnValue='+rtnValue);
            	if (rtnValue !== null) {
            		console.log('Not Null - List Length =  '+rtnValue.length);
                	component.set('v.educationRecordList',rtnValue);
            	}

                // You would typically fire a event here to trigger 
                // client-side notification that the server-side 
                // action is complete
            }
            else if (state === "INCOMPLETE") {
                console.log("Something went wrong. Refresh the page.");
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

       console.log('***Execute Education List doInit***');

       $A.enqueueAction(action);
    }, 

        goToEducationDetails : function(component, event, helper) {

		var education_item = event.target;
        var education_id = education_item.dataset.id;
        var education_type = education_item.dataset.type;
        var detailPageName = component.get("v.detailPage");
        var itemID = event.currentTarget.id;
        var itemType = event.currentTarget.type;

        console.log("Application ID: " + education_id);
        console.log("Application Tyoe: " + education_type);
        console.log("Detail Page Name: " + detailPageName);
        console.log("Target ID: " + itemID);
        console.log("Target Type: " + itemType);

        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
        	"url": '/' + detailPageName + '/' + education_id
        });
        urlEvent.fire();
    },

    goToEducationListPage : function(component, event, helper) {

        var urlEvent = $A.get("e.force:navigateToURL");
        var cpdSummaryList = component.get("v.cpdSummaryList");
        urlEvent.setParams({
            "url": '/relatedlist/' + cpdSummaryList.id + '/Education_Record__r'
        });
        urlEvent.fire();
    }
    
})