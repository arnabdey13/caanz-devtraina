({
	doInit : function(component, event, helper) {
        helper.loadConfigurationData(component);
	},
    handleCountryChange: function(component, event, helper){
      if(!$A.util.isEmpty(component.get("v.selectedCountry"))){
        alert("Handle Country Change...and Display dynamic content base on - " + component.get("v.selectedCountry"));
      }
    }
})