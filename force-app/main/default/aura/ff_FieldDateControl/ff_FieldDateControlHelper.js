({
    validateDate: function (component, event) {

        var val = component.get("v.value");
        if ($A.util.isEmpty(val)) {
            return [];
        }

        var errorMessage = "This is not a valid date. Please enter a valid date format (e.g. " + $A.localizationService.formatDate(new Date(), dateFormat) + ")";
        var dateFormat = "dd/MM/yyyy";

        var splitDate = val.split('-');
        if (splitDate.length != 3) {
            return [errorMessage];
        } else {
            if (splitDate[0].length < 4) {
                return [errorMessage];
            }

            if (splitDate[1].length == 0 || splitDate[1].length > 2) {
                return [errorMessage];
            }

            if (splitDate[2].length == 0 || splitDate[2].length > 2) {
                return [errorMessage];
            }
        }

        var formattedDate = splitDate[0] + '-' + splitDate[1] + '-' + splitDate[2];
        var dateOffsetYears = component.get("v.minYears");
        var today = new Date();
        var offsetDate = new Date(today.setFullYear(today.getFullYear() - dateOffsetYears));

        try {
            var dateValue = $A.localizationService.formatDate(formattedDate, dateFormat);
            var parsedDateValue = $A.localizationService.parseDateTime(dateValue, dateFormat);
            if ($A.util.isUndefinedOrNull(dateValue)) {
                return [errorMessage];
            } else if(dateOffsetYears > 0 && $A.localizationService.isAfter(parsedDateValue, offsetDate)) {
                return [component.get("v.minYearsErrorMessage")];
            } else {
                return [];
            }
        } catch (e) {
            return [errorMessage];
        }
    },
})