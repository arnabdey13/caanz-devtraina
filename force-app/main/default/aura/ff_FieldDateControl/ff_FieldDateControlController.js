({
    validateDate: function (component, event, helper) {
        event.stopPropagation(); // since handling it's own event, nobody else cares
        var dateSpecificErrors = helper.validateDate(component, event);
        component.set("v.specificValidationErrors", dateSpecificErrors);
    }
})