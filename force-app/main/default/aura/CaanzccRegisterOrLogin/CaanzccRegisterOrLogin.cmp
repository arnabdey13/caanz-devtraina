<aura:component description="RegisterOrLogin"
                implements="forceCommunity:availableForAllPageTypes,flexipage:availableForAllPageTypes"
                controller="RegisterOrLoginService">

    <!-- Start Configurable Attributes -->

    <!-- Start Messages -->
    <aura:attribute type="String" name="headerMessageRegistration"
                    description="The message displayed under the caanz logo"
                    default="Join the community today to access group discussions, recommended content and products, and enhanced support."/>
    <aura:attribute type="String" name="headerMessageLogin"
                    description="The message displayed under the caanz logo"
                    default="Login to the community to manage personalised information and customer support."/>
    <aura:attribute type="String" name="registrationInstructionMessage"
                    description="Contains the content for the message that is displayed under the email field if a new registration is required."
                    default="Please complete the details below to create your account."/>
    <aura:attribute type="String" name="loginInstructionMessage"
                    description="Contains the content for the message that is displayed under the email field if a new registration is required."
                    default="It looks like you already have an account. Please login using your password."/>
    <aura:attribute type="String" name="successLine1"
                    description="Contains the content for the message that is displayed on successful save."
                    default="We have created your account successfully."/>
    <aura:attribute type="String" name="successLine2"
                    description="Contains the content for the message that is displayed on successful save."
                    default="Please close this window and check your email to set your password."/>
    <aura:attribute type="String" name="expired"
                    description="Contains the content for the message that is displayed on expired token."
                    default="The initial time to set a password has elapsed. Please click the below button to request a new link."/>
    <aura:attribute type="String" name="passwordNotSet"
                    description="Contains the content for the message that is displayed if the password has not been set token."
                    default="You need to set your password first. Please click the below button to request a new link."/>
    <aura:attribute type="String" name="inactiveLine1"
                    description="Contains the content for the message that is displayed on inactive user return."
                    default="It seems like your user is inactive. Please contact us on 1300 137 322 (AU) / 0800 4 69422 (NZ) / +64 4 474 7840 (Overseas)"/>
    <aura:attribute type="String" name="passwordReset"
                    description="Contains the content for the message that is displayed when the password reset button is clicked."
                    default="We have successfully reset your password. An email has been sent to you that contains a link to reset your password."/>
    <aura:attribute type="String" name="passwordResetting"
                    description="Contains the content for the message that is displayed when the password reset button is clicked and the server has not yet responded."
                    default="We are resetting your password..."/>
    <aura:attribute type="String" name="emailResentMessageLine1"
                    description="Contains the content for the message that is displayed on once an expired token has been resent."
                    default="We have sent you an email with a new link to set your password."/>
    <aura:attribute type="String" name="repeatableErrorMessage"
                    description="Contains the content for the message that is displayed if a request fails and can be retried."
                    default="We are very sorry but something went wrong. Please try again shortly."/>
    <aura:attribute type="String" name="nonRepeatableErrorMessage"
                    description="Contains the content for the message that is displayed if a request fails and should not be retried."
                    default="We are very sorry but something went wrong. Please contact us on 1300 137 322 (AU) / 0800 4 69422 (NZ) / +64 4 474 7840 (Overseas)"/>
    <aura:attribute type="String" name="invalidEmailErrorMessage"
                    description="Contains the content for the message that is displayed if an invalid email has been entered."
                    default="Enter a valid email address."/>
    <aura:attribute type="String" name="invalidPassword"
                    description="Contains the content for the error message that is displayed if an invalid password has been entered."
                    default="The username or password entered is incorrect."/>
    <!-- End Messages -->

    <!-- Start Button Labels -->
    <aura:attribute type="String" name="emailButtonLabel"
                    default="Next"
                    description="The label of the next button"/>
    <aura:attribute type="String" name="emailButtonLabelActive"
                    default="Checking..."
                    description="The label of the next button when clicked"/>
    <aura:attribute type="String" name="registerButtonLabel"
                    default="Register"
                    description="The label of the registration button"/>
    <aura:attribute type="String" name="registerButtonLabelActive"
                    default="Creating your account..."
                    description="The label of the registration button when clicked"/>
    <aura:attribute type="String" name="loginButtonLabel"
                    default="Login"
                    description="The label of the registration button"/>
    <aura:attribute type="String" name="loginButtonLabelActive"
                    default="Logging you in..."
                    description="The label of the registration button when clicked"/>
    <aura:attribute type="String" name="requestLinkButtonLabel"
                    default="Request a new link"
                    description="The label of the button the requests a new link to set the password"/>
    <aura:attribute type="String" name="requestLinkButtonLabelActive"
                    default="Sending a new link..."
                    description="The label of the button the requests a new link to set the password when clicked"/>
    <!-- End Button Labels -->

    <!-- Start Field Labels -->
    <!-- <aura:attribute type="String" name="emailLabel"
                    default="Start by entering your email"
                    description="The label that is displayed above the email field"/> -->    
    <aura:attribute type="String" name="salutationLabel"
                    default="Salutation"
                    description="The label that is displayed above the salutation field"/>
    <aura:attribute type="String" name="firstNameLabel"
                    default="First Name"
                    description="The label that is displayed above the first name field"/>
    <aura:attribute type="String" name="lastNameLabel"
                    default="Last Name"
                    description="The label that is displayed above the last name field"/>
    <aura:attribute type="String" name="countryLabel"
                    default="Country"
                    description="The label that is displayed above the country field"/>
    <aura:attribute type="String" name="passwordLabel"
                    default="Password"
                    description="The label that is displayed above the password field"/>
    <aura:attribute type="string" name="subscribeLabel"
                    default="Sign up to be in the know about training offers, learning support, and upcoming events"
                    description="The label that is displayed above the subscribe field"/>

                    
    <!-- End Field Labels -->

    <!-- Start Field Placeholders -->
    <aura:attribute type="String" name="emailPlaceholder"
                    default="Enter your email..."
                    description="The placeholder that is displayed in the email field"/>    
    <aura:attribute type="String" name="passwordPlaceholder"
                    default="Enter password here..."
                    description="The placeholder that is displayed in the password field"/>
    <aura:attribute type="String" name="firstNamePlaceholder"
                    default="Enter first name here..."
                    description="The placeholder that is displayed in the first name field"/>
    <aura:attribute type="String" name="lastNamePlaceholder"
                    default="Enter last name here..."
                    description="The placeholder that is displayed in the last name field"/>
    <aura:attribute type="String" name="selectOptionPlaceholder"
                    default="-- Please Select --"
                    description="The placeholder that is displayed in the first row of a select option"/>
    
    <!-- End Field Placeholders -->

    <!-- Start Resources -->
    <aura:attribute type="String" name="resourcePrefix"
                    description="The custom prefic in the community url"
                    default="/MyCA"/>
    <aura:attribute type="String" name="caanzLogoUrl"
                    description="The path to the url for the CAANZ logo"
                    default="/resource/CAANZLogo"/>
    <!-- End Resources -->
    <!-- End Configurable Attributes -->

    <!-- Start Non Configurable Attributes-->
    <!-- Start Field Attributes -->
    <aura:attribute type="String" name="email"
                    description="The email field that needs to be captured for login or sign-up"/>
    <aura:attribute type="String" name="password"
                    description="The password field that needs to be captured for login."/>
    <aura:attribute type="String" name="salutation"
                    description="The salutation field that needs to be captured for sign-up"/>
    <aura:attribute type="String" name="firstName"
                    description="The first name field that needs to be captured for sign-up"/>
    <aura:attribute type="String" name="lastName"
                    description="The email field that needs to be captured for sign-up"/>
    <aura:attribute type="String" name="country"
                    description="The country field that needs to be captured for sign-up"/>
    <aura:attribute type="String" name="destination"
                    description="The url that the user is trying to reach"/>
	<aura:attribute type="boolean" name="subscribe"
                    description="Subscribe"  default="false"/>
	<aura:attribute type="boolean" name="termsConditions"
                    description="Terms and conditions"  default="false"/>
    <!-- End Field Attributes -->

    <!-- Start Data Attributes -->
    <aura:attribute type="Object[]" name="salutationValues"
                    description="The salutation values available"/>
    <aura:attribute type="Object[]" name="countryValues"
                    description="The country values available"/>
    <aura:attribute type="String" name="responseMessageLine1"
                    description="Contains the message that is shown to the user upon server response on line 1."/>
    <aura:attribute type="String" name="responseMessageLine2"
                    description="Contains the message that is shown to the user upon server response on line 2."/>
    <aura:attribute type="String" name="instructionMessage"
                    description="Contains the message that is shown to the user under the email field."/>
    <aura:attribute type="String" name="headerMessage"
                    description="Contains the message that is displayed below the logo"/>
    <aura:attribute type="String" name="next"
                    description="Hold the label of the next button"/>
    <aura:attribute type="String" name="login"
                    description="Hold the label of the login button"/>
    <aura:attribute type="String" name="register"
                    description="Hold the label of the register button"/>
    <aura:attribute type="String" name="resend"
                    description="Hold the label of the resend button"/>
    <aura:attribute type="String" name="errorMessage"
                    description="The error message displayed at the top of page"/>
    <!-- End Data Attributes -->

    <!-- Start Display Attributes -->
    <aura:attribute type="Boolean" name="disableNextButton" default="true"
                    description="If true the next button for the email field is disabled"/>
    <aura:attribute type="Boolean" name="disableLoginButton" default="true"
                    description="If true the login button for the email field is disabled"/>
    <aura:attribute type="Boolean" name="disableRegistrationButton" default="true"
                    description="If true the register button for the email field is disabled"/>
    <aura:attribute type="Boolean" name="disableResendButton" default="false"
                    description="If true the resend button to get a new password set email is disabled"/>
    <aura:attribute type="Boolean" name="disableEmailField" default="false"
                    description="If true the email field is disabled"/>
    <aura:attribute type="Boolean" name="disableInputFields" default="true"
                    description="If true all fields will be diabled apart form the email field"/>
    <aura:attribute type="Boolean" name="showHeaderMessage" default="true"
                    description="If true the header message is visible"/>
    <aura:attribute type="Boolean" name="showEmailField" default="true"
                    description="If true the email field is visible"/>
    <aura:attribute type="Boolean" name="showEmailNextBtn" default="true"
                    description="If true the next button for the email field is visible"/>
    <aura:attribute type="Boolean" name="showRegister" default="false"
                    description="If true the registration fields are hidden"/>
    <aura:attribute type="Boolean" name="showLogin" default="false"
                    description="If true the login fields are visible."/>
    <aura:attribute type="Boolean" name="showResendButton" default="false"
                    description="If true the button to resend the password set email if visible."/>
    <aura:attribute type="Boolean" name="showResponseMessage" default="false"
                    description="If true the response message block will be shown"/>
    <aura:attribute type="Boolean" name="showInstruction" default="false"
                    description="If true a message is displayed below the email field."/>
    <aura:attribute type="Boolean" name="showErrorMessage" default="false"
                    description="If true a message is displayed at the top of the page."/>
    <aura:attribute type="Boolean" name="showSubscribeField" default="true"
                    description="If true the subscribe field is displayed."/>

    <aura:attribute type="String" name="routerPageUrl" default=""
                    description="If available, after login user will be redirected to this url."/>

    <!-- End Display Attributes -->
    <!-- End Non Conigurable Attributes-->

    <aura:handler name="init" value="{!this}" action="{!c.doInit}"/>

    <div class="slds">
        <lightning:layout multipleRows="true">

            <!-- Start Logo and Header Message-->
            <lightning:layout multipleRows="true" horizontalAlign="center">

<!--
                <lightning:layoutItem size="12" padding="around-small"
                                      largeDeviceSize="12" mediumDeviceSize="12" smallDeviceSize="12">
                    <img src="{!v.resourcePrefix + v.caanzLogoUrl}"/>
                </lightning:layoutItem>
-->
                <lightning:layoutItem class="{!v.showHeaderMessage ? 'slds-show' : 'slds-hide'}"
                                      size="12" padding="around-small"
                                      largeDeviceSize="12" mediumDeviceSize="12" smallDeviceSize="12">

                    <div class="slds-text-body--regular slds-text-align--center">
                        <h2 class="headerMessage">{!v.headerMessage}</h2>
                    </div>
                </lightning:layoutItem>
            </lightning:layout>
            <!-- End Logo and Header Message-->

            <!-- Start Email field and button -->
            <lightning:layoutItem class="{!v.showEmailField ? 'slds-show' : 'slds-hide'}" aura:id="emailField"
                                  size="12" padding="around-small"
                                  largeDeviceSize="12" mediumDeviceSize="12" smallDeviceSize="12">
                <div class="slds-form-element">
                    <div class="slds-form-element__control">
                        <input id="email"
                               aura:id="emailInput"
                               class="slds-input"
                               type="email"
                               tabindex="1"
                               value="{!v.email}"
                               placeholder="{!v.emailPlaceholder}"
                               required="true"
                               disabled="{!v.disableEmailField}"
                               onfocus="{!c.showNextBtn}"
                               onkeyup="{!c.enableDisableNextBtn}"
                               onchange="{!c.validateEmail}"
                               onblur="{!c.validateEmail}"/>
                    </div>
                    <div aura:id="emailInvalidMessage" class="slds-form-element__help slds-hide">Email is invalid.</div>
                </div>
            </lightning:layoutItem>

            <lightning:layoutItem class="{!v.showEmailNextBtn ? 'slds-show' : 'slds-hide'}"
                                  size="12"
                                  largeDeviceSize="12"
                                  mediumDeviceSize="12"
                                  smallDeviceSize="12"
                                  padding="around-small">

                <lightning:button variant="brand"
                                  label="{!v.next}"
                                  onclick="{!c.validateEmail}"
                                  onfocus="{!c.validateEmail}"
                                  disabled="{!v.disableNextButton}"
                                  class="{!($Browser.isPhone != true ? 'slds-size_full' : 'slds-size_full')}"/>
            </lightning:layoutItem>
            <!-- Start Email field and button -->

            <!-- Start Instruction Message-->
            <lightning:layout multipleRows="true" horizontalAlign="center">
                <lightning:layoutItem class="{!v.showInstruction ? 'slds-show' : 'slds-hide'}"
                                      size="12" padding="around-small"
                                      largeDeviceSize="12" mediumDeviceSize="12" smallDeviceSize="12">

                    <div class="slds-text-body--regular slds-text-align--center">
                        <div>{!v.instructionMessage}</div>
                    </div>
                </lightning:layoutItem>

                <lightning:layoutItem size="12" padding="around-small"
                                      class="{!(v.showErrorMessage == true ? 'slds-show' : 'slds-hide')}"
                                      largeDeviceSize="12" mediumDeviceSize="12" smallDeviceSize="12">
                    <div class="slds-notify_container">
                        <div class="slds-notify slds-notify--alert slds-theme--error slds-theme--alert-texture"
                             role="alert">
                            <button class="slds-button slds-notify__close slds-button--icon-inverse"
                                    onclick="{!c.closeError}" title="Close">
                                <img src="{!v.resourcePrefix + '/resource/SLDS/assets/icons/action/close_60.png'}"
                                     class="error-close-icon"/>
                                <span class="slds-assistive-text">Close</span>
                            </button>
                            <span class="slds-assistive-text">Error</span>
                            <h2>{!v.errorMessage}</h2>
                        </div>
                    </div>
                </lightning:layoutItem>
            </lightning:layout>
            <!-- End Instruction Message-->

            <!-- Start Login Section -->
            <lightning:layoutItem class="{!v.showLogin ? 'slds-show' : 'slds-hide'}" aura:id="passwordField"
                                  size="12" padding="around-small"
                                  largeDeviceSize="12" mediumDeviceSize="12" smallDeviceSize="12">
                <div class="slds-form-element">
                    <label class="slds-form-element__label" for="password">
                        <abbr class="slds-required" title="required">*</abbr> {!v.passwordLabel}</label>
                    <div class="slds-form-element__control">
                        <input id="password"
                               aura:id="passwordInput"
                               tabindex="2"
                               class="slds-input"
                               type="password"
                               value="{!v.password}"
                               placeholder="{!v.passwordPlaceholder}"
                               required="true"
                               disabled="{!v.disableInputFields}"
                               onfocus="{!c.closeError}"
                               onkeyup="{!c.enableDisableLoginBtn}"
                               onchange="{!c.doLogin}"
                               onblur="{!c.doLogin}"/>
                    </div>
                    <div aura:id="passwordErrorMessage" class="slds-form-element__help slds-hide">Password is
                        required.
                    </div>
                </div>
                <div class="slds-text-align--center" aura:id="passwordResetBtn">
                    <lightning:button label="Forgot Password" variant="reset" onclick="{!c.handlePasswordReset}"/>
                </div>
            </lightning:layoutItem>

            <lightning:layoutItem class="{!v.showLogin ? 'slds-size_full slds-show' : 'slds-hide'}"
                                  size="12" padding="around-small"
                                  largeDeviceSize="12" mediumDeviceSize="12" smallDeviceSize="12">
                <lightning:button variant="brand"
                                  label="{!v.login}"
                                  tabindex="3"
                                  onclick="{!c.doLogin}"
                                  disabled="{!v.disableLoginButton}"
                                  class="slds-size_full"/>
                                  />
            </lightning:layoutItem>
            <!-- End Login Section -->

            <!-- Start Registration Section-->
            <lightning:layoutItem class="{!v.showRegister ? 'slds-show' : 'slds-hide'}" aura:id="salutationField"
                                  size="12"
                                  largeDeviceSize="6"
                                  mediumDeviceSize="6"
                                  smallDeviceSize="12"
                                  padding="around-small">

                <div class="slds-form-element">
                    <label class="slds-form-element__label" for="salutation">
                        <abbr class="slds-required" title="required">*</abbr> {!v.salutationLabel}</label>
                    <div class="slds-form-element__control">
                        <div class="slds-select_container">
                            <select id="salutation"
                                    aura:id="salutationInput"
                                    class="slds-select"
                                    required="true"
                                    disabled="{!v.disableInputFields}"
                                    onkeyup="{!c.validateSalutation}"
                                    onfocus="{!c.closeError}"
                                    onblur="{!c.validateSalutation}"
                                    onchange="{!c.validateSalutation}"
                                    tabindex="2">
                                <aura:iteration items="{!v.salutationValues}" var="s">
                                    <option label="{!s.label}" disabled="{!s.disabled}"
                                            selected="{!s.selected}">{!s.label}</option>
                                </aura:iteration>
                            </select>
                        </div>
                    </div>
                    <div aura:id="salutationErrorMessage" class="slds-form-element__help slds-hide">Salutation is
                        required.
                    </div>
                </div>
            </lightning:layoutItem>

            <aura:if isTrue="{!!$Browser.isPhone}">
                <lightning:layoutItem class="{!v.showRegister ? 'slds-show' : 'slds-hide'}"
                                      size="12"
                                      largeDeviceSize="6"
                                      mediumDeviceSize="6"
                                      smallDeviceSize="0"
                                      padding="around-small"/>
            </aura:if>

            <lightning:layoutItem class="{!v.showRegister ? 'slds-show' : 'slds-hide'}" aura:id="firstNameField"
                                  size="12"
                                  largeDeviceSize="6"
                                  mediumDeviceSize="6"
                                  smallDeviceSize="12"
                                  padding="around-small">
                <div class="slds-form-element">
                    <label class="slds-form-element__label" for="firstName">
                        <abbr class="slds-required" title="required">*</abbr> {!v.firstNameLabel}</label>
                    <div class="slds-form-element__control">
                        <input id="firstName"
                               aura:id="firstNameInput"
                               class="slds-input"
                               type="text"
                               tabindex="3"
                               value="{!v.firstName}"
                               placeholder="{!v.firstNamePlaceholder}"
                               required="true"
                               disabled="{!v.disableInputFields}"
                               onfocus="{!c.closeError}"
                               onkeyup="{!c.validateFirstName}"
                               onchange="{!c.validateFirstName}"
                               onblur="{!c.validateFirstName}"/>
                    </div>
                    <div aura:id="firstNameErrorMessage" class="slds-form-element__help slds-hide">First Name is
                        required.
                    </div>
                </div>
            </lightning:layoutItem>

            <lightning:layoutItem class="{!v.showRegister ? 'slds-show' : 'slds-hide'}" aura:id="lastNameField"
                                  size="12"
                                  largeDeviceSize="6"
                                  mediumDeviceSize="6"
                                  smallDeviceSize="12" padding="around-small">
                <div class="slds-form-element">
                    <label class="slds-form-element__label" for="lastName">
                        <abbr class="slds-required" title="required">*</abbr> {!v.lastNameLabel}</label>
                    <div class="slds-form-element__control">
                        <input id="lastName"
                               aura:id="lastNameInput"
                               class="slds-input"
                               type="text"
                               tabindex="4"
                               value="{!v.lastName}"
                               placeholder="{!v.lastNamePlaceholder}"
                               required="true"
                               disabled="{!v.disableInputFields}"
                               onfocus="{!c.closeError}"
                               onkeyup="{!c.validateLastName}"
                               onchange="{!c.validateLastName}"
                               onblur="{!c.validateLastName}"/>
                    </div>
                    <div aura:id="lastNameErrorMessage" class="slds-form-element__help slds-hide">Last Name is
                        required.
                    </div>
                </div>
            </lightning:layoutItem>

            <lightning:layoutItem class="{!v.showRegister ? 'slds-show' : 'slds-hide'}" aura:id="countryField"
                                  size="12"
                                  largeDeviceSize="12"
                                  mediumDeviceSize="12"
                                  smallDeviceSize="12" padding="around-small">

                <div class="slds-form-element">
                    <label class="slds-form-element__label" for="countries">
                        <abbr class="slds-required" title="required">*</abbr> {!v.countryLabel}</label>
                    <div class="slds-form-element__control">
                        <div class="slds-select_container">
                            <select id="countries"
                                    aura:id="countriesInput"
                                    class="slds-select"
                                    required="true"
                                    disabled="{!v.disableInputFields}"
                                    onkeyup="{!c.validateCountry}"
                                    onfocus="{!c.closeError}"
                                    onblur="{!c.validateCountry}"
                                    onchange="{!c.validateCountry}"
                                    tabindex="5">
                                <aura:iteration items="{!v.countryValues}" var="cv">
                                    <option label="{!cv.label}" disabled="{!cv.disabled}"
                                            selected="{!cv.selected}">{!cv.label}</option>
                                </aura:iteration>
                            </select>
                        </div>
                    </div>
                    <div aura:id="countryErrorMessage" class="slds-form-element__help slds-hide">Country is required.
                    </div>
                </div>
            </lightning:layoutItem>
            
            
            
            <lightning:layoutItem class="{!v.showRegister ? 'slds-show' : 'slds-hide'}" aura:id="subscribeField"
                                  size="12"
                                  largeDeviceSize="12"
                                  mediumDeviceSize="12"
                                  smallDeviceSize="12"
                                  padding="around-small">
                <div class="slds-form-element">
                    
                    <div class="slds-form-element__control">
						<ui:inputCheckbox aura:id="subscriptionsInput"
                               class="slds-checkbox"
                               value="false"/>  
                        <label class="slds-form-element__label">{!v.subscribeLabel}</label>
                    </div>
                </div>
            </lightning:layoutItem>

            <lightning:layoutItem class="{!v.showRegister ? 'slds-show' : 'slds-hide'}" aura:id="termsConditionsField"
                                  size="12"
                                  largeDeviceSize="12"
                                  mediumDeviceSize="12"
                                  smallDeviceSize="12"
                                  padding="around-small">
                <div class="slds-form-element">
                    <label class="slds-form-element__label"><p>I have read and agree to be bound by the <a href='https://www.charteredaccountantsanz.com/terms-of-use' style="color:rgb(0, 163, 221)" target="_blank">Terms of Use</a>.</p>  I have read and accept the terms of the <a href='https://www.charteredaccountantsanz.com/collection-statement' style="color:rgb(0, 163, 221)" target="_blank">Collection Statement</a> and CA ANZ's <a href='https://www.charteredaccountantsanz.com/privacy-policy' style="color:rgb(0, 163, 221)" target="_blank">Privacy Policy</a></label>
                    <div class="slds-form-element__control">
						<ui:inputCheckbox aura:id="termsConditionsInput"
                               class="slds-checkbox"
                               value="{!v.termsConditions}"
                               change="{!c.validateTermsConditions}"
                               required="true"
                        />  
                    </div>
                    <div aura:id="termsConditionsInvalidMessage" class="slds-form-element__help slds-hide">Please check to agree</div>
                </div>
            </lightning:layoutItem>
  

            <lightning:layoutItem class="{!v.showRegister ? 'slds-show' : 'slds-hide'}"
                                  size="12"
                                  padding="around-small">
                <lightning:button variant="brand"
                                  label="{!v.register}"
                                  onclick="{!c.doSave}"
                                  tabindex="6"
                                  disabled="{!v.disableRegistrationButton}"
                                  class="registerButton slds-size_full"
                                  />
            </lightning:layoutItem>
            <!-- End Registration Section-->

            <!-- Start Response Message -->
            <lightning:layoutItem class="{!v.showResponseMessage ? 'slds-show' : 'slds-hide'}"
                                  size="12"
                                  padding="around-small">
                <div class="slds-text-body--regular slds-text-align--center">
                    <div>{!v.responseMessageLine1}</div>
                    <br/>
                    <div>{!v.responseMessageLine2}</div>

                </div>
            </lightning:layoutItem>
        </lightning:layout>
        <!-- End Response Message -->

        <!-- Start Resend Section -->
        <lightning:layout horizontalAlign="center">
            <lightning:layoutItem class="{!(v.showResendButton ? 'slds-size_full slds-show ' : 'slds-hide ') + ''}">
                <lightning:button variant="destructive"
                                  label="{!v.resend}"
                                  onclick="{!c.resendLink}"
                                  disabled="{!v.disableResendButton}"/>
            </lightning:layoutItem>
        </lightning:layout>
        <!-- End Resend Section -->
    </div>
</aura:component>