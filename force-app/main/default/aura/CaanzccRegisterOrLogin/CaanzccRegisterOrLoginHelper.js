({
    sendToService: function (component, method, params, callback) {
        var action = component.get(method);
        if (params) {
            action.setParams(params);
        }
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                callback(response.getReturnValue());
            } else {
                helper.setResponseMessage(component, event, helper, "v.nonRepeatableErrorMessage", null);
            }
        });
        $A.enqueueAction(action);
    },
    setSelectOptions : function(component, event, helper, values, attribute, field){
        var options = [];
        options.push({"class": "optionClass", selected: true, disabled: true, label: component.get("v.selectOptionPlaceholder"), value: component.get("v.selectOptionPlaceholder")});
        for(var i = 0; i < values.length; i++){
            options.push({"class": "optionClass", selected: false, disabled: false, label: values[i], value: values[i]});
        }
        component.set(field, options[0].label);
        component.set(attribute, options);
    },
    validateEmailAtService: function (component, event, helper) {

        var email = component.get("v.email");
        helper.disableEmailBeforeAuraCall(component, event, helper);

        helper.sendToService(component, "c.checkUser", {email: email}, function (val) {
            helper.enableEmailAfterAuraCall(component, event, helper);

            console.log('response : ' + val);

            if (val == 'REGISTER') {
                helper.showRegistration(component, event, helper);
            } else if (val == 'EXPIRED') {
                helper.showResend(component, event, helper, "v.expired");
            } else if (val == 'LOGIN') {
                helper.showLogin(component, event, helper);
            } else if (val == 'NON_REPEATABLE_ERROR') {
                helper.setResponseMessage(component, event, helper, "v.nonRepeatableErrorMessage", null);
            } else if (val == 'INACTIVE') {
                helper.setResponseMessage(component, event, helper, "v.inactiveLine1", null);
            } else if (val == 'PASSWORD_NOT_SET') {
                helper.showResend(component, event, helper, "v.passwordNotSet");
            }
        });
    },
    doSave : function(component, event, helper) {
        helper.disableRegisterBeforeAuraCall(component, event, helper);

        var email = component.get("v.email");
        var salutation = component.get("v.salutation");
        var firstName = component.get("v.firstName");
        var subscribe = component.find("subscriptionsInput").get("v.value"); 
        var termsConditions = component.find("termsConditionsInput").get("v.value"); 
        var lastName = component.get("v.lastName");
        var country = component.get("v.country");
        var destination = component.get("v.destination");

        console.log(email + ' ' + firstName + ' ' + lastName + ' ' + country + ' ' + subscribe);

        helper.sendToService(component, "c.save", {
            email: email,
            salutation: salutation,
            firstName: firstName,
            lastName: lastName,
            country: country,
            destination: destination,
            subscribe : subscribe,
            termsConditions : termsConditions
            
        }, function (val) {
            console.log('response : ' + val)
            if (val == 'SUCCESS') {
                helper.setResponseMessage(component, event, helper, "v.successLine1", "v.successLine2");
            } else if (val == 'REPEATABLE_ERROR') {
                helper.enableRegisterAfterAuraCall(component, event, helper);
                component.set("v.errorMessage", component.get("v.repeatableErrorMessage"));
                component.set("v.showErrorMessage", true);
            } else {
                helper.setResponseMessage(component, event, helper, "v.nonRepeatableErrorMessage", null);
            }
        });
    },
    doLogin: function (component, event, helper) {
        var email = component.get("v.email");
        var password = component.get("v.password");

        helper.disableLoginBeforeAuraCall(component, event, helper);

        helper.sendToService(component, "c.login", {
            username: email,
            password: password,
            url: component.get("v.destination")
        }, function (val) {
            if (val.SUCCESS != undefined) {
                var routerPageUrl = cmp.get("v.routerPageUrl");
                if(!$A.util.isEmpty(routerPageUrl)){
                    window.location.href = routerPageUrl;
                } else {
                    window.location.href = val.SUCCESS;
                }
            } else {
                helper.enableLoginAfterAuraCall(component, event, helper);
                component.set("v.errorMessage", component.get("v.invalidPassword"));
                component.set("v.showErrorMessage", true);
            }
        });
    },
    setResponseMessage : function(component, event, helper, line1, line2){
        if(line1 != null) component.set("v.responseMessageLine1", component.get(line1));
        if(line2 != null) component.set("v.responseMessageLine2", component.get(line2));
        component.set("v.showResponseMessage", true);
        helper.hideLogin(component, event, helper);
        helper.hideRegistration(component, event, helper);
        helper.hideResend(component, event, helper);
        component.set("v.showRegister", false);
        component.set("v.showEmailField", false);
        component.set("v.showHeaderMessage", false);
        component.set("v.showEmailNextBtn", false);
        component.set("v.showInstruction", false);
    },
    showRegistration : function(component, event, helper){
        helper.hideLogin(component, event, helper);
        helper.hideResend(component, event, helper);
        helper.showError(component, "termsConditionsField", "termsConditionsInvalidMessage");
        component.set("v.showEmailNextBtn", false);
        component.set("v.disableInputFields", false);
        component.set("v.showRegister", true);
        component.set("v.instructionMessage", component.get("v.registrationInstructionMessage"));
        component.set("v.showInstruction", true);
        window.setTimeout(
            $A.getCallback(function () {
                var elem = component.find("salutationInput").getElement();
                if (!$A.util.isEmpty(elem)) {
                    elem.focus();
                }
            }), 200
        );

    },

    hideRegistration : function(component, event, helper){
        component.set("v.showRegister", false);
        component.set("v.showInstruction", false);
    },
    showLogin : function(component, event, helper){
        helper.hideRegistration(component, event, helper);
        helper.hideResend(component, event, helper);
        component.set("v.headerMessage", component.get("v.headerMessageLogin"));
        component.set("v.showEmailNextBtn", false);
        component.set("v.showLogin", true);
        component.set("v.disableInputFields", false);
        component.set("v.instructionMessage", component.get("v.loginInstructionMessage"));
        component.set("v.showInstruction", true);
    },
    hideLogin : function(component, event, helper){
        component.set("v.headerMessage", component.get("v.headerMessageRegistration"));
        component.set("v.showLogin", false);
        component.set("v.showInstruction", false);
    },
    showResend : function(component, event, helper, instruction){
        helper.hideRegistration(component, event, helper);
        helper.hideLogin(component, event, helper);
        component.set("v.showResendButton", true);
        component.set("v.headerMessage", component.get("v.loginInstructionMessage"));
        component.set("v.showEmailNextBtn", false);
        component.set("v.instructionMessage", component.get(instruction));
        component.set("v.showInstruction", true);
    },
    hideResend : function(component, event, helper){
        component.set("v.showResendButton", false);
        component.set("v.showInstruction", false);
    },
    enableDisableRegistrationBtn: function (component, event, helper, complete) {
        var salutation = component.get("v.salutation");
        var firstName = component.get("v.firstName");
        var lastName = component.get("v.lastName");
        var country = component.get("v.country");
        var termsConditions = component.find("termsConditionsInput").get("v.value");
        if ($A.util.isEmpty(salutation) ||
            salutation == component.get("v.selectOptionPlaceholder") ||
            $A.util.isEmpty(firstName) ||
            $A.util.isEmpty(lastName) ||
            $A.util.isEmpty(country) ||
            country == component.get("v.selectOptionPlaceholder") ||
            termsConditions == false) {
            component.set("v.disableRegistrationButton", true);
            complete(false);
        } else {
            component.set("v.disableRegistrationButton", false);
            complete(true);
        }
    },
    enableDisableLoginBtn: function (component, event, helper) {
        helper.validatePassword(component, event, helper, true, function(valid){
            if (valid) {
                component.set("v.disableLoginButton", false);
            } else {
                component.set("v.disableLoginButton", true);
            }
        });
    },
    enableDisableNextBtn: function (component, event, helper) {
        helper.validateEmail(component, event, helper, false, function(valid){
            if (valid) {
                component.set("v.disableNextButton", false);
                if(event.keyCode == 13) helper.validateEmailAtService(component, event, helper);
            } else {
                component.set("v.disableNextButton", true);
            }
        });
    },
    showNextBtn: function (component, event, helper) {
        component.set("v.showEmailNextBtn", true);
        component.set("v.disableInputFields", true);
        component.set("v.disableLoginButton", true);
        component.set("v.disableRegistrationButton", true);
        helper.hideAllErrors(component, event, helper);
    },
    disableEmailBeforeAuraCall : function(component, event, helper){
        component.set("v.disableNextButton", true);
        component.set("v.disableEmailField", true);
        component.set("v.next", component.get("v.emailButtonLabelActive"));
    },
    enableEmailAfterAuraCall : function(component, event, helper){
        component.set("v.next", component.get("v.emailButtonLabel"));
        component.set("v.disableNextButton", false);
        component.set("v.disableEmailField", false);
    },
    disableRegisterBeforeAuraCall : function(component, event, helper){
        component.set("v.register", component.get("v.registerButtonLabelActive"));
        component.set("v.disableRegistrationButton", true);
        component.set("v.disableInputFields", true);
        component.set("v.disableEmailField", true);
    },
    enableRegisterAfterAuraCall : function(component, event, helper){
        component.set("v.disableInputFields", false);
        component.set("v.register", component.get("v.registerButtonLabel"));
        component.set("v.disableRegistrationButton", false);
        component.set("v.disableEmailField", false);
    },
    disableLoginBeforeAuraCall : function(component, event, helper){
        component.set("v.login", component.get("v.loginButtonLabelActive"));
        component.set("v.disableLoginButton", true);
        component.set("v.disableInputFields", true);
        component.set("v.disableEmailField", true);
    },
    enableLoginAfterAuraCall : function(component, event, helper){
        component.set("v.login", component.get("v.loginButtonLabel"));
        component.set("v.disableLoginButton", false);
        component.set("v.disableInputFields", false);
        component.set("v.disableEmailField", false);
    },
    disableResendBeforeAuraCall : function(component, event, helper){
        component.set("v.resend", component.get("v.requestLinkButtonLabelActive"));
        component.set("v.disableResendButton", true);
        component.set("v.disableEmailField", true);
    },
    enableResendAfterAuraCall : function(component, event, helper){
        component.set("v.resend", component.get("v.requestLinkButtonLabel"));
        component.set("v.disableResendButton", false);
        component.set("v.disableEmailField", false);
    },
    validateSalutation : function(component, event, helper){
        helper.updateFieldAttribute(component, event, helper, "v.salutation");

        var value = component.get("v.salutation");
        var registrationComplete = false;

        helper.validateSelectOption(component, event, helper, value, function(valid){
            helper.enableDisableRegistrationBtn(component, event, helper, function(complete){
                registrationComplete = complete ? true : false;
            });
            if(!valid) {
                //prevent error message to show if field is accessed via tab
                if(event.keyCode != 9) helper.showError(component, "salutationField", "salutationErrorMessage");
            } else {
                helper.hideError(component, "salutationField", "salutationErrorMessage");
                if(registrationComplete && event.keyCode == 13) helper.doSave(component, event, helper);
            }
        });

    },
    validateCountry : function(component, event, helper){
        helper.updateFieldAttribute(component, event, helper, "v.country");

        var value = component.get("v.country");
        var registrationComplete = false;
        helper.validateSelectOption(component, event, helper, value, function(valid){
            helper.enableDisableRegistrationBtn(component, event, helper, function(complete){
                registrationComplete = complete ? true : false;
            });
            if(!valid) {
                //prevent error message to show if field is accessed via tab
                if(event.keyCode != 9) helper.showError(component, "countryField", "countryErrorMessage");
            } else {
                helper.hideError(component, "countryField", "countryErrorMessage");
                if(registrationComplete && event.keyCode == 13) helper.doSave(component, event, helper);
            }
        });
    },
    validateFirstName : function(component, event, helper){
        helper.updateFieldAttribute(component, event, helper, "v.firstName");

        var value = component.get("v.firstName");
        var registrationComplete = false;

        helper.validateNullOrEmpty(component, event, helper, value, function(valid){
            helper.enableDisableRegistrationBtn(component, event, helper, function(complete){
                registrationComplete = complete ? true : false;
            });
            if(!valid) {
                //prevent error message to show if field is accessed via tab
                if(event.keyCode != 9) helper.showError(component, "firstNameField", "firstNameErrorMessage");
            } else {
                helper.hideError(component, "firstNameField", "firstNameErrorMessage");
                if(registrationComplete && event.keyCode == 13) helper.doSave(component, event, helper);
            }
        });
    },
    validateLastName : function(component, event, helper){
        helper.updateFieldAttribute(component, event, helper, "v.lastName");

        var value = component.get("v.lastName");
        var registrationComplete = false;

        helper.validateNullOrEmpty(component, event, helper, value, function(valid){
            helper.enableDisableRegistrationBtn(component, event, helper, function(complete){
                registrationComplete = complete ? true : false;
            });
            if(!valid) {
                //prevent error message to show if field is accessed via tab
                if(event.keyCode != 9) helper.showError(component, "lastNameField", "lastNameErrorMessage");
            } else {
                helper.hideError(component, "lastNameField", "lastNameErrorMessage");
                if(registrationComplete && event.keyCode == 13) helper.doSave(component, event, helper);
            }
        });
    },
    validatePassword : function(component, event, helper, showError, isValid){
        helper.updateFieldAttribute(component, event, helper, "v.password");

        var value = component.get("v.password");

        helper.validateNullOrEmpty(component, event, helper, value, function(valid){
            if(!valid) {
                //prevent error message to show if field is accessed via tab
                if(event.keyCode != 9) helper.showError(component, "passwordField", "passwordErrorMessage");
                isValid(false);
            } else {
                helper.hideError(component, "passwordField", "passwordErrorMessage");
                isValid(true);
            }
        });
    },
    validateEmail : function(component, event, helper, showError, isValid){
        helper.updateFieldAttribute(component, event, helper, "v.email");
        var value = component.get("v.email");
        helper.validateNullOrEmpty(component, event, helper, value, function(valid){
            if(!valid) {
                //prevent error message to show if field is accessed via tab
                if(event.keyCode != 9) helper.showError(component, "emailField", "emailInvalidMessage");
                isValid(false);
                return;
           } else {
                helper.hideError(component, "emailField", "emailInvalidMessage");
           }
        });

        if(!this.isValidEmail(value)){
            if(showError) helper.showError(component, "emailField", "emailInvalidMessage");
            isValid(false);
        } else {
            helper.hideError(component, "emailField", "emailInvalidMessage");
            isValid(true);
        }

    },
    isValidEmail : function(email){
        if($A.util.isEmpty(email)) {
            return false;
        } else {
            var atIndex = email.indexOf('@');
            var dotIndex = email.lastIndexOf('.');
            var validAt = atIndex != -1 && atIndex != 0 && atIndex != email.length - 1;
            var validDot = dotIndex != -1 && dotIndex != 0 && dotIndex != email.length -1;
            var validPositions = dotIndex > atIndex;
            var noDotAfterAt = email.substring(atIndex +1 , atIndex + 2) != '.';

            return validAt && validDot && validPositions && noDotAfterAt;
        }
    },
    validateNullOrEmpty : function (component, event, helper, value, isValid){
        if($A.util.isEmpty(value) || $A.util.isUndefinedOrNull(value)){
            isValid(false);
        } else {
            isValid(true);
        }

    },
    validateSelectOption : function (component, event, helper, value, isValid){
        if(value == component.get("v.selectOptionPlaceholder")){
            isValid(false);
        } else {
            isValid(true);
        }

    },
    showError : function(component, fieldContainer, fieldErrorMessage){
        var field = component.find(fieldContainer);
        var fieldMessage = component.find(fieldErrorMessage);
        $A.util.addClass(field, 'slds-has-error');
        $A.util.addClass(fieldMessage, 'slds-show');
        $A.util.removeClass(fieldMessage, 'slds-hide');
    },
    hideError : function(component, fieldContainer, fieldErrorMessage){
        var field = component.find(fieldContainer);
        var fieldMessage = component.find(fieldErrorMessage);
        $A.util.removeClass(field, 'slds-has-error');
        $A.util.removeClass(fieldMessage, 'slds-show');
        $A.util.addClass(fieldMessage, 'slds-hide');
    },
    parseURL: function () {
        var result = {};
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        
        console.log('### Parse URL - Query ### - '+query);
        console.log('### Parse URL - Vars split by & ### - '+vars);
        
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            result[pair[0]] = decodeURIComponent(pair[1]);
        }
        console.log('### Parse URL - Result ### - '+result);
        return result;
    },
    closeError: function (component, event, helper) {
        component.set("v.showErrorMessage", false);
    },
    updateFieldAttribute : function(component, event, helper, field){
        var value = event.target.value;
        component.set(field, value);
    },
    hideAllErrors : function(component, event, helper){
        helper.hideError(component, "salutationField", "salutationErrorMessage");
        helper.hideError(component, "countryField", "countryErrorMessage");
        helper.hideError(component, "firstNameField", "firstNameErrorMessage");
        helper.hideError(component, "lastNameField", "lastNameErrorMessage");
        helper.hideError(component, "passwordField", "passwordErrorMessage");
        helper.hideError(component, "emailField", "emailInvalidMessage");
        helper.hideError(component, "termsConditionsField", "termsConditionsInvalidMessage")
    }
})