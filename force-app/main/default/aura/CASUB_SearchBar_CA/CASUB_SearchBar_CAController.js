({
    searchKeyChange: function(component, event, helper) {
        var searchKey = event.target.value ;
        if(searchKey){
            var myEvent = $A.get("e.c:CASUB_SearchKeyChange_CA");
            myEvent.setParams({"searchKey": event.target.value});
            myEvent.fire();
        }   
         
    }
})