({
    doInit : function(cmp, event, helper) {

        var action = cmp.get("c.getCPDSummariesForUser");

        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            var rtnValue = response.getReturnValue();

            if (state === "SUCCESS") {
                // Alert the user with the value returned 
                // from the server
                //alert("From server: " + response.getReturnValue());
                console.log('rtnValue='+rtnValue);
                $A.log('rtnValue='+rtnValue);
                if (rtnValue !== null) {
                    console.log('Not Null - List Length =  '+rtnValue.length);
                    for (var i = 0; i < rtnValue.length; i++){
                        if (rtnValue[i].Current_Triennium__c == true){
                            console.log('Found Current Triennium - Start Date = '+ rtnValue[i].Triennium_Start_Date__c);
                            cmp.set('v.cpdEarnedThisTriennium',rtnValue[i].CPD_Earned_This_Triennium__c);                            
                            cmp.set('v.requiredTotalTrienniumHours',rtnValue[i].Required_Total_Triennium_Hours__c);                            
                            cmp.set('v.totalFormalVerifiableHours',rtnValue[i].Total_Formal_Verifiable_Hours__c);                            
                            cmp.set('v.maximumTrenniumInformalHours',rtnValue[i].Maximum_Triennium_Informal_Hours__c);                            
                            cmp.set('v.cpdEarnedThisYear',rtnValue[i].CPD_Earned_This_Year__c);                            
                            cmp.set('v.requiredTotalAnnualHours',rtnValue[i].Required_Total_Annual_Hours__c);                            
                            cmp.set('v.totalExemptHours',rtnValue[i].Total_Exempt_Hours__c);                            
                            cmp.set('v.totalFormalExemptHours',rtnValue[i].Total_Formal_Exempt_Hours__c);                            
                        }

                    }
                    console.log('Loop CompletedList  ');
                    cmp.set('v.cpdSummaryList',rtnValue);

                    if(typeof window.chartJSLoaded !== "undefined" && window.chartJSLoaded !== null){
                        console.log("Regenerating Chart with CPD data.")
                        helper.generateChart(cmp);
                    }
                }

                // You would typically fire a event here to trigger 
                // client-side notification that the server-side 
                // action is complete
                
               
            }
            else if (state === "INCOMPLETE") {
                alert("Something went wrong. Refresh the page.");
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);

          var action2 = cmp.get("c.CPDLogPendingConfirmation");
                  action2.setCallback(this, function(response2) {
                            var state2 = response2.getState();
            var rtnValue2 = response2.getReturnValue();

            if (state2 === "SUCCESS") {
                             console.log('rtnValue='+rtnValue2);
                $A.log('rtnValue='+rtnValue2);
                if (rtnValue2 !== null) {
                    
						cmp.set('v.countPendingConfirmation',rtnValue2);
                    if(rtnValue2>0)
                    {
                        	cmp.set('v.pendingConfirmation',true);
                    }
                    else
                    {
                        	cmp.set('v.pendingConfirmation',false);
                    }
                }
            }
                  });
        
        $A.enqueueAction(action2);
    }, 


    onRender : function(cmp, event, helper) {
        if(typeof window.chartJSLoaded !== "undefined" && window.chartJSLoaded !== null){
            console.log("Regenerating Chart after render.")
            helper.generateChart(cmp);
        }
    },

    handleChartJSLoaded: function(cmp, event, helper) {
        console.log("ChartJS has loaded.")
        window.chartJSLoaded = true;

        helper.extendChartJS();
        helper.generateChart(cmp);
    },

    goToCPD: function(cmp, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/cpd-summary-list"
        });
        urlEvent.fire();
    }


})