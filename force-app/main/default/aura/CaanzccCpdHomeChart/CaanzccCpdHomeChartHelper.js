({
    generateChart: function(cmp) {

        // Completed: CPD Earned This Triennium / Required Total Triennium Hours
        var cpdEarnedThisTriennium = cmp.get("v.cpdEarnedThisTriennium");
        var requiredTotalTrienniumHours = cmp.get("v.requiredTotalTrienniumHours");
        var totalExemptHours = cmp.get("v.totalExemptHours");
        var possibleTrienniumHours = Math.max(0, requiredTotalTrienniumHours - totalExemptHours);
        var cpdTrienniumRemaining = Math.max(0, possibleTrienniumHours - cpdEarnedThisTriennium);


        // Verified: Total Formal Verifiable Hours / (Required Total Triennium Hours - Maximum Triennium Informal Hours)
        var totalFormalVerifiableHours = cmp.get("v.totalFormalVerifiableHours");
        var maximumTrenniumInformalHours = cmp.get("v.maximumTrenniumInformalHours");
        var totalFormalExemptHours = cmp.get("v.totalFormalExemptHours");
        var possibleFormalVerifiableHours = Math.max(0, requiredTotalTrienniumHours - maximumTrenniumInformalHours - totalFormalExemptHours);
        var formalVerifiableHoursRemaining = Math.max(0, possibleFormalVerifiableHours - totalFormalVerifiableHours);


        // Annual: CPD Earned This Year / Required Total Annual Hours
        var cpdEarnedThisYear = cmp.get("v.cpdEarnedThisYear");
        var requiredTotalAnnualHours = cmp.get("v.requiredTotalAnnualHours");
        var cpdAnnualRemaining = Math.max(0, requiredTotalAnnualHours - cpdEarnedThisYear);

        var donutChartData = {
            datasets: [{
                    data: [cpdEarnedThisTriennium, cpdTrienniumRemaining],
                    backgroundColor: [
                        "#00629E",
                        "#EFEFEF"
                    ],
                    borderWidth: 0,
                },
                {
                    data: [totalFormalVerifiableHours, formalVerifiableHoursRemaining],
                    backgroundColor: [
                        "#B7BF10",
                        "#EFEFEF"
                    ],
                    borderWidth: 0,
                },
                {
                    data: [cpdEarnedThisYear, cpdAnnualRemaining],
                    backgroundColor: [
                        "#EA7600",
                        "#EFEFEF"
                    ],
                    borderWidth: 0,
                }
            ]
        };
        var donutOptions = {

            tooltips: {
                enabled: false
            },
            legend: {
                display: false
            },
            elements: {
                arc: {
                    borderWidth: 20
                }
            },
            datasetRadiusBuffer: 5,
            cutoutPercentage: 113
        };

        var barChartData = {
            datasets: [{
                    data: [Math.min(1, cpdEarnedThisTriennium/possibleTrienniumHours)],
                    borderWidth: 1,
                    stack: 'stackOne',
                    backgroundColor: [
                        "#00629E"
                    ]
                },
                {
                    data: [Math.min(1, cpdTrienniumRemaining/possibleTrienniumHours)],
                    stack: 'stackOne',
                    backgroundColor: [
                        "#EFEFEF"
                    ]
                },
                {
                    data: [Math.min(1, totalFormalVerifiableHours/possibleFormalVerifiableHours)],
                    stack: 'stackTwo',
                    backgroundColor: [
                        "#B7BF10"
                    ]
                },
                {
                    data: [Math.min(1, formalVerifiableHoursRemaining/possibleFormalVerifiableHours)],
                    stack: 'stackTwo',
                    backgroundColor: [
                        "#EFEFEF"
                    ]
                },
                {
                    data: [Math.min(1, cpdEarnedThisYear/requiredTotalAnnualHours)],
                    stack: 'stackThree',
                    backgroundColor: [
                        "#EA7600"
                    ]
                },
                {
                    data: [Math.min(1, cpdAnnualRemaining/requiredTotalAnnualHours)],
                    stack: 'stackThree',
                    backgroundColor: [
                        "#EFEFEF"
                    ]
                }
            ]
        };
        var barOptions = {

            tooltips: {
                enabled: false
            },
            legend: {
                display: false,
                // position: 'bottom'
            },
            scales: {
                xAxes: [{
                    display: false,
                    gridLines: {
                        display: false
                    },
                    stacked: true,
                    ticks: {
                        display: false
                    }
                }],
                yAxes: [{
                    display: false,
                    gridLines: {
                        display: false
                    },
                    barPercentage: 0.4
                }]
            }
        }
        var ctx = cmp.find("donutTriple").getElement();
        var donutChart = new Chart(ctx, {
            type: "doughnut",
            options: donutOptions,
            data: donutChartData
        });
        var ctx = cmp.find("barKey").getElement();
        var donutChart = new Chart(ctx, {
            type: "horizontalBar",
            options: barOptions,
            data: barChartData
        });
    },

    extendChartJS: function() {
        var helpers = Chart.helpers;

        // this option will control the white space between embedded charts when there is more than 1 dataset
        helpers.extend(Chart.defaults.doughnut, {
          datasetRadiusBuffer: 0
        });

        Chart.controllers.doughnut = Chart.controllers.doughnut.extend({
          update: function(reset) {
            var me = this;
            var chart = me.chart,
                chartArea = chart.chartArea,
                opts = chart.options,
                arcOpts = opts.elements.arc,
                availableWidth = chartArea.right - chartArea.left - arcOpts.borderWidth,
                availableHeight = chartArea.bottom - chartArea.top - arcOpts.borderWidth,
                minSize = Math.min(availableWidth, availableHeight),
                offset = {
                  x: 0,
                  y: 0
                },
                meta = me.getMeta(),
                cutoutPercentage = opts.cutoutPercentage,
                circumference = opts.circumference;

            // If the chart's circumference isn't a full circle, calculate minSize as a ratio of the width/height of the arc
            if (circumference < Math.PI * 2.0) {
              var startAngle = opts.rotation % (Math.PI * 2.0);
              startAngle += Math.PI * 2.0 * (startAngle >= Math.PI ? -1 : startAngle < -Math.PI ? 1 : 0);
              var endAngle = startAngle + circumference;
              var start = {x: Math.cos(startAngle), y: Math.sin(startAngle)};
              var end = {x: Math.cos(endAngle), y: Math.sin(endAngle)};
              var contains0 = (startAngle <= 0 && 0 <= endAngle) || (startAngle <= Math.PI * 2.0 && Math.PI * 2.0 <= endAngle);
              var contains90 = (startAngle <= Math.PI * 0.5 && Math.PI * 0.5 <= endAngle) || (startAngle <= Math.PI * 2.5 && Math.PI * 2.5 <= endAngle);
              var contains180 = (startAngle <= -Math.PI && -Math.PI <= endAngle) || (startAngle <= Math.PI && Math.PI <= endAngle);
              var contains270 = (startAngle <= -Math.PI * 0.5 && -Math.PI * 0.5 <= endAngle) || (startAngle <= Math.PI * 1.5 && Math.PI * 1.5 <= endAngle);
              var cutout = cutoutPercentage / 100.0;
              var min = {x: contains180 ? -1 : Math.min(start.x * (start.x < 0 ? 1 : cutout), end.x * (end.x < 0 ? 1 : cutout)), y: contains270 ? -1 : Math.min(start.y * (start.y < 0 ? 1 : cutout), end.y * (end.y < 0 ? 1 : cutout))};
              var max = {x: contains0 ? 1 : Math.max(start.x * (start.x > 0 ? 1 : cutout), end.x * (end.x > 0 ? 1 : cutout)), y: contains90 ? 1 : Math.max(start.y * (start.y > 0 ? 1 : cutout), end.y * (end.y > 0 ? 1 : cutout))};
              var size = {width: (max.x - min.x) * 0.5, height: (max.y - min.y) * 0.5};
              minSize = Math.min(availableWidth / size.width, availableHeight / size.height);
              offset = {x: (max.x + min.x) * -0.5, y: (max.y + min.y) * -0.5};
            }

            chart.borderWidth = me.getMaxBorderWidth(meta.data);
            chart.outerRadius = Math.max((minSize - chart.borderWidth) / 2, 0);
            chart.innerRadius = Math.max(cutoutPercentage ? (chart.outerRadius / 100) * (cutoutPercentage) : 0, 0);
            chart.radiusLength = ((chart.outerRadius - chart.innerRadius) / chart.getVisibleDatasetCount()) + 25;
            chart.offsetX = offset.x * chart.outerRadius;
            chart.offsetY = offset.y * chart.outerRadius;

            meta.total = me.calculateTotal();

            me.outerRadius = chart.outerRadius - (chart.radiusLength * me.getRingIndex(me.index));
            me.innerRadius = Math.max(me.outerRadius - chart.radiusLength, 0);

            // factor in the radius buffer if the chart has more than 1 dataset
            if (me.index > 0) {
              me.outerRadius -= opts.datasetRadiusBuffer * me.index;
              me.innerRadius -= opts.datasetRadiusBuffer * me.index;
            }

            helpers.each(meta.data, function(arc, index) {
              me.updateElement(arc, index, reset);
            });
          },
        });
    }
})