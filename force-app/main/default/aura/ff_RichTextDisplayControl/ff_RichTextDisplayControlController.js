({
    load: function (component, event, helper) {
        var readData = event.getParam("value").data;
        var optionsKey = component.get("v.optionsKey");
        var wrappers = readData.wrappers[optionsKey];
        if (wrappers && !$A.util.isEmpty(wrappers)) {
            component.set("v.content", wrappers[0].richtext);
        }
    }
})