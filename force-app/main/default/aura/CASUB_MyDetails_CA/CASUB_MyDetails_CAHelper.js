({
	getAccountData: function(cmp) {
		this.enqueueAction(cmp, "getAccountData", {}, function(error, data){
			if(error) cmp.set("v.error", error);
			else cmp.set("v.data", JSON.parse(data));
		});
	},
	saveAccountData: function(cmp, user, account, contact, callback) {
        var params = { accountDataJSONString: JSON.stringify({ user: user, account: account, contact: contact }) };
		this.enqueueAction(cmp, "saveAccountData", params, function(error, data){
			if(!error){
				cmp.set("v.data.account", (data = JSON.parse(data)).account);
				cmp.set("v.data.contact", data.contact);
			}
			if(callback) callback(error);
		});
	},
	showModal: function(cmp, body, footer){
		var overlay = cmp.find('overlayLib');
		if(overlay){
			footer.set("v.saveFunction", this.performSave.bind(this, cmp, body));
			var params = {
	            header: "Edit " + cmp.get("v.title"), body: body, footer: footer, 
	            showCloseButton: true, closeCallback: this.getAccountData.bind(this, cmp)
	        };
			overlay.showCustomModal(params).then(function (overlay) { 
      		cmp.set('v.overlayPanel', overlay);
    });
		}
	},
	performSave: function(cmp, modalBody){ 
		var data = cmp.get("v.data");
        if($A.util.isEmpty(modalBody.get("v.contact.Email"))){
            modalBody.find('notifLib').showToast({ 
                    "variant":"warning",
                    "title": "Server Error!",
                    "message": "Email can not be empty"
                });
        }else{
            if(!$A.util.isEmpty(data)) {
                modalBody.set("v.showSpinner", true);
                modalBody.set("v.error", "");
                this.saveAccountData(cmp, data.user, data.account, data.contact, function(error){
                    modalBody.set("v.showSpinner", false);
                    if(error){  
                    modalBody.find('notifLib').showToast({ 
                        "variant":"warning",
                        "title": "Server Error!",
                        "message": error
                    });
                    } else {
                        var overlayPanel = cmp.get('v.overlayPanel');
                        overlayPanel[0].close();
                        //var overlay = cmp.find('overlayLib');
                        //if(overlay) overlay.notifyClose();
                        //$A.get('e.force:refreshView').fire()
                    }
                });
            }
         }
	},
	enqueueAction: function(cmp, method, params, callback){
		this.toggleSpinner(cmp, true);
		var action = cmp.get("c." + method); 
		if(params) action.setParams(params);
		action.setCallback(this, function(response){
			if(response.getState() === "SUCCESS") {
				if(callback) callback.call(this, null, response.getReturnValue());
	        } else if(response.getState() === "ERROR") {
	        	var message = 'Unknown error'; 
	        	var errors = response.getError();
	            if (!$A.util.isEmpty(errors)) {
	                message = errors[0].message;
	            }
	        	console.error(message);
	        	if(callback) callback.call(this, message);
	        }
	        this.toggleSpinner(cmp, false);
		});
		$A.enqueueAction(action);
	},
	toggleSpinner: function(cmp, show) {
		var spinner = cmp.find("spinner");
		$A.util.addClass(spinner, show ? "slds-show" : "slds-hide");
		$A.util.removeClass(spinner, !show ? "slds-show" : "slds-hide");
	}
})