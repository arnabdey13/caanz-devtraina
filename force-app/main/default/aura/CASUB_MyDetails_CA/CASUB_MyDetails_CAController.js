({
    doInit: function(cmp, event, helper) {
        helper.getAccountData(cmp);
    },
    handleShowModal: function(cmp, event, helper) {
        var data = cmp.get("v.data");
        var configs = [
            ['c:CASUB_MyDetailsForm_CA', {
                mode: "edit",
                user: cmp.getReference("v.data.user"),
                account: cmp.getReference("v.data.account"),
                contact: cmp.getReference("v.data.contact"),
                accountFields: data.accountFields,
                contactFields: data.contactFields 
            }],
            ['c:CASUB_ModelFooter_CA', {}],
        ];
            $A.createComponents(configs, function(components, status) {
            if (status === "SUCCESS") {
            helper.showModal(cmp, components[0], components[1]);
    }
});
},
    doAction : function(cmp, event, helper){
        cmp.set("v.error", "");
        var params = event.getParam ? event.getParam("arguments").params || {} : {};
        alert('1');
        if(params.callback) {
            params.callback();
        }
    }
})