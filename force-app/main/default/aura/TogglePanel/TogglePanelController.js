({
	toggleVisibility : function(component, event, helper) {
		$A.util.toggleClass(component.find("toggle-show"), "slds-hide");
		$A.util.toggleClass(component.find("icon-show"), "slds-hide");
		$A.util.toggleClass(component.find("toggle-hide"), "slds-hide");
		$A.util.toggleClass(component.find("icon-hide"), "slds-hide");
        $A.util.toggleClass(component.find("toggled"), "slds-hide");
	}
})