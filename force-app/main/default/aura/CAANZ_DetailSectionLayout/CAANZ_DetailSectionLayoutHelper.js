({
    showrecordForm: function(component){
        var action = component.get("c.fieldsRelatedtoPageLayout");
        action.setParams({objectName:"Account",
                          pageLayoutName: component.get("v.LayoutName")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //console.log(JSON.stringify(response.getReturnValue()));
                component.set("v.layoutSections", response.getReturnValue() );
            }
            else if (state === "INCOMPLETE") {
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            component.set("v.showSpinner", false);   
        });
        $A.enqueueAction(action);
    },
    showHide : function(component) {
        var editForm = component.find("objectEditForm");
        $A.util.toggleClass(editForm, "slds-hide");
        var viewForm = component.find("objectViewForm");
		$A.util.toggleClass(viewForm, "slds-hide");
    }
})