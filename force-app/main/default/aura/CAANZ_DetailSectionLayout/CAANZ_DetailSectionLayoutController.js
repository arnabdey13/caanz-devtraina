({
    doInit: function( component, event, helper ) {
        helper.showrecordForm(component);
    },
    activeSection : function(component, event, helper) {                  
        var tab = event.getSource();
        var tabId = tab.get("v.id");
        component.set("v.sectionName",tabId);
    },
    handleSelect : function(component, event, helper) {
        helper.showHide(component);
        event.preventDefault();
    },
    editRecord : function(component, event, helper) {
        helper.showHide(component);
    },
    handleLoad : function(component, event, helper) {
        component.set("v.showSpinner", false);   
    },
    handleHideCmp : function(component, event, helper) {
        helper.showHide(component); 
    },
})