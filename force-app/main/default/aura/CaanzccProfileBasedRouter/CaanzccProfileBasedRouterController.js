({
	doInit: function(cmp, event, helper) {
            var profiles = cmp.get("v.profiles");
        	helper.sendToService(cmp, "c.load", null, function (destination) {
            	cmp.set("v.destination", destination);
                if(!$A.util.isUndefinedOrNull(profiles)){
                	helper.getCurrentUserProfile(cmp, profiles.split(","));    
            	}                
                
            });
        
           
  	}
	
})