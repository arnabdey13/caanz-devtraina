({
	getCurrentUserProfile: function(cmp, profiles) {
		this.enqueueAction(cmp, "getCurrentUserProfile", {}, function(data){
			var profileName = data.toLowerCase();
			var found = profiles.find(function(profile){ return profile.trim().toLowerCase().includes(profileName); });
            var URLvar = 'destination';
            var name = URLvar.replace(/[\[\]]/g, "\\$&");
            var url = window.location.href;
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
            var results = regex.exec(url);
            var deeplink = "/s/";
            
            //only deeplink the destination if it isn't null
            if (cmp.get("v.destination") != null) {
               deeplink = cmp.get("v.destination");
            }
            
            //migration of /provisionalapplication to /s/provisionalapplication
            if (deeplink == "/provisionalapplication") {
               deeplink = "/s/provisionalapplication" 
            }

            //No preconfigured login URL
            if (results==null) {
                if(found) this.performRedirect("/MyCA"+deeplink);
                else this.performRedirect("/customer"+deeplink);
            }
                 
            //Preconfigured login URL
            if (decodeURIComponent(results[2].replace(/\+/g, " ")) == 'preferences')  
            {
                if(found) this.performRedirect("/MyCA/s/preferences");
                else this.performRedirect("/customer/s/subscription-wizard-page-5");
            } else {
                if(found) this.performRedirect("/MyCA"+deeplink);
                else this.performRedirect("/customer"+deeplink);
            }          
		});
	},
    
	enqueueAction: function(cmp, method, params, callback){
        console.log("enqueue start");
		var action = cmp.get("c." + method); 
		if(params) action.setParams(params);
		action.setCallback(this, function(response){
			if(response.getState() === "SUCCESS") {
                if(callback) {
                    console.log("enqueue complete");
                    callback.call(this, response.getReturnValue());
                }
                    
	        } else if(response.getState() === "ERROR") {
                console.log("enqueue error");
	        	console.error(response.getError());
	        }
		});
		$A.enqueueAction(action);
	},
    performRedirect: function(redirectUrl){
        console.log("Redirect URL is - "+redirectUrl);
		if($A.util.isUndefinedOrNull(redirectUrl)) return;
        window.location.href = redirectUrl;
	},
    sendToService : function(component, method, params, callback) {
        
        var action = component.get(method);
        if (params) {
            action.setParams(params);
        }
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                callback(response.getReturnValue());
            }else if (component.isValid() && state === "ERROR") {
                $A.error("service call ERROR");
                this.showUnexpectedError(component);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    $A.error("service call ERROR: "+errors[0].message);
                    this.showUnexpectedError(component);
                } else {
                    $A.error("Unknown error");
                    this.showUnexpectedError(component);
                }
            }
        });
        $A.enqueueAction(action);
    }
    
    
    
    
})