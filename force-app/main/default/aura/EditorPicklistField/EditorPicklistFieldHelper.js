({
    load : function(component, event, helper) {          
        var fieldName = component.get("v.field");
        var value = event.getParam("properties").RECORD[fieldName];
        if (!component.get("v.suppressLoadFromEvent")) {
            var options = this.getOptions(component);
            var myOptions = [];
            if (options) {
                for (i = 0; i < options.length; i++) { 
                    var opt = {label:options[i].label,
                               value:options[i].value} ;
                    myOptions.push(opt);
                }
                component.set("v.options", myOptions);
            }
            if(value && component.get("v.disableIfNull")){
                component.set("v.disabled", true);
            } 
        }
    },
    getOptions : function(component) {
        var opts = [];
        var propertyName = component.get("v.optionsProperty");
        if ($A.util.isEmpty(propertyName)) {
            var valList = component.get("v.valueList");
            if (! $A.util.isEmpty(valList)) {
                var vals = valList.split(",");
                var labels = component.get("v.labelList").split(",");                
                for (i = 0; i < vals.length; i++) { 
                    opts.push({value: vals[i], label: labels[i]});
                }
            }            
        } else {
            return component.get("v.properties")["prop."+propertyName];
        }
        return opts;        
    },
    validate : function(component) {
        var isRequired = component.get("v.required");
        var hidden = component.get("v.hidden");
        var value = component.get("v.value");
        var valueIsMissing = $A.util.isEmpty(value) || value == 'NONE-SELECTED';
        var isInvalidValue = this.checkInvalidValues(component);
        $A.log('validating picklist: '+component.get('v.field'),
               {value: value, hidden: hidden, isRequired: isRequired, missing: valueIsMissing});
        return hidden || (
            	(!isRequired || (isRequired && !valueIsMissing)) 
        		&& !isInvalidValue
            );
    },
    setUIValid : function(component, isValidClient, isValidServer) {
        if (! component.get("v.disabled")) {
            var editor = component.find("editor");
            if (isValidClient && isValidServer) {
                editor.set("v.errors", null);
                $A.util.removeClass(component, "slds-has-error");
            } else {
                var errorMessages = [] ;
                if(!isValidClient){
		            var invalidMessage = this.getInvalidValueMessageFromOptions(component);                    
                    errorMessages.push({message: $A.util.isEmpty(invalidMessage)?"This field is required":invalidMessage}) ;
                }
                
                if(!isValidServer && !isValidServer){                
                    var messages = component.get("v.apexErrors");
                    for(var i=0; i<messages.length; i++){
                        errorMessages.push({message: messages[i]}) ;
                    }
                }
                editor.set("v.errors", errorMessages);
                $A.util.addClass(component, "slds-has-error");
            }
        }
    }    
})