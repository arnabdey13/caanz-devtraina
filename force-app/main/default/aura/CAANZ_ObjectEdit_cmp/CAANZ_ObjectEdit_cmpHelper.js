({
    isFormValid: function (component) {
        return (component.find('requiredField') || [])
        .filter(function (i) {
            var value = i.get('v.value');
            return !value || value == '' || value.length === 0;
        })
        .map(function (i) {
            return i.get('v.fieldName');
        });
    },
    reportUserError : function(component,textmessage) {
        window.scrollTo(0, 0);
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Error!",
            "message": textmessage,
            "type": "warning"
        });
        toastEvent.fire();
    },
})