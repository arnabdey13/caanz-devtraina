({	
    handleSuccess : function(component, event, helper) { 
        component.set("v.showSpinner", false);          
        var compEvent = component.getEvent("componentEvent");
        compEvent.fire();
        var textmessage = component.get("v.sectionName") + " section has been updated.";
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": textmessage,
            "type": "success"
        });
        toastEvent.fire();
    },
    handleCancel : function(component, event, helper) {
        event.preventDefault();
        var compEvent = component.getEvent("componentEvent");
        compEvent.fire();
    },    
    handleLoad : function(component, event, helper) {
    },
    handleSubmit : function(component, event, helper) {
        component.set("v.showSpinner", true);   
        var invalidFields = helper.isFormValid(component);
        if(invalidFields && invalidFields.length > 0){
            component.set("v.showSpinner", false);         
            helper.reportUserError(component,'Please complete all required fields');
                //'Incomplete: '+ invalidFields.join(', ')
            event.preventDefault();
            return;
        }
    },
    handleError : function(component, event, helper) {
        component.set("v.showSpinner", false);          
        var params = event.getParams();    
        helper.reportUserError(params.message);
    },
})