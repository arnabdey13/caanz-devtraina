({    
    handleEducationRecordSaved : function(cmp,event,helper) {
        cmp.find("overlayLib").notifyClose();
	},

	performSubmit : function(cmp,event,helper) {
        cmp.set("v.disabled",true);
		cmp.find("recordEditForm").submit();
	},
    handleEducationRecordError : function(cmp,event,helper) {
        cmp.set("v.disabled",false);
	},    
    showRequiredFields: function(component, event, helper){
		$A.util.removeClass(component.find("Enter_university_instituition__c"), "none");
		$A.util.removeClass(component.find("Event_Course_name__c"), "none");
		$A.util.removeClass(component.find("Qualifying_hours_type__c"), "none");
		$A.util.removeClass(component.find("Qualifying_hours__c"), "none");
		$A.util.removeClass(component.find("Date_completed__c"), "none");
    }
})