({
    goLinkedIn: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://www.linkedin.com/company/chartered-accountants-australia-and-new-zealand"
        });
        urlEvent.fire();
    },
    goTwitter: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://twitter.com/Chartered_Accts"
        });
        urlEvent.fire();
    },
    goFacebook: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://www.facebook.com/charteredaccountants/"
        });
        urlEvent.fire();
    },
    goInsta: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://www.instagram.com/charteredaccountantsanz/"
        });
        urlEvent.fire();
    },
		goYoutube: function(component, event, helper) {
				var urlEvent = $A.get("e.force:navigateToURL");
				urlEvent.setParams({
						"url": "https://www.youtube.com/user/CharteredAccts"
				});
				urlEvent.fire();
		}
})