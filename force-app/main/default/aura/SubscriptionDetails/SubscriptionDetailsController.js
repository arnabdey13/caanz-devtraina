({
    saveAndExit : function(cmp) {
    },
    saveAndView : function(cmp) {
        var appEvent = $A.get("e.c:PageTransitionEvent");
        appEvent.setParams({source: cmp,
                            step : "next"}).fire();
    }	
})