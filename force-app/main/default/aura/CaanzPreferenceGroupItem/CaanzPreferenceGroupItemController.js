({
	change: function(cmp, event, helper) {
		var selection = cmp.get("v.selection") || [];
		selection.push(cmp.get("v.data.myPreference"));
		cmp.set("v.selection", selection);
		$A.enqueueAction(cmp.get("v.change"));
	}
})