({
    doInit: function (component, event, helper) {
        var withStyles = [];
        var labels = component.get("v.labels");
        if (!$A.util.isEmpty(labels)) {
            for (var i = 0; i < labels.length; i++) {
                var col = {label: labels[i]};
                var lastColStyle = component.get("v.lastColStyle");
                if (i == labels.length - 1 && !$A.util.isEmpty(lastColStyle)) {
                    col.style = lastColStyle;
                }
                withStyles.push(col);
            }
            component.set("v.labelsWithStyles", withStyles);
        }
    }
})