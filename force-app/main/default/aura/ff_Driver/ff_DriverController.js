({
    doInit: function (component, event, helper) {
        // use concreteComponent to access the specific apex controller
        var concreteComponent = component.getConcreteComponent(); 
        var applicationRecordType = concreteComponent.get("v.applicationRecordType"); // RXP - added recordtype to make it commonly accessible for all the forms
        console.log('x concrete attribute::::'+applicationRecordType);
        console.log('concreteComponent** '+concreteComponent);
        //helper.sendToService(concreteComponent, helper, "c.load", null, function (result) {
        helper.sendToService(concreteComponent, helper, "c.load", {rectype:applicationRecordType}, function (result) {
            try {

                // readData is used to track changes, generate writeData and undo.
                // also watched by concrete drivers to detect the apex load event
                component.set("v.readData", result);

                // allow concrete drivers to perform actions only before the initial load
                helper.fireComponentEvent(component, "lifeCycleEvent", {type: "pre-load"});

                // load all the fields in the page, including this driver for validation reporting
                helper.fireApplicationEvent(component,
                    "e.c:ff_EventLoadControls",
                    {
                        data: result,
                        driver: component
                    });

                component.set("v.loaded", true);

                //stage events are suppressed during load but one must be fired
                helper.updateStages(component);

                // allow concrete drivers to perform actions only after the initial load
                helper.fireComponentEvent(component, "lifeCycleEvent", {type: "post-load"});

                //set the layout to show all boxes if the builderMode is true
                helper.fireApplicationEvent(component,
                    "e.c:ff_EventBuilderConfig",
                    {
                        isBuilderMode: component.get("v.builderMode"),
                        showRhsBox: component.get("v.showRhsBox") //RXP - added to show the documents section at right
                    });
            } catch (e) {
                console.log(e.message);
                throw e;
            }
        });

        helper.setControlAttributes(component);
    },
    handleValueChange: function (component, event, helper) {
        var params = event.getParams();
        if (!params.ignore) { // fields may fire events that should not be sent to Apex i.e. dirty data
            $A.log("Driver sees value change: " + JSON.stringify(params));
            if ($A.util.isEmpty(params.loadKey)) {
                throw Error("loadKey absent in value change event: " + JSON.stringify(params));
            }
            if ($A.util.isEmpty(params.field)) {
                throw Error("field absent in value change event: " + JSON.stringify(params));
            }

            helper.adjustFocusIfRequired(component, params.loadKey);
            helper.setFieldValidity(component, params.loadKey, params.field, params.valid, params.stage);
            helper.setFocusedFieldValue(component, params.loadKey + "." + params.field, params.value);
            helper.restoreFocusIfRequired(component);
            helper.fireComponentEvent(component, "lifeCycleEvent", {type: "post-value-change", opts: params});
        }
    },
    handleValueRemove: function (component, event, helper) {
        var params = event.getParams();
        helper.removeFocusedFieldValue(component, params.loadKey, params.field);
    },
    handleAutoCompleteSearch: function (component, event, helper) {
        if (component.get("v.searchEnabled")) {
            var concreteComponent = component.getConcreteComponent();
           	var applicationRecordType = concreteComponent.get("v.applicationRecordType");
            if (event.getParam("allowDriverSearch")) {
                helper.sendToService(concreteComponent, helper, "c.search",
                    {
                        objectName: event.getParam("loadKey"),
                        fieldName: event.getParam("field"),
                        searchTerm: event.getParam("searchTerm"),
                        searchContext: event.getParam("searchContext"),
                        rectype:applicationRecordType
                    },
                    event.getParam("callback"));
            }
        }
    },
    handleUploadFile: function (component, event, helper) {
        var p = event.getParams();
        var isSourceProgressAware = p.source.isInstanceOf("c:ff_ProgressComponent");
        if (isSourceProgressAware) {
            p.source.set("v.percentComplete", 0);
        }
        var callback = helper.getUploadCallback(p.source); // no done callback required for application use, only in tests
        helper.uploadFile(component, p.parentId, p.fileName, p.withoutMark, p.contentType, p.source, callback);
    },
    handleSetFocusedRecord: function (component, event, helper) {
        var params = event.getParams();
        switch (params.operation) {
            case "CREATE":
                // recordPosition is how N tempids can stay unique
                var tempId = helper.tempIdInsert(params.loadKey + params.recordPosition);
                // position below is wrapper position
                helper.setFocusedRecord(component, tempId, params.loadKey, params.position);
                break;
            case "UPDATE":
                var rd = component.get("v.readData");
                // position below is wrapper position
                var wrapper = rd.records[params.loadKey][params.position];
                var position = wrapper.single ? 0 : params.recordPosition;
                helper.setFocusedRecord(component, wrapper.sObjectList[position].Id, params.loadKey, params.position);
                break;
            default:
                throw Error("Unsupported operation: " + params.operation);
        }
    },
    handleSaveFocused: function (component, event, helper) {
        
        helper.sendFocusedRecord(component, event.getParam("successMode"), event.getParam("callback"));
    },
    handleCRUDFocused: function (component, event, helper) {
        helper.updateControlsWithFocusedRecord(component, event.getParam("operation"));
    },
    handleDeleteRecord: function (component, event, helper) {
        var p = event.getParams();
        helper.deleteRecordFromReadData(component, p.loadKey, p.wrapperPosition, p.id);
        helper.deleteRecordWithApex(component, p.id, p.callback);
    },

    apiSetFieldValidity: function (component, event, helper) {
        var p = event.getParam("arguments");
        helper.setFieldValidity(component, p.loadKey, p.field, p.isValid, p.stage);
    },
    apiSetAggregateValidity: function (component, event, helper) {
        var p = event.getParam("arguments");
        helper.setAggregateValidity(component, p.aggregateKey, p.isValid, p.stage);
    },
    handleFormValidityChange: function (component, event, helper) {
        helper.updateStages(component);
        component.set("v.validityDataJSON", JSON.stringify(component.get("v.formValidity"), function (key, value) {
            return value;
        }, 3));
    },
    handleStageChange: function (component, event, helper) {
        helper.updateStages(component);
    },
    handleWriteDataChange: function (component, event, helper) {
        component.set("v.writeDataJSON", JSON.stringify(component.get("v.writeData"), function (key, value) {
            return value;
        }, 3));
    }
})