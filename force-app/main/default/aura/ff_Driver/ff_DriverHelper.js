({
    uploadFile: function (component, parentId, fileName, withoutMark, contentType, source, callback) {

        var CHUNK_SIZE = 480000; // Use a multiple of 4
        var fromPos = 0;
        var toPos = Math.min(withoutMark.length, CHUNK_SIZE);
        this.uploadChunk(component, parentId,
            fileName, withoutMark, contentType,
            fromPos, toPos, "", source, callback);
    },
    uploadChunk: function (component, parentId,
                           fileName, fileContents, contentType,
                           fromPos, toPos, attachId, source, completeCallback) {

        var CHUNK_SIZE = 480000; // Use a multiple of 4
        var concreteComponent = component.getConcreteComponent();
        var chunk = fileContents.substring(fromPos, toPos);

        $A.log("sending chunk" + fromPos);
        if (source.isInstanceOf("c:ff_ProgressComponent")) {
            source.set("v.percentComplete", Math.round(100 * fromPos / fileContents.length));
        }

        var params = {
            parentId: parentId,
            attachmentId: attachId,
            fileName: fileName,
            chunk: encodeURIComponent(chunk),
            contentType: contentType
        };

        var helper = this;
        var chunkCallback = function (attachId) {
            var newFromPos = toPos;
            var newToPos = Math.min(fileContents.length, newFromPos + CHUNK_SIZE);
            if (newFromPos < newToPos) {
                helper.uploadChunk(component, parentId,
                    fileName, fileContents, contentType,
                    newFromPos, newToPos, attachId, source, completeCallback);
            } else {
                completeCallback();
            }
        };

        var action = this.prepareServiceAction(concreteComponent, "c.uploadChunk", params, chunkCallback);
        action.setBackground(); // suppress boxcar so other actions aren't blocked
        $A.enqueueAction(action);
    },
    getUploadCallback: function (sourceComponent, doneCallback) {
        return function () {
            if (sourceComponent.isInstanceOf("c:ff_ProgressComponent")) {
                // this change allows the source to run code on completion
                sourceComponent.set("v.percentComplete", 100);
                // this change allows the source to change display to inactive
                sourceComponent.set("v.percentComplete", undefined);
                if (doneCallback) {
                    doneCallback();
                }
            }
        }
    },
    setControlAttributes: function (component) {
        var heights = component.get("v.heightUnitInPixels");
        if (!$A.util.isUndefined(heights)) {
            this.fireApplicationEvent(component, "e.c:ff_EventSetControlAttributes",
                {heightUnitInPixels: heights});
        }
    },
    sendToService: function (component, helper, method, params, callback) {
        var action = this.prepareServiceAction(component, method, params, callback);
        
        $A.enqueueAction(action);
    },
    prepareServiceAction: function (component, method, params, callback) {
        var action = component.get(method);        
        if (params) {
            action.setParams(params);
        }        
                
        action.setCallback(this, $A.getCallback(function (response) {        
            var state = response.getState();			
            if (state === "SUCCESS") {
                try {
                    callback(response.getReturnValue());
                                        
                } catch (e) {
                    $A.warning("Apex callback function ERROR: " + e.message);
                    this.showUnexpectedError(component, response, method, params);
                }
            } else {
                var errors = response.getError();
                if (errors) {
                    $A.warning("service call ERROR: " + errors[0].message);
                    this.showUnexpectedError(component, response, method, params);
                } else {
                    $A.warning("Unknown error");
                    this.showUnexpectedError(component, response, method, params);
                }
            }
            
        }));
        
        return action;
    },
    fireApplicationEvent: function (component, eventType, params) {
        var appEvent = $A.get(eventType);
        if (appEvent) {
            if (params) {
                appEvent.setParams(params);
            }
            appEvent.fire();
        } else {
            console.log("no application event registered: " + eventType);
        }
    },
    fireComponentEvent: function (component, eventName, params) {
        var cmpEvent = component.getEvent(eventName);
        if (cmpEvent) {
            if (params) {
                cmpEvent.setParams(params);
            }
            cmpEvent.fire();
        } else {
            console.log("no component event registered: " + eventName);
        }
    },
    clone: function (src) {
        var s = JSON.stringify(src);
        return JSON.parse(s);
    },
    tempIdInsert: function (id) {
        return "INSERT-" + id;
    },
    tempIdUpsert: function (id) {
        return "UPSERT-" + id;
    },
    isNewRecordId: function (id) {
        return id.indexOf("INSERT-") == 0;
    },
    // sObjectType is the sfdc object type
    // position is the wrapper position i.e. position in the List<ff_Service.SObjectWrapper>> returned by Apex
    setFocusedRecord: function (component, id, sObjectType, wrapperPosition, isPrimaryFocus) {
        if ($A.util.isEmpty(id)) {
            throw Error("id missing: " + sObjectType);
        }
        var wd = component.get("v.writeData");
        wd.focusLoadKey = sObjectType;
        wd.focusPosition = wrapperPosition;
        if (this.isNewRecordId(id)) {
            wd.records[id] = {
                attributes: {type: sObjectType}
                // new records have no id field
            };
            wd.focusId = id;
            // new records have no undo copy either
        } else {
            var rd = component.get("v.readData");
            var recordSet = rd.records[sObjectType][wrapperPosition];
            if (recordSet) {
                // take a copy of the original readData record
                if (recordSet.single) {
                    // check id matches and use that record
                    if (recordSet.sObjectList[0].Id == id) {
                        // seed the sobject in the correct shape for apex deserialization
                        wd.records[recordSet.sObjectList[0].Id] = {
                            attributes: {type: sObjectType},
                            Id: recordSet.sObjectList[0].Id
                        };
                        wd.focusId = recordSet.sObjectList[0].Id;
                        // set the undo copy. this is always the record in readData since
                        // all local saves are sent to apex (to engage validation rules)
                        wd.undo = this.clone(recordSet.sObjectList[0]);
                    } else {
                        $A.warning("single record did not match");
                    }
                } else {
                    wd.records[id] = {
                        Id: id,
                        attributes: {type: sObjectType}
                    };
                    wd.focusId = id;
                    // TODO loop through records and find matching id, then clone this as the undo copy
                }
                // seed the writeData record in expected shape for apex read
            } else {
                $A.warning("no record found: " + sObjectType);
            }
        }
        component.set("v.writeData", wd);
    },
    setPrimaryFocus: function (component, id, loadKey, position) {
        component.set("v.primaryFocus", {
            focusLoadKey: loadKey,
            focusId: id,
            focusPosition: position
        });
    },
    setPrimaryLoadKeys: function (component, loadKeys) {
        component.set("v.primaryLoadKeys", loadKeys);
    },
    cancelFocusedRecord: function (component) {
        var wd = component.get("v.writeData");
        var id = wd.focusId;
        var sObjectKey = wd.records[id].attributes.type;
        if (this.isNewRecordId(id)) {
            delete wd.records[id];
            delete wd.focusId;
            delete wd.focusPosition;
            delete wd.focusLoadKey;
            component.set("v.writeData", wd);
        } else {
            if ($A.util.isUndefined(wd.undo)) {
                $A.warning("no undo copy found");
            } else {
                var rd = component.get("v.readData");
                var recordSet = rd.records[sObjectKey][wd.focusPosition];
                if (recordSet) {
                    if (recordSet.single) {
                        // restore the copy of the original readData record, fire load event
                        rd.records[sObjectKey][wd.focusPosition].sObjectList = [wd.undo];
                        // empty the dirty record from writeData
                        delete wd.records[id];
                        delete wd.focusId;
                        delete wd.focusPosition;
                        delete wd.focusLoadKey;
                        // remove the undo copy
                        delete wd.undo;
                        component.set("v.readData", rd);
                        component.set("v.writeData", wd);
                    } else {
                        // loop through records and find matching id. restore the match
                    }
                    // seed the writeData record in expected shape for apex read
                } else {
                    $A.warning("no record found: " + sObjectKey);
                }
            }
        }
    },
    adjustFocusIfRequired: function (component, loadKey) {
        var wd = component.get("v.writeData");
        if (loadKey != wd.focusLoadKey) {
            var primaryLoadKeys = component.get("v.primaryLoadKeys");
            for (var i = 0; i < primaryLoadKeys.length; i++) {
                if (primaryLoadKeys[i] == loadKey) {
                    var primaryFocus = component.get("v.primaryFocus");
                    wd.focusRestore = {
                        focusLoadKey: wd.focusLoadKey,
                        focusPosition: wd.focusPosition,
                        focusId: wd.focusId
                    };
                    wd.focusLoadKey = primaryFocus.focusLoadKey;
                    wd.focusPosition = primaryFocus.focusPosition;
                    wd.focusId = primaryFocus.focusId;
                    component.set("v.writeData", wd);
                    break;
                }
            }
        }
    },
    restoreFocusIfRequired: function (component) {
        var wd = component.get("v.writeData");
        if (!$A.util.isEmpty(wd.focusRestore)) {
            $A.log("restoring temporary focus");
            wd.focusLoadKey = wd.focusRestore.focusLoadKey;
            wd.focusPosition = wd.focusRestore.focusPosition;
            wd.focusId = wd.focusRestore.focusId;
            delete wd.focusRestore;
            component.set("v.writeData", wd);
        }
    },
    restorePrimaryFocus: function (component) {
        var wd = component.get("v.writeData");        
        var primary = component.get("v.primaryFocus");
        

        if (!$A.util.isEmpty(primary)) {
            $A.log("restoring primary focus");
            wd.focusLoadKey = primary.focusLoadKey;
            wd.focusPosition = primary.focusPosition;
            wd.focusId = primary.focusId;            
                       

            component.set("v.writeData", wd);
        }
    },
    setFocusedFieldValue: function (component, name, value) {
        // set into writeData record
        var wd = component.get("v.writeData");
        var focused = wd.records[wd.focusId];
        if ($A.util.isUndefined(focused)) {
            throw Error("No focused record. focusId: " + wd.focusId);
        }
        var focusedType = focused.attributes.type;
        var parsedName = name.split(".");
        if (parsedName.length != 2) {
            $A.warning("Invalid value name. Must be two parts. Name: " + name);
        } else {
            // TODO write value changes to readData so that it is always in sync with controls
            // currently only list controls re-load from readData so not urgent until field controls are reloaded
            if (focusedType == parsedName[0]) {
                focused[parsedName[1]] = value;
            } else {
                if ($A.util.isUndefined(focused[parsedName[0]])) {
                    var parentRecordTypes = component.get("v.parentRecordTypes");
                    var parentType = parentRecordTypes[parsedName[0]] || parsedName[0];
                    // init nested parent sObject
                    focused[parsedName[0]] = {attributes: {type: parentType}};
                }
                focused[parsedName[0]][parsedName[1]] = value;
            }
            component.set("v.writeData", wd);
        }
        $A.log(JSON.stringify(wd));
    },
    removeFocusedFieldValue: function (component, loadKey, field) {
        $A.log("Removing: " + loadKey + "." + field);
        var wd = component.get("v.writeData");
        var focused = wd.records[wd.focusId];
        var focusedType = focused.attributes.type;
        if (focusedType == loadKey) {
            delete focused[field];
        } else {
            if ($A.util.isUndefined(focused[loadKey])) {
                $A.log("No nested record to remove field: " + loadKey + "." + field);
            } else {
                delete focused[loadKey][field];
            }
        }
        component.set("v.writeData", wd);
    },
    sendDirtyRecords: function (component, event, helper, doneCallback) {
        var concreteComponent = component.getConcreteComponent();
        var applicationRecordType = concreteComponent.get("v.applicationRecordType");     
        
        helper.sendToService(concreteComponent, helper, "c.save",
            // working around http://salesforce.stackexchange.com/questions/55464/sobject-array-parameter-in-lightning-causes-internal-salesforce-com-error-in-ape
            // by sending writeData as a string
            {
                writeData: JSON.stringify(component.get("v.writeData")),
                rectype:applicationRecordType            
            },
            function (result) {

                // make it easy to detect a successful save by adding a boolean
                result.success = $A.util.isEmpty(result.fieldErrors) && $A.util.isEmpty(result.recordErrors);

                if (!result.success) {
                    helper.fireFieldErrors(component, result);
                }

                // concrete drivers may render record level errors next to the button
                component.set("v.writeErrorsForRecord", result.recordErrors);

                // if concrete drivers want to extend post-save, they can handle this event
                component.getEvent("lifeCycleEvent").setParams({type: "post-save", opts: result}).fire();

                if (doneCallback) {
                    doneCallback();
                }
            });
    },
    sendFocusedRecord: function (component, successMode, doneCallback) {
        component.getEvent("lifeCycleEvent").setParams({type: "pre-save"}).fire();
        // extract only the focused record and create a new writeData with only it contained             
        
        var wd = component.get("v.writeData");
        var focused = wd.records[wd.focusId];
                
        var records = {};
        records[wd.focusId] = focused;
                
        var concreteComponent = component.getConcreteComponent();  
        var applicationRecordType = concreteComponent.get("v.applicationRecordType"); // RXP - added recordtype to make it commonly accessible for all the forms
        var helper = this;  
        
        helper.sendToService(concreteComponent, helper, "c.save",
            {
                writeData: JSON.stringify({records: records}),
                rectype:applicationRecordType
            },
            function (result) {                
				
               // make it easy to detect a successful save by adding a boolean
                result.success = $A.util.isEmpty(result.fieldErrors) && $A.util.isEmpty(result.recordErrors);
                              
                if (result.success) {
                    switch (successMode) {
                        case "ADD":
                            helper.addSavedRecordsToReadData(component, records, result);
                            break;
                        case "REPLACE":
                            helper.replaceDirtyFieldsInReadData(component, records, result)
                            break;
                        case "REPLACENEW":
                            helper.replaceFirstRecordInReadData(component, result)
                            break;
                        case "NO-OP":
                            // no client state change after save
                            break;
                        default:
                            throw Error("Unsupported success mode: " + successMode);
                    }

                    // if this save is for a list control, focus back on the primary record
                    var primary = component.get("v.primaryFocus");
                    if (wd.focusId != primary.focusId) {
                        // clear out the dirty data since it is now in sync with the server                        
                        helper.cancelFocusedRecord(component);
                        // restore the primary focus if present
                        helper.restorePrimaryFocus(component);
                    }

                    helper.fireApplicationEvent(component,
                        "e.c:ff_EventUpdateControls",
                        {
                            data: component.get("v.readData"),
                            driver: component
                        });

                } else {
                    helper.fireFieldErrors(component, result);
                    // concrete drivers may render record level errors next to the button
                    component.set("v.writeErrorsForRecord", result.recordErrors);
                }                
                
                // if concrete drivers want to extend post-save, they can handle this event
                component.getEvent("lifeCycleEvent").setParams({type: "post-save", opts: result}).fire();

                if (doneCallback) {
                    doneCallback(result);
                }
            });            
    },
    // to keep readData in sync with successful apex updates, move all dirty values into the readData
    replaceDirtyFieldsInReadData: function (component, records, result) {
        var rd = component.get("v.readData");
        var wd = component.get("v.writeData");
        var wrappersForObject = rd.records[wd.focusLoadKey];
        if ($A.util.isUndefined(wrappersForObject)) {
            throw Error("No wrappers found for: " + wd.focusLoadKey);
        }
        if ($A.util.isUndefined(wrappersForObject[wd.focusPosition])) {
            throw Error("No wrapper list found for: " + wd.focusLoadKey + " at position: " + wd.focusPosition);
        }
        var saveTargetList = wrappersForObject[wd.focusPosition].sObjectList;
        var matchingReadRecord, matchingReadPosition;
        for (var i = 0; i < saveTargetList.length; i++) {
            if (saveTargetList[i].Id == wd.focusId) {
                matchingReadRecord = saveTargetList[i];
                matchingReadPosition = i;
                break;
            }
        }
        if ($A.util.isUndefined(matchingReadRecord)) {
            throw Error("matching record not found in readData: " + JSON.stringify(wd));
        }
        var dirty = wd.records[wd.focusId];
        for (var key in dirty) {
            if (key != "attributes") {
                matchingReadRecord[key] = dirty[key];
            }
        }
        rd.records[wd.focusLoadKey][wd.focusPosition].sObjectList[matchingReadPosition] = matchingReadRecord;
        component.set("v.readData", rd);
    },
    // update the first record in readData with a record from an apex save
    replaceFirstRecordInReadData: function (component, result) {
        var rd = component.get("v.readData");
        var wd = component.get("v.writeData");
        var wrappersForObject = rd.records[wd.focusLoadKey];
        if ($A.util.isUndefined(wrappersForObject)) {
            throw Error("No wrappers found for: " + wd.focusLoadKey);
        }
        if ($A.util.isUndefined(wrappersForObject[wd.focusPosition])) {
            throw Error("No wrapper list found for: " + wd.focusLoadKey + " at position: " + wd.focusPosition);
        }
        var savedRecordTempId = Object.keys(result.results)[0];
        var focusedList = rd.records[wd.focusLoadKey][wd.focusPosition].sObjectList;
        focusedList[0] = result.results[savedRecordTempId];
        rd.records[wd.focusLoadKey][wd.focusPosition].sObjectList = focusedList;
        component.set("v.readData", rd);
    },
    patchedSetId: function (obj, id) {
        // patch for Lightning bug where objects sent in events cannot set k/v's if they weren't present at object construction
        // was a problem in EventUpdateControls passing updated readData to list controls with ids updated from Apex save
        var newObj = JSON.parse(JSON.stringify(obj));
        newObj.Id = id;
        return newObj;
    },
    addSavedRecordsToReadData: function (component, records, result) {
        // adds a record after an Apex save i.e. not optimistic

        // only handles single record insert TODO support single update, N record insert + update etc
        var rd = component.get("v.readData");
        var wd = component.get("v.writeData");
        var wrappersForObject = rd.records[wd.focusLoadKey];
        if ($A.util.isUndefined(wrappersForObject)) {
            throw Error("No wrappers found for: " + wd.focusLoadKey);
        }
        if ($A.util.isUndefined(wrappersForObject[wd.focusPosition])) {
            throw Error("No wrapper list found for: " + wd.focusLoadKey + " at position: " + wd.focusPosition);
        }
        var saveTargetList = wrappersForObject[wd.focusPosition].sObjectList;
        var recordFromApex = result.results[wd.focusId];
        saveTargetList.unshift(recordFromApex); // add saved record to start of list for good UX
        rd.records[wd.focusLoadKey][wd.focusPosition].sObjectList = saveTargetList; // why is this required?
        component.set("v.readData", rd);
    },
    updateControlsWithFocusedRecord: function (component, operation) {
        // for client lazy writes, add the focused/dirty record to the readData and update controls
        switch (operation) {
            case "CREATE":
                this.addFocusedRecord(component);
                this.fireApplicationEvent(component,
                    "e.c:ff_EventUpdateControls",
                    {
                        data: component.get("v.readData"),
                        driver: component
                    });
                break;
            case "CANCEL":
                this.restorePrimaryFocus(component);
                break;
            default:
                throw Error("Unsupported focused write operation in driver: " + operation);
        }
    },
    addFocusedRecord: function (component) {
        // for optimistic client inserts with tempids, adds the focused record to the matching wrapper list
        var rd = component.get("v.readData");
        var wd = component.get("v.writeData");
        var wrappersForObject = rd.records[wd.focusLoadKey];
        if ($A.util.isUndefined(wrappersForObject)) {
            throw Error("No wrappers found for: " + wd.focusLoadKey);
        }
        if ($A.util.isUndefined(wrappersForObject[wd.focusPosition])) {
            throw Error("No wrapper list found for: " + wd.focusLoadKey + " at position: " + wd.focusPosition);
        }
        var saveTargetList = wrappersForObject[wd.focusPosition].sObjectList;
        var focusedRecord = wd.records[wd.focusId];
        saveTargetList.unshift(focusedRecord); // add focused record to start of list for good UX
        rd.records[wd.focusLoadKey][wd.focusPosition].sObjectList = saveTargetList; // why is this required?
        component.set("v.readData", rd);
    },
    fireFieldErrors: function (component, result) {
        console.log("TODO: fire field errors to fields: " + JSON.stringify(result.fieldErrors));
        // fire event with field errors out to field controls so they auto-show server errors
    },
    deleteRecordFromReadData: function (component, loadKey, wrapperPosition, id) {
        var rd = component.get("v.readData");
        var sObjectList = rd.records[loadKey][wrapperPosition].sObjectList;
        if (sObjectList.single) {
            throw Error("Delete of a 'single' record not supported: " + loadKey + " " + wrapperPosition);
        }
        var filteredRecords = [];
        for (var i = 0; i < sObjectList.length; i++) {
            if (sObjectList[i].Id != id) {
                filteredRecords.push(sObjectList[i]);
            }
        }
        rd.records[loadKey][wrapperPosition].sObjectList = filteredRecords;
        component.set("v.readData", rd);
    },
    deleteRecordWithApex: function (component, id, doneCallback) {
        var concreteComponent = component.getConcreteComponent();
        var applicationRecordType = concreteComponent.get("v.applicationRecordType");     
        var helper = this;
        helper.sendToService(concreteComponent, helper, "c.deleteRecord",
            {
              id: id,
              rectype:applicationRecordType	// RXP - added recordtype to make it commonly accessible for all the forms
            },
            function (result) {

                // make it easy to detect a successful save by adding a boolean
                result.success = $A.util.isEmpty(result.fieldErrors) && $A.util.isEmpty(result.recordErrors);

                $A.log("delete result: " + JSON.stringify(result));

                //WDCi (Lean) 15/10/2018 PAN:5340 - start: add error handling for delete
                if(!result.success){
                    helper.fireFieldErrors(component, result);
                    // concrete drivers may render record level errors next to the button
                    component.set("v.writeErrorsForRecord", result.recordErrors);
                }
                //WDCi (Lean) 15/10/2018 PAN:5340 - end

                if (doneCallback) {
                    doneCallback(result);
                }
            });
    },
    setFieldValidity: function (component, loadKey, field, isValid, stage) {
        var validityMap = component.get("v.formValidity");
        var stageKey = this.getStageKey(stage);
        if ($A.util.isUndefined(validityMap[stageKey])) {
            validityMap[stageKey] = {};
        }
        validityMap[stageKey][loadKey + "." + field] = isValid;
        component.set("v.formValidity", validityMap);
        component.set("v.formValid", this.isFormValid(component));
    },
    setAggregateValidity: function (component, aggregateKey, isValid, stage) {
        var validityMap = component.get("v.formValidity");
        var stageKey = this.getStageKey(stage);
        if ($A.util.isUndefined(validityMap[stageKey])) {
            validityMap[stageKey] = {};
        }
        validityMap[stageKey][aggregateKey] = isValid;
        component.set("v.formValidity", validityMap);
        component.set("v.formValid", this.isFormValid(component));
    },
    getStageKey: function (stage) {
        if ($A.util.isUndefinedOrNull(stage)) {
            return "no-stage";
        } else {
            return stage;
        }
    },
    isFormValid: function (component) {
        var singleStage = component.get("v.singleStage");
        var validityMap = component.get("v.formValidity");
        for (var stageKey in validityMap) {
            var stageIsIgnored = !singleStage && stageKey == "no-stage";
            if (!stageIsIgnored) {
                for (var field in validityMap[stageKey]) {
                    if (!validityMap[stageKey][field]) {
                        return false;
                    }
                }
            }
        }
        return true;
    },
    updateStages: function (component) {
        var labels = component.get("v.stageLabels");
        var validityMap = component.get("v.formValidity");
        var loaded = component.get("v.loaded");
        if (labels) {
            var labelArray = labels.split(",");
            var stages = [];
            var current = component.get("v.currentStage");
            var furthest = component.get("v.furthestStage");

            for (var i = 0; i < labelArray.length; i++) {
                var state = "INVALID";

                //user visits this step
                if (i == current) {
                    state = "ACTIVE";
                }
                //user has not visited the step
                else if (i > furthest) {
                    state = "INCOMPLETE";
                }
                //user has completed all fields successfully
                else if (current <= furthest) {
                    var complete = true;
                    for (var property in validityMap[i]) {
                        if (!validityMap[i][property]) {
                            complete = false;
                            break;
                        }
                    }
                    if (complete) {
                        state = "COMPLETE";
                    }
                }
                stages.push({label: labelArray[i], state: state, stage: i});
            }
            component.set("v.allStages", stages);
            if (loaded) {
                this.fireComponentEvent(component, "lifeCycleEvent", {type: "stage-change"});
            }

        }
    },
    setStage: function (component, stage) {
        var labels = component.get("v.stageLabels");
        if (labels) {
            if (stage < labels.length) {
                component.set("v.currentStage", stage);
            } else {
                throw Error("Invalid stage. Max: " + labels.length);
            }
        }
    },
    prevStage: function (component) {
        var labels = component.get("v.stageLabels");
        if (labels) {
            var current = component.get("v.currentStage");
            if (current > 0) {
                component.set("v.currentStage", current - 1);
            } else {
                throw Error("Invalid prev stage. Already at first stage.");
            }
        }
    },
    nextStage: function (component) {
        var labels = component.get("v.stageLabels");
        if (labels) {
            var currentStage = component.get("v.currentStage");
            if (currentStage + 1 < labels.length) {
                var newCurrentStage = currentStage + 1;
                var furthestStage = component.get("v.furthestStage");
                var newFurthestStage = furthestStage >= newCurrentStage ? furthestStage : newCurrentStage;
                component.set("v.furthestStage", newFurthestStage);
                component.set("v.currentStage", newCurrentStage);
            } else {
                throw Error("Invalid next stage. Max: " + labels.length);
            }
        }
    },
    goToStage: function (component, stage) {
        var labels = component.get("v.stageLabels");
        if (labels) {
            if (stage < labels.length) {
                component.set("v.currentStage", stage);
            }
        }
    },
    showUnexpectedError: function (component, response, method, params) {
        this.fireApplicationEvent(component, "e.c:ff_EventError", {
            type: "APEX",
            developerError: {
                source: "Apex callback",
                response: response,
                component: component.getConcreteComponent().getName(),
                method: method,
                params: params
            }
        });
    }
})