({
    handleDelete: function (component, event, helper) {
        component.getEvent("crudEvent")
            .setParams({
                operation: "DELETE",
                id: component.get("v.id")
            })
            .fire();
    }
})