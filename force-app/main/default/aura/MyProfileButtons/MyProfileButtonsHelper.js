({
    optionalValidation : function(component, event, helper) {
        var currentValue = event.getParam("value") ;
        var currentField = event.getParam("field") ;
        var props = component.get("v.properties");
        var isSameAddressEnabled = props.RECORD['Mailing_and_Residential_is_the_same__c'] ;
        
        var addressFieldMap = {PersonOtherStreet:"PersonMailingStreet",
                               PersonOtherCity:"PersonMailingCity",
                               PersonOtherState:"PersonMailingState",
                               PersonOtherPostalCode:"PersonMailingPostalCode",
                               PersonOtherCountryCode:"PersonMailingCountryCode"
                              };
        
        if(currentField && currentValue){
            if(isSameAddressEnabled){
                //checking the undefined error before firing up the SetValueEvent
                if(!$A.util.isUndefined(addressFieldMap[currentField])){
                    //Firing application event to copy the Residential Address values to Mailing Address fields.
                    //Event is only fired when page is not loading i.e. only when User changes Address values.
                    var setValueEvent = $A.get("e.c:EditorSetValueEvent");
                    if(!component.get("v.isLoading")){
                        setValueEvent.setParams({field: addressFieldMap[currentField], 
                                                 value: currentValue}).fire(); 
                    }
                    
                }          
            }else{
                if(currentField == 'Mailing_and_Residential_is_the_same__c'){
                    //Fetching the OtherState value before firing SetValueEvent because when Country changes Sate become empty.
                    //To fill the State with correct option we use this value to fire SetValueEvent on State field.
                    var otherStateValue = props.RECORD['PersonOtherState'] ;
                    for(var addressVar in addressFieldMap) {
                        var key = addressVar ;
                        var value = props.RECORD[addressVar];
                        var setValueEvent = $A.get("e.c:EditorSetValueEvent");
                        setValueEvent.setParams({field: addressFieldMap[addressVar], 
                                                 value: props.RECORD[addressVar]}).fire();
                    }
                    var setValueEvent = $A.get("e.c:EditorSetValueEvent");
                    setValueEvent.setParams({field: 'PersonMailingState', 
                                             value: otherStateValue}).fire();
                }
            }            
        }
    },
    navigate : function(component, event) {
        var urlEvent = $A.get("e.force:navigateToURL").setParams
    	    ({
                "url": component.get("v.saveURL"),
                "isredirect" :false
            });
        urlEvent.fire();
	}
})