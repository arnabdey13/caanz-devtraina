({
    validateMyDetailConsession: function(component, callback) {
        var concessionComp = component.find('concessionComp');
        concessionComp.validateConcession({ callback: $A.getCallback(callback) });
    },
    validateMyDetailsAddress : function(component, callback){
        var addressComp = component.find('addressComp');
        addressComp.validateOtherCountry({ callback: $A.getCallback(callback) });
    },
})