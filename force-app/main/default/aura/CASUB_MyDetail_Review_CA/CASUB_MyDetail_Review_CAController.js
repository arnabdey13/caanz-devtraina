({
    doInit: function(cmp, event, helper) {        
        window.scrollTo('Top',0,0);
    },
    performSave12: function(component, event, helper){
        component.set("v.errorMessage", "");
        helper.validateMyDetailsAddress(component, function(){
            helper.validateMyDetailConsession(component, function(){
               component.set("v.isDetailPageTobeShown",false);
            });
        });
        
        /*else if(params.callback) {
			params.callback();
		}*/
    },
    performSave: function(component, event, helper){
        component.set("v.errorMessage", "");
        var params = event.getParam ? event.getParam("arguments").params || {} : {};
        helper.validateMyDetailsAddress(component, function(){
            helper.validateMyDetailConsession(component, function(){
                if(params.callback) {
					params.callback();
				}
            });
        });
    }
    
})