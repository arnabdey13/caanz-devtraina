({
    onChange : function(component, event, helper) {
        // handle changes from UI, with possible negation setting v.value
        // which is what is sent to apex
        var value = event.getSource().get("v.value");
        if (component.get("v.negateValue")) {
            value = !value;
        }
        helper.applyChange(component, value);
             
        var toggleKeyToFire = component.get("v.fireToggleKey");
        if (toggleKeyToFire) {
            var appEvent = $A.get("e.c:ToggleEvent");
            appEvent.setParams({key: toggleKeyToFire}).fire();	    
        }
    },
    showHint : function(component, event, helper) {
        if (component.get("v.hint")) {
            component.find("popover").showHint();
        }
    },
    hideHint : function(component, event, helper) {
        if (component.get("v.hint")) {
        	component.find("popover").hideHint();
        }
    }
})