({
    load : function(component, event, helper) {
        this.setDisplayValue(component, event, helper);
        
        var disableIfFalse = component.get("v.disableIfFalse");
        if (disableIfFalse && checkboxValue == false) {
            component.set("v.disabled", true);
        }
        
        var fireToggleKey = component.get("v.fireToggleKey");
        var checkboxValue = component.get("v.value");
        
        if(checkboxValue && fireToggleKey){
            var appEvent = $A.get("e.c:ToggleEvent");
            appEvent.setParams({key: fireToggleKey}).fire();
        }
    },
    optionalPostSetValue : function(component, event, helper) {
        this.setDisplayValue(component, event, helper);
    },
    setDisplayValue : function(component, event, helper) {
        // handle possible negation of v.value and set into displayValue
        var checkboxValue = component.get("v.value");
        var displayValue = component.get("v.negateValue")?!checkboxValue:checkboxValue;
        component.set("v.displayValue",displayValue);
    },
    validate : function(component) {
        var isRequired = component.get("v.required");
        var hidden = component.get("v.hidden");
        var value = component.get("v.value");
        var isValueMissing = $A.util.isUndefined(value);
        var isInvalidValue = this.checkInvalidValues(component);        
        $A.log("checkbox validation: "+component.get("v.field"), {required: isRequired,
                                                               hidden: hidden,
                                                               value: value,
                                                               missing: isValueMissing})
        var invalid = !hidden && (
            (isRequired && isValueMissing) ||
            isInvalidValue
            );
        return !invalid;
	},
	setUIValid : function(component) {
        var editor = component.find("editor");
        var checkboxValue = component.get("v.value");
        var isRequired = component.get("v.required");
        if (editor) {
                if (isRequired == true && checkboxValue == true) {
                    editor.set("v.errors", null);
                    $A.util.removeClass(component, "slds-has-error");
                } else {
                    var errorMessages = [] ;
                    if(isRequired == true && (checkboxValue == false || checkboxValue == null)){
                        errorMessages.push({message: "This field is required"});
                        editor.set("v.errors", errorMessages);
                    	$A.util.addClass(component, "slds-has-error");
                        }
                    }
                                        
                }
            }
})