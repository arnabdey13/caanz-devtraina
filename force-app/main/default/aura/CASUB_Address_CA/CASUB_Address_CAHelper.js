({
	getAddressData: function(cmp) {
		var params = { contactId: cmp.get("v.contactId") };
		this.enqueueAction(cmp, "getAddressData", params, function(data){
			cmp.set("v.account", (data = JSON.parse(data)).account);
			cmp.set("v.contact", data.contact);
            cmp.set("v.casub_Address_Notification_CA_Setting", data.casub_Address_Notification_CA_Setting);
            cmp.set("v.isConfirmChecked", cmp.get("v.account.NZICA_confirmation__pc"));
            //var lable = cmp.get("v.casub_Address_Notification_CA_Setting.Confirmation_Message__c") +''+ cmp.get("v.casub_Address_Notification_CA_Setting.Confirmation_Message__c")
            //cmp.set("v.confirmationCheckBoxLable");
            
		});
	},
    updateAccountForDeclarationStatement : function(cmp,callBackParams,declarationValue){
        var params = { personAccountId: cmp.get("v.account.Id"),confirmationChecked:declarationValue };
		this.enqueueAction(cmp, "saveDeclarationStatement", params, function(data){
            if(callBackParams.callback) {
                callBackParams.callback();
            }            
		});
    },
    navigateToPage : function(component,navigateURL) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": navigateURL
        });
        urlEvent.fire();
    },
	enqueueAction: function(cmp, method, params, callback){
		this.toggleSpinner(cmp, true);
		var action = cmp.get("c." + method); 
		if(params) action.setParams(params);
		action.setCallback(this, function(response){
			if(response.getState() === "SUCCESS") {
				if(callback) callback.call(this, response.getReturnValue());
	        } else if(response.getState() === "ERROR") {
	        	console.error(response.getError());
	        }
	        this.toggleSpinner(cmp, false);
		});
		$A.enqueueAction(action);
	},
	toggleSpinner: function(cmp, show) {
		var spinner = cmp.find("spinner");
		$A.util.addClass(spinner, show ? "slds-show" : "slds-hide");
		$A.util.removeClass(spinner, !show ? "slds-show" : "slds-hide");
	},
    showToast : function(component,text) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Warning!",
            "message": text,
            type : 'warning',
            mode: 'dismissible',
            duration :10000
        });
        toastEvent.fire();
    }
})