({
	doInit: function(cmp, event, helper) {
        helper.getAddressData(cmp);
        var user_timezone = $A.get("$Locale.timezone");
        var user_date = new Date().toLocaleString("en-US", {timeZone: user_timezone})
        var lateFeeDate = $A.get("$Label.c.CASUB_LateFee");
        debugger;
        if(user_date == lateFeeDate || user_date > lateFeeDate){
            debugger;
            cmp.set("v.isLateFee", true); 
        }
	},
    handleShowModal: function(cmp, evt, helper) {
        var modalBody, modalFooter;
        $A.createComponents([
                ['c:CASUB_CaanzAddressDetails_CA', {}],
                ['c:CASUB_ModelFooter_CA', {}],
            ],
            function(components, status) {
                if (status === "SUCCESS") {
                    modalBody = components[0];
                    modalFooter = components[1];
                    modalFooter.set('v.saveFunction', modalBody.save)
                    cmp.find('overlayLib').showCustomModal({
                        header: "Edit Residential Address",
                        body: modalBody,
                        footer: modalFooter,
                        cssClass: "cCASUB_Address_CA",
                        showCloseButton: true,
                        closeCallback: function() {
							helper.getAddressData(cmp);
                        }
                    });
                }
            }
        );
    },
    handleNavigation:function(component, event, helper){
      helper.navigateToPage(component,component.get("v.casub_Address_Notification_CA_Setting.Regulatory_Requirements_Web_Link__c"))
    },
    validateOtherCountry : function(component, event, helper){
        var params = event.getParam ? event.getParam("arguments").params || {} : {};
        var existingValueOfDelclaration=component.get("v.account.NZICA_confirmation__pc");
         var errorMessage='';
        //component.set("v.errorMessage",'');
        if($A.util.isEmpty(component.get("v.contact.OtherCountry"))) {
			//component.set("v.errorMessage","Please update Country to proceed.");
			errorMessage="Please update country for residential address to proceed .";
            helper.showToast(component,errorMessage);
		}else if(component.get("v.casub_Address_Notification_CA_Setting.Is_Active__c") && !component.get("v.isConfirmChecked")){
			//component.set("v.errorMessage","Please read the declaration and check the checkbox to proceed.");
            errorMessage="Please read the declaration for residential address and check the checkbox to proceed.";
             helper.showToast(component,errorMessage);
		}else if(component.get("v.casub_Address_Notification_CA_Setting.Is_Active__c") && component.get("v.isConfirmChecked")){
            var activateDeclaration = component.get("v.casub_Address_Notification_CA_Setting.Is_Active__c") 
            if(existingValueOfDelclaration == activateDeclaration){
                if(params.callback) {
                    params.callback();
                }
            }else{
             helper.updateAccountForDeclarationStatement(component,params,activateDeclaration);
            }
            
        }else if(!component.get("v.casub_Address_Notification_CA_Setting.Is_Active__c")){
            var deActivateDeclaration = component.get("v.casub_Address_Notification_CA_Setting.Is_Active__c");
            if(existingValueOfDelclaration == deActivateDeclaration){
                if(params.callback) {
                    params.callback();
                }
            }else{
             helper.updateAccountForDeclarationStatement(component,params,deActivateDeclaration);
            }
        }else if(params.callback) {
			params.callback();
		}
   }
})