({
    doInit : function(component, event, helper) {
        window.scroll(0, 0);
        helper.loadUserRelatedSalesOrder(component);
    },
    movetoContactEdit : function(component, event, helper) {     
        var appEvent = $A.get("e.c:CASUB_PaymentEvent_CA"); 
        appEvent.setParams({"message" : 'ChangeToContact'}); 
        appEvent.fire();
    },    
    completeSubs : function(component, event, helper) {  
        component.set("v.paymentController.subscription.Sales_Order_Status__c","Completed");      
        helper.activateSalesOrder(component);
    },   
    activate : function(component, event, helper) {  
        component.set("v.paymentController.subscription.Sales_Order_Status__c","Invoice Acknowledgement");      
        helper.activateSalesOrder(component);
    },    
    loginToNetSuite : function(component, event, helper) {        
        helper.loginToNetSuiteHelper(component);
    },  
    
})