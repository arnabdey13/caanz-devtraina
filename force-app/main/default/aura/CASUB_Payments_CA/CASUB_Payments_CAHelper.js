({	
    loadUserRelatedSalesOrder: function(component){
        var params;
        this.enqueueActionMethod(component, "getUserRelatedOrder", params, function(response){
            //console.log('==response==' + JSON.stringify(response));            
            component.set("v.paymentController",response);            
            this.getEncodedParamHelper(component);
        });
    },    
    enqueueActionMethod: function(component, method, params, callback){
        component.set("v.showSpinner",true);
        var action = component.get("c." + method); 
        if(params) action.setParams(params);
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                if(callback) callback.call(this, response.getReturnValue());
            } else if(state === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                    }
                }
            }else if (status === "INCOMPLETE") {
                console.log('No response from server or client is offline.');
            }
            component.set("v.showSpinner",false);
        });       
        $A.enqueueAction(action);
    },
    showToast : function(component,text) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Warning!",
            "message": text,
            type : 'warning',
            mode: 'dismissible '
        });
        toastEvent.fire();
    },  
     
    getEncodedParamHelper : function(component) {        
        var userCustomerId =  component.get("v.paymentController.personAccount.Customer_ID__c");
        var netSuiteOrderId = component.get("v.paymentController.userRelatedOrder.NetSuite_Order_ID__c"); 
        var params = {"customerId":userCustomerId,"salesOrderId": netSuiteOrderId};
         this.enqueueActionMethod(component, "getEncryptedParam", params, function(response){
             if(!$A.util.isEmpty(response)){
                 component.set("v.encodedString",response);
             }             
         });
    },
    loginToNetSuiteHelper : function(component) {
        var response = component.get("v.encodedString");
        var netSuiteURL = $A.get("{!$Label.c.CASUB_NetSuiteInvoiceLink}");
        var URLLink = netSuiteURL + response;
        window.open(URLLink, '_blank');
        /*var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": URLLink
        });
        urlEvent.fire();
        
    
        var userCustomerId =  component.get("v.paymentController.personAccount.Customer_ID__c");
        var netSuiteOrderId = component.get("v.paymentController.userRelatedOrder.NetSuite_Order_ID__c");       
        
        var params = {"customerId":userCustomerId,"salesOrderId": netSuiteOrderId};
        this.enqueueActionMethod(component, "getEncryptedParam", params, function(response){
            console.log(response + '==response');
            var result = response;
            var netSuiteURL = $A.get("{!$Label.c.CASUB_NetSuiteInvoiceLink}");
            
            var URLLink = netSuiteURL + response;
            window.open(URLLink, '_blank');
        });
         */
    },
    activateSalesOrder: function(component){
        var subscription = component.get("v.paymentController.subscription");
        var personAccountId = component.get("v.paymentController.personAccount.Id");
        var params = {"userSubscriptionToUpdate":subscription,"personAccountId":personAccountId};
        this.enqueueActionMethod(component, "updateSubscriptionPayment", params, function(response){
            component.set("v.paymentController.subscription",response);            
            var appEvent = $A.get("e.c:CASUB_PaymentEvent_CA"); 
            appEvent.setParams({"message" : 'DisablePrevious'}); 
            appEvent.fire();
        });
    },    
})