({
	doInit: function(cmp, event, helper) {
		helper.checkForUserPermissions(cmp,event);
        cmp.set('v.showLinkedIn',true);
        var cm = cmp.find('linkedInId');
        cm.set('v.load',true);
        $A.util.addClass(cm.find('linkedinFAQ'),'slds-hide');
        $A.util.addClass(cm.find('linkedinLabel'),'slds-hide');
        /*if(window.location.pathname == '/MyCA/s/'){
            //$A.util.addClass(cmp.find('linkedin'),'slds-hide');
            cmp.set('v.showLinkedIn',false);
        }
        else{
            $A.util.addClass(cm.find('linkedinFAQ'),'slds-hide');
            $A.util.addClass(cm.find('linkedinLabel'),'slds-hide');    
        }*/
    },
    
    linkedinComp : function(component,event,helper){
        component.set('v.showLinkedIn',true);
            var cm = component.find('linkedInId');
            $A.util.addClass(cm.find('linkedinFAQ'),'slds-hide');
            $A.util.addClass(cm.find('linkedinLabel'),'slds-hide');
            if(cm.get('v.verify')){
                cm.set('v.load',false);
                cm.set('v.verify',false);
            }
            else{
                cm.set('v.load',true);
                cm.set('v.verify',true);
            }
        /*if(window.location.pathname == '/MyCA/s/'){
            //$A.util.addClass(component.find('linkedin'),'slds-hide');
            component.set('v.showLinkedIn',false);
            
        }
        else{
            //$A.util.removeClass(component.find('linkedin'),'slds-hide');
            component.set('v.showLinkedIn',true);
            var cm = component.find('linkedInId');
            $A.util.addClass(cm.find('linkedinFAQ'),'slds-hide');
            $A.util.addClass(cm.find('linkedinLabel'),'slds-hide');
            
        }*/
    },
    
    refreshView : function(component,event,helper){
        component.set('v.showLinkedIn',true);
        var cm = component.find('linkedInId');
        if(cm.get('v.load')){
            cm.set('v.load',false);
        }
        else{
            cm.set('v.load',true);
        }
        $A.util.addClass(cm.find('linkedinFAQ'),'slds-hide');
        $A.util.addClass(cm.find('linkedinLabel'),'slds-hide');
        $A.get('e.force:refreshView').fire();
        
    } 
    
})