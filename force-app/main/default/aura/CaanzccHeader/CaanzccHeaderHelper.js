({
	checkForUserPermissions: function(cmp,event) {
		var customPermission = cmp.get("v.customPermission");
		if(!$A.util.isEmpty(customPermission)){
			this.getUserPermissions(cmp, event,customPermission);
		}
    },
    
	getUserPermissions: function(cmp, event,customPermissions){
		var action = cmp.get("c.getUserPermissions"); 
		action.setParams({ customPermissions: customPermissions });
		action.setCallback(this, function(response) {
			if(response.getState() === "SUCCESS") {
				this.applyUserPermissions(cmp, event,JSON.parse(response.getReturnValue()));
				console.info('CustomPermission is -->',JSON.parse(response.getReturnValue()));
              } else if(response.getState() === "ERROR") {
	        	this.showError(response.getError());
	        }
		});
		$A.enqueueAction(action);
	},
	applyUserPermissions: function(cmp, event,userPermissions){
		var customPermission = cmp.get("v.customPermission");
		cmp.set("v.isVisible", userPermissions.includes(customPermission[0]));
        var linkedInPermission = cmp.get('v.linkedIncustomPermission');
		cmp.set("v.isLinkedInVisible", userPermissions.includes(linkedInPermission));
		cmp.set('v.isSMPVisible',userPermissions.includes(cmp.get('v.SMPcustomPermission')));

     },
    
	showError: function(errors){
        var message = 'Unknown error'; 
        if ($A.util.isArray(errors) && !$A.util.isEmpty(errors)) {
            message = errors[0].message;
        }
        alert(message);
	}
})