({
    handleStageChange: function (component) {
        var stageParameter = component.get("v.mostRecentStageParams");
        var current = stageParameter.stage;
        component.set("v.stage", current);
        var stages = stageParameter.stages;
        component.set("v.stages", stages);
        var percent = 100 * current / (stages.length - 1);
        component.set("v.progressPercent", percent);
        var navigationAllowed = stageParameter.navigationAllowed;
        component.set("v.navigationAllowed", navigationAllowed);
    }
})