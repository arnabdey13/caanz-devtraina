({
    handleNewStage: function (component, event, helper) {
        helper.handleStageChange(component);
    },
    //handle stage change event to re-render the visibility of the progress bar.
    //This function 'debounces' the multiple stage change events and will just re-render if the stage actually changes.
    handleStageChange: function (component, event, helper) {
        component.set("v.mostRecentStageParams", event.getParams());
        component.set("v.mostRecentStage", event.getParam("stage"));
        if (event.getParam("forceStageChangeHandling")) {
            helper.handleStageChange(component);
        }
    }
})