({
    doInit: function (component, event, helper) {

        var test = helper.driver(component, event, helper);

        // references to all the components being tested
        var text1 = component.find("text1");
        var text2 = component.find("text2");
        var text3 = component.find("text3");
        var select1 = component.find("select1");

        var text10 = component.find("text10");
        var text11 = component.find("text11");
        var text12 = component.find("text12");
        var text14 = component.find("text14");
        var text15 = component.find("text15");
        var text16 = component.find("text16");
        var text17 = component.find("text17");
        var text18 = component.find("text18");

        var date1 = component.find("date1");
        var date2 = component.find("date2");
        var date3= component.find("date3");

        var foundAggregate;

        var startTests = test.start({
            focused: text1,
            description: "valid state after load"
        })

        var wrappers = {};
        wrappers["Account.Type"] = [
            {value: "private", label: "Private Company"},
            {value: "public", label: "Public Company"}];
        wrappers["TestApp.richtext"] = [{
            richtext: "Normal Text<br><b>Bold Text</b><ul><li>Normal Bullet </li><li><b>Bold Bullet</b></li></ul>"
        }]
        wrappers["TestApp.reciprocal"] = [{
            richtext: "If you have a disability or you are experiencing circumstances that will impact on your progress to membership, please indicate below and provide supporting documentation. We will follow up with you directly on your options."
        }]

        // using a single chain.

        startTests

        // IMPORTANT: code invoked via an event breaks the template pattern i.e. helper fn overrides in sub-components.
        // compare this with tests below where code is invoked directly, in which case, overrides are called.

        ////////// loaded without any changes from the user

        // TODO if a field doesn't match a value in the load event but is required, it should still report invalid back to driver so that save can be blocked

        // should only ever be one load event seen by a page so fire once for all 3 fields
            .then(test.wait(helper.fireLoadEvent("Account", {
                Balance1__c: 200,
                Balance2__c: "",
                Balance3__c: "xxx",
                DOB_string__c: "1970-01-01",
                DOB__c: "1970-01-01",
                DOB_Test__c: ""
            }, wrappers)))
            .then(test.assert("v.isValid", "loaded number is valid"))
            .then(test.assertEquals([], "v.validationErrors", "No common errors are displayed after load"))
            .then(test.assertEquals([], "v.specificValidationErrors", "No specific errors are displayed after load"))

            .then(test.focus(text2, "invalid empty value after load"))
            .then(test.assertEquals(false, "v.isValid", "Empty field is invalid"))
            .then(test.assertEquals(["This field is required"], "v.validationErrors",
                "Required error message is present although hidden"))
            .then(test.assertEquals(["This field must be a number"], "v.specificValidationErrors",
                "Specific error message is present although not displayed"))
            .then(test.assertEquals(false, "v.displayValidationErrors", "Invalid field not showing errors after load"))

            .then(test.focus(text3, "invalid integer value after load"))
            .then(test.assertEquals(false, "v.isValid", "Non-integer field is invalid"))
            .then(test.assertEquals(["This field must be a number"], "v.specificValidationErrors",
                "Number error message is present although hidden"))
            .then(test.assertEquals(false, "v.displayValidationErrors",
                "Invalid field not showing errors after load"))

            .then(test.focus(select1, "select with no value loaded"))
            .then(test.assertEquals(false, "v.displayValidationErrors", "Invalid select not showing errors after load"))
            .then(test.assertEquals(["This field is required"], "v.validationErrors",
                "select field is reporting invalid value"))
            .then(test.assertEquals(false, "v.isValid", "empty select is invalid"))

            ////////// edits made by the user after load

            // TODO a number value of "200fff" should be rejected but passes current validation for number

            .then(test.focus(text10, "valid integer value set by user"))
            .then(test.wait(function (context) {
                text10.set("v.value", "100");

                // TODO call api fn "validate" and check that it uses specific validations as well as common
                // text10.validate();
            }))
            .then(test.assert("v.isValid", "100 balance is displayed as valid"))
            .then(test.assertEquals([], "v.validationErrors", "No errors are displayed"))

            .then(test.focus(text11, "invalid state because cleared by user"))
            .then(test.wait(function (context) {
                text11.set("v.value", "");
            }))
            .then(test.assertEquals(false, "v.isValid", "Empty field is invalid"))
            .then(test.assertEquals(["This field is required"], "v.validationErrors",
                "Common required error message is displayed"))
            .then(test.assertEquals(true, "v.displayValidationErrors",
                "Invalid field showing errors after user change to empty"))

            .then(test.focus(text12, "string value in integer field is invalid"))
            .then(test.wait(function (context) {
                text12.set("v.value", "xxx");
            }))
            .then(test.assertEquals(false, "v.isValid", "Non-integer field is invalid"))
            .then(test.assertEquals(["This field must be a number"], "v.specificValidationErrors",
                "Specific Number error message is displayed"))
            .then(test.assertEquals(true, "v.displayValidationErrors",
                "Invalid field showing errors after user change to xxx"))

            ////////// driver fires events to enable/show validation e.g. on submit save is blocked and errors show

            // this event will cause all invalid fields to display errors, even the 1st 3 which were loaded invalid
            .then(test.wait(helper.fireApplicationEvent("e.c:ff_EventSetValidation", {
                disable: false, show: true
            })))
            .then(test.focus(text2, "invalid loaded field, should display errors when driver fires event"))
            .then(test.assert("v.displayValidationErrors", "validation errors are showing after event"))
            .then(test.assertEquals(false, "v.disableValidation", "validation is still enabled after event"))

            // now disable all validation and all errors should disappear
            .then(test.wait(helper.fireApplicationEvent("e.c:ff_EventSetValidation", {
                disable: true, show: true
            })))
            .then(test.assertEquals(true, "v.disableValidation", "validation is disabled"))
            .then(test.assertEquals([], "v.validationErrors", "No errors are displayed for text2 despite empty value"))

            // now change the validation behaviour using an api fn instead of an event (composites need this)
            .then(test.wait(function (context) {
                text2.setValidation(false, false); // not displayed but not disabled i.e. phase 1 behaviour
            }))
            .then(test.assertEquals(false, "v.disableValidation", "validation is enabled"))
            .then(test.assertEquals(["This field is required"], "v.validationErrors", "Errors are present"))
            .then(test.assertEquals(false, "v.displayValidationErrors", "validation errors are not seen"))
            .then(test.assertEquals(false, "v.isValid", "field is blocking"))

            .then(test.focus(text3, "invalid loaded field, should show no errors when validation disables"))
            .then(test.assertEquals([], "v.specificValidationErrors", "No errors are displayed for text3 despite xxx value"))

            // stop text3 from responding to set validation events
            .then(test.wait(function (context) {
                text3.set("v.ignoreSetValidationEvents", true);
            })) // then re-enable validation for "all" fields
            .then(test.wait(helper.fireApplicationEvent("e.c:ff_EventSetValidation", {
                disable: false, show: true
            })))
            .then(test.assertEquals([], "v.validationErrors", "text3 has no errors because it is ignoring"))
            .then(test.assertEquals([], "v.specificValidationErrors", "text3 has no errors because it is ignoring"))

            // disable a single field and ensure that the placeholder doesn't show
            .then(test.focus(text14, "text field with placeholder"))
            .then(test.setAttribute("v.disabled", true))
            .then(test.assertEquals("", "v.placeHolder", "placeHolder empty when text field is disabled"))
            .then(test.setAttribute("v.disabled", false))
            .then(test.assertEquals("Account Balance", "v.placeHolder", "placeHolder restored when text field is enabled"))

            .then(test.focus(text15, "richtext content"))
            .then(test.assert(function () {
                return !$A.util.isEmpty(text15.get("v.content"));
            }, "The richtext field should be loaded after the load event."))

            .then(test.focus(date1, "date field using lightning input"))
            .then(test.assert("v.isValid", "invalid when required and has a value loaded from driver"))
            .then(test.setAttribute("v.value", undefined))
            .then(test.assertEquals(false, "v.isValid", "invalid when empty but required"))
            .then(test.setAttribute("v.disabled", true))
            .then(test.assert("v.isValid", "not blocking when disabled"))

            .then(test.focus(date2, "date field for native date values"))
            .then(test.assert("v.isValid", "invalid when required and has a value loaded from driver"))
            .then(test.setAttribute("v.value", undefined))
            .then(test.assertEquals(false, "v.isValid", "invalid when empty but required"))
            .then(test.setAttribute("v.disabled", true))
            .then(test.assert("v.isValid", "not blocking when disabled"))
            .then(test.setAttribute("v.disabled", false))
            .then(test.setAttribute("v.value", "2009-01-01"))
            .then(test.assertEquals(true, "v.isValid", "validate produces no validation error"))
            .then(test.setAttribute("v.value", "xxx"))
            .then(test.assertEquals(false, "v.isValid", "validate produces a validation error"))
            //TODO add tests for invalid date format e.g. 13/02/2
            .then(test.focus(date3, "date field for negative date offset"))
            .then(test.setAttribute("v.value", "2017-01-01"))
            .then(test.assertNotEquals([], "v.specificValidationErrors", "date is invalid"))

            //assert that ff_FieldControls respond to an aggregate event
            .then(test.focus(text16, "text field for aggregateKey tests"))
            .then(test.wait(helper.fireApplicationEvent("e.c:ff_EventFindAggregateControls", {
                aggregateKey: "QAS", callback: function (c) {
                    foundAggregate = c;
                }
            })))
            .then(test.assert(function () {
                var g1 = text16.getGlobalId();
                var g2 = foundAggregate.getGlobalId();
                return g1 == g2;
            }, "The component responding to the callback is not correct"))

            .then(test.focus(text17, "text field for max length tests"))
            .then(test.setAttribute("v.value", "1234567890"))
            //cannot test what happens if more than 10 characters are set except manual test
            .then(test.assert(function () {
                return text17.get("v.value").length == 10;
            }, "The text field value has a length of 10 characters"))
            //////////// END OF TESTS ////////////

            // always include these fns to handle the end of the test
            .then(test.pass).catch(test.fail);

    }
})