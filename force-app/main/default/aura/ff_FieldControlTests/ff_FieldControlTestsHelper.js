({
    fireLoadEvent: function (loadKey, sObject, wrappers) {
        var records = {};
        records[loadKey] = [{
            single: true,
            sObjectList: [sObject]
        }];
        return this.fireApplicationEvent("e.c:ff_EventLoadControls", {
            data: {
                records: records,
                wrappers: wrappers
            }
        });
    }
})