({
    doInit: function (component, event, helper) {

        var test = helper.driver(component, event, helper);

        // references to all the components being tested
        var spinner = component.find("spinner");

        var startTests = test.start({
            focused: spinner,
            description: "focused on spinner"
        })

        var foundAggregate;
        // using a single chain.

        startTests

        //assert that ff_FieldControls respond to an aggregate event
            .then(test.wait(helper.fireApplicationEvent("e.c:ff_EventFindAggregateControls", {
                aggregateKey: "SpinnerTest", callback: function (c) {
                    foundAggregate = c;
                }
            })))
            .then(test.assert(function () {
                return !foundAggregate.get("v.isLoading");
            }, "The spinner should be hidden"))
            .then(test.wait(function () {
                foundAggregate.set("v.isLoading", true);
            }))
            .then(test.assert(function () {
                return foundAggregate.get("v.isLoading");
            }, "The spinner should be visible"))
            .then(test.assertEquals("Test Label", "v.loadingLabel"), "The label should be visible and have a value")
            // //////////// END OF TESTS ////////////

            // always include these fns to handle the end of the test
            .then(test.pass).catch(test.fail);
    }
})