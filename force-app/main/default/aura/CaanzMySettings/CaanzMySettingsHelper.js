({
	saveNotifications: function(cmp, callback) {
		var notifications = cmp.find("notifications");
		if(notifications && notifications.save){
			notifications.save({ callback: $A.getCallback(callback) });
		} else if(callback){
			callback();
		}
	},
	savePreferences: function(cmp, callback) {
		var preferences = cmp.find("preferences");
		if(preferences && preferences.save){
			preferences.save({ callback: $A.getCallback(callback) });
		} else if(callback){
			callback();
		}
	},
	getTracker: function(count, callback){
		return function(){
			count--;
			if(!count){
				if(callback) callback();
			}
		}
	},
	validate: function(cmp){
	    if(cmp.get("v.isOnBoardWizard") && $A.util.isEmpty(cmp.get("v.nickname"))){
			cmp.set("v.nicknameError", "You must enter a value"); return false;
		} else {
			cmp.set("v.nicknameError", "");
		}

		if(!this.checkPreferencesValid(cmp)) {
			cmp.set("v.interestsError", "You must select at least one area of interest"); return false;			
		} else {
			cmp.set("v.interestsError", "");
		}

		return true;
	},
	checkPreferencesValid: function(cmp) {
		var groups = cmp.find('preferences').get('v.groups');
		for (var i = groups.length - 1; i >= 0; i--) {
			for (var j = groups[i].groupItems.length - 1; j >= 0; j--) {
				if(groups[i].groupItems[j].myPreference.isSelected) {
					return true;
				}
			}
		}
		return false;
	},
	getUserProfileNickname: function(cmp){
		this.enqueueAction(cmp, "getUserProfileNickname", {}, function(nickname){
			cmp.set("v.nickname", nickname);
		});
	},
	updateUserProfileNickname: function(cmp, callback){
		this.enqueueAction(cmp, "updateUserProfileNickname", { nickname: cmp.get("v.nickname") }, function(){
			if(callback) callback();
		});
	},
	enqueueAction: function(cmp, method, params, callback){
		var action = cmp.get("c." + method); 
		if(params) action.setParams(params);
		action.setCallback(this, function(response){
			if(response.getState() === "SUCCESS") {
				if(callback) callback.call(this, response.getReturnValue());
	        } else if(response.getState() === "ERROR") {
	        	cmp.set("v.nicknameError", JSON.stringify(response.getError()));
	        }
		});
		$A.enqueueAction(action);
	}
})