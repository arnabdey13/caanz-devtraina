({
	doInit: function(cmp, event, helper) {
		cmp.set("v.wizardPrefDesc", " These can be changed or updated at any time through My CA Settings.");
		if(cmp.get("v.isOnBoardWizard")) helper.getUserProfileNickname(cmp);
	},
	performSave: function(cmp, event, helper){
		if(!helper.validate(cmp)) return;
		var params = event.getParam ? event.getParam("arguments").params || {} : {};
		var tracker = helper.getTracker(3, params.callback);
		helper.saveNotifications(cmp, tracker);
		helper.savePreferences(cmp, tracker);
		helper.updateUserProfileNickname(cmp, tracker);
	}
})