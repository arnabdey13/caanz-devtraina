({
    doInit: function (component, event, helper) {
        var state = component.get("v.state");
        helper.adjustState(component, state);
        helper.adjustClickable(component, state);
    },
    handleStateChange: function (component, event, helper) {
        var state = event.getParam("value");
        helper.adjustState(component, state);
        helper.adjustClickable(component, state);
    },
    handleStageSelection: function (component, event, helper) {
        helper.fireApplicationEvent(component,
            "e.c:ff_EventStageClick",
            {
                stage: component.get("v.stage")
            });
    },
    handleSetStageLockEvent: function (component, event, helper) {
        component.set("v.selectableHistory", component.get("v.selectable"));

        var params = event.getParams();

        if(params.lock){
            component.set("v.selectable", false);
        } else{
            component.set("v.selectable", component.get("v.selectableHistory"));
        }
    }
})