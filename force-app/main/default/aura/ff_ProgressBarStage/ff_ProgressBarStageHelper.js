({
    adjustState: function (component, state) {
        switch (state) {
            case "ACTIVE":
                component.set("v.cssState", "active");
                break;
            case "COMPLETE":
                component.set("v.cssState", "complete");
                break;
            case "INVALID":
                component.set("v.cssState", "invalid");
                break;
            default:
                component.set("v.cssState", "");
        }
    },
    adjustClickable: function (component, state) {
        switch (state) {
            case "ACTIVE":
                component.set("v.selectable", false);
                break;
            case "COMPLETE":
                component.set("v.selectable", true);
                break;
            case "INVALID":
                component.set("v.selectable", true);
                break;
            default:
                component.set("v.selectable", false);
        }
    },
    fireApplicationEvent: function (component, eventType, params) {
        var appEvent = $A.get(eventType);
        if (appEvent) {
            if (params) {
                appEvent.setParams(params);
            }
            appEvent.fire();
        } else {
            console.log("no application event registered: " + eventType);
        }
    },
})