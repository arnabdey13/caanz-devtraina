({
	doInit: function(cmp, event, helper) {
		if(helper.isRefreshRequired()) helper.refresh();
	}
})