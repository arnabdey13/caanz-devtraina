({
	refresh: function() {
		var url = window.location.href;   
		window.location.href = url + (url.indexOf('?') > -1 ? '&' : '?') + "ref=1";
	},
	isRefreshRequired: function(){
		var url = window.location.search;
		return (url.indexOf("ref=1") < 0);
	}

})