({
    doInit: function (component, event, helper) {
        if (!component.get("v.loaded")) {
            helper.sendToService(component, "c.load", null, function (destination) {
                $A.log("destination: " + destination);
                component.set("v.destination", destination);
                if (destination) {
                    var navDelay = component.get("v.navigationDelay");
                    if (helper.isAbsolute(component, destination)) {
                        setTimeout(function () {
	                        window.location = destination;
                        }, navDelay);
                    } else {
                        var navEvent = $A.get("e.force:navigateToURL").setParams({
                            "url": destination,
                            "isredirect": false
                        });
                        setTimeout(function () {
                            navEvent.fire();
                        }, navDelay);
                    }
                }
            });
            component.set("v.loaded", true);
        }
    }
})