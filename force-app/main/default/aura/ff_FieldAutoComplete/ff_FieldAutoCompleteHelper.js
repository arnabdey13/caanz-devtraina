({
    filterKeyStroke: function (component, event, helper, key) {
        var matches = component.get("v.matches");
        var pos = component.get("v.focusPosition");
        switch (key) {
            case 37: // LEFT
                break; // do nothing when moving in typed text
            case 38: // UP
                if (pos > 0) {
                    component.set("v.focusPosition", pos - 1);
                    helper.updateMatchFocus(component, event, helper);
                }
                break;
            case 39: // RIGHT
                break; // do nothing when moving in typed text
            case 40: // DOWN
                var maxPosition = matches.length - 1;
                if (component.get("v.isOptOutAllowed")) {
                    maxPosition = maxPosition + 1;
                }
                if (pos < maxPosition) {
                    component.set("v.focusPosition", pos + 1);
                    helper.updateMatchFocus(component, event, helper);
                }
                break;
            case 13: // ENTER
                this.selectMatch(component, event, helper);
                break;
            case 9: // TAB i.e. blur
                $A.log('tab');
                break;
            case 27: // ESCAPE
                $A.log('escape');
                break;
            default:
                var searchText;
                if (event.target) {
                    // normal app use, keycode comes from DOM event
                    searchText = event.target.value;
                } else {
                    // test case use where the keycode comes from code
                    var prev = component.get("v.searchText");
                    if ($A.util.isEmpty(prev)) {
                        prev = "";
                    }
                    searchText = prev + String.fromCharCode(key);
                }
                component.set("v.searchText", searchText);
                if (!component.get("v.optedOut")) {
                    this.requestMatches(component, event, helper);
                }
        }
    },
    selectMatch: function (component, event, helper) {
        var position = component.get("v.focusPosition");
        var matches = component.get("v.matches");
        var isOptOutSelection = position == matches.length;
        if (isOptOutSelection) {
            this.optOut(component, event, helper);
        } else {
            var match = matches[position];
            component.set("v.matchSelected", match);
            component.set("v.searchText", match.label1);
            component.set("v.value", match.id);
            component.set("v.state", "MATCHED");
        }
    },
    requestMatches: function (component, event, helper, doneCallback) {
        component.set("v.lookupCSS", "slds-lookup");
        component.set("v.state", "DEBOUNCING");

        var requestSearch = this.search(component, event, helper, doneCallback);
        if (!$A.util.isUndefinedOrNull(requestSearch)) { // only set timer if search HOF returns a fn
            var timer = this.debounce(requestSearch,
                component.get("v.debounceTimeout"),
                component.get("v.debounceTimer"));
            component.set("v.debounceTimer", timer);
        }
    },
    debounce: function (callback, wait, timeout) {
        if (!$A.util.isUndefinedOrNull(timeout)) {
            clearTimeout(timeout);
        }
        return setTimeout(callback, wait);
    },
    isSearchAllowed: function (component, searchTerm) {
        return !$A.util.isEmpty(searchTerm) && searchTerm.length > 1 && !component.get("v.optedOut");
    },
    search: function (component, event, helper, doneCallback) {
        var searchTerm;
        if (event.target) {
            searchTerm = event.target.value; // normal UI event
        } else {
            searchTerm = component.get("v.searchText"); // for tests, use v.value
        }

        if (this.isSearchAllowed(component, searchTerm)) {
            var appEvent = $A.get("e.c:ff_EventAutoComplete");
            var searchTime = new Date().getTime();

            return $A.getCallback(function () {

                // set UI to show searching spinner
                component.set("v.state", "SEARCHING");

                component.set("v.lastSearchTime", searchTime);

                // request a search
                appEvent.setParams({
                    loadKey: component.get("v.loadKey"),
                    field: component.get("v.field"),
                    searchTerm: searchTerm,
                    searchContext: component.get("v.searchContext"),
                    allowDriverSearch: component.get("v.allowDriverSearch"),
                    callback: helper.handleResults(component, event, helper, searchTime, doneCallback)
                });
                appEvent.fire();
            });
        } else {
            return null;
        }
    },
    handleResults: function (component, event, helper, searchTime, doneCallback) {
        return $A.getCallback(function (matches) {
            var isMostRecentSearch = searchTime == component.get("v.lastSearchTime");

            // don't use results if user has removed characters while the apex call was running
            var searchTerm = component.get("v.searchText");
            var isSearchTermLongEnough = !$A.util.isEmpty(searchTerm) && searchTerm.length > 1;

            if (isSearchTermLongEnough) {
                // don't use results for any but the most recent search
                if (isMostRecentSearch) {
                    component.set("v.focusPosition", 0);
                    component.set("v.matches", (matches || []));
                    component.set("v.state", 'RESULTS');
                    helper.updateMatchFocus(component, event, helper);
                    if (doneCallback) {
                        doneCallback();
                    }
                }
            } else {
                component.set("v.matches", ([]));
                component.set("v.state", 'UNMATCHED');
            }
        });
    },
    updateMatchFocus: function (component, event, helper) {
        var matches = component.get("v.matches");
        var newMatches = [];
        var focused = component.get("v.focusPosition");
        for (var i = 0; i < matches.length; i++) {
            // create new object to workaround Lightning bug where objects must have all keys at create time
            newMatches.push({
                position: i,
                id: matches[i].id,
                label1: matches[i].label1,
                label2: matches[i].label2,
                focused: (i == focused)
            })
        }
        component.set("v.matches", newMatches);

        // if there are matches then scroll the list after the user focuses
        if (newMatches.length > 0) {
            var list = component.find("list");
            var listElement = list.getElement();
            if (!$A.util.isUndefinedOrNull(listElement)) { // defensive because sometimes this was undefined in Apex callback, not sure why
                var heightUnit = listElement.firstChild.offsetHeight;
                listElement.scrollTop = (focused * heightUnit) - (2 * heightUnit);
            }
        }
    },
    optOut: function (component, event, helper) {
        component.set("v.optedOut", true);
    },
    optIn: function (component, event, helper) {
        component.set("v.optedOut", false);
    },
    focusOnInputElement: function (component) {
        var inputElement = component.find("input").getElement();
        inputElement.focus();
    }
})