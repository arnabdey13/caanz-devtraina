({
    handleKeyUp: function (component, event, helper) {
        var keyCode = event.keyCode || event.which;
        helper.filterKeyStroke(component, event, helper, keyCode);
    },
    apiKeyUp: function (component, event, helper) {
        var keyCode = event.getParam("arguments").keyCode;
        helper.filterKeyStroke(component, event, helper, keyCode);
    },
    handleFocus: function (component, event, helper) {
        // when user focuses on input, that signals intent to search i.e. opt-in
        component.set("v.state", "INIT");
        if (component.get("v.optInOnFocus") && component.get("v.optedOut")) {
            helper.optIn(component);
        }
        var extension = component.get("v.focusHandlerExtension");
        if (extension) {
            extension(event);
        }
    },
    apiRequestMatches: function (component, event, helper) {
        var args = event.getParam("arguments");
        helper.requestMatches(component, event, helper, args.doneCallback);
    },
    handleOptOutClick: function (component, event, helper) {
        helper.optOut(component, event, helper);
    },
    apiOptOut: function (component, event, helper) {
        helper.optOut(component, event, helper);
    },
    handleOptOutChange: function (component, event, helper) {
        // when opting out, focus back into the input
        if (event.getParam("value")) {

            // if an opted-in value is present, it will be tracked in the driver.
            // then fire an event to remove the value in the driver
            if (!$A.util.isEmpty(component.get("v.value"))) {
                helper.fireApplicationEvent(component,
                    "e.c:ff_EventValueRemove",
                    {
                        loadKey: component.get("v.loadKey"),
                        field: component.get("v.field")
                    })
            }

            // need to stop the focus handler above from opting-in when this code is running because of opt-out
            var optInOnFocusMarkupValue = component.get("v.optInOnFocus");
            component.set("v.optInOnFocus", false);
            helper.focusOnInputElement(component);

            // need a delay here because in IE, the focus handle above runs async and too slow and so the
            // v.optInOnFocus has been set back to original value by the time it runs
            setTimeout($A.getCallback(function () {
                if (component.isValid()) {
                    component.set("v.optInOnFocus", optInOnFocusMarkupValue);
                }
            }), 500);

            // if present, these alternative loadKey.field will create nested parent writeData records in the driver
            component.set("v.loadKeyOverride", component.get("v.optedOutLoadKey"));
            component.set("v.fieldOverride", component.get("v.optedOutField"));

            // also set the searchText into the value since every change will now represent value
            component.set("v.value", component.get("v.searchText"));
        } else {

            // if an opted-out value is present, it will be tracked in the driver.
            // then fire an event to remove the value in the driver
            if (!$A.util.isEmpty(component.get("v.value"))) {
                helper.fireApplicationEvent(component,
                    "e.c:ff_EventValueRemove",
                    {
                        loadKey: component.get("v.optedOutLoadKey") || component.get("v.loadKey"),
                        field: component.get("v.optedOutField") || component.get("v.field")
                    })
            }

            component.set("v.loadKeyOverride", null);
            component.set("v.fieldOverride", null);
        }

        // notify any container components of the opt out state change
        helper.fireApplicationEvent(component,
            "e.c:ff_EventAutoCompleteOptOut",
            {
                optedOut: event.getParam("value"),
                loadKey: component.get("v.loadKey"),
                field: component.get("v.field")
            });
    },
    handleSearchTextChange: function (component, event, helper) {
        // when opted-in, only a match selection should set the value
        if (component.get("v.optedOut")) {
            component.set("v.value", event.getParam("value"));
        } else if ($A.util.isEmpty(component.get("v.searchText"))) {
            // when user clears the field in opt-in mode, they mean to clear the value
            component.set("v.value", "");
        }
    },
    handleStateChange: function (component, event, helper) {
        var state = event.getParam("value");
        var allowed = {INIT: true, DEBOUNCING: true, SEARCHING: true, RESULTS: true, MATCHED: true, UNMATCHED: true};
        var matched = allowed[state];
        if ($A.util.isUndefinedOrNull(matched)) {
            throw Error("invalid state: " + state);
        }
        if (state == "RESULTS") {
            var clickAwayListener = function (event) {
                if (component.isValid()) { // defensive in case the component has been hidden
                    var isClickInsideMe = component.getElement().contains(event.target);
                    if (!isClickInsideMe) {
                        component.set("v.state", "UNMATCHED");
                    }
                }
            };
            window.addEventListener("mousedown", clickAwayListener, false);
            component.set("v.clickAwayListener", clickAwayListener);
        }
    },
    handleDestroy: function (component, event, helper) {
        if (component.isValid()) {
            var listener = component.get("v.clickAwayListener");

            if (!$A.util.isUndefinedOrNull(listener)) {
                window.removeEventListener("mousedown", listener, false);
            }
        }
    },
    handleSelection: function (component, event, helper) {
        event.stopPropagation();
        component.set("v.focusPosition", event.getParam("position"));
        helper.selectMatch(component, event, helper);
        helper.focusOnInputElement(component);
    },
    handleLifeCycle: function (component, event, helper) {
        switch (event.getParam("type")) {
            case "load-field":
                component.set("v.searchText", component.get("v.value"));
                break;
        }
    },
    apiFocusOnInput: function (component, event, helper) {
        helper.focusOnInputElement(component);
    }
})