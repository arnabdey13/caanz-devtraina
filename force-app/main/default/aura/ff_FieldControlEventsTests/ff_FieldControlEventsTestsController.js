({
    doInit: function (component, event, helper) {

        var test = helper.driver(component, event, helper);

        // references to all the components being tested
        var select1 = component.find("select1");
        var select2 = component.find("select2");
        var checkbox1 = component.find("checkbox1");
        var checkbox2 = component.find("checkbox2");
        var autocomplete10 = component.find("autoComplete10");
        var radio1 = component.find("radio1");
        var radio2 = component.find("radio2");

        var startTests = test.start({
            focused: select1,
            description: "valid state after load"
        })

        var text1EventExpected = {
            "loadKey": "Account",
            "field": "Premium1__c",
            "value": true,
            "valid": true
        };

        var wrappers = {};
        wrappers["opts1"] = [
            {value: "1", label: "1 Egg"},
            {value: "2", label: "2 Eggs"}];
        // this is how dependent picklist values are linked to parent values
        wrappers["opts2"] = [
            {value: "t", label: "Tea", parentValues: ["1", "2"]},
            {value: "c", label: "Coffee", parentValues: ["2"]}];
        wrappers["Radio.yn"] = [
            {value: true, label: "Yes"},
            {value: false, label: "No"}];

        startTests

        // must load select controls using an EventLoadControls to load the options correctly

            .then(test.wait(helper.fireLoadEvent("Account",
                {
                    "Premium2__c": true,
                    "Radio1__c": true,
                    "Radio2__c": false
                },
                wrappers)))

            .then(test.assert("v.isValid", "loaded number is valid"))

            .then(test.wait(function (context) {
                select1.set("v.value", "2");
            }))
            .then(test.focus(component, "event log assertions for select 1"))
            .then(test.assertEquals({
                "loadKey": "Account",
                "field": "Breakfast",
                "value": "2",
                "valid": true
            }, "v.mostRecentEventParams", "value change event was fired"))

            ////////// dependent picklist options stay correctly in sync

            .then(test.focus(select2, "2nd select depends on first"))
            .then(test.assertEquals([
                {value: "t", label: "Tea", parentValues: ["1", "2"]},
                {value: "c", label: "Coffee", parentValues: ["2"]}
            ], "v.options", "All options provided for 2 eggs"))
            .then(test.wait(function (context) {
                select1.set("v.value", "1");
            }))
            .then(test.assertEquals([
                {value: "t", label: "Tea", parentValues: ["1", "2"]},
            ], "v.options", "Only tea provided for 1 egg"))

            .then(test.wait(function (context) {
                select2.set("v.value", "t");
            }))
            .then(test.wait(function (context) {
                // TODO this action done manually after the test runs will cause a renderer error
                // to reproduce, comment out this set and do it manually after the test completes
                select1.set("v.value", "2");
            }))
            .then(test.wait(function (context) {
                select1.set("v.value", "2");
                select2.set("v.value", "c");
                select1.set("v.value", "1");
            }))
            .then(test.assert(function(){
                return $A.util.isEmpty(select2.get("v.value"));
            }, "value should be empty because coffee is not available"))


            ////////// checkbox value change event tests

            .then(test.focus(checkbox1, "checkbox changed via attribute should fire change events"))
            .then(test.assert("v.loaded", "checkbox was loaded by load event"))
            .then(test.wait(function (context) {
                checkbox1.set("v.value", true);
                //console.log(component.get("v.mostRecentEventParams").field);
            }))
            .then(test.focus(component, "event log assertions for checkbox1"))
            .then(test.assertEquals(text1EventExpected, "v.mostRecentEventParams",
                "value change event was fired for checkbox1"))

            ////////// checkboxes fire value change events when expected

            .then(test.focus(checkbox2, "checkbox loaded already checked"))
            .then(test.assert("v.loaded", "checkbox was loaded by load event"))
            .then(test.wait(function (context) {
                checkbox2.set("v.value", true);
            }))
            .then(test.focus(component, "event log assertions for checkbox2"))
            .then(test.assertEquals(text1EventExpected, "v.mostRecentEventParams",
                "no new value change event was fired because checkbox2 value set during load"))

            .then(test.focus(autocomplete10, "autocomplete should not fire value change events when opted-in"))
            .then(test.assertEquals(false, "v.optedOut", "autocomplete is opted-in by default"))

            .then(test.focus(component, "event log assertions for autocomplete"))
            .then(test.assertEquals(text1EventExpected, "v.mostRecentEventParams",
                "no new value change event was fired because autocomplete in opt-in mode"))

            .then(test.focus(autocomplete10, "autocomplete should not fire value change events when opted-in"))
            .then(test.setAttribute("v.searchText", "")) // user clears the field
            .then(test.wait(function (context) {
                //console.log(JSON.stringify(component.get("v.mostRecentEventParams")));
            }))
            .then(test.focus(component, "event log assertions for autocomplete"))
            .then(test.assertEquals({
                    "loadKey": "Account",
                    "field": "Name",
                    "value": "",
                    "valid": false
                }, "v.mostRecentEventParams",
                "value change event fired for autocomplete because user cleared it"))

            .then(test.focus(radio1, "the first of two radio buttons"))
            .then(test.setAttribute("v.value", false))

            //////////// END OF TESTS ////////////

            // always include these fns to handle the end of the test
            .then(test.pass).catch(test.fail);

    },
    handleValueChange: function (component, event, helper) {
        var params = event.getParams();
        //console.log(JSON.stringify(params));
        component.set("v.mostRecentEventParams", {
            loadKey: params.loadKey,
            field: params.field,
            value: params.value,
            valid: params.valid
        });
    }
})