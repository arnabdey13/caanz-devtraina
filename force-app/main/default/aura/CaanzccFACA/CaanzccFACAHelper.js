({
    sendToService : function(component, event, helper, method, params, errorAttr, callback) {
        helper.showSpinner(component, event, helper);
        var action = component.get(method);
        if (params) {
            action.setParams(params);
        }
        action.setCallback(this, function(response) {
            var state = response.getState();
            helper.hideSpinner(component, event, helper);
            if (state === "SUCCESS") {
                callback(response.getReturnValue());
            } else {
                helper.showAlert(component, event, 'error', component.get(errorAttr));
            }
        });
        $A.enqueueAction(action);
    },
    showAlert : function(component, event, type, message){
        component.set("v.alertType", type);
        component.set("v.alertMessage", message);
        component.set("v.showAlert",'slds-show');
        window.setTimeout($A.getCallback(function () {
            component.set("v.showAlert",'slds-hide');
        }), component.get("v.alertTimeout"));
    },
    showSpinner : function(component, event, helper) {
        component.set("v.waitingForApex",'');
    },
    hideSpinner : function(component, event, helper) {
        component.set("v.waitingForApex",'slds-hide');
    },
    	checkForUserPermissions: function(cmp) {
		var customPermission = cmp.get("v.customPermission");
		if(!$A.util.isEmpty(customPermission)){
			this.getUserPermissions(cmp, [customPermission]);
		}
	},
	getUserPermissions: function(cmp, customPermissions){
		var action = cmp.get("c.getUserPermissions"); 
		action.setParams({ customPermissions: customPermissions });
		action.setCallback(this, function(response) {
			if(response.getState() === "SUCCESS") {
	            this.applyUserPermissions(cmp, JSON.parse(response.getReturnValue()));
	        } else if(response.getState() === "ERROR") {
	        	this.showError(response.getError());
	        }
		});
		$A.enqueueAction(action);
	},
	applyUserPermissions: function(cmp, userPermissions){
		var customPermission = cmp.get("v.customPermission");
		cmp.set("v.isVisible", userPermissions.includes(customPermission));
	},
	showError: function(errors){
        var message = 'Unknown error'; 
        if ($A.util.isArray(errors) && !$A.util.isEmpty(errors)) {
            message = errors[0].message;
        }
        alert(message);
	}

})