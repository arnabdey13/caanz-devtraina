({
    load : function(component, event) {
        component.set("v.value", event.getParam("properties").RECORD[component.get("v.field")])
    }
})