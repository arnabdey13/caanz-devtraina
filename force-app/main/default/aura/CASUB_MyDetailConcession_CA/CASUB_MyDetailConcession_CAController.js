({
    doInit: function(component, event, helper) {
       helper.doSetUpForFinancialCategory(component, event, helper);
       // component.set('v.helpText', 'Career Break (I have temporarily withdrawn from the Workforce & Annual Income < 30,000) OR Low Income (my Annual income is < 30,000) OR Retired (I have permanently withdrawn from the Workforce & Annual Income < 30,000)');
       component.set('v.helpText', 'If you are eligible, please choose a concession type');
    },
    
    selectChange : function(component, event, helper) {
        var checked = component.get("v.checked");
        component.set("v.errorMessage",'');
        if(checked==true){
            component.set("v.changeitClass","slds-show");
            
        }else{
            component.set("v.selectedFinancialCategory","");
			component.set("v.changeitClass","slds-hide");
        }
    },
    handleConcessionChange:function(component, event, helper){
        component.set("v.errorMessage",'');
        var changeValue = event.getParam("value");
    },
    handleOnSaveChange:function(component, event, helper){
        if($A.util.isEmpty(component.get("v.selectedFinancialCategory"))){
            component.set("v.errorMessage",'Select Concession Type to Save');
        }else{
            component.set("v.errorMessage",'');
        var cmpEvent = component.getEvent("concession_Update_Event");
        cmpEvent.setParams({"concessionType" : component.get("v.selectedFinancialCategory")});
        cmpEvent.fire(); 
        }
        
    },
    validateConcessionOptions : function(component, event, helper){
        /**
         *  If the value is Retired/Career break Break/Low income concession
         */ 
        var errorMessage='';
        var checked = component.get("v.checked");
        if(!component.get("v.disableConcessions")){
            if(checked && $A.util.isEmpty(component.get("v.selectedFinancialCategory"))){
                errorMessage = 'You have opted for Concession, but not selected any of the options, please select one of the option and Save it or Opt Out the concession';
            }
            else if(checked && component.get("v.selectedFinancialCategory")!=component.get("v.account.Financial_Category__c")){
                errorMessage = 'You have opted for ' 
                      + component.get("v.selectedFinancialCategory") +' but not saved Concession, Please click Save to proceed.' ;
            }
        }
        
        var params = event.getParam ? event.getParam("arguments").params || {} : {};
        /**
         * Check whether ARTTB-343 Conditions
         * Financial categories to be considered: (Standard Fee, Retired, Low Income, Career Break, Academic, MICPA)
 		 * Lightning Data service to update the accounts financial category field to "Standard Fee"
         */ 
        var disableConcessions = component.get("v.disableConcessions");
        var selectedFinancialCategory = component.get("v.selectedFinancialCategory");
        var isChecked = component.get("v.checked");
        var isStandardFeeUpdate=false;
        if(!disableConcessions && (!isChecked && $A.util.isEmpty(selectedFinancialCategory))){
            if(component.get("v.account.Financial_Category__c")!=component.get("v.standardFeeCategory")){
                isStandardFeeUpdate = true;
            }
        }
               
        if(!$A.util.isEmpty(errorMessage)) {
			//component.set("v.errorMessage",errorMessage);
            helper.showToast(component,errorMessage);
        }else if(isStandardFeeUpdate){
            //component.set("v.errorMessage",'');
            helper.saveDefaultFinancialCategory(component,component.get("v.standardFeeCategory"),params); 
        }else if(params.callback) {
            //component.set("v.errorMessage",'');
            params.callback();
		}
        /*
        if(!$A.util.isEmpty(errorMessage) && !$A.util.isEmpty(errorMessage)){
            var validateNextButtonEvent = component.getEvent("cASUB_ValidateNextEvent_CA");
            validateNextButtonEvent.setParams({"componentName" : 'Concession','errorMessage':errorMessage});
            validateNextButtonEvent.fire();
        }
        */
        
        
        
    },
    
    showHelpText : function(component,event,helper){
        var cmp = component.find('helptext');
        $A.util.removeClass(cmp,'slds-hide');
        
    },
    
    hideHelpText : function(component,event,helper){
        var cmp = component.find('helptext');
        $A.util.addClass(cmp,'slds-hide');
        
    }
})