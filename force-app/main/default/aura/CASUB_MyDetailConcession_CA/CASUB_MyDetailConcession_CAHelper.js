({
    doSetUpForFinancialCategory : function(component,event,helper){
        this.setEligiblityAccess(component,event);
        var optionForShow=[];
        if(!component.get("v.disableConcessions")){
            var casubConcessionSetting = component.get("v.casubConcessionSetting");
            var allowedConcessions = casubConcessionSetting.CASUB_Concession_Show__c;
           // var allowedConcessions = $A.get("$Label.c.CASUB_Concession_Show");
            var allowedConcessionArray = allowedConcessions.split(",");
            var financialCategoryPickList = component.get("v.financialCategoryPickList");
            for(var i = 0; i <allowedConcessionArray.length; i++){
                if(allowedConcessionArray[i] == component.get("v.account.Financial_Category__c")){
                    component.set("v.selectedFinancialCategory", component.get("v.account.Financial_Category__c"));
                    component.set("v.checked",true);
                }
                for (var j = 0; j < financialCategoryPickList.length; j++) {
                    if(financialCategoryPickList[j].label===allowedConcessionArray[i].trim()){
                        var option = {};
                        //option.label = financialCategoryPickList[j].label;
                        option.value = financialCategoryPickList[j].value;  
                        if(option.value=='Career break concession'){
                            option.label = 'Career Break (I have temporarily withdrawn from the Workforce & Annual Income < 50,000)'; 
                        }else if(option.value=='Low income concession'){
                            option.label = 'Low Income (My Annual income  is < 50,000)';
                        }else if(option.value=='Retired'){
                            option.label = 'Retired (I have permanently withdrawn from the Workforce & Annual Income < 50,000)';
                        }
                        
                        optionForShow.push(option);
                    }
                }
            }
            component.set("v.financialCategoryToShow",optionForShow);
        }
    },
    setEligiblityAccess : function(component,event){
        /* Added by Sumit to disable concession toggle after september 1st */
        var user_timezone = $A.get("$Locale.timezone");
        var user_date = new Date().toLocaleString("en-US", {timeZone: user_timezone})
        var lateFeeDate = $A.get("$Label.c.CASUB_LateFee");
        if(user_date == lateFeeDate || user_date > lateFeeDate){
            component.set("v.disableConcessions", "true"); 
        }
        var casubConcessionSetting = component.get("v.casubConcessionSetting");
        var concessions = casubConcessionSetting.CASUB_Not_Allowed_Concession__c;
        //var concessions = $A.get("$Label.c.CASUB_Not_Allowed_Concession");
        var notAllowedConcession = concessions.split(",");
        var account=component.get("v.account");
        var NMP = $A.util.isUndefined(account.NMP__c)? false : account.NMP__c;
        var Membership_Class = $A.util.isUndefined(account.Membership_Class__c)?'':account.Membership_Class__c;
        
        /**
         * 
         */
        
        if(NMP || (component.get("v.profileName") == $A.get("$Label.c.CAANZ_Profile_Provisional_Member") || 
                   Membership_Class=='Affiliate' || Membership_Class=='Provisional')){
           
              component.set("v.disableConcessions", "true");    
             component.set("v.isNMPORProvisional", "false");
        }else{
            for(var i = 0; i <notAllowedConcession.length; i++){
            if(notAllowedConcession[i]==component.get("v.account.Financial_Category__c")){
                    component.set("v.disableConcessions", "true");    
                    break;
            }
        }
        }
        
        
    },
    saveDefaultFinancialCategory:function(component,concessionType,callBackParams){
        var param = {'personAccountId':component.get("v.account.Id"),'financialCategory':concessionType};
    	this.enqueueAction(component, "saveFinancialCategory", param, function(error, data){
            if(error){
             //component.set("v.errorMessage",error);
 			 this.showToast(component,error);
            }else if(callBackParams.callback) {
            component.set("v.errorMessage",'');
            callBackParams.callback();
		}
                
		});
	},
    enqueueAction: function(component, method, params, callback){
		this.toggleSpinner(component, true);
		var action = component.get("c." + method); 
		if(params) action.setParams(params);
		action.setCallback(this, function(response){
			if(response.getState() === "SUCCESS") {
				if(callback) callback.call(this, null, response.getReturnValue());
	        } else if(response.getState() === "ERROR") {
	        	var message = 'Unknown error'; 
	        	var errors = response.getError();
	            if (!$A.util.isEmpty(errors)) {
	                message = errors[0].message;
	            }
	        	console.error(message);
	        	if(callback) callback.call(this, message);
	        }
	        this.toggleSpinner(component, false);
		});
		$A.enqueueAction(action);
	},
	toggleSpinner: function(component, show) {
		var spinner = component.find("spinner");
		$A.util.addClass(spinner, show ? "slds-show" : "slds-hide");
		$A.util.removeClass(spinner, !show ? "slds-show" : "slds-hide");
	},
    showToast : function(component,text) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Warning!",
            "message": text,
            type : 'warning',
            mode: 'dismissible',
            duration :10000
        });
        toastEvent.fire();
    }
	
})