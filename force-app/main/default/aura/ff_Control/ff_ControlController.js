({
    doInit: function (component, event, helper) {
        for (var i = 1; i <= 5; i++) { //WDCi (Lean) 15/10/2018 PAN:5340 - set to 5 to support 5th controlling field
            helper.loadParent(component, "parent" + i);
        }
        helper.adjustHeight(component);
    },
    handleSetAttributes: function (component, event, helper) {
        var params = event.getParams();
        if (!$A.util.isUndefined(params.heightUnitInPixels)) {
            component.set("v.heightUnitInPixels", params.heightUnitInPixels);
            helper.adjustHeight(component);
        }
    },
    loadControl: function (component, event, helper) {
        // set the readData into local state so that specific controls can handle it.
        // using an attribute to drive sub-component code is a workaround to the Template pattern
        // that is no longer possible via over-ridden helper functions when the LockerService is enabled.
        // the problem stops processes initiated by a super component function from being able to use helper
        // functions which are over-ridden by concrete sub-components. NOTE: this still works if the process
        // is initiated by a concrete component function.
        var loadParams = event.getParams();       
        component.set("v.loadData", loadParams);

        var parentValues = helper.getParentValuesFromLoadData(component, loadParams);
        for (var i = 1; i <= 5; i++) { //WDCi (Lean) 15/10/2018 PAN:5340 - set to 5 to support 5th controlling field
            var parent = helper.getParent(component, i);
            if (parent) {
                component.set("v.parent" + i + "Value", parentValues[parent.loadKey + "." + parent.field]);
            }
        }

        helper.deriveVisibility(component);

        helper.adjustValueFromVisibility(component);

        component.set("v.loaded", true); // set control into loaded state

    },
    handleStageChange: function (component, event, helper) {
        // only set current attr here, let fn below process stage changes i.e. de-dupe change events
        component.set("v.currentStage", event.getParam("stage"));
    },
    handleCurrentStageChange: function (component, event, helper) {
        helper.deriveVisibility(component);
    },
    handleValueChange: function (component, event, helper) {
        var isDependentVizChanging = false;
        if ($A.util.isUndefinedOrNull(event.getParam("field"))) {
            var params = event.getParams();

            throw Error("Empty field in value change event source. " + JSON.stringify(params));
        }
        if ($A.util.isUndefinedOrNull(event.getParam("loadKey"))) {
            throw Error("Empty loadKey in value change event source.");
        }
        for (var i = 1; i <= 5; i++) { //WDCi (Lean) 15/10/2018 PAN:5340 - set to 5 to support 5th controlling field
            var parent = helper.getParent(component, i);
            if (parent) {
                var isEventFromParent = parent.loadKey == event.getParam("loadKey") &&
                    parent.field == event.getParam("field");
                if (isEventFromParent) {
                    component.set("v.parent" + i + "Value", event.getParam("value"));
                    if (!isDependentVizChanging) {
                        isDependentVizChanging = true;
                    }
                }
            }
        }
        if (isDependentVizChanging) {
            helper.deriveVisibility(component);
            helper.adjustValueFromVisibility(component);
        }
    }
})