({
    loadParent: function (component, attributeName) {
        var parent = component.get("v." + attributeName);

        if (parent) {
            var parts = parent.split(".");
            if (parts.length != 2) {
                throw Error("visibility parent must be loadKey.field: " + parent);
            }
            component.set("v." + attributeName + "LoadKey", parts[0]);

            component.set("v." + attributeName + "Field", parts[1]);

            var csv = component.get("v." + attributeName + "ValuesCSV");
            if ($A.util.isUndefined(csv)) {
                throw Error("visibility parent values are required: " + attributeName)
            }
            var values = csv.split(",");
            var valueArray = [];
            for (var i = 0; i < values.length; i++) {
                switch (values[i]) {
                    case "true":
                        valueArray.push(true);
                        break;
                    case "false":
                        valueArray.push(false);
                        break;
                    default:
                        valueArray.push(values[i]);
                }
            }
            component.set("v." + attributeName + "Values", valueArray);
        }
    },
    // returned parentValues is a map of field to value e.g. {"Account.Name": "Acme"}
    getParentValuesFromLoadData: function (component, loadParams) {
        // var isSingleRecord = records[loadPosition].single; // TODO only allow single record loadKeys to control viz
        var data = loadParams.data;

        var values = {};
        for (var i = 1; i <= 4; i++) {
            var parent = this.getParent(component, i);
            if (parent) {
                var parentName = parent.loadKey + "." + parent.field;
                var parentData = data.records[parent.loadKey];
                if ($A.util.isUndefined(parentData)) {
                    throw Error("No parent data found. loadKey: " + component.get("v.loadKey") + " parent: " + parentName);
                }
                var parentWrapperPosition = 0; // TODO should this be an attribute for each parent as well?
                var parentWrapper = parentData[parentWrapperPosition];
                if ($A.util.isUndefined(parentWrapper)) {
                    throw Error("No parent wrapper found. loadKey: " + component.get("v.loadKey") + " parent: " + parentName);
                }
                var parentValue = parentWrapper.sObjectList[0][parent.field];
                values[parentName] = parentValue;
            }
        }
        return values;
    },
    deriveVisibility: function (component) {

        var condition = component.get("v.parentVisibilityCondition");

        var parentControlledVisible = true;
        var parentControlledVisibleOr = false;

        for (var i = 1; i <= 5; i++) { //WDCi (Lean) 15/10/2018 PAN:5340 - set to 5 to support 5th controlling field
            var parent = this.getParent(component, i);
            if (parent) {
                var parentValue = component.get("v.parent" + i + "Value");
                var parentVisibility = this.isVisibleByParentValue(component, parent, parentValue);
                if (condition == "AND") {
                    parentControlledVisible = parentControlledVisible && parentVisibility;
                } else if (condition == "OR") {
                    parentControlledVisibleOr = parentControlledVisibleOr || parentVisibility;
                }
            }
        }

        if (condition == "OR") {
            parentControlledVisible = parentControlledVisibleOr;
        }

        component.set("v.parentVisibility", parentControlledVisible);

        var stage = component.get("v.currentStage");
        var stageFilter = component.get("v.visibleForStage");
        if ($A.util.isUndefinedOrNull(stageFilter)) {
            // when no stage filter, dependent viz has control
            component.set("v.visible", parentControlledVisible);
        } else {
            var hiddenByStage = stage != stageFilter;
            if (hiddenByStage) { // stage viz always take precedence
                component.set("v.visible", false);
            } else { // if stages match the dependent viz has control
                component.set("v.visible", parentControlledVisible);
            }
        }

    },
    getParent: function (component, parentNum) {
        var parent = {
            loadKey: component.get("v.parent" + parentNum + "LoadKey"),
            field: component.get("v.parent" + parentNum + "Field"),
            values: component.get("v.parent" + parentNum + "Values")
        }
        if (parent.loadKey) {
            if ($A.util.isEmpty(parent.field)) {
                throw Error("Parent field name missing. Load key: " + parent.loadKey);
            }
            if ($A.util.isEmpty(parent.values)) {
                throw Error("Parent value list missing. Field: " + parent.loadKey + "." + parent.field);
            }
            return parent;
        }
    },
    isVisibleByParentValue: function (component, parent, parentValue) {
        var valueMatches = {};
        if (parent.values) {
            for (var i = 0; i < parent.values.length; i++) {
                valueMatches[parent.values[i]] = true;
            }
        }

        if (valueMatches[parentValue]) {
            return true;
        } else {
            return false;
        }
    },
    adjustValidation: function (component, helper, display, disable) {
        component.set("v.displayValidationErrors", display);
        component.set("v.disableValidation", disable);
        this.validate(component, event, helper);
    },
    adjustValueFromVisibility: function (component) {
        var value = component.get("v.value");
        var parentControlledVisible = component.get("v.parentVisibility");

        if (parentControlledVisible) {
            // when a control is changing to visible from a parent, it needs to re-report it's validity to the driver
            // but only after load i.e. reporting validity during load is done via the event source, not valueChange events
            if (component.get("v.loaded")) {
                // must re-evaluate validity before firing the event
                // use life-cycle event to allow concrete controls to implement validation
                component.getEvent("lifeCycleEvent").setParams({ type: "validate" }).fire();

                // now tell driver about validation state
                this.fireValueChange(component);
            }
        } else {
            // if a field is hidden by parent values clear it's value. this will fire a value change event if required
            if (!component.get("v.suppressParentVizValueClear")) {
                /** RXP Fix 12/08/2019 - start - If a checkbox control and value is null, set to false, as this was causing server side error as checkbox values must be true/false **/
                var concreteComponent = component.getConcreteComponent();
                var componentType = '';
                if (concreteComponent.getType) {
                    componentType = concreteComponent.getType();
                }
                var nullValue = (componentType == "c:ff_FieldCheckboxControl") ? false : null;
                component.set("v.value", nullValue);
                /** RXP Fix 12/08/2019 - end - If a checkbox control and value is null, set to false, as this was causing server side error as checkbox values must be true/false **/
            }
        }
    },
    fireValueChange: function (component) {

        // TODO this really should not be in control since value only makes sense for FieldControl

        //do not fire value change event for specific controls
        if (!component.get("v.suppressValueChange")) {

            var loadKey = component.get("v.loadKeyOverride") || component.get("v.loadKey");
            var field = component.get("v.fieldOverride") || component.get("v.field");
            var initialValue = component.get("v.value");

            /** RXP Fix 12/08/2019 - start - If a checkbox control and value is null, set to false, as this was causing server side error as checkbox values must be true/false **/
            if (initialValue === null) {
                var concreteComponent = component.getConcreteComponent();
                var componentType = '';
                if (concreteComponent.getType) {
                    componentType = concreteComponent.getType();
                }

                if (componentType == "c:ff_FieldCheckboxControl") {
                    initialValue = false;
                }
            }
            /** RXP Fix 12/08/2019 - end - If a checkbox control and value is null, set to false, as this was causing server side error as checkbox values must be true/false **/

            this.fireApplicationEvent(component,
                "e.c:ff_EventValueChange",
                {
                    loadKey: loadKey,
                    field: field,
                    value: initialValue,
                    valid: component.get("v.isValid"),
                    source: component,
                    stage: component.get("v.visibleForStage"),
                    ignore: component.get("v.valueChangeIgnored")
                });
        }
    },
    fireApplicationEvent: function (component, eventType, params) {
        var appEvent = $A.get(eventType);
        if (appEvent) {
            if (params) {
                appEvent.setParams(params);
            }
            appEvent.fire();
        } else {
            console.log("no event registered: " + eventType);
        }
    },
    adjustHeight: function (component) {

        var singleUnitHeight = component.get("v.heightUnitInPixels");
        if (component.get("v.customHeightUnitInPixels")) {
            singleUnitHeight = component.get("v.customHeightUnitInPixels");
        }

        var units = component.get("v.heightUnits");
        component.set("v.inlineCSS", "min-height:" + (singleUnitHeight * units) + "px");
    }
})