({
    doInit: function (component, event, helper) {

        var test = helper.driver(component, event, helper);

        // references to all the components being tested
        var ac1 = component.find("ac1");
        var driver = component.find("driver");

        var samples = {
            optOutAccountName: "Chiefs Rugby Inc.",
            optedInNestedRecord: {
                "attributes": {
                    "type": "Account"
                },
                "Id": "id1"
            },
            optedOutNestedRecord: {
                "attributes": {
                    "type": "Account"
                },
                "Name": "Chiefs Rugby Inc."
            }
        };

        test.start({
            focused: ac1,
            description: "loaded invalid but not showing errors"
        })

            .then(test.whenDone(function (context, done) {
                ac1.keyUp(65); // 65 = A, 67 = C
                ac1.keyUp(67);
                ac1.requestMatches(done);
            }))
            .then(test.wait(function () {
                ac1.keyUp(13); // 13 = enter i.e. pick first match
            }))

            .then(test.assertEquals(samples.optedInNestedRecord, function () {
                var wd = driver.get("v.writeData");
                return wd.records[wd.focusId].Other_Account__r;
            }, "driver writeData only has nested record containing opted-in values"))

            // opt-out and enter text
            .then(test.wait(function () {
                ac1.set("v.optedOut", true);
                ac1.set("v.value", samples.optOutAccountName);
            }))
            // now the opted-in Id should have been removed
            .then(test.assertEquals(samples.optedOutNestedRecord, function () {
                var wd = driver.get("v.writeData");
                return wd.records[wd.focusId].Other_Account__r;
            }, "driver writeData only has nested record containing opted-out values"))

            // opt-in and search/choose
            .then(test.whenDone(function (context, done) {
                ac1.set("v.optedOut", false);
                ac1.keyUp(65); // 65 = A, 67 = C
                ac1.keyUp(67);
                ac1.requestMatches(done);
            }))
            .then(test.wait(function () {
                ac1.keyUp(13); // 13 = enter i.e. pick first match
            }))

            .then(test.assertEquals(samples.optedInNestedRecord, function () {
                var wd = driver.get("v.writeData");
                return wd.records[wd.focusId].Other_Account__r;
            }, "driver writeData only has nested record containing opted-in values"))

            .then(test.pass).catch(test.fail);
    }
})