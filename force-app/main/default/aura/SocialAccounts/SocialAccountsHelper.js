({
	displayfacebooklinkhelper: function(component) {
        var action = component.get("c.displaylinktofacebook");
        //Set up the callback
        var self = this;
        action.setCallback(this, function(actionResult) {
            component.set("v.displayfblink", actionResult.getReturnValue());            
        });
        $A.enqueueAction(action);
	},    
    displaylinkedinlinkhelper: function(component) {
        var action = component.get("c.displaylinkedinlinkapex");
        //Set up the callback
        var self = this;
        action.setCallback(this, function(actionResult) {
            component.set("v.displaylinktolinkedin", actionResult.getReturnValue());            
        });
        $A.enqueueAction(action);
	},  
  	unlinkUserhelper: function(component) {
    	console.log("in unlinkhelper");
      	var action = component.get("c.unlinkUser");
	
        //Set up the callback
        var self = this;
        action.setCallback(this, function(actionResult) {
            console.log(a.getReturnValue());
            component.set("v.displayfblink", actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
  		console.log(action.getState());
	},
	unlinkLinkedinUserCallHlp: function(component) {
    	console.log("in unlinkLinkedinUserCallHlp");
      	var action = component.get("c.unlinkLinkedinUserCallApex");
	
    	//Set up the callback
    	var self = this;
        action.setCallback(this, function(actionResult) {
            console.log(a.getReturnValue());
            component.set("v.displayfblink", actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
      console.log(action.getState());
	},
	linkUserhelper: function(component) {
        var action = component.get("c.linkUser");
    
        //Set up the callback
        var self = this;
        action.setCallback(this, function(actionResult) {
            component.set("v.fblogo", actionResult.getReturnValue());            
        });
        $A.enqueueAction(action);
	}
})