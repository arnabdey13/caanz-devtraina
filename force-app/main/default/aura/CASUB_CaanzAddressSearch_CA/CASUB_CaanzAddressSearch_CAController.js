({
	doInit: function(cmp, event, helper) {
		helper.setSearchTerm(cmp);
		helper.doSearch(cmp);
	},
	doSearch: function(cmp, event, helper){
		helper.doSearch(cmp);
	}
})