({
	doInit: function(cmp, event, helper) {
		helper.getPreferencesMetadata(cmp);
		helper.updateDescription(cmp);
	},
	updateGroupSelection: function(cmp, event, helper){
		var group = cmp.find(event.getSource().get("v.name"))
		if(group && group.updateSelection){
			group.updateSelection();
		}
	},
	onSelectionChange: function(cmp, event, helper){
		if(helper.validate(cmp, "partial")) helper.savePreferences(cmp);
	},
	performSave: function(cmp, event, helper){
		var params = event.getParam ? event.getParam("arguments").params || {} : {};
		if(helper.validate(cmp, "all")) helper.savePreferences(cmp, params.callback);
		else if(params.callback) params.callback();
	},
	performCancel: function(cmp, event, helper){

	}
})