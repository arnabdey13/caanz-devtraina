({
	LABELS: [
		"$Label.c.Areas_of_Interest_Help"
	],
	getPreferencesMetadata: function(cmp) {
		this.enqueueAction(cmp, "getPreferencesMetadata", {}, function(data){
			cmp.set("v.groups", data = JSON.parse(data));
			cmp.set("v.activeGroupName", data[0].name);
		});
	},
	savePreferences: function(cmp, callback){
		var selectionMap = this.getSelectionMap(cmp);
		var params = { preferencesJSONString: JSON.stringify(Object.values(selectionMap)) };
		this.enqueueAction(cmp, "savePreferences", params, function(data){
			(JSON.parse(data)).forEach(function(pref) { selectionMap[pref.name].id = pref.id; });
			cmp.set("v.selection", []); if(callback) callback();
			this.fireChangeEvent(cmp);
		});
	},
	getSelectionMap: function(cmp){
		var selection = cmp.get("v.selection") || [];
		return selection.reduce(function(map, pref) { map[pref.name] = pref; return map; }, {});
	},
	validate: function(cmp, saveType){
		return (cmp.get("v.saveType") === saveType && !$A.util.isEmpty(cmp.get("v.selection")));
	},
	updateDescription: function(cmp){
		var descriptionText = cmp.get("v.descriptionText") || "";
		if(descriptionText.includes("$Label.")){
			var label = descriptionText.split(' ')[0];
			cmp.set("v.descriptionText", descriptionText.replace(label, $A.get(label)));
		}
	},
	enqueueAction: function(cmp, method, params, callback){
		this.toggleSpinner(cmp, true);
		var action = cmp.get("c." + method); 
		if(params) action.setParams(params);
		action.setCallback(this, function(response){
			if(response.getState() === "SUCCESS") {
				if(callback) callback.call(this, response.getReturnValue());
	        } else if(response.getState() === "ERROR") {
	        	console.error(response.getError());
	        }
	        this.toggleSpinner(cmp, false);
		});
		$A.enqueueAction(action);
	},
	toggleSpinner: function(cmp, show) {
		var spinner = cmp.find("spinner");
		$A.util.addClass(spinner, show ? "slds-show" : "slds-hide");
		$A.util.removeClass(spinner, !show ? "slds-show" : "slds-hide");
	},
	fireChangeEvent: function(cmp) {
        var event = $A.get("e.c:PreferenceChangeEvent");
        if(event) event.fire();
    }
})