({
    doInit: function(cmp, event, helper) {
        //helper.setActiveProgressItem(cmp, 0,helper);
        helper.loadUserSubscription(cmp,helper);
    },
    activateProgressItem: function(cmp, event, helper){
        var activeItemIndex = event.currentTarget.getAttribute("data-progressItemIndex");
        helper.triggerSave(cmp, function(){
            helper.setActiveProgressItem(cmp, Number.parseInt(activeItemIndex),helper);
        });
    },
    activateNextProgressItem: function(cmp, event, helper){
        var activeItemIndex = helper.getActiveProgressItemIndex(cmp);
        var navigation = 'next';
        helper.triggerSave(cmp,navigation, function(){
            helper.updateUserSubscription(cmp,activeItemIndex, helper);
            //helper.setActiveProgressItem(cmp, activeItemIndex + 1,helper);
        });
    },
    activatePreviousProgressItem: function(cmp, event, helper){
        var activeItemIndex = helper.getActiveProgressItemIndex(cmp);
        var navigation = 'previous';
        helper.triggerSave(cmp, navigation ,function(){
            helper.setActiveProgressItem(cmp, activeItemIndex - 1,helper);
        });
    },
    performSave: function(component, event, helper){
        helper.triggerSave(component, function(){
            component.set("v.isDetailPageTobeShown",false);
            component.set("v.customHeaderTitle2","Step 2 of 3");
            component.set("v.progressValue","30");
        });
    },
    confirmCondition : function(component, event, helper) {
        var checked = component.get("v.isConfirmChecked");
    },
    next: function(component, event, helper) {
        var concessionComp = component.find('concessionComp');
        concessionComp.validateConcession();
    },
    
    activatePreviousProgressItem1 : function(component, event, helper){
        component.set("v.isDetailPageTobeShown",true);
    },
    changeToContactDetails : function(cmp, event,helper) { 
        var eventVal = event.getParam("message");
        if(eventVal == 'ChangeToContact')
            helper.setActiveProgressItem(cmp, 0,helper);
        else if(eventVal == 'DisablePrevious')
            cmp.set("v.previousButtonVisible", false);
    },
})