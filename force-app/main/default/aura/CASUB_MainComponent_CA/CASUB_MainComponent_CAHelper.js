({
    SUB_PROGRESS_ITEMS: [{
        id: "CASUB_MyDetail_Review_CA",
        screenTitle: "Step 1 of 3"
    }, {
        id: "CASUB_Mandatory_Notification_CA",
        screenTitle: "Step 2 of 3"
    },{
        id: "CASUB_Payments_CA",
        screenTitle: "Step 3 of 3"
    }],
    validateMyDetailConsession: function(component, callback) {
        var concessionComp = component.find('concessionComp');
        concessionComp.validateConcession({ callback: $A.getCallback(callback) });
    },
    validateMyDetailsAddress : function(component, callback){
        var addressComp = component.find('addressComp');
        addressComp.validateOtherCountry({ callback: $A.getCallback(callback) });
    },
    triggerSave : function(cmp, navigation, callback){
        var instance = cmp.get("v.body")[0];
        if(instance && instance.save) {
            instance.save({ navigation:navigation, callback: $A.getCallback(callback) });
        } else {
            callback();
        }
        
    },
    setActiveProgressItem: function(cmp, activeItemIndex,helper){
        var progressItems = this.getProgressItems(cmp);
        if(activeItemIndex >= progressItems.length) {
            this.callPayment(cmp); 
        }else{
            var nextButtonLabel = activeItemIndex == 0 ? "Next" : activeItemIndex == progressItems.length - 1 ? "Approve" : "Next";	
            if(nextButtonLabel=='Approve'){
                cmp.set("v.nextButtonVisible","false");
            }else{
                cmp.set("v.nextButtonVisible","true");
            }
            var activeItem = progressItems.reduce(this.getProgressItemsIterator.bind(this, activeItemIndex), null);
            cmp.set("v.progressItems", progressItems);
            cmp.set("v.activeProgressItemIndex", activeItemIndex);
            cmp.set("v.nextButtonLabel", nextButtonLabel);
            var params = activeItem.params || {};
            cmp.set("v.customHeaderTitle2", activeItem.screenTitle);
            this.setActiveProgressItemContent(cmp, activeItem.id, params);
        }
    },
    getProgressItems: function(cmp){
        var progressItems = cmp.get("v.progressItems");
        return !$A.util.isEmpty(progressItems) ? progressItems : this.SUB_PROGRESS_ITEMS;
    },
    getProgressItemsIterator: function(activeItemIndex, activeItem, item, index){
        item.isActive = !activeItem; return (!activeItem && index === activeItemIndex) ? item : activeItem;
    },
    setActiveProgressItemContent: function(cmp, contentType, params) {
        //console.log('contentType--'+contentType);
        //console.log('params--',params);
        $A.createComponent("c:" + contentType, params || {}, function(newCmp, status, errorMessage){ 
            if (status === "SUCCESS") {
                cmp.set('v.body', [newCmp]);
                
            } else if (status === "INCOMPLETE") {
                console.log("No response from server or client is offline.")
            } else if (status === "ERROR") {
                console.error("Error: ", errorMessage);
            } 
        });
    },
    getActiveProgressItemIndex: function(cmp){
        return cmp.get("v.activeProgressItemIndex") || 0;
    },
    callPayment : function(cmp){
        alert('Your Subscription recorded successfully.Payment Started...')
    },
    loadUserSubscription: function(component,event,helper){
        var params;
        this.enqueueActionMethod(component, "loadSubscriptionInformation", params, function(response){ 
            //console.log('==response==' + JSON.stringify(response));
            component.set("v.userSubscriptionSobject",response);
            if(!$A.util.isUndefinedOrNull(response)){
                if(response.Contact_Details_Status__c == "Pending"){
                    this.setActiveProgressItem(component, 0,helper); 
                } 
                else if(response.Obligation_Status__c == "Pending"){
                    component.set("v.isConfirmChecked",true);
                    this.setActiveProgressItem(component, 1,helper);                    
                }
                    else if(response.Sales_Order_Status__c == "Pending"){
                        component.set("v.isConfirmChecked",true);
                        this.setActiveProgressItem(component, 2,helper);  
                    }
                        else if(response.Sales_Order_Status__c == "Invoice Acknowledgement"){
                            component.set("v.previousButtonVisible",false);                            
                            this.setActiveProgressItem(component, 2,helper); 
                            
                        }
                            else if(response.Sales_Order_Status__c == "Completed"){
                                component.set("v.showSubscription",false);                        
                            }
            }
            else
                this.setActiveProgressItem(component, 0,helper); 
            
        });
    },    
    enqueueActionMethod: function(component, method, params, callback){
        var action = component.get("c." + method); 
        if(params) action.setParams(params);
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                if(callback) callback.call(this, response.getReturnValue());
            } else if(state === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                    }
                }
            }else if (status === "INCOMPLETE") {
                console.log('No response from server or client is offline.');
            }
        });       
        $A.enqueueAction(action);
    },
    updateUserSubscription: function(component,activeProgressItemIndex, helper){    
        var userSubscription = component.get("v.userSubscriptionSobject");         
        var params={"activeProgressItemIndex" : activeProgressItemIndex,"userSubscriptionToUpdate":userSubscription};
        this.enqueueActionMethod(component, "updateSubscriptionInformation", params, function(response){            
            if(!$A.util.isUndefinedOrNull(response)){
                this.setActiveProgressItem(component, activeProgressItemIndex + 1,helper);
                component.set("v.userSubscriptionSobject",response);
            }
        });        
    },     
    showToast : function(component,text) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Warning!",
            message: text,
            duration:' 5000',
            type: 'warning',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },

})