({
    setStageAndBoxLabels: function (component, event, helper) {
        var currentStage = component.get("v.currentStage");
        var allStages = component.get("v.allStages");

        this.fireApplicationEvent(component,
            "e.c:ff_EventStageChange",
            {
                stage: currentStage,
                stages: allStages,
                headings: this.getBoxLabels(component)
            }
        );
    },
    getBoxLabels: function (component) {
        var allStages = component.get("v.allStages");
        var topBoxValues = this.parseCSV(component.get("v.topBoxLabels"));
        var box10values = this.parseCSV(component.get("v.box10Labels"));
        var box20values = this.parseCSV(component.get("v.box20Labels"));
        var box30values = this.parseCSV(component.get("v.box30Labels"));
        var box40values = this.parseCSV(component.get("v.box40Labels"));
        var bottomBoxValues = this.parseCSV(component.get("v.bottomBoxLabels"));

        if (allStages.length != topBoxValues.length ||
            allStages.length != box10values.length ||
            allStages.length != box20values.length ||
            allStages.length != box30values.length ||
            allStages.length != box40values.length ||
            allStages.length != bottomBoxValues.length) {
            console.log('The progress bar labels have not the same size as the box labels');
            throw e;
        }

        var headings = [];

        for (var i = 0; i < allStages.length; i++) {
            headings[i] = {
                topBoxLabel: topBoxValues[i],
                box10Label: box10values[i],
                box20Label: box20values[i],
                box30Label: box30values[i],
                box40Label: box40values[i],
                bottomBoxLabel: bottomBoxValues[i]
            }
        }
        return headings;
    },
    parseCSV: function (csvList) {
        if (csvList) {
            return csvList.split(',');
        } else {
            return null;
        }
    },
    //WDCi (Lean) 15/10/2018 PAN:5340 - start: replaced with new method below
    setButtonsVisibility_Old_Backup: function (component) {
        var currentStage = component.get("v.currentStage");
        //subtract 1 because stages start at 0 index and the below is just a count of all stages
        var totalStages = component.get("v.allStages").length - 1;

        //set the previous button behaviour
        if (currentStage == 0) {
            component.set("v.previousBtnDisabled", true);
        } else {
            component.set("v.previousBtnDisabled", false);
        }

        //set the next and save button behaviour
        if (currentStage == totalStages - 1) {
            component.set("v.nextBtnVisible", false);
            component.set("v.submitBtnVisible", true);
        } else {
            component.set("v.nextBtnVisible", true);
            component.set("v.submitBtnVisible", false);
        }
        if (currentStage == totalStages) {
            component.set("v.prevBtnVisible", false);
            component.set("v.nextBtnVisible", false);
            component.set("v.submitBtnVisible", false);
        }
    },
    getFurthestStage: function (component, step) {
        var furthestStage = component.get("v.furthestStage");
        var currentStage;
        if (step == this.getNavigationOptions().next) {
            currentStage = component.get("v.currentStage") + 1;
        } else {
            currentStage = component.get("v.currentStage") - 1;
        }
        return furthestStage >= currentStage ? furthestStage : currentStage;
    },
    getNavigationOptions: function () {
        return {
            previous: "PREVIOUS",
            next: "NEXT",
            submit: "SUBMIT",
            progressBar: "PROGRESS-BAR"
        };
    },
    prepareNavigation: function (component, navigationStep) {
        var currentStage = component.get("v.currentStage"); //WDCi (Lean) 15/10/2018 PAN:5340
        console.log('currentStage :: ' + currentStage);

        switch (navigationStep) {
            case this.getNavigationOptions().next:
                component.find("next-button").set("v.isLoading", true);
                component.find("previous-button").set("v.disabled", true);
                component.find("submit-button").set("v.disabled", true);

                component.find("agree-button").set("v.isLoading", true); //WDCi (Lean) 15/10/2018 PAN:5340
                //component.find("disagree-button").set("v.disabled", true); //WDCi (Lean) 15/10/2018 PAN:5340
                break;
            case this.getNavigationOptions().previous:
                component.find("previous-button").set("v.isLoading", true);
                component.find("next-button").set("v.disabled", true);
                component.find("submit-button").set("v.disabled", true);
                break;
            case this.getNavigationOptions().submit:
                component.find("submit-button").set("v.isLoading", true);
                component.find("previous-button").set("v.disabled", true);
                component.find("next-button").set("v.disabled", true);
                break;
            case this.getNavigationOptions().progressBar:
                component.find("next-button").set("v.disabled", true);
                component.find("previous-button").set("v.disabled", true);
                component.find("submit-button").set("v.disabled", true);
                break;
        }
        component.set("v.submissionError", false);

        component.set("v.navigationStep", navigationStep);
    },
    saveToApex: function (component) {
        // ensure that focus is on primary object
        this.restorePrimaryFocus(component);
        // now send that object to apex
        this.sendFocusedRecord(component, "NO-OP");
    },
    handleSaveResponse: function (component, helper) {
        if (component.get("v.isProvAppSave")) {
            component.find("previous-button").set("v.isLoading", false);
            component.find("next-button").set("v.isLoading", false);
            component.find("submit-button").set("v.isLoading", false);
            component.find("previous-button").set("v.disabled", false);
            component.find("next-button").set("v.disabled", false);
            component.find("submit-button").set("v.disabled", false);
            component.find("agree-button").set("v.isLoading", false); //WDCi (Lean) 15/10/2018 PAN:5340 

            var navigationStep = component.get("v.navigationStep");

            if (navigationStep == this.getNavigationOptions().next ||
                navigationStep == this.getNavigationOptions().submit) {
                helper.nextStage(component);
            } else if (navigationStep == this.getNavigationOptions().previous) {
                helper.prevStage(component);
            } else if (navigationStep == this.getNavigationOptions().progressBar) {
                helper.goToStage(component, component.get("v.progressBarNavigationStage"));
            }
            this.scrollToTop();
            this.setButtonsVisibility(component);
            component.set("v.isProvAppSave", false);
        }
    },
    fireAdobeEvent: function (component, helper, navigationStep) {
        var sourceLabel;
        if (navigationStep == this.getNavigationOptions().next) {
            sourceLabel = "ProvApp" + component.get("v.currentStage") + ".next";
        } else if (navigationStep == this.getNavigationOptions().previous) {
            sourceLabel = "ProvApp" + component.get("v.currentStage") + ".previous";
        } else if (navigationStep == this.getNavigationOptions().submit) {
            sourceLabel = "ProvApp.submit";
        } else if (navigationStep == this.getNavigationOptions().progressBar) {
            sourceLabel = "ProvApp.progressBarNavigation";
        }
        console.log('firing ' + sourceLabel);
        if (!$A.util.isEmpty(sourceLabel)) {
            helper.fireApplicationEvent(component,
                "e.c:UserClickEvent",
                {
                    sourceLabel: sourceLabel
                });
        }
    },
    //function blocks pagination is server side required fields are invalid
    allowPagination: function (component) {
        if (component.get("v.currentStage") == 1) {
            var email = component.get("v.emailControlReference");
            var dob = component.get("v.dobControlReference");
            if (!$A.util.isEmpty(email)) {
                return email.get("v.isValid");
            } else if (!$A.util.isEmpty(dob)) {
                return dob.get("v.isValid");
            } else {
                //return true if no email control reference is found, because this means that the email has not changed
                return true;
            }
        } if (component.get("v.currentStage") == 2) {

            // PAN:6357 Custom validation for Residency Status to block Australian Student Visa
            var newResidencyStatus;            
            var wr = component.get("v.writeData");
            var wrRecords = wr.records;
            for (var key in wrRecords) {
                if (wrRecords.hasOwnProperty(key)) {
                    var content = wrRecords[key];                    
                    if (content.hasOwnProperty("Account__r")) {
                        var newAccount = content["Account__r"];                        
                        if (newAccount.hasOwnProperty("Current_citizen_residency_status__c")) {
                            newResidencyStatus = newAccount["Current_citizen_residency_status__c"];
                        }
                    }               
                }
            }

            var existingResidencyStatus;
            var rd = component.get("v.readData");
            var rdRecords = rd.records;
            console.log("rdRecords: " +JSON.stringify(rdRecords));
            if (rdRecords.hasOwnProperty("Account__r")) {
                var rdAccount = rdRecords.Account__r[0];
                if (rdAccount.hasOwnProperty("sObjectList")) {
                    var sObjectList = rdAccount.sObjectList[0];
                    if (sObjectList.hasOwnProperty("Current_citizen_residency_status__c")) {
                        existingResidencyStatus = sObjectList["Current_citizen_residency_status__c"];
                    }
                }
            }
                        
            if (newResidencyStatus !== undefined) {
                if  (newResidencyStatus == "Australian Student Visa") {
                    wrRecords[Object.keys(wrRecords)[0]]["Application_Status__c"] = "Ineligible - AU Student Visa";
                    component.set("v.writeData", wr);
                    component.set("v.isProvAppSave", true);
                    this.saveToApex(component);
                    return false;
                } else {
                    wrRecords[Object.keys(wrRecords)[0]]["Application_Status__c"] = "Draft";
                    component.set("v.writeData", wr);
                    console.log("wr: " +JSON.stringify(wr));
                    return true;
                }
            } else if (existingResidencyStatus !== undefined && existingResidencyStatus == "Australian Student Visa") {                
                    wrRecords[Object.keys(wrRecords)[0]]["Application_Status__c"] = "Ineligible - AU Student Visa";
                    component.set("v.writeData", wr);
                    component.set("v.isProvAppSave", true);
                    this.saveToApex(component);
                    return false;                    
            } else {                				  
                wrRecords[Object.keys(wrRecords)[0]]["Application_Status__c"] = "Draft";
                component.set("v.writeData", wr);
                console.log("wr: " +JSON.stringify(wr));
                return true;
            }                        
            // END OF PAN:6357
        } else {
            //since no extra validation for other stages is required, always return true
            return true;
        }
    },
    getSubmissionStatus: function (component) {
        var rd = component.get("v.readData");
        var rdRecords = rd.records;
        var rdApplication = rdRecords.Application__c[0].sObjectList[0];
        var rdApplicationCollege = rdApplication.College__c;

        var wd = component.get("v.writeData");
        var wdRecords = wd.records;
        var wdApplication = wdRecords[rdApplication.Id];
        var wdApplicationCollege = wdApplication.College__c;

        var college = !$A.util.isEmpty(wdApplicationCollege) ? wdApplicationCollege : rdApplicationCollege;

        if (college == "Chartered Accountants") {
            return "Submitted for Assessment";
        } else {
            return "Applied";
        }
    },
    isProgressBarNavigationAllowed: function (component, navigationStep) {
        var currentStage = component.get("v.currentStage");
        if (navigationStep == this.getNavigationOptions().next || navigationStep == this.getNavigationOptions().submit) {
            currentStage = currentStage + 1;
        } else if (navigationStep == this.getNavigationOptions().previous) {
            currentStage = currentStage - 1;
        }

        if (currentStage == component.get("v.allStages").length - 1) {
            component.set("v.navigationAllowed", false);
        } else {
            component.set("v.navigationAllowed", true);
        }
    },
    scrollToTop: function () {
        window.scrollTo(0, 0);
    },
    fireStageChangeEvent: function (component, helper, forceStageChangeHandling) {
        this.fireApplicationEvent(component, "e.c:ff_EventStageChange", {
            stage: component.get("v.currentStage"),
            stages: component.get("v.allStages"),
            headings: helper.getBoxLabels(component),
            navigationAllowed: component.get("v.navigationAllowed"),
            forceStageChangeHandling: forceStageChangeHandling
        });
    },
    //WDCi (Lean) 15/10/2018 PAN:5340 - start: new method for button visibility as we introduced a new stage
    setButtonsVisibility: function (component) {
        var currentStage = component.get("v.currentStage");
        //subtract 1 because stages start at 0 index and the below is just a count of all stages
        var totalStages = component.get("v.allStages").length - 1;
        
        //set the agree, disagree, next and save button behaviour
        if(currentStage == 0){
            component.set("v.disagreeBtnVisible", true);
            component.set("v.agreeBtnVisible", true);
            component.set("v.prevBtnVisible", false);
            component.set("v.nextBtnVisible", false);
            component.set("v.submitBtnVisible", false);

        } else if (currentStage == totalStages - 1) {
            component.set("v.nextBtnVisible", false);
            component.set("v.submitBtnVisible", true);
            component.set("v.prevBtnVisible", true);

            component.set("v.disagreeBtnVisible", false);
            component.set("v.agreeBtnVisible", false);

        } else {
            component.set("v.nextBtnVisible", true);
            component.set("v.submitBtnVisible", false);
            component.set("v.prevBtnVisible", true);

            component.set("v.disagreeBtnVisible", false);
            component.set("v.agreeBtnVisible", false);

        }

        if (currentStage == totalStages) {
            component.set("v.prevBtnVisible", false);
            component.set("v.nextBtnVisible", false);
            component.set("v.submitBtnVisible", false);
        }
    },
    //WDCi (Lean) 15/10/2018 PAN:5340 - start: new method to handle record deletion
    deleteRecord: function(component, event, helper){

        if(component.get("v.deleteUponDisagree")){
            var readData = component.get("v.readData");

            var records = readData.records;
            var application = records.Application__c[0].sObjectList[0];
            var eduHistories = records.edu_Education_History__c[0];
            var empHistories = records.Employment_History__c[0];

            if(application.Id){

                for (var i = 0; i < eduHistories.sObjectList.length; i++) {
                    this.deleteRecordWithApex(component, eduHistories.sObjectList[i].Id);
                }

                for (var i = 0; i < empHistories.sObjectList.length; i++) {
                    this.deleteRecordWithApex(component, empHistories.sObjectList[i].Id);
                }

                var response = this.deleteRecordWithApex(component, application.Id);
            }


        }
        
        var redirectUrl = component.get("v.deleteLandingUrl");
        var eUrl= $A.get("e.force:navigateToURL");
        eUrl.setParams({
            "url": redirectUrl,
            "isredirect": true
        });
        eUrl.fire();
    }
})