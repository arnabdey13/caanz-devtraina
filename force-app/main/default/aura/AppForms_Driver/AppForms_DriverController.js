({
    doInit: function (component, event, helper) {
		var showRhsBox = component.get("v.showRhsBox");
        
        //set the layout to show all boxes if the builderMode is true
        //setTimeout($A.getCallback(function() {
            /*helper.fireApplicationEvent(component,
                                    "e.c:AppForms_EventShowRHSBox",
                                    {
                                        showRhsBox: showRhsBox
                                    });*/
        //}));
        

    },
    handleSave: function (component, event, helper) {
        var wd = component.get("v.writeData");

        //send data to apex
        if (component.get("v.formValid")) {
            helper.prepareNavigation(component, helper.getNavigationOptions().submit);
            helper.setFocusedFieldValue(component, "Application__c.Resumption_Stage_Number__c", component.get("v.currentStage") + 1);
            helper.setFocusedFieldValue(component, "Application__c.Furthest_Stage_Number__c", helper.getFurthestStage(component, helper.getNavigationOptions().next));
            helper.setFocusedFieldValue(component, "Application__c.Application_Status__c", helper.getSubmissionStatus(component));
            helper.isProgressBarNavigationAllowed(component, helper.getNavigationOptions().next);
            helper.fireAdobeEvent(component, helper, helper.getNavigationOptions().submit);
            component.set("v.isProvAppSave", true);
            helper.saveToApex(component);
        } else {
            component.set("v.submissionError", true);
            helper.fireApplicationEvent(component,
                "e.c:ff_EventSetValidation",
                {
                    disable: false,
                    show: true
                }
            );
        }
    },
    handleLifeCycle: function (component, event, helper) {
        event.stopPropagation();
        switch (event.getParam("type")) {
            case "stage-change":
                // if required, drivers can exclude stages when firing this event
                if (!$A.util.isEmpty(component.get("v.currentStage"))) {
                    helper.fireStageChangeEvent(component, helper, false);
                }
                break;
            case "pre-save":
                var wd = component.get("v.writeData");
                var isOptedOutEmployer = wd.focusId.indexOf(component.get("v.unknownEmployerIdPrefix")) == 0;
                if (isOptedOutEmployer) { // service expects id in Employer_Name_at_Commencement__c to work around mismatched SObject type
                    var focused = wd.records[wd.focusId];
                    focused.Employer_Name_at_Commencement__c = wd.focusId;
                    delete focused.Id;
                }
                break;
            case "post-save":
                helper.handleSaveResponse(component, helper);
                break;
            case "pre-load":
                var readData = component.get("v.readData");
                var records = readData.records;
                var application = records.Application__c[0].sObjectList[0];

                // set initial focus on the application record
                helper.setFocusedRecord(component, application.Id, "Application__c", 0);
                // make the application the primary focus for saves and focus auto-switch
                helper.setPrimaryFocus(component, application.Id, "Application__c", 0);
                break;
            case "post-load":
                var readData = component.get("v.readData");
                var records = readData.records;
                var application = records.Application__c[0].sObjectList[0];
                var stageNumber = $A.util.isEmpty(application.Resumption_Stage_Number__c) ? 0 : application.Resumption_Stage_Number__c;
                var furthestStage = $A.util.isEmpty(application.Furthest_Stage_Number__c) ? 0 : application.Furthest_Stage_Number__c;
                component.set("v.furthestStage", furthestStage);
                helper.setStage(component, stageNumber);
                helper.setStageAndBoxLabels(component);
                helper.setButtonsVisibility(component);
                helper.fireApplicationEvent(component,
                                    "e.c:AppForms_EventControlRegistration",
                                    {
                                        allControlsLoaded: true
                                    });

                helper.fireApplicationEvent(component,
                    "e.c:ff_EventSetFocus",
                    {
                        loadKey: "Application",
                        field: "College__c"
                    }
                );
                break;
            case "post-value-change":
                var opts = event.getParam("opts");
                if (opts.field == "PersonMailingState" || opts.field == "PersonOtherState") {
                    helper.fireStageChangeEvent(component, helper, true);
                }
                break;
        }
    },

    handleNext: function (component, event, helper) {
        var allowPagination = helper.allowPagination(component);
        if (allowPagination) {
            helper.prepareNavigation(component, helper.getNavigationOptions().next);
            helper.setFocusedFieldValue(component, "Application__c.Resumption_Stage_Number__c", component.get("v.currentStage") + 1);
            helper.setFocusedFieldValue(component, "Application__c.Furthest_Stage_Number__c", helper.getFurthestStage(component, helper.getNavigationOptions().next));
            helper.isProgressBarNavigationAllowed(component, helper.getNavigationOptions().next);
            helper.fireAdobeEvent(component, helper, helper.getNavigationOptions().next);
            component.set("v.isProvAppSave", true);
            helper.saveToApex(component);
        }
    },
    handlePrevious: function (component, event, helper) {
        var allowPagination = helper.allowPagination(component);
        if (allowPagination) {
            helper.prepareNavigation(component, helper.getNavigationOptions().previous);
            helper.setFocusedFieldValue(component, "Application__c.Resumption_Stage_Number__c", component.get("v.currentStage") - 1);
            helper.setFocusedFieldValue(component, "Application__c.Furthest_Stage_Number__c", helper.getFurthestStage(component, helper.getNavigationOptions().previous));
            helper.isProgressBarNavigationAllowed(component, helper.getNavigationOptions().next);
            helper.fireAdobeEvent(component, helper, helper.getNavigationOptions().previous);
            component.set("v.isProvAppSave", true);
            helper.saveToApex(component);
        }
    },
    handleValueChange: function (component, event, helper) {
        component.set("v.submissionError", false);

        var params = event.getParams();
        if (params.loadKey == "Account__r" && params.field == "PersonEmail") {
            component.set("v.emailControlReference", params.source);
        }
        if (params.loadKey == "Account__r" && params.field == "PersonBirthdate") {
            component.set("v.dobControlReference", params.source);
        }
    },
    handleProgressBarClick: function (component, event, helper) {
        var allowPagination = helper.allowPagination(component);
        if (allowPagination) {
            component.set("v.progressBarNavigationStage", event.getParam("stage"));
            helper.prepareNavigation(component, helper.getNavigationOptions().progressBar);
            helper.setFocusedFieldValue(component, "Application__c.Resumption_Stage_Number__c", event.getParam("stage"));
            helper.fireAdobeEvent(component, helper, helper.getNavigationOptions().progressBar);
            component.set("v.isProvAppSave", true);
            helper.saveToApex(component);
        }
    },
    handleSetStageLockEvent: function (component, event, helper) {
        var params = event.getParams();
        if (params.lock) {
            component.find("next-button").set("v.disabled", true);
            component.find("previous-button").set("v.disabled", true);
            component.find("submit-button").set("v.disabled", true);
        } else {
            component.find("next-button").set("v.disabled", false);
            component.find("previous-button").set("v.disabled", false);
            component.find("submit-button").set("v.disabled", false);
        }
    },
    //WDCi (Lean) 15/10/2018 PAN:5340 - start: new method to handle record deletion
    handleDisagree: function(component, event, helper){        
        helper.deleteRecord(component, event, helper);
    },
    
    //RXP - To Handle the Required Field missing error 
    handleReload:function(component, event, helper){
       		component.find("next-button").set("v.isLoading", false); 
        	component.find("next-button").set("v.disabled", false);
        	component.find("previous-button").set("v.isLoading", false);
        	component.find("previous-button").set("v.disabled", false);
    }
})