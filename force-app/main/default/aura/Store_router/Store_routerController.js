({

    "echo" : function(cmp) {
    
        var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
        var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
        var sParameterName;
        var i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('='); //to split the key from the value.
        }
        console.log('Param name'+sParameterName[0]);  //equals site
        console.log('Param value'+sParameterName[1]); //equals 7

        var action = cmp.get("c.updateUser");
        var actionRestore = cmp.get("c.restoreUser");
        

        var destination = '';
        if (sParameterName[0] === 'site' && (sParameterName[1]==='1' || sParameterName[1]==='3' || sParameterName[1]==='4')){
            destination = sParameterName[1];
        }
        else
        {
            destination = '0';
        }



        
            
		action.setParams({ site : destination });
       // actionRestore.setParams();
        
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                $A.enqueueAction(actionRestore);
                window.open('/customer/idp/login?app=0sp90000000TNPR', '_self');
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });


        $A.enqueueAction(action);
        
        
    }
   


})