({
	checkForUserPermissions: function(cmp) {
		var customPermissions = [];
		for(var i = 0; i < 5; i++){
			var suffix = i == 1 ? "One" : i == 2 ? "Two" : i == 3 ? "Three" : i == 4 ? "Four" : "Five";
			var permission = cmp.get("v.permission" + suffix);
			if(!$A.util.isEmpty(permission) && !customPermissions.includes(permission)) {
				customPermissions.push(permission);
			}
		}
		if(!$A.util.isEmpty(customPermissions)) {
			this.getUserPermissions(cmp, customPermissions);
		}
	},
	getUserPermissions: function(cmp, customPermissions){
		var action = cmp.get("c.getUserPermissions"); 
		action.setParams({ customPermissions: customPermissions });
		action.setCallback(this, function(response) {
			if(response.getState() === "SUCCESS") {
	            this.applyUserPermissions(cmp, JSON.parse(response.getReturnValue()));
	        } else if(response.getState() === "ERROR") {
	        	this.showError(response.getError());
	        }
		});
		$A.enqueueAction(action);
	},
	applyUserPermissions: function(cmp, userPermissions){
		for(var i = 0; i < 5; i++){
			var suffix = i == 1 ? "One" : i == 2 ? "Two" : i == 3 ? "Three" : i == 4 ? "Four" : "Five";
			var permission = cmp.get("v.permission" + suffix);
			if(!$A.util.isEmpty(permission) && !userPermissions.includes(permission)) {
				cmp.set("v.heading" + suffix, null);
			}
		}
	},
	showError: function(errors){
        var message = 'Unknown error'; 
        if ($A.util.isArray(errors) && !$A.util.isEmpty(errors)) {
            message = errors[0].message;
        }
        alert(message);
	}
})