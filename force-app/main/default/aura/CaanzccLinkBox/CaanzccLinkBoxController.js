({
    doInit: function(component, event, helper) {
        var compColour = component.get("v.componentColour");
        var userColourChoice = component.get("v.backgroundColour");
        if (userColourChoice == false) {
          component.set("v.componentColour", "transparent");
        }

        var compMargin = component.get("v.marginTop");
        var userMarginChoice = component.get("v.moveUp");
        if (userMarginChoice == true) {
          component.set("v.marginTop", "-26px");
        }

        var iconResourceOne = component.get("v.iconOne");
        var iconResourcePathOne = "$Resource." + iconResourceOne;
        var iconResourcePathFullOne = $A.get(iconResourcePathOne);
        component.set("v.iconPathOne", iconResourcePathFullOne);

        var iconResourceTwo = component.get("v.iconTwo");
        var iconResourcePathTwo = "$Resource." + iconResourceTwo;
        var iconResourcePathFullTwo = $A.get(iconResourcePathTwo);
        component.set("v.iconPathTwo", iconResourcePathFullTwo);

        var iconResourceThree = component.get("v.iconThree");
        var iconResourcePathThree = "$Resource." + iconResourceThree;
        var iconResourcePathFullThree = $A.get(iconResourcePathThree);
        component.set("v.iconPathThree", iconResourcePathFullThree);

        var iconResourceFour = component.get("v.iconFour");
        var iconResourcePathFour = "$Resource." + iconResourceFour;
        var iconResourcePathFullFour = $A.get(iconResourcePathFour);
        component.set("v.iconPathFour", iconResourcePathFullFour);

        var iconResourceFive = component.get("v.iconFive");
        var iconResourcePathFive = "$Resource." + iconResourceFive;
        var iconResourcePathFullFive = $A.get(iconResourcePathFive);
        component.set("v.iconPathFive", iconResourcePathFullFive);

        //check if user has correct permissions
        helper.checkForUserPermissions(component);
    },

    goToDestination: function(component, event, helper) {
        var compLinkDest = event.currentTarget.id;

        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": compLinkDest
        });
        urlEvent.fire();
    }
})