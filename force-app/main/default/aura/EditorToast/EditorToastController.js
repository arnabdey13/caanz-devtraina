({
    handleSetLabels : function(component, event, helper) {
        if (event.getParam("field") == component.get("v.field")) {
            var label = event.getParam("label1");
            var labelBelow = event.getParam("label2");
            if (label) {
                component.set("v.label", label);
            }
            if (labelBelow) {
                component.set("v.label2", labelBelow);
            }
        }
    },
})