({
    fireFindAggregateQASControlEvent: function (component, aggregateKey) {
        this.fireApplicationEvent(component,
            "e.c:ff_EventFindAggregateControls",
            {
                aggregateKey: aggregateKey,
                source: component,
                callback: this.getFindAggregateControlsCallback(component)
            });
    },
    getFindAggregateControlsCallback: function (component) {
        return function (aggregateComponent) {
            var source = component.get("v.sourceQasKey");
            var target = component.get("v.targetQasKey");
            var componentName = aggregateComponent.getName();

            if (aggregateComponent.get("v.aggregateKey") == source && componentName == "cQASControl") {
                component.set("v.sourceQasControlReference", aggregateComponent);
            }
            if (aggregateComponent.get("v.aggregateKey") == target && componentName == "cQASControl") {
                component.set("v.targetQasControlReference", aggregateComponent);
            }
        };
    },
    fireApplicationEvent: function (component, eventType, params) {
        var appEvent = $A.get(eventType);
        if (appEvent) {
            if (params) {
                appEvent.setParams(params);
            }
            appEvent.fire();
        } else {
            $A.log("no event registered: " + eventType);
        }
    },
    apiCopyAddress: function (component, value) {
        var source = component.get("v.sourceQasControlReference");
        if ($A.util.isUndefined(source)) {
            throw Error("QAS Copy source key not loaded correctly");
        }
        var target = component.get("v.targetQasControlReference");
        if ($A.util.isUndefined(target)) {
            throw Error("QAS Copy target key not loaded correctly");
        }
        if (source) { /** RXP - 20/09/2019 - Add safety check here */
            source.apiCopyAddress(target, value);
        }
    }
})