({
    handleLoad: function (component, event, helper) {

        helper.fireFindAggregateQASControlEvent(component, component.get("v.sourceQasKey"));
        helper.fireFindAggregateQASControlEvent(component, component.get("v.targetQasKey"));

        window.setTimeout(
            $A.getCallback(function () {
                if(component.find("checkboxControl").get("v.value")){
                    component.get("v.targetQasControlReference").set("v.suppressValueChangeEventHandling", true);
                }
            }), 100
        );
    },
    handleValueChangeEvent: function (component, event, helper) {
        var params = event.getParams();

        if (component.get("v.loadKey") == params.loadKey && component.get("v.field") == params.field) {
            helper.apiCopyAddress(component, params.value);
        } else if (component.find("checkboxControl").get("v.value") &&
            component.get("v.sourceQasKey") == params.source.get("v.aggregateKey")) {
            helper.apiCopyAddress(component, component.find("checkboxControl").get("v.value"));
        }
    }
})