({
	setSearchTerm: function(cmp){
		var address = cmp.get("v.address");
		if(address) cmp.set("v.searchTerm", address.street+" "+address.city+" "+address.state+" "+address.postalCode);
	},
	doSearch: function(cmp){
		var params = {
			searchTerm: cmp.get("v.searchTerm"),
			countryCode: cmp.get("v.countryCode"),
			searchLimit: cmp.get("v.searchLimit")
		};
		this.enqueueAction(cmp, "searchAddress", params, function(data){
			var addressOptions = (JSON.parse(data).SUCCESS || []).map(function(result) {
				return { label: result.label1 + " " + result.label2, value: result.id };
			});
			cmp.set("v.addressOptions", addressOptions);
		});
	},
	enqueueAction: function(cmp, method, params, callback){
		this.toggleSpinner(cmp, true);
		var action = cmp.get("c." + method); 
		if(params) action.setParams(params);
		action.setCallback(this, function(response){
			if(response.getState() === "SUCCESS") {
				if(callback) callback.call(this, response.getReturnValue());
	        } else if(response.getState() === "ERROR") {
	        	console.error(response.getError());
	        }
	        this.toggleSpinner(cmp, false);
		});
		$A.enqueueAction(action);
	},
	toggleSpinner: function(cmp, show) {
		var spinner = cmp.find("spinner");
		$A.util.addClass(spinner, show ? "slds-show" : "slds-hide");
		$A.util.removeClass(spinner, !show ? "slds-show" : "slds-hide");
	}
})