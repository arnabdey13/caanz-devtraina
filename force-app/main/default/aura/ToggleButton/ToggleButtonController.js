({
    doInit : function(component, event, helper) {
        component.set("v.dependencyKeysToHandle", helper.parseCSV(component.get("v.dependencyKeysToHandleList")));
        component.set("v.toggleFireKeysToHandle", helper.parseCSV(component.get("v.toggleFireKeysToHandleList")));
    },
    handleDependency : function(component, event, helper) {        
        var depKeysToHandle = component.get("v.dependencyKeysToHandle");
        var depKey = event.getParam("dependencyKey");
        var isDependent = depKeysToHandle[depKey];
        if (isDependent) {
            component.set("v.hidden", !event.getParam("isMatch"));
        }
    },
    handleToggleFire : function(component, event, helper) {     
        var keysToHandle = component.get("v.toggleFireKeysToHandle");
        var key = event.getParam("key");
        var isForThisComponent = keysToHandle[key];
        if (isForThisComponent) {
            helper.toggle(component, event, helper);
        }
    },
    toggle : function(component, event, helper) {
        helper.toggle(component, event, helper);
    }
})