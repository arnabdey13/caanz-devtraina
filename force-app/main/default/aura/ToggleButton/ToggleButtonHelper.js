({
    parseCSV : function(csv) {
        var result = {};
        if (csv) {
            var strings = csv.split(",");
            for (i = 0; i < strings.length; i++) {
                result[strings[i]] = true;
            }
        }
        return result;
    },
    toggle : function(component, event, helper) {
        component.set("v.toggleState", !component.get("v.toggleState"));
        var appEvent = $A.get("e.c:ToggleEvent");
        appEvent.setParams({key: component.get("v.toggleKey")}).fire();	
		
        if (!component.get("v.toggleState") && component.get("v.hideOnExpand")) {
            $A.util.addClass(component, "slds-hide");
            // suppress further handling of dep viz events
            component.set("v.dependencyKeysToHandle",{});
            // suppress further handling of toggle fire events
            component.set("v.toggleFireKeysToHandle",{});            
        }       
        
    }
})