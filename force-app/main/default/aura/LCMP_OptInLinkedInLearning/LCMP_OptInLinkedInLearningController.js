({
    /** *loadInitialData Method used to get logged in user's profile name and Custom Metadata **/ 
    loadInitialData : function(component, event, helper) {
        var action = component.get('c.getInitialData');
        action.setCallback(this,function(response){
            var status = response.getState();
            if(status === 'SUCCESS'){
                var wrapperData = response.getReturnValue();
                component.set('v.collectionDocURL',wrapperData.optInCusMet.URLPrefix__c+wrapperData.fileId);
                component.set('v.customMetadata',wrapperData.optInCusMet);
                component.set('v.accountData',JSON.parse(wrapperData.accountData));
                if(component.get('v.accountData.account.Opt_in_for_LinkedIn_Learning__c')){
                    component.set('v.linkedInLabel','Access LinkedIn Learning');
                }
                else{
                    component.set('v.linkedInLabel','Activate LinkedIn Learning');
                }
            }
        });
        $A.enqueueAction(action);
        
    },
    
    /* Edit the email while opting for LinkedIn Learning */
    editEmail : function(component,event,helper){
        /* In Edit Email Field - Copy the Current Email value so that user can edit OR completely change the Email */
        component.set('v.updatedEmail',component.get('v.contactEmail'));
        component.set('v.isDisabled',true);
        component.set('v.showEditableEmailField',true);
        component.set('v.isCheckBoxDisabled',true);
        component.set('v.showEmailError',false);
    },
    
    /* Verify if user has already Opted in for LinkedIn Learning or not, if not then show modal 
      popup for Opt-in for LinkedIn Learning */
    verifyOptedIn : function(component,event,helper){
        if(component.get('v.accountData.account.Opt_in_for_LinkedIn_Learning__c')){
            window.open(component.get('v.customMetadata.IDPLink__c'),'_blank');
        }
        else{
                component.set('v.contactEmail',component.get('v.accountData.contact.Email'));
                component.set('v.showModal',true);
                component.set('v.isDisabled',true);    
        }
    },
    
    /* Cancel/Close the modal if user dont want to Opt-in for LinkedIn Leanring */
    closeModal : function(component,event,helper){
        component.set('v.showEditableEmailField',false);
        component.set('v.showModal',false);
        component.set('v.isCheckBoxDisabled',false);
        $A.get('e.force:refreshView').fire();
    },
    
    /* Enabled Save button only when user has chekced Terms and Conditions */
    toggleCheckBox : function(component,event,helper){
        if(event.currentTarget.checked){
            component.set('v.isDisabled',false);
        }
        else{
            component.set('v.isDisabled',true);
        }
    },
    
    /* Save the Updated email on Opt-in for LinkedIn Learning Page */
    saveEmail : function(component,event,helper){
        helper.validateEmail(component,event);
    },
    
    /* Update Email and Opt-in for LinkedIn Learning Checkbox after Opting for LiL */
    updateOptIn : function(component,event,helper){
        var accountData = component.get('v.accountData');
        var emailToUpdate = component.get('v.contactEmail');
        if($A.util.isUndefinedOrNull(emailToUpdate)){
            component.set('v.showEmailError',true); 
        }
        else{
            component.set('v.showSpinner',true);
            component.set('v.showEmailError',false);
            var action = component.get('c.updateAccount');
            action.setParams({
                'recordId' : accountData.account.Id,
                'emailToUpdate' : emailToUpdate 
            });
            action.setCallback(this,function(response){
                component.set('v.showSpinner',false);
                if(response.getState() === 'SUCCESS'){
                    helper.callLiL(component,event);
                    component.set('v.linkedInLabel','Access LinkedIn Learning');
                    component.set('v.showModal',false);
                    $A.get('e.force:refreshView').fire();
                    var appEvent = $A.get("e.c:LCMP_RefreshView");
                    appEvent.fire();  
                    window.setTimeout(function(){ 
                        $A.get('e.force:refreshView').fire();                
                    }, 1000)
                }
                else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type" : "error",
                        "message": response.getError()[0].message
                    });
                    toastEvent.fire();
                }
            });
            $A.enqueueAction(action);
        }
    },
    
    
    refreshView : function(component,event,helper){
        $A.get('e.force:refreshView').fire();
    } 
})