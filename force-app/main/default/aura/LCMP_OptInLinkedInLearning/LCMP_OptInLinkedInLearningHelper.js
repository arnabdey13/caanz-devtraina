({
    validateEmail : function(component,event) {
        var emailField = component.find("emailUpdate");
        var emailFieldValue = emailField.get("v.value");
        // Store Regular Expression That 99.99% Works. [ http://emailregex.com/] 
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;  
        
        if(!$A.util.isEmpty(emailFieldValue)){   
            if(emailFieldValue.match(regExpEmailformat)){
                $A.util.removeClass(emailField, 'slds-has-error');
                emailField.setCustomValidity('');
                emailField.reportValidity();
                this.updateEmail(component,event);
                
            }else{
                $A.util.addClass(emailField, 'slds-has-error');
                emailField.setCustomValidity('Please Enter a Valid Email Address');
                emailField.reportValidity();
            }
        }
        else{
            $A.util.addClass(emailField, 'slds-has-error');
            emailField.setCustomValidity('Please Enter a Valid Email Address and Save');
            emailField.reportValidity();
        }
        
    },
    
    updateEmail : function(component,event){
        var cmp = component.find('emailUpdate');
        if(!cmp.get('v.validity').typeMismatch){
            component.set('v.contactEmail',component.get('v.updatedEmail'));
            component.set('v.showEditableEmailField',false);
            component.set('v.isCheckBoxDisabled',false);
            if(document.getElementById(component.get('v.checkboxLabel')).checked){
                component.set('v.isDisabled',false);
            }
        }
    },
    
    callLiL : function(component,event){
        var device = $A.get("$Browser.formFactor");
        if(device == 'DESKTOP'){
            window.open(component.get('v.customMetadata.IDPLink__c'),'_blank');  
        }
        else{
            window.location.assign(component.get('v.customMetadata.IDPLink__c'));
        }
     },
   
})