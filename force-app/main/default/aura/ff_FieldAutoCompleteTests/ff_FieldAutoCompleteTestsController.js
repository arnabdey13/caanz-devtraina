({
    doInit: function (component, event, helper) {

        var test = helper.driver(component, event, helper);

        // references to all the components being tested
        var ac10 = component.find("autoComplete10");
        var ac20 = component.find("autoComplete20");
        var ac25 = component.find("autoComplete25");
        var ac30 = component.find("autoComplete30");
        var ac40 = component.find("autoComplete40");
        var ac50 = component.find("autoComplete50");
        var ac60 = component.find("autoComplete60");
        var ac70 = component.find("autoComplete70");
        var ac80 = component.find("autoComplete80");
        var ac90 = component.find("autoComplete90");

        var wrappers = {};
        var startTests = test.start({
            focused: ac10,
            description: "loaded invalid but not showing errors"
        })

        startTests

            .then(test.wait(function (context) {
                var comps = [10, 20, 25, 30, 40, 50, 60, 70, 80];
                for (var i = 0; i < comps.length; i++) {
                    component.find("autoComplete" + comps[i]).set("v.loaded", true);
                }
            }))

            .then(test.assert("v.isValid", "loaded number is valid"))

            .then(test.focus(ac20, "user cleared the field"))
            .then(test.setAttribute("v.value", ""))
            .then(test.assertEquals([ac20.get("v.requiredErrorMessage")], "v.validationErrors",
                "the required error is showing"))

            .then(test.focus(ac25, "user entered a search term"))
            .then(test.wait(function (context) {
                ac25.set("v.searchText", "go");
                ac25.requestMatches();
            }))

            .then(test.focus(ac30, "user entered a search term, chooses a result"))
            .then(test.whenDone(function (context, done) {
                ac30.keyUp(65); // 65 = A, 67 = C
                ac30.keyUp(67);
                ac25.requestMatches(done);
            }))
            .then(test.wait(function () {
                ac30.keyUp(13); // 13 = enter i.e. pick first match
            }))

            .then(test.focus(ac50, "user entered a search term, then opts out and and back in"))
            .then(test.setAttribute("v.searchText", "fun50"))
            .then(test.whenDone(function (context, done) {
                ac50.requestMatches(done);
            }))
            .then(test.wait(function () {
                // since fake search below returns 1 match, 2 down arrow should focus on optOut
                ac50.keyUp(40); // 40 is down arrow
                ac50.keyUp(13); // 13 = enter i.e. pick last match
            }))
            .then(test.assertEquals(true, "v.optedOut", "opted out attr is set true i.e. event will have been fired"))
            // TODO can't test focus -> optIn because cannot set focus in tests. why?
            // to test focus -> optIn, manually click in #50 and observe the search icon in the right of the input
            .then(test.setAttribute("v.searchText", "funk soul brutha"))
            .then(test.assertEquals("funk soul brutha", "v.value",
                "opted out search text is passed as a value, like a text control"))
            // these two assertions ensure the driver will create writeData in the correct shape when opt out
            // value change events are fired
            .then(test.assertEquals("ParentAccount", function () {
                return component.get("v.mostRecentValueChangeParams.loadKey");
            }, "value changes in opt-out mode should use a loadKey which is the name of the relationship"))
            .then(test.assertEquals("Name", function () {
                return component.get("v.mostRecentValueChangeParams.field");
            }, "value changes in opt-out mode should use a field which is the name of the parent field to be inserted"))
            // now opt back in (simulating a user focusing on the input again) and searches should not fire
            // value change events until selections are made
            .then(test.wait(function () { // reset recorded value change data
                component.set("v.mostRecentValueChangeParams", {});
            }))
            .then(test.setAttribute("v.optedOut", false))
            .then(test.setAttribute("v.searchText", "funk me up"))
            .then(test.assertEquals({}, function () {
                return component.get("v.mostRecentValueChangeParams");
            }, "value change events are not fired for search changes when opted back in"))

            // see http://salesforce.stackexchange.com/questions/71604/lightning-bug-in-lightning-framework-when-using-aurarenderif for details
            // and commit 4f6ef18bf74535ae909aaf5237b04c4652cae8d1 for the fix.
            .then(test.focus(ac60, "opt in toggling covering a nasty aura:if bug in Lightning framework"))
            .then(test.whenDone(function (context, done) {
                ac60.set("v.searchText", "foo");
                ac60.requestMatches(done);
            }))
            // to reproduce we had to wait 2 secs after the search before toggling opt-in/out
            .then(test.sleep(2000))
            // somehow, after a matches request, toggling the opt out attribute caused rendering problems with the aura:if
            .then(test.wait(function (context) {
                ac60.set("v.optedOut", true);
                ac60.set("v.optedOut", false);
            }))
            .then(test.focus(component, "event log assertions in focus"))
            .then(test.assertEquals({optedOut: false}, "v.mostRecentEventParams",
                "opt back in was broadcast as an event"))

            .then(test.focus(ac70, "opting out should fire a component event"))
            .then(test.setAttribute("v.optedOut", true))
            .then(test.focus(component, "event log assertions in focus"))
            .then(test.assertEquals({optedOut: true}, "v.mostRecentEventParams",
                "opt back in was broadcast as an event"))

            .then(test.focus(ac80, "validating less than 2 chars"))
            .then(test.setAttributes({"v.optedOut": true, "v.searchText": ""}))
            .then(test.assertEquals(false, "v.isValid", "validation is checked for opted out autocomplete"))
            .then(test.setAttribute("v.searchText", "f"))
            .then(test.assertEquals(true, "v.isValid", "validation is checked for opted out autocomplete"))
            .then(test.setAttributes({"v.optedOut": false, "v.searchText": ""}))
            .then(test.assertEquals(false, "v.isValid", "validation is checked for opted in empty autocomplete"))
            .then(test.setAttribute("v.searchText", "f"))
            .then(test.assertEquals(false, "v.isValid",
                "validation is not checked for opted in single char autocomplete because only match selection is a value change"))

            .then(test.focus(ac90))
            .then(test.wait(helper.fireLoadEvent("Account", {
                AnotherAccount__c: "Value from Database"
            }, wrappers)))
            .then(test.assertEquals("Value from Database", "v.searchText",
                "Value from Database is expected to be displayed to user."))

            //////////// END OF TESTS ////////////

            // always include these fns to handle the end of the test
            .then(test.pass).catch(test.fail);

    },
    handleAutoCompleteSearch: function (component, event, helper) {
        setTimeout($A.getCallback(function () {
            var p = event.getParams();
            var results = [];
            var count = p.searchTerm == "fun50" ? 1 : 50;
            for (var i = 0; i < count; i++) {
                results.push({label1: "Acme" + i, id: "account" + i, position: i});
            }
            p.callback(results);
        }), 1000);
    },
    handleOptOut: function (component, event, helper) {
        var params = event.getParams();
        component.set("v.mostRecentEventParams", {
            optedOut: params.optedOut
        });
    },
    handleValueChange: function (component, event, helper) {
        component.set("v.mostRecentValueChangeParams", event.getParams());
    }
})