({
    handleValueChange: function (component, event, helper) {
        var params = event.getParams();
        var isFromAccountSearch = params.loadKey == "AccountSearch" && params.ignore;
        var isFromContactSearch = params.loadKey == component.get("v.loadKey");
        if (isFromAccountSearch) {
            component.find("contactSearch").set("v.searchContext", {accountId: params.value});
            component.set("v.contactSearchDisabled", false);
            // TODO set focus on contact search field
        } else if (isFromContactSearch) {
            component.set("v.saving", true);
            var saveCallback = $A.getCallback(function (result) {
                if (result.success) {
                    component.set("v.editMode", false);
                    // note: don't need to clear out prev auto-complete values because they are destroyed/created as v.editMode toggles
                }
                component.set("v.saving", false);
            });

            // fire save event async so that the driver can handle this value change event before sending to Apex
            setTimeout($A.getCallback(function () {
                helper.fireApplicationEvent(component, "e.c:ff_EventSaveFocused", {
                    successMode: "ADD",
                    callback: saveCallback
                });
            }), 100);
        }
    }
    // set attribute to disable contact
})