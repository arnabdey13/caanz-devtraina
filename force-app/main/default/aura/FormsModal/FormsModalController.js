({
    doInit : function(component, event, helper){
        helper.changeURL(component, event);
    },
    hideModal : function(component, event, helper){
        helper.hideModal(component, event);
    }
})