({
    hideModal: function (component, event) {
        var componentEvent = component.getEvent("FormsModalEvent");
        componentEvent.setParams({
            cancel : true
        });
        componentEvent.fire();
    }
})