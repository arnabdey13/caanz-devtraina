({
    upsertAccounting: function(component,questionaireResponseObjectToUpdate,callBackParams){
        this.validateRegistrationQuestionsAndAnswers(component,questionaireResponseObjectToUpdate,function(){
            console.log('==questionaireResponseObjectToUpdate==' + JSON.stringify(questionaireResponseObjectToUpdate));
            var subscriptionSobject = component.get("v.globalSubscriptionSobject");
            console.log('==globalSubscriptionSobject==' + JSON.stringify(subscriptionSobject));
            if($A.util.isUndefinedOrNull(subscriptionSobject.Obligation_Sub_Components__c )){
                subscriptionSobject.Obligation_Sub_Components__c = 'Accounting Services;';
            }
            else if(!subscriptionSobject.Obligation_Sub_Components__c.includes("Accounting Services")){
                if(subscriptionSobject.Obligation_Sub_Components__c.substring(subscriptionSobject.Obligation_Sub_Components__c.length - 1) == ';')
                    subscriptionSobject.Obligation_Sub_Components__c += 'Accounting Services;';
                else
                    subscriptionSobject.Obligation_Sub_Components__c += ';Accounting Services;';
            }
            console.log('==globalSubscriptionSobject==' + JSON.stringify(subscriptionSobject));
            var param = {
                'personAccountId':component.get("v.personAccountId"),
                'questionaireResponseObjectToUpdate':questionaireResponseObjectToUpdate,
                'selectedCountry':component.get("v.selectedCountry"),
                'whichSectionSave' : 'CPP',
                'subscriptionSobject': subscriptionSobject
            };
            this.enqueueAction(component, "upsertQuestionaireResponse", param, function(error, data){
                var questionResponseSubscriptionWrappper = data;
                if(error){
                    component.set("v.errorMessage",error);
                }else if(!$A.util.isUndefinedOrNull(callBackParams) && callBackParams.callback) {
                    if (!$A.util.isUndefinedOrNull(questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id)) {
                        component.set("v.questionnaireResponseSobject.Id",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id);
                        component.set("v.questionnaireResponseSobject.Residential_Country__c",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Residential_Country__c);
                        component.set("v.globalSubscriptionSobject",questionResponseSubscriptionWrappper.subscriptionSObject);
                        /*
                        component.set("v.questionnaireResponseSobject.Id",data.Id);
                        component.set("v.questionnaireResponseSobject.Residential_Country__c",data.Residential_Country__c);
                        */
                    }
                    component.set("v.errorMessage",'');
                    callBackParams.callback(); 
                }else{
                    if (!$A.util.isUndefinedOrNull(questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id)) {
                        component.set("v.questionnaireResponseSobject.Id",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id);
                        component.set("v.questionnaireResponseSobject.Residential_Country__c",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Residential_Country__c);
                        component.set("v.globalSubscriptionSobject",questionResponseSubscriptionWrappper.subscriptionSObject);
                        /*
                        component.set("v.questionnaireResponseSobject.Id",data.Id);
                        component.set("v.questionnaireResponseSobject.Residential_Country__c",data.Residential_Country__c);*/
                    }
                }
            })
        });
        
    },
    enqueueAction: function(component, method, params, callback){
        this.toggleSpinner(component, true);
        var action = component.get("c." + method); 
        if(params) action.setParams(params);
        action.setCallback(this, function(response){
            this.toggleSpinner(component, false);
            if(response.getState() === "SUCCESS") {
                if(callback) callback.call(this, null, response.getReturnValue());
            } else if(response.getState() === "ERROR") {
                var message = 'Unknown error'; 
                var errors = response.getError();
                if (!$A.util.isEmpty(errors)) {
                    message = errors[0].message;
                }
                console.error(message);
                if(callback) callback.call(this, message);
            }
        });
        $A.enqueueAction(action);
    },
    toggleSpinner: function(component, show) {
        var spinner = component.find("accountingSpinner");
        $A.util.addClass(spinner, show ? "slds-show" : "slds-hide");
        $A.util.removeClass(spinner, !show ? "slds-show" : "slds-hide");
    },
    
    validateRegistrationQuestionsAndAnswers : function(component,questionaireResponseObjectToUpdate,callback){
        var errorMessage = '';
        var isValidate=false;
        var isCPP = ( component.get('v.accountData.CPP_Picklist__c')=='Suspended' || component.get('v.accountData.CPP_Picklist__c')=='Removed' || $A.util.isEmpty(component.get('v.accountData.CPP_Picklist__c')));
        console.log('CPP-->'+isCPP);
        var isQualifiedAuditor = component.get('v.accountData.Qualified_Auditor__c');
        var isLicensedAuditor = component.get('v.accountData.Licensed_Auditor__c');
        var isInsolvencyPractitioner = component.get('v.accountData.Insolvency_Practitioner__c');
        var input7Cmp = component.find("Answer_7__c");
        var Ans7value = '';
        if(!$A.util.isUndefinedOrNull(input7Cmp)){            
            Ans7value = input7Cmp.get("v.value");
        }
        var input10Cmp = component.find("Answer_10__c");
        var Ans10value = ''
        if(!$A.util.isUndefinedOrNull(input10Cmp)){            
            Ans10value = input10Cmp.get("v.value");
        }
        debugger;
        if(isCPP && ( $A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_5__c) || questionaireResponseObjectToUpdate.Answer_5__c=='')){
            errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_5__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_5__c');
        }else if(questionaireResponseObjectToUpdate.Answer_5__c == 'Yes' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_6__c) || 
                                                                             questionaireResponseObjectToUpdate.Answer_6__c=='')){
            errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_6__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_6__c');
            
        }else if(questionaireResponseObjectToUpdate.Answer_5__c=='Yes' && (!$A.util.isUndefined(component.find('Answer_6__c')) && !component.find('Answer_6__c').checkValidity())){
            component.find('Answer_6__c').focus();
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_6__c');
        }/*else if(questionaireResponseObjectToUpdate.Answer_5__c == 'Yes' && (!$A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_6__c) || !$A.util.isEmpty(questionaireResponseObjectToUpdate.Answer_6__c)) && questionaireResponseObjectToUpdate.Answer_6__c.startsWith('-')){
            errorMessage = 'Negative values are not allowed for ' + questionaireResponseObjectToUpdate.Question_6__c;
            isValidate=true;
        }*/
            else if(questionaireResponseObjectToUpdate.Answer_5__c == 'Yes' && $A.util.isEmpty(Ans7value)){
                errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_7__c;
                isValidate=true;
                //    input7Cmp.focus();
                //this.setFocusForErrorMessage(component,'Answer_7__c');
                
            }
                else if(questionaireResponseObjectToUpdate.Answer_5__c == 'Yes' && !$A.util.isEmpty(Ans7value) && Ans7value.toString().startsWith('-')){
                    // errorMessage = 'Negative values are not allowed for ' + questionaireResponseObjectToUpdate.Question_7__c;
                    input7Cmp.set("v.errors", [{message:"Negative values are not allowed."}]);
                    isValidate=true;
                    $A.util.addClass(input7Cmp, 'error');
                    //      input7Cmp.focus();
                }else if(!(isQualifiedAuditor || isLicensedAuditor) && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_8__c) || questionaireResponseObjectToUpdate.Answer_8__c=='')){
                    errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_8__c;
                    isValidate=true;
                    this.setFocusForErrorMessage(component,'Answer_8__c');
                }else if(questionaireResponseObjectToUpdate.Answer_8__c == 'Yes' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_9__c) || 
                                                                                     questionaireResponseObjectToUpdate.Answer_9__c=='')){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_9__c;
                    isValidate=true;
                    
                }else if(questionaireResponseObjectToUpdate.Answer_8__c=='Yes' && (!$A.util.isUndefined(component.find('Answer_9__c')) && !component.find('Answer_9__c').checkValidity())){
                    component.find(input7Cmp).focus();
                    isValidate=true;
                    this.setFocusForErrorMessage(component,'Answer_9__c');
                }/*else if(questionaireResponseObjectToUpdate.Answer_8__c == 'Yes' && (!$A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_9__c) || !$A.util.isEmpty(questionaireResponseObjectToUpdate.Answer_9__c)) && questionaireResponseObjectToUpdate.Answer_9__c.startsWith('-')){
            errorMessage = 'Negative values are not allowed for ' + questionaireResponseObjectToUpdate.Question_9__c;
            isValidate=true;
        }*/
                    else if(questionaireResponseObjectToUpdate.Answer_8__c == 'Yes' && $A.util.isEmpty(Ans10value)){
                        errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_10__c;
                        isValidate=true;
                        $A.util.addClass(input10Cmp, 'error');
                        input10Cmp.focus();
                    }
                        else if(questionaireResponseObjectToUpdate.Answer_8__c == 'Yes' && !$A.util.isEmpty(Ans10value) && Ans10value.toString().startsWith('-')){
                            //errorMessage = 'Negative values are not allowed for ' + questionaireResponseObjectToUpdate.Question_10__c;
                            input10Cmp.set("v.errors", [{message:"Negative values are not allowed."}]); 
                            isValidate=true;
                            $A.util.addClass(input7Cmp, 'error');
                            input10Cmp.focus();
                        }else if(!isInsolvencyPractitioner && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_11__c) || questionaireResponseObjectToUpdate.Answer_11__c=='')){
                            debugger;
                            errorMessage = 'Please answer ' +questionaireResponseObjectToUpdate.Question_11__c;
                            isValidate=true;
                            this.setFocusForErrorMessage(component,'Answer_11__c');
                        }
        if(questionaireResponseObjectToUpdate.Answer_5__c == 'Yes' && !$A.util.isEmpty(Ans7value) && !Ans7value.toString().startsWith('-')){
            input7Cmp.set("v.errors", [{}]);
            $A.util.removeClass(input7Cmp, 'error');
        }
        if(questionaireResponseObjectToUpdate.Answer_8__c == 'Yes' && !$A.util.isEmpty(Ans10value) && !Ans10value.toString().startsWith('-')){
            input10Cmp.set("v.errors", [{}]);
            $A.util.removeClass(input10Cmp, 'error');
        } 
        
        if(!isValidate){
            if(callback) callback.call(this);
        }else{
            this.showToast(component,errorMessage);
        }
        
    },
    showToast : function(component,text,questionnaireResponseSobjectId) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Warning!",
            "message": text,
            type : 'warning',
            mode: 'dismissible ',
        });
        toastEvent.fire();
    },
    /* Method to display Text box when 'No' option is selected */
    setFocusForErrorMessage : function(component,componentId) {
        if(!$A.util.isUndefined(component.find(componentId))){
            component.find(componentId).focus();
        }
        
    }
    
    
})