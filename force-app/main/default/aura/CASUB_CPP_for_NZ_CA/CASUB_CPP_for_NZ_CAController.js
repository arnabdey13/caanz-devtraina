({	
  //To handle accountingService 
    handleaccountingService : function(cmp,event,helper){
        if(event.getParam('value') == 'Yes'){
            //cmp.set('v.questionnaireResponseSobject.Answer_6__c','');
            //cmp.set('v.questionnaireResponseSobject.Answer_7__c','');
        }
        else{
            cmp.set('v.questionnaireResponseSobject.Answer_6__c','');
            cmp.set('v.questionnaireResponseSobject.Answer_7__c','');
        }
    },
    //To handle Accredited Insolvancy practitioner 
    handleAIPoptions : function(cmp,event,helper){
        if(event.getParam('value') == 'Yes'){
            cmp.set('v.confirmAIP',true);
        }
        else{
            cmp.set('v.confirmAIP',false);
        }
    },
    //To handle Qualified Auditor or Licensed Auditor
    handleQA_LA_options : function(cmp,event,helper){
        if(event.getParam('value') == 'Yes'){
            //cmp.set('v.questionnaireResponseSobject.Answer_9__c','');
            //cmp.set('v.questionnaireResponseSobject.Answer_10__c','');
        }
        else{
            cmp.set('v.questionnaireResponseSobject.Answer_9__c','');
            cmp.set('v.questionnaireResponseSobject.Answer_10__c','');
        }
    },
    
    saveAccountingInformation : function(component,event,helper){
       
        var questionnaireResponseSobject = component.get("v.questionnaireResponseSobject");
        var questionaireResponseObjectToUpdate = component.get("v.questionaireResponseObjectToUpdate");
        // var answr7 = questionnaireResponseSobject.Question_7__c;
        questionaireResponseObjectToUpdate.Residential_Country__c = 'New Zealand';
        questionaireResponseObjectToUpdate.Question_5__c = questionnaireResponseSobject.Question_5__c;
        questionaireResponseObjectToUpdate.Answer_5__c = questionnaireResponseSobject.Answer_5__c;
        questionaireResponseObjectToUpdate.Question_6__c = questionnaireResponseSobject.Question_6__c;
        questionaireResponseObjectToUpdate.Answer_6__c = questionnaireResponseSobject.Answer_6__c;
        questionaireResponseObjectToUpdate.Question_7__c = questionnaireResponseSobject.Question_7__c;
       	
        // if(!$A.util.isUndefinedOrNull(questionaireResponseObjectToUpdate.Answer_7__c)){
            questionaireResponseObjectToUpdate.Answer_7__c = $A.util.isUndefinedOrNull(questionnaireResponseSobject.Answer_7__c) ? questionnaireResponseSobject.Answer_7__c : questionnaireResponseSobject.Answer_7__c.toString();
       // }
        
        questionaireResponseObjectToUpdate.Question_8__c = questionnaireResponseSobject.Question_8__c;
        questionaireResponseObjectToUpdate.Answer_8__c = questionnaireResponseSobject.Answer_8__c;
        questionaireResponseObjectToUpdate.Question_9__c = questionnaireResponseSobject.Question_9__c;
        questionaireResponseObjectToUpdate.Answer_9__c = questionnaireResponseSobject.Answer_9__c;
        questionaireResponseObjectToUpdate.Question_10__c = questionnaireResponseSobject.Question_10__c;
      //  if(!$A.util.isUndefinedOrNull(questionaireResponseObjectToUpdate.Answer_10__c)){
           
        questionaireResponseObjectToUpdate.Answer_10__c = $A.util.isUndefinedOrNull(questionnaireResponseSobject.Answer_10__c) ? questionnaireResponseSobject.Answer_10__c : questionnaireResponseSobject.Answer_10__c.toString();
    //}
        questionaireResponseObjectToUpdate.Question_11__c = questionnaireResponseSobject.Question_11__c;
        questionaireResponseObjectToUpdate.Answer_11__c = questionnaireResponseSobject.Answer_11__c;
        if (!$A.util.isUndefinedOrNull(questionnaireResponseSobject.Id)) {
            questionaireResponseObjectToUpdate.Id = questionnaireResponseSobject.Id;
            questionaireResponseObjectToUpdate.Residential_Country__c = questionnaireResponseSobject.Residential_Country__c;
        }
        helper.upsertAccounting(component,questionaireResponseObjectToUpdate); 
    }
})