({    
    doInit : function(component, event, helper) {
        var header2 = component.get("v.header2");
        component.set("v.header", component.get("v.header1")+ (header2? (" : "+header2):""));
    },
    handleResourceChange : function(component, event, helper) {
        component.set("v.faqIcon", component.get("v.resourcePrefix") + "/resource/iconContact");
        component.set("v.helpIcon", component.get("v.resourcePrefix") + "/resource/iconHelp");
    }
})