({
    doInit : function (component,event, helper) {
        var action = component.get('c.callCaseAssignmentRule');
        action.setParams({
            "caseId": component.get("v.recordId")
        });         
        action.setCallback(this,function(a){
            var state = a.getState();
            if(state === "SUCCESS"){
                var result = a.getReturnValue();
                component.set("v.message",result);
                var text = '';
                if(result){
                    text = 'Successfully assigned this case to the queue based on your case classification';
                    helper.showToast(component, event, text);
                    component.set("v.message",text);
                    $A.get('e.force:refreshView').fire();
                    
                }
                else{
                    text = 'Quick Assign will not trigger for a case which is in Resolved or Cancelled status. ';
                    component.set("v.message",text);
                    helper.showToastInfo(component, event, text);
                }
            } else if(state === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        var errorMessg = "System Error:" + errors[0].message;
                        component.set("v.message",errorMessg);
                    }
                }
            }else if (status === "INCOMPLETE") {
                component.set("v.message","No response from server or client is offline.");
            }
        }); 
        $A.enqueueAction(action);
    },    
})