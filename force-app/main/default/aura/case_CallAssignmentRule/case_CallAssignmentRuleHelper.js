({	
    showToast : function(component, event, text) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": text,
            "type": 'success'
        });
        toastEvent.fire();
    },
    showToastInfo : function(component, event, text) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Info!",
            "message": text,
            "type": 'info'
        });
        toastEvent.fire();
    },
})