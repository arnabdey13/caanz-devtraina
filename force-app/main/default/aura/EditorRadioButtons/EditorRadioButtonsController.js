({
    onChange : function(component, event, helper) {
        // a native dom change event will have a target attr so use that.
        // an EditorSetValueEvent will have the value as a param
        var newValue;
        if (event.getParam) {            
            newValue = event.getParam("value");
        } else if (event.target) {
            newValue = event.target.id;
        }
        
        helper.applyChange(component, newValue);
        var opts = component.get("v.options");
        if (opts) {
            for (i = 0; i < opts.length; i++) { 
                opts[i].selected = opts[i].value == newValue;
            }
            component.set("v.options",opts);        
        }
    },
    handleSetLabels : function(component, event, helper) {
        if (event.getParam("field") == component.get("v.field")) {
            var label = event.getParam("label1");
            var labelBelow = event.getParam("label2");
            if (label) {
                component.set("v.label", label);
            }
            if (labelBelow) {
                component.set("v.helpBelow", labelBelow);
            }
        }
    },
    showHint : function(component, event, helper) {
        // TODO show hint from EditorPopover
        // probably easiest to use $A.createComponent to create all the EditorPopovers in doInit
        // and keep the references in an attribute. then use the exposed showHint fn of EditorPopover
    },
    hideHint : function(component, event, helper) {
        // TODO 
    }
})