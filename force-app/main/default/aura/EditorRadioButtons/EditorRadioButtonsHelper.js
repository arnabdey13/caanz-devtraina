({
    load : function(component, event) {  
        var value = component.get("v.value");
        var opts = [];
        var valList = component.get("v.valueList");
        var vals = valList.split(",");
        var labels = component.get("v.labelList").split(",");                
        var hints = component.get("v.hintList");
        if (hints) {
            hints = hints.split(",");
        }
        for (i = 0; i < vals.length; i++) { 
            var opt = {value: vals[i], 
                       label: labels[i], 
                       selected: vals[i] == value};
            if (hints && hints[i]) {
                opt.hint = hints[i].split("comma").join(",")
                       			   .split("lthan").join("<")
				                   .split("gthan").join(">");
            }
            opts.push(opt);
        }
        component.set("v.options", opts);
    },    
    validate : function(component) {
        var isRequired = component.get("v.required");
        var hidden = component.get("v.hidden");
        var value = component.get("v.value");
        var isValueMissing = $A.util.isUndefined(value);
        var isInvalidValue = this.checkInvalidValues(component);        
        $A.log("radio validation: "+component.get("v.field"), {required: isRequired,
                                                               hidden: hidden,
                                                               value: value,
                                                               missing: isValueMissing})
        var invalid = !hidden && (
            (isRequired && isValueMissing) ||
            isInvalidValue
            );
        return !invalid;
    },
    setUIValid : function(component, isValid) {
        var editor = component.find("editor");
        if (isValid) {
            component.set("v.invalid", false);
            component.set("v.validationMessage", null);
        } else {
            component.set("v.invalid", true);
            var invalidMessage = this.getInvalidValueMessageFromOptions(component);
            component.set("v.validationMessage", $A.util.isEmpty(invalidMessage)?"Please choose an option":invalidMessage);
        }
    }
})