({
    doInit: function(component, event, helper) {
        helper.loadUserSubscription(component,helper);
    },
    gotoURL : function (component, event, helper) {
        if( $A.util.isEmpty(component.get("v.userSubscriptionSobject"))){
            helper.createSubscriptionSObject(component,event,helper);            
        }
        else{
            var userSubscriptionSobject =  component.get("v.userSubscriptionSobject");
            helper.navigate(component,event,helper);
        }
    }
})