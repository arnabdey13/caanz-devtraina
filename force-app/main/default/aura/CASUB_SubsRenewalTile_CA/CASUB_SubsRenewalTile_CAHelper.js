({	
    loadUserSubscription: function(component,helper){
        var params;
        this.enqueueActionMethod(component, "loadSubscriptionInformation", params, function(response){            
            component.set("v.userSubscriptionSobject",response);
        });
    },    
    enqueueActionMethod: function(component, method, params, callback){
        var action = component.get("c." + method); 
        if(params) action.setParams(params);
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                if(callback) callback.call(this, response.getReturnValue());
            } else if(state === "ERROR"){
                var errors = action.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(errors[0].message);
                    }
                }
            }else if (status === "INCOMPLETE") {
                console.log('No response from server or client is offline.');
            }
        });       
        $A.enqueueAction(action);
    },    
    createSubscriptionSObject: function(component,event, helper){        
        var params;
        this.enqueueActionMethod(component, "createSubscriptionInformation", params, function(response){  
            this.navigate(component, event, helper);
        });        
    },   
    navigate : function(component, event, helper) {
        var nagigateLightning = component.find('navigate');
        var URLLink = $A.get("{!$Label.c.CASUB_MySubscriptionPageName}");
        var pageReference = {
            type: "standard__namedPage",
            attributes: {
                pageName: URLLink
            }
        };
        nagigateLightning.navigate(pageReference);
    },
})