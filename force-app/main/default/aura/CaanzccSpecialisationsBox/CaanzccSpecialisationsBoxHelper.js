({
	getData : function(cmp) {

        var cpdSummary = cmp.get('v.cpdSummaryList')
	    if (cpdSummary !== null) {
	        for (var i = 0; i < cpdSummary.length; i++){
	            if (cpdSummary[i].Current_Triennium__c == true){                          
	                cmp.set('v.totalExemptHours',cpdSummary[i].Total_Exempt_Hours__c);                            
	                cmp.set('v.totalFormalExemptHours',cpdSummary[i].Total_Formal_Exempt_Hours__c);                            
	                cmp.set('v.specialisationType1',cpdSummary[i].Specialisation_1__c);                            
	                cmp.set('v.specialisationHours1',cpdSummary[i].Specialisation_Hours_1__c);                            
	                cmp.set('v.specialisationType2',cpdSummary[i].Specialisation_2__c);                            
	                cmp.set('v.specialisationHours2',cpdSummary[i].Specialisation_Hours_2__c);                            
	                cmp.set('v.specialisationType3',cpdSummary[i].Specialisation_3__c);                            
	                cmp.set('v.specialisationHours3',cpdSummary[i].Specialisation_Hours_3__c);                            
	                cmp.set('v.specialisationType4',cpdSummary[i].Specialisation_4__c);                            
	                cmp.set('v.specialisationHours4',cpdSummary[i].Specialisation_Hours_4__c);                            
	                cmp.set('v.specialisationType5',cpdSummary[i].Specialisation_5__c);                            
	                cmp.set('v.specialisationHours5',cpdSummary[i].Specialisation_Hours_5__c);                            
	                cmp.set('v.specialisationType6',cpdSummary[i].Specialisation_6__c);                            
	                cmp.set('v.specialisationHours6',cpdSummary[i].Specialisation_Hours_6__c);                            
	                cmp.set('v.specialisationType7',cpdSummary[i].Specialisation_7__c);                            
	                cmp.set('v.specialisationHours7',cpdSummary[i].Specialisation_Hours_7__c);                            
	                cmp.set('v.specialisationType8',cpdSummary[i].Specialisation_8__c);                            
	                cmp.set('v.specialisationHours8',cpdSummary[i].Specialisation_Hours_8__c);                            
	            }
	        }
	    }
	}
})