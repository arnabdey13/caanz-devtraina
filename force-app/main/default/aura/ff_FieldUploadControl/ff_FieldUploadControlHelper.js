({
    loadFromWrappers: function (component, wrappers) {
        if (wrappers) {
            var field = component.get("v.field");
            for (var i = 0; i < wrappers.length; i++) {
                var singleFileFromService = wrappers[i].attachments[0];
                if (singleFileFromService) { // might be the dummy/empty list so exclude that
                    var nameParsed = singleFileFromService.Name.split(":");
                    if (nameParsed[0] == field) {
                        var fileNames = [];
                        for (var j = 0; j < wrappers[i].attachments.length; j++) {
                            var withPrefix = wrappers[i].attachments[j].Name;
                            fileNames.push(withPrefix.substring(field.length + 1));
                        }
                        component.set("v.files", fileNames);
                    }
                }
                // always load the parentId, this ensures that empty controls still have a parentId
                component.set("v.parentId", wrappers[i].parent);
            }
        } else {
            throw Error("wrappers not present");
        }
    },
    save: function (component) {

        var maxFileSize = component.get("v.maxFileSize");

        var fileInput = component.find("file").getElement();
        var file = fileInput.files[0];

        if (file.size > maxFileSize) {
            if (maxFileSize > (1024 * 1024)) {
                component.set("v.maxFileErrorMessage", 'Max. file size is ' + Math.round(maxFileSize / (1024 * 1024)) + 'Mb!');
            } else {
                component.set("v.maxFileErrorMessage", 'Max. file size is ' + Math.round(maxFileSize / 1024) + 'kb!');
            }
            return;
        } else {
            component.set("v.maxFileErrorMessage", "");
        }

        var fr = new FileReader();

        var helper = this;

        fr.onload = $A.getCallback(function () {
            var fileContents = fr.result;
            var base64Mark = 'base64,';
            var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;

            fileContents = fileContents.substring(dataStart);

            helper.upload(component, file, fileContents);
        });

        fr.readAsDataURL(file);
    },

    upload: function (component, file, fileContents) {
        var fileName = file.name;
        var field = component.get("v.field");
        if (field) {
            fileName = field + ":" + fileName;
        }

        // set the name so that, when complete, the value can be set and validated
        component.set("v.fileInProgress", fileName);

        this.fireApplicationEvent(component, "e.c:ff_EventUploadFile",
            {
                source: component,
                parentId: component.get("v.parentId"),
                fileName: fileName,
                withoutMark: fileContents,
                contentType: file.type
            });
    }
})