({
    handleLifeCycle: function (component, event, helper) {
        switch (event.getParam("type")) {
            case "load-field":
                var readData = component.get("v.loadData").data;
                var wrapperKey = component.get("v.loadKey") + ".Attachments";
                var wrappers = readData.wrappers[wrapperKey];
                helper.loadFromWrappers(component, wrappers);
                break;
        }
    },
    parentVizChangeHandler: function (component, event, helper) {
        helper.validate(component, event, helper);
        helper.fireValueChange(component);
    },
    validateUploads: function (component, event, helper) {
        event.stopPropagation(); // since handling it's own event, nobody else cares
        var uploadSpecificErrors = [];
        var parentVisibility = component.get("v.parentVisibility");

        if (parentVisibility && component.get("v.files").length < component.get("v.fileCountRequired")) {
            uploadSpecificErrors.push(component.get("v.minRequiredMessage"));
        }
        component.set("v.specificValidationErrors", uploadSpecificErrors);
    },
    handleFileInputChange: function (component, event, helper) {
        helper.save(component);
    },
    handleProgressChange: function (component, event, helper) {
        var percent = event.getParam("value");
        if (percent == 100) {

            var files = component.get("v.files");
            var withoutPrefix = component.get("v.fileInProgress").substring(component.get("v.field").length + 1);
            var fileAlreadyDisplayed = false;
            for (var i = 0; i < files.length; i++) {
                if (files[i] == withoutPrefix) {
                    fileAlreadyDisplayed = true;
                    break;
                }
            }
            if (!fileAlreadyDisplayed) {
                files.push(withoutPrefix);
                component.set("v.files", files);
            }

            component.find("file").getElement().value = null;

            // clean up state for good house-keeping
            component.set("v.fileInProgress", undefined);

            helper.validate(component, event, helper);
            helper.fireValueChange(component);
        }
    },
    apiLoadWrappers: function (component, event, helper) {
        var wrappers = event.getParam("arguments").wrappers;
        if (!$A.util.isEmpty(wrappers)) {
            helper.loadFromWrappers(component, wrappers);
        }
    }
})