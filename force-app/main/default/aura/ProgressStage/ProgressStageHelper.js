({
	calcStyle : function(cmp) {
        var active = cmp.get("v.active");
        var styles = "slds-wizard__item";
        if (active) {
            styles += " slds-is-active";
        }
        cmp.set("v.stateClass", styles);
	}
})