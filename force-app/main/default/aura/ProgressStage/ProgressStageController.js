({
    doInit : function(cmp, event, helper) {
        helper.calcStyle(cmp);
    },
    doUpdate : function(cmp, event, helper) {
        helper.calcStyle(cmp);
    }
})