({
    doInit : function(component, event, helper) {
        window.scroll(0, 0);
        helper.loadConfigurationData(component);
        //helper.loadConfigurationDataOnCountryChange(component);
        if(!$A.util.isUndefinedOrNull(component.find('privacyConsent'))){
            //component.find('privacyConsent').set('v.mandNot',component.get('v.completeComponent'));    
        }
    },
    handleCountryChange: function(component, event, helper){ 
        console.log("old value: " + event.getParam("oldValue"));
        console.log("current value: " + event.getParam("value"));
        
       if(!$A.util.isEmpty(event.getParam("value"))) {
            component.set("v.selectedCountry",event.getParam("value"));
            helper.loadConfigurationData(component);
        }else{
            component.set("v.selectedCountry",event.getParam("oldValue"));
        }
    },
    performSave: function(component, event, helper){
        var params = event.getParam ? event.getParam("arguments").params || {} : {};
        if(params.navigation=='previous' && params.callback){	
            params.callback();
        }else{
            helper.validatePrivacyConsent(component, function(){
                if(params.callback) {
                    params.callback();
                }
            });
        }
    },
    
})