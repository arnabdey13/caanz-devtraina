({
    loadConfigurationData: function(component){
        
        var params={"selectedCountry":component.get("v.selectedCountry")};
        this.enqueueAction(component, "getConfigurationMetaDataForMandatoryObligation", params, function(data){
            /* Code Corrected By Sumit - data wrapper should be mapped to component attribute as 
            data.PersonAccount.account instead of data.account which gives unidentified*/
            
            component.set("v.account",data.PersonAccount.account);
            component.set("v.questionnaireResponseSobject",data.questionnaireResponse);
            component.set("v.casubMandatoryNotificationConfig",data.casubMandatoryNotificationConfig);
            component.set("v.memberAge",data.PersonAccount.subscription.Member_Age__c);
            /* Added my Mayukh the added subscription */
            component.set("v.globalSubscriptionSobject",data.PersonAccount.subscription);
            /* Code Corrected By Sumit - data wrapper should be mapped to component attribute as 
            data.PersonAccount.contact instead of data.contact which gives unidentified*/
            
            if(!$A.util.isEmpty(component.get("v.selectedCountry"))){
                component.set('v.showPrivacyConsent',true); 
            }
            
            /* make all component as false */
            component.set('v.isCPDForNZ',false);
            component.set('v.isRegistrationForNZ',false);
            component.set('v.isCPPForNZ', false);
            
            component.set('v.isCPPForAUToDisplay', false);
            component.set('v.toDisplayAUAndOverseasRegistration',false);
            component.set('v.isCertPublicPractise',false);
            component.set('v.isCPPQuestionAllowed', false);
            
            component.set("v.memberProfile",data.PersonAccount.profileName);
            
            /* Check if CPD Component for NZ has to be displayed */
            if(component.get("v.selectedCountry") == 'New Zealand'){
                
                if(data.PersonAccount.account.NMP__c){
                    // if(data.PersonAccount.account.Membership_Class__c == 'Full'){
                    component.set('v.isCPDForNZ',true);  
                    //}
                    if((data.PersonAccount.account.CPP_Picklist__c=='Suspended' || data.PersonAccount.account.CPP_Picklist__c=='Removed' || $A.util.isEmpty(data.PersonAccount.account.CPP_Picklist__c)) 
                       || !(data.PersonAccount.account.Qualified_Auditor__c == true && 
                            data.PersonAccount.account.Licensed_Auditor__c == true) || 
                       !(data.PersonAccount.account.Insolvency_Practitioner__c == true)){
                        component.set('v.isCPPForNZ', true);
                    }      
                    if(data.PersonAccount.account.Qualified_Auditor__c == true || data.PersonAccount.account.Licensed_Auditor__c == true || data.PersonAccount.account.Insolvency_Practitioner__c == true){
                        component.set('v.isRegistrationForNZ',true);
                    }
                }
                
                else if(data.PersonAccount.profileName == $A.get("$Label.c.CAANZ_Profile_Member")|| data.PersonAccount.profileName == $A.get("$Label.c.CAANZ_Profile_Reviewer") ){
                    console.log('profileName In'+data.PersonAccount.account.NMP__c);
                    if(data.PersonAccount.account.Membership_Class__c == 'Full'){
                        console.log('Membership_Class__c In');
                        component.set('v.isCPDForNZ',true); 
                    }
                    if(data.PersonAccount.account.Qualified_Auditor__c == true || data.PersonAccount.account.Licensed_Auditor__c == true || data.PersonAccount.account.Insolvency_Practitioner__c == true){
                        component.set('v.isRegistrationForNZ',true);
                    }
                    if((data.PersonAccount.account.CPP_Picklist__c=='Suspended' || data.PersonAccount.account.CPP_Picklist__c=='Removed' || $A.util.isEmpty(data.PersonAccount.account.CPP_Picklist__c)) 
                       || !(data.PersonAccount.account.Qualified_Auditor__c == true && 
                            data.PersonAccount.account.Licensed_Auditor__c == true) || 
                       !(data.PersonAccount.account.Insolvency_Practitioner__c == true)){
                        component.set('v.isCPPForNZ', true);
                    } 
                    
                }
                    else if(data.PersonAccount.profileName == $A.get("$Label.c.CAANZ_Profile_Provisional_Member")){
                        component.set('v.isCPDForNZ',false);
                        component.set('v.isRegistrationForNZ',false);
                        component.set('v.isCPPForNZ', false); 
                    }
                /* else if(data.PersonAccount.profileName == $A.get("$Label.c.CAANZ_Profile_Non_Member")){
                        console.log('profileName IN');
                        if(data.PersonAccount.account.Membership_Class__c == 'Full'){
                             console.log('Membership_Class__c IN');
                            component.set('v.isCPDForNZ',true);  
                       //     component.set('v.isCPPForNZ', true);
                        }
                         if((data.PersonAccount.account.CPP_Picklist__c=='Suspended' || data.PersonAccount.account.CPP_Picklist__c=='Removed' || $A.util.isEmpty(data.PersonAccount.account.CPP_Picklist__c)) 
                           || !(data.PersonAccount.account.Qualified_Auditor__c == true && 
                                                                           data.PersonAccount.account.Licensed_Auditor__c == true) || 
                                                                           !(data.PersonAccount.account.Insolvency_Practitioner__c == true){
                            component.set('v.isCPPForNZ', true);
                        }      
                        component.set('v.isRegistrationForNZ',false);
                    } */
            }else if(component.get("v.selectedCountry") == 'Australia' || component.get("v.selectedCountry") == 'Overseas'){
                var membershipAppDate = data.PersonAccount.account.Membership_Approval_Date__c;
                var cppPickList = data.PersonAccount.account.CPP_Picklist__c;
                var cpp = false;
                var configuredDataSetUp = data.casubMandatoryNotificationConfig;
                var configureFullMembershipConditionalDate = configuredDataSetUp.Membership_Approval_Date__c;  
                var isMembershipApprovalDateIsMoreThenConfiguredOne = false;
                
                if(!$A.util.isUndefined(membershipAppDate) && membershipAppDate > configureFullMembershipConditionalDate){
                    isMembershipApprovalDateIsMoreThenConfiguredOne=true;
                }
                if(cppPickList=='Full' || cppPickList=='Exempt' || cppPickList=='Monitored Member' || cppPickList=='Concessional'){
                    cpp=true;
                }
                if(data.PersonAccount.profileName == $A.get("$Label.c.CAANZ_Profile_Member") || data.PersonAccount.profileName == $A.get("$Label.c.CAANZ_Profile_Reviewer") ){
                    
                    if(cpp || data.PersonAccount.account.Membership_Class__c == 'Affiliate' || ( data.PersonAccount.account.Membership_Class__c == 'Full'
                                                                                                && (data.PersonAccount.subscription.Member_Age__c <= 60 || isMembershipApprovalDateIsMoreThenConfiguredOne ))){
                        component.set('v.isCPPForAUToDisplay', true);
                        component.set('v.isCPPQuestionAllowed', true);
                    }
                    
                    if(data.PersonAccount.account.Membership_Class__c == 'Affiliate' || data.PersonAccount.account.Membership_Class__c == 'Full'){
                        component.set('v.toDisplayAUAndOverseasRegistration',true); 
                    }
                    
                    if(data.PersonAccount.account.Membership_Class__c == 'Full'){
                        if(data.PersonAccount.account.CPP_Picklist__c=='Exempt' || 
                           data.PersonAccount.account.CPP_Picklist__c=='Suspended' || 
                           data.PersonAccount.account.CPP_Picklist__c=='Removed' || 
                           data.PersonAccount.account.CPP_Picklist__c=='Monitored Member' || 
                           data.PersonAccount.account.CPP_Picklist__c=='Concessional' ||
                           $A.util.isEmpty(data.PersonAccount.account.CPP_Picklist__c)){
                            component.set('v.isCertPublicPractise',true); 
                        }
                    }
                    
                }else if(data.PersonAccount.profileName == $A.get("$Label.c.CAANZ_Profile_Provisional_Member")){
                    if(cpp || data.PersonAccount.account.Membership_Class__c == 'Affiliate' || ( data.PersonAccount.account.Membership_Class__c == 'Full'
                                                                                                && (data.PersonAccount.subscription.Member_Age__c <= 60 || isMembershipApprovalDateIsMoreThenConfiguredOne ))){
                        component.set('v.isCPPForAUToDisplay', true);
                        component.set('v.isCPPQuestionAllowed', true);
                    }
                    
                    if(data.PersonAccount.account.Membership_Class__c == 'Affiliate' || data.PersonAccount.account.Membership_Class__c == 'Full'){
                        component.set('v.toDisplayAUAndOverseasRegistration',true); 
                    }
                    
                    if(data.PersonAccount.account.Membership_Class__c == 'Full'){
                        if(data.PersonAccount.account.CPP_Picklist__c=='Exempt' || 
                           data.PersonAccount.account.CPP_Picklist__c=='Suspended' || 
                           data.PersonAccount.account.CPP_Picklist__c=='Removed' || 
                           data.PersonAccount.account.CPP_Picklist__c=='Monitored Member' || 
                           data.PersonAccount.account.CPP_Picklist__c=='Concessional' ||
                           data.PersonAccount.account.CPP_Picklist__c==''){
                            component.set('v.isCertPublicPractise',true);
                        }
                    }
                    
                }else if(data.PersonAccount.profileName == $A.get("$Label.c.CAANZ_Profile_Non_Member")){
                    if(cpp || data.PersonAccount.account.Membership_Class__c == 'Affiliate' || ( data.PersonAccount.account.Membership_Class__c == 'Full'
                                                                                                && (data.PersonAccount.subscription.Member_Age__c <= 60 || isMembershipApprovalDateIsMoreThenConfiguredOne ))){
                        component.set('v.isCPPForAUToDisplay', true);
                        component.set('v.isCPPQuestionAllowed', true);
                    }
                    
                    if(data.PersonAccount.account.Membership_Class__c == 'Affiliate' || data.PersonAccount.account.Membership_Class__c == 'Full'){
                        component.set('v.toDisplayAUAndOverseasRegistration',true); 
                    }
                    if(data.PersonAccount.account.Membership_Class__c == 'Full'){
                        if(data.PersonAccount.account.CPP_Picklist__c=='Exempt' || 
                           data.PersonAccount.account.CPP_Picklist__c=='Suspended' || 
                           data.PersonAccount.account.CPP_Picklist__c=='Removed' || 
                           data.PersonAccount.account.CPP_Picklist__c=='Monitored Member' || 
                           data.PersonAccount.account.CPP_Picklist__c=='Concessional' ||
                           $A.util.isEmpty(data.PersonAccount.account.CPP_Picklist__c)){
                            component.set('v.isCertPublicPractise',true);
                        } 
                    }
                }
            }
            /*if(component.get("v.selectedCountry") == 'New Zealand' && data.PersonAccount.account.Membership_Class__c == 'Full'){
                    component.set('v.isCPDForNZ',true);
                }
                else{
                    component.set('v.isCPDForNZ',false);
                }*/
            
            /* Check if Registration component for NZ has to be displayed */
            /*if(component.get("v.selectedCountry") == 'New Zealand' && (data.PersonAccount.account.Qualified_Auditor__c == true || 
                                                                           data.PersonAccount.account.Licensed_Auditor__c == true || 
                                                                           data.PersonAccount.account.Insolvency_Practitioner__c == true) && 
                   !(data.PersonAccount.profileName==$A.get("$Label.c.CAANZ_Profile_Non_Member"))){
                    component.set('v.isRegistrationForNZ',true);
                }
                else{
                    component.set('v.isRegistrationForNZ',false);
                }*/
            
            
            //component.set("v.memberProfile",data.PersonAccount.profileName);
            /*if(data.PersonAccount.profileName==$A.get("$Label.c.CAANZ_Profile_Provisional_Member")){
                    component.set('v.disProvisionMember',false);
                }*/ 
            /*if(!$A.util.isEmpty(component.get("v.selectedCountry")) && component.get("v.disProvisionMember")==false){
                    component.set('v.showPrivacyConsent',true);
                }*/
            /* if((component.get("v.selectedCountry") == 'Australia' || component.get("v.selectedCountry") == 'Overseas') &&
               (data.PersonAccount.account.CPP_Picklist__c=='Suspended' || data.PersonAccount.account.CPP_Picklist__c=='Removed')){
                component.set('v.isCertPublicPractise',true);
            } */
            /*if((data.PersonAccount.account.CPP_Picklist__c=='Suspended' || data.PersonAccount.account.CPP_Picklist__c=='Removed')&&(component.get("v.selectedCountry") == 'New Zealand')){
                    component.set('v.isCPPForNZ', true);component.get("v.selectedCountry") == 'Overseas'
                }
                else{
                    component.set('v.isCPPForNZ', false);
                }*/
            
            /**
             * Write the condition to display CPD For AU or Not based on the user Story, this is just based on Country
             */ 
            if(component.get("v.selectedCountry") == 'Australia' || component.get("v.selectedCountry") == 'Overseas'){
                
                
                // https://charteredaccountantsanz.atlassian.net/browse/ARTTB-424
                // If member is a full or affiliate audito
                /* System checks if the member is a CPP holder and Affiliate, or full members and is not:
                    -aged 60 or over at 30/6/19; or
                    -admitted to Full membership after 30/6/18
				*/
                //component.set('v.isCPPForAUToDisplay', true);
                /* var membershipAppDate = data.PersonAccount.account.Membership_Approval_Date__c;
                var cppPickList = data.PersonAccount.account.CPP_Picklist__c;
                var cpp = false;
                var configuredDataSetUp = data.casubMandatoryNotificationConfig;
                var configureFullMembershipConditionalDate = configuredDataSetUp.Membership_Approval_Date__c;  
                var isMembershipApprovalDateIsMoreThenConfiguredOne = false;
                
                if(!$A.util.isUndefined(membershipAppDate) && membershipAppDate > configureFullMembershipConditionalDate){
                    isMembershipApprovalDateIsMoreThenConfiguredOne=true;
                }
                if(cppPickList=='Full' || cppPickList=='Exempt' || cppPickList=='Monitored Member' || cppPickList=='Concessional'){
                    cpp=true;
                }
                if(cpp && (data.PersonAccount.account.Membership_Class__c == 'Affiliate' || data.PersonAccount.account.Membership_Class__c == 'Full') 
                   && (data.PersonAccount.subscription.Member_Age__c <= 60 || isMembershipApprovalDateIsMoreThenConfiguredOne )){
                    component.set('v.isCPPQuestionAllowed', true);
                    component.set('v.isCPPForAUToDisplay', true);
                }else{
                    component.set('v.isCPPForAUToDisplay', false);
                }
                */
                
                /*if(data.PersonAccount.account.Membership_Class__c == 'Affiliate' || data.PersonAccount.account.Membership_Class__c == 'Full'){
                    component.set('v.toDisplayAUAndOverseasRegistration',true);
                }*/
                
            }else{
                //component.set('v.toDisplayAUAndOverseasRegistration',false);
                //component.set('v.isCPPForAUToDisplay', false);
            }
            component.set("v.casubMandatoryNotificationCASetting",data.casubMandatoryNotificationCASettingQuestions);
            
            
            
            if(!$A.util.isEmpty(data)){
                this.setCountryOptions(component,data);
            }
            
            //this.createDestroyCPD(component); 
            /*
             * Set Questionnaire_Response__c Questions and Ansers
             */ 
            
            this.setQustionairResponseServiceOptions(component);
        });
    },
    loadConfigurationDataOnCountryChange: function(component){
        var params;
        this.enqueueAction(component, "getConfigurationMetaDataForMandatoryObligationOnCountryChange", params, function(data){
            component.set("v.casubMandatoryNotificationCASetting",data.casubMandatoryNotificationCASettingQuestions);
        });
        /* Added by Sumit - To dynamically creating and Destryoing CPD Component based on Country */
        //this.createDestroyCPD(component);    
    },
    setCountryOptions : function(component,data){
        var countryOptions = [];
        countryOptions = (data.countryPickListValuesList || []).map(function(result) {
            var selectedCountry = false;
            if(!$A.util.isUndefined(data.questionnaireResponse)){
                if(!$A.util.isUndefined(data.questionnaireResponse.Residential_Country__c) && 
                   !$A.util.isEmpty(data.questionnaireResponse.Residential_Country__c) && 
                   data.questionnaireResponse.Residential_Country__c===result){
                    //selectedCountry = true;
                    /* Code Corrected By Paras - If the Country is Set, the sub component will be loaded. */
                    component.set("v.selectedCountry",result);
                }
                if(component.get("v.selectedCountry") == result){
                    selectedCountry = true;
                }
            }
            return { label: result, value: result, selected:selectedCountry };	         
        });
        component.set("v.countryOptions",countryOptions);
    },
    enqueueAction: function(component, method, params, callback){
        this.toggleSpinner(component, true);
        var action = component.get("c." + method); 
        if(params) action.setParams(params);
        action.setCallback(this, function(response){
            if(response.getState() === "SUCCESS") {
                if(callback) callback.call(this, response.getReturnValue());
            } else if(response.getState() === "ERROR") {
                console.error(response.getError());
            }
            this.toggleSpinner(component, false);
        });
        $A.enqueueAction(action);
    },
    toggleSpinner: function(component, show) {
        var spinner = component.find("spinner");
        $A.util.addClass(spinner, show ? "slds-show" : "slds-hide");
        $A.util.removeClass(spinner, !show ? "slds-show" : "slds-hide");
    },
    
    setQustionairResponseServiceOptions:function(component){
        var questionnaireResponseSobject = component.get("v.questionnaireResponseSobject");
        if (!$A.util.isUndefinedOrNull(component.get("v.questionnaireResponseSobject")) && 
            !$A.util.isUndefinedOrNull(component.get("v.questionnaireResponseSobject.Answer_42__c"))) {
            var selectedOptions = component.get("v.questionnaireResponseSobject.Answer_42__c");
            var  selectedOptionsArray= selectedOptions.split(",");
            component.set("v.selectedServiceOptions",selectedOptionsArray);
        }
        
    },
    
    validatePrivacyConsent: function(component, callback) {
        var memberProfile = component.get("v.memberProfile");
        var subscriptionSobject = component.get("v.globalSubscriptionSobject");
        console.log('subscriptionSobject===' + JSON.stringify(subscriptionSobject));
        var isAnySectionVisible=false;
        var sectionVisibilityObject = {};
        sectionVisibilityObject.isCPDForNZ = component.get("v.isCPDForNZ");
        sectionVisibilityObject.isCPPForNZ = component.get("v.isCPPForNZ");
        sectionVisibilityObject.isRegistrationForNZ = component.get("v.isRegistrationForNZ");
        sectionVisibilityObject.isCPPForAUToDisplay = component.get("v.isCPPForAUToDisplay");
        sectionVisibilityObject.isCertPublicPractise = component.get("v.isCertPublicPractise");
        sectionVisibilityObject.toDisplayAUAndOverseasRegistration = component.get("v.toDisplayAUAndOverseasRegistration");
        
        if(component.get("v.isCPDForNZ") || component.get("v.isCPPForNZ") || component.get("v.isRegistrationForNZ") || 
           component.get("v.isCPPForAUToDisplay") || component.get("v.isCertPublicPractise") || component.get("v.toDisplayAUAndOverseasRegistration")){
            isAnySectionVisible = true;
        }
        if(component.get("v.selectedCountry")!='' && component.get("v.selectedCountry") == 'New Zealand'){
            if(isAnySectionVisible && ($A.util.isUndefinedOrNull(subscriptionSobject.Obligation_Sub_Components__c ) || $A.util.isEmpty(subscriptionSobject.Obligation_Sub_Components__c ))){                        
                var errorMessage = "Please complete the section above and fill all neccessary details to proceed,click Save & Continue for each section";
                this.showToast(component,errorMessage);
            }else if(component.get("v.isCPDForNZ") && !subscriptionSobject.Obligation_Sub_Components__c.includes("CPD (Continuing Professional Development)")){
                var errorMessage = "Please complete the CPD (Continuing Professional Development) section to proceed";
                this.showToast(component,errorMessage);
            }else if(component.get("v.isRegistrationForNZ") && !subscriptionSobject.Obligation_Sub_Components__c.includes("Registration Details")){
                var errorMessage = "Please complete the Registration Details section to proceed";
                this.showToast(component,errorMessage);
            }else if(component.get("v.isCPPForNZ") && !subscriptionSobject.Obligation_Sub_Components__c.includes("Accounting Services")){
                var errorMessage = "Please complete the Accounting Services section to proceed";
                this.showToast(component,errorMessage);
            }else{
                var privacyConsentComp = component.find('privacyConsent');
                privacyConsentComp.validatePrivacyConsent({sectionVisibilityObject: sectionVisibilityObject,memberProfile:memberProfile,callback: $A.getCallback(callback) }); 
            }
        }else if(component.get("v.selectedCountry") == 'Australia' || component.get("v.selectedCountry") == 'Overseas' ){
            if(isAnySectionVisible &&  ($A.util.isUndefinedOrNull(subscriptionSobject.Obligation_Sub_Components__c ) || $A.util.isEmpty(subscriptionSobject.Obligation_Sub_Components__c ))){                        
                var errorMessage = "Please complete the section above and fill all neccessary details to proceed,click Save & Continue for each sections";
                this.showToast(component,errorMessage);
            }else if(component.get("v.isCPPForAUToDisplay") && !subscriptionSobject.Obligation_Sub_Components__c.includes("CPD (Continuing Professional Development)")){
                var errorMessage = "Please complete the CPD (Continuing Professional Development) section to proceed";
                this.showToast(component,errorMessage);
            }else if(component.get("v.toDisplayAUAndOverseasRegistration") && !subscriptionSobject.Obligation_Sub_Components__c.includes("Registration Details")){
                var errorMessage = "Please complete the Registration Details section to proceed";
                this.showToast(component,errorMessage);
            }else if(component.get("v.isCertPublicPractise") && !subscriptionSobject.Obligation_Sub_Components__c.includes("Certificate of Public Practice")){
                var errorMessage = "Please complete the Certificate of Public Practice section to proceed";
                this.showToast(component,errorMessage);
            }else{
                var privacyConsentComp = component.find('privacyConsent');
                privacyConsentComp.validatePrivacyConsent({sectionVisibilityObject: sectionVisibilityObject, memberProfile:memberProfile,callback: $A.getCallback(callback) });                           
            }
        }else if(component.get('v.showPrivacyConsent')){
            var privacyConsentComp = component.find('privacyConsent');
            privacyConsentComp.validatePrivacyConsent({sectionVisibilityObject: sectionVisibilityObject, memberProfile:memberProfile,callback: $A.getCallback(callback) }); 
        } else{
            var errorMessage = "Please select Country of residence and fill all neccessary details to proceed";
            this.showToast(component,errorMessage);
        }   
        
    },
    showToast : function(component,text,questionnaireResponseSobjectId) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Warning!",
            "message": text,
            type : 'warning',
            mode: 'dismissible '
        });
        toastEvent.fire();
    },   
    
})