({
    init : function(component, event, helper) {
        console.log("Check if thei init is running");
        helper.sendToService(component, "c.load", null, function(results) {
            
            // adjust the isLoading control attribute so that page specific code can tell if loading is complete
            component.set("v.isLoading", false);
            
            var sm = component.find("stateManager");
            var flow = sm.get("v.getter")("flow");
            
            var questionnaireCountryCode = results["prop.countryCode"];
            if (flow) {
                if ($A.util.isEmpty(questionnaireCountryCode)) {
                    if (flow == 'wizard') {
    //                    component.set("v.chooseUrl","/customer/s/subscription-wizard-page-1");     // Removed the reference to /customer/s               
                        component.set("v.chooseUrl","/subscription-wizard-page-1");                    
                    } // if flow == 'home' then leave the chooseUrl as the default in component xml
                    helper.showMessage(component);
                } else if (results["prop.skipNotifications"]) {
                    if (flow == 'wizard') {
                        //helper.navigate("/subscription-wizard-page-5"); Removed for provs 2018
                        helper.navigate("/subscription-wizard-page-4-provs-2018"); //Added for provs 2018
                    } else { // if flow == 'home', show return messages as default
                        //component.set("v.chooseLabel","Mandatory Notifications are not required for Provisional Members"); Removed for provs 2018                   
                        //component.set("v.chooseUrlLabel","Click here to return to the Home page");   Removed for provs 2018                  
                        component.set("v.chooseUrl","/subscription-wizard-page-4-provs-2018");                    
                        //helper.showMessage(component);
                    }
                } else {
                    helper.navigate(component.get("v.url")+"-"+questionnaireCountryCode);
                }
            } else {
                console.log("flow state data not set!");
            }
        });
    },    
    navigate : function(destination) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": destination,
            "isredirect" :false
        });
        urlEvent.fire();        
    },
    showMessage : function(component) {
        $A.util.addClass(component.find("loading"),"slds-hide");
        $A.util.removeClass(component.find("chooser"),"slds-hide");
    }
})