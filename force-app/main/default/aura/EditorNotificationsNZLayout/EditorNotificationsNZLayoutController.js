({
    handleDependency: function(component, event, helper) {
        if (event.getParam("dependencyKey") == "ACC_NOTCPP") {
            var notCPP = event.getParam("isMatch");
            component.set("v.section4CSS", notCPP?'slds-show':'slds-hide');         
        }
    } 
  
})