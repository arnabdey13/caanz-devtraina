({
    getQuestionnaire : function(component, event, helper) {
        var action = component.get("c.fetchQPRQuestionnaire");
        if(component.get("v.questionnairesStatuses")!= null && component.get("v.questionnairesStatuses")!= ''){
            action.setParams({"questionnaireStatus": component.get("v.questionnairesStatuses")})
        }
        action.setCallback(this, function(a){            
        	var rtnValue = a.getReturnValue();
            $A.log('rtnValue='+rtnValue);
            if (rtnValue !== null) {
                component.set('v.questionnaires',rtnValue);
            }
        });
        $A.enqueueAction(action);
    }
})