({
    getQuestionnaire: function(component, event, helper) {
        var action = component.get("c.fetchQPRQuestionnaire");
        if (component.get("v.questionnairesStatuses") != null && component.get("v.questionnairesStatuses") != '') {
            action.setParams({
                "questionnaireStatus": component.get("v.questionnairesStatuses")
            })
        }
        action.setCallback(this, function(a) {
            var rtnValue = a.getReturnValue();
            $A.log('rtnValue=' + rtnValue);
            if (rtnValue !== null) {
                component.set('v.questionnaires', rtnValue);
            }
        });
        $A.enqueueAction(action);
    },

    // Navigate to Questionnaire detail on button click in Questionnaires list
    gotoQuestionnaire: function(component, event, helper) {
        var idx = event.target.id;
        console.log(idx);

        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/detail/" + idx
        });
        urlEvent.fire();
    }
})