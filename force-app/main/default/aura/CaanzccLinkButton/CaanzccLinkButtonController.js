({
    goToDestination: function(component, event, helper) {

        var compLinkDest = event.currentTarget.id;
        var compLinkDestLow = compLinkDest.replace(/\s+/g, '-').toLowerCase();
        console.log(compLinkDest);
        console.log(compLinkDestLow);

        if (compLinkDest.includes('.') == false) {
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": '/' + compLinkDestLow
            });
            urlEvent.fire();
        } else {
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": compLinkDest
            });
            urlEvent.fire();
        }
    }
})