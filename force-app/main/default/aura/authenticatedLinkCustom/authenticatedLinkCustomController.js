({
    linkClicked : function(component, event, helper) {
        var sm = component.find("stateManager");
        sm.get("v.setter")("flow","home");
        
        var appEvent = $A.get("e.c:UserClickEvent");
        appEvent.setParams({sourceLabel: component.get("v.linkButton")}).fire();
        
    },
	getServices : function(component, event, helper) {
        //alert('getServiceAccess');
        
        $A.util.toggleClass('url','hide');

        var serviceName = component.get("v.linkName");
        var action = component.get("c.getServiceAccess");
        action.setParams({serviceName:serviceName});
        action.setCallback(this, function(a){
        	var rtnValue = a.getReturnValue();
            $A.log('rtnValue='+rtnValue);
            if (rtnValue !== null) {
                component.set('v.userHasAccess',rtnValue);
            }
        });
        $A.enqueueAction(action);
    }
})