({
    load : function(component, event, helper) {
       var items = [];
       for (i = 1; i <= 5; i++) {
            var item = component.get("v.item"+i);
            if (item) {
                items.push(item);
            }
       }
       component.set("v.items", items);
    }
})