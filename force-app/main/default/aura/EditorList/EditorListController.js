({
    handleSetLabels : function(component, event, helper) {
        if (event.getParam("field") == component.get("v.field")) {
           for (i = 1; i <= 5; i++) {
                var label = event.getParam("label"+i);
                component.set("v.item"+i, label);
           }
           helper.load(component, event, helper);
        }
    }
})