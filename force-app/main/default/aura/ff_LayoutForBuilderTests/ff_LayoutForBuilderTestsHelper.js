({
    createComponent: function (component, type, params, name) {
        return $A.getCallback(
            function (context) { // when invoked using .then, this will wait for the previous promise to resolve
                return new Promise(function (resolve, reject) {
                    $A.createComponent(type, params,
                        function (newComponent, status, errorMessage) {
                            if (status === "SUCCESS") {
                                context[name] = newComponent;
                                resolve(context);
                            } else {
                                console.log(errorMessage);
                                reject(errorMessage);
                            }
                        }
                    );
                });
            }
        );
    },
    createTestComponent: function (component, label, name, stage) {
        return this.createComponent(component,
            "c:ff_FieldTextControl",
            {
                required: true,
                label: label,
                typeRequired: "Integer",
                loadKey: "Account",
                field: "Balance1__c",
                visibleForStage: stage
            },
            name);
    },
    fireStageChangeEvent: function (done, stage) {
        var appEvent = $A.get("e.c:ff_EventStageChange");

        var headings = [];

        for (var i = 0; i < 3; i++) {
            headings[i] = {
                topBoxLabel: "Top Box",
                box10Label: "Box 10",
                box20Label: "Box 20",
                box30Label: "Box 30",
                box40Label: "Box 40",
                bottomBoxLabel: "Bottom Box"
            }
        }


        appEvent.setParams(
            {
                stage: stage,
                headings: headings,
                callback: done
            }
        );
        appEvent.fire();
    },
    includes: function (str, substr) {
        return str.indexOf(substr) >= 0;
    }
})