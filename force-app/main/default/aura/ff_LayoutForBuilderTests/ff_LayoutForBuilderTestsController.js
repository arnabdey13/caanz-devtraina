({
    doInit: function (component, event, helper) {

        var test = helper.driver(component, event, helper);

        // references to all the components being tested
        var layout = component.find("layout1");

        var startTests = test.start({
            focused: layout,
            description: "valid state after load"
        })

        startTests

        //////////// START OF TOP BOX TESTS ////////////

        //create ff_* component
            .then(helper.createTestComponent(component, "topBoxText", "topBoxText", 0))
            //set component in layout to simulate drag and drop
            .then(test.wait(function (context) {
                layout.set("v.topBox", [context.topBoxText]);
            }))
            //validate that component array is not empty
            .then(test.wait(function (context) {
                if ($A.util.isEmpty(layout.get("v.topBox"))) {
                    throw Error("Top Box array should hold one component");
                }
            }))
            //validate that component is visible
            .then(test.wait(function (context) {
                var components = layout.get("v.topBox");
                if (components.length == 1) {
                    var isVisible = components[0].get("v.visible");
                    if (!isVisible) {
                        throw Error("The text field component should be visible");
                    }
                }
            }))
            //validate that the label in the top box is empty
            .then(test.wait(function (context) {
                if (!$A.util.isEmpty(layout.get("v.topBoxLabel"))) {
                    throw Error("Label should be empty")
                }
            }))
            //set the label in the top box
            .then(test.setAttribute("v.topBoxLabel", "Test Label"))
            //validate the label is visible now
            .then(test.assertEquals("Test Label", "v.topBoxLabel", "Label should not be empty"))

            //fire stage change event to invoke rendering of boxes
            .then(test.whenDone(function (context, done) {
                helper.fireStageChangeEvent(done, 0);
            }))
            // validate that all boxes are hidden except the topBox
            .then(test.assertEquals(false, function () {
                return helper.includes(layout.get("v.topBoxCss"), "slds-hide");
            }, "Top Box should be visible"))
            .then(test.assertEquals(false, function () {
                return helper.includes(layout.get("v.box10Css"), "slds-show");
            }, "Box 10 should be hidden"))
            .then(test.assertEquals(false, function () {
                return helper.includes(layout.get("v.box20Css"), "slds-show");
            }, "Box 20 should be hidden"))
            .then(test.assertEquals(false, function () {
                return helper.includes(layout.get("v.box30Css"), "slds-show");
            }, "Box 30 should be hidden"))
            .then(test.assertEquals(false, function () {
                return helper.includes(layout.get("v.box40Css"), "slds-show");
            }, "Box 40 should be hidden"))
            .then(test.assertEquals(false, function () {
                return helper.includes(layout.get("v.bottomBoxCss"), "slds-hide");
            }, "Bottom Box should be hidden"))

            //////////// START OF BOX 10 TESTS ////////////
            //create multiple ff_* components
            .then(helper.createTestComponent(component, "box10Row1Cell1Text", "box10Row1Cell1Text", 0))
            .then(helper.createTestComponent(component, "box10Row2Cell1Text", "box10Row2Cell1TextA", 0))
            .then(helper.createTestComponent(component, "box10Row2Cell1Text", "box10Row2Cell1TextB", 0))
            .then(helper.createTestComponent(component, "box10Row2Cell1Text", "box10Row2Cell1TextC", 0))
            .then(helper.createTestComponent(component, "box10Row2Cell2Text", "box10Row2Cell2TextA", 0))
            .then(helper.createTestComponent(component, "box10Row2Cell2Text", "box10Row2Cell2TextB", 0))
            .then(helper.createTestComponent(component, "box10Row3Cell1Text", "box10Row3Cell1Text", 0))
            //set components to layout to simulate drag and drop behaviour
            .then(test.wait(function (context) {
                layout.set("v.box10Row1Cell1", [context.box10Row1Cell1Text]);
                layout.set("v.box10Row2Cell1", [context.box10Row2Cell1TextA, context.box10Row2Cell1TextB, context.box10Row2Cell1TextC]);
                layout.set("v.box10Row2Cell2", [context.box10Row2Cell2TextA, context.box10Row2Cell2TextB]);
                layout.set("v.box10Row3Cell1", [context.box10Row3Cell1Text]);
            }))
            //fire stage change event to invoke rendering of boxes
            .then(test.whenDone(function (context, done) {
                helper.fireStageChangeEvent(done, 0);
            }))
            //validate that all boxes are hidden except the topBox and box 10
            .then(test.assertEquals(false, function () {
                return helper.includes(layout.get("v.topBoxCss"), "slds-hide");
            }, "Top Box should be visible"))
            .then(test.assertEquals(false, function () {
                return helper.includes(layout.get("v.box10Css"), "slds-hide");
            }, "Box 10 should be visible"))
            .then(test.assertEquals(false, function () {
                return helper.includes(layout.get("v.box20Css"), "slds-show");
            }, "Box 20 should be hidden"))
            .then(test.assertEquals(false, function () {
                return helper.includes(layout.get("v.box30Css"), "slds-show");
            }, "Box 30 should be hidden"))
            .then(test.assertEquals(false, function () {
                return helper.includes(layout.get("v.box40Css"), "slds-show");
            }, "Box 40 should be hidden"))
            .then(test.assertEquals(false, function () {
                return helper.includes(layout.get("v.bottomBoxCss"), "slds-hide")
            }, "Bottom Box should be hidden"))

            /////  leave layout in a state for testing autocompletes in list controls overlapping with driver

            .then(helper.createComponent(component,
                "c:ff_FieldAutoComplete",
                {
                    label: "Autocomplete for testing z-index results overlapping driver div",
                    loadKey: "Account",
                    field: "Foo",
                    visibleForStage: 2
                },
                "box40Row1Cell1"))
            .then(helper.createComponent(component,
                "c:ff_SampleDriver",
                {
                    searchEnabled: false // this test handles search requests instead of the driver
                },
                "bottomBox"))
            .then(test.wait(function (context) {
                layout.set("v.box40Row1Cell1", [context.box40Row1Cell1]);
                layout.set("v.bottomBox", [context.bottomBox]);
            }))

            /////  end of layout state for testing

            .then(test.sleep(100))
            //fire stage change event to invoke rendering of boxes
            .then(test.whenDone(function (context, done) {
                helper.fireStageChangeEvent(done, 2);
            }))

            //////////// END OF TESTS ////////////

            // always include these fns to handle the end of the test
            .then(test.pass).catch(test.fail);
    },
    handleAutoCompleteSearch: function (component, event, helper) {
        setTimeout($A.getCallback(function () {
            var p = event.getParams();
            var results = [];
            var count = 50;
            for (var i = 0; i < count; i++) {
                results.push({
                    label1: "Acme" + i,
                    label2: "Address" + i,
                    id: "account" + i,
                    position: i
                });
            }
            p.callback(results);
        }), 100);
    }
})