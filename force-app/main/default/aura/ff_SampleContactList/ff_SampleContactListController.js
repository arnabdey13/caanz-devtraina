({
    handleLifeCycle: function (component, event, helper) {
        switch (event.getParam("type")) {
            case "load-list":
                var readData = event.getParam("opts");
                component.set("v.salutationOptions", readData.wrappers["Contact.Salutation"]);

                helper.loadUploadControls(component, "Contact", "resumeUpload", event.getParam("opts").wrappers);
                break;
            case "load-list-editor":
                component.find("salutation").set("v.options", component.get("v.salutationOptions"));
                var opts = event.getParam("opts");
                if (!$A.util.isEmpty(opts)) { // opts will be present for editing existing records, empty for new
                    // load the auto-complete from the parent data
                    var employer = component.find("employer");
                    employer.set("v.valueChangeIgnored", true);
                    employer.set("v.searchText", opts.record.Account.Name);
                    employer.set("v.value", opts.record.Account.Id);
                    employer.set("v.valueChangeIgnored", false);
                }
                break;
            case "editor-pre-close":
                // don't need to store auto-complete value because it's not used in the list view
                break;
        }
    },
    handleValuesChange: function (component, event, helper) {
        //helper.showValuesJSON(component, event.getParam("value"));
    }
})