({
	doInit: function(cmp, event, helper) {
		helper.getAddressData(cmp);
	},
    parentHandling : function(cmp, event,helper) {
        var currentAddressOption = event.getParam("currentAddressOption");
        var addressId = event.getParam("selectedAddressId");
        cmp.set("v.selectedAddressId", addressId);
        event.stopPropagation();
        helper.setAddress(cmp, true);
    },
    
    getAddressPrefix: function(otherAddress){
		return otherAddress ? "Other" : "Mailing";
	},
    /*
	onMailingSameAsResidentialChange: function(cmp, event, helper){
		if(cmp.get("v.initDone") && event.getParam("value") !== event.getParam("oldValue")){
			if(helper.isMailingSameAsResidential(cmp)) helper.setMailingAsResidential(cmp); 
			else helper.setAddressData(cmp, false);
		}
	},
    */
	onOtherAddressChange: function(cmp, event, helper){
		helper.setAddressData(cmp, true);
		/*if(helper.isMailingSameAsResidential(cmp)) {
			helper.setMailingAsResidential(cmp); 
		}*/
	},
    /*
	onMailingAddressChange: function(cmp, event, helper){
		helper.setAddressData(cmp, false);
	},
    */
	validateOtherAddress: function(cmp, event, helper){
		helper.showAddressSearch(cmp, true);
	},
	validateMailingAddress: function(cmp, event, helper){
		helper.showAddressSearch(cmp, false);
	},
	setOtherAddress: function(cmp, event, helper){
		helper.triggerCallback(event);
		if(helper.validateSelectedAddress(cmp)) helper.setAddress(cmp, true);
	},
    /*
	setMailingAddress: function(cmp, event, helper){
		helper.triggerCallback(event);
		if(helper.validateSelectedAddress(cmp)) helper.setAddress(cmp, false);
	},
    */
	performSave: function(cmp, event, helper){
		cmp.set("v.errorMessage", "");
		var params = event.getParam ? event.getParam("arguments").params || {} : {};
		if(cmp.get("v.account") && cmp.get("v.contact")) {
			helper.saveAddressData(cmp, params.callback,helper);
		} else if(params.callback) {
			params.callback();
		}
	},
	performCancel: function(cmp, event, helper){
	}
})