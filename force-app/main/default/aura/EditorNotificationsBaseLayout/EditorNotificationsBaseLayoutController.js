({
    handleDependency: function(component, event, helper) {
        if (event.getParam("dependencyKey") == "ACC_FELLOWNOMS1") {
           var FELLOWNOMS1 = event.getParam("isMatch");
            component.set("v.section5CSS", FELLOWNOMS1?'slds-show':'slds-hide');   
       
        }
        if (event.getParam("dependencyKey") == "ACC_PROVMN") {
           var PROVMN = event.getParam("isMatch");
            component.set("v.section6CSS",PROVMN?'slds-show':'slds-hide');   
       
        }
    } 
  
})