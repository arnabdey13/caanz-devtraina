({
	init : function(component, event, helper) {
        var educationRecordId = component.get("v.recordId");
        var action = component.get("c.restoreEducationRecord");
		action.setParams({
        "educationRecordId":educationRecordId
    });
    // Queue this action to send to the server
    $A.enqueueAction(action);
	}
})