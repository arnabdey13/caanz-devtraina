({
	doInit: function(cmp, event, helper) {
		helper.getWizardData(cmp);
	},
	acceptTcAndCs: function(cmp, event, helper){
		var onSuccess = event.getParam ? event.getParam("arguments").onSuccess : null;
		helper.acceptTcAndCs(cmp, onSuccess);
	},
	rejectTcAndCs: function(cmp, event, helper){
		var onSuccess = event.getParam ? event.getParam("arguments").onSuccess : null;
		helper.rejectTcAndCs(cmp, onSuccess);
	},
	getContactId: function(cmp, event, helper){
		var callback = event.getParam ? event.getParam("arguments").callback : null;
		if(callback) callback(cmp.get("v.wizardData.ContactId"));
	},
	finishWizard: function(cmp, event, helper){
		helper.finishWizard(cmp);
	}
})