({
	getWizardData: function(cmp) {
		this.enqueueAction(cmp, "getWizardData", {}, function(data){
			cmp.set("v.wizardData", data = JSON.parse(data));
			this.initWizardCheck(cmp);
		});
	},
	initWizardCheck: function(cmp){
		var isHomePage = cmp.get("v.isHomePage");
		var data = cmp.get("v.wizardData");
		if(isHomePage && data.OnBoarding_Required__c){
			this.navigateToOnBoardWizard(cmp);
		} else if(data.OnBoarding_Required__c){
			this.fireOnCheckDoneEvent(cmp);
		} else if(!isHomePage){
			this.navigateToHomePage(cmp);
		}
	},
	acceptTcAndCs: function(cmp, onSuccess){
		this.enqueueAction(cmp, "acceptTermsAndConditions", {}, function(){
			if(onSuccess) $A.enqueueAction(onSuccess);
		});
	},
	rejectTcAndCs: function(cmp, onSuccess){
		this.enqueueAction(cmp, "rejectTermsAndConditions", {}, function(){
			if(onSuccess) $A.enqueueAction(onSuccess);
		});
	},
	finishWizard: function(cmp){
		this.navigateToHomePage(cmp);
	},
	enqueueAction: function(cmp, method, params, callback){
		this.toggleSpinner(cmp, true);
		var action = cmp.get("c." + method); 
		if(params) action.setParams(params);
		action.setCallback(this, function(response){
			if(response.getState() === "SUCCESS") {
				if(callback) callback.call(this, response.getReturnValue());
	        } else if(response.getState() === "ERROR") {
	        	console.error(response.getError());
	        }
	        this.toggleSpinner(cmp, false);
		});
		$A.enqueueAction(action);
	},
	toggleSpinner: function(cmp, show) {
		var spinner = cmp.find("spinner");
		$A.util.addClass(spinner, show ? "slds-show" : "slds-hide");
		$A.util.removeClass(spinner, !show ? "slds-show" : "slds-hide");
	},
	navigateToOnBoardWizard: function(cmp){
		$A.get("e.force:navigateToURL").setParams({ "url": '/onboard-wizard' }).fire();
	},
	navigateToHomePage: function(cmp){
		$A.get("e.force:navigateToURL").setParams({ "url": '/' }).fire();
	},
	fireOnCheckDoneEvent: function(cmp){
		var event = cmp.get("v.onCheckDone");
		if(event) $A.enqueueAction(event);
	}
})