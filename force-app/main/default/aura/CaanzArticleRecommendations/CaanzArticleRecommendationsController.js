({
	doInit: function(cmp, event, helper) {
		helper.getRecommendations(cmp);
	},
	updateRecommendations: function(cmp, event, helper) {
		helper.getRecommendations(cmp);
	},
	showArticle: function(cmp, event, helper) {
		var url = event.currentTarget.dataset.url;
		var id = event.currentTarget.dataset.id;
		if (typeof url === "undefined" || url === null || url === "") {
			url = "/" + id;
		}
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
        	"url": url
        });
        urlEvent.fire();
	},
	goToRecoArticles : function(cmp, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
        	"url": '/recoarticle'
        });
        urlEvent.fire();		
	}
})