({
	getRecommendations: function(cmp) {
		var params = { limitQuery: cmp.get("v.limit") };
		this.enqueueAction(cmp, "getRecommendations", params, function(data){
			cmp.set("v.recommendations", JSON.parse(data));
			//this.assignTopicColours(cmp);
		});
	},
	enqueueAction: function(cmp, method, params, callback){
		var action = cmp.get("c." + method); 
		if(params) action.setParams(params);
		action.setCallback(this, function(response){
			if(response.getState() === "SUCCESS") {
				if(callback) callback.call(this, response.getReturnValue());
	        } else if(response.getState() === "ERROR") {
	        	console.error(response.getError());
	        }
		});
		$A.enqueueAction(action);
	},

	// [TS] This is a really hacky way of assigning colours to topics.
	// Unfortunately Salesforce doesn't let you use "document.createElement('style');".
	// So instead this function just grabs the first stylesheet in "document.styleSheets"
	// and adds new css rules to that stylesheet.
	/*assignTopicColours: function(cmp) {
		var topics = this.cleanString(cmp.get('v.topics')).split(',');
		var colours = this.cleanString(cmp.get('v.topic-colours')).split(',');
		var articles = cmp.get('v.recommendations');

	    var style = document.styleSheets[0];

		for (var i=0; i<articles.length; i++) {
			var item = articles[i]
			var topic = item.article.Topic__c;
			if (typeof topic !== "undefined" && topic !== null) {
				var index = topics.indexOf(this.cleanString(topic));
				var colour = colours[index] || "#000000"

			    if ( style.addRule ){
			        style.addRule("#" + item.article.Id, "background: " + colour);
			    } else if ( style.insertRule ) {
			        style.insertRule("#" + item.article.Id + "{background: " + colour + "}",0);
			    }
			}
		}
	},

	// Remove white spaces and make all characters lower case.
	cleanString: function(str) {
		return str.toLowerCase().replace(/\s/g, '')
	}*/
})