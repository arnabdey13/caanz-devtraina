({
    loadRelatedFieldsInEditor: function (component, record) {
        if (!component.get("v.adding")) {
            // inherited behaviour only loads values on record, not parent fields so do that here
            component.find("employer").set("v.value", record.Employer__r.Name);
        }
    }
})