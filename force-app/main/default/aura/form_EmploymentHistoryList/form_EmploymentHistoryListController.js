({
    doInit: function (component, event, helper) {
        var mode = component.get("v.mode");
        switch (mode) {
            case "PROV-APP":
                component.set("v.fieldIds", ['employer', 'title', 'status', 'primary', 'website', 'startDate', 'endDate', 'type', 'hours'
                    , 'country', 'street', 'city', 'state', 'postcode']);
                component.find("table").set("v.headerLabels",
                    [component.get("v.editorEmployerLabel"),
                    component.get("v.editorTitleLabel"),
                    component.get("v.editorStartLabel"),
                    component.get("v.editorStatusLabel"),
                        '']);
                break;
            case "MY-DETAILS":
                component.set("v.fieldIds", ['employer', 'title', 'status', 'primary', 'startDate', 'endDate']);
                component.find("table").set("v.headerLabels",
                    [component.get("v.editorEmployerLabel"),
                    component.get("v.editorTitleLabel"),
                    component.get("v.editorStartLabel"),
                    component.get("v.editorStatusLabel"),
                    component.get("v.editorPrimaryLabel")]);
                break;
            default:
                throw Error("Invalid mode: " + mode);
        }
    },
    handleLifeCycle: function (component, event, helper) {
        switch (event.getParam("type")) {
            case "load-list":
                var readData = event.getParam("opts");
                component.set("v.statusOptions", readData.wrappers["Status.opts"]);
                component.set("v.typeOptions", readData.wrappers["Type.opts"]);
                // RXP 05/09/2019 - Add new field 'Organisation Type'
                component.set("v.organisationTypeOptions", readData.wrappers["Account.Type"]);

                // work-around id restrictions in server by transferring any ids in alternative field into id
                var histories = component.get("v.values");
                for (var i = 0; i < histories.length; i++) {
                    if (!$A.util.isEmpty(histories[i].Employer_Name_at_Commencement__c)) {
                        histories[i].Id = histories[i].Employer_Name_at_Commencement__c;
                        delete histories[i].Employer_Name_at_Commencement__c;
                    }
                }
                component.set("v.values", histories);

                break;
            case "load-list-editor":
                component.find("status").set("v.options", component.get("v.statusOptions"));
                if (component.get("v.mode") == "PROV-APP") {
                    component.find("type").set("v.options", component.get("v.typeOptions"));
                }
                if (event.getParam("opts") && event.getParam("opts").record) { // no opts.record for a new load
                    var record = event.getParam("opts").record;
                    component.set("v.adding", false);
                    helper.loadRelatedFieldsInEditor(component, record);
                } else {
                    component.set("v.adding", true);
                    // reset employer box to search mode, in case it has already been used/opted out
                    component.set("v.employerOptOut", false);
                }
                break;
            case "editor-pre-close":
                if (component.get("v.mode") == "PROV-APP") {
                    if (component.get("v.adding")) {
                        // if an employer was selected, extract the label and set it into the optimistically displayed row
                        var selected = component.find("employer").get("v.matchSelected");
                        if (selected) {
                            var values = component.get("v.values");
                            // the Employer__r in the optimistically inserted record will be the id
                            // so replace it with shape expected by a load
                            values[0].Employer__r = { Name: selected.label1, Id: selected.id };
                            component.set("v.values", values);
                            // NOTE: when opted-out, there is no id for the selected employer so that
                            // needs to be loaded in the "load-list-editor" hook above
                        }
                    }

                    // also reset the opt-out so that it's hidden by default when editing another record
                    component.set("v.employerOptOut", false);
                }
                break;
        }
    },
    handleOptOut: function (component, event, helper) {

        var params = event.getParams();
        if (params.field != "Employer__c" && params.field != "BillingStreet") {
            return; // only handle opt-outs from autocompletes in this control
        }

        var isProvApp = component.get("v.mode") == "PROV-APP";

        switch (params.field) {
            case "Employer__c":
                // set editor into employer opt out mode
                component.set("v.employerOptOut", params.optedOut);

                // for prov-app, load countries/states in the QAS control
                if (params.optedOut && isProvApp) {
                    component.find("qas").apiHandleLoad();
                }
                break;
            case "BillingStreet":
                $A.log("street opt out: " + params.optedOut + " provApp: " + isProvApp);
                break;
            default:
                throw Error("Unhandled opt out: " + params.field);
        }
    },
    handleValuesChange: function (component, event, helper) {
        // RXP 16/08/2019 - start - Fire event to register control so it can be accessed by stores that need reference to this control
        helper.fireApplicationEvent(component, 'e.c:AppForms_EventControlRegistration');
        //helper.showValuesJSON(component, event.getParam("value"));
    }
})