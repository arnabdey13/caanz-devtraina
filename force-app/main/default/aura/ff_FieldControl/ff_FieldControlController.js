({
    // the common code fired when a field control is loaded. concrete controls can also watch
    // the attribute and perform extra work. using a change handler is a workaround to the
    // template pattern locker-service issue which only affects concrete sub-components. using
    // the same technique here just to be consistent with sub-component load triggering.
    load: function (component, event, helper) {

        // extract the field value out of the record being loaded and set that into the value
        var readData = event.getParam("value").data;
        var loadKey = component.get("v.loadKey");
        var suppressed = component.get("v.suppressLoad");
        if (!suppressed && loadKey) {
            var records = readData.records[loadKey]; // only if loadKey matches
            if (records && !$A.util.isEmpty(records)) { // and records are present
                var loadPosition = component.get("v.loadPosition");
                if (records[loadPosition].single) { // and the loadKey is for a single record set
                    var firstRecordInList = records[loadPosition].sObjectList[0] // first record since it's a single record set
                    // if the form is an insert form, then there may be no records in the wrapper so check for that
                    if (firstRecordInList) {
                        component.set("v.value", firstRecordInList[component.get("v.field")]);
                    }
                }
            }
        }

        // TODO remove this, it should only be in the concrete rich text component
        var richTextKey = component.get("v.richTextKey");

        if (!$A.util.isUndefinedOrNull(richTextKey)) {
            var wrappers = readData.wrappers[richTextKey];
            if (wrappers && !$A.util.isEmpty(wrappers)) {
                component.set("v.richTextContent", wrappers[0].richtext);
            }
        }

        component.getEvent("lifeCycleEvent").setParams({ type: "load-field" }).fire();

        // report validity back to the driver via it's api fn. this happens for all field controls, even
        // when they don't load from the event so that non-loaded fields can block form save
        // invoke validation (even though may have been invoked by value change) so that all fields
        // in a form can block save, even when not loaded
        helper.validate(component, event, helper);

        // driver may be undefined when running in tests. in that case, don't report
        var driverComponent = event.getParam("value").driver;
        if (driverComponent) {
            var isValid = component.get("v.isValid");
            driverComponent.setFieldValidity(
                component.get("v.loadKey"),
                component.get("v.field"),
                isValid,
                component.get("v.visibleForStage"));
        } else {
            $A.log("No driver found, not reporting field validity on load: " + component.get("v.field"));
        }
        
        // RXP 16/08/2019 - start - Fire event to register control so it can be accessed by stores that need reference to this control
        helper.fireApplicationEvent(component, 'e.c:AppForms_EventControlRegistration');
    },
    parentVizChangeHandler: function (component, event, helper) {
        var parentsAllowVisibility = event.getParam("value");

        if (parentsAllowVisibility) {
            // if parents show a control, enable validation but don't immediately report
            component.set("v.disableValidation", false);
        } else {
            // but if a field is being displayed then enable validation and report using an event
            component.set("v.disableValidation", true);
            helper.validate(component, event, helper);
            if (component.get("v.loaded")) { // RXP - 24/09/2019 - Potential fix, as address fields were getting added to payload with empty values on form submit even when not on address page
                helper.fireValueChange(component);
            }
        }
    },
    handleChange: function (component, event, helper) {
        if (component.get("v.loaded")) {
            helper.validate(component, event, helper);
            component.set("v.displayValidationErrors", true);
            helper.fireValueChange(component);
        }
    },
    handleSetValidation: function (component, event, helper) {
        if (!component.get("v.ignoreSetValidationEvents")) {
            helper.adjustValidation(component, helper, event.getParam("show"), event.getParam("disable"));
        }
    },
    apiSetValidation: function (component, event, helper) {
        var args = event.getParam("arguments");
        helper.adjustValidation(component, helper, args.display, args.disable);
    },
    apiValidate: function (component, event, helper) {
        helper.validate(component, event, helper);
    },
    apiDeriveVisibility: function (component, event, helper) {
        helper.deriveVisibility(component);
    },
    disabledChangeHandler: function (component, event, helper) {
        var params = event.getParams();
        var disabled = params.value;
        if (disabled) {
            component.set("v.disableValidation", true);
            component.set("v.displayValidationErrors", false);
            component.set("v.validationErrors", []);
            component.set("v.specificValidationErrors", []);
            component.set("v.isValid", true);
            component.set("v.placeHolderCopy", component.get("v.placeHolder"));
            component.set("v.placeHolder", "");
        } else {
            component.set("v.disableValidation", false);
            component.set("v.displayValidationErrors", true);
            component.set("v.placeHolder", component.get("v.placeHolderCopy"));
            helper.validate(component, event, helper);
        }
    },
    handleFindAggregateControls: function (component, event, helper) {
        var params = event.getParams();
        var aggregateKey = component.get("v.aggregateKey");

        if (params.aggregateKey == aggregateKey) {
            params.callback(component);
        }
    },
    handleLifeCycle: function (component, event, helper) {
        switch (event.getParam("type")) {
            case "validate":
                helper.validate(component, event, helper);
                break;
        }
    }
})