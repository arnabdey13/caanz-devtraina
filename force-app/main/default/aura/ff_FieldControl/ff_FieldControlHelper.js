({
    validate: function (component, event, helper) {

        if (component.get("v.disableValidation")) {
            component.set("v.validationErrors", []);
            component.set("v.specificValidationErrors", []);
            component.set("v.isValid", true);
        } else {
            var validationErrors = [];

            var value = component.get("v.value");

            // control data for common validation
            var required = component.get("v.required");

            // common validation evaluation
            var isRequiredSatisfied = required && !$A.util.isEmpty(value) || !required;
            if (!isRequiredSatisfied) {
                validationErrors.push(component.get("v.requiredErrorMessage"));
            }

            component.set("v.validationErrors", validationErrors);

            // now invoke any specific validation TODO stop using specificValidationEvent, instead use EventControlLifeCycle
            component.getEvent("specificValidationEvent").fire();

            // set the total validity based on common and specific errors
            this.deriveValidity(component);
        }
    },
    deriveValidity: function (component) {
        var common = component.get("v.validationErrors");
        var specific = component.get("v.specificValidationErrors");
        var isValid = $A.util.isEmpty(common) && $A.util.isEmpty(specific);
        component.set("v.isValid", isValid);
    }
})