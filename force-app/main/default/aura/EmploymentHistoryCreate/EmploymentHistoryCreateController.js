({
    showModalBox: function(component, event, helper) {
        document.getElementById("backGroundSectionId").style.display = "none";
        document.getElementById("newAccountSectionId").style.display = "none";
    },
    
    saveAccount: function(component, event, helper) {
        
        var action = component.get("c.getAccountupdatedlist");
        action.setParams({
            "newAcc": component.get("v.newAccount"),
            "accId" : component.get("v.accountId")
        });
        
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                document.getElementById("backGroundSectionId").style.display = "none";
                document.getElementById("newAccountSectionId").style.display = "none";
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
            }
        });
        
        $A.enqueueAction(action);
    },
    
    searchKeyChange: function(component, event) {
        //console.log('***INSIDE SEARCH BOX**');
        var searchKey = event.getParam("searchKey");
        var action = component.get("c.findByName");
        action.setParams({
            "searchKey": searchKey
        });
        action.setCallback(this, function(a) {           
            component.set("v.contacts", a.getReturnValue());
            //console.log('inside action retunr' + component.get("v.contacts"));
        });
        $A.enqueueAction(action);
    },
    
    parentHandling : function(cmp, event) {       
        var currentTabNumber = event.getParam("accname");
        cmp.set("v.accountname", currentTabNumber);
        var currentAccount = event.getParam("activeAcc");
        console.log('***INSIDE currentAccount**' + currentAccount.Id);
        cmp.set("v.accountId", currentAccount.Id);
        var cmpTarget = cmp.find("changeIt");
        console.log('***cmpTarget**' + cmpTarget);
        $A.util.toggleClass(cmpTarget, "toggle");

    }
})