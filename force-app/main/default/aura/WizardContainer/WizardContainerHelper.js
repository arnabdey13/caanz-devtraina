({
    loadChild : function(cmp, position) {
        var pageList = cmp.get("v.pageList");
        var children = cmp.get("v.children");
        if (false //children[position]
           ) {
            console.log(children[position]);
            cmp.set("v.body", children[position]);
        } else {
            console.log("Load new");
            $A.createComponent(
                pageList[position],
                {},
                function(newItem){
                    if (newItem.isValid()) {
                        cmp.set("v.body", newItem);
                        children[position] = newItem;
                        cmp.set("v.children", children);
                        cmp.set("v.currentPosition", position);
                    } else {
                        console.log("Invalid item widget!");
                    }
                }
            );  	
        }
    },
    nextChildPosition : function(cmp, currentChild) {
        return cmp.get("v.currentPosition")+1;
    },
    prevChildPosition : function(cmp, currentChild) {
        return cmp.get("v.currentPosition")-1;
    }
})