({
    doInit : function(cmp, event, helper) {
        var pageList = cmp.get("v.pages").split(",");
        var currentChild = 0;        
        cmp.set("v.pageList", pageList);        
        helper.loadChild(cmp, currentChild);        
    },
    handleTransition : function(cmp, event, helper) {
        //console.log(event);
        var newPosition;
        if (event.getParam("step") == "next") {
            newPosition = helper.nextChildPosition(cmp, event.getParam("source"))
        } else if (event.getParam("step") == "prev") {
            newPosition = helper.prevChildPosition(cmp, event.getParam("source"));                    
        }        
        helper.loadChild(cmp, newPosition);                    
        var appEvent = $A.get("e.c:WizardPositionEvent");
        appEvent.setParams({position: newPosition}).fire();
    }
})