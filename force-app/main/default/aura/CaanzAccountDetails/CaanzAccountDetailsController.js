({
	doInit: function(cmp, event, helper) {
		helper.getAccountData(cmp);
	},
	handleShowModal: function(cmp, event, helper) {
		var data = cmp.get("v.data");
		var configs = [
            ['c:CaanzPersonAccountForm', {
            	mode: "edit",
                user: cmp.getReference("v.data.user"),
            	account: cmp.getReference("v.data.account"),
            	contact: cmp.getReference("v.data.contact"),
            	accountFields: data.accountFields,
            	contactFields: data.contactFields 
            }],
            ['c:CaanzccModalFooter', {}],
        ];
        $A.createComponents(configs, function(components, status) {
            if (status === "SUCCESS") {
                helper.showModal(cmp, components[0], components[1]);
            }
        });
    }
})