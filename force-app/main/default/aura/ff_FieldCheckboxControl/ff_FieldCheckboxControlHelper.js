({
    validateCheckbox: function (component, event) {

        if(component.get("v.isAlwaysRequired") && component.get("v.visible")){
            var val = component.get("v.value");
            if ($A.util.isUndefinedOrNull(val) || !val) {
                return ["This field is required"];
            } else {
                return [];
            }
        } else {
            return [];
        }
    }
})