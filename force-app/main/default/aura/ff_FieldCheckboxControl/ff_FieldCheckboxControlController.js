({
    doInit: function (component, event, helper) {
        var trueValue = component.get("v.truePicklistValue");
        var falseValue = component.get("v.falsePicklistValue");
        if ($A.util.isEmpty(trueValue) && $A.util.isEmpty(falseValue)) {
            component.set("v.isPicklistData", false);
        } else {
            component.set("v.isPicklistData", true);
        }
    },
    handleClick: function (component, event, helper) {
        if (component.get("v.isPicklistData")) {
            component.set("v.valueOverride", !component.get("v.valueOverride"));
            var trueValue = component.get("v.truePicklistValue");
            var falseValue = component.get("v.falsePicklistValue");
            if (component.get("v.valueOverride")) {
                component.set("v.value", trueValue);
            } else {
                component.set("v.value", falseValue);
            }
        } else {
            component.set("v.value", !component.get("v.value"));
        }
    },
    handleLifeCycle: function (component, event, helper) {
        switch (event.getParam("type")) {
            case "load-field":
                if (component.get("v.isPicklistData")) {
                    var trueValue = component.get("v.truePicklistValue");
                    if (component.get("v.value") == trueValue) {
                        component.set("v.valueOverride", true);
                    } else {
                        component.set("v.valueOverride", false);
                    }
                }
        }
    },
    validateCheckbox: function (component, event, helper) {
        event.stopPropagation(); // since handling it's own event, nobody else cares
        var dateSpecificErrors = helper.validateCheckbox(component, event);
        component.set("v.specificValidationErrors", dateSpecificErrors);
    },
    handleValueChange: function (component, event, helper) {
        if ($A.util.isEmpty(component.get("v.value"))) {
            component.set("v.valueOverride", false);
        }
    }
})