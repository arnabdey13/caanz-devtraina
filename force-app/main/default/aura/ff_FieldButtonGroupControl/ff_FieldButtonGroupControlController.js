({
    load : function(component, event, helper) {
        var readData = event.getParam("value").data;
        var loadKey = component.get("v.loadKey");
        var field = component.get("v.field");
        var records = readData.records[loadKey];
        if(records.length > 0){
            var object = records[0].sObjectList[0];
            component.set("v.selected", object[field]);
        }
        var optionsKey = component.get("v.optionsKey");
        var wrappers = readData.wrappers[optionsKey];
        if (wrappers && !$A.util.isEmpty(wrappers)) {
            component.set("v.options", wrappers);
        }
    }
})