({
    fireCancel: function(component) {
        component.getEvent("crudEvent")
            .setParams({
                operation: "CANCEL"
            })
            .fire();
    }
})