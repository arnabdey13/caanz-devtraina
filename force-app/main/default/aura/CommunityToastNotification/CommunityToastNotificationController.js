({
    showToast : function(component, event, helper) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
        
       "type": "Warning",
        "title": "Awaiting Confirmation",
        "message": "You have new CPD hours awaiting your confirmation.Click Here to update",
        messageTemplate: 'You have new CPD hours awaiting your confirmation.To update {1}',
         messageTemplateData: ['CPD', {
            url: '/cpd-summary-list',
            label: 'Click Here'
            }],
           duration:'5000',
           key: 'alt',
           mode: 'sticky' 
    });
    toastEvent.fire();
},
	myAction : function(component, event, helper) {
		
	}
})