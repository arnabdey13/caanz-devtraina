({
    loadControl : function(component, event, helper) {
        var j = JSON.stringify(event.getParam("data"), function(key, value) {
           if (key == "User.Countries") {
               return value.length + " Countries redacted";
           } else {
               return value;
           }
        });
        component.set("v.json", j);
    }
})