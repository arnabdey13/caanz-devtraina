({
    doInit : function(component, event, helper) {
        component.set("v.dependencyValues1",{"expanded" : true});
    },
	toggle : function(component, event, helper) {
		var value = component.get("v.value");
        var currentlyExpanded = (value == "expanded");
        component.set("v.value", currentlyExpanded?'':'expanded');
        helper.fireDependencyEventIfRequired(component, 1);
        if (!currentlyExpanded && component.get("v.hideOnExpand")) {
            component.set("v.hidden", true);
            // suppress further handling of dep viz events
            component.set("v.dependencyKeysToHandle",{});
        }
	}
})