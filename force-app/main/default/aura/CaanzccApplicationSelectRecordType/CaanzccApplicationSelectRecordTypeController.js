({
    doInit: function(component, event, helper){
        component.set("v.delimiter", "&");
    },
    showModalBox: function(component, event, helper) {
        //alert('applicationselectrecordtype:showModalBox');
        $A.log('applicationselectrecordtype:showModalBox');
        component.set("v.isVisible",true);
    },
    
    hideModalBox: function(component, event, helper){
        //alert('applicationselectrecordtype:hideModalBox');
        $A.log('applicationselectrecordtype:hideModalBox');
        component.set("v.isVisible",false);
    },
    
    selectRecordType: function(component, event, helper){
        //alert('applicationselectrecordtype:selectRecordType');
        $A.log('applicationselectrecordtype:selectRecordType');
        //to be completed
    },

    getRecordTypes : function(component, event, helper) {
        var action = component.get("c.getApplicationRecordTypeNames");

        action.setParams({excludeProvAppIfDraft: component.get("v.enableNewProvAppLink"),
                          excludeIPPAppIfDraft: component.get("v.enableNewIPPAppLink")
                         })
        action.setCallback(this, function(a){
            
        	var rtnValue = a.getReturnValue();
            //alert('applicationselectrecordtype:getRecordTypes:rtnValue='+rtnValue);
            $A.log('rtnValue='+rtnValue);
            if (rtnValue !== null) {
                component.set('v.recordTypeNames',rtnValue);
            }
        });
        $A.enqueueAction(action);		

    }
})