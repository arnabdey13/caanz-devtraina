({
    handleClick: function(component, event, helper) {
        component.getEvent("crudEvent")
            .setParams({
                operation: "UPDATE",
                id: component.get("v.id")
            })
            .fire();
    }
})