({
    doInit : function(cmp, event, helper) {
        var current = cmp.get("v.active");
        var currentlink = cmp.get("v.link");
        //setting the default tab as ACTIVe tab.
        cmp.set("v.link", currentlink);
        if(current){
            cmp.set("v.stateClass", "slds-tabs--default__item  slds-text-heading--label slds-active"); 
            cmp.set("v.ariaselected", true);
            cmp.set("v.tabindex", 0); 
        }else{
            cmp.set("v.stateClass", "slds-tabs--default__item slds-text-heading--label");
            cmp.set("v.ariaselected", false);
            cmp.set("v.tabindex", -1);
        }    
    },
    //Function used on onClick of tab links.
    fireEvent : function(cmp, event, helper) {
        helper.fireEventHelper(cmp,  cmp.get("v.index"));
    },
    
    //Function used for updating the style of selected tab as ACTIVE.
    doUpdate : function(cmp, event, helper) {
        helper.calcStyle(cmp);
    }
})