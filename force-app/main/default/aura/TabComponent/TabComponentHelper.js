({
    //Calculates the style of Tabs and sets the ACTIVE tab as highlighted in red color.
    calcStyle : function(cmp) {
        var active = cmp.get("v.active");
        var styles = "slds-tabs--default__item slds-text-heading--label";
        if (active) {
            styles += " slds-active";
        }
        cmp.set("v.stateClass", styles);
    },
    
    //Fires the Component level event to set the Tab as Active.
    fireEventHelper : function(cmp, indexNum){
        var sm = cmp.find("stateManager");
        sm.get("v.setter")("flow","tab");
        var cmpEvent = cmp.getEvent("settingSelectedTab");
        cmpEvent.setParams({
            "activeTab": indexNum
        });
        cmpEvent.fire();
    }
})