({
    getQuestionnaire: function(component, event, helper) {
        var action = component.get("c.fetchQPRQuestionnaire");
        if (component.get("v.questionnairesStatuses") != null && component.get("v.questionnairesStatuses") != '') {
            action.setParams({
                "questionnaireStatus": component.get("v.questionnairesStatuses")
            })
        }
        action.setCallback(this, function(a) {
            var rtnValue = a.getReturnValue();
            $A.log('rtnValue=' + rtnValue);
            if (rtnValue !== null) {
                component.set('v.questionnaires', rtnValue);
            }
        });
        $A.enqueueAction(action);
    },

    // Navigate to Questionnaire detail on button click in Questionnaires list
    gotoQuestionnaire: function(component, event, helper) {
        var idx = event.target.id;
        var orgURL = $A.get("$Label.c.Org_URL");
        var commnunityPrefix = $A.get("$Label.c.Community_Prefix_CCH");
        var userAgent = window.navigator.userAgent;
       
        // To check if call is from any version of IE
        if (userAgent.indexOf("MSIE ") > -1 || userAgent.indexOf("Trident/") > -1 || userAgent.indexOf("Edge/") > -1){
         window.location.href  = orgURL+'/'+commnunityPrefix+'/' + idx +"/e";   
        }else{
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
            "url": orgURL+'/'+commnunityPrefix+'/' + idx +"/e"        
        });
        urlEvent.fire();
        }
        /*
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
           // "url": "/detail/" + idx
      //      "url": "https://preprod-charteredaccountantsanz.cs57.force.com/MyCA/qprquestionnaireoverride?id=" + idx +"&mode=edit"
           // "url": "https://devtraina-charteredaccountantsanz.cs6.force.com/MyCA/" + idx +"/e"
            "url": orgURL+'/'+commnunityPrefix+'/' + idx +"/e"
        //    "url": " https://devtraina-charteredaccountantsanz.cs6.force.com/MyCA/s/qpr-questionnaire/" + idx +"/e"
  //        "url": "https://devtraina-charteredaccountantsanz.cs6.force.com/MyCA/qprquestionnaireoverride?id="+idx+"&mode=edit"            
        });
        urlEvent.fire();
        */
          
    }
})