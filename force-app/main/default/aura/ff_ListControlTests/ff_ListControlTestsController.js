({
    doInit: function (component, event, helper) {

        var pause = 2000;

        var test = helper.driver(component, event, helper);

        // references to all the components being tested
        var formDriver = component.find("driver");
        var campaignList = component.find("list1");
        var contactList = component.find("list2");

        var kevinsLastName = "Bacon-" + new Date().getTime();
        var dirtyBeauden = {
            attributes: {type: "Contact"},
            FirstName: "Beauden",
            LastName: "Barrett",
            Department: null
        };
        var accountId, contactId, contactTempId1, contactTempId2, campaignMemberTempId1, firstContact;

        test.start({
            focused: contactList,
            description: "initial focus on contacts to put it in edit mode"
        })
            .then(test.wait(function () {
                // setup shared test values
                var rd = formDriver.get("v.readData");
                accountId = rd.records.Account[0].sObjectList[0].Id;

                // store the first contact id from the SECOND list in the Contact wrappers since the sample form uses 2 Contacts lists
                // IMPORTANT: this test will fail if there are no Contacts present. The SampleDriverTests will create a Contact so run that if you need to.
                contactId = rd.records.Contact[1].sObjectList[0].Id;
            }))

            //////////// CONTACT LIST: lazy, form below

            .then(test.sleep(pause)) // to see the list before adds start

            /////// add kevin

            .then(test.wait(function () {
                // use clicks Add button, the list control fires a SetDriverFocus event to focus on a new Contact
                contactList.addRecord();
                // user enters data into forms
                contactList.find("firstName").set("v.value", "Kevin");
                contactList.find("title").set("v.value", "Chief Introduction Officer");
                contactList.find("dob").set("v.value", "1999-12-31");
                contactList.find("salutation").set("v.value", "Mr.");
                contactList.find("department").set("v.value", "Athletics");
            }))
            .then(test.whenDone(function (context, done) {
                // user searches for employer and opts out
                // name is after WKRP so that it does not affect finding the same account each in the load
                contactList.find("employer").set("v.searchText", "X-Prize Inc");
                contactList.find("employer").requestMatches(done);
            }))
            .then(test.wait(function () {
                contactList.find("employer").optOut();
            }))

            .then(test.assertEquals(true, "v.isFormEditingNewRecord", "form knows it's editing a new record"))
            .then(test.wait(function () {
                contactList.find("save").click();
            }))
            .then(test.assertEquals(true, "v.editMode", "form still showing because not all fields are valid"))

            // note: user could send record using driver buttons in this state. using a modal editor avoids this

            .then(test.wait(function () {
                // user fixes invalid fields in the form
                contactList.find("lastName").set("v.value", kevinsLastName);
                contactList.find("save").click();
            }))
            .then(test.assertEquals(false, "v.editMode", "form not showing because optimistic save started"))

            .then(test.assertEquals({
                attributes: {type: "Contact"},
                FirstName: "Kevin",
                LastName: kevinsLastName,
                Title: "Chief Introduction Officer",
                Birthdate: "1999-12-31",
                Salutation: "Mr.",
                Department: "Athletics"
            }, "v.values[0]", "kevin is visible first in the list control after save"))
            .then(test.sleep(pause)) // to see the optimistically added record in the list and the callback remove the spinner

            .then(test.wait(function () {
                // contact multi-record list is the second sobject wrapper list, hence position 1
                firstContact = formDriver.get("v.readData").records.Contact[1].sObjectList[0];
            }))

            /////// edit kevin

            .then(test.wait(function () {
                // use clicks Edit link, the list control fires a SetDriverFocus event to focus on the existing
                contactList.editRecord(firstContact.Id);
                // user enters data into forms
                contactList.find("title").set("v.value", "CIO");
            }))
            .then(test.assertEquals(false, function () {
                return $A.util.isEmpty(contactList.find("employer").get("v.value"));
            }, "auto-complete is loaded, even for a record that was just created"))
            .then(test.assertEquals(false, "v.isFormEditingNewRecord", "form knows it's editing an existing record"))
            .then(test.assertEquals(true, function () {
                return contactList.find("department").get("v.visible");
            }, "dependent viz field is displayed "))
            .then(test.assert(function () {
                var dirty = formDriver.get("v.writeData").records[firstContact.Id];
                //console.log("dirty kevin after edit: " + JSON.stringify(dirty));
                return JSON.stringify(dirty) == JSON.stringify({
                        "Id": firstContact.Id,
                        "attributes": {
                            "type": "Contact"
                        },
                        "Title": "CIO"
                    });
            }, "dirty record in driver has changed title"))
            .then(test.sleep(pause))
            .then(test.wait(function () {
                contactList.find("save").click();
            }))
            // TODO assert optimistic update of kevins job in UI
            // TODO whenDone on save
            // TODO assert clean driver
            // TODO edit again, clear a required field and check blocked save
            // TODO cancel and assert clean driver

            /////// delete kevin

            .then(test.sleep(pause))
            .then(test.whenDone(function (context, done) {
                // click delete on first row
                var contacts = contactList.get("v.values");
                var c1 = contacts[0];
                contactList.deleteRecord(c1.Id, done); // blocks till apex delete complete
            }))
            .then(test.assert(function () {
                return contactList.get("v.deleteResults").success;
            }, "server delete returned no errors"))
            .then(test.assert(function () {
                return firstContact != formDriver.get("v.readData").records.Contact[1].sObjectList[0];
            }, "kevin was removed from readData")) // so that any EventUpdateControl doesn't re-add him back later

            .then(test.sleep(pause))
            .then(test.wait(function () {
                contactList.addRecord();
                contactList.find("firstName").set("v.value", "Beauden");
                contactList.find("lastName").set("v.value", "Barrett");
            }))
            .then(test.assertEquals(dirtyBeauden, function () {
                contactTempId2 = formDriver.get("v.writeData").focusId;
                return formDriver.get("v.writeData.records")[contactTempId2];
            }, "another contact with fields changed are in driver dirty data"))

            //// fake an autocomplete opt-in value change event with a custom nested parent relationship
            //// as it if was a field in the contact editor. TODO add UI for this instead of firing event
            //// leaving this commented out for now as it breaks the server save but is useful for testing v.parentRecordTypes in ff_Driver
            // .then(test.wait(helper.fireApplicationEvent("e.c:ff_EventValueChange", {
            //     loadKey: "Other_Account__r", // will create a nested object
            //     field: "Id", // and will set the id of the nested object
            //     value: "001adflkjljasf",
            //     valid: true,
            //     source: component,
            //     ignore: false
            // })))
            // .then(test.assertEquals({
            //     attributes: {type: "Account"},
            //     Id: "001adflkjljasf"
            // }, function () {
            //     return formDriver.get("v.writeData.records")[contactTempId2]["Other_Account__r"];
            // }, "the nested Account has correct type and Id. Type was mapped from loadKey by SampleDriver"))

            .then(test.sleep(pause)) // to see the two adds during the test
            .then(test.wait(function () {
                contactList.find("save").click();
            }))
            .then(test.assertEquals(dirtyBeauden, "v.values[0]", "beauden is visible first in the list control after save"))

            // delete Beauden
            .then(test.sleep(pause))
            .then(test.whenDone(function (context, done) {
                // click delete on first row
                var contacts = contactList.get("v.values");
                var c1 = contacts[0];
                contactList.deleteRecord(c1.Id, done); // blocks till apex delete complete
            }))

            .then(test.sleep(pause)) // to allow save callback to complete before the focus below is used for campaign inserts
            // TODO use whenDone to wait for this

            //////////// CAMPAIGN LIST : eager, inline using auto-completes (some ignored) and file-uploads

            .then(test.focus(campaignList, "checking eager create flow"))

            .then(test.assertEquals([campaignList.get("v.minimumErrorMessage")], "v.validationErrors",
                "Campaign list is invalid after load because only 1 record"))

            .then(test.wait(function () {
                // TODO are api fns inherited? if so, the validation api fns can be move from FieldControl and ListControl into Control
                campaignList.setValidation(true, false);
                console.log(formDriver.get("v.formValidity"));
            }))
            .then(test.assertEquals(false, function () {
                var v = formDriver.get("v.formValidity");
                return v["no-stage"]["CampaignMember.LIST-0"];
            }, "the campaign list reports invalid because it has 1 row but requires 2"))

            .then(test.focus(campaignList, "checking eager create flow"))
            .then(test.assertEquals(false, "v.editMode", "List controls are not in edit mode right after load"))
            .then(test.wait(function () {
                // use clicks Add button, the list control fires a SetDriverFocus event to focus on a new Campaign Member
                campaignList.addRecord();
            }))

            .then(test.focus(formDriver, "checking dirty data in driver when using ignored autocomplete"))
            .then(test.assertEquals("INSERT-CampaignMember", function () {
                var wd = formDriver.get("v.writeData");
                campaignMemberTempId1 = wd.focusId;
                return wd.focusId.substring(0, 21);
            }, "driver focus is now on a Campaign Member temp id"))
            .then(test.assertEquals({attributes: {type: "CampaignMember"}}, function () {
                var wd = formDriver.get("v.writeData");
                return wd.records[campaignMemberTempId1];
            }, "driver focus is on a new empty Campaign Member record"))

            // TODO change the value of the first name field from the primary Contact and ensure that all focus
            // is restored and that the update went to the right place

            // user enters search term in first autocomplete and chooses option
            .then(test.wait(function () {
                // user chose account id (same as loaded account but could be any)
                campaignList.find("accountSearch").set("v.value", accountId);
            }))
            .then(test.assertEquals({attributes: {type: "CampaignMember"}}, function () {
                var wd = formDriver.get("v.writeData");
                return wd.records[campaignMemberTempId1];
            }, "driver recorded no changes to writeData for the ignored field"))

            .then(test.sleep(1000)) // allow change of disabled to be visible during test

            .then(test.assert(function () {
                var context = campaignList.find("contactSearch").get("v.searchContext");
                // compare strings to avoid locker service proxy object equality checks
                return JSON.stringify(context) == JSON.stringify({accountId: accountId});
            }, "second auto-complete has value from first auto-complete in it's search context"))

            .then(test.wait(function () {
                //console.log("choosing contact: " + contactId);
                // user chose contact id using second auto-complete
                campaignList.find("contactSearch").set("v.value", contactId);
                // TODO find a way to use a callback for this so that tests block until after the apex callback that this fires
            }))
            .then(test.assert(function () {
                var dirtyCampaignMember = formDriver.get("v.writeData").records["INSERT-CampaignMember1"];
                return dirtyCampaignMember.ContactId == contactId;
            }, "driver has recorded the value change from the cascaded auto-complete"))

            // TODO when second value is changed, list control eagerly creates and afterwards the new record is present in v.values

            // TODO go into edit mode again but opt out of first a/c. enter a value
            // TODO driver should have new dirty record with text entered
            // TODO second a/c still searchable, choose a match here

            // TODO same opt-out test for a/c1 but also opt out of a/c2

            // TODO driver saves all non-eager records and returns id translations for all records
            // .then(test.whenDone(function (context, done) {
            //     formDriver.apiSendDirty
            // }))

            //////////// END OF TESTS ////////////

            // always include these fns to handle the end of the test
            .then(test.pass).catch(test.fail);

    }
})