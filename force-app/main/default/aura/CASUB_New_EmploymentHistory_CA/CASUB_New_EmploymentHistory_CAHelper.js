({
    loadCurrentUserMemberDetail : function(component){
        var currentUserMemberDetail = component.get('v.currentUserMemberDetail');
        
        component.set("v.userMemberOf", currentUserMemberDetail.Member_Of__c);
        component.set("v.personAccountId", currentUserMemberDetail.Id);
        component.set("v.memberHasCPP", currentUserMemberDetail.Has_CPP_in_AU_and_or_NZ__c);
        component.set("v.countPrimaryEmp", currentUserMemberDetail.Primary_Employer_EH_Count__c);
        component.set("v.countCPPEmploymentHistory", currentUserMemberDetail.CPP_Employment_History_Count_Member__c);
        /* Bug Fix - user story - 425 - replaced '!component.get("v.isEdit")' with 'component.get("v.isEdit") == true' */
        if(component.get("v.countPrimaryEmp") != null && component.get("v.countPrimaryEmp") > 0 && component.get("v.isEdit") == true){
            component.set("v.isPrimaryChecked","true");
            var primaryField = component.find("primaryemp");
            primaryField.setCustomValidity("You have an existing Primary Employer.");
            primaryField.reportValidity();
        }
    },
    validateStartDate : function(component, event){
        var startDate = component.get("v.simpleNewEmploymentHistory.Employee_Start_Date__c");
        var endDate = component.get("v.simpleNewEmploymentHistory.Employee_End_Date__c");
        var empstartdateField = component.find("empstartdateField");
        var empenddateField = component.find("empenddateField");
        if(startDate != null && startDate != undefined && startDate != '' &&
           endDate != null && endDate != undefined && endDate != '' && startDate > endDate) {
            empstartdateField.setCustomValidity("Start Date can not be greator then End Date.");
            empstartdateField.reportValidity();
        }else{
            console.log('in else validity');
            empstartdateField.setCustomValidity("");
            /* Commented by Sumit to Fix start Date issue */
            //empstartdateField.reportValidity();
            empenddateField.setCustomValidity("");
            empenddateField.reportValidity();
        }
    },
    validateEndDate : function(component, event) {
        var startDate = component.get("v.simpleNewEmploymentHistory.Employee_Start_Date__c");
        var endDate = component.get("v.simpleNewEmploymentHistory.Employee_End_Date__c");
        var empstartdateField = component.find("empstartdateField");
        var empenddateField = component.find("empenddateField");
        if(endDate != null && endDate != undefined) {
            if(startDate == null || startDate == undefined || startDate == ''){
                empstartdateField.setCustomValidity("Start Date require to fill End Date.");
                empstartdateField.reportValidity();
            }else if(startDate != null && startDate != undefined && startDate != '' &&
                endDate != '' && startDate > endDate) {
                empenddateField.setCustomValidity("End Date can not be less then Start Date.");
                empenddateField.reportValidity();
            }else{
                empstartdateField.setCustomValidity("");
            	empstartdateField.reportValidity();
                empenddateField.setCustomValidity("");
                empenddateField.reportValidity();
            }
        }
    },
    //Function used to load Employment History details while Edit.
    getEmployerDetails: function(component, empid) {
        
        var params = {"empId" : empid };
        this.enqueueAction(component, "getEmpRecordDetail", params, function(data){
            component.set("v.simpleNewEmploymentHistory", data);
           if(component.get("v.simpleNewEmploymentHistory.Primary_Employer__c")){
            component.set("v.isPrimaryChecked",component.get("v.simpleNewEmploymentHistory.Primary_Employer__c"));
          }
        if(component.get("v.simpleNewEmploymentHistory.Is_CPP_Provided__c")){
               component.set("v.selectedCPPValue","Yes");
         }else{
               component.set("v.selectedCPPValue","No");
        }
        });
    },
    checkPrimaryEmpDuplicate : function(component){
        var primaryField = component.find("primaryemp");
        primaryField.setCustomValidity('');
        primaryField.reportValidity();
        if(component.get("v.isPrimaryChecked")){
            var checkboxValue = component.get("v.isPrimaryChecked");
            var personAccountId = component.get("v.personAccountId");
            //var action = component.get("c.checkPrimaryEmpDuplicate");
            var params = { 
				"personAccountId": component.get("v.personAccountId")
            };
            this.enqueueAction(component, "checkPrimaryEmpDuplicate", params, function(data){
                if(data && data == true){
                        primaryField.setCustomValidity("You have an existing Primary Employer.");
                        primaryField.reportValidity();
                 }
            });
        }
    },
    validateEmployeeHistoryCreateForm: function(component) {
        var isFormValid = true;
        var accountId = component.get("v.accountId");
        var empstartdateField = component.find("empstartdateField");
        var empenddateField = component.find("empenddateField");
        var formFieldArray=['empstartdateField','empenddateField','primaryemp','newempjobtitle']
        for(var index=0;index<formFieldArray.length;index++){
            var formComponentAuraId = formFieldArray[index];
                var isDefined = !$A.util.isUndefined(component.find(formComponentAuraId));
            if(isDefined){
             var formComponent = component.find(formComponentAuraId);
                formComponent.showHelpMessageIfInvalid();
                if(!formComponent.get('v.validity').valid){
                    isFormValid = false;
                    formComponent.focus();
                    break;
                }
            }
            
        }
        if($A.util.isEmpty(component.get("v.accountId")) && !component.get("v.isEdit")){
             isFormValid = false;
             component.set("v.isEmploymentBlank","true");
             component.find("errorDiv").focus();
            
        }
        return isFormValid;
    },
    enqueueAction: function(cmp, method, params,callback){
		this.toggleSpinner(cmp, true);
		var action = cmp.get("c." + method); 
		if(params) action.setParams(params);
		action.setCallback(this, function(response){
			if(response.getState() === "SUCCESS") {
				if(callback) callback.call(this, response.getReturnValue());
	        } else if(response.getState() === "ERROR") {
	        	cmp.set("v.errorMessage", JSON.stringify(response.getError()));
	        	console.error(response.getError());
	        }
	        this.toggleSpinner(cmp, false);
		});
		$A.enqueueAction(action);
	},
	toggleSpinner: function(cmp, show) {
		var spinner = cmp.find("spinner");
		$A.util.addClass(spinner, show ? "slds-show" : "slds-hide");
		$A.util.removeClass(spinner, !show ? "slds-show" : "slds-hide");
	},
    handleCreateEmploymentHistory: function(component) {
        if(this.validateEmployeeHistoryCreateForm(component)) {
            debugger;
            if(!component.get("v.isEdit")){
               component.set("v.simpleNewEmploymentHistory.Employer__c", component.get("v.accountId"));
               component.set("v.simpleNewEmploymentHistory.Primary_Employer__c",component.get("v.isPrimaryChecked"));
                var params = { 
                    "newAcc": component.get("v.simpleNewEmploymentHistory"),
                    "accId" : component.get("v.accountId")
                };
                this.enqueueAction(component, "getAccountupdatedlist", params,function(data){
                    component.set("v.showSpinner", false);
                    this.handleClose(component);
                    var appEvent = $A.get("e.c:CASUB_UpdateEmploymentHistory_CA"); 
                    appEvent.setParams({"updateEmploymentHistory" : true}); 
                    appEvent.fire();
                    //$A.get('e.force:refreshView').fire();
                });
            }else{
                component.set("v.simpleNewEmploymentHistory.Primary_Employer__c",component.get("v.isPrimaryChecked"));
                /*if(component.get("v.isPrimaryChecked")){
                    component.set("v.simpleNewEmploymentHistory.Primary_Employer__c",component.get("v.isPrimaryChecked"));
                }*/
                var params = { 
                    "updateAcc": component.get("v.simpleNewEmploymentHistory")
                };
                this.enqueueAction(component, "updateEmpRecord", params,function(data){
                    component.set("v.showSpinner", false);
                    var appEvent = $A.get("e.c:CASUB_UpdateEmploymentHistory_CA"); 
                    appEvent.setParams({"updateEmploymentHistory" : true}); 
                    appEvent.fire();
                    //$A.get('e.force:refreshView').fire();
                    this.handleClose(component);
                });
               
                
            }
            
        }
    },
    handleClose : function(component) {	
        //Closing the Modal Window
         component.find("overlayLib").notifyClose();
		
	}
})