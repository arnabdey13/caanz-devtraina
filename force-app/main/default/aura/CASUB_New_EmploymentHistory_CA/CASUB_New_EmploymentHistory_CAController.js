({
    doInit : function(component, event, helper) {
        var isEditOrNewEmploymentRequest = component.get("v.mode");
        console.log(component.get("v.employmentId"))
        if(isEditOrNewEmploymentRequest != 'newEmployment'){
            component.set("v.isEdit","true");
            var empid = component.get("v.employmentId");
           helper.getEmployerDetails(component, empid);
           helper.loadCurrentUserMemberDetail(component);
        }else{
            console.log('in else');
            component.find("employmentHistoryRecordCreator").getNewRecord(
            "Employment_History__c", // objectApiName
            null, // recordTypeId
            false, // skip cache?
            $A.getCallback(function() {
                var rec = component.get("v.newEmploymentHistory");
                var error = component.get("v.newEmploymentHistoryError");
                component.set("v.changeitClass", "slds-hide");
                var currentUserMemberDetail = component.get('v.currentUserMemberDetail');
                helper.loadCurrentUserMemberDetail(component);
                if(error || (rec === null)) {
                    console.log("Error initializing record template: " + error);
                }
                else {
                    console.log("Record template initialized: " + rec.apiName);
                }
            })
        );
        }
       
    },
    //New Employer - Function used to perform auto complete for Employer field.
    searchKeyChange: function(component, event) {
        component.set("v.changeitClass", "slds-hide");
        component.set("v.isEmploymentBlank","false");
        component.get("v.accounts",[]);
        var searchKey = event.getParam("searchKey");
        if($A.util.isEmpty(searchKey)){
            component.set("v.changeitClass", "slds-hide");
            
        }else{
            var action = component.get("c.findByName");
            action.setParams({
                "searchKey": searchKey,
                "memberOfValue": component.get("v.userMemberOf")
            });
            action.setCallback(this, function(a) {
                component.set("v.accounts", a.getReturnValue());
                if(component.get("v.accounts").length==0){
                    component.set("v.changeitClass", "slds-hide");
                }else{component.set("v.changeitClass", "slds-show");}
            });
            $A.enqueueAction(action);
        }
        
        
    },
    // on change of the CPP picklist (new)
    onNewCPPSelectChange : function(component, event, helper) {
        var selectedCPPValue = component.find("cppPicklist").get("v.value");
        var memberHasCPP = component.get("v.memberHasCPP");
        var cppField = component.find("cppPicklist");
        if(selectedCPPValue == null || selectedCPPValue == "" || selectedCPPValue == "No"){
            component.set("v.simpleNewEmploymentHistory.Is_CPP_Provided__c", false);
        }else if(selectedCPPValue == 'Yes'){
            component.set("v.simpleNewEmploymentHistory.Is_CPP_Provided__c", true);
        }
    },
    //New Employer - Function fired on handling of Auto Complete for Employer field. Used to hide the list of Employers.
    parentHandling : function(cmp, event) {
        var currentAccName = event.getParam("accname");
        cmp.set("v.accountname", currentAccName);
        var currentAccount = event.getParam("activeAcc");
        cmp.set("v.accountId", currentAccount.Id);
        cmp.set("v.changeitClass", "slds-hide");
    },
    checkForStartDate : function(component, event, helper) {
        helper.validateStartDate(component, event);
    },
    
    checkForEndDate : function(component, event, helper) {
        helper.validateEndDate(component, event);
    },
    //Function used to validate Primary Employer duplication
    checkPrimaryEmployerDeDupe : function(component, event, helper){
        var isPrimaryChecked = component.get("v.isPrimaryChecked");
        /* Bug Fix - user story - 425 - Added condition '(isPrimaryChecked && component.get("v.mode") == 'newEmployment')' to fire validation for checkbox when new employee pop up is opened up. */
        if((!component.get("v.simpleNewEmploymentHistory.Primary_Employer__c") && isPrimaryChecked)
            || (isPrimaryChecked && component.get("v.mode") == 'newEmployment')){
           helper.checkPrimaryEmpDuplicate(component);
        }
        else{
            var primaryField = component.find("primaryemp");
            primaryField.setCustomValidity("");
            primaryField.reportValidity();
        }
    },
    handleCreateEmploymentHistoryRecord : function(component, event, helper){
        //helper.handleCreateEmploymentHistory(component);
        helper.handleCreateEmploymentHistory(component);
    },
    handleRecordUpdated: function(component, event, helper){
        
    },
    handleCancel : function(component, event, helper) {
		component.find("overlayLib").notifyClose();
	}
})