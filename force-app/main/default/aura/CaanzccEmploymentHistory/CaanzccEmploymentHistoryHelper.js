({
    //Function used to load default Employment History existing records.
    loadMethod : function(component) {
        var action = component.get("c.EmpList");
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                component.set("v.accountlist", a.getReturnValue());
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
            }
        });
        $A.enqueueAction(action);
    },
    //Function used to load logged in User details.
    loadCurrentUserMemberDetail : function(component){
        var action = component.get("c.returnCurrntUserMember");
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                component.set("v.userMemberOf", a.getReturnValue().Member_Of__c);
                component.set("v.personAccountId", a.getReturnValue().Id);
                component.set("v.memberHasCPP", a.getReturnValue().Has_CPP_in_AU_and_or_NZ__c);
                component.set("v.countPrimaryEmp", a.getReturnValue().Primary_Employer_EH_Count__c);
                
                if(a.getReturnValue().Has_CPP_in_AU_and_or_NZ__c){
                	component.set("v.showHideItClass", "slds-show");
                }
                else{
                    component.set("v.showHideItClass", "slds-hide");    
                }
                
                if(a.getReturnValue().CPP_Employment_History_Count_Member__c == 0 && a.getReturnValue().Has_CPP_in_AU_and_or_NZ__c){
                	component.set("v.showHideWarningClass", "slds-show");    
                }
                else{
                    component.set("v.showHideWarningClass", "slds-hide");    
                }
                if(a.getReturnValue().Primary_Employer_EH_Count__c != null && a.getReturnValue().Primary_Employer_EH_Count__c > 0){
                    var peCheckbox = component.find("primaryemp");
                    peCheckbox.set("v.errors", [{message:"You have an existing Primary Employer."}]);
                }
                
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
            }
        });
        $A.enqueueAction(action);
    },
    //Function used to load Employment History details while Edit.
    getEmployerDetails: function(component, empid) {
        var action = component.get("c.getEmpRecordDetail");
        action.setParams({
            "empId" : empid
        });
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                component.set("v.editEmployerRec", a.getReturnValue());
                component.find("editselect").set("v.value", a.getReturnValue().Status__c);
                component.find("editselect").set("v.selected", true);
                
                if(a.getReturnValue().Is_CPP_Provided__c){
                	component.find("editcppPicklist").set("v.value", 'Yes');
                }else{
                    component.find("editcppPicklist").set("v.value", 'No');
                }
                component.find("editcppPicklist").set("v.selected", true);
                
                document.getElementById("backGroundSectionId").style.display = "block" ;
                document.getElementById("editEmpSectionId").style.display = "block";
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
            }
        });

        $A.enqueueAction(action);
    },
    //Function used to fetch the picklist values for Status field.
    getStatusPickListValues : function(component){
        var action = component.get("c.getStatusPickListClass");
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                component.set("v.statusvalue", a.getReturnValue());
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
            }
        });

        $A.enqueueAction(action);
    },
    
    getCPPPicklistValues : function(component){
        var action = component.get("c.getCPPPickListClass");
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                component.set("v.providesCPPValues", a.getReturnValue());
                
                // need to default the value from this method, doesn't seem to work when in the controller showModal method
                component.find("cppPicklist").set("v.value", 'Yes');
                component.find("cppPicklist").set("v.selected", true);
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
            }
        });

        $A.enqueueAction(action);
    },

    checkPrimaryEmpDuplicate : function(component, checkBox){
        var checkboxValue = checkBox.get("v.value");
        var personAccountId = component.get("v.personAccountId");
        if(checkboxValue == true){
            var action = component.get("c.checkPrimaryEmpDuplicate");
            action.setParams({
                "personAccountId" : personAccountId
            });
            action.setCallback(this, function(a) {
                if (a.getState() === "SUCCESS") {
                    if(a.getReturnValue() && a.getReturnValue() == true){
                        checkBox.set("v.errors", [{message:"You have an existing Primary Employer."}]);
                    }
                } else if (a.getState() === "ERROR") {
                    $A.log("Errors", a.getError());
                }
            });
            $A.enqueueAction(action);
        }else{
            checkBox.set("v.errors", null);
        }
    },
    
    statusChangedToClosed : function(statusValue, endDateValue, endDateField){       
        if(statusValue == 'Closed' && (endDateValue == null || endDateValue == '')){    
            endDateField.set("v.errors", [{message:"End Date is required when status is 'Closed'."}]);
        }else{
            endDateField.set("v.errors", null);
        }
    },
    toogleActionButtonsVisibility: function(component, enabled){
        var buttons = component.find("action_button");
        if(!$A.util.isEmpty(buttons)){
            for(var i = 0; i < buttons.length; i++){
                buttons[i].getElement().disabled = !enabled;
            }
        }
    }
    
})

// ({
//     //Function used to load Employment History existing records.
//     loadEmploymentHistory : function(component) {
//         var action = component.get("c.EmpList");
//         action.setCallback(this, function(a) {
//             if (a.getState() === "SUCCESS") {
//             	var rtnValue = a.getReturnValue();
//                 var data = this.addUrlsToData(rtnValue);
//             	component.set("v.employmentList", data);
//             } else if (a.getState() === "ERROR") {
//                 $A.log("Errors", a.getError());
//             }
//         });
//         $A.enqueueAction(action);
//     },

//     addUrlsToData: function (data) {
//         for (var i=0; i<data.length; i++) {
//             data[i]["URL"] = '/CustomerHub/s/detail/' + data[i]["Id"]
//         }
//         return data;
//     },
// })