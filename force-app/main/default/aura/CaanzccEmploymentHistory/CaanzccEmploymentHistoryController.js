({
    "doInit": function(component, event, helper) {
        helper.loadMethod(component);
        helper.loadCurrentUserMemberDetail(component);
    },
    //Edit Employer - Function used to check Start Date and End Date. End Date must be greater than Start Date always.
    checkDateVal : function(component){
        var endDateVal = component.get("v.editEmployerRec.Employee_End_Date__c").split('-').join('/');
        var startDateVal = component.get("v.editEmployerRec.Employee_Start_Date__c").split('-').join('/');
        var editEndDate = component.find("enddateField");
        var endDate ;
        var startDate ;
        var isDateRangeValid = true;
        //formating Start and End dates.
        if(endDateVal != null && endDateVal != ''){
            endDate = new Date(endDateVal);
            startDate = new Date(startDateVal);
        }
        //validating Employer End Date against Start Date.
        if(endDate instanceof Date && isFinite(endDate)){
            if(startDate > endDate){
                editEndDate.set("v.errors", [{message:"End date cannot be earlier than start date.  Please amend."}]);
                isDateRangeValid = false;
            }else{
                editEndDate.set("v.errors", null);
            }
        }else{
            editEndDate.set("v.errors", null);
        }
        
        // add validation to ensure end date is populated when status is Closed
        var statusValue = component.find("editselect").get("v.value");
        if(statusValue == 'Closed' && (endDateVal == null || endDateVal == '')){    
            editEndDate.set("v.errors", [{message:"End Date is required when status is 'Closed'."}]);
        }else if(isDateRangeValid){
            editEndDate.set("v.errors", null);
        }
    },
    //New Employer - Function used to check Start Date and End Date. End Date must be greater than Start Date always.
    checkNewEmpDateVal : function(component){
        var endDateVal = component.get("v.newAccount.Employee_End_Date__c").split('-').join('/');
        var startDateVal = component.get("v.newAccount.Employee_Start_Date__c").split('-').join('/');
        var editEndDate = component.find("empenddateField");
        var endDate ;
        var startDate ;
        var isDateRangeValid = true;
        
        //formating Start and End dates.
        if(endDateVal != null && endDateVal != ''){
            endDate = new Date(endDateVal);
            startDate = new Date(startDateVal);
        }
        //validating Employer End Date against Start Date.
        if(endDate instanceof Date && isFinite(endDate)){
            if(startDate > endDate){
                editEndDate.set("v.errors", [{message:"End date cannot be earlier than start date.  Please amend."}]);
                isDateRangeValid = false;
            }else{
                editEndDate.set("v.errors", null);
            }
        }else{
            editEndDate.set("v.errors", null);
        }
        
        // add validation to ensure end date is populated when status is Closed
        var statusValue = component.find("statuspicklist").get("v.value");
        if(statusValue == 'Closed' && (endDateVal == null || endDateVal == '')){    
            editEndDate.set("v.errors", [{message:"End Date is required when status is 'Closed'."}]);
        }else if(isDateRangeValid){
            editEndDate.set("v.errors", null);
        }
    },

    showModalBox: function(component, event, helper) {
        document.getElementById("backGroundSectionId").style.display = "none";
        document.getElementById("newAccountSectionId").style.display = "none";
        document.getElementById("editEmpSectionId").style.display = "none";
        $A.get('e.force:refreshView').fire();
    },
    //New Employer - Function used to save Employment History. Performs client side validations also.
    saveAccount: function(component, event, helper) {
        
        var empJobTitle = component.find("newempjobtitle");
        var empStartDate = component.find("empstartdateField");
        var errorDiv = component.find("errorDiv");
        var accountId = component.get("v.accountId");
        var value = empJobTitle.get("v.value");
        var startDateValue = empStartDate.get("v.value");
        
        var endDateField = component.find("empenddateField");
        var endDateValue = endDateField.get("v.value");
        
        var statusField = component.find("statuspicklist");
        var statusValue = statusField.get("v.value");
        
        var cppField = component.find("cppPicklist");
        var cppValue = cppField.get("v.value");
        
        var memberHasCPP = component.get("v.memberHasCPP");
        
		var endDate ;
		var startDate ;
        
        if(value || accountId || startDateValue || endDateValue || statusValue || cppValue){
            if(accountId){
                component.set("v.errorClass", '');
                component.set("v.errorMsg", "slds-hide");
            }else{
                component.set("v.errorClass", ' has-error');
                component.set("v.errorMsg", "slds-show errormsg");
            }
            if(value){
                empJobTitle.set("v.errors", null);
            }else{
                empJobTitle.set("v.errors", [{message:"Job Title cannot be blank. Fill and Save."}]);
            }
            if(startDateValue){
                empStartDate.set("v.errors", null);
            }else{
                empStartDate.set("v.errors", [{message:"Start Date is required"}]);
            }
			var isDateValid = true;
            
            //formating Start and End dates.
            if(endDateValue != null && endDateValue != ''){
                endDate = new Date(endDateValue);
                startDate = new Date(startDateValue);
            }
            
            //validating Employer End Date against Start Date.
            if(endDate instanceof Date && isFinite(endDate)){
                if(startDate > endDate){
                    endDateField.set("v.errors", [{message:"End date cannot be earlier than start date.  Please amend."}]);
                    isDateValid = false;
                }else{
                    endDateField.set("v.errors", null);
                }
            }else{
                endDateField.set("v.errors", null);
            }            
            
            
            var isStatusValid = true;
            if(statusValue == 'Closed' && (endDateValue == null || endDateValue == '')){
                endDateField.set("v.errors", [{message:"End Date is required when status is 'Closed'."}]);
                isStatusValid = false;
            }
            else if(isDateValid) {
                endDateField.set("v.errors", null);
            }
            
            var isCPPValid = true;
            if(memberHasCPP && (cppValue == null || cppValue == "")){
                cppField.set("v.errors", [{message:"Please select Yes or No."}]);
                isCPPValid = false;
            }
            else{
                cppField.set("v.errors", null);
            }
            
            var isValid = true;
            if(component.find("primaryemp").get("v.errors") != '' && component.find("primaryemp").get("v.errors") != null){
                isValid = false ;
            }
            
            if(accountId && value && startDateValue && isValid && isStatusValid && isCPPValid){
                var action = component.get("c.getAccountupdatedlist");
                action.setParams({
                    "newAcc": component.get("v.newAccount"),
                    "accId" : component.get("v.accountId")
                });
                action.setCallback(this, function(a) {
                    if (a.getState() === "SUCCESS") {
                        document.getElementById("backGroundSectionId").style.display = "none";
                        document.getElementById("newAccountSectionId").style.display = "none";
                        $A.get('e.force:refreshView').fire();
                    } else if (a.getState() === "ERROR") {
                        $A.log("Errors", a.getError());
                    }
                    helper.toogleActionButtonsVisibility(component, true);
                });
                helper.toogleActionButtonsVisibility(component, false);
                $A.enqueueAction(action);
            }
        }else{
            if(!accountId){
                component.set("v.errorClass", ' has-error');
                component.set("v.errorMsg", "slds-show errormsg");
            }
            if(!value){
                empJobTitle.set("v.errors", [{message:"Job Title cannot be blank. Fill and Save."}]);
            }
            if(!startDateValue){
                empStartDate.set("v.errors", [{message:"Start Date is required"}]);
            }
            if(statusValue == 'Closed' && (endDateValue == null || endDateValue == '')){
                endDateField.set("v.errors", [{message:"End Date is required when status is 'Closed'."}]);
            }
            
            if(memberHasCPP && (cppValue == null || cppValue == "")){
                cppField.set("v.errors", [{message:"Please select Yes or No" }]);
            
            }
        }
    },
    //Edit Employer - Function used edit and save Employment History.
    editEmployer: function(component, event, helper) {      
        
        var isPEValid = true;
        var isDateValid = true;
        var isStatusValid = true;
        
        // check if Primary Employer can be ticked
        if(component.find("primaryempEdit").get("v.errors") != '' && component.find("primaryempEdit").get("v.errors") != null){
			isPEValid = false ;
		}
        
       	var startDateField = component.find("startdateField");
        var startDateValue = startDateField.get("v.value");        
        var endDateField = component.find("enddateField");
        var endDateValue = endDateField.get("v.value");
        var statusField = component.find("statuspicklist");
        var statusValue = statusField.get("v.value"); 
        var endDate ;
		var startDate ;
        
        //formating Start and End dates.
        if(endDateValue != null && endDateValue != ''){
            endDate = new Date(endDateValue);
            startDate = new Date(startDateValue);
        }
        //validating Employer End Date against Start Date.
        if(endDate instanceof Date && isFinite(endDate)){
            if(startDate > endDate){
                endDateField.set("v.errors", [{message:"End date cannot be earlier than start date.  Please amend."}]);
                isDateValid = false;
            }else{
                endDateField.set("v.errors", null);
            }
        }else{
            endDateField.set("v.errors", null);
        }
		
        // check if dates are correct - end date when status is Closed
        if(statusValue || endDateValue){
            if(statusValue == 'Closed' && (endDateValue == null || endDateValue == '')){
                endDateField.set("v.errors", [{message:"End Date is required when status is 'Closed'."}]);
                isStatusValid = false;
            }
            else if(isDateValid){
                endDateField.set("v.errors", null);
            }
        }

        if(isPEValid && isStatusValid && isDateValid){
            var action = component.get("c.updateEmpRecord");
            action.setParams({
                "updateAcc": component.get("v.editEmployerRec")
            });
    
            action.setCallback(this, function(a) {
                if (a.getState() === "SUCCESS") {
                    document.getElementById("backGroundSectionId").style.display = "none";
                    document.getElementById("editEmpSectionId").style.display = "none";
                    $A.get('e.force:refreshView').fire();
                } else if (a.getState() === "ERROR") {
                    $A.log("Errors", a.getError());
                }
            });
            $A.enqueueAction(action);
    	}
    },
    //New Employer - Function used to perform auto complete for Employer field.
    searchKeyChange: function(component, event) {
        component.set("v.changeitClass", "slds-show");
        var searchKey = event.getParam("searchKey");
        var action = component.get("c.findByName");
        action.setParams({
            "searchKey": searchKey,
            "memberOfValue": component.get("v.userMemberOf")
        });
        action.setCallback(this, function(a) {
            component.set("v.accounts", a.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    //New Employer - Function fired on handling of Auto Complete for Employer field. Used to hide the list of Employers.
    parentHandling : function(cmp, event) {
        var currentAccName = event.getParam("accname");
        cmp.set("v.accountname", currentAccName);
        var currentAccount = event.getParam("activeAcc");
        cmp.set("v.accountId", currentAccount.Id);
        cmp.set("v.changeitClass", "slds-hide");
    },
    //New Employer - Function used to show New Employer modal window.
    showModal: function(component, event, helper) {
        helper.getStatusPickListValues(component);
        helper.getCPPPicklistValues(component);
        
        document.getElementById("backGroundSectionId").style.display = "block" ;
        document.getElementById("newAccountSectionId").style.display = "block";

    },
    //Edit Employer - Function used to show Edit Employer modal window.
    showEditModal: function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var empid = selectedItem.dataset.record;
        helper.getStatusPickListValues(component);
        helper.getCPPPicklistValues(component);
        helper.getEmployerDetails(component, empid);
    },
	
    // on change of the status picklist (new records)
    onSelectChange : function(component, event, helper) {
        // Print out the selected value
        var selected = component.find("statuspicklist").get("v.value");
        component.set("v.newAccount.Status__c", selected);
        
        // add validation to ensure end date is populated 
        var endDateValue = component.find("empenddateField").get("v.value");
        var dateComponent = component.find("empenddateField");
        helper.statusChangedToClosed(selected, endDateValue, dateComponent);
    },
	// on change of the status picklist (edit records)
    onEditSelectChange : function(component, event, helper) {
        // Print out the selected value
        var selected = component.find("editselect").get("v.value");
        component.set("v.editEmployerRec.Status__c", selected);
        
        // add validation to ensure end date is populated 
        var endDateValue = component.find("enddateField").get("v.value");
        var dateComponent = component.find("enddateField");
        helper.statusChangedToClosed(selected, endDateValue, dateComponent);
    },
    
    // on change of the CPP picklist (new)
    onNewCPPSelectChange : function(component, event, helper) {
        var selected = component.find("cppPicklist").get("v.value");
        var memberHasCPP = component.get("v.memberHasCPP");
        var cppField = component.find("cppPicklist");
        
        if(selected == null || selected == "" || selected == "No"){
            component.set("v.newAccount.Is_CPP_Provided__c", false);
        }
        else if(selected == 'Yes'){
        	component.set("v.newAccount.Is_CPP_Provided__c", true);
        }
        
        // add validation to ensure the user selects something
        if(memberHasCPP && (selected == null || selected == "")){
            cppField.set("v.errors", [{message:"Please select Yes or No."}]);
        }else{
            cppField.set("v.errors", null);
        }
    },
    
    // on change of the CPP picklist (edit)
    onEditCPPSelectChange : function(component, event, helper) {
        var selected = component.find("editcppPicklist").get("v.value");
        var memberHasCPP = component.get("v.memberHasCPP");
        var cppField = component.find("editcppPicklist");
        
        if(selected == null || selected == '' || selected == 'No'){
            component.set("v.editEmployerRec.Is_CPP_Provided__c", false);
        }
        else if(selected == 'Yes'){
        	component.set("v.editEmployerRec.Is_CPP_Provided__c", true);
        }
        
        // add validation to ensure the user selects something
        if(memberHasCPP && (selected == null || selected == '')){
            cppField.set("v.errors", [{message:"Please select Yes or No."}]);
        }else{
            cppField.set("v.errors", null);
        }
    },
    
    //Function used to validate Primary Employer duplication
    checkPrimaryEmployerDeDupe : function(component, event, helper){
        var checkBox = event.getSource();
        //var chechboxValue = checkBox.get("v.value");
        helper.checkPrimaryEmpDuplicate(component, checkBox);
    }
})

// ({
//     doInit: function(cmp, event, helper) {
//     	cmp.set('v.mycolumns', [
//             { label: 'ID', fieldName: 'URL', type: 'url', initialWidth:120, typeAttributes: { label: { fieldName: 'Name' } } },
//             { label: 'Employer Name', fieldName: 'Employer_Name__c', type: 'text' },
//             { label: 'Job Title', fieldName: 'Job_Title__c', type: 'text' },
//             { label: 'Start Date', fieldName: 'Employee_Start_Date__c', type: 'date', initialWidth:120 },
//             { label: 'End Date', fieldName: 'Employee_End_Date__c', type: 'date', initialWidth:120 },
//             { label: 'Status', fieldName: 'Status__c', type: 'text', initialWidth:120 },
//             { label: 'Primary Employer', fieldName: 'Primary_Employer__c', type: 'boolean' },
//         ]);
//         helper.loadEmploymentHistory(cmp);
//     },

//     showCreateModel: function(cmp, evt, helper) {
//         $A.createComponent("c:CaanzccEmploymentRecordModal", {},
//             function(content, status) {
//                 if (status === "SUCCESS") {
//                     cmp.find('employmentRecordModal').showCustomModal({
//                         header: "Create a new Employment Record",
//                         body: content, 
//                         showCloseButton: true,
//                         cssClass: "mymodal",
//                         closeCallback: function() {
//                            helper.loadEmploymentHistory(cmp);
//                         }
//                     });
//                 }
//             }
//         );
//     },

//     showEditModal: function(cmp, evt, helper) {
//         var employmentId = evt.target.id;
//         $A.createComponent("c:CaanzccEmploymentRecordModal", {recordId: employmentId},
//             function(content, status) {
//                 if (status === "SUCCESS") {
//                     cmp.find('employmentRecordModal').showCustomModal({
//                         header: "Edit Employment Record",
//                         body: content, 
//                         showCloseButton: true,
//                         cssClass: "mymodal",
//                         closeCallback: function() {
//                            helper.loadEmploymentHistory(cmp);
//                         }
//                     });
//                 }
//             }
//         );
//     },
// })