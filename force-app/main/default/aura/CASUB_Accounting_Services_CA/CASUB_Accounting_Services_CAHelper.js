({
    dynamicallycreateComponent : function(event,cmp,componentName) {
        debugger;
        if(cmp.get('v.selectedCountry') == 'New Zealand'){
            /* Create Question Component Dynamically */
            $A.createComponent(
                'c:'+componentName,
                {
                    'aura:id':'accSerBlock'
                },
                function(accServiceBlock, status, errorMessage){
                    //Add the new button to the body array
                    if (status === "SUCCESS") {
                        var targetCmp = cmp.find('accServiceBlock');
                        var body = targetCmp.get("v.body");
                        body.push(accServiceBlock);
                        targetCmp.set("v.body", body);
                    }
                    else if (status === "INCOMPLETE") {
                        console.log("No response from server or client is offline.")
                        // Show offline error
                    }
                        else if (status === "ERROR") {
                            console.log("Error: " + errorMessage);
                            // Show error message
                        }
                }
            );
        }
        else{
            /* Destroy CPD Component Dynamically */
            var targetCmp = cmp.find('accServiceBlock');
            targetCmp.set("v.body", []);
            
        }        
        
        
    }
})