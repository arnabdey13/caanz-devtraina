({
    createComp : function(component, event, helper) {
        var accData = component.get('v.accountData');
        if(!$A.util.isUndefinedOrNull(accData)){
            if(accData.CPP_Picklist__c=='Suspended' || accData.CPP_Picklist__c=='Removed'){
                helper.dynamicallycreateComponent(event,component,'CASUB_CPP_for_NZ_CA');
            }
            if(accData.Licensed_Auditor__c == false || accData.Qualified_Auditor__c == false){
                helper.dynamicallycreateComponent(event,component,'CASUB_AccountingService_QA_LA_False_CA');
            }
            if(accData.Insolvency_Practitioner__c == false){
                helper.dynamicallycreateComponent(event,component,'CASUB_AccountingService_AccreditedInsolvencyPractitioner_CA');   
            }
        }
    }
})