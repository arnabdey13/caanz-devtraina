({
    load: function (component, event, helper) {

        // load the v.values if loadKey matches
        var readData = event.getParam("value").data;

        var loadKey = component.get("v.loadKey");
        if (loadKey) {
            var records = readData.records[loadKey];
            if (records && !$A.util.isEmpty(records)) {
                var position = component.get("v.wrapperPosition");
                if (!records[position].single) {
                    // this line assumes that any sub-component has a "values" attribute
                    // it's not defined in this component so that the concrete components can strongly type it
                    component.set("v.values", records[position].sObjectList);

                    component.getEvent("lifeCycleEvent").setParams({type: "load-list", opts: readData}).fire();

                    // report validity back to the driver via it's api fn. this happens for all field controls, even
                    // when they don't load from the event so that non-loaded fields can block form save
                    // invoke validation (even though may have been invoked by value change) so that all fields
                    // in a form can block save, even when not loaded
                    helper.validate(component, event, helper);

                    // driver may be undefined when running in tests. in that case, don't report
                    var driverComponent = event.getParam("value").driver;
                    if (driverComponent) {
                        var isValid = component.get("v.isValid");
                        var position = component.get("v.wrapperPosition");
                        driverComponent.setFieldValidity(
                            component.get("v.loadKey"),
                            "LIST-" + position,
                            isValid,
                            component.get("v.visibleForStage"));
                    } else {
                        $A.log("No driver found, not reporting list validity on load: " + component.get("v.loadKey"));
                    }

                }
            }
        }
    },
    updateControl: function (component, event, helper) {
        var loadParams = event.getParams();
        component.set("v.loadData", loadParams); // this will cause load above to run since it is a change
    },
    handleCRUD: function (component, event, helper) {
        event.stopPropagation();
        var op = event.getParam("operation");
        switch (op) {
            case "CREATE":
                helper.addRecord(component);
                break;
            case "UPDATE":
                var id = event.getParam("id");
                helper.editRecord(component, id);
                break;
            case "DELETE":
                var id = event.getParam("id");
                helper.deleteRecord(component, id);
                break;
            case "SAVE": // optimistic write with different callback behaviours (called successMode)

                // must use concrete component since form fields are in concrete markup
                var isFormValid = helper.isFormValid(component.getConcreteComponent());
                component.set("v.isFormValid", isFormValid);
                if (isFormValid) {
                    if (component.get("v.isFormEditingNewRecord")) {
                        // eager send of focused record to Apex
                        helper.fireApplicationEvent(component, "e.c:ff_EventSaveFocused", {
                            successMode: "REPLACENEW", // replace the record
                            //callback: saveCallback
                        });
                        // local crud will add the optimistically created row at start of list
                        helper.fireApplicationEvent(component, "e.c:ff_EventCRUDFocused", {operation: "CREATE"});
                    } else {
                        // eager send of focused record to Apex
                        helper.fireApplicationEvent(component, "e.c:ff_EventSaveFocused", {
                            successMode: "REPLACE" // replace the values in the readData with dirty fields
                            //callback: saveCallback
                        });
                        // TODO fire local crud event (like new above) to optimistically update v.values with focused data?
                    }

                    component.getEvent("lifeCycleEvent").setParams({type: "editor-pre-close"}).fire();

                    // destroy form and show list
                    component.set("v.editMode", false);
                }
                else {
                    helper.setValidationForFields(component.getConcreteComponent(), true, false);
                }
                break;
            case "CANCEL":
                // cause driver to clean up dirty changes in focused record so that it's not blocked
                helper.fireApplicationEvent(component, "e.c:ff_EventCRUDFocused", {operation: "CANCEL"});
                component.set("v.editMode", false);
                break;
            default:
                throw Error("Unhandled CRUD component op: " + op);
        }
    },
    handleSetValidation: function (component, event, helper) {
        if (!component.get("v.ignoreSetValidationEvents")) {
            helper.adjustValidation(component, helper, event.getParam("show"), event.getParam("disable"));
        }
    },
    apiSetValidation: function (component, event, helper) {
        var args = event.getParam("arguments");
        helper.adjustValidation(component, helper, args.display, args.disable);
    },
    apiValidate: function (component, event, helper) {
        helper.validate(component, event, helper);
    },
    apiAddRecord: function (component, event, helper) {
        helper.addRecord(component);
    },
    apiEditRecord: function (component, event, helper) {
        var p = event.getParam("arguments");
        helper.editRecord(component, p.id);
    },
    apiDeleteRecord: function (component, event, helper) {
        var p = event.getParam("arguments");
        helper.deleteRecord(component, p.id, p.callback);
    }
})