({
    addRecord: function (component) {
        component.set("v.isFormEditingNewRecord", true);
        component.set("v.editMode", true);
        component.getEvent("lifeCycleEvent").setParams({type: "load-list-editor"}).fire();
        this.fireApplicationEvent(component, "e.c:ff_EventSetDriverFocus",
            {
                operation: "CREATE",
                loadKey: component.get("v.loadKey"),
                position: component.get("v.wrapperPosition"),
                recordPosition: component.get("v.values").length
            });
    },
    editRecord: function (component, id) {
        if ($A.util.isEmpty(id)) {
            throw Error("Id absent");
        }
        component.set("v.isFormEditingNewRecord", false);
        component.set("v.editMode", true);

        // tell the driver to focus on this record
        var recordPosition;
        var values = component.get("v.values");
        for (var i = 0; i < values.length; i++) {
            if (values[i].Id == id) {
                recordPosition = i;
                break;
            }
        }
        if ($A.util.isUndefined(recordPosition)) {
            throw Error("Id not found in record array: " + id);
        }

        // driver needs to setup a dirty record for changes that will be loaded
        this.fireApplicationEvent(component, "e.c:ff_EventSetDriverFocus",
            {
                operation: "UPDATE",
                loadKey: component.get("v.loadKey"),
                position: component.get("v.wrapperPosition"),
                recordPosition: recordPosition
            });

        // now load the record. use concrete since finding fields in concrete markup
        this.loadEditor(component.getConcreteComponent(), values[recordPosition]);

        component.getEvent("lifeCycleEvent").setParams({
            type: "load-list-editor",
            opts: {record: values[recordPosition]}
        }).fire();

    },
    loadEditor: function (component, record) {
        var auraIds = component.get("v.fieldIds");

        // set all fields to fire ignored events first. this stops dependent viz fields which hide/show from
        // creating dirty driver data during the load
        for (var i = 0; i < auraIds.length; i++) {
            var fieldControl = component.find(auraIds[i]);
            if (fieldControl) { // editors might use aura:if to hide some fields
                fieldControl.set("v.valueChangeIgnored", true);
            }
        }

        for (var i = 0; i < auraIds.length; i++) {
            var fieldControl = component.find(auraIds[i]);
            if (fieldControl) { // editors might use aura:if to hide some fields
                var field = fieldControl.get("v.field");
                fieldControl.set("v.value", record[field]);
            }
        }

        for (var i = 0; i < auraIds.length; i++) {
            var fieldControl = component.find(auraIds[i]);
            if (fieldControl) { // editors might use aura:if to hide some fields
                fieldControl.set("v.valueChangeIgnored", false);
            }
        }
    },
    deleteRecord: function (component, id, callback) {
        if ($A.util.isEmpty(id)) {
            throw Error("Delete called without and id");
        }
        var isConfirmRequired = component.get("v.confirmDeletes");
        if (!isConfirmRequired || confirm(component.get("v.deleteConfirmLabel"))) {
            $A.log("deleting: " + id);
            // optimistically delete the record by filtering values
            var values = component.get("v.values");
            var newValues = [];
            for (var i = 0; i < values.length; i++) {
                if (values[i].Id != id) {
                    newValues.push(values[i]);
                }
            }
            component.set("v.values", newValues);

            // fire delete event to driver
            this.fireApplicationEvent(component, "e.c:ff_EventDeleteRecord",
                {
                    loadKey: component.get("v.loadKey"),
                    wrapperPosition: component.get("v.wrapperPosition"),
                    id: id,
                    callback: this.deleteCallback(component, callback)
                });
        }
    },
    deleteCallback: function (component, doneCallback) {
        return $A.getCallback(function (result) {
            component.set("v.deleteResults", result);
            if (doneCallback) {
                doneCallback(result);
            }
        });
    },
    // this is validation of form fields only, not for the control.
    isFormValid: function (component) {
        var fields = this.getFields(component);
        var valid = true;
        for (var i = 0; i < fields.length; i++) {
            if (fields[i].get("v.visible")) {
                fields[i].validate();
                var isFieldValid = fields[i].get("v.isValid");
                if (!isFieldValid) {
                    valid = false;
                    break;
                }
            }
        }
        return valid;
    },
    setValidationForFields: function (component, display, disable) {
        var fields = this.getFields(component);
        for (var i = 0; i < fields.length; i++) {
            fields[i].setValidation(display, disable);
        }
    },
    getFields: function (component) {
        var fieldIds = component.get("v.fieldIds");
        if (fieldIds.length == 0) {
            throw Error("No field ids set for list control");
        }
        var fields = [];
        for (var i = 0; i < fieldIds.length; i++) {
            var f = component.find(fieldIds[i]);
            if (f) {
                fields.push(f);
            }
        }
        return fields;
    },
    // this is aggregate validation across all records. this should report/block the driver save if invalid
    validate: function (component, event, helper) {

        if (component.get("v.disableValidation")) {
            component.set("v.validationErrors", []);
            component.set("v.specificValidationErrors", []);
            component.set("v.isValid", true);
        } else {
            var validationErrors = [];

            var value = component.get("v.value");

            // control data for common validation
            var minRows = component.get("v.minimumRows");

            // common validation evaluation
            var vals = component.get("v.values");
            var isMinSatisfied = $A.util.isUndefined(minRows) || minRows > 0 && vals.length >= minRows;
            if (!isMinSatisfied) {
                validationErrors.push(component.get("v.minimumErrorMessage"));
            }

            component.set("v.validationErrors", validationErrors);

            // now invoke any specific validation
            // TODO use EventControlLifeCycle when concrete lists need to add validation

            // set the total validity based on common and specific errors
            this.deriveValidity(component);
        }
    },
    deriveValidity: function (component) {
        var common = component.get("v.validationErrors");
        var isValid = $A.util.isEmpty(common); // see FieldControl for how to combine with specific errors
        component.set("v.isValid", isValid);
    },
    // transform the attachment wrapper data for an sobject into a map grouped by parent id
    // this is only used when attachments are returned in list controls i.e. follow the <entity><id>.Attachments naming standard
    getAttachmentsByParent: function (sobjectType, wrappers) {
        var wrappersByParent = {};
        for (var wrapperKey in wrappers) {
            if (wrapperKey.indexOf(sobjectType) == 0 && wrapperKey.indexOf(".Attachments") != -1) {
                var parentId = wrapperKey.substring(sobjectType.length, wrapperKey.length - ".Attachments".length);
                wrappersByParent[parentId] = wrappers[wrapperKey];
            }
        }
        return wrappersByParent;
    },
    loadUploadControls: function (component, parentType, uploadId, wrappers) {
        if (!$A.util.isEmpty(component.get("v.values"))) {
            var attachmentsGrouped = this.getAttachmentsByParent(parentType, wrappers);

            var uploadControls = component.find(uploadId);
            if (Array.isArray(uploadControls)) { // could be only one row
                for (var i = 0; i < uploadControls.length; i++) {
                    uploadControls[i].loadWrappers(attachmentsGrouped[uploadControls[i].get("v.parentId")]);
                }
            } else {
                uploadControls.loadWrappers(attachmentsGrouped[uploadControls.get("v.parentId")]);
            }
        }
    },
    showValuesJSON: function (component, values) {
        component.set("v.valuesJSON", JSON.stringify(values, function (key, value) {
            return value;
        }, 3));
    }
})