({
    //Function used to load default Employment History existing records.
    loadMethod : function(component) {
        var action = component.get("c.EmpList");
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                component.set("v.employmentHistoryList", a.getReturnValue());
                this.sortBy(component, 'Primary_Employer__c',false);
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
            }
        });
        $A.enqueueAction(action);
    },
    sortBy: function(component, field,sortAsc) {
        var records = component.get("v.employmentHistoryList");
        records.sort(function(a,b){
            var t1 = a[field] == b[field],
                t2 = a[field] > b[field];
            return t1? 0: (sortAsc?-1:1)*(t2?-1:1);
        });
        component.set("v.employmentHistoryList", records);
    },
    showCreateModalForEmp:function(cmp){
        var modalBody;
        var statusvalue=cmp.get("v.statusvalue");
        var providesCPPValues=cmp.get("v.providesCPPValues");
        var currentUserMemberDetail=cmp.get("v.currentUserMemberDetail");
        $A.createComponent("c:CASUB_New_EmploymentHistory_CA", 
                           {
                            mode: "newEmployment",
                            statusvalue:statusvalue,
                            providesCPPValues:providesCPPValues,
                            currentUserMemberDetail:currentUserMemberDetail
                           },
               function(content, status) {
               if (status === "SUCCESS") {
                   modalBody = content;
                   cmp.find('overlayLib').showCustomModal({
                       header: "Create New Employment",
                       body: modalBody, 
                       showCloseButton: true,
                       cssClass: "cCASUB_EmploymentHistoryList_CA",
                       closeCallback: function() {
                       }
                   }).then(function (overlay) {})
               }                               
           });
    },
    showEditModalForEmp:function(cmp,employmentRecordId){
        var modalBody;
         var statusvalue=cmp.get("v.statusvalue");
        var providesCPPValues=cmp.get("v.providesCPPValues");
        var currentUserMemberDetail=cmp.get("v.currentUserMemberDetail");
        $A.createComponent("c:CASUB_New_EmploymentHistory_CA", 
                           {
                            mode: "editEmployment",
                            employmentId:employmentRecordId,
                            statusvalue:statusvalue,
                            providesCPPValues:providesCPPValues,
                            currentUserMemberDetail:currentUserMemberDetail
                           },
               function(content, status) {
               if (status === "SUCCESS") {
                   modalBody = content;
                   cmp.find('overlayLib').showCustomModal({
                       header: "Edit Employment",
                       body: modalBody, 
                       showCloseButton: true,
                       cssClass: "cCASUB_EmploymentHistoryList_CA",
                       closeCallback: function() {}
                   }).then(function (overlay) {})
               }                               
        });
    },
	showCreateModal: function(cmp, body, footer){
		var overlay = cmp.find('overlayLib');
		if(overlay){
			//footer.set("v.saveFunction", this.performSave.bind(this, cmp, body));
			var params = {
	            header: "Create New Employment", 
                body: body,footer:footer,
	            showCloseButton: true, 
                cssClass: "cCASUB_EmploymentHistoryList_CA",
                closeCallback: this.loadMethod.bind(this, cmp)
	        };
			overlay.showCustomModal(params).then(function (overlay) { 
      			//cmp.set('v.overlayPanel', overlay);
    		});
		}
	},
	showEditModal: function(cmp, body, footer){
		var overlay = cmp.find('overlayLib');
		if(overlay){
			//footer.set("v.saveFunction", this.performSave.bind(this, cmp, body));
			var params = {
	            header: "Edit Employment", 
                body: body, 
	            showCloseButton: true, 
                cssClass: "cCASUB_EmploymentHistoryList_CA",
                closeCallback: this.loadMethod.bind(this, cmp)
	        };
			overlay.showCustomModal(params).then(function (overlay) { 
      			cmp.set('v.overlayPanel', overlay);
    		});
		}
	},
    //Function used to fetch the picklist values for Status field.
    getStatusPickListValues : function(component){
        var action = component.get("c.getStatusPickListClass");
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                component.set("v.statusvalue", a.getReturnValue());
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
            }
        });

        $A.enqueueAction(action);
    },
    getCPPPicklistValues : function(component){
        var action = component.get("c.getCPPPickListClass");
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                component.set("v.providesCPPValues", a.getReturnValue());
                //component.find("cppPicklist").set("v.value", 'Yes');
                //component.find("cppPicklist").set("v.selected", true);
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
            }
        });

        $A.enqueueAction(action);
    },
    loadCurrentUserMemberDetail : function(component){
        var action = component.get("c.returnCurrntUserMember");
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                component.set("v.currentUserMemberDetail", a.getReturnValue());
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
            }
        });
        $A.enqueueAction(action);
    },
})