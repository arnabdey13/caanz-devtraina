({
    doInit: function(component, event, helper) {
        helper.loadMethod(component);
        helper.getStatusPickListValues(component);
        helper.getCPPPicklistValues(component);
        helper.loadCurrentUserMemberDetail(component);
    },
    handleShowModal: function(cmp, event, helper) {
        helper.showCreateModalForEmp(cmp);
    },
    handleEmploymentEdit: function(cmp, event, helper) {
        var employmentRecordId = event.getParam("employmentId");
        helper.showEditModalForEmp(cmp,employmentRecordId);
    },    
    updateEmpHistory : function(component, event,helper) { 
        var message = event.getParam("updateEmploymentHistory");
        if(typeof message != 'undefined'){
            helper.loadMethod(component);
            helper.getStatusPickListValues(component);
            helper.getCPPPicklistValues(component);
            helper.loadCurrentUserMemberDetail(component);
        }
    },
})