({
    sampleLoad: function (component, event, helper) {
        helper.setStage(component, 0);
    },
    handleSave: function (component, event, helper) {

        // // dev only code
        // var d = component.get("v.readData").records.Account[0].sObjectList[0];
        // console.log(component.get("v.readData").records);
        //

        var wd = component.get("v.writeData");

        // real code
        helper.sendDirtyRecords(component, event, helper);
    },
    handleLifeCycle: function (component, event, helper) {
        event.stopPropagation();
        var eventType = event.getParam("type");
        switch (eventType) {
            case "pre-load":
                // best place to set focus so that any value change events fired by fields hidden by parents
                // will have a focused record ready to record the dirty changes

                // this form always saves a new Contact so it should be able to set focus on a new Contact record

                var contactId = component.get("v.contactId");
                if ($A.util.isEmpty(contactId)) {
                    contactId = helper.tempIdInsert("Contact1");
                    component.set("v.contactId", contactId);
                }
                // this focus is driver focus
                helper.setFocusedRecord(component, contactId, "Contact", 0);

                // setup for auto-switch of focus on value change: see design docs
                helper.setPrimaryFocus(component, contactId, "Contact", 0);
                // when focused on Campaign records, value changes for these will still update the dirty data correctly
                helper.setPrimaryLoadKeys(component, ["Contact", "Account"]);

                break;
            case "post-load":
                // this focus is DOM focus i.e. put the cursor in an element
                var isInitialLoad = $A.util.isUndefinedOrNull(event.getParam("oldValue"));
                if (isInitialLoad) {
                    helper.fireApplicationEvent(component,
                        "e.c:ff_EventSetFocus",
                        {loadKey: "Contact", field: "FirstName"});
                }
                break;
            case "stage-change":
                // if required, drivers can exclude stages when firing this event
                helper.fireApplicationEvent(component, "e.c:ff_EventStageChange", {
                    stage: component.get("v.currentStage"),
                    stages: component.get("v.allStages")
                    // no layout box headings in sample test
                })
                break;
            case "post-save":
                var saveResult = event.getParam("opts");
                if (saveResult.success) {
                    $A.log("Write results: " + JSON.stringify(saveResult.results));
                } else {
                    console.log("Save Failed!");
                    console.log(JSON.stringify(saveResult));
                }
                break;
            case "post-value-change":
                // nothing required here
                break;
            default:
                console.log("unhandled life-cycle event: " + eventType);
        }
    },

    //// fns exposed for tests or other components with references to the driver (should be rare) ////

    apiCancelFocusedRecord: function (component, event, helper) {
        helper.cancelFocusedRecord(component);
    },
    apiNextStage: function (component, event, helper) {
        helper.nextStage(component);
    },
    apiSendDirtyRecords: function (component, event, helper) {
        var args = event.getParam("arguments");
        helper.sendDirtyRecords(component, event, helper, args.callback);
    },
    apiUploadFile: function (component, event, helper) {
        var a = event.getParam("arguments");
        var callback = helper.getUploadCallback(a.source, a.callback);
        helper.uploadFile(component, a.parentId, a.fileName, a.base64Data, a.contentType, a.source, callback);
    }
})