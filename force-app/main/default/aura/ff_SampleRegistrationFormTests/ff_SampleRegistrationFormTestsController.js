({
    doInit: function (component, event, helper) {

        var test = helper.driver(component, event, helper);

        // references to all the components being tested
        var form1 = component.find("form1");
        var driver1 = form1.find("driver");

        // IMPORTANT: focus descriptions must be unique since they control how assertions are groups
        var startTests = test.start({
            focused: driver1,
            description: "The 1st form/driver will test general use"
        })

        // using a single chain.

        //////////// #1 EMPTY ////////////

        var initialValidity = {
            "no-stage": {
                "Contact.FirstName": false,
                "Contact.LastName": false,
                "Contact.Email": false,
                "Contact.Title": false,
                "Account.Name": true,
                "Contact.MailingCountry": false,
                "Account.MailingPostalCode": false,
                "Contact.Username__c": false,
                "undefined.emailOptIn": true,
                "undefined.tsandcs": false
            }
        };

        startTests
            .then(test.wait(function (context) {
                //console.log(form1.find("postalCode"));
                var x = form1.find("postalCode").get("v.displayValidationErrors");
                //console.log(validity);
                //debugger;
            }))
            .then(test.assert("v.loaded", "The form should be loaded before tests start"))
            .then(test.assertEquals(initialValidity, "v.formValidity", "The driver validity state should be partially true"))

            // focus on postal code to check post-load error-display
            .then(test.focus(form1.find("postalCode")))
            .then(test.assertEquals(false, "v.displayValidationErrors",
                "Postal code (and all fields) is not showing errors just after load"))

            // focus back on driver for the rest of the tests
            .then(test.focus(driver1)) 
            .then(test.wait(function (context) {
                form1.find("postalCode").set("v.value", "2010");
            }))
            .then(test.assertEquals({
                    "no-stage": {
                        "Contact.FirstName": false,
                        "Contact.LastName": false,
                        "Contact.Email": false,
                        "Contact.Title": false,
                        "Account.Name": true,
                        "Contact.MailingCountry": false,
                        "Account.MailingPostalCode": true,
                        "Contact.Username__c": false,
                        "undefined.emailOptIn": true,
                        "undefined.tsandcs": false
                    }
                }, "v.formValidity",
                "The postcode field is now seen as valid by the driver"))

            .then(test.wait(function (context) {
                form1.find("postalCode").set("v.value", ""); // postalCode is configured to be required
            }))
            .then(test.assertEquals(initialValidity, "v.formValidity",
                "The empty postcode field is seen as invalid by the driver"))

            .then(test.wait(function (context) {
                form1.find("postalCode").set("v.value", "xxx"); // postalCode is configured to only accept numbers
            }))
            .then(test.assertEquals(initialValidity, "v.formValidity",
                "The string postcode field is now seen as invalid by the driver"))

            // TODO save the form and check state after save
            // whenDone blocks the chain until the done fn is called
            // .then(test.whenDone(function (context, done) {
            //     loginRejected.login(done);
            // }))

            //////////// END OF TESTS ////////////

            // always include these fns to handle the end of the test
            .then(test.pass).catch(test.fail);

    }
})