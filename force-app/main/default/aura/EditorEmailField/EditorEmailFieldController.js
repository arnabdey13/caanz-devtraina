({
    onChange : function(component, event, helper) {
        helper.applyChange(component, event.getSource().get("v.value"));
    }
})