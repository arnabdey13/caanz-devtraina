({	
    validate : function(component) {
        var isRequired = component.get("v.required");
        var hidden = component.get("v.hidden");
        var value = component.get("v.value");
        var isValueMissing = typeof value === 'undefined' || value == "";
        var invalid = !hidden && isRequired && isValueMissing;
        $A.log("validate "+component.get("v.field"), {
            hidden: hidden,
            typeUndefined: typeof value === 'undefined',
            empty: value == "",
            invalid: invalid});
        return !invalid;
    },
    setUIValid : function(component, isValidClient, isValidServer) {
        var editor = component.find("editor");
        if (isValidClient && isValidServer) {
            editor.set("v.errors", null);
            $A.util.removeClass(component, "slds-has-error");
        } else {
            var errorMessages = [] ;
            if(!isValidClient){
                errorMessages.push({message: "This field is required"}) ;
            }
            
            if(!isValidServer){                
                var messages = component.get("v.apexErrors");
                for(var i=0; i<messages.length; i++){
                    errorMessages.push({message: messages[i]}) ;
                }
            }
            editor.set("v.errors", errorMessages);
            $A.util.addClass(component, "slds-has-error");
        }
    }
})