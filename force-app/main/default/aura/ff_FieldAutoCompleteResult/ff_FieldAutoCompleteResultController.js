({
    handleClick: function (component, event, helper) {
        component.getEvent("selectionEvent")
            .setParams({position: component.get("v.position")})
            .fire();
    }
})