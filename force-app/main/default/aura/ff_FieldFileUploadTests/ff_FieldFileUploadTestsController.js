({
    doInit: function (component, event, helper) {

        var test = helper.driver(component, event, helper);

        // references to all the components being tested
        var file1 = component.find("file1");
        var file2 = component.find("file2");
        var file3 = component.find("file3");
        var file5 = component.find("file5");

        var startTests = test.start({
            focused: file1,
            description: "valid state after load"
        })

        var wrappers = {};

        // this data shape is based on System.debug output of ff_WrapperAttachmentSourceTests.cls
        wrappers["Account.Attachments"] = [
            {// service returns N files, de-duped by file name, for each prefix
                attachments: [
                    {Name: "CV:Latest CV with a very long name - page 1.doc"},
                    {Name: "CV:Latest CV with a very long name - page 2.doc"}],
                parent: "0016F00001rY10zQAC"
            },
            {
                attachments: [
                    {Name: "References:CEO recommendation.doc"}],
                parent: "0016F00001rY10zQAC"
            },
            {// service always returns an empty object so that empty controls can set a parentId
                attachments: [],
                parent: "0016F00001rY10zQAC"
            }];

        startTests

            .then(test.wait(helper.fireLoadEvent("Account", {
                Resume__c: "" // generic input only supports empty string to be pre-loaded
            }, wrappers)))

            // file1 demonstrates that the generic input doesn't provide good load display
            // hence the need for a custom upload control i.e. file2 & file3
            .then(test.assertEquals(false, "v.isValid", "generic file doesn't validate"))

            .then(test.focus(file2))
            .then(test.assertEquals(true, "v.loaded", "CV field is loaded by the event"))
            .then(test.assertEquals(true, "v.isValid", "CV field is valid because a file was loaded"))
            .then(test.assertEquals(false, function () {
                return $A.util.isEmpty(file2.get("v.parentId"))
            }, "CV field has a parent id after the load"))

            .then(test.focus(file3))
            .then(test.assertEquals(true, "v.loaded", "Resume field is loaded by the event"))
            .then(test.assertEquals(false, "v.isValid", "Resume field is invalid because no prior upload"))
            .then(test.assertEquals(false, function () {
                return $A.util.isEmpty(file3.get("v.parentId"))
            }, "Resume field has a parent id after the load"))

            // mimic how a driver interacts with the component as it handles the upload
            .then(test.focus(file5))
            .then(test.setAttribute("v.fileInProgress", "recipes.txt")) // control makes this change before firing event
            .then(test.setAttribute("v.percentComplete", 0)) // from driver when upload starts
            .then(test.setAttribute("v.percentComplete", 100)) // from driver when upload ends
            .then(test.sleep(1000))
            .then(test.setAttribute("v.percentComplete", undefined)) // from driver when upload ends

            //////////// END OF TESTS ////////////

            // always include these fns to handle the end of the test
            .then(test.pass).catch(test.fail);

    }
})