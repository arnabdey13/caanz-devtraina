({
	getAccountData: function(cmp) {
		this.enqueueAction(cmp, "getAccountData", {}, function(error, data){
            if(error){
                cmp.set("v.error", error);
            }else{
             cmp.set("v.data", JSON.parse(data));
             this.getFinancialCategory(cmp);
            }
                
		});
	},
	
	enqueueAction: function(cmp, method, params, callback){
		this.toggleSpinner(cmp, true);
		var action = cmp.get("c." + method); 
		if(params) action.setParams(params);
		action.setCallback(this, function(response){
			if(response.getState() === "SUCCESS") {
				if(callback) callback.call(this, null, response.getReturnValue());
                
	        } else if(response.getState() === "ERROR") {
	        	var message = 'Unknown error'; 
	        	var errors = response.getError();
	            if (!$A.util.isEmpty(errors)) {
	                message = errors[0].message;
	            }
	        	console.error(message);
	        	if(callback) callback.call(this, message);
	        }
	        this.toggleSpinner(cmp, false);
		});
		$A.enqueueAction(action);
	},
    getFinancialCategory : function(cmp){
        this.enqueueAction(cmp, "getFinancialCategoryPicklistValues", {}, function(error, data){
            if(error){
                cmp.set("v.error", error);
            }else{
             cmp.set("v.financialCategoryPickList", data);
             this.getConcessionConfigurationData(cmp);
            }
            //cmp.set("v.initDone","true");
                
		});
    },
    getConcessionConfigurationData : function(cmp){
        this.enqueueAction(cmp, "getConcessionConfigData", {}, function(error, data){
            if(error){
                cmp.set("v.error", error);
            }else{
            cmp.set("v.casubConcessionSetting", data);
            }
            cmp.set("v.initDone","true");
                
		});
    },
    saveFinancialCategory:function(cmp,concessionType){
        //saveFinancialCategory(String personAccountId,String financialCategory)
        var param = {'personAccountId':cmp.get("v.data.account.Id"),'financialCategory':concessionType};
    	this.enqueueAction(cmp, "saveFinancialCategory", param, function(error, data){
            if(error){
                cmp.set("v.error", error);
            }else{
             this.getAccountData(cmp);
            }
                
		});
	},
	toggleSpinner: function(cmp, show) {
		var spinner = cmp.find("spinner");
		$A.util.addClass(spinner, show ? "slds-show" : "slds-hide");
		$A.util.removeClass(spinner, !show ? "slds-show" : "slds-hide");
	}
})