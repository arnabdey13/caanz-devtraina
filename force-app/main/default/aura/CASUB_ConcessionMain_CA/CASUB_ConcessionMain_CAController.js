({
	doInit: function(cmp, event, helper) {
		helper.getAccountData(cmp);
	},
    handleConcessionSave : function(cmp, event, helper){
        var concessionType = event.getParam("concessionType");
        console.log('==concessionType==' + concessionType);
        helper.saveFinancialCategory(cmp,concessionType);
    },
    validateConcessionOptions : function(cmp, event, helper){
        var params = event.getParam ? event.getParam("arguments").params || {} : {};
        var childConcessionComponent = cmp.find("myDetailConcession");
        childConcessionComponent.validateChildConcession({ callback: $A.getCallback(params.callback) });
    }
})