({
    doInit : function(component, event, helper) {
        var action = component.get("c.getApplicationsForUser");
        if(component.get("v.filterRecordTypesCSV")!=null || component.get("v.filterStatusesCSV")!=null){
            action.setParams({"applicationRecordTypesCSV": component.get("v.filterRecordTypesCSV"),
							  "applicationStatusesCSV": component.get("v.filterStatusesCSV")})
        };
        action.setCallback(this, function(a){
            
        	var rtnValue = a.getReturnValue();
            //alert('applicationlist:getApplications:rtnValue='+rtnValue);
            console.log('rtnValue='+rtnValue);
            $A.log('rtnValue='+rtnValue);
            if (rtnValue !== null) {
                component.set('v.applications',rtnValue);
            }
        });
        $A.enqueueAction(action);
        
        var userAction = component.get("c.getIsAgent");

        userAction.setCallback(this, function(a){
            var rtnValue = a.getReturnValue();
            $A.log('rtnValue='+rtnValue);
            component.set('v.isAgent', rtnValue);
            
        });
        $A.enqueueAction(userAction);

    },
    
    goToApplicationDetails : function(component, event, helper) {


//        var target = event.getSource();
//        cmp.set("v.text", target.get("v.label"));
//        

		var application_item = event.target;
        var application_id = application_item.dataset.id;
        var application_type = application_item.dataset.type;
        var detailPageName = component.get("v.detailPage");
        var provisionalApplicationPage = component.get("v.provAppPageLink");
        var itemID = event.currentTarget.id;
        var itemType = event.currentTarget.type;
        var orgURL = $A.get("$Label.c.Org_URL");
        var commnunityPrefix = $A.get("$Label.c.Community_Prefix_CCH");
        var navigateToURL;

        console.log("Application ID: " + application_id);
        console.log("Application Tyoe: " + application_type);
        console.log("Detail Page Name: " + detailPageName);
        console.log("Provisional Page Link: " + provisionalApplicationPage);
        console.log("Target ID: " + itemID);
        console.log("Target Type: " + itemType);
        console.log("Org URL: " + orgURL);
        console.log("Community Prefix: " + commnunityPrefix);
        
        switch (application_type) {
            case "Provisional Member":
                navigateToURL = "/" + provisionalApplicationPage;
                break;
            case "IPP Provisional Member":
                navigateToURL = "/ipp";
                break;
            case "Special Admissions":
                navigateToURL = "/specialadmissions";
                break;
            default:
                navigateToURL = orgURL + '/' + commnunityPrefix + '/' + detailPageName + '?id=' + application_id + "&CommunityPrefix=" + commnunityPrefix;
                break;
        }
        
        var urlEvent = $A.get("e.force:navigateToURL");            
            
        urlEvent.setParams({
            "url": navigateToURL
        });
        urlEvent.fire();
    }
})