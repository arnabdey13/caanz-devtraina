({
    upsertCPD: function(component,questionaireResponseObjectToUpdate,callBackParams){
        this.validateRegistrationQuestionsAndAnswers(component,questionaireResponseObjectToUpdate,function(){
            console.log('==questionaireResponseObjectToUpdate==' + JSON.stringify(questionaireResponseObjectToUpdate));
            var subscriptionSobject = component.get("v.globalSubscriptionSobject");
            console.log('==globalSubscriptionSobject==' + JSON.stringify(subscriptionSobject));
            if($A.util.isUndefinedOrNull(subscriptionSobject.Obligation_Sub_Components__c )){
                subscriptionSobject.Obligation_Sub_Components__c = 'CPD (Continuing Professional Development);';
            }
            else if(!subscriptionSobject.Obligation_Sub_Components__c.includes("CPD (Continuing Professional Development)")){
                if(subscriptionSobject.Obligation_Sub_Components__c.substring(subscriptionSobject.Obligation_Sub_Components__c.length - 1) == ';')
                    subscriptionSobject.Obligation_Sub_Components__c += 'CPD (Continuing Professional Development);;';
                else
                    subscriptionSobject.Obligation_Sub_Components__c += ';CPD (Continuing Professional Development);';
            }
            
            console.log('==globalSubscriptionSobject==' + JSON.stringify(subscriptionSobject));
            var param = {
                'personAccountId':component.get("v.personAccountId"),
                'questionaireResponseObjectToUpdate':questionaireResponseObjectToUpdate,
                'selectedCountry':component.get("v.selectedCountry"),
                'whichSectionSave' : 'CPD',
                'subscriptionSobject': subscriptionSobject
            };
            this.enqueueAction(component, "upsertQuestionaireResponse", param, function(error, data){
                 var questionResponseSubscriptionWrappper = data;
                if(error){
                    component.set("v.errorMessage",error);
                }else if(!$A.util.isUndefinedOrNull(callBackParams) && callBackParams.callback) {
                    if (!$A.util.isUndefinedOrNull(questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id)) {
                        component.set("v.questionnaireResponseSobject.Id",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id);
                        component.set("v.questionnaireResponseSobject.Residential_Country__c",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Residential_Country__c);
                        component.set("v.globalSubscriptionSobject",questionResponseSubscriptionWrappper.subscriptionSObject);
                        /*
                        component.set("v.questionnaireResponseSobject.Id",data.Id);
                        component.set("v.questionnaireResponseSobject.Residential_Country__c",data.Residential_Country__c);
                        */
                    }
                    component.set("v.errorMessage",'');
                    callBackParams.callback(); 
                }else{
                    if (!$A.util.isUndefinedOrNull(questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id)) {
                        component.set("v.questionnaireResponseSobject.Id",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id);
                        component.set("v.questionnaireResponseSobject.Residential_Country__c",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Residential_Country__c);
                        component.set("v.globalSubscriptionSobject",questionResponseSubscriptionWrappper.subscriptionSObject);
                        /*
                        component.set("v.questionnaireResponseSobject.Id",data.Id);
                        component.set("v.questionnaireResponseSobject.Residential_Country__c",data.Residential_Country__c);*/
                    }
                }
            })
        });
        
    },
    enqueueAction: function(component, method, params, callback){
        this.toggleSpinner(component, true);
        var action = component.get("c." + method); 
        if(params) action.setParams(params);
        action.setCallback(this, function(response){
            this.toggleSpinner(component, false);
            if(response.getState() === "SUCCESS") {
                if(callback) callback.call(this, null, response.getReturnValue());
            } else if(response.getState() === "ERROR") {
                var message = 'Unknown error'; 
                var errors = response.getError();
                if (!$A.util.isEmpty(errors)) {
                    message = errors[0].message;
                }
                console.error(message);
                if(callback) callback.call(this, message);
            }
        });
        $A.enqueueAction(action);
    },
    toggleSpinner: function(component, show) {
        var spinner = component.find("cppSpinner");
        $A.util.addClass(spinner, show ? "slds-show" : "slds-hide");
        $A.util.removeClass(spinner, !show ? "slds-show" : "slds-hide");
    },
    
    validateRegistrationQuestionsAndAnswers : function(component,questionaireResponseObjectToUpdate,callback){
        var errorMessage = '';
        var isValidate=false;
        if(questionaireResponseObjectToUpdate.Answer_2__c=='false' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_1__c) ||
                                                                     questionaireResponseObjectToUpdate.Answer_1__c=='')){
            errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_1__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_1__c');
        }
        
        else if(!$A.util.isUndefined(component.find('Answer_1__c')) && !component.find('Answer_1__c').checkValidity()){
           component.find('Answer_1__c').focus();
           isValidate=true;
        }
        else if(questionaireResponseObjectToUpdate.Answer_2__c=='true' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_3__c) || 
                                                                           questionaireResponseObjectToUpdate.Answer_3__c=='')){
            errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_3__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_3__c');
            
        }else if(questionaireResponseObjectToUpdate.Answer_3__c=='Other' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_4__c) || 
                                                                           questionaireResponseObjectToUpdate.Answer_4__c=='')){
            errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_4__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_4__c');
            
        }else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_37__c) || 
                                                                           questionaireResponseObjectToUpdate.Answer_37__c==''){
            errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_37__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_37__c');
            
        }
		if(!isValidate){
            if(callback) callback.call(this);
        }else{
            this.showToast(component,errorMessage);
        }
        
    },
    showToast : function(component,text,questionnaireResponseSobjectId) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Warning!",
            "message": text,
            duration:' 5000',
            mode: 'dismissible ',            
            type : 'warning',
        });
        toastEvent.fire();
    },
    
    setFocusForErrorMessage : function(component,componentId) {
        if(!$A.util.isUndefined(component.find(componentId))){
            component.find(componentId).focus();
        }
        
    }
})