({
    /* Method to assign values in radio Option */
    doInit : function(component,event,helper){
        
        var customMetadata = component.get('v.casubMandatoryNotificationCASetting');
        var year = customMetadata.Notification_Year__c;
        var exemptOption = [];
        exemptOption.push(
            {
                'label': 'Career break (parental leave, unemployment, illness)', 'value': 'Career break (parental leave, unemployment, illness)' 
            },
            {
                'label': 'Full member admitted between '+year, 'value': 'Full member admitted between '+year  
            },
            {
                'label': 'Retired', 'value': 'Retired'
            },
            {
                'label': 'Other', 'value': 'Other'
            }
        );
        component.set('v.exmReasonOption',exemptOption);
    },
    
    
    /* Method to display Option when Exempted option is selected */
    handleChange : function(component, event, helper) {
        if(event.getParam('checked')){
            component.set('v.questionnaireResponseSobject.Answer_2__c','true');
        }
        else{
            component.set('v.questionnaireResponseSobject.Answer_2__c','false');
            component.set('v.questionnaireResponseSobject.Answer_3__c','');
            component.set('v.questionnaireResponseSobject.Answer_4__c','');
        }
        
    },
    
    saveCPDInformation : function(component,event,helper){
        
        var questionnaireResponseSobject = component.get("v.questionnaireResponseSobject");
        var questionaireResponseObjectToUpdate = component.get("v.questionaireResponseObjectToUpdate");
        questionaireResponseObjectToUpdate.Question_1__c = questionnaireResponseSobject.Question_1__c;
        questionaireResponseObjectToUpdate.Answer_1__c = questionnaireResponseSobject.Answer_1__c;
        questionaireResponseObjectToUpdate.Question_2__c = questionnaireResponseSobject.Question_2__c;
        questionaireResponseObjectToUpdate.Answer_2__c = questionnaireResponseSobject.Answer_2__c == '' ? 'false' : questionnaireResponseSobject.Answer_2__c;
        questionaireResponseObjectToUpdate.Question_3__c = questionnaireResponseSobject.Question_3__c;
        questionaireResponseObjectToUpdate.Answer_3__c = questionnaireResponseSobject.Answer_3__c;
        questionaireResponseObjectToUpdate.Question_4__c = questionnaireResponseSobject.Question_4__c;
        questionaireResponseObjectToUpdate.Answer_4__c = questionnaireResponseSobject.Answer_4__c;
        questionaireResponseObjectToUpdate.Question_37__c = questionnaireResponseSobject.Question_37__c;
        questionaireResponseObjectToUpdate.Answer_37__c = questionnaireResponseSobject.Answer_37__c;
        if (!$A.util.isUndefinedOrNull(questionnaireResponseSobject.Id)) {
            questionaireResponseObjectToUpdate.Id = questionnaireResponseSobject.Id;
            questionaireResponseObjectToUpdate.Residential_Country__c = questionnaireResponseSobject.Residential_Country__c;
        }
        helper.upsertCPD(component,questionaireResponseObjectToUpdate); 
        
    }
    
})