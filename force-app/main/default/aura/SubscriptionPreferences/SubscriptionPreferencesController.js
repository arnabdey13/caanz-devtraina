({
    saveAndExit : function(c,e,h) {
        alert("leaving billing wizard");
    },
	goBack : function(cmp, event, helper) {
        var appEvent = $A.get("e.c:PageTransitionEvent");
        appEvent.setParams({source: cmp,
                            step : "prev"}).fire();
	}
})