({
    handleAIPoptions : function(cmp,event,helper){
        if(event.getParam('value') == 'Yes'){
            cmp.set('v.confirmAIP',true);
        }
        else{
            cmp.set('v.confirmAIP',false);
        }
    }
})