({
    validateText: function (component, event, helper) {

        // note: cannot check if common errors are present here because the order of execution
        // of change handlers is indeterminite i.e. race condition

        var type = component.get("v.typeRequired");

        if (type) {
            var value = component.get("v.value");
            switch (type) {
                case "Integer":
                    if (isNaN(parseInt(value))) {
                        return ["This field must be a number"];
                    } else {
                        return [];
                    }
                    break;
                case "Email":
                    if (!this.isValidEmail(value)) {
                        return ["The email address you entered is not valid"];
                    } else {
                        return [];
                    }
                case "Any":
                    return [];
                    break;
                default:
                    throw "Unsupported type: " + type + " for "
                    + component.get("v.loadKey") + "." + component.get("v.field");
            }
        } else {
            return [];
        }
    },
    isValidEmail: function (email) {
        var atIndex = email.indexOf('@');
        var dotIndex = email.lastIndexOf('.');
        var validAt = atIndex != -1 && atIndex != 0 && atIndex != email.length - 1;
        var validDot = dotIndex != -1 && dotIndex != 0 && dotIndex != email.length - 1;
        var validPositions = dotIndex > atIndex;
        var noDotAfterAt = email.substring(atIndex + 1, atIndex + 2) != '.';

        return validAt && validDot && validPositions && noDotAfterAt;
    },
})