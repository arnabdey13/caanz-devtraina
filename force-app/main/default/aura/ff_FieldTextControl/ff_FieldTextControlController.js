({
    handleKeyUp: function (component, event, helper) {
        if (component.get("v.updateOn") == "keyUp") {
            component.set("v.value", event.target.value);
        }
    },
    handleBlur: function (component, event, helper) {
        component.set("v.value", event.target.value);
    },
    validateText: function(component, event, helper) {
        event.stopPropagation(); // since handling it's own event, nobody else cares
        var textSpecificErrors = helper.validateText(component, event, helper);
        component.set("v.specificValidationErrors", textSpecificErrors);
    }
})