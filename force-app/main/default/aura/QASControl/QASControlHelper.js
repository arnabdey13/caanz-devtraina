({
    sendToService: function (component, method, params, isStorable, callback) {
        var action = component.get(method);
        //set parameters send to apex
        if (params) {
            action.setParams(params);
        }
        action.setBackground(true);

        action.setCallback(this,
            $A.getCallback(
                function (response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        callback(response.getReturnValue());
                    } else {
                        this.optOut(component);
                        this.showQASError(component);
                        //fire adobe event to notify about apex call error
                        this.fireAdobeEvent(component, "QASAddressServiceCall.error");
                    }
                }));
        $A.enqueueAction(action);
    },
    //this is a HOF because a closure over the component is required
    getFindAggregateControlsCallback: function (component) {
        var helper = this;
        return function (aggregateComponent) {
            var field = aggregateComponent.get("v.field");
            if (!$A.util.isEmpty(field)) {
                var attributeName = helper.getAggregateControlAttributeName(field);
                component.set(attributeName, aggregateComponent);
            }
        };
    },
    isDriverLoadComplete: function (component) {
        var country = !$A.util.isEmpty(component.get("v.countryCmp"));
        var street = !$A.util.isEmpty(component.get("v.streetCmp"));
        var city = !$A.util.isEmpty(component.get("v.cityCmp"));
        var state = !$A.util.isEmpty(component.get("v.stateCmp"));
        var postcode = !$A.util.isEmpty(component.get("v.postalCodeCmp"));

        return country && street && city && state && postcode;
    },
    getAggregateControlAttributeName: function (field) {
        if (field.indexOf("Country") != -1) {
            return "v.countryCmp";
        } else if (field.indexOf("Street") != -1) {
            return "v.streetCmp";
        } else if (field.indexOf("City") != -1) {
            return "v.cityCmp";
        } else if (field.indexOf("State") != -1) {
            return "v.stateCmp";
        } else if (field.indexOf("Code") != -1) {
            return "v.postalCodeCmp";
        } else {
            throw Error("No match: " + field);
        }
    },
    setCountryAndStateOptions: function (component) {
        var countryValues = component.get("v.countryValues");
        var countryCmp = component.get("v.countryCmp");

        var stateValues = component.get("v.stateValues");
        var stateCmp = component.get("v.stateCmp");
        if (!$A.util.isEmpty(countryCmp) && !$A.util.isEmpty(stateCmp)) {
            countryCmp.set("v.options", countryValues);
            stateCmp.set("v.allOptions", stateValues);
            stateCmp.set("v.controllerValue", countryCmp.get("v.value"));
        }

    },
    fireApplicationEvent: function (component, eventType, params) {
        var appEvent = $A.get(eventType);
        if (appEvent) {
            if (params) {
                appEvent.setParams(params);
            }
            appEvent.fire();
        } else {
            $A.log("no event registered: " + eventType);
        }
    },
    prepareControlStateAsync: function (component, helper) {
        window.setTimeout(
            $A.getCallback(function () {
                helper.prepareControlState(component);
            }), 50
        )
    },
    prepareControlState: function (component) {
        if (component.get("v.driverLoadComplete") &&
            component.get("v.countryAndStateLoadComplete") && !component.get("v.isCopiedAddress") && !component.get("v.suppressValueChangeEventHandling")) {

            var controlState;
            console.log('component.get("v.currentStage")-->'+component.get("v.currentStage"));
            console.log('component.get("v.visibleForStage")-->'+component.get("v.currentStage"));
            if (component.get("v.currentStage") != component.get("v.visibleForStage")) {
                controlState = "ALL_HIDDEN";
            } else {
                //get the input to derive the visibility and disabled state
                var controlStateInput = this.getControlStateInput(component);

                //check if the country is opt-out country and updated corresponding parameters.
                //THis is needed because the autocomplete control of forms common automatically sets itself to optedIn on focus
                if(this.isOptOutCountry(controlStateInput.country)){
                    component.get("v.streetCmp").set("v.optedOut", true);
                    controlStateInput.optedOut = true;
                }
                //set the controlState attribute
                controlState = this.deriveControlState(controlStateInput);
            }

            component.set("v.controlState", controlState);
            //update the state
            this.updateDerivedControlState(component)
        }

    },
    updateDerivedControlState: function (component) {
        //derive the visibility and disabled state

        var derivedVisibilityObject = this.createControlStateObject(component, component.get("v.controlState"));
        if (!component.get("v.isCopiedAddress")) {
            //apply the visibility changes
            this.setDerivedVisibility(component, derivedVisibilityObject);
        }
    },
    //returns an object that is send to deriveControlState function
    getControlStateInput: function (component) {
        var result = {
            country: component.get("v.countryCmp").get("v.value"),
            street: component.get("v.streetCmp").get("v.value"),
            city: component.get("v.cityCmp").get("v.value"),
            state: component.get("v.stateCmp").get("v.value"),
            stateOptions: component.get("v.stateCmp").get("v.options"),
            postcode: component.get("v.postalCodeCmp").get("v.value"),
            driverLoadComplete: component.get("v.driverLoadComplete"),
            countryAndStateLoadComplete: component.get("v.countryAndStateLoadComplete"),
            optedOut: component.get("v.optedOut"),
            error: component.get("v.error")
        }
        return result;
    },
    deriveControlState: function (params) {

        var loadComplete = params.driverLoadComplete && params.countryAndStateLoadComplete;
        //opt-in mode 1 - only country is selected and street may or may not be filled in
        //this is the most common use-case
        if (loadComplete && !$A.util.isEmpty(params.country) &&
            $A.util.isEmpty(params.city) &&
            $A.util.isEmpty(params.state) &&
            $A.util.isEmpty(params.postcode) &&
            !params.optedOut) {

            return "OPT_IN";

        }
        //opt-in mode 2 - only country ans state is selected. All other fields are blank
        //this is a very unique case and mostly solves corrupted data
        else if (loadComplete && !$A.util.isEmpty(params.country) &&
            $A.util.isEmpty(params.city) && !$A.util.isEmpty(params.state) &&
            $A.util.isEmpty(params.postcode) && !params.optedOut) {

            return "OPT_IN";

        }
        //opt-in-selected mode 1 - country and street are completed as well as at least city, state or postcode and states have no options
        //this is the most common use case for opted in addresses
        else if (loadComplete && !$A.util.isEmpty(params.country) && !$A.util.isEmpty(params.street) &&
            (!$A.util.isEmpty(params.city) || !$A.util.isEmpty(params.state) || !$A.util.isEmpty(params.postcode)) &&
            $A.util.isEmpty(params.stateOptions) && !params.optedOut) {

            return "OPT_IN_SELECTED_STATE_DISABLED";

        }
        //opt-in-selected mode 2 - country and street are completed as well as at least city, state or postcode
        //this is the most common use case for opted in addresses
        else if (loadComplete && !$A.util.isEmpty(params.country) && !$A.util.isEmpty(params.street) &&
            (!$A.util.isEmpty(params.city) || !$A.util.isEmpty(params.state) || !$A.util.isEmpty(params.postcode)) && !params.optedOut) {

            return "OPT_IN_SELECTED";

        }
        //opt-in-selected mode 3 - country is completed as well as at least city, state or postcode but street is blank and states have no options
        //this is required when a user is in opt-in mode and has selected an address via QAS but then deletes the street manually and saves.
        else if (loadComplete && !$A.util.isEmpty(params.country) && $A.util.isEmpty(params.street) &&
            (!$A.util.isEmpty(params.city) || !$A.util.isEmpty(params.state) || !$A.util.isEmpty(params.postcode)) &&
            $A.util.isEmpty(params.stateOptions) && !params.optedOut) {

            return "OPT_IN_SELECTED_STATE_DISABLED";

        }
        //opt-in-selected mode 4 - country is completed as well as at least city, state or postcode but street is blank
        //this is required when a user is in opt-in mode and has selected an address via QAS but then deletes the street manually and saves.
        else if (loadComplete && !$A.util.isEmpty(params.country) && $A.util.isEmpty(params.street) &&
            (!$A.util.isEmpty(params.city) || !$A.util.isEmpty(params.state) || !$A.util.isEmpty(params.postcode)) && !params.optedOut) {

            return "OPT_IN_SELECTED";

        } else if (loadComplete && !$A.util.isEmpty(params.country) &&
            $A.util.isEmpty(params.stateOptions) &&
            params.stateOptions.length == 0 &&
            params.optedOut) {

            return "OPT_OUT_STATE_DISABLED";

        } else if (loadComplete && !$A.util.isEmpty(params.country) && !$A.util.isEmpty(params.stateOptions) &&
            params.optedOut) {

            return "OPT_OUT_STATE_ENABLED";

        } else if (loadComplete && $A.util.isEmpty(params.country)) {

            return "ALL_DISABLED_BUT_COUNTRY";

        }
    },
    //determines the visibility and disabled state for all qas fields
    createControlStateObject: function (component, controlState) {
        var showValidationErrors = false;
        if (component.get("v.suppressErrorDisplayHide")) {
            showValidationErrors = true;
            component.set("v.suppressErrorDisplayHide", false);
        }
        //initial load state ALL_DISABLED
        var result = {
            country: {
                disabled: true,
                visible: true,
                showError: showValidationErrors
            },
            street: {
                disabled: true,
                optedOut: false,
                visible: true,
                showError: showValidationErrors
            },
            city: {
                disabled: true,
                visible: false,
                showError: showValidationErrors
            },
            state: {
                disabled: true,
                visible: false,
                showError: showValidationErrors
            },
            postcode: {
                disabled: true,
                visible: false,
                showError: showValidationErrors
            },
            errorMessage: {
                visible: false
            }
        };

        //no country selected but load is complete
        if (controlState == "ALL_DISABLED_BUT_COUNTRY") {
            result.country.disabled = false;
        }
        //country is selected and opt-in country and address is not present
        else if (controlState == "OPT_IN") {
            result.country.disabled = false;
            result.street.disabled = false;
        }
        //address select apex call is in progress for address that has no states
        else if (controlState == "OPT_IN_SELECT_STATE_DISABLED") {
            result.country.disabled = false;
            result.city.visible = true;
            result.postcode.visible = true;
        }
        //address select apex call is in progress
        else if (controlState == "OPT_IN_SELECT") {
            result.country.disabled = false;
            result.city.visible = true;
            result.state.visible = true;
            result.postcode.visible = true;
        }
        //country is selected and opt-in country and address is present and address has no states
        else if (controlState == "OPT_IN_SELECTED_STATE_DISABLED") {
            result.country.disabled = false;
            result.street.disabled = false;
            result.city.visible = true;
            result.postcode.visible = true;
        }
        //country is selected and opt-in country and address is present
        else if (controlState == "OPT_IN_SELECTED") {
            result.country.disabled = false;
            result.street.disabled = false;
            result.city.visible = true;
            result.state.visible = true;
            result.postcode.visible = true;
        }
        //country is selected and opt-out country and state has values
        else if (controlState == "OPT_OUT_STATE_ENABLED") {
            result.country.disabled = false;
            result.street.disabled = false;
            result.street.optedOut = true;
            result.city.disabled = false;
            result.city.visible = true;
            result.state.disabled = false;
            result.state.visible = true;
            result.postcode.disabled = false;
            result.postcode.visible = true;
        }
        //country is selected and opt-out country and state has no values
        else if (controlState == "OPT_OUT_STATE_DISABLED") {
            result.country.disabled = false;
            result.street.disabled = false;
            result.street.optedOut = true;
            result.city.disabled = false;
            result.city.visible = true;
            result.state.visible = false;
            result.postcode.disabled = false;
            result.postcode.visible = true;
        }
        //country is selected and opt-out country and error occurred
        else if (controlState == "OPT_OUT_ERROR") {
            result.country.disabled = false;
            result.street.disabled = false;
            result.street.optedOut = true;
            result.city.disabled = false;
            result.city.visible = true;
            result.state.disabled = false;
            result.state.visible = true;
            result.postcode.disabled = false;
            result.postcode.visible = true;
            result.errorMessage.visible = true;
        }
        //all fields should be hidden
        else if (controlState == "ALL_HIDDEN") {
            result.country.visible = false;
            result.street.visible = false;
        }

        result.country.showError = result.country.disabled ? false : showValidationErrors;
        result.street.showError = result.street.disabled ? false : showValidationErrors;
        result.city.showError = result.city.disabled ? false : showValidationErrors;
        result.state.showError = result.state.disabled ? false : showValidationErrors;
        result.postcode.showError = result.postcode.disabled ? false : showValidationErrors;
        return result;
    },
    setDerivedVisibility: function (component, params) {
        var countryCmp = component.get("v.countryCmp");
        countryCmp.set("v.disabled", params.country.disabled);
        countryCmp.set("v.visible", params.country.visible);
        countryCmp.set("v.displayValidationErrors", params.country.showError);

        var streetCmp = component.get("v.streetCmp");
        streetCmp.set("v.disabled", params.street.disabled);
        streetCmp.set("v.optedOut", params.street.optedOut);
        streetCmp.set("v.visible", params.street.visible);
        streetCmp.set("v.displayValidationErrors", params.street.showError);

        var cityCmp = component.get("v.cityCmp");
        cityCmp.set("v.disabled", params.city.disabled);
        cityCmp.set("v.visible", params.city.visible);
        cityCmp.set("v.displayValidationErrors", params.city.showError);

        var stateCmp = component.get("v.stateCmp");
        stateCmp.set("v.disabled", params.state.disabled);
        stateCmp.set("v.visible", params.state.visible);
        stateCmp.set("v.required", params.state.visible);
        stateCmp.validate();
        stateCmp.set("v.displayValidationErrors", params.state.showError);

        var postalCodeCmp = component.get("v.postalCodeCmp");
        postalCodeCmp.set("v.disabled", params.postcode.disabled);
        postalCodeCmp.set("v.visible", params.postcode.visible);
        postalCodeCmp.set("v.displayValidationErrors", params.postcode.showError);
    },
    isOptOutCountry: function (value) {
        return !(value == "AU") && !(value == "NZ");
    },
    //calls apex to get address suggestions
    getAddressSuggestions: function (component, searchTermValue, callback) {
        var countryCode = component.get("v.countryCmp").get("v.value");
        this.sendToService(component,
            "c.searchAddress", {
                searchTerm: searchTermValue,
                countryCode: countryCode,
                maxResults: 10
            },
            false,
            callback);

        //fire adobe event to notify about search
        this.fireAdobeEvent(component, "QASAddress.search");
    },
    //handles the logic to select and address and trigger the apex callout, before setting
    //the retrieved values into the address fields.
    selectAddress: function (component, helper, id) {
        //disable street field

        var states = component.get("v.stateCmp").get("v.options");

        if ($A.util.isEmpty(states)) {
            component.set("v.controlState", "OPT_IN_SELECT_STATE_DISABLED");
        } else {
            component.set("v.controlState", "OPT_IN_SELECT");
        }

        this.updateDerivedControlState(component);

        var streetCmp = component.get("v.streetCmp");
        //if id received make callout to QAS to get formatted address
        if (!$A.util.isEmpty(id) && streetCmp.get("v.value") != streetCmp.get("v.searchText")) {
            this.setSpinner(component, true);
            this.fireStageLockEvent(component, true);
            this.sendToService(component,
                "c.formatAddress",
                {
                    id: id,
                    countryCode: component.get("v.countryCmp").get("v.value"),
                },
                false,
                function (results) {
                    if (results.SUCCESS) {
                        component.set("v.allowQASAddressSelect", false);
                        helper.setQASAddressControls(component, results.SUCCESS);
                        component.set("v.allowQASAddressSelect", true);
                        if ($A.util.isEmpty(states)) {
                            component.set("v.controlState", "OPT_IN_SELECTED_STATE_DISABLED");
                        } else {
                            component.set("v.controlState", "OPT_IN_SELECTED");
                        }
                        helper.updateDerivedControlState(component);
                        helper.forceStateValueChange(component);
                    }
                    //if the results return an error opt-out immediately
                    else if (results.ERROR) {
                        helper.optOut(component);
                        helper.showQASError(component);
                        //fire adobe event to notify about select error
                        this.fireAdobeEvent(component, "QASAddressSelect.error");
                    }
                    helper.setSpinner(component, false);
                    helper.fireStageLockEvent(component, false);
                });
            //fire adobe event to notify about select
            this.fireAdobeEvent(component, "QASAddress.select");
        }
    },
    setQASAddressControls: function (component, address) {
        if (!$A.util.isEmpty(address)) {            
            component.get("v.countryCmp").set("v.value", address.countryCode);            
            var streetCmp = component.get("v.streetCmp")
            console.log('streetCmp-->'+streetCmp);
            streetCmp.set("v.value", $A.util.isEmpty(address.street) ? "" : address.street);
            streetCmp.set("v.searchText", $A.util.isEmpty(address.street) ? "" : address.street);
            component.get("v.cityCmp").set("v.value", $A.util.isEmpty(address.city) ? "" : address.city);
            component.get("v.stateCmp").set("v.value", $A.util.isEmpty(address.state) ? "" : address.state);
            component.get("v.postalCodeCmp").set("v.value", $A.util.isEmpty(address.postcode) ? "" : address.postcode);
            component.set("v.dpid", $A.util.isEmpty(address.dpid) ? "" : address.dpid);
            component.set("v.qasValidated", $A.util.isEmpty(address.qasValidated) ? false : address.qasValidated);

            if (!$A.util.isUndefinedOrNull(address.optedOut)) {
                component.set("v.optedOut", address.optedOut);
            }
        }
    },
    getFieldControlReferences: function () {
        return ["v.streetCmp", "v.cityCmp", "v.stateCmp", "v.postalCodeCmp"];
    },
    optOut: function (component) {
        component.set("v.optedOut", true);
        this.prepareControlState(component);
        this.setSpinner(component, false);
        component.set("v.dpid", "");
        component.set("v.qasValidated", false);
    },
    optIn: function (component) {
        component.set("v.optedOut", false);
        this.prepareControlState(component);
        this.setSpinner(component, false);
    },
    fireAdobeEvent: function (component, label) {
        this.fireApplicationEvent(component,
            "e.c:UserClickEvent",
            {
                sourceLabel: label
            });
    },
    fireStageLockEvent : function(component, lock){
        this.fireApplicationEvent(component,
            "e.c:ff_EventSetStageLock",
            {
                lock: lock,
                aggregateKey: component.get("v.aggregateKey")
            });
    },
    getFullAddress: function (component) {
        return {
            countryCode: component.get("v.countryCmp").get("v.value"),
            street: component.get("v.streetCmp").get("v.value"),
            city: component.get("v.cityCmp").get("v.value"),
            state: component.get("v.stateCmp").get("v.value"),
            postcode: component.get("v.postalCodeCmp").get("v.value"),
            dpid: component.get("v.dpid"),
            qasValidated: component.get("v.qasValidated"),
            optedOut: component.get("v.optedOut")
        }
    },
    getClearedAddress: function (component) {
        return {
            countryCode: component.get("v.countryCmp").get("v.value"),
            street: "",
            city: "",
            state: "",
            postcode: "",
            dpid: "",
            qasValidated: ""
        }
    },
    setSpinner: function (component, visible) {
        var spinnerCmp = component.find("spinner");
        spinnerCmp.set("v.isLoading", visible);
    },
    handleLoad: function (component, helper) {
        this.fireApplicationEvent(component,
            "e.c:ff_EventFindAggregateControls",
            {
                aggregateKey: component.get("v.aggregateKey"),
                source: component,
                callback: this.getFindAggregateControlsCallback(component)
            });

        window.setTimeout(
            $A.getCallback(function () {
                var driverLoadComplete = helper.isDriverLoadComplete(component);
                component.set("v.driverLoadComplete", driverLoadComplete);

                if (driverLoadComplete) {
                    component.get("v.streetCmp").set("v.allowDriverSearch", false);
                }

                //sets the country and state values in the select component options
                helper.setCountryAndStateOptions(component);
                helper.prepareControlState(component);
                
                // RXP - START - 14/08/2019 - Fire value change event for country as this is required for AppForms_CPPQuestions component
                var country = component.get("v.countryCmp");
				helper.fireValueChange(country, true);
                // RXP - END - 14/08/2019

                
                // //if the state is disabled fire value change event to report correct validity
                // //this is needed because the states may report as invalid due to the disabling being delayed by 50ms
                helper.forceStateValueChange(component);
            }), 50
        );
    },
    setQASFields: function (component, event, helper) {
        var readData = event.getParam("data");
        var dpidLoadKey = component.get("v.dpidLoadKey");
        var dpidField = component.get("v.dpidField");
        var qasValidatedLoadKey = component.get("v.qasValidatedLoadKey");
        var qasValidatedField = component.get("v.qasValidatedField");

        var dpidSobject = this.getLoadedSObject(component, readData, dpidLoadKey);
        var qasValidatedSobject = this.getLoadedSObject(component, readData, qasValidatedLoadKey);
        if (!$A.util.isEmpty(dpidSobject[dpidField])) {
            component.set("v.dpid", dpidSobject[dpidField]);
        }
        if (!$A.util.isEmpty(qasValidatedSobject[dpidField])) {
            component.set("v.qasValidated", qasValidatedSobject[qasValidatedField]);
        }
    },
    getLoadedSObject: function (component, readData, loadKey) {
        var records = readData.records[loadKey]; // only if loadKey matches
        
        if (records && !$A.util.isEmpty(records)) { // and records are present
            if (records[0].single) { // and the loadKey is for a single record set
                var firstRecordInList = records[0].sObjectList[0] // first record since it's a single record set
                // if the form is an insert form, then there may be no records in the wrapper so check for that
                if (firstRecordInList) {
                    return firstRecordInList
                }
            }
        }
    },
    showQASError: function (component) {
        var errorMessage = component.find("errorMessage");
        errorMessage.set("v.isVisible", true);
        window.setTimeout(
            $A.getCallback(function () {
                errorMessage.set("v.isVisible", false);
            }), 3000
        );
    },
    fireValueChange: function (component, dontFire) {

        // TODO this really should not be in control since value only makes sense for FieldControl

        //do not fire value change event for specific controls
        if (!component.get("v.suppressValueChange")) {

            var loadKey = component.get("v.loadKeyOverride") || component.get("v.loadKey");
            var field = component.get("v.fieldOverride") || component.get("v.field");

            this.fireApplicationEvent(component,
                "e.c:ff_EventValueChange",
                {
                    loadKey: loadKey,
                    field: field,
                    value: component.get("v.value"),
                    valid: component.get("v.isValid"),
                    source: component,
                    stage: component.get("v.visibleForStage"),
                    ignore: component.get("v.valueChangeIgnored"),
                    dontFire: dontFire
                });
        }
    },
    //this function is called every time an address changes. It must be in place to force
    //that a state reports itself as valid if it is disabled.
    forceStateValueChange: function (component) {
        var stateCmp = component.get("v.stateCmp");
        if (stateCmp.get("v.disabled")) {
            this.fireValueChange(component.get("v.stateCmp"));
        }
    }
})