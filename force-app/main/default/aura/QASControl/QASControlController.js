({
    doInit: function (component, event, helper) {
        helper.sendToService(component, "c.loadCountryAndStates", undefined, true, function (jsonVal) {
            //set the country and state attributes with the values returned from apex
            var val = JSON.parse(jsonVal);

            component.set("v.countryValues", val['User.Countries']);
            component.set("v.stateValues", val['User.States']);

            component.set("v.countryAndStateLoadComplete", true);
            //sets the country and state values in the select component options
            helper.setCountryAndStateOptions(component);
            helper.prepareControlState(component);
        });
    },
    handleLoad: function (component, event, helper) {
        helper.setQASFields(component, event);
        helper.handleLoad(component, helper);
    },
    apiHandleLoad: function (component, event, helper) {
        helper.handleLoad(component, helper);
    },
    handleValueChangeEvent: function (component, event, helper) {
        if (!component.get("v.suppressValueChangeEventHandling")) {
            var params = event.getParams();            
            var aggregateKey = params.source.get("v.aggregateKey")			
            if (!$A.util.isEmpty(aggregateKey) && aggregateKey == component.get("v.aggregateKey")) {                
                var countryCmp = component.get("v.countryCmp");                
                //on country change determine opt-out requirement and derive control state
                if (!$A.util.isEmpty(countryCmp) &&
                   	!params.dontFire && // RXP Change - Need to still invoke this (see helper method handleLoad)
                    params.field == component.get("v.countryCmp").get("v.field") && !component.get("v.isCopiedAddress")) {                    
                    component.set("v.allowQASAddressSelect", false);                    
                    //clear the address
                    helper.setQASAddressControls(component, helper.getClearedAddress(component));
                    console.log('after setQASAddressControls-->');
                    component.set("v.allowQASAddressSelect", true);
                    console.log('after allowQASAddressSelect-->');
                    component.set("v.optedOut", helper.isOptOutCountry(params.value));
                    console.log('after optedOut-->');
                    helper.prepareControlStateAsync(component, helper);
                    console.log('after prepareControlStateAsync-->');
                }

                var streetCmp = component.get("v.streetCmp");
                //on street change determine check if street select is allowed and select street
                if (!$A.util.isEmpty(streetCmp) &&
                    params.field == streetCmp.get("v.field") &&
                    component.get("v.allowQASAddressSelect") && !component.get("v.optedOut") && !component.get("v.isCopiedAddress")) {
                    var street = component.get("v.streetCmp");
                    if (!$A.util.isEmpty(street.get("v.value"))) {
                        helper.selectAddress(component, helper, street.get("v.value"));
                    }
                }
            }
        }
    },
    handleAutoCompleteAddressSearch: function (component, event, helper) {
        var params = event.getParams();

        if (params.field == component.get("v.streetCmp").get("v.field")) {
            //invoke callout to apex to get suggestions and
            //pass those back to ff_FieldAutoComplete
            var callback = function (results) {
                //on successful response of results return those to callback
                if (results.SUCCESS) {
                    params.callback(results.SUCCESS);
                }
                //if the results return an error opt-out immediately
                else if (results.ERROR) {
                    helper.optOut(component);
                    helper.showQASError(component);
                    //fire adobe event to notify about search error
                    helper.fireAdobeEvent(component, "QASAddressSearch.error");
                }
            };
            helper.getAddressSuggestions(component, params.searchTerm, callback);
        }
    },
    handleDpidChange: function (component, event, helper) {
        if (component.get("v.driverLoadComplete") && component.get("v.countryAndStateLoadComplete")) {
            helper.fireApplicationEvent(component,
                "e.c:ff_EventValueChange",
                {
                    loadKey: component.get("v.dpidLoadKey"),
                    field: component.get("v.dpidField"),
                    value: component.get("v.dpid"),
                    valid: true,
                    source: component
                });
        }
    },
    handleQasValidatedChange: function (component, event, helper) {
        if (component.get("v.driverLoadComplete") && component.get("v.countryAndStateLoadComplete")) {
            helper.fireApplicationEvent(component,
                "e.c:ff_EventValueChange",
                {
                    loadKey: component.get("v.qasValidatedLoadKey"),
                    field: component.get("v.qasValidatedField"),
                    value: component.get("v.qasValidated"),
                    valid: true,
                    source: component
                });
        }
    },
    handleAutoCompleteOptOutEvent: function (component, event, helper) {
        var params = event.getParams();
        var streetCmp = component.get("v.streetCmp");
        var countryCode = component.get("v.countryCmp").get("v.value");
        if (params.loadKey == streetCmp.get("v.loadKey") && params.field == streetCmp.get("v.field")) {
            if (params.optedOut || helper.isOptOutCountry(countryCode) && !component.get("v.optedOut")) {
                helper.optOut(component);
            } else {
                helper.optIn(component);
            }

        }
    },
    apiCopyAddress: function (component, event, helper) {
        var params = event.getParam("arguments");
        var sourceAddress = helper.getFullAddress(component);


        params.targetReference.set("v.suppressValueChangeEventHandling", true);
        if (params.checkboxValue) {
            //always force the target qas control to hide themselves
            //this is needed even though visibility is controlled by ff_* framework dependent viz logic,
            //value changes are handled by the framework and this control. To work around this race condition
            // it is important to force re-rendering every time an address is copied
            params.targetReference.set("v.controlState", "ALL_HIDDEN");
            params.targetReference.apiUpdateDerivedControlState();
            params.targetReference.set("v.isCopiedAddress", true);
            params.targetReference.set("v.allowQASAddressSelect", false);
            window.setTimeout(
                $A.getCallback(function () {
                    params.targetReference.apiSetAddress(sourceAddress);
                    params.targetReference.get("v.stateCmp").set("v.disabled", true);
                    helper.forceStateValueChange(params.targetReference);
                }), 50
            );

        } else {
            window.setTimeout(
                $A.getCallback(function () {
                    params.targetReference.apiSetAddress(sourceAddress);
                    params.targetReference.set("v.isCopiedAddress", false);
                    params.targetReference.set("v.suppressValueChangeEventHandling", false);
                    params.targetReference.set("v.allowQASAddressSelect", true);
                    params.targetReference.apiPrepareControlState();
                }), 50
            );
        }

    },
    apiSetAddress: function (component, event, helper) {
        var params = event.getParam("arguments");
        helper.setQASAddressControls(component, params.address);
    },
    apiUpdateDerivedControlState: function (component, event, helper) {
        helper.updateDerivedControlState(component);
    },
    apiPrepareControlState: function (component, event, helper) {
        helper.prepareControlState(component);
    },
    handleFindAggregateControls: function (component, event, helper) {
        var params = event.getParams();
        var aggregateKey = component.get("v.aggregateKey");

        var isSelf = params.source.getGlobalId() == component.getGlobalId();
        if (params.aggregateKey == aggregateKey && !isSelf) {
            params.callback(component);
        }
    },
    //handle stage change event to re-render the visibility of the QAS fields when correct stage is loaded.
    //This allows to hide all QAS fields on load and on other stages and will just render to expected behaviour (vis, enabled, disabled etc.) only if the stage matches
    handleStageChange: function (component, event, helper) {
        component.set("v.mostRecentStage", event.getParam("stage"));
    },
    handleNewStage: function (component, event, helper) {
        if (component.get("v.visibleForStage") == event.getParam("value")) {
            //delay the rendering of the layout to allow forms framework dependent
            // rendering to render first, before applying changes.
            helper.prepareControlStateAsync(component, helper);
        }
    },
    handleSetValidationEvent: function (component, event, helper) {
        component.set("v.suppressErrorDisplayHide", true);
    }
})