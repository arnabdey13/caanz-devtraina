({
    handleEditEmployment: function(cmp, event, helper) {
        var employmentHistoryRecordEvent = cmp.getEvent("casub_employmentHistoryRecordEvent");
        employmentHistoryRecordEvent.setParams({"employmentId": cmp.get("v.employmentHistory.Id")});
        employmentHistoryRecordEvent.fire();
	}
})