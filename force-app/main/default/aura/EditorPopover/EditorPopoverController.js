({
    doInit : function(component, event, helper) {
        var contents = component.get("v.contents");
        var offset = 40 + (15 * (contents.length / 47));
        component.set("v.positionTop", "-"+offset+"px");
    },
    handleStaticResourceConfig : function(component, event, helper) {

        component.set("v.resourcePrefix", event.getParam("resourcePrefix"));
    },
    showHint : function(component) {
        $A.util.removeClass(component.find("popover"), "slds-hide");
    },
    hideHint : function(component) {
        $A.util.addClass(component.find("popover"), "slds-hide");
    }   
})