({
    doInit : function(component,event,helper){
       
        
    },
    handleChange4cpp : function(component,event,helper){
        if(event.getParam('value') == 'Yes'){
            component.set("v.accSerpubrewardValue" , true);
        }
        else{
            component.set("v.accSerpubrewardValue" , false);
            component.set("v.questionnaireResponseSobject.Answer_41__c" , '');
            component.set("v.selectedServiceOptions" , []);
        }
    },
    handleChange5cpp : function(component,event,helper){
        if(event.getParam('value') == 'Yes'){
            component.set("v.principleOfPractise" , true);
        }
        else{
            component.set("v.principleOfPractise" , false);
            component.set("v.selectedServiceOptions" , []);
            
        }
    },
    handleChange6cpp : function(component,event,helper){
        if(event.getParam('value') == 'Yes'){
            component.set("v.intendtoProvideAS" , true);
        }else{
            component.set("v.intendtoProvideAS" , false);
            component.set("v.questionnaireResponseSobject.Answer_44__c" , '');
        }
    },
    handleChange7cpp : function(component,event,helper){
        if(event.getParam('value') == 'Yes'){
            component.set('v.continuetoHoldCPP' , true);
        }
        else{
            component.set('v.continuetoHoldCPP' , false);
            component.set('v.questionnaireResponseSobject.Answer_45__c' ,'');
        }
    },
    saveCertificateofPublicPractice : function(component,event,helper){
        var questionnaireResponseSobject = component.get("v.questionnaireResponseSobject");
                var questionaireResponseObjectToUpdate = component.get("v.questionaireResponseObjectToUpdate");
                questionaireResponseObjectToUpdate.Question_40__c = questionnaireResponseSobject.Question_40__c;
                questionaireResponseObjectToUpdate.Answer_40__c = questionnaireResponseSobject.Answer_40__c;
                
                questionaireResponseObjectToUpdate.Question_41__c = questionnaireResponseSobject.Question_41__c;
                questionaireResponseObjectToUpdate.Answer_41__c = questionnaireResponseSobject.Answer_41__c;
                
                questionaireResponseObjectToUpdate.Question_42__c = questionnaireResponseSobject.Question_42__c;
                var selectedServiceOptionsArray = component.get("v.selectedServiceOptions");
                questionaireResponseObjectToUpdate.Answer_42__c='';
                selectedServiceOptionsArray.forEach(function(options) {
                    if($A.util.isUndefinedOrNull(questionaireResponseObjectToUpdate.Answer_42__c) || $A.util.isEmpty(questionaireResponseObjectToUpdate.Answer_42__c)){
                        questionaireResponseObjectToUpdate.Answer_42__c=options;
                    }else{
                        questionaireResponseObjectToUpdate.Answer_42__c+=',' + options;
                    }
                });
                
                questionaireResponseObjectToUpdate.Question_43__c = questionnaireResponseSobject.Question_43__c;
                questionaireResponseObjectToUpdate.Answer_43__c = questionnaireResponseSobject.Answer_43__c;
                
                questionaireResponseObjectToUpdate.Question_44__c = questionnaireResponseSobject.Question_44__c;
                questionaireResponseObjectToUpdate.Answer_44__c = questionnaireResponseSobject.Answer_44__c;
                
                questionaireResponseObjectToUpdate.Question_45__c = questionnaireResponseSobject.Question_45__c;
                questionaireResponseObjectToUpdate.Answer_45__c = questionnaireResponseSobject.Answer_45__c;
                
                if (!$A.util.isUndefinedOrNull(questionnaireResponseSobject.Id)) {
                    questionaireResponseObjectToUpdate.Id = questionnaireResponseSobject.Id;
                    questionaireResponseObjectToUpdate.Residential_Country__c = questionnaireResponseSobject.Residential_Country__c;
                }
        helper.upsertCertificateofPublicPractice(component,questionaireResponseObjectToUpdate); 
    }
})