({
    upsertCertificateofPublicPractice: function(component,questionaireResponseObjectToUpdate,callBackParams){
        var subscriptionSobject = component.get("v.globalSubscriptionSobject");
        if($A.util.isUndefinedOrNull(subscriptionSobject.Obligation_Sub_Components__c )){
            subscriptionSobject.Obligation_Sub_Components__c = 'Certificate of Public Practice;';
        }
        else if(!subscriptionSobject.Obligation_Sub_Components__c.includes("Certificate of Public Practice")){
            if(subscriptionSobject.Obligation_Sub_Components__c.substring(subscriptionSobject.Obligation_Sub_Components__c.length - 1) == ';')
                subscriptionSobject.Obligation_Sub_Components__c += 'Certificate of Public Practice;';
                else
                    subscriptionSobject.Obligation_Sub_Components__c += ';Certificate of Public Practice;';
        }
        this.validateCertificateQuestionsAndAnswers(component,questionaireResponseObjectToUpdate,function(){
            var param = {
                'personAccountId':component.get("v.personAccountId"),
                'questionaireResponseObjectToUpdate':questionaireResponseObjectToUpdate,
                'selectedCountry':component.get("v.selectedCountry"),
                'whichSectionSave' : '',
                'subscriptionSobject': subscriptionSobject
            };
            this.enqueueAction(component, "upsertQuestionaireResponse", param, function(error, data){
                if(error){
                    component.set("v.errorMessage",error);
                }else if(!$A.util.isUndefinedOrNull(callBackParams) && callBackParams.callback) {
                    if (!$A.util.isUndefinedOrNull(questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id)) {
                        component.set("v.questionnaireResponseSobject.Id",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id);
                        component.set("v.questionnaireResponseSobject.Residential_Country__c",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Residential_Country__c);
                        component.set("v.globalSubscriptionSobject",questionResponseSubscriptionWrappper.subscriptionSObject);
                        /*
                        component.set("v.questionnaireResponseSobject.Id",data.Id);
                        component.set("v.questionnaireResponseSobject.Residential_Country__c",data.Residential_Country__c);
                        */
                        }
                        component.set("v.errorMessage",'');
                        callBackParams.callback(); 
                    }else{
                        if (!$A.util.isUndefinedOrNull(questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id)) {
                            component.set("v.questionnaireResponseSobject.Id",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id);
                            component.set("v.questionnaireResponseSobject.Residential_Country__c",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Residential_Country__c);
                            component.set("v.globalSubscriptionSobject",questionResponseSubscriptionWrappper.subscriptionSObject);
                            /*
                        component.set("v.questionnaireResponseSobject.Id",data.Id);
                        component.set("v.questionnaireResponseSobject.Residential_Country__c",data.Residential_Country__c);
                        */
                        }
                    }
                    
                });
            
        })
    },
    validateCertificateQuestionsAndAnswers : function(component,questionaireResponseObjectToUpdate,callback){
        var answer40 = questionaireResponseObjectToUpdate.Answer_40__c;
        var answer41 = questionaireResponseObjectToUpdate.Answer_41__c;
        var answer42 = questionaireResponseObjectToUpdate.Answer_42__c;
        var answer43 = questionaireResponseObjectToUpdate.Answer_43__c;
        var answer44 = questionaireResponseObjectToUpdate.Answer_44__c;
        var answer45 = questionaireResponseObjectToUpdate.Answer_45__c;
        
        var question40 = questionaireResponseObjectToUpdate.Question_40__c;
        var question41 = questionaireResponseObjectToUpdate.Question_41__c;
        var question42 = questionaireResponseObjectToUpdate.Question_42__c;
        var question43 = questionaireResponseObjectToUpdate.Question_43__c;
        var question44 = questionaireResponseObjectToUpdate.Question_44__c;
        var question45 = questionaireResponseObjectToUpdate.Question_45__c;
        var errorMessage = '';
        var isValidate=false;
         var isCPPEXEMPT = component.get('v.isCPPExempt');
        console.log('--exempt--'+isCPPEXEMPT);
        debugger;
        if((!isCPPEXEMPT) && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_40__c) || questionaireResponseObjectToUpdate.Answer_40__c=='')){
            errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_40__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_40__c');
        }else if(answer40=='Yes' && ($A.util.isUndefined(answer41) || answer41=='')){
            errorMessage = 'Please select valid option for  ' + question41;
            isValidate=true;
        }else if(answer41=='Yes' && ($A.util.isUndefined(answer42) || answer42=='')){
            errorMessage = 'Please select valid option for  ' + question42;
            isValidate=true;
        }else if(isCPPEXEMPT && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_43__c) || questionaireResponseObjectToUpdate.Answer_43__c=='')){
            errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_43__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_43__c');
        }else if(answer43=='Yes' && ($A.util.isUndefined(answer44) || answer44=='')){
            errorMessage = 'Please select valid option for  ' + question44;
            isValidate=true;
        }else if(answer44=='Yes' && ($A.util.isUndefined(answer45) || answer45=='')){
            errorMessage = 'Please select valid option for  ' + question45;
            isValidate=true;
        }
        
        if(!isValidate){
            if(callback) callback.call(this);
        }else{
            //component.set("v.errorMessage",errorMessage);
            this.showToast(component,errorMessage);
            
            //component.find('certificateError').focus();
        }
        
    },
    enqueueAction: function(component, method, params, callback){
        this.toggleSpinner(component, true);
        var action = component.get("c." + method); 
        if(params) action.setParams(params);
        action.setCallback(this, function(response){
            this.toggleSpinner(component, false);
            if(response.getState() === "SUCCESS") {
                if(callback) callback.call(this, null, response.getReturnValue());
            } else if(response.getState() === "ERROR") {
                var message = 'Unknown error'; 
                var errors = response.getError();
                if (!$A.util.isEmpty(errors)) {
                    message = errors[0].message;
                }
                console.error(message);
                if(callback) callback.call(this, message);
            }
        });
        $A.enqueueAction(action);
    },
    toggleSpinner: function(component, show) {
        var spinner = component.find("cppSpinner");
        $A.util.addClass(spinner, show ? "slds-show" : "slds-hide");
        $A.util.removeClass(spinner, !show ? "slds-show" : "slds-hide");
    },
    showToast : function(component,text,questionnaireResponseSobjectId) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Warning!",
            "message": text,
            type : 'warning',
            mode: 'dismissible',
            duration :10000
        });
        toastEvent.fire();
    },
      /* Method to display Text box when 'No' option is selected */
    setFocusForErrorMessage : function(component,componentId) {
        if(!$A.util.isUndefined(component.find(componentId))){
            component.find(componentId).focus();
        }
      
    }
})