({
    stages: function (stage, total) {
        var result = [];
        for (var i = 0; i < total; i++) {
            result.push({label: "Step " + (i + 1), state: "INCOMPLETE", stageNumber: i});
        }
        for (var i = 0; i < stage; i++) {
            result[i].state = "COMPLETE";
        }
        result[stage].state = "ACTIVE";
        return result;
    },
    fireStageChange: function (stage, stages, headings, navigationAllowed) {
        // TODO cannot use inherited fireApplicationEvent here for some reason. why?
        var appEvent = $A.get("e.c:ff_EventStageChange");
        return this.fireAppEvent(appEvent, {
            stage: stage,
            stages: stages,
            headings: headings,
            navigationAllowed: navigationAllowed

        });
    },
    fireAppEvent: function (event, params) {
        return function (context) {
            event.setParams(params).fire();
            return context;
        }
    }
})