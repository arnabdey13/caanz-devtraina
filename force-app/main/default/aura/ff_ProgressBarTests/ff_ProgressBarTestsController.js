({
    doInit: function (component, event, helper) {

        var test = helper.driver(component, event, helper);

        var p1 = component.find("p1");

        var startTests = test.start({
            focused: p1,
            description: "the first progress bar"
        })

        startTests

            .then(helper.fireStageChange(1, helper.stages(1, 5), {}, true))
            .then(test.sleep(1000))
            .then(helper.fireStageChange(2, helper.stages(2, 5), {}, true))
            .then(test.sleep(1000))
            .then(helper.fireStageChange(3, helper.stages(3, 5), {}, false))

                //TODO coloring the stages based on competeness
                //TODO only coloring stages that are less or equal to furthest stage
                //TODO any colored stage is clickable

            //////////// END OF TESTS ////////////

            // always include these fns to handle the end of the test
            .then(test.pass).catch(test.fail);

    }
})