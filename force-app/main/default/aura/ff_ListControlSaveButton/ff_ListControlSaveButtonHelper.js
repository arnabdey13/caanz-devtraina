({
    fireSave: function(component) {
        component.getEvent("crudEvent")
            .setParams({
                operation: "SAVE"
            })
            .fire();
    }
})