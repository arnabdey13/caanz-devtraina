({
	doInit: function(cmp, event, helper) {
		helper.setActiveProgressItem(cmp, 0);
	},
	activateProgressItem: function(cmp, event, helper){
		var activeItemIndex = event.currentTarget.getAttribute("data-progressItemIndex");
		helper.triggerSave(cmp, function(){
			helper.setActiveProgressItem(cmp, Number.parseInt(activeItemIndex));
		});
	},
	activateNextProgressItem: function(cmp, event, helper){
		var activeItemIndex = helper.getActiveProgressItemIndex(cmp);
		helper.triggerSave(cmp, function(){
			helper.setActiveProgressItem(cmp, activeItemIndex + 1);
		});
	},
	activatePreviousProgressItem: function(cmp, event, helper){
		var activeItemIndex = helper.getActiveProgressItemIndex(cmp);
		helper.triggerSave(cmp, function(){
			helper.setActiveProgressItem(cmp, activeItemIndex - 1);
		});
	},
	closeModal: function(cmp, event, helper){
		helper.finishWizard(cmp);
	}
})