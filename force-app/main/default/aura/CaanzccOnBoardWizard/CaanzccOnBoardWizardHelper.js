({
	PROGRESS_ITEMS: [{
		id: "CaanzWelcomeVideo",
		screenTitle: "Welcome Video"
	}, {
		id: "CaanzAddressDetails",
		screenTitle: "Confirm Address"
	}, {
		id: "CaanzMySettings",
		screenTitle: "My CA Settings",
		params: {
			isOnBoardWizard: "true"
		}
	}, {
		id: "CaanzMyCommunicationPreferences",
		screenTitle: "My Communication Preferences",
		params: {
			isOnBoardWizard: "true",
			showActionButtons: false,
			saveType: "all" 
		}
	}],
	getProgressItems: function(cmp){
		var progressItems = cmp.get("v.progressItems");
		return !$A.util.isEmpty(progressItems) ? progressItems : this.PROGRESS_ITEMS;
	},
	getActiveProgressItemIndex: function(cmp){
		return cmp.get("v.activeProgressItemIndex") || 0;
	},
	triggerSave: function(cmp, callback){
		var instance = cmp.get("v.body")[0];
		if(instance && instance.save) {
			instance.save({ callback: $A.getCallback(callback) });
		} else {
			callback();
		}
	},
	setActiveProgressItem: function(cmp, activeItemIndex){
		var progressItems = this.getProgressItems(cmp);
		if(activeItemIndex >= progressItems.length) {
			this.finishWizard(cmp); return;
		}
		var nextButtonLabel = activeItemIndex == 0 ? "Skip Video" : activeItemIndex == progressItems.length - 1 ? "Go to My CA" : "Next";	
		var activeItem = progressItems.reduce(this.getProgressItemsIterator.bind(this, activeItemIndex), null);

		cmp.set("v.progressItems", progressItems);
		cmp.set("v.activeProgressItemIndex", activeItemIndex);
		cmp.set("v.nextButtonLabel", nextButtonLabel);

		var params = activeItem.params || {};
		params.contactId = cmp.get("v.contactId");
		this.setActiveProgressItemContent(cmp, activeItem.id, params);
	},
	getProgressItemsIterator: function(activeItemIndex, activeItem, item, index){
		item.isActive = !activeItem; return (!activeItem && index === activeItemIndex) ? item : activeItem;
	}, 
	setActiveProgressItemContent: function(cmp, contentType, params) {
		$A.createComponent("c:" + contentType, params || {}, function(newCmp, status, errorMessage){ 
            if (status === "SUCCESS") {
                cmp.set("v.body", [newCmp]); 
            } else if (status === "INCOMPLETE") {
                console.log("No response from server or client is offline.")
            } else if (status === "ERROR") {
                console.error("Error: ", errorMessage);
            } 
        });
	},
	finishWizard: function(cmp){
		var onComplete = cmp.get("v.onComplete");
		if(onComplete) $A.enqueueAction(onComplete);
	}
})