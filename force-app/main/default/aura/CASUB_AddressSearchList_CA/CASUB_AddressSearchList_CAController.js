({
    doInit:function(cmp){
        console.log(cmp.get("v.addressOption"));
        console.log(cmp.get("v.selectedAddressId"))
    }
    ,
    fireEvent : function(cmp) {
        console.log('firing acount name fill event');
        var cmpEvent = cmp.getEvent("addressFillEvent");
        var currentAddressOption = cmp.get("v.addressOption");
        var selectedAddressId = cmp.get("v.selectedAddressId"); 
        
        cmpEvent.setParams({
            "currentAddressOption": currentAddressOption.label,
            "selectedAddressId" : currentAddressOption.value
        });
        cmpEvent.fire();
    }
})