({
    doInit : function(component, event, helper) {
        
        component.set('v.mycolumns', [
            { label: 'ID', fieldName: 'URL', type: 'url', sortable: 'true', initialWidth:120, typeAttributes: { label: { fieldName: 'Name' } } },
            { label: 'Activity / Course Name', fieldName: 'Event_Course_name__c', type: 'text', sortable: 'true' },
            { label: 'Completed Date', fieldName: 'Date_completed__c', type: 'date', sortable: 'true', initialWidth:120 },
            { label: 'Manually Added', fieldName: 'Community_User_Entry__c', type: 'boolean', sortable: 'true', initialWidth:95 },
            { label: 'CPD Hours', fieldName: 'Qualifying_hours__c', type: 'number', sortable: 'true', initialWidth:85 }, 
            { label: 'Type', fieldName: 'Qualifying_hours_type__c', type: 'text', sortable: 'true', initialWidth:130 },
            { label: 'Specialisation / Registration', fieldName: 'Specialist_hours_code__c', type: 'text', sortable: 'true' },
            { label: 'Status', fieldName: 'Status__c', type: 'text', sortable: 'true' },
            { label: 'Provider / University', fieldName: 'Enter_university_instituition__c', type: 'text', sortable: 'true' }
        ]);
        console.log('***Execute getData***');
        helper.getTrienniumDates(component, event, helper);
        helper.getData(component);
    },
    
    
    // Client-side controller called by the onsort event handler
    updateColumnSorting: function (cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        // assign the latest attribute with the sorted column fieldName and sorted direction
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);
        helper.sortData(cmp, fieldName, sortDirection);
    },
    
    
    handleFilter: function (cmp, event, helper) {
        var data = cmp.get("v.mydata");
        if (typeof data !== "undefined" && data !== null) {
            var filterFrom = cmp.get("v.filterFrom");
            var filterTo = cmp.get("v.filterTo");
            var filterSearch = cmp.get("v.filterSearch");
            var searchFields = cmp.get("v.mycolumns").map(function (el) { return el.fieldName; });
            var filteredData = data.filter(helper.filterData(filterFrom, filterTo, filterSearch, searchFields));
            cmp.set("v.filteredData", filteredData);
        }
    },
    
    downloadCSV: function (component, event, helper) {
        
        var data = component.get("v.mydata");
        
        // call the helper function which "return" the CSV data as a String   
        var csv = helper.convertArrayOfObjectsToCSV(component,data);   
        if (csv == null){return;} 
        
        // ####--code for create a temp. <a> html tag [link tag] for download the CSV file--####     
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_self'; // 
        hiddenElement.download = 'CPDRecordsExport.csv';  // CSV file Name* you can change it.[only name not .csv] 
        document.body.appendChild(hiddenElement); // Required for FireFox browser
        hiddenElement.click(); // using click() js function to download csv file
    },
    
    clearFilters: function (cmp, event, helper) {
        cmp.set("{!v.filterFrom}", null);
        cmp.set("{!v.filterTo}", null);
        cmp.set("{!v.filterSearch}", "");
    },
    
    handleShowModal: function(cmp, event, helper) {
        var modalBody, modalFooter;
        $A.createComponents([
            ['c:CaanzccEducationRecordModal', {}]
            //, ['c:CaanzccModalFooter', { confirmButtonLabel: "Create new" }],
        ],
                            function(components, status) {
                                if (status === "SUCCESS") {
                                    modalBody = components[0];
                                    //modalFooter = components[1];
                                    //modalFooter.set('v.saveFunction', modalBody.submit)
                                    cmp.find('overlayLib').showCustomModal({
                                        header: "Create New Education Record",
                                        body: modalBody,
                                        footer: modalFooter,
                                        showCloseButton: true,
                                        closeCallback: function() {
                                            var appEvent = $A.get("e.c:CaanzccCpdUpdateEvent");
                                            appEvent.fire();
                                        }
                                    });
                                }
                            }
                           );
    },
    
    updateCpd: function(cmp, evt, helper) {
        helper.getData(cmp);
    },
    
    deleteEducationRecords: function (cmp, event,helper) {
        cmp.set('v.deleteConfirmation',true);
    },
    
    closeModal: function(component,event,helper){
        component.set('v.deleteConfirmation',false);
    },
    
    deleteRecord: function(cmp,event,helper){
        helper.deleteEduRecord(cmp,event);
        cmp.set('v.deleteConfirmation',false);
    }
    
    
    
    
})