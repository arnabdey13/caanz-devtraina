({
    getData : function(component) {
        
        var action = component.get("c.getEducationRecordsForUser");
        
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            var rtnValue = response.getReturnValue();
            
            if (state === "SUCCESS") {
                // Alert the user with the value returned 
                // from the server
                //alert("From server: " + response.getReturnValue());
                console.log('Education List Sorted rtnValue 2='+rtnValue);
                $A.log('Education List Sorted  rtnValue 2='+rtnValue);
                if (rtnValue !== null) {
                    var data = this.addUrlsToData(rtnValue);
                    component.set('v.mydata',data);
                }
                
                // You would typically fire a event here to trigger 
                // client-side notification that the server-side 
                // action is complete
            }
            else if (state === "INCOMPLETE") {
                alert("Something went wrong. Refresh the page.");
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
    },
    
    getTrienniumDates : function(component, event, helper) {
        
        var action = component.get("c.getCPDSummariesForUser");
        
        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            var rtnValue = response.getReturnValue();
            
            if (state === "SUCCESS") {
                // Alert the user with the value returned 
                // from the server
                //alert("From server: " + response.getReturnValue());
                console.log('CPD Summry rtnValue='+rtnValue);
                $A.log('CPD Summry rtnValue='+rtnValue);
                if (rtnValue !== null) {
                    console.log('Not Null - List Length =  '+rtnValue.length);
                    for (var i = 0; i < rtnValue.length; i++){
                        if (rtnValue[i].Current_Triennium__c == true){
                            console.log('Found Current Triennium - Start Date = '+ rtnValue[i].Triennium_Start_Date__c);
                            component.set('v.filterFrom',rtnValue[i].Triennium_Start_Date__c);                            
                            component.set('v.filterTo',rtnValue[i].Triennium_End_Date__c);                                          
                        }
                        else {
                            console.log('Not Current Triennium - List Index = ' + i + ' Triennium Start Date = '+ rtnValue[i].Triennium_Start_Date__c);
                        }
                        //c.generateChart();
                        //var ctx = component.find("barKey").getElement();
                        //ctx.update();
                        
                    }
                    console.log('Loop Completed List  ');
                }
                
                // You would typically fire a event here to trigger 
                // client-side notification that the server-side 
                // action is complete
            }
            else if (state === "INCOMPLETE") {
                alert("Something went wrong. Refresh the page.");
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
    }, 
    
    sortData: function (cmp, fieldName, sortDirection) {
        var data = cmp.get("v.filteredData");
        var reverse = sortDirection !== 'asc';
        //sorts the rows based on the column header that's clicked
        console.log('Event onSort fired. Fieldname =  '+fieldName + ' Sort Direction = ' + sortDirection);
        
        data.sort(this.sortBy(fieldName, reverse))
        cmp.set("v.filteredData", data);
    },
    
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
        function(x) {return x[field]};
        //checks if the two rows should switch places
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            a = key(a);
            b = key(b);
            if (typeof a === "undefined" || a === null) {
                return 1
            } else if (typeof b === "undefined" || b === null) {
                return -1
            } else {
                return reverse * ((a > b) - (b > a))
            }
        }
    },
    
    filterData: function (from, to, search, fields) {
        console.log("Filtering Data - From: ", from, " To: ", to, " Search: ", search, " Fields: ", fields);
        return function(item) {
            
            // Skip this item if date < from
            if (typeof from !== "undefined" && from !== null) {
                var date = item["Date_completed__c"];
                if (typeof date === "undefined" || date === null || date < from) {
                    return false;
                }
            }
            
            // Skip this item if date > from
            if (typeof to !== "undefined" && to !== null) {
                var date = item["Date_completed__c"];
                if (typeof date === "undefined" || date === null || date > to) {
                    return false;
                }
            }
            
            // Skip this item if search returns negative
            if (typeof search !== "undefined" && search !== null && search !== "") {
                var searchResult = false;
                for (var i=0; i<fields.length; i++) {
                    if (typeof item[fields[i]] !== "undefined" && item[fields[i]] !== null) {
                        if (item[fields[i]].toString().toLowerCase().indexOf(search.toLowerCase()) != -1) {
                            searchResult = true;
                        }
                    }
                }
                if (!searchResult) {
                    return false;
                }
            }
            
            // If you get to here, the item should be included in the filtered data.
            return true;
            
        }
    },
    
    addUrlsToData: function (data) {
        
        var commnunityPrefix = $A.get("$Label.c.Community_Prefix_CCH");
        
        for (var i=0; i<data.length; i++) {
            data[i]["URL"] = '/' + commnunityPrefix + '/s/education-record/' + data[i]["Id"]
        }
        return data;
    },
    
    convertArrayOfObjectsToCSV : function(component,objectRecords){
        // declare variables
        var csvStringResult, counter, keys, labels, columnDivider, lineDivider;
        
        // check if "objectRecords" parameter is null, then return from function
        if (objectRecords == null || !objectRecords.length) {
            return null;
        }
        // store ,[comma] in columnDivider variabel for sparate CSV values and 
        // for start next line use '\n' [new line] in lineDivider varaible  
        columnDivider = ',';
        lineDivider =  '\n';
        
        // in the keys valirable store fields API Names as a key 
        // this labels use in CSV file header  
        keys = ['Id','Event_Course_name__c','Date_completed__c','Community_User_Entry__c','Qualifying_hours__c','Qualifying_hours_type__c','Specialist_hours_code__c','Enter_university_instituition__c' ];
        labels = ['Id','Event/Course Name','Date Completed','Manually Added','Qualifying Hours','Type','Specialisation','Provider/University' ];
        
        csvStringResult = '';
        csvStringResult += labels.join(columnDivider);
        csvStringResult += lineDivider;
        
        for(var i=0; i < objectRecords.length; i++){   
            counter = 0;
            
            for(var sTempkey in keys) {
                var skey = keys[sTempkey] ;  
                
                // add , [comma] after every String value,. [except first]
                if(counter > 0){ 
                    csvStringResult += columnDivider; 
                }   
                
                csvStringResult += '"'+ objectRecords[i][skey]+'"'; 
                
                counter++;
                
            } // inner for loop close 
            csvStringResult += lineDivider;
        }// outer main for loop close 
        
        // return the CSV formate String 
        return csvStringResult;        
    },
    
    deleteEduRecord: function(cmp,event){
        var selectedEducationRecords = cmp.find('cpdEducationSortTable').getSelectedRows();
        if(selectedEducationRecords.length === 0){
            alert('Please select Education records');
        }else{
                var educationRecordIds = [];
                for (var i = 0; i < selectedEducationRecords.length; i++){
                    educationRecordIds.push(selectedEducationRecords[i].Id);
                }
                var action = cmp.get("c.deleteEducationRecordByID");
                action.setParams({
                    "idList": educationRecordIds
                });
                action.setCallback(this,function(result){
                    var state = result.getState();
                    console.log('State',state);
                    if(state === 'SUCCESS'){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Success",
                            "message": "Record has been deleted successfully.",
                            "type": "Success"
                        });
                        toastEvent.fire();
                        $A.get('e.force:refreshView').fire();
                    }
                    else{
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Error!",
                            "message": result.getError()[0].message,
                            "type": "error"
                        });
                        toastEvent.fire();
                    }
                });
                
                $A.enqueueAction(action);
            
        }
    }
    
})