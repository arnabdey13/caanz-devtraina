({	
    handleNewStage : function(cmp, isLast) {
        // return a closure over the isLast to allow setStage after last component is created
        return function(newItem) {
            var current = cmp.get("v.currentStage")
            if (cmp.isValid()) {
                var body = cmp.get("v.body");
                body.push(newItem);
                cmp.set("v.body", body);
                if (isLast) {                    
                    var stages = cmp.find({ instancesOf : "c:ProgressStage" });
			        for (i = 0; i < stages.length; i++) { 
                        stages[i].set("v.active", i <= current);
                    }
                    cmp.set("v.progressPercent", 100 * current / (stages.length - 1));	
                }
            } else {
                $A.log("Invalid item widget!");
            }
        }    
    }
})