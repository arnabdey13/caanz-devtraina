({
    doInit : function(cmp, event, helper) {
        var stageList = cmp.get("v.stages").split(",");
        cmp.set("v.stageList", stageList);
        for (i = 0; i < stageList.length; i++) { 
            $A.createComponent(
                "c:ProgressStage",
                {label: stageList[i]},
                helper.handleNewStage(cmp,(i+1) == stageList.length));
        }
    },
    handleParams : function(cmp, event, helper) {
        var np = event.getParam("urlParams").np;
        if (np == 'y') {
            cmp.set("v.visible", false);
        }
    }
})