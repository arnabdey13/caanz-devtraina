({
    doInit: function (component, event, helper) {
        helper.sendToService(component, event, helper, "c.load", null, "v.alertLoadError", function (val) {
            component.set("v.facaOptIn", val);
        });
    },
    showModal: function (component, event, helper) {
        component.set("v.showModal", true);
    },
    handleModalEvent: function (component, event, helper) {
        if (event.getParam('cancel')) {
            component.set("v.showModal", false);
        }
    },
    radioSelect: function (component, event, helper) {
        var params;
        if (event.target.id == 'Public PractitionerYes') {
            params = {'caOptIn': true};
        } else {
            params = {'caOptIn': false};
        }
        helper.sendToService(component, event, helper, "c.save", params, "v.alertSaveError", function (val) {
            helper.showAlert(component, event, 'success', component.get("v.alertSaveSuccess"));
        });
    }
})