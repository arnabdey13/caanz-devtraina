({
    sendToService : function(component, event, helper, method, params, errorAttr, callback) {
        helper.showSpinner(component, event, helper);
        var action = component.get(method);
        if (params) {
            action.setParams(params);
        }
        action.setCallback(this, function(response) {
            var state = response.getState();
            helper.hideSpinner(component, event, helper);
            if (state === "SUCCESS") {
                callback(response.getReturnValue());
            } else {
                helper.showAlert(component, event, 'error', component.get(errorAttr));
            }
        });
        $A.enqueueAction(action);
    },
    showAlert : function(component, event, type, message){
        component.set("v.alertType", type);
        component.set("v.alertMessage", message);
        component.set("v.showAlert",'visible');
        window.setTimeout($A.getCallback(function () {
            component.set("v.showAlert",'hidden');
        }), component.get("v.alertTimeout"));
    },
    showSpinner : function(component, event, helper) {
        component.set("v.waitingForApex",'');
    },
    hideSpinner : function(component, event, helper) {
        component.set("v.waitingForApex",'slds-hide');
    }
})