({
    doInit : function(cmp, event, helper) {
        cmp.set("v.getter", function(key) {
            return Cookies.get(key);
        });
        cmp.set("v.setter", function(key, value) {
            Cookies.set(key, value);
        });        
    }
})