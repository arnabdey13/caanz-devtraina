({   
     /* Method to display Text box when 'No' option is selected */
    handleChange : function(component, event, helper) {
        helper.handleChangeHelper(component, event, helper);        
    },
    handleCompanyAuditorChange : function(component, event, helper) { 
        if(event.getParam('value') == 'No'){
            component.set("v.questionnaireResponseSobject.Answer_3__c" , '');
        }
    },
    handleCompanyLiquidator: function(component, event, helper) {
        if(event.getParam('value') == 'No'){
            component.set("v.questionnaireResponseSobject.Answer_5__c" , '');
        } 
    },
    handleRegisteredTaxAgent: function(component, event, helper) {
        if(event.getParam('value') == 'No'){
            component.set("v.questionnaireResponseSobject.Answer_7__c" , '');
        }
    },
    handleRegisteredBASAgent: function(component, event, helper) {
        if(event.getParam('value') == 'No'){
            component.set("v.questionnaireResponseSobject.Answer_9__c" , '');
        }
    },
    handleTaxFinancialAdviser: function(component, event, helper) {
        if(event.getParam('value') == 'No'){
            component.set("v.questionnaireResponseSobject.Answer_11__c" , '');
        }
    },
    handleTrusteeinBankruptcy :function(component, event, helper) {
        if(event.getParam('value') == 'No'){
            component.set("v.questionnaireResponseSobject.Answer_13__c" , '');
        }
    },
    handleSMSFAuditor: function(component, event, helper) {
        if(event.getParam('value') == 'No'){
            component.set("v.questionnaireResponseSobject.Answer_15__c" , '');
        }
    },
    handleAustralianFinancialServicesLicence: function(component, event, helper) {
        if(event.getParam('value') == 'No'){
            component.set("v.questionnaireResponseSobject.Answer_18__c" , '');
            component.set("v.questionnaireResponseSobject.Answer_19__c" , '');
            component.set("v.questionnaireResponseSobject.Answer_20__c" , '');
            component.set("v.questionnaireResponseSobject.Answer_21__c" , '');
            component.set("v.questionnaireResponseSobject.Answer_22__c" , '');
            component.set("v.questionnaireResponseSobject.Answer_23__c" , '');
            component.set("v.questionnaireResponseSobject.Answer_24__c" , '');
            component.set("v.questionnaireResponseSobject.Answer_25__c" , '');
            component.set("v.questionnaireResponseSobject.Answer_26__c" , '');
            component.set("v.questionnaireResponseSobject.Answer_27__c" , '');
            component.set("v.questionnaireResponseSobject.Answer_28__c" , '');
            component.set("v.questionnaireResponseSobject.Answer_29__c" , '');
            component.set("v.questionnaireResponseSobject.Answer_30__c" , '');
            component.set("v.questionnaireResponseSobject.Answer_31__c" , '');
            component.set("v.questionnaireResponseSobject.Answer_32__c" , '');
            component.set("v.questionnaireResponseSobject.Answer_33__c" , '');
            component.set("v.questionnaireResponseSobject.Answer_47__c" , '');
            if(!$A.util.isUndefinedOrNull(component.get('v.accountData.Date_commenced_holding_AFSL_Licence__c')) && $A.util.isUndefinedOrNull(component.get('v.accountData.Date_Ceased_Holding_AFSL__c'))){
               component.set("v.questionnaireResponseSobject.Answer_34__c" , '2019-06-30'); 
            }
            if(!$A.util.isUndefinedOrNull(component.get('v.accountData.Date_commenced_being_rep_Authorised_rep__c')) && $A.util.isUndefinedOrNull(component.get('v.accountData.Date_Ceased_Being_Authorised_REP__c'))){
               component.set("v.questionnaireResponseSobject.Answer_35__c" , '2019-06-30'); 
            }
        }else{
            component.set("v.questionnaireResponseSobject.Answer_34__c" , '');
            component.set("v.questionnaireResponseSobject.Answer_35__c" , '');
        }
    },
    handleAustralianCreditLicence: function(component, event, helper) {
        if(event.getParam('value') == 'No'){
            component.set("v.questionnaireResponseSobject.Answer_37__c" , '');
        }else{
            component.set("v.questionnaireResponseSobject.Answer_38__c" , '');
            component.set("v.questionnaireResponseSobject.Answer_39__c" , ''); 
        }
    },
    handleAustralianCreditLicenceHoldACL: function(component, event, helper) {
        if(event.getParam('value') == 'No'){
            component.set("v.questionnaireResponseSobject.Answer_39__c" , '');
        }
    },
    saveSelectedOption : function(component,event,helper){
        var previousOption = event.getParam("value");
        if(!$A.util.isUndefinedOrNull(previousOption)){
            if(previousOption == 'I personally hold an AFSL'){
                component.set("v.questionnaireResponseSobject.Answer_20__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_22__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_23__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_24__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_25__c" , '');                
                component.set("v.questionnaireResponseSobject.Answer_29__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_30__c" , '');                
                component.set("v.questionnaireResponseSobject.Answer_31__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_32__c" , '');
            }else if(previousOption == 'I am personally an authorised representative of an AFSL'){
                component.set("v.questionnaireResponseSobject.Answer_19__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_23__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_24__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_28__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_30__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_31__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_32__c" , '');
            }else if(previousOption == 'I operate under a corporate authorised representative of an AFSL'){
                component.set("v.questionnaireResponseSobject.Answer_19__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_28__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_29__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_31__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_32__c" , '');
            }else if(previousOption == 'I am a director of an AFS licensee'){
                component.set("v.questionnaireResponseSobject.Answer_19__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_23__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_24__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_25__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_28__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_29__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_30__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_31__c" , '');
            }else if(previousOption == 'I am an employee or other representative of an AFSL'){
                component.set("v.questionnaireResponseSobject.Answer_19__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_23__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_25__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_28__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_29__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_30__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_32__c" , '');
            }else if(previousOption == 'I am a responsible manager of an AFSL'){
                component.set("v.questionnaireResponseSobject.Answer_19__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_23__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_24__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_25__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_26__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_27__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_28__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_29__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_30__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_31__c" , '');
                component.set("v.questionnaireResponseSobject.Answer_32__c" , '');
            }
        }
       // component.set('v.selectedOption',event.getSource().get('v.value'));
    },
    updateAUSRegistration : function(component, event, helper) {
        var questionnaireResponseSobject = component.get("v.questionnaireResponseSobject");
        var questionaireResponseObjectToUpdate = component.get("v.questionaireResponseObjectToUpdate");
      
        questionaireResponseObjectToUpdate.Question_2__c = questionnaireResponseSobject.Question_2__c;  
        questionaireResponseObjectToUpdate.Answer_2__c = questionnaireResponseSobject.Answer_2__c;
        
        questionaireResponseObjectToUpdate.Question_3__c = questionnaireResponseSobject.Question_3__c;  
        questionaireResponseObjectToUpdate.Answer_3__c = questionnaireResponseSobject.Answer_3__c;
            
        questionaireResponseObjectToUpdate.Question_4__c = questionnaireResponseSobject.Question_4__c;
        questionaireResponseObjectToUpdate.Answer_4__c = questionnaireResponseSobject.Answer_4__c;
            
        questionaireResponseObjectToUpdate.Question_5__c = questionnaireResponseSobject.Question_5__c;
        questionaireResponseObjectToUpdate.Answer_5__c = questionnaireResponseSobject.Answer_5__c;
            
        questionaireResponseObjectToUpdate.Question_6__c = questionnaireResponseSobject.Question_6__c;
        questionaireResponseObjectToUpdate.Answer_6__c = questionnaireResponseSobject.Answer_6__c;
            
        questionaireResponseObjectToUpdate.Question_7__c = questionnaireResponseSobject.Question_7__c;
        questionaireResponseObjectToUpdate.Answer_7__c = questionnaireResponseSobject.Answer_7__c;
            
        questionaireResponseObjectToUpdate.Question_8__c = questionnaireResponseSobject.Question_8__c;
        questionaireResponseObjectToUpdate.Answer_8__c = questionnaireResponseSobject.Answer_8__c;
            
        questionaireResponseObjectToUpdate.Question_9__c = questionnaireResponseSobject.Question_9__c;
        questionaireResponseObjectToUpdate.Answer_9__c = questionnaireResponseSobject.Answer_9__c;
        
        questionaireResponseObjectToUpdate.Question_10__c = questionnaireResponseSobject.Question_10__c;
        questionaireResponseObjectToUpdate.Answer_10__c = questionnaireResponseSobject.Answer_10__c;
        
        questionaireResponseObjectToUpdate.Question_11__c = questionnaireResponseSobject.Question_11__c;
        questionaireResponseObjectToUpdate.Answer_11__c = questionnaireResponseSobject.Answer_11__c;
        
        questionaireResponseObjectToUpdate.Question_12__c = questionnaireResponseSobject.Question_12__c;
        questionaireResponseObjectToUpdate.Answer_12__c = questionnaireResponseSobject.Answer_12__c;
        
        questionaireResponseObjectToUpdate.Question_13__c = questionnaireResponseSobject.Question_13__c;
        questionaireResponseObjectToUpdate.Answer_13__c = questionnaireResponseSobject.Answer_13__c;
        
        questionaireResponseObjectToUpdate.Question_14__c = questionnaireResponseSobject.Question_14__c;
        questionaireResponseObjectToUpdate.Answer_14__c = questionnaireResponseSobject.Answer_14__c;
        
        questionaireResponseObjectToUpdate.Question_15__c = questionnaireResponseSobject.Question_15__c;
        questionaireResponseObjectToUpdate.Answer_15__c = questionnaireResponseSobject.Answer_15__c;
        
        questionaireResponseObjectToUpdate.Question_16__c = questionnaireResponseSobject.Question_16__c;
        questionaireResponseObjectToUpdate.Answer_16__c = questionnaireResponseSobject.Answer_16__c;
        
        questionaireResponseObjectToUpdate.Question_17__c = questionnaireResponseSobject.Question_17__c;
        questionaireResponseObjectToUpdate.Answer_17__c = questionnaireResponseSobject.Answer_17__c;
        
        questionaireResponseObjectToUpdate.Question_18__c = questionnaireResponseSobject.Question_18__c;
        questionaireResponseObjectToUpdate.Answer_18__c = questionnaireResponseSobject.Answer_18__c;
        
        questionaireResponseObjectToUpdate.Question_19__c = questionnaireResponseSobject.Question_19__c;
        questionaireResponseObjectToUpdate.Answer_19__c = questionnaireResponseSobject.Answer_19__c;
        
        questionaireResponseObjectToUpdate.Question_20__c = questionnaireResponseSobject.Question_20__c;
        questionaireResponseObjectToUpdate.Answer_20__c = questionnaireResponseSobject.Answer_20__c;
        
        questionaireResponseObjectToUpdate.Question_21__c = questionnaireResponseSobject.Question_21__c;
        questionaireResponseObjectToUpdate.Answer_21__c = questionnaireResponseSobject.Answer_21__c;
        
        questionaireResponseObjectToUpdate.Question_22__c = questionnaireResponseSobject.Question_22__c;
        questionaireResponseObjectToUpdate.Answer_22__c = questionnaireResponseSobject.Answer_22__c;
        
        questionaireResponseObjectToUpdate.Question_23__c = questionnaireResponseSobject.Question_23__c;
        questionaireResponseObjectToUpdate.Answer_23__c = questionnaireResponseSobject.Answer_23__c;
        
        questionaireResponseObjectToUpdate.Question_24__c = questionnaireResponseSobject.Question_24__c;
        questionaireResponseObjectToUpdate.Answer_24__c = questionnaireResponseSobject.Answer_24__c;
        
        questionaireResponseObjectToUpdate.Question_25__c = questionnaireResponseSobject.Question_25__c;
        questionaireResponseObjectToUpdate.Answer_25__c = questionnaireResponseSobject.Answer_25__c;
        
        questionaireResponseObjectToUpdate.Question_26__c = questionnaireResponseSobject.Question_26__c;
        questionaireResponseObjectToUpdate.Answer_26__c = questionnaireResponseSobject.Answer_26__c;
        
        questionaireResponseObjectToUpdate.Question_27__c = questionnaireResponseSobject.Question_27__c;
        questionaireResponseObjectToUpdate.Answer_27__c = questionnaireResponseSobject.Answer_27__c;
        
        questionaireResponseObjectToUpdate.Question_28__c = questionnaireResponseSobject.Question_28__c;
        questionaireResponseObjectToUpdate.Answer_28__c = questionnaireResponseSobject.Answer_28__c;
        
        questionaireResponseObjectToUpdate.Question_29__c = questionnaireResponseSobject.Question_29__c;
        questionaireResponseObjectToUpdate.Answer_29__c = questionnaireResponseSobject.Answer_29__c;
        
        questionaireResponseObjectToUpdate.Question_30__c = questionnaireResponseSobject.Question_30__c;
        questionaireResponseObjectToUpdate.Answer_30__c = questionnaireResponseSobject.Answer_30__c;
        
        questionaireResponseObjectToUpdate.Question_31__c = questionnaireResponseSobject.Question_31__c;
        questionaireResponseObjectToUpdate.Answer_31__c = questionnaireResponseSobject.Answer_31__c;
        
        questionaireResponseObjectToUpdate.Question_32__c = questionnaireResponseSobject.Question_32__c;
        questionaireResponseObjectToUpdate.Answer_32__c = questionnaireResponseSobject.Answer_32__c;
        
        questionaireResponseObjectToUpdate.Question_33__c = questionnaireResponseSobject.Question_33__c;
        questionaireResponseObjectToUpdate.Answer_33__c = questionnaireResponseSobject.Answer_33__c;
        
        questionaireResponseObjectToUpdate.Question_34__c = questionnaireResponseSobject.Question_34__c;
        questionaireResponseObjectToUpdate.Answer_34__c = questionnaireResponseSobject.Answer_34__c;
        
        questionaireResponseObjectToUpdate.Question_35__c = questionnaireResponseSobject.Question_35__c;
        questionaireResponseObjectToUpdate.Answer_35__c = questionnaireResponseSobject.Answer_35__c;
        
        questionaireResponseObjectToUpdate.Question_36__c = questionnaireResponseSobject.Question_36__c;
        questionaireResponseObjectToUpdate.Answer_36__c = questionnaireResponseSobject.Answer_36__c;
        
        questionaireResponseObjectToUpdate.Question_37__c = questionnaireResponseSobject.Question_37__c;
        questionaireResponseObjectToUpdate.Answer_37__c = questionnaireResponseSobject.Answer_37__c;
        
        questionaireResponseObjectToUpdate.Question_38__c = questionnaireResponseSobject.Question_38__c;
        questionaireResponseObjectToUpdate.Answer_38__c = questionnaireResponseSobject.Answer_38__c;
        
        questionaireResponseObjectToUpdate.Question_39__c = questionnaireResponseSobject.Question_39__c;
        questionaireResponseObjectToUpdate.Answer_39__c = questionnaireResponseSobject.Answer_39__c;
        
        //questionaireResponseObjectToUpdate.Question_47__c = questionnaireResponseSobject.Question_47__c;
        //questionaireResponseObjectToUpdate.Answer_47__c = questionnaireResponseSobject.Answer_47__c;
        
        if (!$A.util.isUndefinedOrNull(questionnaireResponseSobject.Id)) {
            questionaireResponseObjectToUpdate.Id = questionnaireResponseSobject.Id;
            questionaireResponseObjectToUpdate.Residential_Country__c = questionnaireResponseSobject.Residential_Country__c;
        }
        helper.upsertCPD(component,questionaireResponseObjectToUpdate); 
    },
})