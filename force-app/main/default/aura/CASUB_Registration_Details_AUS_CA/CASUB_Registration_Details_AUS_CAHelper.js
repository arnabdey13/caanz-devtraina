({  
    upsertCPD: function(component,questionaireResponseObjectToUpdate,callBackParams){
        var subscriptionSobject = component.get("v.globalSubscriptionSobject");
        if($A.util.isUndefinedOrNull(subscriptionSobject.Obligation_Sub_Components__c )){
            subscriptionSobject.Obligation_Sub_Components__c = 'Registration Details;';
        }
        else if(!subscriptionSobject.Obligation_Sub_Components__c.includes("Registration Details")){
            if(subscriptionSobject.Obligation_Sub_Components__c.substring(subscriptionSobject.Obligation_Sub_Components__c.length - 1) == ';')
                subscriptionSobject.Obligation_Sub_Components__c += 'Registration Details;';
            else
                subscriptionSobject.Obligation_Sub_Components__c += ';Registration Details;';
        }
        this.validateRegistrationQuestionsAndAnswers(component,questionaireResponseObjectToUpdate,function(){
            console.log('==questionaireResponseObjectToUpdate==' + JSON.stringify(questionaireResponseObjectToUpdate));
            var param = {
                'personAccountId':component.get("v.accountData.Id"),
                'questionaireResponseObjectToUpdate':questionaireResponseObjectToUpdate,
                'selectedCountry' : component.get('v.selectedCountry'),
                'whichSectionSave' : 'RegistrationAU',
                'subscriptionSobject': subscriptionSobject
            };
            this.enqueueAction(component, "upsertQuestionaireResponse", param, function(error, data){
                var questionResponseSubscriptionWrappper = data;
                if(error){
                    component.set("v.errorMessage",error);
                }else if(!$A.util.isUndefinedOrNull(callBackParams) && callBackParams.callback) {
                    if (!$A.util.isUndefinedOrNull(questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id)) {
                        component.set("v.questionnaireResponseSobject.Id",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id);
                        component.set("v.questionnaireResponseSobject.Residential_Country__c",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Residential_Country__c);
                        component.set("v.globalSubscriptionSobject",questionResponseSubscriptionWrappper.subscriptionSObject);
                        /*
                        component.set("v.questionnaireResponseSobject.Id",data.Id);
                        component.set("v.questionnaireResponseSobject.Residential_Country__c",data.Residential_Country__c);
                        */
                    }
                    
                    component.set("v.errorMessage",'');
                    callBackParams.callback(); 
                }else{
                    if (!$A.util.isUndefinedOrNull(questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id)) {
                        component.set("v.questionnaireResponseSobject.Id",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id);
                        component.set("v.questionnaireResponseSobject.Residential_Country__c",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Residential_Country__c);
                        component.set("v.globalSubscriptionSobject",questionResponseSubscriptionWrappper.subscriptionSObject);
                        /*
                        component.set("v.questionnaireResponseSobject.Id",data.Id);
                        component.set("v.questionnaireResponseSobject.Residential_Country__c",data.Residential_Country__c);
                        */
                }
            }
                
            }); 
        })
    },
    enqueueAction: function(component, method, params, callback){
        this.toggleSpinner(component, true);
        var action = component.get("c." + method); 
        if(params) action.setParams(params);
        action.setCallback(this, function(response){
            this.toggleSpinner(component, false);
            if(response.getState() === "SUCCESS") {
                if(callback) callback.call(this, null, response.getReturnValue());
            } else if(response.getState() === "ERROR") {
                var message = 'Unknown error'; 
                var errors = response.getError();
                if (!$A.util.isEmpty(errors)) {
                    message = errors[0].message;
                }
                console.error(message);
                if(callback) callback.call(this, message);
            }
        });
        $A.enqueueAction(action);
    },
    toggleSpinner: function(component, show) {
        var spinner = component.find("registrationAU");
        $A.util.addClass(spinner, show ? "slds-show" : "slds-hide");
        $A.util.removeClass(spinner, !show ? "slds-show" : "slds-hide");
    },
    validateRegistrationQuestionsAndAnswers : function(component,questionaireResponseObjectToUpdate,callback){
        var errorMessage = '';
        var isValidate=false;
        if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_2__c) || questionaireResponseObjectToUpdate.Answer_2__c==''){
            errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_2__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_2__c');
        }else if(questionaireResponseObjectToUpdate.Answer_2__c=='Yes' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_3__c) || 
                                                                           questionaireResponseObjectToUpdate.Answer_3__c=='')){
            errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_3__c;
            isValidate=true;
            component.find('Answer_3__c').focus();
        }else if(questionaireResponseObjectToUpdate.Answer_2__c=='Yes' && (!$A.util.isUndefined(component.find('Answer_3__c')) && !component.find('Answer_3__c').checkValidity())){
           component.find('Answer_3__c').focus();
           isValidate=true;
        }
            
        else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_4__c) || questionaireResponseObjectToUpdate.Answer_4__c==''){
            errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_4__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_4__c');
        }else if(questionaireResponseObjectToUpdate.Answer_4__c=='Yes' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_5__c) || 
                                                                           questionaireResponseObjectToUpdate.Answer_5__c=='')){
            errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_5__c;
            isValidate=true;
            component.find('Answer_5__c').focus();
        }else if(questionaireResponseObjectToUpdate.Answer_4__c=='Yes' && (!$A.util.isUndefined(component.find('Answer_5__c')) && !component.find('Answer_5__c').checkValidity())){
           component.find('Answer_5__c').focus();
           isValidate=true;
        }
            
        else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_6__c) || questionaireResponseObjectToUpdate.Answer_6__c==''){
            errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_6__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_6__c');
        }else if(questionaireResponseObjectToUpdate.Answer_6__c=='Yes' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_7__c) || 
                                                                           questionaireResponseObjectToUpdate.Answer_7__c=='')){
            errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_7__c;
            isValidate=true;
            component.find('Answer_7__c').focus();
        }else if(questionaireResponseObjectToUpdate.Answer_6__c=='Yes' && (!$A.util.isUndefined(component.find('Answer_7__c')) && !component.find('Answer_7__c').checkValidity())){
           component.find('Answer_7__c').focus();
           isValidate=true;
        }
            
        else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_8__c) || questionaireResponseObjectToUpdate.Answer_8__c==''){
            errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_8__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_8__c');
        }else if(questionaireResponseObjectToUpdate.Answer_8__c=='Yes' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_9__c) || 
                                                                           questionaireResponseObjectToUpdate.Answer_9__c=='')){
            errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_9__c;
            isValidate=true;
            component.find('Answer_9__c').focus();
        }else if(questionaireResponseObjectToUpdate.Answer_8__c=='Yes' && (!$A.util.isUndefined(component.find('Answer_9__c')) && !component.find('Answer_9__c').checkValidity())){
           component.find('Answer_9__c').focus();
           isValidate=true;
        }
        
        else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_10__c) || questionaireResponseObjectToUpdate.Answer_10__c==''){
            errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_10__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_10__c');
        }else if(questionaireResponseObjectToUpdate.Answer_10__c=='Yes' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_11__c) || 
                                                                            questionaireResponseObjectToUpdate.Answer_11__c=='')){
            errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_11__c;
            isValidate=true;
            component.find('Answer_11__c').focus();
        }else if(questionaireResponseObjectToUpdate.Answer_10__c=='Yes' && (!$A.util.isUndefined(component.find('Answer_11__c')) && !component.find('Answer_11__c').checkValidity())){
           component.find('Answer_11__c').focus();
           isValidate=true;
        }
        else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_12__c) || questionaireResponseObjectToUpdate.Answer_12__c==''){
            errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_12__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_12__c');
        }else if(questionaireResponseObjectToUpdate.Answer_12__c=='Yes' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_13__c) || 
                                                                            questionaireResponseObjectToUpdate.Answer_13__c=='')){
            errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_13__c;
            isValidate=true;
            component.find('Answer_13__c').focus();
        }else if(questionaireResponseObjectToUpdate.Answer_12__c=='Yes' && (!$A.util.isUndefined(component.find('Answer_13__c')) && !component.find('Answer_13__c').checkValidity())){
           component.find('Answer_13__c').focus();
           isValidate=true;
        }
            
        else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_14__c) || questionaireResponseObjectToUpdate.Answer_14__c==''){
            errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_14__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_14__c');
        }else if(questionaireResponseObjectToUpdate.Answer_14__c=='Yes' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_15__c) || 
                                                                            questionaireResponseObjectToUpdate.Answer_15__c=='')){
            errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_15__c;
            isValidate=true;
            component.find('Answer_15__c').focus();
        }else if(questionaireResponseObjectToUpdate.Answer_14__c=='Yes' && (!$A.util.isUndefined(component.find('Answer_15__c')) && !component.find('Answer_15__c').checkValidity())){
           component.find('Answer_15__c').focus();
           isValidate=true;
        }
        else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_16__c) || questionaireResponseObjectToUpdate.Answer_16__c==''){
                errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_16__c;
                isValidate=true;
                this.setFocusForErrorMessage(component,'Answer_16__c');
       }
        
        /*** Validation for Australian Financial Services Licence (AFSL) Start here ***/
            else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_17__c) || questionaireResponseObjectToUpdate.Answer_17__c==''){
                errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_17__c;
                isValidate=true;
                this.setFocusForErrorMessage(component,'Answer_17__c');
            }else if(questionaireResponseObjectToUpdate.Answer_17__c=='Yes' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_18__c) || 
                                                                                questionaireResponseObjectToUpdate.Answer_18__c=='')){
                errorMessage = 'Please select ' + questionaireResponseObjectToUpdate.Question_18__c;
                isValidate=true;
                component.find('Answer_18__c').focus();
            }
        	/* Below code is commented as part of User Story 622 point no 2. */
            /*else if(questionaireResponseObjectToUpdate.Answer_17__c=='Yes' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_33__c) || 
                                                                                questionaireResponseObjectToUpdate.Answer_33__c=='')){
                errorMessage = questionaireResponseObjectToUpdate.Question_33__c;
                isValidate=true;
            }*/
        /*
            else if(questionaireResponseObjectToUpdate.Answer_17__c=='No' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_34__c) || 
                                                                                questionaireResponseObjectToUpdate.Answer_34__c=='')){
                errorMessage = 'Please select date for ' + questionaireResponseObjectToUpdate.Question_34__c;
                isValidate=true;
                this.setFocusForErrorMessage(component,'Answer_34__c');
            }else if(!$A.util.isUndefined(component.find('Answer_34__c')) && !component.find('Answer_34__c').checkValidity()){
                    component.find('Answer_34__c').focus();
                    isValidate=true;
            }else if(questionaireResponseObjectToUpdate.Answer_17__c=='No' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_35__c) || 
                                                                                questionaireResponseObjectToUpdate.Answer_35__c=='')){
                errorMessage = 'Please select date for ' + questionaireResponseObjectToUpdate.Question_35__c;
                isValidate=true;
                this.setFocusForErrorMessage(component,'Answer_35__c');
            }else if(!$A.util.isUndefined(component.find('Answer_35__c')) && !component.find('Answer_35__c').checkValidity()){
                    component.find('Answer_35__c').focus();
                    isValidate=true;
             }*//*else if(questionaireResponseObjectToUpdate.Answer_17__c=='Yes' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_35__c) || 
                                                                                questionaireResponseObjectToUpdate.Answer_35__c=='')){
                errorMessage = 'Please select date for ' + questionaireResponseObjectToUpdate.Question_35__c;
                isValidate=true;
                 this.setFocusForErrorMessage(component,'Answer_35__c');
            }*/else if(questionaireResponseObjectToUpdate.Answer_18__c == 'I personally hold an AFSL'){
                if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_19__c) || questionaireResponseObjectToUpdate.Answer_19__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_19__c;
                    isValidate=true;
                    component.find('Answer_19__c').focus();
                }else if(!$A.util.isUndefined(component.find('Answer_19__c')) && !component.find('Answer_19__c').checkValidity()){
                    component.find('Answer_19__c').focus();
                    isValidate=true;
                }/*else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_21__c) || questionaireResponseObjectToUpdate.Answer_21__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_21__c;
                    isValidate=true;
                    component.find('Answer_21__c').focus();
                }*//*else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_47__c) || questionaireResponseObjectToUpdate.Answer_47__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_47__c;
                    isValidate=true;
                    component.find('Answer_47__c').focus();
                }*/else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_26__c) || questionaireResponseObjectToUpdate.Answer_26__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_26__c;
                    isValidate=true;
                    component.find('Answer_26__c').focus();
                }else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_27__c) || questionaireResponseObjectToUpdate.Answer_27__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_27__c;
                    isValidate=true;
                    component.find('Answer_27__c').focus();
                }/*else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_21__c) || questionaireResponseObjectToUpdate.Answer_21__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_21__c;
                    isValidate=true;
                    component.find('Answer_21__c').focus();
                }else if(!$A.util.isUndefined(component.find('Answer_21__c')) && !component.find('Answer_21__c').checkValidity()){
                    component.find('Answer_21__c').focus();
                    isValidate=true;
                }*//*else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_22__c) || questionaireResponseObjectToUpdate.Answer_22__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_22__c;
                    isValidate=true;
                }*/else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_28__c) || questionaireResponseObjectToUpdate.Answer_28__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_28__c;
                    isValidate=true;
                    this.setFocusForErrorMessage(component,'Answer_28__c');
                }else if(!$A.util.isUndefined(component.find('Answer_28__c')) && !component.find('Answer_28__c').checkValidity()){
                   component.find('Answer_28__c').focus();
                   isValidate=true;
                }  
            }else if(questionaireResponseObjectToUpdate.Answer_18__c == 'I am personally an authorised representative of an AFSL'){
                if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_20__c) || questionaireResponseObjectToUpdate.Answer_20__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_20__c;
                    isValidate=true;
                    component.find('Answer_20__c').focus();
                }else if(!$A.util.isUndefined(component.find('Answer_20__c')) && !component.find('Answer_20__c').checkValidity()){
                    component.find('Answer_20__c').focus();
                    isValidate=true;
                }/*else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_21__c) || questionaireResponseObjectToUpdate.Answer_21__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_21__c;
                    isValidate=true;
                    component.find('Answer_21__c').focus();
                }*//*else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_47__c) || questionaireResponseObjectToUpdate.Answer_47__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_47__c;
                    isValidate=true;
                    component.find('Answer_47__c').focus();
                }*/else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_26__c) || questionaireResponseObjectToUpdate.Answer_26__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_26__c;
                    isValidate=true;
                    component.find('Answer_26__c').focus();
                }else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_27__c) || questionaireResponseObjectToUpdate.Answer_27__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_27__c;
                    isValidate=true;
                    component.find('Answer_27__c').focus();
                }else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_22__c) || questionaireResponseObjectToUpdate.Answer_22__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_22__c;
                    isValidate=true;
                    component.find('Answer_22__c').focus();
                }else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_25__c) || questionaireResponseObjectToUpdate.Answer_25__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_25__c;
                    this.setFocusForErrorMessage(component,'Answer_25__c');
                    isValidate=true;
                }else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_29__c) || questionaireResponseObjectToUpdate.Answer_29__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_29__c;
                    isValidate=true;
                    this.setFocusForErrorMessage(component,'Answer_29__c');
                }else if(!$A.util.isUndefined(component.find('Answer_29__c')) && !component.find('Answer_29__c').checkValidity()){
                   component.find('Answer_29__c').focus();
                   isValidate=true;
                }
            }else if(questionaireResponseObjectToUpdate.Answer_18__c == 'I operate under a corporate authorised representative of an AFSL'){
                if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_20__c) || questionaireResponseObjectToUpdate.Answer_20__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_20__c;
                    isValidate=true;
                    component.find('Answer_20__c').focus();
                }/*else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_21__c) || questionaireResponseObjectToUpdate.Answer_21__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_21__c;
                    isValidate=true;
                    component.find('Answer_21__c').focus();
                }*//*else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_47__c) || questionaireResponseObjectToUpdate.Answer_47__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_47__c;
                    isValidate=true;
                    component.find('Answer_47__c').focus();
                }*/else if(!$A.util.isUndefined(component.find('Answer_20__c')) && !component.find('Answer_20__c').checkValidity()){
                    component.find('Answer_20__c').focus();
                    isValidate=true;
                }else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_22__c) || questionaireResponseObjectToUpdate.Answer_22__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_22__c;
                    isValidate=true;
                    component.find('Answer_22__c').focus();
                }else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_26__c) || questionaireResponseObjectToUpdate.Answer_26__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_26__c;
                    isValidate=true;
                    component.find('Answer_26__c').focus();
                }else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_27__c) || questionaireResponseObjectToUpdate.Answer_27__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_27__c;
                    isValidate=true;
                    component.find('Answer_27__c').focus();
                }else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_23__c) || questionaireResponseObjectToUpdate.Answer_23__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_23__c;
                    isValidate=true;
                    component.find('Answer_23__c').focus();
                }/*else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_24__c) || questionaireResponseObjectToUpdate.Answer_24__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_24__c;
                    isValidate=true;
                    component.find('Answer_24__c').focus();
                }else if(!$A.util.isUndefined(component.find('Answer_24__c')) && !component.find('Answer_24__c').checkValidity()){
                    component.find('Answer_24__c').focus();
                    isValidate=true;
                }*/else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_25__c) || questionaireResponseObjectToUpdate.Answer_25__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_25__c;
                    isValidate=true;
                    component.find('Answer_25__c').focus();
                }else if(!$A.util.isUndefined(component.find('Answer_25__c')) && !component.find('Answer_25__c').checkValidity()){
                    component.find('Answer_25__c').focus();
                    isValidate=true;
                /*}else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_29__c) || questionaireResponseObjectToUpdate.Answer_29__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_29__c;
                    isValidate=true;
                }else if(!$A.util.isUndefined(component.find('Answer_29__c')) && !component.find('Answer_29__c').checkValidity()){
                   component.find('Answer_29__c').focus();
                   isValidate=true;*/
                }else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_30__c) || questionaireResponseObjectToUpdate.Answer_30__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_30__c;
                    isValidate=true;
                    this.setFocusForErrorMessage(component,'Answer_30__c');
                }else if(!$A.util.isUndefined(component.find('Answer_30__c')) && !component.find('Answer_30__c').checkValidity()){
                   component.find('Answer_30__c').focus();
                   isValidate=true;
                }  
            }else if(questionaireResponseObjectToUpdate.Answer_18__c == 'I am a director of an AFS licensee'){
                if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_20__c) || questionaireResponseObjectToUpdate.Answer_20__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_20__c;
                    isValidate=true;
                    component.find('Answer_20__c').focus();
                }else if(!$A.util.isUndefined(component.find('Answer_20__c')) && !component.find('Answer_20__c').checkValidity()){
                    component.find('Answer_20__c').focus();
                    isValidate=true;
                }/*else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_21__c) || questionaireResponseObjectToUpdate.Answer_21__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_21__c;
                    isValidate=true;
                    component.find('Answer_21__c').focus();
                }*/else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_22__c) || questionaireResponseObjectToUpdate.Answer_22__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_22__c;
                    isValidate=true;
                    component.find('Answer_22__c').focus();
                }else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_32__c) || questionaireResponseObjectToUpdate.Answer_32__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_32__c;
                    isValidate=true;
                    this.setFocusForErrorMessage(component,'Answer_32__c');
                }/*else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_47__c) || questionaireResponseObjectToUpdate.Answer_47__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_47__c;
                    isValidate=true;
                    component.find('Answer_47__c').focus();
                }*/else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_26__c) || questionaireResponseObjectToUpdate.Answer_26__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_26__c;
                    isValidate=true;
                    component.find('Answer_26__c').focus();
                }else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_27__c) || questionaireResponseObjectToUpdate.Answer_27__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_27__c;
                    isValidate=true;
                    component.find('Answer_27__c').focus();
                }else if(!$A.util.isUndefined(component.find('Answer_32__c')) && !component.find('Answer_32__c').checkValidity()){
                   component.find('Answer_32__c').focus();
                   isValidate=true;
                }
            }else if(questionaireResponseObjectToUpdate.Answer_18__c == 'I am an employee or other representative of an AFSL'){
                if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_20__c) || questionaireResponseObjectToUpdate.Answer_20__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_20__c;
                    isValidate=true;
                    component.find('Answer_20__c').focus();
                }else if(!$A.util.isUndefined(component.find('Answer_20__c')) && !component.find('Answer_20__c').checkValidity()){
                    component.find('Answer_20__c').focus();
                    isValidate=true;
                }/*else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_21__c) || questionaireResponseObjectToUpdate.Answer_21__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_21__c;
                    isValidate=true;
                    component.find('Answer_21__c').focus();
                }*//*else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_47__c) || questionaireResponseObjectToUpdate.Answer_47__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_47__c;
                    isValidate=true;
                    component.find('Answer_47__c').focus();
                }*/else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_26__c) || questionaireResponseObjectToUpdate.Answer_26__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_26__c;
                    isValidate=true;
                    component.find('Answer_26__c').focus();
                }else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_27__c) || questionaireResponseObjectToUpdate.Answer_27__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_27__c;
                    isValidate=true;
                    component.find('Answer_27__c').focus();
                }else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_22__c) || questionaireResponseObjectToUpdate.Answer_22__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_22__c;
                    isValidate=true;
                    component.find('Answer_22__c').focus();
                }/*else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_24__c) || questionaireResponseObjectToUpdate.Answer_24__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_24__c;
                    isValidate=true;
                    component.find('Answer_24__c').focus();
                }*/else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_31__c) || questionaireResponseObjectToUpdate.Answer_31__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_31__c;
                    isValidate=true;
                    this.setFocusForErrorMessage(component,'Answer_31__c');
                }else if(!$A.util.isUndefined(component.find('Answer_31__c')) && !component.find('Answer_31__c').checkValidity()){
                   component.find('Answer_31__c').focus();
                   isValidate=true;
                }  
            }else if(questionaireResponseObjectToUpdate.Answer_18__c == 'I am a responsible manager of an AFSL'){
                if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_20__c) || questionaireResponseObjectToUpdate.Answer_20__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_20__c;
                    isValidate=true;
                    component.find('Answer_20__c').focus();
                }else if(!$A.util.isUndefined(component.find('Answer_20__c')) && !component.find('Answer_20__c').checkValidity()){
                    component.find('Answer_20__c').focus();
                    isValidate=true;
                }else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_22__c) || questionaireResponseObjectToUpdate.Answer_22__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_22__c;
                    isValidate=true;
                    component.find('Answer_22__c').focus();
                }/*else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_21__c) || questionaireResponseObjectToUpdate.Answer_21__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_21__c;
                    isValidate=true;
                    component.find('Answer_21__c').focus();
                }*//*else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_47__c) || questionaireResponseObjectToUpdate.Answer_47__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_47__c;
                    isValidate=true;
                    component.find('Answer_47__c').focus();
                }*//*else if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_27__c) || questionaireResponseObjectToUpdate.Answer_27__c==''){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_27__c;
                    isValidate=true;
                }*/
            }
        
        
        
        /*** Validation for Australian Financial Services Licence (AFSL) Ends here ***/
        
        /*** Validation for Australian Credit Licence (ACL) Start here ***/
                if($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_36__c) || questionaireResponseObjectToUpdate.Answer_36__c==''){
                    errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_36__c;
                    isValidate=true;
                    this.setFocusForErrorMessage(component,'Answer_36__c');
                }else if(questionaireResponseObjectToUpdate.Answer_36__c=='Yes' && 
                         ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_37__c) ||
                          questionaireResponseObjectToUpdate.Answer_37__c=='' )){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_37__c;
                    isValidate=true;
                    this.setFocusForErrorMessage(component,'Answer_37__c');
                }else if(questionaireResponseObjectToUpdate.Answer_36__c=='Yes' && 
                         (!$A.util.isUndefined(component.find('Answer_37__c')) && !component.find('Answer_37__c').checkValidity())){
                   component.find('Answer_37__c').focus();
                   isValidate=true;
                }else if(questionaireResponseObjectToUpdate.Answer_36__c=='No' && 
                         ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_38__c) ||
                          questionaireResponseObjectToUpdate.Answer_38__c=='')){
                    errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_38__c;
                    isValidate=true;
                    component.find('Answer_38__c').focus();
                }else if(questionaireResponseObjectToUpdate.Answer_38__c=='Yes' && 
                         ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_39__c) ||
                          questionaireResponseObjectToUpdate.Answer_39__c=='')){
                    errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_39__c;
                    isValidate=true;
                    this.setFocusForErrorMessage(component,'Answer_39__c');
                }
                    /*else if(questionaireResponseObjectToUpdate.Answer_36__c=='No' && (!$A.util.isUndefined(component.find('Answer_39__c')) && !component.find('Answer_39__c').checkValidity())){
                   component.find('Answer_39__c').focus();
                   isValidate=true;
                }*/
        /*** Validation for Australian Credit Licence (ACL) Ends here ***/
        if(!isValidate){
            if(callback) callback.call(this);
        }else{
            this.showToast(component,errorMessage);
        }
        
    },
    showToast : function(component,text) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Warning!",
            "message": text,
            type : 'warning',
            mode: 'dismissible',
            duration :10000
        });
        toastEvent.fire();
    },
    /* Method to display Text box when 'No' option is selected */
    handleChangeHelper : function(component, event, helper) {
        
        if(event.getSource().get('v.value') == 'true'){
            var tempCmp = event.getSource().get('v.name');
            var cmp = component.find('c_' + tempCmp); 
            $A.util.removeClass(cmp,'slds-hide');
        }
        else{
            var tempCmp = event.getSource().get('v.name');
            var cmp = component.find('c_' + tempCmp);
            var cmp2 = component.find( tempCmp);
            cmp2.set("v.value","");
            $A.util.addClass(cmp,'slds-hide');
        }
        
    },
    /* Method to display Text box when 'No' option is selected */
    setFocusForErrorMessage : function(component,componentId) {
        if(!$A.util.isUndefined(component.find(componentId))){
            component.find(componentId).focus();
        }
      
    }
})