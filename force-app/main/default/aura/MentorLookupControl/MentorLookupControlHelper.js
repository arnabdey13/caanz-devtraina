({
    //this is a HOF because a closure over the component is required
    getFindAggregateControlsCallback: function (component) {
        var helper = this;
        return function (aggregateComponent) {
            var aggregateKey = component.get("v.aggregateKey");
            var aggregateComponentAggregateKey = aggregateComponent.get("v.aggregateKey")
            var field = aggregateComponent.get("v.field");
            var loadKey = aggregateComponent.get("v.loadKey");
            var mentorNameLoadKey = component.get("v.mentorNameLoadKey");
            var mentorAccountNameLoadKey = component.get("v.mentorAccountNameLoadKey");
            var mentorNameField = component.get("v.mentorNameField");
            var mentorAccountNameField = component.get("v.mentorAccountNameField");
            var mentorIdLoadKey = component.get("v.mentorIdLoadKey");
            var mentorIdField = component.get("v.mentorIdField");
            var mentorEmailField = component.get("v.mentorEmailField");
            var mentorEmailLoadKey = component.get("v.mentorEmailLoadKey");
            var designationLoadKey = component.get("v.designationLoadKey");
            var designationField = component.get("v.designationField");
            if (aggregateKey == aggregateComponentAggregateKey) {
                if (field == mentorNameField && loadKey == mentorNameLoadKey) {
                    component.set("v.mentorNameControlReference", aggregateComponent);
                } else if (field == mentorAccountNameField && loadKey == mentorAccountNameLoadKey) {
                    component.set("v.mentorAccountControlReference", aggregateComponent);
                } else if (field == mentorIdField && loadKey == mentorIdLoadKey) {
                    component.set("v.mentorIdControlReference", aggregateComponent);
                } else if (field == mentorEmailField && loadKey == mentorEmailLoadKey) {
                    component.set("v.mentorEmailControlReference", aggregateComponent);
                } else if (field == designationField && loadKey == designationLoadKey) {
                    component.set("v.designationControlReference", aggregateComponent);
                }
            }
        };
    },
    handleMentorFieldsChange: function (component, helper) {
        if (!this.isMentorLookupLoaded(component)) {
            throw Error("Not all references found for mentor lookup to work.")
        }
        var mentorId = component.get("v.mentorIdControlReference").get("v.value");
        var mentorEmail = component.get("v.mentorEmailControlReference").get("v.value");

        if (!$A.util.isEmpty(mentorId) && !$A.util.isEmpty(mentorEmail)) {
            this.findMentor(component, helper, mentorId, mentorEmail);
        } else if ($A.util.isEmpty(mentorId) && $A.util.isEmpty(mentorEmail)) {
            this.clearSearchFields(component);
        }
    },
    findMentor: function (component, helper, mentorId, mentorEmail) {

        var designtation = component.get("v.designationControlReference").get("v.value");
        //setting the below attr's to work around issue SFS-392
        component.set("v.idValue", mentorId);
        component.set("v.emailValue", mentorEmail);
        component.set("v.disableValidateButton", true);

        this.setSearchErrorMessage(component, false);
        this.setSpinner(component, true);
        this.sendToService(component, "c.findMentor", {
                mentorId: mentorId,
                mentorEmail: mentorEmail,
                designation: designtation
            },
            function (response) {
                helper.setSpinner(component, false);
                component.set("v.disableValidateButton", false);
                if (!$A.util.isEmpty(response.SUCCESS)) {
                    component.get("v.mentorNameControlReference").set("v.value", response.SUCCESS.name);
                    component.get("v.mentorAccountControlReference").set("v.value", response.SUCCESS.accountName);
                } else {
                    helper.setSearchErrorMessage(component, true);
                    helper.clearSearchFields(component);
                }
            });
    },
    sendToService: function (component, method, params, callback) {
        var action = component.get(method);
        //set parameters send to apex
        if (params) {
            action.setParams(params);
        }

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                callback(response.getReturnValue());
            } else {
                //TODO show APEX error
            }
        });
        $A.enqueueAction(action);
    },
    setSpinner: function (component, visible) {
        var spinnerCmp = component.find("spinner");
        spinnerCmp.set("v.isLoading", visible);
        spinnerCmp.set("v.visible", visible);
    },
    setSearchErrorMessage: function (component, visible) {
        var errorMessageCmp = component.find("errorMessage");
        errorMessageCmp.set("v.isVisible", visible);
    },
    clearSearchFields: function (component) {
        component.get("v.mentorIdControlReference").set("v.value", "");
        component.get("v.mentorEmailControlReference").set("v.value", "");
        component.get("v.mentorNameControlReference").set("v.value", "");
        component.get("v.mentorAccountControlReference").set("v.value", "");

        //setting the below attr's to work around issue SFS-392
        component.set("v.idValue", "");
        component.set("v.emailValue", "");
    },
    isMentorLookupLoaded: function (component) {
        var idCmp = !$A.util.isEmpty(component.get("v.mentorIdControlReference"));
        var emailCmp = !$A.util.isEmpty(component.get("v.mentorEmailControlReference"));
        var nameCmp = !$A.util.isEmpty(component.get("v.mentorNameControlReference"));
        var accountCmp = !$A.util.isEmpty(component.get("v.mentorAccountControlReference"));
        var designationCmp = !$A.util.isEmpty(component.get("v.designationControlReference"));
        return idCmp && emailCmp && nameCmp && accountCmp && designationCmp;
    }
})