({
    handleLoad: function (component, event, helper) {
        helper.fireApplicationEvent(component,
            "e.c:ff_EventFindAggregateControls",
            {
                aggregateKey: component.get("v.aggregateKey"),
                source: component,
                callback: helper.getFindAggregateControlsCallback(component)
            });
    },
    handleValueChangeEvent: function (component, event, helper) {
        var params = event.getParams();
        var mentorIdLoadKey = component.get("v.mentorIdLoadKey");
        var mentorIdField = component.get("v.mentorIdField");
        var mentorEmailField = component.get("v.mentorEmailField");
        var mentorEmailLoadKey = component.get("v.mentorEmailLoadKey");
        var designationLoadKey = component.get("v.designationLoadKey");
        var designationField = component.get("v.designationField");

        if (params.loadKey == mentorIdLoadKey && params.field == mentorIdField) {
            //checking for a changed value to work around issue SFS-392
            if(!$A.util.isUndefinedOrNull(params.value) && params.value != component.get("v.idValue")) {
                helper.handleMentorFieldsChange(component, helper);
            }
        }
        if (params.loadKey == mentorEmailLoadKey && params.field == mentorEmailField) {
            //checking for a changed value to work around issue SFS-392
            if(!$A.util.isUndefinedOrNull(params.value) && params.value != component.get("v.emailValue")) {
                helper.handleMentorFieldsChange(component, helper);
            }
        }
        if (params.loadKey == designationLoadKey && params.field == designationField) {
            helper.clearSearchFields(component);
        }
    },
    validateFields : function(component, event, helper){
        var id = component.get("v.mentorIdControlReference");
        var email = component.get("v.mentorEmailControlReference");

        id.set("v.displayValidationErrors", true);
        email.set("v.displayValidationErrors", true);
        id.set("v.disableValidation", false);
        email.set("v.disableValidation", false);
        id.validate();
        email.validate();
        helper.handleMentorFieldsChange(component, helper);
    }
})