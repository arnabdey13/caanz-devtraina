({
	/**
	 * resetFields Resets all validated fields
	 */
	resetFields: function (component) {
		var fieldIds = component.get("v.fieldIds");

		fieldIds.forEach(function (fieldId) {
			var cmp = component.find(fieldId);
			if (cmp) {
				cmp.set("v.value", "");
			}
		});
	}
})