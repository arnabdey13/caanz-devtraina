({
    /** doInit Sets which fieldIds are processed during validation */
    doInit: function (component, event, helper) {
        var manualEntry = component.get('v.manualEntry');
        if (manualEntry) {
            component.set("v.fieldIds", ['membershipNumber', 'membershipBody', 'name', 'email']);
        }
        else {
            component.set("v.fieldIds", ['memberId']);
        }
    },
    /** handleValuesChange Fire event to register control so it can be accessed by stores that need reference to this control */
    handleValuesChange: function (component, event, helper) {
        var valuesFiltered = component.get('v.valuesFiltered');
        if (valuesFiltered) {
            helper.fireApplicationEvent(component, 'e.c:AppForms_EventControlRegistration');
        }
    },
    /** handleLifeCycle When this component loads, set values based on internal/external.
     * When closing the editor, reset field values.
     */
    handleLifeCycle: function (component, event, helper) {
        // Only respond to events relevant to this component
        if (!component.isInstanceOf('c:AppForms_References')) {
            return;
        }

        var manualEntry = component.get('v.manualEntry');

        switch (event.getParam("type")) {

            case "load-list":
                var manualEntry = component.get('v.manualEntry');
                var values = component.get('v.values');
                var filterValue = (manualEntry) ? 'External' : 'Internal';

                var newValues = values.filter(function (ele) {
                    return ele && ele.Id && ele.Reference_Type__c === filterValue;
                });

                component.set('v.valuesFiltered', true); // Order here is important
                component.set('v.values', newValues);
                break;
            case "editor-pre-close":
                helper.resetFields(component);
                break;
        }
    }
})