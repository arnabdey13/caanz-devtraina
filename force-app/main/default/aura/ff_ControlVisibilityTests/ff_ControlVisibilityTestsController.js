({
    doInit: function (component, event, helper) {
        var test = helper.driver(component, event, helper);
        // references to all the components being tested
        var q1 = component.find("q1");
        var q2 = component.find("q2");
        var q3 = component.find("q3");
        var q4 = component.find("q4");
        var richtext1 = component.find("richtext1");

        var q11 = component.find("q11");
        var q12 = component.find("q12");
        var q50 = component.find("q50");
        var q51 = component.find("q51");
        var q52 = component.find("q52");
        var q53 = component.find("q53");
        var error = component.find("error-modal");

        var btn1 = component.find("btn1");

        var startTests = test.start({
            focused: q1,
            description: "grand parent question"
        })

        var wrappers = {};
        wrappers["Account.yn"] = [
            {value: true, label: "Yes"},
            {value: false, label: "No"}];

        wrappers["Richtext.LoadKey"] = [
            {richtext: "Fake Richtext Content"}];

        startTests

        // should only ever be one load event seen by a page so fire once for all 3 fields
            .then(test.wait(helper.fireLoadEvent("Account", {
                Employed1__c: true, // q1
                Assistance2__c: false, // q2
                Help3__c: "should not be populated", // q3 because it is not visible in the UI
                Questions4__c: "should not be populated", // q4 because it is not visible in the UI
                Employed11__c: true, // q11
                Assistance12__c: true, // q12
                q50__c: true
            }, wrappers)))

            /////// DEPENDENT VIZ ///////

            .then(test.assert("v.loaded", "q1 is loaded from service"))
            .then(test.assert("v.isValid", "q1 is valid after load"))

            .then(test.focus(q2, "second question should be visible after load"))
            .then(test.assert("v.loaded", "q2 is loaded from service"))
            .then(test.assertEquals(false, "v.value", "q2 is unchecked"))
            .then(test.assert("v.isValid", "q2 is valid after load because it's not required"))

            .then(test.focus(q3, "third question should be hidden after load"))
            .then(test.assert("v.loaded", "q3 is loaded from service"))
            .then(test.assert(function () {
                    return $A.util.isEmpty(q3.get("v.value"));
                },
                "q3 was cleared because it is hidden during the load"))
            .then(test.assertEquals(false, "v.visible", "q3 is hidden because it depends on q2"))
            // TODO value change event for q3 qnd q4 value clearing was fired

            .then(test.assertEquals(true, "v.disableValidation", "q3 is not validating because it is hidden"))
            .then(test.assert("v.isValid", "q3 is valid after load because it's not validating while hidden"))

            // user changes q2 to true and q3 should display again and be required
            .then(test.wait(function (context) {
                q2.set("v.value", true);
            }))
            // TODO value change event for q2 should have fired
            .then(test.assertEquals(true, "v.visible", "q3 is visible again because q2 changed"))
            .then(test.assertEquals(true, "v.required", "q3 is required again because q2 changed and it was originally required"))
            .then(test.focus(q4, "4th question should be visible again"))
            .then(test.assertEquals(true, "v.visible", "q4 is visible again because q2 changed"))
            .then(test.assertEquals(false, "v.required", "q4 is not required again it was not originally required"))

            // TODO when Q2 is changed to true, Q3 should re-report invalid via a value change event

            .then(test.wait(function (context) {
                q1.set("v.value", false);
            }))
            .then(test.focus(q2, "q2 depends on q1"))
            .then(test.assertEquals(false, "v.visible", "q2 is hidden by q1"))
            .then(test.assert(function () {
                return $A.util.isUndefinedOrNull(q2.get("v.value"));
            }, "q2 was cleared because it was hidden by q1"))
            .then(test.focus(q3, "q3 depends on q1 and q2"))
            .then(test.assertEquals(false, "v.visible", "q3 is hidden by q1"))
            .then(test.focus(q4, "q4 depends on q1"))
            .then(test.assertEquals(false, "v.visible", "q4 is hidden by q1"))

            ////// STAGE CHANGE VIZ ///////

            .then(test.focus(q11, "4th question is stage 1"))
            .then(test.wait(helper.fireStageChange(0,
                [{label: "Step 1", state: "ACTIVE"}, {label: "Step 2", state: "INCOMPLETE"}],
                {section1: "Employment Info"})))
            .then(test.assertEquals(true, "v.visible", "q11 is visible"))
            .then(test.focus(q12, "5th question is stage 2"))
            .then(test.assertEquals(false, "v.visible", "q12 is hidden"))

            //.then(test.sleep(2000))

            .then(test.wait(helper.fireStageChange(1,
                [{label: "Step 1", state: "COMPLETE"}, {label: "Step 2", state: "ACTIVE"}],
                {section1: "Education"})))
            .then(test.focus(q11, "4th question is stage 1"))
            .then(test.assertEquals(false, "v.visible", "q11 is now hidden"))
            .then(test.focus(q12, "5th question is stage 2"))
            .then(test.assertEquals(true, "v.visible", "q12 is now visible"))

            // leave it in a displayed state to visibly check height units
            .then(test.wait(function () {
                q1.set("v.value", true);
                q2.set("v.value", true);
            }))

            .then(test.focus(q51, "51 is visible because of 50"))
            .then(test.assertEquals(true, "v.visible", "q51 is now visible"))
            .then(test.focus(q52, "52 is visible because of 50"))
            .then(test.assertEquals(true, "v.visible", "q52 is now visible"))
            .then(test.focus(q53, "53 is visible because of 50"))
            .then(test.assertEquals(true, "v.visible", "q53 is now visible"))
            .then(test.focus(q50, "changing value of controlling field for multi-child viz test"))
            .then(test.setAttribute("v.value", false))
            .then(test.focus(q51, "51 is visible because of 50"))
            .then(test.assertEquals(false, "v.visible", "q51 is now hidden"))
            .then(test.focus(q52, "52 is visible because of 50"))
            .then(test.assertEquals(false, "v.visible", "q52 is now hidden"))
            .then(test.focus(q51, "53 is visible because of 50"))
            .then(test.assertEquals(false, "v.visible", "q53 is now hidden"))

            .then(test.focus(btn1, "focus on button 1"))
            .then(test.setAttribute("v.isLoading", true))

            .then(test.focus(error, "focus on error modal"))
            .then(test.wait(helper.fireErrorModal("APEX")))
            .then(test.assertEquals(true, "v.show", "modal is displayed"))
            .then(test.sleep(300))
            //hide the modal
            .then(test.setAttribute("v.show", false))
            .then(test.wait(helper.fireErrorModal("SYSTEM")))
            .then(test.assertEquals(false, "v.show", "modal is not displayed"))

            //////////// END OF TESTS ////////////

            // always include these fns to handle the end of the test
            .then(test.pass).catch(test.fail);

    },
    handleButtonClick: function (component, event, helper) {
        console.log('clicked');
    }
})