({
    fireLoadEvent: function (loadKey, sObject, wrappers) {
        var records = {};
        records[loadKey] = [{
            single: true,
            sObjectList: [sObject]
        }];
        return this.fireApplicationEvent("e.c:ff_EventLoadControls", {
            data: {
                records: records,
                wrappers: wrappers
            }
        });
    },
    fireStageChange: function (stage, stages, headings) {
        return this.fireApplicationEvent("e.c:ff_EventStageChange", {
            stage: stage,
            stages: stages,
            headings: headings
        });
    },
    fireErrorModal: function (type) {
        return this.fireApplicationEvent("e.c:ff_EventError", {
            type: type,
            developerError: {
                TEST: "Error Message",
                TEST2: "Error Message 2"
            }
        });
    }
})