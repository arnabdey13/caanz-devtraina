({
    handleAdd: function (component, event, helper) {
        component.getEvent("crudEvent")
            .setParams({operation: "CREATE"})
            .fire();
    }
})