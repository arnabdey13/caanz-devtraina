({
	doInit: function(cmp, event, helper) {
		helper.getPreferenceData(cmp);
	},
	handleValueChange: function(cmp, event, helper){
		if(helper.validate(cmp, "partial")) helper.saveNotificationData(cmp);
	},
	performSave: function(cmp, event, helper){
		var params = (event.getParam ? event.getParam("arguments") : {}).params || {};
		if(helper.validate(cmp, "all")) {
			helper.saveNotificationData(cmp, params.callback);
		} else if(params.callback) {
			params.callback();
		}
	},
	performReset: function(cmp, event, helper){
		helper.getPreferenceData(cmp);
	}
})