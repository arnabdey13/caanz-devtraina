({
	getPreferenceData: function(cmp) {
		var params = { contactId: cmp.get("v.contactId") };
		this.enqueueAction(cmp, "getPreferenceData", params, function(data){
			cmp.set("v.optoutValues", (data = JSON.parse(data)).optoutValues);
			cmp.set("v.newsletterValues", data.newsletterValues);
			cmp.set("v.affinityValues", data.affinityValues);
			cmp.set("v.productsAndServicesValues", data.productsAndServicesValues);
			this.updateFieldHelpText(cmp, data.fieldHelpTextMap);

			//now update the contact
			cmp.set("v.contact", data.contact);
		});
	},
	updateFieldHelpText: function(cmp, fieldHelpTextMap){
		this.updateOptionHelpText(cmp, "optout", fieldHelpTextMap);
		this.updateOptionHelpText(cmp, "affinity", fieldHelpTextMap);
		this.updateOptionHelpText(cmp, "newsletter", fieldHelpTextMap);
		this.updateOptionHelpText(cmp, "productsAndServices", fieldHelpTextMap);
	},
	updateOptionHelpText: function(cmp, type, fieldHelpTextMap){
		var options = cmp.get("v." + type + "Options");
		options.forEach(function(option){
			option.helpText = fieldHelpTextMap[option.value];
		});
		cmp.set("v." + type + "Options", options);
	},
	validate: function(cmp, saveType){
		var contact = cmp.get("v.contact");
		var optoutValues = cmp.get("v.optoutValues");
		var newsletterValues = cmp.get("v.newsletterValues");
		var affinityValues = cmp.get("v.affinityValues");
		var productsAndServicesValues = cmp.get("v.productsAndServicesValues");
		var isValid = !!((cmp.get("v.saveType") === saveType 
			&& contact && optoutValues && newsletterValues && affinityValues && productsAndServicesValues));
		if(isValid){
			var truthValues = optoutValues.concat(newsletterValues).concat(affinityValues).concat(productsAndServicesValues);
			for(var key in contact){
				var value = contact[key]; if(typeof value !== 'string' && typeof value !== 'boolean') continue;
				value = value === 'Yes' ? true : value === 'No' ? false : value; if(typeof value !== 'boolean') continue;
				if(value !== truthValues.includes(key)) return true; 
			}
		}
		return false;
	},
	saveNotificationData: function(cmp, callback) {
		var params = { 
			preferencesDataJSONString: JSON.stringify({
				contact: cmp.get("v.contact"), optoutValues: cmp.get("v.optoutValues"), 
				newsletterValues: cmp.get("v.newsletterValues"), affinityValues: cmp.get("v.affinityValues"),
				productsAndServicesValues: cmp.get("v.productsAndServicesValues")
			})
		};
		this.enqueueAction(cmp, "savePreferenceData", params, function(data){
			cmp.set("v.contact", (data = JSON.parse(data)).contact);
			if(callback) callback();
		});
	},
	enqueueAction: function(cmp, method, params, callback){
		this.toggleSpinner(cmp, true);
		var action = cmp.get("c." + method); 
		if(params) action.setParams(params);
		action.setCallback(this, function(response){
			if(response.getState() === "SUCCESS") {
				if(callback) callback.call(this, response.getReturnValue());
	        } else if(response.getState() === "ERROR") {
	        	console.error(response.getError());
	        }
	        this.toggleSpinner(cmp, false);
		});
		$A.enqueueAction(action);
	},
	toggleSpinner: function(cmp, show) {
		var spinner = cmp.find("spinner");
		$A.util.addClass(spinner, show ? "slds-show" : "slds-hide");
		$A.util.removeClass(spinner, !show ? "slds-show" : "slds-hide");
	}
})