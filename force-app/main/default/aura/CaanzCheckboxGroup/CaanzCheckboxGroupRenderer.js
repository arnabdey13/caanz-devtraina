({
    afterRender: function(cmp, helper){
        this.superAfterRender(cmp);
        $A.enqueueAction(cmp.get("c.doInit"));
    }
})