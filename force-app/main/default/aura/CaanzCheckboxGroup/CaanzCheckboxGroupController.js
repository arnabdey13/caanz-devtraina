({
	doInit: function(cmp, event, helper) {
		var options = cmp.find("option") || [];
		var values = cmp.get("v.value") || [];
		options.forEach(function(option){
			var element = option.getElement();
			element.checked = values.includes(element.value);
		});
	},
	onchange: function(cmp, event, helper){
		var options = cmp.find("option") || [];
		var values = options.reduce(function(values, option){
			var element = option.getElement();
			if(element.checked) values.push(element.value);
			return values;
		}, []);
		cmp.set("v.value", values);
		var onchange = cmp.get("v.onchange");
		if(onchange) $A.enqueueAction(onchange);
	}
})