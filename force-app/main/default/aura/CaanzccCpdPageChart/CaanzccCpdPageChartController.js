({
    newCpdSummary: function(cmp, evt, helper) {
        helper.getData(cmp);
    },

    handleChartJSLoaded: function(cmp, event, helper) {
        console.log("ChartJS has loaded.")
        window.chartJSLoaded = true;
        
        helper.extendChartJS();
        helper.generateChart(cmp);
    }
})