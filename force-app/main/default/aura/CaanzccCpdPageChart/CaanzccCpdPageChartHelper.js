({
    getData: function(cmp) {

        var cpdSummary = cmp.get('v.cpdSummaryList')

        if (cpdSummary !== null) {
            console.log('Not Null - List Length =  '+cpdSummary.length);
            for (var i = 0; i < cpdSummary.length; i++){
                if (cpdSummary[i].Current_Triennium__c == true){
                    console.log('Found Current Triennium - Start Date = '+ cpdSummary[i].Triennium_Start_Date__c);
                    cmp.set('v.cpdEarnedThisTriennium',cpdSummary[i].CPD_Earned_This_Triennium__c);                            
                    cmp.set('v.requiredTotalTrienniumHours',cpdSummary[i].Required_Total_Triennium_Hours__c);                            
                    cmp.set('v.totalFormalVerifiableHours',cpdSummary[i].Total_Formal_Verifiable_Hours__c);                            
                    cmp.set('v.maximumTrenniumInformalHours',cpdSummary[i].Maximum_Triennium_Informal_Hours__c);                            
                    cmp.set('v.cpdEarnedThisYear',cpdSummary[i].CPD_Earned_This_Year__c);                            
                    cmp.set('v.requiredTotalAnnualHours',cpdSummary[i].Required_Total_Annual_Hours__c);                            
                    cmp.set('v.totalExemptHours',cpdSummary[i].Total_Exempt_Hours__c);                            
                    cmp.set('v.totalFormalExemptHours',cpdSummary[i].Total_Formal_Exempt_Hours__c);                       
                }

            }
            console.log('Loop Completed List  ');

            if(typeof window.chartJSLoaded !== "undefined" && window.chartJSLoaded !== null){
                console.log("Regenerating Chart with CPD data.")
                this.generateChart(cmp);
            }
        }
    },

    generateChart: function(cmp) {

        // Completed: CPD Earned This Triennium / Required Total Triennium Hours
        var cpdEarnedThisTriennium = cmp.get("v.cpdEarnedThisTriennium");
        var requiredTotalTrienniumHours = cmp.get("v.requiredTotalTrienniumHours");
        var totalExemptHours = cmp.get("v.totalExemptHours");
        var possibleTrienniumHours = Math.max(0, requiredTotalTrienniumHours - totalExemptHours);
        var cpdTrienniumRemaining = Math.max(0, possibleTrienniumHours - cpdEarnedThisTriennium);


        // Verified: Total Formal Verifiable Hours / (Required Total Triennium Hours - Maximum Triennium Informal Hours)
        var totalFormalVerifiableHours = cmp.get("v.totalFormalVerifiableHours");
        var maximumTrenniumInformalHours = cmp.get("v.maximumTrenniumInformalHours");
        var totalFormalExemptHours = cmp.get("v.totalFormalExemptHours");
        var possibleFormalVerifiableHours = Math.max(0, requiredTotalTrienniumHours - maximumTrenniumInformalHours - totalFormalExemptHours);
        var formalVerifiableHoursRemaining = Math.max(0, possibleFormalVerifiableHours - totalFormalVerifiableHours);


        // Annual: CPD Earned This Year / Required Total Annual Hours
        var cpdEarnedThisYear = cmp.get("v.cpdEarnedThisYear");
        var requiredTotalAnnualHours = cmp.get("v.requiredTotalAnnualHours");
        var cpdAnnualRemaining = Math.max(0, requiredTotalAnnualHours - cpdEarnedThisYear);

        var donutChartData01 = {
            datasets: [{
                data: [cpdEarnedThisTriennium, cpdTrienniumRemaining],
                backgroundColor: [
                    "#00629E",
                    "#EFEFEF"
                ],
                borderWidth: 0,
            }],
            // labels: [
            //   "Triennium"
            //   // "Completed Triennium Hours"
            // ]
        };

        //two
        var donutChartData02 = {
            datasets: [{
                data: [totalFormalVerifiableHours, formalVerifiableHoursRemaining],
                backgroundColor: [
                    "#B7BF10",
                    "#EFEFEF"
				],
                borderWidth: 0,
            }]
                // labels: [
                //   "Triennium"
                //   // "Verified/Formal Triennium  Hours"
                // ]
        };

        //three
        var donutChartData03 = {
            datasets: [{
                data: [cpdEarnedThisYear, cpdAnnualRemaining],
                backgroundColor: [
                    "#EA7600",
                    "#EFEFEF"
				],
                borderWidth: 0,
            }]
                // labels: [
                //   "Annual"
                //   // "Hours This Year"
                // ]
        };

        var donutOptions = {

            tooltips: {
                enabled: false
            },
            legend: {
                display: true,
                position: "bottom"
            },
            cutoutPercentage: 100
        };
        var ctx = cmp.find("donutchart01").getElement();
        Chart.defaults.global.defaultFontFamily = "'Nexa', Arial, Helvetica, sans-serif'";
        // Chart.defaults.global.defaultFontColor = 'red';
        var donutChart = new Chart(ctx, {
            type: "doughnut",
            options: donutOptions,
            data: donutChartData01
        });

        var ctx = cmp.find("donutchart02").getElement();
        var donutChart = new Chart(ctx, {
            type: "doughnut",
            options: donutOptions,
            data: donutChartData02
        });

        var ctx = cmp.find("donutchart03").getElement();
        var donutChart = new Chart(ctx, {
            type: "doughnut",
            options: donutOptions,
            data: donutChartData03
        });

    },

    extendChartJS: function() {
        var helpers = Chart.helpers;

        // this option will control the white space between embedded charts when there is more than 1 dataset
        helpers.extend(Chart.defaults.doughnut, {
          datasetRadiusBuffer: 0
        });

        Chart.controllers.doughnut = Chart.controllers.doughnut.extend({
          update: function(reset) {
            var me = this;
            var chart = me.chart,
                chartArea = chart.chartArea,
                opts = chart.options,
                arcOpts = opts.elements.arc,
                availableWidth = chartArea.right - chartArea.left - arcOpts.borderWidth,
                availableHeight = chartArea.bottom - chartArea.top - arcOpts.borderWidth,
                minSize = Math.min(availableWidth, availableHeight),
                offset = {
                  x: 0,
                  y: 0
                },
                meta = me.getMeta(),
                cutoutPercentage = opts.cutoutPercentage,
                circumference = opts.circumference;

            // If the chart's circumference isn't a full circle, calculate minSize as a ratio of the width/height of the arc
            if (circumference < Math.PI * 2.0) {
              var startAngle = opts.rotation % (Math.PI * 2.0);
              startAngle += Math.PI * 2.0 * (startAngle >= Math.PI ? -1 : startAngle < -Math.PI ? 1 : 0);
              var endAngle = startAngle + circumference;
              var start = {x: Math.cos(startAngle), y: Math.sin(startAngle)};
              var end = {x: Math.cos(endAngle), y: Math.sin(endAngle)};
              var contains0 = (startAngle <= 0 && 0 <= endAngle) || (startAngle <= Math.PI * 2.0 && Math.PI * 2.0 <= endAngle);
              var contains90 = (startAngle <= Math.PI * 0.5 && Math.PI * 0.5 <= endAngle) || (startAngle <= Math.PI * 2.5 && Math.PI * 2.5 <= endAngle);
              var contains180 = (startAngle <= -Math.PI && -Math.PI <= endAngle) || (startAngle <= Math.PI && Math.PI <= endAngle);
              var contains270 = (startAngle <= -Math.PI * 0.5 && -Math.PI * 0.5 <= endAngle) || (startAngle <= Math.PI * 1.5 && Math.PI * 1.5 <= endAngle);
              var cutout = cutoutPercentage / 100.0;
              var min = {x: contains180 ? -1 : Math.min(start.x * (start.x < 0 ? 1 : cutout), end.x * (end.x < 0 ? 1 : cutout)), y: contains270 ? -1 : Math.min(start.y * (start.y < 0 ? 1 : cutout), end.y * (end.y < 0 ? 1 : cutout))};
              var max = {x: contains0 ? 1 : Math.max(start.x * (start.x > 0 ? 1 : cutout), end.x * (end.x > 0 ? 1 : cutout)), y: contains90 ? 1 : Math.max(start.y * (start.y > 0 ? 1 : cutout), end.y * (end.y > 0 ? 1 : cutout))};
              var size = {width: (max.x - min.x) * 0.5, height: (max.y - min.y) * 0.5};
              minSize = Math.min(availableWidth / size.width, availableHeight / size.height);
              offset = {x: (max.x + min.x) * -0.5, y: (max.y + min.y) * -0.5};
            }

            chart.borderWidth = me.getMaxBorderWidth(meta.data);
            chart.outerRadius = Math.max((minSize - chart.borderWidth) / 2, 0);
            chart.innerRadius = Math.max(cutoutPercentage ? (chart.outerRadius / 100) * (cutoutPercentage) : 0, 0);
            chart.radiusLength = ((chart.outerRadius - chart.innerRadius) / chart.getVisibleDatasetCount()) + 25;
            chart.offsetX = offset.x * chart.outerRadius;
            chart.offsetY = offset.y * chart.outerRadius;

            meta.total = me.calculateTotal();

            me.outerRadius = chart.outerRadius - (chart.radiusLength * me.getRingIndex(me.index));
            me.innerRadius = Math.max(me.outerRadius - chart.radiusLength, 0);

            // factor in the radius buffer if the chart has more than 1 dataset
            if (me.index > 0) {
              me.outerRadius -= opts.datasetRadiusBuffer * me.index;
              me.innerRadius -= opts.datasetRadiusBuffer * me.index;
            }

            helpers.each(meta.data, function(arc, index) {
              me.updateElement(arc, index, reset);
            });
          },
        });
    }
})