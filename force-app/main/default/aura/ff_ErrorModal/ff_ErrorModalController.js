({
    doInit : function(component, event, helper) {
      	component.set("v.message2Default", component.get("v.message2"));  
    },
    closeExceptionModal: function (component, event, helper) {
        component.set("v.show", false);
        helper.fireApplicationEvent(component,
            "e.c:IPP_EventDismissedError"            
        );
    },
    handleErrorEvent: function (component, event, helper) {
        var params = event.getParams();
        var message2Default = component.get("v.message2Default");

        if (params.type == "APEX") {
            /** RXP - 04/09/2019 - Set message2 to message returned from backend, if no message, set to default message **/
            component.set("v.message2", (params.developerError.response.error[0] && params.developerError.response.error[0].message) || message2Default);
            component.set("v.developerMessage",
                JSON.stringify(params.developerError, function (key, value) {
                    return value;
                }, 3));
            component.set("v.show", true);
        }
    },
    
})