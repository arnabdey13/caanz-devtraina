({
	updateSelection: function(cmp, event, helper) {
		var data = cmp.get("v.data");
		var selection = cmp.get("v.selection");
		data.groupItems.forEach(function(item){
			item.myPreference.isSelected = data.isSelectedAll;
			selection.push(item.myPreference);
		});
		cmp.set("v.data", data);
		cmp.set("v.selection", selection);
	},
	change: function(cmp, event, helper){
		var data = cmp.get("v.data");
		var isSelectedAll = !data.groupItems.find(function(item){ return !item.myPreference.isSelected });
		if(data.isSelectedAll !== isSelectedAll){
			data.isSelectedAll = isSelectedAll;
			cmp.set("v.data", data);
		}
	}
})