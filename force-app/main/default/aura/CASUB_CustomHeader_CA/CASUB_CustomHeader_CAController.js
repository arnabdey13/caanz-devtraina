({
	doInit: function(cmp, event, helper) {
		var helpText = cmp.get("v.helpText") || "";
		if(helpText.includes("$Label.")){
			cmp.set("v.helpText", $A.get(helpText));
		}
	}
})