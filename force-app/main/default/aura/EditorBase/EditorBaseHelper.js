({
    getConcreteHelper : function(component) {
        var cc = component.getConcreteComponent();
        return cc.getDef().getHelper();
    },
    optionalInit : function(component, event, helper) {
	    // invoked after base init code
        // no-op by default
    },
    optionalPostSetValue : function(component, event, helper) {
        // invoked after base handling of EditorSetValueEvent
        // no-op by default
    },
    load : function(component, event, helper) {
        $A.log("TODO implement load fn in concrete component", component.getConcreteComponent());        
    },
    displayValidationState : function(component, event, helper) {
        var concreteComponent = component.getConcreteComponent();
        var concreteHelper = this.getConcreteHelper(component);
        var valid = concreteHelper.validate(concreteComponent);
        var validationErrorsFromApex = event.getParam('validationErrors');
        if (validationErrorsFromApex) {
            var apexErrors = event.getParam('validationErrors')[component.get("v.field")];
            component.set("v.apexErrors", apexErrors);
        }
        if (event.getParam("show")) {
            concreteHelper.setUIValid(concreteComponent, valid, typeof apexErrors === "undefined");        
        }
    },
    checkInvalidValues : function(component) {
        var invalidValues = {};
        var invalidList = component.get("v.invalidValuesList");
        if (! $A.util.isEmpty(invalidList)) {
            var vals = invalidList.split(",");
            for (i = 0; i < vals.length; i++) { 
                invalidValues[vals[i]] = true;
            }
        }            
        var value = component.get("v.value");
        var isInvalidValue = invalidValues[value];
        component.set("v.currentValueIsInvalid", isInvalidValue);
        return isInvalidValue;
    },
    // returns an error message assuming that an array of options is in v.options
    // and each option has "value" and "label" keys
    getInvalidValueMessageFromOptions : function(component) {
        var options = component.get("v.options");
        for(var i=0; i<options.length; i++){                        
            if (options[i].value == component.get("v.value")) {
                return "Choosing '" + options[i].label + "' is not allowed";
            }
        }
    },
    // dynamically create an icon and set it as an attribute. useful for updating icons when static resource prefixes are required
    createIcon : function(component, prefix, name, category, size, assistiveText, attribute) {
        $A.createComponent(
            "c:SVGIcon",
            {
                svgPath: prefix+'/resource/icons/salesforce-lightning-design-system-icons/utility-sprite/svg/symbols.svg#'+name,
                name: name,
                category: category,
                size: size,
                assistiveText: assistiveText
            },
            function(newIcon){
                if (component.isValid()) {
                    component.set(attribute, newIcon);
                }
            }
        );
    },
    isProperty : function(component) {
        var fieldName = component.get("v.field");
        if (fieldName) {
            return fieldName.substring(0,5) == "prop.";  
        } else {
            return null;
        }        
    },
    applyChange : function(component, newValue) {
        this.applyChangeValidating(component, newValue, false);
    },
    validateAndNotify : function(component, suppressValidation, newValue, oldValue) {
        var concreteComponent = component.getConcreteComponent();
        var concreteHelper = this.getConcreteHelper(component);
        
        var fieldName = component.get("v.field");
        var props = component.get("v.properties");
                
        var clientValidationDisabled = !component.get("v.clientValidationEnabled");
        // fire validation event
        var isValid = clientValidationDisabled || suppressValidation || concreteHelper.validate(concreteComponent);
        var appEvent = $A.get("e.c:EditorValidationEvent");
        appEvent.setParams({source: component,
                            field: fieldName, 
                            isValid: isValid, 
                            value: newValue,
                            oldValue: oldValue}).fire();
        return isValid;
    },    
    applyChangeValidating : function(component, newValue, suppressValidation) {
        // allows callers of applyChange to stop the client side validation

		var oldValue = component.get("v.oldValue");        
        component.set("v.oldValue", newValue);        
        component.set("v.value", newValue);        

        var fieldName = component.get("v.field");
        var props = component.get("v.properties");
                
        var isValid = this.validateAndNotify(component, suppressValidation, newValue, oldValue);
        var isServerValid = true; // for client side validation, we have to assume server is valid until the next save
        var concreteComponent = component.getConcreteComponent();
        var concreteHelper = this.getConcreteHelper(component);
        concreteHelper.setUIValid(concreteComponent, isValid, isServerValid); // concrete component sets UI
        
        if (isValid) {            
            // transfer the v.value back into the properties object ready for send to apex
            if (this.isProperty(component)) {
                props[fieldName] = newValue;                
            } else {
                props.RECORD[fieldName] = newValue;
            }
            
            // record the dirty flag            
            if ($A.util.isUndefined(props.DIRTY)) {
                props.DIRTY = {};
            }
            props.DIRTY[fieldName] = true;
            
            // record the label used for any change, in case the service needs it
            var label = concreteHelper.getLabel(concreteComponent);
            if (label) {
                if ($A.util.isUndefined(props['LABEL-MAPPINGS'])) {
                    props['LABEL-MAPPINGS'] = {};
                }
                props['LABEL-MAPPINGS'][fieldName] = label;
            }
            
            // fire change event for parent/container components - a component event
            this.fireChangeEvent(component);            
        }
        
        // always fire dependency events since some editors show/hide based on changes to invalid values
        // e.g. when a user chooses No in a confirmation and a different list editor displays
        this.fireDependencyEventIfRequired(component, 1);
        this.fireDependencyEventIfRequired(component, 2);
        this.fireDependencyEventIfRequired(component, 3);
        
        $A.log("applied change in EditorBase", {fieldName: fieldName, isValid: isValid, value: newValue});
    },
    fireChangeEvent : function(component) {
        $A.log("change event firing");
        var changeEvent = component.getEvent("changeEvent");
        changeEvent.setParams({field: component.get("v.field"), 
                               value: component.get("v.value")}).fire();        
    },
    // callers of this fn may(not) pass the isMatchOverride arg
    fireDependencyEventIfRequired : function(component, depsNumber, isMatchOverride) {
        var depVals = component.get("v.dependencyValues"+depsNumber);
        var depKey = component.get("v.dependencyKey"+depsNumber);
        if ($A.util.isUndefined(depKey) && this.getObjectSize(depVals) > 0) {
            $A.log("no depKey set", {label: component.get("v.label")});
        }
        // do nothing is there are no dependency values to match against
        if (depKey && this.getObjectSize(depVals) > 0) { 
            var isMatch;
            if ($A.util.isUndefined(isMatchOverride)) {
                isMatch = depVals[component.get("v.value")];
            } else {
                isMatch = isMatchOverride;
            }
            var eventParams = {dependencyKey: component.get("v.dependencyKey"+depsNumber),
                               isMatch: isMatch};
            $A.log("firing initial dependency event for "+component.get("v.field"), eventParams);
            var appEvent = $A.get("e.c:DependentDisplayEvent");
            appEvent.setParams(eventParams).fire();     
        }    
    },
    setVisibility : function(component, visible) {
        $A.log("setting visibility: "+component.get("v.field"), {visible: visible});
        component.set("v.hidden", !visible);
    },
    toggleVisibility : function(component) {
        component.set("v.hidden", !component.get("v.hidden"));
    },
    // validate the value but don't change the UI state when invalid. used for initially loading the page and when toggling visibility
    handleValidationWithoutUI : function(component) {
		var value = component.get("v.value");        
        this.validateAndNotify(component, false, value, value);        
    },
    validate : function(component) {
        // override this fn and return false if invalid.
        // the override is also reponsible for setting the v.errors on the ui component i.e. to show the error
        return true;
    },
    setUIValid : function(component, isValid) {
        $A.log("TODO implement setUIValid fn to show/hide error markup: "+component.getConcreteComponent());
    },
    getLabel : function(component) {
        // implemented as a helper to allow sub-components to override e.g. EditorTextField
        return component.get("v.label");
    },
    transformListToMap : function(list) {
        var result = {};
        if (list) {
            var strings = list.split(",");
            for (i = 0; i < strings.length; i++) {
                result[strings[i]] = true;
            }
        }
        return result;
    },
    getObjectSize : function(obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    }
})