({
    // validation event logic is important to specify. they are:
    // onLoad: 	if value is empty
    // 				if field required
    // 					fire validation event
    // 			else (value present)
    // 				if value invalid
    // 					fire validation event
    //onChange: always fire validation event AND adjust UI validation state
    // onShow:	same as onLoad
    // 
    // overridden validate helper fns should check the hidden attribute as part of validation
    
    doInit : function(component, event, helper) {
        component.set("v.dependencyValues1",helper.transformListToMap(component.get("v.dependencyValueList1")));
        component.set("v.dependencyValues2",helper.transformListToMap(component.get("v.dependencyValueList2")));
        component.set("v.dependencyValues3",helper.transformListToMap(component.get("v.dependencyValueList3")));
        component.set("v.dependencyKeysToHandle",helper.transformListToMap(component.get("v.dependencyKeysToHandleList")));  
        
        var concreteComponent = component.getConcreteComponent();
        var concreteHelper = helper.getConcreteHelper(component);
        concreteHelper.optionalInit(concreteComponent, event, concreteHelper);
    },
    handleStaticResourceConfig : function(component, event, helper) {
        component.set("v.resourcePrefix",event.getParam("resourcePrefix"));
    },
    handleRecordLoad : function(component, event, helper) {
        
        var properties = event.getParam("properties");
        
        // keep properties in local state
        component.set("v.properties",properties);

        var concreteComponent = component.getConcreteComponent();
        var concreteHelper = helper.getConcreteHelper(component);
        
        // always load the value so that sub-components don't have to
        var fieldName = component.get("v.field");    

        // controller base converts the list of labels required into a map
		var fieldsRequiringLabels = properties['prop.labelsRequiredMap'];
        if (fieldsRequiringLabels && fieldsRequiringLabels[fieldName]) {
            if ($A.util.isUndefined(properties['LABEL-MAPPINGS'])) {
                properties['LABEL-MAPPINGS'] = {};
            }
            properties['LABEL-MAPPINGS'][fieldName] = concreteHelper.getLabel(concreteComponent);
        }        

        if (helper.isProperty(component)) {
            component.set("v.value", properties[fieldName]);
            component.set("v.oldValue", properties[fieldName]);
        } else {
            component.set("v.value", properties.RECORD[fieldName]);
            component.set("v.oldValue", properties.RECORD[fieldName]);
        }
        
        // pass event/properties to all editors
        concreteHelper.load(concreteComponent, event, concreteHelper);
        
        // fire a component event in case a parent component cares
        $A.log("child loaded event firing");
        var changeEvent = component.getEvent("loadedEvent");
        changeEvent.setParams({field: component.get("v.field")}).fire();        
        
        helper.handleValidationWithoutUI(component);
        
        // fire init dependency events to allow other components to adjust visibility
        for (var i = 1; i <= 3; i++) {
	        helper.fireDependencyEventIfRequired(component, i);
        }
    },    
    handleValidationDisplay : function(component, event, helper) {
        try {
            helper.displayValidationState(component, event);        
        } catch (e) {
            console.log("setting validation state failed: "+component.get("v.field"), e);
        }
    },
    // this is a delegate fn so that it can be exposed as a public fn. see cmp file
    handleValidationWithoutUI : function(component, event, helper) {
        helper.handleValidationWithoutUI(component, event, helper);
    },
    // this is a delegate fn so that it can be exposed as a public fn. see cmp file
    setUIValid : function(component, event, helper) {
        var concreteComponent = component.getConcreteComponent();
        var concreteHelper = helper.getConcreteHelper(component);
        concreteHelper.setUIValid(concreteComponent, true, true); // concrete component sets UI
    },
    handleDependency : function(component, event, helper) {
        var field = component.get("v.field");            
        var depKeysToHandle = component.get("v.dependencyKeysToHandle");
        if (helper.getObjectSize(depKeysToHandle) > 0) {
            var isDependent = depKeysToHandle[event.getParam("dependencyKey")];
            var thisEditorWasVisible = !component.get("v.hidden");
            var thisEditorIsBecomingVisible = event.getParam("isMatch");
            if (isDependent) {
                helper.setVisibility(component, thisEditorIsBecomingVisible);
                                
                // check to see if the changing field should be written back to apex as empty
                // i.e. when a dependant field hides and was visible, it should write an empty value
                // back to apex
                var shouldNullValueBeDirty = field // only for editors that are part of apex data
                	&& !$A.util.isEmpty(component.get("v.value")) // only for editors with values
                	&& thisEditorWasVisible // only for editors that were already visible
                	&& !thisEditorIsBecomingVisible; // and is about to hide
                if (shouldNullValueBeDirty) {
                    helper.applyChange(component, null);
                }
                
                // only adjust fire validation state events to controller from editors that change viz state
                helper.handleValidationWithoutUI(component);
                
                // fire dependency events for other editors which depend on this one.
                if (!thisEditorIsBecomingVisible) {
                    for (var i = 1; i <= 3; i++) {
                        // hide all dependent editors
                        helper.fireDependencyEventIfRequired(component, i, false);
                    }
                } else {
                    for (var i = 1; i <= 3; i++) {
                        // conditionally show all dependent editors
                        helper.fireDependencyEventIfRequired(component, i);
                    }
                }
            }
        }
    },
    handleToggle : function(component, event, helper) {
        var key = event.getParam("key");
        if (component.get("v.toggleKey") == key) {
            helper.toggleVisibility(component);
            helper.handleValidationWithoutUI(component);
        }
    },
    handleRecordEditable : function(component, event, helper) {
        component.set("v.disabled", event.getParam("isReadOnly"));
    },
    handleValidationEnable : function(component, event, helper) {
        component.set("v.clientValidationEnabled", event.getParam("enabled"));
        var value = component.get("v.value");        
        var isValid = helper.validateAndNotify(component, false, value, value);        
        var concreteComponent = component.getConcreteComponent();
        var concreteHelper = helper.getConcreteHelper(component);
        concreteHelper.setUIValid(concreteComponent, isValid, true);
    },    
    handleSetValue : function(component, event, helper) {
        $A.log("filtering EditorSetValueEvent: "+component.get("v.field"), {field: component.get("v.field"),
                                                                            eventField: event.getParam("field"),
                                                                            handle: component.get("v.handleSetValueEvents")});
        if (component.get("v.handleSetValueEvents") && component.get("v.field") == event.getParam("field")) {
            $A.log("handling EditorSetValueEvent", {field: component.get("v.field"),
                                                    value: event.getParam("value")});
            helper.applyChangeValidating(component,
                                         event.getParam("value"),
                                         event.getParam("suppressValidation"));
            var concreteComponent = component.getConcreteComponent();
            var concreteHelper = helper.getConcreteHelper(component);
            concreteHelper.optionalPostSetValue(concreteComponent, event, concreteHelper);            
        }
    },
    handleSetClasses : function(component, event, helper) {
        component.set("v.pageClasses", event.getParam("classes"));
    },
    // this fn is exposed as a public api fn so that container components can use it. see EditorAddressFields
    setVisibility : function(component, event, helper) {
        var params = event.getParam('arguments');
        var visible = params.visible;
        helper.setVisibility(component, visible);
        helper.handleValidationWithoutUI(component);
    }
})