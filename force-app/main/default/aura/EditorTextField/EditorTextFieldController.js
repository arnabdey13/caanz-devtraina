({
    onChange : function(component, event, helper) {
        helper.applyChange(component, event.getSource().get("v.value"));
    },
    onExemptChange : function(component, event, helper) {
        var v = component.find("exempt").get("v.value");
        if (v) {
	        helper.applyChangeValidating(component, "EXEMPT", true);
        } else {
	        helper.applyChangeValidating(component, "", false);
        }
        $A.get("e.c:DependentDisplayEvent")
        .setParams({dependencyKey: component.get("v.exemptDependencyKey"),
                    isMatch: v})
        .fire(); 
    }    
})