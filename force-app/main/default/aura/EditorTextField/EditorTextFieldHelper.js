({
    load : function(component, event) {
        var value = component.get("v.value") ;
        // should be disabled if NOT null. NOTE: the property has a bad (opposite) name i.e. disableIfNull 
        // TODO rename disableIfNull
        if(value && component.get("v.disableIfNull")){
            component.set("v.disabled", true);
        }       
        // init the exempt checkbox
        if (component.get("v.exemptEnabled")) {
            var isExempt = value == "EXEMPT";
			component.find("exempt").set("v.value", isExempt);
            
            // fire dep. display for any dependants based on exempt
            $A.get("e.c:DependentDisplayEvent")
            .setParams({dependencyKey: component.get("v.exemptDependencyKey"),
                        isMatch: isExempt})
            .fire();             
        }        
    },
    validate : function(component) {
        var field = component.get("v.field");
        var isRequired = component.get("v.required");
        var maxLength = component.get("v.maxLength");
        var hidden = component.get("v.hidden");
        var value = component.get("v.value");
        var isExemptEnabled = component.get("v.exemptEnabled");
        var isValueMissing = $A.util.isEmpty(value);
        var isNumberInvalid = !(isExemptEnabled && value == "EXEMPT") && (component.get("v.valueType") == "NUMBER") && isNaN(value);
        var isValueTooLong = !isValueMissing && value.length > maxLength;
        var invalid = !hidden && (
            (isRequired && isValueMissing) ||
            isNumberInvalid ||
            isValueTooLong);
        $A.log("validate "+component.get("v.field"), {
            hidden: hidden,
            typeUndefined: $A.util.isUndefined(value),
            empty: value == "",
            invalid: invalid});
        return !invalid;
    },
    setUIValid : function(component, isValidClient, isValidServer) {
        var editor = component.find("editor");
        // if exempt is true then no editor is shown so can skip this
        if (editor) {
            if (! component.get("v.disabled")) {
                if (isValidClient && isValidServer) {
                    editor.set("v.errors", null);
                    $A.util.removeClass(component, "slds-has-error");
                } else {
                    var errorMessages = [] ;
                    if(!isValidClient){
                        var value = component.get("v.value");
                        var isNumberInvalid = (component.get("v.valueType") == "NUMBER") && isNaN(value);
                        var maxLength = component.get("v.maxLength");
				        var isValueTooLong = !$A.util.isEmpty(value) && value.length > maxLength;
                        if (isNumberInvalid) {
                            errorMessages.push({message: "This field must be a number"});
                        } else if (isValueTooLong) {
                            errorMessages.push({message: "This field must be less than "+(maxLength+1)+" characters"});
                        } else {
                            errorMessages.push({message: "This field is required"}) ;
                        }
                    }
                    
                    if(!isValidServer && !isValidServer){                
                        var messages = component.get("v.apexErrors");
                        for(var i=0; i<messages.length; i++){
                            errorMessages.push({message: messages[i]}) ;
                        }
                    }
                    editor.set("v.errors", errorMessages);
                    $A.util.addClass(component, "slds-has-error");
                }
            }
        }
    },
    getLabel : function(component) {
        var label = component.get("v.label");
        // text fields can use empty label and a placeholder instead.
        if ($A.util.isEmpty(label)) {
            label = component.get("v.placeHolder");
        }
        return label;
    },
    
})