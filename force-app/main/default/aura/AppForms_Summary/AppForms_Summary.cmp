<!--
	Author: Kevin Jarvis
	Company: RXP
  	Description: Collects all field values and displays label/value summary representation of the whole application.
					Allows the user to click 'Edit' on a particular section and navigate to it.
	Attributes: See attribute descriptions.
	Event Handlers: TBC
 	History: 10/09/2019 - Created component
-->
<aura:component implements="forceCommunity:availableForAllPageTypes" access="global" extends="c:ff_Control">
    
    <!--Attributes-->
    <aura:attribute name="stages" type="String[]" access="public" description="An internal representation of each form stage, which stores key/value pairs."/>
    <aura:attribute name="activeStage" type="Integer" description="An internal reference of the activeStage. This prevents multiple re-renders if this component is not visible on the current stage."/>
    <aura:attribute name="allControlsLoaded" type="Boolean" default="false" description="A flag to detect on page load if all controls are loaded."/>

	<!--Handlers-->
    <aura:handler event="c:ff_EventStageChange" action="{!c.handleStageChange}"/>
    <aura:handler event="c:AppForms_EventControlRegistration" action="{!c.handleControlRegistration}"/>
    <aura:handler event="c:AppForms_EventDocumentChange" action="{!c.handleDocumentChange}"/>
    <aura:handler event="c:ff_EventValueChange" action="{!c.handleValueChange}"/>
    
    <div>
		<aura:if isTrue="{!and(v.visibleForStage == v.activeStage, v.allControlsLoaded)}">        
            <aura:iteration items="{!v.stages}" var="stage">
                
                <aura:if isTrue="{!stage.fields.length}">

					<lightning:card title="{!stage.label}">
                        <aura:set attribute="actions">
                            <lightning:button label="Edit" class="slds-button--neutral" onclick="{!c.handleStageSelection}" value="{!stage}" />
                        </aura:set>
                    	<aura:iteration items="{!stage.fields}" var="field">
                            <lightning:layout multipleRows="true">
                                <lightning:layoutItem size="6" padding="around-small">
                                    <div>{!field.label}:</div>
                                </lightning:layoutItem>
                                <lightning:layoutItem size="6" padding="around-small">
                                    <div><lightning:formattedRichText value="{!field.value}"></lightning:formattedRichText></div>
                                </lightning:layoutItem>
                            </lightning:layout>
                        </aura:iteration>
                    </lightning:card>
                    
                </aura:if>
                
            </aura:iteration>
        </aura:if>
    </div>
    
</aura:component>