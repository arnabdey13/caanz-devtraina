({
    /** handleStageChange Set the activeStage property when a stage is changed and create the corresponding stage object on v.stages (if not already set) **/
    handleStageChange : function(component, event, helper) {
        var stages = component.get('v.stages');
        var params = event.getParams();
        
        if (Number.isInteger(params.stage)) {
            component.set('v.activeStage', params.stage);
        }
        
        if (params.stages.length && stages.length !== params.stages.length) {
            var combinedStages = params.stages.map(function(ele, index) {
                if (stages[index] && stages[index]['fields']) {
                    ele.fields = stages[index]['fields'];
                    return ele;
                }
                else {
                    return ele;
                }
            });
            component.set('v.stages', combinedStages);
        }
    },
    /** handleStageSelection Navigates the user to the selected stage (used when clicking 'Edit' button for stage) **/
    handleStageSelection : function(component, event, helper) {
		var stageObj = event.getSource().get('v.value');
        helper.fireApplicationEvent(component,
            "e.c:ff_EventStageClick",
            {
                stage: stageObj.stage
            });
    },
    /** handleValueChange When a component value changes, update in summary **/
    handleValueChange : function(component, event, helper) {
        var params = event.getParams();
        var source = params.source;
        helper.addControlValuesToStore(component, source);
    },
    /** handleControlRegistration When a component value loads, update in summary **/
    handleControlRegistration : function(component, event, helper) {
        var params = event.getParams();
        var source = event.getSource();
        if (params.allControlsLoaded) {
            component.set('v.allControlsLoaded', params.allControlsLoaded);
        }
        helper.addControlValuesToStore(component, source);
    },
    /** handleDocumentChange When a document (file upload) is changed, update in summary **/
    handleDocumentChange : function(component, event, helper) {
        var source = event.getSource();
        helper.addControlValuesToStore(component, source);
    }
})