({
    /** addControlValuesToStore Populates the v.stages object representation of all fields, only if the individual field is not hidden.
    		If it is a CPP field, show if CPP is required/not required. **/
    addControlValuesToStore: function (component, source) {
        var stages = component.get('v.stages');
        var visibleForStage = source.get('v.visibleForStage');
        var parentVisibility = source.get('v.parentVisibility');
        var parent1 = source.get('v.parent1');
        var label = source.get('v.label');
        var value = source.get('v.valueLabel') || source.get('v.value');
        var sortIndex = source.get('v.tabIndex');
        var allControlsLoaded = component.get('v.allControlsLoaded');

        // Only run logic for fields with a label
        if (!label) {
            return;
        }

        if (!isNaN(visibleForStage)) {
            if (!stages[visibleForStage]) {
                stages[visibleForStage] = {};
            }
            if (!stages[visibleForStage]['fields']) {
                stages[visibleForStage]['fields'] = [];
            }
            var fieldId = source.get('v.loadKey') + '.' + this.getFieldName(source);
            var fieldIndex = -1;

            if (stages[visibleForStage]['fields']) {
                fieldIndex = stages[visibleForStage]['fields'].findIndex(function (ele) {
                    return ele.id === fieldId;
                });
            }

            value = this.transformValue(source, value);

            var field = {
                id: fieldId,
                label,
                value,
                sortIndex
            };

            if (fieldIndex >= 0) {
                if (parent1 && parentVisibility === false) {
                    stages[visibleForStage]['fields'].splice(fieldIndex, 1);
                }
                else {
                    stages[visibleForStage]['fields'][fieldIndex] = field;
                }
            }
            else {
                if (parent1 && parentVisibility === false) {
                    return;
                }
                stages[visibleForStage]['fields'].push(field);
            }

            if (this.isCPPField(fieldId)) {
                this.setCPPRequiredField(stages);
            }

            // Sort fields as per tabIndex
            if (allControlsLoaded) {
                stages.forEach(function (stage) {
                    if (stage.fields && stage.fields.length) {
                        stage.fields.sort(function (a, b) { return (a.sortIndex || 0) - (b.sortIndex || 0) });
                    }
                });
            }

            component.set('v.stages', stages);
        }
    },
    /** getFieldName Get the current fields name via field attribute or label. **/
    getFieldName: function (source) {
        var field = source.get('v.field');

        if (!field) {
            field = source.get('v.label');
        }

        return field;
    },
    /** isCPPField Determines if the field is a CPP determining field **/
    isCPPField: function (fieldId) {
        return ['Account__r.Principal_Provider_of_Public_Service__c',
            'Account__r.Any_of_CPP_Questions__c',
            'Account__r.AccountingServices_EngagementServices__c'].includes(fieldId);
    },
    /** transformValue For the current field, transform it's value to display in a user friendly manner in the summary **/
    transformValue: function (source, value) {
        if (source.getConcreteComponent) {
            if (value && source.getConcreteComponent().isInstanceOf('c:ff_FieldDateControl')) {
                return new Date(value).toLocaleDateString('en-AU');
            }

            if (source.getConcreteComponent().isInstanceOf('c:IPP_FileUploadControl') && source.get('v.files').length) {
                return source.get('v.files').toString().replace(',', ', ');
            }
        }

        if (source.isInstanceOf('c:form_EmploymentHistoryList')) {
            var values = source.get('v.values');
            var valueString = '';

            values.forEach(function (employer) {
                if (employer.Status__c && employer.Status__c === 'Current') {
                    valueString += 'Current Employer<br>';
                }
                valueString += employer.Employer__r.Name + '<br>' + employer.Job_Title__c + '<br>';
                valueString += employer.Work_Hours__c + ' ' + new Date(employer.Employee_Start_Date__c).toLocaleDateString('en-AU');
                if (employer.Employee_End_Date__c) {
                    valueString += ' - ' + new Date(employer.Employee_End_Date__c).toLocaleDateString('en-AU') + '<br>';
                }
                else {
                    valueString += '<br>';
                }
                valueString += '<br>';
            });
            return valueString;
        }

        if (source.isInstanceOf('c:AppForms_References')) {
            var manualEntry = source.get('v.manualEntry');
            var values = source.get('v.values');
            var valueString = '';

            if (manualEntry) {
                values.forEach(function (reference) {
                    valueString += reference.Membership_Number__c;
                    valueString += '<br>';
                    valueString += reference.Membership_Body__c;
                    valueString += '<br>';
                    valueString += reference.External_Reference_Name__c;
                    valueString += '<br>';
                    valueString += reference.External_Reference_Email__c;
                    valueString += '<br><br>';
                });
            }
            else {
                values.forEach(function (reference) {
                    valueString += reference.Member_ID__c;
                    valueString += '<br><br>';
                });
            }
            return valueString;
        }

        if (typeof value === 'boolean') {
            return (value === true) ? 'Yes' : 'No';
        }
        return value;
    },
    /** setCPPRequiredField Sets the label for CPP required field **/
    setCPPRequiredField: function (stages) {
        var cppId = 'CPPRequired';
        var cppIndex = stages.findIndex(function (stage) {
            return stage && stage.label === 'Certificate of Public Practice';
        });

        if (!stages[cppIndex] || !stages[cppIndex].fields) {
            return;
        }

        var requiresCPP = stages[cppIndex].fields.some(function (field) {
            return field.value === "Yes";
        });

        var cppRequiredIndex = stages[cppIndex].fields.findIndex(function (field) {
            return field.id === cppId;
        });

        var cppField = {
            id: cppId,
            label: "CPP Required",
            value: (requiresCPP) ? "CPP Required" : "Does not require CPP",
            sortIndex: 1
        };

        if (cppRequiredIndex >= 0) {
            stages[cppIndex]['fields'][cppRequiredIndex] = cppField;
        }
        else {
            stages[cppIndex]['fields'].push(cppField);
        }
    }
})