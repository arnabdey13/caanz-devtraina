({
    getApplications : function(component, event, helper) {
        var action = component.get("c.getApplicationsForUser");
        if(component.get("v.filterStatusesCSV")!=null){
            action.setParams({"applicationStatusesCSV": component.get("v.filterStatusesCSV")})
        }
        action.setCallback(this, function(a){
        	var rtnValue = a.getReturnValue();
            //alert('applicationlist:getApplications:rtnValue='+rtnValue);
            $A.log('rtnValue='+rtnValue);
            if (rtnValue !== null) {
                component.set('v.applications',rtnValue);
            }
        });
        $A.enqueueAction(action);
        
        var userAction = component.get("c.getIsAgent");

        userAction.setCallback(this, function(a){
            var rtnValue = a.getReturnValue();
            $A.log('rtnValue='+rtnValue);
            component.set('v.isAgent', rtnValue);
            
        });
        $A.enqueueAction(userAction);

    },
    goToApplicationDetails : function(component, event, helper) { 
        
       
        var ipp = component.get("v.ippPageLink");
        var provisionalApplicationPage = component.get("v.provAppPageLink");
        var application_item = event.target;
        var application_id = application_item.dataset.id;
        var application_type = application_item.dataset.type;
       
        var urlEvent = $A.get("e.force:navigateToURL");
        
        if (application_type == 'Provisional Member') {        
        	urlEvent.setParams({
                "url": '/' + provisionalApplicationPage
            });
        }
        else if (application_type == 'IPP Provisional Member') {        
        	urlEvent.setParams({
                "url": '/' + ipp
            });
        }
        else if (application_type == 'Special Admissions') {
            urlEvent.setParams({
                "url": '/specialadmissions'
            }); 
        }
            
            urlEvent.fire();
    }
})