({
    load : function(component, event) {
        // don't load on EditorLoadEvent because in IE sometimes it is handled before
        // the picklists have handled their own EditorLoadEvent. Instead use the custom loadAfterCountry fn which
        // is called when the country picklist fires an EditorChildLoadedEvent
    },
    loadAfterCountry : function(component) {

        var countryComponent = component.find("country");
        
        var cs = countryComponent.get("v.properties")["prop.countriesAndStates"];
        component.set("v.countriesAndStates", cs);
        
        var selectedCountryCode = countryComponent.get("v.value");
        var country;
        
        var countryOptions = [];
        for (i = 0; i < cs.length; i++) {             
            var opt = {label:cs[i].countryLabel,
                       value:cs[i].countryCode} ;
            countryOptions.push(opt);
            if (selectedCountryCode == cs[i].countryCode) {
                country = cs[i];
            }
        }        
        countryComponent.set("v.options", countryOptions);

        this.loadStates(component, country);
        component.find("country").set("v.selectLabel", $A.util.isUndefined(country)?"Choose a Country":"");
    },
    loadStates : function(component, country) {        
        var stateOptions = [];
        if (country) {
            for (i = 0; i < country.states.length; i++) {             
                var opt = {label:country.states[i][1],
                           value:country.states[i][0]} ;
                stateOptions.push(opt);
            }        
        }
        var statesArePresent = ! $A.util.isEmpty(stateOptions);        

        component.find("state").set("v.options", stateOptions);     
        component.find("state").set("v.required", statesArePresent);
        
        component.find("state").validateWithoutChangingUI();
            
        var promptIsRequired = statesArePresent && $A.util.isEmpty(component.find("state").get("v.value"));
        component.find("state").set("v.selectLabel", promptIsRequired?"Choose a State":"");
    }
})