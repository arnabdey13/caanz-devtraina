({
    doInit : function(component, event, helper) {
        var handleSetValueEventEnabled = component.get("v.handleSetValueEvents") ;
        var fields = ["street", "city", "state", "postcode", "country"];
        for (var i = 0; i < fields.length; i++) {
            component.find(fields[i]).set("v.handleSetValueEvents", handleSetValueEventEnabled);
        }
    },
    handleChildLoadedEvent : function(component, event, helper) {
        if (event.getParam("field") == ("Person" + component.get("v.field") + "CountryCode")) {
            helper.loadAfterCountry(component);
        }
    },
    // pass hidden changes to the address container down to editors so that they adjust validation status
    handleHiddenChange : function(component, event, helper) {
        var hidden = event.getParam("value");
        var fields = ["street", "city", "state", "postcode", "country"];
        for (var i = 0; i < fields.length; i++) {
            component.find(fields[i]).setVisible(!hidden);
        }
    },
    // keep country and state in sync
    handleChangeEvent : function(component, event, helper) {
        var editor = event.getSource();
        var country = component.find("country");
        var isCountryChange = editor === country;
        if (isCountryChange) {
            var selectedCountryCode = component.find("country").get("v.value");
            var cs = component.get("v.countriesAndStates");
            var country;
            for (i = 0; i < cs.length; i++) {             
                if (selectedCountryCode == cs[i].countryCode) {
                    country = cs[i];
                    break;
                }
            }
            
            // unset the state value for the picklist 
            component.find("state").set("v.value", "");
            // and in the share properties object
            component.get("v.properties").RECORD[component.find("state").get("v.field")] = "";   
            
            // must load after state values have been emptied since loadStates performs a validation 
            // of the null value which blocks save until it has been populated.
            helper.loadStates(component, country);
            
            // since values in state have changed, remove invalid UI state until the user tries another save
            component.find("state").setUIValid();
        }
    }
})