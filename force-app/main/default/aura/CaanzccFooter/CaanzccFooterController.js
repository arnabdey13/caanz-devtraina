({
    goPrivacy: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://www.charteredaccountantsanz.com/privacy-policy"
        });
        urlEvent.fire();
    },
    goTerms: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://www.charteredaccountantsanz.com/terms-of-use"
        });
        urlEvent.fire();
    },
    goContact: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://www.charteredaccountantsanz.com/contact-us"
        });
        urlEvent.fire();
    },
    goComplaint: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "https://www.charteredaccountantsanz.com/about-us/complaints"
        });
        urlEvent.fire();
    }
})