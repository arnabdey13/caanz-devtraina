({
    load: function (component, event, helper) {
        var readData = event.getParam("value").data;
        var optionsKey = component.get("v.optionsKey");
        var wrappers = readData.wrappers[optionsKey];
        
        if (wrappers && !$A.util.isEmpty(wrappers)) {
            component.set("v.allOptions", wrappers);
        }
        var cfp = helper.getControllingField(component);

        if (!$A.util.isUndefinedOrNull(cfp)) {
            var controllingWrappers = readData.records[cfp[0]];

            if ($A.util.isEmpty(controllingWrappers)) {
                throw Error("Wrapper for dependent select is empty: " + cfp[0]);
            }
            //TODO what if wrapper position for controlling field is not 0
            var controllingWrapperPosition = 0;
            var controllingSObject = controllingWrappers[controllingWrapperPosition].sObjectList[0];

            if ($A.util.isEmpty(controllingSObject)) {
                throw Error("SObject for dependent select is empty");
            }
            var controllingValue = controllingSObject[cfp[1]];

            if (!$A.util.isUndefinedOrNull(controllingValue)) {
                component.set("v.controllerValue", controllingValue);
            }
        }
        helper.setOptions(component);
        helper.setValueLabel(component);
    },
    handleUIChange: function (component, event, helper) {
        var v = event.target.value;
        component.set("v.value", v);
    },
    handleSelectValueChange: function (component, event, helper) {
        var cfp = helper.getControllingField(component);
        if (!$A.util.isUndefinedOrNull(cfp)) {
            var params = event.getParams();
            if (params.loadKey == cfp[0] && params.field == cfp[1]) {
                // controlling field just fired a value change, adjust options
                helper.adjustDependency(component, params.value);
            }
        }
        if (event.getParams().source.getGlobalId() === component.getGlobalId()) {
            helper.setValueLabel(component);
        }
    },
    //TODO removed api function because it is not exposed when component is referenced in Aura.Component
    // apiAdjustDependency: function (component, event, helper) {
    //     var params = event.getParam("arguments");
    //     helper.adjustDependency(component, params.value);
    // },
    adjustDependency: function (component, event, helper) {
        helper.setOptions(component);
    },
    handleOptionsChange: function (component, event, helper) {
        //helper.setValueLabel(component);
        helper.adjust(component);
    },
    handleAllOptionsChange: function (component, event, helper) {
        helper.setOptions(component);
    }
})