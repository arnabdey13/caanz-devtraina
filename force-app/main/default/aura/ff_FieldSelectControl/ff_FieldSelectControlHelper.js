({
    setOptions: function (component) {
        var controllingField = component.get("v.controllingField");
        var allOptions = component.get("v.allOptions");
        var value = component.get("v.value");
        var newOptions = [];
        /** RXP 29/08/2019 - START - optionsToExclude does not render option in select **/
        var optionsToExclude = component.get("v.optionsToExclude");
        
        if (optionsToExclude) {
            optionsToExclude = optionsToExclude.split(',');
        }
        /** RXP 29/08/2019 - END - optionsToExclude does not render option in select **/

        if ($A.util.isEmpty(controllingField)) {
            for (var i = 0; i < allOptions.length; i++) {
                    newOptions.push(allOptions[i]);
            }
            
        } else {
            var controllingValue = component.get("v.controllerValue");
            var isValueInNewOptions = false;
            for (var i = 0; i < allOptions.length; i++) {
                if (allOptions[i].parentValues && this.arrayContains(allOptions[i].parentValues, controllingValue)) {
                    newOptions.push(allOptions[i]);
                    if(value == allOptions[i].value && !isValueInNewOptions){
                        isValueInNewOptions = true;
                    }
                }
            }

            if(!$A.util.isEmpty(value) && !$A.util.isEmpty(newOptions) && !isValueInNewOptions){
                component.set("v.value", "");
            }
        }
        
        /** RXP 29/08/2019 - START - optionsToExclude does not render option in select **/
        if (optionsToExclude && optionsToExclude.length) {
            newOptions = newOptions.filter((ele, index) => {
              	return !optionsToExclude.includes(ele.value);
            });
        }
        /** RXP 29/08/2019 - END - optionsToExclude does not render option in select **/
        
        /** RXP 29/08/2019 - START - sort options alphabetically **/
        newOptions.sort(function(a,b){
            	var nameA = a.label && a.label.toUpperCase();
            	var nameB = b.label && b.label.toUpperCase();
            if(nameA < nameB){
                return -1;
            }
            if(nameA > nameB){
                return 1;
            }
            	return 0;
            });
        /** RXP 29/08/2019 - END - sort options alphabetically **/
       component.set("v.options", newOptions);
    },
    /** RXP 29/08/2019 - START - Set valueLabel which is the label of the selected value **/
    setValueLabel: function(component) {
      	var value = component.get("v.value");
        var options = component.get("v.options");
        
        var selectedOption = options.find(function(option) {
           return option.value === value;
        });
        
        component.set("v.valueLabel", (selectedOption) ? selectedOption.label : "");
        // RXP 16/08/2019 - start - Fire event to register control so it can be accessed by stores that need reference to this control
        this.fireApplicationEvent(component, 'e.c:AppForms_EventControlRegistration');
    },
    /** RXP 29/08/2019 - END - Set valueLabel which is the label of the selected value **/
    adjust: function (component) {
        var value = component.get("v.value");
        var options = component.get("v.options");
        var firstOptionLabel = $A.util.isEmpty(options) ? "" : options[0].label;
        component.set("v.disabled", options.length == (value ? 0 : 1));
    },
    getControllingField: function (component) {
        var controllingField = component.get("v.controllingField");
        if (!$A.util.isEmpty(controllingField)) {
            var cfp = controllingField.split(".");
            if (cfp.length == 2) {
                return cfp;
            } else {
                $A.warning(component.get("v.loadKey") + "." + component.get("v.field") + ": Invalid controlling field, cannot load!");
            }
        }
    },
    arrayContains: function (a, obj) {
        for (var i = 0; i < a.length; i++) {
            if (a[i] === obj) {
                return true;
            }
        }
        return false;
    },
    adjustDependency: function (component, value) {
        component.set("v.controllerValue", value);
        //Commented the below because a change handler is listening to the controllerValue
        // change which then call the setOptions helper.
        // this.setOptions(component);
    }
})