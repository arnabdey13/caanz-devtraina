({
	getNotificationData: function(cmp) {
		var params = { contactId: cmp.get("v.contactId") };
		this.enqueueAction(cmp, "getNotificationData", params, function(data){
			cmp.set("v.member", (data = JSON.parse(data)).member);
			cmp.set("v.memberValues", data.memberValues);
			cmp.set("v.originalDisabledValue", data.member.PreferencesDisableAllFeedsEmail);
		});
	},
	validate: function(cmp, saveType){
		var member = cmp.get("v.member");
		var memberValues = cmp.get("v.memberValues");
		var isValid = !!(cmp.get("v.saveType") === saveType && member && memberValues);
		if(isValid){
			var truthValues = memberValues;
			for(var key in member){
				var value = member[key]; if(typeof value !== 'boolean') continue;
				if(key === 'PreferencesDisableAllFeedsEmail'){
					if(value !== cmp.get("v.originalDisabledValue")) return true; 
				} else if((value = !value) !== truthValues.includes(key)) {
					return true;
				}
			}
		}
		return false;
	},
	saveNotificationData: function(cmp, callback) {
		var params = { 
			notificationDataJSONString: JSON.stringify({
				member: cmp.get("v.member"),
				memberValues: cmp.get("v.memberValues")
			})
		};
		this.enqueueAction(cmp, "saveNotificationData", params, function(data){
			cmp.set("v.member", (data = JSON.parse(data)).member);
			cmp.set("v.originalDisabledValue", data.member.PreferencesDisableAllFeedsEmail);
			if(callback) callback();
		});
	},
	enqueueAction: function(cmp, method, params, callback){
		this.toggleSpinner(cmp, true);
		var action = cmp.get("c." + method); 
		if(params) action.setParams(params);
		action.setCallback(this, function(response){
			if(response.getState() === "SUCCESS") {
				if(callback) callback.call(this, response.getReturnValue());
	        } else if(response.getState() === "ERROR") {
	        	console.error(response.getError());
	        }
	        this.toggleSpinner(cmp, false);
		});
		$A.enqueueAction(action);
	},
	toggleSpinner: function(cmp, show) {
		var spinner = cmp.find("spinner");
		$A.util.addClass(spinner, show ? "slds-show" : "slds-hide");
		$A.util.removeClass(spinner, !show ? "slds-show" : "slds-hide");
	}
})