({
    /* Method to assign values in radio Option */
    doInit : function(component,event,helper){
        //helper.setCPPQuestionVisibility(component);
    },
    saveCPDInformation : function(component,event,helper){
        var checkValidity = true
        var questionnaireResponseSobject = component.get("v.questionnaireResponseSobject");
        if((!$A.util.isUndefined(component.find('Answer_1__c')) && !component.find('Answer_1__c').checkValidity())){
           checkValidity = component.find('Answer_1__c').checkValidity();
        }
       
        if(checkValidity){
            var questionaireResponseObjectToUpdate = component.get("v.questionaireResponseObjectToUpdate");
            questionaireResponseObjectToUpdate.Question_1__c = questionnaireResponseSobject.Question_1__c;
            questionaireResponseObjectToUpdate.Answer_1__c = questionnaireResponseSobject.Answer_1__c;
            questionaireResponseObjectToUpdate.Question_46__c = questionnaireResponseSobject.Question_46__c;
            questionaireResponseObjectToUpdate.Answer_46__c = questionnaireResponseSobject.Answer_46__c;
            if (!$A.util.isUndefinedOrNull(questionnaireResponseSobject.Id)) {
                questionaireResponseObjectToUpdate.Id = questionnaireResponseSobject.Id;
                questionaireResponseObjectToUpdate.Residential_Country__c = questionnaireResponseSobject.Residential_Country__c;
            }
            helper.upsertCPD(component,questionaireResponseObjectToUpdate); 
        }else{
            helper.showToast(component,questionnaireResponseSobject.Question_1__c);
            component.find('Answer_1__c').focus();
        }
         
    },
    handleCPDHoursChange: function(component,event,helper){
        var cpp = event.getParam("value");
        if($A.util.isEmpty(cpp)){
            component.set("v.questionnaireResponseSobject.Answer_46__c","");
        }
    }
    
})