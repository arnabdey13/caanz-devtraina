({  
    setCPPQuestionVisibility:function(component){
        // https://charteredaccountantsanz.atlassian.net/browse/ARTTB-424
        // If member is a full or affiliate audito
        /* System checks if the member is a CPP holder and Affiliate, or full members and is not:
                    -aged 60 or over at 30/6/19; or
                    -admitted to Full membership after 30/6/18
	    */
        var account = component.get("v.account");
        var casubMandatoryNotificationConfig = component.get("v.casubMandatoryNotificationConfig");
        var memberAge = component.get("v.memberAge");
        
        var membershipAppDate = account.Membership_Approval_Date__c;
        var cppPickList = account.CPP_Picklist__c;
        var cpp = false;
        var configuredDataSetUp = casubMandatoryNotificationConfig;
        var configureFullMembershipConditionalDate = configuredDataSetUp.Membership_Approval_Date__c; 
        var isMembershipApprovalDateIsMoreThenConfiguredOne = false;
        if(!$A.util.isUndefined(membershipAppDate) && membershipAppDate > configureFullMembershipConditionalDate){
            isMembershipApprovalDateIsMoreThenConfiguredOne=true;
        }
        if(cppPickList=='Full' || cppPickList=='Exempt' || cppPickList=='Monitored Member' || cppPickList=='Concessional'){
            cpp=true;
        }
        if(cpp && (account.Membership_Class__c == 'Affiliate' || account.Membership_Class__c == 'Full') 
           && (memberAge <= 60 || isMembershipApprovalDateIsMoreThenConfiguredOne )){
            component.set('v.isCPPQuestionAllowed', true);
        }
    },
    upsertCPD: function(component,questionaireResponseObjectToUpdate,callBackParams){
        var subscriptionSobject = component.get("v.globalSubscriptionSobject");
        if($A.util.isUndefinedOrNull(subscriptionSobject.Obligation_Sub_Components__c )){
            subscriptionSobject.Obligation_Sub_Components__c = 'CPD (Continuing Professional Development);';
        }
        else if(!subscriptionSobject.Obligation_Sub_Components__c.includes("CPD (Continuing Professional Development)")){
            if(subscriptionSobject.Obligation_Sub_Components__c.substring(subscriptionSobject.Obligation_Sub_Components__c.length - 1) == ';')
                subscriptionSobject.Obligation_Sub_Components__c += 'CPD (Continuing Professional Development);;';
            else
                subscriptionSobject.Obligation_Sub_Components__c += ';CPD (Continuing Professional Development);';
        }
        this.validateRegistrationQuestionsAndAnswers(component,questionaireResponseObjectToUpdate,function(){
            var param = {
                'personAccountId':component.get("v.personAccountId"),
                'questionaireResponseObjectToUpdate':questionaireResponseObjectToUpdate,
                'selectedCountry':component.get("v.selectedCountry"),
                'subscriptionSobject': subscriptionSobject
            };
            this.enqueueAction(component, "upsertQuestionaireResponse", param, function(error, data){
                var questionResponseSubscriptionWrappper = data;
                if(error){
                    component.set("v.errorMessage",error);
                }else if(!$A.util.isUndefinedOrNull(callBackParams) && callBackParams.callback) {
                    if (!$A.util.isUndefinedOrNull(questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id)) {
                        component.set("v.questionnaireResponseSobject.Id",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id);
                        component.set("v.questionnaireResponseSobject.Residential_Country__c",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Residential_Country__c);
                        component.set("v.globalSubscriptionSobject",questionResponseSubscriptionWrappper.subscriptionSObject);
                        /*
                        component.set("v.questionnaireResponseSobject.Id",data.Id);
                        component.set("v.questionnaireResponseSobject.Residential_Country__c",data.Residential_Country__c);
                        */
                }
                component.set("v.errorMessage",'');
                callBackParams.callback(); 
            }else{
                if (!$A.util.isUndefinedOrNull(questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id)) {
                    component.set("v.questionnaireResponseSobject.Id",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id);
                    component.set("v.questionnaireResponseSobject.Residential_Country__c",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Residential_Country__c);
                    component.set("v.globalSubscriptionSobject",questionResponseSubscriptionWrappper.subscriptionSObject);
                    /*
                        component.set("v.questionnaireResponseSobject.Id",data.Id);
                        component.set("v.questionnaireResponseSobject.Residential_Country__c",data.Residential_Country__c);*/
                }
            }
            
        });
        });
        
    },
    enqueueAction: function(component, method, params, callback){
        this.toggleSpinner(component, true);
        var action = component.get("c." + method); 
        if(params) action.setParams(params);
        action.setCallback(this, function(response){
            this.toggleSpinner(component, false);
            if(response.getState() === "SUCCESS") {
                if(callback) callback.call(this, null, response.getReturnValue());
            } else if(response.getState() === "ERROR") {
                var message = 'Unknown error'; 
                var errors = response.getError();
                if (!$A.util.isEmpty(errors)) {
                    message = errors[0].message;
                }
                console.error(message);
                if(callback) callback.call(this, message);
            }
        });
        $A.enqueueAction(action);
    },
    toggleSpinner: function(component, show) {
        var spinner = component.find("cppSpinner");
        $A.util.addClass(spinner, show ? "slds-show" : "slds-hide");
        $A.util.removeClass(spinner, !show ? "slds-show" : "slds-hide");
    },
    validateRegistrationQuestionsAndAnswers : function(component,questionaireResponseObjectToUpdate,callback){ 
        var errorMessage = '';
        var isValidate=false;
        var isCPPQuestionAllowed = component.get('v.isCPPQuestionAllowed');
        if(isCPPQuestionAllowed && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_1__c) || questionaireResponseObjectToUpdate.Answer_1__c=='') ){
            errorMessage = questionaireResponseObjectToUpdate.Question_1__c;
            isValidate=true;
        }else if(isCPPQuestionAllowed && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_46__c) || questionaireResponseObjectToUpdate.Answer_46__c=='')){
            errorMessage = "Please select option for - " + questionaireResponseObjectToUpdate.Question_46__c;
            isValidate=true;
        }
        if(!isValidate){
            if(callback) callback.call(this);
        }else{
            this.showToast(component,errorMessage);
        }
        
    },
    showToast : function(component,text,questionnaireResponseSobjectId) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Warning!",
            "message": text,
            type : 'warning',
            mode: 'dismissible ',
        });
        toastEvent.fire();
    }
})