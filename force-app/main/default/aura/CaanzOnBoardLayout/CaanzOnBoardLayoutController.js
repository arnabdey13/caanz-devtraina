({
	doInit: function(cmp, event, helper){
		$A.createComponent(
            "c:CaanzOnBoardWizardRouter", { onCheckDone: cmp.get("c.showTsAndCs") },
            function(router){ cmp.set("v.router", router); }            
        );
	},
	showTsAndCs: function(cmp, event, helper){
		cmp.set("v.showWizard", false);
		cmp.set("v.showTsAndCs", true);
	},
	showWizard: function(cmp, event, helper){
		cmp.get("v.router").getContactId($A.getCallback(function(contactId) {
			cmp.set("v.contactId", contactId);
			cmp.set("v.showWizard", true);
			cmp.set("v.showTsAndCs", false);
		}));
	},
	showRejectMessage: function(cmp, event, helper){
		window.location.href = "/" + $A.get("$Label.c.Community_Prefix_CCH") + '/secur/logout.jsp';
	},
	acceptTcAndCs: function(cmp, event, helper) {
		cmp.get("v.router").acceptTcAndCs(cmp.get("c.showWizard"));
	},
	rejectTcAndCs: function(cmp, event, helper) {
		cmp.get("v.router").rejectTcAndCs(cmp.get("c.showRejectMessage"));
	},
	finishWizard: function(cmp){
		cmp.get("v.router").finishWizard();
	}
})