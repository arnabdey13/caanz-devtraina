({	
    activeSection : function(component, event, helper) {
        var tab = event.getSource();
        var tabId = tab.get("v.id");
        component.set("v.sectionName",tabId);
    },
})