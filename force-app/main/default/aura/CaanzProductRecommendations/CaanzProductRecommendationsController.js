({
	doInit: function(cmp, event, helper) {
		helper.getRecommendations(cmp);
	},
	updateRecommendations: function(cmp, event, helper) {
		helper.getRecommendations(cmp);
	},
	showProduct: function(cmp, event, helper) {
		var url = event.currentTarget.dataset.url;
		var id = event.currentTarget.dataset.id;
		if (typeof url === "undefined" || url === null || url === "") {
			url = "/" + id;
		}
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
        	"url": url
        });
        urlEvent.fire();
	},
	goToRecoProducts : function(cmp, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
        	"url": '/recoproduct'
        });
        urlEvent.fire();		
	}
})