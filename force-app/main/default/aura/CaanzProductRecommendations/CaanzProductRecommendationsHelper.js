({
	getRecommendations: function(cmp) {
		var params = { limitQuery: cmp.get("v.limit"), sortUniqueFirst: cmp.get("v.sortUniqueFirst") };
		this.enqueueAction(cmp, "getRecommendations", params, function(data){
			cmp.set("v.recommendations", JSON.parse(data));
		});
	},
	enqueueAction: function(cmp, method, params, callback){
		var action = cmp.get("c." + method); 
		if(params) action.setParams(params);
		action.setCallback(this, function(response){
			if(response.getState() === "SUCCESS") {
				if(callback) callback.call(this, response.getReturnValue());
	        } else if(response.getState() === "ERROR") {
	        	console.error(response.getError());
	        }
		});
		$A.enqueueAction(action);
	}
})