({
    handleLifeCycle: function (component, event, helper) {
        event.stopPropagation();
        var eventType = event.getParam("type");
        switch (eventType) {
            case "pre-load":
                var readData = component.get("v.readData");
                var records = readData.records;
                var account = records.Account[0].sObjectList[0];
                helper.setFocusedRecord(component, account.Id, "Account", 0);
                // make the application the primary focus for saves and focus auto-switch
                helper.setPrimaryFocus(component, account.Id, "Account", 0);
                break;
            case "post-load":
                // nothing to do here
                break;
            case "post-value-change":
                // nothing to do here
                break;
            case "pre-save":
                component.find("save-button").set("v.isLoading", true);
                break;
            case "post-save":
                component.find("save-button").set("v.isLoading", false);
                var saveResult = event.getParam("opts");
                if (saveResult.success) {
                    $A.log("Write results: " + JSON.stringify(saveResult.results));
                } else {
                    console.log("Save Failed!");
                    console.log(JSON.stringify(saveResult));
                }
                break;
            default:
                console.log("unhandled life-cycle event: " + eventType);
        }
    },
    handleSave: function (component, event, helper) {
        if (component.get("v.formValid")) {
            // ensure that focus is on primary object
            helper.restorePrimaryFocus(component);
            // now send that object to apex
            helper.sendFocusedRecord(component, "NO-OP");
        } else {
            component.set("v.submissionError", true);
            helper.fireApplicationEvent(component,
                "e.c:ff_EventSetValidation",
                {
                    disable: false,
                    show: true
                }
            );
        }
    },
    handleFormValidChange: function (component, event, helper) {
        var valid = event.getParams();
        if(valid.value){
            component.set("v.submissionError", false);
        }
    }
})