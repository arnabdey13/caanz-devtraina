({
	handleLifeCycle: function (component, event, helper) {

		switch (event.getParam("type")) {
			case "load-field":
				//get loadData object
				var readData = component.get("v.loadData").data;

				//get wrapperFilesKey object using its key - Application__c.Files
				var wrapperFileKey = component.get("v.loadKey") + ".Files";

				//get wrapperFiles using wrapperFilesKey - WrapperFiles obj that contains files
				var wrapperFiles = readData.wrappers[wrapperFileKey];

				//utilise wrapperFiles parent to load related existing files
				component.set("v.myRecordId", wrapperFiles[0].parent);

				helper.loadFromWrappers(component, wrapperFiles);

				break;
		}

	},
	//this method is needed for validateUpload to access parent component
	parentVizChangeHandler: function (component, event, helper) {

		helper.validate(component, event, helper);
		helper.fireValueChange(component);

	},
	//set up error validation on the breadcrumb of the page		
	validateUploads: function (component, event, helper) {
		//it list out all fileUploads in entire forms
		event.stopPropagation(); // since handling it's own event, nobody else cares

		var uploadSpecificErrors = [];
		var parentVisibility = component.get("v.parentVisibility");

		if (parentVisibility && component.get("v.files").length < component.get("v.fileCountRequired")) {
			uploadSpecificErrors.push(component.get("v.minRequiredMessage"));
			component.set("v.filerequirederror", true);
		}

		//set red error on page's breadcrumb
		component.set("v.specificValidationErrors", uploadSpecificErrors);

	},
	//if there is a change in fileUpload, this function is executed	
	handleProgressChange: function (component, event, helper) {
		var fileLength = parseInt(component.get("v.files").length);
		if (fileLength) {
			helper.validate(component, event, helper);
			helper.fireValueChange(component);
		}
	},
	//after upload has completed
	handleUploadFinished: function (component, event, helper) {
		helper.updateDocument(component, event);
	},

	deleteDocument: function (component, event, helper) {
		var fileId = event.getSource().get("v.value");
		alert('deleting document ' + fileId);
	}

})