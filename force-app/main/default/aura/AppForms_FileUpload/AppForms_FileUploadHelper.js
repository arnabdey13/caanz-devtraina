({
    loadFromWrappers: function (component, wrappers) {

        //Load existing files and display them on the page
        if (wrappers) {
            //field - LetterOfGoodStanding
            var field = component.get("v.field");

            for (var i = 0; i < wrappers.length; i++) {

                //get object from wrapper - it contains field and filename
                var singleFileFromService = wrappers[i].fileList[0];
                if (singleFileFromService) {
                    var fileNames = [];
                    var fileChangeObject = {
                        label: component.get("v.label"),
                        files: [],
                        sortIndex: component.get("v.visibleForStage")
                    };

                    for (var j = 0; j < wrappers[i].fileList.length; j++) {

                        var nameParsed = wrappers[i].fileList[j].Title.split(":");
                        var withPrefix = wrappers[i].fileList[j].Title;

                        if (nameParsed[0] === field) {
                            var fileName = withPrefix.substring(field.length + 1);
                            var fileId = wrappers[i].fileList[j].Id;

                            fileNames.push(fileName);
                            fileChangeObject.files.push({
                                fileName,
                                fileId
                            });
                        }
                    }
                    //pass unique file only
                    //component.set("v.files", fileNames.filter((e,i)=> fileNames.indexOf(e) >= i));
                    //var uniqueFiles = fileNames.filter((e, i) => fileNames.indexOf(e) >= i);
                    var files = this.setFilesArray(fileChangeObject.files);
                    component.set("v.files", files);

                    // Fire application event to notify any subscribed components that files have been loaded
                    this.fireApplicationEvent(component, "e.c:AppForms_EventDocumentChange",
                        {
                            eventType: "LOAD",
                            payload: fileChangeObject
                        });
                }

                // always load the parentId, this ensures that empty controls still have a parentId
                component.set("v.parentId", wrappers[i].parent);
            }
        }
        else {
            throw Error("wrappers not present");
        }

    },
    /** setIconNames Converts the files array to object array, setting fileName, iconName, fileId */
    setFilesArray: function (files) {
        return files.map((file) => {
            var fileExt = file.fileName.split('.');
            fileExt = fileExt[fileExt.length - 1];

            return {
                fileName: file.fileName,
                fileId: file.fileId,
                iconName: this.getIconName(fileExt)
            }
        });
    },
    /** getIconName Based on file extension, set icon type for display in UI */
    getIconName: function (fileExt) {
        switch (fileExt.toLowerCase()) {
            case "csv":
                return "doctype:csv";
            case "doc":
            case "docx":
                return "doctype:word";
            case "pdf":
                return "doctype:pdf";
            case "txt":
                return "doctype:txt";
            case "xls":
            case "xlsx":
                return "doctype:excel";
            case "xml":
                return "doctype:xml";
            case "png":
            case "jpg":
            case "jpeg":
            case "gif":
                return "doctype:image";
            default:
                return "doctype:box_notes";
        }
    },

    updateDocument: function (component, event) {
        //label of fileUpload that is inherited from parent

        //declare local variables	
        var parentId = component.get("v.myRecordId");
        var field = component.get("v.field");
        var action = component.get("c.UpdateFiles");
        var uploadFiles = event.getParam("files");
        var toastEvent = $A.get("e.force:showToast");

        //declare empty arrays
        var nameRes = [];
        var filenames = [];
        var docIds = [];

        if (field) {

            //concat field with filename
            for (var i = 0; i < uploadFiles.length; i++) {
                filenames.push(field + ":" + uploadFiles[i].name);
                docIds.push(uploadFiles[i].documentId);
            }

            action.setParams({
                docId: docIds,
                title: filenames,
                recId: parentId
            })

            //update filename with concat field and filename
            action.setCallback(this, function (response) {
                var state = response.getState();

                if (state === 'SUCCESS') {
                    var result = response.getReturnValue();
                    var fileChangeObject = {
                        label: component.get("v.label"),
                        files: [],
                        sortIndex: component.get("v.visibleForStage")
                    };
                    component.set("v.filerequirederror", false);

                    //iterate result, retrieve filename only, and assign to empty array
                    for (var j = 0; j < result.length; j++) {
                        var nameParsed = result[j].Title.split(":");
                        if (nameParsed[0] == field) {
                            var fileName = (result[j].Title.substring(field.length + 1));
                            var fileId = result[j].Id;
                            nameRes.push(fileName);

                            //fileNames.push(fileName);
                            fileChangeObject.files.push({
                                fileName,
                                fileId
                            });
                        }
                    }

                    //pass unique filename only via filter
                    //component.set("v.files", nameRes.filter((e,i)=> nameRes.indexOf(e) >= i));   
                    //var uniqueFiles = nameRes.filter((e, i) => nameRes.indexOf(e) >= i);
                    var files = this.setFilesArray(fileChangeObject.files);
                    component.set("v.files", files);

                    // Fire application event to notify any subscribed components that files have been added
                    this.fireApplicationEvent(component, "e.c:AppForms_EventDocumentChange",
                        {
                            eventType: "ADD",
                            payload: fileChangeObject
                        });

                    //displays success message after upload			
                    toastEvent.setParams({ "title": "Success", "type": "Success", "message": uploadFiles.length + " files have been uploaded" });
                    toastEvent.fire();

                }
                else if (state === 'ERROR') {
                    var errors = response.getError();

                    if (errors) {
                        console.log('error message: ' + errors);
                    }
                    toastEvent.setParams({ "title": "Error", "message": "There was an error to upload the file" });
                    toastEvent.fire();
                }
            });

            $A.enqueueAction(action);

        }
    }
})