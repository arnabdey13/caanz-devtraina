({
    /** doInit Method to retrieve application types from backend and initialise component with values **/
	doInit: function(component, event, helper){
        //helper.setIsMemberCommunity(component);
        helper.getApplications(component);
    },
    /** showModalBox Method to make this components modal visible **/
    showModalBox: function(component, event, helper) {
        component.set("v.isVisible",true);
    },
    /** hideModalBox Method to make this components modal hidden **/
    hideModalBox: function(component, event, helper){
        component.set("v.isVisible",false);
    },
})