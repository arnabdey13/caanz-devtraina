({
    // Detect if in member or non-member community
	//setIsMemberCommunity : function(component) {
	//	component.set("v.isMemberCommunity", window.location.pathname.includes("MyCA"));
	//},
    
    /** getApplications Method to retrieve application types from backend and initialise component with values **/
    getApplications : function(component) {
        let action = component.get("c.getApplicationTypes");
        let applicationsArray = component.get("v.applicationsArray");
        var communityName = window.location.pathname.split("/")[1]; // Get URL Extension for communityName - e.g. MyCA, customer
        //const isMemberCommunity = component.get("v.isMemberCommunity");
        
        action.setParams({communityName});
        action.setCallback(this, function(a){
        	var rtnValue = a.getReturnValue();

            if (rtnValue !== null) {
                this.setupApplicationUrls(component, rtnValue, communityName);
                /**let memberSpecificCategory = (isMemberCommunity) ? rtnValue["Specialisation Applications"] : rtnValue["Migration Applications"];
                applicationsArray = [
                    rtnValue["Admissions Applications"],
                    rtnValue["Public Practice & Affiliate membership Applications"],
                    memberSpecificCategory,
                    rtnValue["Other Applications"]
                ];**/
				var valueArray = Object.values(rtnValue);
                valueArray.sort(function(a, b) {
                	return a[0].Parent_Category__c.localeCompare(b[0].Parent_Category__c);
                });
                component.set("v.applicationsArray", valueArray);
            }
            // Set to false here so accordion can start with no sections open
            component.set("v.allowMultipleSectionsOpen", false);
            
        });
        $A.enqueueAction(action);
    },
    
    /** getApplications Method to map application values from backend to appropriate community page or form assembly page url **/
    setupApplicationUrls : function(component, responseObj, communityName) {
        //const isMemberCommunity = component.get("v.isMemberCommunity");
        
        
        // Loop though each key of the Object response from backend
        Object.keys(responseObj).forEach((key) => {
            responseObj[key].forEach((application) => {
                if (application.Landing_URL__c) {
                    if (application.Landing_URL__c.includes("RecordType=")) {
            			//application.urlPath = (isMemberCommunity) ? "/MyCA" : "/customer";
            			application.urlPath = "/" + communityName + application.Landing_URL__c;
            			//application.urlPath += (isMemberCommunity) ? "&CommunityPrefix=MyCA" : "";
            			application.urlPath += "&CommunityPrefix=" + communityName;
                	}
                    else {
                    	//application.urlPath = (isMemberCommunity) ? "/MyCA/s" : "/customer/s";
                    	application.urlPath = "/" + communityName + "/s";                    	 
            			application.urlPath += application.Landing_URL__c;
                	}
                }
        	});
        });
    }
})