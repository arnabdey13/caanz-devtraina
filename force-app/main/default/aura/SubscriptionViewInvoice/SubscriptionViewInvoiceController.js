({
    goBack : function(cmp) {
        var appEvent = $A.get("e.c:PageTransitionEvent");
        appEvent.setParams({source: cmp,
                            step : "prev"}).fire();
    },	
    goPay : function(cmp) {
        var appEvent = $A.get("e.c:PageTransitionEvent");
        appEvent.setParams({source: cmp,
                            step : "next"}).fire();
    }	
})