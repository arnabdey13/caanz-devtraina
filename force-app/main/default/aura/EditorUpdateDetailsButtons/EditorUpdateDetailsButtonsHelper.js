({
    /* Event flows managed by this controller
     * 
     * Numbers in dep keys mean level 0 = expander
     * All events fired by this helper unless in []
     * 
     * Where possible use dependent display events from editors to initiate cascading flows because
     * validation events tend to loop when used for setting values
     * 
     * Apex callback load
     * - if Country fields missing and ToggleFireEvent(address0) -> AddressExpander, cascade fires ToggleEvent(address)
     *   also fire DependentDisplay(address0) false -> AddressExpander to hide it
     * - DependentDisplay(discretionary0) user not provisional and country present -> Discretionary Expander     
     * 
	 * User expands discretionary (key = expand0)
     * - DependentDisplay(discretionary1CPP) user has CPP -> CPP checkbox
     * - DependentDisplay(discretionary1Concessions) user not provisional -> Concessions Radio Group
     * - DependentDisplay(discretionary2Concessions) concession not none -> Concessions Confirm
     * 
     * User collapses discretionary
     * - DependentDisplay(discretionary1) false -> all level > 0 editors
     * 
     * optionalValidation handler fn below here unless in []
     * 
     * User chooses country
     * - DependentDisplay(discretionary0) country not null -> Discretionary Expander
     * 
     * User chooses Australia
     * - DependentDisplay(discretionary2CPPAU) true -> Indemnity Confirm
     * 
     * User unchecks CPP
     * - DependentDisplay(discretionary2CPP) not(checkbox val) -> all CPP level 2 editors
     * - DependentDisplay(discretionary2CPPAU) country is AU -> Indemnity Confirm
     * User checks CPP
     * - DependentDisplay(discretionary2CPP) checkbox val -> all CPP level 2 editors
     * 
     * User denies indemnity
     * - DependentDisplay(discretionary3Indemnity) [RADIO CONFIG] -> indemnity hints 
     * 
     * User chooses No for CPP confirmation
     * - SetValueEvent true -> CPP checkbox
     * - same dep events as above for manual check of CPP box
     * 
     * - User changes concession
     * - DependentDisplay(discretionary2Concessions) concession not none -> Concessions Confirm
     */
    
    navigate : function(component, event) {
        // set state to tell following pages how to navigate/display progress bar
        var sm = component.find("stateManager");
        sm.get("v.setter")("flow","wizard");
        
        var javascriptSaveURL = component.get("v.javascriptSaveURL");
        if (javascriptSaveURL) {
            if (this.isTimerRequired(component)) {
                component.find("timer").set("v.completeCallback", function() {
	                window.location = javascriptSaveURL;
                });      
                component.find("timer").start(component.get("v.timerDelay"));        
            } else {
                window.location = javascriptSaveURL;
            }
        } else {
            if (component.get("v.saveURL")) {            
                var navEvent = $A.get("e.force:navigateToURL").setParams({
                    "url": component.get("v.saveURL"),
                    "isredirect" :false
                });
                if (this.isTimerRequired(component)) {
                    component.find("timer").set("v.completeCallback", function() {
	                    navEvent.fire();
                    });      
                    component.find("timer").start(component.get("v.timerDelay"));        
                } else {
                    navEvent.fire();
                }
            } else {
                console.log("no url found");
            } 
        }
    },
    isTimerRequired : function(component) {
        var dirty = component.get("v.properties").DIRTY;
        if ($A.util.isUndefined(dirty)) { 
            dirty = {};
        }
        var isInvoiceCalculationRequired = 
            dirty["prop.CPP"] ||
            dirty["Financial_Category__c"] ||
            dirty["PersonOtherCountryCode"];

        return component.get("v.timerEnabled") && isInvoiceCalculationRequired;
	},
    // load is after the apex callback
    optionalLoad : function(component, event) {
        // display the address section if any fields are missing
        if (component.get("v.properties").RECORD.PersonOtherCountryCode == "NZ"){
            this.fireDependentDisplay("NZICAmember", true);
        } else {
            this.fireDependentDisplay("NZICAmember", false);
        }
        var requiredAddressFields = ["PersonOtherStreet","PersonOtherCity","PersonOtherPostalCode","PersonOtherCountryCode"];
        for (var i=0;i<requiredAddressFields.length;i++) {
            if ($A.util.isEmpty(component.get("v.properties").RECORD[requiredAddressFields[i]])) {
                $A.get("e.c:ToggleFireEvent").setParams({key: "address0"}).fire();	
                $A.get("e.c:DependentDisplayEvent").setParams({dependencyKey: "address0",
                                                               isMatch: false}).fire();	
                break;
            }
        }
        // DependentDisplay(discretionary0) user not provisional and country present -> Discretionary Expander        
        this.fireDependentDisplay("discretionary0", this.isDiscretionarySectionEnabled(component, component.get("v.properties").RECORD.PersonOtherCountryCode));
    },
    isDiscretionarySectionEnabled : function(component, countryCode) {
        return ! $A.util.isEmpty(countryCode) 
        && component.get("v.properties").RECORD.Membership_Class__c != "Provisional";
    },
    fireDependentDisplay : function(key, match) {
        var appEvent = $A.get("e.c:DependentDisplayEvent");
        appEvent.setParams({dependencyKey: key,
                            isMatch: match}).fire();	
    },
    handleDependency : function(component, event, helper) {
        var key = event.getParam("dependencyKey");
        var match = event.getParam("isMatch");
        if (key == "expand0") {
            if (match) {
                // DependentDisplay(discretionary1CPP) user has CPP -> CPP checkbox
                // DependentDisplay(discretionary1Concessions) user not provisional -> Concessions Radio Group
                // DependentDisplay(discretionary2Concessions) concession not none -> Concessions Confirm
                var userHasCPP = component.get("v.properties")["prop.CPP"];
                var userNotProvisional = component.get("v.properties").RECORD.Membership_Class__c != "Provisional";
                var category = component.get("v.properties").RECORD.Financial_Category__c;
                var userHasConcession = !$A.util.isUndefined(category) && category != "None";
                
                this.fireDependentDisplay("discretionary1CPP", userHasCPP);
                this.fireDependentDisplay("discretionary1Concessions", userNotProvisional);
                this.fireDependentDisplay("discretionary2Concessions", userHasConcession);
            } else {
                // DependentDisplay(discretionary1) false -> all level > 0 editors                
                this.fireDependentDisplay("discretionary1", false);
                this.fireDependentDisplay("discretionary2", false);
                

            }            
        } 
    },    
    // validation occurs any time a field changes, including page load in the apex callback
    optionalValidation : function(component, event){
        
        var field = event.getParam("field");
        var oldValue = event.getParam("oldValue");
        var value = event.getParam("value");
        var isValueChanging = oldValue != value;
        
        // user is changing country        
        if (field == "PersonOtherCountryCode") {  
            var countryComponent = event.getParam("source");

            if (isValueChanging) {
                if (value == "NZ") {
                    this.fireDependentDisplay("NZICAmember", true);
                } else {
                    this.fireDependentDisplay("NZICAmember", false);
                }       



                // DependentDisplay(discretionary0) country not null -> Discretionary Expander
                this.fireDependentDisplay("discretionary0", this.isDiscretionarySectionEnabled(component, value));
                
                // avoid firing on load when CPP is false in db
                if (!countryComponent.get("v.hidden")) {
                    if (value == "AU") {
	                    // should be user has unset CPP instead of the prop which came from db
	                    var dirty = component.get("v.properties")["DIRTY"];
                        var userHasDeselectedCPP = dirty && dirty["prop.CPP"] && component.get("v.properties")["prop.CPP"] == false;
                        var shouldAUCPPEditorsShow = this.isDiscretionarySectionEnabled(component, value) && userHasDeselectedCPP;
	                    // DependentDisplay(discretionary2CPPAU) true -> Indemnity Confirm
	                    this.fireDependentDisplay("discretionary2CPPAU", shouldAUCPPEditorsShow);
                    } else {
	                    this.fireDependentDisplay("discretionary2CPPAU", false);
                    }
                }                
            }            
        } else if (field == "prop.CPP") {
            var cppComponent = event.getParam("source");
            if (!cppComponent.get("v.hidden")) { // avoid this path on initial record load
                
                var wasDeselected = !value;
                
                // DependentDisplay(discretionary2CPP) not(checkbox val) -> all CPP level 2 editors
                this.fireDependentDisplay("discretionary2CPP", wasDeselected);
                
                var country = component.get("v.properties").RECORD.PersonOtherCountryCode;
                
                // DependentDisplay(discretionary2CPPAU) country is AU -> Indemnity Confirm
                this.fireDependentDisplay("discretionary2CPPAU", wasDeselected && country == "AU");                
                
                if (country) {
                    
                    var countryCode = country=="NZ"?"NZ":"AU"; // AU is all countries except NZ
                    var labelsEvent = $A.get("e.c:EditorSetLabelsEvent");
                    var eventData = {field: "prop.confirmCPPHints"};
                    for (i = 1; i < 5; i++) {
                        eventData["label"+i] = component.get("v.confirmCPPHint"+countryCode+i);
                    }
                    labelsEvent.setParams(eventData);
                    labelsEvent.fire();
                }
                
            }
        } else if (field == "prop.ConfirmCPP") {
            if (value == "No") {
                
                // IMPORTANT : infinite event loops can happen here! using BOTH a timeout and value checks around
                // each event to avoid the loop. this is important to test because otherwise it locks the
                // browser for the user.
                // TODO a MUCH better solution for breaking the infinite loop is to use a control attribute. See EditorPreferencesButtonsHelper/suppressCustomValidation
                setTimeout(function() {
                    
                    // re-check the CPP checkbox
                    if (! component.get("v.properties")["prop.CPP"]) {
                        var valueEvent = $A.get("e.c:EditorSetValueEvent");
                        valueEvent.setParams({field: 'prop.CPP', value: true}).fire();
                    }
                    
                    var confirmComponent = event.getParam("source");                    
                    // un-select the No choice in case it is re-used
                    if (! $A.util.isUndefined(confirmComponent.get("v.value"))) {
                        // even though we have a reference to the component in the ValidationEvent, use an 
                        // event to set the new value so that all validation and extra logic is invoked during the change
                        var valueEvent = $A.get("e.c:EditorSetValueEvent");
                        // not including a value i.e. set to null
                        valueEvent.setParams({field: 'prop.ConfirmCPP', suppressValidation: true}).fire();
                    }
                    
                }, 250);
            }
        } else if (field == "Financial_Category__c") {
            
            var concessionsComponent = event.getParam("source");
            var concessionsValue = concessionsComponent.get("v.value");
            var isNone = concessionsValue == "None";
            
            // suppress this on page load so that confirmation stays hidden
            if (!concessionsComponent.get("v.hidden")) {
                // DependentDisplay(discretionary2Concessions) concession not none -> Concessions Confirm
                this.fireDependentDisplay("discretionary2Concessions", !isNone);
            }
                        
        } else if (field == "prop.ConfirmConcession") {
            if (value == "No") {
                
                var confirmationComponent = event.getParam("source");
                
                // IMPORTANT : see comment above about infinite event loops
                setTimeout(function() {
                    
                    // set the Concession choice to None
                    if (component.get("v.properties").RECORD["Financial_Category__c"] != "None") {
                        var valueEvent = $A.get("e.c:EditorSetValueEvent");
                        valueEvent.setParams({field: 'Financial_Category__c',
                                              value: "None"}).fire();     
                    }
                    
                    // unset the No value in case it is re-used
                    if (confirmationComponent.get("v.value")) {   
                        // see comment above for why using an event to unset the value
                        var valueEvent = $A.get("e.c:EditorSetValueEvent");
                        valueEvent.setParams({field: 'prop.ConfirmConcession', 
                                              suppressValidation: true}).fire();
                    }                       
                }, 250);                
            }
        }
    },
    handleConfirmCheckbox : function(component, event){        
        // adjust the save button UI state
        var selected = event.getParam("value");
        component.set("v.saveDisabled", !selected);        
    },
    preventsNavigation : function(component, fieldName){
        var excludedFields = {"prop.confirmed": false};
        var isExcluded = excludedFields[fieldName];
        if ($A.util.isUndefined(isExcluded)) {
            return true;
        } else {
            return isExcluded;
        }
    }
})