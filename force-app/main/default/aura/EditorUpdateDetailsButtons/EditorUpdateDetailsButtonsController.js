({
	handleDependency : function(component, event, helper) {
		helper.handleDependency(component, event, helper);
	},
    handleChangeEvent : function(component, event, helper) {       
        helper.handleConfirmCheckbox(component, event);
        var valueEvent = $A.get("e.c:EditorSetValueEvent");
        valueEvent.setParams({field: "Confirmed_Details__c", value: true}).fire();//Field Value Hard Coded
    }
})