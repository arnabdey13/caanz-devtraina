({
    evaluateTopBox: function (component, label) {
        component.set("v.topBoxLabel", label);
        var componentArrays = this.getComponentArrays(component, ["v.topBox"]);
    },
    evaluateBox10: function (component, label, alwaysShow) {
        component.set("v.box10Label", label);
        var componentArrays = this.getComponentArrays(component, ["v.box10Row1Cell1", "v.box10Row2Cell1", "v.box10Row2Cell2", "v.box10Row3Cell1"]);
        this.adjustBoxVisibility(component, componentArrays, "v.showBorderBox10", "v.box10Css", alwaysShow);
    },
    evaluateBox20: function (component, label, alwaysShow) {
        component.set("v.box20Label", label);
        var componentArrays = this.getComponentArrays(component, ["v.box20Row1Cell1", "v.box20Row2Cell1", "v.box20Row2Cell2", "v.box20Row3Cell1"]);
        this.adjustBoxVisibility(component, componentArrays, "v.showBorderBox20", "v.box20Css", alwaysShow);
    },
    evaluateBox30: function (component, label, alwaysShow) {
        component.set("v.box30Label", label);
        var componentArrays = this.getComponentArrays(component, ["v.box30Row1Cell1", "v.box30Row2Cell1", "v.box30Row2Cell2", "v.box30Row3Cell1"]);
        this.adjustBoxVisibility(component, componentArrays, "v.showBorderBox30", "v.box30Css", alwaysShow);
    },
    evaluateBox40: function (component, label, alwaysShow) {
        component.set("v.box40Label", label);
        var componentArrays = this.getComponentArrays(component, ["v.box40Row1Cell1", "v.box40Row2Cell1", "v.box40Row2Cell2", "v.box40Row3Cell1"]);
        this.adjustBoxVisibility(component, componentArrays, "v.showBorderBox40", "v.box40Css", alwaysShow);
    },
    evaluateBottomBox: function (component, label) {
        component.set("v.bottomBoxLabel", label);
        var componentArrays = this.getComponentArrays(component, ["v.bottomBox"]);
    },
    getComponentArrays: function (component, attributeNames) {
        var componentArrays = [];
        for (var i = 0; i < attributeNames.length; i++) {
            componentArrays.push(component.get(attributeNames[i]));
        }
        return componentArrays;
    },
    adjustBoxVisibility: function (component, componentArrays, borderAttribute, boxCss, alwaysShow) {
        var css = 'slds-m-bottom--medium';
        var topBoxBorder = component.get(borderAttribute);

        if (topBoxBorder) {
            css += ' border';
        }

        var isVisible = alwaysShow || this.anyComponentsVisible(componentArrays);

        if (isVisible) css += ' slds-show';
        else css += ' slds-hide';

        component.set(boxCss, css);

    },
    anyComponentsVisible: function (componentArrays) {
        for (var i = 0; i < componentArrays.length; i++) {
            var region = componentArrays[i][0];
            var isRegion = this.isCommunityRegion(componentArrays[i]);
            var visible;

            if(isRegion){
                var regionBody = region.get("v.body");

                var draggedChildren = [];
                for(var j = 0; j < regionBody.length; j++){
                    var draggedLightningComponentContainer = regionBody[j].get("v.body");
                    draggedChildren.push(draggedLightningComponentContainer[0]);
                }
                visible = this.isAnyComponentVisible(draggedChildren);

            } else {
                visible = this.isAnyComponentVisible(componentArrays[i]);
            }

            if (visible) return true;
        }
        return false;
    },
    isAnyComponentVisible: function (components) {
        for (var i = 0; i < components.length; i++) {
            var isFormsField = components[i].isInstanceOf("c:ff_Control");

            if (isFormsField) {
                var isVisible = components[i].get("v.visible");
                if (isVisible) return true;
            }
        }
        return false;
    },
    //true when an array has been populated using drag and drop in the community builder
    //this assumes that the community builder wraps drag and dropped components in a siteforce:runtimeRegion
    isCommunityRegion: function (componentArray) {
        return componentArray.length == 1 &&
            componentArray[0].isInstanceOf("siteforce:runtimeRegion");

    }
})