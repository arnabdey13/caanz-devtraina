({
    handleStageChange: function (component, event, helper) {
        //run with a slight delay to allow fields to set their validity state first,
        // before evaluating display of boxes
        setTimeout(function () {
            var show = component.get("v.isLayoutVisible");
            var params = event.getParams();

            if (!$A.util.isUndefined(params.stage)) {
                helper.evaluateTopBox(component, params.headings[params.stage].topBoxLabel);
                helper.evaluateBox10(component, params.headings[params.stage].box10Label, show);
                helper.evaluateBox20(component, params.headings[params.stage].box20Label, show);
                helper.evaluateBox30(component, params.headings[params.stage].box30Label, show);
                helper.evaluateBox40(component, params.headings[params.stage].box40Label, show);
                helper.evaluateBottomBox(component, params.headings[params.stage].bottomBoxLabel);

                var callback = event.getParam("callback");
                if (callback) {
                    callback();
                }
                component.set("v.state", "LOADED");
            }
        }, 50);
    },
    handleBuilderConfig: function (component, event, helper) {
        var params = event.getParams();
        component.set("v.isLayoutVisible", params.isBuilderMode);
        
        if (params.isBuilderMode && params.showRhsBox) {
         	component.set("v.showRhsBox", true);
            return;
        }
        
        if (!$A.util.isUndefined(params.showRhsBox)) {
            component.set("v.showRhsBox", params.showRhsBox);
        }
    },
    handleShowRHSBox: function (component, event, helper) {
        var params = event.getParams();
        
        if (params.isBuilderMode && params.showRhsBox) {
         	component.set("v.showRhsBox", true);
            return;
        }
        
        if (!$A.util.isUndefined(params.showRhsBox)) {
            component.set("v.showRhsBox", params.showRhsBox);
        }
    }
})