({
    doInit: function (component, event, helper) {
        helper.sendToService(component, "c.load", null, function (val) {
            var countries = val['countries'];
            var salutation = val['salutations'];

            helper.setSelectOptions(component, event, helper, countries, "v.countryValues", "v.salutation");
            helper.setSelectOptions(component, event, helper, salutation, "v.salutationValues", "v.country");
        });

        component.set("v.headerMessage", component.get("v.headerMessageRegistration"));
        component.set("v.login", component.get("v.loginButtonLabel"));
        component.set("v.next", component.get("v.emailButtonLabel"));
        component.set("v.register", component.get("v.registerButtonLabel"));
        component.set("v.resend", component.get("v.requestLinkButtonLabel"));

        window.setTimeout(
            $A.getCallback(function () {
                var elem = component.find("emailInput").getElement();
                if (!$A.util.isEmpty(elem)) {
                    elem.focus();
                }
            }), 200
        );

        $A.run(function () {
            var params = helper.parseURL();
            component.set("v.destination", params["d"]);
        });
        helper.showError(component, "termsConditionsField", "termsConditionsInvalidMessage");

    },
    doSave: function (component, event, helper) {
        helper.doSave(component, event, helper);
    },
    doLogin: function (component, event, helper) {
        var valid = false;
        helper.validatePassword(component, event, helper, true, function (val) {
            valid = val ? true : false;
        });
        if (!valid) return;

        helper.doLogin(component, event, helper);
    },
    validateEmail: function (component, event, helper) {
        var valid = false;
        helper.validateEmail(component, event, helper, true, function (val) {
            valid = val ? true : false;
        });
        if (!valid) return;

        helper.validateEmailAtService(component, event, helper);
    },
    resendLink: function (component, event, helper) {
        var email = component.get("v.email");
        helper.disableResendBeforeAuraCall(component, event, helper);
        helper.sendToService(component,
            "c.resendEmail",
            {email: email},
            function (val) {
                if (val == 'SUCCESS') {
                    helper.setResponseMessage(component, event, helper, "v.emailResentMessageLine1", null);
                    component.set("v.showResendButton", false);
                } else if (val == 'REPEATABLE_ERROR') {
                    helper.enableResendAfterAuraCall(component, event, helper);
                    helper.enableRegisterAfterAuraCall(component, event, helper);
                    component.set("v.errorMessage", component.get("v.repeatableErrorMessage"));
                    component.set("v.showErrorMessage", true);
                } else {
                    helper.setResponseMessage(component, event, helper, "v.nonRepeatableErrorMessage", null);
                    component.set("v.showResendButton", false);
                }
            });
    },
    closeError: function (component, event, helper) {
        helper.closeError(component, event, helper);
    },
    validateSalutation: function (component, event, helper) {
        helper.validateSalutation(component, event, helper);
    },
    validateCountry: function (component, event, helper) {
        helper.validateCountry(component, event, helper);
    },
    validateFirstName: function (component, event, helper) {
        helper.validateFirstName(component, event, helper);
    },
    validateLastName: function (component, event, helper) {
        helper.validateLastName(component, event, helper);
    },
    validateTermsConditions: function (component, event, helper) {

        var value = component.find("termsConditionsInput").get("v.value")
        var registrationComplete = false;
        helper.enableDisableRegistrationBtn(component, event, helper, function(complete){
                registrationComplete = complete ? true : false;
        });
            if(value == false) {
                //prevent error message to show if field is accessed via tab
                if(event.keyCode != 9) helper.showError(component, "termsConditionsField", "termsConditionsInvalidMessage");
            } else {
                helper.hideError(component, "termsConditionsField", "termsConditionsInvalidMessage");
                if(registrationComplete && event.keyCode == 13) helper.doSave(component, event, helper);
            }
    },    

    showNextBtn: function (component, event, helper) {
        helper.closeError(component, event, helper);
        helper.showNextBtn(component, event, helper);
    },
    enableDisableNextBtn: function (component, event, helper) {
        helper.enableDisableNextBtn(component, event, helper);
    },
    enableDisableLoginBtn: function (component, event, helper) {
        helper.enableDisableLoginBtn(component, event, helper);
    },
    handlePasswordReset : function(component, event, helper){
        helper.setResponseMessage(component, event, helper, "v.passwordResetting", null);
        component.set("v.showLogin", false);
        var email = component.get("v.email");
        helper.sendToService(component,
            "c.resetPW",
            {email: email},
            function (val) {
                if (val == 'SUCCESS') {
                    helper.setResponseMessage(component, event, helper, "v.passwordReset", null);
                } else {
                    helper.setResponseMessage(component, event, helper, "v.nonRepeatableErrorMessage", null);
                }
            });

     }
})