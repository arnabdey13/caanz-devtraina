({
    /* method to identify whether Qualified Auditor/Licensed Auditor/Accrediated field on Person Account is checked */
    createComp : function(component, event, helper) {
       
    },
    
    
      /* Method to display Empty value in Text box when 'Yes' option is selected */
	handleChange : function(component, event, helper) {
        if(event.getSource().get('v.value') == 'Yes'){
            component.set(event.getSource().get('v.name'),'');
        }
   },
   handleChangeForNo : function(component, event, helper) {
        if(event.getSource().get('v.value') == 'No'){
            component.set(event.getSource().get('v.name'),'');
        }
   },
    
    saveRegistrationInformation : function(component,event,helper){
        var questionnaireResponseSobject = component.get("v.questionnaireResponseSobject");
        var questionaireResponseObjectToUpdate = component.get("v.questionaireResponseObjectToUpdate");
        //questionaireResponseObjectToUpdate.Residential_Country__c = 'New Zealand';
        questionaireResponseObjectToUpdate.Question_12__c = questionnaireResponseSobject.Question_12__c;
        questionaireResponseObjectToUpdate.Answer_12__c = questionnaireResponseSobject.Answer_12__c;
        questionaireResponseObjectToUpdate.Question_13__c = questionnaireResponseSobject.Question_13__c;
        questionaireResponseObjectToUpdate.Answer_13__c = questionnaireResponseSobject.Answer_13__c;
        questionaireResponseObjectToUpdate.Question_14__c = questionnaireResponseSobject.Question_14__c;
        questionaireResponseObjectToUpdate.Answer_14__c = questionnaireResponseSobject.Answer_14__c;
        questionaireResponseObjectToUpdate.Question_15__c = questionnaireResponseSobject.Question_15__c;
        questionaireResponseObjectToUpdate.Answer_15__c = questionnaireResponseSobject.Answer_15__c;
        questionaireResponseObjectToUpdate.Question_16__c = questionnaireResponseSobject.Question_16__c;
        questionaireResponseObjectToUpdate.Answer_16__c = questionnaireResponseSobject.Answer_16__c;
        questionaireResponseObjectToUpdate.Question_17__c = questionnaireResponseSobject.Question_17__c;
        questionaireResponseObjectToUpdate.Answer_17__c = questionnaireResponseSobject.Answer_17__c;
        questionaireResponseObjectToUpdate.Question_18__c = questionnaireResponseSobject.Question_18__c;
        questionaireResponseObjectToUpdate.Answer_18__c = questionnaireResponseSobject.Answer_18__c;
        questionaireResponseObjectToUpdate.Question_19__c = questionnaireResponseSobject.Question_19__c;
        questionaireResponseObjectToUpdate.Answer_19__c = questionnaireResponseSobject.Answer_19__c;
        questionaireResponseObjectToUpdate.Question_20__c = questionnaireResponseSobject.Question_20__c;
        questionaireResponseObjectToUpdate.Answer_20__c = questionnaireResponseSobject.Answer_20__c;
        questionaireResponseObjectToUpdate.Question_21__c = questionnaireResponseSobject.Question_21__c;
        questionaireResponseObjectToUpdate.Answer_21__c = questionnaireResponseSobject.Answer_21__c;
        questionaireResponseObjectToUpdate.Question_22__c = questionnaireResponseSobject.Question_22__c;
        questionaireResponseObjectToUpdate.Answer_22__c = questionnaireResponseSobject.Answer_22__c;
        questionaireResponseObjectToUpdate.Question_23__c = questionnaireResponseSobject.Question_23__c;
        questionaireResponseObjectToUpdate.Answer_23__c = questionnaireResponseSobject.Answer_23__c;
        questionaireResponseObjectToUpdate.Question_24__c = questionnaireResponseSobject.Question_24__c;
        questionaireResponseObjectToUpdate.Answer_24__c = questionnaireResponseSobject.Answer_24__c;
        questionaireResponseObjectToUpdate.Question_25__c = questionnaireResponseSobject.Question_25__c;
        questionaireResponseObjectToUpdate.Answer_25__c = questionnaireResponseSobject.Answer_25__c;
        questionaireResponseObjectToUpdate.Question_26__c = questionnaireResponseSobject.Question_26__c;
        questionaireResponseObjectToUpdate.Answer_26__c = questionnaireResponseSobject.Answer_26__c;
        questionaireResponseObjectToUpdate.Question_27__c = questionnaireResponseSobject.Question_27__c;
        questionaireResponseObjectToUpdate.Answer_27__c = questionnaireResponseSobject.Answer_27__c;
        questionaireResponseObjectToUpdate.Question_28__c = questionnaireResponseSobject.Question_28__c;
        questionaireResponseObjectToUpdate.Answer_28__c = questionnaireResponseSobject.Answer_28__c;
        questionaireResponseObjectToUpdate.Question_29__c = questionnaireResponseSobject.Question_29__c;
        questionaireResponseObjectToUpdate.Answer_29__c = questionnaireResponseSobject.Answer_29__c;
        questionaireResponseObjectToUpdate.Question_30__c = questionnaireResponseSobject.Question_30__c;
        questionaireResponseObjectToUpdate.Answer_30__c = questionnaireResponseSobject.Answer_30__c;
        questionaireResponseObjectToUpdate.Question_31__c = questionnaireResponseSobject.Question_31__c;
        questionaireResponseObjectToUpdate.Answer_31__c = questionnaireResponseSobject.Answer_31__c;
        questionaireResponseObjectToUpdate.Question_32__c = questionnaireResponseSobject.Question_32__c;
        questionaireResponseObjectToUpdate.Answer_32__c = questionnaireResponseSobject.Answer_32__c;
        questionaireResponseObjectToUpdate.Question_33__c = questionnaireResponseSobject.Question_33__c;
        questionaireResponseObjectToUpdate.Answer_33__c = questionnaireResponseSobject.Answer_33__c;
        questionaireResponseObjectToUpdate.Question_34__c = questionnaireResponseSobject.Question_34__c;
        questionaireResponseObjectToUpdate.Answer_34__c = questionnaireResponseSobject.Answer_34__c;
        questionaireResponseObjectToUpdate.Question_35__c = questionnaireResponseSobject.Question_35__c;
        questionaireResponseObjectToUpdate.Answer_35__c = questionnaireResponseSobject.Answer_35__c;
        questionaireResponseObjectToUpdate.Question_36__c = questionnaireResponseSobject.Question_36__c;
        questionaireResponseObjectToUpdate.Answer_36__c = questionnaireResponseSobject.Answer_36__c;
        debugger;
        if (!$A.util.isUndefinedOrNull(questionnaireResponseSobject.Id)) {
            questionaireResponseObjectToUpdate.Id = questionnaireResponseSobject.Id;
            questionaireResponseObjectToUpdate.Residential_Country__c = questionnaireResponseSobject.Residential_Country__c;
        }
        
        helper.upsertRegistration(component,questionaireResponseObjectToUpdate); 
    }
})