({
    upsertRegistration: function(component,questionaireResponseObjectToUpdate,callBackParams){
        this.validateRegistrationQuestionsAndAnswers(component,questionaireResponseObjectToUpdate,function(){
            console.log('==questionaireResponseObjectToUpdate==' + JSON.stringify(questionaireResponseObjectToUpdate));
            var subscriptionSobject = component.get("v.globalSubscriptionSobject");
            console.log('==globalSubscriptionSobject==' + JSON.stringify(subscriptionSobject));
            if($A.util.isUndefinedOrNull(subscriptionSobject.Obligation_Sub_Components__c )){
                subscriptionSobject.Obligation_Sub_Components__c = 'Registration Details;';
            }
            else if(!subscriptionSobject.Obligation_Sub_Components__c.includes("Registration Details")){
                if(subscriptionSobject.Obligation_Sub_Components__c.substring(subscriptionSobject.Obligation_Sub_Components__c.length - 1) == ';')
                    subscriptionSobject.Obligation_Sub_Components__c += 'Registration Details;';
                else
                    subscriptionSobject.Obligation_Sub_Components__c += ';Registration Details;';
            }
            
            console.log('==globalSubscriptionSobject==' + JSON.stringify(subscriptionSobject));
            var param = {
                'personAccountId':component.get("v.personAccountId"),
                'questionaireResponseObjectToUpdate':questionaireResponseObjectToUpdate,
                'selectedCountry':component.get("v.selectedCountry"),
                'whichSectionSave' : 'RegistrationNZ',
                'subscriptionSobject': subscriptionSobject
            };
            this.enqueueAction(component, "upsertQuestionaireResponse", param, function(error, data){
                var questionResponseSubscriptionWrappper = data;
                if(error){
                    component.set("v.errorMessage",error);
                }else if(!$A.util.isUndefinedOrNull(callBackParams) && callBackParams.callback) {
                    if (!$A.util.isUndefinedOrNull(questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id)) {
                        component.set("v.questionnaireResponseSobject.Id",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id);
                        component.set("v.questionnaireResponseSobject.Residential_Country__c",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Residential_Country__c);
                        component.set("v.globalSubscriptionSobject",questionResponseSubscriptionWrappper.subscriptionSObject);
                        /*
                        component.set("v.questionnaireResponseSobject.Id",data.Id);
                        component.set("v.questionnaireResponseSobject.Residential_Country__c",data.Residential_Country__c);
                        */
                    }
                    component.set("v.errorMessage",'');
                    callBackParams.callback(); 
                }else{
                    if (!$A.util.isUndefinedOrNull(questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id)) {
                        component.set("v.questionnaireResponseSobject.Id",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Id);
                        component.set("v.questionnaireResponseSobject.Residential_Country__c",questionResponseSubscriptionWrappper.questionnaireResponseSObject.Residential_Country__c);
                        component.set("v.globalSubscriptionSobject",questionResponseSubscriptionWrappper.subscriptionSObject);
                        /*
                        component.set("v.questionnaireResponseSobject.Id",data.Id);
                        component.set("v.questionnaireResponseSobject.Residential_Country__c",data.Residential_Country__c);
                        */
                    }
                }
            })   
        });
        
    },
    enqueueAction: function(component, method, params, callback){
        this.toggleSpinner(component, true);
        var action = component.get("c." + method); 
        if(params) action.setParams(params);
        action.setCallback(this, function(response){
            this.toggleSpinner(component, false);
            if(response.getState() === "SUCCESS") {
                if(callback) callback.call(this, null, response.getReturnValue());
            } else if(response.getState() === "ERROR") {
                var message = 'Unknown error'; 
                var errors = response.getError();
                if (!$A.util.isEmpty(errors)) {
                    message = errors[0].message;
                }
                console.error(message);
                if(callback) callback.call(this, message);
            }
        });
        $A.enqueueAction(action);
    },
    toggleSpinner: function(component, show) {
        var spinner = component.find("registrationSpinner");
        $A.util.addClass(spinner, show ? "slds-show" : "slds-hide");
        $A.util.removeClass(spinner, !show ? "slds-show" : "slds-hide");
    },
    
    validateRegistrationQuestionsAndAnswers : function(component,questionaireResponseObjectToUpdate,callback){
        var errorMessage = '';
        var isValidate=false;
        var isQualifiedAuditor = component.get('v.accountData.Qualified_Auditor__c');
        var isLicensedAuditor = component.get('v.accountData.Licensed_Auditor__c');
        var isAccredited = component.get('v.accountData.Insolvency_Practitioner__c');
        
        /*** Validation for Qualified Auditor Start here ***/
        if(isQualifiedAuditor && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_12__c) || questionaireResponseObjectToUpdate.Answer_12__c=='')){
            errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_12__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_12__c');
            
        }else if(questionaireResponseObjectToUpdate.Answer_12__c=='No' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_13__c) ||
                                                                           questionaireResponseObjectToUpdate.Answer_13__c=='')){
            errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_12__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_13__c');
            
        }else if(isQualifiedAuditor && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_14__c) || questionaireResponseObjectToUpdate.Answer_14__c=='')){
            errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_14__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_14__c');
            
        }else if(questionaireResponseObjectToUpdate.Answer_14__c=='No' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_15__c) || 
                                                                           questionaireResponseObjectToUpdate.Answer_15__c=='')){
            errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_14__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_15__c');
            
        }else if(isQualifiedAuditor && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_16__c) || questionaireResponseObjectToUpdate.Answer_16__c=='')){
            errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_16__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_16__c');
            
        }else if(questionaireResponseObjectToUpdate.Answer_16__c=='No' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_17__c) || 
                                                                           questionaireResponseObjectToUpdate.Answer_17__c=='')){
            errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_16__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_17__c');
            
        }else if(isQualifiedAuditor && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_18__c) || questionaireResponseObjectToUpdate.Answer_18__c=='')){
            errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_18__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_18__c');
            
        }else if(questionaireResponseObjectToUpdate.Answer_18__c=='No' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_19__c) || 
                                                                           questionaireResponseObjectToUpdate.Answer_19__c=='')){
            errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_18__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_19__c');
            
        }else if(isQualifiedAuditor && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_20__c) || questionaireResponseObjectToUpdate.Answer_20__c=='')){
            errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_20__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_20__c');
            
        }else if(questionaireResponseObjectToUpdate.Answer_20__c=='No' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_21__c) || 
                                                                           questionaireResponseObjectToUpdate.Answer_21__c=='')){
            errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_20__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_21__c');
            
        }else if(isQualifiedAuditor && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_20__c) || questionaireResponseObjectToUpdate.Answer_22__c=='')){
            errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_22__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_22__c');
            
        }else if(questionaireResponseObjectToUpdate.Answer_22__c=='No' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_23__c) || 
                                                                           questionaireResponseObjectToUpdate.Answer_23__c=='')){
            errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_22__c;
            isValidate=true;
            this.setFocusForErrorMessage(component,'Answer_23__c');
        }
        
        /*** Validation for Qualified Auditor Ends here ***/    
        
        /*** Validation for Licensed Auditor Start here ***/
            else if(isLicensedAuditor && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_24__c) || questionaireResponseObjectToUpdate.Answer_24__c=='')){
                errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_24__c;
                isValidate=true;
                this.setFocusForErrorMessage(component,'Answer_24__c');
                
            }else if(questionaireResponseObjectToUpdate.Answer_24__c=='No' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_25__c) || 
                                                                               questionaireResponseObjectToUpdate.Answer_25__c=='')){
                errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_24__c;
                isValidate=true;
                this.setFocusForErrorMessage(component,'Answer_25__c');
                
            }else if(isLicensedAuditor && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_26__c) || questionaireResponseObjectToUpdate.Answer_26__c=='')){
                errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_26__c;
                isValidate=true;
                this.setFocusForErrorMessage(component,'Answer_26__c');
                
            }else if(questionaireResponseObjectToUpdate.Answer_26__c=='No' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_27__c) || 
                                                                               questionaireResponseObjectToUpdate.Answer_27__c=='')){
                errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_26__c;
                isValidate=true;
                this.setFocusForErrorMessage(component,'Answer_27__c');
                
            }else if(isLicensedAuditor && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_28__c) || questionaireResponseObjectToUpdate.Answer_28__c=='')){
                errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_28__c;
                isValidate=true;
                this.setFocusForErrorMessage(component,'Answer_28__c');
                
            }else if(questionaireResponseObjectToUpdate.Answer_28__c=='No' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_29__c) || 
                                                                               questionaireResponseObjectToUpdate.Answer_29__c=='')){
                errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_28__c;
                isValidate=true;
                this.setFocusForErrorMessage(component,'Answer_29__c');
                
            }else if(isLicensedAuditor && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_30__c) || questionaireResponseObjectToUpdate.Answer_30__c=='')){
                errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_30__c;
                isValidate=true;
                this.setFocusForErrorMessage(component,'Answer_30__c');
                
                
            }else if(questionaireResponseObjectToUpdate.Answer_30__c=='No' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_31__c) || 
                                                                               questionaireResponseObjectToUpdate.Answer_31__c=='')){
                errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_30__c;
                isValidate=true;
                this.setFocusForErrorMessage(component,'Answer_31__c');
                
            }else if(isLicensedAuditor && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_32__c) || questionaireResponseObjectToUpdate.Answer_32__c=='')){
                errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_32__c;
                isValidate=true;
                this.setFocusForErrorMessage(component,'Answer_32__c');
                
            }else if(questionaireResponseObjectToUpdate.Answer_32__c=='No' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_33__c) || 
                                                                               questionaireResponseObjectToUpdate.Answer_33__c=='')){
                errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_32__c;
                isValidate=true;
                this.setFocusForErrorMessage(component,'Answer_33__c');
                
            }else if(isLicensedAuditor && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_34__c) || questionaireResponseObjectToUpdate.Answer_34__c=='')){
                errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_34__c;
                isValidate=true;
                this.setFocusForErrorMessage(component,'Answer_34__c');
                
            }else if(questionaireResponseObjectToUpdate.Answer_34__c=='Yes' && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_35__c) || 
                                                                               questionaireResponseObjectToUpdate.Answer_35__c=='')){
                errorMessage = 'Please provide detail for ' + questionaireResponseObjectToUpdate.Question_34__c;
                isValidate=true;
                this.setFocusForErrorMessage(component,'Answer_35__c');
            
        /*** Validation for Licensed Auditor Ends here ***/ 
        
        /*** Validation for Accredited Starts here ***/
        
            }else if(isAccredited && ($A.util.isUndefined(questionaireResponseObjectToUpdate.Answer_36__c) || questionaireResponseObjectToUpdate.Answer_36__c=='')){
                    errorMessage = 'Please answer ' + questionaireResponseObjectToUpdate.Question_36__c;
                    isValidate=true;
                    this.setFocusForErrorMessage(component,'Answer_36__c');
                    
            }
        
        /*** Validation for Accredited Ends here ***/ 
        
        if(!isValidate){
            if(callback) callback.call(this);
        }else{
            this.showToast(component,errorMessage);
        }
        
    },
    showToast : function(component,text,questionnaireResponseSobjectId) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Warning!",
            "message": text,
            type : 'warning',
            mode: 'dismissible ',
        });
        toastEvent.fire();
    },
    
    setFocusForErrorMessage : function(component,componentId) {
        if(!$A.util.isUndefined(component.find(componentId))){
            component.find(componentId).focus();
        }
        
    }
})