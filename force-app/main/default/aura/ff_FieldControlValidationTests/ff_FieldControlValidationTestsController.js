({
    doInit: function (component, event, helper) {

        var test = helper.driver(component, event, helper);

        // references to all the components being tested
        var text1 = component.find("text1");
        var text2 = component.find("text2");
        var text3 = component.find("text3");
        var q1 = component.find("q1");
        var q2 = component.find("q2");
        var check1 = component.find("check1");
        var check2 = component.find("check2");

        var startTests = test.start({
            focused: text1,
            description: "valid state after load"
        })

        startTests

            .then(test.wait(function (context) {
                text1.set("v.loaded", true)
                text1.set("v.value", "Acme")
            }))
            .then(test.assertEquals(false, "v.isValid", "should be a number"))
            .then(test.assertEquals(["This field must be a number"], "v.specificValidationErrors", "errors say number required"))

            .then(test.focus(text2, "invalid value is disabled"))
            .then(test.setAttributes({"v.loaded": true, "v.value": "Acme", "v.disabled": true}))
            .then(test.assert("v.isValid", "is now considered valid so it won't block save"))
            .then(test.assertEquals([], "v.specificValidationErrors", "no errors when disabled"))
            .then(test.setAttribute("v.disabled", false))
            // // TODO value change events should fire when disable/enable so that form is not blocked by disabled fields
            .then(test.assertEquals(false, "v.isValid", "is invalid again when re-enabled"))
            .then(test.assertEquals(["This field must be a number"], "v.specificValidationErrors", "error is back again"))
            .then(test.setAttribute("v.value", "23"))
            .then(test.assertEquals(true, "v.isValid", "is valid when set to a number"))
            .then(test.assertEquals([], "v.specificValidationErrors", "no errors when set to a number"))

            .then(test.focus(check1, "invalid value is unchecked"))
            .then(test.setAttributes({"v.loaded": true, "v.value": false}))
            .then(test.assertEquals(false, "v.isValid", "is not valid when false"))
            .then(test.setAttributes({"v.value": true}))
            .then(test.assertEquals(true, "v.isValid", "is valid when true"))

            .then(test.focus(check2, "focus on checkbox 2"))
            .then(test.setAttributes({"v.loaded": true, "v.value": true}))
            .then(test.assertEquals(true, "v.isValid", "is not valid when false"))

            /////// dependent viz with validation tests. checking that validation after load/change is sent to driver correctly

            .then(helper.fireApplicationEvent("e.c:ff_EventLoadControls",
                {
                    driver: component, // this test will handle validity reports during load. see apiSetFieldValidity below
                    data: {
                        records: {
                            "User": [{
                                single: true,
                                sObjectList: [{Employed1__c: true, Title: "CEO"}]
                            }]
                        },
                        wrappers: {
                            "User.yn": [
                                {value: true, label: "Yes"},
                                {value: false, label: "No"}]
                        }
                    }
                }))

            // all dependent fields are loaded
            .then(test.focus(q1, "parent vis control checks"))
            .then(test.assert("v.loaded", "is loaded and ready for change"))
            .then(test.focus(q2, "child vis control checks"))
            .then(test.assert("v.loaded", "q2 is loaded and ready for change"))
            .then(test.focus(check1, "child vis control checks"))
            .then(test.assert("v.loaded", "checkbox1 is loaded and ready for change"))
            .then(test.focus(text3, "child vis control checks"))
            .then(test.assert("v.loaded", "text 3 is loaded and ready for change"))

            .then(test.assertEquals(true, helper.fieldValidity(component, "User.Title"),
                "title reports valid because it has value in a required field"))

            // now cause dependent viz to hide other fields
            .then(test.wait(function () {
                q1.set("v.value", false);
            }))

            // title field
            .then(test.assert(function () {
                return $A.util.isUndefinedOrNull(q2.get("v.value"));
            }, "title was cleared when is was hidden"))
            .then(test.assertEquals(true, helper.fieldValidity(component, "User.Title"),
                "title has empty string in a required field but hidden so reports valid"))

            // checkbox 2
            .then(test.assert(function () {
                return $A.util.isUndefinedOrNull(check2.get("v.value"));
            }, "checkbox 2 was cleared when is was hidden"))

            // text 3
            .then(test.focus(text3, "checking dependent integer field"))
            .then(test.assertEquals(false, "v.visible", "text 3 is hidden because of dependent viz"))
            // covering a bug found in dev where specific validation was still reporting for hidden fields
            .then(test.assertEquals(true, helper.fieldValidity(component, "Account.Balance3__c"),
                "dependent balance field reports valid because hidden"))

            // cause dependent viz to show fields again
            .then(test.wait(function () {
                //console.log(">>>>> showing...");
                q1.set("v.value", true)
                check1.set("v.value", false)
            }))
            .then(test.assertEquals(false, helper.fieldValidity(component, "User.Title"),
                "title is empty, visible and reports invalid after dep viz shows it"))

            //////////// END OF TESTS ////////////

            // always include these fns to handle the end of the test
            .then(test.pass).catch(test.fail);

    },
    apiSetFieldValidity: function (component, event, helper) {
        var p = event.getParam("arguments");
        var v = component.get("v.formValidity");
        v[p.loadKey + "." + p.field] = p.isValid;
        component.set("v.formValidity", v);
        //console.log("report: " + JSON.stringify(p));
    },
    handleValueChange: function (component, event, helper) {
        var p = event.getParams();
        if (!p.ignore) {
            var v = component.get("v.formValidity");
            v[p.loadKey + "." + p.field] = p.valid;
            component.set("v.formValidity", v);
        }
        //console.log("value change: " + JSON.stringify(p));
    }
})