({
    fieldValidity: function (component, field) {
        return function () {
            return component.get("v.formValidity")[field];
        }
    }
})