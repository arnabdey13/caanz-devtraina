({
    doInit : function(component, event, helper) {
        component.set("v.hidden", true);
    },    
    start : function(component, event, helper) {
        var params = event.getParam('arguments');
        component.set("v.hidden", false);
        component.set("v.active", true);
        if (params) {
            var seconds = params.seconds;
            component.set("v.current", seconds);
            helper.tick(component);
        }        
    },    
    cancel : function(component, event, helper) {
        component.set("v.hidden", true);
        component.set("v.active", false);
        var callback = component.get("v.cancelCallback");
        if (callback) {
            callback();
        }
    }
})