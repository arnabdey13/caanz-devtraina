({
	tick : function(component) {
		var current = component.get("v.current");
        if (current <= 1) {
	        component.set("v.hidden", true);
	        component.set("v.active", false);
            var callback = component.get("v.completeCallback");
            if (callback) {
	            callback();
            }
        } else {
            component.set("v.current", current - 1);
            if (component.get("v.active")) {
                setTimeout(function() {
                    // can't use this.tick inside a callback
                    component.getDef().getHelper().tick(component);
                }, 1000);
            }
        }
	}
})