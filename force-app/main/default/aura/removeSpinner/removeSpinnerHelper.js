({
    hideSpinner : function(component) {
        var parent = document.getElementById("spinnerParent");
        var spinner = document.getElementById("spinner");
        if (parent && spinner) {
            parent.removeChild(spinner);            
        }
	}
})