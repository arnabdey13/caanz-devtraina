({
    load: function (component, event, helper) {
        var optionsKey = component.get("v.optionsKey");
        var readData = event.getParam("value").data;
        var wrappers = readData.wrappers[optionsKey];
        if (wrappers && !$A.util.isEmpty(wrappers)) {
            // clone the wrappers so that they can be used by more than 1 control independently
            wrappers = JSON.parse(JSON.stringify(wrappers));
            component.set("v.options", wrappers);
        }
    },
    handleInputChange: function (component, event, helper) {
        var idPrefix = component.get("v.loadKey") + '.' + component.get("v.field") + '-';
        var value = event.target.id.substring(idPrefix.length);
        component.set("v.value", value);
    }
})