({
	handleGetRecordIdMethod: function(cmp, event, helper){
        var callback = event.getParam("arguments").callback;
        if(callback) callback(cmp.get("v.recordId")); 
    },
	handleGetRecordIdEvent: function(cmp, event, helper){
        var callback = event.getParam("callback");
        if(callback) callback(cmp.get("v.recordId"));
    }
})