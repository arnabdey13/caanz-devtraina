({
	handleCancel : function(cmp, event, helper) {
		cmp.find("overlayLib").notifyClose();
	},

	handleSave : function(cmp, event, helper) {
        cmp.set("v.disabled", true);
		cmp.get("v.saveFunction").call(this, {
			callback: $A.getCallback(function() {
                cmp.set("v.disabled", false);
				cmp.find("overlayLib").notifyClose();
			})
		});
	}
})