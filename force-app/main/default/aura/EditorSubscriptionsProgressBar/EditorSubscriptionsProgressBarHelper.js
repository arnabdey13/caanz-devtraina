({
    optionalInit : function(cmp, event) {
        
        // this controller should not send events to editors
        cmp.set("v.loadEditors", false);
        // should not deal with static resources either
        cmp.set("v.loadResourcePrefix", false);        
        
        // hide until apex callback completes
        cmp.set("v.visible", false);
        
        var current = cmp.get("v.currentStage");
        var count = cmp.get("v.isStage2Enabled")?4:1;//First Number Represents Last #  of Seq, Last # Is Number of Mandatory Notification
        cmp.set("v.progressPercent", 100 * current / count);			
    },
    optionalLoad : function(cmp, event, helper) {
        
        cmp.set("v.isStage2Enabled", cmp.get("v.properties")["prop.Notifications"]);
        
        var sm = cmp.find("stateManager");
        var flow = sm.get("v.getter")("flow");
        // should not be shown when coming from home page or tabs for Notifications or Comms Prefs
		var hidden = cmp.get("v.currentStage") > 0 && (flow == "home" || flow == "tab");
        if (!hidden) {
	        cmp.set("v.visible", false);        
        }
    }
})