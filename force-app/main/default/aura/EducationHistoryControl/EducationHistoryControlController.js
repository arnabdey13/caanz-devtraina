({
    doInit: function (component, event, helper) {
        var showRowUpload = component.get("v.showRowUpload");

        component.set("v.headerLabels",
            (showRowUpload) ?
                ['Commenced', 'Finished', 'University', 'Degree', 'Attachments', ''] :
                ['Commenced', 'Finished', 'University', 'Degree', '']);
    },
    handleValueChange: function (component, event, helper) {

        /* there are 3 combinations of opt-in/opt-out:
         mode1: opt-in for uni and degree
         mode2: opt-out for uni, opt-in for degree
         mode3: opt out for both uni and degree
         mode4: opt in for uni and out for degree

         focused/write data-shape for each mode:
         mode1: edu_Education_History__c with FK for edu_University_Degree_Join__c
         mode2: edu_Education_History__c with nested edu_University_Degree_Join__r. FK for degree, nested edu_University__c with Name
         mode3: edu_Education_History__c with nested edu_University_Degree_Join__r. both parents for join are nested with Name
         mode4: edu_Education_History__c with nested edu_University_Degree_Join__r. FK for uni, nested edu_Degree__c with Name

         both modes 2 & 3 need to be custom populated so this control handles the (ignored) value change events from the
         two auto-completes and fires un-ignored events that create the above shapes in the driver.
         */

        var params = event.getParams();
        if (params.source == component) {
            // if this event was fired by this function, break the infinite loop
            return;
        }

        var isFromUniversitySearch = params.source == component.find("universitySearch");
        var isFromDegreeSearch = params.source == component.find("degreeSearch");

        //WDCi Lean : 2018-10-17 : PAN:5340 - start - validate the year value and ensure both are populated by user
        var yearCommenceCmp = component.find("yearCommence");
        var yearFinishCmp = component.find("yearFinish");

        var yearCommence = yearCommenceCmp.get('v.value');
        var yearFinish = yearFinishCmp.get('v.value');

        if ((isFromUniversitySearch || isFromDegreeSearch) && !(yearCommence) && !(yearFinish)) {
            alert('Commence On and Finish On are required.');
            return;
        }

        if (yearCommence && yearFinish) {

            if (yearCommence > yearFinish) {
                alert('Commence On cannot be later than Finish On. Please select the year again.');
                return;
            }
        }
        //WDCi Lean : 2018-10-17 : PAN:5340 - end

        if (isFromUniversitySearch || isFromDegreeSearch) {

            // fire an un-ignored value change event to the driver
            helper.fireTranslatedValueChangeEvent(component);
            var degreeSearch = component.find("degreeSearch");

            if ($A.util.isEmpty(params.value)) {
                if (isFromUniversitySearch) {
                    degreeSearch.set("v.searchContext", {});
                    component.set("v.degreeSearchDisabled", true);
                }
            } else { // avoid cleared field value change SFS-400
                if (isFromUniversitySearch) {
                    component.set("v.degreeSearchDisabled", false);
                    if (!params.source.get("v.optedOut")) { // i.e. uni search choose a value
                        degreeSearch.set("v.searchContext", { universityId: params.value });
                        component.set("v.degreeSearchDisabled", false);
                        degreeSearch.focusOnInput();
                    }
                } else if (isFromDegreeSearch) {

                    // DEBUG
                    var valuesCheck = component.get("v.values");
                    console.log(valuesCheck);
                    // END DEBUG

                    component.set("v.saving", true);
                    var saveCallback = $A.getCallback(function () {
                        component.set("v.saving", false);
                        component.set("v.editMode", false);
                        // note: don't need to clear out prev auto-complete values because they are destroyed/created as v.editMode toggles
                        // 12 July 2019 - WDCi (LKoh) - the statement above is not true for the added commenced date and finished date on Education History, we have to reset their value to prepare for the new entry                        
                        yearCommenceCmp.set('v.value', null);
                        yearFinishCmp.set('v.value', null);
                    });

                    if (component.get("v.mockSave")) {
                        saveCallback();
                    } else {
                        // fire save event async so that the driver can handle this value change event before sending to Apex
                        setTimeout($A.getCallback(function () {
                            helper.fireApplicationEvent(component, "e.c:ff_EventSaveFocused",
                                {
                                    successMode: "ADD",
                                    callback: saveCallback
                                });
                        }), 100);
                    }

                    // component.find('universitySearch').value("");
                    // component.find('degreeSearch').value("");
                }
            }
        }
    },
    handleOptOut: function (component, event, helper) {

        var params = event.getParams();
        if (params.loadKey != "UniversitySearch" && params.loadKey != "edu_Education_History__c") {
            return; // only handle opt-outs from autocompletes in this control
        }

        var degreeSearch = component.find("degreeSearch");

        var sourceLoadKey = params.loadKey;
        var optedOut = params.optedOut;

        switch (sourceLoadKey) {
            case "UniversitySearch":
                if (optedOut) {
                    degreeSearch.set("v.searchContext", {}); // tells service to search all approved degrees
                    component.set("v.degreeSearchDisabled", false);
                } else {
                    component.set("v.degreeSearchDisabled", true);
                }
                break;
            case "edu_Education_History__c":
                break;
            default:
                throw Error("Unhandled opt out source: " + sourceLoadKey);
        }
    },
    handleLifeCycle: function (component, event, helper) {

        switch (event.getParam("type")) {
            case "load-list":
                /** RXP - 20/09/2019 - If showRowUpload is true, initialise upload controls */
                var showRowUpload = component.get("v.showRowUpload");
                if (showRowUpload) {
                    helper.loadUploadControls(component, "edu_Education_History__c", "transcriptUpload", event.getParam("opts").wrappers);
                }
                break;
            case "load-list-editor":
                setTimeout($A.getCallback(function () { // let editor load before using its children
                    component.find("universitySearch").set("v.focusHandlerExtension", helper.handleUniFocus(component));
                    component.find("yearCommence").focusOnInput();
                }), 100);
                break;
        }
    }
})