({
    getMode: function (component) {
        // returns and integer representing the opt-in/out "mode" described in the controller
        var uniOut = component.find("universitySearch").get("v.optedOut");
        var degreeOut = component.find("degreeSearch").get("v.optedOut");
        switch (JSON.stringify([uniOut, degreeOut])) {
            case "[false,false]":
                return 1;
                break;
            case "[true,false]":
                return 2;
                break;
            case "[true,true]":
                return 3;
                break;
            case "[false,true]":
                return 4;
                break;
            default:
                throw Error("invalid opt-out mode: " + JSON.stringify([uniOut, degreeOut]));
        }
    },
    fireTranslatedValueChangeEvent: function (component) {
        var valChangeParams = {
            loadKey: "edu_Education_History__c",
            valid: true,
            source: component,
            stage: component.get("v.visibleForStage"),
            ignore: false // this event is not ignored, the val-chgs from the a/c's are
        };
        switch (this.getMode(component)) {
            case 1:
                valChangeParams.field = "University_Degree__c";
                valChangeParams.value = component.find("degreeSearch").get("v.value");
                break;
            case 2:
                valChangeParams.field = "University_Degree__r";
                valChangeParams.value = {
                    attributes: {type: "edu_University_Degree_Join__c"},
                    University__r: {
                        attributes: {type: "edu_University__c"},
                        University_Name__c: component.find("universitySearch").get("v.value")
                    },
                    Degree__c: component.find("degreeSearch").get("v.value")
                };
                break;
            case 3:
                valChangeParams.field = "University_Degree__r";
                valChangeParams.value = {
                    attributes: {type: "edu_University_Degree_Join__c"},
                    University__r: {
                        attributes: {type: "edu_University__c"},
                        University_Name__c: component.find("universitySearch").get("v.value")
                    },
                    Degree__r: {
                        attributes: {type: "edu_Degree__c"},
                        Degree_Name__c: component.find("degreeSearch").get("v.value")
                    }
                };
                break;
            case 4:
                valChangeParams.field = "University_Degree__r";
                valChangeParams.value = {
                    attributes: {type: "edu_University_Degree_Join__c"},
                    University__c: component.find("universitySearch").get("v.value"),
                    Degree__r: {
                        attributes: {type: "edu_Degree__c"},
                        Degree_Name__c: component.find("degreeSearch").get("v.value")
                    }
                };
                break;
        }
        
        this.fireApplicationEvent(component, "e.c:ff_EventValueChange", valChangeParams);
    },
    handleUniFocus: function (component) {
        return function (event) {
            var degreeSearchContext = component.find("degreeSearch").get("v.searchContext");
            var isUniOptedOut = component.find("universitySearch").get("v.optedOut");
            var noContext = $A.util.isEmpty(degreeSearchContext);
            component.set("v.degreeSearchDisabled", !isUniOptedOut && noContext);
        }
    }
})