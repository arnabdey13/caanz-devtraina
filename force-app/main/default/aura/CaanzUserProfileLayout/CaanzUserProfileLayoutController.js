({
	doInit: function(cmp, event, helper) {
		var userId = cmp.get("v.userId") || "";
		$A.get("e.c:GetRecordId").setParams({ 
			callback: function(recordId){
				cmp.set('v.recordId', recordId);
				cmp.set('v.isCurrentUserRecord', (userId.substring(0,15) === recordId.substring(0,15)));
			}
		}).fire();
	}
})