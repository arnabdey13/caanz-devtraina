({
	newApplicationClick : function(component, event, helper) {

        $A.log('newapplicationcustomcontroller:newapplicationclick:new application button clicked');
        //reusing toggle event for button click
        var appEvent = $A.get("e.c:ToggleEvent");
        appEvent.setParams({key: "newApplicationButtonPressed"}).fire();	
	}
})