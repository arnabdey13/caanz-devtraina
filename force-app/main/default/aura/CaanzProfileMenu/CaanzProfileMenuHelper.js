({
	NAVIGATION_ITEMS: {
		"Home": "/",
		"My Profile & Privacy": "/s/profile/{userId}?userId={userId}",
		"My CA Settings": "/settings/{userId}",
		"My Communication Preferences": "/preferences",
		"My Messages": "/messages/Home",
		"Logout": "/secur/logout.jsp",
	},
	getUserProfile: function(cmp) {
		this.enqueueAction(cmp, "getUserProfile", {}, function(data){
			cmp.set("v.userProfile", JSON.parse(data));
		});
	},
	enqueueAction: function(cmp, method, params, callback){
		var action = cmp.get("c." + method); 
		if(params) action.setParams(params);
		action.setCallback(this, function(response){
			if(response.getState() === "SUCCESS") {
				if(callback) callback.call(this, response.getReturnValue());
	        } else if(response.getState() === "ERROR") {
	        	console.error(response.getError());
	        }
		});
		action.setStorable(true);
		$A.enqueueAction(action);
	},
	navigate: function(cmp, url){
		if(url.includes("{userId}")) {
			url = url.replace(new RegExp("{userId}", 'g'), cmp.get("v.userProfile.userId"));
		}
		if(url.includes("logout")){
            window.location.href = "https://charteredaccountantsanz.force.com/MyCA/SingleLogOut";
		} 
        else if(url.includes("profile")){
			window.location.href = "/" + $A.get("$Label.c.Community_Prefix_CCH") + url;
		} 
        else {
			$A.get("e.force:navigateToURL").setParams({ "url": url }).fire();
		}
    }
})