({
	doInit: function(cmp, event, helper) {
		cmp.set("v.menuItems", Object.keys(helper.NAVIGATION_ITEMS));
		helper.getUserProfile(cmp);
	},
	navigate: function(cmp, event, helper){
		var item = event.getSource().get("v.label");
		helper.navigate(cmp, helper.NAVIGATION_ITEMS[item]);
	}
})