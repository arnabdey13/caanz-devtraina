({
    fireDependentDisplayLoadEvents: function (component) {
        var props = component.get("v.properties");
        for (var key in props) {
            if (!$A.util.isEmpty(key) && key.indexOf("prop.ACC_") == 0) {
                var appEvent = $A.get("e.c:DependentDisplayEvent");
                appEvent.setParams({
                    dependencyKey: key.substring(5),
                    isMatch: props[key]
                }).fire();
            }
        }
    },
    getServiceParams : function(component, event, helper) {
        var sm = component.find("stateManager");
        var isPreviousYear = sm.get("v.getter")("mnp"); // mnp = mandatory notifications previous
        //console.log("prev " + isPreviousYear);
        sm.get("v.setter")("mnp", null); // immediately remove to stop other pages from being affected
        return null;
    },
    optionalLoad : function(component, event){
        // show the last year printable link if props indicates its available
        var lastYearAvailable = component.get("v.properties")["prop.lastYearPresent"];
        var appEvent = $A.get("e.c:DependentDisplayEvent");
        appEvent.setParams({dependencyKey: "printLastYear", isMatch: lastYearAvailable}).fire(); 
        
        // disable validation until the user checks the confirm box
        var appEvent = $A.get("e.c:EditorValidationEnableEvent");
        appEvent.setParams({enabled: false}).fire(); 
        this.fireDependentDisplayLoadEvents(component);
        
    },
    optionalValidation : function(component, event){
        if (event.getParam("field") == "prop.countryCode") {
            var country = event.getParam("value");
            var oldCountry = event.getParam("oldValue");
            if (!component.get("v.isLoading") && country != oldCountry) { // suppress when loading and when confirm checkbox changes
                var propsForServer = {"DIRTY": {"prop.countryCode": true},
                                      "prop.countryCode": country};
                this.sendToService(component, "c.save", {properties: propsForServer}, function(newCountryCodeSuffix) {
                    // navigate to the new notifications page
                    var destination = "/subscription-wizard-page-4-"+newCountryCodeSuffix;
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({
                        "url": destination,
                        "isredirect" :false
                    });
                    urlEvent.fire();                    
                });
            }
        }
    },    
    handleConfirmCheckbox : function(component, event){
        
        // adjust the submit button UI state
        var selected = event.getParam("value");
        component.set("v.submitDisabled", !selected);        
        
        // set client validation based on if submitting i.e. on when submitting
        // this will also re-enable the validation
        var appEvent = $A.get("e.c:EditorValidationEnableEvent");
        appEvent.setParams({enabled: selected}).fire();  
    },
    next : function(component) {
        var sm = component.find("stateManager");
        var flow = sm.get("v.getter")("flow");

        // default navigation is to next page in wizard but...
        var url = component.get("v.saveURL");
        // ...when coming from home page, save should return to home page
        if (flow == "home") {
            url = component.get("v.homeURL");
        }
        
        $A.get("e.force:navigateToURL").setParams({
            "url": url,
            "isredirect" :false
        }).fire();
    },
    navigate : function(component, event) {
        
        var sm = component.find("stateManager");
        var flow = sm.get("v.getter")("flow");
                                
        var isSave = component.get("v.submitDisabled")
       	var label1, label2;
        if (isSave) {
            label1 = component.get("v.successSaveLabel1");
        } else { // must be submit 
            label1 = component.get("v.successSubmitLabel1");
        }
        if (flow == "home") {
            if (isSave) {
                label2 = component.get("v.successSaveLabel2Home");
            } else { // must be submit 
                label2 = component.get("v.successSubmitLabel2Home");
            }
        } else { // must be wizard flow
            if (isSave) {
                label2 = component.get("v.successSaveLabel2Prefs");
            } else { // must be submit 
                label2 = component.get("v.successSubmitLabel2Prefs");
            }
        }

        // hide buttons after a submit
        component.set("v.hidden", !isSave);
            
        $A.get("e.c:EditorSetLabelsEvent").setParams({field: "success", 
                                                      label1: label1, 
                                                      label2: label2}).fire();         
        // always show the confirm toast
   		$A.get("e.c:DependentDisplayEvent").setParams({dependencyKey: "success", isMatch: true}).fire(); 
        
        // readonly after a submit
        if (!isSave) {
	        $A.get("e.c:EditorEditableEvent").setParams({isReadOnly: true}).fire();
        }
    },
    preventsNavigation : function(component, fieldName){
        var excludedFields = {"prop.confirmed" : false};
        var isExcluded = excludedFields[fieldName];
        if ($A.util.isUndefined(isExcluded)) {
            return true;
        } else {
            return isExcluded;
        }
    }
})