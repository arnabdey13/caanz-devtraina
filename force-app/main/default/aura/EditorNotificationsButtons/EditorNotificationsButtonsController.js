({
    onSave : function(component, event, helper) {
        // hide the confirm toast while saving
   		$A.get("e.c:DependentDisplayEvent").setParams({dependencyKey: "success", isMatch: false}).fire(); 
        helper.doSave(component, event, helper);
    },
    handleRecordEditable : function(component, event, helper) {
        component.set("v.locked", event.getParam("isReadOnly"));
    },
    handleChangeEvent : function(component, event, helper) {       
        helper.handleConfirmCheckbox(component, event);
    },    
    onNext : function(component, event, helper) {
        helper.next(component);
    }
})