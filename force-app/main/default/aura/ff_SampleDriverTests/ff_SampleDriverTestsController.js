({
    doInit: function (component, event, helper) {

        // NOTE: see ff_ServiceHandlerTest.testSaveMethodFocusOnNewContactWithUpsertedAccount for the same tests in Apex only
        var test = helper.driver(component, event, helper);

        var driver = component.find("driver");
        var firstNameField = component.find("firstName");
        var lastNameField = component.find("lastName");
        var accountNameField = component.find("accountName");

        var readDataAfterLoad;

        var startTime = new Date().getTime();

        // IMPORTANT: focus descriptions must be unique since they control how assertions are groups
        var startTests = test.start({
            focused: driver,
            description: "The driver being tested"
        })

        startTests
            .then(test.wait(function (context) {
                readDataAfterLoad = JSON.parse(JSON.stringify(driver.get("v.readData")));
                console.log(readDataAfterLoad);
            }))

            /////// LOAD PHASE ///////

            .then(test.assert("v.loaded", "The driver should be loaded before tests start"))
            .then(test.assert("v.readData", "The readData should be present after load"))
            .then(test.assertEquals(false, "v.formValid",
                "The form is loaded invalid because the first name field is required and empty"))
            .then(test.assertEquals(248, function () {
                return driver.get("v.readData").wrappers["User.Countries"].length;
            }, "countries were loaded"))
            .then(test.assertEquals(656, function () {
                return driver.get("v.readData").wrappers["User.States"].length;
            }, "states were loaded"))
            .then(test.assertEquals({
                    focusId: "INSERT-Contact1",
                    focusPosition: 0,
                    focusLoadKey: "Contact",
                    records: {
                        "INSERT-Contact1": {
                            attributes: {
                                type: "Contact"
                            }
                        }
                    }
                }, "v.writeData",
                "The form focuses on a new contact on-load. The temp-id and record has been created."))
            .then(test.assert(function () {
                return $A.util.isUndefined(driver.get("v.writeData").undo);
            }, "WriteData contains an undo copy of the read data record"))
            .then(test.assertEquals(0, "v.currentStage", "Form always starts on first stage"))
            .then(test.assertEquals([{
                    "label": "Step 1",
                    "state": "ACTIVE",
                    "stage": 0
                }, {
                    "label": "Step 2",
                    "state": "INCOMPLETE",
                    "stage": 1
                }], "v.allStages",
                "Two stages, 2nd incomplete because never seen despite the fact that Company Name is pre-filled"))

            .then(test.focus(component, "focus on test to read test attributes"))
            .then(test.assertEquals(1, "v.stageChangeEventCount", "Only one stage change event is required during load phase"))

            //////// EDIT PHASE SINGLE RECORD ///////
            .then(test.focus(driver, "focus on driver for edit tests"))
            .then(test.wait(function () {
                firstNameField.set("v.value", "Donald");
            }))
            // TODO important! check the readData has also been updated
            .then(test.assertEquals({
                    focusId: "INSERT-Contact1",
                    focusPosition: 0,
                    focusLoadKey: "Contact",
                    records: {
                        "INSERT-Contact1": {
                            attributes: {
                                type: "Contact"
                            },
                            FirstName: "Donald"
                        }
                    }
                }, "v.writeData",
                "Driver has updated write data ready to be sent to Apex"))
            .then(test.assertEquals(false, "v.formValid",
                "The form is still invalid because the last name field is required and empty"))
            .then(test.wait(function () {
                lastNameField.set("v.value", "Fagan");
            }))
            .then(test.assertEquals(true, "v.formValid",
                "The form is valid because all fields have values"))

            .then(test.wait(function () {
                driver.apiNextStage();
            }))
            .then(test.assertEquals(1, "v.currentStage", "Second Stage now active"))
            .then(test.assertEquals([{
                    "label": "Step 1",
                    "state": "COMPLETE",
                    "stage": 0
                }, {
                    "label": "Step 2",
                    "state": "ACTIVE",
                    "stage": 1
                }], "v.allStages",
                "Two stages, 2nd is now active"))
            .then(test.sleep(1000)) // allows tester to see old value when running test
            .then(test.wait(function () {
                accountNameField.set("v.value", "W.K.A.Z-" + startTime); // use millis so that name always changes
            }))
            .then(test.assertEquals({
                    focusId: "INSERT-Contact1",
                    focusPosition: 0,
                    focusLoadKey: "Contact",
                    records: {
                        "INSERT-Contact1": {
                            "attributes": {
                                "type": "Contact"
                            },
                            "FirstName": "Donald",
                            "LastName": "Fagan",
                            "Account": {
                                "attributes": {
                                    "type": "Account"
                                },
                                "Name": "W.K.A.Z-" + startTime
                            }
                        }
                    }
                }, "v.writeData",
                "Driver has updated write data ready to be sent to Apex"))

            //////// UPLOAD ///////

            // fire a non-blocking event to check that the driver updates an upload source with progress
            .then(test.wait(function () {
                var params = {
                    "source": component, // this test is the progress component
                    "parentId": readDataAfterLoad.wrappers["Account.Attachments"][0].parent,
                    "fileName": "CV:test1.txt",
                    "withoutMark": "dGhpcyBpcyBhIHRlc3QgZmlsZSBmb3IgdXBsb2Fk",
                    "contentType": "text/plain"
                };
                $A.get("e.c:ff_EventUploadFile").setParams(params).fire();
            }))
            .then(test.assertEquals(0, function () {
                return component.get("v.percentComplete");
            }, "the driver should have updated the event source (this test) with initial progress"))

            // call the upload via an api fn to check the driver updates progress when finished
            .then(test.whenDone(function (context, done) {
                driver.apiUploadFile(
                    component, // this test is the progress component
                    readDataAfterLoad.wrappers["Account.Attachments"][0].parent,
                    "Resume:test2.txt",
                    "dGhpcyBpcyBhIHRlc3QgZmlsZSBmb3IgdXBsb2Fk",
                    "text/plain",
                    done)
            }))
            .then(test.assert(function () {
                return $A.util.isUndefined(component.get("v.percentComplete"));
            }, "the driver should have updated the event source (this test) with completed progress"))

            //////// SAVE ///////

            // TODO stage from driver should be sent to apex as well as sobject fields

            .then(test.whenDone(function (context, done) {
                // this saves 2 sobjects since the Account is nested inside the Contact
                driver.apiSendDirtyRecords(done);
            }))
            .then(test.assert(function () {
                //console.log(JSON.stringify(driver.get("v.writeErrorsForRecord")));
                return $A.util.isEmpty(driver.get("v.writeErrorsForRecord")[0])
            }, "No errors are returned for the first (and only) Save result"))
            .then(test.wait(function () {
                //debugger;
            }))

            // i.e. also need a stage labels attribute to generate stages
            // state colours are grey, blue, orange and green.

            //////// CANCEL DIRTY ///////

            // TODO cancel behaviour for single record i.e. wipe dirty data, restore loaded copy and reload UI

            //////// POST-SAVE ///////

            // TODO inserted contact id is moved into data rendered in UI i.e. tempid substitution

            // TODO saving an invalid (from server) record and verify record and field level behaviour in client post-save

            ////// TODO's from old test ///////

            // TODO value change event for a field on a non-focused record is fired.
            // driver should check loadKeys and auto change focus to handle this scenario

            // TODO save change and check wd, undo and readData copies of data
            // TODO invoke post-save code to update undo copy with latest from db i.e. new undo point

            // TODO cancel a insert/upsert value change to ensure that the upsert data is cleaned out during undo

            // TODO also test two updates to the same single record.
            // TODO undo the first time should clone/restore the readData record for undo
            // the only way to apply changes to a child is to "save" which sends to apex (this engages validation rules)
            // TODO test cancel of an update on an existing record
            // TODO the second update should use the record returned by apex as the undo copy.
            // the second update implicitely tests update of records from initial readData

            //////////// END OF TESTS ////////////

            // always include these fns to handle the end of the test
            .then(test.pass).catch(test.fail);

    },
    handleStageChange: function (component, event, helper) {
        component.set("v.stageChangeEventCount", component.get("v.stageChangeEventCount") + 1);
    }

})