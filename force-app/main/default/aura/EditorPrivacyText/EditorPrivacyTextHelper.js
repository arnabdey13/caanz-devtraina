({
    optionalInit : function(component, event, helper) {
        var csv = component.get("v.csv");
        if (!$A.util.isEmpty(csv)) {
            this.loadLinkedObject(component, this.parseLinkedText(csv));
        }
    },
    parseLinkedText : function(src) {
        var strings = src.split(",");
        var result = {text1 : this.replaceAll(strings[0])};
        if (strings.length > 1) {
            result.link2 = this.replaceAll(strings[1]);
            result.url2 = strings[2];
        }        
        if (strings.length > 3) {
            result.text3 = this.replaceAll(strings[3]);
        }        
        if (strings.length > 4) {
            result.link4 = this.replaceAll(strings[4]);
            result.url4 = strings[5];
        }        
        if (strings.length > 6) {
            result.text5 = this.replaceAll(strings[6]);
        }        
        return result;        
    },
    replaceAll : function(s) {
        return this.replacegt(this.replacelt(this.replaceComma(s)));
    },
    replaceComma : function(s) {
        return s.split("comma").join(",");
    },
    replacelt : function(s) {
        return s.split("lthan").join("<");
    },
    replacegt : function(s) {
        return s.split("gthan").join(">");
    },
    // load from an object parsed by parseLinkedText
    loadLinkedObject : function(component, m) {
        for (key in m) {
	        component.set("v."+key, m[key]);
        }        
    }
})