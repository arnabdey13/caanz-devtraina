({
	csvChange : function(component, event, helper) {
        $A.log("csv changed", event.getParams());
        var csv = event.getParam("value");
		if (!$A.util.isEmpty(csv)) {
            helper.loadLinkedObject(component, helper.parseLinkedText(csv));
        }
    }
})