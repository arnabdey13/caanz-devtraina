({
	getData : function(component) {

        var action = component.get("c.getCPDSummariesForUser");

        // Create a callback that is executed after 
        // the server-side action returns
        action.setCallback(this, function(response) {
            var state = response.getState();
            var rtnValue = response.getReturnValue();

            if (state === "SUCCESS") {
                // Alert the user with the value returned 
                // from the server
                //alert("From server: " + response.getReturnValue());
            	console.log('rtnValue='+rtnValue);
            	$A.log('rtnValue='+rtnValue);
            	if (rtnValue) {
            		console.log('Not Null - List Length =  '+rtnValue.length);
                    component.set('v.cpdSummaryList',rtnValue);
                    for (var i = 0; i < rtnValue.length; i++){
                        if (rtnValue[i].Current_Triennium__c == true){
                    		console.log('Found Current Triennium - Start Date = '+ rtnValue[i].Triennium_Start_Date__c);
                            var a = rtnValue[i].Triennium_Start_Date__c;
                            component.set('v.TrienniumStartDate',rtnValue[i].Triennium_Start_Date__c);
                            component.set('v.TrienniumEndDate',rtnValue[i].Triennium_End_Date__c);
                            component.set('v.CurrentYearStartDate',rtnValue[i].Current_Year_Start_Date__c);
                            component.set('v.CurrentYearEndDate',rtnValue[i].Current_Year_End_Date__c);
                        }
                        else {
                            console.log('Not Current Triennium - List Index = ' + i + ' Triennium Start Date = '+ rtnValue[i].Triennium_Start_Date__c);
                        }
                    }
            	}

                // You would typically fire a event here to trigger 
                // client-side notification that the server-side 
                // action is complete
            }
            else if (state === "INCOMPLETE") {
                alert("Something went wrong. Refresh the page.");
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

       $A.enqueueAction(action);
		
	}
})