({
	doInit : function(cmp, event, helper) {
		if (window.localStorage.getItem('CaanzccIdeasIntroDismissed') === 'true') {
			cmp.set('v.hide', true);
		}
	},

	close : function(cmp, event, helper) {
		cmp.set('v.hide', true);
		window.localStorage.setItem('CaanzccIdeasIntroDismissed', 'true');
	}
})