({
    doInit : function(cmp) {
        var stageList = cmp.get("v.stages").split(",");

        var current = cmp.get("v.currentStage");
        cmp.set("v.stageList", stageList);

        
        stageList.forEach(function(stage, index, arr ) {
            $A.createComponent(
                "c:navigationComponent",
                {index:index,
                 label: stage,
                 state: index == current,
                 assistiveText: stage},
                function(newItem){
                    console.log(newItem);
                    if (cmp.isValid()) {
                        var body = cmp.get("v.body");
                        console.log(body);
                        body.push(newItem);
                        cmp.set("v.body", body);
                    } else {
                        console.log("Invalid item widget!");
                    }
                }
            );
            
        });        
    }
})