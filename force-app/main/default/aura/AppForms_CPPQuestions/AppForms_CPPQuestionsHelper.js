({
    /** resetValues Resets each valueComponent child control within this component to empty value **/
    resetValues: function (component, event, helper) {
        var valueComponents = component.find('valueComponent');

        valueComponents.forEach(function (cmp) {
            cmp.set('v.value', null);
        });
    },
    /** mapValues React to field load/value change for fields contained within fieldsToWatch (such as country) and show/hide appropriate CPP questions **/
    mapValues: function (component, event, helper, field, value) {
        var params = event.getParams();
        var fieldName = field || params.field;
        var fieldValue = value || params.value;

        var fieldsToWatch = component.get('v.fieldsToWatch');
        var fieldsObject = component.get('v.fieldsObject');

        if (!fieldsObject) {
            fieldsObject = {};
        }

        if (fieldsToWatch.includes(fieldName)) {
            if (fieldValue) {
                if (fieldName === 'PersonOtherCountryCode' && fieldsObject[fieldName] && fieldsObject[fieldName] !== fieldValue) {
                    //helper.resetValues(component, event, helper);
                }
                var valueComponents = component.find('valueComponent');
                if (fieldValue === "AU") {
                    valueComponents[2].set('v.parentVisibility', false);
                }
                else if (fieldValue === "NZ") {
                    valueComponents[0].set('v.parentVisibility', false);
                    valueComponents[1].set('v.parentVisibility', false);
                }
                else {
                    valueComponents.forEach(function (cmp) {
                        cmp.set('v.parentVisibility', false);
                    });
                }
            }

            fieldsObject[fieldName] = fieldValue;
            component.set('v.fieldsObject', fieldsObject);
        }
    }
})