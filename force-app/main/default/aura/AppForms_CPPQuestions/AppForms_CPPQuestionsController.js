({
    /** handleLifeCycle On field load event, get value and map to fieldsObject **/
    handleLifeCycle : function(component, event, helper) {
        var eventType = event.getParam("type");
        var eventSource = event.getSource();
        
        if (eventType === 'load-field') {
            helper.mapValues(component, event, helper, eventSource.get('v.field'), eventSource.get('v.value'));
        }
    },
    /** handleValueChange On field value change, get value and map to fieldsObject **/
    handleValueChange : function(component, event, helper) {
      	helper.mapValues(component, event, helper);
    }
})