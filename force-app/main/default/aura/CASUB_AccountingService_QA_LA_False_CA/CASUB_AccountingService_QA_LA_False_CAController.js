({
    handleQA_LA_options : function(cmp,event,helper){
        if(event.getParam('value') == 'Yes'){
            cmp.set('v.confirmQA_LA',true);
        }
        else{
            cmp.set('v.confirmQA_LA',false);
        }
    }
})