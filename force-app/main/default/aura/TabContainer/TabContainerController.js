({
    doInit : function(cmp, event, helper) {
        var pathname = helper.setCurrentTab(cmp); // holds current URL pathname.
        var tabsListVar = cmp.get("v.tabs").split(",");
        var linksListVar = cmp.get("v.links").split(","); //holds Links for Tabs. Comes from community builder confi
        var tabListVar = cmp.get("v.tabURLName").split(",") //holds URL names for setting selected Tab. Comes from community builder config
        var current  ; // holds the current tab number based on URL
        var activeTabFlag; //used to check if ongoing tab matches with URL  
        cmp.set("v.tabsList", tabsListVar);
        cmp.set("v.linksList", linksListVar)
        for (i = 0; i < tabsListVar.length; i++) { 
            var currval = tabListVar[i] ;
            if(pathname.indexOf(currval) > -1 ){
                current = i ;
                activeTabFlag = true ;
            }
            $A.createComponent(
                "c:TabComponent",
                {index : i,
                 label : tabsListVar[i],
                 active : i == current,
                 link : linksListVar[i],
                 currentIndex : activeTabFlag ? current : 0 
                },
                function(newItem){
                    if (cmp.isValid()) {
                        var body = cmp.get("v.body");
                        body.push(newItem);
                        cmp.set("v.body", body);
                        //setting Component level event for each TabComponent created
                        newItem.addHandler("settingSelectedTab", cmp, "c.parentHandling");
                        //firing an event to set the selected Tab based on URL.
                        var cmpEvent = newItem.getEvent("settingSelectedTab");                       
                        var currentIndex = newItem.get("v.currentIndex");
                        cmpEvent.setParams({
                            "activeTab": currentIndex
                        });
                        cmpEvent.fire();
                    }
                }
            );
        }
    },
    
    parentHandling : function(cmp, event, helper) {        
        var currentTabNumber = event.getParam("activeTab");
        helper.updateTabIndex(cmp, currentTabNumber);
    }    
})