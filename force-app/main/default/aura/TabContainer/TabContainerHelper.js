({
    //Function used to set find TabComponent instances and set the clicked Tab as Active.
    updateTabIndex : function(cmp, currentTab) {
        var cmpnentChildList = cmp.find({ instancesOf : "c:TabComponent" });
        cmpnentChildList.forEach(function(stage, index) {
            stage.set("v.active", index == currentTab);
        });
    },
    
    //Function used in detecting the current Tab from browser URL.
    setCurrentTab : function(cmp){
        var query = window.location.pathname ;
        return query ;
    }
    
})