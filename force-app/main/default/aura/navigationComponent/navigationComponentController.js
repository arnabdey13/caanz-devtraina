({
    doInit : function(cmp) {
        var current = cmp.get("v.state");
        //var currentlink = cmp.get("v.link");
        cmp.set("v.link", currentlink);
        //console.log('link---' +currentlink);
        if(current){
			cmp.set("v.stateClass", "slds-tabs--default__item slds-text-heading--label slds-active"); 
            cmp.set("v.ariaselected", true);
            cmp.set("v.tabindex", 0);
        }else{
            cmp.set("v.stateClass", "slds-tabs--default__item slds-text-heading--label");
            cmp.set("v.ariaselected", false);
            cmp.set("v.tabindex", -1);
        }
        
    }
})