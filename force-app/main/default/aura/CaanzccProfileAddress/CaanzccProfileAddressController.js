({
	doInit: function(cmp, event, helper) {
		helper.getAddressData(cmp);
	},
    handleShowModal: function(cmp, evt, helper) {
        var modalBody, modalFooter;
        $A.createComponents([
                ['c:CaanzAddressDetails', {}],
                ['c:CaanzccModalFooter', {}],
            ],
            function(components, status) {
                if (status === "SUCCESS") {
                    modalBody = components[0];
                    modalFooter = components[1];
                    modalFooter.set('v.saveFunction', modalBody.save)
                    cmp.find('overlayLib').showCustomModal({
                        header: "Edit Address",
                        body: modalBody,
                        footer: modalFooter,
                        cssClass: "cCaanzccProfileAddress",
                        showCloseButton: true,
                        closeCallback: function() {
							helper.getAddressData(cmp);
                        }
                    });
                }
            }
        );
    }
})