({
    afterSVG4EverybodyLoaded : function(component, event, helper) {
        // disabling this since we are using pngs instead of svg to support IE until Lightning does svg better.
        // also this is failing to run in IE11 in some cases so just no good
        //svg4everybody();
    },
    // this init is used to delay init processing until after js assets are loaded
    postAssetsInit : function(component, event, helper) {
        var concreteComponent = component.getConcreteComponent();
        var concreteHelper = helper.getConcreteHelper(component);
        concreteHelper.init(concreteComponent, event, concreteHelper);
        
        var editorClasses = component.get("v.editorClasses");
        if (!$A.util.isEmpty(editorClasses)) {
            $A.get("e.c:EditorSetClassesEvent").setParams({classes: editorClasses}).fire();
        }                
    },
    handleValidation : function(component, event, helper) {
        var validations = component.get("v.failedValidations");
        var field = event.getParam("field");
        if (event.getParam("isValid")) {
            delete validations[field];
        } else {
            validations[field] = true;
        }
        var validationErrorsExist = helper.getObjectSize(validations) > 0;
        component.set("v.validationMessage", validationErrorsExist ? component.get("v.validationFailedMessage"): null);
        component.set("v.disabled", validationErrorsExist);
        $A.log("tracking v8n: "+event.getParam("field"), validations);
        var concreteComponent = component.getConcreteComponent();
        var concreteHelper = helper.getConcreteHelper(component);
        concreteHelper.optionalValidation(concreteComponent, event);
        
        if (!component.get("v.isLoading")) {
            if (event.getParam("value") != event.getParam("oldValue")) {
                var fieldName = event.getParam("source").get("v.field");
                if (concreteHelper.preventsNavigation(concreteComponent, fieldName)) {
                    $A.log("navigation block starts: "+fieldName);
                    helper.preventNavigation("You have unsaved changes!");
                }
            }
        }
    },
    onSave : function(component, event, helper) {
        helper.doSave(component, event, helper);
    },
    showSpinner : function(component, event, helper) {
        component.set("v.waitingForApex",true);
    },
    hideSpinner : function(component, event, helper) {
        component.set("v.waitingForApex",false);
    },
    handleException : function(component, event, helper) {
        component.set("v.waitingForApex",false);
        
        m = component.find('modalerror');
        $A.util.removeClass(m, "slds-hide");    
    },
    closeExceptionModal : function(component, event, helper) {
        m = component.find('modalerror');
        $A.util.addClass(m, "slds-hide");    
    }
})