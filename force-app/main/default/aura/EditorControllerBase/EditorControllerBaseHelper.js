({
    init : function(component, event, helper) {
        component.set("v.failedValidations",{});
                
        // in communities pages there's no guaranteed order of create/init of components so use a slight delay 
        // for all events that should be seen by all components. this allows all components to be ready with a 
        // handler. without this the handling was indeterminite.
        setTimeout(function() {
            if (component.get("v.loadResourcePrefix")) {
                var prefix = component.get("v.resourcePrefix");
                var appEvent = $A.get("e.c:StaticResourceConfigEvent");
                appEvent.setParams({resourcePrefix: $A.util.isUndefined(prefix)?'':prefix}).fire();
            }
            
            var params = helper.parseURL();
            if (helper.getObjectSize(params) > 0) {
                var appEvent = $A.get("e.c:EditorURLParamsEvent");
                appEvent.setParams({urlParams: params}).fire();
            }
        },250);
        
        var concreteComponent = component.getConcreteComponent();
        var concreteHelper = helper.getConcreteHelper(component);

        // this is the call to apex which will return data for the page to load from
        var params = concreteHelper.getServiceParams(concreteComponent,event,concreteHelper);
        helper.sendToService(concreteComponent, "c.load", params , function(val) {
                                
            // save all the apex data in a properties attribute (editors also have this attribute)
            component.set("v.properties", val);
            
            // allow buttons/controllers to implement page specific behaviour
            concreteHelper.optionalLoad(concreteComponent, event);
            
            // if labels are required by apex, convert to a map to make editor code simpler
            var labelsRequired = val['prop.labelsRequired'];
            if (labelsRequired) {
                var lrm = {};
                for (var i = 0; i < labelsRequired.length; i++) {
					lrm[labelsRequired[i]] = true;
                }
                val['prop.labelsRequiredMap'] = lrm;
            }
            
            if (component.get("v.loadEditors")) {
                // fire an event for all editors to load themselves using the apex data
                var appEvent = $A.get("e.c:EditorLoadEvent");
                appEvent.setParams({properties: val}).fire();
            }
            
            // if the apex data indicates read-only, fire an event for all editors to change to read-only mode
            if (val["prop.readOnly"]) {
                component.set("v.disabled", true);
                var appEvent = $A.get("e.c:EditorEditableEvent");
                appEvent.setParams({isReadOnly: true}).fire();
            }
            
            // reset the dirty object in case editors change it during load
            // e.g. EditorRadioButtons value-change handler
            var dirty = val.DIRTY;
            if (dirty) {
                val.DIRTY = {};
            }
                        
            concreteHelper.optionalPostLoad(concreteComponent, event);
            
            // adjust the isLoading control attribute so that page specific code can tell if loading is complete
            component.set("v.isLoading", false);            
        
        });
        
        concreteHelper.optionalInit(concreteComponent, event, concreteHelper);
    },
    doSave : function(component, event, helper) {
        
        var componentName = component.getDef().getDescriptor().getName();
        var appEvent = $A.get("e.c:UserClickEvent");
        appEvent.setParams({sourceLabel: componentName+".save"}).fire();        
        
        component.set("v.saveWasAttempted", true);
        if (component.get("v.validationMessage")) {
            $A.log("validations blocking");
            var appEvent = $A.get("e.c:ValidationDisplayEvent");
            appEvent.setParams({show: true}).fire();
        } else {
            this.enableNavigation();
            $A.log("sending to service", component.get("v.properties"));
            helper.sendToService(component, "c.save", {properties: component.get("v.properties")}, function(val) {
                if (val && val.VALIDATION_ERRORS) {
                    component.set("v.validationMessage", component.get("v.validationFailedMessage"));
                    var appEvent = $A.get("e.c:ValidationDisplayEvent");
                    appEvent.setParams({show: true, validationErrors: val.VALIDATION_ERRORS}).fire();
                } else {
                    helper.getConcreteHelper(component).navigate(component, event);
                }                               
            });
        }
    },
    sendToService : function(component, method, params, callback) {
        var action = component.get(method);
        if (params) {
            action.setParams(params);
        }
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {              
                callback(response.getReturnValue());              
            }else if (component.isValid() && state === "ERROR") {
                $A.error("service call ERROR");
                this.showUnexpectedError(component);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    $A.error("service call ERROR: "+errors[0].message);                
                    this.showUnexpectedError(component);
                } else {
                    $A.error("Unknown error");
                    this.showUnexpectedError(component);
                }
            }
        });        
        $A.enqueueAction(action);            
    },
    getServiceParams : function(component, event, helper) {
        return null;
    },
    showUnexpectedError : function(component) {
        var appEvent = $A.get("e.c:ApexExceptionEvent");
        appEvent.fire();
    },
    navigate : function(component, event) {
        $A.log("TODO implement the navigate function for: ", component.getConcreteComponent());   
    },
    getConcreteHelper : function(component) {
        var cc = component.getConcreteComponent();
        return cc.getDef().getHelper();
    },
    getObjectSize : function(obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    },
    parseURL : function () {
        var query_string = {};
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            // If first entry with this name
            if (typeof query_string[pair[0]] === "undefined") {
                query_string[pair[0]] = decodeURIComponent(pair[1]);
                // If second entry with this name
            } else if (typeof query_string[pair[0]] === "string") {
                var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
                query_string[pair[0]] = arr;
                // If third or later entry with this name
            } else {
                query_string[pair[0]].push(decodeURIComponent(pair[1]));
            }
        } 
        return query_string;
    },
    endsWith : function endsWith(str, suffix) {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    },
    optionalLoad : function(component, event){
        // this fn is called just before the apex callback is invoked and after the properties attribute has been set 
        // i.e. use component.get("v.properties") for record/props access
        // no-op is default
    },        
    optionalPostLoad : function(component, event){
        // this fn is called after the apex callback code is complete
        // but before the isLoading property becomes false
        // i.e. use component.get("v.properties") for record/props access
        // no-op is default
    },        
    optionalInit : function(component, event){
        // this fn is called at page load/init after the apex call has been sent
        // but before the callback code has run
        // no-op is default
    },
    optionalValidation : function(component, event){
        // no-op is default
    },
    // return true for any field that, when changed, should engage the navigation block
    preventsNavigation : function(component, fieldName){
        return false; // by default navigation is not blocked by changes to all fields i.e. navigation blocks are off by default
    },
    // navigation fns below from http://stackoverflow.com/questions/1119289/how-to-show-the-are-you-sure-you-want-to-navigate-away-from-this-page-when-ch
    preventNavigation : function(message) {
        var confirmOnPageExit = function (e) {
            // If we haven't been passed the event get the window.event
            e = e || window.event;
            
            // For IE6-8 and Firefox prior to version 4
            if (e)
            {
                e.returnValue = message;
            }
            
            // For Chrome, Safari, IE8+ and Opera 12+
            return message;
        };
        window.onbeforeunload = confirmOnPageExit;
    },
    enableNavigation : function () {
        window.onbeforeunload = null;
    }    
})