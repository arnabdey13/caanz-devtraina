({
    //To handle save function of the form
    handleSave : function(component, event, helper) {
        /* add question and answere for CDP Component */
        helper.saveComponentQsnAns(component,event);
    },
    //To handle the validation on Save
    handlePrivacy1: function(component, event, helper) {
        var privacy1=component.find("Question_19__c").get("v.checked");
        var privacy2=component.find("Question_20__c").get("v.checked");
        if(privacy1){
            component.set("v.isDisabled", false);
        }
        else{
            component.set("v.isDisabled", true); 
        }
    },
    validatevalidatePrivacyConsentCondition : function(component, event, helper){
        if(!component.get("v.questionnaireResponseSobject.Privacy_Policy__c")){
            helper.showToast(component,'Please indicate your understanding and agreement with our Privacy Policy to proceed with your Subscription Renewal');
        }
        /*else if(!component.get("v.questionnaireResponseSobject.Nomination_Assessment_Consent__c")){
             helper.showToast(component,'Please confirm nomination by CA ANZ to proceed for payment');
        }*/
        else{
            var params = event.getParam ? event.getParam("arguments").params || {} : {}; 
            var memberProfile = '';
            var sectionVisibilityObject = params.sectionVisibilityObject;
            if(!$A.util.isUndefinedOrNull(params) && params.memberProfile){
                memberProfile = params.memberProfile;
            }  
            /*
          if(!$A.util.isUndefinedOrNull(params) && params.memberProfile && params.memberProfile==$A.get("$Label.c.CAANZ_Profile_Non_Member")){
              memberProfile = params.memberProfile;
          }*/
            var questionnaireResponseSobject = component.get("v.questionnaireResponseSobject");
            var questionaireResponseObjectToUpdate = component.get("v.questionaireResponseObjectToUpdate");
            questionaireResponseObjectToUpdate.Privacy_Policy__c = component.get("v.questionnaireResponseSobject.Privacy_Policy__c"); 
            questionaireResponseObjectToUpdate.Nomination_Assessment_Consent__c = component.get("v.questionnaireResponseSobject.Nomination_Assessment_Consent__c");
            var isAnySectionVisible = false;
            if(sectionVisibilityObject.isCPDForNZ || sectionVisibilityObject.isCPPForNZ || sectionVisibilityObject.isRegistrationForNZ || 
               sectionVisibilityObject.isCPPForAUToDisplay || sectionVisibilityObject.isCertPublicPractise || sectionVisibilityObject.toDisplayAUAndOverseasRegistration){
                isAnySectionVisible = true;
            }
            /**
             * If none of the section is visible to the user, then he should be allowed to go next
             */ 
            if(!isAnySectionVisible){
                if(!$A.util.isUndefinedOrNull(questionnaireResponseSobject.Id)){
                    questionaireResponseObjectToUpdate.Id = questionnaireResponseSobject.Id; 
                }
                questionaireResponseObjectToUpdate.Residential_Country__c = questionnaireResponseSobject.Residential_Country__c;
                helper.saveMandatoryPrivacyConsent(component,questionaireResponseObjectToUpdate,params);
            }else if(memberProfile==$A.get("$Label.c.CAANZ_Profile_Non_Member")){
                if(!$A.util.isUndefinedOrNull(questionnaireResponseSobject.Id)){
                    questionaireResponseObjectToUpdate.Id = questionnaireResponseSobject.Id; 
                }
                questionaireResponseObjectToUpdate.Residential_Country__c = questionnaireResponseSobject.Residential_Country__c;
                helper.saveMandatoryPrivacyConsent(component,questionaireResponseObjectToUpdate,params);
            }
            else if(memberProfile==$A.get("$Label.c.CAANZ_Profile_Provisional_Member")){
                if(!$A.util.isUndefinedOrNull(questionnaireResponseSobject.Id)){
                    questionaireResponseObjectToUpdate.Id = questionnaireResponseSobject.Id; 
                }
                questionaireResponseObjectToUpdate.Residential_Country__c = questionnaireResponseSobject.Residential_Country__c;
                helper.saveMandatoryPrivacyConsent(component,questionaireResponseObjectToUpdate,params);
            }
                else{
                    if ($A.util.isUndefinedOrNull(questionnaireResponseSobject.Id)) {
                        helper.showToast(component,'Please save your membership details to proceed with renewal');
                    }else{
                        questionaireResponseObjectToUpdate.Id = questionnaireResponseSobject.Id;
                        questionaireResponseObjectToUpdate.Residential_Country__c = questionnaireResponseSobject.Residential_Country__c;
                        helper.saveMandatoryPrivacyConsent(component,questionaireResponseObjectToUpdate,params); 
                    }
                }
            
        }
    }
})