({
    saveComponentQsnAns : function(component,event) {
        console.log('in questionans save - CPD');
        var cpdComp = component.get('v.mandNot').find('cpdComp');
        var qAns = component.get("v.questnSobject");
        qAns.Question_1__c = 'Country';
        
        qAns.Question_1__c = 'Country';
        qAns.Answer_1__c = 'New Zealand';
        
        qAns.Question_2__c = $A.util.isUndefinedOrNull(cpdComp.find('Question_2__c')) ? '' :
        cpdComp.find('Question_2__c').getElements()[0].innerHTML;
        qAns.Answer_2__c = $A.util.isUndefinedOrNull(cpdComp.find('Answer_2__c')) ? '' :
        cpdComp.find('Answer_2__c').get('v.value');
        
        console.log('in questionans save - CPD---');
        
        qAns.Question_3__c = $A.util.isUndefinedOrNull(cpdComp.find('Question_3__c')) ? '' :
        cpdComp.find('Question_3__c').getElements()[0].innerHTML;
        qAns.Answer_3__c =  $A.util.isUndefinedOrNull(cpdComp.find('Answer_3__c')) ? '' :
        cpdComp.find('Answer_3__c').get('v.checked').toString();
        
        qAns.Question_4__c = $A.util.isUndefinedOrNull(cpdComp.find('Question_4__c')) ? '' :
        cpdComp.find('Question_4__c').get('v.label');
        qAns.Answer_4__c = $A.util.isUndefinedOrNull(cpdComp.find('Question_4__c')) ? '' :
        cpdComp.find('Question_4__c').get('v.value') == 'Other' ? cpdComp.find('other_Answer_4__c').get('v.value') : cpdComp.find('Question_4__c').get('v.value');
        
        
        /* Save AccountingService Component Question and Answers */
        console.log('in questionans save - AccSer');
        var accSer = component.get('v.mandNot').find('accServc');
        qAns.Question_5__c = $A.util.isUndefinedOrNull(accSer.find('Question_5__c')) ? '' :
        accSer.find('Question_5__c').getElements()[0].innerHTML;
        qAns.Answer_5__c = $A.util.isUndefinedOrNull(accSer.find('Answer_5__c')) ? '' :
        accSer.find('Answer_5__c').get('v.value').toString();
        
        qAns.Question_6__c = $A.util.isUndefinedOrNull(accSer.find('Question_6__c')) ? '' :
        accSer.find('Question_6__c').getElements()[0].innerHTML;
        qAns.Answer_6__c = $A.util.isUndefinedOrNull(accSer.find('Answer_6__c')) ? '' :
        accSer.find('Answer_6__c').get('v.value');
        
        qAns.Question_7__c = $A.util.isUndefinedOrNull(accSer.find('Question_7__c')) ? '' :
        accSer.find('Question_7__c').getElements()[0].innerHTML;
        qAns.Answer_7__c = $A.util.isUndefinedOrNull(accSer.find('Answer_7__c')) ? '' :
        accSer.find('Answer_7__c').get('v.value');
        
        qAns.Question_8__c = $A.util.isUndefinedOrNull(accSer.find('Question_8__c')) ? '' :
        accSer.find('Question_8__c').getElements()[0].innerHTML;
        qAns.Answer_8__c = $A.util.isUndefinedOrNull(accSer.find('Answer_8__c')) ? '' :
        accSer.find('Answer_8__c').get('v.value').toString();
        
        qAns.Question_9__c = $A.util.isUndefinedOrNull(accSer.find('Question_9__c')) ? '' :
        accSer.find('Question_9__c').getElements()[0].innerHTML;
        qAns.Answer_9__c = $A.util.isUndefinedOrNull(accSer.find('Answer_9__c')) ? '' :
        accSer.find('Answer_9__c').get('v.value');
        
        qAns.Question_10__c = $A.util.isUndefinedOrNull(accSer.find('Question_10__c')) ? '' :
        accSer.find('Question_10__c').getElements()[0].innerHTML;
        qAns.Answer_10__c = $A.util.isUndefinedOrNull(accSer.find('Answer_10__c')) ? '' :
        accSer.find('Answer_10__c').get('v.value');
        
        qAns.Question_11__c = $A.util.isUndefinedOrNull(accSer.find('Question_11__c')) ? '' :
        accSer.find('Question_11__c').getElements()[0].innerHTML;
        qAns.Answer_11__c =  $A.util.isUndefinedOrNull(accSer.find('Answer_11__c')) ? '' :
        accSer.find('Answer_11__c').get('v.value').toString();
        
        
        /* Save RegistrationDetails Component Question and Answers */
        console.log('in questionans save - RegDet');
        var regDet = component.get('v.mandNot').find('regDetails').find('qstnComp');
        if(!component.get('v.mandNot').find('regDetails').get('v.isAccreditedInsolvency')){
            console.log('in if');
            qAns.Question_12__c = $A.util.isUndefinedOrNull(regDet.find('Question_12__c')) ? '' :
            regDet.find('Question_12__c').getElements()[0].innerHTML;
            qAns.Answer_12__c = $A.util.isUndefinedOrNull(regDet.find('Answer_12__c')) ? '' :
            regDet.find('Answer_12__c').get('v.value') == 'No' ? regDet.find('c_Answer_12__c').get('v.value') : regDet.find('Answer_12__c').get('v.value').toString();
            
            qAns.Question_13__c = $A.util.isUndefinedOrNull(regDet.find('Question_13__c')) ? '' :
            regDet.find('Question_13__c').getElements()[0].innerHTML;
            qAns.Answer_13__c = $A.util.isUndefinedOrNull(regDet.find('Answer_13__c')) ? '' :
            regDet.find('Answer_13__c').get('v.value') == 'No' ? regDet.find('c_Answer_13__c').get('v.value') : regDet.find('Answer_13__c').get('v.value').toString();
            
            qAns.Question_14__c = $A.util.isUndefinedOrNull(regDet.find('Question_14__c')) ? '' :
            regDet.find('Question_14__c').getElements()[0].innerHTML;
            qAns.Answer_14__c = $A.util.isUndefinedOrNull(regDet.find('Answer_14__c')) ? '' :
            regDet.find('Answer_14__c').get('v.value') == 'No' ? regDet.find('c_Answer_14__c').get('v.value') : regDet.find('Answer_14__c').get('v.value').toString();
            
            qAns.Question_15__c = $A.util.isUndefinedOrNull(regDet.find('Question_15__c')) ? '' :
            regDet.find('Question_15__c').getElements()[0].innerHTML;
            qAns.Answer_15__c =  $A.util.isUndefinedOrNull(regDet.find('Answer_15__c')) ? '' :
            regDet.find('Answer_15__c').get('v.value') == 'No' ? regDet.find('c_Answer_15__c').get('v.value') : regDet.find('Answer_15__c').get('v.value').toString();
            
            qAns.Question_16__c = $A.util.isUndefinedOrNull(regDet.find('Question_16__c')) ? '' :
            regDet.find('Question_16__c').getElements()[0].innerHTML;
            qAns.Answer_16__c = $A.util.isUndefinedOrNull(regDet.find('Answer_16__c')) ? '' :
            regDet.find('Answer_16__c').get('v.value') == 'No' ? regDet.find('c_Answer_16__c').get('v.value') : regDet.find('Answer_16__c').get('v.value').toString();
            
            qAns.Question_17__c = $A.util.isUndefinedOrNull(regDet.find('Question_17__c')) ? '' :
            regDet.find('Question_17__c').getElements()[0].innerHTML;
            qAns.Answer_17__c = $A.util.isUndefinedOrNull(regDet.find('Answer_17__c')) ? '' :
            regDet.find('Answer_17__c').get('v.value') == 'No' ? regDet.find('c_Answer_17__c').get('v.value') : regDet.find('Answer_17__c').get('v.value').toString();
        }
        else{
            console.log('in else');
            qAns.Question_18__c = $A.util.isUndefinedOrNull(regDet.find('Question_18__c')) ? '' :
            regDet.find('Question_18__c').getElements()[0].innerHTML;
            qAns.Answer_18__c = $A.util.isUndefinedOrNull(regDet.find('Answer_18__c')) ? '' :
            regDet.find('Answer_18__c').get('v.value') == 'No' ? regDet.find('c_Answer_18__c').get('v.value') : regDet.find('Answer_18__c').get('v.value').toString();   
            
        }	
        
        
        /* Save PrivacyConcent Component Question and Answers */
        console.log('in questionans save - Privacy');
        var priv = component.get('v.mandNot').find('privacyConsent');
        
        qAns.Question_19__c = 'Privacy1';//priv.find('Question_19__c').get('v.label');
        qAns.Answer_19__c = $A.util.isUndefinedOrNull(priv.find('Question_19__c')) ? '' :
        priv.find('Question_19__c').get('v.checked').toString();	
        
        qAns.Question_20__c = 'Privacy2';//priv.find('Question_20__c').get('v.label');
        qAns.Answer_20__c =  $A.util.isUndefinedOrNull(priv.find('Question_20__c')) ? '' :
        priv.find('Question_20__c').get('v.checked').toString();
        
        debugger;
        component.get('v.mandNot').set('v.finalquestnSobject',qAns);
        
    },
    saveMandatoryPrivacyConsent:function(component,questionaireResponseObjectToUpdate,callBackParams){
        var param = {
            'personAccountId':component.get("v.personAccountId"),
            'questionaireResponseObjectToUpdate':questionaireResponseObjectToUpdate,
            'selectedCountry':component.get("v.selectedCountry"),
            'whichSectionSave':'PrivacyConsent'
        };
        this.enqueueAction(component, "upsertQuestionaireResponse", param, function(error, data){
            if(error){
                this.showToast(component,errorMessage);
            }else if(!$A.util.isUndefinedOrNull(callBackParams) && callBackParams.callback) {
                component.set("v.errorMessage",'');
                callBackParams.callback();
            }
            
        });
    },
    showToast : function(component,text) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Warning!",
            "message": text,
            type : 'warning',
        });
        toastEvent.fire();
    },
    enqueueAction: function(component, method, params, callback){
        this.toggleSpinner(component, true);
        var action = component.get("c." + method); 
        if(params) action.setParams(params);
        action.setCallback(this, function(response){
            this.toggleSpinner(component, false);
            if(response.getState() === "SUCCESS") {
                if(callback) callback.call(this, null, response.getReturnValue());
            } else if(response.getState() === "ERROR") {
                var message = 'Unknown error'; 
                var errors = response.getError();
                if (!$A.util.isEmpty(errors)) {
                    message = errors[0].message;
                }
                console.error(message);
                if(callback) callback.call(this, message);
            }
        });
        $A.enqueueAction(action);
    },
    toggleSpinner: function(component, show) {
        var spinner = component.find("privacySpinner");
        $A.util.addClass(spinner, show ? "slds-show" : "slds-hide");
        $A.util.removeClass(spinner, !show ? "slds-show" : "slds-hide");
    }
})