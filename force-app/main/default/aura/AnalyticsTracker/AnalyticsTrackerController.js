({
    init: function(component, event, helper) {
        // call apex to get config/user data
        helper.sendToService(component, "c.load", {}, 
                             function(result) {
                                 $A.log(result);
                                 // don't track when no asset has been configured
                                 if (result.asset) {
                                     // load the anayltics data object
                                     window.dataLayer = {user: {membershipID: result.userId,
                                                                username: result.userName,
                                                                membershipType: result.membershipType,
                                                                env: result.env}}; 
                                     component.set("v.mappings", result);
                                     // load the satellite/tracker asset
                                     helper.loadTracker(component, event, helper, result.asset);
                                 } else {
                                     console.log("analytics not configured");
                                 }
                             });                
    },    
    afterTrackerLoaded : function(component, event, helper) {
        _satellite.pageBottom();
        component.set("v.trackerReady", true);
    },
    track : function(component, event, helper) {
        helper.track(component, event, helper);
    },
    trackClick : function(component, event, helper) {
        var mappings = component.get("v.mappings");
        if (mappings) { // won't be present if tracker disabled or not loaded yet
            var isDebugEnabled = mappings.debug == "true";
            var mappedEvent = mappings[event.getParam("sourceLabel")];
            if (mappedEvent) {
                _satellite.track(mappedEvent);
                if (isDebugEnabled) {
	                console.log("Sending click: "+event.getParam("sourceLabel"));
                }
            } else if (isDebugEnabled) {
                console.log("Unmapped click: "+event.getParam("sourceLabel"));
            }
        }
    }
})