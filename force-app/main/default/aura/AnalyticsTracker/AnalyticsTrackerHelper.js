({
    track : function(component, event, helper) {
        if (window.location.pathname != component.get("v.path")) {
            // track navigation i.e. path changes
            if (component.get("v.trackerReady")) {
                
                window.dataLayer.page = {pageInfo: {pageURL: window.location.href,
                                                    brand: 'SFDC',
                                                    domain: window.location.host}};
                var mappings = component.get("v.mappings");
                var adobeMapping = mappings[window.location.pathname];
                if (adobeMapping) {
                    var parts = adobeMapping.split(':');
                    window.dataLayer.page.pageInfo.pageName = 'SFDC:' + parts[1];
                    window.dataLayer.category = {primaryCategory: parts[0]};
                } else {
                    window.dataLayer.page.pageInfo.pageName = 'Unmapped';
                    window.dataLayer.category = {primaryCategory: 'Unmapped'};
                }
                _satellite.track("spa-page-view");
                component.set("v.path", window.location.pathname);
            }
        }
    },
    loadTracker : function(component, event, helper, asset) {
        $A.createComponent("ltng:require", {
            scripts: [asset],
            afterScriptsLoaded: component.getReference("c.afterTrackerLoaded")
        }, function(requireCmp) {
            component.set("v.dynamicRequire", [requireCmp]);
        });    
    },
    sendToService : function(component, method, params, callback) {
        var action = component.get(method);
        if (params) {
            action.setParams(params);
        }
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {              
                callback(response.getReturnValue());              
            }else if (component.isValid() && state === "ERROR") {
                $A.error("service call ERROR");
                this.showUnexpectedError(component);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    $A.error("service call ERROR: "+errors[0].message);                
                    this.showUnexpectedError(component);
                } else {
                    $A.error("Unknown error");
                    this.showUnexpectedError(component);
                }
            }
        });        
        $A.enqueueAction(action);            
    }
})