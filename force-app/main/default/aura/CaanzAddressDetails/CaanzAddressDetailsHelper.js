({
	SEARCHABLE_COUNTRY_CODES: ["AU", "NZ"],
	ADDRESS_FIELDS: ["Country", "CountryCode", "State", "StateCode", "Street", "City", "PostalCode"],

	getAddressData: function(cmp) {
		var params = { contactId: cmp.get("v.contactId") };
		this.enqueueAction(cmp, "getAddressData", params, false, function(data){
			cmp.set("v.account", (data = JSON.parse(data)).account);
			cmp.set("v.contact", data.contact);
			this.getCountryAndStateOptions(cmp);
		});
	},
	getCountryAndStateOptions: function(cmp){
		this.enqueueAction(cmp, "getCountryAndStateOptions", {}, true, function(data){
			cmp.set("v.countryStateOptions", JSON.parse(data));
			this.setCountryOptions(cmp, JSON.parse(data));
			this.setAddressData(cmp, true);
			this.setAddressData(cmp, false);
			cmp.set("v.initDone", true);
		});
	},
	saveAddressData: function(cmp, callback) {
		this.setAddressValues(cmp, true);
		this.setAddressValues(cmp, false);
		var params = { 
			addressDataJSONString: JSON.stringify({ 
				account: cmp.get("v.account"), contact: cmp.get("v.contact") 
			}) 
		};
		this.enqueueAction(cmp, "saveAddressData", params, false, function(data){
			cmp.set("v.account", (data = JSON.parse(data)).account);
			cmp.set("v.contact", data.contact);
			if(callback) callback();
		});
	},
	isMailingSameAsResidential: function(cmp){
		return cmp.get("v.account.Mailing_and_Residential_is_the_same__c");
	},
	setMailingAsResidential: function(cmp){
		var contact = cmp.get("v.contact");
		this.ADDRESS_FIELDS.forEach(function(field){
			contact["Mailing" + field] = contact["Other" + field];
		});
		this.setAddressData(cmp, false);
	},
	setCountryOptions: function(cmp, countryStateOptions){
		countryStateOptions.forEach(function(option){
			option.states = option.states || [];
		});
		cmp.set("v.countryOptions", countryStateOptions);
	},
	setAddressData: function(cmp, otherAddress){
		this.setAddressValues(cmp, otherAddress);
		this.setStateOptions(cmp, otherAddress);
		this.setValidateButtonVisibility(cmp, otherAddress);
	},
	setStateOptions: function(cmp, otherAddress){
		var prefix = this.getAddressPrefix(otherAddress);
		var countryOption = this.getCountryOption(cmp, this.getCountryValue(cmp, otherAddress));
		cmp.set("v."+prefix.toLowerCase()+"StateOptions", countryOption ? countryOption.states : []);
	},
	setAddressValues: function(cmp, otherAddress){
		var prefix = this.getAddressPrefix(otherAddress); 
		var countryOption = this.getCountryOption(cmp, this.getCountryValue(cmp, otherAddress));
		var stateOption = this.getStateOption(cmp, otherAddress, this.getStateValue(cmp, otherAddress), countryOption);
		var contact = cmp.get("v.contact");
		contact[prefix+"Country"] = countryOption ? countryOption.label : "";
		contact[prefix+"CountryCode"] = countryOption ? countryOption.value : "";
		contact[prefix+"State"] = stateOption ? stateOption.label : "";
		contact[prefix+"StateCode"] = stateOption ? stateOption.value : "";
		cmp.set("v.contact", contact);
	},
	getAddressPrefix: function(otherAddress){
		return otherAddress ? "Other" : "Mailing";
	},
	getCountryOption: function(cmp, countryValue){
		return cmp.get("v.countryOptions").find(function(option){ 
			return (option.label === countryValue || option.value === countryValue); 
		});
	},
	getStateOption: function(cmp, otherAddress, stateValue, countryOption){
		countryOption = countryOption || this.getCountryOption(cmp, this.getCountryCode(cmp, otherAddress)); if(!countryOption) return;
		return countryOption.states.find(function(option){ 
			return (option.label === stateValue || option.value === stateValue); 
		});
	},
	getCountryValue: function(cmp, otherAddress){
		return cmp.get("v.contact."+this.getAddressPrefix(otherAddress)+"CountryCode");
	},
	getStateValue: function(cmp, otherAddress){
		return cmp.get("v.contact."+this.getAddressPrefix(otherAddress)+"StateCode");
	},
	getCountryCode: function(cmp, otherAddress, countryValue){
		var countryOption = this.getCountryOption(cmp, countryValue || this.getCountryValue(cmp, otherAddress));
		return countryOption ? countryOption.value : "";
	},
	getStateCode: function(cmp, otherAddress, stateValue){
		var stateOption = this.getStateOption(cmp, otherAddress, stateValue || this.getStateValue(cmp, otherAddress));
		return stateOption ? stateOption.value : "";
	},
	setValidateButtonVisibility: function(cmp, otherAddress){
		var visibility = this.SEARCHABLE_COUNTRY_CODES.includes(this.getCountryValue(cmp, otherAddress));
		if(otherAddress) cmp.set("v.showOtherValidateButton", visibility); 
		else cmp.set("v.showMailingValidateButton", !cmp.get("v.account.Mailing_and_Residential_is_the_same__c") && visibility);
	},
    getAddress: function(cmp, otherAddress){
    	var prefix = this.getAddressPrefix(otherAddress); 
    	var contact = cmp.get("v.contact");
		return {
    		street: contact[prefix+"Street"] || "",
            city: contact[prefix+"City"] || "",
            state: contact[prefix+"State"] || "",
            postalCode: contact[prefix+"PostalCode"] || "",
            country: contact[prefix+"Country"] || ""
    	};
    },
    setAddress: function(cmp, otherAddress){
		var params = { addressId: cmp.get("v.selectedAddressId"), countryCode: this.getCountryValue(cmp, otherAddress) };
		this.enqueueAction(cmp, "formatAddress", params, true, function(data){ 
			var address = JSON.parse(data).SUCCESS; 
			var prefix = this.getAddressPrefix(otherAddress); 
			var account = cmp.get("v.account"), contact = cmp.get("v.contact");
			contact[prefix+"Street"] = address.street || "";
			contact[prefix+"City"] = address.city || "";
			contact[prefix+"StateCode"] = address.state || "";
			contact[prefix+"PostalCode"] = address.postcode || "";
			account[(otherAddress ? "Residential" : "Mailing") + "_DPID__c"] = address.dpid || "";
			cmp.set("v.account", account);
			cmp.set("v.contact", contact);
			this.setAddressData(cmp, otherAddress);
			if(this.isMailingSameAsResidential(cmp)) this.setMailingAsResidential(cmp); 
		});
    },
    triggerCallback: function(event){
    	var params = event.getParam ? event.getParam("arguments").params || {} : {};
		if(params.callback) params.callback();
    },
    validateSelectedAddress: function(cmp, event){
    	return !$A.util.isUndefinedOrNull(cmp.get("v.selectedAddressId"))
    },
	showAddressSearch: function(cmp, otherAddress) {
		var configs = [
            ['c:CaanzAddressSearch', {
            	address: this.getAddress(cmp, otherAddress), 
            	countryCode: this.getCountryValue(cmp, otherAddress), 
            	selectedAddressId: cmp.getReference("v.selectedAddressId")
            }],
            ['c:CaanzccModalFooter', {}],
        ];
        var helper = this;
        $A.createComponents(configs, function(components, status) {
            if (status === "SUCCESS") {
                helper.showModal(cmp, components[0], components[1], 
                	otherAddress ? cmp.setOtherAddress : cmp.setMailingAddress);
            }
        });
    },
    showModal: function(cmp, body, footer, successHandler){
		var overlay = cmp.find('overlayLib');
		if(overlay){
			footer.set("v.saveFunction", successHandler);
			var params = {
	            header: "Validate Address", 
	            body: body, footer: footer, 
	            cssClass: "cCaanzAddressDetails",
	            showCloseButton: true
	        };
			overlay.showCustomModal(params);
		}
	},
	enqueueAction: function(cmp, method, params, storable, callback){
		this.toggleSpinner(cmp, true);
		var action = cmp.get("c." + method); 
		if(params) action.setParams(params);
		action.setCallback(this, function(response){
			if(response.getState() === "SUCCESS") {
				if(callback) callback.call(this, response.getReturnValue());
	        } else if(response.getState() === "ERROR") {
	        	cmp.set("v.errorMessage", JSON.stringify(response.getError()));
	        	console.error(response.getError());
	        }
	        this.toggleSpinner(cmp, false);
		});
		if(storable) action.setStorable();
		$A.enqueueAction(action);
	},
	toggleSpinner: function(cmp, show) {
		var spinner = cmp.find("spinner");
		$A.util.addClass(spinner, show ? "slds-show" : "slds-hide");
		$A.util.removeClass(spinner, !show ? "slds-show" : "slds-hide");
	}
})