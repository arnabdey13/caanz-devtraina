({
    handleClick : function(cmp, event, helper) {
        var url = cmp.get("v.url");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({ "url": url });
        urlEvent.fire();
    },
    
    handleLinkedInClick : function(cmp,event,helper){
        helper.fireCompEvent(cmp,event);
        /*if(window.location.pathname == '/MyCA/s/'){
            var appEvent = $A.get("e.c:LCMP_LinkedInApplicationEvent");
            appEvent.fire();
        }*/
   }
    
    
})