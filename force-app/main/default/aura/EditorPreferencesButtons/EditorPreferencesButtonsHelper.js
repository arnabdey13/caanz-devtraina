({
    

    optionalPostLoad : function(component, event){
        var record = component.get("v.properties").RECORD;
        var allOptOut = record.PersonHasOptedOutOfEmail &&
            record.PersonHasOptedOutOfMail__pc &&
            record.PersonDoNotCall &&
            record.PersonHasOptedOutOfSMS__pc;
        if (allOptOut) {
            var valueEvent = $A.get("e.c:EditorSetValueEvent");
            valueEvent.setParams({field: "prop.contactAll", value: true}).fire();
        }
        
        //retrieve URL variable
        URLvar = 'page';
        name = URLvar.replace(/[\[\]]/g, "\\$&");
        var url = window.location.href;
        var domain = window.location.hostname;
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
        var results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';

        //retrieve contents of Bulk_pay__pc
        var bulkpay = record.Bulk_pay__pc;
        
       	//override label and save when part of subs process
        if (decodeURIComponent(results[2].replace(/\+/g, " ")) == 1)  {
            if (bulkpay)
            {
                component.set("v.saveLabel", "Save");
                component.set("v.saveURL", "https://" + domain +"/customer/s/bulk-billed-thank-you" );
            } else {
            	component.set("v.saveLabel", "Save and view invoice");
            	//component.set("v.saveURL", "https://" + domain +"/customer/idp/login?app=0sp90000000TNPR" );
            	component.set("v.saveURL", "../idp/login?app=0sp90000000TNPR" );
            }          
        }
    },
    navigate : function(component, event) {
        var sm = component.find("stateManager");
        $A.log(sm.get("v.getter")("flow"));            
        
        if (component.get("v.saveURL")) {
            //var urlEvent = $A.get("e.force:navigateToURL");
            //urlEvent.setParams({
            //    "url": component.get("v.saveURL"),
            //    "isredirect" :false
            //});
            //urlEvent.fire();
            window.open(component.get("v.saveURL"),'_self');
        }
    },
    optionalValidation : function(component, event) {
        if (!component.get("v.isLoading") && 
            !component.get("v.suppressCustomValidation")) {            
            
            var fields = ["PersonHasOptedOutOfEmail","PersonHasOptedOutOfMail__pc",
                          "PersonDoNotCall","PersonHasOptedOutOfSMS__pc"];
            var fieldSet = {};
            for (var i = 0; i < fields.length; i++) {
                fieldSet[fields[i]] = true;
            }
            var eventField = event.getParam("field");
            var isOneOfTheFourFields = fieldSet[eventField];
            
            if (eventField == "prop.contactAll") {
                // toggle value of the 4 persisted fields in the UI
                var optingOut = event.getParam("value");
                component.set("v.suppressCustomValidation", true);
                for (var i = 0; i < fields.length; i++) {
                    var eventData = {field: fields[i], value: optingOut};
                    $A.get("e.c:EditorSetValueEvent").setParams(eventData).fire();
                }
                component.set("v.suppressCustomValidation", false);
            } else if (isOneOfTheFourFields) {
                
                var newValue = event.getParam("value");
                
                // count all 4 comms fields to see if they match
                var trueCount = 0, falseCount = 0;
                for (var i = 0; i < fields.length; i++) {
                    if (fields[i] == eventField) {
                        if (newValue) { // use the new value of the changing field
							trueCount++;                            
                        } else {
							falseCount++;                            
                        }
                    } else {
                        if (component.get("v.properties")["RECORD"][fields[i]]) { // use the current value of other fields
							trueCount++;                            
                        } else {
							falseCount++;                            
                        }
                    }
                }

                // IMPORTANT, the boolean values of the checkboxes are inverted so the boolean tests below are inverted also
                
                $A.log("newValue:" + newValue + " trueCount:"+trueCount+" falseCount:"+falseCount);
                                
                var eventData;
                if (trueCount == fields.length || falseCount == fields.length) {
                    // if all fields have the same value, adjust the all checkbox
                    eventData = {field: "prop.contactAll", value: event.getParam("value")};                    
                } else if (!newValue && trueCount == fields.length-1) {
                    // if checking one of the fields and the rest are unchecked, unset the all checkbox
                    eventData = {field: "prop.contactAll", value: false};
                }
                
                // only when an adjustment is required, fire the event to change the all checkbox
                if (eventData) {
                    component.set("v.suppressCustomValidation", true);
                    $A.get("e.c:EditorSetValueEvent").setParams(eventData).fire();
                    component.set("v.suppressCustomValidation", false);
                }
            }
        }
    },
    preventsNavigation : function(component, fieldName){
        return true;
    }
    
    
})