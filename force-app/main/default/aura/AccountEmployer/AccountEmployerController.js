({
    fireEvent : function(cmp) {
        console.log('firing acount name fill event');
        var cmpEvent = cmp.getEvent("accEvent");
        var currentAccRec = cmp.get("v.accountrec");
        var accname = cmp.get("v.label");
        
        cmpEvent.setParams({
            "activeAcc": currentAccRec,
            "accname" : accname
        });
        cmpEvent.fire();
    }
})