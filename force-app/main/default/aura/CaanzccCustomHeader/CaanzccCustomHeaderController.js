({
	doInit: function(cmp, event, helper) {
		// hints to ensure labels are preloaded
		// $Label.c.Areas_of_Interest_Help
		var helpText = cmp.get("v.helpText") || "";
		if(helpText.includes("$Label.")){
			cmp.set("v.helpText", $A.get(helpText));
		}
	}
})