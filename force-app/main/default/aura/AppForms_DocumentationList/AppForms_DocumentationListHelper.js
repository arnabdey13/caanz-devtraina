({
    /** populateFileList Method to do the initial (on load) population of the documents in the fileList **/
	populateFileList : function(component, payload, helper) {
		var fileList = component.get("v.fileList");
        
        // only add if has files
        if (payload.files.length) {
            fileList.push(payload);
            this.sortFileList(fileList);
        	component.set("v.fileList", fileList);
        }
        helper.toggleRHS(component, null, helper);
	},
    /** toggleRHS Method to decide whether to show/hide the RHS box in ff_LayoutForBuilder if files exist **/
    toggleRHS : function(component, event, helper) {
      	var fileList = component.get("v.fileList");
      	var appEvent = $A.get("e.c:AppForms_EventShowRHSBox");
        var showRhsBox = fileList.length > 0;
        
        if (appEvent) {
            appEvent.setParams({showRhsBox});
            appEvent.fire();
        } else {
            console.log("no application event registered: " + eventType);
        }  
    },
    /** addToFileList Method to add new files to the existing fileList (replaces existing ones) **/
    addToFileList : function(component, payload) {
        var fileList = component.get("v.fileList");
        var existingIndex = fileList.findIndex((ele) => {
            return ele.label === payload.label;
        });
        
        if (existingIndex >= 0) { // Exists
            fileList[existingIndex] = payload;
        }
        else { // Does not exist
            fileList.push(payload);
        }
        
        this.sortFileList(fileList);
        component.set("v.fileList", fileList);
    },
    /** sortFileList Method to sort files by the sortIndex they have provided **/
    sortFileList : function(fileList) {
        fileList.sort(function(a, b) {
           return a.sortIndex - b.sortIndex; 
        });
    }
})