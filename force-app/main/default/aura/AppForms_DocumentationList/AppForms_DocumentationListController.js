({
    /** handleDocumentChange Method to handle when a document is loaded/added, adds to internal fileList array representation **/
	handleDocumentChange : function(component, event, helper) {
		let params = event.getParams();

        if (params.eventType === "LOAD") {
            helper.populateFileList(component, params.payload, helper);
        }
        else if (params.eventType === "ADD") {
            helper.addToFileList(component, params.payload);
        }
	},
    toggleRHS : function(component, event, helper) {
        helper.toggleRHS(component, event, helper);
    },
    /** toggleAccordion Method to toggle the accordion open/closed (mobile view) **/
    toggleAccordion : function(component, event, helper) {
        var accordionOpen = component.get("v.accordionOpen");
        
        component.set("v.accordionOpen", !accordionOpen);
    },
    /** getSelected Method to show the modal preview of the document **/
    getSelected : function(component,event,helper){
        // display model and set seletedDocumentId attribute with selected record Id   
        component.set("v.hasModalOpen" , true);
        component.set("v.selectedDocumentId" , event.currentTarget.getAttribute("data-Id")); 
    },
    /** closeModel Method to close the file preview modal **/
    closeModel: function(component, event, helper) {
        // for Close Model, set the "hasModalOpen" attribute to "FALSE"  
        component.set("v.hasModalOpen", false);
        component.set("v.selectedDocumentId" , null); 
    },
    
})