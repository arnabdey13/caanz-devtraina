({
	setSearchTerm: function(cmp){
		var address = cmp.get("v.address");
		//if(address) cmp.set("v.searchTerm", address.street+" "+address.city+" "+address.state+" "+address.postalCode);
	},
	doSearch: function(cmp){
        if(!($A.util.isUndefinedOrNull(cmp.get("v.searchTerm")) || 
            $A.util.isEmpty(cmp.get("v.searchTerm")))){
            var params = {
                searchTerm: cmp.get("v.searchTerm"),
                countryCode: cmp.get("v.countryCode"),
                searchLimit: cmp.get("v.searchLimit")
            };
		this.enqueueAction(cmp, "searchAddress", params, function(data){
			var addressOptions = (JSON.parse(data).SUCCESS || []).map(function(result) { 
				return { label: result.label1 + " " + result.label2, value: result.id };
			});
			cmp.set("v.addressOptions", addressOptions);
		});
        }else{
            cmp.set("v.addressOptions", {});
            cmp.set("v.changeitClass", "slds-hide");
        }
	},
	enqueueAction: function(cmp, method, params, callback){
		this.toggleSpinner(cmp, true);
		var action = cmp.get("c." + method); 
		if(params) action.setParams(params);
		action.setCallback(this, function(response){
			if(response.getState() === "SUCCESS") {
                if($A.util.isUndefinedOrNull(JSON.parse(response.getReturnValue()).SUCCESS) ||
                   $A.util.isEmpty(JSON.parse(response.getReturnValue()).SUCCESS) &&
                   (JSON.parse(response.getReturnValue()).SUCCESS.length==0)){ 
               		cmp.set("v.changeitClass", "slds-hide");
                }else{
                   if(callback) callback.call(this, response.getReturnValue()); 
                    cmp.set("v.changeitClass", "slds-show");
                }
				
	        } else if(response.getState() === "ERROR") {
	        	console.error(response.getError());
                cmp.set("v.changeitClass", "slds-hide");
	        }
	        this.toggleSpinner(cmp, false);
		});
		$A.enqueueAction(action);
	},
	toggleSpinner: function(cmp, show) {
		var spinner = cmp.find("spinner");
		$A.util.addClass(spinner, show ? "slds-show" : "slds-hide");
		$A.util.removeClass(spinner, !show ? "slds-show" : "slds-hide");
	}
})