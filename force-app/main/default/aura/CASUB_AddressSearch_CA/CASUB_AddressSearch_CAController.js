({
	doInit: function(cmp, event, helper) {
         
		helper.setSearchTerm(cmp);
		helper.doSearch(cmp);
        cmp.set("v.changeitClass", "slds-hide");
	},
	doSearch: function(cmp, event, helper){
		helper.doSearch(cmp);
	},
    showSearchDiv:function(component){
        component.set("v.changeitClass", "slds-show");
    },
    hideSearchDiv:function(component){
        component.set("v.changeitClass", "slds-hide");
    },
    parentHandling : function(cmp, event) {
        var currentAddressOption = event.getParam("currentAddressOption");
        //cmp.set("v.address", currentAddressOption);
        var addressId = event.getParam("selectedAddressId");
        cmp.set("v.selectedAddressId", addressId);
        cmp.set("v.changeitClass", "slds-hide");
        cmp.set("v.searchTerm",currentAddressOption);
    },
})