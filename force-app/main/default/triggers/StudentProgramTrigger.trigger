/***********************************************************************************************************************************************************************
Name: StudentProgramTrigger
============================================================================================================================= 
Purpose: Single Trigger for LuanaSMS__Student_Program__c sObject. 
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR                 DATE          DETAIL     Description 
1.0         Vinay               20/02/2019    Created    Trigger for LuanaSMS__Student_Program__c sObject
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/
trigger StudentProgramTrigger on LuanaSMS__Student_Program__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    //Get the Trigger Setting for the LuanaSMS__Student_Program__c Object from Custom Meta Data.
    Trigger_Settings_Configuration__mdt trSettConf = ApexUtil.getTriggSettConf('LuanaSMS__Student_Program__c');
    
    //Exit Trigger Execution if Configuration is to Mute or Custom Permission is Assinged to the current User.
    if((trSettConf != null && trSettConf.Trigger_Mute__c) || ApexUtil.getIfUserHasCustPerm(Label.CustomPermissionMuteTriggerProcesses))
        return;
    
    //Call trigger Handler Class with Max Loop Count Set from Configuration. 
    StudentProgramTriggerHandler  studProgHandler = new StudentProgramTriggerHandler();   
    
    if(trSettConf != null && trSettConf.Trigger_Loop_Count__c != null)
        studProgHandler.setMaxLoopCount(Integer.valueOf(trSettConf.Trigger_Loop_Count__c));     
    
    try{studProgHandler.run();}catch(exception e){ }
}