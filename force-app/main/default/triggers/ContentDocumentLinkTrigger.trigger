trigger ContentDocumentLinkTrigger on ContentDocumentLink (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    //Get the Trigger Setting for the ContentDocument Object from Custom Meta Data.
    Trigger_Settings_Configuration__mdt trSettConf = ApexUtil.getTriggSettConf('ContentDocumentLink');
    
    //Exit Trigger Execution if Configuration is to Mute or Custom Permission is Assinged to the current User.
    if((trSettConf != null && trSettConf.Trigger_Mute__c) || ApexUtil.getIfUserHasCustPerm(Label.CustomPermissionMuteTriggerProcesses))
        return;    
    
    //Call trigger Handler Class with Max Loop Count Set from Configuration. 
    ContentDocumentLinkTriggerHandler  cntDocLinkHandler = new ContentDocumentLinkTriggerHandler();   
    
    if(trSettConf != null && trSettConf.Trigger_Loop_Count__c != null)
        cntDocLinkHandler.setMaxLoopCount(Integer.valueOf(trSettConf.Trigger_Loop_Count__c));     
    
    try{cntDocLinkHandler.run();}catch(exception e){ }
}