trigger luana_ProgramDeleteLog on LuanaSMS__Program__c (before delete) {
    
    for(LuanaSMS__Program__c p : trigger.old) {
        luana_AuditLog.insertLog('Info', JSON.serializePretty(p), 'LuanaSMS__Program__c', 'Completed', 'Record Deletion');
    }
}