trigger FP_SPSCompetencyArea on LuanaSMS__Student_Program_Subject__c (after insert, after update) {
    List<Id> spsID = new List<Id>();
    for(LuanaSMS__Student_Program_Subject__c a : [select id from LuanaSMS__Student_Program_Subject__c where Id in:trigger.new]){
        spsID.add(a.id);
    }
    
    FP_SPSCompetencyAreaHandler.createGainedCompetencyArea(spsID);
}