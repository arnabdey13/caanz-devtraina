/**
 * @author akopec - CAANZ internal project	
 *
 *  Trigger on Employment History Change -> updates Segmentation objects.
 *    
 *  Does not run during Tests or when SegmentationMembersTriggerHandlerTest.forceTriggerDuringTest == false
 * 
 **/ 
trigger SegmentationEmploymentHistoryChange on Employment_History__c (after insert, after update) {
    if(Trigger.isAfter){ 

		if (Test.isRunningTest() && SegmentationMembersTriggerHandlerTest.forceTriggerDuringTest == false) {
			System.debug('THIS IS TEST RUN - DO NOT PROCESS');
            return;
        }  
        Map <Id, String> MembersSegmentsToUpdateIds = SegmentationMembersTriggerHandler.handleSegmentsAfterEHUpdate(Trigger.new, Trigger.newMap, Trigger.old, Trigger.oldMap);  
        
		// async method future call
        if (MembersSegmentsToUpdateIds.size() >0 ){
			SegmentationMembersTriggerHandler.handleSegmentsAfterUpdateAsync(MembersSegmentsToUpdateIds);
        } 
    } 
}