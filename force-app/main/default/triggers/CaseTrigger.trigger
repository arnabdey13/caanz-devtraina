trigger CaseTrigger on Case (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    if((trigger.isAfter && trigger.isInsert) || (trigger.isAfter && trigger.isUpdate)){
    // Only get relevant case (recordtype = Request Competency Area)
    List<Case> caseList = [SELECT Id, ContactId, FP_Requested_Competency_Area__c, Resolution__c FROM Case WHERE Id in: trigger.new AND RecordType.Name = :luana_RequestConstants.REQ_COMPETENCY_RT_NAME AND Resolution__c = 'Approved'];
    if(caseList.size() > 0)
    FP_CaseHandler.upsertGainedCompetencyArea(caseList);
    }
    
    //Get the Trigger Setting for the case Object from Custom Meta Data.
    Trigger_Settings_Configuration__mdt trSettConf = ApexUtil.getTriggSettConf('Case');
    
    //Exit Trigger Execution if Configuration is to Mute or Custom Permission is Assinged to the current User.
    if((trSettConf != null && trSettConf.Trigger_Mute__c) || ApexUtil.getIfUserHasCustPerm(Label.CustomPermissionMuteTriggerProcesses))
        return; 
    
    //Call trigger Handler Class with Max Loop Count Set from Configuration. 
    CaseTriggerHandler caseHandler = new CaseTriggerHandler();  
    
    if(trSettConf != null && trSettConf.Trigger_Loop_Count__c != null)
        caseHandler.setMaxLoopCount(Integer.valueOf(trSettConf.Trigger_Loop_Count__c));
    
    try{        
        caseHandler.run();        
    }catch(exception e){
        system.debug('exception Message-->'+e.getMessage());
        system.debug('exception stack trace-->'+e.getStackTraceString());
    }
}