/*
* Trigger for QPR Referral 
*/

trigger QPRReferralTrigger on QPR_Referral__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
 //Get the Trigger Setting for the Account Object from Custom Meta Data.
    Trigger_Settings_Configuration__mdt trSettConf = ApexUtil.getTriggSettConf('QPR_Referral__c');
    
  //Exit Trigger Execution if Configuration is to Mute or Custom Permission is Assinged to the current User.
    if((trSettConf != null && trSettConf.Trigger_Mute__c) || ApexUtil.getIfUserHasCustPerm(Label.CustomPermissionMuteTriggerProcesses))
        return; 
    
    //Call trigger Handler Class with Max Loop Count Set from Configuration. 
   
    QPRReferralTriggerHandler handler = new QPRReferralTriggerHandler();
       if(trSettConf != null && trSettConf.Trigger_Loop_Count__c != null)
        handler.setMaxLoopCount(Integer.valueOf(trSettConf.Trigger_Loop_Count__c));
    
    try{        
        handler.run();        
    }catch(exception e){
        system.debug('exception Message-->'+e.getMessage());
        system.debug('exception stack trace-->'+e.getStackTraceString());
    }
    
    	
}