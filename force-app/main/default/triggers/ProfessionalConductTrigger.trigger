trigger ProfessionalConductTrigger on Conduct_Support__c (after insert, after update, after delete, after undelete) {
    
    List<Conduct_Support__c> PC_List = (trigger.isDelete) ? trigger.old : trigger.new;
    Set<Account> AccountsToUpdate_Set = new Set<Account>();
    for(Integer i = 0; i < PC_List.size(); i++){
        
        if(PC_List[i].Member__c == null){
            continue;
        }
        
        // Added check on record type to determine whether to process an AU or NZ record
        // The IF part of if statement processes AU, the ELSE processes NZ
        if(trigger.isInsert || trigger.isDelete || trigger.isUnDelete ||
          (trigger.isUpdate && (           
           PC_List[i].Member__c != trigger.old[i].Member__c ||
           PC_List[i].Country__c != trigger.old[i].Country__c ||
           PC_List[i].Overall_Status__c != trigger.old[i].Overall_Status__c ||
           PC_List[i].Complaint__c != trigger.old[i].Complaint__c ||
           PC_List[i].Outcome__c != trigger.old[i].Outcome__c || PC_List[i].Record_Type__c == 'AU'))){
               
               if(PC_List[i].Member__c != null){
                   Account AccountToUpdate = new Account(Id=PC_List[i].Member__c);
                   AccountToUpdate.RollUpFromProfessionalConductAndFineCost__c = true;
                   AccountsToUpdate_Set.add(AccountToUpdate);
               }                  
           }
           else
           {
           
                   // Business rules state that if the complaint box is ticked and status is
                   // anything but Closed then the flag should be shown otherwise turn it off
                   if(PC_List[i].Complaint__c == true && PC_List[i].Status__c != 'Closed')
                   {
                        Account AccountToUpdate = new Account(Id=PC_List[i].Member__c);
                        AccountToUpdate.Professional_Conduct_Flag_NZ_Checkbox__c = true;
                        AccountsToUpdate_Set.add(AccountToUpdate);
                   } 
                   else
                   {
                        Account AccountToUpdate = new Account(Id=PC_List[i].Member__c);
                        AccountToUpdate.Professional_Conduct_Flag_NZ_Checkbox__c = false;
                        AccountsToUpdate_Set.add(AccountToUpdate);
                   }
           }
       
           
           
        }    
    update new List<Account> (AccountsToUpdate_Set);       
}