/*
* Trigger for QPR Survey Related Contact
*/
trigger QPRSurveyRelatedContactTrigger on QPR_Survey_Related_Contact__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    //Get the Trigger Setting for the Account Object from Custom Meta Data.
    Trigger_Settings_Configuration__mdt trSettConf = ApexUtil.getTriggSettConf('QPR_Survey_Related_Contact__c');
    
  //Exit Trigger Execution if Configuration is to Mute or Custom Permission is Assinged to the current User.
    if((trSettConf != null && trSettConf.Trigger_Mute__c) || ApexUtil.getIfUserHasCustPerm(Label.CustomPermissionMuteTriggerProcesses))
        return; 
    
    //Call trigger Handler Class with Max Loop Count Set from Configuration. 
   
    QPRSurveyRelatedContactTriggerHandler handler = new QPRSurveyRelatedContactTriggerHandler();
       if(trSettConf != null && trSettConf.Trigger_Loop_Count__c != null)
        handler.setMaxLoopCount(Integer.valueOf(trSettConf.Trigger_Loop_Count__c));
    
    try{        
        handler.run();        
    }catch(exception e){
        system.debug('exception Message-->'+e.getMessage());
        system.debug('exception stack trace-->'+e.getStackTraceString());
    }
 

}