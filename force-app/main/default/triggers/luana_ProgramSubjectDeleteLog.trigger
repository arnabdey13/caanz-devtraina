trigger luana_ProgramSubjectDeleteLog on LuanaSMS__Program_Subject__c (before delete) {
    for(LuanaSMS__Program_Subject__c p : trigger.old) {
        luana_AuditLog.insertLog('Info', JSON.serializePretty(p), 'LuanaSMS__Program_Subject__c', 'Completed', 'Record Deletion');
    }
}