/**
 * @author akopec - CAANZ internal project  
 *
 *  Trigger on Account Address Change -> updates Segmentation and Account objects.
 *  The following Account fields are updated: Longitude, Latitude, NZ  Territorial Authority and Affiliated Branch
 *  It makes a callout to Experian SOAP API using Queueable interface.
 *  
 *  Trigger on Account Membership Details Change -> updates Segmentation objects.  
 *  Does not run during Tests or when SegmentationAddressTriggerHandlerTest.forceTriggerDuringTest == false
 * 
 **/ 
trigger SegmentationAccountChange on Account (after insert, after update) {

    if(Trigger.isAfter){ 
        // skip the trigger during tests, unless this handler Test
        if (Test.isRunningTest() && SegmentationAddressTriggerHandlerTest.forceTriggerDuringTest == false && SegmentationMembersTriggerHandlerTest.forceTriggerDuringTest == false) {
            System.debug('THIS IS TEST RUN - DO NOT PROCESS');
            return;     
        }        
        // New Version
        Map <Id, String> AddressSegmentsToUpdateIds = SegmentationAddressTriggerHandler.handleSegmentsAfterUpdate(Trigger.new, Trigger.newMap, Trigger.old, Trigger.oldMap); 
        Map <Id, String> MembersSegmentsToUpdateIds = SegmentationMembersTriggerHandler.handleSegmentsAfterAccountUpdate(Trigger.new, Trigger.newMap, Trigger.old, Trigger.oldMap); 
        //System.debug('Trigger - Members Segments ' + MembersSegmentsToUpdateIds);

        // Changed async method
       if (AddressSegmentsToUpdateIds.size() >0 ){
                SegmentationAddressTriggerUpdate segUpdateJob = new SegmentationAddressTriggerUpdate(AddressSegmentsToUpdateIds);
                ID jobID = System.enqueueJob(segUpdateJob);
                //System.debug('LongLatUpdateJob Id:' + jobID); 
        } 
        if (MembersSegmentsToUpdateIds.size() >0 ){
            SegmentationMembersTriggerHandler.handleSegmentsAfterUpdateAsync(MembersSegmentsToUpdateIds); 
        } 
    }
}