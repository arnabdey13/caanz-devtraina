/**
* @author WDCi-LKoh
* @date 11/06/2019
* @group Competency Automation
* @description Trigger for FT_Student_Paper__c
* @change-history
*/
trigger wdci_StudentPaperTrigger on FT_Student_Paper__c (after insert, after delete) {

    wdci_StudentPaperHandler.initFlow(Trigger.new, Trigger.old, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete, Trigger.isUndelete, Trigger.isBefore, Trigger.isAfter);
}