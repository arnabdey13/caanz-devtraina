trigger OrderTrigger on Order (after insert, after update) {
    Set<Id> setApplicationIds = new Set<Id>();
    for(Order o:trigger.new) {
        if(o.Application__c != null && o.Status == 'Activated' && o.NetSuite_Payment_Date__c != null) 
            setApplicationIds.add(o.Application__c);
    }
    
    if(setApplicationIds.size()>0) {
        List<Application__c> applications = new List<Application__c>();
        for(Id idApplication:setApplicationIds) {
            Application__c app = new Application__c(Id = idApplication, Application_Fee_Paid__c = true, 
                                                    Application_Fee_Paid1__c = 'Paid');
            applications.add(app);
        }
        if(applications.size()>0)
            Database.update(applications, false);
    }
    
    /**
*  This code is added as part of SUBS 2019 implementation
*/ 
    if(Trigger.isUpdate){
        CASUB_Order_Trigger_Helper_CA.OnAfterOrderPaymentUpdate(trigger.New, trigger.Old, Trigger.NewMap, Trigger.OldMap);
    }
    /**
*  This code is added as part of SUBS 2019 implementation
*/ 
    if(Trigger.isInsert && Trigger.isAfter){
        CASUB_Order_Trigger_Helper_CA.OnAfterOrderInsertUpdate(trigger.New, trigger.Old, Trigger.NewMap, Trigger.OldMap);
    }
    
    
}