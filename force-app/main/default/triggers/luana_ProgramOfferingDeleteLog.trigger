trigger luana_ProgramOfferingDeleteLog on LuanaSMS__Program_Offering__c (before delete) {
    for(LuanaSMS__Program_Offering__c p : trigger.old) {
        luana_AuditLog.insertLog('Info', JSON.serializePretty(p), 'LuanaSMS__Program_Offering__c', 'Completed', 'Record Deletion');
    }
}