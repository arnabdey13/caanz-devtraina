trigger ReviewTrigger on Review__c (after insert, after update) {

    ReviewTriggerHandler handler = new ReviewTriggerHandler();
    //After insert
    If (Trigger.isInsert && Trigger.isAfter) {
    {
        handler.onAfterInsert(Trigger.new);
    }
    }

    else if (Trigger.isUpdate && Trigger.isAfter) {
  //  if(checkRecursive.runOnce())
//{
        handler.onAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
  //  }
}	
}