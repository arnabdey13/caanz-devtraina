trigger luana_CourseDeleteLog on LuanaSMS__Course__c (before delete) {
    for(LuanaSMS__Course__c p : trigger.old) {
        luana_AuditLog.insertLog('Info', JSON.serializePretty(p), 'LuanaSMS__Course__c', 'Completed', 'Record Deletion');
    }
}