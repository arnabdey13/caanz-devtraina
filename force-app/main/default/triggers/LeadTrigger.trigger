trigger LeadTrigger on Lead (after insert) {
	if(trigger.isInsert){
		if(trigger.isBefore){
			
		}
		else{
			Set<String> LeadName_Set = new Set<String>();
			Set<String> LeadEmail_Set = new Set<String>();
			List<Lead> LeadCreatedByIntegrationUser_List = new List<Lead>();
			for(Lead LeadObj : Trigger.new){
				if(UserInfo.getProfileId()==ProfileCache.getId('Integration Profile')
				//|| LeadObj.CreatedById=='005900000024aY1'
				){
					LeadName_Set.add( LeadObj.FirstName+' '+LeadObj.LastName );
					LeadEmail_Set.add( LeadObj.Email );
					LeadCreatedByIntegrationUser_List.add( LeadObj );
				}
			}
			
			if(LeadName_Set.size()>0){
				List<Account> Account_List = [Select a.Name, a.PersonEmail, a.IsCustomerPortal 
					From Account a where a.IsPersonAccount=true and 
						a.Name IN :LeadName_Set and a.PersonEmail IN :LeadEmail_Set];
				
				Set<String> AccountNameAndEmail_Set = new Set<String>();
				for(Account acc : Account_List){
					AccountNameAndEmail_Set.add( acc.Name + acc.PersonEmail );
				}
				
				for(Lead LeadObj : LeadCreatedByIntegrationUser_List){
					if( !AccountNameAndEmail_Set.contains( LeadObj.FirstName+' '+LeadObj.LastName + LeadObj.Email) ){
						if(LeadObj.isConverted==false){ //to prevent recursion
							Database.LeadConvert lc = new Database.LeadConvert();
							lc.setLeadId( LeadObj.Id );
							
							lc.setDoNotCreateOpportunity(true);
							// List<LeadStatus> LeadStatus_List = [Select l.IsConverted From LeadStatus l limit 1];
							lc.setConvertedStatus( 'Qualified' );
							
							Database.LeadConvertResult lcr = Database.convertLead(lc);
							//System.assert(lcr.isSuccess());
						}
					}
					else{
						System.debug('Duplicate');
					}
				}
			}
		}
	}
	else{
		
	}
}