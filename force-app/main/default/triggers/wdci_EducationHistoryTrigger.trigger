/**
* @author WDCi-LKoh
* @date 11/06/2019
* @group Competency Automation
* @description Trigger for Application__c
* @change-history
*/
trigger wdci_EducationHistoryTrigger on edu_Education_History__c (before delete) {
    wdci_EducationHistoryHandler.initFlow(Trigger.new, Trigger.old, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete, Trigger.isUndelete, Trigger.isBefore, Trigger.isAfter);    
}