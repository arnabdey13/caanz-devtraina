/**
 * Created by Admin on 6/04/2016.
 */

trigger ReviewTeamTrigger on Review_Team__c (after update, before update) {

    ReviewTeamTriggerHandler handler = new ReviewTeamTriggerHandler();
    //After insert
    /*
    If (Trigger.isInsert && Trigger.isAfter) {
        handler.onAfterInsert(Trigger.new);
    }
    */
    // Before Update

    if (Trigger.isUpdate && Trigger.isBefore) {
        handler.onBeforeUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
    }

    // After Update
    else if (Trigger.isUpdate && Trigger.isAfter) {
        handler.onAfterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
    }
    // Before Insert
    /*
    else if(Trigger.isInsert && Trigger.isBefore){
        handler.OnBeforeInsert(Trigger.new);
    }
    */

}