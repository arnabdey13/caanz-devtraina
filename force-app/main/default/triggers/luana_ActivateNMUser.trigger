trigger luana_ActivateNMUser on Account (after insert, after update) {
    List<Account> accList = [SELECT Id, IsPersonAccount, Assessible_for_CAF_Program__c, PersonContactId FROM Account WHERE Id IN: Trigger.new AND Assessible_for_CAF_Program__c=true];
    List<Id> conIdList = new List<Id>();
    // AccId, ConId
    Map<Id, Id> accConMap = new Map<Id, Id>();
    
    for(Account acc : accList) {
        conIdList.add(acc.PersonContactId);
        accConMap.put(acc.PersonContactId, acc.Id);
    }
        
    List<User> userList = [SELECT Id, isActive, Username, Email, ContactId FROM User WHERE ContactID IN: conIdList];
    // AccId, User
    Map<Id, User> accUserMap = new Map<Id, User>();
    List<Id> userIdList = new List<Id>();
        
    for(User user : userList) {
        if(accConMap.containsKey(user.ContactId)) {        
            accUserMap.put(accConMap.get(user.ContactId), user);
        }
        userIdList.add(user.Id);   
    }
    
    
    PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name='Luana_NonMemberCommunityPermission'];
    List<PermissionSetAssignment> permSetAssList = [SELECT Id, AssigneeId, PermissionSetId FROM PermissionSetAssignment WHERE PermissionSetId =: ps.Id AND AssigneeId IN: userIdList];
    // UserId, PermSetId
    Map<Id, Id> userPermMap = new Map<Id, Id>();
        
    for(PermissionSetAssignment psa : permSetAssList) {
        userPermMap.put(psa.AssigneeId, psa.PermissionSetId);
    }
        
    List<User> userUpdateList = new List<User>();
    List<PermissionSetAssignment> permSetAssInsertList = new List<PermissionSetAssignment>();
    //UserId, PSId
    Map<Id, Id> userPermSetMap = new Map<Id, Id>();    
        
    for(Account acc : Trigger.new) {
        if(acc.IsPersonAccount && acc.Assessible_for_CAF_Program__c) {
            // Check if Account has Community User
            if(accUserMap.containsKey(acc.Id)) {
                userPermSetMap.put(accUserMap.get(acc.Id).Id, ps.Id);
            } else
                acc.addError('You can\'t enrol this Person in CA Foundations. Please ensure that the Community User is created.');
        }
    }
    luana_NMUserAndPermSetUtil.runFutureUpdate(userPermSetMap, userPermMap);
}