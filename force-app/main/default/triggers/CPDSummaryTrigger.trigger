/*------------------------------------------------------------------------------------
Author:        Sumit Gupta
Company:       Salesforce
Description:   cpd account trigger

History
Date            Author             Comments
--------------------------------------------------------------------------------------
05-05-2018     Sumit Gupta       	Initial Release
------------------------------------------------------------------------------------*/
trigger CPDSummaryTrigger on CPD_Summary__c (before insert, before update) {

	//Set summary specialisations
	if(Trigger.isBefore && Trigger.isInsert){
		CPDSummaryHandler.setSummarySpecialisations(Trigger.new);
	}

	//Update summary specialisation hours
	if(Trigger.isBefore && Trigger.isUpdate){
		List<CPD_Summary__c> updatableSummaries = new List<CPD_Summary__c>();
		for(CPD_Summary__c summary : Trigger.new){
			//Check if summary specialisations changed
			if(CPDSummaryHandler.isSummarySpecialisationsChanged(summary, Trigger.oldMap.get(summary.Id))){
				updatableSummaries.add(summary);
			}
		}
		if(!updatableSummaries.isEmpty()){
			CPDSummaryHandler.updateSummarySpecialisationHours(updatableSummaries);
		}
	}
}