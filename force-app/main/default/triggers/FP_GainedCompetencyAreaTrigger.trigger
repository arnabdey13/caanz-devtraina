trigger FP_GainedCompetencyAreaTrigger on FP_Gained_Competency_Area__c (after insert, after update) {
    List<FP_Gained_Competency_Area__c> gcaList = [SELECT Id, FP_Contact__c, FP_Competency_Area__c, FP_Competency_Area__r.FP_Parent_Competency_Area__c FROM FP_Gained_Competency_Area__c WHERE Id in: trigger.new AND FP_Competency_Area__r.FP_Parent_Competency_Area__c != null];
    FP_GainedCompetencyAreaHandler.calcCADependency(gcaList);
}