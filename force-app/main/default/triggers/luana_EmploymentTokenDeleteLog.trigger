trigger luana_EmploymentTokenDeleteLog on Employment_Token__c (before delete) {
    for(Employment_Token__c p : trigger.old) {
        luana_AuditLog.insertLog('Info', JSON.serializePretty(p), 'Employment_Token__c', 'Completed', 'Record Deletion');
    }
}