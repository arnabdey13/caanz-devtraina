/**
    Developer: WDCi (Lean)
    Development Date: 18/05/2016
    Task #: Controller for luana_MyVirtualClassNew vf page LCA-499
**/

trigger luana_AttendanceAction on LuanaSMS__Attendance2__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
    luana_AttendanceHandler.initiateFlow(Trigger.new, Trigger.newMap, Trigger.old, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete, Trigger.isUndelete, Trigger.isBefore, Trigger.isAfter);
}