/**
    Developer: WDCi (KH)
    Development Date: 12/12/2016
    Reason : Used to check if the AUS status is change to Ready to process, must have attachment
**/
trigger AAS_ADUAttachmentFileCheck on AAS_Assessment_Data_Upload__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
    AAS_AssessmentDataUploadHandler.initiateFlow(Trigger.new, Trigger.newMap, Trigger.old, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete, Trigger.isUndelete, Trigger.isBefore, Trigger.isAfter);
}