trigger ProfessionalSupportTrigger on Professional_Support__c (after insert, after update) {
	if(trigger.isBefore){
		
	}
	else{
		if(trigger.isUpdate){
			TriggerConfigHandler.updatesBasedOnTriggerConfig();
		}
		else{
			// insert only
		}
		Map<Id,Id> CaseIdToProfessionalSupportOwnerId_Map = new Map<Id,Id>();
		for(Integer i=0; i<trigger.new.size(); i++){
			if(
			trigger.isInsert ||
			(trigger.isUpdate && trigger.new[i].OwnerId!=trigger.old[i].OwnerId)
			){
				CaseIdToProfessionalSupportOwnerId_Map.put(trigger.new[i].Case__c, trigger.new[i].OwnerId);
			}
		}
		if( CaseIdToProfessionalSupportOwnerId_Map.size()>0 ){
			List<Case> CaseToUpdate_List = new List<Case>();
			for(Id CaseId : CaseIdToProfessionalSupportOwnerId_Map.keySet() ){
				Case CaseToUpdate = new Case(Id=CaseId);
				CaseToUpdate.OwnerId = CaseIdToProfessionalSupportOwnerId_Map.get(CaseId);
				CaseToUpdate_List.add( CaseToUpdate );
			}
			update CaseToUpdate_List;
		}
	}
}