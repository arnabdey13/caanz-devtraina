trigger luana_CourseDLDeleteLog on Course_Delivery_Location__c (before delete) {
    for(Course_Delivery_Location__c p : trigger.old) {
        luana_AuditLog.insertLog('Info', JSON.serializePretty(p), 'Course_Delivery_Location__c', 'Completed', 'Record Deletion');
    }
}