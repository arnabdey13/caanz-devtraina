/***********************************************************************************************************************************************************************
Name: StudentProgramSubjectTrigger
============================================================================================================================= 
Purpose: Single Trigger for LuanaSMS__Student_Program_Subject__c sObject. 
============================================================================================================================== 
History 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
VERSION    AUTHOR                 DATE          DETAIL     Description 
1.0         Vinay               26/02/2019    Created    Trigger for LuanaSMS__Student_Program_Subject__c sObject
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
***********************************************************************************************************************************************************************/
trigger StudentProgramSubjectTrigger on LuanaSMS__Student_Program_Subject__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    //Get the Trigger Setting for the LuanaSMS__Student_Program__c Object from Custom Meta Data.
    Trigger_Settings_Configuration__mdt trSettConf = ApexUtil.getTriggSettConf('LuanaSMS__Student_Program_Subject__c');
    
    //Exit Trigger Execution if Configuration is to Mute or Custom Permission is Assinged to the current User.
    if((trSettConf != null && trSettConf.Trigger_Mute__c) || ApexUtil.getIfUserHasCustPerm(Label.CustomPermissionMuteTriggerProcesses))
        return;
    
    //Call trigger Handler Class with Max Loop Count Set from Configuration. 
    StudentProgramSubjectTriggerHandler  studProgSubjectHandler = new StudentProgramSubjectTriggerHandler();   
    
    if(trSettConf != null && trSettConf.Trigger_Loop_Count__c != null)
        studProgSubjectHandler.setMaxLoopCount(Integer.valueOf(trSettConf.Trigger_Loop_Count__c));     
    
    try{studProgSubjectHandler.run();}catch(exception e){}
}