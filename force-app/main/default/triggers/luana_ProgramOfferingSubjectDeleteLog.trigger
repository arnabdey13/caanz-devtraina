trigger luana_ProgramOfferingSubjectDeleteLog on LuanaSMS__Program_Offering_Subject__c (before delete) {
    for(LuanaSMS__Program_Offering_Subject__c p : trigger.old) {
        luana_AuditLog.insertLog('Info', JSON.serializePretty(p), 'LuanaSMS__Program_Offering_Subject__c', 'Completed', 'Record Deletion');
    }
}