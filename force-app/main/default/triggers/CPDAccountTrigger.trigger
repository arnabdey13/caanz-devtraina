/*------------------------------------------------------------------------------------
Author:        Sumit Gupta
Company:       Salesforce
Description:   cpd account trigger

History
Date            Author             Comments
--------------------------------------------------------------------------------------
02-05-2018     Sumit Gupta          Initial Release
------------------------------------------------------------------------------------*/
trigger CPDAccountTrigger on Account (before insert, after insert, before update, after update) {

    //Filter accounts where type is member and has membership approval date
    List<Account> accounts = CPDSummaryHandler.filterSummaryAccounts(Trigger.new);

    //Check if there are no records to process and return
    if(accounts.isEmpty()) return;

    //Set account specialisations
    if(Trigger.isBefore && Trigger.isInsert){
        CPDSummaryHandler.setAccontSpecialisations(accounts);
    }

    //Create account summary records
    if(Trigger.isAfter && Trigger.isInsert){
        CPDSummaryHandler.createAccountSummaryRecords(accounts);
    }

    //Update account specialisations
    if(Trigger.isBefore && Trigger.isUpdate){
        for(Account acc : accounts){
            CPDSummaryHandler.setAccontSpecialisations(Trigger.new);
        }
    }

    //Create missing account summary records and update summary specialisations
    if(Trigger.isAfter && Trigger.isUpdate){
        //Update summary specialisations
        List<Account> updatableSummaryAccounts = new List<Account>();
        for(Account acc : accounts){
            //Check if account specialisations changed
            if(CPDSummaryHandler.isAccountSpecialisationsChanged(acc, Trigger.oldMap.get(acc.Id))){
                updatableSummaryAccounts.add(acc);
            }
        }
        if(!updatableSummaryAccounts.isEmpty()){
            CPDSummaryHandler.updateSummarySpecialisations(updatableSummaryAccounts);
        }
        //Create missing account summary records
        CPDSummaryHandler.createMissingCPDSummaryRecords(accounts);

    }
}