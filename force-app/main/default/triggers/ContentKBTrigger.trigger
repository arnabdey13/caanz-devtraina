/*------------------------------------------------------------------------------------
Author:        Sumit Gupta
Company:       Salesforce
Description:   Content kb trigger

History
Date            Author             Comments
--------------------------------------------------------------------------------------
04-06-2018     Sumit Gupta          Initial Release
------------------------------------------------------------------------------------*/
trigger ContentKBTrigger on Content_KB__c (before insert, before update) {

    if(Trigger.isBefore && Trigger.isInsert){
        //ContentKBTriggerHandler.createArticles(Trigger.new);
    }

    if(Trigger.isBefore && Trigger.isUpdate){
        //List<Content_KB__c> contents = ContentKBTriggerHandler.getUpdatableArticleContents(Trigger.new, Trigger.oldMap);
        //if(!contents.isEmpty()) ContentKBTriggerHandler.updateArticles(contents); 
    }
}