trigger GenerateCAProgramSUOS on LuanaSMS__Student_Program_Subject__c (before insert) {

    // Get related SP Set
    Set<Id> spSet = new Set<Id>();
    for(LuanaSMS__Student_Program_Subject__c sps : trigger.new) {
        spSet.add(sps.LuanaSMS__Student_Program__c);
    }

    List<LuanaSMS__Student_Program__c> relSPList = [SELECT Id, LuanaSMS__Course__r.LuanaSMS__Delivery_Location__c FROM LuanaSMS__Student_Program__c WHERE LuanaSMS__Course__r.LuanaSMS__Program_Offering__r.Product_Type__c='CA Program' AND Id IN: spSet];

    // Cache SPId with Course Delivery Location Id
    Map<Id, Id> spDLMap = new Map<Id, Id>();
    for(LuanaSMS__Student_Program__c sp : relSPList) {
        spDLMap.put(sp.Id, sp.LuanaSMS__Course__r.LuanaSMS__Delivery_Location__c);
    }

    // Insert SUOS for each unique SP
    List<LuanaSMS__Student_Unit_of_Study__c> newSUOSList = new List<LuanaSMS__Student_Unit_of_Study__c>();
    for(Id spId : spDLMap.keySet()) {
        LuanaSMS__Student_Unit_of_Study__c suos = new LuanaSMS__Student_Unit_of_Study__c();
        suos.LuanaSMS__Student_Program__c = spId;
        suos.Name = 'Graduate Diploma of Chartered Accounting';
        suos.LuanaSMS__Delivery_Location__c = spDLMap.get(spId);
        suos.LuanaSMS__Unit_of_Study_Code__c = 'CAP';
        //More fields can be mapped here
        newSUOSList.add(suos);
    }
    
    insert newSUOSList;
    
    // Cache SPId with newly created SUOS Id
    Map<Id, Id> spSUOSMap = new Map<Id, Id>();
    for(LuanaSMS__Student_Unit_of_Study__c suos : newSUOSList) {
        spSUOSMap.put(suos.LuanaSMS__Student_Program__c, suos.Id);
    }
    
    // Populate SUOS Id for related SPS
    for(LuanaSMS__Student_Program_Subject__c sps : trigger.new) {
        if(spSUOSMap.containsKey(sps.LuanaSMS__Student_Program__c)) {
            sps.LuanaSMS__Student_Unit_of_Study__c = spSUOSMap.get(sps.LuanaSMS__Student_Program__c);            
        }   
    }
}