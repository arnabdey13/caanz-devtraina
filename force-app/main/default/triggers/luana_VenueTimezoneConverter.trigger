// LCA-916 Session Display Venue Timezone (Temporary Workaround)

trigger luana_VenueTimezoneConverter on LuanaSMS__Session__c (before insert, before update) {
    for(LuanaSMS__Session__c sess : Trigger.new) {
        try{
        // Check if Start Time is empty
            if(!String.isBlank(sess.Venue_Timezone__c) && sess.LuanaSMS__Start_Time__c != null) {
                sess.Session_Start_Time_Venue__c = sess.LuanaSMS__Start_Time__c.format('dd/MM/yyyy h:mm aa', sess.Venue_Timezone__c); 
            }
            // Check if End Time is empty
            if(!String.isBlank(sess.Venue_Timezone__c) && sess.LuanaSMS__End_Time__c != null) {
                sess.Session_End_Time_Venue__c = sess.LuanaSMS__End_Time__c.format('dd/MM/yyyy h:mm aa', sess.Venue_Timezone__c); 
            }
        } catch (Exception exp){
            sess.addError('Error setting session timezone (' + sess.Id + '). Error: ' + exp.getMessage() + ' - ' + exp.getStackTraceString());
        }
    }
}