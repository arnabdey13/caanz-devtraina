trigger luana_EducationRecordDeleteLog on Education_Record__c (before delete) {
    for(Education_Record__c p : trigger.old) {
        luana_AuditLog.insertLog('Info', JSON.serializePretty(p), 'Education_Record__c', 'Completed', 'Record Deletion');
        
    }
      CPDSummaryHandler.createBackupOfDeletedEducationRecord(Trigger.old);
}