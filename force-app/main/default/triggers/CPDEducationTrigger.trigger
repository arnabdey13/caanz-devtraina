/*------------------------------------------------------------------------------------
Author:        Sumit Gupta
Company:       Salesforce
Description:   cpd education record trigger

History
Date            Author             Comments
--------------------------------------------------------------------------------------
02-05-2018     Sumit Gupta          Initial Release
------------------------------------------------------------------------------------*/
trigger CPDEducationTrigger on Education_Record__c (before insert, before update, after insert, after update,before delete) {

    //Update missing contact or summary details
    if (Trigger.isBefore && Trigger.isInsert) {
        CPDSummaryHandler.updateEducationContactAndSummary(Trigger.new);
        //Set Mannually Added flag when created by a community user
        CPDSummaryHandler.setEducationManuallyAdded(Trigger.new);
    }

    //Update missing contact or summary details
    if (Trigger.isBefore && Trigger.isUpdate) {
        CPDSummaryHandler.updateEducationContactAndSummary(Trigger.new);
    }

    //Update triennium year, summary rollup fields and summary specialisation hours
    if (Trigger.isAfter && Trigger.isInsert) {
        //Update education triennium year
        CPDSummaryHandler.updateEducationTrienniumYear(Trigger.new, Trigger.oldMap);
        //Update education summary rollup fields
        CPDSummaryHandler.updateSummaryRollupFields(Trigger.new, Trigger.oldMap);
        //Update education summary specialisation hours
        CPDSummaryHandler.updateSummarySpecialisationHours(Trigger.new, Trigger.oldMap);
    }

    //Update triennium year, summary rollup fields and summary specialisation hours
    if (Trigger.isAfter && Trigger.isUpdate) {
        //Update education triennium year
        CPDSummaryHandler.updateEducationTrienniumYear(Trigger.new, Trigger.oldMap);
        //Update education summary rollup fields
        CPDSummaryHandler.updateSummaryRollupFields(Trigger.new, Trigger.oldMap);
        //Update education summary specialisation hours
        CPDSummaryHandler.updateSummarySpecialisationHours(Trigger.new, Trigger.oldMap);
    }
    /*
    if(Trigger.isBefore && Trigger.isDelete){
        CPDSummaryHandler.createBackupOfDeletedEducationRecord(Trigger.old);
    }
    */
}