trigger luana_TODeleteLog on LuanaSMS__Training_Organisation__c (before delete) {
    for(LuanaSMS__Training_Organisation__c p : trigger.old) {
        luana_AuditLog.insertLog('Info', JSON.serializePretty(p), 'LuanaSMS__Training_Organisation__c', 'Completed', 'Record Deletion');
    }
}