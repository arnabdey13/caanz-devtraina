trigger luana_PaymentTokenDeleteLog on Payment_Token__c (before delete) {
    for(Payment_Token__c p : trigger.old) {
        luana_AuditLog.insertLog('Info', JSON.serializePretty(p), 'Payment_Token__c', 'Completed', 'Record Deletion');
    }
}