/*
    Created by: WDCi (KH)
    Create on: 22 Mar 2017
    Task: Trigger for Program Offering
*/
trigger luana_ProgramOfferingAction on LuanaSMS__Program_Offering__c (before insert, before update, before delete, after insert, after update, after delete) {
    
    luana_ProgramOfferingExtension.ProgramOfferingAfterAction(Trigger.new, Trigger.newMap, Trigger.old, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete, Trigger.isUndelete, Trigger.isBefore, Trigger.isAfter);
    luana_ProgramOfferingExtension.ProgramOfferingBeforeAction(Trigger.new, Trigger.newMap, Trigger.old, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete, Trigger.isUndelete, Trigger.isBefore, Trigger.isAfter);
}