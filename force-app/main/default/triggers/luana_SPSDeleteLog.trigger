trigger luana_SPSDeleteLog on LuanaSMS__Student_Program_Subject__c (before delete) {
    for(LuanaSMS__Student_Program_Subject__c p : trigger.old) {
        luana_AuditLog.insertLog('Info', JSON.serializePretty(p), 'LuanaSMS__Student_Program_Subject__c', 'Completed', 'Record Deletion');
    }
}