trigger UserTrigger on User (before update, after insert, after update) {
    if ( trigger.isBefore && trigger.isInsert){
        Map<Id,User> ContactIdToUser_Map = new Map<Id,User>();
        for(User UserObject : trigger.new){
            if(UserObject.ProfileId == ProfileCache.getId('NZICA Community Login User') &&
                // Member Portal Site Guest User (Portal registation)
                //UserObject.CreatedById=='00590000001I38aAAC' // Not created yet
                (UserInfo.getProfileId() == ProfileCache.getId('Member Portal Profile') 
                    || Test.isRunningTest()
                )
            ){
                ContactIdToUser_Map.put( UserObject.ContactId, UserObject );
            }
        }
        if( ContactIdToUser_Map.size()>0 && !(Test.isRunningTest())
          ){
            List<Contact> Contact_List = [Select c.Birthdate, c.Account.Member_Of__c, c.Account.Customer_ID__c
                From Contact c where c.id IN :ContactIdToUser_Map.keySet()];
            
            for(Contact ContactObject : Contact_List){
                User UserObject = ContactIdToUser_Map.get(ContactObject.Id);
                boolean isMemberOfNzica = (ContactObject.Account.Member_Of__c!='NZICA')?false:true;
                UserObject.TimeZoneSidKey = (isMemberOfNzica)?'Pacific/Auckland':'Australia/Sydney';
                //UserObject.UserName = ContactObject.Account.Customer_ID__c + 
                //  ((isMemberOfNzica)?'@nzica.co.nz':'@caanz.com'); -- Shouldn't set the username
                UserObject.FederationIdentifier = ContactObject.Account.Customer_ID__c;
                
                //UserObject.UserCreatedViaPortal__c=true; // Triggers Welcome email
                // Other fields?
            }
        }
    }
    
    if ( trigger.isBefore ) {
        // Before Insert check. Execute before insert logic, calls to UserTriggerHandlerClass.
        if ( trigger.isInsert ) {
            //UserTriggerHandlerClass.onBeforeInsert(trigger.new);
        } // Before Update check. Execute before update logic, calls to UserTriggerHandlerClass.
        else if ( trigger.isUpdate ) {
            UserTriggerHandlerClass.onBeforeUpdate(trigger.new, trigger.oldMap);
        } 
    } else if ( trigger.isAfter ) {
        // After Insert check. Execute After insert logic, calls to UserTriggerHandlerClass.
        if ( trigger.isInsert ) {
            //UserTriggerHandlerClass.onAfterInsert(trigger.new);
        } // After Update check. Execute After Update logic, calls to UserTriggerHandlerClass. 
        else if ( trigger.isUpdate ) {
            //UserTriggerHandlerClass.onAfterUpdate(trigger.new, trigger.oldMap);
        }
    }
}