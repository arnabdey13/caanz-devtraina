trigger ApplicationTrigger on Application__c (before insert, after insert, before update, after update ) {
	
	//Get the Trigger Setting for the case Object from Custom Meta Data.
    Trigger_Settings_Configuration__mdt trSettConf = ApexUtil.getTriggSettConf('Application__c');
    
    //Exit Trigger Execution if Configuration is to Mute or Custom Permission is Assinged to the current User.
    if((trSettConf != null && trSettConf.Trigger_Mute__c) || ApexUtil.getIfUserHasCustPerm(Label.CustomPermissionMuteTriggerProcesses))
        return; 
    
    ApplicationTriggerHandler applicationhandler = new ApplicationTriggerHandler();
    
    if(trSettConf != null && trSettConf.Trigger_Loop_Count__c != null)
        applicationhandler.setMaxLoopCount(Integer.valueOf(trSettConf.Trigger_Loop_Count__c));
    
    if(trSettConf != null && trSettConf.Trigger_Loop_Count__c != null)
        applicationhandler.setMaxLoopCount(Integer.valueOf(trSettConf.Trigger_Loop_Count__c));
    
    try{        
        applicationhandler.run();        
    }catch(exception e){
        system.debug('exception Message-->'+e.getMessage()+ '');
        system.debug('exception stack trace-->'+e.getStackTraceString());
    }
    
    // PAN:6255     19/07/2019      WDCi (LKoh): Merged the trigger for Application with existing Application trigger
    if (trigger.isAfter && (trigger.isInsert || trigger.isUpdate)) {
        wdci_ApplicationHandler.initFlow(Trigger.new, Trigger.old, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete, Trigger.isUndelete, Trigger.isBefore, Trigger.isAfter);    
    }
}
	/*/ Code before it was generic!
	Set<Id> AccountIdOfApprovedApplication_Set = new Set<Id>();
	Set<Id> AccountIdOfDeclinedApplication_Set = new Set<Id>();
	for(Integer i=0; i<trigger.new.size(); i++){
		
		//System.assert(false, '## ' + trigger.new[i].get('Application_Status__c') );
		
		if(
		trigger.new[i].Application_Status__c=='Approved' &&
		trigger.old[i].Application_Status__c=='For Approval'
		){
			AccountIdOfApprovedApplication_Set.add( trigger.new[i].Account__c );
		}
		
		if(
		trigger.new[i].Application_Status__c!=trigger.old[i].Application_Status__c &&
		trigger.new[i].Application_Status__c=='Declined'
		){
			AccountIdOfDeclinedApplication_Set.add( trigger.new[i].Account__c );
		}
	}
	if( AccountIdOfDeclinedApplication_Set.size()>0){
		List<Account> Account_List = [Select a.RecordType.Name, a.RecordTypeId From Account a
			where a.Id IN :AccountIdOfDeclinedApplication_Set];
		
		for( Account AccountToUpdate : Account_List ){
			AccountToUpdate.Status__c = 'Declined';
		}
		update Account_List;
	}
	if( AccountIdOfApprovedApplication_Set.size()>0){
		List<Account> Account_List = [Select a.RecordType.Name, a.RecordTypeId From Account a
			where a.Id IN :AccountIdOfApprovedApplication_Set and a.RecordType.Name='Student Affiliate'];
		
		for( Account AccountToUpdate : Account_List ){
			AccountToUpdate.RecordTypeId = RecordTypeCache.getId(AccountToUpdate, 'Full_Member');
		}
		update Account_List;
	}
	//*/