trigger QPRQuestionnaireTrigger on QPR_Questionnaire__c (before insert, before update) {
    /* REMOVED 25/10/2016 RNg, removed as code is no longer required.  Process Builder logic performs this function.
    if(trigger.isBefore) {
    	Set<Id> reviewCodeIds = new Set<Id>();
        for(QPR_Questionnaire__c a:trigger.new) {
            if(a.Status__c == 'Submitted' || 
                a.Status__c == 'Approved' ||
                a.Status__c == 'Declined' ||
                a.Status__c == 'Escalated for Approval') {
                	a.Lock__c = true;
					
                	// 31/08/2015 - When the questionnaire is submitted, the Submitted Date on the Review object needs to be populated with the current date
                	if(a.Status__c == 'Submitted' && a.Review_Code__c != null) {
                		reviewCodeIds.add(a.Review_Code__c);
                	}
			}
            else {
                a.Lock__c = false;
            }
        }
        system.debug('@@@ reviewCodeIds: ' + reviewCodeIds);
		
		if(reviewCodeIds != null && !reviewCodeIds.isEmpty()) {
	        // Get list of review
	        List<Review__c> reviewList = [select Id, Questionnaire_Submitted_Date__c from Review__c where Id in :reviewCodeIds];
	        List<Review__c> reviewListToUpdate;
	        if(reviewList != null && reviewList.size() > 0) {
	        	reviewListToUpdate = new List<Review__c>();
	        	for(Review__c r : reviewList) {
	        		// Update Submitted Date to current date
	        		r.Questionnaire_Submitted_Date__c = system.today();
					
	        		reviewListToUpdate.add(r);
	        	}
	        }
	        
			if(reviewListToUpdate != null && reviewListToUpdate.size() > 0) {
	            try {
	                update reviewListToUpdate;
	                system.debug('@@@ reviewListToUpdate successfully updated.');
	            }
	            catch(Exception ex) {
	                system.debug('@@@ Update reviewListToUpdate error: ' + ex);
	            }
	        }
		}
    }*/    
}