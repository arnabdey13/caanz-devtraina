trigger luana_SubjectDeleteLog on LuanaSMS__Subject__c (before delete) {
    for(LuanaSMS__Subject__c p : trigger.old) {
        luana_AuditLog.insertLog('Info', JSON.serializePretty(p), 'LuanaSMS__Subject__c', 'Completed', 'Record Deletion');
    }
}