/*
    Developer: WDCi (Lean)
    Date: 08/Jul/2016
    Task #: LCA-724 CPD Hours allocation
*/

trigger luana_SessionAction on LuanaSMS__Session__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    
    luana_SessionHandler.initiateFlow(Trigger.new, Trigger.newMap, Trigger.old, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete, Trigger.isUndelete, Trigger.isBefore, Trigger.isAfter);
}