/*------------------------------------------------------------------------------------
Author:        Sumit Gupta
Company:       Salesforce
Description:   cpd exemption trigger

History
Date            Author             Comments
--------------------------------------------------------------------------------------
02-05-2018     Sumit Gupta       	Initial Release
------------------------------------------------------------------------------------*/
trigger CPDExemptionTrigger on CPD_Exemption__c (before insert, after insert, before update, after update) {

	//Set cpd exemption account lookup
	if (Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)){
		CPDSummaryHandler.updateExemptionAccountAndSummary(Trigger.new);
	}

	//Update exemption summary rollup fields
	if (Trigger.isAfter && Trigger.isInsert) {
		CPDSummaryHandler.updateSummaryRollupFields(Trigger.new, null);
	}

	//Update exemption summary rollup fields
	if (Trigger.isAfter && Trigger.isInsert) {
		CPDSummaryHandler.updateSummaryRollupFields(Trigger.new, Trigger.oldMap);
	}
}