/**
* @author           WDCi-LKoh
* @date             20/06/2019
* @group            Competency Automation
* @description      Trigger for FT_University_Subject__c
* @change-history
*/
trigger wdci_UniversitySubjectTrigger on FT_University_Subject__c (before update, before delete) {

    wdci_UniversitySubjectHandler.initFlow(Trigger.new, Trigger.old, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete, Trigger.isUndelete, Trigger.isBefore, Trigger.isAfter);
}