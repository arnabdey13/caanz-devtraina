trigger FineCostTrigger on Fine_Cost__c (after insert, after update, after delete, after undelete) {    
    
    List<Fine_Cost__c> FC_List = (trigger.isDelete) ? trigger.old : trigger.new;
    Set<Account> AccountsToUpdate_Set = new Set<Account>();
    for(Integer i = 0; i < FC_List.size(); i++){
        
        if(FC_List[i].Member_Formula__c == null){
            continue;
        }
        
        if(trigger.isInsert || trigger.isDelete || trigger.isUnDelete ||
          (trigger.isUpdate && (           
           FC_List[i].Member_Formula__c != trigger.old[i].Member_Formula__c ||
           FC_List[i].Cost_Recovery_Status__c != trigger.old[i].Cost_Recovery_Status__c))){
               
               if(FC_List[i].Member_Formula__c != null){
                   Account AccountToUpdate = new Account(Id=FC_List[i].Member_Formula__c);
                   AccountToUpdate.RollUpFromProfessionalConductAndFineCost__c = true;
                   AccountsToUpdate_Set.add(AccountToUpdate);
               }                  
           } 	
    update new List<Account> (AccountsToUpdate_Set);   
}   
}