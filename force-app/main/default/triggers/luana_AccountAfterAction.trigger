/**
    Developer: WDCi (KH)
    Development Date: 07 Mar 2016
    Project/Task Name/Task Number: Account trigger for after action
    
    ChangeHistory:
    PAN:5342 12/10/2018 WDCi Lean: CAF/CAP validation
**/
trigger luana_AccountAfterAction on Account (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    luana_MemberDataIntegrationHandler.accountAction(trigger.new, trigger.newMap, trigger.old, trigger.oldMap, trigger.isInsert, trigger.isUpdate, trigger.isDelete, trigger.isUndelete, trigger.isBefore, trigger.isAfter);
    
}