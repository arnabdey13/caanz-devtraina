/*
    Triggerfor Employment History 
    ======================================================
    Changes:
    	May 2014	Created
    	June 2016 	Davanti - RN	EHCH - Updated to call trigger handler to populate Member record with a count of EH records a member provides CPP services at
    	Aug 2016	Davanti - RN	EHCH - After delete of EHCH record - update parent account record. 
					Davanti	- RN	EHCH - Added logic for Provides_CPP_Services_here_AU/NZ__c calculation
    	
*/
trigger EmploymentHistoryTrigger on Employment_History__c (before insert, before update, after delete) {
	if(trigger.isBefore){
		
		// EHCH changes
		if(trigger.IsInsert){
			EmploymentHistoryTriggerHandler.onBeforeInsert(trigger.new);
		}
		if(trigger.IsUpdate){
			EmploymentHistoryTriggerHandler.onBeforeUpdate(trigger.new, trigger.old, trigger.oldMap);
		}// end of EHCH changes
		
		
		Map<Id,List<Employment_History__c>> AccountIdToEmploymentHistoryList_Map = new Map<Id,List<Employment_History__c>>();
		
		for(Employment_History__c EmploymentHistory : Trigger.new){
			
			Employment_History__c oldEH = (trigger.isUpdate) ? trigger.oldMap.get(EmploymentHistory.Id) : null; // EHCH 
			
			if(trigger.isInsert || (trigger.isUpdate && 
				(EmploymentHistory.Rollups_Recalculation__c || (oldEH != null && oldEH.Is_CPP_Provided__c != EmploymentHistory.Is_CPP_Provided__c)))){ // EHCH - updated Update condition
				system.debug('### in my if statement');
				List<Employment_History__c> EmploymentHistory_List = AccountIdToEmploymentHistoryList_Map.get(EmploymentHistory.Member__c);
				if(EmploymentHistory_List==null){
					AccountIdToEmploymentHistoryList_Map.put( EmploymentHistory.Member__c, new List<Employment_History__c>{ EmploymentHistory } );
				}
				else{
					EmploymentHistory_List.add(EmploymentHistory);
					AccountIdToEmploymentHistoryList_Map.put( EmploymentHistory.Member__c, EmploymentHistory_List );
				}
				EmploymentHistory.Rollups_Recalculation__c=false;
			}
		}
		if(AccountIdToEmploymentHistoryList_Map.size()>0){
			List<Account> Account_List = [Select CPP__c, CPP_Picklist__c, Membership_Class__c, CPP_Picklist_AU__c, CPP_Picklist_NZ__c // EHCH
											from Account where Id IN :AccountIdToEmploymentHistoryList_Map.keySet()];
			
			for(Account AccountObj : Account_List){
				List<Employment_History__c> EmploymentHistory_List = AccountIdToEmploymentHistoryList_Map.get( AccountObj.Id );
				for(Employment_History__c EmploymentHistory : EmploymentHistory_List){
					EmploymentHistory.Rollup_Provisional_Member__c = (AccountObj.Membership_Class__c=='Provisional')?true:false;
						
					EmploymentHistory.Rollup_Full_Member__c = (AccountObj.Membership_Class__c=='Full')?true:false;
						
					EmploymentHistory.Rollup_Holding_CPP__c = 
						(AccountObj.CPP__c 
						/*AccountObj.CPP_Picklist__c=='Full' ||
						AccountObj.CPP_Picklist__c=='Exempt' ||
						AccountObj.CPP_Picklist__c=='Monitored Member' ||
						AccountObj.CPP_Picklist__c=='Concessional'*/
						)?true:false;
					
					EmploymentHistory.Provides_CPP_Services_here_both__c = (AccountObj.CPP_Picklist_AU__c == 'Full' || AccountObj.CPP_Picklist_NZ__c == 'Full') && EmploymentHistory.Is_CPP_Provided__c; // EHCH 30/8/16
					EmploymentHistory.Provides_CPP_Services_here_AU__c = ((AccountObj.CPP_Picklist_AU__c == 'Full') && EmploymentHistory.Is_CPP_Provided__c) ? true : false; // EHCH 30/8/16
					EmploymentHistory.Provides_CPP_Services_here_NZ__c = ((AccountObj.CPP_Picklist_NZ__c == 'Full') && EmploymentHistory.Is_CPP_Provided__c) ? true : false; // EHCH 30/8/16
					
				}
			}
		}
		else{ // trigger.isUpdate
			
		}
	}
	else{ // trigger.isAfter
		if(trigger.isDelete){
			EmploymentHistoryTriggerHandler.onAfterDelete(trigger.old, trigger.oldMap);
		}
	}
}