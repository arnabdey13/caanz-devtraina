trigger luana_DLDeleteLog on LuanaSMS__Delivery_Location__c (before delete) {
    for(LuanaSMS__Delivery_Location__c p : trigger.old) {
        luana_AuditLog.insertLog('Info', JSON.serializePretty(p), 'LuanaSMS__Delivery_Location__c', 'Completed', 'Record Deletion');
    }
}