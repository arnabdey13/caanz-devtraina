trigger TaskTrigger on Task (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    //Get the Trigger Setting for the task Object from Custom Meta Data.
    Trigger_Settings_Configuration__mdt trSettConf = ApexUtil.getTriggSettConf('Task');
    
    //Exit Trigger Execution if Configuration is to Mute or Custom Permission is Assinged to the current User.
    if((trSettConf != null && trSettConf.Trigger_Mute__c) || ApexUtil.getIfUserHasCustPerm(Label.CustomPermissionMuteTriggerProcesses))
        return; 
    
    //Call trigger Handler Class with Max Loop Count Set from Configuration. 
    TaskTriggerHandler tskHandler = new TaskTriggerHandler();  
    
    if(trSettConf != null && trSettConf.Trigger_Loop_Count__c != null)
        tskHandler.setMaxLoopCount(Integer.valueOf(trSettConf.Trigger_Loop_Count__c));
    
    try{        
        tskHandler.run();        
    }catch(exception e){
        system.debug('exception Message-->'+e.getMessage());
        system.debug('exception stack trace-->'+e.getStackTraceString());
    }
}