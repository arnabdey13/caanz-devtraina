/**
    Developer: WDCi (KH)
    Development Date: 07 Mar 2016
    Project/Task Name/Task Number: LuanaSMS__Resource_Booking__c trigger after action for update/insert Luana_ContributorComnunity permissionSet
**/
trigger luana_ResourceBookingAction on LuanaSMS__Resource_Booking__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    
    luana_ResourceBookingHandler.initiateFlow(trigger.new, trigger.newMap, trigger.old, trigger.oldMap, trigger.isInsert, trigger.isUpdate, trigger.isDelete, trigger.isUndelete, trigger.isBefore, trigger.isAfter);
    
}