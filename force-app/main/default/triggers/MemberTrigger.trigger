trigger MemberTrigger on Account (before update) {       
       
    Set<Id> UnpaidFineCost_Set = new Set<Id>();
    Set<Id> AUPC_Set = new Set<Id>();
    Set<Id> NZPC_Set = new Set<Id>();
        
    if(trigger.isBefore){
    
        Set<String> AccountId_Set = new Set<String>();
                                          
        for(Account AccountObj : trigger.new){
        
            if(AccountObj.RollUpFromProfessionalConductAndFineCost__c){
                
                AccountObj.RollUpFromProfessionalConductAndFineCost__c = false;
                AccountId_Set.add(AccountObj.Id); 
                                                
            }            
        }
          
    if(AccountId_Set.size() > 0){                                
        List<Conduct_Support__c> PC_List = [Select Id, Member__c, Country__c, Overall_Status__c, Complaint__c, Outcome__c FROM Conduct_Support__c WHERE Member__c IN : AccountId_Set];          
        List<Professional_Conduct_Tribunal__c> PCT_List = [Select Id FROM Professional_Conduct_Tribunal__c WHERE Professional_Conduct__c IN : PC_List];            
        List<Appeal_Tribunal__c> AT_List = [Select Id FROM Appeal_Tribunal__c WHERE Professional_Conduct__c IN : PC_List];                        
        List<Fine_Cost__c> Fine_Cost_List = [SELECT Id, Professional_Conduct_Tribunal__c, Appeal_Tribunal__c, Cost_Recovery_Status__c, Member_Formula__c
             FROM Fine_Cost__c
             WHERE (Professional_Conduct_Tribunal__c IN : PCT_List OR
             Appeal_Tribunal__c IN : AT_List) AND
             Cost_Recovery_Status__c != 'Paid in Full' ];     
                                                                 
        for(Conduct_Support__c PC : PC_List){        
            if(PC.Country__c == 'AU' && PC.Overall_Status__c == 'Open' && PC.Member__c != null){            
                AUPC_Set.add(PC.Member__c);            
            }
            else if(PC.Country__c == 'NZ' && PC.Complaint__c == true && PC.Member__c != null
            ){
                NZPC_Set.add(PC.Member__c);
            }                   
        }
        
        for(Fine_Cost__c FineCost : Fine_Cost_List){            
            if(FineCost.Member_Formula__c != null){       
                UnpaidFineCost_Set.add(FineCost.Member_Formula__c);                
            }                        
        }
        
        for(Account AccountObj : trigger.new){
        
            if(UnpaidFineCost_Set.contains(AccountObj.Id)){            
                AccountObj.Unpaid_Fine_Cost_Flag_Checkbox__c = true;            
            }
            else{            
                AccountObj.Unpaid_Fine_Cost_Flag_Checkbox__c = false;            
            }
            
            if(AUPC_Set.contains(AccountObj.Id)){            
                AccountObj.Professional_Conduct_Flag_AU_Checkbox__c = true;            
            } 
            else{            
                AccountObj.Professional_Conduct_Flag_AU_Checkbox__c = false;            
            }
            
            if(NZPC_Set.contains(AccountObj.Id)){            
                AccountObj.Professional_Conduct_Flag_NZ_Checkbox__c = true; 
       SYSTEM.DEBUG('SFDC DEBUG AccountObj.Professional_Conduct_Flag_NZ_Checkbox__c = true');                     
            } 
            else{            
                AccountObj.Professional_Conduct_Flag_NZ_Checkbox__c = false;            
       SYSTEM.DEBUG('SFDC DEBUG AccountObj.Professional_Conduct_Flag_NZ_Checkbox__c = false');                
            }         
        }                                   
    }       
    }    
}