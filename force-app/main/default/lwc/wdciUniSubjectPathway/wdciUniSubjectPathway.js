/**
* @author           WDCi-LKoh
* @date             19/06/2019
* @group            Related Pathway
* @description      Javascript for the LWC responsible to display the Pathway that is related to a University Subject
* @change-history
*/
/* eslint-disable vars-on-top */
/* eslint-disable no-console */
// eslint-disable-next-line no-unused-vars
import { NavigationMixin } from 'lightning/navigation';
import { refreshApex } from '@salesforce/apex';
import { LightningElement, api, wire } from 'lwc';
import grabRelatedPathway from '@salesforce/apex/wdci_UniSubjectPathwayController.grabRelatedPathway';

export default class WdciUniSubjectPathway extends NavigationMixin(LightningElement) {

    @api recordId;
    @api context;
    @wire(grabRelatedPathway, { uniSubjectId: '$recordId' })
    pathways;

    handlePathwayView(event) {

        console.log("Entering handlePathwayView");
        // console.log(JSON.stringify(event.target.value));        
        // console.log("context: " +this.context);

        // Navigate to Pathway record page
        /*  Unfortunately the navigation standard function does NOT WORK when this component is embedded for display in Salesforce Classic */
        if (this.context !== undefined) {            
            // This is a workaround to cater for the navigation in Salesforce Classic as the standard LWC navigation does not work in Classic
            window.open('/' +event.target.value+ '','_blank'); 
        } else {
            this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: event.target.value,                
                    actionName: 'view',
                },
            });
        }
    }

    handleRefreshView() {
        console.log("Refresh Pathway View");        
        return refreshApex(this.pathways);
    }
}