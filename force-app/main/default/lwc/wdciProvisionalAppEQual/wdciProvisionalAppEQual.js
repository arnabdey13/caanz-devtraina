/**
* @author           WDCi-LKoh
* @date             04/06/2019
* @group            Provisional Application Student Paper
* @description      Javascript for the LWC responsible to display the eQualification on Education History of the Provisional Application
* @change-history   #FIX004 -   WDCi-Lkoh   -   05/09/2019  -   Changed the EQual saving behavior
*/
/* eslint-disable no-console */
import { LightningElement, track, api, wire } from 'lwc';

import grabEQual from '@salesforce/apex/wdci_ProvisionalAppController.grabEQual';
import saveEQual from '@salesforce/apex/wdci_ProvisionalAppController.saveEQual';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class WdciProvisionalAppEQual extends LightningElement {

    @api educationHistoryId;
    @track eQualValue;
    @track error;

    @wire(grabEQual, {
        educationHistoryId: "$educationHistoryId"
    })
    fetchedEQual({error, data}) {        
        if (data) {            
            this.eQualValue = String(data);            
            this.error = undefined;            
        } else if (error) {
            this.error = error;
            this.eQualValue = undefined;
            // console.log(error);
        }
    }

    handleChange(event) {
                
        window.clearTimeout(this.delayTimeout);
        const updatedURLString = event.target.value;        
        
        // #FIX004  -   removed the delay mechanism as they are no longer needed
        saveEQual({
            educationHistoryId: this.educationHistoryId,
            urlString: updatedURLString
        })
        .then(result => {
            if (result.localeCompare("My eQuals link saved") === 0) {
                const toastEvent = new ShowToastEvent({
                    title: 'Success',
                    variant: 'success',
                    message: result,
                });
                this.dispatchEvent(toastEvent);
            } else {
                console.log(result);
                const toastEvent = new ShowToastEvent({
                    title: 'Error',
                    message: result,
                    variant: 'error',
                    mode: 'dismissable'
                });
                this.dispatchEvent(toastEvent);
            }
        })
        .catch(error => {
            // console.log(error);
            const toastEvent = new ShowToastEvent({
                title: 'Error',
                message: error.message,
                variant: 'error',
                mode: 'dismissable'
            });
            this.dispatchEvent(toastEvent);
        });
    }
}