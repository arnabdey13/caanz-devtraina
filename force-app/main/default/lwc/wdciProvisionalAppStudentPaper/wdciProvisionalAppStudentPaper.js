/**
* @author           WDCi-LKoh
* @date             31/05/2019
* @group            Provisional Application Student Paper
* @description      Javascript for the LWC responsible to display the Subject History / Student Paper selection on Education History of the Provisional Application
* @changehistory    #FIX005 - WDCi-LKoh - 05/09/2019 - Change Request to hide the Subject Paper panel if there is no valid Subject Paper
* @changehistory    #UAT001 - WDCi-LKoh - 09/09/2019 - Added new configurable Text for subject paper on top and bottom (ff_RichTextStores)
*/
/* eslint-disable vars-on-top */
/* eslint-disable no-console */
// eslint-disable-next-line no-unused-vars
import { LightningElement, api, wire, track } from 'lwc';

import grabUniSubjectListMk3 from '@salesforce/apex/wdci_ProvisionalAppController.grabUniSubjectListMk3';
import grabAdditionalRemark from '@salesforce/apex/wdci_ProvisionalAppController.grabAdditionalRemark';
import grabTopDescription from '@salesforce/apex/wdci_ProvisionalAppController.grabTopDescription';
import grabBottomDescription from '@salesforce/apex/wdci_ProvisionalAppController.grabBottomDescription';
import saveUniSubjectList from '@salesforce/apex/wdci_ProvisionalAppController.saveUniSubjectList';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class WdciProvisionalAppStudentPaper extends LightningElement {

    @api educationHistoryId;    
    @track wrapperMapList;
    @track remarkValue;     
    @track error;
    @track isPanelVisible = false;  // #FIX005
    @track topInstruction;
    @track bottomInstruction;
    
    // This is called during initialization and loads 
    connectedCallback() {        
        grabUniSubjectListMk3({educationHistoryId: this.educationHistoryId})
            .then(data => {
                console.log('loading wrapperMapList');
                this.wrapperMapList = JSON.parse(JSON.stringify(data));
                // #FIX005
                if (this.wrapperMapList.length !== 0) {
                    this.isPanelVisible = true;
                }
            })
            .catch(error => {
                this.error = error;
            });            
    }

    @wire(grabAdditionalRemark, {
        educationHistoryId: "$educationHistoryId"
    })
    fetchedRemark({error, data}) {        
        if (data) {            
            this.remarkValue = String(data);            
            this.error = undefined;            
        } else if (error) {
            this.error = error;
            this.remarkValue = undefined;            
        }
    }    

    // #UAT001
    @wire(grabTopDescription)
    fetchedTopDescription({error, data}) {
        console.log(data);
        if (data) {
            this.topInstruction = String(data);
            this.error = undefined;
        } else if (error) {
            this.error = error;
            this.topInstruction = undefined;
        }
    }

    // #UAT001
    @wire(grabBottomDescription)
    fetchedBottomDescription({error, data}) {
        console.log(data);
        if (data) {
            this.bottomInstruction = String(data);
            this.error = undefined;
        } else if (error) {
            this.error = error;
            this.bottomInstruction = undefined;
        }
    }

    handleChange(event) {

        var itemChecked = event.target.checked;
        
        for (let i = 0; i < this.wrapperMapList.length; i ++) {
            if (event.target.dataset.item.localeCompare(this.wrapperMapList[i].uniSubjectId) === 0) {
                this.wrapperMapList[i].selected = itemChecked;
            }
        }
    }

    saveStudentPaper() {
        // console.log("currentEducationHistoryId: " +this.educationHistoryId);
        // console.log("wrapperMapList: " +JSON.stringify(this.wrapperMapList, null, 4));

        saveUniSubjectList({
            educationHistoryId: this.educationHistoryId,
            subjectMapList: this.wrapperMapList,
            internalNoteValue: this.remarkValue
        })
        .then(result => {
            if (result.localeCompare("Subject History Saved") === 0) {
                const event = new ShowToastEvent({
                    title: 'Success',
                    variant: 'success',
                    message: result,
                });
                this.dispatchEvent(event);
            } else {
                console.log(result);
                const event = new ShowToastEvent({
                    title: 'Error',
                    message: result,
                    variant: 'error',
                    mode: 'dismissable'
                });
                this.dispatchEvent(event);
            }            
        })
        .catch(error => {
            // console.log(error);
            const event = new ShowToastEvent({
                title: 'Error',
                message: error.message,
                variant: 'error',
                mode: 'dismissable'
            });
            this.dispatchEvent(event);
        })
    }

    handleNoteChange(event) {
        var noteValue = event.target.value;
        this.remarkValue = noteValue
    }
}