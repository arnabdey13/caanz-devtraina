var j$ = jQuery.noConflict();
var search = false;

j$(document).ready(function(){

	var iconLinkedIn = j$("span[id$='IdIconLinkedIn']").html();
	var iconTwitter = j$("span[id$='IdIconTwitter']").html();
	var iconFacebook = j$("span[id$='IdIconFacebook']").html();
	var iconInstagram = j$("span[id$='IdIconInstagram']").html();
	var iconYouTube = j$("span[id$='IdIconYouTube']").html();
	
	j$("span[id$='IdSpanLinkedIn']").attr('data-icon', iconLinkedIn);
	j$("span[id$='IdSpanTwitter']").attr('data-icon', iconTwitter);
	j$("span[id$='IdSpanFacebook']").attr('data-icon', iconFacebook);
	j$("span[id$='IdSpanInstagram']").attr('data-icon', iconInstagram);
	j$("span[id$='IdSpanYouTube']").attr('data-icon', iconYouTube);

	j$.validator.addMethod(
		"countryValidation", 
		function(value, element) {
			return value != '';  
		},
		"Country is required."
	);
	
	j$.validator.addMethod(
		"locationValidation", 
		function(value, element) {
			return value != '';  
		},
		"Location is required."
	);
	
	j$.validator.addMethod(
		"typeValidation", 
		function(value, element) {
			return value != '';  
		},
		"Type is required."
	);
	
	var validator = j$("form[name$='IdFindCAForm']").validate({
		errorPlacement: function(error, element) {
			if (element.attr("name") == j$('[id$="IdAffiliatedBranchCountry"]').attr('Id')) {
				error.appendTo(j$('[id$="IdCountryRequired"]'));
			}
			else if (element.attr("name") == j$('[id$="IdAffiliatedBranch"]').attr('Id')) {
				error.appendTo(j$('[id$="IdLocationRequired"]'));
			}
			else if (element.attr("name") == j$('[id$="IdMemberType"]').attr('Id')) {
				error.appendTo(j$('[id$="IdTypeRequired"]'));
			}
			else {
				error.insertAfter(element);
			}
		}
	}); 
	
	j$("select[name$='IdAffiliatedBranchCountry']").rules( "add", {
		countryValidation: true
	});
	j$("select[name$='IdAffiliatedBranch']").rules( "add", {
		locationValidation: true
	});
	j$("select[name$='IdMemberType']").rules( "add", {
		typeValidation: true
	});
});

function performSearch(){
	search = true;
	var isValid = validateParameters();
	if(isValid){
		searchCA();
	}
}

function validateParameters(){
	if(search){
		return j$('[id$="IdFindCAForm"]').valid();
	}
	else{
		return false;
	}
}

function scrollTo(hash){
	location.hash="#" + hash;
}

function displayLinks(isDisplay){
	if(isDisplay)
		j$('a').css('pointer-events', 'auto');
	else
		j$('a').css('pointer-events', 'none');
}

var previousPageNum;
function highlightSelected(pageNum){
	
	var selectedPageId;
	
	if(previousPageNum != null){
		selectedPageId = ':' + (previousPageNum-1) + ':IdResultsPageNum';
		j$("a[id$='" + selectedPageId + "']").css('background-color', '');
		j$("a[id$='" + selectedPageId + "']").css('color', '');
		j$("a[id$='" + selectedPageId + "']").css('font-size', '');
		j$("a[id$='" + selectedPageId + "']").css('text-decoration', '');
		j$("a[id$='" + selectedPageId + "']").css('pointer-events', '');
	}
	previousPageNum = pageNum;
	
	selectedPageId = ':' + (pageNum-1) + ':IdResultsPageNum';
	j$("a[id$='" + selectedPageId + "']").css('background-color', '#00a3dd');
	j$("a[id$='" + selectedPageId + "']").css('color', 'white');
	j$("a[id$='" + selectedPageId + "']").css('font-size', 'large');
	j$("a[id$='" + selectedPageId + "']").css('text-decoration', 'none');
	j$("a[id$='" + selectedPageId + "']").css('pointer-events', 'none');
}

var userAgent = window.navigator.userAgent;
function setPositionToolTipType(){
	if(userAgent.indexOf('Trident/') == -1){
		var position = j$("a[id$='IdToolTipType']").position();
		j$("div[id$='IdModalDialogType']").css('padding-top', position.top);
	}
}
function setPositionToolTipName(){
	if(userAgent.indexOf('Trident/') == -1){
		var position = j$("a[id$='IdToolTipName']").position();
		j$("div[id$='IdModalDialogName']").css('padding-top', position.top);
	}
}
function setPositionToolTipConditions(element, info){
	if(userAgent.indexOf('Trident/') == -1){
		var position = j$(element).position();
		j$("div[id$='IdModalDialogConditions']").css('padding-top', position.top);
	}
	j$("span[id$='IdModalConditionsText']").text(info);
}
// Rev 01 akopec start
function setPositionToolTipSpecialties(element, info){
	if(userAgent.indexOf('Trident/') == -1){
		var position = j$(element).position();
		j$("div[id$='IdModalDialogSpecialties']").css('padding-top', position.top);
	}
	j$("span[id$='IdModalSpecialtiesText']").text(info);
}
// Rev 01 akopec end