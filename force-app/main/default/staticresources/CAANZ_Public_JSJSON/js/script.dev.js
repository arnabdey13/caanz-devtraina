// ==========================================================================
// Deloitte Digital
// ==========================================================================





window.DDIGITAL = window.DDIGITAL || {};
DDIGITAL.IS_RESPONSIVE = true;
DDIGITAL.IS_EDIT = DDIGITAL.IS_EDIT || false;

/* Stop console.log errors */
if (typeof console === 'undefined') {
	window.console = {};
	console.log = console.error = console.info = console.debug = console.warn = console.trace = console.dir = console.dirxml = console.group = console.groupEnd = console.time = console.timeEnd = console.assert = console.profile = console.table = function() {};
}

(function(NAMESPACE, $) {

	'use strict';

	DD.bp.options({
		breakpoints: [
			{
				name: 'xxs',
				px: 359
			},
			{
				name: 'xs',
				px: 480
			},
			{
				name: 's',
				px: 640
			},
			{
				name: 'm',
				px: 768
			},
			{
				name: 'l',
				px: 1024
			},
			{
				name: 'xl',
				px: 1244
			},
			{
				name: 'xxl',
				px: 1410
			}
		]
	});

	NAMESPACE.INIT = {
		visual: function() {
			NAMESPACE.contentModules.init();

			NAMESPACE.util.helpers.init();
			NAMESPACE.util.device.init();

			NAMESPACE.navOffscreen.init();
			NAMESPACE.navOnscreen.init();

			NAMESPACE.expandCollapse.init();
			NAMESPACE.modal.init();

			NAMESPACE.tooltip.init();

			NAMESPACE.carousel.init();

			NAMESPACE.tabs.init();

			NAMESPACE.linkIcons.init();

			NAMESPACE.responsiveTable.init();

			NAMESPACE.doWhen.init();

			NAMESPACE.equalHeights.init();

			NAMESPACE.globalHeaderNav.init();
		},
		functional: function() {
			//if you need modals on page load - put this higher
			NAMESPACE.modal.init();

			NAMESPACE.validate.init();
			NAMESPACE.errors.init();

			NAMESPACE.togglePopover.init();
			NAMESPACE.autocomplete.init();

			NAMESPACE.util.print.init();
			NAMESPACE.util.scroll.init();

			// if you need the shade to slide with the page
			// (e.g. if the page uses the offscreen nav in "side" position)
			// put the shade inside the '.page-wrap' instead
			$('.page-wrap').ddShade();

			NAMESPACE.buildInfo.init();

			if (NAMESPACE.IS_EDIT) {
				$('html').addClass('is-edit');
			}
		}
	};

	NAMESPACE.init = function() {
		NAMESPACE.INIT.visual();
		NAMESPACE.INIT.functional();
		NAMESPACE.triggerReady();
	};

	$(document).ready(function() {
		NAMESPACE.init();
	});

}(DDIGITAL, jQuery));
// ==========================================================================
// BUILD INFORMATION
// ==========================================================================

window.DD_BUILD = window.DD_BUILD || {};

// Uncomment this line to emulate the global variables that are added to build/assets/js/script.js after doing a
// middleman build
//window.DD_BUILD = window.DD_BUILD || {}, window.DD_BUILD.NUMBER = '4', window.DD_BUILD.BRANCH = 'master';

(function(NAMESPACE, $) {

	'use strict';

	NAMESPACE.buildInfo = (function() {
		var init;

		init =  function() {

			// Very early exit if no build information is at hand
			if (DD_BUILD.NUMBER === undefined || DD_BUILD.BRANCH === undefined) {
				return;
			}

			var $target = $('.js-buildinfo');

			// Early exit if target DOM node is not present
			if ($target.length === 0) {
				return;
			}

			$target.each(function() {
				var $el = $(this);

				$el.removeClass('hidden')
					.find('.middleman.build-number')
					.text(DD_BUILD.NUMBER);

				$el.find('.middleman.datetime')
					.text(DD_BUILD.DATETIME || '');

				$el.find('.middleman.branch')
					.text(DD_BUILD.BRANCH);
			});
		};

		return {
			init: init
		};

	}());

}(DDIGITAL, jQuery));
// ==========================================================================
// DEVICE UTILITIES
// ==========================================================================

/**
 * Deloitte Digital global namespace
 * @namespace DDIGITAL
 */

(function(NAMESPACE, $) {

	'use strict';

	/**
	 * Utility namespace
	 * @namespace DDIGITAL.util
	 * @memberof DDIGITAL
	 */
	NAMESPACE.util = NAMESPACE.util || {};

	/**
	 * Breakpoints for JavaScript. Works with the Deloitte Digital SCSS @bp mixin
	 *
	 * @namespace device
	 * @memberof DDIGITAL.util
	 * @version 1.0.0
	 * @author Deloitte Digital Australia deloittedigital@deloitte.com.au
	 */

	NAMESPACE.util.device = (function() {
		var isDevice,
			init;

		/**
		 * Initialiser for the helpers
		 *
		 * @memberof DDIGITAL.util.device
		 */
		isDevice = {
			Android: function() {
				return navigator.userAgent.match(/Android/i);
			},
			AndroidChrome: function() {
				return window.chrome && navigator.userAgent.match(/Android/i);
			},
			BlackBerry: function() {
				return navigator.userAgent.match(/BlackBerry/i);
			},
			iOS: function() {
				return navigator.userAgent.match(/iPhone|iPad|iPod/i);
			},
			Opera: function() {
				return navigator.userAgent.match(/Opera Mini/i);
			},
			Windows: function() {
				return navigator.userAgent.match(/IEMobile/i);
			},
			any: function() {
				var isany = (isDevice.Android() || isDevice.BlackBerry() || isDevice.iOS() || isDevice.Opera() || isDevice.Windows());
				return (isany !== null);
			},
			touch: function() {
				return 'ontouchstart' in window || 'onmsgesturechange' in window;
			}
		};

		/**
		 * Initialiser for the device utilities - auto adds classes to the page.
		 *
		 * @memberof DDIGITAL.util.device
		 * @private
		 */
		init = function() {
			// uses isDevice in local scope, to add classes to the HTML tag
			// this means that we can use CSS to identify (handle) different user agents
			// assumes: jQuery dependancy + only one user-agent is possible

			if (isDevice.Android()) {
				$('html').addClass('d-android');
			} else if (isDevice.BlackBerry()) {
				$('html').addClass('d-blackberry');
			} else if (isDevice.iOS()) {
				$('html').addClass('d-ios');
			} else if (isDevice.Opera()) {
				$('html').addClass('d-opera');
			} else if (isDevice.Windows()) {
				$('html').addClass('d-windows');
			} else {
				$('html').addClass('d-other'); //something we're not checking, maybe desktop?
			}

			if (isDevice.any()) {
				$('html').addClass('d-any');
			}

			if (isDevice.touch()) {
				$('html').addClass('d-touch');
			}
		};

		return {
			is: isDevice,
			init: init
		};
	}());

}(DDIGITAL, jQuery));
// ==========================================================================
// FORMATTER
// ==========================================================================

/**
 * Deloitte Digital global namespace
 * @namespace DDIGITAL
 */

(function(NAMESPACE) {

	'use strict';

	/**
	 * Utility namespace
	 * @namespace DDIGITAL.util
	 * @memberof DDIGITAL
	 */
	NAMESPACE.util = NAMESPACE.util || {};

	/**
	 * Breakpoints for JavaScript. Works with the Deloitte Digital SCSS @bp mixin
	 *
	 * @namespace formatter
	 * @memberof DDIGITAL.util
	 * @version 1.0.0
	 * @author Deloitte Digital Australia deloittedigital@deloitte.com.au
	 */
	NAMESPACE.util.formatter = (function() {
		var toCurrency,
			abbrNum,
			roundNum,
			isNumber;

		/**
		 * Converts a number to a currency format.
		 *
		 * @memberof DDIGITAL.util.formatter
		 * @param  {Number} number The number to convert to a currency
		 * @param  {Number} [decimals=2] The number of decimals to display
		 * @param  {String} [decimalSep=.] String for the decimal separator
		 * @param  {String} [thousandsSep=,] String for the thousands separator
		 *
		 * @example
		 * NAMESPACE.util.formatter.toCurrency(1400, 2, '.', ','); // returns 1,400.00
		 */
		toCurrency = function(number, decimals, decimalSep, thousandsSep) {
			var c = isNaN(decimals) ? 2 : Math.abs(decimals), //if decimal is zero we must take it, it means user does not want to show any decimal
				d = decimalSep || '.', //if no decimal separator is passed we use the dot as default decimal separator (we MUST use a decimal separator)

				t = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep, //if you don't want to use a thousands separator you can pass empty string as thousandsSep value

				sign = (number < 0) ? '-' : '',

				//extracting the absolute value of the integer part of the number and converting to string
				i = parseInt(number = Math.abs(number).toFixed(c), 10) + '',

				j;
			j = ((j = i.length) > 3) ? j % 3 : 0;
			return sign + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + t) + (c ? d + Math.abs(number - i).toFixed(c).slice(2) : '');
		};

		/**
		 * Abbreviates a number to use k, m, b or t after it to help shorten the phsyical space taken up by the number
		 *
		 * @memberof DDIGITAL.util.formatter
		 * @param  {Number} number The number to abbreviate
		 * @param  {Number} [decimals=2] The number of decimals to display
		 *
		 * @example
		 * NAMESPACE.util.formatter.abbrNum(1000000, 2); // returns 1m
		 *
		 * @example
		 * DDIGITAL.util.formatter.abbrNum(999999, 0); // returns 1000k
		 *
		 * @example
		 * DDIGITAL.util.formatter.abbrNum(999999, 3); // returns 999.999k
		 *
		 * @example
		 * DDIGITAL.util.formatter.abbrNum(1999999, 5); // returns 2m
		 *
		 * @example
		 * DDIGITAL.util.formatter.abbrNum(1999999, 6); // returns 1.999999m
		 */
		abbrNum = function(number, decimals) {
			// 2 decimal places => 100, 3 => 1000, etc
			decimals = Math.pow(10, decimals);

			// Enumerate number abbreviations
			var abbrev = ['k', 'm', 'b', 't'];

			// Go through the array backwards, so we do the largest first
			for (var i = abbrev.length - 1; i >= 0; i -= 1) {

				// Convert array index to "1000", "1000000", etc
				var size = Math.pow(10, (i + 1) * 3);

				// If the number is bigger or equal do the abbreviation
				if (size <= number) {
					// Here, we multiply by decimals, round, and then divide by decimals.
					// This gives us nice rounding to a particular decimal place.
					number = Math.round(number * decimals / size) / decimals;

					// Add the letter for the abbreviation
					number += abbrev[i];

					break;
				}
			}

			return number;
		};

		/**
		 * Rounds a number to the specified number of decimal places
		 *
		 * @memberof DDIGITAL.util.formatter
		 * @param  {Number} number The number to round
		 * @param  {Number} [decimals=2] The number of decimals to display
		 *
		 * @example
		 * NAMESPACE.util.formatter.roundNum(3.3333, 2); // returns 3.33
		 *
		 * @example
		 * DDIGITAL.util.formatter.roundNum(3.3333, 0); // returns 3
		 *
		 * @example
		 * DDIGITAL.util.formatter.roundNum(666.666, 0); // returns 667
		 *
		 * @example
		 * DDIGITAL.util.formatter.roundNum(666.666, 2); // returns 666.67
		 */
		roundNum = function(number, decimals) {
			return Math.round(number * Math.pow(10, decimals)) / Math.pow(10, decimals);
		};

		/**
		 * Checks if a value is a number
		 *
		 * @memberof DDIGITAL.util.formatter
		 * @param  {Number} value The value to check if it's a number
		 * @return {Boolean}
		 */
		isNumber = function(value) {
			var isANum = (!isNaN(value - 0) && value !== null && value !== '' && value !== false);
			return isANum;
		};

		return {
			toCurrency: toCurrency,
			abbrNum: abbrNum,
			roundNum: roundNum,
			isNumber: isNumber
		};
	}());

}(DDIGITAL));
// ==========================================================================
// HELPER UTILITIES
// ==========================================================================

/**
 * Deloitte Digital global namespace
 * @namespace DDIGITAL
 */

(function(NAMESPACE, $) {

	'use strict';

	/**
	 * Utility namespace
	 * @namespace DDIGITAL.util
	 * @memberof DDIGITAL
	 */
	NAMESPACE.util = NAMESPACE.util || {};

	/**
	 * Helper functions to get older browsers more aligned with newer ones
	 *
	 * @namespace helpers
	 * @memberof DDIGITAL.util
	 * @version 1.0.0
	 * @author Deloitte Digital Australia deloittedigital@deloitte.com.au
	 */
	NAMESPACE.util.helpers = (function() {
		var _setResponsiveToStatic,
			_polyfillTextAreaMaxlength,
			_polyfillInputSiblingSelectors,
			_polyfillSelectorsForIE8,
			init;

		/**
		 * Set the page options to be static instead of responsive
		 *
		 * @memberof DDIGITAL.util.helpers
		 * @private
		 */
		_setResponsiveToStatic = function() {
			NAMESPACE.IS_RESPONSIVE = false;

			DD.bp.options({
				isResponsive: false
			});
		};

		/**
		 * Text area polyfill to allow maxlength attribute
		 *
		 * @memberof DDIGITAL.util.helpers
		 * @private
		 */
		_polyfillTextAreaMaxlength = function() {
			$('textarea[maxlength]').each(function() {
				var $textarea = $(this),
					maxlength = parseInt($textarea.attr('maxlength'), 10);

				if (isNaN(maxlength) === false) { //make sure it's a number
					$textarea.on('keyup.textareaMaxlength blur.textareaMaxlength', function() {
						var val = $(this).val();
						if (val.length > maxlength) {
							$(this).val(val.substr(0, maxlength));
						}
					});
				}
			});
		};

		/**
		 * Add css classes in replacement for selectors that aren't supported in IE8
		 *
		 * @memberof DDIGITAL.util.helpers
		 * @private
		 */
		_polyfillSelectorsForIE8 = function() {
			$('li:last-child, th:last-child, td:last-child, tr:last-child').addClass('last-child');
			$('tr:nth-child(2n)').addClass('odd');
		};

		/**
		 * Checkbox polyfill for sibling selectors
		 *
		 * @memberof DDIGITAL.util.helpers
		 * @private
		 */
		_polyfillInputSiblingSelectors = function() {
			var checkValue = function($elem) {
				var $label = $('label[for="' + $elem.attr('id') + '"]');
				if ($elem.prop('checked')) {
					$elem.add($label).addClass('is-checked');
				} else {
					$elem.add($label).removeClass('is-checked');
				}

				// We modify the label as well as the input because authors may want to style the labels based on the state of the chebkox, and IE7 and IE8 don't fully support sibling selectors.
				return $elem;
			};

			$('input:radio, input:checkbox').each(function() {
				var $self = $(this);

				if ($self.prop('type') === 'radio') {
					$('input[name="' + $self.prop('name') + '"]').on('change.checkboxPolyfill', function() {
						checkValue($self);
					});
				} else if ($self.prop('type') === 'checkbox') {
					$self.change(function() {
						checkValue($self);
					});
				}

				// Check value when polyfill is first called, in case a value has already been set.
				checkValue($self);
			});
		};

		/**
		 * Initialiser for the helpers
		 *
		 * @memberof DDIGITAL.util.helpers
		 */
		init = function() {
			if ($('.lt-ie10').length) {
				_polyfillTextAreaMaxlength();
			}

			if ($('.lt-ie9').length) {
				_setResponsiveToStatic();
				_polyfillSelectorsForIE8();
				_polyfillInputSiblingSelectors();
			}

			// add placeholder polyfill
			if ($.placeholder && !Modernizr.input.placeholder) {
				$('input, textarea').placeholder();
			}

			// focus on an element without actually scrolling the page to it
			$.fn.noScrollFocus = function() {
				var x = window.scrollX,
					y = window.scrollY;

				this.focus();

				if ($('.lt-ie10').length === 0) {
					window.scrollTo(x, y);
				}

				return this; //chainability
			};
		};

		return {
			init: init
		};
	}());

}(DDIGITAL, jQuery));
// ==========================================================================
// HELPER UTILITIES
// ==========================================================================
/**
 * Deloitte Digital global namespace
 * @namespace DDIGITAL
 */

(function(NAMESPACE, $) {

	'use strict';

	/**
	 * Utility namespace
	 * @namespace DDIGITAL.util
	 * @memberof DDIGITAL
	 */
	NAMESPACE.util = NAMESPACE.util || {};

	/**
	 * Bind print functionality to the page
	 *
	 * @namespace print
	 * @memberof DDIGITAL.util
	 * @version 1.0.0
	 * @author Deloitte Digital Australia deloittedigital@deloitte.com.au
	 */
	NAMESPACE.util.print = (function() {
		var SELECTORS,
			init;

		SELECTORS = {
			PRINT: '.js-print'
		};

		/**
		 * On click, open the print dialog
		 *
		 * @memberof DDIGITAL.util.print
		 */
		init = function() {
			$(document).on('click.print', SELECTORS.PRINT, function(e) {
				e.preventDefault();
				window.print();
			});
		};

		return {
			init: init
		};
	}());

}(DDIGITAL, jQuery));
// ==========================================================================
// SCROLL TO
// ==========================================================================

/**
 * Deloitte Digital global namespace
 * @namespace DDIGITAL
 */

(function(NAMESPACE, $) {

	'use strict';

	/**
	 * Utility namespace
	 * @namespace DDIGITAL.util
	 * @memberof DDIGITAL
	 */
	NAMESPACE.util = NAMESPACE.util || {};

	/**
	 * Breakpoints for JavaScript. Works with the Deloitte Digital SCSS @bp mixin
	 *
	 * @namespace scroll
	 * @memberof DDIGITAL.util
	 * @version 1.0.0
	 * @author Deloitte Digital Australia deloittedigital@deloitte.com.au
	 */
	NAMESPACE.util.scroll = (function() {
		var SELECTORS,
			DATA,
			scrollPage,
			scrollPageOnClick,
			init;

		SELECTORS = {
			SCROLLTO: '.scrollto'
		};

		DATA = {
			OFFSET: 'data-scroll-offset',
			TABTO: 'data-scroll-tabto',
			HASH: 'data-scroll-hash'
		};

		/**
		 * Scrolls the page
		 *
		 * @memberof DDIGITAL.util.scroll
		 * @param  {Number} top Top of the scren in pixels to scroll to.
		 * @param  {String} hash The hash to be updated in the URL upon completion of the scroll. Can be null.
		 * @param  {Number} _duration Time taken to animate to the new location. If null it will automatically determine the speed based on distance.
		 * @param  {Function} callback Callback function that is called on complete of the scroll.
		 */
		scrollPage = function(top, hash, _duration, callback) {
			var duration = _duration,
				isCallbackCalled = false;

			hash = (typeof (hash) === 'string') ? hash.substring(hash.indexOf('#') + 1) : null;

			if (duration === null) {
				var currentTop = $(document).scrollTop(),
					currentDistance = Math.abs(top - currentTop),
					maxDistance = 2000,
					maxDuration = 1000;

				if (currentDistance > maxDistance) {
					duration = maxDuration;
				} else {
					duration = (top > currentTop) ? (1 - currentTop / top) * maxDuration : (1 - top / currentTop) * maxDuration;
				}
			}

			$('html').velocity('stop').velocity('scroll', {
				offset: top,
				duration: duration,
				complete: function() {
					if (!isCallbackCalled) {
						isCallbackCalled = true;

						if (typeof (hash) === 'string') {
							if (window.history && window.history.pushState) {
								window.history.pushState(null, null, '#' + hash);
							} else {
								window.location.hash = hash;
							}
						}

						if (typeof (callback) === 'function') {
							callback();
						}
					}
				}
			});
		};

		/**
		 * Scrolls the page on click of a button
		 *
		 * @memberof DDIGITAL.util.scroll
		 * @private
		 * @param  {Event} top Top of the scren in pixels to scroll to.
		 */
		scrollPageOnClick = function(event) {
			var $btn = $(this),
				target = $btn.attr('href'),
				scrollOffset = parseInt($btn.attr(DATA.OFFSET), 10) || 0,
				tabTo = ($btn.attr(DATA.TABTO) === 'true'),
				updateHash = ($btn.attr(DATA.HASH) === 'false') ? false : true;

			// scroll to location
			target = target.substr(target.indexOf('#') + 1);

			var $target = $('#' + target),
				newHash = (updateHash) ? target : null;

			// we've assumed ID, if we can't find it, assume name attribute instead
			if ($target.length === 0) {
				$target = $('a[name="' + target + '"]');
			}

			// if can't be found, it's invalid - so treat it as a normal link
			if ($target.length === 0) {
				return;
			}

			// if the above checks have all passed, we can definitely scroll
			event.preventDefault();

			if (tabTo) {
				scrollPage($target.offset().top + scrollOffset, newHash, null, function() {
					$target.eq(0).noScrollFocus();
				});
			} else {
				scrollPage($target.offset().top + scrollOffset, newHash);
			}
		};

		/**
		 * Binds the appropriate click events to the appropriate buttons on the page
		 *
		 * @memberof DDIGITAL.util.scroll
		 */
		init = function() {
			$('body').off('click.scrollto').on('click.scrollto', SELECTORS.SCROLLTO, scrollPageOnClick);
		};

		return {
			page: scrollPage,
			init: init
		};
	}());

}(DDIGITAL, jQuery));
// ==========================================================================
// AUTOCOMPLETE
// ==========================================================================

(function(NAMESPACE, $) {

	'use strict';

	NAMESPACE.autocomplete = (function() {
		var init;

		init = function() {
			var options = {};

			$('.js-autocomplete').each(function(i, el) {
				var $el = $(el);
				$el.ddAutocomplete(options);
			});
		};

		return {
			init: init
		};

	}());

}(DDIGITAL, jQuery));
// ==========================================================================
// CAROUSEL
// ==========================================================================

(function(NAMESPACE, $) {

	'use strict';

	/**
	 * Creates a basic carousel slider using slick.js
	 *
	 * @namespace carousel
	 * @memberof DDIGITAL
	 * @version 0.0.1
	 * @author Deloitte Digital Australia
	 */
	NAMESPACE.carousel = (function() {
		var CONST,
			ATTRS,
			CLASSES,
			SELECTORS,
			_tweakDefaultUI,
			_checkCarouselTheme,
			_getCarouselOptions,
			_equaliseHeights,
			_enable,
			_disable,
			init;

		CONST = {
			DEFAULT_OPTIONS: {
				infinite: false,
				cssEase: 'ease-out',
				dots: true,
				speed: 400,
				adaptiveHeight: false
			}
		};

		ATTRS = {
			CAROUSEL_TYPE: 'data-carousel-type',
			HAS_CAROUSEL: 'has-carousel'
		};

		CLASSES = {
			IS_DARK: 'is-dark',
			DOTS_WRAPPER: 'slick-dots-wrapper',
			LAYOUT_PADDING: 'l-padding'
		};

		SELECTORS = {
			CAROUSEL: '.js-carousel',
			CAROUSEL_SLIDE: '.slick-slide',
			CAROUSEL_TRACK: '.slick-track',
			UI: {
				DOTS: '.slick-dots'
			}
		};

		/**
		 * Tweak default UI to suit designs
		 *
		 * @memberof DDIGITAL.carousel
		 * @param  {Object} $carousel jQuery object of the carousel item
		 * @param  {String} type Type of carousel to update - comes from the data attr on the carousel
		 * @private
		 */
		_tweakDefaultUI = function($carousel, type) {
			if (type === 'hero') {
				var $dotsWrap = $('<div />', {
					class: CLASSES.DOTS_WRAPPER,
					html: $('<div />', {
						class: CLASSES.LAYOUT_PADDING
					})
				});

				$carousel.find(SELECTORS.UI.DOTS).wrap($dotsWrap);
			}
		};

		/**
		 * Check the current slide and detect if it's light (default) or dark
		 *
		 * @memberof DDIGITAL.carousel
		 * @param  {Object} $carousel jQuery object of the carousel item
		 * @param  {Object} $currentSlide jQuery object of the currently selected slide
		 * @private
		 */
		_checkCarouselTheme = function($carousel, $currentSlide) {
			if ($currentSlide.hasClass(CLASSES.IS_DARK)) {
				$carousel.addClass(CLASSES.IS_DARK);
			} else {
				$carousel.removeClass(CLASSES.IS_DARK);
			}
		};

		/**
		 * Get the options for the carousel slider
		 *
		 * @memberof DDIGITAL.carousel
		 * @param  {Object} $carousel jQuery object of the carousel item
		 * @return {Object|Boolean} Returns an object of options or if the carousel needs to be disabled returns false
		 * @private
		 */
		_getCarouselOptions = function($carousel) {
			var type = $carousel.attr(ATTRS.CAROUSEL_TYPE) || 'default',
				options = {};

			options = {
				type: type,
				opts: CONST.DEFAULT_OPTIONS,
				events: {
					beforeChange: function(event, slick, currentSlide, nextSlide) {
						var $carousel = $(this),
							$currentSlide = $carousel.find(SELECTORS.CAROUSEL_SLIDE).eq(nextSlide);

						_checkCarouselTheme($carousel, $currentSlide);
					}
				}
			};

			if (type === 'hero' || type === 'default') {
				return options;
			}

			return false;
		};

		/**
		 * If required, equalise the heights of the carousel slides so
		 * it doesn't look weird between slides with various heights
		 *
		 * @memberof DDIGITAL.carousel
		 * @param  {Object} $carousel jQuery object of the carousel item
		 * @private
		 */
		_equaliseHeights = function($carousel) {
			var equaliseSlideHeights;

			equaliseSlideHeights = function() {
				$carousel.find(SELECTORS.CAROUSEL_SLIDE).css('height', '');

				var trackHeight = parseInt($carousel.find(SELECTORS.CAROUSEL_TRACK).height(), 10);

				$carousel.find(SELECTORS.CAROUSEL_SLIDE).css('height', trackHeight + 'px');
			};

			$(window).on('resize.carousel', $.debounce(250, equaliseSlideHeights));
			equaliseSlideHeights();
		};

		/**
		 * Enable the carousel
		 *
		 * @memberof DDIGITAL.carousel
		 * @param  {Object} $carousel jQuery object of the carousel item
		 * @private
		 */
		_enable = function($carousel) {
			var options = _getCarouselOptions($carousel);

			if ($carousel.data(ATTRS.HAS_CAROUSEL)) {
				console.warn('Carousel is already enabled');
				return;
			}

			$.extend(options, CONST.DEFAULT_OPTIONS, options.opts);

			$carousel.slick(options);

			$carousel.data(ATTRS.HAS_CAROUSEL, true);

			// check theme (dark or light) on load
			_checkCarouselTheme($carousel, $carousel.find(SELECTORS.CAROUSEL_SLIDE).eq(0));
			_tweakDefaultUI($carousel, options.type);

			if (options.adaptiveHeight !== true) {
				_equaliseHeights($carousel, options);
			}

			// apply events
			if (options.events) {
				for (var key in options.events) {
					if (options.events.hasOwnProperty(key)) {
						$carousel.on(key, options.events[key]);
					}
				}
			}
		};

		/**
		 * Disable the carousel
		 *
		 * @memberof DDIGITAL.carousel
		 * @param  {Object} $carousel jQuery object of the carousel item
		 * @private
		 */
		_disable = function($carousel) {
			if ($carousel.data(ATTRS.HAS_CAROUSEL)) {
				$carousel.slick('unslick');
			}

			$carousel.data(ATTRS.HAS_CAROUSEL, false);
		};

		/**
		 * Init the carousel with checking code to detect when changes should be made to it on screen resize
		 *
		 * @memberof DDIGITAL.carousel
		 */
		init = function() {
			var check;

			check = function() {
				$(SELECTORS.CAROUSEL).each(function(i, el) {
					var $carousel = $(el),
						options = _getCarouselOptions($carousel);

					if (options === false) {
						_disable($carousel);
						return;
					}

					if ($carousel.data(ATTRS.HAS_CAROUSEL) === false) {
						_enable($carousel);
					}
				});
			};

			$(SELECTORS.CAROUSEL).each(function(i, el) {
				$(el).data(ATTRS.HAS_CAROUSEL, false);
			});

			// add a size check using enquire here and call `check` on size change.
			// in order to change options depending on screensize, use the _getCarouselOptions
			// function to do a size check there. To disable the carousel completely return false
			// from _getCarouselOptions
			check();
		};

		return {
			init: init
		};
	}());

}(DDIGITAL, jQuery));
// ==========================================================================
// CONTENT MODULES
// ==========================================================================

(function(NAMESPACE, $) {

	'use strict';

	/**
	 * Manages the widths of content modules so instead of
	 * using complicated and very specific CSS to detect which size the
	 * module should be we can do it in JavaScript
	 * Essentially is helping us achieve element media queries
	 *
	 * @namespace contentModules
	 * @memberof DDIGITAL
	 * @version 0.0.5
	 * @author Deloitte Digital Australia
	 */
	NAMESPACE.contentModules = (function() {
		var CONST,
			CLASSES,
			SELECTORS,
			_removeAllWidthClasses,
			_setAllToDefault,
			_checkContentModuleSize,
			init;

		CONST = {
			SMALL_WIDTH: 350,
			MEDIUM_WIDTH: 500,
			LARGE_WIDTH: 800
		};

		CLASSES = {
			IS_SMALL: 'is-small',
			IS_MEDIUM: 'is-medium',
			IS_LARGE: 'is-large'
		};

		SELECTORS = {
			CM: '.cm, .hm'
		};

		/**
		 * Removes all the width classes from a content module
		 *
		 * @memberof DDIGITAL.contentModules
		 * @private
		 */
		_removeAllWidthClasses = function($cm) {
			$cm.removeClass(CLASSES.IS_SMALL)
				.removeClass(CLASSES.IS_MEDIUM)
				.removeClass(CLASSES.IS_LARGE);
		};

		/**
		 * Set all the contentModules to default (small) size
		 *
		 * @memberof DDIGITAL.contentModules
		 * @private
		 */
		_setAllToDefault = function() {
			$(SELECTORS.CM).each(function(i, el) {
				_removeAllWidthClasses($(el));
			});
		};

		/**
		 * Check the size of the content module and apply a class
		 * depending on the size of the module
		 *
		 * @memberof DDIGITAL.contentModules
		 * @private
		 */
		_checkContentModuleSize = function() {
			$(SELECTORS.CM).each(function(i, el) {
				var $cm = $(el),
					cmWidth;

				// remove the classes before checking the width
				_removeAllWidthClasses($cm);

				// get current width
				cmWidth = $cm.width();

				// set width into ranges
				if (cmWidth >= CONST.SMALL_WIDTH && cmWidth < CONST.MEDIUM_WIDTH) {

					$cm.addClass(CLASSES.IS_SMALL);
					$cm.removeClass(CLASSES.IS_MEDIUM)
						.removeClass(CLASSES.IS_LARGE);

				} else if (cmWidth >= CONST.MEDIUM_WIDTH && cmWidth < CONST.LARGE_WIDTH) {

					$cm.addClass(CLASSES.IS_MEDIUM);
					$cm.removeClass(CLASSES.IS_SMALL)
						.removeClass(CLASSES.IS_LARGE);

				} else if (cmWidth >= CONST.LARGE_WIDTH) {

					$cm.addClass(CLASSES.IS_LARGE);
					$cm.removeClass(CLASSES.IS_SMALL)
						.removeClass(CLASSES.IS_MEDIUM);
				}
			});
		};

		/**
		 * Initialise the content module size checking on page load
		 * - Requires enquire.js
		 *
		 * @memberof DDIGITAL.contentModules
		 */
		init = function() {
			if (typeof (enquire) !== 'object') {
				console.error('DDIGITAL.contentModules: enquire.js is required.');
			}

			// register listeners for only the required breakpoints
			// because xs->s is fluid responsive, we need to check on resize
			// else, just need to check at the adaptive responsive breakpoints
			enquire.register(DD.bp.get('0,xxs'), {
				match: _setAllToDefault
			}).register(DD.bp.get('xs,s'), {
				match: function() {
					// use throttle to ensure that it runs ASAP
					$(window).on('resize.cm', $.throttle(200, _checkContentModuleSize));
				},
				unmatch: function() {
					// unbind the throttle event because it's not needed outside of this breakpoint
					$(window).off('resize.cm', $.throttle(200, _checkContentModuleSize));
				}
			}).register(DD.bp.get('m,m'), {
				match: _checkContentModuleSize
			}).register(DD.bp.get('l,l'), {
				match: _checkContentModuleSize
			}).register(DD.bp.get('xl,xl'), {
				match: _checkContentModuleSize
			}).register(DD.bp.get('xxl'), {
				match: _checkContentModuleSize
			});

			// do initial large check because sometimes enquire doesn't run on load
			if (DD.bp.is('xs')) {
				_checkContentModuleSize();
			}
		};

		return {
			init: init
		};
	}());

}(DDIGITAL, jQuery));
// ==========================================================================
// DO WHEN - A jQuery plugin to do stuff when you want
// https://github.com/dkeeghan/jQuery-doWhen
// ==========================================================================

(function(NAMESPACE, $) {

	'use strict';

	NAMESPACE.doWhen = (function() {

		var init;

		init = function() {
			$(document).doWhen();
		};

		return {
			init: init
		};
	}());

}(DDIGITAL, jQuery));
// ==========================================================================
// EQUAL HEIGHTS
// ==========================================================================

(function(NAMESPACE, $) {

	'use strict';

	/**
	 * Manages heights of like items to keep them the same
	 * using the $.ddEqualHeights plugin
	 *
	 * @namespace equalHeights
	 * @memberof DDIGITAL
	 * @version 1.1.0
	 * @author Deloitte Digital Australia
	 */
	NAMESPACE.equalHeights = (function() {
		var init;

		/*
		 * @example
		 *
		 * $.fn.ddEqualHeights.addType('feature-item', {
		 *     itemsSelector: '.item',
		 *     sectionSelector: '.section',
		 *     numItemsPerRow: 2
		 * });
		 */

		// add the eqh-demo type
		$.ddEqualHeights.addType('eqh-demo', {
			numItemsPerRow: function() {
				// you can do tricky things here like look at layouts and check if the site is responsive or not.
				if ($('.l-three-column').length) {
					return (NAMESPACE.IS_RESPONSIVE) ? {'0,xs': 1, 's,l': 2, xl: 3} : 2;
				}

				return (NAMESPACE.IS_RESPONSIVE) ? {'0,xs': 1, 's,m': 2, 'l,xl': 3, xxl: 4} : 3;
			}
		});

		// add the sl-list types (2 items)
		$.ddEqualHeights.addType('sl-list-has-2-items', {
			itemsSelector: '.cm',
			numItemsPerRow: function() {
				return (NAMESPACE.IS_RESPONSIVE) ? {'0,m': 1, l: 2} : 2;
			}
		});

		// add the sl-list types (3 items)
		$.ddEqualHeights.addType('sl-list-has-3-items', {
			itemsSelector: '.cm',
			numItemsPerRow: function() {
				return (NAMESPACE.IS_RESPONSIVE) ? {'0,m': 1, l: 3} : 3;
			}
		});

		// add the sl-list-landing type
		$.ddEqualHeights.addType('sl-list-landing', {
			itemsSelector: '.cm',
			sectionSelector: '.title, > ul',
			numItemsPerRow: function() {
				return (NAMESPACE.IS_RESPONSIVE) ? {'0,m': 1, l: 2} : 2;
			}
		});

		// add the nav-onscreen type
		$.ddEqualHeights.addType('nav-onscreen', {
			numItemsPerRow: function() {
				return (NAMESPACE.IS_RESPONSIVE) ? {'0,m': 1, l: 4} : 4;
			}
		});

		// add the site map type
		$.ddEqualHeights.addType('cm-site-map', {
			numItemsPerRow: function() {
				return (NAMESPACE.IS_RESPONSIVE) ? {'0,xs': 1, 's,m': 2, l: 4} : 4;
			}
		});

		// add the homepage hero-links type
		$.ddEqualHeights.addType('hero-links', {
			itemsSelector: '.link-item',
			sectionSelector: '.content',
			numItemsPerRow: function() {
				return (NAMESPACE.IS_RESPONSIVE) ? {'0,xxs': 1, 'xs,m': 2, l: 4} : 4;
			}
		});

		init = function(scope) {
			var $scope = (scope) ? $('[data-heights-type]', scope) : $('[data-heights-type]');
			$scope.ddEqualHeights();
		};

		return {
			init: init
		};
	}());

}(DDIGITAL, jQuery));
// ==========================================================================
// EXPAND COLLAPSE
// ==========================================================================

(function(NAMESPACE, $) {

	'use strict';

	NAMESPACE.expandCollapse = (function() {
		var reset,
			init;

		reset = function($container, newIdSuffix) {
			var changeId = (typeof (newIdSuffix) === 'string');

			$container.find('.js-ec').trigger('destroy.ddExpandCollapse');

			if (changeId) {
				$container.find('.js-ec').each(function(i, el) {
					$(el).attr('id', $(el).attr('id') + newIdSuffix);
				});

				$container.find('.js-ec-link').each(function(i, el) {
					var href = $(el).attr('href');
					href = href.substring(href.indexOf('#'));

					$(el).attr('href', href + newIdSuffix);
				});
			}

			init($container);
		};

		init = function(scope) {
			var $ecScope = (scope) ? $('.js-ec', scope) : $('.js-ec');
			$ecScope.ddExpandCollapse();

			// grouped in JS
			var $ecScopeGrouped = (scope) ? $('.js-ec-grouped', scope) : $('.js-ec-grouped');
			$ecScopeGrouped.ddExpandCollapse({
				group: 'group-2'
			});

			// always scroll - even if on screen
			var $ecScopeScroll = (scope) ? $('.js-ec-scroll', scope) : $('.js-ec-scroll');
			$ecScopeScroll.ddExpandCollapse({
				durations: {
					scroll: 1000
				},
				animations: {
					scrollPage: function($container, options, callback) {
						var offset;

						// display the container to get the position
						$container.css({
							display: 'block'
						});

						offset = $container.offset().top + options.scrollOffset;

						// rehide the container
						$container.css({
							display: ''
						});

						// scroll the page
						$('html').velocity('stop').velocity('scroll', {
							offset: offset,
							duration: options.durations.scroll,
							complete: callback
						});
					}
				}
			});
		};

		return {
			reset: reset,
			init: init
		};
	}());

}(DDIGITAL, jQuery));
/* ==========================================================================
 * LINK ICONS
 *
 * Instructions for the client:
 *
 * The JavaScript module that adds external link icons is located at
 * `source/js/modules/_linkIcons.js`. External link icons will be added to
 * any link with target="_blank"
 *
 * After editing source/js/modules/_linkIcons.js, the developer should run
 * `middleman build` to generate the compiled JavaScript file: build/js/script.js.
 * For more information about Middleman, visit: http://middlemanapp.com/
 *
 * There is another option for disabling the external link icons. If there is a
 * section of the page where no link icons should appear, you can add a
 * classname of "link-icons-disabled" to an element and no links inside this
 * element will have a link icon added.
 *
 * ========================================================================== */


(function(NAMESPACE, $) {

	'use strict';

	NAMESPACE.linkIcons = (function() {

		// Do not add icons to links that are descendents of elements with the following selector
		var IGNORE_PATTERN = '.link-icons-disabled',
			_addClass,
			init;

		_addClass = function($link, classname, caption, doAlways) {
			var $icon,
				$iconText;

			doAlways = doAlways || false;

			// Do not proceed if one of the parent elements of this link has a classname of "link-icons-disabled"
			// This test is contained in the addClass function so that it is only run on elements with a matching URL because it involved a lot of DOM lookups
			if (!doAlways && ($link.filter(IGNORE_PATTERN).length > 0 || $link.closest(IGNORE_PATTERN).length > 0)) {
				return;
			}
			// If this link contains an image and nothing else, don't add icons to it
			if ($link.children().length === 1 && $link.text() === '' && $link.children('img').length === 1) {
				return;
			}

			$icon = $link.find('.link-icon');

			if ($icon.length === 0) {
				if ($link.hasClass('link-caret-block')) {
					$link.append('<span class="link-icon"></span>');
				} else {
					var justText = $link.clone().children().remove().end().text(),
						linkWords = justText.split(' '),
						lastWord = linkWords.pop();

					// wrap the icon around the last word
					lastWord = '<span class="link-icon">' + lastWord + '<span class="vh"></span></span>';
					linkWords.push(lastWord);
					linkWords = linkWords.join(' ');

					var html = $link.html();
					$link.html(html.replace(justText, linkWords));

					$icon = $link.find('.link-icon');
				}
			}

			$iconText = $icon.find('.vh');

			$iconText.text($iconText.text() + caption);
			$link.addClass(classname);
		};

		init = function(scope) {
			var $scope = (scope) ? $('a', scope) : $('a');

			$scope.each(function() {
				var $link = $(this),
					href = $link.attr('href');

				if (!href) {
					return;
				}

				if ($link.hasClass('link-caret')) {
					_addClass($link, 'link-caret', '', true);
				}

				/*
				if ($link.attr('target') === '_blank') {
					_addClass($link, 'link-external', ' (External link)');
				}
				*/

				/*
				if (href.substr(href.length - 4, 4) === '.pdf') {
					_addClass($link, 'link-pdf', ' (PDF)');
				}
				*/
			});

		};

		return {
			init: init
		};
	}());

}(DDIGITAL, jQuery));
// ==========================================================================
// MODAL WINDOWS
// ==========================================================================

(function(NAMESPACE, $) {

	'use strict';

	NAMESPACE.modal = (function() {
		var _addDynamicModalConfiguration,
			openModal,
			closeModal,
			isModalOpen,
			init;

		_addDynamicModalConfiguration = function() {
			// DEMO Page example
			$.ddModals.dynamicModal.addType('modal-dynamic-example', {
				mq: '0,s',
				type: 'modal-dynamic-example',
				callback: function($modal) {
					NAMESPACE.expandCollapse.reset($modal, '-inmodal');
				}
			});

			// SEARCH/COLLETION Pages for DSP
			$.ddModals.dynamicModal.addType('modal-collection-header-sort', {
				mq: '0,xl',
				type: 'modal-collection-header-sort',
				callback: function() {}
			});

			// SEARCH/COLLETION Pages for DSP
			$.ddModals.dynamicModal.addType('modal-search-facets', {
				mq: '0,s',
				type: 'modal-search-facets',
				callback: function($modal) {
					NAMESPACE.expandCollapse.reset($modal, '-inmodal');
				}
			});
		};

		openModal = function(id, opener, callback) {
			$.ddModals.open(id, opener, callback);
		};

		closeModal = function(callback) {
			$.ddModals.close(callback);
		};

		isModalOpen = function() {
			return $.ddModals.isOpen();
		};

		init = function() {
			_addDynamicModalConfiguration();

			// Initialiser
			$('.js-modal-container').ddModals({
				repositionModalOnEvent: 'collapsed.ddExpandCollapse expanded.ddExpandCollapse',
				preOpen: function() {
					// close navigation
					$('.js-nav-offscreen').trigger('close.offscreen');
				}
			});
		};

		return {
			open: openModal,
			close: closeModal,
			isOpen: isModalOpen,
			init: init
		};

	}());

}(DDIGITAL, jQuery));
/* ==========================================================================
 * MAGICAL AMAZING RESPONSIVE TABLES
 *
 * This module applies to tables with a classname of "js-responsive-table".
 * By default, it will apply a horizontal scroll to tables.
 * It also has an option to display an interface for toggling table columns.
 *
 * To enable column toggling:
 *  - Add a classname of "responsive-table-column-toggling" to the table
 *  - Add a classname of "column-persist" to any <th> elements for columns that
 *    should never ever be hidden (such as the title column)
 *  - Add a classname of "column-important" to any <th> elements for columns that
 *    will be displayed by default on all breakpoints (including extra small and below)
 *  - Add a classname of "column-optional" to any <th> elements for columns that
 *    will be displayed by default on small-and-above breakpoints
 *  - <th> elements without one of the above classnames will only be displayed
 *    by default on the large-and-above breakpoint
 *
 * Table column toggling supports tables with colspan attributes in the head
 * <th> elements, but not in the body <td> elements.
 *
 * ========================================================================== */


(function(NAMESPACE, $) {

	'use strict';

	NAMESPACE.responsiveTable = (function() {
		var CLASSES = {
			MENU_BUTTON: 'responsive-table-toggle-menu-btn cta is-secondary is-small',
			DO_COLUMN_TOGGLE: 'responsive-table-column-toggling'
		};

		var init = function() {
			$('.js-responsive-table').each(function(i, el) {
				var options = {
					classes: {
						columnToggle: {
							menuButton: CLASSES.MENU_BUTTON
						}
					}
				};

				if ($(el).hasClass(CLASSES.DO_COLUMN_TOGGLE)) {
					options.columnToggle = true;
				}

				$(el).ddResponsiveTable(options);
			});
		};

		return {
			init: init
		};

	}());

}(DDIGITAL, jQuery));
// ==========================================================================
// TABS
// ==========================================================================

(function(NAMESPACE, $) {

	'use strict';

	// tabs functionality, including the ability to have tabs in tabs
	NAMESPACE.tabs = (function() {

		var SELECTORS,
			init;

		SELECTORS = {
			TABS: '.js-tabs',
			TAB_EC_TITLE: '.tab-ec-title',
			TAB_EC_CONTENT: '.tab-ec-content'
		};

		init = function() {
			$(SELECTORS.TABS).each(function(i, el) {
				var $tabsContainer = $(el);

				$tabsContainer.ddTabs();

				// if the tab has an expand/collapse title, listen for when the tab changes
				if ($tabsContainer.find(SELECTORS.TAB_EC_TITLE).length > 0) {

					$tabsContainer.on('tabChanged.ddTabs', function(event, id, $activeTab, $tabsList) {
						event.stopPropagation();

						$tabsList.find(SELECTORS.TAB_EC_CONTENT).trigger('collapse.ddExpandCollapse');
						$activeTab.find(SELECTORS.TAB_EC_CONTENT).trigger('expand.ddExpandCollapse');
					});

				}
			});

		};

		return {
			init: init
		};
	}());

}(DDIGITAL, jQuery));
/* ==========================================================================
 * TOGGLE SEARCH BAR
 * ========================================================================== */


(function(NAMESPACE, $) {

	'use strict';

	NAMESPACE.togglePopover = (function() {

		var ATTRS,
			CLASSES,
			SELECTORS,
			_togglePopover,
			_showPopover,
			_hidePopover,
			init;

		ATTRS = {
			POPOVER_ID: 'data-popover-id',
			IS_ANIMATING: 'popover-is-animating'
		};

		CLASSES = {
			IS_ACTIVE: 'is-active',
			IS_READY: 'is-ready'
		};

		SELECTORS = {
			TOGGLE: '.js-popover-toggle'
		};

		_hidePopover = function($popover, $button) {
			if ($popover.data(ATTRS.IS_ANIMATING) === true) {
				return;
			}

			$popover.data(ATTRS.IS_ANIMATING, true);

			$button.removeClass(CLASSES.IS_ACTIVE);

			$popover.removeClass(CLASSES.IS_ACTIVE)
				.find('input[type="search"]').val('');

			DD.a11y.tabInsideContainer.unset();
			DD.a11y.onEscape.unset();
			DD.a11y.onClickOutside.unset();

			// focus back on the original button when closing
			$button.focus();

			// hide shade
			if ($.ddShade.isAnimating()) {
				// if the shade is already animating we shouldn't hide it
				$.ddShade.setBehindHeader(false);
				$popover.removeClass(CLASSES.IS_READY);
				$popover.data(ATTRS.IS_ANIMATING, false);
			} else {
				// fade out and hide the shade entirely
				$.ddShade.opacity(0, 150, true, function() {
					$.ddShade.setActive(false);
					$.ddShade.setBehindHeader(false);

					$popover.removeClass(CLASSES.IS_READY);

					$popover.data(ATTRS.IS_ANIMATING, false);
				});
			}
		};

		_showPopover = function($popover, $button) {
			if ($popover.data(ATTRS.IS_ANIMATING) === true) {
				return;
			}

			$popover.data(ATTRS.IS_ANIMATING, true);

			var hideCurrentPopover = function() {
				_hidePopover($popover, $button);
			};

			// add class to toggle display state
			$popover.addClass(CLASSES.IS_READY);
			$button.addClass(CLASSES.IS_ACTIVE);

			// setup shade
			$.ddShade.setBehindHeader(true);
			$.ddShade.setActive(true);

			// bind close to escape button
			DD.a11y.onEscape.set(hideCurrentPopover);

			// bind close on outside click
			DD.a11y.onClickOutside.set($popover, hideCurrentPopover);

			// display shade
			$.ddShade.opacity(0.75, 150, false, function() {
				$popover.addClass(CLASSES.IS_ACTIVE);

				DD.a11y.tabInsideContainer.set($popover, true);
				$popover.find('input[type="search"]').focus();

				$popover.data(ATTRS.IS_ANIMATING, false);
			});
		};

		_togglePopover = function(event) {
			event.preventDefault();

			var $button = $(this),
				popoverId = $button.attr(ATTRS.POPOVER_ID),
				$popover = $(document.getElementById(popoverId));

			if ($popover.hasClass(CLASSES.IS_ACTIVE)) {
				_hidePopover($popover, $button);

				return;
			}

			_showPopover($popover, $button);
		};

		init = function() {
			$(document).on('click.togglePopover', SELECTORS.TOGGLE, _togglePopover);
		};

		return {
			init: init
		};
	}());

}(DDIGITAL, jQuery));
// ==========================================================================
// TOOLTIPS
// ==========================================================================

(function(NAMESPACE, $) {

	'use strict';

	NAMESPACE.tooltip = (function() {
		var init;

		init = function() {
			$('.js-tooltip').ddTooltip();
		};

		return {
			init: init
		};
	}());

}(DDIGITAL, jQuery));
// ==========================================================================
// FORM VALIDATION
// ==========================================================================

(function(NAMESPACE) {

	'use strict';

	/**
	 * Form validation with jQuery validate
	 *
	 * @namespace validate
	 * @memberOf DDIGITAL
	 */
	NAMESPACE.validate = (function() {

		var CONST = {
				TPL_REQUIRED: '<em>*<span class="vh">Required field</span></em>',
				CLASSES: {
					ERROR: 'is-error',
					VALID: 'is-valid',
					IGNORE: 'v_ignore',
					IGNORELIST: '.v_ignore, :hidden, [disabled]',
					STATUS_MSG: 'status-msg',
					STATUS_ICON: 'status-icon',
					IS_DISPLAYED: 'is-displayed',
					CTRLHOLDER: 'ctrl-holder',
					CTRLSHOLDER: 'ctrls-holder'
				}
			},
			init,
			ignore,
			valid,
			isInGroup,
			isGroupValid,
			getCtrlsInGroup,
			getClasses,
			getGroupsInForm,
			isFirstInGroup,
			getLabelText,
			renderErrorSummary,
			_clearErrorSummary,
			validateAndSubmit,
			reset,
			_getRules,
			_decorateRules;

		/**
		 * Renders the error summary list into the DOM and manages the display
		 *
		 * @todo revise this monster
		 * @param {object} validator jQuery validation instance
		 * @param {array} errors Array of error objects
		 * @private
		 * @inner
		 * @memberOf DDIGITAL.validate
		 */
		renderErrorSummary = function(validator, errors) {
			var $summaryContainer = $(validator.currentForm).find('.fn_validate_summary'),
				$errorSummary = $summaryContainer.find('.form-summary.is-error').first(),
				_renderError,
				_scrollIntoView,
				_renderSummary,
				$ul = $(document.createElement('ul'));

			errors = errors || [];

			// Exit if there is no container
			if ($summaryContainer.length === 0) {
				return;
			}

			/**
			 * Renders a single error message a <li> and appends it to the <ul>
			 * @param {object} error
			 * @private
			 * @ignore
			 */
			_renderError = function(error) {
				var $ctrl = $(error.element),
					$ctrlHolder = NAMESPACE.formDecorator.getCtrlHolder($ctrl),
					label = getLabelText($ctrlHolder),
					$li = $(document.createElement('li')),
					$link = $(document.createElement('a')),
					$labelEl = $(document.createElement('strong'));

				$labelEl.text(label.formLabel + ': ');

				$link
					.attr('href', '#' + $ctrl.attr('id'))
					.append($labelEl)
					.append(document.createTextNode(error.message));

				$li.html($link);

				$ul.append($li);
			};

			/**
			 * Scrolls error summary into view
			 * @private
			 * @ignore
			 */
			_scrollIntoView = function() {
				NAMESPACE.util.scroll.page($summaryContainer.offset().top - 10, null, 250, function() {
					// Focus the error summary container
					$errorSummary
						.attr('tabindex', '-1')
						.focus();

					$errorSummary.find('a')
						.off('click.validationSummary')
						.on('click.validationSummary', function(evt) {
							var $link = $(this),
								target = $link.attr('href');

							evt.preventDefault();

							target = target.substr(target.indexOf('#') + 1);

							if ($('#' + target).length > 0) {
								NAMESPACE.util.scroll.page($('#' + target).closest('.ctrl-holder').offset().top, null, 250, function() {
									$('#' + target).get(0).focus();
								});
							}
						});
				});
			};

			// Are we rendering jQuery validate errors? Or those passed in as the 2nd argument?
			if (errors.length === 0) {
				// Iterate error list and check for each control whether it should be ignored as a result of it being
				// hidden, tagged with v_ignore, or disabled
				$.each(validator.errorList, function(key, error) {
					var $ctrl = $(error.element),
						_isInGroup = isInGroup($ctrl);

					// Should we ignore this control?
					// @todo Determine whether this ignore business is still relevant
					if (ignore($ctrl) === false) {
						// Only render one message per group
						if ((_isInGroup && isFirstInGroup($ctrl)) || !_isInGroup) {
							_renderError(error);
						}
					}
				});
			} else {
				// Render errors transmitted from the server side
				$.each(errors, function(key, value) {
					var $ctrl = $('#' + value.ControlId);

					_renderError({
						element: $ctrl[0],
						message: value.ErrorMsg
					});
				});
			}

			/**
			 * Injects the error summary into DOM
			 * @private
			 * @ignore
			 */
			_renderSummary = function() {
				// Write the H3 to the error summary container

				var $heading = $(document.createElement('h3')),
					$count = $(document.createElement('strong')),
					errorCount = $ul.children().length,
					errorMessage = errorCount > 1 ? ' fields contain errors' : ' field contains an error';

				$count.text(errorCount);

				$heading
					.addClass('form-summary-title')
					.append(document.createTextNode('The following '))
					.append($count)
					.append(document.createTextNode(errorMessage));

				$errorSummary.html($heading);

				// Append the error list
				$errorSummary.append($ul);
			};

			// Is there already a summary on screen?
			if ($errorSummary.length > 0) {
				_renderSummary();
				// Scroll error summary container into view and place focus on it
				_scrollIntoView();
			} else {
				// Create a container for the error summary
				$errorSummary = $(document.createElement('div'))
					.addClass('form-summary')
					.addClass('is-error')
					.hide();
				$summaryContainer.append($errorSummary);

				_renderSummary();
				// Scroll error summary container into view and place focus on it
				_scrollIntoView();

				// Animate the summary into view
				$errorSummary
					.css('opacity', 0)
					.slideDown(250, function() {
						$errorSummary.animate({
							opacity: 1
						}, 250);
					});
			}
		};

		/**
		 * Clears error summary from the DOM
		 *
		 * @param {object} validator jQuery validation instance
		 * @private
		 * @inner
		 * @memberOf DDIGITAL.validate
		 */
		_clearErrorSummary = function(validator) {
			$(validator.currentForm)
				.find('.fn_validate_summary')
				.empty();
		};

		/**
		 * Returns true if control is part of a group
		 *
		 * @memberOf DDIGITAL.validate
		 * @inner
		 * @param {object} $ctrl Reference to control element
		 * @return {boolean}
		 */
		isInGroup = function($ctrl) {
			var validator = $.data($ctrl.closest('.fn_validate')[ 0 ], 'validator');

			return $.map(validator.groups, function(groupName, controlName) {
				if (controlName.indexOf($ctrl.attr('id')) >= 0) {
					return true;
				} else {
					return false;
				}
			}).indexOf(true) !== -1;
		};

		/**
		 * Returns true if control is the first in a group
		 *
		 * @memberOf DDIGITAL.validate
		 * @param {object} $ctrl Reference to control element
		 * @return {boolean}
		 */
		isFirstInGroup = function($ctrl) {
			var $ctrls = getCtrlsInGroup($ctrl);
			return $ctrls.first().is($ctrl);
		};

		/**
		 * Returns label text for control
		 *
		 * @todo Revise this monster (consider using xpath for this?)
		 *
		 * @memberOf DDIGITAL.validate
		 * @param {object} $ctrlHolder Control ancestor holding .ctrl-holder class
		 * @return {{formLabel: string, errorLabel: string, uniqueGroupLabel: string}}
		 */
		getLabelText = function($ctrlHolder) {
			var $elem = $ctrlHolder.find('> label:visible, > .label:visible'),
				$errorElem = $ctrlHolder.find('span.is-error:visible').length > 0 ?
					$ctrlHolder.find('span.is-error:visible') :
					$ctrlHolder.find('span.is-error:first'),
				groupLabel = $ctrlHolder.closest('fieldset').find('legend, .legend').text(),
				specialLabel,
				getCleanText,
				label;

			getCleanText = function($elem) {
				return $elem.clone().children().remove().end().text().replace(/^\s+|\s+$/g, '');
			};

			if ($ctrlHolder.find('> label, > .label').length === 0) {
				if ($ctrlHolder.closest('.form-questions').length > 0) {
					// $elem = $ctrlHolder.closest('li').find('h3');
					specialLabel = 'Question ' + parseInt($ctrlHolder.closest('li').index() + 1, 10);
				} else if ($ctrlHolder.find('> .options > li').length === 1) {
					$elem = $ctrlHolder.find('> .options > li label');
				}
			}

			// Grouped controls have a different structure
			if ($ctrlHolder.closest('.ctrls-holder').length > 0) {
				$elem = $ctrlHolder.closest('.ctrls-holder').find('> label, > .label');
				$errorElem = $ctrlHolder.closest('.ctrls-holder').find('span.is-error:visible');
			}

			label = specialLabel || getCleanText($elem);

			// Clone the item, remove the children, and get the original text
			return {
				formLabel: label,
				errorLabel: $errorElem.text(),
				uniqueGroupLabel: label + '_' + groupLabel
			};
		};

		/**
		 * Checks if any of the controls in the group has an associated error in the errorMap
		 *
		 * @memberOf DDIGITAL.validate
		 * @inner
		 * @param {object} $group Reference to .ctrls-holder/.fn_validate_group container
		 * @return {bool}
		 */
		isGroupValid = function($group) {
			var valid = true,
				$ctrls = $group.find(':input, select'),
				validator = $.data($group.closest('.fn_validate')[ 0 ], 'validator');

			// Iterate controls in group
			$ctrls.each(function() {
				var id = $(this).attr('id');

				// Check if the errorMap has a corresponding error
				if (validator.errorMap.hasOwnProperty(id)) {
					valid = false;
				}
			});

			return valid;
		};

		/**
		 * Returns a jQuery collection of controls
		 *
		 * @memberOf DDIGITAL.validate
		 * @param {object} $ctrl Reference to control element
		 * @return {*}
		 */
		getCtrlsInGroup = function($ctrl) {
			var $ctrlHolder = NAMESPACE.formDecorator.getCtrlHolder($ctrl, false);
			return $ctrlHolder.find(':input, select');
		};

		/**
		 * @memberOf DDIGITAL.validate
		 * Returns the CLASSES object as part of the CONST hash
		 * @return {object}
		 */
		getClasses = function() {
			return CONST.CLASSES;
		};

		/**
		 * Checks if a control should be ignored for the purpose of validation
		 *
		 * @memberOf DDIGITAL.validate
		 * @param $ctrl Reference to control element
		 * @return {boolean}
		 */
		ignore = function($ctrl) {
			//var $ctrlHolder = NAMESPACE.formDecorator.getCtrlHolder($ctrl);
			if ($ctrl.is(':hidden') || $ctrl.is(':disabled')) {
				return true;
			}
			return false;
		};

		/**
		 * Returns true if a logical form is considered to be valid
		 * It takes into consideration whether any elements are currently disabled and/or hidden
		 *
		 * @memberOf DDIGITAL.validate
		 * @inner
		 * @param {object} validator Instance of jQuery validate
		 * @returns {boolean}
		 */
		valid = function(validator) {
			var valid = true;

			$.each(validator.errorList, function(key, value) {
				var $ctrl = $(value.element);

				if (ignore($ctrl) === false) {
					valid = false;
				}
			});

			return valid;
		};

		/**
		 * Produces a groups hash to pass into the jQuery validate initializer
		 *
		 * @memberOf DDIGITAL.validate
		 * @param {object} $form Reference to .fn_validate container
		 * @return {hash}
		 */
		getGroupsInForm = function($form) {
			var groups = {};

			$form.find('.fn_validate_group').each(function() {
				var $group = $(this),
					$ctrls = $group.find(':input, select'),
					key = $ctrls.first().attr('id'),
					ids = [];

				$ctrls.each(function() {
					ids.push($(this).attr('id'));
				});

				groups[key] = ids.join(' ');
			});

			return groups;
		};

		/**
		 * Validate form and trigger submission
		 *
		 * @todo Rewrite this using the submitHandler callback
		 *
		 * @memberOf DDIGITAL.validate
		 * @inner
		 * @param {object} evt Click event from submit button
		 * @param {object} $form Reference to closest container with the .fn_validate class
		 */
		validateAndSubmit = function(evt, $form) {
			var validator = $.data($form[0], 'validator'),
				submit = true;

			// Trigger validation
			$form.valid();

			// Check for form validity taking form state into account
			if (valid(validator)) {

				if (submit === true) {
					// If the logical form is not an actual form, grab a reference to the parent form
					if ($form[0].tagName.toLowerCase() !== 'form') {
						$form = $form.closest('form');
					}

					// Trigger event for
					$form.trigger('submit.valid', evt);

					// Submit the form
					//$form[0].submit();
				}
			} else {
				evt.preventDefault();
				// Render error summary
				renderErrorSummary(validator);
			}
		};

		/**
		 * Resets logical form
		 *
		 * @memberOf DDIGITAL.validate
		 * @inner
		 * @param {object} $form Reference to logical form element
		 * @param {object} $form Reference to element with .fn_validate class
		 */
		reset = function($form) {
			// Clear error summary
			_clearErrorSummary($.data($form[0], 'validator'));

			// Reset form through jQuery validate API
			$form.validate().resetForm();
		};

		/**
		 * Looks for a rules configuration object as part of the server side message protocol.
		 *
		 * @see http://jqueryvalidation.org/validate/
		 * @private
		 * @memberOf DDIGITAL.validate
		 * @param $form Reference to closest container with the .fn_validate class
		 * @return {hash}
		 */
		_getRules = function($form) {
			var formId = $form.data('form-id'),
				rules = {};

			try {
				rules = window.forms[formId].rules;
			} catch (error) {}

			return rules;
		};

		/**
		 * Updates control state to reflect the required rules in case this was added from the server side
		 * message protocol instead of by adding a class name in the DOM
		 *
		 * @memberOf DDIGITAL.validate
		 * @param {object} rules
		 * @private
		 */
		_decorateRules = function(rules) {
			try {
				$.each(rules, function(key, value) {
					if (value === 'required') {
						NAMESPACE.formDecorator.markAsRequired($('[name="' + key + '"]'));
					} else if (typeof value !== 'string') {
						$.each(value, function(_key, _value) {
							if (_key === 'required' && _value === true) {
								NAMESPACE.formDecorator.markAsRequired($('[name="' + key + '"]'));
							}
						});
					}
				});
			} catch (error) {};
		};

		/**
		 * Initializes module
		 * @memberOf DDIGITAL.validate
		 * @param {object} $scope Reference to container; defaults to $('body')
		 */
		init = function($scope) {
			$scope = $scope || $('body');

			// Iterate logical forms within the requested scope
			$scope.find('.fn_validate')
				.each(function() {
					var $form = $(this),
						rules = _getRules($form);

					// Mark up controls that are required as required fields
					_decorateRules(rules);

					// Initialise jQuery validate against logical form container
					$form.validate({
						errorClass: CONST.CLASSES.ERROR,
						validClass: CONST.CLASSES.VALID,
						ignore: CONST.CLASSES.IGNORELIST,
						wrapper: 'div',
						errorElement: 'span',
						groups: getGroupsInForm($form),
						rules: rules,

						/**
						 * Only called once per group if group is defined as part of the init
						 *
						 * @ignore
						 * @param {HTMLElement} error
						 * @param {HTMLElement} ctrl
						 */
						errorPlacement: function(error, ctrl) {
							NAMESPACE.formDecorator.placeError($(ctrl), $(error));
						},

						/**
						 * Triggered for each control regardless of whether it belongs to a group
						 *
						 * @ignore
						 * @param {HTMLElement} ctrl
						 * @param {string} errorClass
						 * @param {string} validClass
						 */
						highlight: function(ctrl) {
							NAMESPACE.formDecorator.highlight($(ctrl));
						},

						/**
						 * Triggered for each control regardless of whether it belongs to a group
						 *
						 * @ignore
						 * @param {HTMLElement} ctrl
						 * @param {string} errorClass
						 * @param {string} validClass
						 */
						unhighlight: function(ctrl, errorClass, validClass) {
							NAMESPACE.formDecorator.unhighlight($(ctrl), errorClass, validClass);
						},

						/**
						 * Triggered once a control is considered top be valid
						 *
						 * @ignore
						 * @param {HTMLElement} error
						 * @param {HTMLElement} ctrl
						 */
						success: function(error, ctrl) {
							var $ctrl = $(ctrl),
								$error = $(error);

							// Check if control is in group
							if (isInGroup($ctrl)) {
								// Check if all controls in group are valid before marking an element as valid
								if (isGroupValid(NAMESPACE.formDecorator.getCtrlHolder($ctrl, false))) {
									NAMESPACE.formDecorator.markAsValid($error, $ctrl);
								}
							} else {
								// Simply mark the control as valid
								NAMESPACE.formDecorator.markAsValid($error, $ctrl);
							}
						}
					});

					// Bind event handler to submit buttons/links
					$form.find('.fn_validate_submit')
						.on('click.validate', function(evt) {
							//evt.preventDefault();
							// Validate and submit if deemed valid
							validateAndSubmit(evt, $form, $(this));
						});

					// Catch enter key submission
					$form.keydown(function(evt) {
						if (evt.which === 13) {
							//evt.stopPropagation();
							//evt.preventDefault();
							$form.find('.fn_validate_submit')
								.trigger('click.validate');
							// Validate and submit if deemed valid
							//validateAndSubmit(evt, $form, $(this));
						}
					});

					// Bind event handler to reset button for logical form
					$form.find('.fn_validate_reset')
						.on('click.validate', function(evt) {
							evt.preventDefault();
							reset($form, $(this));
						});

					// Also, validate radio buttons 'onfocusout'
					$form.find('input[type="radio"]')
						.on('click.validate', function() {
							// Trigger validation on the currentTarget
							$form.validate().element(this);
						});
				});

			// Forcing maxlength for IE
			try {
				if ($.browser.msie && $.browser.version < 10) {
					$('.v_maxlength').each(function() {
						var $textarea = $(this),
							maxlength = parseInt($textarea.attr('maxlength'), 10);

						if (/^[0-9]+$/.test(maxlength)) {
							$textarea.on('keyup blur', function() {
								var val = $(this).val();
								if (val.length > maxlength) {
									$(this).val(val.substr(0, maxlength));
								}
							});
						}
					});
				}
			} catch (error) {}
		};

		return {
			init: init,
			ignore: ignore,
			isInGroup: isInGroup,
			isGroupValid: isGroupValid,
			getCtrlsInGroup: getCtrlsInGroup,
			getClasses: getClasses,
			renderErrorSummary: renderErrorSummary,
			reset: reset,
			getGroupsInForm: getGroupsInForm,
			validateAndSubmit: validateAndSubmit,
			getLabelText: getLabelText,
			valid: valid
		};

	}());

}(DDIGITAL));
/* =============================================================================
   ____ _       _           _   _   _                _             _   _
  / ___| | ___ | |__   __ _| | | | | | ___  __ _  __| | ___ _ __  | \ | | __ ___   __
 | |  _| |/ _ \| '_ \ / _` | | | |_| |/ _ \/ _` |/ _` |/ _ \ '__| |  \| |/ _` \ \ / /
 | |_| | | (_) | |_) | (_| | | |  _  |  __/ (_| | (_| |  __/ |    | |\  | (_| |\ V /
  \____|_|\___/|_.__/ \__,_|_| |_| |_|\___|\__,_|\__,_|\___|_|    |_| \_|\__,_| \_/
   ========================================================================== */


(function(NAMESPACE, $) {

	'use strict';

	NAMESPACE.globalHeaderNav = (function() {
		var init;

		// initialiser
		init = function() {

        var $links = $('.ca-global-header-links a').clone();
        var wrapLinks = [];

        $links.each(function() {
          var $this = $(this);
          $this.addClass('nav-lvl1');
          wrapLinks.push($this);
        });

        $('.ca-global-links-aside').append(wrapLinks);
		};

		return {
			init: init
		};

	}());

}(DDIGITAL, jQuery));
/* =============================================================================
   OFFSCREEN NAV
   ========================================================================== */


(function(NAMESPACE, $) {

	'use strict';

	NAMESPACE.navOffscreen = (function() {
		var init;

		// initialiser
		init = function() {
			// if you use animationType: 'side' you need to move the shade inside the page wrap
			$('.js-offscreen').ddOffscreen();
		};

		return {
			init: init
		};

	}());

}(DDIGITAL, jQuery));
/* =============================================================================
   HEADER NAV
   ========================================================================== */


(function(NAMESPACE, $) {

	'use strict';

	NAMESPACE.navOnscreen = (function() {

		var OPTIONS,
			CONST,
			_isInit = false,
			_dropdowns = [],
			enableDropdowns,
			disableDropdowns,
			isInit,
			init;

		// options
		OPTIONS = {
			DROPDOWNS: [
				'.nav-onscreen .nav',
				'.other-sites'
			],
			HAS_BACKGROUND: true,
			BACKGROUND_OPACITY: 0.75,
			TIMEOUT_DURATION: 250,
			TRANSITION_DURATION: 300,
			DEBUG_MODE: false
		};

		// constants
		CONST = {
			CLASS: {
				HOVER: 'is-hover', // Used to show the dropdown
				HOVERING: 'is-hovering', // Used to apply style to the link
				NAV_BG: 'dropdown-nav-bg'
			},
			IS_LT_IE9: ($('.lt-ie9').length > 0)
		};

		// enable the navigation (used by enquire to turn the navigation on/off)
		enableDropdowns = function() {
			for (var i = 0, len = _dropdowns.length; i < len; i += 1) {
				var dd = _dropdowns[i];
				dd.isEnabled = true;
			}
		};

		// disable the navigation (used by enquire to turn the navigation on/off)
		disableDropdowns = function() {
			for (var i = 0, len = _dropdowns.length; i < len; i += 1) {
				var dd = _dropdowns[i];

				if (dd.alwaysEnabled === false) {
					dd.isEnabled = false;
				}
			}
		};

		isInit = function() {
			return _isInit;
		};

		// initialiser
		init = function() {
			if (_isInit) {
				return;
			}

			if ($(OPTIONS.DROPDOWNS).length === 0) {
				return;
			}

			$(OPTIONS.DROPDOWNS).each(function(i, el) {
				var $nav = $(el),
					navid = 'dropdown-nav-' + i,
					$items = $nav.find('> li, > div'),
					alwaysEnabled = ($nav.attr('data-dropdown-enabled') === 'always'),
					disableBg = ($nav.attr('data-dropdown-bg') === 'false'),
					options = {
						isEnabled: true,
						alwaysEnabled: alwaysEnabled,
						disableBackground: disableBg
					},
					navBlurTimeout,
					resetNavBlurTimeout,
					openDropdown,
					closeDropdown,
					closeCurrentlyOpen,
					closeNavOnDocumentClick;

				_dropdowns.push(options);

				// open the dropdown
				openDropdown = function(id, instantly, currentItemHasDropdown) {
					var $link = $('#' + id),
						$dropdown = $link.find('.dropdown');

					$dropdown.find('.dropdown-close')
						.off('click.dropdown-close')
						.on('click.dropdown-close', function() {
							closeDropdown(id);
						});

					// by default the item selected currently doesn't have a dropdown
					currentItemHasDropdown = currentItemHasDropdown || false;

					// only show if the dropdown is enabled and if it hasn't already got a hover class on it
					if (options.isEnabled) {
						if (!$link.hasClass(CONST.CLASS.HOVER)) {

							// display the background only if there is a menu item with a dropdown selected
							if (!options.disableBackground && OPTIONS.HAS_BACKGROUND && $dropdown.length > 0) {
								$.ddShade.setActive(true);
								$.ddShade.setBehindHeader(true);
								$.ddShade.opacity(OPTIONS.BACKGROUND_OPACITY, OPTIONS.TRANSITION_DURATION, true);
							}

							// give the static animation only if it's IE8 and below, or if the currently opened item has a dropdown
							if (CONST.IS_LT_IE9 || (instantly && currentItemHasDropdown)) {
								$dropdown.attr('style', '');
								$link.addClass(CONST.CLASS.HOVER);
							} else {
								$dropdown.css({
									opacity: 0,
									'margin-left': 0
								});

								$dropdown.velocity({
									opacity: 1
								}, {
									duration: OPTIONS.TRANSITION_DURATION,
									complete: function() {
										$link.addClass(CONST.CLASS.HOVER);
										$dropdown.attr('style', '');
									}
								});
							}
						}
					}
				};

				// close the dropdown item
				closeDropdown = function(id, instantly, newItemHasDropdown) {
					var $link = $('#' + id),
						$dropdown = $link.find('.dropdown');

					if (!OPTIONS.DEBUG_MODE) {

						// by default the new item doesn't have a dropdown
						newItemHasDropdown = newItemHasDropdown || false;

						// give the static animation only if it's IE8 and below, or if requested (occurs when the dropdown is already open)
						if (CONST.IS_LT_IE9 || instantly) {
							$dropdown.attr('style', '');
							$link.removeClass(CONST.CLASS.HOVER).removeClass(CONST.CLASS.HOVERING);
						} else {
							$dropdown.css({
								opacity: 1,
								'margin-left': 0
							});

							$link.removeClass(CONST.CLASS.HOVER).removeClass(CONST.CLASS.HOVERING);

							$dropdown.velocity({
								opacity: 0
							}, OPTIONS.TRANSITION_DURATION, function() {
								$dropdown.attr('style', '');
							});
						}

						// handles the dropdown navigation background fadeout
						if (OPTIONS.HAS_BACKGROUND && (!instantly || !newItemHasDropdown)) {
							$.ddShade.opacity(0, OPTIONS.TRANSITION_DURATION, true, function() {
								$.ddShade.setActive(false);
								$.ddShade.setBehindHeader(false);
							});
						}

						// so touch devices can close the navigation easily
						$('body').off('touchstart', closeNavOnDocumentClick);
					}
				};

				// close the dropdown that's currently open
				closeCurrentlyOpen = function(callback, instantly, newItemHasDropdown) {
					newItemHasDropdown = newItemHasDropdown || false;

					$nav.find('> .' + CONST.CLASS.HOVER).each(function() {
						closeDropdown($(this).attr('id'), instantly, newItemHasDropdown);
					});

					if (typeof (callback) === 'function') {
						callback();
					}
				};

				// close on click outside of a navigation item
				closeNavOnDocumentClick = function(e) {
					if ($nav.find('> .' + CONST.CLASS.HOVER).find(e.target).length === 0) {
						e.preventDefault();
						closeCurrentlyOpen(null, false);
					}
				};

				resetNavBlurTimeout = function() {
					if (navBlurTimeout) {
						clearTimeout(navBlurTimeout);
						navBlurTimeout = null;
					}
				};

				$items.each(function(i, el) {
					var $item = $(el),
						id = navid + '-item-' + i;

					// assign a unique ID to the navigation
					$item.attr('id', id);

					if (Modernizr.touch) {
						// touch screens should be able to click to open the navigation, then click again to be taken to the navigation link
						$item.find('> a').on('touchstart.navdropdown', function(e) {
							// only allow if the navigation is enabled
							if (options.isEnabled) {
								if ($item.hasClass(CONST.CLASS.HOVER)) {
									// click through to the link
									return true;
								} else {
									e.preventDefault();

									// check if the previous and next items have a dropdown (used for the background blockout transition)
									var fromAnotherNavItem = ($nav.find('> .' + CONST.CLASS.HOVER).length > 0),
										currentItemHasDropdown = ($nav.find('> .' + CONST.CLASS.HOVER).find('.dropdown').length > 0),
										newItemHasDropdown = ($item.find('.dropdown').length > 0);

									$item.addClass(CONST.CLASS.HOVERING);

									// close the currently open item
									closeCurrentlyOpen(function() {
										openDropdown(id, fromAnotherNavItem, currentItemHasDropdown);
										$('body').on('touchstart.navdropdown', closeNavOnDocumentClick);
									}, fromAnotherNavItem, newItemHasDropdown);
								}
							}
						});

					} else {
						// Show hover state on menu item instantly on hover
						$item.on('mouseenter.navdropdown', function() {
							if (options.isEnabled) {
								$item.addClass(CONST.CLASS.HOVERING);
							}
						});

						// Remove hover state on menu item instantly when rolled off
						$item.on('mouseout.navdropdown', function(e) {
							$item.removeClass(CONST.CLASS.HOVERING);

							if (options.isEnabled) {
								if (!$item.hasClass(CONST.CLASS.HOVER)) {
									// check if the user has moved from one to another navigation item
									var toAnotherNavItem = ($nav.find(e.relatedTarget).length > 0);
									if (!toAnotherNavItem) {
										closeCurrentlyOpen(null, false);
									}
								}
							}
						});

						// Only show the dropdown when they really mean to hover on the menu item
						$item.hoverIntent({
							timeout: OPTIONS.TIMEOUT_DURATION,
							over: function(e) {
								if (options.isEnabled) {
									// check if the previous and next items have a dropdown (used for the background blockout transition)
									var fromAnotherNavItem = ($nav.find(e.relatedTarget).length > 0),
										currentItemHasDropdown = ($nav.find('> .' + CONST.CLASS.HOVER).find('.dropdown').length > 0),
										newItemHasDropdown = ($item.find('.dropdown').length > 0);

									if (fromAnotherNavItem) {
										closeCurrentlyOpen(function() {
											openDropdown(id, true, currentItemHasDropdown);
										}, true, newItemHasDropdown);
									} else {
										openDropdown(id, false);
									}
								}
							},
							out: function(e) {
								var toAnotherNavItem = ($nav.find(e.relatedTarget).length > 0);
								if (!toAnotherNavItem) {
									closeCurrentlyOpen(null, false);
								}
							}
						});

						// Show the dropdown on focus of links inside nav and dropdown - allows for keyboard navigation
						$item.find('a').on('focus.navdropdown', function() {
							resetNavBlurTimeout();

							if (options.isEnabled) {
								var currentItemHasDropdown = ($nav.find('> .' + CONST.CLASS.HOVER).find('.dropdown').length > 0),
									newItemHasDropdown = ($item.find('.dropdown').length > 0);

								closeCurrentlyOpen(function() {
									openDropdown(id, true, currentItemHasDropdown);
								}, true, newItemHasDropdown);
							}
						}).on('blur.navdropdown', function() {
							resetNavBlurTimeout();

							navBlurTimeout = setTimeout(function() {
								closeDropdown(id, false);
							}, OPTIONS.TIMEOUT_DURATION);
						});

						// Clear the blur timeout on click to prevent the dropdown from closing if a link inside the dropdown has focus and then loses focus because the user clicks inside the dropdown
						$item.on('click.navdropdown', function() {
							resetNavBlurTimeout();
						});
					}
				});
			});

			_isInit = true;
		};

		return {
			init: init,
			isInit: isInit,
			enable: enableDropdowns,
			disable: disableDropdowns,
			OPTIONS: OPTIONS
		};

	}());

}(DDIGITAL, jQuery));
// ==========================================================================
// FORM DECORATION
// ==========================================================================

(function(NAMESPACE) {

	'use strict';

	/**
	 * Form decorator which is responsible for decorating forms valid and invalid states
	 *
	 * @namespace formDecorator
	 * @memberOf DDIGITAL
	 */
	NAMESPACE.formDecorator = (function() {

		var init,
			CONST,
			highlight,
			unhighlight,
			placeError,
			removeError,
			getCtrlHolder,
			insertStatusIcon,
			removeStatusIcon,
			markAsRequired,
			unmarkAsRequired,
			cleanup,
			markAsValid,
			removeFromSummary;

		CONST = {
			TPL_REQUIRED: '<em>*<span class="visuallyhidden">Required field</span></em>',
			STATUS_ICONS: false,
			CLASSES: NAMESPACE.validate.getClasses()
		};

		init = function() {};

		/**
		 * Highlights the ctrl (and other controls if part of a group) when invalid
		 *
		 * @memberOf DDIGITAL.formDecorator
		 * @param {object} $ctrl
		 */
		highlight = function($ctrl) {
			var $ctrlHolder = getCtrlHolder($ctrl, true);

			$ctrl.attr('aria-invalid', true);

			if (!$ctrlHolder.hasClass(CONST.CLASSES.IGNORE)) {

				// Is this a control inside a group?
				if (NAMESPACE.validate.isInGroup($ctrl)) {
					// Yes, get a reference to the closest .ctrls-holder instead so that the status icon is injected
					// in that scope
					$ctrlHolder = getCtrlHolder($ctrl, false);
				}

				$ctrlHolder
					.removeClass(CONST.CLASSES.VALID)
					.addClass(CONST.CLASSES.ERROR);

				if (CONST.STATUS_ICONS === true) {
					insertStatusIcon($ctrlHolder);
				}
			}
		};

		/**
		 * Highlights invalid control
		 *
		 * Adds the error class on the .ctrl-holder or if the control is part of a group, on .ctrls-holder.
		 *
		 * @memberOf DDIGITAL.formDecorator
		 * @param {object} $ctrl
		 * @param {string} errorClass
		 * @param {string} validClass
		 */
		unhighlight = function($ctrl, errorClass, validClass) {
			var $ctrlHolder = getCtrlHolder($ctrl, true);

			$ctrl.removeAttr('aria-invalid');
			$ctrl.removeAttr('aria-describedby');

			removeError($ctrlHolder);

			if (!$ctrlHolder.hasClass(CONST.CLASSES.IGNORE)) {
				// Is this a control inside a group?
				if (NAMESPACE.validate.isInGroup($ctrl)) {
					// Yes, get a reference to the closest .ctrls-holder instead so that the status icon is injected
					// in that scope
					$ctrlHolder = getCtrlHolder($ctrl, false);
				}

				$ctrlHolder
					.removeClass(CONST.CLASSES.ERROR);

				// Valid class is an empty string when unhighlight is triggered on a form reset event, in which case
				// we don't want to mark the control as valid
				if (validClass !== '') {
					$ctrlHolder
						.addClass(CONST.CLASSES.VALID);

					if (CONST.STATUS_ICONS === true) {
						insertStatusIcon($ctrlHolder);
					}
				}
			}
		};

		/**
		 * @todo Write me
		 * @memberOf DDIGITAL.formDecorator
		 * @param {object} $ctrl
		 * @param {object} $error
		 */
		placeError = function($ctrl, $error) {
			var $ctrlHolder = getCtrlHolder($ctrl),
				errorId = $ctrl.attr('id') + '_error';

			$error.attr('id', errorId);
			$ctrl.attr('aria-describedby', errorId);
			$error.addClass(CONST.CLASSES.STATUS_MSG);

			// Remove error that might be hanging around
			removeError($ctrlHolder);

			// Check if is in group
			if (NAMESPACE.validate.isInGroup($ctrl)) {
				// Get closest .ctrls-holder
				$ctrlHolder = getCtrlHolder($ctrl, false);

				// Append to .ctrls-holder element
				$ctrlHolder
					.addClass(CONST.CLASSES.ERROR)
					.append($error);
			} else {
				$ctrlHolder.append($error);
			}
		};

		/**
		 * Removes the error message from a .ctrl-holder or .ctrls-holder
		 *
		 * @memberOf DDIGITAL.formDecorator
		 * @param {object} $ctrlHolder
		 */
		removeError = function($ctrlHolder) {
			// And remove error
			$ctrlHolder.find('.' + CONST.CLASSES.STATUS_MSG).remove();
		};

		/**
		 * Returns a reference to the closest .ctrl-holder for a form control. If the control is part of a group
		 * it returns a reference to the closest .ctrls-holder instead.
		 *
		 * @memberOf DDIGITAL.formDecorator
		 * @param {object} $labelOrElement
		 * @param {boolean} ctrlHolderOnly
		 * @return {object}
		 */
		getCtrlHolder = function($labelOrElement, ctrlHolderOnly) {
			ctrlHolderOnly = ctrlHolderOnly || false;

			if ($labelOrElement.closest('.' + CONST.CLASSES.CTRLSHOLDER).length > 0 && !ctrlHolderOnly) {
				return $labelOrElement.closest('.' + CONST.CLASSES.CTRLSHOLDER);
			}

			return $labelOrElement.closest('.' + CONST.CLASSES.CTRLHOLDER);
		};

		/**
		 * Inserts the markup that makes up the status icon
		 *
		 * @todo Clean me up
		 *
		 * @memberOf DDIGITAL.formDecorator
		 * @param {object} $ctrlHolder
		 */
		insertStatusIcon = function($ctrlHolder) {
			if ($ctrlHolder.find('.' + CONST.CLASSES.STATUS_ICON).length === 0) {

				if ($ctrlHolder.hasClass(CONST.CLASSES.CTRLSHOLDER)) {
					$ctrlHolder.find('.' + CONST.CLASSES.CTRLHOLDER + ':last-child').after('<div class="' + CONST.CLASSES.STATUS_ICON + '">&nbsp;</div>');
				} else if ($ctrlHolder.find('.simple-select').length > 0) {
					$ctrlHolder.find('.simple-select').after('<div class="' + CONST.CLASSES.STATUS_ICON + '">&nbsp;</div>');
				} else if ($ctrlHolder.find('.options').length > 0) {
					$ctrlHolder.find('.options').after('<div class="' + CONST.CLASSES.STATUS_ICON + '">&nbsp;</div>');
				} else if ($ctrlHolder.hasClass('is-fileupload') && $ctrlHolder.parent().find('.' + CONST.CLASSES.STATUS_ICON).length === 0) { // File upload control exception
					$ctrlHolder.parent().find('.file-upload').append('<div class="' + CONST.CLASSES.STATUS_ICON + '">&nbsp;</div>');
				} else {
					$ctrlHolder.find(':input').after('<div class="' + CONST.CLASSES.STATUS_ICON + '">&nbsp;</div>');
				}
			}
		};

		/**
		 * Removes the markup that makes up the status icon
		 *
		 * @memberOf DDIGITAL.formDecorator
		 * @param {object} $ctrl
		 */
		removeStatusIcon = function($ctrl) {
			var $ctrlHolder = getCtrlHolder($ctrl, true);
			$ctrlHolder.find('.' + CONST.CLASSES.STATUS_ICON).remove();
		};

		/**
		 * Removes the invalid state marks from a control (or a group if the referenced control is part of a group)
		 *
		 * @memberOf DDIGITAL.formDecorator
		 * @param $element
		 */
		cleanup = function($ctrl) {
			unhighlight($ctrl);
			if (CONST.STATUS_ICONS === true) {
				removeStatusIcon($ctrl);
			}
			removeFromSummary($ctrl);
		};

		/**
		 * Removes the required mark for a control
		 *
		 * @memberOf DDIGITAL.formDecorator
		 * @param {object} $ctrl
		 */
		unmarkAsRequired = function($ctrl) {
			var $ctrlHolder = getCtrlHolder($ctrl, true);
			$ctrlHolder
				.find('label > em')
				.remove();
		};

		/**
		 * Marks a control as required
		 *
		 * @memberOf DDIGITAL.formDecorator
		 * @param {object} $ctrl
		 */
		markAsRequired = function($ctrl) {
			var $ctrlHolder = getCtrlHolder($ctrl, true);
			$ctrlHolder
				.children('label')
				.append(CONST.TPL_REQUIRED);
		};

		/**
		 * Marks a control as valid.
		 *
		 * This is triggered from the success callback as passed to the jQuery validate init
		 *
		 * @todo Consider whether v_ignore behavior should be implemented again
		 * @memberOf DDIGITAL.formDecorator
		 * @param {object} $error
		 * @param {object} $ctrl
		 */
		markAsValid = function($error, $ctrl) {
			var $ctrlHolder = getCtrlHolder($ctrl);

			// If is in group, we get the ctrls-holder
			if (NAMESPACE.validate.isInGroup($ctrl)) {
				$ctrlHolder = getCtrlHolder($ctrl, false);
			}

			if (CONST.STATUS_ICONS === true) {
				insertStatusIcon($ctrlHolder);
			}

			$ctrlHolder
				.removeClass(CONST.CLASSES.ERROR)
				.addClass(CONST.CLASSES.VALID);
		};

		/**
		 * Removes validation message from summary
		 *
		 * @memberOf DDIGITAL.formDecorator
		 * @param {object} $element
		 */
		removeFromSummary = function($element) {
			$('.fn_validate_summary')
				.find('a[href="#' + $element.attr('id') + '"]')
				.closest('li')
				.remove();
		};

		return {
			init: init,
			highlight: highlight,
			unhighlight: unhighlight,
			placeError: placeError,
			removeError: removeError,
			getCtrlHolder: getCtrlHolder,
			insertStatusIcon: insertStatusIcon,
			removeStatusIcon: removeStatusIcon,
			markAsRequired: markAsRequired,
			unmarkAsRequired: unmarkAsRequired,
			cleanup: cleanup,
			markAsValid: markAsValid
		};

	}());

}(DDIGITAL));
(function(NAMESPACE) {

	'use strict';

	/**
	 * Error message dictionary
	 *
	 * @namespace errorMessages
	 * @memberOf DDIGITAL
	 */
		NAMESPACE.errorMessages = [{"ERR01":"This field is required.","ERR02":"Please enter a valid email address.","ERR03":"The email addresses you entered do not match.","ERR04":"Please enter a valid expiry date in mm yy format (e.g. 01 18)."}];

}(DDIGITAL));
// ==========================================================================
// FORM ERROR MESSAGES
// ==========================================================================

(function(NAMESPACE) {

	'use strict';

	/**
	 * Error message protocol
	 *
	 * @namespace error
	 * @memberOf DDIGITAL
	 */
	NAMESPACE.error = (function() {
		var getMessage;

		/**
		 * Returns error message
		 *
		 * @memberOf DDIGITAL.error
		 * @param {string} key Error mesage key (ERR01, ERR02, etc)
		 * @return {mixed}
		 */
		getMessage = function(key) {
			var message;

			if (NAMESPACE.errorMessages && NAMESPACE.errorMessages[0]) {
				message = NAMESPACE.errorMessages[0][key];
			}

			if (message === undefined && NAMESPACE.IS_DEBUG) {
				console.warn('Error message with key ' + key + ' not found.');
			}

			return message;
		};

		return {
			getMessage: getMessage
		};

	}());

}(DDIGITAL));
// ==========================================================================
// FORM ERROR RENDERER
// ==========================================================================

(function(NAMESPACE) {

	'use strict';

	/**
	 * Renders errors which were raised on the server side
	 *
	 * @description A protocol has been established to communicate errors that were raised on the server side but are
	 * rendered on the client side.
	 *
	 * @example
	 * window.forms = window.forms || {};
	 * window.forms['xx-form'] = window.forms['xx-form'] || {};
	 * window.forms['xx-form'].errors = '[{"ControlId": "text-field", "ErrorMsg": "I\'m from the server side"}, {"ControlId": "abn", "ErrorMsg": "Bad ABN"}]';
	 *
	 * @namespace errors
	 * @memberOf DDIGITAL
	 */
	NAMESPACE.errors = (function() {

		var init,
			CONST,
			_renderServerSideErrors;

		CONST = {
			CLASSES: NAMESPACE.validate.getClasses()
		};

		/**
		 * Picks up error messages communicated from the server side
		 *
		 * @memberOf DDIGITAL.errors
		 * @param {object} validator
		 * @private
		 */
		_renderServerSideErrors = function(validator) {
			var $form = $(validator.currentForm),
				formId = $form.data('form-id'),
				errors = [];

			try {
				errors = JSON.parse(window.forms[formId].errors);
			} catch (error) {}

			if (errors.length !== 0) {
				NAMESPACE.validate.renderErrorSummary(validator, errors);

				$.each(errors, function(key, value) {
					var $ctrl = $('#' + value.ControlId),
						id = $ctrl.attr('id') + '-error',
						$error = $(document.createElement('div'));

					// Simulate error message as generated by jQuery validate
					$error.append(
						$(document.createElement('span'))
							.attr('id', id)
							.addClass(CONST.CLASSES.ERROR)
							.text(value.ErrorMsg)
					);

					NAMESPACE.formDecorator.placeError($ctrl, $error);
					NAMESPACE.formDecorator.highlight($ctrl);
				});
			}
		};

		/**
		 * @memberOf DDIGITAL.errors
		 * @param {object} $scope Reference to element to use as ancestor for finding logical forms
		 */
		init = function($scope) {
			var $forms;

			$scope = $scope || $('body');

			// Get all logical forms
			$forms = $scope.find('.fn_validate');

			// Iterate each logical form
			$forms.each(function() {
				// Pick up server side errors if exist
				_renderServerSideErrors($.data(this, 'validator'));
			});
		};

		return {
			init: init
		};

	}());

}(DDIGITAL));
(function(NAMESPACE) {

	'use strict';

	// Email confirmation validators
	$.validator.addMethod('confirmemail', function(value, ctrl) {
		var confirm = $(ctrl).attr('data-validate-confirm') || false;

		if ($(confirm).length === 0) {
			// If the data attribute, or the item referenced can't be found, just return valid
			return true;
		}

		return this.optional(ctrl) || ($(ctrl).val() === $(confirm).val());
	}, NAMESPACE.error.getMessage('ERR03'));

	$.validator.addClassRules('v-confirmemail', {
		confirmemail: true
	});

}(DDIGITAL));
(function(NAMESPACE) {

	'use strict';

	$.validator.addMethod('email', function(value, element) {
		var regEmailRFC2822 = /^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/g,
			isEmail = regEmailRFC2822.test(value);

		return this.optional(element) || isEmail;
	}, NAMESPACE.error.getMessage('ERR02'));

	$.validator.addClassRules('v-email', {
		email: true
	});

}(DDIGITAL));
(function(NAMESPACE) {

	'use strict';

	$.validator.addMethod('expirydate', function(value, ctrl) {
		var valid = false,
			$ctrl = $(ctrl),
			$ctrls = NAMESPACE.validate.getCtrlsInGroup($ctrl),
			month = $ctrls.eq(0).val(),
			year = $ctrls.eq(1).val(),
			currentYear = new Date().getFullYear().toString().substr(2, 2) === year;

		if (month !== ''
			&& year !== ''
			&& !(currentYear && parseInt(month, 10) - 1 < new Date().getMonth())) {
			valid = true;
		}

		return valid;
	}, NAMESPACE.error.getMessage('ERR04'));

	$.validator.addClassRules('v-expirydate', {
		expirydate: true
	});

}(DDIGITAL));
(function(NAMESPACE) {

	'use strict';

	// Overwrite the required validator to have custom error messages
	$.validator.addMethod('required', function(value, element, param) {
		var $ctrl = $(element);

		// check if dependency is met
		if (!this.depend(param, element)) {
			return 'dependency-mismatch';
		}

		// Check if is in group, if any of the other elements already has a value, return true
		if (NAMESPACE.validate.isInGroup($ctrl)) {
			var $ctrls = NAMESPACE.validate.getCtrlsInGroup($ctrl),
				requirementMet = false;

			// Iterate all controls in the group
			$ctrls.each(function() {
				var _$ctrl = $(this);

				// For all but the control that this rule is executed for
				// @todo consider writing this without jQuery as per below
				if (_$ctrl.attr('id') !== $ctrl.attr('id')) {
					// Check if we're dealing with a checkbox/radio button and if so, is it checked?
					if (_$ctrl.is(':checkbox') || _$ctrl.is(':radio')) {
						if (_$ctrl.is(':checked')) {
							requirementMet = true;
						}
					} else {
						// Assume we're dealing with a select or text input
						if (_$ctrl.val() !== '' && _$ctrl.val() !== null) {
							requirementMet = true;
						}
					}
				}
			});

			if (requirementMet === true) {
				return true;
			}
		}

		if (element.nodeName.toLowerCase() === 'select') {
			// could be an array for select-multiple or a string, both are fine this way
			var val = $(element).val();
			return val && val.length > 0;
		}
		if (this.checkable(element)) {
			return this.getLength(value, element) > 0;
		}
		return $.trim(value).length > 0;
	}, function(params, element) {
		return $(element).attr('data-v-msg-required') || NAMESPACE.error.getMessage('ERR01');
	});

	$.validator.addClassRules('v-required', {
		required: true
	});

}(DDIGITAL));
window.DD_BUILD=window.DD_BUILD||{},window.DD_BUILD.NUMBER='28',window.DD_BUILD.DATETIME='19/07/2016 14:16 AEST',window.DD_BUILD.BRANCH='develop';
